package ue.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import ue.exception.InvalidArgumentsException;
import ue.util.calc.Calc;

public class ToolsTest {

  @Nested
  class Calculate {

    @Test
    void usesCorrectOperatorPriority() throws InvalidArgumentsException {
      assertEquals(61, Calc.calculate("10^2 + 5 / 5 - 4 * 10"), 0.002);
      assertEquals(0, Calc.calculate("1-2/1+1"), 0.002);
    }

    @Test
    void correctlyProcessesBrackets() throws InvalidArgumentsException {
      assertEquals(12, Calc.calculate("10*(10*(4*(5-2))/10^(1+1))"), 0.002);
      assertEquals(5, Calc.calculate("5*10^(1-2/1+1)+(10-10)"), 0.002);
    }

    @Test
    void preservesNumberLiteral() throws InvalidArgumentsException {
      assertEquals(9, Calc.calculate("9"), 0.002);
    }
  }
}
