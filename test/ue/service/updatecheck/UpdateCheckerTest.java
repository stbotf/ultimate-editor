package ue.service.updatecheck;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import lombok.Builder;
import lombok.Cleanup;
import lombok.val;
import reactor.test.StepVerifier;
import ue.UE;
import ue.service.SettingsManager;
import ue.util.app.AppVersion;

public class UpdateCheckerTest {

  // Notice, the AppVersion time stamp is truncated to minutes!
  private static final Instant startTime = Instant.now().truncatedTo(ChronoUnit.MINUTES);
  private static final String sampleLink = "https://www.google.com";

  private static final String releaseVString = "2.7.4";
  private static final AppVersion releaseVersion = AppVersion.builder()
    .versionTag(releaseVString)
    .downloadLink(sampleLink)
    .timeStamp(startTime)
    .build();

  private static final String alphaVString = "2.7.4dev3";
  private static final AppVersion alphaVersion = AppVersion.builder()
    .versionTag(alphaVString)
    .downloadLink(sampleLink)
    .timeStamp(startTime)
    .build();

  private static final String updateVString = "10.27.4";
  private static final AppVersion updateVersion = AppVersion.builder()
    .versionTag(updateVString)
    .downloadLink(sampleLink)
    .timeStamp(startTime)
    .build();

  private static final String previewVString = "10.27.4pre2";
  private static final AppVersion previewVersion = AppVersion.builder()
    .versionTag(previewVString)
    .downloadLink(sampleLink)
    .timeStamp(startTime)
    .build();

  @BeforeEach
  void beforeEach() {
    // TODO: mock properly after switching to beans
    UE.SETTINGS = new SettingsManager();
  }

  @Nested
  class UpdateExceptionTest {

    @Test
    void onAnInvalidUrl() {
      val updateChecker = new UpdateChecker("invalid url", 0);
      callCheckAndVerifyThrownError(updateChecker);
    }

    @Test
    void onNoData() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = fileSystem.getPath(UUID.randomUUID().toString());
      val updateChecker = new UpdateChecker(path.toUri().toURL().toString(), 0);
      callCheckAndVerifyThrownError(updateChecker);
    }

    private void callCheckAndVerifyThrownError(UpdateChecker updateChecker) {
      StepVerifier.create(updateChecker.checkForUpdates(true))
        .expectNextCount(0)
        .expectErrorMatches(throwable -> throwable instanceof UpdateCheckException)
        .verify();
    }
  }

  @Nested
  class ParseVersionTest {

    @Test
    void truncatedAppVersionTimeStamp() throws IOException {
      val time = Instant.now();
      val minutes = time.truncatedTo(ChronoUnit.MINUTES);
      val version = AppVersion.builder().versionTag(updateVString).timeStamp(time).build();

      assertEquals(minutes, version.getTimeStamp().get());
    }
  }

  @Nested
  class RssFeedTest extends ReleaseTest {

    @Test
    void onAnEmptyRssFeed() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, new ArrayList<>());
      val updateChecker = new UpdateChecker(path.toUri().toURL().toString(), 0);
      callCheckAndVerifyNoResult(updateChecker);
    }

    @Test
    void onMissingRssEntryTitle() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      val updateChecker = new UpdateChecker(path.toUri().toURL().toString(), 0);
      callCheckAndVerifyNoResult(updateChecker);
    }

    @Test
    void onInvalidRssEntryTitle() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version("abc")
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      val updateChecker = new UpdateChecker(path.toUri().toURL().toString(), 0);
      callCheckAndVerifyNoResult(updateChecker);
    }

    @Test
    void onMissingDownloadLink() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(updateVString)
          .downloadLink(null)
          .updated(startTime)
          .build()
      ));

      AppVersion expected = AppVersion.builder()
        .versionTag(updateVString)
        .downloadLink(null)
        .timeStamp(startTime)
        .build();

      callCheckAndVerify(path, false, 0, expected);
    }
  }

  @Nested
  class UpdateDelayTest extends ReleaseTest {

    @Test
    void onLastCheckBeingTooRecent() throws IOException {
      UE.SETTINGS.setOffsetDateTimeProperty(SettingsManager.LAST_CHECK_FOR_UPDATES, OffsetDateTime.now());

      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(updateVString)
          .downloadLink(null)
          .updated(startTime)
          .build()
      ));

      val updateChecker = new UpdateChecker(path.toUri().toURL().toString(), 1);
      callCheckAndVerifyNoResult(updateChecker);
    }

    @Test
    void onForceCheckAndCheckTooRecent() throws IOException {
      UE.SETTINGS.setOffsetDateTimeProperty(SettingsManager.LAST_CHECK_FOR_UPDATES, OffsetDateTime.now());

      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(updateVString)
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      callCheckAndVerify(path, true, 1, updateVersion);
    }
  }

  @Nested
  class ReleaseVersionUpdateTest extends ReleaseTest {

    @Test
    void onLatestVersionBeingEqualToCurrentVersion() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(UE.getVersion().getVersionTag())
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      val updateChecker = new UpdateChecker(path.toUri().toURL().toString(), 0);
      callCheckAndVerifyNoResult(updateChecker);
    }

    @Test
    void onSingleNewVersionAvailable() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(updateVString)
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      callCheckAndVerify(path, false, 0, updateVersion);
    }

    @Test
    void onMultipleVersionsAvailable() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Arrays.asList(
        RssEntry.builder()
          .version("3.2.0")
          .updated(startTime.plus(5, ChronoUnit.DAYS))
          .build(),
        RssEntry.builder()
          .version(updateVString)
          .downloadLink(sampleLink)
          .updated(startTime)
          .build(),
        RssEntry.builder()
          .version("2.8.9")
          .updated(startTime.plus(20, ChronoUnit.DAYS))
          .build()
      ));

      // check for the version number, not the release date
      callCheckAndVerify(path, false, 0, updateVersion);
    }

    @Test
    void onAlphaVersionAvailable() throws IOException {
      UE.SETTINGS.setProperty(SettingsManager.INCLUDE_DEV_UPDATES, true);

      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(previewVersion.getVersionString())
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      callCheckAndVerify(path, false, 0, previewVersion);
    }
  }

  @Nested
  class AlphaVersionUpdateTest extends AlphaTest {

    @Test
    void onObsoletedByRelease() throws IOException {
      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(releaseVString)
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      callCheckAndVerify(path, false, 0, releaseVersion);
    }

    @Test
    void onExtraordinaryVersionTitle() throws IOException {
      UE.SETTINGS.setProperty(SettingsManager.INCLUDE_DEV_UPDATES, true);

      @Cleanup val fileSystem = Jimfs.newFileSystem(Configuration.forCurrentPlatform());
      val versionName = "abc_12.40.105_!#$%MKF";
      val path = createPathToFeedXml(fileSystem, Collections.singletonList(
        RssEntry.builder()
          .version(versionName)
          .downloadLink(sampleLink)
          .updated(startTime)
          .build()
      ));

      AppVersion expected = AppVersion.builder()
        .versionTag(versionName)
        .downloadLink(sampleLink)
        .timeStamp(startTime)
        .build();

      callCheckAndVerify(path, false, 0, expected);
    }
  }

  @TestInstance(Lifecycle.PER_CLASS)
  private class ReleaseTest {

    private final MockedStatic<UE> ueMock = Mockito.mockStatic(UE.class);

    @BeforeAll
    void beforeAll() {
      ueMock.when(UE::getVersion).thenReturn(releaseVersion);
    }

    @AfterAll
    void afterAll() {
      ueMock.close();
    }
  }

  @TestInstance(Lifecycle.PER_CLASS)
  private class AlphaTest {

    private final MockedStatic<UE> ueMock = Mockito.mockStatic(UE.class);

    @BeforeAll
    void beforeAll() {
      ueMock.when(UE::getVersion).thenReturn(alphaVersion);
    }

    @AfterAll
    void afterAll() {
      ueMock.close();
    }
  }

  private void callCheckAndVerify(Path pathToRssFeed, boolean forceCheck,
      int minDaysBetweenUpdateChecks, AppVersion expectedValue)
      throws MalformedURLException {
    val url = pathToRssFeed.toUri().toURL();

    val updateChecker = new UpdateChecker(url.toString(), minDaysBetweenUpdateChecks);
    val update = updateChecker.checkForUpdates(forceCheck);

    StepVerifier.create(update)
      .expectNext(expectedValue)
      .verifyComplete();
  }

  private void callCheckAndVerifyNoResult(UpdateChecker updateChecker) {
    val updateInfo = updateChecker.checkForUpdates(false);
    StepVerifier.create(updateInfo)
      .expectNextCount(0)
      .verifyComplete();
  }

  private Path createPathToFeedXml(FileSystem fileSystem, List<RssEntry> rssEntries)
      throws IOException {
    val path = fileSystem.getPath(UUID.randomUUID().toString());
    Files.write(path, createFeedXml(rssEntries).getBytes(StandardCharsets.UTF_8));
    return path;
  }

  private String createFeedXml(List<RssEntry> rssEntries) {
    val stringBuilder = new StringBuilder();
    stringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    stringBuilder.append("<feed xmlns=\"http://www.w3.org/2005/Atom\">");
    stringBuilder.append("<title>Ultimate Editor tags</title>");
    stringBuilder.append("<id>https://gitlab.com/stbotf/ultimate-editor/-/tags</id>");

    for (val rssEntry : rssEntries) {
      stringBuilder.append(rssEntry.toXmlString());
    }

    stringBuilder.append("</feed>");
    return stringBuilder.toString();
  }

  @Builder
  private static class RssEntry {

    private final String downloadLink;
    private final String version;
    private final Instant updated;

    public String toXmlString() {
      val stringBuilder = new StringBuilder();
      stringBuilder.append("<entry><id>").append(UUID.randomUUID()).append("</id>");

      if (!Objects.isNull(downloadLink))
        stringBuilder.append("<link href=\"").append(downloadLink).append("\"/>");
      if (!Objects.isNull(version))
        stringBuilder.append("<title>").append(version).append("</title>");

      stringBuilder.append("<updated>").append(updated.toString())
          .append("</updated></entry>");

      return stringBuilder.toString();
    }
  }

}
