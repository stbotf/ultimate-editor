package ue.audio;

import java.io.InputStream;
import java.util.concurrent.locks.ReentrantLock;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.SourceDataLine;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;

class AudioPlayer {

  private Playback playback;
  private Thread runner;
  private final ReentrantLock playerLock = new ReentrantLock();

  // stops current playback and starts playback of the new stream
  // closes the stream once playback is complete or cancelled
  public void playFromStream(InputStream inputStream) {
    playerLock.lock();

    try {
      stopPlayback();
      startPlayback(inputStream);
    } finally {
      playerLock.unlock();
    }
  }

  private void stopPlayback() {
    if (playback != null) {
      playback.stop();
      playback = null;
      runner = null;
    }
  }

  // launch a new thread to start playback and automatically
  // close the inputStream once complete or cancelled
  private void startPlayback(InputStream inputStream) {
    // since playback is run by a thread, pass the stream ownership
    // to make sure it is closed
    playback = new Playback(inputStream, true);
    runner = new Thread(playback);
    runner.start();
  }

  @RequiredArgsConstructor
  private static class Playback implements Runnable {

    private AudioInputStream audioStream;
    private SourceDataLine dataLine;
    private boolean stop = false;
    private boolean ownsStream = false;

    @SneakyThrows
    public Playback(InputStream inputStream, boolean ownsStream) {
      this.audioStream = AudioSystem.getAudioInputStream(inputStream);
      this.ownsStream = ownsStream;

      val format = audioStream.getFormat();
      val info = new DataLine.Info(SourceDataLine.class, format);
      dataLine = (SourceDataLine) AudioSystem.getLine(info);
      dataLine.open(format);

      dataLine.addLineListener(new LineListener() {
        @Override
        public void update(LineEvent e) {
          if (e.getType() == LineEvent.Type.STOP)
            e.getLine().close();
        }
      });
    }

    @SneakyThrows
    @Override
    public void run() {
      try {
        val buffer = new byte[524288]; // 128 kB
        dataLine.start();

        while (!stop && audioStream.available() > 0) {
          val bytesRead = audioStream.read(buffer);
          int bytesWritten = 0;

          do {
            bytesWritten += dataLine.write(buffer, bytesWritten, bytesRead - bytesWritten);
          } while (!stop && bytesWritten < bytesRead);
        }
      } finally {
        if (ownsStream)
          audioStream.close();
      }
    }

    public void stop() {
      if (!stop) {
        stop = true;
        dataLine.stop();
        dataLine.flush();
      }
    }
  }
}
