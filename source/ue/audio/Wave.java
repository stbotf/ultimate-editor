package ue.audio;

import java.io.IOException;
import java.io.OutputStream;
import ue.audio.WaveHeader.WaveFormat;
import ue.exception.UnsupportedWave;
import ue.service.Language;

public class Wave {

  public interface WaveType {
    public static final int English = 1;
    public static final int Sfx = 2;
    public static final int Music = 3;
  }


  // data
  private WaveHeader header;
  private int SNDTYPE;
  private byte[] DATA;

  /**
   * Constructor to insert raw data.
   * @param sndType the wave type: english, sfx, music
   * @param data    the raw content data
   * @param frames  the number of data frames
   */
  public Wave(int sndType, byte[] data, int frames) throws UnsupportedWave {
    SNDTYPE = sndType;
    header = new WaveHeader();

    switch (sndType) {
      case WaveType.English: {
        //english.snd
        header.fmtLength = 20;
        header.format = 17;
        header.channels = 1;
        header.bytesPerSecond = 11100;
        header.blockAlignment = 512;
        header.bitsPerSample = 4;
        header.extraFmt = 2;
        header.samplesPerBlock = 1017;
        header.frames = frames / 4;
        break;
      }
      case WaveType.Music: {
        //music.snd
        header.fmtLength = 20;
        header.format = 17;
        header.channels = 2;
        header.bytesPerSecond = 22201;
        header.blockAlignment = 1024;
        header.bitsPerSample = 4;
        header.extraFmt = 2;
        header.samplesPerBlock = 1017;
        header.frames = frames / 4;
        break;
      }
      case WaveType.Sfx: {
        //sfx.snd
        header.fmtLength = 16;
        header.format = 1;
        header.channels = 1;
        header.bytesPerSecond = 44100;
        header.blockAlignment = 2;
        header.bitsPerSample = 16;
        header.frames = frames / 4;
        break;
      }
      default: {
        throw new UnsupportedWave(Language.getString("Wave.0")); //$NON-NLS-1$
      }
    }

    header.frequency = 22050;
    header.setDataSize(data.length);

    DATA = new byte[data.length];
    System.arraycopy(data, 0, DATA, 0, data.length);
  }

  /**
   * Constructor to read full file data.
   * @param data the full file data to read
   */
  public Wave(byte[] data) throws IOException {
    header = new WaveHeader(data);
    header.check(data.length);
    determineSoundType();

    //copy data
    int headerSize = header.size();
    DATA = new byte[data.length - headerSize];
    System.arraycopy(data, headerSize, DATA, 0, data.length - headerSize);
  }

  /**
   * Constructor to ensure sound type.
   * @param data the full file data to read
   */
  public Wave(int sndType, byte[] data) throws IOException {
    this(data);
    if (SNDTYPE != sndType)
      throw new IOException(Language.getString("Wave.1")); //$NON-NLS-1$
  }

  private void determineSoundType() throws IOException {
    switch (header.format) {
      case WaveFormat.DVI_IMA_ADPCM: {
        switch (header.channels) {
          case 1:
            SNDTYPE = WaveType.English; // english/german.snd
            break;
          case 2:
            SNDTYPE = WaveType.Music;   // music.snd
            break;
          default:
            throw new IOException(Language.getString("Wave.2")); //$NON-NLS-1$
        }
        break;
      }
      case WaveFormat.PCM:
        SNDTYPE = WaveType.Sfx;         // sfx.snd
        break;
      default:
        throw new IOException(Language.getString("Wave.3")); //$NON-NLS-1$
    }
  }

  /**
   * Saves data to OutputStream
   */
  public void save(OutputStream out) throws IOException {

    switch (SNDTYPE) {
      case WaveType.Sfx:
      case WaveType.English:
      case WaveType.Music:
        break;
      default:
        throw new IOException(Language.getString("Wave.4")); //$NON-NLS-1$
    }

    header.save(out);
    out.write(DATA);
  }

  public byte[] getRawData() {
    byte[] data = new byte[DATA.length];
    System.arraycopy(DATA, 0, data, 0, DATA.length);
    return data;
  }

  public int getDataSize() {
    return DATA.length;
  }

  public int getSize() {
    return header.size() + DATA.length;
  }

  public int getFrames() {
    return header.frames * 4;
  }

  public int getSndType() {
    return SNDTYPE;
  }
}
