package ue.audio;

import java.io.IOException;
import java.io.OutputStream;
import ue.service.Language;
import ue.util.data.DataTools;

public class WaveHeader {

  public interface WaveFormat {
    public static final int PCM = 1;
    public static final int MS_ADPCM  = 2;
    public static final int IEEE_FLOAT  = 3;
    public static final int IBM_CVSD  = 5;
    public static final int ALAW  = 6;
    public static final int MULAW  = 7;
    public static final int OKI_ADPCM  = 16;
    public static final int DVI_IMA_ADPCM  = 17;
    // ...
    public static final int EXTENSIBLE = 0xFFFE;
  }

  public static final int STR_RIFF = 0x46464952; // "RIFF"
  public static final int STR_WAVE = 0x45564157; // "WAVE"
  public static final int STR_FMT =  0x20746D66; // "fmt "
  public static final int STR_FACT = 0x74636166; // "FACT"
  public static final int INT_FACT = 4;
  public static final int STR_DATA = 0x61746164; // "DATA"

  // riff header
  public int strRiff;           // "RIFF"
  public int riffSize;          // file size - 8 bytes

  // wave header
  public int strWave;           // "WAVE"
  public int strFormat;         // "fmt "
  public int fmtLength;         // format header length

  // format header
  public short format;          // data format tag (1=PCM, 17=DVI/IMA ADPCM,...)
  public short channels;
  public int frequency;         // sample rate
  public int bytesPerSecond;
  public short blockAlignment;
  public short bitsPerSample;

  // DVI/IMA ADPCM header (fmtLength == 20)
  public short extraFmt;
  public short samplesPerBlock;

  // fact chunk
  // all non-PCM formats must have a fact chunk
  public int strFact;           // "FACT"
  public int intFact;
  public int frames;            // frames / 4

  // data header
  public int strData;           // "data"
  public int dataSize;          // data size

  public WaveHeader() {
    strRiff = STR_RIFF;
    strWave = STR_WAVE;
    strFormat = STR_FMT;
    strFact = STR_FACT;
    intFact = INT_FACT;
    strData = STR_DATA;
  }

  public WaveHeader(byte[] data) {
    load(data);
  }

  public int size() {
    switch (format) {
      // PCM:       44 = 20 (riff+wave header) + 16 (format header) + 8 (data header)
      case WaveFormat.PCM:  return 44;
      // IMA ADPCM: 60 = 20 (riff+wave header) + 20 (format header) + 12 (fact chunk) + 8 (data header)
      default:              return 40 + fmtLength;
    }
  }

  public void setDataSize(int size) {
    dataSize = size;
    riffSize = size + size() - 8;
  }

  public void load(byte[] data) {
    int index = 0;

    // RIFF header
    // RIFF                 (4)
    strRiff = DataTools.toInt(data, index, true);
    index += 4;
    // size - 8             (8)
    riffSize = DataTools.toInt(data, index, true);
    index += 4;

    // WAVE header
    // WAVE                 (12)
    strWave = DataTools.toInt(data, index, true);
    index += 4;
    // FMT                  (16)
    strFormat = DataTools.toInt(data, index, true);
    index += 4;
    // format header size   (20)
    fmtLength = DataTools.toInt(data, index, true);
    index += 4;

    // format header
    // format               (22)
    format = DataTools.toShort(data, index, true);
    index += 2;
    // channels             (24)
    channels = DataTools.toShort(data, index, true);
    index += 2;
    // frequency            (28)
    frequency = DataTools.toInt(data, index, true);
    index += 4;
    // bytes per second     (32)
    bytesPerSecond = DataTools.toInt(data, index, true);
    index += 4;
    // block alignment      (34)
    blockAlignment = DataTools.toShort(data, index, true);
    index += 2;
    // bits per sample      (36)
    bitsPerSample = DataTools.toShort(data, index, true);
    index += 2;

    // format specific header
    switch (format) {
      case WaveFormat.DVI_IMA_ADPCM:
        // extra chunk size   (38)
        extraFmt = DataTools.toShort(data, index, true);
        index += 2;
        // samples per block  (40)
        samplesPerBlock = DataTools.toShort(data, index, true);
        index += 2;
        break;
      case WaveFormat.PCM:
      default:
        // skip extra header length
        index += fmtLength - 16;
        break;
    }

    // fact chunk
    if (format != WaveFormat.PCM) {
      // FACT               (44)
      strFact = DataTools.toInt(data, index, true);
      index += 4;
      // intFACT            (48)
      intFact = DataTools.toInt(data, index, true);
      index += 4;
      // frames / 4         (52)
      frames = DataTools.toInt(data, index, true);
      index += 4;
    }

    // data header
    // DATA                 (40/56)
    strData = DataTools.toInt(data, index, true);
    index += 4;
    // data size            (44/60)
    dataSize = DataTools.toInt(data, index, true);
  }

  public void save(OutputStream out) throws IOException {
    // riff header
    // RIFF
    out.write(DataTools.toByte(STR_RIFF, true));
    // size - 8           (4)
    out.write(DataTools.toByte(dataSize + size() - 8, true));

    // wave header
    // WAVE               (8)
    out.write(DataTools.toByte(STR_WAVE, true));
    // FMT                (12)
    out.write(DataTools.toByte(STR_FMT, true));
    // format header size (16)
    out.write(DataTools.toByte(fmtLength, true));

    // format header
    // format             (18)
    out.write(DataTools.toByte(format, true));
    // channels         (20)
    out.write(DataTools.toByte(channels, true));
    // frequency        (24)
    out.write(DataTools.toByte(frequency, true));
    // bytes per sec    (28)
    out.write(DataTools.toByte(bytesPerSecond, true));
    // block alignment  (30)
    out.write(DataTools.toByte(blockAlignment, true));
    // bits per sample  (32)
    out.write(DataTools.toByte(bitsPerSample, true));

    // format specific header
    switch (format) {
      case WaveFormat.DVI_IMA_ADPCM:
        // extra chunk size
        out.write(DataTools.toByte(extraFmt, true));
        // samples per block
        out.write(DataTools.toByte(samplesPerBlock, true));
        break;
      case WaveFormat.PCM:
        break;
      default:
        throw new UnsupportedOperationException();
    }

    // fact chunk
    if (format != WaveFormat.PCM) {
      // FACT
      out.write(DataTools.toByte(STR_FACT, true));
      // intFACT
      out.write(DataTools.toByte(INT_FACT, true));
      // frames
      out.write(DataTools.toByte(frames, true));
    }

    // data header
    // DATA
    out.write(DataTools.toByte(STR_DATA, true));
    // data size
    out.write(DataTools.toByte(dataSize, true));
  }

  public void check(int size) throws IOException {
    if (STR_RIFF != strRiff)
      throw new IOException(Language.getString("WaveHeader.1")); //$NON-NLS-1$
    if (size - 8 != riffSize)
      throw new IOException(Language.getString("WaveHeader.2")); //$NON-NLS-1$
    if (STR_WAVE != strWave)
      throw new IOException(Language.getString("WaveHeader.3")); //$NON-NLS-1$
    if (STR_FMT != strFormat)
      throw new IOException(Language.getString("WaveHeader.4")); //$NON-NLS-1$

    switch (format) {
      case WaveFormat.PCM: {
        if (16 != fmtLength)
          throw new IOException(Language.getString("WaveHeader.27")); //$NON-NLS-1$
        if (channels != 1)
          throw new IOException(Language.getString("WaveHeader.28")); //$NON-NLS-1$
        if (frequency != 22050)
          throw new IOException(Language.getString("WaveHeader.6")); //$NON-NLS-1$
        if (bytesPerSecond != 44100)
          throw new IOException(Language.getString("WaveHeader.30")); //$NON-NLS-1$
        if (blockAlignment != 2)
          throw new IOException(Language.getString("WaveHeader.31")); //$NON-NLS-1$
        if (bitsPerSample != 16)
          throw new IOException(Language.getString("WaveHeader.32")); //$NON-NLS-1$
        break;
      }
      case WaveFormat.DVI_IMA_ADPCM: {
        //english and music
        if (20 != fmtLength)
          throw new IOException(Language.getString("WaveHeader.5")); //$NON-NLS-1$
        if (frequency != 22050)
          throw new IOException(Language.getString("WaveHeader.6")); //$NON-NLS-1$
        if (bitsPerSample != 4)
          throw new IOException(Language.getString("WaveHeader.9")); //$NON-NLS-1$
        if (extraFmt != 2)
          throw new IOException(Language.getString("WaveHeader.10")); //$NON-NLS-1$
        if (samplesPerBlock != 1017)
          throw new IOException(Language.getString("WaveHeader.11")); //$NON-NLS-1$
        if (STR_FACT != strFact)
          throw new IOException(Language.getString("WaveHeader.12")); //$NON-NLS-1$
        if (INT_FACT != intFact)
          throw new IOException(Language.getString("WaveHeader.13")); //$NON-NLS-1$

        switch (channels) {
          case 1: {
            //english.snd
            if (bytesPerSecond != 11100)
              throw new IOException(Language.getString("WaveHeader.7")); //$NON-NLS-1$
            if (blockAlignment != 512)
              throw new IOException(Language.getString("WaveHeader.8")); //$NON-NLS-1$
            break;
          }
          case 2: {
            //music.snd
            if (bytesPerSecond != 22201)
              throw new IOException(Language.getString("WaveHeader.17")); //$NON-NLS-1$
            if (blockAlignment != 1024)
              throw new IOException(Language.getString("WaveHeader.18")); //$NON-NLS-1$
            break;
          }
          default:
            throw new IOException(Language.getString("WaveHeader.26")); //$NON-NLS-1$
        }
        break;
      }
      default:
        throw new IOException(Language.getString("WaveHeader.35")); //$NON-NLS-1$
    }

    if (STR_DATA != strData)
      throw new IOException(Language.getString("WaveHeader.14")); //$NON-NLS-1$
    if (size - size() < dataSize)
      throw new IOException(Language.getString("WaveHeader.15")); //$NON-NLS-1$
  }
}
