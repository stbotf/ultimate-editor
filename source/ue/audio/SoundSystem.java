package ue.audio;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Used to play sound.
 */
public class SoundSystem {

  private static AudioPlayer audioPlayer = null;

  /**
   * Reads and plays sound from the given input stream.
   * The input stream will be closed once playback stops.
   *
   * @param inputStream the input stream to read from.
   * @throws IOException
   * @throws LineUnavailableException
   * @throws UnsupportedAudioFileException
   */
  public static void playSound(InputStream inputStream) {
    if (SoundSystem.audioPlayer == null)
      SoundSystem.audioPlayer = new AudioPlayer();

    SoundSystem.audioPlayer.playFromStream(inputStream);
  }

  public static void playSound(byte[] data) {
    playSound(new ByteArrayInputStream(data));
  }
}
