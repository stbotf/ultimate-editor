package ue.exception;

import java.io.PrintStream;

/**
 * @author Alan
 */
public abstract class StackTracePrinter {

  public static void printStackTrace(Throwable e, PrintStream dest, boolean filter) {
    if (filter) {
      StackTraceElement[] trace = e.getStackTrace();

      int i;
      for (i = trace.length - 1; i >= 0; i--) {
        if (trace[i].getClassName().startsWith("ue")) {
          break;
        }
      }

      StackTraceElement[] newtrace = new StackTraceElement[i + 1];
      System.arraycopy(trace, 0, newtrace, 0, i + 1);

      e.setStackTrace(newtrace);
      e.printStackTrace(dest);
    } else {
      e.printStackTrace(dest);
    }
  }
}
