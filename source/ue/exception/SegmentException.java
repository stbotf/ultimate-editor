package ue.exception;

/**
 * An exception thrown by classes that work with segments.
 */
public class SegmentException extends DataException {

  public SegmentException() {
    super();
  }

  public SegmentException(String message) {
    super(message);
  }

  public SegmentException(String message, Throwable cause) {
    super(message, cause);
  }

  public SegmentException(Throwable cause) {
    super(cause);
  }
}
