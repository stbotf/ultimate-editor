package ue.exception;

import java.io.IOException;

/**
 * An exception thrown by classes that work with saved games.
 */
public class SavException extends IOException {

  public SavException() {
    super();
  }

  public SavException(String message) {
    super(message);
  }

  public SavException(String message, Throwable cause) {
    super(message, cause);
  }

  public SavException(Throwable cause) {
    super(cause);
  }
}