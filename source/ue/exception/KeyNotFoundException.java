package ue.exception;

/**
 * Thrown when an expected Map or Set collection key is missing.
 *
 * This is a checked exception that enforces exception handling.
 * For an unchecked alternative you might opt to java.util.NoSuchElementException.
 */
public class KeyNotFoundException extends InvalidArgumentsException {

  public KeyNotFoundException() {
    super();
  }

  public KeyNotFoundException(String message) {
    super(message);
  }

  public KeyNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public KeyNotFoundException(Throwable cause) {
    super(cause);
  }
}