package ue.exception;

import java.io.IOException;

/**
 * When a wave file is processed that is not supported this exception may be thrown.
 */
public class UnsupportedWave extends IOException {

  public UnsupportedWave() {
    super();
  }

  public UnsupportedWave(String message) {
    super(message);
  }

  public UnsupportedWave(String message, Throwable cause) {
    super(message, cause);
  }

  public UnsupportedWave(Throwable cause) {
    super(cause);
  }
}
