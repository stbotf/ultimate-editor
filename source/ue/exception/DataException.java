package ue.exception;

import java.io.IOException;

/**
 * An exception thrown by classes that work with data files.
 */
public class DataException extends IOException {

  public DataException() {
    super();
  }

  public DataException(String message) {
    super(message);
  }

  public DataException(String message, Throwable cause) {
    super(message, cause);
  }

  public DataException(Throwable cause) {
    super(cause);
  }
}
