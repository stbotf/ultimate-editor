package ue.exception;

/**
 * If invalid argument(s) have been passed to an object this exception may be thrown.
 *
 * This is a checked exception that enforces exception handling.
 * For an unchecked alternative you might opt to java.lang.IllegalArgumentException.
 */
public class InvalidArgumentsException extends Exception {

  public InvalidArgumentsException() {
    super();
  }

  public InvalidArgumentsException(String message) {
    super(message);
  }

  public InvalidArgumentsException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidArgumentsException(Throwable cause) {
    super(cause);
  }
}