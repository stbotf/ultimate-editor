package ue.exception;

import java.io.IOException;

public class CorruptedFile extends IOException {

  public CorruptedFile() {
    super();
  }

  public CorruptedFile(String message) {
    super(message);
  }

  public CorruptedFile(String message, Throwable cause) {
    super(message, cause);
  }

  public CorruptedFile(Throwable cause) {
    super(cause);
  }
}