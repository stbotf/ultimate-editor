package ue.exception;

import java.io.IOException;

/**
 * When an image is processed that is not supported this exception may be thrown.
 */
public class UnsupportedImage extends IOException {

  public UnsupportedImage() {
    super();
  }

  public UnsupportedImage(String message) {
    super(message);
  }

  public UnsupportedImage(String message, Throwable cause) {
    super(message, cause);
  }

  public UnsupportedImage(Throwable cause) {
    super(cause);
  }
}