package ue;

import static org.apache.commons.lang3.ArrayUtils.addFirst;
import java.awt.Image;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.Objects;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.ArrayUtils;
import lombok.Getter;
import lombok.val;
import reactor.core.scheduler.Schedulers;
import reactor.util.annotation.NonNull;
import ue.gui.common.MainWindow;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.updatecheck.UpdateChecker;
import ue.util.app.AppVersion;

/**
 * Main class.
 *
 * <p>Used to run the editor.
 */
public class UE {

  private static final String CURRENT_VERSION = "0.9.0dev3";
  private static final LocalDate CURRENT_VERSION_DATE = LocalDate.of(2023, 3, 31);
  private static final LocalTime CURRENT_VERSION_TIME = LocalTime.of(9, 28);
  private static final ZoneId CURRENT_VERSION_ZONE = ZoneId.of("UTC+2");
  private static final ZonedDateTime CURRENT_VERSION_RELEASE = ZonedDateTime.of(CURRENT_VERSION_DATE, CURRENT_VERSION_TIME, CURRENT_VERSION_ZONE);

  @Getter
  private static final AppVersion Version = new AppVersion(CURRENT_VERSION,
    null, CURRENT_VERSION_RELEASE.toInstant());

  // since this class is static, the strings need to be looked up again
  // for in case the settings changed
  private static final String HELPED_MODDING = "UE.0";
  private static final String HELPED_LZSS = "UE.1";
  private static final String HELPED_GERMAN = "UE.2";
  private static final String HELPED_SND_DESC = "UE.3";
  private static final String HELPED_BUILDVIEW2 = "UE.4";
  private static final String NO_UPDATE = "UE.7";
  private static final String UPDATE_TITLE = "UE.8";
  private static final String DISABLE_UPDATES = "UE.9";
  private static final String REMIND_LATER = "UE.10";
  private static final String DOWNLOAD = "UE.11";
  private static final String UPDATE_FOUND = "UE.12";

  public static final String APP_NAME = "Ultimate Editor"; //$NON-NLS-1$
  public static final String[] APP_AUTHORS = new String[]{"DCER", "Flocke", "Lathon",
      "Thunderchero"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

  // alphabetic order (people first, websites second)
  public static final String[] APP_HELPERS = new String[]{
    Language.getString(HELPED_MODDING).replace("%1", "Gowron"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_BUILDVIEW2).replace("%1", "Gowron"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_MODDING).replace("%1", "Jigalypuff"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_MODDING).replace("%1", "Joker"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_MODDING).replace("%1", "Peter1981"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_MODDING).replace("%1", "QuasarDonkey"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_LZSS).replace("%1", "Riky S."), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_GERMAN).replace("%1", "Siggi"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_MODDING).replace("%1", "Spocks-cuddly-tribble"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_SND_DESC).replace("%1", "WhiteEagle"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_MODDING).replace("%1", "ArmadaFleetCommand Webpage"), //$NON-NLS-1$ //$NON-NLS-2$
    Language.getString(HELPED_MODDING).replace("%1", "BotF-cooperation Webpage") //$NON-NLS-1$ //$NON-NLS-2$
  };

  public static final String UPDATE_URL = "https://gitlab.com/stbotf/ultimate-editor/-/tags?format=atom"; //$NON-NLS-1$

  public enum DebugFlags {
    AniOrCur,
    TurnResult,
    Order,
    EconomicOrder,
    MilitaryOrder,
    WonderBuildOrder
  }

  public static final boolean DEBUG_MODE = true;
  public static final EnumSet<DebugFlags> DEBUG_FLAGS = EnumSet.noneOf(DebugFlags.class);

  /*
   * SettingsManager, FileManager and the MainWindow are
   * static and accessible from other classes.
   */
  /**
   * Global settings.
   */
  public static SettingsManager SETTINGS = null;
  /**
   * Global file manager.
   */
  public static FileManager FILES = null;
  /**
   * Main UE window.
   */
  public static MainWindow WINDOW = null;
  /**
   * Update checker.
   */
  public static UpdateChecker UPDATE_CHECKER = null;
  /**
   * Editor icon.
   */
  public static Image ICON = null;

  private static boolean awaitUpdate = false;

  /**
   * Initializes a SettingsManager and a FileManager, then creates a MainWindow instance.
   *
   * @param args UE has no optional parameters to use
   */
  public static void main(String[] args) throws Exception {
    try {
      // load settings
      UE.SETTINGS = new SettingsManager();

      // detect missing settings for first application start
      if (!UE.SETTINGS.foundSettings()) {
        System.out.println("No " + SettingsManager.SETTINGS_INI + " file found, loaded default settings.");
      }

      // sets the error log file
      val logFilePath = UE.SETTINGS.getProperty(SettingsManager.LOG_FILE);

      if (!logFilePath.isEmpty()) {
        val logFile = FileManager.getWritableLocalFile(logFilePath);
        System.setErr(new PrintStream(new FileOutputStream(logFile, true)));
        System.out.println("Log file: " + logFile.getAbsolutePath());
      }

      System.out.println("Debug mode: " + ((UE.DEBUG_MODE) ? "ON" : "OFF"));

      // set default exception handler
      Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
        Dialogs.displayError(throwable);

        if (throwable instanceof Error) {
          System.exit(0);
        }
      });

      // check memory
      val maxMem = Runtime.getRuntime().maxMemory();
      if (UE.DEBUG_MODE) {
        System.out.println("Max memory: " + maxMem + " bytes.");
      }

      // init file manager
      UE.FILES = new FileManager();

      // translator
      Language.loadLanguage(UE.SETTINGS.getProperty(SettingsManager.LANGUAGE));

      // icon
      val url = Objects.requireNonNull(UE.class.getResource("icon.png")); //$NON-NLS-1$
      UE.ICON = new ImageIcon(url).getImage();

      // init update checker
      int updateInterval = UE.SETTINGS.getIntProperty(SettingsManager.UPDATE_INTERVAL);
      UPDATE_CHECKER = new UpdateChecker(UPDATE_URL, updateInterval);

      // init main window
      UE.WINDOW = new MainWindow();

      // open start page
      UE.WINDOW.showStartPage();

      // check for updates
      if (SETTINGS.getBooleanProperty(SettingsManager.CHECK_FOR_UPDATES))
        checkForUpdate(false);
    } catch (Exception e) {
      Dialogs.displayError(e);
    }
  }

  public static void checkForUpdate(boolean forceCheck) {
    // if forced, enable awaitUpdate to even prompt the user when there is no update
    awaitUpdate = forceCheck;

    UPDATE_CHECKER.checkForUpdates(forceCheck)
      .subscribeOn(Schedulers.single())
      .doOnError(Dialogs::displayError)
      .doOnSuccess(x -> {

        if (x != null) {
          displayUpdateNotification(x.getVersionString(), x.getDownloadLink().orElse(null));
        } else if(awaitUpdate) {
          String msg = Language.getString(NO_UPDATE).replace("%1", CURRENT_VERSION);
          String title = Language.getString(UPDATE_TITLE);
          JOptionPane.showMessageDialog(UE.WINDOW, msg, title, JOptionPane.INFORMATION_MESSAGE);
        }

        UE.WINDOW.notifyUpdate(x);
      })
      .subscribe();
  }

  /**
   * Displays a dialog informing the user of an update being available and offering appropriate
   * actions.
   *
   * @param latestVersion the version of the available update
   * @param downloadLink  the link to the update (may be null)
   */
  private static void displayUpdateNotification(@NonNull String latestVersion, String downloadLink) {
    val disableFutureChecksOption = Language.getString(DISABLE_UPDATES);
    val remindMeLaterOption = Language.getString(REMIND_LATER);
    val downloadOption = Language.getString(DOWNLOAD);

    String[] dialogOptions = new String[]{
        disableFutureChecksOption,
        remindMeLaterOption
    };

    if (!Objects.isNull(downloadLink))
      dialogOptions = addFirst(dialogOptions, downloadOption);

    val userResponse = JOptionPane.showOptionDialog(
        UE.WINDOW,
        Language.getString(UPDATE_FOUND).replace("%1", latestVersion),
        Language.getString(UPDATE_TITLE),
        JOptionPane.DEFAULT_OPTION,
        JOptionPane.INFORMATION_MESSAGE,
        null,
        dialogOptions,
        remindMeLaterOption
    );

    val selectedOption = ArrayUtils.get(dialogOptions, userResponse, remindMeLaterOption);
    if (selectedOption.equals(downloadOption)) {
      try {
        GuiTools.openInDefaultBrowser(downloadLink);
      } catch (IOException e) {
        Dialogs.displayError(e);
      }
    } else if (selectedOption.equals(disableFutureChecksOption)) {
      UE.SETTINGS.setProperty(SettingsManager.CHECK_FOR_UPDATES, false);
    }
  }

}
