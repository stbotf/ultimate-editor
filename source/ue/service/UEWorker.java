package ue.service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import javax.swing.SwingWorker;
import lombok.val;
import ue.UE;
import ue.gui.util.dialog.Dialogs;

/**
 * Use this with Tools.runUEWorker()
 */
public abstract class UEWorker<T> extends SwingWorker<T, Object> {

  public static final String MESSAGE = "msg"; //$NON-NLS-1$
  public static final String STATUS = "status"; //$NON-NLS-1$
  public static final String VISIBLE = "visible"; //$NON-NLS-1$
  public static final int STATUS_STARTED = 0;
  public static final int STATUS_WORKING = 1;
  public static final int STATUS_FAILED = 2;
  public static final int STATUS_DONE = 3;
  public static final int STATUS_CANCEL = 4;
  public static final int STATUS_ERROR_MSG = 5;
  public static final int STATUS_DONE_MSG = 6;

  private static final int FEED_DECAY_MS = 50;

  private String oldMSG = ""; //$NON-NLS-1$
  private Instant tsMsgDecay = Instant.ofEpochMilli(0);
  private Integer oldSTATUS = 0;
  private Boolean oldVISIBLE = true;

  protected T RESULT = null;

  /**
   * @returns the task result or null
   */
  public T getResult() {
    return RESULT;
  }

  /**
   * Send message via property listeners.
   * @param msg the new message
   */
  protected void sendMessage(String msg) {
    firePropertyChange(MESSAGE, oldMSG, msg);
    tsMsgDecay = Instant.now().plusMillis(FEED_DECAY_MS);
    oldMSG = msg;
  }

  /**
   * For high frequency messages, send message only in case last message decay is elapsed.
   * @param msg the new message
   */
  protected void feedMessage(String msg) {
    val now = Instant.now();
    if (now.isAfter(tsMsgDecay)) {
      firePropertyChange(MESSAGE, oldMSG, msg);
      tsMsgDecay = now.plusMillis(FEED_DECAY_MS);
      oldMSG = msg;
    }
  }

  /**
   * Send status via property listeners.
   * @param status the new status
   */
  protected void updateStatus(int status) {
    firePropertyChange(STATUS, oldSTATUS, status);
    oldSTATUS = status;
  }

  protected void setProgressVisible(boolean b) {
    firePropertyChange(VISIBLE, oldVISIBLE, b);
    oldVISIBLE = b;
  }

  protected static String getLogStamp() {
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy"); //$NON-NLS-1$
    return UE.APP_NAME + " " + UE.getVersion().getVersionString() //$NON-NLS-1$ //$NON-NLS-2$
      + " - " + sdf.format(Calendar.getInstance().getTime()); //$NON-NLS-1$
  }

  protected static void logError(String message) {
    // print log info
    System.err.println("\n" + getLogStamp()); //$NON-NLS-1$
    System.err.println(message);
  }

  protected static void logError(Throwable e) {
    // print log info
    System.err.println("\n" + getLogStamp()); //$NON-NLS-1$
    e.printStackTrace();
  }

  @Override
  protected final T doInBackground() {
    try {
      updateStatus(STATUS_WORKING);
      RESULT = work();
    }
    catch (Throwable e) {
      // report unhandled errors
      logError(e);
      // update status, which ensures to close the GuiTools.runUEWorker popup
      updateStatus(STATUS_FAILED);
      // display error, but ignore the exception and return partial results
      // alternatively the error could be rethrown to raise an ExecutionException
      // the task however must be checked for cancellation anyhow
      // and usually returns a reasonable result state
      Dialogs.displayError(e);
      return RESULT;
    }

    // report final status
    // and make sure to close the GuiTools.runUEWorker popup
    if (oldSTATUS == STATUS_WORKING)
      updateStatus(isCancelled() ? STATUS_CANCEL : STATUS_DONE);
    return RESULT;
  }

  protected abstract T work() throws Exception;

}
