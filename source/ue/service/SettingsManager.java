package ue.service;

import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.DataFile;
import ue.gui.util.dialog.Dialogs;
import ue.util.app.AppVersion;
import ue.util.data.StringTools;
import ue.util.func.RegQuery;

/**
 * Manages Ultimate Editor's settings. Settings are loaded from Settings.ini
 */
public class SettingsManager extends DataFile {

  /*All settings here are global, therefore static.*/
  //Default properties
  private Properties DEFAULTS;
  //real ones
  private Properties SETTINGS;
  // listeners
  private ArrayList<PropertyChangeListener> listeners = new ArrayList<PropertyChangeListener>();
  // settings file
  private File fileSETTINGS;

  /**
   * PROPERTY FIELDS
   */
  public static final String SETTINGS_INI = "Settings.ini"; //$NON-NLS-1$
  public static final String STBOF = "stbof"; //$NON-NLS-1$
  public static final String ALT = "alt"; //$NON-NLS-1$
  public static final String ENGLISH = "english"; //$NON-NLS-1$
  public static final String SFX = "sfx"; //$NON-NLS-1$
  public static final String MUSIC = "music"; //$NON-NLS-1$
  public static final String EXE = "exe"; //$NON-NLS-1$
  public static final String AUTOSAVE = "autoSave"; //$NON-NLS-1$
  public static final String LAST = "last"; //$NON-NLS-1$
  public static final String RECENT = "recent"; //$NON-NLS-1$
  public static final String AUTO_PATH = "autoPath"; //$NON-NLS-1$
  public static final String STORE_SND_DESC = "saveSndDescriptionToUserFolder"; //$NON-NLS-1$
  public static final String LANGUAGE = "language"; //$NON-NLS-1$
  public static final String LOG_FILE = "logFile"; //$NON-NLS-1$
  public static final String FONT_NAME = "fontName"; //$NON-NLS-1$
  public static final String FONT_SIZE = "fontSize"; //$NON-NLS-1$
  public static final String FONT_STYLE = "fontStyle"; //$NON-NLS-1$
  public static final String CHECK_FOR_UPDATES = "checkForUpdates"; //$NON-NLS-1$
  public static final String INCLUDE_DEV_UPDATES = "includeDevUpdates"; //$NON-NLS-1$
  public static final String UPDATE_INTERVAL = "updateInterval"; //$NON-NLS-1$
  public static final String LAST_CHECK_FOR_UPDATES = "lastCheckForUpdates"; //$NON-NLS-1$
  public static final String NUMBER_OF_BACKUPS = "numberOfBackups"; //$NON-NLS-1$
  public static final String WORK_PATH = "workpath"; //$NON-NLS-1$
  public static final String FILTER_STACK_TRACE = "useExceptionFilter"; //$NON-NLS-1$
  public static final String EXPERIMENTAL = "enableExperimental"; //$NON-NLS-1$
  public static final String BACKUP_WARNING = "backupSizeWarning"; //$NON-NLS-1$
  public static final String FILE_SHOW_PREVIEW = "fileShowPreview"; //$NON-NLS-1$
  public static final String FILE_RAW_EXPORT = "fileRawExport"; //$NON-NLS-1$
  public static final String FILE_SNEAK_CHECKING = "fileSneakChecking"; //$NON-NLS-1$
  public static final String FILE_HEX_COMPARE = "fileHexCompare"; //$NON-NLS-1$
  public static final String MAP_HIDE_OWNERSHIP = "mapDrawOwnership"; //$NON-NLS-1$
  public static final String MAP_HIDE_OBJECTS = "mapDrawObjects"; //$NON-NLS-1$
  public static final String MAP_HIDE_OUTPOSTS = "mapDrawOutposts"; //$NON-NLS-1$
  public static final String MAP_HIDE_SHIPS = "mapDrawShips"; //$NON-NLS-1$
  public static final String MAP_HIDE_PATHS = "mapDrawPaths"; //$NON-NLS-1$
  public static final String MAP_HIDE_SYSTEM_LABELS = "mapDrawSystemLabels"; //$NON-NLS-1$
  public static final String MAP_HIDE_STELLAR_LABELS = "mapDrawStellarLabels"; //$NON-NLS-1$
  public static final String SHIP_MOD_PHASER = "shipModPhaser"; //$NON-NLS-1$
  public static final String SHIP_MOD_TORP = "shipModTorp"; //$NON-NLS-1$
  public static final String SHIP_MOD_SUPERRAY = "shipModSuperRay"; //$NON-NLS-1$
  public static final String SHIP_MOD_SHIELD = "shipModShield"; //$NON-NLS-1$
  public static final String SHIP_MOD_RECHARGE = "shipModRecharge"; //$NON-NLS-1$
  public static final String SHIP_MOD_HULL = "shipModHull"; //$NON-NLS-1$
  public static final String SHIP_MOD_DEFENSE = "shipModDefense"; //$NON-NLS-1$
  public static final String SHIP_MOD_AGILITY = "shipModAgility"; //$NON-NLS-1$
  public static final String SHIP_AUTO_OFFENSIVE = "shipAutoOffensive"; //$NON-NLS-1$
  public static final String SHIP_AUTO_DEFENSIVE = "shipAutoDefensive"; //$NON-NLS-1$

  /**
   * Values
   */
  public static final String  YES = "yes"; //$NON-NLS-1$
  public static final String  NO = "no"; //$NON-NLS-1$
  public static final String  ZERO = "0"; //$NON-NLS-1$
  public static final String  NEVER = "never"; //$NON-NLS-1$

  /**
   * Defaults
   */
  public static final int     DEFAULT_FONT_SIZE = 8;
  public static final int     DEFAULT_FONT_STYLE = Font.PLAIN;
  public static final String  DEFAULT_LANGUAGE = Language.DEFAULT_LANGUAGE;
  public static final int     DEFAULT_UPDATE_INTERVAL = 2; // days
  public static final int     MAX_FONT_SIZE = 14;

  @Getter @Accessors(fluent = true)
  private boolean foundSettings = false;

  /**
   * Set's the default values, loads saved settings. Uses RegQuery to get BotF installation path
   * from registry.
   */
  public SettingsManager() {
    NAME = SETTINGS_INI;
    initDefaults();
    loadSettings();
  }

  public void initDefaults() {
    DEFAULTS = new Properties();

    try {
      // set default file paths
      File botfDir = new File(findGameDir());
      if (botfDir.exists()) {
        File stbofFile = new File(botfDir, "stbof.res"); //$NON-NLS-1$
        File altFile = new File(botfDir, "alt.res"); //$NON-NLS-1$
        File englishFile = new File(botfDir, "english.snd"); //$NON-NLS-1$
        File sfxFile = new File(botfDir, "sfx.snd"); //$NON-NLS-1$
        File musicFile = new File(botfDir, "music.snd"); //$NON-NLS-1$
        File trekFile = new File(botfDir, "trek.exe"); //$NON-NLS-1$
        File autoSaveFile = new File(botfDir, "auto.sav"); //$NON-NLS-1$

        if (stbofFile.exists())
          DEFAULTS.setProperty(STBOF, stbofFile.getAbsolutePath());
        if (altFile.exists())
          DEFAULTS.setProperty(ALT, altFile.getAbsolutePath());
        if (englishFile.exists())
          DEFAULTS.setProperty(ENGLISH, englishFile.getAbsolutePath());
        if (sfxFile.exists())
          DEFAULTS.setProperty(SFX, sfxFile.getAbsolutePath());
        if (musicFile.exists())
          DEFAULTS.setProperty(MUSIC, musicFile.getAbsolutePath());
        if (trekFile.exists())
          DEFAULTS.setProperty(EXE, trekFile.getAbsolutePath());
        if (autoSaveFile.exists())
          DEFAULTS.setProperty(AUTOSAVE, autoSaveFile.getAbsolutePath());

        DEFAULTS.setProperty(LAST, botfDir.getAbsolutePath());
        DEFAULTS.setProperty(WORK_PATH, botfDir.getAbsolutePath());
      }
    } catch (Exception e) {
      Dialogs.displayError("Failed to determine default game path: " + e);
    }

    // set further default settings
    DEFAULTS.setProperty(RECENT + 0, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 1, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 2, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 3, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 4, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 5, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 6, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 7, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 8, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(RECENT + 9, ""); //$NON-NLS-1$
    DEFAULTS.setProperty(AUTO_PATH, YES);
    DEFAULTS.setProperty(STORE_SND_DESC, NO);
    DEFAULTS.setProperty(LANGUAGE, DEFAULT_LANGUAGE);
    DEFAULTS.setProperty(LOG_FILE, "error.log"); //$NON-NLS-1$
    DEFAULTS.setProperty(FONT_NAME, "Times New Roman"); //$NON-NLS-1$
    DEFAULTS.setProperty(CHECK_FOR_UPDATES, NO);

    // update checking
    AppVersion appVersion = UE.getVersion();
    DEFAULTS.setProperty(INCLUDE_DEV_UPDATES, appVersion.isRelease() ? NO : YES);
    DEFAULTS.setProperty(UPDATE_INTERVAL, Integer.toString(DEFAULT_UPDATE_INTERVAL));
    DEFAULTS.setProperty(LAST_CHECK_FOR_UPDATES, DateTimeFormatter.RFC_1123_DATE_TIME
      .format(appVersion.getDateTime().get()));

    DEFAULTS.setProperty(NUMBER_OF_BACKUPS, ZERO);
    DEFAULTS.setProperty(FILTER_STACK_TRACE, YES);
    DEFAULTS.setProperty(EXPERIMENTAL, YES);
    DEFAULTS.setProperty(FONT_SIZE, "12");
    DEFAULTS.setProperty(BACKUP_WARNING, YES);
    DEFAULTS.setProperty(FILE_SHOW_PREVIEW, YES);
  }

  public void loadSettings() {
    try {
      SETTINGS = new Properties(DEFAULTS);
      fileSETTINGS = FileManager.findLocalFile(NAME);
      foundSettings = fileSETTINGS != null;

      if (foundSettings) {
        readSettings();
      } else {
        fileSETTINGS = FileManager.getWritableLocalFile(NAME);
      }

      updateUIFont();
    } catch (Exception e) {
      Dialogs.displayError(e);
    }
  }

  @Override
  public int getSize() {
    return SETTINGS.size();
  }

  @Override
  public boolean isSneaking() {
    return false;
  }

  /*SETS*/
  public void setProperty(String property, String value) {
    boolean remove = StringTools.isNullOrEmpty(value);
    String old = (String) (remove ? SETTINGS.remove(property)
      : SETTINGS.setProperty(property, value));

    if (!StringTools.equals(value, old) && (!remove || old != null)) {
      markChanged();

      if (property.equals(LANGUAGE)) {
        Language.loadLanguage(value);
      } else if (property.equals(FONT_NAME)) {
        this.updateUIFont();
      }

      this.firePropertyChangeEvent(new PropertyChangeEvent(this, property, old, value));
    }
  }

  public void setProperty(String property, boolean value) {
    if (value) {
      setProperty(property, YES);
    } else {
      setProperty(property, NO);
    }
  }

  public void setProperty(String property, int value) {
    setProperty(property, Integer.toString(value));
  }

  public void setProperty(String property, double value) {
    setProperty(property, Double.toString(value));
  }

  public String getProperty(String property) {
    String val = SETTINGS.getProperty(property);
    if (val == null)
      return null;

    if (val.equalsIgnoreCase(YES)) {
      return YES;
    } else if (val.equalsIgnoreCase(NO)) {
      return NO;
    } else if (val.equalsIgnoreCase(DEFAULT_LANGUAGE)) {
      return DEFAULT_LANGUAGE;
    } else if (val.equalsIgnoreCase(NEVER)) {
      return NEVER;
    } else {
      return val;
    }
  }

  /**
   * Retrieves the given property parsed as a {@link OffsetDateTime}.
   *
   * @param property the property to parse and return.
   * @return the property value parsed as a {@link OffsetDateTime}.
   *         Invalid property values return {@link OffsetDateTime#MIN}.
   */
  public OffsetDateTime getOffsetDateTimeProperty(String property) {
    val propertyValue = SETTINGS.getProperty(property);
    if (propertyValue == null)
      return OffsetDateTime.MIN;

    try {
      return OffsetDateTime.parse(propertyValue, DateTimeFormatter.RFC_1123_DATE_TIME);
    } catch (Exception e) {
      return OffsetDateTime.MIN;
    }
  }

  /**
   * Sets the property value using the provided date.
   *
   * @param property  the property to set.
   * @param dateTime  the date time being formatted and stored.
   */
  public void setOffsetDateTimeProperty(String property, OffsetDateTime dateTime) {
    String time = DateTimeFormatter.RFC_1123_DATE_TIME.format(dateTime);
    String prev = (String) SETTINGS.setProperty(property, time);
    if (!time.equals(prev))
      markChanged();
  }

  public boolean getBooleanProperty(String property) {
    String str = getProperty(property);
    return YES.equalsIgnoreCase(str);
  }

  public int getIntProperty(String property) {
    return getIntProperty(property, 0);
  }

  public int getIntProperty(String property, int defaultValue) {
    String val = getProperty(property);
    if (val == null)
      return defaultValue;

    try {
      return Integer.parseInt(val);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
      return defaultValue;
    }
  }

  public double getDoubleProperty(String property) {
    return getDoubleProperty(property, 0);
  }

  public double getDoubleProperty(String property, double defaultValue) {
    String val = getProperty(property);
    if (val == null)
      return defaultValue;

    try {
      return Double.parseDouble(val);
    }
    catch (NumberFormatException e) {
      e.printStackTrace();
      return defaultValue;
    }
  }

  public Font getDefaultFont() {
    int style = getIntProperty(FONT_STYLE, Font.PLAIN);
    int size = getIntProperty(FONT_SIZE, 12);
    size = Math.max(8, size);

    return new Font(this.getProperty(FONT_NAME), style, size);
  }

  public Font getDefaultBigFont() {
    int size = getIntProperty(FONT_SIZE, DEFAULT_FONT_SIZE);
    int style = getIntProperty(FONT_STYLE, DEFAULT_FONT_STYLE);
    size = Math.max(MAX_FONT_SIZE, size) + 2;
    style = style | Font.BOLD;

    return new Font(getProperty(FONT_NAME), style, size);
  }

  /**
   * @return a String array of recent files full paths. If no recent file paths exist null is
   * returned.
   */
  public String[] getRecentFiles() {
    int i = 0;
    for (int o = 0; o < 10; o++) {
      if (SETTINGS.getProperty(RECENT + o).length() != 0) {
        i++;
      }
    }
    if (i != 0) {
      String[] results = new String[i];
      i = 0;
      for (int o = 0; o < 10; o++) {
        if (SETTINGS.getProperty(RECENT + o).length() != 0) {
          results[i] = SETTINGS.getProperty(RECENT + o);
          i++;
        }
      }
      return results;
    }

    return null;
  }

  /**
   * Adds a file path to the recent files list. Ignores double entries.
   *
   * @param file the file to be added.
   */
  public void addToRecentFiles(String file) {
    //get recent files
    String[] recent = this.getRecentFiles();
    //set this one
    SETTINGS.setProperty(RECENT + 0, file);
    markChanged();

    //add rest
    if (recent != null) {
      int i = recent.length;
      int u = 1;
      File added = new File(file);
      for (int n = 0; (n < i) && (u < 10) && (n < 10); n++) {
        if (!added.equals(new File(recent[n]))) {
          SETTINGS.setProperty(RECENT + u, recent[n]);
          u++;
        }
      }
      for (int n = u; n < 10; n++) {
        SETTINGS.remove(RECENT + n);
      }
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    SETTINGS.load(in);
    markSaved();
  }

  /*
   * Reads settings from Settings.ini
   */
  private void readSettings() {
    try {
      load(new FileInputStream(fileSETTINGS));
    } catch (FileNotFoundException e) {
      Dialogs.displayError(e);
    } catch (IOException e) {
      e.printStackTrace();
      int n = JOptionPane.showConfirmDialog(UE.WINDOW, Language.getString("2"),
          Language.getString("1"), JOptionPane.YES_NO_OPTION,
          JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
      switch (n) {
        case JOptionPane.YES_OPTION: {
          this.readSettings();
          return;
        }
        default: {
          return;
        }
      }
    } catch (Exception e) {
      Dialogs.displayError(e);
    }
  }

  /**
   * Saves settings to "Settings.ini". If the file doesn't exist it is created.
   */
  public void saveSettings() {
    while (true) {
      try {
        saveSettingsImpl();
        break;
      } catch (IOException e) {
        e.printStackTrace();
        String title = Language.getString("1"); //$NON-NLS-1$
        String msg = Language.getString("3"); //$NON-NLS-1$
        int res = JOptionPane.showConfirmDialog(UE.WINDOW, msg, title,
          JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        switch (res) {
          case JOptionPane.YES_OPTION:
            continue;
          default:
            break;
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
        break;
      }
    }
  }

  @Override
  public void save(OutputStream out) throws IOException {
    SETTINGS.store(out, "UE settings:"); //$NON-NLS-1$
  }

  @Override
  public void clear() {
    if (!SETTINGS.isEmpty()) {
      SETTINGS.clear();
      markChanged();
    }
  }

  public void addPropertyChangeListener(PropertyChangeListener l) {
    listeners.add(l);
  }

  public void removePropertyChangeListener(PropertyChangeListener l) {
    listeners.remove(l);
  }

  protected void firePropertyChangeEvent(PropertyChangeEvent e) {
    for (int i = 0; i < listeners.size(); i++) {
      listeners.get(i).propertyChange(e);
    }
  }

  private static String findGameDir() {
    String def_path;
    String tmp = null;

    if (System.getProperty("os.name").contains("Windows")) {
      def_path = "C:";
      tmp = RegQuery.getBOTFInstallPath(false);
    } else {
      def_path = ".";

      // check if wine is installed
      String prefix = System.getProperty("user.home") + File.separator + ".wine" + File.separator
          + "dosdevices";

      File path = new File(prefix);

      if (path.exists() && path.isDirectory()) {
        def_path = prefix + File.separator + "c:";

        tmp = RegQuery.getBOTFInstallPath(true);

        if (tmp != null) {
          tmp = prefix + File.separator + tmp;
        }
      }
    }

    return tmp != null ? tmp : def_path;
  }

  private void updateUIFont() {
    // set font in UIManager
    Font def = getDefaultFont();

    java.util.Enumeration<Object> keys = UIManager.getDefaults().keys();
    while (keys.hasMoreElements()) {
      Object key = keys.nextElement();
      Object value = UIManager.get(key);
      if (value instanceof FontUIResource) {
        UIManager.put(key, def);
      }
    }
  }

  private void saveSettingsImpl() throws IOException {
    if (!fileSETTINGS.exists())
      fileSETTINGS.createNewFile();

    try (val fos = new FileOutputStream(fileSETTINGS, false)) {
      save(fos);
    }

    // saved to "Settings.ini", so mark saved
    markSaved();
  }
}
