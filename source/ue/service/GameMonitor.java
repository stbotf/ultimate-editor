package ue.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JTextArea;
import ue.UE;

/**
 * This class is used for monitoring network games.
 */
public class GameMonitor {

  private final String errPORT = Language.getString("GameMonitor.0"); //$NON-NLS-1$
  private final String strREASON = Language.getString("GameMonitor.1"); //$NON-NLS-1$
  private final String strPORT = Language.getString("GameMonitor.2"); //$NON-NLS-1$
  private final String errNotRunToKill = Language.getString("GameMonitor.8"); //$NON-NLS-1$
  private final String msgExitCode = Language.getString("GameMonitor.13"); //$NON-NLS-1$

  // ui components
  // Write messages to this JTextArea
  // TODO: This is not good, use listeners instead
  private JTextArea INFO;

  // data
  // IPs of friends
  private ArrayList<String> IP = new ArrayList<String>();
  // Ports of friends
  private ArrayList<Integer> PORT = new ArrayList<Integer>();
  // Connections with friends: me to them
  private ArrayList<ConnectionThread> CONNECTION = new ArrayList<ConnectionThread>();
  // Connections with friends: them to me
  private ArrayList<ServerConnectionThread> SERVERCON = new ArrayList<ServerConnectionThread>();
  // The ServerSocket used to listen for connections
  private ServerSocket SERVER = null;

  private int SETTING = -1;
  private RunAndMonitorThread monitorThread = null;

  /**
   * Constructor
   *
   * @param inf the JTextArea to write messeges too
   */
  public GameMonitor(JTextArea inf) {
    INFO = inf;
  }

  /**
   * Add a peer to the list.
   *
   * @param ip   the peers ip
   * @param port the peers port
   */
  public void addPeer(String ip, int port) {
    IP.add(ip);
    PORT.add(port);
  }

  /**
   * Removes a peer from the list.
   *
   * @param index the index of the peer
   */
  public void removePeer(int index) {
    IP.remove(index);
    PORT.remove(index);
  }

  /**
   * Sets a peer on the list.
   *
   * @param ip    the peers ip
   * @param port  the peers port
   * @param index peers index
   */
  public void setPeer(String ip, int port, int index) {
    IP.set(index, ip);
    PORT.set(index, port);
  }

  /**
   * Opens port to listen.
   *
   * @param port the port to listen on
   */
  public void openPort(int port) {
    if (SERVER != null) {
      try {
        INFO.setText(INFO.getText() + Language.getString("GameMonitor.3")
            + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
        for (int i = 0; i < SERVERCON.size(); i++) {
          ServerConnectionThread scon = SERVERCON.get(i);
          scon.end();
        }
        SERVER.close();
      } catch (IOException er) {
        er.printStackTrace();
        INFO.setText(INFO.getText() + er.getMessage() + "\n"); //$NON-NLS-1$
      }
    }
    try {
      SERVER = new ServerSocket(port, 5);
    } catch (IOException er) {
      er.printStackTrace();
      INFO.setText(INFO.getText() + errPORT.replace("%1", Integer.toString(port)) + "\n"
          //$NON-NLS-1$ //$NON-NLS-2$
          + strREASON.replace("%1", er.getMessage())
          + "\n");             //$NON-NLS-1$ //$NON-NLS-2$
      return;
    }
    ServerConnectionThread thr = new ServerConnectionThread();
    thr.setPriority(Thread.MIN_PRIORITY);
    thr.setDaemon(true);
    thr.start();
    INFO.setText(INFO.getText() + strPORT.replace("%1", Integer.toString(port))
        + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
  }

  /**
   * Connects to peers.
   */
  public void connect() {
    disconnect();
    INFO.setText(
        INFO.getText() + Language.getString("GameMonitor.4") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
    for (int i = 0; i < IP.size(); i++) {
      ConnectionThread con = new ConnectionThread(IP.get(i), PORT.get(i));
      con.setPriority(Thread.MIN_PRIORITY);
      con.setDaemon(true);
      con.start();
      CONNECTION.add(con);
    }
  }

  /**
   * disConnects from peers.
   */
  public void disconnect() {
    for (int i = 0; i < CONNECTION.size(); i++) {
      if (i == 0) {
        INFO.setText(INFO.getText() + Language.getString("GameMonitor.5")
            + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
      }
      ConnectionThread con = CONNECTION.get(i);
      con.disconnect();
      con.end();
      CONNECTION.remove(con);
      if (i == CONNECTION.size() - 1) {
        INFO.setText(INFO.getText() + Language.getString("GameMonitor.6")
            + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
      }
    }
  }

  /**
   * disConnects from peers.
   */
  public void disconnectAll() {
    disconnect();
    if (SERVER != null) {
      try {
        INFO.setText(INFO.getText() + Language.getString("GameMonitor.7")
            + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
        for (int i = 0; i < SERVERCON.size(); i++) {
          ServerConnectionThread scon = SERVERCON.get(i);
          scon.end();
        }
        SERVER.close();
      } catch (IOException er) {
        er.printStackTrace();
        INFO.setText(INFO.getText() + er.getMessage() + "\n"); //$NON-NLS-1$
      }
    }
  }

  /**
   * reports crash.
   */
  public void reportCrash() {
    for (int i = 0; i < CONNECTION.size(); i++) {
      ConnectionThread con = CONNECTION.get(i);
      con.reportCrash();
    }
  }

  private void killBotf() {
    if (monitorThread != null) {
      monitorThread.kill();
      monitorThread = null;
    }

    INFO.setText(INFO.getText() + Language.getString("GameMonitor.9") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
  }

  public void playAlert() {
    try {
      URL url = UE.class.getResource("alert.wav"); //$NON-NLS-1$
      AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);
      Clip clip = AudioSystem.getClip();
      clip.open(audioInputStream);
      clip.start();
    } catch (Exception re) {
      re.printStackTrace();
    }
    INFO.setText(
        INFO.getText() + Language.getString("GameMonitor.10") + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
  }

  public void runAndMonitor() {
    if (monitorThread != null && monitorThread.isAlive())
      return;

    monitorThread = new RunAndMonitorThread();
    monitorThread.setPriority(Thread.MIN_PRIORITY);
    monitorThread.setDaemon(true);
    monitorThread.start();
  }

  public String getLocalAddress() {
    try {
      String str = ""; //$NON-NLS-1$
      Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
      InetAddress ip;
      while (netInterfaces.hasMoreElements()) {
        NetworkInterface ni = netInterfaces.nextElement();
        ip = ni.getInetAddresses().nextElement();
        if (!ip.isLoopbackAddress() && ip.getHostAddress().indexOf(":") == -1) { //$NON-NLS-1$
          if (str.length() == 0) {
            str = str + ip;
          } else {
            str = str + " " + Language.getString("GameMonitor.11") + " "
                + ip; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          }
        } else {
          ip = null;
        }
      }
      if (str.length() == 0) {
        str = Language.getString("GameMonitor.12"); //$NON-NLS-1$
      }
      return str;
    } catch (Exception ze) {
      ze.printStackTrace();
    }
    return Language.getString("GameMonitor.12"); //$NON-NLS-1$
  }

  public String[] getPeerList() {
    String[] str = new String[IP.size()];
    for (int i = 0; i < IP.size(); i++) {
      str[i] = IP.get(i) + ":" + PORT.get(i); //$NON-NLS-1$
    }
    return str;
  }

  public void setAction(int i) {
    SETTING = i;
  }

  private class RunAndMonitorThread extends Thread {

    // Use this to run botf and monitor if the game is running
    private Process runner = null;
    // Has the game been killed?
    private boolean killed = false;

    @Override
    public void run() {
      if (runner != null)
        return;

      try {
        Runtime rt = Runtime.getRuntime();
        runner = rt.exec(UE.SETTINGS.getProperty(SettingsManager.EXE));
        runner.waitFor();

        // if not killed by UE, report trek.exe application exit code
        if (!killed) {
          String str = msgExitCode.replace("%1", Integer.toString(runner.exitValue())); //$NON-NLS-1$
          INFO.setText(INFO.getText() + str + "\n"); //$NON-NLS-1$

          // notify connected UE instances
          reportCrash();
        }
      } catch (InterruptedException e) {
        reportError(e);
        // Restore interrupted state...
        Thread.currentThread().interrupt();
      } catch (Exception e) {
        reportError(e);
      } finally {
        runner = null;
      }
    }

    public void kill() {
      killed = true;

      if (runner == null) {
        INFO.setText(INFO.getText() + errNotRunToKill + "\n"); //$NON-NLS-1$
        return;
      }

      runner.destroy();
      runner = null;
    }

    private void reportError(Exception e) {
      e.printStackTrace();
      INFO.setText(INFO.getText() + e.getMessage() + "\n"); //$NON-NLS-1$
    }
  }

  private class ServerConnectionThread extends Thread {

    private boolean END = false;

    public void end() {
      END = true;
    }

    @Override
    public void run() {
      //accept
      Socket sct = null;
      SERVERCON.add(this);
      try {
        sct = SERVER.accept();
      } catch (IOException er) {
        if (END) {
          return;
        }
        er.printStackTrace();
        INFO.setText(INFO.getText() + Language.getString("GameMonitor.14") + "\n" + strREASON
            .replace("%1", er.getMessage())
            + "\n");             //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        return;
      }
      InetAddress addy = sct.getInetAddress();
      //ready
      ServerConnectionThread thr = new ServerConnectionThread();
      thr.setPriority(Thread.MIN_PRIORITY);
      thr.setDaemon(true);
      thr.start();
      //get stream & wait for input
      String str = Language.getString("GameMonitor.15"); //$NON-NLS-1$
      INFO.setText(
          INFO.getText() + str.replace("%1", addy.toString()) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
      while (!END && !sct.isClosed()) {
        try {
          InputStream input = null;
          try {
            input = sct.getInputStream();
          } catch (IOException er) {
            er.printStackTrace();
            str = Language.getString("GameMonitor.16"); //$NON-NLS-1$
            str = str.replace("%1", addy.toString()); //$NON-NLS-1$
            INFO.setText(INFO.getText() + str.replace("%1", er.getMessage())
                + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
          }

          if (input == null) {
            try {
              sct.close();
            } catch (IOException er) {
              er.printStackTrace();
              INFO.setText(INFO.getText() + er.getMessage() + "\n"); //$NON-NLS-1$
            }
            END = true;
            return;
          }

          if (input.available() > 0) {
            int h = input.read();

            //remote crash occured
            if (h == 1) {
              INFO.setText(INFO.getText() + Language.getString("GameMonitor.17")
                  + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
              switch (SETTING) {
                case 1: {
                  killBotf();
                  break;
                }
                case 2: {
                  playAlert();
                  break;
                }
                case 3: {
                  playAlert();
                  killBotf();
                  break;
                }
              }
            }

            //disconect
            if (h == 2) {
              str = Language.getString("GameMonitor.18"); //$NON-NLS-1$
              INFO.setText(INFO.getText() + str.replace("%1", addy.toString())
                  + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
              sct.close();
              SERVERCON.remove(this);
              END = true;
            }
          }
        } catch (IOException er) {
          er.printStackTrace();
          INFO.setText(INFO.getText() + er.getMessage() + "\n"); //$NON-NLS-1$
          return;
        }
      }
    }
  }

  private class ConnectionThread extends Thread {

    private boolean END = false;
    private String HOST;
    private int PORT;
    private boolean crash = false;
    private boolean disconnect = false;

    public void end() {
      END = true;
    }

    public void reportCrash() {
      crash = true;
    }

    public void disconnect() {
      disconnect = true;
    }

    public ConnectionThread(String host, int port) {
      HOST = host;
      PORT = port;
    }

    @Override
    public void run() {
      //connect
      try (Socket sct = openSocket()) {
        if (sct == null)
          return;

        InetAddress addy = sct.getInetAddress();
        //get stream
        OutputStream output = null;
        try {
          output = sct.getOutputStream();
        } catch (IOException er) {
          er.printStackTrace();
          String err = Language.getString("GameMonitor.21") + "\n" + er.getMessage(); //$NON-NLS-1$ //$NON-NLS-2$
          INFO.setText(INFO.getText() + err + "\n"); //$NON-NLS-1$
        }

        if (output == null) {
          END = true;
          return;
        }

        // wait for order
        String msg = Language.getString("GameMonitor.22") //$NON-NLS-1$
          .replace("%1", addy.toString()); //$NON-NLS-1$
        INFO.setText(INFO.getText() + msg + "\n"); //$NON-NLS-1$

        while (!END && !sct.isClosed()) {
          try {
            if (crash) {
              output.write(1);
              crash = false;
            }
            if (disconnect) {
              output.write(2);
              END = true;
              return;
            }
          } catch (IOException er) {
            er.printStackTrace();
            INFO.setText(INFO.getText() + er.getMessage() + "\n"); //$NON-NLS-1$
            return;
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
        INFO.setText(INFO.getText() + e.getMessage() + "\n"); //$NON-NLS-1$
        return;
      }

      // disconnected
      String msg = Language.getString("GameMonitor.23"); //$NON-NLS-1$
      INFO.setText(msg.replace("%1", HOST) + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
    }

    private Socket openSocket() {
      for (int count = 0; count < 3; ++count) {
        try {
          return new Socket(InetAddress.getByName(HOST), PORT);
        } catch (Exception er) {
          if (!END) {
            er.printStackTrace();
            INFO.setText(INFO.getText() + er.getMessage() + "\n"); //$NON-NLS-1$
          }
        }
      }

      String err = Language.getString("GameMonitor.19") //$NON-NLS-1$
        .replace("%1", HOST); //$NON-NLS-1$
      err += "\n" + Language.getString("GameMonitor.20"); //$NON-NLS-1$ //$NON-NLS-2$
      INFO.setText(INFO.getText() + err + "\n"); //$NON-NLS-1$
      END = true;

      return null;
    }
  }
}
