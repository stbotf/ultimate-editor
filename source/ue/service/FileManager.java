package ue.service;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import javax.swing.JOptionPane;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.val;
import ue.UE;
import ue.edit.common.CanBeEdited;
import ue.edit.exe.trek.Trek;
import ue.edit.res.alt.Alt;
import ue.edit.res.stbof.Stbof;
import ue.edit.sav.SavGame;
import ue.edit.snd.English;
import ue.edit.snd.Music;
import ue.edit.snd.Sfx;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.util.data.CollectionTools;
import ue.util.data.StringTools;
import ue.util.file.PathHelper;
import ue.util.stream.StreamTools;

/**
 * Manages files.
 */
public class FileManager {

  @Data @AllArgsConstructor
  public class BackupEntry {
    private File file;
    private long timestamp;
  }

  // static final fields - used for setting the PRIMARY field and reading from the FILE array
  private static final int P_NULL = -1;
  public static final int P_ALT = 0;
  public static final int P_ENGLISH = 1;
  public static final int P_MUSIC = 2;
  public static final int P_SAVE_GAME = 3;
  public static final int P_SFX = 4;
  public static final int P_STBOF = 5;
  public static final int P_TREK = 6;
  // default game file locations
  public static final String DEF_FN_TREK = "trek.exe"; //$NON-NLS-1$
  public static final String DEF_FN_STBOF = "stbof.res"; //$NON-NLS-1$
  public static final String DEF_FN_ALT = "alt.res"; //$NON-NLS-1$
  public static final String DEF_FN_ENGLISH = "ENGLISH.snd"; //$NON-NLS-1$
  public static final String DEF_FN_GERMAN = "GERMAN.snd"; //$NON-NLS-1$
  public static final String DEF_FN_MUSIC = "music.snd"; //$NON-NLS-1$
  public static final String DEF_FN_SFX = "sfx.snd"; //$NON-NLS-1$
  public static final String DEF_FN_AUTO_SAVE = "auto.sav"; //$NON-NLS-1$
  public static final String DEF_FN_LAST_SAVE = "last.sav"; //$NON-NLS-1$
  // backup file date format
  public static final String BackupDateFormat = "dd.MM.yyyy - HH.mm,ss";

  // files (Alt, English, Music, SaveGame, Sfx, Stbof, Trek)
  private static CanBeEdited[] FILE = new CanBeEdited[7];
  // used to determine the primary file
  @Getter private static int PRIMARY = -1;

  // config
  private static final String HELP_ARCHIVE = "help.zip"; //$NON-NLS-1$
  public static final String SHPath = "help/screen/"; //$NON-NLS-1$
  public static final String FAQPath = "help/faq/"; //$NON-NLS-1$
  public static final String BLDPath = "bin/main"; //$NON-NLS-1$

  @Getter
  private String openedPrimaryFileName = null;

  /**
   * Returns help file contents.
   *
   * @throws IOException
   * @throws ZipException
   */
  public String getHelpFile(String path, String filename) throws IOException {
    // check inputs
    if ((filename == null) || (path == null))
      return null;

    // find help file
    File helpFile = getLocalFile(HELP_ARCHIVE);

    URL url = helpFile.toURI().toURL();
    byte[] data = null;
    ZipFile zip = null;

    try {
      try {
        zip = new ZipFile(new File(url.toURI()));
      } catch (URISyntaxException e) {
        zip = new ZipFile(new File(url.getPath()));
      }

      ZipEntry entry = zip.getEntry(
        path + UE.SETTINGS.getProperty(SettingsManager.LANGUAGE) + "/" + filename); //$NON-NLS-1$

      if (entry == null) {
        entry = zip.getEntry(path + "def/" + filename); //$NON-NLS-1$

        if (entry == null) {
          throw new FileNotFoundException(Language.getString("FileManager.11")
            .replace("%1", filename)); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }

      try (InputStream in = zip.getInputStream(entry)) {
        // read file
        data = new byte[in.available()];

        int i = 0;
        while (in.available() > 0) {
          i += in.read(data, i, in.available());
        }
      }
    } finally {
      if (zip != null)
        zip.close();
    }

    return new String(data, 0, data.length, "UTF-8"); //$NON-NLS-1$
  }

  public boolean restoreBackups(String toTime) {
    if (PRIMARY == P_NULL)
      return false;

    RestoreBackups TSK = new RestoreBackups(toTime);
    return GuiTools.runUEWorker(TSK);
  }

  /**
   * @param filePath  path to the file to open
   * @return a String array of valid edit commands specific for the opened file.
   */
  public void open(String filePath) throws IOException {
    open(new File(filePath));
  }

  public void open(File file) throws IOException {
    try {
      openPrimaryFile(file);
    } catch(IOException e) {
      resetOpenFiles();
      throw e;
    }
  }

  /**
   * Opens a primary file according to suffix and content.
   * Furthermore, adds the file to the recent files list,
   * and auto-updates the secondary file paths.
   *
   * @param file the primary file to be opened.
   * @throws IOException
   */
  private void openPrimaryFile(File file) throws IOException {
    resetOpenFiles();

    // update recent files ahead for in case of an error
    addRecentFile(file.getPath());

    // if enabled, auto-update file paths
    if (UE.SETTINGS.getProperty(SettingsManager.AUTO_PATH).equals(SettingsManager.YES))
      autoUpdateFilePaths(PathHelper.toFolderPath(file));

    // for re-attempt, remember file path even if failed to open
    int fileType = determineFileType(file);
    updateFilePath(fileType, file);

    openSecondaryFiles(fileType);
    openFileImpl(fileType, file);

    // update open primary file
    PRIMARY = fileType;
    openedPrimaryFileName = file.getName();
  }

  /**
   * Attempt to open secondary files.
   *
   * @param fileType the primary file type.
   * @throws IOException
   */
  private void openSecondaryFiles(int fileType) throws IOException {
    switch (fileType) {
      case P_TREK: {
        // try open stbof.res
        tryOpenStbof();
        break;
      }
      case P_STBOF: {
        // try open trek.exe
        tryOpenTrek();
        break;
      }
      case P_ALT: {
        // try open trek.exe
        tryOpenTrek();
        break;
      }
      case P_SAVE_GAME: {
        // try open stbof.res
        tryOpenStbof();
        // try open trek.exe
        tryOpenTrek();
        break;
      }
      case P_ENGLISH: {
        // try open stbof.res
        tryOpenStbof();
        break;
      }
    }
  }

  /**
   * Open file implementation.
   *
   * @param fileType the file type to be opened.
   * @param file the file object to be opened.
   * @throws IOException
   */
  private void openFileImpl(int fileType, File file) throws IOException {
    String lcName = file.getName().toLowerCase();

    switch (fileType) {
      case P_TREK: {
        // open trek.exe
        openTrek(file);
        break;
      }
      case P_STBOF: {
        // open stbof.res
        openStbof(file);
        break;
      }
      case P_ALT: {
        // open alt.res
        openAlt(file);
        break;
      }
      case P_SAVE_GAME: {
        // open save game
        openSaveGame(file);
        break;
      }
      case P_ENGLISH: {
        boolean isEnglishSnd = lcName.indexOf("english") > -1; //$NON-NLS-1$
        openEnglish(file, isEnglishSnd);
        break;
      }
      case P_SFX: {
        openSfx(file);
        break;
      }
      case P_MUSIC: {
        openMusic(file);
        break;
      }
    }
  }

  /**
   * @return the primary file or null
   */
  public CanBeEdited getPrimaryFile() {
    return PRIMARY != P_NULL ? FILE[PRIMARY] : null;
  }

  public Optional<File[]> listBackupFiles() {
    if (PRIMARY == P_NULL || FILE[PRIMARY] == null)
      return Optional.empty();

    // get primary file directory
    File dir = FILE[PRIMARY].getTargetFile().getParentFile();

    // list available backups of all open files
    val filter = new FilenameFilter() {
      @Override
      public boolean accept(File dir, String fileName) {
        final String lcFileName = fileName.toLowerCase();

        for (int i = 0; i < FILE.length; i++) {
          if (FILE[i] != null) {
            String usedFn = FILE[i].getTargetFile().getName().toLowerCase();
            if (lcFileName.startsWith(usedFn) && lcFileName.endsWith(".bkp"))
              return true;
          }
        }
        return false;
      }
    };

    return Optional.ofNullable(dir.listFiles(filter));
  }

  public Collection<Date> getBackupDates() {
    val files = listBackupFiles();
    return files.isPresent() ? collectBackupDates(files.get())
      : new TreeSet<>();
  }

  // collect sorted unique backup times
  private SortedSet<Date> collectBackupDates(File[] files) {
    TreeSet<Date> dates = new TreeSet<>();

    if (files != null && files.length >= 1) {
      val dateFormat = dateFormat();
      for (File bkp : files) {
        long time = fileTime(bkp, dateFormat);
        dates.add(new Date(time));
      }
    }

    return dates;
  }

  public void deleteOldBackups() {
    int maxBackups = UE.SETTINGS.getIntProperty(SettingsManager.NUMBER_OF_BACKUPS);
    if (maxBackups < 0 || PRIMARY == P_NULL)
      return;

    File[] files = listBackupFiles().orElse(null);
    if (files == null || files.length <= maxBackups)
      return;

    Collection<Date> dates = collectBackupDates(files);
    if (dates.size() <= maxBackups)
      return;

    long minTime = Long.MAX_VALUE;
    if (maxBackups > 0) {
      // find first excessive backup time
      int excessive = dates.size() - maxBackups;
      Date date = CollectionTools.elementAt(dates, excessive);
      minTime = date.getTime();
    }

    // remove excessive backups
    val dateFormat = dateFormat();
    for (File file : files) {
      boolean deleteBkp = maxBackups == 0 || fileTime(file, dateFormat) < minTime;
      if (deleteBkp && !file.delete())
        System.out.println("Failed to delete backup " + file.getAbsolutePath());
    }
  }

  private static long fileTime(File bkp, SimpleDateFormat dateFormat) {
    String name = bkp.getName();
    long time;

    if (name.contains("_")) {
      name = name.substring(name.lastIndexOf("_") + 1, name.lastIndexOf("."));

      try {
        time = dateFormat.parse(name).getTime();
      } catch (Exception ex) {
        time = bkp.lastModified();
      }
    } else {
      time = bkp.lastModified();
    }

    return time;
  }

  private Map<Integer, BackupEntry> getBackupFiles(String toTime, SimpleDateFormat dateFormat) {
    // always search for the next most backup files
    // when there is no backup, the file is assumed to be unchanged
    long maxT = 0;
    if (toTime != null) {
      try {
        maxT = dateFormat.parse(toTime).getTime();
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }

    // list related backup files
    File[] files = listBackupFiles().orElse(null);
    if (files == null || files.length == 0)
      return new HashMap<>();

    HashMap<Integer, BackupEntry> backups = new HashMap<>();
    for (File file : files) {
      // skip old backup files that precede the selected date
      // they represent an obsolete file state and would break file dependencies
      long backupTime = fileTime(file, dateFormat);
      if (backupTime < maxT)
        continue;

      // find cached file slot
      int idx = findBackupFileIndex(file.getName());
      if (idx < 0)
        continue;

      // choose closest backup files starting from selected date
      backups.merge(idx, new BackupEntry(file, backupTime), (x,y) -> x.timestamp < y.timestamp ? x : y);
    }

    return backups;
  }

  private static File findFile(Path dir, final String name) {
    Path filePath = dir.resolve(name);
    return Files.exists(filePath) ? filePath.toFile() : null;
  }

  private int findBackupFileIndex(String backupFileName) {
    String lcFileName = backupFileName.toLowerCase();

    for (int i = 0; i < FILE.length; ++i) {
      if (FILE[i] != null) {
        File file = FILE[i].getTargetFile();
        if (lcFileName.startsWith(file.getName().toLowerCase()))
          return i;
      }
    }

    return -1;
  }

  /**
   * Closes all files. If changes were made to the files, prompts the user
   * and invokes the save() function if requested.
   * Should be called before application close.
   *
   * @return true if successful and false if action was canceled by user.
   */
  public boolean closeAll() {
    // don't close if save failed
    if (!checkSaveChanges())
      return false;

    Arrays.fill(FILE, null);
    PRIMARY = P_NULL;
    openedPrimaryFileName = null;

    return true;
  }

  /**
   * Opens all files not yet opened and returns all open files.
   */
  public CanBeEdited[] openAllGameArchives() {
    ArrayList<String> errors = new ArrayList<String>();

    File lastFile = new File(UE.SETTINGS.getProperty(SettingsManager.LAST));
    if (lastFile.isFile())
      lastFile = PathHelper.getParentFile(lastFile);
    Path wdir = lastFile.toPath();

    if (FILE[P_STBOF] == null)
      tryOpenStbof(wdir, errors);
    if (FILE[P_ALT] == null)
      tryOpenAlt(wdir, errors);
    if (trek() == null)
      tryOpenTrek(wdir, errors);
    if (FILE[P_ENGLISH] == null)
      tryOpenEnglish(wdir, errors);
    if (FILE[P_MUSIC] == null)
      tryOpenMusic(wdir, errors);
    if (FILE[P_SFX] == null)
      tryOpenSfx(wdir, errors);

    if (!errors.isEmpty()) {
      String title = Language.getString("FileManager.3"); //$NON-NLS-1$
      String err = String.join("\n", errors); //$NON-NLS-1$
      Dialogs.displayError(title, err);
    }

    // return opened files
    ArrayList<CanBeEdited> re = new ArrayList<CanBeEdited>();
    for (int i = 0; i < FILE.length; i++)
      if (FILE[i] != null)
        re.add(FILE[i]);

    return re.toArray(new CanBeEdited[0]);
  }

  /**
   * @param file one of P_STBOF etc.
   * @return opened CanBeEdited file or null
   */
  public CanBeEdited getFile(int file) {
    if ((file < 0) || (file > FILE.length))
      return null;

    return FILE[file];
  }

  /**
   * @param file one of P_STBOF etc.
   * @return whether the file is opened
   */
  public boolean isLoaded(int file) {
    if ((file < 0) || (file > FILE.length))
      return false;

    return FILE[file] != null;
  }

  /**
   * Shorthand getFile call to retrieve the opened trek.exe file.
   * @return opened Trek file or null
   */
  public Trek trek() {
    return (Trek) FILE[P_TREK];
  }

  /**
   * Shorthand getFile call to retrieve the opened stbof.res file.
   * @return opened Stbof file or null
   */
  public Stbof stbof() {
    return (Stbof) FILE[P_STBOF];
  }

  /**
   * Shorthand getFile call to retrieve the opened alt.res file.
   * @return opened Alt file or null
   */
  public Alt alt() {
    return (Alt) FILE[P_ALT];
  }

  /**
   * Shorthand getFile call to retrieve the opened english.snd file.
   * @return opened English file or null
   */
  public English english() {
    return (English) FILE[P_ENGLISH];
  }

  /**
   * Shorthand getFile call to retrieve the opened music.snd file.
   * @return opened Music file or null
   */
  public Music music() {
    return (Music) FILE[P_MUSIC];
  }

  /**
   * Shorthand getFile call to retrieve the opened sfx.snd file.
   * @return opened Sfx file or null
   */
  public Sfx sfx() {
    return (Sfx) FILE[P_SFX];
  }

  /**
   * Shorthand getFile call to retrieve the opened game.sav file.
   * @return opened SavGame file or null
   */
  public SavGame savGame() {
    return (SavGame) FILE[P_SAVE_GAME];
  }

  public static String fileName(int file) {
    switch (file) {
      case P_TREK:
        return DEF_FN_TREK;
      case P_STBOF:
        return DEF_FN_STBOF;
      case P_ALT:
        return DEF_FN_ALT;
      case P_ENGLISH:
        return DEF_FN_ENGLISH;
      case P_MUSIC:
        return DEF_FN_MUSIC;
      case P_SFX:
        return DEF_FN_SFX;
      case P_SAVE_GAME:
        return DEF_FN_AUTO_SAVE;
      default:
        throw new UnsupportedOperationException("Invalid file type: " + file);
    }
  }

  public boolean checkLoaded(int file) {
    if (isLoaded(file))
      return true;

    String fileName = fileName(file);

    switch (file) {
      case P_TREK:
      case P_STBOF:
      case P_ALT:
      case P_ENGLISH:
      case P_MUSIC:
      case P_SFX: {
        String msg = "The " + fileName + " file must be loaded to perform this action.";
        Dialogs.displayError("Missing " + fileName, msg);
        break;
      }
      case P_SAVE_GAME: {
        String msg = "A game.sav file must be loaded to perform this action.";
        Dialogs.displayError("Missing game.sav", msg);
        break;
      }
      default:
        throw new UnsupportedOperationException("Invalid file type: " + file);
    }

    return false;
  }

  /**
   * Saves changes made to opened files to a specified path.
   *
   * @param dest destination where to save the file to. If null save to opened file.
   * @return true, if successful.
   */
  public boolean save(File dest, boolean onlyIfChanged) {
    if (PRIMARY == P_NULL)
      return false;

    val primary = getPrimaryFile();
    if (dest == null)
      dest = primary.getTargetFile();

    // update recent files ahead for in case of a crash
    addRecentFile(dest.getPath());

    Date time = Calendar.getInstance().getTime();
    val dateFormat = dateFormat();
    String bkpExt = "_" + dateFormat.format(time) + ".bkp"; //$NON-NLS-1$ //$NON-NLS-2$
    int max = UE.SETTINGS.getIntProperty(SettingsManager.NUMBER_OF_BACKUPS);
    boolean bkp = max != 0;

    // first collect all files to save and ask on secondary files
    val filesToSave = new ArrayList<CanBeEdited>(FILE.length);

    for (CanBeEdited file : FILE) {
      if (file != null && file != primary) {
        if (file.isChanged()) {
          String msg = Language.getString("FileManager.12") //$NON-NLS-1$
            .replace("%1", file.getTargetFile().toString()); //$NON-NLS-1$

          boolean trekChange = file == trek();
          boolean resChange = !trekChange && (file == stbof() || file == alt());

          if (trekChange || resChange) {
            String[] options = new String[] {"Yes", "Skip", "Investigate", "Cancel"};
            int response = JOptionPane.showOptionDialog(
              UE.WINDOW, msg, UE.APP_NAME, JOptionPane.DEFAULT_OPTION,
              JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

            switch (response) {
              case 0: {
                // yes
                filesToSave.add(file);
                break;
              }
              // case 1: skip by default
              case 2: {
                // investigate
                UE.WINDOW.showFileChanges(file.type());
                // cancel save
                return false;
              }
              case 3: {
                // cancel save
                return false;
              }
            }
          }
          else {
            String[] options = new String[] {"Yes", "Skip", "Cancel"};
            int response = JOptionPane.showOptionDialog(
              UE.WINDOW, msg, UE.APP_NAME, JOptionPane.DEFAULT_OPTION,
              JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

            switch (response) {
              case 0: {
                // yes
                filesToSave.add(file);
                break;
              }
              // case 1: skip by default
              case 2: {
                // cancel save
                return false;
              }
            }
          }
        }
      }
    }

    // next save primary file
    boolean success = true;
    if (primary != null && (!onlyIfChanged || primary.isChanged() || primary.isSneaked())) {
      try {
        success = primary.save(dest, bkp, bkpExt);
        openedPrimaryFileName = primary.getName();
      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }

    // then proceed to save agreed secondary files
    for (int i = 0; success && i < filesToSave.size(); ++i) {
      CanBeEdited file = filesToSave.get(i);

      try {
        success = file.save(null, bkp, bkpExt);
      } catch (Exception e) {
        Dialogs.displayError(e);
        return false;
      }
    }

    return success;
  }

  /**
   * @return true if changes were made to opened files and false if not.
   */
  public boolean madeChanges() {
    if (PRIMARY == P_NULL)
      return false;

    for (CanBeEdited file : FILE) {
      if (file != null) {
        if (file.isChanged())
          return true;
      }
    }

    return false;
  }

  public boolean checkSaveChanges() {
    if (madeChanges()) {
      int response = JOptionPane.showConfirmDialog(UE.WINDOW,
          Language.getString("FileManager.6"), //$NON-NLS-1$
          UE.APP_NAME, JOptionPane.YES_NO_CANCEL_OPTION);

      if (response == JOptionPane.YES_OPTION) {
        // save but keep backups
        if (!save(null, true))
          return false;
      } else if (response == JOptionPane.CANCEL_OPTION) {
        return false;
      }
    }

    return true;
  }

  private int determineFileType(File file) {
    String fileName = file.getName().toLowerCase();
    String suffix = StringTools.after(fileName, ".");

    switch (suffix) {
      case "exe": //$NON-NLS-1$
        return P_TREK;
      case "res": //$NON-NLS-1$
        try (FileInputStream fis = new FileInputStream(file)) {
          byte[] data = StreamTools.readBytes(fis, 4);
          if ((data[0] == 'P') && (data[1] == 'K') && (data[2] == 3) && (data[3] == 4)) {
            // stbof.res
            return P_STBOF;
          } else {
            // alt.res
            return P_ALT;
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
          return -1;
        }
      case "sav": //$NON-NLS-1$
        return P_SAVE_GAME;
      case "snd": //$NON-NLS-1$
        if (fileName.indexOf("sfx") > -1) //$NON-NLS-1$
          return P_SFX;
        else if (fileName.indexOf("music") > -1) //$NON-NLS-1$
          return P_MUSIC;
        else
          return P_ENGLISH;
      default:
        return -1;
    }
  }

  private void resetOpenFiles() {
    PRIMARY = P_NULL;
    Arrays.fill(FILE, null);
    openedPrimaryFileName = null;
  }

  /**
   * open trek.exe
   * @throws IOException
   */
  private Trek openTrek(File file) throws IOException {
    checkFileValid(file);
    Trek trek = new Trek();

    // set file ahead for recursive lookup
    FILE[P_TREK] = trek;

    trek.open(file);
    return trek;
  }

  /**
   * try open known trek.exe
   */
  private Trek tryOpenTrek() {
    return tryOpenTrek(null, null);
  }

  /**
   * try open or search for trek.exe
   */
  private Trek tryOpenTrek(Path dir, List<String> errors) {
    // first try stored settings path
    // fallback to search the provided directory
    String filePath = UE.SETTINGS.getProperty(SettingsManager.EXE);
    File file = filePath != null ? new File(filePath)
      : dir != null ? findFile(dir, DEF_FN_TREK) : null;

    if (file == null || !file.exists())
      return null;

    try {
      return openTrek(file);
    } catch (Exception e) {
      FILE[P_TREK] = null;
      String err = Language.getString("FileManager.3") //$NON-NLS-1$
        .replace("%1", file.getName());

      System.err.println(err);
      if (errors != null)
        errors.add(err);
      return null;
    }
  }

  /**
   * opens stbof.res
   * @throws IOException
   */
  private Stbof openStbof(File file) throws IOException {
    checkFileValid(file);
    Stbof stbof = new Stbof();

    // set file ahead for recursive lookup
    FILE[P_STBOF] = stbof;

    stbof.open(file);
    return stbof;
  }

  /**
   * try open known stbof.res
   */
  private Stbof tryOpenStbof() {
    return tryOpenStbof(null, null);
  }

  /**
   * try open or search for stbof.res
   */
  private Stbof tryOpenStbof(Path dir, List<String> errors) {
    // first try stored settings path
    // fallback to search the provided directory
    String filePath = UE.SETTINGS.getProperty(SettingsManager.STBOF);
    File file = filePath != null ? new File(filePath)
      : dir != null ? findFile(dir, DEF_FN_STBOF) : null;

    if (file == null || !file.exists())
      return null;

    try {
      return openStbof(file);
    } catch (Exception e) {
      FILE[P_STBOF] = null;
      String err = Language.getString("FileManager.3") //$NON-NLS-1$
        .replace("%1", file.getName());

      System.err.println(err);
      if (errors != null)
        errors.add(err);
      return null;
    }
  }

  /**
   * open alt.res
   * @throws IOException
   */
  private Alt openAlt(File file) throws IOException {
    checkFileValid(file);
    Alt alt = new Alt();

    // set file ahead for recursive lookup
    FILE[P_ALT] = alt;

    alt.open(file);
    return alt;
  }

  /**
   * try open or search for alt.res
   */
  private Alt tryOpenAlt(Path dir, List<String> errors) {
    // first try stored settings path
    // fallback to search the provided directory
    String filePath = UE.SETTINGS.getProperty(SettingsManager.ALT);
    File file = filePath != null ? new File(filePath)
      : dir != null ? findFile(dir, DEF_FN_ALT) : null;

    if (file == null || !file.exists())
      return null;

    try {
      return openAlt(file);
    } catch (Exception e) {
      FILE[P_ALT] = null;
      String err = Language.getString("FileManager.3") //$NON-NLS-1$
        .replace("%1", file.getName());

      System.err.println(err);
      if (errors != null)
        errors.add(err);
      return null;
    }
  }

  /**
   * open ENGLISH.snd or GERMAN.snd
   * @throws IOException
   */
  private English openEnglish(File file, boolean isEnglish) throws IOException {
    checkFileValid(file);
    English eng = new English(isEnglish);

    // set file ahead for recursive lookup
    FILE[P_ENGLISH] = eng;

    eng.open(file);
    return eng;
  }

  /**
   * try open or search for ENGLISH.snd or GERMAN.snd
   */
  private English tryOpenEnglish(Path dir, List<String> errors) {
    // first try stored settings path
    // fallback to search the provided directory
    String filePath = UE.SETTINGS.getProperty(SettingsManager.EXE);
    File file = null;
    boolean isEnglish = true;

    if (filePath != null) {
      file = new File(filePath);
      isEnglish = file.getName().indexOf("english") > -1; //$NON-NLS-1$
    } else if (dir != null) {
      file = findFile(dir, DEF_FN_ENGLISH);
      if (file == null) {
        file = findFile(dir, DEF_FN_GERMAN);
        isEnglish = false;
      }
    }

    if (file == null || !file.exists())
      return null;

    try {
      return openEnglish(file, isEnglish);
    } catch (Exception e) {
      FILE[P_ENGLISH] = null;
      String err = Language.getString("FileManager.3") //$NON-NLS-1$
        .replace("%1", file.getName());

      System.err.println(err);
      if (errors != null)
        errors.add(err);
      return null;
    }
  }

  /**
   * open music.snd
   * @throws IOException
   */
  private Music openMusic(File file) throws IOException {
    checkFileValid(file);
    Music music = new Music();

    // set file ahead for recursive lookup
    FILE[P_MUSIC] = music;

    music.open(file);
    return music;
  }

  /**
   * try open or search for music.snd
   */
  private Music tryOpenMusic(Path dir, List<String> errors) {
    // first try stored settings path
    // fallback to search the provided directory
    String filePath = UE.SETTINGS.getProperty(SettingsManager.MUSIC);
    File file = filePath != null ? new File(filePath)
      : dir != null ? findFile(dir, DEF_FN_MUSIC) : null;

    if (file == null || !file.exists())
      return null;

    try {
      return openMusic(file);
    } catch (Exception e) {
      FILE[P_MUSIC] = null;
      String err = Language.getString("FileManager.3") //$NON-NLS-1$
        .replace("%1", file.getName());

      System.err.println(err);
      if (errors != null)
        errors.add(err);
      return null;
    }
  }

  /**
   * open sfx.snd
   * @throws IOException
   */
  private Sfx openSfx(File file) throws IOException {
    checkFileValid(file);
    Sfx sfx = new Sfx();

    // set file ahead for recursive lookup
    FILE[P_SFX] = sfx;

    sfx.open(file);
    return sfx;
  }

  /**
   * try open or search for sfx.snd
   */
  private Sfx tryOpenSfx(Path dir, List<String> errors) {
    // first try stored settings path
    // fallback to search the provided directory
    String filePath = UE.SETTINGS.getProperty(SettingsManager.SFX);
    File file = filePath != null ? new File(filePath)
      : dir != null ? findFile(dir, DEF_FN_SFX) : null;

    if (file == null || !file.exists())
      return null;

    try {
      return openSfx(file);
    } catch (Exception e) {
      FILE[P_SFX] = null;
      String err = Language.getString("FileManager.3") //$NON-NLS-1$
        .replace("%1", file.getName());

      System.err.println(err);
      if (errors != null)
        errors.add(err);
      return null;
    }
  }

  /**
   * opens the given save game
   * @throws IOException
   */
  private SavGame openSaveGame(File file) throws IOException {
    checkFileValid(file);
    SavGame sav = new SavGame();

    // set file ahead for recursive lookup
    FILE[P_SAVE_GAME] = sav;

    sav.open(file);
    return sav;
  }

  private void updateFilePath(int fileType, File file) {
    String setting = fileTypeToSetting(fileType);
    UE.SETTINGS.setProperty(setting, file.getAbsolutePath());
  }

  private class FilePaths {
    public File trek = null;
    public File stbof = null;
    public File alt = null;
    public File english = null;
    public File music = null;
    public File sfx = null;
    public File autoSave = null;
  }

  private FilePaths searchFilePaths(Path dir) {
    FilePaths filePaths = new FilePaths();

    filePaths.trek = findFile(dir, DEF_FN_TREK);
    filePaths.stbof = findFile(dir, DEF_FN_STBOF);
    filePaths.alt = findFile(dir, DEF_FN_ALT);

    filePaths.english = findFile(dir, DEF_FN_ENGLISH);
    filePaths.music = findFile(dir, DEF_FN_MUSIC);
    filePaths.sfx = findFile(dir, DEF_FN_SFX);
    if (filePaths.english == null)
      filePaths.english = findFile(dir, DEF_FN_GERMAN);

    filePaths.autoSave = findFile(dir, DEF_FN_AUTO_SAVE);
    if (filePaths.autoSave == null)
      filePaths.autoSave = findFile(dir, DEF_FN_LAST_SAVE);

    return filePaths;
  }

  private void autoUpdateFilePaths(Path dir) {
    // search known directory files
    FilePaths filePaths = searchFilePaths(dir);

    // update known file paths
    if (filePaths.trek != null)
      UE.SETTINGS.setProperty(SettingsManager.EXE, filePaths.trek.getAbsolutePath());
    if (filePaths.stbof != null)
      UE.SETTINGS.setProperty(SettingsManager.STBOF, filePaths.stbof.getAbsolutePath());
    if (filePaths.alt != null)
      UE.SETTINGS.setProperty(SettingsManager.ALT, filePaths.alt.getAbsolutePath());
    if (filePaths.english != null)
      UE.SETTINGS.setProperty(SettingsManager.ENGLISH, filePaths.english.getAbsolutePath());
    if (filePaths.music != null)
      UE.SETTINGS.setProperty(SettingsManager.MUSIC, filePaths.music.getAbsolutePath());
    if (filePaths.sfx != null)
      UE.SETTINGS.setProperty(SettingsManager.SFX, filePaths.sfx.getAbsolutePath());
    if (filePaths.autoSave != null)
      UE.SETTINGS.setProperty(SettingsManager.AUTOSAVE, filePaths.autoSave.getAbsolutePath());
  }

  private static String fileTypeToSetting(int fileType) throws UnsupportedOperationException {
    switch (fileType) {
      case P_ALT:
        return SettingsManager.ALT;
      case P_ENGLISH:
        return SettingsManager.ENGLISH;
      case P_MUSIC:
        return SettingsManager.MUSIC;
      case P_SFX:
        return SettingsManager.SFX;
      case P_STBOF:
        return SettingsManager.STBOF;
      case P_TREK:
        return SettingsManager.EXE;
      case P_SAVE_GAME:
        return SettingsManager.AUTOSAVE;
    }

    String msg = "File type %1 not supported." //$NON-NLS-1$
      .replace("%1", Integer.toString(fileType)); //$NON-NLS-1$
    throw new UnsupportedOperationException(msg);
  }

  private static void checkFileValid(File file) throws IOException {
    checkFileExists(file);

    // throw if empty
    if (file.length() <= 0) {
      String msg = Language.getString("FileManager.1") //$NON-NLS-1$
        .replace("%1", file.getName()); //$NON-NLS-1$
      throw new IOException(msg);
    }
  }

  private static void checkFileExists(File file) throws FileNotFoundException {
    if (file == null)
      throw new NullPointerException();
    if (!file.exists())
      throw new FileNotFoundException(file.getName());
  }

  private class RestoreBackups extends UEWorker<Boolean> {

    private String toTime;

    public RestoreBackups(String toTime) {
      this.toTime = toTime;
      RESULT = false;
    }

    @Override
    protected Boolean work() throws Exception {
      if (PRIMARY == P_NULL || FILE[PRIMARY] == null) {
        // no primary file open to restore any backups
        return false;
      }

      val dateFormat = dateFormat();
      val backups = getBackupFiles(toTime, dateFormat);
      if (backups == null || backups.size() == 0) {
        String err = Language.getString("FileManager.14") //$NON-NLS-1$
          .replace("%1", toTime);
        sendMessage(err);
        updateStatus(UEWorker.STATUS_ERROR_MSG);
        return false;
      }

      for (int i = 0; i < FILE.length; i++) {
        // skip file types that aren't loaded
        if (FILE[i] == null)
          continue;

        // always discard current file changes
        FILE[i].discard();

        // skip missing backup files
        BackupEntry bkp = backups.get(i);
        if (bkp == null)
          continue;

        // determine previous target for file save
        File origTarget = FILE[i].getTargetFile();

        // restore backup file
        sendMessage(Language.getString("FileManager.13")
          .replace("%1", bkp.file.getName())); //$NON-NLS-1$ //$NON-NLS-2$
        openFileImpl(i, bkp.file);

        // set target file for file save
        FILE[i].setTargetFile(origTarget);
        // mark loaded backup file changed
        FILE[i].markChanged();
      }

      return true;
    }
  }

  public static SimpleDateFormat dateFormat() {
    // wrap SimpleDateFormat for thread safety
    // reported by SonarQube if static
    return new SimpleDateFormat(BackupDateFormat);
  }

  public static File getAppLocation() {
    File file;
    try {
      file = new File(ue.UE.class.getProtectionDomain().getCodeSource().getLocation().toURI());
    } catch (URISyntaxException ex) {
      ex.printStackTrace();
      file = new File("");
    }

    // strip executable
    return file.isDirectory() ? file : file.getParentFile();
  }

  public static File getLocalFile(String filePath) throws FileNotFoundException {
    File file = findLocalFile(filePath);
    if (file == null) {
      throw new FileNotFoundException(Language.getString("FileManager.4") //$NON-NLS-1$
        .replace("%1", filePath)); //$NON-NLS-1$
    }
    return file;
  }

  public static File findLocalFile(String filePath) {
    // try work path
    File file = new File(filePath);
    if (file.exists())
      return file;

    // try the app dir
    File appLocation = getAppLocation();
    file = new File(appLocation, filePath);
    if (file.exists())
      return file;

    // adjust when debugging
    if (appLocation.toPath().endsWith(BLDPath)) {
      appLocation = appLocation.getParentFile().getParentFile();

      // Changelog.txt
      file = new File(appLocation, filePath);
      if (file.exists())
        return file;

      // Readme.txt
      file = new File(appLocation, Paths.get("app", filePath).toString());
      if (file.exists())
        return file;

      // help.zip
      file = new File(appLocation, Paths.get("build/runtime", filePath).toString());
      if (file.exists())
        return file;
    }

    // try the user home dir
    file = new File(new File(System.getProperty("user.home")), filePath);
    if (file.exists())
      return file;

    // try temp dir
    file = new File(new File(System.getProperty("java.io.tmpdir")), filePath);
    if (file.exists())
      return file;

    return null;
  }

  public static File getWritableLocalFile(String filePath) throws FileNotFoundException {
    // try work path
    File file = new File(filePath);
    try (FileOutputStream fos = new FileOutputStream(file, true)) {
      return file;
    } catch (Exception e1) {
    }

    // try the app dir
    file = new File(getAppLocation(), filePath);
    try (FileOutputStream fos = new FileOutputStream(file, true)) {
      return file;
    } catch (Exception e2) {
    }

    // try the user home dir
    file = new File(new File(System.getProperty("user.home")), filePath);
    try (FileOutputStream fos = new FileOutputStream(file, true)) {
      return file;
    } catch (Exception e3) {
    }

    // try temp dir
    file = new File(new File(System.getProperty("java.io.tmpdir")), filePath);
    try (FileOutputStream fos = new FileOutputStream(file, true)) {
      return file;
    } catch (Exception e4) {
    }

    throw new FileNotFoundException(Language.getString("FileManager.5") //$NON-NLS-1$
      .replace("%1", filePath)); //$NON-NLS-1$
  }

  public static void openLocalFile(String filePath) throws IOException {
    Desktop.getDesktop().edit(getLocalFile(filePath));
  }

  private void addRecentFile(String filePath) {
    // add to recent files
    UE.SETTINGS.addToRecentFiles(filePath);
    // update open, save, import and export file path suggestions
    UE.SETTINGS.setProperty(SettingsManager.LAST, filePath);
    // save settings for in case of a crash
    UE.SETTINGS.saveSettings();
  }

}
