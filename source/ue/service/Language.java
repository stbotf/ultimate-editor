// Two translators on a ship are talking.
// "Can you swim?" asks one.
// "No" says the other, "but I can shout for help in nine languages."

package ue.service;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javax.swing.UIManager;
import ue.UE;

/**
 * Used for changing UI language.
 */
public abstract class Language {

  public static final String DEFAULT_LANGUAGE = "en"; //$NON-NLS-1$

  private static final ArrayList<PropertyChangeListener> listeners = new ArrayList<>();
  private static final String BUNDLE_NAME = "language"; //$NON-NLS-1$

  private static Locale CURRENT = Locale.ENGLISH;
  private static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(Language.BUNDLE_NAME, Locale.ENGLISH);

  /**
   * Loads the given language.
   *
   * @param lang iso language code
   */
  public static void loadLanguage(String lang) {
    Locale locOld = Language.RESOURCE_BUNDLE.getLocale();
    if (lang == null)
      lang = DEFAULT_LANGUAGE;

    Language.CURRENT = new Locale(lang);

    try {
      String strBundle = Language.BUNDLE_NAME + "_" + lang + ".properties"; //$NON-NLS-1$ //$NON-NLS-2$
      Language.RESOURCE_BUNDLE = new PropertyResourceBundle(new InputStreamReader(
          UE.class.getClassLoader().getResourceAsStream(strBundle), StandardCharsets.UTF_8));
    } catch (Exception e) {
      e.printStackTrace();

      try {
        String strBundle = Language.BUNDLE_NAME + ".properties"; //$NON-NLS-1$
        Language.RESOURCE_BUNDLE = new PropertyResourceBundle(new InputStreamReader(
            UE.class.getClassLoader().getResourceAsStream(strBundle), StandardCharsets.UTF_8));
      } catch (Exception x) {
        x.printStackTrace();
        System.exit(0);
      }
    }

    Locale.setDefault(Language.CURRENT);

    Language.updateUiLanguage();

    Language.firePropertyChangeEvent(
        new PropertyChangeEvent(Language.class, "locale", locOld,
            Language.CURRENT.clone())); //$NON-NLS-1$
  }

  /**
   * Returns the currently loaded language iso code.
   *
   * @return currently loaded language iso code
   */
  public static String getLanguage() {
    return Language.CURRENT.getLanguage();
  }

  /**
   * Used to retrieve translated strings.
   *
   * @param key
   * @return localized string
   */
  public static String getString(String key) {
    try {
      return Language.RESOURCE_BUNDLE.getString(key);
    } catch (MissingResourceException e) {
      return '!' + key + '!';
    }
  }

  public static void addPropertyChangeListener(PropertyChangeListener l) {
    Language.listeners.add(l);
  }

  public static void removePropertyChangeListener(PropertyChangeListener l) {
    Language.listeners.remove(l);
  }

  protected static void firePropertyChangeEvent(PropertyChangeEvent e) {
    for (int i = 0; i < Language.listeners.size(); i++) {
      Language.listeners.get(i).propertyChange(e);
    }
  }

  private static void updateUiLanguage() {
    // localization for standard swing components
    UIManager.put("FileChooser.openDialogTitleText",
        Language.getString("Language.0")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.saveDialogTitleText",
        Language.getString("Language.1")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.lookInLabelText",
        Language.getString("Language.2")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.saveInLabelText",
        Language.getString("Language.3")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.upFolderToolTipText",
        Language.getString("Language.4")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.homeFolderToolTipText",
        Language.getString("Language.5")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.newFolderToolTipText",
        Language.getString("Language.6")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.listViewButtonToolTipText",
        Language.getString("Language.7")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.detailsViewButtonToolTipText",
        Language.getString("Language.8")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.fileNameHeaderText",
        Language.getString("Language.9")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.fileSizeHeaderText",
        Language.getString("Language.10")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.fileTypeHeaderText",
        Language.getString("Language.11")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.fileDateHeaderText",
        Language.getString("Language.12")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.fileAttrHeaderText",
        Language.getString("Language.13")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.fileNameLabelText",
        Language.getString("Language.14")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.filesOfTypeLabelText",
        Language.getString("Language.15")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.openButtonText",
        Language.getString("Language.16")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.openButtonToolTipText",
        Language.getString("Language.17")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.saveButtonText",
        Language.getString("Language.18")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.saveButtonToolTipText",
        Language.getString("Language.19")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.directoryOpenButtonText",
        Language.getString("Language.20")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.directoryOpenButtonToolTipText",
        Language.getString("Language.21")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.cancelButtonText",
        Language.getString("Language.22")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.cancelButtonToolTipText",
        Language.getString("Language.23")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.updateButtonText",
        Language.getString("Language.24")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.updateButtonToolTipText",
        Language.getString("Language.25")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.helpButtonText",
        Language.getString("Language.26")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.helpButtonToolTipText",
        Language.getString("Language.27")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.newFolderErrorText",
        Language.getString("Language.28")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("FileChooser.acceptAllFileFilterText",
        Language.getString("Language.29")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("OptionPane.yesButtonText",
        Language.getString("Language.30")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("OptionPane.noButtonText",
        Language.getString("Language.31")); //$NON-NLS-1$ //$NON-NLS-2$
    UIManager.put("OptionPane.cancelButtonText",
        Language.getString("Language.32")); //$NON-NLS-1$ //$NON-NLS-2$
  }
}
