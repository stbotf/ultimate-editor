package ue.service.updatecheck;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ue.util.app.AppVersion;

/**
 * Contains a download link and version of an UE update.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateInfo {

  AppVersion releaseVersion;
  AppVersion devVersion;

  public boolean hasDevVersion() { return devVersion != null; }
}
