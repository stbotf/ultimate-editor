package ue.service.updatecheck;

import static java.time.temporal.ChronoUnit.DAYS;
import static ue.service.SettingsManager.INCLUDE_DEV_UPDATES;
import static ue.service.SettingsManager.LAST_CHECK_FOR_UPDATES;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;
import lombok.experimental.Accessors;
import reactor.core.publisher.Mono;
import ue.UE;
import ue.service.Language;
import ue.util.app.AppVersion;

/**
 * A service used to check for updates.
 */
@RequiredArgsConstructor
public class UpdateChecker {

  private final String rssUpdatesUrl;
  private final long minDaysBetweenUpdateChecks;

  @Getter @Accessors(fluent = true)
  private boolean alreadyChecked = false;
  @Getter @Accessors(fluent = true)
  private AppVersion lastResult = null;

  public OffsetDateTime lastCheck() {
    return UE.SETTINGS.getOffsetDateTimeProperty(LAST_CHECK_FOR_UPDATES);
  }

  /**
   * Checks for updates if the appropriate time has passed since the last check or the parameter
   * forceCheck is set to true.
   *
   * @param forceCheck if true the method ignores days passed since last check.
   * @return an instance of UpdateInfo containing information on an update if available.
   */
  public Mono<AppVersion> checkForUpdates(boolean forceCheck) {
    val lastCheck = UE.SETTINGS.getOffsetDateTimeProperty(LAST_CHECK_FOR_UPDATES);
    val daysSinceLastCheck = DAYS.between(lastCheck, OffsetDateTime.now());

    if (forceCheck || daysSinceLastCheck >= minDaysBetweenUpdateChecks) {
      if (UE.DEBUG_MODE) {
        System.out.print("Checking for updates...");
      }

      return Mono.fromCallable(this::getLatestVersion)
        .filter(upd -> upd.isAfter(UE.getVersion()))
        .onErrorMap(throwable -> new UpdateCheckException(Language.getString("UpdateChecker.0"),
            throwable))
        .doOnSuccess(upd -> {
          lastResult = upd;
          alreadyChecked = true;
        })
        .doFinally(signalType -> {
          UE.SETTINGS.setOffsetDateTimeProperty(LAST_CHECK_FOR_UPDATES, OffsetDateTime.now());

          if (UE.DEBUG_MODE) {
            switch (signalType) {
              case ON_ERROR:
                System.out.println(" Update check completed with an error.");
                break;
              case CANCEL:
                System.out.println(" Update check was cancelled.");
                break;
              case ON_COMPLETE:
              default:
                System.out.println(" Update check done.");
                break;
            }
          }
        });
    } else {
      alreadyChecked = true;
      return Mono.empty();
    }
  }

  @SneakyThrows
  private AppVersion getLatestVersion() {
    val checkDev = UE.SETTINGS.getBooleanProperty(INCLUDE_DEV_UPDATES);
    val updateInfo = getUpdateInfo();
    return checkDev && updateInfo.hasDevVersion() ? updateInfo.getDevVersion()
      : updateInfo.getReleaseVersion();
  }

  @SneakyThrows
  private UpdateInfo getUpdateInfo() {
    val url = new URL(rssUpdatesUrl);
    val versions = getVersionEntries(url);
    val lastRelease = versions.stream()
      .filter(x -> x.isRelease())
      .findFirst();
    val lastDev = versions.stream()
      .filter(x -> !x.isRelease() && (!lastRelease.isPresent() || !x.isBefore(lastRelease.get())))
      .findFirst();

    return UpdateInfo.builder()
      .releaseVersion(lastRelease.orElse(null))
      .devVersion(lastDev.orElse(null))
      .build();
  }

  @SneakyThrows
  private List<AppVersion> getVersionEntries(URL url) {
    val rssFeed = getRssFeedEntries(url);
    return rssFeed.stream()
      // filter for numbered patterns like "release_v_12.40.105_dev"
      .filter(x -> x.getTitle() != null && x.getTitle().matches("\\D*\\d+\\.\\d+\\.\\d+.*"))
      .map(entry -> AppVersion.FromSyndEntry(entry))
      .sorted(Comparator.reverseOrder())
      .collect(Collectors.toList());
  }

  @SneakyThrows
  private List<SyndEntry> getRssFeedEntries(URL url) {
    try (val xmlReader = new XmlReader(url)) {
      val feed = new SyndFeedInput().build(xmlReader);
      return feed.getEntries();
    }
  }
}
