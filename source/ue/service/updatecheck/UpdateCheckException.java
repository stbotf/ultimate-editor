package ue.service.updatecheck;

/**
 * Thrown by the UpdateChecker when an exception happens in the update check.
 */
public class UpdateCheckException extends Exception {

  private static final long serialVersionUID = 8698666524478156329L;

  public UpdateCheckException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
