package ue.util.app;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;
import javax.annotation.Nullable;
import com.rometools.rome.feed.synd.SyndEntry;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import ue.util.data.StringTools;

/**
 * Contains a download link and version of an UE update.
 */
@Data
@EqualsAndHashCode
public class AppVersion implements Comparable<AppVersion> {

  private String versionString;           // the version string identifier
  private Optional<String> downloadLink;  // the gitlab release url
  private Optional<Instant> timeStamp;    // optional update timestamp
  private int majorVersion = 0;
  private int minorVersion = 0;
  private int patchVersion = 0;
  private boolean isRelease = false;

  @Builder
  public AppVersion(String versionTag, @Nullable String downloadLink, @Nullable Instant timeStamp) {
    setVersionTag(versionTag);
    parseVersionNbr(versionString);
    this.downloadLink = Optional.ofNullable(downloadLink);

    // Truncate timestamp to minutes since any more precision is unrealistic to match.
    // Not only that it would be hard to match the commit time,
    // but the SyndEntry date isn't even based on the commit time!
    // Instead it is set after the release tags are pushed to the server repository.
    this.timeStamp = timeStamp != null ? Optional.of(timeStamp
      .truncatedTo(ChronoUnit.MINUTES)) : Optional.empty();
  }

  public String getVersionTag() {
    return "v" + versionString;
  }

  public void setVersionTag(String versionTag) {
    // remove "v" prefix from version tag
    this.versionString = versionTag.startsWith("v") ? versionTag.substring(1) : versionTag;
  }

  private void parseVersionNbr(@NonNull String version) {
    int idx1 = StringTools.nextDigit(version, 0);
    if (idx1 < 0)
      return;

    int idx2 = version.indexOf(".", idx1);
    if (idx2 < 0)
      return;

    String majorNbr = version.substring(idx1, idx2);
    majorVersion = Integer.parseInt(majorNbr);

    idx1 = version.indexOf(".", ++idx2);
    String minorNbr = version.substring(idx2, idx1 < 0 ? version.length() : idx1);
    minorVersion = Integer.parseInt(minorNbr);
    if (idx1 < 0)
      return;

    idx2 = StringTools.nextLiteral(version, ++idx1);
    String patchNbr = version.substring(idx1, idx2 < 0 ? version.length() : idx2);
    patchVersion = Integer.parseInt(patchNbr);

    isRelease = idx2 < 0;
  }

  public static AppVersion FromVersionDate(String version, String downloadLink, @Nullable Date date) {
    // version v0.7.2dev5c has no update date
    return new AppVersion(version, downloadLink, date != null ? date.toInstant() : null);
  }

  public static AppVersion FromSyndEntry(@NonNull SyndEntry syndEntry) {
    return FromVersionDate(syndEntry.getTitle(), syndEntry.getLink(), syndEntry.getUpdatedDate());
  }

  public Optional<OffsetDateTime> getDateTime() {
    return timeStamp.isPresent() ? Optional.of(timeStamp.get().atZone(
      ZoneId.of("UTC")).toOffsetDateTime()) : Optional.empty();
  }

  public boolean isBefore(AppVersion version) {
    return compareTo(version) < 0;
  }

  public boolean isAfter(AppVersion version) {
    return compareTo(version) > 0;
  }

  @Override
  public int compareTo(AppVersion other_version) {
    // minor release updates of an older version should never succeed any major release
    if (majorVersion != other_version.majorVersion)
      return majorVersion < other_version.majorVersion ? -1 : 1;
    if (minorVersion != other_version.minorVersion)
      return minorVersion < other_version.minorVersion ? -1 : 1;
    if (patchVersion != other_version.patchVersion)
      return patchVersion < other_version.patchVersion ? -1 : 1;

    // each release version at least the patch version will be incremented
    if (isRelease)
      return other_version.isRelease ? 0 : 1;

    // for test versions fallback to the timestamp
    // but given that the feed timestamp is set after the manually updated version timestamp
    // first check whether the unique version identifier got changed
    if (versionString.equals(other_version.versionString))
      return 0;

    // check that the version timestamp is set
    // when there are no binary files attached, the timestamp is not set either
    if (!timeStamp.isPresent())
      return other_version.timeStamp.isPresent() ? -1 : 0;
    else if (!other_version.timeStamp.isPresent())
      return 1;

    // compare the version timestamp
    return timeStamp.get().compareTo(other_version.timeStamp.get());
  }

}
