package ue.util.func;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Wrapper class for auto-closable locks.
 */
public class AutoLock implements AutoCloseable, Lock {

  private final Lock lock;

  // to not introduce unnecessary locking overhead with e.g. a dictionary merge call,
  // and to allow pass in already locked locks without re-locking,
  // skip locking the lock on instaniation
  public AutoLock(Lock lock) {
    this.lock = lock;
  }

  // conveneient auto-lock function
  public static AutoLock reentrantLock() {
    AutoLock lock = new AutoLock(new ReentrantLock());
    lock.lock();
    return lock;
  }

  @Override
  public void lock() {
    lock.lock();
  }

  @Override
  public void lockInterruptibly() throws InterruptedException {
    lock.lockInterruptibly();
  }

  @Override
  public boolean tryLock() {
    return lock.tryLock();
  }

  @Override
  public boolean tryLock(long time, TimeUnit unit) {
    return lock.tryLock();
  }

  @Override
  public void unlock() {
    lock.unlock();
  }

  @Override
  public Condition newCondition() {
    return lock.newCondition();
  }

  @Override
  public void close() {
    lock.unlock();
  }
}
