package ue.util.func;

import java.lang.reflect.Constructor;
import java.util.concurrent.Callable;
import ue.gui.util.dialog.Dialogs;

/**
 * Function helpers for error handling.
 */
public class FunctionTools {

  @FunctionalInterface
  public interface CheckedConsumer<T, X extends Throwable> {
    void accept(T t) throws X;
  }

  @FunctionalInterface
  public interface CheckedBiConsumer<T1, T2, X extends Throwable> {
    void accept(T1 t1, T2 t2) throws X;
  }

  @FunctionalInterface
  public interface CheckedFunction<T, R, X extends Throwable> {
    R apply(T t) throws X;
  }

  @FunctionalInterface
  public interface CheckedBiFunction<T1, T2, R, X extends Throwable> {
    R apply(T1 t1, T2 t2) throws X;
  }

  @FunctionalInterface
  public interface CheckedPredicate<T, X extends Throwable> {
    boolean test(T t) throws X;
  }

  @FunctionalInterface
  public interface CheckedBiPredicate<T1, T2, X extends Throwable> {
    boolean test(T1 t1, T2 t2) throws X;
  }

  public static <T> T defaultIfThrown(Callable<T> a, T defaultValue) {
    try {
      return a.call();
    }
    catch(Exception e) {
      return defaultValue;
    }
  }

  public static <T,U> T defaultIfThrown(CheckedFunction<U, T, Exception> a, T defaultValue, U param) {
    try {
      return a.apply(param);
    }
    catch(Exception e) {
      return defaultValue;
    }
  }

  public static <T> T displayIfThrown(Callable<T> a, T defaultValue) {
    try {
      return a.call();
    }
    catch(Exception e) {
      Dialogs.displayError(e);
      return defaultValue;
    }
  }

  public static <T,U> T displayIfThrown(CheckedFunction<U, T, Exception> a, T defaultValue, U param) {
    try {
      return a.apply(param);
    }
    catch(Exception e) {
      Dialogs.displayError(e);
      return defaultValue;
    }
  }

  public static <T> T logIfThrown(Callable<T> a, T defaultValue) {
    try {
      return a.call();
    }
    catch(Exception e) {
      e.printStackTrace();
      return defaultValue;
    }
  }

  public static <T,U> T logIfThrown(CheckedFunction<U, T, Exception> a, T defaultValue, U param) {
    try {
      return a.apply(param);
    }
    catch(Exception e) {
      e.printStackTrace();
      return defaultValue;
    }
  }

  /**
   * Search matching constructor and throw if not found.
   * @throws NoSuchMethodException
   */
  public static Constructor<?> getConstructor(Class<?> type, Object[] allArgs) throws NoSuchMethodException {
    Constructor<?> con = findConstructor(type, allArgs);
    if (con == null) {
      throw new NoSuchMethodException();
    }
    return con;
  }

  /**
   * Search matching constructor including those with matching primitive types that can be auto-converted.
   */
  public static Constructor<?> findConstructor(Class<?> type, Object[] allArgs) {
    for (Constructor<?> con : type.getDeclaredConstructors()) {
      Class<?>[] types = con.getParameterTypes();
      if (types.length != allArgs.length)
        continue;

      boolean match = true;
      for (int i = 0; i < types.length; i++) {
        Class<?> con_type = types[i], arg_type = allArgs[i].getClass();
        if (con_type.isAssignableFrom(arg_type))
          continue;

        if (con_type.isPrimitive() && (
          (int.class.equals(con_type) && Integer.class.equals(arg_type))
          || (boolean.class.equals(con_type) && Boolean.class.equals(arg_type))
          || (short.class.equals(con_type) && Short.class.equals(arg_type))
          || (long.class.equals(con_type) && Long.class.equals(arg_type))
          || (char.class.equals(con_type) && Character.class.equals(arg_type))
          || (byte.class.equals(con_type) && Byte.class.equals(arg_type))))
            continue;

        match = false;
        break;
      }

      if (match)
        return con;
    }
    return null;
  }
}
