package ue.util.func;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

/**
 * This class is used to read the registry. It calls the windows program "reg" and reads it's
 * output.
 */
public class RegQuery {

  private static final String REGQUERY_UTIL = "reg query "; //$NON-NLS-1$
  private static final String REGSTR_TOKEN = "REG_SZ"; //$NON-NLS-1$

  private static final String BOTF_INSTALL_PATH = REGQUERY_UTIL +
      "\"HKLM\\SOFTWARE\\Wow6432Node\\Microprose\\Star Trek: BOTF\" /v InstallPath"; //$NON-NLS-1$
  private static final String BOTF_VERSION_PATH = REGQUERY_UTIL +
      "\"HKLM\\SOFTWARE\\Wow6432Node\\Microprose\\Star Trek: BOTF\" /v Version"; //$NON-NLS-1$

  /**
   * Returns BOTF installation path as writen in "HKLM\\SOFTWARE\\Microprose\\Star Trek:
   * BOTF\\InstallPath".
   */
  public static String getBOTFInstallPath(boolean wine) {
    if (wine) {
      String userHome = System.getProperty("user.home");
      String regPath = userHome + File.separator + ".wine" + File.separator + "system.reg";
      File registry = new File(regPath);

      if (registry.exists()) {
        try (
          FileInputStream in = new FileInputStream(registry);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
        ) {
          while (in.available() > 0) {
            String line = br.readLine();

            if (line.contains("Star Trek: BOTF")) {
              while (in.available() > 0) {
                line = br.readLine();

                if (line.contains("InstallPath")) {
                  if (line.contains("="))
                    line = line.substring(line.indexOf("=") + 1, line.length() - 1);

                  String result = line.replaceAll("\"", "");
                  result = result.replaceAll("\\\\", File.separator);
                  result = result.replaceAll("//", File.separator);
                  result = result.substring(0, 1).toLowerCase() + result.substring(1);
                  return result;
                }
              }
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } else {
      return execRegQuery(BOTF_INSTALL_PATH);
    }

    return null;
  }

  /**
   * Returns BOTF version as writen in "HKLM\\SOFTWARE\\Microprose\\Star Trek: BOTF\\Version".
   */
  public static String getBOTFVersion() {
    return execRegQuery(BOTF_VERSION_PATH);
  }

  private static String execRegQuery(String regQuery) {
    try {
      Process process = Runtime.getRuntime().exec(regQuery);
      StreamReader reader = new StreamReader(process.getInputStream());

      reader.start();
      process.waitFor();
      reader.join();

      String result = reader.getResult();
      int p = result.indexOf(REGSTR_TOKEN);
      if (p != -1)
        return result.substring(p + REGSTR_TOKEN.length()).trim();
    } catch (InterruptedException e) {
      e.printStackTrace();
      // Restore interrupted state...
      Thread.currentThread().interrupt();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  private static class StreamReader extends Thread {

    private InputStream is;
    private StringWriter sw;

    StreamReader(InputStream is) {
      this.is = is;
      sw = new StringWriter();
    }

    @Override
    public void run() {
      try {
        int c;
        while ((c = is.read()) != -1) {
          sw.write(c);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    String getResult() {
      return sw.toString();
    }
  }
}