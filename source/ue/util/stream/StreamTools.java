package ue.util.stream;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.MemoryTools;
import ue.util.stream.chunk.ChunkInputStream;

/**
 * Stream data conversion helpers.
 */
public class StreamTools {

  public static void read(InputStream in, byte[] data) throws IOException {
    int read = 0;

    while (read < data.length) {
      long rd = in.read(data, read, data.length-read);
      if (rd <= 0)
        break;
      read += rd;
    }

    if (read != data.length) {
      String err = Language.getString("StreamTools.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(read))
        .replace("%2", Integer.toString(data.length));
      throw new EOFException(err);
    }
  }

  public static void read(InputStream in, short[] data, boolean littleEndian) throws IOException {
    for (int i = 0; i < data.length; ++i)
      data[i] = readShort(in, littleEndian);
  }

  public static void read(InputStream in, int[] data, boolean littleEndian) throws IOException {
    for (int i = 0; i < data.length; ++i)
      data[i] = readInt(in, littleEndian);
  }

  public static void read(InputStream in, long[] data, boolean littleEndian) throws IOException {
    for (int i = 0; i < data.length; ++i)
      data[i] = readLong(in, littleEndian);
  }

  public static void read(InputStream in, float[] data, boolean littleEndian) throws IOException {
    for (int i = 0; i < data.length; ++i)
      data[i] = readFloat(in, littleEndian);
  }

  public static void read(InputStream in, double[] data, boolean littleEndian) throws IOException {
    for (int i = 0; i < data.length; ++i)
      data[i] = readDouble(in, littleEndian);
  }

  public static void skip(InputStream in, final long len) throws IOException {
    long skipped = 0;

    while (skipped < len) {
      long skp = in.skip(len-skipped);
      if (skp <= 0)
        break;
      skipped += skp;
    }

    if (skipped != len) {
      String err = Language.getString("StreamTools.0") //$NON-NLS-1$
        .replace("%1", Long.toString(skipped))
        .replace("%2", Long.toString(len));
      throw new EOFException(err);
    }
  }

  public static byte readByte(InputStream in) throws IOException {
    int b = in.read();
    if (b < 0)
      throw new EOFException();
    return (byte) b;
  }

  public static int readUByte(InputStream in) throws IOException {
    int b = in.read();
    if (b < 0)
      throw new EOFException();
    return b;
  }

  public static byte[] readBytes(InputStream in, int size) throws IOException {
    return IOUtils.readFully(in, size);
  }

  public static byte[] readAllBytes(InputStream in) throws IOException {
    int available = in.available();
    if (available <= 0) {
      // return empty byte array since we have a valid data stream and it just happens to be empty
      // this also helps on data comparison with empty files that are stored to a stream buffer,
      // because the stream buffer returns an ampty byte array too
      // @see FileArchive.getFileStatus
      return new byte[0];
    }

    return readBytes(in, available);
  }

  // read all data, processing all available chunks
  public static byte[] readAllChunks(InputStream in) throws IOException {
    return in instanceof ChunkInputStream ? ((ChunkInputStream)in).readAllChunks()
      : readAllBytes(in);
  }

  /**
   * Reads an int from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static int readInt(InputStream in, boolean littleEndian) throws IOException {
    int i1 = in.read();
    int i2 = in.read();
    int i3 = in.read();
    int i4 = in.read();
    if ((i1 | i2 | i3 | i4) < 0)
      throw new EOFException();

    return littleEndian
      ? i4 << 24 | i3 << 16 | i2 << 8 | i1
      : i1 << 24 | i2 << 16 | i3 << 8 | i4;
  }

  /**
   * Reads an unsigned int from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static long readUInt(InputStream in, boolean littleEndian) throws IOException {
    return Integer.toUnsignedLong(readInt(in, littleEndian));
  }

  /**
   * Reads a short from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static short readShort(InputStream in, boolean littleEndian) throws IOException {
    int i1 = in.read();
    int i2 = in.read();
    if ((i1 | i2) < 0)
    throw new EOFException();

    return (short)(littleEndian ? (i2 << 8) | i1 : (i1 << 8) | i2);
  }

  /**
   * Reads an unsigned short from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static int readUShort(InputStream in, boolean littleEndian) throws IOException {
    return Short.toUnsignedInt(readShort(in, littleEndian));
  }

  /**
   * Reads a long from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static long readLong(InputStream in, boolean littleEndian) throws IOException {
    long i1 = Integer.toUnsignedLong(readInt(in, littleEndian));
    long i2 = Integer.toUnsignedLong(readInt(in, littleEndian));
    return littleEndian ? (i2 << 32) | i1 : (i1 << 32) | i2;
  }

  /**
   * Reads a float from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static float readFloat(InputStream in, boolean littleEndian) throws IOException {
    int val = readInt(in, littleEndian);
    return Float.intBitsToFloat(val);
  }

  /**
   * Reads a double from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static double readDouble(InputStream in, boolean littleEndian) throws IOException {
    long val = readLong(in, littleEndian);
    return Double.longBitsToDouble(val);
  }

  /**
   * Reads an int array from a byte stream.
   *
   * @param data the byte stream to convert
   * @param count the number of values to read
   * @throws IOException
   */
  public static int[] readIntArray(InputStream in, int count, boolean littleEndian) throws IOException {
    int[] res = new int[count];
    for (int i = 0; i < count; ++i)
      res[i] = readInt(in, littleEndian);
    return res;
  }

  /**
   * Reads a short array from a byte stream.
   *
   * @param data the byte stream to convert
   * @param count the number of values to read
   * @throws IOException
   */
  public static short[] readShortArray(InputStream in, int count, boolean littleEndian) throws IOException {
    short[] res = new short[count];
    for (int i = 0; i < count; ++i)
      res[i] = readShort(in, littleEndian);
    return res;
  }

  /**
   * Reads a long array from a byte stream.
   *
   * @param data the byte stream to convert
   * @param count the number of values to read
   * @throws IOException
   */
  public static long[] readLongArray(InputStream in, int count, boolean littleEndian) throws IOException {
    long[] res = new long[count];
    for (int i = 0; i < count; ++i)
      res[i] = readLong(in, littleEndian);
    return res;
  }

  /**
   * Reads a float array from a byte stream.
   *
   * @param data the byte stream to convert
   * @param count the number of values to read
   * @throws IOException
   */
  public static float[] readFloatArray(InputStream in, int count, boolean littleEndian) throws IOException {
    float[] res = new float[count];
    for (int i = 0; i < count; ++i)
      res[i] = readFloat(in, littleEndian);
    return res;
  }

  /**
   * Reads a double from a byte stream.
   *
   * @param data the byte stream to convert
   * @param count the number of values to read
   * @throws IOException
   */
  public static double[] readDoubleArray(InputStream in, int count, boolean littleEndian) throws IOException {
    double[] res = new double[count];
    for (int i = 0; i < count; ++i)
      res[i] = readDouble(in, littleEndian);
    return res;
  }

  public static ByteBuffer readByteTerminatedData(InputStream in, int termination, int reservedCapacity)
    throws IOException {

    ByteBuffer buffer = ByteBuffer.allocate(reservedCapacity).order(ByteOrder.LITTLE_ENDIAN);
    int sizeRead = 0;

    while (true) {
      // read unsigned byte value
      int b = in.read();

      // break on EOF or 0-termination
      if (b == termination)
        break;

      // since ByteBuffer doesn't support any resise, re-assign recreated buffers
      buffer = reserveBufferSize(buffer, ++sizeRead);
      buffer.put((byte) b);
    }

    // return whole allocated ByteBuffer to access the used byte array without re-allocation
    buffer.limit(sizeRead);
    return buffer;
  }

  /**
   * Reads an US ASCII string from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static String readAsciiString(InputStream in) throws IOException {
    ByteBuffer data = readByteTerminatedData(in, 0, 10);
    return new String(data.array(), 0, data.limit(), StandardCharsets.US_ASCII);
  }

  /**
   * Reads an US ASCII string from a byte stream with given length.
   *
   * @param in  the byte stream to read
   * @param len the data length
   * @throws IOException
   */
  public static String readAsciiString(InputStream in, int len) throws IOException {
    byte[] data = StreamTools.readBytes(in, len);
    // limit on termination character, required for concatenation
    int strLen = DataTools.lengthOf(data, (byte) 0);
    return new String(data, 0, strLen, StandardCharsets.US_ASCII);
  }

  /**
   * Reads an ISO-8859-1 string from a byte stream.
   *
   * @param data the byte stream to convert
   * @throws IOException
   */
  public static String readNullTerminatedBotfString(InputStream in) throws IOException {
    ByteBuffer data = readByteTerminatedData(in, 0, 40);
    return new String(data.array(), 0, data.limit(), StandardCharsets.ISO_8859_1);
  }

  /**
   * Reads an ISO-8859-1 string from a byte stream with given length.
   *
   * @param in  the byte stream to read
   * @param len the data length
   * @throws IOException
   */
  public static String readNullTerminatedBotfString(InputStream in, int len) throws IOException {
    byte[] data = StreamTools.readBytes(in, len);
    // limit on termination character, required for concatenation
    int strLen = DataTools.lengthOf(data, (byte) 0);
    return new String(data, 0, strLen, StandardCharsets.ISO_8859_1);
  }

  public static void write(OutputStream out, int value, int cnt) throws IOException {
    for (int i = 0; i < cnt; ++i)
      out.write(value);
  }

  public static void write(OutputStream out, int value, boolean littleEndian) throws IOException {
    if (littleEndian) {
      out.write(value & 0xFF);
      out.write((value >>> 8) & 0xFF);
      out.write((value >>> 16) & 0xFF);
      out.write((value >>> 24) & 0xFF);
    }
    else {
      out.write((value >>> 24) & 0xFF);
      out.write((value >>> 16) & 0xFF);
      out.write((value >>> 8) & 0xFF);
      out.write(value & 0xFF);
    }
  }

  public static void write(OutputStream out, short value, boolean littleEndian) throws IOException {
    if (littleEndian) {
      out.write(value & 0xFF);
      out.write((value >>> 8) & 0xFF);
    }
    else {
      out.write((value >>> 8) & 0xFF);
      out.write(value & 0xFF);
    }
  }

  public static void transfer(InputStream in, OutputStream out) throws IOException {
    // with Java 9 consider InputStream.transferTo()
    int len;
    byte[] buf = new byte[4096];
    while ((len = in.read(buf)) != -1)
      out.write(buf, 0, len);
  }

  // @returns a doubled, newly alloced buffer
  private static ByteBuffer reserveBufferSize(ByteBuffer buffer, int len) {
    int cap = buffer.capacity();
    if (len <= cap)
      return buffer;

    // make sure to retain correct byte order when the buffer is re-allocated
    return MemoryTools.reserve(buffer, cap * 2).order(ByteOrder.LITTLE_ENDIAN);
  }
}