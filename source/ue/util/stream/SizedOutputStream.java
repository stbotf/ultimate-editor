package ue.util.stream;

import java.io.IOException;
import java.io.OutputStream;

/**
 * This is an OutputStream wrapper. Used to count the size of written data.
 */
public class SizedOutputStream extends WrappedOutputStream {

  private int size = 0;

  public SizedOutputStream(OutputStream out) {
    super(out, false);
  }

  /**
   * Writes an array of bytes to this stream.
   *
   * @param b the byte array to be written
   */
  @Override
  public void write(byte[] b) throws IOException {
    size += b.length;
    out.write(b);
  }

  /**
   * Writes an array of bytes to this stream.
   *
   * @param b   the byte array to be written
   * @param off the array index where to start from
   * @param len the length to be written
   */
  @Override
  public void write(byte[] b, int off, int len) throws IOException {
    size += len;
    out.write(b, off, len);
  }

  /**
   * Writes a byte to this stream.
   *
   * @param b the byte to be written
   */
  @Override
  public void write(int b) throws IOException {
    size++;
    out.write(b);
  }

  public void resetLength() {
    size = 0;
  }

  public int getLength() {
    return size;
  }
}
