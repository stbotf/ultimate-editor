package ue.util.stream;

import java.io.IOException;
import java.io.InputStream;

/**
 * This is an InputStream wrapper.
 */
public abstract class WrappedInputStream extends InputStream {

  protected InputStream in;
  private boolean ownership = false;

  public WrappedInputStream(InputStream in, boolean ownership) {
    this.in = in;
    this.ownership = ownership;
  }

  public boolean hasOwnership() {
    return ownership;
  }

  public void setOwnership(boolean ownership) {
    this.ownership = ownership;
  }

  /**
   * If owned, closes the underlying InputStream.
   */
  @Override
  public void close() throws IOException {
    if (ownership) {
      in.close();
    }
  }
}
