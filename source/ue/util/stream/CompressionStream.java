package ue.util.stream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class CompressionStream extends SizedOutputStream {

  private OutputStream fos;

  // uncompressed data written to the stream
  private ByteArrayOutputStream buffer;

  public CompressionStream(OutputStream out) {
    this(out, new ByteArrayOutputStream());
  }

  private CompressionStream(OutputStream out, ByteArrayOutputStream writeBuffer) {
    super(writeBuffer);
    this.fos = out;
    this.buffer = writeBuffer;
  }

  /**
   * Writes all data to the underlying OutputStream, and if owned, closes the stream.
   */
  @Override
  public void close() throws IOException {
    flush();
    if (hasOwnership()) {
      fos.close();
    }
  }

  /**
   * Writes all data to the underlying OutputStream.
   *
   * @throws IOException
   */
  @Override
  public void flush() throws IOException {
    byte[] data = buffer.toByteArray();
    encode(fos, data);
    buffer.reset();
    fos.flush();
  }

  protected abstract void encode(OutputStream fos, byte[] data) throws IOException;
}
