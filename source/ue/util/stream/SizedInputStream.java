package ue.util.stream;

import java.io.IOException;
import java.io.InputStream;
import ue.service.Language;

/**
 * This is an InputStream wrapper. It is used to set up artificial read limits.
 */
public class SizedInputStream extends WrappedInputStream {

  private int sizeLimit = Integer.MAX_VALUE;
  private boolean hasSizeLimit = false;

  public SizedInputStream(InputStream in) {
    super(in, false);
  }

  public SizedInputStream(InputStream in, int end) {
    super(in, false);
    this.sizeLimit = end;
    this.hasSizeLimit = true;
  }

  @Override
  public int available() throws IOException {
    return hasSizeLimit ? Integer.min(sizeLimit, in.available()) : in.available();
  }

  // pass -1 to unset the limit
  public void setSizeLimit(int size) {
    if (size == -1) {
      hasSizeLimit = false;
      sizeLimit = Integer.MAX_VALUE;
    } else {
      sizeLimit = size;
    }
  }

  public byte[] readAllBytes() throws IOException {
    int len = available();
    byte[] b = new byte[len];
    int offset = 0;

    do {
      int sizeRead = in.read(b, offset, len);
      if (sizeRead <= 0) {
        break;
      }

      offset += sizeRead;
      len -= sizeRead;
      sizeLimit -= sizeRead;
    }
    while (len > 0);

    return b;
  }

  @Override
  public int read() throws IOException {
    checkEOF();
    sizeLimit--;
    return in.read();
  }

  @Override
  public int read(byte[] b) throws IOException {
    return read(b, 0, b.length);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    checkEOF();
    int maxlen = available();
    len = Integer.min(len, maxlen);

    int sizeRead = in.read(b, off, len);
    if (sizeRead > 0) {
      sizeLimit -= sizeRead;
    }

    return sizeRead;
  }

  @Override
  public synchronized void reset() throws IOException {
    throw new IOException(Language.getString("SizedInputStream.0")); //$NON-NLS-1$
  }

  @Override
  public long skip(long n) throws IOException {
    if (hasSizeLimit) {
      if (sizeLimit <= 0) {
        return -1;
      }
      if (sizeLimit < n) {
        n = sizeLimit;
      }

      sizeLimit -= n;
    }

    return in.skip(n);
  }

  private void checkEOF() throws IOException {
    if (available() <= 0) {
      throw new IOException(Language.getString("SizedInputStream.1")); //$NON-NLS-1$
    }
  }
}
