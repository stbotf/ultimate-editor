package ue.util.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Compression {

  CompressionStream getCompressionStream(OutputStream out);

  DecompressionStream getDecompressionStream(InputStream in) throws IOException;
}
