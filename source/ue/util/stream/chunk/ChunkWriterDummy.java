package ue.util.stream.chunk;

import java.io.IOException;
import java.io.OutputStream;

public class ChunkWriterDummy extends ChunkOutputStream {

  private int chunkCount = 0;

  /**
   * takes ownership of the provided data stream.
   */
  public ChunkWriterDummy(OutputStream out) {
    super(out, false);
  }

  @Override
  public void flush() throws IOException {
    out.flush();
  }

  @Override
  public void write(int b) throws IOException {
    out.write(b);
  }

  @Override
  public void write(byte[] b) throws IOException {
    out.write(b);
  }

  @Override
  public void write(byte[] b, int off, int len) throws IOException {
    out.write(b, off, len);
  }

  public int numChunks() {
    return chunkCount;
  }

  public int nextChunk() throws IOException {
    return ++chunkCount;
  }
}
