package ue.util.stream.chunk;

import java.io.IOException;
import java.io.OutputStream;
import ue.util.stream.WrappedOutputStream;

public abstract class ChunkOutputStream extends WrappedOutputStream {

  public ChunkOutputStream(OutputStream out, boolean ownership) {
    super(out, ownership);
  }

  public abstract int numChunks();
  public abstract int nextChunk() throws IOException;
}
