package ue.util.stream.chunk;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class ChunkHeader {

  // the .sav file entry header size used by BotF
  public static final int SAV_SIZE = 12;

  private boolean withChunkAddress = true;
  private boolean withDataSize = true;

  // the outer wrapped data size including the header meta data
  @Getter @Accessors(fluent = true)
  private int compressedSize = 0;
  // the inner content data size
  @Getter @Accessors(fluent = true)
  private int uncompressedSize = 0;

  // initialize the chunk address to the header size
  // so on write it matches when manually set
  private int chunkAddress = headerSize();

  // maintain another address for the read operation
  // so it can be incremented by the current size
  private int readAddress = 0;

  public boolean withChunkAddress() {
    return withChunkAddress;
  }

  public boolean withDataSize() {
    return withDataSize;
  }

  public void setHeaderInfo(boolean withChunkAddress, boolean withDataSize) {
    this.withChunkAddress = withChunkAddress;
    this.withDataSize = withDataSize;
  }

  public int headerSize() {
    int size = 4;
    if (withChunkAddress) {
      size += 4;
    }
    if (withDataSize) {
      size += 4;
    }
    return size;
  }

  // the current chunk data address
  public int chunkAddress() {
    return this.chunkAddress;
  }

  public void setChunkAddress(int address) {
    this.chunkAddress = address;
  }

  public void setUncompressedSize(int size) {
    this.uncompressedSize = size;
  }

  public void setCompressedSize(int size) {
    this.compressedSize = size;
  }

  public int read(InputStream in) throws IOException {
    int sizeRead = 0;

    // increment the read address
    // which on first call now should match the chunk address
    readAddress += headerSize() + compressedSize;

    // update the chunk address
    if (withChunkAddress) {
      int address = StreamTools.readInt(in, true);
      sizeRead += 4;

      if (address != readAddress) {
        System.out.println("Chunk header address 0x" + Integer.toHexString(address)
            + " doesn't match the expected address 0x" + Integer.toHexString(readAddress) + "!");
        readAddress = address;
      }
    }
    chunkAddress = readAddress;

    if (withDataSize) {
      uncompressedSize = StreamTools.readInt(in, true);
      sizeRead += 4;
    }

    compressedSize = StreamTools.readInt(in, true);
    sizeRead += 4;

    if (!withDataSize)
      uncompressedSize = compressedSize;

    return sizeRead;
  }

  void write(OutputStream out) throws IOException {
    if (withChunkAddress) {
      out.write(DataTools.toByte(chunkAddress, true));
    }
    if (withDataSize) {
      out.write(DataTools.toByte(uncompressedSize, true));
    }

    out.write(DataTools.toByte(compressedSize, true));

    // increment address adding the new chunk size
    chunkAddress += headerSize() + compressedSize;
  }
}
