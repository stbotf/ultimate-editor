package ue.util.stream.chunk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import lombok.val;
import ue.util.stream.WrappedInputStream;

public abstract class ChunkInputStream extends WrappedInputStream {

  public ChunkInputStream(InputStream in, boolean ownership) {
    super(in, ownership);
  }

  public abstract int currentChunk();
  public abstract int nextChunk() throws IOException;
  public abstract byte[] readAllBytes() throws IOException;

  public byte[] readAllChunks() throws IOException {
    val out = new ByteArrayOutputStream();

    // read entries, each of them introduced by a header chunk
    while (nextChunk() >= 0)
      out.write(readAllBytes());

    return out.toByteArray();
  }
}
