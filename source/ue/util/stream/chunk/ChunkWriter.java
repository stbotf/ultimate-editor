package ue.util.stream.chunk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import ue.util.stream.Compression;
import ue.util.stream.CompressionStream;

public class ChunkWriter extends ChunkOutputStream {

  private int chunkCount = 0;
  private ChunkHeader header = new ChunkHeader();
  private CompressionStream compressionStream;
  private OutputStream writeStream;

  // buffer the data that's to be written to determine and first write the header info
  ByteArrayOutputStream dataBuffer = new ByteArrayOutputStream();

  /**
   * takes ownership of the provided data stream.
   */
  public ChunkWriter(OutputStream out) {
    this(out, (Compression) null);
  }

  public ChunkWriter(OutputStream out, Compression compression) {
    super(out, false);

    if (compression != null) {
      // write to compression stream counting the written data size
      this.compressionStream = compression.getCompressionStream(dataBuffer);
      this.writeStream = compressionStream;
    } else {
      // write directly to the data buffer
      this.writeStream = dataBuffer;
    }
  }

  public ChunkHeader header() {
    return header;
  }

  @Override
  public void flush() throws IOException {
    this.writeStream.flush();
  }

  @Override
  public void write(int b) throws IOException {
    writeStream.write(b);
  }

  @Override
  public void write(byte[] b) throws IOException {
    writeStream.write(b);
  }

  @Override
  public void write(byte[] b, int off, int len) throws IOException {
    writeStream.write(b, off, len);
  }

  public int numChunks() {
    return chunkCount;
  }

  public int nextChunk() throws IOException {
    int lengthWritten;
    if (compressionStream != null) {
      // flush and compress written data to the data buffer
      compressionStream.flush();
      // determine and reset written length
      lengthWritten = compressionStream.getLength();
      compressionStream.resetLength();
    } else {
      lengthWritten = dataBuffer.size();
    }

    byte[] encrypted = dataBuffer.toByteArray();

    // write chunk header
    header.setUncompressedSize(lengthWritten);
    header.setCompressedSize(encrypted.length);
    header.write(out);

    // write data
    out.write(encrypted);

    // clear data buffer
    dataBuffer.reset();

    return ++chunkCount;
  }
}
