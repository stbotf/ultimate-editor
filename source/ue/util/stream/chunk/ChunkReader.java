package ue.util.stream.chunk;

import java.io.IOException;
import java.io.InputStream;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.util.stream.Compression;
import ue.util.stream.DecompressionStream;
import ue.util.stream.SizedInputStream;
import ue.util.stream.WrappedInputStream;

public class ChunkReader extends ChunkInputStream {

  @Getter @Accessors(fluent = true)
  private int currentChunk = -1;
  private ChunkHeader header = new ChunkHeader();
  private DecompressionStream decompressionStream;
  private SizedInputStream uncompressedStream;
  private WrappedInputStream readStream;

  public ChunkReader(InputStream in) throws IOException {
    this(in, (Compression) null);
  }

  public ChunkReader(InputStream in, Compression compression) throws IOException {
    super(in, false);

    if (compression != null) {
      // read from decompression stream
      this.decompressionStream = compression.getDecompressionStream(in);
      this.readStream = decompressionStream;
    } else {
      // read directly from the input stream
      this.uncompressedStream = new SizedInputStream(in, 0);
      this.readStream = uncompressedStream;
    }
  }

  public ChunkHeader header() {
    return header;
  }

  @Override
  public int available() throws IOException {
    return readStream.available();
  }

  public byte[] readAllBytes() throws IOException {
    return decompressionStream != null ?
        decompressionStream.readAllBytes()
        : this.uncompressedStream.readAllBytes();
  }

  @Override
  public int read() throws IOException {
    return readStream.read();
  }

  @Override
  public int read(byte[] b) throws IOException {
    return readStream.read(b);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    return readStream.read(b, off, len);
  }

  public int nextChunk() throws IOException {
    if (in.available() < header.headerSize()) {
      return -1;
    }

    // read chunk header
    header.read(in);

    if (decompressionStream != null) {
      // limit by the decoded chunk size
      decompressionStream.decode(header.compressedSize());
    } else {
      // limit the input stream size
      uncompressedStream.setSizeLimit(header.uncompressedSize());
    }

    return ++currentChunk;
  }
}
