package ue.util.stream;

import java.io.IOException;
import java.io.OutputStream;

/**
 * This is an OutputStream wrapper.
 */
public abstract class WrappedOutputStream extends OutputStream {

  protected OutputStream out;
  private boolean ownership = false;

  public WrappedOutputStream(OutputStream out, boolean ownership) {
    this.out = out;
    this.ownership = ownership;
  }

  public boolean hasOwnership() {
    return ownership;
  }

  public void setOwnership(boolean ownership) {
    this.ownership = ownership;
  }

  /**
   * If owned, closes the underlying OutputStream.
   */
  @Override
  public void close() throws IOException {
    flush();
    if (ownership) {
      out.close();
    }
  }

  @Override
  public void flush() throws IOException {
    out.flush();
  }
}
