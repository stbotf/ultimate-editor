package ue.util.stream;

import java.io.IOException;
import java.io.InputStream;
import ue.service.Language;

public abstract class DecompressionStream extends WrappedInputStream {

  private int index = 0;
  private int mark = 0;
  private byte[] data;

  public DecompressionStream(InputStream in) {
    super(in, false);
  }

  public DecompressionStream decode() throws IOException {
    return decode(Integer.MAX_VALUE);
  }

  public DecompressionStream decode(int size) throws IOException {
    reset();

    // limit to available read size
    size = Integer.min(size, in.available());

    if (size > 0) {
      data = decode(in, size);
    }

    return this;
  }

  protected abstract byte[] decode(InputStream in, int size) throws IOException;

  /**
   * Returns the number of bytes available in this stream.
   */
  @Override
  public int available() {
    if (data == null) {
      return 0;
    }
    if (data.length - index <= 0) {
      return 0;
    }
    return data.length - index;
  }

  public byte[] readAllBytes() {
    if (data == null || index >= data.length) {
      return new byte[0];
    }

    byte[] b;
    if (index == 0) {
      b = data;
      reset();
    } else {
      int size = data.length - index;
      b = new byte[size];
      for (int i = 0; i < size; i++) {
        b[i] = data[index++];
      }
    }

    return b;
  }

  @Override
  public int read() throws IOException {
    checkEOF();
    return Byte.toUnsignedInt(data[index++]);
  }

  @Override
  public int read(byte[] b) throws IOException {
    // check available size
    int size = available();
    if (size <= 0) {
      return -1;
    }

    // limit to requested data bounds
    size = Integer.min(size, b.length);

    int i = 0;
    for (; i < size; i++) {
      b[i] = data[index++];
    }

    return i;
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    // check available size
    int size = available();
    if (size <= 0) {
      return -1;
    }

    // limit to requested data offset bounds
    size = Integer.min(size, len);
    size = Integer.min(size, b.length - off);

    int i = 0;
    for (; i < size; i++) {
      b[off + i] = data[index++];
    }

    return i;
  }

  @Override
  public long skip(long n) throws IOException {
    // check available size
    int size = available();
    if (size <= 0) {
      return -1;
    }

    // limit to available size
    size = Integer.min(size, (int) n);

    index += size;
    return size;
  }

  /**
   * If owned, closes the underlying InputStream.
   */
  @Override
  public void close() throws IOException {
    super.close();
    reset();
  }

  /**
   * Enable mark-reset support.
   */
  @Override
  public boolean markSupported() {
    return true;
  }

  @Override
  public synchronized void mark(int readAheadLimit) {
    mark = index;
  }

  @Override
  public synchronized void reset() {
    index = mark;
  }

  private void checkEOF() throws IOException {
    if (data == null || index >= data.length) {
      throw new IOException(Language.getString("DecompressionStream.0")); //$NON-NLS-1$
    }
  }
}
