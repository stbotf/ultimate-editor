package ue.util.img;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * A GIF color map.
 */
public class GifColorMap {

  private byte[] PIXELS;

  public GifColorMap(InputStream in, int numPixels, boolean alpha) throws IOException {
    if (numPixels > 256)
      throw new IOException(Language.getString("GifColorMap.0")); //$NON-NLS-1$

    if (alpha) {
      PIXELS = StreamTools.readBytes(in, numPixels * 4);
    } else {
      byte[] px = StreamTools.readBytes(in, numPixels * 3);
      PIXELS = new byte[numPixels * 4];

      for (int i = 0; i < numPixels; i++) {
        PIXELS[i * 4] = px[i * 3];
        PIXELS[i * 4 + 1] = px[i * 3 + 1];
        PIXELS[i * 4 + 2] = px[i * 3 + 2];
        if (px[i * 3] == 0 && px[i * 3 + 1] == 0 && px[i * 3 + 2] == 0) {
          PIXELS[i * 4 + 3] = 0;  // transparent
        } else {
          PIXELS[i * 4 + 3] = -1; // opaque
        }
      }
    }
  }

  public GifColorMap(GifColorMap map) {
    PIXELS = Arrays.copyOf(map.PIXELS, map.PIXELS.length);
  }

  public void save(OutputStream out, boolean alpha) throws IOException {
    if (alpha) {
      out.write(PIXELS);
    } else {
      for (int i = 0; i * 4 < PIXELS.length; ++i) {
        out.write(PIXELS[i * 4]);
        out.write(PIXELS[i * 4 + 1]);
        out.write(PIXELS[i * 4 + 2]);
      }
    }
  }

  public int size() {
    return PIXELS.length;
  }

  public int numColors() {
    return PIXELS.length >>> 2;
  }

  public byte colorTableSize() {
    int size = 8;
    byte i = 0;
    for (; size < PIXELS.length && i < 8; ++i) {
      size <<= 1;
    }
    return i;
  }

  public int[] getMapPixels() {
    int numPixels = PIXELS.length / 4;
    int[] ret = new int[numPixels];
    byte[] b = new byte[4];

    int j = 0;
    for (int i = 0; i < numPixels; ++i) {
      b[1] = PIXELS[j++]; //red
      b[2] = PIXELS[j++]; //green
      b[3] = PIXELS[j++]; //blue
      b[0] = PIXELS[j++]; //alpha

      ret[i] = DataTools.toInt(b, false);
    }

    return ret;
  }

  public int getPixel(int index) throws InvalidArgumentsException {
    if (index < 0 || index * 4 >= PIXELS.length)
      throw new InvalidArgumentsException(Language.getString("GifColorMap.1")); //$NON-NLS-1$

    byte[] b = new byte[4];
    int j = index * 4;

    b[1] = PIXELS[j++]; //red
    b[2] = PIXELS[j++]; //green
    b[3] = PIXELS[j++]; //blue
    b[0] = PIXELS[j++]; //alpha

    return DataTools.toInt(b, false);
  }

  public byte[] getPixelBytes(int index) throws InvalidArgumentsException {
    if (index < 0 || index * 4 >= PIXELS.length)
      throw new InvalidArgumentsException(Language.getString("GifColorMap.1")); //$NON-NLS-1$

    byte[] b = new byte[4];
    int j = index * 4;

    b[1] = PIXELS[j++]; //red
    b[2] = PIXELS[j++]; //green
    b[3] = PIXELS[j++]; //blue
    b[0] = PIXELS[j++]; //alpha

    return b;
  }

  public void setPixelBytes(int index, byte[] b) throws InvalidArgumentsException {
    if (index < 0 || index * 4 >= PIXELS.length)
      throw new InvalidArgumentsException(Language.getString("GifColorMap.1")); //$NON-NLS-1$

    PIXELS[4 * index] = b[1];
    PIXELS[4 * index + 1] = b[2];
    PIXELS[4 * index + 2] = b[3];
    PIXELS[4 * index + 3] = b[0];
  }

  public void setPixel(int index, int pixel) throws InvalidArgumentsException {
    if (index < 0 || index * 4 >= PIXELS.length)
      throw new InvalidArgumentsException(Language.getString("GifColorMap.1")); //$NON-NLS-1$

    PIXELS[4 * index] = (byte) ((pixel >>> 16) & 0xFF);
    PIXELS[4 * index + 1] = (byte) ((pixel >>> 8) & 0xFF);
    PIXELS[4 * index + 2] = (byte) (pixel & 0xFF);
    PIXELS[4 * index + 3] = (byte) (pixel >>> 24);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null || !(obj instanceof GifColorMap))
      return false;

    GifColorMap a = (GifColorMap) obj;
    if (a.PIXELS.length != PIXELS.length)
      return false;

    for (int i = 0; i < PIXELS.length; i++) {
      if (a.PIXELS[i] != PIXELS[i])
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int ret = 0;
    for (int i = 0; i < PIXELS.length; ++i) {
      ret += PIXELS[i];
    }
    return ret;
  }
}
