package ue.util.img;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.util.stream.StreamTools;

/**
 * TGA conversion helper class.
 */
public abstract class TgaHelper {

  private static final int NO_TRANSPARENCY = 255;
  private static final int FULL_TRANSPARENCY = 0;

  public static class ParseResult {
    public int[] pixels;
    public boolean hasAlpha;
  }

  // convert to 32-bit pixel buffer
  public static ParseResult readPixelStream(InputStream in, int width, int height, int pixelDepth) throws IOException {
    ParseResult result = new ParseResult();
    result.pixels = new int[width * height];

    // read the pixel data
    for (int i = (height - 1); i >= 0; i--) {
      int srcLine = i * width;

      for (int j = 0; j < width; j++) {
        if (pixelDepth == 32) {
          int blue = in.read() & 0xFF;
          int green = in.read() & 0xFF;
          int red = in.read() & 0xFF;
          int alpha = in.read() & 0xFF;
          result.pixels[srcLine + j] = alpha << 24 | red << 16 | green << 8 | blue;

        } else if (pixelDepth == 24) {
          int blue = in.read() & 0xFF;
          int green = in.read() & 0xFF;
          int red = in.read() & 0xFF;
          result.pixels[srcLine + j] = red << 16 | green << 8 | blue;

        } else if (pixelDepth == 16) {
          // read little endian
          int second = in.read() & 0xFF;
          int first = in.read() & 0xFF;

          // Always read the alpha overlay bit, so it is restored on save.
          // For original game files, all the mov-images, as well as the i_ho30.tga file
          // have the flag set, even though it is ignored by the attribute bit number.
          // But in mods the usage is mixed, and it doesn't harm to keep that alpha information.
          // Whether to use the alpha channel for display, is set by the getImage and getThumbnail color models.
          boolean hidden = (first & 0x80) == 0x80;
          // detect whether the 16-bit overlay transparency is actually used
          if (hidden)
            result.hasAlpha = true;

          // parse 16-bit TGA format
          // alpha: 1000 0000 0000 0000
          int alpha = hidden ? FULL_TRANSPARENCY : NO_TRANSPARENCY;
          // red:   0111 1100 0000 0000
          int red = ((first << 1) & 0xF8) & 0xFF;
          // green: 0000 0011 1110 0000
          int green = (((first << 6) & 0xC0) | ((second >>> 2) & 0x38)) & 0xFF;
          // blue:  0000 0000 0001 1111
          int blue = (second << 3) & 0xFF;

          result.pixels[srcLine + j] = alpha << 24 | red << 16 | green << 8 | blue;
        }
      }
    }

    return result;
  }

  // convert from 32-bit pixel buffer
  public static void writePixelStream(OutputStream out, int[] pixels, int width, int height,
      int pixelDepth, boolean invertLines) throws IOException {

    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        // invert TGA pixel lines
        int idx = invertLines ? ((height - 1) - y) * width + x : y * width + x;
        writePixel(out, pixels[idx], pixelDepth);
      }
    }
  }

  public static void writePixel(OutputStream out, int pixel, int pixelDepth) throws IOException {
    // write little endian
    if (pixelDepth == 32) {
      StreamTools.write(out, pixel, true);
    }
    else if (pixelDepth == 24) {
      out.write(pixel & 0xFF);
      out.write((pixel >>> 8) & 0xFF);
      out.write((pixel >>> 16) & 0xFF);
    }
    else if (pixelDepth == 16) {
      // convert to 16-bit TGA format
      short rgb16 = ImageConversion.convert_32to16bit(pixel);
      StreamTools.write(out, rgb16, true);
    }
  }

  // convert from 16-bit pixel buffer
  public static void writePixelStream(OutputStream out, short[] pixels, int width, int height,
      int pixelDepth, boolean invertLines) throws IOException {

    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        // invert TGA pixel lines
        int idx = invertLines ? ((height - 1) - y) * width + x : y * width + x;
        writePixel(out, pixels[idx], pixelDepth);
      }
    }
  }

  public static void writePixel(OutputStream out, short pixel, int pixelDepth) throws IOException {
    // write little endian
    if (pixelDepth == 32) {
      // convert to 32-bit TGA format
      int rgb32bit = ImageConversion.convert_16to32bit(pixel);
      StreamTools.write(out, rgb32bit, true);
    }
    else if (pixelDepth == 24) {
      // convert to 32-bit TGA format
      int rgb32 = ImageConversion.convert_16to32bit(pixel);
      // strip transparency
      out.write(rgb32 & 0xFF);
      out.write((rgb32 >>> 8) & 0xFF);
      out.write((rgb32 >>> 16) & 0xFF);
    }
    else if (pixelDepth == 16) {
      StreamTools.write(out, pixel, true);
    }
  }

  // convert from pixel data buffer
  public static void writePixelStream(OutputStream out, byte[] pixelData, int width, int height,
      int srcPixelDepth, int tgtPixelDepth, boolean invertLines) throws IOException {

    if (!invertLines && srcPixelDepth == tgtPixelDepth) {
      out.write(pixelData);
      return;
    }

    // invert TGA pixel lines
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        int idx = invertLines ? ((height - 1) - y) * width + x : y * width + x;

        // write little endian
        if (srcPixelDepth == 32) {
          idx *= 4;
          int pixel = pixelData[idx] << 24 | pixelData[idx+1] << 16 | pixelData[idx+2] << 8 | pixelData[idx+3];
          writePixel(out, pixel, tgtPixelDepth);
        }
        else if (srcPixelDepth == 24) {
          idx *= 3;
          int pixel = 0xFF000000 | pixelData[idx] << 16 | pixelData[idx+1] << 8 | pixelData[idx+2];
          writePixel(out, pixel, tgtPixelDepth);
        }
        else if (srcPixelDepth == 16) {
          idx *= 2;
          short pixel = (short) (pixelData[idx] << 8 | pixelData[idx+1]);
          writePixel(out, pixel, tgtPixelDepth);
        }
      }
    }
  }

}
