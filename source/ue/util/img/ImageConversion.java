package ue.util.img;

import java.nio.ByteBuffer;
import ue.util.data.DataTools;
import ue.util.data.ValueCast;

/**
 * Image conversion helper class.
 */
public abstract class ImageConversion {

  public static final int COLOR_MASK_16BIT_RED = 0x7C00;
  public static final int COLOR_MASK_16BIT_GREEN = 0x3E0;
  public static final int COLOR_MASK_16BIT_BLUE = 0x1F;
  public static final int COLOR_MASK_16BIT_ALPHA = 0x8000;
  public static final int COLOR_MASK_16BIT_RGB = 0x7FFF;
  public static final int COLOR_MASK_32BIT_RED = 0xFF0000;
  public static final int COLOR_MASK_32BIT_GREEN = 0xFF00;
  public static final int COLOR_MASK_32BIT_BLUE = 0xFF;
  public static final int COLOR_MASK_32BIT_ALPHA = 0xFF000000;
  public static final int COLOR_MASK_32BIT_RGB = 0xFFFFFF;
  public static final int HUE_MAX_16BIT = 0x1F;
  public static final double HUE_FAC_16TO32 = 255.0 / HUE_MAX_16BIT;

  public static int bitDepthBytes(int bitDepth) {
    // add 7 for up rounding
    return (bitDepth + 7) >>> 3;
  }

  /**
   * Converts an arbitrary byte value to a 32 bit pixel.
   */
  public static int read32BitPixel(byte[] data, int offset, int bytesPerValue, boolean littleEndian) {
    switch (bytesPerValue) {
      case 1:
        return Byte.toUnsignedInt(data[offset]);
      case 2: {
        int b1 = data[offset] & 0xFF;
        int b2 = data[offset+1] & 0xFF;
        return littleEndian ? (b2 << 8) | b1 : (b1 << 8) | b2;
      }
      case 3: {
        int b1 = data[offset] & 0xFF;
        int b2 = data[offset+1] & 0xFF;
        int b3 = data[offset+2] & 0xFF;
        int pixel = littleEndian ? (b3 << 16) | (b2 << 8) | b1 : (b1 << 16) | (b2 << 8) | b3;
        return COLOR_MASK_32BIT_ALPHA | pixel;
      }
      case 4:
        return DataTools.toInt(data, offset, littleEndian);
      default:
        throw new IllegalArgumentException();
    }
  }

  /**
   * Converts an arbitrary byte value array to a 32 bit pixel array.
   */
  public static int[] read32BitPixels(byte[] data, int bytesPerValue, boolean littleEndian) {
    switch (bytesPerValue) {
      case 4:
        return DataTools.toIntArray(data, littleEndian);
      case 2:
      case 3: {
        int[] result = new int[data.length / bytesPerValue];
        for (int i = 0; i < result.length; ++i) {
          result[i] = read32BitPixel(data, i * bytesPerValue, bytesPerValue, littleEndian);
        }
        return result;
      }
      case 1:
        return ValueCast.castUIntArray(data);
      default:
        throw new UnsupportedOperationException();
    }
  }

  /**
   * Converts an arbitrary byte value array to a 16 bit pixel array.
   */
  public static short[] read16BitPixels(byte[] data, int bytesPerValue, boolean littleEndian) {
    switch (bytesPerValue) {
      case 2:
        return DataTools.toShortArray(data, littleEndian);
      case 1:
        return ValueCast.castUShortArray(data);
      default:
        throw new UnsupportedOperationException();
    }
  }

  public static int getPixel(ByteBuffer pixels, int index, int bitDepth) {
    switch (bitDepth) {
      case 15:
      case 16: {
        return pixels.getShort(index * 2);
      }
      case 24: {
        byte[] raw = pixels.array();
        return Byte.toUnsignedInt(raw[index * 3 + 2]) << 16
          | Byte.toUnsignedInt(raw[index * 3 + 1]) << 8
          | Byte.toUnsignedInt(raw[index * 3]);
      }
      case 32: {
        return pixels.getInt(index * 4);
      }
    }

    String err = "Invalid bit depth: " + bitDepth;
    throw new RuntimeException(err);
  }

  public static int convert_16to32bit(int pixel16bit) {
    // alpha: 1000 0000 0000 0000
    boolean visible = (pixel16bit & COLOR_MASK_16BIT_ALPHA) != 0;
    int alpha = visible ? 0 : COLOR_MASK_32BIT_ALPHA;
    // red:   0111 1100 0000 0000
    int red = (pixel16bit >>> 10) & 0x1F;
    red = (int) Math.round(red * HUE_FAC_16TO32);
    // green: 0000 0011 1110 0000
    int green = (pixel16bit >>> 5) & 0x1F;
    green = (int) Math.round(green * HUE_FAC_16TO32);
    // blue:  0000 0000 0001 1111
    int blue = pixel16bit & 0x1F;
    blue = (int) Math.round(blue * HUE_FAC_16TO32);

    // argb:  AA RR GG BB (32bit)
    return alpha | red << 16 | green << 8 | blue;
  }

  public static int[] convert_32to16bit(short[] pixels16bit) {
    int[] result = new int[pixels16bit.length];
    for (int i = 0; i < pixels16bit.length; ++i) {
      result[i] = convert_16to32bit(pixels16bit[i]);
    }
    return result;
  }

  public static int[] convert_16to32bit_inverted(short[] pixels16bit, int lineWidth) {
    int height = pixels16bit.length / lineWidth;
    int[] result = new int[lineWidth * height];

    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < lineWidth; ++x) {
        int sourceIndex = (y * lineWidth) + x;
        int targetIndex = ((height - y) * lineWidth) + x;
        result[targetIndex] = convert_16to32bit(pixels16bit[sourceIndex]);
      }
    }
    return result;
  }

  public static short convert_32to16bit(int pixel32bit) {
    // alpha: FF 00 00 00
    boolean visible = (pixel32bit & COLOR_MASK_32BIT_ALPHA) != 0;
    int alpha = visible ? 0 : COLOR_MASK_16BIT_ALPHA;
    // red:   00 FF 00 00
    int red = (pixel32bit >>> 19) & 0x1F;
    // green: 00 00 FF 00
    int green = (pixel32bit >>> 11) & 0x1F;
    // blue:  00 00 00 FF
    int blue = (pixel32bit >>> 3) & 0x1F;

    // argb:  arrr rrgg gggb bbbb (16bit)
    return (short) (alpha | red << 10 | green << 5 | blue);
  }

  public static short[] convert_32to16bit(int[] pixels32bit) {
    short[] result = new short[pixels32bit.length];
    for (int i = 0; i < pixels32bit.length; ++i) {
      result[i] = convert_32to16bit(pixels32bit[i]);
    }
    return result;
  }

  public static short[] convert_32to16bit_inverted(int[] pixels32bit, int lineWidth) {
    int height = pixels32bit.length / lineWidth;
    short[] result = new short[lineWidth * height];

    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < lineWidth; ++x) {
        int sourceIndex = (y * lineWidth) + x;
        int targetIndex = ((height - y) * lineWidth) + x;
        result[targetIndex] = convert_32to16bit(pixels32bit[sourceIndex]);
      }
    }
    return result;
  }

  public static int pixelColor(int pixel, int colorMask, int bitOffset, int bitCnt) {
    // color value
    int colVal = (pixel & colorMask) >>> bitOffset;
    int max = colorMask >>> bitOffset;
    double fac = 255.0 / max;
    // adjust luminance
    if (bitCnt != 8)
      colVal = (int) (colVal * fac);
    return colVal;
  }

  public static void copyPixel(byte[] pixelData, byte[] sourceData, int pixelDepth, int targetIndex, int sourceIndex) {
    pixelData[targetIndex] = sourceData[sourceIndex];
    if (pixelDepth > 8)
      pixelData[targetIndex+1] = sourceData[sourceIndex + 1];
    if (pixelDepth > 16)
      pixelData[targetIndex+2] = sourceData[sourceIndex + 2];
    if (pixelDepth > 24)
      pixelData[targetIndex+3] = sourceData[sourceIndex + 3];
  }

  public static void invertLines(byte[] data, int bytesPerLine) {
    int rows = data.length / bytesPerLine;
    int halfRows = rows >> 1;

    for (int r = 0; r < halfRows; ++r) {
      for (int c = 0; c < bytesPerLine; ++c) {
        int pixelIndex = r * bytesPerLine + c;
        int invertIndex = ((rows - r) - 1) * bytesPerLine + c;
        byte pixelCopy = data[pixelIndex];
        data[pixelIndex] = data[invertIndex];
        data[invertIndex] = pixelCopy;
      }
    }
  }

}
