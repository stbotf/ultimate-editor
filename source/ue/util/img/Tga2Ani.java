package ue.util.img;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import lombok.val;
import lombok.var;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.Pair;

/**
 * This class is used to convert a group of 16-bit tga images to an ani/cur file.
 */
public class Tga2Ani {

  public static final byte[] TGA_HEADER = new byte[] {
    0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0
  };

  private LinkedList<Pair<String, byte[]>> DATA = new LinkedList<>();
  private short frameh; //frame height
  private short framew; //frame width
  int size = 34; //animation size in bytes (34 bytes is for the ani header)

  /**
   * Adds a frame to the existing group of images.
   *
   * @param name the name of the image
   * @param tga  tga data
   * @throws InvalidArgumentsException if the tga file is invalid or if the size doesn't match the
   *                                   other frames
   */
  public void addFrame(String name, byte[] tga) throws InvalidArgumentsException {
    // check if image is valid
    checkTgaFrame(tga);

    // check frame width and height to match first frame
    if (DATA.isEmpty()) {
      framew = DataTools.toShort(tga, 12, true);
      frameh = DataTools.toShort(tga, 14, true);
    } else {
      checkFrameSize(tga, framew, frameh);
    }

    size += tga.length - 18; //tga size-tga header
    DATA.add(new Pair<>(name, tga));
  }

  /**
   * Creates and returns an animation file from the frames that were added.
   *
   * @return an animation file or null if no frames were added yet.
   * @throws IOException
   */
  public AniOrCur getAniOrCur(boolean cursor) throws IOException {
    if (DATA.isEmpty())
      return null;

    ByteArrayOutputStream baos = new ByteArrayOutputStream(size);

    // BOTF ANI HEADER
    // frame dimensions
    short width = cursor ? (short) (framew / 2) : framew;
    short height = cursor ? (short) (frameh / 2) : frameh;
    baos.write(DataTools.toByte(height, true));
    baos.write(DataTools.toByte(width, true));

    // number of frames
    baos.write(DataTools.toByte((short) DATA.size(), true));

    // delay in miliseconds (this is generic)
    baos.write(DataTools.toByte((short) 1000, true));

    // tga dimensions, placing data as if it were a cursor
    baos.write(DataTools.toByte(framew, true));
    baos.write(DataTools.toByte((short) (DATA.size() * frameh), true));

    // size
    baos.write(DataTools.toByte(size - 16, true));

    // tga header
    baos.write(TGA_HEADER);

    // tga dimensions, placing data as if it were a cursor
    baos.write(DataTools.toByte(framew, true));
    baos.write(DataTools.toByte((short) (DATA.size() * frameh), true));

    // tga header index 32-33
    baos.write(16);
    baos.write(1);

    // data
    for (int i = DATA.size() - 1; i >= 0; i--) {
      val frame = DATA.get(i);
      for (int j = 0; j < frameh * framew; j++) {
        baos.write(frame.RIGHT[2 * j + 18]);
        baos.write(frame.RIGHT[2 * j + 19]);
      }
    }

    return new AniOrCur(baos.toByteArray(), "finito.cur"); //$NON-NLS-1$
  }

  /**
   * Returns a list of frames that were added (their names), null if none.
   */
  public String[] getFrameList() {
    return DATA.size() <= 0 ? null :
      DATA.stream().map(x-> x.LEFT).toArray(String[]::new);
  }

  /**
   * Removes all frames of the given name from the list.
   * @param str the name of the image to remove
   */
  public void remove(String str) {
    DATA.removeIf(x -> {
      if (x.LEFT.equals(str)) {
        size -= x.RIGHT.length - 18;
        return true;
      }
      return false;
    });
  }

  /**
   * Clears frames, removes all frames from the list.
   */
  public void clear() {
    DATA.clear();
    size = 34;
  }

  /**
   * Moves the image a position up.
   */
  public void moveUp(String str) {
    // use list iterator to alter linked lists
    val it = DATA.listIterator();
    if (!it.hasNext())
      return;

    var prev = it.next();

    while (it.hasNext()) {
      val frame = it.next();
      if (frame.LEFT.equals(str)) {
        it.set(prev);
        it.previous();  // cursor is positioned between elements,
        it.previous();  // so it must be moved twice on altering call
        it.set(frame);
        break;
      }
      prev = frame;
    }
  }

  /**
   * Moves the image a position down.
   */
  public void moveDown(String str) {
    // use list iterator to alter linked lists
    val it = DATA.listIterator();
    if (!it.hasNext())
      return;

    var frame = it.next();

    while (it.hasNext()) {
      if (frame.LEFT.equals(str)) {
        val next = it.next();
        it.set(frame);
        it.previous();  // cursor is positioned between elements,
        it.previous();  // so it must be moved twice on altering call
        it.set(next);
        break;
      }
      frame = it.next();
    }
  }

  public void remove(int index) {
    if (index < 0)
      return;

    val frame = DATA.get(index);
    size -= frame.RIGHT.length - 18;
    DATA.remove(index);
  }

  /**
   * Checks whether a frame is in a valid tga format.
   * @param tga     tga data
   * @throws InvalidArgumentsException
   */
  private static void checkTgaFrame(byte[] tga) throws InvalidArgumentsException {
    if (tga[0] != 0 || tga[1] != 0 || tga[2] != 2 || tga[3] != 0 || tga[4] != 0
        || tga[5] != 0 || tga[6] != 0 || tga[7] != 0 || tga[8] != 0
        || tga[9] != 0 || tga[10] != 0 || tga[11] != 0 || tga[16] != 16) {
      throw new InvalidArgumentsException(Language.getString("Tga2Ani.2")); //$NON-NLS-1$
    }
  }

  /**
   * Checks a frame for valid extends.
   *
   * @param tga     tga data
   * @param width   expected width
   * @param height  expected height
   * @throws InvalidArgumentsException if the tga file is invalid or if the size doesn't match.
   */
  private static void checkFrameSize(byte[] tga, short width, short height) throws InvalidArgumentsException {
    short frameWidth = DataTools.toShort(tga, 12, true);
    if (frameWidth != width) {
      final String err = Language.getString("Tga2Ani.0"); //$NON-NLS-1$
      throw new InvalidArgumentsException(err.replace("%1", Short.toString(frameWidth))
        .replace("%2", Short.toString(width))); //$NON-NLS-1$ //$NON-NLS-2$
    }

    short frameHeight = DataTools.toShort(tga, 14, true);
    if (frameHeight != height) {
      final String err = Language.getString("Tga2Ani.1"); //$NON-NLS-1$
      throw new InvalidArgumentsException(err.replace("%1", Short.toString(frameHeight))
        .replace("%2", Short.toString(height))); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }
}
