package ue.util.img;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import lombok.Data;
import ue.util.data.DataTools;
import ue.util.data.HexTools;
import ue.util.stream.StreamTools;

/**
 * The GIF application extension block, introduced by '21 FF' sentinel.
 */
@Data
public class GifApplicationExtension {

  public static final int ExtensionSentinel = 0xFF;
  public static final byte[] SegmentSentinel = new byte[] { 0x21, (byte) ExtensionSentinel };

  private int appNameSize;            // unsigned byte, name + verification size = always 11
  private String appName;             // 8-byte application name plue 3 verification bytes
  private int subSize;                // unsigned byte, sub-block size
  private int subIndex;               // unsigned byte
  private int repetions;              // unsigned short number of repetions
  private byte[] unspecified = null;

  public GifApplicationExtension(InputStream in) throws IOException {
    loadData(in);
  }

  public int size() {
    return SegmentSentinel.length + 3 + appNameSize + subSize;
  }

  public void load(InputStream in) throws IOException {
    byte[] sentinel = StreamTools.readBytes(in, 2);
    if (!Arrays.equals(sentinel, SegmentSentinel))
      throw new IOException("Invalid extension sentinel!");
    loadData(in);
  }

  public void loadData(InputStream in) throws IOException {
    appNameSize = in.read();
    appName = StreamTools.readAsciiString(in, appNameSize);

    subSize = in.read();
    if (subSize < 3) {
      String err = "Invalid GIF application extension sub-block size of %1!"
        .replace("%1", Integer.toString(subSize));
      throw new IOException(err);
    }

    subIndex = in.read();
    repetions = StreamTools.readUShort(in, true);


    if (subSize != 3)
      unspecified = StreamTools.readBytes(in, appNameSize - 3);

    // extension block end
    int blockEnd = in.read();
    if (blockEnd != 0) {
      String err = "Invalid GIF block end '%1' detected!"
        .replace("%1", HexTools.toHex((byte) blockEnd));
      System.out.println(err);
    }
  }

  public void save(OutputStream out) throws IOException {
    out.write(SegmentSentinel);

    appNameSize = appName.length();
    out.write(appNameSize);
    subSize = unspecified != null ? 3 + unspecified.length : 3;
    out.write(subSize);
    out.write(subIndex);
    out.write(DataTools.toByte((short) repetions, true));

    if (unspecified != null)
      out.write(unspecified);

    // extension block end
    out.write(0);
  }

}
