package ue.util.img;

import java.awt.Image;
import lombok.Getter;
import lombok.experimental.Accessors;

public class ImageFile {
  @Getter @Accessors(fluent = true) private String name;
  @Getter @Accessors(fluent = true) private Image image;
  @Getter @Accessors(fluent = true) private int size;

  public ImageFile(String name, Image img, int size) {
    this.name = name;
    this.image = img;
    this.size = size;
  }
}
