package ue.util.img;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import lombok.Data;
import ue.exception.UnsupportedImage;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * The GIF image data block, introduced by '2C' sentinel.
 */
@Data
public class GifImageData {

  public static final int   SegmentSentinel = 0x2C;

  private static final boolean  LOCAL_COLOR_TABLE_FLAG = false;
  private static final byte     LOCAL_RESERVED = 0x00;

  private int IMAGE_LEFT_POSITION;            // 2 bytes (unsigned short)
  private int IMAGE_TOP_POSITION;             // 2 bytes (unsigned short)
  private int IMAGE_WIDTH;                    // 2 bytes (unsigned short)
  private int IMAGE_HEIGHT;                   // 2 bytes (unsigned short)
  // LOCAL_COLOR_TABLE_FLAG                   // 1 bit   (boolean)
  private boolean LOCAL_INTERLACE_FLAG;       // 1 bit   (boolean)
  private boolean LOCAL_SORT_FLAG;            // 1 bit   (boolean)
  // LOCAL_RESERVED                           // 2 bits
  private int LOCAL_COLOR_TABLE_SIZE;         // 3 bit
  private int IMAGE_START;                    // 1 byte (unsigned byte)
  private int LZW_SIZE = 0;
  private ArrayList<byte[]> LZW_DATA = new ArrayList<>();

  public GifImageData(InputStream in) throws IOException {
    loadData(in);
  }

  public int size() {
    // sentinel + descriptor + LZW data + termination
    return 13 + LZW_SIZE;
  }

  public void load(InputStream in) throws IOException {
    int sentinel = in.read();
    if (sentinel != SegmentSentinel)
      throw new IOException("Invalid image descriptor sentinel!");
    loadData(in);
  }

  public void loadData(InputStream in) throws IOException {
    //IMAGE_LEFT_POSITION
    IMAGE_LEFT_POSITION = StreamTools.readUShort(in, true);   // READ 2 BYTES
    //IMAGE_TOP_POSITION
    IMAGE_TOP_POSITION = StreamTools.readUShort(in, true);    // READ 2 BYTES
    //IMAGE_WIDTH
    IMAGE_WIDTH = StreamTools.readUShort(in, true);           // READ 2 BYTES
    //IMAGE_HEIGHT
    IMAGE_HEIGHT = StreamTools.readUShort(in, true);          // READ 2 BYTES

    //LOCAL_COLOR_TABLE_FLAG
    int flags = in.read();                                    // READ 1 BYTE
    if (LOCAL_COLOR_TABLE_FLAG != ((0x80 & flags) == 0x80))
      throw new UnsupportedImage(Language.getString("GifImageData.0")); //$NON-NLS-1$
    //LOCAL_RESERVED
    if (LOCAL_RESERVED != (byte) ((flags >>> 3) & 0x03))
      throw new UnsupportedImage(Language.getString("GifImageData.1")); //$NON-NLS-1$

    //LOCAL_INTERLACE_FLAG
    LOCAL_INTERLACE_FLAG = ((0x40 & flags) == 0x40);
    //LOCAL_SORT_FLAG, don't care
    LOCAL_SORT_FLAG = ((0x20 & flags) == 0x20);
    //LOCAL_COLOR_TABLE_SIZE
    LOCAL_COLOR_TABLE_SIZE = (byte) (0x07 & flags);
    //IMAGE_START
    IMAGE_START = in.read();                                  // READ 1 BYTE

    // read image data
    while (true) {
      //LZW_SIZE
      int lzwSize = in.read();                                // READ 1 BYTE
      // check for the extension block end
      if (lzwSize == 0)
        break;
      //LZW_DATA
      LZW_DATA.add(StreamTools.readBytes(in, lzwSize));
      LZW_SIZE += lzwSize;
    }
  }

  public void save(OutputStream out) throws IOException {
    // write extension block identifier
    out.write(SegmentSentinel);                                       // 1
    //IMAGE_LEFT_POSITION
    out.write(DataTools.toByte((short)IMAGE_LEFT_POSITION, true));    // 2
    //IMAGE_TOP_POSITION
    out.write(DataTools.toByte((short)IMAGE_TOP_POSITION, true));     // 2
    //IMAGE_WIDTH
    out.write(DataTools.toByte((short)IMAGE_WIDTH, true));            // 2
    //IMAGE_HEIGHT
    out.write(DataTools.toByte((short)IMAGE_HEIGHT, true));           // 2

    //LOCAL_COLOR_TABLE_FLAG
    int flags = 0;
    //LOCAL_INTERLACE_FLAG
    if (LOCAL_INTERLACE_FLAG)
      flags |= 0x40;
    //LOCAL_SORT_FLAG
    if (LOCAL_SORT_FLAG)
      flags |= 0x20;
    //LOCAL_RESERVED: zeros
    //LOCAL_COLOR_TABLE_SIZE, usually zero
    flags |= LOCAL_COLOR_TABLE_SIZE;
    out.write(flags);                                                 // 1

    //IMAGE_START
    out.write(IMAGE_START);                                           // 1
    //LZW_DATA
    if (LZW_DATA != null) {
      for (byte[] data : LZW_DATA) {
        //LZW_SIZE
        out.write(data.length);                                       // 1
        //LZW_DATA
        out.write(data);
      }
    }

    // extension block end
    out.write(0);                                                     // 1
  }

  public void clear() {
    IMAGE_LEFT_POSITION = 0;
    IMAGE_TOP_POSITION = 0;
    IMAGE_WIDTH = 0;
    IMAGE_HEIGHT = 0;
    LOCAL_INTERLACE_FLAG = false;
    LOCAL_SORT_FLAG = false;
    LOCAL_COLOR_TABLE_SIZE = 0;
    IMAGE_START = 0;
    LZW_SIZE = 0;
    LZW_DATA = null;
  }

}
