package ue.util.img;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import lombok.Data;
import ue.util.data.DataTools;
import ue.util.data.HexTools;
import ue.util.stream.StreamTools;

/**
 * The GIF control extension block, introduced by '21 F9' sentinel.
 */
@Data
public class GifControlExtension {

  public static final int ExtensionSentinel = 0xF9;
  public static final byte[] SegmentSentinel = new byte[] { 0x21, (byte) ExtensionSentinel };

  private int dataSize;               // unsigned byte
  private int backgroundColor;        // unsigned byte, 1 = transparent
  private int animationDelay;         // unsigned short, in hundredths of a second
  private int transparentColorIndex;  // unsigned byte, color table index for transparent color, FF = unused
  private byte[] unspecified = null;

  public GifControlExtension(InputStream in) throws IOException {
    loadData(in);
  }

  public int size() {
    return SegmentSentinel.length + 2 + dataSize;
  }

  public void load(InputStream in) throws IOException {
    byte[] sentinel = StreamTools.readBytes(in, 2);
    if (!Arrays.equals(sentinel, SegmentSentinel))
      throw new IOException("Invalid extension sentinel!");
    loadData(in);
  }

  public void loadData(InputStream in) throws IOException {
    dataSize = in.read();
    if (dataSize < 4) {
      String err = "Invalid control extension size of %1!"
        .replace("%1", Integer.toString(dataSize));
      throw new IOException(err);
    }

    backgroundColor = in.read();
    animationDelay = StreamTools.readUShort(in, true);
    transparentColorIndex = in.read();

    if (dataSize != 4)
      unspecified = StreamTools.readBytes(in, dataSize - 4);

    // extension block end
    int blockEnd = in.read();
    if (blockEnd != 0) {
      String err = "Invalid block end '%1' detected!"
        .replace("%1", HexTools.toHex((byte) blockEnd));
      System.out.println(err);
    }
  }

  public void save(OutputStream out) throws IOException {
    out.write(SegmentSentinel);

    dataSize = unspecified != null ? 4 + unspecified.length : 4;
    out.write(dataSize);
    out.write(backgroundColor);
    out.write(DataTools.toByte((short) animationDelay, true));
    out.write(transparentColorIndex);

    if (unspecified != null)
      out.write(unspecified);

    // extension block end
    out.write(0);
  }

}
