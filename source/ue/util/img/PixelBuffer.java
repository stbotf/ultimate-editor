package ue.util.img;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import lombok.Getter;
import ue.util.data.DataTools;
import ue.util.data.IsCloneable;
import ue.util.data.ValueCast;
import ue.util.stream.StreamTools;

// Abstraction layer for the TargaImage pixel data.
public class PixelBuffer implements IsCloneable {

  public enum DataType {
    Byte,
    Short,
    Int
  }

  @Getter private int bytesPerPixel;
  @Getter private int width;
  @Getter private int height;
  @Getter private DataType dataType;
  @Getter private Buffer dataBuffer;

  public PixelBuffer(int width, int height, int bytesPerPixel) {
    this.bytesPerPixel = bytesPerPixel;
    this.width = width;
    this.height = height;
    this.dataType = pixelBytesType(bytesPerPixel);
  }

  public static DataType pixelBytesType(int bytesPerPixel) {
    return bytesPerPixel > 2 ? DataType.Int : bytesPerPixel == 2 ? DataType.Short : DataType.Byte;
  }

  public static PixelBuffer allocate(int width, int height, int bytesPerPixel) {
    PixelBuffer pb = new PixelBuffer(width, height, bytesPerPixel);

    switch (pb.bytesPerPixel) {
      case 4:
      case 3:
        pb.dataBuffer = IntBuffer.allocate(width * height);
        pb.dataType = DataType.Int;
        break;
      case 2:
        pb.dataBuffer = ShortBuffer.allocate(width * height);
        pb.dataType = DataType.Short;
        break;
      case 1:
        pb.dataBuffer = ByteBuffer.allocate(width * height);
        pb.dataType = DataType.Byte;
        break;
      default:
        throw new IllegalArgumentException("Invalid bit depth.");
    }

    return pb;
  }

  public static PixelBuffer wrap(int[] pixels, int width, int height) {
    PixelBuffer pb = new PixelBuffer(width, height, 4);
    pb.dataBuffer = IntBuffer.wrap(pixels);
    pb.dataType = DataType.Int;
    return pb;
  }

  public static PixelBuffer wrap(short[] pixels, int width, int height) {
    PixelBuffer pb = new PixelBuffer(width, height, 2);
    pb.dataBuffer = ShortBuffer.wrap(pixels);
    pb.dataType = DataType.Short;
    return pb;
  }

  public static PixelBuffer wrap(byte[] pixels, int width, int height, int bytesPerPixel, boolean littleEndian) {
    PixelBuffer pb = new PixelBuffer(width, height, bytesPerPixel);
    pb.dataBuffer = ByteBuffer.wrap(pixels).order(littleEndian
      ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
    pb.dataType = DataType.Byte;
    return pb;
  }

  public int getPixelCount() {
    switch (dataType) {
      case Int:
      case Short:
        return dataBuffer.limit();
      default:
        return dataBuffer.limit() / bytesPerPixel;
    }
  }

  public int getPixel(int index) {
    switch (dataType) {
      case Int:
        return ((IntBuffer) dataBuffer).get(index);
      case Short:
        return ((ShortBuffer) dataBuffer).get(index);
      default:
        ByteBuffer buffer = (ByteBuffer) dataBuffer;
        switch (bytesPerPixel) {
          case 4:
            return buffer.getInt(index * bytesPerPixel);
          case 3:
            return ImageConversion.read32BitPixel(buffer.array(), index * bytesPerPixel,
              bytesPerPixel, buffer.order() == ByteOrder.LITTLE_ENDIAN);
          case 2:
            return buffer.getShort(index * bytesPerPixel);
          default:
            return Byte.toUnsignedInt(buffer.get(index * bytesPerPixel));
        }
    }
  }

  public int getPixel(int x, int y) {
    int index = y * width + x;
    return getPixel(index);
  }

  public void setPixel(int index, int value) {
    switch (dataType) {
      case Int:
        ((IntBuffer) dataBuffer).put(index, value);
        break;
      case Short:
        ((ShortBuffer) dataBuffer).put(index, (short) value);
        break;
      default:
        ByteBuffer buffer = (ByteBuffer) dataBuffer;
        switch (bytesPerPixel) {
          case 4:
            buffer.putInt(index * bytesPerPixel, value);
            break;
          case 3:
            boolean le = buffer.order() == ByteOrder.LITTLE_ENDIAN;
            byte b1 = (byte) (le ? value : value >>> 2);
            byte b2 = (byte) (value >>> 1);
            byte b3 = (byte) (le ? value >>> 2 : value);
            buffer.put(index * bytesPerPixel, (byte) b1);
            buffer.put(index * bytesPerPixel + 1, (byte) b2);
            buffer.put(index * bytesPerPixel + 2, (byte) b3);
            break;
          case 2:
            buffer.putShort(index * bytesPerPixel, (short) value);
            break;
          default:
            buffer.put(index * bytesPerPixel, (byte) value);
            break;
        }
        break;
    }
  }

  public void setPixel(int x, int y, int value) {
    int index = y * width + x;
    setPixel(index, value);
  }

  public void fillPixels(int index, int num, int value) {
    switch (dataType) {
      case Int: {
        int[] data = ((IntBuffer) dataBuffer).array();
        Arrays.fill(data, index, index + num, value);
        break;
      }
      case Short: {
        short[] data = ((ShortBuffer) dataBuffer).array();
        Arrays.fill(data, index, index + num, (short) value);
        break;
      }
      default:
        ByteBuffer buffer = (ByteBuffer) dataBuffer;
        if (value == 0 || bytesPerPixel == 1) {
          Arrays.fill(buffer.array(), index * bytesPerPixel, (index + num) * bytesPerPixel, (byte) value);
        } else {
          for (int i = index; i < index + num; ++i)
            setPixel(i, value);
        }
        break;
    }
  }

  public int[] toIntArray() {
    switch (dataType) {
      case Int: {
        return ((IntBuffer) dataBuffer).array();
      }
      case Short: {
        short[] data = ((ShortBuffer) dataBuffer).array();
        return ValueCast.castUIntArray(data);
      }
      default: {
        ByteBuffer buffer = ((ByteBuffer) dataBuffer);
        boolean le = buffer.order() == ByteOrder.LITTLE_ENDIAN;
        return ImageConversion.read32BitPixels(buffer.array(), bytesPerPixel, le);
      }
    }
  }

  public short[] toShortArray() {
    switch (dataType) {
      case Int: {
        int[] data = ((IntBuffer) dataBuffer).array();
        return ValueCast.castShortArray(data);
      }
      case Short: {
        return ((ShortBuffer) dataBuffer).array();
      }
      default: {
        ByteBuffer buffer = ((ByteBuffer) dataBuffer);
        boolean le = buffer.order() == ByteOrder.LITTLE_ENDIAN;
        return ImageConversion.read16BitPixels(buffer.array(), bytesPerPixel, le);
      }
    }
  }

  public byte[] toByteArray(boolean littleEndian) {
    switch (dataType) {
      case Int: {
        int[] data = ((IntBuffer) dataBuffer).array();
        return DataTools.toByte(data, littleEndian);
      }
      case Short: {
        short[] data = ((ShortBuffer) dataBuffer).array();
        return DataTools.toByte(data, littleEndian);
      }
      default: {
        ByteBuffer buffer = ((ByteBuffer) dataBuffer);
        boolean le = buffer.order() == ByteOrder.LITTLE_ENDIAN;
        if (le != littleEndian)
          throw new UnsupportedOperationException();
        return buffer.array();
      }
    }
  }

  public int size() {
    switch (dataType) {
      case Int:
      case Short:
        return dataBuffer.limit() * bytesPerPixel;
      default:
        return dataBuffer.limit();
    }
  }

  // resize expands or shrinks the image but doesn't scale it
  public PixelBuffer resizedCopy(int resizedWidth, int resizedHeight, int fillColor, boolean center) {
    if (width == resizedWidth && height == resizedHeight)
      return clone();

    PixelBuffer resized = PixelBuffer.allocate(resizedWidth, resizedHeight, bytesPerPixel);

    // calculate canvas extent changes
    int extendedWidth = Integer.max(resizedWidth - width, 0);
    int extendedHeight = Integer.max(resizedWidth - height, 0);
    int leftBorderWidth = center ? extendedWidth / 2 : 0;
    int topBorderHeight = center ? extendedHeight / 2 : 0;
    int rightBorderWidth = center ? extendedWidth - leftBorderWidth: extendedWidth;
    int bottomBorderHeight = center ? extendedHeight - topBorderHeight : extendedHeight;

    // fill upper border
    if (fillColor != 0 && topBorderHeight > 0)
      resized.fillPixels(0, topBorderHeight * resizedWidth, fillColor);

    // write image
    int croppedWidth = Integer.min(width, resizedWidth);
    int croppedHeight = Integer.min(height, resizedHeight);
    int skippedRows = (width - croppedWidth) / 2;
    int skippedColumns = (height - croppedHeight) / 2;
    for (int i = 0; i < croppedHeight; ++i) {
      int sourceRowIndex = (skippedRows + i) * width;
      int targetRowIndex = (topBorderHeight + i) * resizedWidth;

      // fill left border
      if (fillColor != 0 && leftBorderWidth > 0)
        resized.fillPixels(targetRowIndex, leftBorderWidth, fillColor);

      // copy image row
      for (int j = 0; j < croppedWidth; ++j) {
        int sourceIndex = sourceRowIndex + skippedColumns + j;
        int targetIndex = targetRowIndex + leftBorderWidth + j;
        resized.setPixel(targetIndex, getPixel(sourceIndex));
      }

      // fill right border
      if (fillColor != 0 && rightBorderWidth > 0) {
        int targetIndex = targetRowIndex + leftBorderWidth + croppedWidth;
        resized.fillPixels(targetIndex, rightBorderWidth, fillColor);
      }
    }

    // fill bottom border
    if (fillColor != 0 && bottomBorderHeight > 0) {
      int rowIndex = (topBorderHeight + croppedHeight) * resizedWidth;
      resized.fillPixels(rowIndex, bottomBorderHeight * resizedWidth, fillColor);
    }

    return resized;
  }

  public void load(InputStream in, boolean invertLines) throws IOException {
    // speed load by pre-loading all the bytes
    byte[] data = StreamTools.readBytes(in, width * height * bytesPerPixel);
    ImageConversion.invertLines(data, width * bytesPerPixel);

    switch (dataType) {
      case Int:
        dataBuffer = IntBuffer.wrap(ImageConversion.read32BitPixels(data, bytesPerPixel, true));
        break;
      case Short:
        dataBuffer = ShortBuffer.wrap(DataTools.toShortArray(data, true));
        break;
      default:
        dataBuffer = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN);
        break;
    }
  }

  public void save(OutputStream out, boolean invertLines) throws IOException {
    save(out, invertLines, bytesPerPixel << 3);
  }

  public void save(OutputStream out, boolean invertLines, int bitDepth) throws IOException {
    // convert bit depth
    switch (dataType) {
      case Int: {
        int[] buffer = ((IntBuffer) dataBuffer).array();
        TgaHelper.writePixelStream(out, buffer, width, height, bitDepth, true);
        break;
      }
      case Short: {
        short[] buffer = ((ShortBuffer) dataBuffer).array();
        TgaHelper.writePixelStream(out, buffer, width, height, bitDepth, true);
        break;
      }
      default: {
        byte[] buffer = ((ByteBuffer) dataBuffer).array();
        TgaHelper.writePixelStream(out, buffer, width, height, bytesPerPixel << 3, bitDepth, true);
        break;
      }
    }
  }

  @Override
  public PixelBuffer clone() {
    switch (dataType) {
      case Int: {
        int[] data = ((IntBuffer) dataBuffer).array();
        return wrap(Arrays.copyOf(data, data.length), width, height);
      }
      case Short: {
        short[] data = ((ShortBuffer) dataBuffer).array();
        return wrap(Arrays.copyOf(data, data.length), width, height);
      }
      default: {
        ByteBuffer buffer = ((ByteBuffer) dataBuffer);
        boolean le = buffer.order() == ByteOrder.LITTLE_ENDIAN;
        byte[] data = buffer.array();
        return wrap(Arrays.copyOf(data, data.length), width, height, bytesPerPixel, le);
      }
    }
  }

}
