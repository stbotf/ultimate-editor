

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ue.util.img;

/**
 *
 */
public interface ImageSource {

  public ImageFile getImage(String name);

  public ImageFile getCharacterImage(String font, char character);
}
