

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ue.util.img;

import java.awt.Image;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public class ImageCache {

  private static final int MIN_FREE_MEMORY = 500 * 1024 * 1024; // 500MB
  private static final int FREE_MEM_BYTES = MIN_FREE_MEMORY;

  public class ImageEntry {
    public Image image;
  }

  public ImageSource source;
  public HashMap<String, ImageFile> cache = new HashMap<String, ImageFile>();
  public LinkedList<ImageFile> accessHistory = new LinkedList<ImageFile>();

  public ImageCache(ImageSource source) {
    this.source = source;
  }

  public Image getImage(String name) {
    ImageFile img = cache.get(name);

    // free memory space
    freeMemory(img);

    if (img == null) {
      img = source.getImage(name);
      cache.put(name, img);
    } else {
      accessHistory.remove(img);
    }

    // update access history
    accessHistory.addFirst(img);
    return img.image();
  }

  public Image getCharacterImage(String font, char character) {
    String name = font + File.pathSeparator + character;
    ImageFile img = cache.get(name);

    // free memory space
    freeMemory(img);

    if (img == null) {
      img = source.getCharacterImage(font, character);
      cache.put(name, img);
    } else {
      accessHistory.remove(img);
    }

    // update access history
    accessHistory.addFirst(img);
    return img.image();
  }

  private void freeMemory(ImageFile imgToSkip) {
    if (memAvailable() < MIN_FREE_MEMORY) {
      long removed = 0;
      while (!accessHistory.isEmpty() && removed < FREE_MEM_BYTES) {
        ImageFile last = accessHistory.removeLast();
        if (last == imgToSkip)
          continue;

        removed += last.size();
        cache.remove(last.name());
      }

      // notify to run the garbage collector
      Runtime.getRuntime().gc();
    }
  }

  public void clearCache() {
    accessHistory.clear();
    cache.clear();
  }

  private long memAvailable() {
    long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    return Runtime.getRuntime().maxMemory() - used;
  }
}
