package ue.util.calc;

/**
 * Random generator helper routines.
 */
public class Dice {
  // generate random values with certain propability towards the given target value
  // @returns the generated random value between 0 and below 1.0
  public static double rand(double target, double spread) {
    if (target < 0)
      return Math.random();
    if (spread < 0 || spread > 1.0)
      throw new IllegalArgumentException("Variance must be >= 0 and <= 1.0!");
    if (spread == 0.0)
      return target;

    final double r = Math.random();
    final double variance = 0.5 * (r + r * r) * spread;
    final double invariance = 1.0 - variance;
    return target * invariance + r * variance;
  }

  public static double rand(double target) {
    return rand(target, Math.random());
  }

  public static int dice(int min, int max) {
    return min + (int) ((++max - min) * Math.random());
  }
}
