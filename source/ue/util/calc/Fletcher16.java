package ue.util.calc;

/**
 * Attempt at fletcher16 implementation.
 */
public class Fletcher16 {

  private int sum1 = 0, sum2 = 0;

  public void update(byte[] data) {
    update(data, 0, data.length);
  }

  public void update(byte[] data, int off, int len) {
    len = off + len;

    for (int i = off; i < len && i < data.length; i++) {
      sum1 = (sum1 + data[i]) % 255;
      sum2 = (sum1 + sum2) % 255;
    }
  }

  public void reset() {
    sum1 = sum2 = 0;
  }

  public int getValue() {
    return (sum2 << 8) | sum1;
  }
}
