package ue.util.calc;

/**
 * ANSI CRC
 */
public class CRC16 {

  private static final int POLINOMIAL = 0x8005;

  private int crc = 0;

  public void update(byte[] data) {
    update(data, 0, data.length);
  }

  public void update(byte[] data, int off, int len) {
    int bit;

    len = len + off;

    for (int i = off; i < len && i < data.length; i++) {
      for (int b = 7; b >= 0; b--) {
        bit = (data[i] >>> b) & 1;

        if ((crc & 0x8000) != 0) {
          crc = ((crc << 1) + bit) ^ POLINOMIAL;
        } else {
          crc = (crc << 1) + bit;
        }
      }
    }

    crc = crc & 0xFFFF;
  }

  public void reset() {
    crc = 0;
  }

  public int getValue() {
    return crc;
  }
}
