package ue.util.calc;

import java.util.ArrayList;
import java.util.StringTokenizer;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;

/**
 * Math helper routines.
 */
public class Calc {
  /**
   * The calculation can contain double values, operators allowed are ^, /, *, -, + ( and ) are
   * allowed. TODO: simplify or migrate to SpEL
   *
   * @param calc the calculation
   * @return the value of the calculation
   */
  public static double calculate(String calc) throws InvalidArgumentsException {
    ArrayList<Double> values = new ArrayList<>();
    ArrayList<Integer> operators = new ArrayList<>();

    // calculate sub-calculations - remove (,)
    String subCalc;
    int i = calc.lastIndexOf("("); //$NON-NLS-1$
    int j = calc.indexOf(")", i); //$NON-NLS-1$

    while (i >= 0) {
      if (j < 0) {
        throw new InvalidArgumentsException(Language.getString("Calc.1")); //$NON-NLS-1$
      }

      subCalc = calc.substring(i + 1, j);
      double v = calculate(subCalc);

      if (v < 0) {
        subCalc = calc.substring(0, i) + "~" + Math.abs(v); //$NON-NLS-1$
      } else {
        subCalc = calc.substring(0, i) + v;
      }

      if (j < calc.length() - 1) {
        calc = subCalc + calc.substring(j + 1);
      } else {
        calc = subCalc;
      }
      i = calc.lastIndexOf("("); //$NON-NLS-1$
      j = calc.indexOf(")", i); //$NON-NLS-1$
    }
    if (j >= 0) {
      throw new InvalidArgumentsException(Language.getString("Calc.2")); //$NON-NLS-1$
    }

    // calculate
    StringTokenizer tokenizer = new StringTokenizer(calc, " ^/*-+", true); //$NON-NLS-1$

    String token;
    boolean op;

    while (tokenizer.hasMoreTokens()) {
      token = tokenizer.nextToken();

      switch (token) {
        case " ":  //$NON-NLS-1$
          continue;
        case "^":  //$NON-NLS-1$
          op = true;
          operators.add(4);
          break;
        case "/":  //$NON-NLS-1$
          op = true;
          operators.add(3);
          break;
        case "*":  //$NON-NLS-1$
          op = true;
          operators.add(2);
          break;
        case "-":  //$NON-NLS-1$
          op = true;
          operators.add(1);
          break;
        case "+":  //$NON-NLS-1$
          op = true;
          operators.add(0);
          break;
        default:
          try {
            token = token.replaceAll("~", "-"); //$NON-NLS-1$ //$NON-NLS-2$
            values.add(Double.parseDouble(token));
          } catch (Exception e) {
            throw new InvalidArgumentsException(
                Language.getString("Calc.3").replace("%1", token)); //$NON-NLS-1$ //$NON-NLS-2$
          }
          op = false;
          break;
      }

      if (!op && !operators.isEmpty()) {
        int ope = operators.get(operators.size() - 1);

        switch (ope) {
          case 1: {
            values.set(values.size() - 1, -values.get(values.size() - 1));
            operators.set(operators.size() - 1, 0);
            break;
          }
          case 3: {
            values.set(values.size() - 1, 1 / values.get(values.size() - 1));
            operators.set(operators.size() - 1, 2);
            break;
          }
          default:
        }
      }

      if (op && operators.size() > 1) {
        // check priority
        int ope1 = operators.get(operators.size() - 1);
        int ope2 = operators.get(operators.size() - 2);
        if (ope2 > ope1) {
          if (values.size() < 2) {
            throw new InvalidArgumentsException(Language.getString("Calc.4")); //$NON-NLS-1$
          }

          operators.remove(operators.size() - 2);

          double val2 = values.remove(values.size() - 1);
          double val1 = values.remove(values.size() - 1);

          val1 = calculate(val1, val2, ope2);
          values.add(val1);
        }
      }
    }

    while (!operators.isEmpty()) {
      if (values.size() < 2) {
        throw new InvalidArgumentsException(Language.getString("Calc.4")); //$NON-NLS-1$
      }
      int ope = operators.remove(operators.size() - 1);
      values
          .add(calculate(values.remove(values.size() - 2), values.remove(values.size() - 1), ope));
    }

    if (values.size() != 1) {
      throw new InvalidArgumentsException(Language.getString("Calc.5")); //$NON-NLS-1$
    }
    return values.get(0);
  }

  private static double calculate(double val1, double val2, int operator)
      throws InvalidArgumentsException {
    switch (operator) {
      case 0: // +
        return (val1 + val2);
      case 1: // -
        return (val1 - val2);
      case 2: // *
        return (val1 * val2);
      case 3: // /
        return (val1 / val2);
      case 4: // ^
        return Math.pow(val1, val2);
      default:
        throw new InvalidArgumentsException(Language.getString("Calc.6")); //$NON-NLS-1$
    }
  }

  /**
   * Returns a value that is within the given bounds.
   *
   * @param value the value to be converted to a value within bounds.
   * @param min   the minimum allowed value.
   * @param max   the maximum allowed value.
   * @return min when value is equal or less than min, max when value is equal or greater then max,
   * or the value itself when it's already within the given bounds.
   */
  public static Number getValueWithinBounds(Number value, Number min, Number max) {
    return Math.max(min.doubleValue(), Math.min(value.doubleValue(), max.doubleValue()));
  }

  /**
   * Null checked Integer::sum operation.
   *
   * Used to resolve compile time warnings like:
   * "Null type safety: parameter X provided via method descriptor Y
   *  needs unchecked conversion to conform to 'int'"
   */
  public static Integer sum(Integer x, Integer y) {
    return x != null ? y != null ? x + y : x : y;
  }
}
