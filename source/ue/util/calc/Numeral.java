package ue.util.calc;

/**
 * Number system helper routines.
 */
public class Numeral {
  static final String[] romanLetters  = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
  static final int[]    romanValues   = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

  // convert to roman number string
  public static String roman(int nbr) {
    if (nbr == 0)
      return "";

    StringBuilder sb = new StringBuilder();
    if (nbr < 0) {
      sb.append('-');
      nbr *= -1;
    }

    for (int ltr = 0; ltr < romanLetters.length; ++ltr) {
      int value = romanValues[ltr];
      int cnt = nbr / value;
      for (int i = 0; i < cnt; ++i)
        sb.append(romanLetters[ltr]);
      nbr -= value * cnt;
    }

    return sb.toString();
  }
}
