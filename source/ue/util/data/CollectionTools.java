package ue.util.data;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.IntStream;

/**
 * Collection helper routines.
 */
public class CollectionTools {

  /**
   * Searches a list and returns the found index or -1.
   *
   * @param list        the list to search.
   * @param predicate   custom predicate to check for a searched value.
   * @param <T>         The type of object and list.
   * @return            the found index or -1
   */
  public static <T> int findIndex(List<T> list, Function<T, Boolean> predicate) {
    return IntStream.range(0, list.size())
      .filter(i -> predicate.apply(list.get(i)))
      .findFirst().orElse(-1);
  }

  /**
   * Returns the index at which the object is located in the list or -1.
   *
   * @param list                 the list to search.
   * @param objectToFind         the object to search for.
   * @param customEqualsFunction the custom equals method to use when comparing objects.
   * @param <T>                  The type of object and list.
   * @return the index of the object in the list or -1.
   */
  public static <T> int getIndex(List<T> list, T objectToFind,
      BiFunction<T, T, Boolean> customEqualsFunction) {
    return IntStream.range(0, list.size())
      .filter(i -> customEqualsFunction.apply(list.get(i), objectToFind))
      .findFirst().orElse(-1);
  }

  public static <T> T elementAt(Collection<T> list, int index) {
    return list.stream().skip(index).findFirst().get();
  }

  /**
   * Converts a collection of Short values to a primitive short array.
   */
  public static short[] toShortArray(Collection<Short> data) {
    short[] res = new short[data.size()];

    int i = 0;
    for (short val : data)
      res[i++] = val;

    return res;
  }

  /**
   * Converts a collection of Integer values to a primitive short array.
   */
  public static short[] intToShortArray(Collection<Integer> data) {
    short[] res = new short[data.size()];

    int i = 0;
    for (int val : data)
      res[i++] = (short)val;

    return res;
  }

  public static Set<Integer> toSet(int[] vals) {
    HashSet<Integer> res = new HashSet<Integer>();
    for (int val : vals)
      res.add(val);
    return res;
  }

  public static Set<Short> toSet(short[] vals) {
    HashSet<Short> res = new HashSet<Short>();
    for (short val : vals)
      res.add(val);
    return res;
  }

  public static List<Integer> toLinkedList(int[] vals) {
    LinkedList<Integer> res = new LinkedList<Integer>();
    for (int val : vals)
      res.add(val);
    return res;
  }

  public static List<Short> toLinkedList(short[] vals) {
    LinkedList<Short> res = new LinkedList<Short>();
    for (short val : vals)
      res.add(val);
    return res;
  }

  public static <T> BinaryOperator<T> throwingMerger() {
    return (u,v) -> { throw new IllegalStateException(String.format("Duplicate key %s", u)); };
}
}