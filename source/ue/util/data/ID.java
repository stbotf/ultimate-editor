package ue.util.data;

/**
 * I can't believe I actually need this class...
 */
public class ID implements Comparable<ID> {

  public final int ID;
  public String NAME;

  public ID(String name, int id) {
    ID = id;
    NAME = name;
  }

  @Override
  public String toString() {
    return NAME;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof ID))
      return false;

    ID a = (ID) o;
    return a.ID == ID;
  }

  @Override
  public int hashCode() {
    return ID;
  }

  @Override
  public int compareTo(ID o) {
    return hashCode() - o.hashCode();
  }
}
