package ue.util.data;

/**
 * A pair of objects stuck together.
 */
public class Pair<A, B> {

  public A LEFT;
  public B RIGHT;

  /**
   * Constructor, takes two objects and saves them as a pair
   */
  public Pair(A left, B right) {
    LEFT = left;
    RIGHT = right;
  }

  @Override
  public boolean equals(Object other) {
    if (this == other)
      return true;
    if (other == null || getClass() != other.getClass())
      return false;

    Pair<?, ?> o = (Pair<?, ?>) other;
    return (o.LEFT.equals(LEFT) && o.RIGHT.equals(RIGHT));
  }

  @Override
  public int hashCode() {
    return LEFT.hashCode() + RIGHT.hashCode();
  }

  @Override
  public String toString() {
    return LEFT + ":" + RIGHT;
  }
}
