package ue.util.data;

import java.awt.FontMetrics;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Data;
import lombok.val;

/**
 * String helpers.
 */
public interface StringTools {
  public static final String DOTS = "..."; //$NON-NLS-1$

  public static String valueOf(Object o) {
    return o == null ? null : o.toString();
  }

  public static boolean equals(String a, String b) {
    return a == null ? b == null : a.equals(b);
  }

  public static int compare(String a, String b) {
    return a == null ? b == null ? 0 : -1 : a.compareTo(b);
  }

  public static boolean isNullOrEmpty(String prev) {
    return prev == null || prev.isEmpty();
  }

  // Filter irrelevant control characters like 'SOH', which is signaled
  // when the control key is pressed in addition to a valid text letter.
  //
  // Filtered characters are:
  // 0:NUL('\0'),    1:SOH,  2:STX,  3:ETX,  4:EOT,  5:ENQ,  6:ACK,  7:BEL('\a')
  // 14:SO,  15:SI, 16:DL,  17:DC1, 18:DC2, 19:DC3, 20:DC4, 21:NAK, 22:SYN, 23:ETB
  // 24:CAN, 25:EM, 26:SUB, 27:ESC, 28:FS,  29:GS,  30:RS,  31:US, 127:DEL
  public static boolean isTextCharacter(char ch) {
    return ch > 31 && ch != 127 || ch > 7 && ch < 14;
  }

  public static boolean isDigit(char ch) {
    return ch >= '0' && ch <= '9';
  }

  // find next digit character
  public static int nextDigit(String str, int pos) {
    for (int i = pos; i < str.length(); ++i)
      if (isDigit(str.charAt(i)))
        return i;
    return -1;
  }

  // find next non-digit character
  public static int nextLiteral(String str, int pos) {
    for (int i = pos; i < str.length(); ++i)
      if (!isDigit(str.charAt(i)))
        return i;
    return -1;
  }

  public static String before(String text, String delimeter) {
    if (text == null)
      return null;
    int pos = text.indexOf(delimeter);
    return pos >= 0 ? text.substring(0, pos) : null;
  }

  public static String beforeLast(String text, String delimeter) {
    if (text == null)
      return null;
    int pos = text.lastIndexOf(delimeter);
    return pos >= 0 ? text.substring(0, pos) : null;
  }

  public static String after(String text, String delimeter) {
    if (text == null)
      return null;
    int pos = text.lastIndexOf(delimeter);
    return pos >= 0 ? text.substring(pos + delimeter.length(), text.length()) : null;
  }

  public static String afterFirst(String text, String delimeter) {
    if (text == null)
      return null;
    int pos = text.indexOf(delimeter);
    return pos >= 0 ? text.substring(pos + delimeter.length(), text.length()) : null;
  }

  public static String joinNotEmpty(String delimeter, String prev, String added) {
    return isNullOrEmpty(prev) ? added : isNullOrEmpty(added) ? prev : prev + delimeter + added;
  }

  @SafeVarargs
  public static<T> String joinNotEmpty(String delimeter, T... values) {
    return Stream.of(values)
      .filter(o -> o != null)
      .map(x -> x.toString())
      .filter(s -> !s.isEmpty())
      .collect(Collectors.joining(delimeter));
  }

  public static String addLine(String text, String toAppend) {
    return addLine(text, toAppend, "\n"); //$NON-NLS-1$
  }

  public static String addLine(String text, String toAppend, String delimeter) {
    if (!text.isEmpty())
      text += delimeter;
    return text + toAppend;
  }

  public static String padLeft(String val, int len) {
    if (val.length() >= len)
      return val;

    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < len - val.length(); ++i)
      sb.append("&nbsp;");
    sb.append(val);
    return sb.toString();
  }

  // inserts filling after every x characters specified by the gap
  public static String insertEvery(String text, int gap, String filling) {
    String regex = "(.{" + Integer.toString(gap) + "})";
    return text.replaceAll(regex, "$1" + filling);
  }

  // replaces one single character at every x characters specified by the gap
  public static String replaceEvery(String text, int gap, String replacement) {
    String regex = "(.{" + Integer.toString(gap - 1) + "}).";
    return text.replaceAll(regex, "$1" + replacement);
  }

  public static String escapeLineBreaks(String s) {
    StringBuffer buffer = new StringBuffer();

    int len = s.length();
    for (int i = 0; i < len; i++) {
      char c = s.charAt(i);
      switch (c) {
        case '\\':
          buffer.append("\\\\");
          break;
        case '\n':
          buffer.append("\\n");
          break;
        case '\0':
          buffer.append("\\0");
          break;
        default:
          buffer.append(c);
      }
    }

    return buffer.toString();
  }

  public static String unescapeLineBreaks(String s) {
    StringBuffer buffer = new StringBuffer();

    int len = s.length();
    for (int i = 0; i < len; i++) {
      char c = s.charAt(i);

      if (c == '\\' && i + 1 < len) {
        char nextChar = s.charAt(i + 1);
        switch (nextChar) {
          case '\\':
            buffer.append('\\');
            break;
          case 'n':
            buffer.append('\n');
            break;
          case '0':
            buffer.append('\0');
            break;
          default:
            buffer.append('\\');
            buffer.append(nextChar);
        }
        ++i;
      }
      else {
        buffer.append(c);
      }
    }

    return buffer.toString();
  }

  public static String toUpperFirstChar(String text) {
    if (text == null || text.length() < 1)
      return text;
    return Character.toUpperCase(text.charAt(0)) + text.substring(1);
  }

  public static String wrapAtWidth(String text, FontMetrics metrics, int maxWidth) {
    if (text == null || text.length() <= 1)
      return text;

    String[] lines = text.split("\n");
    StringBuilder sb = new StringBuilder();

    for (int l = 0; l < lines.length; ++ l) {
      String line = lines[l];
      if (l > 0)
        sb.append("\n"); //$NON-NLS-1$

      // add full line if max width permits
      int lineWidth = metrics.stringWidth(line);
      if (lineWidth <= maxWidth) {
        sb.append(line);
        continue;
      }

      for (int i = 0, wrap; i < line.length(); i = wrap) {
        if (i > 0)
          sb.append("\n"); //$NON-NLS-1$

        // skip whitespace
        i = nextNonSpace(line, i);
        if (i < 0)
          break;

        // break line if width is exceeded or end reached
        wrap = nextLineWrap(line, metrics, i, maxWidth);
        sb.append(line.substring(i, wrap));
      }
    }

    return sb.toString();
  }

  public static String shortenPath(String str, FontMetrics metrics, int max) {
    if (metrics.stringWidth(str) <= max)
      return str;

    max -= metrics.stringWidth(DOTS);
    if (str.startsWith(DOTS)) //$NON-NLS-1$
      str = str.substring(4);

    while (metrics.stringWidth(str) > max) {
      int j = str.indexOf("/"); //$NON-NLS-1$
      if (j < 0)
        j = str.indexOf("\\"); //$NON-NLS-1$

      int start = j <= 1 ? 1 : j;
      str = str.substring(start); //$NON-NLS-1$
    }

    return DOTS + str;
  }

  public static int nextNonSpace(String line, int start) {
    for (int i = start; i < line.length(); ++i) {
      char c = line.charAt(i);
      if (!Character.isWhitespace(c))
        return i;
    }
    return -1;
  }

  // @returns the next line wrap index
  public static int nextLineWrap(String line, FontMetrics metrics, int start, int maxWidth) {
    int width = 0;
    int lastSpaceIdx = -1;
    int lineLength = line.length();

    for (int i = start; i < lineLength; ++i) {
      char c = line.charAt(i);

      // update last space to wrap
      if (Character.isWhitespace(c) && i > start)
        lastSpaceIdx = i;

      // return index if width is exceeded
      width += metrics.charWidth(c);
      if (width > maxWidth)
        return lastSpaceIdx > 0 ? lastSpaceIdx : i;
    }

    return lineLength;
  }

  @Data
  public class MarkedResult {
    public final String txt1;
    public final String txt2;
    public final int cnt;
  }

  public static MarkedResult markDiff(String txt1, String txt2, String startMark, String endMark) {
    if (txt1 == null && txt2 == null)
      return new MarkedResult(null, null, 0);
    else if (txt1 == null)
      return new MarkedResult(null, startMark + txt2 + endMark, 1);
    else if (txt2 == null)
      return new MarkedResult(startMark + txt1 + endMark, null, 1);

    val sb1 = new StringBuffer();
    val sb2 = new StringBuffer();
    int len = Integer.min(txt1.length(), txt2.length());
    boolean changeDetected = false;
    int changeCnt = 0;

    for (int i = 0; i < len; ++i) {
      char c1 = txt1.charAt(i);
      char c2 = txt2.charAt(i);

      if (changeDetected) {
        if (c1 == c2) {
          sb1.append(endMark);
          sb2.append(endMark);
          changeDetected = false;
        }
      }
      else if (c1 != c2) {
        sb1.append(startMark);
        sb2.append(startMark);
        changeDetected = true;
        changeCnt++;
      }

      sb1.append(c1);
      sb2.append(c2);
    }

    if (len < txt1.length())
      sb1.append(txt1, len, txt1.length());
    else if (len < txt2.length())
      sb2.append(txt2, len, txt2.length());

    if (changeDetected) {
      sb1.append(endMark);
      sb2.append(endMark);
    }

    return new MarkedResult(sb1.toString(), sb2.toString(), changeCnt);
  }
}
