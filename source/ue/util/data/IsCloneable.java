package ue.util.data;

/**
 * I can't believe a Cloneable must be derived to allow access the clone operation!
 */
public interface IsCloneable extends Cloneable {

  Object clone();

}
