package ue.util.data;

import ue.service.Language;

/**
 * Value cast helper routines.
 */
public class ValueCast {

  public static final int MAX_UBYTE = (Byte.MAX_VALUE << 1) + 1;
  public static final int MAX_USHORT = (Short.MAX_VALUE << 1) + 1;

  public static void checkByte(int value) {
    if (value < Byte.MIN_VALUE || value > Byte.MAX_VALUE) {
      String msg = Language.getString("ValueCast.0")
        .replace("%1", Integer.toString(value));
      throw new IllegalArgumentException(msg);
    }
  }

  public static void checkUByte(int value) {
    if (value < 0 || value > MAX_UBYTE) {
      String msg = Language.getString("ValueCast.1")
        .replace("%1", Integer.toString(value));
      throw new IllegalArgumentException(msg);
    }
  }

  public static void checkShort(int value) {
    if (value < Short.MIN_VALUE || value > Short.MAX_VALUE) {
      String msg = Language.getString("ValueCast.2")
        .replace("%1", Integer.toString(value));
      throw new IllegalArgumentException(msg);
    }
  }

  public static void checkUShort(int value) {
    if (value < 0 || value > MAX_USHORT) {
      String msg = Language.getString("ValueCast.3")
        .replace("%1", Integer.toString(value));
      throw new IllegalArgumentException(msg);
    }
  }

  public static int cutUShort(int value) {
    return value < 0 ? 0 : value > MAX_USHORT ? MAX_USHORT : value;
  }

  /**
   * Casts byte array to an int array.
   */
  public static int[] castUIntArray(byte[] values) {
    int[] res = new int[values.length];
    for (int i = 0; i < values.length; ++i)
      res[i] = Byte.toUnsignedInt(values[i]);
    return res;
  }

  /**
   * Casts short array to an int array.
   */
  public static int[] castIntArray(short[] values) {
    int[] res = new int[values.length];
    for (int i = 0; i < values.length; ++i)
      res[i] = values[i];
    return res;
  }

  /**
   * Casts a 2D short array to a 2D int array.
   */
  public static int[][] castIntArray(short[][] values) {
    int[][] res = new int[values.length][];
    for (int i = 0; i < values.length; ++i)
      res[i] = castIntArray(values[i]);
    return res;
  }

  /**
   * Casts short array to an unsigned int array.
   */
  public static int[] castUIntArray(short[] values) {
    int[] res = new int[values.length];
    for (int i = 0; i < values.length; ++i)
      res[i] = Short.toUnsignedInt(values[i]);
    return res;
  }

  /**
   * Casts a 2D short array to a 2D unsigned int array.
   */
  public static int[][] castUIntArray(short[][] values) {
    int[][] res = new int[values.length][];
    for (int i = 0; i < values.length; ++i)
      res[i] = castUIntArray(values[i]);
    return res;
  }

  /**
   * Casts byte array to a short array.
   */
  public static short[] castUShortArray(byte[] values) {
    short[] res = new short[values.length];
    for (int i = 0; i < values.length; ++i)
      res[i] = (short)Byte.toUnsignedInt(values[i]);
    return res;
  }

  /**
   * Casts int array to a short array.
   */
  public static short[] castShortArray(int[] values) {
    short[] res = new short[values.length];
    for (int i = 0; i < values.length; ++i)
      res[i] = (short)values[i];
    return res;
  }

  /**
   * Casts a 2D int array to a 2D short array.
   */
  public static short[][] castShortArray(int[][] values) {
    short[][] res = new short[values.length][];
    for (int i = 0; i < values.length; ++i)
      res[i] = castShortArray(values[i]);
    return res;
  }

  /**
   * Cast int array to a byte array.
   */
  public static byte[] castByteArray(int[] values) {
    byte[] res = new byte[values.length];
    for (int i = 0; i < values.length; ++i)
      res[i] = (byte)values[i];
    return res;
  }

  /**
   * Cast short array to a byte array.
   */
  public static byte[] castByteArray(short[] values) {
    byte[] res = new byte[values.length];
    for (int i = 0; i < values.length; ++i)
      res[i] = (byte)values[i];
    return res;
  }
}
