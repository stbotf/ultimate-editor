package ue.util.data;

import java.util.ArrayList;
import com.google.common.collect.Range;
import com.google.common.io.BaseEncoding;
import lombok.Data;

/**
 * Hex conversion tools.
 */
public class HexTools {

  public static String toHex(byte[] data) {
    return BaseEncoding.base16().encode(data);
  }

  public static String toHex(byte[] data, String spacing) {
    return toHex(data, -1, spacing, null);
  }

  public static String toHex(byte[] data, int bytesPerLine) {
    return toHex(data, bytesPerLine, " ");
  }

  public static String toHex(byte[] data, int bytesPerLine, String spacing) {
    return toHex(data, bytesPerLine, spacing, "\n");
  }

  public static String toHex(byte[] data, int bytesPerLine, String spacing, String lineBreak) {
    if (data == null)
      return null;
    if (data.length == 0)
      return "";

    // calculate hex length for buffer allocation
    int hexLen = calcHexLen(data, bytesPerLine, spacing, lineBreak);
    StringBuffer hex = new StringBuffer(hexLen);
    int nextBreak = bytesPerLine;

    // lookup values in HexMap for faster byte conversion
    int idx = Byte.toUnsignedInt(data[0]);
    hex.append(HexMap[idx]);

    for (int i = 1; i < data.length; ++i) {
      if (i == nextBreak) {
        hex.append(lineBreak);
        nextBreak += bytesPerLine;
      } else {
        hex.append(spacing);
      }

      idx = Byte.toUnsignedInt(data[i]);
      hex.append(HexMap[idx]);
    }

    return hex.toString();
  }

  public static String toHex(byte val) {
    byte[] data = new byte[] {val};
    return BaseEncoding.base16().encode(data);
  }

  public static String toHex(short val) {
    byte[] data = DataTools.toByte(val, false);
    return BaseEncoding.base16().encode(data);
  }

  public static String toHex(int val) {
    byte[] data = DataTools.toByte(val, false);
    return BaseEncoding.base16().encode(data);
  }

  public static String toHex(long val) {
    byte[] data = DataTools.toByte(val, false);
    return BaseEncoding.base16().encode(data);
  }

  public static String toHex(float val) {
    byte[] data = DataTools.toByte(val, false);
    return BaseEncoding.base16().encode(data);
  }

  public static String toHex(double val) {
    byte[] data = DataTools.toByte(val, false);
    return BaseEncoding.base16().encode(data);
  }

  public static String toHexValue(byte value)   { return "0x" + toHex(value); }
  public static String toHexValue(short value)  { return "0x" + toHex(value); }
  public static String toHexValue(int value)    { return "0x" + toHex(value); }
  public static String toHexValue(long value)   { return "0x" + toHex(value); }
  public static String toHexValue(float value)  { return "0x" + toHex(value); }
  public static String toHexValue(double value) { return "0x" + toHex(value); }

  public static String undecorate(String value) {
    if (value.startsWith("0x"))
      value = value.substring(2);
    else if (value.endsWith("h"))
      value = value.substring(0, value.length()-1);
    return value;
  }

  public static byte toByte(String value) {
    value = undecorate(value);
    return Byte.parseByte(value, 16);
  }

  public static byte[] toByteArray(String value) {
    return BaseEncoding.base16().decode(value);
  }

  public static short toShort(String value) {
    value = undecorate(value);
    return (short) Integer.parseUnsignedInt(value, 16);
  }

  public static int toInt(String value) {
    value = undecorate(value);
    return Integer.parseUnsignedInt(value, 16);
  }

  public static long toLong(String value) {
    value = undecorate(value);
    return Long.parseUnsignedLong(value, 16);
  }

  /**
   * Calculate byte array hex string length.
   * Each byte is represented by two characters and separated by the spacing, e.g. "0A 42 F7 B3".
   */
  public static int calcHexLen(byte[] data, int bytesPerLine, String spacing, String lineBreak) {
    // only count intermediate spacings
    int singleLineSpacings = data.length > 1 ? (data.length - 1) * (2 + spacing.length()) : 0;
    // on line breaks, substract last spacing
    int lineBreakSpacings = (data.length/bytesPerLine) * (lineBreak.length() - spacing.length());
    return singleLineSpacings + lineBreakSpacings;
  }

  @Data
  public static class DiffResult {
    private String orig;
    private String changed;
    private ArrayList<Range<Integer>> markings = new ArrayList<>();
  }

  /**
   * Optimized diff routine that processes both byte arrays at the same time.
   */
  public static DiffResult diff(byte[] orig, byte[] changed, int bytesPerLine, String spacing,
    String lineBreak, String startMark, String endMark) {
    DiffResult result = new DiffResult();

    if (orig != null && changed != null) {
      // calculate hex length for when there is no diff
      int origHexLen = calcHexLen(orig, bytesPerLine, spacing, lineBreak);
      int changedHexLen = calcHexLen(changed, bytesPerLine, spacing, lineBreak);

      // allocate buffers with some extra space for the diff markings
      StringBuffer hexOrig = new StringBuffer(origHexLen + origHexLen >> 1);
      StringBuffer hexChanged = new StringBuffer(changedHexLen + changedHexLen >> 1);
      int len = Integer.max(orig.length, changed.length);
      int nextBreak = bytesPerLine;

      // read first byte
      int origByte = orig.length > 0 ? Byte.toUnsignedInt(orig[0]) : -1;
      int changedByte = changed.length > 0 ? Byte.toUnsignedInt(changed[0]) : -1;
      boolean changeDetected = origByte != changedByte;
      boolean origMarked = changeDetected;
      boolean changedMarked = changeDetected;
      int changeStart = changeDetected ? 0 : -1;

      // mark change start
      if (changeDetected) {
        if (origByte >= 0)
          hexOrig.append(startMark);
        if (changedByte >= 0)
          hexChanged.append(startMark);
      }

      // lookup values in HexMap for faster byte conversion
      if (origByte >= 0)
        hexOrig.append(HexMap[origByte]);
      if (changedByte >= 0)
        hexChanged.append(HexMap[changedByte]);

      for (int i = 1; i < len; ++i) {
        origByte = orig.length > i ? Byte.toUnsignedInt(orig[i]) : -1;
        changedByte = changed.length > i ? Byte.toUnsignedInt(changed[i]) : -1;
        boolean isChanged = origByte != changedByte;
        boolean markChange = isChanged != changeDetected;
        String space = i == nextBreak ? lineBreak : spacing;

        if (markChange) {
          int pos = origByte >= 0 ? hexOrig.length() : hexChanged.length();
          if (changeStart != -1) {
            result.markings.add(Range.closedOpen(changeStart, pos));
            changeStart = -1;
          } else {
            // add one for the spacing, which is appended below
            changeStart = pos + 1;
          }
        }

        if (origByte >= 0) {
          // unmark first to end background coloring
          if (markChange && !isChanged) {
            hexOrig.append(endMark);
            origMarked = false;
          }

          // add spacings and line breaks
          hexOrig.append(space);

          // start mark detected changes
          if (markChange && isChanged) {
            hexOrig.append(startMark);
            origMarked = true;
          }

          // convert byte to hex value
          hexOrig.append(HexMap[origByte]);
        }

        if (changedByte >= 0) {
          // unmark first to end background coloring
          if (markChange && !isChanged) {
            hexChanged.append(endMark);
            changedMarked = false;
          }

          // add spacings and line breaks
          hexChanged.append(space);

          // start mark detected changes
          if (markChange && isChanged) {
            hexChanged.append(startMark);
            changedMarked = true;
          }

          // convert byte to hex value
          hexChanged.append(HexMap[changedByte]);
        }

        // update change detection
        changeDetected = isChanged;
        if (i == nextBreak)
          nextBreak += bytesPerLine;
      }

      // unmark end changes, which might or might not differ on size mismatch
      if (origMarked)
        hexOrig.append(endMark);
      if (changedMarked)
        hexChanged.append(endMark);

      if (changeStart != -1) {
        int end = Integer.max(hexOrig.length(), hexChanged.length());
        result.markings.add(Range.closedOpen(changeStart, end));
      }

      result.orig = hexOrig.toString();
      result.changed = hexChanged.toString();
    } else {
      result.orig = HexTools.toHex(orig, bytesPerLine, spacing, lineBreak);
      result.changed = HexTools.toHex(changed, bytesPerLine, spacing, lineBreak);
    }

    return result;
  }

  private static final String[] HexMap = new String[] {
    "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0A", "0B", "0C", "0D", "0E", "0F",
    "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1A", "1B", "1C", "1D", "1E", "1F",
    "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2A", "2B", "2C", "2D", "2E", "2F",
    "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3A", "3B", "3C", "3D", "3E", "3F",
    "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4A", "4B", "4C", "4D", "4E", "4F",
    "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5A", "5B", "5C", "5D", "5E", "5F",
    "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6A", "6B", "6C", "6D", "6E", "6F",
    "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7A", "7B", "7C", "7D", "7E", "7F",
    "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8A", "8B", "8C", "8D", "8E", "8F",
    "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9A", "9B", "9C", "9D", "9E", "9F",
    "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "AA", "AB", "AC", "AD", "AE", "AF",
    "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "BA", "BB", "BC", "BD", "BE", "BF",
    "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "CA", "CB", "CC", "CD", "CE", "CF",
    "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "DA", "DB", "DC", "DD", "DE", "DF",
    "E0", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "EA", "EB", "EC", "ED", "EE", "EF",
    "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "FA", "FB", "FC", "FD", "FE", "FF"
  };
}
