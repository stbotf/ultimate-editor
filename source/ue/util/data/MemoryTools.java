package ue.util.data;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Memory management tools.
 */
public class MemoryTools {

  public static ByteBuffer reserve(ByteBuffer buffer, int capacity) {
    if (buffer.capacity() < capacity) {
      byte[] data = Arrays.copyOf(buffer.array(), capacity);
      ByteBuffer copy = ByteBuffer.wrap(data);
      // restore read/write position
      copy.position(buffer.position());
      return copy;
    }
    return buffer;
  }
}