package ue.util.data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import lombok.val;

/**
 * Converts different types of data.
 */
public class DataTools {

  public static int ceilDiv(int divident, int divisor) {
    return (divident + divisor - 1) / divisor;
  }

  /**
   * Converts an int value into a byte array.
   */
  public static byte[] toByte(int value, boolean littleEndian) {
    return ByteBuffer.allocate(4)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .putInt(value).array();
  }

  /**
   * Converts a short value into a byte array.
   */
  public static byte[] toByte(short value, boolean littleEndian) {
    return ByteBuffer.allocate(2)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .putShort(value).array();
  }

  /**
   * Converts a long value into a byte array.
   */
  public static byte[] toByte(long value, boolean littleEndian) {
    return ByteBuffer.allocate(8)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .putLong(value).array();
  }

  /**
   * Converts a float value into a byte array.
   */
  public static byte[] toByte(float value, boolean littleEndian) {
    return ByteBuffer.allocate(4)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .putFloat(value).array();
  }

  /**
   * Converts a double value into a byte array.
   */
  public static byte[] toByte(double value, boolean littleEndian) {
    return ByteBuffer.allocate(8)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .putDouble(value).array();
  }


  /**
   * Converts a string into a byte array containing ISO-8859-1 characters.
   *
   * @param value           the string to convert
   * @param arraySize       the size of the returning array
   * @param maxStringLength the max size of the string
   */
  public static byte[] toByte(String value, int arraySize, int maxStringLength) {
    val result = new byte[arraySize];
    if (value != null) {
      val stringBytes = value.getBytes(StandardCharsets.ISO_8859_1);
      val stringLength = Math.min(Math.min(stringBytes.length, result.length), maxStringLength);
      System.arraycopy(stringBytes, 0, result, 0, stringLength);
    }
    return result;
  }

  /**
   * Converts a byte array into an int value.
   */
  public static int toInt(byte[] data, boolean littleEndian) {
    return toInt(data, 0, littleEndian);
  }

  public static int toInt(byte[] data, int offset, boolean littleEndian) {
    return ByteBuffer.wrap(data, offset, 4)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .getInt();
  }

  /**
   * Converts a byte array into a short value.
   */
  public static short toShort(byte[] data, boolean littleEndian) {
    return toShort(data, 0, littleEndian);
  }

  public static short toShort(byte[] data, int offset, boolean littleEndian) {
    return ByteBuffer.wrap(data, offset, 2)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .getShort();
  }

  /**
   * Converts a byte array into a long value.
   */
  public static long toLong(byte[] data, boolean littleEndian) {
    return toLong(data, 0, littleEndian);
  }

  public static long toLong(byte[] data, int offset, boolean littleEndian) {
    return ByteBuffer.wrap(data, offset, 8)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .getLong();
  }

  /**
   * Converts a byte array into a float value.
   */
  public static float toFloat(byte[] data, boolean littleEndian) {
    return toFloat(data, 0, littleEndian);
  }

  public static float toFloat(byte[] data, int offset, boolean littleEndian) {
    return ByteBuffer.wrap(data, offset, 4)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .getFloat();
  }

  /**
   * Converts a byte array into a double value.
   */
  public static double toDouble(byte[] data, boolean littleEndian) {
    return toDouble(data, 0, littleEndian);
  }

  public static double toDouble(byte[] data, int offset, boolean littleEndian) {
    return ByteBuffer.wrap(data, offset, 8)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN)
      .getDouble();
  }

  /**
   * Converts a byte array into an int array.
   */
  public static int[] toIntArray(byte[] data, boolean littleEndian) {
    return toIntArray(data, 0, data.length, littleEndian);
  }

  public static int[] toIntArray(byte[] data, int offset, int length, boolean littleEndian) {
    if (offset < 0 || length < 0 || (length > data.length - offset) || length % 4 > 0)
      throw new IllegalArgumentException("Invalid data size.");

    int[] res = new int[length / 4];
    ByteBuffer buffer = ByteBuffer.wrap(data, offset, length)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);

    for (int i = 0; i < res.length; ++i)
      res[i] = buffer.getInt();

    return res;
  }

  /**
   * Converts a byte array into a short array.
   */
  public static short[] toShortArray(byte[] data, boolean littleEndian) {
    return toShortArray(data, 0, data.length, littleEndian);
  }

  public static short[] toShortArray(byte[] data, int offset, int length, boolean littleEndian) {
    if (offset < 0 || length < 0 || (length > data.length - offset) || length % 2 > 0)
      throw new IllegalArgumentException("Invalid data size.");

    short[] res = new short[length / 2];
    ByteBuffer buffer = ByteBuffer.wrap(data, offset, length)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);

    for (int i = 0; i < res.length; ++i)
      res[i] = buffer.getShort();

    return res;
  }

  /**
   * Converts an int array into a byte array.
   */
  public static byte[] toByte(int[] values, boolean littleEndian) {
    ByteBuffer buffer = ByteBuffer.allocate(values.length * 4)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);

    for (int value : values)
      buffer.putInt(value);

    return buffer.array();
  }

  /**
   * Converts a short array into a byte array.
   */
  public static byte[] toByte(short[] values, boolean littleEndian) {
    ByteBuffer buffer = ByteBuffer.allocate(values.length * 2)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);

    for (short value : values)
      buffer.putShort(value);

    return buffer.array();
  }

  /**
   * Converts a long array into a byte array.
   */
  public static byte[] toByte(long[] values, boolean littleEndian) {
    ByteBuffer buffer = ByteBuffer.allocate(values.length * 8)
      .order(littleEndian ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);

    for (long value : values)
      buffer.putLong(value);

    return buffer.array();
  }

  public static void setByteValue(byte[] data, int index, long value, int bytes, boolean littleEndian) {
    if (littleEndian) {
      for (int i = 0; i < bytes; ++i)
      data[index+i] = (byte) ((value >>> (i*8)) & 0xFF);
    } else {
      index += bytes - 1;
      for (int i = 0; i < bytes; ++i)
        data[index-i] = (byte) ((value >>> (i*8)) & 0xFF);
    }
  }

  public static void copyRange(byte[] dest, byte[] source, int index, int start, int length) {
    if (start + length > source.length || index + length > dest.length)
      throw new IndexOutOfBoundsException();

    for (int i = 0; i < length; ++i) {
      dest[index+i] = source[start+i];
    }
  }

  /**
   * Converts a byte array into a string using the ISO-8859-1 charset.
   *
   * @param data the byte array to convert
   */
  public static String fromNullTerminatedBotfString(byte[] data) {
    // limit on termination character, required for concatenation
    return StringUtils.substringBefore(new String(data, StandardCharsets.ISO_8859_1), 0);
  }

  /**
   * Converts a byte array into a string using the ISO-8859-1 charset.
   *
   * @param data      the byte array
   * @param start     the starting index
   * @param maxLength the maximum string length
   */
  public static String fromNullTerminatedBotfString(byte[] data, int start, int maxLength) {
    // limit on '\0' character, used for *.tdb text database entry concatenation
    int len = lengthOf(data, (byte) 0, start, maxLength);
    return new String(data, start, len, StandardCharsets.ISO_8859_1);
  }

  public static int indexOf(byte[] data, byte value) {
    return indexOf(data, value, 0, data.length);
  }

  public static int indexOf(byte[] data, byte value, int start, int end) {
    int i = start;
    for (; i < end; ++i) {
      if (data[i] == value)
        return i;
    }
    return -1;
  }

  public static int lengthOf(byte[] data, byte termination) {
    return lengthOf(data, termination, 0, data.length);
  }

  public static int lengthOf(byte[] data, byte termination, int start, int maxLength) {
    int maxDataLen = Integer.min(maxLength, data.length - start);
    if (maxDataLen <= 0)
      return 0;

    int end = indexOf(data, termination, start, start + maxDataLen);
    return end >= 0 ? end - start : maxDataLen;
  }

  public static boolean equals(byte[] read, byte[] expected) {
    return Arrays.equals(read, expected);
  }

  public static boolean equals(byte[] read, int expected) {
    return equals(read, 0, read.length, expected);
  }

  public static boolean equals(byte[] read, int offset, int length, byte[] expected) {
    return equals(read, offset, length, expected, 0);
  }

  public static boolean equals(byte[] read, int rd_offset, int length, byte[] expected, int exp_offset) {
    if (length + exp_offset > expected.length)
      throw new IndexOutOfBoundsException();
    if (rd_offset + length > read.length)
      return false;

    // to be replaced when upgrading to java 9 or above
    for (int i = 0; i < length; ++i)
      if (read[i+rd_offset] != expected[i+exp_offset])
        return false;

    return true;
  }

  public static boolean equals(byte[] read, int offset, byte[] expected) {
    if (offset + expected.length > read.length)
      return false;

    for (int i = 0; i < expected.length; ++i)
      if (read[offset+i] != expected[i])
        return false;

    return true;
  }

  public static boolean equals(byte[] read, int offset, int length, int expected) {
    if (offset + length > read.length)
      return false;

    for (int i = 0; i < length; ++i)
      if (read[offset+i] != expected)
        return false;

    return true;
  }

  public static Optional<Integer> tryParseInt(String value) {
    try {
      return Optional.of(Integer.parseInt(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  public static Optional<Integer> tryParseUInt(String value) {
    try {
      return Optional.of(Integer.parseUnsignedInt(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  public static Optional<Short> tryParseShort(String value) {
    try {
      return Optional.of(Short.parseShort(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  public static Optional<Long> tryParseLong(String value) {
    try {
      return Optional.of(Long.parseLong(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  public static Optional<Long> tryParseULong(String value) {
    try {
      return Optional.of(Long.parseUnsignedLong(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  public static Optional<Integer> tryParseByte(String value) {
    try {
      Integer x = Integer.parseUnsignedInt(value);
      return Optional.of(x < 0 ? 0 : x > 255 ? 255 : x);
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  public static Optional<Byte> tryParseSByte(String value) {
    try {
      return Optional.of(Byte.parseByte(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  public static Optional<Double> tryParseDouble(String value) {
    try {
      return Optional.of(Double.parseDouble(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  // for full precision, parse double and cast the value instead
  // all excessive digits are skipped and not rounded
  public static Optional<Float> tryParseFloat(String value) {
    try {
      return Optional.of(Float.parseFloat(value));
    } catch (NumberFormatException e) {
      return Optional.empty();
    }
  }

  /**
   *  Extract available data at the given offset up to the specified length.
   *
   *  If offset is out of range an empty byte array is returned.
   *  If offset + length exceeds the data length it is truncated.
   */
  public static byte[] chunkOfRange(byte[] data, int offset, int length) {
    // skip copy when full data is matched
    if (offset == 0 && data.length <= length)
      return data;
    // check bounds
    if (offset >= data.length)
      return new byte[0];

    // copy but truncate if needed
    int len = Integer.min(data.length - offset, length);
    return Arrays.copyOfRange(data, offset, offset + len);
  }

}
