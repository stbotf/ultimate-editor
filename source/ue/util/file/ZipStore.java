package ue.util.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import lombok.val;
import ue.util.func.FunctionTools.CheckedBiPredicate;

public class ZipStore implements FileStore {

  private ZipFile file;

  public ZipStore(File file) throws IOException {
    this.file = new ZipFile(file);
  }

  public ZipStore(String filePath) throws IOException {
    this(new File(filePath));
  }

  public int numFiles() {
    return file.size();
  }

  public boolean contains(String fileName) {
    return file.getEntry(fileName) != null;
  }

  public Optional<ArchiveEntry> findEntry(String fileName) {
    // iterate all entries for case insensitive search
    return file.stream()
      .filter(x -> x.getName().equalsIgnoreCase(fileName))
      .map(x -> (ArchiveEntry) new ZipFileEntry(x))
      .findFirst();
  }

  public List<String> listFileNames() {
    return file.stream().map(ZipEntry::getName).collect(Collectors.toList());
  }

  public List<ZipFileEntry> listFileEntries() {
    return file.stream().map(ZipFileEntry::new).collect(Collectors.toList());
  }

  public boolean processFiles(CheckedBiPredicate<FileStore, ArchiveEntry, IOException> callback) throws IOException {
    val entries = file.entries();
    while (entries.hasMoreElements()) {
      val entry = entries.nextElement();
      if (!callback.test(this, new ZipFileEntry(entry)))
        return false;
    }
    return true;
  }

  public Stream<ZipFileEntry> stream() {
    return file.stream().map(ZipFileEntry::new);
  }

  public InputStream getInputStream(ArchiveEntry entry, boolean chunked) throws IOException {
    if (!(entry instanceof ZipFileEntry))
      throw new IllegalArgumentException("no ZipFileEntry");

    ZipFileEntry zipEntry = (ZipFileEntry)entry;
    return file.getInputStream(zipEntry.getZipEntry());
  }

  @Override
  public void close() throws IOException {
    if (file != null) {
      file.close();
      file = null;
    }
  }

}
