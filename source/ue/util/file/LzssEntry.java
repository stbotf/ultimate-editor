package ue.util.file;

import java.io.IOException;
import java.io.OutputStream;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import ue.util.lzss.LzssCompression;
import ue.util.lzss.LzssOutputStream;
import ue.util.stream.SizedOutputStream;
import ue.util.stream.chunk.ChunkWriter;

/**
 * This class represents an entry in a saved game.
 * It holds all data used for the map. Inspired by ZipEntry ^_^
 */
public class LzssEntry implements Comparable<LzssEntry>, ArchiveEntry {

  @Getter private String name;
  @Getter private String lcName;

  @Getter @Setter private int address = 0;          // file offset
  @Getter @Setter private int index = -1;           // entry index - handled by SavFile
  @Getter @Setter private int uncompressedSize = 0;
  @Getter @Setter private int compressedSize = 0;
  @Getter @Setter private int subEntries = 1;       // number of compressed sub-entries
  @Getter @Setter private int unknown = 0;          // possibly some computed but ignored hash value

  /**
   * @param n        the name of the entry.
   * @param isBinary
   */
  public LzssEntry(String name) {
    this.name = name;
    this.lcName = name.toLowerCase();
  }

  @Override
  public String getFileType() {
    return FileFilters.getFileType(name);
  }

  /**
   * Returns an OutputStream used to write entry data.
   */
  public OutputStream getOutputStream(OutputStream dataStream, boolean chunked, boolean binary)
      throws IOException {

    // wrap to cumulate written compressed file size
    val sized = new SizedOutputStream(dataStream);
    compressedSize = 0;
    subEntries = 1;

    if (chunked) {
      // LZSS encrypt written chunks and update size and chunk count when the stream is closed
      return new ChunkWriter(sized, new LzssCompression(binary)) {
        @Override
        public void close() throws IOException {
          // first close parent to flush all data and apply the compression
          super.close();
          compressedSize = sized.getLength();
          subEntries = numChunks();
        }
      };
    } else {
      // LZSS encrypt written data and update size when the stream is closed
      return new LzssOutputStream(sized, binary) {
        @Override
        public void close() throws IOException {
          // first close parent to flush all data and apply the compression
          super.close();
          compressedSize = sized.getLength();
        }
      };
    }
  }

  // compare on the written SavFile index
  @Override
  public int compareTo(LzssEntry se) throws ClassCastException {
    boolean hasIdx = index >= 0;
    boolean hseIdx = se.index >= 0;
    if (hasIdx != hseIdx) {
      return hasIdx ? -1 : 1;
    }
    return (index - se.index);
  }

  @Override
  public boolean equals(Object t) {
    if (t instanceof LzssEntry) {
      LzssEntry ot = (LzssEntry) t;
      if ((ot.getName()).equals(name)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 43 * hash + (this.name != null ? this.name.hashCode() : 0);
    return hash;
  }

  @Override
  public String toString() {
    return name;
  }
}
