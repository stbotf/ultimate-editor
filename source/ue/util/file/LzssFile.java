package ue.util.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Getter;
import lombok.val;
import ue.exception.InvalidArgumentsException;
import ue.exception.SavException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.func.FunctionTools.CheckedBiPredicate;
import ue.util.stream.Compression;
import ue.util.stream.SizedInputStream;
import ue.util.stream.StreamTools;
import ue.util.stream.WrappedInputStream;
import ue.util.stream.chunk.ChunkReader;
import ue.util.lzss.LzssCompression;

/**
 * This class is used to process saved games files and alt.res.
 */
public class LzssFile implements FileStore {

  private static final int HEADER_SIZE = 24;

  @Getter private String name;

  //open source, files, read from it
  //open dest file, write to it.
  //when closing, rewrite header, replace file if required
  private File SOURCE = null;
  private File DEST = null;
  private OutputStream D_OUTPUT = null; //destination, opened until file is closed
  private LzssEntry CURRENT_ENTRY = null;
  private ArrayList<LzssEntry> S_ENTRY = new ArrayList<LzssEntry>();
  private ArrayList<LzssEntry> D_ENTRY = new ArrayList<LzssEntry>();

  /**
   * Opens a *.sav file or alt.res, or creates one if the file does not exist yet.
   * If the file already exists, the name argument is not needed (can be null).
   */
  public LzssFile(File go, String savName) throws IOException {
    if (go.exists())
      load(go);
    else
      create(go, savName);
  }

  private void load(File go) throws IOException {
    // load existing save game
    SOURCE = go;

    try (FileInputStream in = new FileInputStream(SOURCE)) {
      //name 16
      name = StreamTools.readNullTerminatedBotfString(in, 16);

      //map address 4
      int mapAddr = StreamTools.readInt(in, true);

      //number of map entries 4
      int numEntries = StreamTools.readInt(in, true); //no need for making this global

      //entries
      StreamTools.skip(in, mapAddr - HEADER_SIZE);

      for (int i = 0; i < numEntries; i++) {
        //name 16
        String fileName = StreamTools.readNullTerminatedBotfString(in, 16);
        LzssEntry savEntry = new LzssEntry(fileName);

        //address 4
        int address = StreamTools.readInt(in, true);
        savEntry.setAddress(address);

        //index 4
        savEntry.setIndex(i);
        StreamTools.skip(in, 4);

        //uncompressed 4
        int uncompSize = StreamTools.readInt(in, true);
        savEntry.setUncompressedSize(uncompSize);

        //compressed 4
        int compSize = StreamTools.readInt(in, true);
        savEntry.setCompressedSize(compSize);

        //sub-entries 4
        int subCount = StreamTools.readInt(in, true);
        savEntry.setSubEntries(subCount);

        //unknown 4
        int unknown = StreamTools.readInt(in, true);
        savEntry.setUnknown(unknown);

        //add to set
        S_ENTRY.add(savEntry);
      }
    }
  }

  private void create(File go, String savName) throws IOException {
    if (savName == null)
      throw new NullPointerException(Language.getString("LzssFile.0")); //$NON-NLS-1$

    name = savName;
    int numEntries = 0;

    //create
    DEST = go;
    DEST.createNewFile();
    D_OUTPUT = new FileOutputStream(DEST);
    SOURCE = null;

    //write name
    byte[] writeBuffer = DataTools.toByte(name, 16, 15);
    D_OUTPUT.write(writeBuffer);

    //write values
    byte[] b = DataTools.toByte(HEADER_SIZE, true);
    D_OUTPUT.write(b);
    b = DataTools.toByte(numEntries, true);
    D_OUTPUT.write(b);
  }

  /**
   * Sets the internal name of the saved game.
   */
  public void setName(String name) throws InvalidArgumentsException {
    if (name.length() > 15 || name.length() < 1) {
      String msg = Language.getString("LzssFile.1"); //$NON-NLS-1$
      msg = msg.replace("%1", name); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    this.name = name;
  }

  @Override
  public int numFiles() {
    return S_ENTRY.size();
  }

  /**
   * Returns true if there's an entry with the name in the file.
   */
  @Override
  public boolean contains(String fileName) {
    if (S_ENTRY == null)
      throw new NullPointerException(getSavFileClosedString());

    String lcName = fileName.toLowerCase();
    for (LzssEntry entry : S_ENTRY) {
      String entryName = entry.getName().toLowerCase();
      if (entryName.equals(lcName))
        return true;
    }

    return false;
  }

  @Override
  public Optional<LzssEntry> findEntry(String fileName) {
    if (S_ENTRY == null)
      throw new NullPointerException(getSavFileClosedString());

    for (val entry : S_ENTRY) {
      if (entry.getName().equalsIgnoreCase(fileName))
        return Optional.of(entry);
    }

    return Optional.empty();
  }

  @Override
  public List<String> listFileNames() {
    return S_ENTRY.stream().map(ArchiveEntry::getName).collect(Collectors.toList());
  }

  @Override
  public List<LzssEntry> listFileEntries() {
    return Collections.unmodifiableList(S_ENTRY);
  }

  @Override
  public boolean processFiles(CheckedBiPredicate<FileStore, ArchiveEntry, IOException> callback) throws IOException {
    for (val entry : S_ENTRY) {
      if (!callback.test(this, entry))
        return false;
    }
    return true;
  }

  @Override
  public Stream<LzssEntry> stream() {
    return S_ENTRY.stream();
  }

  /**
   * Adds a new SavEntry to the file.
   */
  public void putNextEntry(LzssEntry se) throws SavException {
    if (CURRENT_ENTRY != null)
      throw new SavException(Language.getString("LzssFile.2")); //$NON-NLS-1$

    if (D_ENTRY == null)
      throw new SavException(getSavFileClosedString());

    if (D_ENTRY.contains(se)) {
      String msg = Language.getString("LzssFile.3"); //$NON-NLS-1$
      msg = msg.replace("%1", se.toString()); //$NON-NLS-1$
      throw new SavException(msg);
    }

    CURRENT_ENTRY = se;
    D_ENTRY.add(se);
  }

  public void checkOutputStream(LzssEntry se) throws SavException {
    if (D_ENTRY == null)
      throw new SavException(getSavFileClosedString());

    if (!D_ENTRY.contains(se))
      throw new SavException(getFileNotFoundString(se));

    // check that it is the last file entry that is written
    int index = D_ENTRY.indexOf(se);
    if (index != D_ENTRY.size() - 1) {
      String msg = Language.getString("LzssFile.5"); //$NON-NLS-1$
      msg = msg.replace("%1", se.toString()); //$NON-NLS-1$
      throw new SavException(msg);
    }
  }

  /**
   * Returns the encrypted raw file output stream, which is shared for the different entries.
   */
  public OutputStream getRawOutputStream() {
    return D_OUTPUT;
  }

  /**
   * Checks and returns the wrapped file output stream used to write the entry data.
   */
  public OutputStream getOutputStream(LzssEntry se, boolean chunked, boolean binary) throws IOException {
    checkOutputStream(se);
    return se.getOutputStream(D_OUTPUT, chunked, binary);
  }

  /**
   * Returns a new raw data stream for the given entry.
   */
  public SizedInputStream getRawInputStream(ArchiveEntry entry) throws IOException {
    if (D_ENTRY == null)
      throw new SavException(getSavFileClosedString());

    int destAddress = entry.getAddress();
    FileInputStream in = new FileInputStream(SOURCE);
    StreamTools.skip(in, destAddress);

    SizedInputStream sin = new SizedInputStream(in, entry.getCompressedSize());
    sin.setOwnership(true);
    return sin;
  }

  /**
   * Returns a new InputStream with default textual LZSS compression.
   */
  public InputStream getInputStream(ArchiveEntry entry, boolean chunked) throws IOException {
    return get_LZSS_InputStream(entry, false, chunked);
  }

  /**
   * Returns a new InputStream used to read the entry data.
   */
  public InputStream getInputStream(ArchiveEntry entry, Compression compression, boolean chunked) throws IOException {
    SizedInputStream raw = getRawInputStream(entry);
    WrappedInputStream ws = chunked ? new ChunkReader(raw, compression)
      : compression.getDecompressionStream(raw).decode();
    ws.setOwnership(true);
    return ws;
  }

  /**
   * Returns a new InputStream used to read LZSS compressed entry data.
   */
  public InputStream get_LZSS_InputStream(ArchiveEntry entry, boolean binary, boolean chunked) throws IOException {
    return getInputStream(entry, new LzssCompression(binary), chunked);
  }

  /**
   * Closes the currently opened entry.
   */
  public void closeEntry() {
    CURRENT_ENTRY = null;
  }

  /**
   * Flushes all data to the file and closes it.
   *
   * @throws IOException
   */
  @Override
  public void close() throws IOException {
    if (D_ENTRY == null)
      return;

    // close source
    S_ENTRY = null;

    // clear last entry output stream
    if (CURRENT_ENTRY != null)
      closeEntry();

    if (D_ENTRY.isEmpty()) {
      //close destination
      if (D_OUTPUT != null) {
        D_OUTPUT.close();
        D_OUTPUT = null;
      }
      D_ENTRY = null;

      //delete first temp
      if (DEST != null)
        DEST.delete();

      return;
    }

    if (SOURCE != null) {
      String msg = Language.getString("LzssFile.7"); //$NON-NLS-1$
      msg = msg.replace("%1", SOURCE.getName()); //$NON-NLS-1$
      throw new IOException(msg);
    }

    //write map
    int index = 0;
    int entryCount = 0;
    int fileOffset = HEADER_SIZE;

    for (LzssEntry sev : D_ENTRY) {
      //name
      byte[] b = DataTools.toByte(sev.getName(), 16, 15);
      D_OUTPUT.write(b);
      //address
      sev.setAddress(fileOffset);
      D_OUTPUT.write(DataTools.toByte(fileOffset, true));
      //index
      sev.setIndex(index);
      D_OUTPUT.write(DataTools.toByte(index++, true));
      //uncompressed
      D_OUTPUT.write(DataTools.toByte(sev.getUncompressedSize(), true));
      //compressed
      D_OUTPUT.write(DataTools.toByte(sev.getCompressedSize(), true));
      fileOffset += sev.getCompressedSize();
      // sub-entries
      D_OUTPUT.write(DataTools.toByte(sev.getSubEntries(), true));
      //unknown
      D_OUTPUT.write(DataTools.toByte(sev.getUnknown(), true));

      ++entryCount;
    }

    //close destination
    D_OUTPUT.close();
    D_ENTRY = null;

    //rewrite header
    File xDEST = new File("xsav.tmp"); //$NON-NLS-1$
    int mapAddr = fileOffset;

    try (
      FileOutputStream fos = new FileOutputStream(xDEST);
      FileInputStream fis = new FileInputStream(DEST);
    ){
      fos.write(DataTools.toByte(name, 16, 15));
      fos.write(DataTools.toByte(mapAddr, true));
      fos.write(DataTools.toByte(entryCount, true));

      //copy all info
      StreamTools.skip(fis, HEADER_SIZE);
      int rem = fis.available();

      while (rem > 0) {
        int size = Integer.min(rem, 1024);
        fos.write(StreamTools.readBytes(fis, size));
        rem -= size;
      }
    }

    //delete first temp
    DEST.delete();

    //copy
    xDEST.renameTo(DEST);
  }

  public Vector<String> check() {
    Vector<String> response = new Vector<String>();
    boolean first = true;
    LzssEntry se = null;

    //check
    Iterator<LzssEntry> it = S_ENTRY.iterator();
    while (it.hasNext()) {
      if (first) {
        se = it.next();
        first = false;
      } else {
        LzssEntry prev = se;
        se = it.next();

        int fblock = prev.getAddress() + prev.getCompressedSize();
        if (fblock > se.getAddress() && fblock <= (se.getAddress() + se.getCompressedSize())) {
          if (se.getUncompressedSize() != 0 || se.getCompressedSize() != 0) {
            String msg = Language.getString("LzssFile.8"); //$NON-NLS-1$
            msg = msg.replace("%1", SOURCE.getName()); //$NON-NLS-1$
            msg = msg.replace("%2", prev.getName()); //$NON-NLS-1$
            msg = msg.replace("%3", se.getName()); //$NON-NLS-1$
            response.add(msg);
          }
        }
      }
    }

    if (first) {
      String msg = Language.getString("LzssFile.9"); //$NON-NLS-1$
      msg = msg.replace("%1", SOURCE.getName()); //$NON-NLS-1$
      response.add(msg);
    }

    return response;
  }

  private String getSavFileClosedString() {
    return Language.getString("LzssFile.10"); //$NON-NLS-1$
  }

  private String getFileNotFoundString(LzssEntry se) {
    String msg = Language.getString("LzssFile.11"); //$NON-NLS-1$
    msg = msg.replace("%1", se.toString()); //$NON-NLS-1$
    return msg;
  }
}
