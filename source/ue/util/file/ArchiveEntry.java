package ue.util.file;

/**
 * For iterating the archive entries.
 */
public interface ArchiveEntry {

  // @return the file name
  String      getName();
  // @return the lower case file name
  String      getLcName();
  // @return the file type ending
  String      getFileType();

  // @return the file offset of the entry
  int         getAddress();
  // @return uncompressed file size
  int         getUncompressedSize();
  // @return compressed file size
  int         getCompressedSize();
  // @return chunked sub-entry count
  int         getSubEntries();

}
