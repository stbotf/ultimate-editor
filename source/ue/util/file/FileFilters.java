package ue.util.file;

import java.io.File;
import java.io.FilenameFilter;
import ue.service.Language;

public abstract class FileFilters {

  public static final FilenameFilter Ani        = suffix(".ani"); //$NON-NLS-1$
  public static final FilenameFilter AniOrCur   = suffix(".ani", ".cur"); //$NON-NLS-1$ //$NON-NLS-2$
  public static final FilenameFilter AuthorList = suffix("author.lst"); //$NON-NLS-1$
  public static final FilenameFilter ClassList  = suffix("class.lst"); //$NON-NLS-1$
  public static final FilenameFilter Cur        = suffix(".cur"); //$NON-NLS-1$
  public static final FilenameFilter Dic        = suffix(".dic"); //$NON-NLS-1$
  public static final FilenameFilter Fnt        = suffix(".fnt"); //$NON-NLS-1$
  public static final FilenameFilter Gif        = suffix(".gif"); //$NON-NLS-1$
  public static final FilenameFilter Hob        = suffix(".hob"); //$NON-NLS-1$
  public static final FilenameFilter Mnu        = suffix(".mnu"); //$NON-NLS-1$
  public static final FilenameFilter Sav        = suffix(".sav"); //$NON-NLS-1$
  public static final FilenameFilter Tdb        = suffix(".tdb"); // text database //$NON-NLS-1$
  public static final FilenameFilter Tga        = suffix(".tga"); //$NON-NLS-1$
  public static final FilenameFilter TgaMaps    = suffix("maps.tga"); //$NON-NLS-1$
  public static final FilenameFilter Wdf        = suffix(".wdf"); //$NON-NLS-1$
  public static final FilenameFilter Wtf        = suffix(".wtf"); //$NON-NLS-1$

  public static boolean isModelGraphic(String fileName) {
    fileName = fileName.toLowerCase();
    return fileName.endsWith(".hob") || fileName.startsWith("i_") && fileName.endsWith(".tga"); //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$
  }

  public static final FilenameFilter ModelGraphics = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase();
      return isModelGraphic(name);
    }
  };

  public static FilenameFilter prefix(String prefix) {
    return new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        name = name.toLowerCase();
        return name.startsWith(prefix);
      }
    };
  }

  public static FilenameFilter prefix(String... prefixes) {
    return new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        name = name.toLowerCase();
        for (String pfx : prefixes)
          if (name.startsWith(pfx))
            return true;
        return false;
      }
    };
  }

  public static FilenameFilter suffix(String suffix) {
    return new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        name = name.toLowerCase();
        return name.endsWith(suffix);
      }
    };
  }

  public static FilenameFilter suffix(String... suffixes) {
    return new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        name = name.toLowerCase();
        for (String sfx : suffixes)
          if (name.endsWith(sfx))
            return true;
        return false;
      }
    };
  }

  public static FilenameFilter decorated(String prefix, String suffix) {
    return new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        name = name.toLowerCase();
        return name.startsWith(prefix) && name.endsWith(suffix);
      }
    };
  }

  public static String getFileType(String fileName) {
    // determine file type specifier
    // these must not be static for language change
    if (FileFilters.Tga.accept(null, fileName)) {
      return Language.getString("FileFilters.0"); //$NON-NLS-1$
    } else if (FileFilters.Gif.accept(null, fileName)) {
      return Language.getString("FileFilters.1"); //$NON-NLS-1$
    } else if (FileFilters.Wtf.accept(null, fileName)) {
      return Language.getString("FileFilters.2"); //$NON-NLS-1$
    } else if (FileFilters.Wdf.accept(null, fileName)) {
      return Language.getString("FileFilters.3"); //$NON-NLS-1$
    } else if (FileFilters.Ani.accept(null, fileName)) {
      return Language.getString("FileFilters.4"); //$NON-NLS-1$
    } else if (FileFilters.Cur.accept(null, fileName)) {
      return Language.getString("FileFilters.5"); //$NON-NLS-1$
    } else if (FileFilters.Dic.accept(null, fileName)) {
      return Language.getString("FileFilters.6"); //$NON-NLS-1$
    } else if (FileFilters.Fnt.accept(null, fileName)) {
      return Language.getString("FileFilters.7"); //$NON-NLS-1$
    } else if (FileFilters.Tdb.accept(null, fileName)) {
      return Language.getString("FileFilters.8"); //$NON-NLS-1$
    } else {
      int idx = fileName.lastIndexOf("."); //$NON-NLS-1$

      if (idx >= 0) {
        // <SUFFIX> File
        String suffix = fileName.substring(idx + 1);
        String type = Language.getString("FileFilters.9"); //$NON-NLS-1$
        return type.replace("%1", suffix.toUpperCase()); //$NON-NLS-1$
      } else {
        // unknown 'Binary File'
        return Language.getString("FileFilters.10"); //$NON-NLS-1$
      }
    }
  }
}
