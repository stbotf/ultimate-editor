package ue.util.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import lombok.val;
import ue.service.Language;
import ue.util.func.FunctionTools.CheckedBiPredicate;
import ue.util.stream.StreamTools;

public interface FileStore extends AutoCloseable {
  int numFiles();

  boolean contains(String fileName);

  // returns null if not found
  Optional<? extends ArchiveEntry> findEntry(String fileName);

  default ArchiveEntry getEntry(String fileName) throws FileNotFoundException {
    val entry = findEntry(fileName);
    if (!entry.isPresent()) {
      String msg = Language.getString("FileStore.0") //$NON-NLS-1$
        .replace("%1", fileName); //$NON-NLS-1$
      throw new FileNotFoundException(msg);
    }
    return entry.get();
  }

  default Optional<? extends ArchiveEntry> getEntry(String fileName, boolean throwIfMissing)
      throws FileNotFoundException {
    return throwIfMissing ? Optional.of(getEntry(fileName)) : findEntry(fileName);
  }

  default int getFileSize(String fileName) throws FileNotFoundException {
    val entry = getEntry(fileName);
    return (int) entry.getUncompressedSize();
  }

  List<String> listFileNames();
  List<? extends ArchiveEntry> listFileEntries();

  boolean processFiles(CheckedBiPredicate<FileStore, ArchiveEntry, IOException> callback) throws IOException;
  Stream<? extends ArchiveEntry> stream();

  InputStream getInputStream(ArchiveEntry entry, boolean chunked) throws IOException;
  default InputStream getInputStream(String fileName, boolean chunked) throws IOException {
    return getInputStream(getEntry(fileName), chunked);
  }

  default byte[] getRawData(ArchiveEntry entry, boolean chunked) throws IOException {
    try (InputStream in = getInputStream(entry, chunked)) {
      // for result.lst we need to decompress each chunk individually
      // to access the raw file data
      return StreamTools.readAllChunks(in);
    }
  }

  default Optional<byte[]> getRawData(String fileName, boolean chunked) throws IOException {
    val entry = getEntry(fileName, false);
    return entry.isPresent() ? Optional.of(getRawData(entry.get(), chunked))
      : Optional.empty();
  }

  @Override
  void close() throws IOException;

}
