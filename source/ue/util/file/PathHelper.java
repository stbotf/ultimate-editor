package ue.util.file;

import java.io.File;
import java.nio.file.Path;

public interface PathHelper {

  public static Path toFolderPath(File file) {
    Path filePath = file.toPath();
    if (file.isFile())
      filePath = filePath.getParent();
    return filePath.toAbsolutePath();
  }

  public static File getParentFile(File file) {
    // for windows media library files .getParentFile() returns a non-writable virtual folder
    // like ::{031E4825-7B94-4DC3-B131-E946B44C8DD5}\Pictures.library-ms
    // see https://stackoverflow.com/questions/10777647/java-dealing-with-libraries-in-windows-7-such-as-documents-pictures-etc
    // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=59046#p59046
    Path parentPath = file.toPath().getParent();
    return parentPath.toFile();
  }

}
