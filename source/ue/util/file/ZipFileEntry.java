package ue.util.file;

import java.util.zip.ZipEntry;
import lombok.Getter;

/**
 * For iterating zipped stbof.res archive entries.
 */
public class ZipFileEntry implements ArchiveEntry {

  @Getter private ZipEntry zipEntry;
  @Getter private String lcName;

  public ZipFileEntry(ZipEntry zipEntry) {
    this.zipEntry = zipEntry;
    this.lcName = zipEntry.getName().toLowerCase();
  }

  @Override
  public String getName() {
    return zipEntry.getName();
  }

  @Override
  public String getFileType() {
    return FileFilters.getFileType(lcName);
  }

  @Override
  public int getAddress() {
    // not supported
    return -1;
  }

  @Override
  public int getUncompressedSize() {
    return (int) zipEntry.getSize();
  }

  @Override
  public int getCompressedSize() {
    return (int) zipEntry.getCompressedSize();
  }

  @Override
  public int getSubEntries() {
    return 1;
  }

}
