package ue.util.file;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import ue.util.stream.StreamTools;

public interface ZipHelper {

  public static void copySourceFile(ZipOutputStream zos, ZipStore source, ArchiveEntry ze) throws IOException {
    ZipEntry zfl = new ZipEntry(ze.getName());
    try (InputStream is = source.getInputStream(ze, false)) {
      int ln = is.available();
      zfl.setSize(ln);
      zfl.setCompressedSize(-1);
      zfl.setMethod(ZipEntry.DEFLATED);
      zos.putNextEntry(zfl);
      StreamTools.transfer(is, zos);
    }
  }
}
