/**
 * Author of the original algorithm written in C is Riky S <afax at afax.cjb.net>. Modified by: Alan
 * Podlesek <seqdcer@yahoo.com> (6.10.2007)
 */
package ue.util.lzss;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import ue.util.stream.CompressionStream;


/**
 * This class encodes data using the modified LZSS algorithm used for the saved games.
 */
public class LzssOutputStream extends CompressionStream {

  // constants
  private final int N = 4096;         // number of nodes
  private final int F = 17;           // string length
  private final int NIL = N;          // means not in use
  private final int THRESHOLD = 1;

  // other
  private byte[] DICT = new byte[N + F - 1];
  private int[] lson = new int[N + 1];
  private int[] rson = new int[N + 257];      // N + 1 to N + 256 are roots of trees
  private int[] dad = new int[N + 1];         // node parent
  private boolean isBinary;

  // It is unclear with what values trek.exe fills the dictionary.
  // It might even be uninitialized data and sometimes work either way.
  //
  // For techInfo I have evidence, that in my test setup,
  // using the 0x20 space character messes up the file for trek.exe.
  // For many other files however using 0x00 crashes trek.exe already at load.
  // I didn't find a clear pattern nor evidence by the asm code,
  // but I noticed the trek.exe encrypted files are slightly larger.
  //
  // Further in my sav file analysis I never found reference that
  // the trek.exe LZSS encryption is using this feature.
  // Therefore I assume it is not used and only ever worked by accident that the
  // space character happens to occur less frequent in the binary files.
  // This further explains why it never made a difference to UE
  // with what value the dictionary is filled.
  // So better keep safe and only allow dictionary references to already written data.
  private boolean referUnwritten = false;

  private class NodeMatch {

    public int match_position;
    public int match_length;
  }

  public LzssOutputStream(OutputStream fos, boolean isBinary) {
    super(fos);
    this.isBinary = isBinary;
  }

  //initializes the tree
  private void initTree() {
    int i;
    /*
     * For i = 0 to N - 1, rson[i] and lson[i] will be the right and
     * left children of node i.  These nodes need not be initialized.
     * Also, dad[i] is the parent of node i.  These are initialized to
     * NIL (= N), which stands for 'not used.'
     * For i = 0 to 255, rson[N + i + 1] is the root of the tree
     * for strings that begin with character i. These are initialized
     * to NIL. Note there are 256 trees.
     */
    for (i = N + 1; i <= N + 256; i++) {
      rson[i] = NIL;        // set all roots to NIL (unused)
    }

    for (i = 0; i < N; i++) {
      dad[i] = NIL;
    }
  }

  // inserts a node
  private void insertNode(int r, NodeMatch nodeMatch) {
    // no match found
    nodeMatch.match_position = 0;
    nodeMatch.match_length = 0;

    /*
     * Inserts string of length F, DICT[r..r+F-1], into one of the
     * trees (DICT[r]'th tree) and returns the longest-match position
     * and length via the global variables match_position and match_length.
     * If match_length = F, then removes the old node in favor of the new
     * one, because the old one will be deleted sooner.
     *
     * Note r plays double role, as tree node and position in buffer.
     */

    // pointer key is replaced by DICT[r+index]
    int i, p, cmp = 1, key = r;
    int temp1;
    int temp2;

    p = N + 1 + ((DICT[key]) & 0xff);

    lson[r] = rson[r] = NIL;

    while (true) {
      if (cmp >= 0) {
        if (rson[p] != NIL) {
          p = rson[p];
        } else {
          rson[p] = r;
          dad[r] = p;
          return;
        }
      } else {
        if (lson[p] != NIL) {
          p = lson[p];
        } else {
          lson[p] = r;
          dad[r] = p;
          return;
        }
      }

      // determine match length
      for (i = 1; i < F; i++) {
        temp1 = (DICT[key + i]) & 0xff;
        temp2 = (DICT[p + i]) & 0xff;

        cmp = temp1 - temp2;

        if (cmp != 0) {
          break;
        }
      }

      // update match result
      if (i > nodeMatch.match_length) {
        nodeMatch.match_position = p;

        nodeMatch.match_length = i;

        if (nodeMatch.match_length >= F) {
          break;
        }
      }
    }

    dad[r] = dad[p];
    lson[r] = lson[p];
    rson[r] = rson[p];

    dad[lson[p]] = r;
    dad[rson[p]] = r;

    if (rson[dad[p]] == p) {
      rson[dad[p]] = r;
    } else {
      lson[dad[p]] = r;
    }
    // remove p
    dad[p] = NIL;
  }

  //deletes a node
  private void deleteNode(int p) {
    int q;

    if (dad[p] == NIL) {
      // not in tree
      return;
    }

    if (rson[p] == NIL) {
      q = lson[p];
    } else if (lson[p] == NIL) {
      q = rson[p];
    } else {
      q = lson[p];

      if (rson[q] != NIL) {
        do {
          q = rson[q];
        } while (rson[q] != NIL);

        rson[dad[q]] = lson[q];
        dad[lson[q]] = dad[q];
        lson[q] = lson[p];
        dad[lson[p]] = q;
      }

      rson[q] = rson[p];
      dad[rson[p]] = q;
    }

    dad[q] = dad[p];

    if (rson[dad[p]] == p) {
      rson[dad[p]] = q;
    } else {
      lson[dad[p]] = q;
    }

    dad[p] = NIL;
  }

  @Override
  protected void encode(OutputStream fos, byte[] data) throws IOException {
    int data_size = data.length;
    if (data_size == 0) {
      return;
    }

    // initialize trees
    initTree();

    // code_buf[1..16] saves eight units of code, and
    // code_buf[0] works as eight flags,
    // "1" representing that the unit is an uncoded letter (1 byte),
    // "0" a position-and-length pair (2 bytes).
    // Thus, eight units require at most 16 bytes of code.
    byte[] code_buf = new byte[17];
    code_buf[0] = 0;
    int code_buf_ptr = 1;
    byte mask = 1;

    int dataIndex = 0; // current data position
    int match_position2;
    NodeMatch nodeMatch = new NodeMatch();

    // Read F bytes into the last F bytes of the buffer
    int F_len = Integer.min(F, data_size);
    for (int i = 0; i < F_len; i++) {
      DICT[i + N - F] = data[dataIndex++];
    }

    if (referUnwritten) {
      // Clear dictionary filling it with space characters 0x20.
      // This is a common method to slightly increase compression rate
      // for text files and is required by BotF!
      // This 'phantom' value is not content to the compressed file format,
      // so it must always be same for compression and decompression!
      Arrays.fill(DICT, 0, N - F, isBinary ? 0 : (byte) 0x20);

      // Insert the F strings,
      // each of which begins with one or more 'phantom' characters.
      // Note the order in which these strings are inserted.
      // This way, degenerate trees will be less likely to occur.
      for (int i = 2; i <= F; i++) {
        insertNode((N - F) - i, nodeMatch);
      }
    }

    // Finally, insert the whole string just read.
    // The global variables match_length and match_position are set.
    insertNode(N - F, nodeMatch);

    int s = 0, r = N - F;
    do {
      // get newest search match
      int match_length = nodeMatch.match_length;
      int match_position = nodeMatch.match_position;

      if (match_length > F_len) {
        // match_length may be spuriously long near the end of text.
        match_length = F_len;
      }
      if (match_length <= THRESHOLD) {
        match_length = 1;  /* Not long enough match. Send one byte. */
        code_buf[0] |= mask;  /* 'send one byte' flag */
        code_buf[code_buf_ptr++] = DICT[r];  /* Send uncoded. */
      } else {
        match_position2 = match_position - N + F + 1;

        // Send position and length pair. Note match_length > THRESHOLD.
        code_buf[code_buf_ptr++] = (byte)
            (((match_position2 >>> 8) & 0x0f)
                | (((match_length - (THRESHOLD + 1)) << 4) & 0xf0));

        code_buf[code_buf_ptr++] = (byte) match_position2;
      }

      // Shift mask left one bit.
      mask = (byte) (mask << 1);

      if (mask == 0) {
        // Send at most 8 units of code together
        for (int i = 0; i < code_buf_ptr; i++) {
          fos.write(code_buf[i]);
        }

        code_buf[0] = 0;
        code_buf_ptr = mask = 1;
      }

      int i = 0;
      for (; i < match_length && (data_size - dataIndex > 0); i++) {
        // Delete old strings and read new bytes
        deleteNode(s);
        DICT[s] = data[dataIndex];

        // read new bytes
        if (s < F - 1) {
          DICT[s + N] = data[dataIndex];
        }

        dataIndex++;

        // Since this is a ring buffer, increment the position modulo N.
        s = (s + 1) & (N - 1);
        // Register the string in DICT[r..r+F-1]
        r = (r + 1) & (N - 1);
        insertNode(r, nodeMatch);
      }

      // After the end of text, no need to read,
      // but buffer may not be empty.
      while (i++ < match_length) {
        deleteNode(s);
        s = (s + 1) & (N - 1);
        r = (r + 1) & (N - 1);
        --F_len;

        if (F_len != 0) {
          insertNode(r, nodeMatch);
        }
      }
    } while (F_len > 0); // until length of string to be processed is zero

    // Send remaining code.
    if (code_buf_ptr > 1) {
      for (int i = 0; i < code_buf_ptr; i++) {
        fos.write(code_buf[i]);
      }
    }
  }
}
