package ue.util.lzss;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.util.stream.Compression;
import ue.util.stream.CompressionStream;
import ue.util.stream.DecompressionStream;

public class LzssCompression implements Compression {

  private boolean isBinary;

  public LzssCompression(boolean isBinary) {
    this.isBinary = isBinary;
  }

  @Override
  public CompressionStream getCompressionStream(OutputStream out) {
    return new LzssOutputStream(out, isBinary);
  }

  @Override
  public DecompressionStream getDecompressionStream(InputStream in) throws IOException {
    return new LzssInputStream(in, isBinary, false);
  }
}
