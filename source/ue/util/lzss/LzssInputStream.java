/**
 * Author of the original algoritm written in C is Riky S <afax at afax.cjb.net>. Modified by: Alan
 * Podlesek <seqdcer@yahoo.com> (6.10.2007)
 */
package ue.util.lzss;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import ue.util.stream.DecompressionStream;

// FIXME: add error messages

/**
 * This InputStream is used to decompress read botf compressed data using the modified lzss
 * algorithm. This class was 'ported' from C code given by Riky S.
 */
public class LzssInputStream extends DecompressionStream {

  private final int N = 4096;
  private final int F = 17;
  private final int THRESHOLD = 1;

  private byte[] DICT = new byte[N + F - 1];
  private boolean isBinary;

  public LzssInputStream(InputStream in, boolean isBinary) throws IOException {
    this(in, isBinary, true);
  }

  public LzssInputStream(InputStream in, boolean isBinary, boolean decompress) throws IOException {
    super(in);
    this.isBinary = isBinary;
    if (decompress) {
      this.decode(in.available());
    }
  }

  // works like a charm :) thanks Riky, 'copied' this from lzss.c
  @Override
  protected byte[] decode(InputStream in, int size) throws IOException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    // read and decode
    int pos, len, dictPos, val, byte1, byte2, flags;

    // Clear dictionary filling it with space characters 0x20.
    // This is a common method to slightly increase compression rate
    // for text files and is required by BotF!
    // This 'phantom' value is not content to the compressed file format,
    // so it must always be same for compression and decompression!
    Arrays.fill(DICT, 0, N - F, isBinary ? 0 : (byte) 0x20);

    dictPos = N - F;
    flags = 0;
    while (size > 0) {
      if (((flags >>>= 1) & 256) == 0) {
        try {
          --size;
          val = in.read();
        } catch (Exception e) {
          break;
        }
        flags = val | 0xff00; /* uses higher byte cleverly */
      } /* to count eight */
      if ((flags & 1) != 0) {
        try {
          --size;
          val = in.read();
        } catch (Exception e) {
          break;
        }
//                                DATA.add(c);
        if (val > 127) {
          buffer.write((byte) (val - 256));
          DICT[dictPos++] = Byte.parseByte(Integer.toString(val - 256));
        } else {
          buffer.write((byte) val);
          DICT[dictPos++] = Byte.parseByte(Integer.toString(val));
        }
        dictPos &= (N - 1);
      } else {
        try {
          size -= 2;
          byte1 = in.read();
          byte2 = in.read();
        } catch (Exception e) {
          break;
        }
        pos = (((byte1 & 0x0f) << 8) | byte2) + N - F - 1;
        len = ((byte1 & 0xf0) >>> 4) + THRESHOLD;

        for (int k = 0; k <= len; k++) {
          val = DICT[(pos + k) & (N - 1)];
//                                        DATA.add(c);
          if (val > 127) {
            buffer.write((byte) (val - 256));
            DICT[dictPos++] = Byte.parseByte(Integer.toString(val - 256));
          } else {
            buffer.write((byte) val);
            DICT[dictPos++] = Byte.parseByte(Integer.toString(val));
          }
          dictPos &= (N - 1);
        }
      }
    }

    return buffer.toByteArray();
  }
}