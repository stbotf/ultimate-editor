package ue.gui.sav;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.SeedInfo;
import ue.edit.sav.files.Version;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.data.StringTools;

/**
 * Used to edit game info of a saved game.
 */
public class GameConfigGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_ERAS = CStbof.NUM_ERAS;

  // read
  private Lexicon LEX;
  private RaceRst RACE;
  private SavGame game;

  // edited
  private Version VERSION;
  private GameInfo GAME;
  private SeedInfo SEEDINFO;

  // ui components
  private JTextField txtGameName = new JTextField();
  private JTextField txtTurn = new JTextField();
  private JTextField txtVersion = new JTextField();
  private JTextField txtStartingSeed = new JTextField();
  private JComboBox<String> jcbGameType = new JComboBox<String>();
  private JComboBox<ID> jcbPlayerEmpire = new JComboBox<ID>();
  private JLabel[] jcbCivLvls = new JLabel[NUM_ERAS];
  private JLabel jcbMinors = new JLabel();
  private JComboBox<ID> jcbDifficulty = new JComboBox<ID>();
  private JComboBox<ID> jcbTacticalCombat = new JComboBox<ID>();
  private JComboBox<ID> jcbVictoryConditions = new JComboBox<ID>();
  private JLabel jcbGalShape = new JLabel();
  private JLabel jcbGalSize = new JLabel();
  private JTextField txtStrategicTimer = new JTextField(5);
  private JTextField txtTacticalTimer = new JTextField(5);
  private JCheckBox chkRandomEvents = new JCheckBox();
  // labels
  private JLabel lblGameName = new JLabel(Language.getString("GameConfigGUI.14"));
  private JLabel lblTurn = new JLabel(Language.getString("GameConfigGUI.15"));
  private JLabel lblVersion = new JLabel(Language.getString("GameConfigGUI.16"));
  private JLabel lblStartingSeed = new JLabel(Language.getString("GameConfigGUI.17"));
  private JLabel lblGameType = new JLabel(Language.getString("GameConfigGUI.0"));
  private JLabel lblPlayerEmpire = new JLabel(Language.getString("GameConfigGUI.3"));
  private JLabel[] lblCivLvls = new JLabel[NUM_ERAS];
  private JLabel lblMinors = new JLabel(Language.getString("GameConfigGUI.5"));
  private JLabel lblDifficulty = new JLabel(Language.getString("GameConfigGUI.6"));
  private JLabel lblTacticalCombat = new JLabel(Language.getString("GameConfigGUI.10"));
  private JLabel lblVictoryCondition = new JLabel(Language.getString("GameConfigGUI.11"));
  private JLabel lblGalShape = new JLabel(Language.getString("GameConfigGUI.12"));
  private JLabel lblGalSize = new JLabel(Language.getString("GameConfigGUI.13"));
  private JLabel lblStrategicTimer = new JLabel(Language.getString("GameConfigGUI.7"));
  private JLabel lblTacticalTimer = new JLabel(Language.getString("GameConfigGUI.8"));
  private JLabel lblRandomEvents = new JLabel(Language.getString("GameConfigGUI.9"));

  /**
   * Constructor.
   * @throws IOException
   */
  public GameConfigGUI(SavGame sav, Stbof stbof) throws IOException {
    this.game = sav;

    Version version = (Version) sav.getInternalFile(CSavFiles.Version, true);
    GameInfo gameInfo = (GameInfo) sav.getInternalFile(CSavFiles.GameInfo, true);
    SeedInfo seedInfo = (SeedInfo) sav.getInternalFile(CSavFiles.SeedInfo, true);

    RACE = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    LEX = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);

    setupControls();
    setupLayout();
    loadSelectionValues();
    load(version, gameInfo, seedInfo);

    // set action listeners last, so the load function raises no events
    addActionListeners();
  }

  private void setupControls() {
    Font def = UE.SETTINGS.getDefaultFont();
    lblGameName.setFont(def);
    txtGameName.setFont(def);
    lblTurn.setFont(def);
    txtTurn.setFont(def);
    lblVersion.setFont(def);
    txtVersion.setFont(def);
    lblStartingSeed.setFont(def);
    txtStartingSeed.setFont(def);
    lblGameType.setFont(def);
    jcbGameType.setFont(def);
    lblPlayerEmpire.setFont(def);
    jcbPlayerEmpire.setFont(def);

    for (int i = 0; i < NUM_ERAS; i++) {
      JLabel lblCivLvl = new JLabel();
      lblCivLvl.setFont(def);
      lblCivLvls[i] = lblCivLvl;
      JLabel jcbCivLvl = new JLabel();
      jcbCivLvl.setFont(def);
      jcbCivLvls[i] = jcbCivLvl;
    }

    lblMinors.setFont(def);
    jcbMinors.setFont(def);
    lblDifficulty.setFont(def);
    jcbDifficulty.setFont(def);
    lblTacticalCombat.setFont(def);
    jcbTacticalCombat.setFont(def);
    lblVictoryCondition.setFont(def);
    jcbVictoryConditions.setFont(def);
    lblGalShape.setFont(def);
    jcbGalShape.setFont(def);
    lblGalSize.setFont(def);
    jcbGalSize.setFont(def);
    lblStrategicTimer.setFont(def);
    txtStrategicTimer.setFont(def);
    lblTacticalTimer.setFont(def);
    txtTacticalTimer.setFont(def);
    lblRandomEvents.setFont(def);
    chkRandomEvents.setFont(def);
  }

  static final int RowHeight = 25;

  private void setupLayout() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(0, 5, 0, 5);
    // 1st column
    c.gridx = 0;
    c.gridy = 0;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblGameName, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblVersion, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblGameType, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblTurn, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblPlayerEmpire, c);
    for (JLabel lblCivLvl : lblCivLvls) {
      c.gridy++;
      add(Box.createVerticalStrut(RowHeight), c);
      add(lblCivLvl, c);
    }
    // 2nd column
    c.gridwidth = 3;
    c.gridx++;
    c.gridy = 0;
    add(txtGameName, c);
    c.gridwidth = 1;
    c.gridy++;
    add(txtVersion, c);
    c.gridy += 2;
    add(jcbGameType, c);
    c.gridy++;
    add(txtTurn, c);
    c.gridy += 2;
    add(jcbPlayerEmpire, c);
    for (JLabel jcbCivLvl : jcbCivLvls) {
      c.gridy++;
      add(jcbCivLvl, c);
    }
    // 3rd column
    c.gridx++;
    c.gridy = 1;
    add(lblStartingSeed, c);
    c.gridy += 2;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblMinors, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblDifficulty, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblStrategicTimer, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblTacticalTimer, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblTacticalCombat, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblVictoryCondition, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblGalShape, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblGalSize, c);
    c.gridy++;
    add(Box.createVerticalStrut(RowHeight), c);
    add(lblRandomEvents, c);
    // 4th column
    c.gridx++;
    c.gridy = 1;
    add(txtStartingSeed, c);
    c.gridy += 2;
    add(jcbMinors, c);
    c.gridy++;
    add(jcbDifficulty, c);
    c.gridy++;
    add(txtStrategicTimer, c);
    c.gridy++;
    add(txtTacticalTimer, c);
    c.gridy++;
    add(jcbTacticalCombat, c);
    c.gridy++;
    add(jcbVictoryConditions, c);
    c.gridy++;
    add(jcbGalShape, c);
    c.gridy++;
    add(jcbGalSize, c);
    c.gridy++;
    add(chkRandomEvents, c);
  }

  private void addActionListeners() {
    // game name
    GuiTools.addTextChangeListeners(txtGameName, s -> GAME.setGameName(s));
    // save version
    GuiTools.addTextChangeListeners(txtVersion, s -> {
      int version = Integer.parseInt(s);
      VERSION.setVersion(version);
    });
    // starting seed
    GuiTools.addTextChangeListeners(txtStartingSeed, s -> {
      int seed = Integer.parseInt(s);
      SEEDINFO.setStartingSeed(seed);
    });
    // game type
    jcbGameType.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (jcbGameType.getSelectedIndex() == 0) {
          GAME.setMultiplayer(false);
        } else {
          GAME.setMultiplayer(true);
        }
      }
    });
    // turn
    GuiTools.addTextChangeListeners(txtTurn, s -> {
      short turn = Short.parseShort(s);
      GAME.setCurrentTurn(turn);
    });
    // player empire
    jcbPlayerEmpire.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          GAME.setPlayerEmpire((byte) jcbPlayerEmpire.getSelectedIndex());
        } catch (Exception ze) {
          Dialogs.displayError(ze);
        }
      }
    });
    // difficulty
    jcbDifficulty.addActionListener(new ActionListener(){
      @Override
      public void actionPerformed(ActionEvent ae){
        try{
          GAME.setDifficulty((short)((ID)jcbDifficulty.getSelectedItem()).ID);
        }catch (InvalidArgumentsException ze){
          Dialogs.displayError(ze);
        }
      }
    });
    // strategic timer
    GuiTools.addTextChangeListeners(txtStrategicTimer, s -> {
      int timer = Integer.parseInt(s);
      GAME.setStrategicalTimer(timer);
    });
    // tactical timer
    GuiTools.addTextChangeListeners(txtTacticalTimer, s -> {
      int timer = Integer.parseInt(s);
      GAME.setTacticalTimer(timer);
    });
    // tactical combat
    jcbTacticalCombat.addActionListener(new ActionListener(){
      @Override
      public void actionPerformed(ActionEvent ae){
        try{
          GAME.setTacticalMode(((ID)jcbTacticalCombat.getSelectedItem()).ID);
        }catch (InvalidArgumentsException ze){
          Dialogs.displayError(ze);
        }
      }
    });
    // victory condition
    jcbVictoryConditions.addActionListener(new ActionListener(){
      @Override
      public void actionPerformed(ActionEvent ae){
        try{
          GAME.setVictoryConditions(((ID)jcbVictoryConditions.getSelectedItem()).ID);
        }catch (InvalidArgumentsException ze){
          Dialogs.displayError(ze);
        }
      }
    });
    // random events
    chkRandomEvents.addActionListener(new ActionListener(){
      @Override
      public void actionPerformed(ActionEvent ae){
        GAME.setRandomEvents(chkRandomEvents.isSelected());
      }
    });
  }

  private void loadSelectionValues() {
    String errors = "";

    // game type
    jcbGameType.addItem(Language.getString("GameConfigGUI.1")); //$NON-NLS-1$
    jcbGameType.addItem(Language.getString("GameConfigGUI.2")); //$NON-NLS-1$

    // player empire / empire names
    for (int i = 0; i < NUM_EMPIRES; i++) {
      try {
        String raceName = RACE.getName(i);
        lblCivLvls[i].setText(raceName + ':');
        jcbPlayerEmpire.addItem(new ID(raceName, i));
      } catch (Exception ze) {
        errors = StringTools.joinNotEmpty("\n", errors, ze.getMessage());
      }
    }

    // difficulty
    try{
      jcbDifficulty.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.Difficulty.SIMPLE), 1));
      jcbDifficulty.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.Difficulty.EASY), 2));
      jcbDifficulty.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.Difficulty.NORMAL), 3));
      jcbDifficulty.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.Difficulty.HARD), 4));
      jcbDifficulty.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.Difficulty.IMPOSSIBLE), 5));
    } catch (Exception ze){
      errors = StringTools.joinNotEmpty("\n", errors, ze.getMessage());
    }

    // tactical combat
    try{
      jcbTacticalCombat.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.TacticalCombat.MANUAL), 0));
      jcbTacticalCombat.addItem(new ID(LEX.getEntry(LexDataIdx.Shared.Invalid), 1));
      jcbTacticalCombat.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.TacticalCombat.AUTOMATIC), 2));
    } catch (Exception ze){
      errors = StringTools.joinNotEmpty("\n", errors, ze.getMessage());
    }

    // victory condition
    try{
      jcbVictoryConditions.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.VictoryCondition.DOMINATION), 0));
      jcbVictoryConditions.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.VictoryCondition.TEAM_PLAY), 1));
      jcbVictoryConditions.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.VictoryCondition.VENDETTA), 2));
    } catch (Exception ze){
      errors = StringTools.joinNotEmpty("\n", errors, ze.getMessage());
    }

    if (!errors.isEmpty())
      Dialogs.displayError(new Exception(errors));
  }

  private void load(Version v, GameInfo gameInfo, SeedInfo seedInfo) {
    VERSION = v;
    GAME = gameInfo;
    SEEDINFO = seedInfo;

    txtGameName.setText(GAME.getSaveName());
    txtVersion.setText(Integer.toString(VERSION.getVersion()));
    txtStartingSeed.setText(Integer.toString(SEEDINFO.getStartingSeed()));

    jcbGameType.setSelectedIndex(GAME.isMultiplayer() ? 1 : 0);
    txtTurn.setText(Integer.toString(GAME.getCurrentTurn()));
    jcbPlayerEmpire.setSelectedIndex(GAME.getPlayerEmpire());

    String invalid = LEX.getEntryOrDefault(LexDataIdx.Shared.Invalid, "Invalid"); //$NON-NLS-1$

    // civilization levels
    byte[] evoLvls = GAME.getStartEvoLvls();
    for (int i = 0; i < NUM_ERAS; i++) {
      String text = LEX.getEntryOrDefault(LexHelper.lexEvoLvl(evoLvls[i]), invalid);
      jcbCivLvls[i].setText(text.toUpperCase());
    }

    String text = LEX.getEntryOrDefault(LexHelper.lexMinorAmount(GAME.getMinorRaceAmount()), invalid);
    jcbMinors.setText(text.toUpperCase());
    jcbDifficulty.setSelectedIndex(GAME.getDifficulty()-1);
    jcbTacticalCombat.setSelectedIndex(GAME.getTacticalMode());
    jcbVictoryConditions.setSelectedIndex(GAME.getVictoryConditions());

    text = LEX.getEntryOrDefault(LexHelper.lexGalShape(GAME.getGalaxyShape()), invalid);
    jcbGalShape.setText(text.toUpperCase());
    text = LEX.getEntryOrDefault(LexHelper.lexGalSize(GAME.getGalaxySize()), invalid);
    jcbGalSize.setText(text.toUpperCase());

    txtStrategicTimer.setText(Integer.toString(GAME.getStrategicalTimer()));
    txtTacticalTimer.setText(Integer.toString(GAME.getTacticalTimer()));
    chkRandomEvents.setSelected(GAME.isRandomEvents());
  }

  @Override
  public String menuCommand() {
    return MenuCommand.GameConfig;
  }

  @Override
  public boolean hasChanges() {
    return GAME.madeChanges() || SEEDINFO.madeChanges() || VERSION.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    game.checkChanged(GAME, changes);
    game.checkChanged(SEEDINFO, changes);
    game.checkChanged(VERSION, changes);
  }

  @Override
  public void reload() throws IOException {
    GameInfo gameInfo = (GameInfo) game.getInternalFile(CSavFiles.GameInfo, true);
    SeedInfo seedInfo = (SeedInfo) game.getInternalFile(CSavFiles.SeedInfo, true);
    Version version = (Version) game.getInternalFile(CSavFiles.Version, true);
    load(version, gameInfo, seedInfo);
  }

  @Override
  public void finalWarning() {
  }
}
