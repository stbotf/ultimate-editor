package ue.gui.sav.emp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import lombok.val;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.map.PlanetBonusTypes;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.CountReq;
import ue.edit.res.stbof.common.CStbof.SystemStatusReq;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.AIBldReq;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.AlienInfo.AlienRelationship;
import ue.edit.sav.files.emp.AlienInfo.RaceStatus;
import ue.edit.sav.files.emp.StrcInfo;
import ue.edit.sav.files.emp.TechInfo;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.SystInfo.SystemFilter;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.sav.map.MapGUI;
import ue.gui.util.component.DropdownSpinner;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.style.InvisibleScrollBarUI;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.func.FunctionTools;

/**
 * Used to edit game info of a saved game.
 */
public class StrcInfoGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private static final int TABLE_WIDTH = 100;
  private static final int TABLE_HEIGHT = 200;
  private static final int HEADER_HEIGHT = 22;
  private static final int MIN_LABEL_WIDTH = 80;

  private final String strEmpire = Language.getString("StrcInfoGUI.0"); //$NON-NLS-1$
  private final String[] columns = new String[] {
    Language.getString("StrcInfoGUI.12"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.13"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.14"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.15"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.16"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.17"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.18"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.19"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.20"), //$NON-NLS-1$
    Language.getString("StrcInfoGUI.21") //$NON-NLS-1$
  };
  private final String strNA = Language.getString("StrcInfoGUI.6"); //$NON-NLS-1$
  private final String strYes = Language.getString("StrcInfoGUI.7"); //$NON-NLS-1$
  private final String strNo = Language.getString("StrcInfoGUI.8"); //$NON-NLS-1$
  private final String strPop = Language.getString("StrcInfoGUI.9"); //$NON-NLS-1$
  private final String strPopMul = Language.getString("StrcInfoGUI.10"); //$NON-NLS-1$
  private final String strBldNone = Language.getString("StrcInfoGUI.22"); //$NON-NLS-1$
  private final String strNone = Language.getString("StrcInfoGUI.23"); //$NON-NLS-1$

  // read
  private Stbof STBOF;
  private Trek EXE;
  private SavGame GAME;
  // stbof.res
  private Edifice EDIFICE;
  private Environ ENVIRON;
  private AIBldReq AIBLDREQ;
  private RaceRst RACE;
  private Lexicon LEX;
  private Planboni PLANBONI;
  // trek.exe
  private PlanetBonusTypes PLANBONITYPES;
  // game.sav
  private AlienInfo ALIENINFO;
  private SystInfo SYSTEMS;
  private TechInfo TECHINFO;

  // edited
  private StrcInfo STRUCT;

  // ui components
  private DropdownSpinner spiSystems = new DropdownSpinner();
  private JPanel pnlTable = new JPanel();
  private JLabel lblDilithium = new JLabel();
  private JLabel lblPlanetTypes = new JLabel();
  private JLabel lblPopulation = new JLabel();
  private JLabel lblEnOutput = new JLabel();
  private JLabel lblEnConsumed = new JLabel();
  private JButton btnSystemView = new JButton(Language.getString("StrcInfoGUI.24")); //$NON-NLS-1$
  private JButton btnMapView = new JButton(Language.getString("StrcInfoGUI.25")); //$NON-NLS-1$
  private JComboBox<ID> cmbFilterSystems;
  private JScrollPane scrHeader;
  private JScrollPane scrTable;
  private StrcHeaderLine header;

  // data
  private final Font def;
  private final Font tableDef;
  private short starID = -21;
  private ArrayList<StrcLine> tableLines = new ArrayList<StrcLine>();

  public StrcInfoGUI(SavGame game, Stbof stbof, Trek trek, int systemID) throws IOException {
    this(game, stbof, trek);
    spiSystems.setValue(new ID("", systemID)); //$NON-NLS-1$
  }

  /**
   * Constructor.
   * @throws IOException
   */
  public StrcInfoGUI(SavGame sav, Stbof stbof, Trek trek) throws IOException {
    def = UE.SETTINGS.getDefaultFont();
    tableDef = UE.SETTINGS.getDefaultFont();

    GAME = sav;
    STBOF = stbof;
    EXE = trek;

    RACE = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);
    LEX = (Lexicon) STBOF.getInternalFile(CStbofFiles.LexiconDic, true);
    EDIFICE = (Edifice) STBOF.getInternalFile(CStbofFiles.EdificeBst, true);
    AIBLDREQ = (AIBldReq) STBOF.getInternalFile(CStbofFiles.AIBldReqBin, true);
    ENVIRON = (Environ) STBOF.getInternalFile(CStbofFiles.EnvironEst, true);
    PLANBONI = (Planboni) STBOF.getInternalFile(CStbofFiles.PlanBoniBin, true);
    PLANBONITYPES = (PlanetBonusTypes) EXE.getInternalFile(CTrekSegments.PlanetBonusTypes, true);

    STRUCT = (StrcInfo) sav.getInternalFile(CSavFiles.StrcInfo, true);
    SYSTEMS = (SystInfo) sav.getInternalFile(CSavFiles.SystInfo, true);
    ALIENINFO = (AlienInfo) sav.getInternalFile(CSavFiles.AlienInfo, true);
    TECHINFO = (TechInfo) sav.getInternalFile(CSavFiles.TechInfo, true);

    Font defSys = UE.SETTINGS.getDefaultBigFont();
    spiSystems.setFont(defSys);

    spiSystems.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        fillSystem();
      }
    });

    pnlTable.setLayout(new GridBagLayout());

    lblDilithium.setFont(def);
    lblPlanetTypes.setFont(def);
    lblPopulation.setFont(def);
    lblEnOutput.setFont(def);
    lblEnConsumed.setFont(def);

    btnSystemView.setFont(def);
    btnSystemView.addActionListener(new CBActionListener(evt -> {
      ID item = spiSystems.getValue();
      UE.WINDOW.showPanel(new SystemsInfoGUI(GAME, STBOF, item.ID));
    }));

    btnMapView.setFont(def);
    btnMapView.addActionListener(new CBActionListener(evt -> {
      ID item = spiSystems.getValue();
      int sectorIndex = SYSTEMS.getSystem(item.ID).getSectorIndex();
      UE.WINDOW.showPanel(new MapGUI(GAME, STBOF, EXE, sectorIndex));
    }));

    cmbFilterSystems = new JComboBox<ID>();
    cmbFilterSystems.setFont(defSys);
    cmbFilterSystems.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED
            && cmbFilterSystems.isEnabled()) {
          try {
            finalWarning();
          } catch (Exception ex) {
            // really bad stuff happening - bail
            Dialogs.displayError(ex);
            return;
          }

          int filter = cmbFilterSystems.getSelectedIndex() - 1;

          ID[] sysIds = SYSTEMS.getSystemIDs(filter, true);

          if (sysIds.length == 0) {
            spiSystems.setEnabled(false);
            spiSystems.setItems(new ID[]{new ID(strNA, 0)});

            pnlTable.removeAll();
            tableLines.clear();

            // general info
            lblDilithium.setText(strNA);
            lblPlanetTypes.setText(strNA);
            lblPopulation.setText(strNA);
            lblEnOutput.setText(strNA);
            lblEnConsumed.setText(strNA);
            return;
          }

          Arrays.sort(sysIds, new Comparator<ID>() {
            @Override
            public int compare(ID o1, ID o2) {
              return o1.toString().compareToIgnoreCase(o2.toString());
            }
          });

          // include race name
          for (ID sysId : sysIds) {
            val system = SYSTEMS.getSystem(sysId.ID);
            int ctrlRace = system.getControllingRace();

            if (ctrlRace >= 0 && ctrlRace < RACE.getNumberOfEntries()) {
              sysId.NAME = "%1 (%2)".replace("%1", sysId.toString()) //$NON-NLS-1$ //$NON-NLS-2$
                .replace("%2", RACE.getName(ctrlRace)); //$NON-NLS-1$
            }
          }

          spiSystems.setItems(sysIds);
        }
      }
    });

    //lay out
    JPanel top = new JPanel(new BorderLayout());

    JPanel pnl = new JPanel();
    pnl.setLayout(new BoxLayout(pnl, BoxLayout.X_AXIS));
    pnl.add(cmbFilterSystems);
    pnl.add(Box.createHorizontalStrut(5));
    pnl.add(spiSystems);

    top.add(pnl, BorderLayout.NORTH);

    pnl = new JPanel();
    pnl.setLayout(new BoxLayout(pnl, BoxLayout.Y_AXIS));

    addLabel(" ", def, pnl); //$NON-NLS-1$
    addLabel(Language.getString("StrcInfoGUI.1"), def, pnl); //$NON-NLS-1$
    addLabel(Language.getString("StrcInfoGUI.2"), def, pnl); //$NON-NLS-1$
    addLabel(" ", def, pnl); //$NON-NLS-1$
    addLabel(Language.getString("StrcInfoGUI.3"), def, pnl); //$NON-NLS-1$
    addLabel(Language.getString("StrcInfoGUI.4"), def, pnl); //$NON-NLS-1$
    addLabel(Language.getString("StrcInfoGUI.5"), def, pnl); //$NON-NLS-1$

    top.add(pnl, BorderLayout.WEST);

    pnl = new JPanel(new GridLayout(0, 1, 0, 0));

    addLabel(" ", def, pnl); //$NON-NLS-1$
    lblDilithium.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    pnl.add(lblDilithium);
    lblPlanetTypes.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    pnl.add(lblPlanetTypes);
    addLabel(" ", def, pnl); //$NON-NLS-1$
    lblPopulation.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    pnl.add(lblPopulation);
    lblEnOutput.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    pnl.add(lblEnOutput);
    lblEnConsumed.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    pnl.add(lblEnConsumed);

    pnl.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    top.add(pnl, BorderLayout.CENTER);

    pnl = new JPanel(new GridBagLayout());
    val c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets.top = 15;
    c.gridx = 0;
    c.gridy = 0;
    pnl.add(btnSystemView, c);
    c.insets.top = 2;
    c.gridy++;
    pnl.add(btnMapView, c);
    c.gridy++;
    c.weighty = 1;
    pnl.add(Box.createVerticalGlue(), c);
    pnl.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));

    top.add(pnl, BorderLayout.EAST);

    setLayout(new BorderLayout());

    top.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));

    add(top, BorderLayout.NORTH);

    pnl = new JPanel(new BorderLayout());
    pnl.add(pnlTable, BorderLayout.NORTH);

    scrTable = new JScrollPane(pnl);
    scrTable.setPreferredSize(new Dimension(TABLE_WIDTH, TABLE_HEIGHT));
    scrTable.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

    header = new StrcHeaderLine();
    scrHeader = new JScrollPane(header);
    scrHeader.setPreferredSize(new Dimension(TABLE_WIDTH, HEADER_HEIGHT));
    scrHeader.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    scrHeader.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    scrHeader.getVerticalScrollBar().setUI(new InvisibleScrollBarUI());
    scrHeader.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.GRAY));

    scrTable.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      @Override
      public void adjustmentValueChanged(AdjustmentEvent e) {
        Rectangle rect = scrHeader.getViewport().getViewRect();

        rect.x = scrTable.getViewport().getViewRect().x - rect.x;

        scrHeader.getViewport().scrollRectToVisible(rect);
      }
    });

    pnl = new JPanel(new BorderLayout());
    pnl.add(scrHeader, BorderLayout.NORTH);
    pnl.add(scrTable, BorderLayout.CENTER);
    pnl.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
    add(pnl, BorderLayout.CENTER);

    // init
    String filterName = LexHelper.mapSystemStatusReq(SystemStatusReq.None);
    cmbFilterSystems.addItem(new ID(filterName, SystemFilter.Any));

    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      try {
        cmbFilterSystems.addItem(new ID(RACE.getName(emp), emp));
      } catch (Exception ex) {
        filterName = strEmpire.replace("%1", Integer.toString(emp)); //$NON-NLS-1$
        cmbFilterSystems.addItem(new ID(filterName, emp));
      }
    }

    filterName = LexHelper.mapAlienRelationship(AlienRelationship.Neutral);
    cmbFilterSystems.addItem(new ID(filterName, SystemFilter.Neutral));

    filterName = LexHelper.mapAlienRelationship(AlienRelationship.Membership);
    cmbFilterSystems.addItem(new ID(filterName, SystemFilter.Membered));

    filterName = LexHelper.mapMinorRaceStatus(RaceStatus.Subjugated);
    cmbFilterSystems.addItem(new ID(filterName, SystemFilter.Subjugated));
  }

  protected void fillSystem() {
    ID selected = spiSystems.getValue();
    if (selected == null)
      return;

    pnlTable.removeAll();
    tableLines.clear();

    starID = (short) spiSystems.getValue().ID;
    val system = SYSTEMS.getSystem(starID);

    // general info
    lblDilithium.setText(system.hasDilithium() ? strYes : strNo);

    int i = 0;
    String str = ""; //$NON-NLS-1$

    for (ID plan : ENVIRON.planetGfx()) {
      for (int p = 0; p < system.getNumPlanets(); ++p) {
        if (system.getPlanet(p).getPlanetType() == i) {
          str = str.length() == 0 ? plan.NAME : str + ", " + plan; //$NON-NLS-1$
          break;
        }
      }

      i++;
    }

    lblPlanetTypes.setText(str);

    int pop = system.getPopulation();

    if (pop == 0) {
      lblPopulation.setText(strNone);
    } else {
      int residentRace = system.getResidentRace();
      int ctrlRace = system.getControllingRace();

      String raceName = RACE.getName(residentRace);
      if (residentRace != ctrlRace && ctrlRace >= 0)
        raceName += ", " + RACE.getName(ctrlRace); //$NON-NLS-1$

      String txt = pop > 1 ? strPopMul : strPop;
      txt = txt.replace("%1", Integer.toString(pop)) //$NON-NLS-1$
        .replace("%2", raceName); //$NON-NLS-1$
      lblPopulation.setText(txt);
    }

    updateEnergyUsageStrings(starID);

    // buildings
    short[] bids = STRUCT.getBuildings(starID);

    val c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1;
    c.gridx = 0;
    c.gridy = 0;

    for (short bid : bids) {
      StrcLine line = new StrcLine(starID, bid);
      tableLines.add(line);
      line.addTo(pnlTable, c);
      c.gridy++;
    }

    // add extra line
    StrcLine line = new StrcLine(starID, (short) -1);
    tableLines.add(line);
    line.addTo(pnlTable, c);

    setVisible(false);
    setVisible(true);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Strcinfo;
  }

  @Override
  public void validateTree() {
    // To validate and determine the table width,
    // the panel must already be added to the main window.
    // Only thereafter the header width can be updated.
    super.validateTree();
    header.updateSizes();
  }

  private void updateEnergyUsageStrings(short starID) {
    val system = SYSTEMS.getSystem(starID);

    // get energy bonus
    int en_boni = 0;
    int boni;
    int btype;

    for (int p = 0; p < system.getNumPlanets(); p++) {
      val planet = system.getPlanet(p);
      btype = PLANBONITYPES.getBonusType(planet.getPlanetType());

      if (btype == PlanetBonusTypes.energy) {
        boni = PLANBONI.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());

        if (boni != 0) {
          boni = boni * 5;
          en_boni += boni;
        }
      }
    }

    // get energy output
    short[] bids = STRUCT.getBuildings(starID);
    int count = system.getPop2Energy() / 10;
    int output = 0;
    int total = 0;
    int en_cost = 0;

    for (short bid : bids) {
      try {
        Building bld = EDIFICE.building(bid);
        btype = bld.getBonusType();

        if (bld.isMainBuilding()) {
          if (btype == 2 || btype == 38) {
            if (count > 0) {
              output = Math.min(count, STRUCT.getCount(starID, bid));
              count -= STRUCT.getCount(starID, bid);

              int race = Math.min(system.getControllingRace(), NUM_EMPIRES);
              output = output * bld.getBonusValue(race);
              total += output;
            }
          }
        } else {
          output = STRUCT.getCount(starID, bid);

          if (bld.getEnergyCost() > 0) {
            output = Math.min(output, STRUCT.getPoweredOnCount(starID, bid));

            en_cost += output * bld.getEnergyCost();
          }

          if (btype == 2 || btype == 38) {
            int raceId = Math.min(system.getControllingRace(), NUM_EMPIRES);
            output = output * bld.getBonusValue(raceId);
            total += output;
          } else if (btype == 3) {
            int raceId = Math.min(system.getControllingRace(), NUM_EMPIRES);
            output = output * bld.getBonusValue(raceId);
            en_boni += output;
          }
        }
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }
    }

    String str = Integer.toString((int) Math.floor(total * (100 + en_boni) / 100.0));

    if (en_boni > 0) {
      str = str + " ( " + total + " +" + en_boni + "% )"; //$NON-NLS-1$ //$NON-NLS-1$ //$NON-NLS-1$
    }

    lblEnOutput.setText(str);
    lblEnConsumed.setText(Integer.toString(en_cost));
  }

  @Override
  public boolean hasChanges() {
    return STRUCT.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    GAME.checkChanged(STRUCT, changes);
  }

  @Override
  public void reload() throws IOException {
    STRUCT = (StrcInfo) GAME.getInternalFile(CSavFiles.StrcInfo, true);
    fillSystem();
  }

  //final warning
  @Override
  public void finalWarning() {
  }

  private void addLabel(String str, Font def, JPanel pnl) {
    JLabel lbl = new JLabel(str);
    lbl.setFont(def);
    lbl.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    pnl.add(lbl);
  }

  private class StrcHeaderLine extends JPanel implements ActionListener {

    private ArrayList<JButton> buttons;

    public StrcHeaderLine() {
      setLayout(new GridBagLayout());

      buttons = new ArrayList<JButton>();
      val c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.weightx = 1;
      c.gridx = 0;
      c.gridy = 0;

      add(setupButton(columns[0], def), c);
      c.gridx++;
      add(setupButton(columns[1], def), c);
      c.gridx++;
      add(setupButton(columns[2], def), c);
      c.gridx++;
      add(setupButton(columns[3], def), c);
      c.gridx++;
      add(setupButton(columns[4], def), c);
      c.gridx++;
      add(setupButton(columns[5], def), c);
      c.gridx++;
      add(setupButton(columns[6], def), c);
      c.gridx++;
      add(setupButton(columns[7], def), c);
      c.gridx++;
      add(setupButton(columns[8], def), c);
      c.gridx++;
      add(setupButton(columns[9], def), c);
    }

    private JButton setupButton(String str, Font def) {
      JButton lbl = new JButton(str);
      lbl.setFont(tableDef);
      lbl.setHorizontalAlignment(JLabel.CENTER);
      lbl.setBorder(BorderFactory.createLineBorder(Color.GRAY));
      lbl.addActionListener(this);
      buttons.add(lbl);
      return lbl;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void actionPerformed(ActionEvent e) {
      Object btn = e.getSource();

      if (tableLines.size() < 3) {
        return;
      }

      Comparable<Object> o1, o2;

      for (int i = 0; i < buttons.size(); i++) {
        if (btn == buttons.get(i)) {
          final int index = i;

          o1 = (Comparable<Object>) tableLines.get(0).getColumnValue(i);
          o2 = (Comparable<Object>) tableLines.get(tableLines.size() - 2).getColumnValue(i);

          // sort
          if (o1.compareTo(o2) >= 0) {
            Collections.sort(tableLines, new Comparator<StrcLine>() {
              @Override
              public int compare(StrcLine o1, StrcLine o2) {
                if (o1.getBuildingID() < 0) {
                  return 1000;
                } else if (o2.getBuildingID() < 0) {
                  return -1000;
                } else {
                  Comparable<Object> comparable = (Comparable<Object>) o1.getColumnValue(index);
                  return comparable.compareTo(o2.getColumnValue(index));
                }
              }
            });
          } else {
            Collections.sort(tableLines, new Comparator<StrcLine>() {
              @Override
              public int compare(StrcLine o1, StrcLine o2) {
                if (o1.getBuildingID() < 0) {
                  return 1000;
                } else if (o2.getBuildingID() < 0) {
                  return -1000;
                } else {
                  Comparable<Object> comparable = (Comparable<Object>) o2.getColumnValue(index);
                  return comparable.compareTo(o1.getColumnValue(index));
                }
              }
            });
          }

          pnlTable.removeAll();

          val c = new GridBagConstraints();
          c.fill = GridBagConstraints.BOTH;
          c.weightx = 1;
          c.gridx = 0;
          c.gridy = 0;

          for (; c.gridy < tableLines.size(); c.gridy++) {
            StrcLine line = tableLines.get(c.gridy);
            line.addTo(pnlTable, c);
          }

          pnlTable.setVisible(false);
          pnlTable.setVisible(true);

          break;
        }
      }
    }

    public void updateSizes() {
      val lm = (GridBagLayout) pnlTable.getLayout();
      int[][] dims = lm.getLayoutDimensions();
      if (dims.length <= 0 || dims[0].length <= 0)
        return;

      // update column widths
      for (int i = 0; i < dims[0].length; ++i) {
        val btn = buttons.get(i);
        val dim = new Dimension(dims[0][i], HEADER_HEIGHT);
        // minimum width needs to be set to avoid reduction and flickering on resize
        btn.setMinimumSize(dim);
        btn.setPreferredSize(dim);
        btn.setMaximumSize(dim);
        btn.setSize(dim);
      }

      // update scaled table width
      Dimension pref = pnlTable.getPreferredSize();
      pref.height = HEADER_HEIGHT;
      setMinimumSize(pref);
      setPreferredSize(pref);
      setMaximumSize(pref);
      setSize(pref);
    }
  }

  private class StrcLine {

    public JComboBox<ID> cmbBuildings;
    public JTextField txtLevel;
    public JTextField txtEnReq;
    public JTextField txtAIEnReq;
    public JTextField txtCanBeBuilt;
    public JTextField txtSysReq;
    public JTextField txtPolReq;
    public JTextField txtLimit;
    public JSpinner spiCount;
    public JSpinner spiPowered;

    private short starID, buildingID;

    public StrcLine(short starID, short buildingID) {
      this.starID = starID;
      this.buildingID = buildingID;

      // components
      cmbBuildings = new JComboBox<ID>();
      cmbBuildings.removeAllItems();

      for (ID bid : EDIFICE.getShortIDs((byte) 0, 40)) {
        bid.NAME = bid.ID + ": " + bid.toString(); //$NON-NLS-1$
        cmbBuildings.addItem(bid);
      }

      cmbBuildings.addItem(new ID(strBldNone, -1));

      cmbBuildings.setSelectedItem(new ID("", buildingID)); //$NON-NLS-1$
      cmbBuildings.setFont(tableDef);

      cmbBuildings.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (e.getStateChange() == ItemEvent.DESELECTED) {
            if (((ID)e.getItem()).ID < 0) {
              try {
                // add another empty line
                StrcLine line = new StrcLine(getStarID(), (short) -1);
                tableLines.add(line);
                val gbl = (GridBagLayout) pnlTable.getLayout();
                int columns = gbl.getLayoutDimensions()[1].length;

                val c = new GridBagConstraints();
                c.fill = GridBagConstraints.BOTH;
                c.weightx = 1;
                c.gridy = columns;
                line.addTo(pnlTable, c);

                pnlTable.setVisible(false);
                pnlTable.setVisible(true);
              } catch (Exception ex) {
                Dialogs.displayError(ex);
              }
            }
          } else if (e.getStateChange() == ItemEvent.SELECTED) {
            short bid = (short) ((ID)e.getItem()).ID;

            if (bid >= 0) {
              if (getBuildingID() >= 0) {
                STRUCT.changeBuildingID(getStarID(), getBuildingID(), bid);
              }

              // remove others
              for (int i = 0; i < tableLines.size(); i++) {
                StrcLine line = tableLines.get(i);

                if (line.getBuildingID() == bid) {
                  tableLines.remove(i);
                  line.remFrom(pnlTable);
                  i--;
                }
              }

              pnlTable.setVisible(false);
              pnlTable.setVisible(true);

              setBuildingID(bid);

              updateFields();
              updateEnergyUsageStrings(getStarID());
            } else {
              bid = getBuildingID();
              STRUCT.setCount(getStarID(), bid, (short) 0);

              for (int i = 0; i < tableLines.size(); i++) {
                StrcLine line = tableLines.get(i);

                if (line.getBuildingID() == bid) {
                  tableLines.remove(i);
                  line.remFrom(pnlTable);
                  i--;
                }
              }

              pnlTable.setVisible(false);
              pnlTable.setVisible(true);
            }
          }
        }
      });

      spiCount = new JSpinner(new SpinnerNumberModel(0, 0, Short.MAX_VALUE, 1));
      spiCount.setFont(tableDef);
      spiCount.addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
          if (!spiCount.isEnabled())
            return;

          short count = ((Integer) spiCount.getValue()).shortValue();
          STRUCT.setCount(getStarID(), getBuildingID(), count);

          updateFields();
          updateEnergyUsageStrings(getStarID());
        }
      });

      spiPowered = new JSpinner(new SpinnerNumberModel(0, 0, Short.MAX_VALUE, 1));
      spiPowered.setFont(tableDef);
      spiPowered.addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
          if (!spiPowered.isEnabled())
            return;

          short count = ((Integer) spiPowered.getValue()).shortValue();
          STRUCT.setPoweredOnCount(getStarID(), getBuildingID(), count);

          updateFields();
          updateEnergyUsageStrings(getStarID());
        }
      });

      Insets lblMargin = new Insets(2, 4, 2, 4);
      txtLevel = new JTextField();
      txtLevel.setEditable(false);
      txtLevel.setHorizontalAlignment(JTextField.RIGHT);
      txtLevel.setFont(def);
      txtLevel.setMargin(lblMargin);

      txtEnReq = new JTextField();
      txtEnReq.setEditable(false);
      txtEnReq.setHorizontalAlignment(JTextField.RIGHT);
      txtEnReq.setFont(def);
      txtEnReq.setMargin(lblMargin);

      txtAIEnReq = new JTextField();
      txtAIEnReq.setEditable(false);
      txtAIEnReq.setHorizontalAlignment(JTextField.RIGHT);
      txtAIEnReq.setFont(def);
      txtAIEnReq.setMargin(lblMargin);

      txtCanBeBuilt = new JTextField();
      txtCanBeBuilt.setEditable(false);
      txtCanBeBuilt.setHorizontalAlignment(JTextField.CENTER);
      txtCanBeBuilt.setFont(tableDef);
      txtCanBeBuilt.setMargin(lblMargin);

      txtSysReq = new JTextField();
      txtSysReq.setEditable(false);
      txtSysReq.setHorizontalAlignment(JTextField.CENTER);
      txtSysReq.setFont(tableDef);
      txtSysReq.setMargin(lblMargin);

      txtPolReq = new JTextField();
      txtPolReq.setEditable(false);
      txtPolReq.setHorizontalAlignment(JTextField.CENTER);
      txtPolReq.setFont(tableDef);
      txtPolReq.setMargin(lblMargin);

      txtLimit = new JTextField();
      txtLimit.setEditable(false);
      txtLimit.setHorizontalAlignment(JTextField.CENTER);
      txtLimit.setFont(tableDef);
      txtLimit.setMargin(lblMargin);

      updateFields();
    }

    protected void remFrom(JPanel pnlTable) {
      pnlTable.remove(cmbBuildings);
      pnlTable.remove(spiCount);
      pnlTable.remove(spiPowered);
      pnlTable.remove(txtEnReq);
      pnlTable.remove(txtAIEnReq);
      pnlTable.remove(txtLevel);
      pnlTable.remove(txtCanBeBuilt);
      pnlTable.remove(txtSysReq);
      pnlTable.remove(txtPolReq);
      pnlTable.remove(txtLimit);
    }

    public void addTo(JPanel pnlTable, GridBagConstraints c) {
      c.gridx = 0;
      pnlTable.add(cmbBuildings, c);
      c.gridx++;
      pnlTable.add(spiCount, c);
      c.gridx++;
      pnlTable.add(spiPowered, c);
      c.gridx++;
      pnlTable.add(txtEnReq, c);
      c.gridx++;
      pnlTable.add(txtAIEnReq, c);
      c.gridx++;
      pnlTable.add(txtLevel, c);
      c.gridx++;
      pnlTable.add(txtCanBeBuilt, c);
      c.gridx++;
      pnlTable.add(txtSysReq, c);
      c.gridx++;
      pnlTable.add(txtPolReq, c);
      c.gridx++;
      pnlTable.add(txtLimit, c);
    }

    public short getStarID() {
      return starID;
    }

    public short getBuildingID() {
      return buildingID;
    }

    public void setBuildingID(short id) {
      buildingID = id;
    }

    public void updateFields() {
      val system = SYSTEMS.getSystem(starID);

      if (buildingID >= 0) {
        Building bld = EDIFICE.building(buildingID);
        txtLevel.setText(Integer.toString(bld.getTechLevel()));
        int encost = bld.getEnergyCost();
        txtEnReq.setText(Integer.toString(encost));

        val bldReq = AIBLDREQ.getReq(buildingID);
        if (bldReq.isPresent())
          encost = bldReq.get().getEnergyReq();

        txtAIEnReq.setText(Integer.toString(encost));

        int count = STRUCT.getCount(starID, buildingID);

        spiCount.setEnabled(false);
        spiCount.setValue(count);

        spiPowered.setEnabled(false);
        spiPowered.setModel(new SpinnerNumberModel(
            Math.min(STRUCT.getPoweredOnCount(starID, buildingID), count),
            0, count, 1));

        int[] req = new int[]{
          LexDataIdx.System.Restrictions.ArcticPlanet,
          LexDataIdx.System.Restrictions.BarrenPlanet,
          LexDataIdx.System.Restrictions.DesertPlanet,
          LexDataIdx.System.PlanetTypes.GasGiant,
          LexDataIdx.System.Restrictions.JunglePlanet,
          LexDataIdx.System.Restrictions.OceanicPlanet,
          LexDataIdx.System.Restrictions.TerranPlanet,
          LexDataIdx.System.Restrictions.VolcanicPlanet,
          LexDataIdx.Shared.None,
          LexDataIdx.System.PlanetTypes.Asteroid,
          LexDataIdx.System.Restrictions.AsteroidBeltDilithium,
          LexDataIdx.System.Restrictions.Dilithium,
          LexDataIdx.Galaxy.StellarObjects.Wormhole,
          LexDataIdx.Galaxy.StellarObjects.RadioPulsar,
          LexDataIdx.Galaxy.StellarObjects.XRayPulsar,
          LexDataIdx.Shared.Invalid
        };

        count = bld.getSystemReq();
        if (count < 0 || count >= req.length) {
          count = req.length - 1;
        }

        txtSysReq.setText(req[count] != LexDataIdx.Shared.None ? LEX.getEntry(req[count]) : "-"); //$NON-NLS-1$
        req = new int[]{
          LexDataIdx.System.Restrictions.HomeSystem,
          LexDataIdx.System.Restrictions.NativeMemberSystem,
          LexDataIdx.System.Restrictions.NonNativeMemberSystem,
          LexDataIdx.System.Restrictions.SubjugatedSystem,
          LexDataIdx.System.Restrictions.AffiliatedSystem,
          LexDataIdx.System.Restrictions.IndependentMinorSystem,
          LexDataIdx.System.Restrictions.ConqueredHomeSystem,
          LexDataIdx.SpaceCraft.CategoryDescriptions.Invalid,
          LexDataIdx.SpaceCraft.CategoryDescriptions.Invalid,
          LexDataIdx.Shared.None,
          LexDataIdx.System.Restrictions.RebelSystem,
          LexDataIdx.System.Restrictions.EmptySystem,
          LexDataIdx.SpaceCraft.CategoryDescriptions.Invalid
        };

        count = bld.getPoliticReq();
        if (count < 0 || count >= req.length) {
          count = req.length - 1;
        }

        txtPolReq.setText(req[count] != LexDataIdx.Shared.None ? LEX.getEntry(req[count]) : "-"); //$NON-NLS-1$
        req = new int[]{
          LexDataIdx.Shared.None,
          LexDataIdx.System.Restrictions.OnePerSystem,
          LexDataIdx.System.Restrictions.OnePerEmpire,
          LexDataIdx.SpaceCraft.CategoryDescriptions.Invalid
        };

        count = bld.getBuildLimit();
        if (count < 0 || count >= req.length) {
          count = req.length - 1;
        }

        txtLimit.setText(req[count] != LexDataIdx.Shared.None ? LEX.getEntry(req[count]) : "-"); //$NON-NLS-1$

        final int ctrlRace = system.getControllingRace();
        final int residentRace = system.getResidentRace();

        // check race req
        boolean canBeBuilt = bld.canBeBuiltBy(ctrlRace, residentRace);

        // check sys req
        count = bld.getSystemReq();

        if (count <= 7) {
          for (int p = 0; p < system.getNumPlanets(); ++p) {
            if (system.getPlanet(p).getPlanetType() == count) {
              count = 8;
              break;
            }
          }

          canBeBuilt = canBeBuilt && (count == 8);
        } else if (count == 11) {
          canBeBuilt = canBeBuilt && system.hasDilithium();
          count = (system.hasDilithium()) ? 8 : 0;
        }

        if (count == 8) {
          txtSysReq.setBackground(cmbBuildings.getBackground());
        } else {
          txtSysReq.setBackground(Color.PINK);
        }

        // check pol req
        int polReq = bld.getPoliticReq();
        boolean systemStatusMatched = true;

        if (polReq == SystemStatusReq.ConqueredHome) {
          systemStatusMatched = residentRace == ALIENINFO.getRace(starID) && ctrlRace != residentRace;
        } else if (polReq == SystemStatusReq.HomeSystem) {
          systemStatusMatched = residentRace == ALIENINFO.getRace(starID) && ctrlRace == residentRace;
        } else if (polReq == SystemStatusReq.NativeMember) {
          systemStatusMatched = ctrlRace == residentRace;
        } else if (polReq == SystemStatusReq.NonNativeMember) {
          systemStatusMatched = ALIENINFO.getRaceStatus(residentRace) == AlienInfo.RaceStatus.Member
            && ctrlRace != residentRace;
        } else if (polReq == SystemStatusReq.Subjugated) {
          systemStatusMatched = ALIENINFO.getRaceStatus(residentRace) == AlienInfo.RaceStatus.Subjugated;
        }

        if (systemStatusMatched) {
          txtPolReq.setBackground(cmbBuildings.getBackground());
        } else {
          txtPolReq.setBackground(Color.PINK);
        }

        canBeBuilt = canBeBuilt && systemStatusMatched;

        // check num req
        int freq = bld.getBuildLimit();
        boolean systemLimitsMatched = true;

        if (freq == CountReq.OnePerSystem) {
          systemLimitsMatched = STRUCT.getCount(starID, buildingID) <= 1;
        } else if (freq == CountReq.OnePerEmpire) {
          ArrayList<Short> stars = STRUCT.getStarsWithBuilding(buildingID);
          int num = 0;

          for (int i = 0; i < stars.size(); i++) {
            val sys2 = SYSTEMS.getSystem(stars.get(i));
            if (sys2.getControllingRace() == ctrlRace) {
              num++;

              if (num > 1) {
                systemLimitsMatched = false;
                break;
              }
            }
          }
        }

        if (systemLimitsMatched) {
          txtLimit.setBackground(cmbBuildings.getBackground());
        } else {
          txtLimit.setBackground(Color.PINK);
        }

        canBeBuilt = canBeBuilt && systemLimitsMatched;

        // check level
        int techReq = bld.getTechField();
        boolean techLvlReached = FunctionTools.defaultIfThrown(() -> bld.getTechLevel()
          <= TECHINFO.getTechLevel((short) ctrlRace, techReq), false);

        txtLevel.setBackground(techLvlReached ? cmbBuildings.getBackground() : Color.PINK);
        canBeBuilt = canBeBuilt && techLvlReached;

        if (canBeBuilt) {
          txtCanBeBuilt.setText(strYes);
          txtCanBeBuilt.setBackground(Color.CYAN);
        } else {
          txtCanBeBuilt.setText(strNo);
          txtCanBeBuilt.setBackground(Color.PINK);
        }

        txtLevel.setEnabled(true);
        txtEnReq.setEnabled(true);
        txtAIEnReq.setEnabled(true);
        spiCount.setEnabled(true);
        txtSysReq.setEnabled(true);
        txtPolReq.setEnabled(true);
        txtLimit.setEnabled(true);
        txtCanBeBuilt.setEnabled(true);
        spiPowered.setEnabled(bld.getEnergyCost() > 0 && !bld.isMainBuilding());
      } else {
        // set empty label size to enforce a minimum column width
        Dimension dim = txtLevel.getPreferredSize();
        dim.width = MIN_LABEL_WIDTH;
        txtLevel.setText(strNA);
        txtLevel.setEnabled(false);
        txtLevel.setPreferredSize(dim);
        txtEnReq.setText(strNA);
        txtEnReq.setPreferredSize(dim);
        txtEnReq.setEnabled(false);
        txtAIEnReq.setText(strNA);
        txtAIEnReq.setPreferredSize(dim);
        txtAIEnReq.setEnabled(false);
        spiCount.setValue(0);
        spiCount.setEnabled(false);
        spiPowered.setModel(new SpinnerNumberModel(0, 0, Short.MAX_VALUE, 1));
        spiPowered.setEnabled(false);
        txtSysReq.setText(strNA);
        txtSysReq.setPreferredSize(dim);
        txtSysReq.setEnabled(false);
        txtPolReq.setText(strNA);
        txtPolReq.setPreferredSize(dim);
        txtPolReq.setEnabled(false);
        txtLimit.setText(strNA);
        txtLimit.setPreferredSize(dim);
        txtLimit.setEnabled(false);
        txtCanBeBuilt.setText(strNA);
        txtCanBeBuilt.setPreferredSize(dim);
        txtCanBeBuilt.setEnabled(false);
      }

      if (system.getPopulation() <= 0) {
        cmbBuildings.setEnabled(false);
      } else {
        cmbBuildings.setEnabled(true);
      }
    }

    public Object getColumnValue(int index) {
      switch (index) {
        case 0: {
          return ((ID)cmbBuildings.getSelectedItem()).ID;
        }
        case 1: {
          return spiCount.getValue();
        }
        case 2: {
          return spiPowered.getValue();
        }
        case 3: {
          return Integer.parseInt(txtEnReq.getText());
        }
        case 4: {
          return Integer.parseInt(txtAIEnReq.getText());
        }
        case 5: {
          return Integer.parseInt(txtLevel.getText());
        }
        case 6: {
          return txtCanBeBuilt.getText();
        }
        case 7: {
          return txtSysReq.getText();
        }
        case 8: {
          return txtPolReq.getText();
        }
        default: {
          return txtLimit.getText();
        }
      }
    }
  }
}
