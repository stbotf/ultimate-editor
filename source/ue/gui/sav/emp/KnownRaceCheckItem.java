package ue.gui.sav.emp;

import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.files.emp.EmpsInfo;
import ue.gui.util.style.CheckItem;

public class KnownRaceCheckItem extends CheckItem {

  private EmpsInfo empsInfo;
  private int empire;
  private int raceId;

  public KnownRaceCheckItem(RaceRst raceRst, EmpsInfo empsInfo, int empire, int raceId, boolean editable) {
    super(raceRst.getName(raceId), empsInfo.isRaceKnown(empire, raceId), editable);

    this.empsInfo = empsInfo;
    this.empire = empire;
    this.raceId = raceId;
  }

  @Override
  public void setSelected(boolean sel) {
    if (isEditable()) {
      empsInfo.setRaceKnown(empire, raceId, sel);
      super.setSelected(sel);
    }
  }

  @Override
  public void toggleSelected() {
    if (isEditable()) {
      empsInfo.setRaceKnown(empire, raceId, !isSelected());
      super.toggleSelected();
    }
  }

  public void updateEmpire(int empire) {
    this.empire = empire;
    boolean known = empsInfo.isRaceKnown(empire, raceId);
    super.setSelected(known);
  }
}
