package ue.gui.sav.emp;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Vector;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.trek.Trek;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.SystInfo;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.sav.map.MapGUI;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.file.FileFilters;

/**
 * Used to edit systems info in a saved game.
 */
public class SystemsInfoGUI extends MainPanel {

  private final String strEDIT = Language.getString("SystemsInfoGUI.0"); //$NON-NLS-1$
  private final String strENTER_NEW_NAME = Language.getString("SystemsInfoGUI.1"); //$NON-NLS-1$

  // read
  private Stbof STBOF;
  private SavGame GAME;
  private Planspac SPACE;
  private Planboni BONI;
  private Environ ENVIRON;
  private RaceRst RACES;

  // edited
  private SystInfo SYSTEMS;

  // ui components
  private JComboBox<ID> jcbSYSTEM = new JComboBox<ID>();
  private JButton btnEDIT = new JButton(strEDIT);
  private JButton btnSTRUCT = new JButton(Language.getString("SystemsInfoGUI.68"));
  private JButton btnMapView = new JButton(Language.getString("SystemsInfoGUI.69"));
  private JButton btnLEFT = new JButton("<<"); //$NON-NLS-1$
  private JButton btnRIGHT = new JButton(">>"); //$NON-NLS-1$
  private CardLayout CARDS = new CardLayout();
  private JPanel panCARDS = new JPanel();
  private SystemPanel INFO;
  private SystemPlanet PLANETS;

  // data
  private int selectedSystem = -1;
  private boolean init = true;

  public SystemsInfoGUI(SavGame sav, Stbof stbof, int systemID) throws IOException {
    this(sav, stbof);
    jcbSYSTEM.setSelectedItem(new ID("", systemID));
  }

  /**
   * Constructor.
   * @throws IOException
   */
  public SystemsInfoGUI(SavGame sav, Stbof stbof) throws IOException {
    if (stbof == null)
      return;

    GAME = sav;
    STBOF = stbof;

    SYSTEMS = (SystInfo) GAME.getInternalFile(CSavFiles.SystInfo, true);
    SPACE = (Planspac) STBOF.getInternalFile(CStbofFiles.PlanSpacBin, true);
    BONI = (Planboni) STBOF.getInternalFile(CStbofFiles.PlanBoniBin, true);
    ENVIRON = (Environ) STBOF.getInternalFile(CStbofFiles.EnvironEst, true);
    RACES = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);

    //build
    INFO = new SystemPanel();
    PLANETS = new SystemPlanet();

    //set up
    panCARDS.setLayout(CARDS);
    panCARDS.add(INFO, "0"); //$NON-NLS-1$
    panCARDS.add(PLANETS, "1"); //$NON-NLS-1$
    Font def = UE.SETTINGS.getDefaultFont();
    jcbSYSTEM.setFont(def);
    btnEDIT.setFont(def);
    btnLEFT.setFont(def);
    btnRIGHT.setFont(def);
    Dimension dimo = new Dimension(30, 20);
    btnLEFT.setSize(dimo);
    btnRIGHT.setSize(dimo);

    //edit
    btnEDIT.addActionListener(new CBActionListener(evt -> {
      if (selectedSystem != -1) {
        val system = SYSTEMS.getSystem(selectedSystem);
        String n = JOptionPane.showInputDialog(strENTER_NEW_NAME, system.getName());

        if (n != null) {
          int ij = selectedSystem;
          system.setName(n);
          fillSystems();

          jcbSYSTEM.setSelectedItem(new ID("", ij));
        }
      }
    }));

    //combo
    jcbSYSTEM.addActionListener(new CBActionListener(evt -> {
      if (init)
        return;

      if (jcbSYSTEM.getSelectedIndex() >= 0) {
        finalWarning();
        selectedSystem = jcbSYSTEM.getSelectedItem().hashCode();
        INFO.init();
        PLANETS.init();
      }
    }));

    //left and right
    btnLEFT.addActionListener(new CBActionListener(evt -> {
      if (selectedSystem != -1) {
        finalWarning();
        INFO.init();
        CARDS.previous(panCARDS);
      }
    }));

    btnRIGHT.addActionListener(new CBActionListener(evt -> {
      if (selectedSystem != -1) {
        finalWarning();
        INFO.init();
        CARDS.next(panCARDS);
      }
    }));

    btnSTRUCT.addActionListener(new CBActionListener(evt -> {
      Trek trek = UE.FILES.trek();
      UE.WINDOW.showPanel(new StrcInfoGUI(GAME, STBOF, trek, selectedSystem));
    }));

    btnMapView.addActionListener(new CBActionListener(evt -> {
      Trek trek = UE.FILES.trek();
      val system = SYSTEMS.getSystem(selectedSystem);
      int sectorIndex = system.getSectorIndex();
      UE.WINDOW.showPanel(new MapGUI(GAME, STBOF, trek, sectorIndex));
    }));

    CARDS.first(panCARDS);
    fillSystems();

    //lay out
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.NONE;
    c.anchor = GridBagConstraints.CENTER;

    //up
    JPanel pnlSYSTEM = new JPanel(new GridBagLayout());
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    pnlSYSTEM.add(jcbSYSTEM, c);
    c.gridx = 1;
    pnlSYSTEM.add(btnEDIT, c);
    c.gridx = 2;
    pnlSYSTEM.add(btnSTRUCT, c);
    c.gridx = 3;
    pnlSYSTEM.add(btnMapView, c);

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 4;
    add(pnlSYSTEM, c);
    c.gridx = 0;
    c.gridy = 1;
    add(panCARDS, c);
    c.gridx = 0;
    c.gridy = 2;
    c.gridwidth = 2;
    c.anchor = GridBagConstraints.LINE_START;
    add(btnLEFT, c);
    c.gridx = 2;
    c.anchor = GridBagConstraints.LINE_END;
    add(btnRIGHT, c);

    INFO.init();
    PLANETS.init();
    init = false;
  }

  @Override
  public String menuCommand() {
    return MenuCommand.SystemInfo;
  }

  //fill systems
  private void fillSystems() {
    ID[] items = SYSTEMS.getSystemIDs(-1, false);

    Arrays.sort(items, new Comparator<ID>() {
      @Override
      public int compare(ID o1, ID o2) {
        return o1.toString().compareToIgnoreCase(o2.toString());
      }
    });

    int prev = selectedSystem;
    jcbSYSTEM.removeAllItems();

    int prevIdx = -1;
    for (int i = 0; i < items.length; ++i) {
      ID sid = items[i];
      jcbSYSTEM.addItem(sid);
      if (sid.ID == prev)
        prevIdx = i;
    }

    // restore selection
    if (prevIdx >= 0) {
      jcbSYSTEM.setSelectedIndex(prevIdx);
    }
    else {
      ID id = (ID)jcbSYSTEM.getSelectedItem();
      selectedSystem = id != null ? id.ID : -1;
    }
  }

  @Override
  public boolean hasChanges() {
    return SYSTEMS.madeChanges() || INFO.hasChanges() || PLANETS.hasChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    GAME.checkChanged(SYSTEMS, changes);
    INFO.listChangedFiles(changes);
    PLANETS.listChangedFiles(changes);
  }

  @Override
  public void reload() throws IOException {
    // reload files
    SYSTEMS = (SystInfo) GAME.getInternalFile(CSavFiles.SystInfo, true);
    // update ui data
    init = true;
    fillSystems();
    INFO.reload();
    PLANETS.reload();
    init = false;
  }

  //final warning
  @Override
  public void finalWarning() {
    if (INFO != null)
      INFO.finalWarning();
    if (PLANETS != null)
      PLANETS.finalWarning();
  }

  /*This panel is for editing system info only*/
  private class SystemPanel extends MainPanel {
    // ui components
    private JLabel[] lblLABEL = new JLabel[11];
    private JLabel lblMAXPOP = new JLabel();
    private JLabel lblMORALE = new JLabel();
    private JLabel lblUNALL = new JLabel();
    private JLabel lblBID = new JLabel();
    private JComboBox<String> cobTYPE = new JComboBox<String>();
    private JComboBox<String> cobSTARANI = new JComboBox<String>();
    private JCheckBox chbDIL = new JCheckBox();
    private JComboBox<String> cobRESIDENT = new JComboBox<String>();
    private JSlider spiPOPUL = new JSlider();
    private JSlider sliMORALE = new JSlider(0, 195);
    private JSlider[] sliUNITS = new JSlider[5];
    private AniLabel aniSTAR = new AniLabel();

    // data
    int MAX_POP = 0;

    public SystemPanel() throws IOException {
      setupComponents();
      addActionListeners();
      setupLayout();
      loadData();
    }

    private void setupComponents() {
      //create
      lblLABEL[0] = new JLabel(Language.getString("SystemsInfoGUI.2")); //$NON-NLS-1$
      lblLABEL[1] = new JLabel(Language.getString("SystemsInfoGUI.3")); //$NON-NLS-1$
      lblLABEL[2] = new JLabel(Language.getString("SystemsInfoGUI.4")); //$NON-NLS-1$
      lblLABEL[3] = new JLabel(Language.getString("SystemsInfoGUI.5")); //$NON-NLS-1$
      lblLABEL[4] = new JLabel(Language.getString("SystemsInfoGUI.6")); //$NON-NLS-1$
      lblLABEL[5] = new JLabel(Language.getString("SystemsInfoGUI.7")); //$NON-NLS-1$
      lblLABEL[6] = new JLabel(Language.getString("SystemsInfoGUI.8")); //$NON-NLS-1$
      lblLABEL[7] = new JLabel(Language.getString("SystemsInfoGUI.9")); //$NON-NLS-1$
      lblLABEL[8] = new JLabel(Language.getString("SystemsInfoGUI.10")); //$NON-NLS-1$
      lblLABEL[9] = new JLabel(Language.getString("SystemsInfoGUI.11")); //$NON-NLS-1$
      lblLABEL[10] = new JLabel(Language.getString("SystemsInfoGUI.12")); //$NON-NLS-1$
      sliUNITS[0] = new JSlider();
      sliUNITS[1] = new JSlider();
      sliUNITS[2] = new JSlider();
      sliUNITS[3] = new JSlider();
      sliUNITS[4] = new JSlider();

      //set up
      Font def = UE.SETTINGS.getDefaultFont();
      for (int i = 0; i < 11; i++) {
        lblLABEL[i].setFont(def);
        if (i < 5) {
          sliUNITS[i].setFont(def);
          sliUNITS[i].setMinorTickSpacing(10);
          sliUNITS[i].setMajorTickSpacing(10);
          sliUNITS[i].setSnapToTicks(true);
          sliUNITS[i].setPreferredSize(new Dimension(100, 16));
        }
      }

      lblUNALL.setFont(def);
      lblMAXPOP.setFont(def);
      lblMORALE.setFont(def);
      cobTYPE.setFont(def);
      cobSTARANI.setFont(def);
      chbDIL.setFont(def);
      cobRESIDENT.setFont(def);
      spiPOPUL.setFont(def);
      spiPOPUL.setPreferredSize(new Dimension(100, 16));
      sliMORALE.setFont(def);
      sliMORALE.setPreferredSize(new Dimension(100, 16));

      aniSTAR.setForceFirstPixelAlpha(true);
    }

    private void addActionListeners() {
      //type
      cobTYPE.addActionListener(new CBActionListener(x -> {
        if (selectedSystem != -1 && cobTYPE.getSelectedIndex() >= 0) {
          val system = SYSTEMS.getSystem(selectedSystem);
          system.setStarType(cobTYPE.getSelectedIndex());
        }
      }));

      //animation
      cobSTARANI.addActionListener(new CBActionListener(x -> {
        if (selectedSystem != -1 && cobSTARANI.getSelectedIndex() >= 0) {
          val system = SYSTEMS.getSystem(selectedSystem);
          system.setAni((String) cobSTARANI.getSelectedItem());
          updateStarAni();
        }
      }));

      //dilithium
      chbDIL.addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1) {
          try {
            val system = SYSTEMS.getSystem(selectedSystem);
            system.setDilithium(chbDIL.isSelected());
            updateDilithiumIcon();
          } catch (Exception ze) {
            Dialogs.displayError(ze);
            lblBID.setIcon(null);
          }
        }
      }));

      //resident race
      cobRESIDENT.addActionListener(new CBActionListener(x -> {
        if (selectedSystem != -1 && cobRESIDENT.getSelectedIndex() >= 0) {
          val system = SYSTEMS.getSystem(selectedSystem);
          system.setResidentRace((short) cobRESIDENT.getSelectedIndex());
        }
      }));

      //population
      spiPOPUL.addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1) {
          val system = SYSTEMS.getSystem(selectedSystem);
          system.setPopulation(spiPOPUL.getValue());

          int alloc = system.getPop2Energy() + system.getPop2Food() + system.getPop2Industry()
            + system.getPop2Intel() + system.getPop2Research();

          if (alloc > spiPOPUL.getValue()) {
            alloc = sliUNITS[4].getValue();
            sliUNITS[4].setValue(0);
            sliUNITS[4].setValue(alloc);
            alloc = sliUNITS[3].getValue();
            sliUNITS[3].setValue(0);
            sliUNITS[3].setValue(alloc);
            alloc = sliUNITS[1].getValue();
            sliUNITS[1].setValue(0);
            sliUNITS[1].setValue(alloc);
            alloc = sliUNITS[2].getValue();
            sliUNITS[2].setValue(0);
            sliUNITS[2].setValue(alloc);
            alloc = sliUNITS[0].getValue();
            sliUNITS[0].setValue(0);
            sliUNITS[0].setValue(alloc);
          }

          updatePopText();
        }
      }));

      //morale
      sliMORALE.addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1 && cobRESIDENT.getSelectedIndex() >= 0) {
          val system = SYSTEMS.getSystem(selectedSystem);
          system.setMorale((short) sliMORALE.getValue());
          updateMoraleText();
        }
      }));

      //population 2 food
      sliUNITS[0].addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1) {
          val system = SYSTEMS.getSystem(selectedSystem);
          int available = system.getPopulation() - system.getPop2Energy() - system.getPop2Industry()
            - system.getPop2Intel() - system.getPop2Research();

          if (sliUNITS[0].getValue() > available) {
            int lo = (available / 10) * 10;
            if (system.getPopulation() < 10)
              lo = 10;

            sliUNITS[0].setValue(lo);
          }

          system.setPop2Food(sliUNITS[0].getValue());
          updatePopText();
        }
      }));

      //population 2 industry
      sliUNITS[1].addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1) {
          val system = SYSTEMS.getSystem(selectedSystem);
          int available = system.getPopulation() - system.getPop2Energy() - system.getPop2Food()
            - system.getPop2Intel() - system.getPop2Research();

          if (sliUNITS[1].getValue() > available) {
            int lo = (available / 10) * 10;
            if (system.getPopulation() < 10)
              lo = 10;

            sliUNITS[1].setValue(lo);
          }

          system.setPop2Industry(sliUNITS[1].getValue());
          updatePopText();
        }
      }));

      //population 2 energy
      sliUNITS[2].addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1) {
          val system = SYSTEMS.getSystem(selectedSystem);
          int available = system.getPopulation() - system.getPop2Industry() - system.getPop2Food()
            - system.getPop2Intel() - system.getPop2Research();

          if (sliUNITS[2].getValue() > available) {
            int lo = (available / 10) * 10;
            if (system.getPopulation() < 10)
              lo = 10;

            sliUNITS[2].setValue(lo);
          }

          system.setPop2Energy(sliUNITS[2].getValue());
          updatePopText();
        }
      }));

      //population 2 intel
      sliUNITS[3].addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1) {
          val system = SYSTEMS.getSystem(selectedSystem);
          int available = system.getPopulation() - system.getPop2Industry() - system.getPop2Food()
            - system.getPop2Energy() - system.getPop2Research();
          if (sliUNITS[3].getValue() > available) {
            int lo = (available / 10) * 10;
            if (system.getPopulation() < 10)
              lo = 10;

            sliUNITS[3].setValue(lo);
          }

          system.setPop2Intel(sliUNITS[3].getValue());
          updatePopText();
        }
      }));

      //population 2 research
      sliUNITS[4].addChangeListener(new CBChangeListener(x -> {
        if (selectedSystem != -1) {
          val system = SYSTEMS.getSystem(selectedSystem);
          int available = system.getPopulation() - system.getPop2Industry() - system.getPop2Food()
            - system.getPop2Energy() - system.getPop2Intel();

          if (sliUNITS[4].getValue() > available) {
            int lo = (available / 10) * 10;
            if (system.getPopulation() < 10)
              lo = 10;

            sliUNITS[4].setValue(lo);
          }

          system.setPop2Research(sliUNITS[4].getValue());
          updatePopText();
        }
      }));
    }

    private void setupLayout() {
      //lay out
      setLayout(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;

      //o_o add to panels
      JPanel pnlPOPU = new JPanel(new GridBagLayout());
      JPanel pnlSYST = new JPanel(new GridBagLayout());

      //syste
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      pnlSYST.add(lblBID, c); //dilithium sign
      c.gridwidth = 2;
      c.gridheight = 2;
      pnlSYST.add(aniSTAR, c); //animation
      c.gridwidth = 1;
      c.gridheight = 1;
      c.gridy = 2;
      pnlSYST.add(lblLABEL[0], c); //type
      c.gridy = 3;
      pnlSYST.add(lblLABEL[1], c); //ani
      c.gridy = 4;
      pnlSYST.add(lblLABEL[10], c); //has dil
      c.gridx = 1;
      c.gridy = 2;
      pnlSYST.add(cobTYPE, c); //type
      c.gridy = 3;
      pnlSYST.add(cobSTARANI, c); //star ani
      c.gridy = 4;
      pnlSYST.add(chbDIL, c); //ani
      c.gridx = 0;
      c.gridy = 5;
      c.gridwidth = 2;
      pnlSYST.add(lblMAXPOP, c); //max pop

      //popul
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      pnlPOPU.add(lblLABEL[2], c); //resident
      c.gridy = 1;
      pnlPOPU.add(lblLABEL[3], c); //pop
      c.gridy = 2;
      pnlPOPU.add(lblMORALE, c); //morale
      c.gridx = 1;
      c.gridy = 0;
      pnlPOPU.add(cobRESIDENT, c); //resident
      c.gridy = 1;
      pnlPOPU.add(spiPOPUL, c); //pop
      c.gridy = 2;
      pnlPOPU.add(sliMORALE, c); //morale
      c.gridx = 0;
      c.gridy = 3;
      pnlPOPU.add(lblLABEL[4], c); //alloc

      for (int i = 0; i < 5; i++) {
        c.gridy = 4 + i;
        pnlPOPU.add(lblLABEL[i + 5], c); //alloc
      }

      c.gridy = 9;
      c.gridwidth = 2;
      pnlPOPU.add(lblUNALL, c); //unalloc
      c.gridwidth = 1;
      c.gridx = 1;

      for (int i = 0; i < 5; i++) {
        c.gridy = 4 + i;
        pnlPOPU.add(sliUNITS[i], c); //alloc
      }

      pnlPOPU.setBorder(new LineBorder(Color.BLACK));
      pnlSYST.setBorder(new LineBorder(Color.BLACK));
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      add(pnlSYST, c);
      c.gridx = 1;
      add(pnlPOPU, c);
    }

    private void loadData() throws IOException {
      //fill them up
      //type
      cobTYPE.removeAllItems();
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.13")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.14")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.15")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.16")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.17")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.18")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.19")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.20")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.21")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.22")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.23")); //$NON-NLS-1$
      cobTYPE.addItem(Language.getString("SystemsInfoGUI.24")); //$NON-NLS-1$

      //Star ani
      cobSTARANI.removeAllItems();
      Collection<String> anis = STBOF.getFileNames(FileFilters.Ani);
      for (String ani : anis)
        cobSTARANI.addItem(ani);

      //resident
      cobRESIDENT.removeAllItems();
      int ij = RACES.getNumberOfEntries();
      for (int i = 0; i < ij; i++) {
        cobRESIDENT.addItem(RACES.getName(i));
      }
    }

    @Override
    public String menuCommand() {
      return null;
    }

    // should be called every time SELECTED changes or this class gains focus
    public void init() {
      if (selectedSystem == -1)
        return;

      try {
        val system = SYSTEMS.getSystem(selectedSystem);
        MAX_POP = 0;

        // calculate maximum population
        for (int i = 0; i < system.getNumPlanets(); ++i) {
          val planet = system.getPlanet(i);
          int energyBonusLvl = planet.getEnergyBonusLvl();
          int growthBonusLvl = planet.getGrowthBonusLvl();
          MAX_POP += SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
            energyBonusLvl, growthBonusLvl);
        }

        // unset selected to skip events
        int sysId = selectedSystem;
        selectedSystem = -1;

        // set limits
        for (int i = 0; i < 5; i++) {
          sliUNITS[i].setMinimum(0);
          sliUNITS[i].setMaximum(MAX_POP);
        }
        spiPOPUL.setMinimum(0);
        spiPOPUL.setMaximum(MAX_POP);

        // set cobRESIDENT
        short ku = system.getResidentRace();
        if (ku < 0) {
          btnSTRUCT.setEnabled(false);

          system.setResidentRace((short) -1);
          // morale
          sliMORALE.setEnabled(false);
          system.setMorale((short) 0);
          // population
          spiPOPUL.setEnabled(false);
          system.setPopulation(0);
          // units
          for (int i = 0; i < 5; i++) {
            sliUNITS[i].setEnabled(false);
            sliUNITS[i].setValue(0);
          }
          cobRESIDENT.setSelectedIndex(-1);
          cobRESIDENT.setEnabled(false);
        } else {
          btnSTRUCT.setEnabled(true);
          cobRESIDENT.setEnabled(true);
          cobRESIDENT.setSelectedIndex(ku);
          // morale
          sliMORALE.setEnabled(true);
          // population
          spiPOPUL.setEnabled(true);
          // units
          for (int i = 0; i < 5; i++) {
            sliUNITS[i].setEnabled(true);
          }
        }

        // set cobTYPE
        cobTYPE.setSelectedIndex(system.getStarType());
        // set cobSTARANI
        cobSTARANI.setSelectedItem(system.getAni());
        // set chbDIL
        chbDIL.setSelected(system.hasDilithium());
        // set spiPOPUL
        spiPOPUL.setValue(system.getPopulation());
        // set pop2food
        sliUNITS[0].setValue(system.getPop2Food());
        // set pop2ind
        sliUNITS[1].setValue(system.getPop2Industry());
        // set pop2ene
        sliUNITS[2].setValue(system.getPop2Energy());
        // set pop2intel
        sliUNITS[3].setValue(system.getPop2Intel());
        // set pop2res
        sliUNITS[4].setValue(system.getPop2Research());

        // restore selected
        selectedSystem = sysId;

        updateMoraleText();
        updatePopText();
        updateStarAni();
        updateDilithiumIcon();
      } catch(Exception ex) {
        Dialogs.displayError(ex);
      }
    }

    @Override
    public boolean hasChanges() {
      return false;
    }

    @Override
    protected void listChangedFiles(FileChanges changes) {
    }

    @Override
    public void reload() throws IOException {
      init();
    }

    @Override
    public void finalWarning() {
    }

    private void updateDilithiumIcon() throws IOException {
      if (chbDIL.isSelected()) {
        lblBID.setIcon(new ImageIcon((STBOF.getTargaImage(
          "bid.tga", LoadFlags.CACHE)).getImage(AlphaMode.FirstPixelAlpha))); //$NON-NLS-1$
      } else {
        lblBID.setIcon(null);
      }
    }

    private void updatePopText() {
      val system = SYSTEMS.getSystem(selectedSystem);

      String msg = Language.getString("SystemsInfoGUI.31"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(system.getPopulation())); //$NON-NLS-1$
      lblLABEL[3].setText(msg);
      msg = Language.getString("SystemsInfoGUI.25"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(MAX_POP)); //$NON-NLS-1$
      lblMAXPOP.setText(msg);
      msg = Language.getString("SystemsInfoGUI.26"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(sliUNITS[0].getValue())); //$NON-NLS-1$
      lblLABEL[5].setText(msg);
      msg = Language.getString("SystemsInfoGUI.27"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(sliUNITS[1].getValue())); //$NON-NLS-1$
      lblLABEL[6].setText(msg);
      msg = Language.getString("SystemsInfoGUI.28"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(sliUNITS[2].getValue())); //$NON-NLS-1$
      lblLABEL[7].setText(msg);
      msg = Language.getString("SystemsInfoGUI.29"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(sliUNITS[3].getValue())); //$NON-NLS-1$
      lblLABEL[8].setText(msg);
      msg = Language.getString("SystemsInfoGUI.30"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(sliUNITS[4].getValue())); //$NON-NLS-1$
      lblLABEL[9].setText(msg);
      int ava = system.getPopulation() - sliUNITS[0].getValue() - sliUNITS[1].getValue()
        - sliUNITS[2].getValue() - sliUNITS[3].getValue() - sliUNITS[4].getValue();
      msg = Language.getString("SystemsInfoGUI.32"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(ava)); //$NON-NLS-1$
      lblUNALL.setText(msg);
    }

    private void updateMoraleText() {
      int morale = SYSTEMS.getSystem(selectedSystem).getMorale();
      sliMORALE.setValue(morale);

      String msg = Language.getString("SystemsInfoGUI.33"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(morale)); //$NON-NLS-1$
      if (morale <= 0) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.34")); //$NON-NLS-1$ //$NON-NLS-2$
      } else if (morale < 25) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.35")); //$NON-NLS-1$ //$NON-NLS-2$
      } else if (morale < 51) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.36")); //$NON-NLS-1$ //$NON-NLS-2$
      } else if (morale < 76) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.37")); //$NON-NLS-1$ //$NON-NLS-2$
      } else if (morale < 100) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.38")); //$NON-NLS-1$ //$NON-NLS-2$
      } else if (morale < 126) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.39")); //$NON-NLS-1$ //$NON-NLS-2$
      } else if (morale < 151) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.40")); //$NON-NLS-1$ //$NON-NLS-2$
      } else if (morale < 176) {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.41")); //$NON-NLS-1$ //$NON-NLS-2$
      } else {
        msg = msg
            .replace("%2", Language.getString("SystemsInfoGUI.42")); //$NON-NLS-1$ //$NON-NLS-2$
      }
      lblMORALE.setText(msg);
    }

    protected void updateStarAni() throws IOException {
      String aniName = SYSTEMS.getSystem(selectedSystem).getAni();
      AniOrCur ani = STBOF.getAnimation(aniName, LoadFlags.CACHE);
      aniSTAR.setAnimation(ani, true);
    }
  }

  /*To edit planets*/
  private class SystemPlanet extends MainPanel {
    // ui components
    private JLabel[] LABEL = new JLabel[8];
    private JButton btnADD = new JButton(Language.getString("SystemsInfoGUI.43")); //$NON-NLS-1$
    private JButton btnEDIT = new JButton(Language.getString("SystemsInfoGUI.0")); //$NON-NLS-1$
    private JButton btnREMOVE = new JButton(Language.getString("SystemsInfoGUI.44")); //$NON-NLS-1$
    private JButton btnMOVEUP = new JButton(Language.getString("SystemsInfoGUI.45")); //$NON-NLS-1$
    private JButton btnMOVEDW = new JButton(Language.getString("SystemsInfoGUI.46")); //$NON-NLS-1$
    private JTextField jtfPOINT = new JTextField(10);
    private JTextField jtfTERRA = new JTextField(10);
    private JList<String> jcbNAME = new JList<String>();
    private JComboBox<String> jcbANI = new JComboBox<String>();
    private JComboBox<ID> jcbTYPE = new JComboBox<ID>();
    private JComboBox<ID> jcbATMO = new JComboBox<ID>();
    private JComboBox<String> jcbSIZE = new JComboBox<String>();
    private Vector<JComboBox<String>> jcbBONUS = new Vector<JComboBox<String>>(3);
    private AniLabel aniANIM = new AniLabel();
    private JLabel jlPOP = new JLabel();
    private JLabel jlBONI = new JLabel();

    // data
    private int selectedPlanet = -1;

    public SystemPlanet() throws IOException {
      //build elements
      LABEL[0] = new JLabel(Language.getString("SystemsInfoGUI.48")); //$NON-NLS-1$
      LABEL[1] = new JLabel(Language.getString("SystemsInfoGUI.49")); //$NON-NLS-1$
      LABEL[2] = new JLabel(Language.getString("SystemsInfoGUI.50")); //$NON-NLS-1$
      LABEL[3] = new JLabel(Language.getString("SystemsInfoGUI.51")); //$NON-NLS-1$
      LABEL[4] = new JLabel(Language.getString("SystemsInfoGUI.52")); //$NON-NLS-1$
      LABEL[5] = new JLabel(Language.getString("SystemsInfoGUI.53")); //$NON-NLS-1$
      LABEL[6] = new JLabel(Language.getString("SystemsInfoGUI.54")); //$NON-NLS-1$
      LABEL[7] = new JLabel(Language.getString("SystemsInfoGUI.55")); //$NON-NLS-1$
      jcbBONUS.add(new JComboBox<String>());
      jcbBONUS.add(new JComboBox<String>());
      jcbBONUS.add(new JComboBox<String>());

      //set font
      Font def = UE.SETTINGS.getDefaultFont();
      for (int i = 0; i < 8; i++) {
        LABEL[i].setFont(def);
      }

      jcbBONUS.forEach(b -> b.setFont(def));
      btnEDIT.setFont(def);
      btnSTRUCT.setFont(def);
      btnMapView.setFont(def);
      jcbNAME.setFont(def);
      jcbANI.setFont(def);
      jcbTYPE.setFont(def);
      jcbATMO.setFont(def);
      jcbSIZE.setFont(def);
      jlPOP.setFont(def);
      jlBONI.setFont(def);
      jtfPOINT.setFont(def);
      jtfTERRA.setFont(def);
      btnADD.setFont(def);
      btnREMOVE.setFont(def);
      btnMOVEUP.setFont(def);
      btnMOVEDW.setFont(def);
      jcbNAME.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

      aniANIM.setForceFirstPixelAlpha(true);

      //add listeners
      btnEDIT.addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);

        String newName = JOptionPane.showInputDialog(strENTER_NEW_NAME, planet.getName());
        if (newName != null) {
          finalWarning();
          planet.setName(newName);
          fillPlanets();
          jcbNAME.setSelectedIndex(planetIdx);
        }
      }));

      btnADD.addActionListener(new CBActionListener(evt -> {
        if (selectedSystem == -1)
          return;

        finalWarning();
        SYSTEMS.getSystem(selectedSystem).addPlanet();
        fillPlanets();
      }));

      btnREMOVE.addActionListener(new CBActionListener(evt -> {
        if (selectedSystem == -1 || jcbNAME.getSelectedIndex() < 0)
          return;

        finalWarning();
        SYSTEMS.getSystem(selectedSystem).removePlanet((short) jcbNAME.getSelectedIndex());
        fillPlanets();
      }));

      btnMOVEUP.addActionListener(new CBActionListener(evt -> {
        if (selectedSystem == -1 || jcbNAME.getSelectedIndex() < 0)
          return;

        finalWarning();
        SYSTEMS.getSystem(selectedSystem).movePlanetUp((short) jcbNAME.getSelectedIndex());
        fillPlanets();
      }));

      btnMOVEDW.addActionListener(new CBActionListener(evt -> {
        if (selectedSystem == -1 || jcbNAME.getSelectedIndex() < 0)
          return;

        finalWarning();
        SYSTEMS.getSystem(selectedSystem).movePlanetDown((short) jcbNAME.getSelectedIndex());
        fillPlanets();
      }));

      jcbNAME.addListSelectionListener(new CBListSelectionListener(evt -> {
        if (selectedSystem < 0)
          return;

        //save
        finalWarning();

        //show
        selectedPlanet = (short) jcbNAME.getSelectedIndex();
        if (selectedPlanet >= 0) {
          val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(selectedPlanet);

          jcbTYPE.setEnabled(true);
          jcbATMO.setEnabled(true);
          jcbSIZE.setEnabled(true);
          jcbANI.setEnabled(true);
          jcbTYPE.setEnabled(true);
          jcbBONUS.forEach(b -> b.setEnabled(true));
          jtfPOINT.setEnabled(true);
          jtfTERRA.setEnabled(true);
          btnEDIT.setEnabled(true);
          //btnREMOVE.setEnabled(true);
          btnMOVEUP.setEnabled(true);
          btnMOVEDW.setEnabled(true);
          jcbTYPE.setSelectedItem(new ID(null, planet.getPlanetType()));
          jcbATMO.setSelectedItem(new ID(null, planet.getAtmosphere()));
          jcbSIZE.setSelectedIndex(planet.getPlanetSize());
          jcbANI.setSelectedItem(planet.getAnimation());
          jcbBONUS.elementAt(0).setSelectedIndex(planet.getFoodBonusLvl());
          jcbBONUS.elementAt(1).setSelectedIndex(planet.getEnergyBonusLvl());
          jcbBONUS.elementAt(2).setSelectedIndex(planet.getGrowthBonusLvl());
          jtfPOINT.setText(Integer.toString(planet.getTerraformPoints()));
          jtfTERRA.setText(Integer.toString(planet.getTerraformed()));
        } else {
          jcbTYPE.setEnabled(false);
          jcbATMO.setEnabled(false);
          jcbSIZE.setEnabled(false);
          jcbANI.setEnabled(false);
          jcbTYPE.setEnabled(false);
          jcbBONUS.forEach(b -> b.setEnabled(false));
          jtfPOINT.setEnabled(false);
          jtfTERRA.setEnabled(false);
          btnEDIT.setEnabled(false);
          btnREMOVE.setEnabled(false);
          btnMOVEUP.setEnabled(false);
          btnMOVEDW.setEnabled(false);
          aniANIM.setAnimation(null, false);
        }

        updateText();
      }));

      jcbANI.addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        try {
          val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);
          planet.setAnimation((String) jcbANI.getSelectedItem());
          jcbANI.setSelectedItem(planet.getAnimation());
          AniOrCur ani = STBOF.getAnimation(planet.getAnimation(), LoadFlags.CACHE);
          aniANIM.setAnimation(ani, true);
        } catch (Exception fnf) {
          Dialogs.displayError(fnf);
          aniANIM.setAnimation(null, false);
        }
      }));

      jcbTYPE.addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        ID planetType = (ID) jcbTYPE.getSelectedItem();
        if (planetType == null)
          return;

        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);
        planet.setPlanetType(planetType.ID);
        updateText();
      }));

      jcbATMO.addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        ID planetAtmosphere = (ID) jcbATMO.getSelectedItem();
        if (planetAtmosphere == null)
          return;

        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);
        planet.setAtmosphere(planetAtmosphere.ID);
      }));

      jcbSIZE.addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        short planetSize = (short) jcbSIZE.getSelectedIndex();
        if (planetSize == -1)
          return;

        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);
        planet.setPlanetSize(planetSize);
        jcbSIZE.setSelectedItem(planet.getPlanetSize());
        updateText();
      }));

      jcbBONUS.elementAt(0).addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        JComboBox<String> cmb = jcbBONUS.elementAt(0);
        int foodBonusLvl = cmb.getSelectedIndex();
        if (foodBonusLvl == -1)
          return;

        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);
        planet.setFoodBonusLvl(foodBonusLvl);
        cmb.setSelectedItem(planet.getFoodBonusLvl());
        updateText();
      }));

      jcbBONUS.elementAt(1).addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        JComboBox<String> cmb = jcbBONUS.elementAt(1);
        int energyBonusLvl = (byte) cmb.getSelectedIndex();
        if (energyBonusLvl == -1)
          return;

        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);
        planet.setEnergyBonusLvl(energyBonusLvl);
        cmb.setSelectedItem(planet.getEnergyBonusLvl());
        updateText();
      }));

      jcbBONUS.elementAt(2).addActionListener(new CBActionListener(evt -> {
        int planetIdx = jcbNAME.getSelectedIndex();
        if (selectedSystem == -1 || planetIdx < 0)
          return;

        JComboBox<String> cmb = jcbBONUS.elementAt(2);
        byte growthBonusLvl = (byte) cmb.getSelectedIndex();
        if (growthBonusLvl == -1)
          return;

        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(planetIdx);
        planet.setGrowthBonusLvl(growthBonusLvl);
        cmb.setSelectedItem(planet.getGrowthBonusLvl());
        updateText();
      }));

      //fill combos
      Collection<String> fileNames = STBOF.getFileNames(new java.io.FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
          return name.toLowerCase().endsWith(".ani"); //$NON-NLS-1$
        }
      });

      for (String fileName : fileNames)
        jcbANI.addItem(fileName);

      for (ID planGfx : ENVIRON.planetGfx()) {
        jcbTYPE.addItem(planGfx);
      }

      for (ID atmoGfx : ENVIRON.atmosphereGfx()) {
        jcbATMO.addItem(atmoGfx);
      }

      jcbSIZE.addItem(Language.getString("SystemsInfoGUI.57")); //$NON-NLS-1$
      jcbSIZE.addItem(Language.getString("SystemsInfoGUI.58")); //$NON-NLS-1$
      jcbSIZE.addItem(Language.getString("SystemsInfoGUI.59")); //$NON-NLS-1$

      for (int i = 0; i < 3; i++) {
        String str = null;
        switch (i) {
          case 0:
            str = Language.getString("SystemsInfoGUI.60"); //$NON-NLS-1$
            break;
          case 1:
            str = Language.getString("SystemsInfoGUI.61"); //$NON-NLS-1$
            break;
          case 2:
            str = Language.getString("SystemsInfoGUI.62"); //$NON-NLS-1$
            break;
        }

        for (JComboBox<String> cmb : jcbBONUS)
          cmb.addItem(str);
      }

      fillPlanets();

      //place
      setLayout(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;
      JPanel jpPREV = new JPanel(new GridBagLayout());
      JPanel jpEDIT = new JPanel(new GridBagLayout());

      //preview
      JScrollPane scrolly = new JScrollPane(jcbNAME);
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 6;
      c.weightx = 1.0;
      c.weighty = 1.0;
      jpPREV.add(scrolly, c);
      c.gridx = 1;
      c.gridheight = 1;
      c.weightx = 0;
      c.weighty = 0;
      jpPREV.add(btnADD, c);
      c.gridy = 1;
      jpPREV.add(btnEDIT, c);
      c.gridy = 2;
      jpPREV.add(btnREMOVE, c);
      c.gridy = 3;
      jpPREV.add(btnMOVEUP, c);
      c.gridy = 4;
      jpPREV.add(btnMOVEDW, c);
      c.gridx = 2;
      c.gridy = 0;
      c.gridheight = 3;
      jpPREV.add(aniANIM, c);

      JPanel jpInfo = new JPanel();
      jpInfo.setLayout(new BoxLayout(jpInfo, BoxLayout.Y_AXIS));
      jpInfo.add(jlPOP);
      jpInfo.add(jlBONI);

      c.gridheight = 2;
      c.gridy = 3;
      jpPREV.add(jpInfo, c);
      // reserve some space to avoid resize by text changes
      jpPREV.add(Box.createHorizontalStrut(120), c);

      //edit
      c.gridx = 0;
      c.gridheight = 1;
      for (int i = 0; i < 8; i++) {
        if (i == 5) {
          c.gridx = 2;
        }
        if (i < 5) {
          c.gridy = i;
        } else {
          c.gridy = i - 5;
        }
        jpEDIT.add(LABEL[i], c);
      }

      c.gridy = 3;
      JLabel lbl = new JLabel(Language.getString("SystemsInfoGUI.63")); //$NON-NLS-1$
      lbl.setFont(def);
      jpEDIT.add(lbl, c);
      c.gridx = 1;
      c.gridy = 0;
      jpEDIT.add(jcbANI, c);
      c.gridy = 1;
      jpEDIT.add(jcbTYPE, c);
      c.gridy = 2;
      jpEDIT.add(jcbSIZE, c);
      c.gridy = 3;
      jpEDIT.add(jtfPOINT, c);
      c.gridy = 4;
      jpEDIT.add(jtfTERRA, c);
      c.gridx = 3;
      c.gridy = 0;
      jpEDIT.add(jcbBONUS.elementAt(0), c);
      c.gridy = 1;
      jpEDIT.add(jcbBONUS.elementAt(1), c);
      c.gridy = 2;
      jpEDIT.add(jcbBONUS.elementAt(2), c);
      c.gridy = 3;
      jpEDIT.add(jcbATMO, c);
      jpPREV.setBorder(new LineBorder(Color.BLACK));
      jpEDIT.setBorder(new LineBorder(Color.BLACK));
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      add(jpPREV, c);
      c.gridy = 1;
      add(jpEDIT, c);
    }

    @Override
    public String menuCommand() {
      return null;
    }

    @Override
    public void setVisible(boolean visible) {
      super.setVisible(visible);
      // auto-select first planet
      if (visible && selectedPlanet < 0 && jcbNAME.getComponentCount() > 0)
        jcbNAME.setSelectedIndex(0);
    }

    private void updateText() {
      if (selectedPlanet >= 0) {
        val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(selectedPlanet);

        int pot = SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());

        String msg = Language.getString("SystemsInfoGUI.64") //$NON-NLS-1$
          .replace("%1", Integer.toString(pot)); //$NON-NLS-1$
        jlPOP.setText(msg);

        float pov = BONI.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());

        msg = Language.getString("SystemsInfoGUI.65") //$NON-NLS-1$
          .replace("%1", (pov * 100 * 0.05) + "%"); //$NON-NLS-1$ //$NON-NLS-2$
        jlBONI.setText(msg);
      } else {
        String msg = Language.getString("SystemsInfoGUI.66"); //$NON-NLS-1$
        jlPOP.setText(msg);
        msg = Language.getString("SystemsInfoGUI.67"); //$NON-NLS-1$
        jlBONI.setText(msg);
      }
    }

    private void fillPlanets() {
      if (selectedSystem == -1)
        return;

      try {
        String[] str = SYSTEMS.getSystem(selectedSystem).getPlanetNames();
        jcbNAME.setListData(str);

        jcbTYPE.setEnabled(false);
        jcbSIZE.setEnabled(false);
        jcbANI.setEnabled(false);
        jcbATMO.setEnabled(false);
        jcbBONUS.forEach(cmb -> cmb.setEnabled(false));
        jtfPOINT.setEnabled(false);
        jtfTERRA.setEnabled(false);
        updateText();
        btnADD.setEnabled(false);
        btnEDIT.setEnabled(false);
        btnREMOVE.setEnabled(false);
        btnMOVEUP.setEnabled(false);
        btnMOVEDW.setEnabled(false);
        aniANIM.setAnimation(null, false);
        updateText();

        // auto-select first planet
        if (isVisible() && str.length > 0)
          jcbNAME.setSelectedIndex(0);
      } catch (Exception ze) {
        Dialogs.displayError(ze);
      }
    }

    //should be called every time SELECTED changes
    public void init() {
      if (selectedSystem == -1)
        return;

      selectedPlanet = -1;
      fillPlanets();
    }

    @Override
    public boolean hasChanges() {
      return false;
    }

    @Override
    protected void listChangedFiles(FileChanges changes) {
    }

    @Override
    public void reload() {
      init();
    }

    @Override
    public void finalWarning() {
      if (selectedSystem < 0 || selectedPlanet < 0)
        return;

      val planet = SYSTEMS.getSystem(selectedSystem).getPlanet(selectedPlanet);
      planet.setTerraformPoints(Integer.parseInt(jtfPOINT.getText()));
      planet.setTerraformed(Integer.parseInt(jtfTERRA.getText()));
    }
  }

  @Override
  public String getHelpFileName() {
    return "SystemsInfo.html"; //$NON-NLS-1$
  }

}
