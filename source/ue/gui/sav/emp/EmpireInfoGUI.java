package ue.gui.sav.emp;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.tec.data.TechEntry;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.emp.EmpsInfo;
import ue.edit.sav.files.emp.TechInfo;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.style.CheckListRenderer;
import ue.service.Language;

/**
 * @author Alan
 */
public class EmpireInfoGUI extends MainPanel {

  private final String strEmpireColumn = Language.getString("EmpireInfoGUI.12"); //$NON-NLS-1$
  private final String strTreatyColumn = Language.getString("EmpireInfoGUI.13"); //$NON-NLS-1$
  private final String strOpenBorderColumn = Language.getString("EmpireInfoGUI.14"); //$NON-NLS-1$
  private final String strAllowsTradeColumn = Language.getString("EmpireInfoGUI.15"); //$NON-NLS-1$
  private final String strYes = Language.getString("EmpireInfoGUI.16"); //$NON-NLS-1$
  private final String strNo = Language.getString("EmpireInfoGUI.17"); //$NON-NLS-1$

  // ui components
  private JLabel lblRacePic = new JLabel();
  // base properties
  private JPanel pnlBaseProps = new JPanel();
  private JLabel lblEmpName = new JLabel(Language.getString("EmpireInfoGUI.1")); //$NON-NLS-1$
  private JLabel lblEmperor = new JLabel(Language.getString("EmpireInfoGUI.2")); //$NON-NLS-1$
  private JLabel lblCredits = new JLabel(Language.getString("EmpireInfoGUI.3")); //$NON-NLS-1$
  private JTextField txtCredits = new JTextField();
  private JTextField txtEmpName = new JTextField();
  private JTextField txtEmperor = new JTextField();
  private JLabel lblIncome = new JLabel(Language.getString("EmpireInfoGUI.4")); //$NON-NLS-1$
  private JLabel lblPopSupport = new JLabel(Language.getString("EmpireInfoGUI.5")); //$NON-NLS-1$
  private JLabel lblDilithium = new JLabel(Language.getString("EmpireInfoGUI.6")); //$NON-NLS-1$
  private JLabel lblMorale = new JLabel(Language.getString("EmpireInfoGUI.8")); //$NON-NLS-1$
  private JLabel lblShipsInBuild = new JLabel(Language.getString("EmpireInfoGUI.9")); //$NON-NLS-1$
  private JLabel lblIncomeValue = new JLabel();
  private JLabel lblPopSupportValue = new JLabel();
  private JLabel lblDilithiumValue = new JLabel();
  private JLabel lblMoraleValue = new JLabel();
  private JLabel lblShipsinBuildValue = new JLabel();
  // tech levels
  private JLabel[] lblTechLvl = new JLabel[CStbof.NUM_TECH_FIELDS];
  private JSpinner[] spiTechLvl = new JSpinner[CStbof.NUM_TECH_FIELDS];
  // empire relations
  private JLabel lblEmpStatus = new JLabel(Language.getString("EmpireInfoGUI.10")); //$NON-NLS-1$
  private JLabel lblKnownRaces = new JLabel(Language.getString("EmpireInfoGUI.11")); //$NON-NLS-1$
  private JScrollPane jspEmpStatus = new JScrollPane();
  private JScrollPane jspKnownRaces = new JScrollPane();
  private JList<KnownRaceCheckItem> lstKnownRaces = new JList<KnownRaceCheckItem>();
  private MinorRaceListModel knownRacesModel;
  private JTable tblEmpStatus = new JTable();
  // navigation
  private JButton btnNext = new JButton();
  private JButton btnPrev = new JButton();

  // data
  private int empire = 0;
  private SavGame game;
  private Stbof STBOF;
  private EmpsInfo EMPSINFO;
  private TechInfo TECHINFO;
  private RaceRst RACE;

  /**
   * Creates new form EmpireInfoGUI
   */
  public EmpireInfoGUI(SavGame sav, Stbof stbof) throws IOException {
    this.game = sav;
    this.STBOF = stbof;

    RACE = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);
    EMPSINFO = (EmpsInfo) sav.getInternalFile(CSavFiles.EmpsInfo, true);
    TECHINFO = (TechInfo) sav.getInternalFile(CSavFiles.TechInfo, true);

    setupComponents();
    initComponents();
    refreshData();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Empires;
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();
    lblRacePic.setFont(def);

    // base properties
    lblEmpName.setFont(def);
    txtEmpName.setFont(def);
    lblEmperor.setFont(def);
    txtEmperor.setFont(def);
    lblCredits.setFont(def);
    txtCredits.setFont(def);
    lblIncome.setFont(def);
    lblPopSupport.setFont(def);
    lblDilithium.setFont(def);
    lblMorale.setFont(def);
    lblShipsInBuild.setFont(def);
    lblIncomeValue.setFont(def);
    lblPopSupportValue.setFont(def);
    lblDilithiumValue.setFont(def);
    lblMoraleValue.setFont(def);
    lblShipsinBuildValue.setFont(def);

    // tech levels
    for (int i = 0; i < CStbof.NUM_TECH_FIELDS; ++i) {
      lblTechLvl[i] = new JLabel(LexHelper.mapTechField(i) + ":");
      spiTechLvl[i] = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
      lblTechLvl[i].setFont(def);
      spiTechLvl[i].setFont(def);
      val editor = (JSpinner.DefaultEditor) spiTechLvl[i].getEditor();
      editor.getTextField().setColumns(2);
      lblTechLvl[i].setHorizontalAlignment(SwingConstants.RIGHT);
    }

    // empire relations
    lblEmpStatus.setFont(def);
    tblEmpStatus.setFont(def);
    lblKnownRaces.setFont(def);
    lstKnownRaces.setFont(def);

    // navigation
    btnNext.setFont(def);
    btnPrev.setFont(def);

    tblEmpStatus.setRowHeight(UE.WINDOW.getGraphics().getFontMetrics(def).getHeight());
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">
  // GEN-BEGIN:initComponents
  private void initComponents() {
    final int DefaultSize = GroupLayout.DEFAULT_SIZE;
    final int PreferredSize = GroupLayout.PREFERRED_SIZE;

    jspEmpStatus.setFocusable(false);
    jspEmpStatus.setMinimumSize(new java.awt.Dimension(0, 0));
    jspEmpStatus.setPreferredSize(new java.awt.Dimension(0, 0));

    tblEmpStatus.setAutoCreateRowSorter(true);
    tblEmpStatus.setModel(new EmRelStatusTableModel());
    tblEmpStatus.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    tblEmpStatus.setFillsViewportHeight(true);
    tblEmpStatus.setFocusable(false);
    tblEmpStatus.setRequestFocusEnabled(false);
    tblEmpStatus.setRowSelectionAllowed(false);
    jspEmpStatus.setViewportView(tblEmpStatus);
    jspEmpStatus.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 0, Color.GRAY));

    lblRacePic.setHorizontalAlignment(SwingConstants.CENTER);
    lblRacePic.setHorizontalTextPosition(SwingConstants.CENTER);
    lblRacePic.setIconTextGap(0);

    lblEmpName.setHorizontalAlignment(SwingConstants.RIGHT);
    lblEmperor.setHorizontalAlignment(SwingConstants.RIGHT);
    lblCredits.setHorizontalAlignment(SwingConstants.RIGHT);
    lblIncome.setHorizontalAlignment(SwingConstants.RIGHT);
    lblPopSupport.setHorizontalAlignment(SwingConstants.RIGHT);
    lblDilithium.setHorizontalAlignment(SwingConstants.RIGHT);
    lblMorale.setHorizontalAlignment(SwingConstants.RIGHT);
    lblShipsInBuild.setHorizontalAlignment(SwingConstants.RIGHT);

    GroupLayout pnlBasePropsLayout = new GroupLayout(pnlBaseProps);
    pnlBaseProps.setLayout(pnlBasePropsLayout);

    pnlBasePropsLayout.setHorizontalGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING)
      .addGroup(pnlBasePropsLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
          .addComponent(lblEmpName, DefaultSize, DefaultSize, Short.MAX_VALUE)
          .addComponent(lblEmperor, DefaultSize, DefaultSize, Short.MAX_VALUE)
          .addComponent(lblCredits, DefaultSize, DefaultSize, Short.MAX_VALUE)
          .addComponent(lblDilithium, DefaultSize, DefaultSize, Short.MAX_VALUE)
          .addComponent(lblTechLvl[0], DefaultSize, DefaultSize, Short.MAX_VALUE)
          .addComponent(lblTechLvl[3], DefaultSize, DefaultSize, Short.MAX_VALUE))
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, true)
          .addComponent(txtEmpName, Alignment.LEADING, DefaultSize, 334, Short.MAX_VALUE)
          .addComponent(txtEmperor, Alignment.LEADING)
          .addGroup(pnlBasePropsLayout.createSequentialGroup()
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, true)
              .addComponent(txtCredits, Alignment.LEADING)
              .addComponent(lblDilithiumValue, Alignment.LEADING, DefaultSize, DefaultSize, Short.MAX_VALUE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.TRAILING, false)
              .addComponent(lblIncome, DefaultSize, DefaultSize, Short.MAX_VALUE)
              .addComponent(lblShipsInBuild, DefaultSize, DefaultSize, Short.MAX_VALUE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, true)
              .addComponent(lblIncomeValue, Alignment.LEADING, DefaultSize, DefaultSize, Short.MAX_VALUE)
              .addComponent(lblShipsinBuildValue, Alignment.LEADING, DefaultSize, DefaultSize, Short.MAX_VALUE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.TRAILING, false)
              .addComponent(lblPopSupport, DefaultSize, DefaultSize, Short.MAX_VALUE)
              .addComponent(lblMorale, DefaultSize, DefaultSize, Short.MAX_VALUE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.TRAILING, false)
              .addComponent(lblPopSupportValue, Alignment.LEADING, DefaultSize, DefaultSize, Short.MAX_VALUE)
              .addComponent(lblMoraleValue, Alignment.LEADING, DefaultSize, DefaultSize, Short.MAX_VALUE)))
          .addGroup(pnlBasePropsLayout.createSequentialGroup()
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
              .addComponent(spiTechLvl[0])
              .addComponent(spiTechLvl[3]))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
              .addComponent(lblTechLvl[1])
              .addComponent(lblTechLvl[4]))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
              .addComponent(spiTechLvl[1])
              .addComponent(spiTechLvl[4]))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
              .addComponent(lblTechLvl[2])
              .addComponent(lblTechLvl[5]))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
              .addComponent(spiTechLvl[2])
              .addComponent(spiTechLvl[5]))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
              .addComponent(lblTechLvl[6]))
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.LEADING, false)
              .addComponent(spiTechLvl[6]))))
        .addContainerGap()
    ));
    pnlBasePropsLayout.setVerticalGroup(
      pnlBasePropsLayout.createParallelGroup(Alignment.LEADING).addGroup(pnlBasePropsLayout
          .createSequentialGroup().addContainerGap()
          .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.BASELINE)
            .addComponent(lblEmpName)
            .addComponent(txtEmpName, PreferredSize, DefaultSize, PreferredSize))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.BASELINE)
            .addComponent(lblEmperor)
            .addComponent(txtEmperor))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.BASELINE)
            .addComponent(lblCredits)
            .addComponent(txtCredits, PreferredSize, DefaultSize, PreferredSize)
            .addComponent(lblIncome)
            .addComponent(lblIncomeValue)
            .addComponent(lblPopSupport)
            .addComponent(lblPopSupportValue))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.BASELINE)
            .addComponent(lblDilithium)
            .addComponent(lblDilithiumValue)
            .addComponent(lblShipsInBuild)
            .addComponent(lblShipsinBuildValue)
            .addComponent(lblMorale)
            .addComponent(lblMoraleValue))
          .addGap(20)
          .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.BASELINE)
            .addComponent(lblTechLvl[0])
            .addComponent(spiTechLvl[0])
            .addComponent(lblTechLvl[1])
            .addComponent(spiTechLvl[1])
            .addComponent(lblTechLvl[2])
            .addComponent(spiTechLvl[2])
            .addComponent(lblTechLvl[6])
            .addComponent(spiTechLvl[6]))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(pnlBasePropsLayout.createParallelGroup(Alignment.BASELINE)
            .addComponent(lblTechLvl[3])
            .addComponent(spiTechLvl[3])
            .addComponent(lblTechLvl[4])
            .addComponent(spiTechLvl[4])
            .addComponent(lblTechLvl[5])
            .addComponent(spiTechLvl[5]))
    ));

    knownRacesModel = new MinorRaceListModel();
    lstKnownRaces.setModel(knownRacesModel);
    lstKnownRaces.setCellRenderer(new CheckListRenderer());
    lstKnownRaces.setLayoutOrientation(JList.HORIZONTAL_WRAP);
    lstKnownRaces.setSelectionBackground(new java.awt.Color(255, 255, 255));
    lstKnownRaces.setSelectionForeground(new java.awt.Color(0, 0, 0));
    lstKnownRaces.setVisibleRowCount(-1); // auto-calculate column count
    lstKnownRaces.setSelectionModel(new DefaultListSelectionModel() {
      boolean gestureStarted = false;

      // removeSelectionInterval somehow doesn't work properly here
      // it often clears additional other selections
      // therefore the  model is updated directly
      @Override
      public void setSelectionInterval(int index0, int index1) {
        if (!gestureStarted) {
          KnownRaceCheckItem item = lstKnownRaces.getModel().getElementAt(index0);
          item.toggleSelected();
          gestureStarted = true;
          repaint();
        }
      }

      @Override
      public void setValueIsAdjusting(boolean isAdjusting) {
        if (isAdjusting == false)
          gestureStarted = false;
      }
    });
    jspKnownRaces.setViewportView(lstKnownRaces);

    btnNext.setText(">>>");
    btnNext.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnNextActionPerformed(evt);
      }
    });

    btnPrev.setText("<<<");
    btnPrev.setEnabled(false);
    btnPrev.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        btnPrevActionPerformed(evt);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addGroup(layout.createParallelGroup(Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
              .addComponent(pnlBaseProps, PreferredSize, DefaultSize, Short.MAX_VALUE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(lblRacePic, PreferredSize, 155, PreferredSize))
            .addGroup(layout.createSequentialGroup()
              .addGroup(layout.createParallelGroup(Alignment.LEADING)
                .addComponent(lblEmpStatus, DefaultSize, 133, Short.MAX_VALUE)
                .addComponent(jspEmpStatus, DefaultSize, 506, Short.MAX_VALUE)
                .addComponent(lblKnownRaces, DefaultSize, DefaultSize, Short.MAX_VALUE)
                .addComponent(jspKnownRaces, DefaultSize, 506, Short.MAX_VALUE)))
            .addGroup(layout.createSequentialGroup()
              .addComponent(btnPrev)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 392, Short.MAX_VALUE)
              .addComponent(btnNext)))
          .addContainerGap()
    ));
    layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
      .addGroup(layout.createSequentialGroup().addGap(11, 11, 11)
        .addGroup(layout.createParallelGroup(Alignment.LEADING)
          .addComponent(pnlBaseProps, PreferredSize, DefaultSize, PreferredSize)
          .addComponent(lblRacePic, PreferredSize, PreferredSize, PreferredSize))
        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(lblEmpStatus)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jspEmpStatus, DefaultSize, 80, Short.MAX_VALUE)
        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(lblKnownRaces)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jspKnownRaces, DefaultSize, 118, Short.MAX_VALUE)
        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(layout.createParallelGroup(Alignment.LEADING)
          .addComponent(btnPrev)
          .addComponent(btnNext))
        .addGap(11, 11, 11)
    ));
  }// </editor-fold>//GEN-END:initComponents

  private void btnNextActionPerformed(ActionEvent evt) {
    // save
    try {
      finalWarning();
    } catch (Exception ex) {
      // really bad stuff happening - bail
      Dialogs.displayError(ex);
      return;
    }

    if (empire < 4)
      empire++;

    btnNext.setEnabled(empire < 4);
    btnPrev.setEnabled(empire > 0);

    // load new
    refreshData();
  }

  private void btnPrevActionPerformed(ActionEvent evt) {
    // save
    try {
      finalWarning();
    } catch (Exception ex) {
      // really bad stuff happening - bail
      Dialogs.displayError(ex);
      return;
    }

    if (empire > 0)
      empire--;

    btnNext.setEnabled(empire < 4);
    btnPrev.setEnabled(empire > 0);

    // load new
    refreshData();
  }

  @Override
  public boolean hasChanges() {
    return EMPSINFO.madeChanges() || TECHINFO.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    game.checkChanged(EMPSINFO, changes);
    game.checkChanged(TECHINFO, changes);
  }

  @Override
  public void reload() throws IOException {
    EMPSINFO = (EmpsInfo) game.getInternalFile(CSavFiles.EmpsInfo, true);
    TECHINFO = (TechInfo) game.getInternalFile(CSavFiles.TechInfo, true);
    refreshData();
  }

  @Override
  public void finalWarning() {
    EMPSINFO.setCreditsAvailable(empire, Integer.parseInt(txtCredits.getText()));
    EMPSINFO.setEmperor(empire, txtEmperor.getText());
    EMPSINFO.setEmpireTitle(empire, this.txtEmpName.getText());

    TechEntry techEntry = TECHINFO.getTechEntry((short)empire);
    for (int i = 0; i < CStbof.NUM_TECH_FIELDS; ++i) {
      short lvl = ((Number)spiTechLvl[i].getValue()).shortValue();
      techEntry.setTechLvl(i, lvl);
    }
  }

  private void refreshData() {
    try {
      // editable properties
      txtEmpName.setText(EMPSINFO.getEmpireTitle(empire));
      txtEmperor.setText(EMPSINFO.getEmperor(empire));
      txtCredits.setText(Integer.toString(EMPSINFO.getCreditsAvailable(empire)));

      val techStats = TECHINFO.getEmpireStats((short)empire);
      for (int i = 0; i < CStbof.NUM_TECH_FIELDS; ++i)
        spiTechLvl[i].setValue(techStats.techLevels[i]);

      // picture
      TargaImage tga = STBOF.getTargaImage(RACE.getPic(empire), LoadFlags.UNCACHED);
      lblRacePic.setIcon(new ImageIcon(tga.getImage(AlphaMode.Opaque)));

      // informative read-only properties
      lblIncomeValue.setText(Integer.toString(EMPSINFO.getIncome(empire)));
      lblPopSupportValue.setText(Integer.toString(EMPSINFO.getShipPopSupport(empire)));
      lblDilithiumValue.setText(Integer.toString(EMPSINFO.getDilithiumIncome(empire)));
      int morale = EMPSINFO.getEmpireMorale(empire);
      lblMoraleValue.setText(LexHelper.mapMoraleLvl(morale));
      int numShips = EMPSINFO.getNumberOfShipsUnderConstruction(empire);
      lblShipsinBuildValue.setText(Integer.toString(numShips));
    } catch (Exception e) {
      Dialogs.displayError(e);
    }

    tblEmpStatus.tableChanged(null);
    knownRacesModel.refresh();
    lstKnownRaces.updateUI();
  }

  private class EmRelStatusTableModel extends AbstractTableModel {

    @Override
    public int getRowCount() {
      return 4;
    }

    @Override
    public int getColumnCount() {
      return 4;
    }

    @Override
    public String getColumnName(int column) {
      switch (column) {
        case 0:
          return strEmpireColumn;
        case 1:
          return strTreatyColumn;
        case 2:
          return strOpenBorderColumn;
        default:
          return strAllowsTradeColumn;
      }
    }

    @Override
    public boolean isCellEditable(int arg0, int arg1) {
      return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      rowIndex = (empire <= rowIndex) ? rowIndex + 1 : rowIndex;

      switch (columnIndex) {
        case 0:
          return RACE.getName(rowIndex);
        case 1:
          int treaty = EMPSINFO.getTreatyLabel(empire, rowIndex);
          return LexHelper.mapRelationship(treaty);
        case 2:
          return (EMPSINFO.getTerritoryAccess(empire, rowIndex) == 0) ? strNo : strYes;
        default:
          return (EMPSINFO.getTradeAccess(empire, rowIndex) == 0) ? strNo : strYes;
      }
    }
  }

  private class MinorRaceListModel extends AbstractListModel<KnownRaceCheckItem> {

    private ArrayList<KnownRaceCheckItem> items = new ArrayList<KnownRaceCheckItem>(
        RACE.getNumberOfEntries());

    public MinorRaceListModel() {
      for (int i = 0; i < RACE.getNumberOfEntries(); i++) {
        items.add(new KnownRaceCheckItem(RACE, EMPSINFO, empire, i, true));
      }
    }

    @Override
    public int getSize() {
      return items.size();
    }

    @Override
    public KnownRaceCheckItem getElementAt(int index) {
      return items.get(index);
    }

    public void refresh() {
      for (KnownRaceCheckItem item : items)
        item.updateEmpire(empire);
    }
  }
}
