package ue.gui.sav.map.menu;

import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import lombok.val;
import ue.edit.exe.trek.Trek;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.exception.KeyNotFoundException;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;
import ue.util.data.ID;

public abstract class MapGUISectorMenu {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  /**
   * Returns the menu shown when a sector is right-clicked
   *
   * @param map
   * @param viewMode -2 = none, -1 = all, else the race id
   * @param sector
   * @return
   * @throws IOException
   * @throws KeyNotFoundException
   */
  public static ArrayList<JMenuItem> getMenuItems(MapPanel map, SavGame sav,
    Stbof stbof, Trek trek, int sector) throws IOException, KeyNotFoundException {

    ArrayList<JMenuItem> items = new ArrayList<JMenuItem>();
    int viewMode = sav.getEmpireViewMode();

    if (viewMode >= -1) {
      val sgif = sav.files();
      val stif = stbof.files();

      addAddMenu(items, map, sgif, stif, sector, viewMode);
      // Changing the sector claim doesn't work yet, @see addSectorClaimMenu.
      // addSectorClaimMenu(items, map, sgif, stif, sector);
      addExplorationMenu(items, map, sgif, sector, viewMode);
      addSystemMenu(items, map, sav, stbof, trek, sector);
      addStellarObjectMenu(items, map, sgif, stbof, trek, sector);
      addOutpostMenu(items, map, sgif, stif, sector);
      addTaskForceMenu(items, map, sgif, stif, sector);
      addMaintenanceMenu(items, map, sgif, stif, sector);
      addMoveMenu(items, map, sector);
      createRemoveAllItem(items, map, sgif, stif, sector);
    }

    return items;
  }

  private static void addAddMenu(ArrayList<JMenuItem> items, MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sector, int viewMode) throws KeyNotFoundException, IOException {

    MapGUIAddMenu mnuAdd = new MapGUIAddMenu(map, sgif, stif, sector);
    boolean visible = viewMode < 0 || sgif.sectorLst().getExploredBy(sector, viewMode) ||
      sgif.sectorLst().getScannedBy(sector, viewMode);

    // add mnuAdd menu to the context menu
    if (mnuAdd.getItemCount() > 0) {
      mnuAdd.setEnabled(visible);
      items.add(mnuAdd);
    }
  }

  private static void addExplorationMenu(ArrayList<JMenuItem> items, MapPanel map,
    SavGameInterface sgif, int sector, int viewMode) throws IOException {

    JMenu mnuExplorationStatus = new JMenu("Exploration status");
    mnuExplorationStatus.setEnabled(viewMode >= 0);

    if (viewMode >= 0) {
      final JCheckBoxMenuItem mnuExplored = new JCheckBoxMenuItem("Explored",
        sgif.sectorLst().getExploredBy(sector, viewMode));

      mnuExplored.addActionListener(new CBActionListener(evt -> {
        boolean value = mnuExplored.isSelected();
        sgif.sectorLst().setExploredBy(sector, viewMode, value);
        map.refreshMap();
      }));
      mnuExplorationStatus.add(mnuExplored);

      final JCheckBoxMenuItem mnuScanned = new JCheckBoxMenuItem("Scanned",
        sgif.sectorLst().getScannedBy(sector, viewMode));

      mnuScanned.addActionListener(new CBActionListener(evt -> {
        boolean value = mnuScanned.isSelected();
        sgif.sectorLst().setScannedBy(sector, viewMode, value);
        map.refreshMap();
      }));
      mnuExplorationStatus.add(mnuScanned);
    }

    items.add(mnuExplorationStatus);
  }

  // TODO: Changing claim in sector.lst has no effect in the game (?)
  @SuppressWarnings("unused")
  private static void addSectorClaimMenu(ArrayList<JMenuItem> items, MapPanel map,
    SavGameInterface sgif, StbofFileInterface stif, int sector) throws IOException {

    // skip systems claimed by starbase presence
    int starbaseId = sgif.sectorLst().getStarbase(sector);
    if (starbaseId >= 0)
      return;

    // skip inhabited systems
    int systemId = sgif.sectorLst().getSystem(sector);
    boolean inhabited = systemId >= 0 && sgif.systInfo().getSystem(systemId).getPopulation() > 0;
    if (inhabited)
      return;

    JMenu mnuClaim = new JMenu("Ownership claim");

    ArrayList<ID> ids  = new ArrayList<ID>();
    for (int i = 0; i < NUM_EMPIRES; i++) {
        ids.add(new ID(stif.raceTools().mapRace(i), i));
    }

    if (ids.size() > 0) {
      val editClaim = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        short id = Short.parseShort(text.substring(0, text.indexOf(":")));

        short mask = sgif.sectorLst().getOwnerMask(sector);
        mask = (short)(mask ^ (1 << id));
        sgif.sectorLst().setOwnerClaim(sector, mask);

        // if (mnu.isSelected())
        //   map.sphOfInf[id].setInfluence(sector, 10);
        // else
        //   map.sphOfInf[id].setInfluence(sector, 0);

        map.refreshMap();
      });

      for (int i = 0; i < ids.size(); i++) {
        ID id = ids.get(i);
        JCheckBoxMenuItem mnuRace = new JCheckBoxMenuItem(id.ID + ": " + id.toString());
        mnuRace.setSelected((sgif.sectorLst().getOwnerMask(sector) & (1 << id.ID)) != 0);
        mnuRace.addActionListener(editClaim);
        mnuClaim.add(mnuRace);
      }
    }

    items.add(mnuClaim);
  }

  private static void addSystemMenu(ArrayList<JMenuItem> items, MapPanel map,
    SavGame sav, Stbof stbof, Trek trek, int sectorIndex) throws IOException {

    val sectorLst = sav.files().sectorLst();

    if (sectorLst.getSystem(sectorIndex) >= 0) {
      short systemId = sectorLst.getSystem(sectorIndex);
      items.add(new MapGUISystemMenu(map, sav, stbof, trek, systemId));

    }
  }

  private static void addStellarObjectMenu(ArrayList<JMenuItem> items, MapPanel map,
    SavGameInterface sgif, Stbof stbof, Trek trek, int sectorIndex) throws IOException {

    if (sgif.sectorLst().getStellarObject(sectorIndex) >= 0) {
      short stellId = sgif.sectorLst().getStellarObject(sectorIndex);
      items.add(new MapGUIStellarMenu(map, sgif, stbof, trek, stellId));
    }
  }

  private static void addOutpostMenu(ArrayList<JMenuItem> items, MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sector) throws KeyNotFoundException, IOException {

    if (sgif.sectorLst().getStarbase(sector) >= 0) {
      short outpostId = sgif.sectorLst().getStarbase(sector);
      items.add(new MapGUIOutpostMenu(map, sgif, stif, outpostId));
    }
  }

  private static void addTaskForceMenu(ArrayList<JMenuItem> items, MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sectorIndex) throws KeyNotFoundException, IOException {

    if (sgif.sectorLst().hasTaskForces(sectorIndex))
      items.add(new MapGUITaskForceSelectionMenu(map, sgif, stif, sectorIndex));
  }

  private static void addMaintenanceMenu(ArrayList<JMenuItem> items, MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sectorIndex) throws KeyNotFoundException, IOException {

    JMenuItem mnuClearAITasks = new JMenuItem("Remove Sector AI Tasks");
    mnuClearAITasks.addActionListener(new CBActionListener(evt ->
      sgif.aiTaskTools().removeSectorTasks(sectorIndex)));

    JMenuItem mnuClearOrders = new JMenuItem("Remove Sector Orders");
    mnuClearOrders.addActionListener(new CBActionListener(evt ->
      sgif.sectorTools().clearAllSectorOrders(sectorIndex)));

    JMenuItem mnuClearResults = new JMenuItem("Remove Sector Results");
    mnuClearResults.addActionListener(new CBActionListener(evt ->
      sgif.sectorTools().clearAllSectorResults(sectorIndex)));

    JMenu mnuOrders = new JMenu("Maintenance");
    mnuOrders.add(mnuClearAITasks);
    mnuOrders.add(mnuClearOrders);
    mnuOrders.add(mnuClearResults);
    items.add(mnuOrders);
  }

  private static void addMoveMenu(ArrayList<JMenuItem> items, MapPanel map, int sectorIndex) {
    JMenuItem mnuMove = new JMenuItem("Move All");
    mnuMove.addActionListener(new CBActionListener(evt -> map.switchSectors(sectorIndex)));
    items.add(mnuMove);
  }

  private static void createRemoveAllItem(ArrayList<JMenuItem> items, MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sectorIndex) {

    JMenuItem mnuRemove = new JMenuItem("Remove All");
    mnuRemove.addActionListener(new CBActionListener(evt -> {
      sgif.sectorTools().clearFullSectorAsync(sectorIndex);
      map.refreshMap();
    }));
    items.add(mnuRemove);
  }

}
