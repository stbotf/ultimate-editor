package ue.gui.sav.map.menu;

import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import lombok.val;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.sav.SavGameInterface;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;

public class MapGUITaskForceMenu extends JMenu {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public MapGUITaskForceMenu(MapPanel map, SavGameInterface sgif, StbofFileInterface stif,
    int taskForceId) throws KeyNotFoundException, IOException {
    super("Task Force");

    ArrayList<JMenuItem> items = MapGUITaskForceSelectionMenu.getMenuItems(map, sgif, stif, taskForceId);
    for (val item : items)
      add(item);
  }

  /**
   * Returns the menu show when a task force is right-clicked
   *
   * @param map
   * @param taskForceId
   * @return the menu items
   * @throws FileNotFoundException
   * @throws InvalidArgumentsException
   * @throws IOException
   */
  public static ArrayList<JMenuItem> getMenuItems(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, short taskForceId) throws InvalidArgumentsException, IOException {

    ArrayList<JMenuItem> items = new ArrayList<JMenuItem>();
    items.add(createOwnerMenu(map, sgif, stif, taskForceId));
    items.add(createClearOrdersItem(map, sgif, taskForceId));
    items.add(createClearResultsItem(map, sgif, taskForceId));
    items.add(createMoveItem(map, taskForceId));
    items.add(createRemoveItem(map, sgif, taskForceId));

    return items;
  }

  private static JMenuItem createOwnerMenu(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, short taskForceId) throws KeyNotFoundException, IOException {

    JMenu mnuChangeOwner = new JMenu("Change Owner");
    int tfOwner = sgif.gwtForce().taskForce(taskForceId).getOwnerId();

    ActionListener changeOwner = new CBActionListener(evt -> {
      JCheckBoxMenuItem cb = (JCheckBoxMenuItem) evt.getSource();
      String text = cb.getText();
      short raceId = Short.parseShort(text.substring(0, text.indexOf(":")));

      sgif.taskForceTools().setTaskForceOwner(taskForceId, raceId);
      map.refreshMap();
    });

    for (int r = 0; r < NUM_EMPIRES; ++r) {
      JCheckBoxMenuItem mnuCheckItem = new JCheckBoxMenuItem(r + ": " +  stif.raceTools().mapRace(r));
      mnuCheckItem.setSelected(tfOwner == r);
      mnuCheckItem.addActionListener(changeOwner);
      mnuChangeOwner.add(mnuCheckItem);
    }

    return mnuChangeOwner;
  }

  private static JMenuItem createClearOrdersItem(MapPanel map, SavGameInterface sgif, short taskForceId) {
    JMenuItem mnuClearOrders = new JMenuItem("Clear Orders");
    mnuClearOrders.addActionListener(new CBActionListener(evt -> {
      sgif.taskForceTools().clearTaskForceOrders(taskForceId);
      map.refreshMap();
    }));

    return mnuClearOrders;
  }

  private static JMenuItem createClearResultsItem(MapPanel map, SavGameInterface sgif, short taskForceId) {
    JMenuItem mnuClearOrders = new JMenuItem("Clear Results");
    mnuClearOrders.addActionListener(new CBActionListener(evt -> {
      sgif.taskForceTools().clearTaskForceResults(taskForceId);
    }));

    return mnuClearOrders;
  }

  private static JMenuItem createMoveItem(MapPanel map, short taskForceId) {
    JMenuItem mnuMove = new JMenuItem("Move");
    mnuMove.addActionListener(new CBActionListener(evt -> map.moveTaskForce(taskForceId)));
    return mnuMove;
  }

  private static JMenuItem createRemoveItem(MapPanel map, SavGameInterface sgif, short taskForceId) {
    JMenuItem mnuRemove = new JMenuItem("Remove");
    mnuRemove.addActionListener(new CBActionListener(evt -> {
      sgif.taskForceTools().removeTaskForce(taskForceId);
      map.refreshMap();
    }));
    return mnuRemove;
  }

}
