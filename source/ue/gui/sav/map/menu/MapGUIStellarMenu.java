package ue.gui.sav.map.menu;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import lombok.val;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.seg.map.StellTypeDetailsDescMap;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.tools.StellarTools;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.data.StellEntry;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;
import ue.util.data.ID;

public class MapGUIStellarMenu extends JMenu {

  public MapGUIStellarMenu(MapPanel map, SavGameInterface sgif,
    Stbof stbof, Trek trek, short stellId) throws IOException {
    super("Stellar Object");

    val items = getMenuItems(map, sgif, stbof, trek, stellId);
    for (val item : items)
      add(item);
  }

  /**
   * Returns the menu shown when a stellar object is right clicked
   *
   * @param map
   * @param stellar object
   * @return
   * @throws IOException
   */
  public static List<JMenuItem> getMenuItems(MapPanel map, SavGameInterface sgif,
    Stbof stbof, Trek trek, short stellId) throws IOException {

    val stif = stbof.files();
    ArrayList<JMenuItem> items = new ArrayList<JMenuItem>();

    JMenu mnuEdit = new JMenu("Edit");
    mnuEdit.add(createNameItem(map, sgif, stellId));
    mnuEdit.add(createTypeMenu(map, sgif, stif, stellId));
    addGraphicsMenu(mnuEdit, map, sgif, stbof, trek, stellId);
    addEndpointMenu(mnuEdit, map, sgif, stellId);
    items.add(mnuEdit);

    addMoveMenu(items,  map, stellId);
    addRemoveMenu(items, map, sgif, stellId);

    return items;
  }

  // #region edit menues

  private static JMenuItem createNameItem(MapPanel map, SavGameInterface sgif, short stellId) {
    JMenuItem mnuName = new JMenuItem("Name");

    mnuName.addActionListener(new CBActionListener(evt -> {
      StellEntry stellObj = sgif.stellInfo().getStellarObject(stellId);
      Object retVal = JOptionPane.showInputDialog(UE.WINDOW, "Enter new name:", stellObj.getName());
      if (retVal != null) {
        stellObj.setName(retVal.toString());
        map.refreshMap();
      }
    }));

    return mnuName;
  }

  private static JMenu createTypeMenu(MapPanel map, SavGameInterface sgif, StbofFileInterface stif, short stellId) throws IOException {
    JMenu mnuType = new JMenu("Type");
    StellEntry stellObj = sgif.stellInfo().getStellarObject(stellId);

    val editType = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      String text = mnu.getText();
      short id = Short.parseShort(text.substring(0, text.indexOf(":")));

      int[] dest = stellObj.getPosition();
      dest[1] *= sgif.sectorLst().getHSectors();
      dest[1] += dest[0];

      String sector = sgif.sectorLst().getDescription(dest[1], false);
      sector = sector.substring(sector.indexOf(" ") + 1);
      stellObj.setName(text.substring(text.indexOf(":") + 2) + " " + sector);
      stellObj.setType(id);
      stellObj.setAnimation(stif.objStruc().getGraph(id));

      map.refreshMap();
    });

    StellarTools stTools = stif.stellarTools();
    ArrayList<ID> ids = stTools.listStellarTypes();

    // add stellar type menu selections
    for (int i = 0; i < ids.size(); i++) {
      ID id = ids.get(i);
      JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(id.ID + ": " + id.toString());
      mnuItem.setSelected(stellObj.getType() == id.ID);
      mnuItem.addActionListener(editType);
      mnuType.add(mnuItem);
    }

    return mnuType;
  }

  private static JMenu createGraphicsMenu(MapPanel map, SavGameInterface sgif,
    Stbof stbof, short stellId) throws IOException {

    JMenu mnuGraph = new JMenu("Graphics");
    StellEntry stellObj = sgif.stellInfo().getStellarObject(stellId);

    val editGraph = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      stellObj.setAnimation(mnu.getText());
      map.refreshMap();
    });

    Collection<String> anis = stbof.getFileNames(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (name.endsWith(".ani")) {
          name = name.substring(0, name.lastIndexOf("."));

          return stbof.hasInternalFile(name + "map.tga") &&
              stbof.hasInternalFile(name + "maps.tga");
        }
        return false;
      }
    });

    ArrayList<JMenuItem> graphItems = new ArrayList<JMenuItem>();

    for (String ani : anis) {
      ani = ani.substring(0, ani.lastIndexOf("."));
      JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(ani);
      mnuItem.setSelected(stellObj.getAnimation().equalsIgnoreCase(ani));
      mnuItem.addActionListener(editGraph);
      graphItems.add(mnuItem);
    }

    Collections.sort(graphItems, new Comparator<JMenuItem>() {
      @Override
      public int compare(JMenuItem o1, JMenuItem o2) {
        return o1.getText().compareTo(o2.getText());
      }
    });

    for (int i = 0; i < graphItems.size(); i++) {
      mnuGraph.add(graphItems.get(i));
    }

    return mnuGraph;
  }

  private static void addGraphicsMenu(JMenu menu, MapPanel map, SavGameInterface sgif,
    Stbof stbof, Trek trek, short stellId) throws IOException {

    val nhcg = (StellTypeDetailsDescMap) trek.getSegment(StellTypeDetailsDescMap.SEGMENT_DEFINITION, false);
    StellEntry stellObj = sgif.stellInfo().getStellarObject(stellId);

    if (stellObj.getType() != StellarType.Nebula || !nhcg.isNebulaHardCoded())
      menu.add(createGraphicsMenu(map, sgif, stbof, stellId));
  }

  private static JMenu createEndpointMenu(MapPanel map, SavGameInterface sgif,
    short stellId, ArrayList<Integer> destinations) throws IOException {

    JMenu mnuLink = new JMenu("Endpoint");
    StellEntry stellObj = sgif.stellInfo().getStellarObject(stellId);

    val editLink = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      String text = mnu.getText();
      short linkId = Short.parseShort(text.substring(0, text.indexOf(":")));

      if (linkId >= 0) {
        // ask whether to connect both the wormholes
        int ret = JOptionPane.showConfirmDialog(UE.WINDOW,
          "Create a two-way link between the wormholes?");

        if (ret == JOptionPane.CANCEL_OPTION) {
          return;
        } else if (ret == JOptionPane.YES_OPTION) {
          sgif.stellInfo().getStellarObject(linkId).setLink(stellId);
        }
      }

      stellObj.setLink(linkId);
      map.refreshMap();
    });

    JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem("-1: None");
    mnuItem.setSelected(stellObj.getLink() < 0);
    mnuItem.addActionListener(editLink);
    mnuLink.add(mnuItem);

    for (int i = 0; i < destinations.size(); i++) {
      int index = destinations.get(i);
      StellEntry destObj = sgif.stellInfo().getStellarObject(index);
      int[] dest = destObj.getPosition();
      String itemStr = index + ": " + destObj.getName() + " ("
        + sgif.sectorLst().getDescription(dest[1] * sgif.sectorLst().getHSectors() + dest[0], false)
        + ")";

      mnuItem = new JCheckBoxMenuItem(itemStr);
      mnuItem.setSelected(stellObj.getLink() == index);
      mnuItem.addActionListener(editLink);
      mnuLink.add(mnuItem);
    }

    return mnuLink;
  }

  private static void addEndpointMenu(JMenu menu, MapPanel map,
    SavGameInterface sgif, short stellId) throws IOException {

    StellEntry stellObj = sgif.stellInfo().getStellarObject(stellId);
    if (stellObj.getType() == StellarType.WormHole) {
      ArrayList<Integer> destinations = new ArrayList<Integer>();

      // fill dests
      for (int i = 0; i < sgif.stellInfo().getNumberOfEntries(); ++i) {
        if (i != stellId) {
          StellEntry destObj = sgif.stellInfo().getStellarObject(i);
          if (destObj.getType() == StellarType.WormHole)
            destinations.add(i);
        }
      }

      if (!destinations.isEmpty())
        menu.add(createEndpointMenu(map, sgif, stellId, destinations));
    }
  }

  // #endregion edit menues

  // #region move menues

  private static void addMoveMenu(ArrayList<JMenuItem> items, MapPanel map, short stellId) {
    JMenuItem mnuMove = new JMenuItem("Move");
    mnuMove.addActionListener(new CBActionListener(evt -> map.moveStellarObject(stellId)));
    items.add(mnuMove);
  }

  // #endregion move menues

  // #region remove menues

  private static void addRemoveMenu(ArrayList<JMenuItem> items, MapPanel map, SavGameInterface sgif, short stellId) {
    JMenuItem mnuRemove = new JMenuItem("Remove");

    mnuRemove.addActionListener(new CBActionListener(evt -> {
      int[] pos = sgif.stellInfo().getStellarObject(stellId).getPosition();
      int sector = pos[1] * sgif.sectorLst().getHSectors() + pos[0];
      sgif.stellarTools().removeFromSector(sector);
      map.refreshMap();
    }));

    items.add(mnuRemove);
  }

  // #endregion remove menues

}
