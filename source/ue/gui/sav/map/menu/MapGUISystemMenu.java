package ue.gui.sav.map.menu;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import lombok.val;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.tools.StellarTools;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.SystInfo;
import ue.gui.sav.emp.StrcInfoGUI;
import ue.gui.sav.emp.SystemsInfoGUI;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;
import ue.util.data.ID;

/**
 * The context menu shown when a system is right-clicked.
 */
public class MapGUISystemMenu extends JMenu {

  public MapGUISystemMenu(MapPanel map, SavGame sav, Stbof stbof,
    Trek trek, short systemId) throws IOException {
    super("Star System");

    val items = getMenuItems(map, sav, stbof, trek, systemId, false);
    for (val item : items)
      this.add(item);
  }

  public static List<JMenuItem> getMenuItems(MapPanel map, SavGame sav,
    Stbof stbof, Trek trek, short systemId, boolean systemLabel) throws IOException {

    ArrayList<JMenuItem> items = new ArrayList<>();
    items.add(createSysEditItem(map, sav, stbof, systemId));

    addEditMenu(items, map, sav, stbof, trek, systemId, systemLabel ? "Edit System" : "Edit");
    addMaintenanceMenu(items, map, sav.files(), systemId);
    addMoveMenu(items, map, systemId, systemLabel ? "Move System" : "Move");
    addEliminateMenu(items, map, sav.files(), systemId, systemLabel ? "Eliminate System" : "Eliminate");
    addRemoveMenu(items, map, sav.files(), systemId, systemLabel ? "Remove System" : "Remove");

    return items;
  }

  // #region menus

  private static void addEditMenu(List<JMenuItem> items, MapPanel map, SavGame sav,
    Stbof stbof, Trek trek, short systemId, String label) throws IOException {

    val sgif = sav.files();
    val systInfo = sgif.systInfo();

    JMenu mnuEdit = new JMenu(label);
    mnuEdit.add(createNameItem(map, sgif, systemId));
    mnuEdit.add(createTypeMenu(map, stbof.files(), systInfo, systemId));
    mnuEdit.add(createDilithiumMenu(map,systInfo, systemId));
    mnuEdit.add(createGraphicsMenu(map, stbof, systInfo, systemId));
    addStrcListItems(mnuEdit, sav, stbof, trek, systemId);
    items.add(mnuEdit);
  }

  private static void addMaintenanceMenu(List<JMenuItem> items, MapPanel map,
    SavGameInterface sgif, short systemId) throws IOException {

    JMenuItem mnuRemoveTasks = new JMenuItem("Remove AITasks");
    mnuRemoveTasks.addActionListener(new CBActionListener(evt -> {
      sgif.aiTaskTools().removeSystemTasks(systemId);
    }));

    JMenuItem mnuResetSubTasks = new JMenuItem("Reset TskSh & TskSy");
    mnuResetSubTasks.addActionListener(new CBActionListener(evt -> {
      sgif.aiTaskTools().resetSystemSubTasks(systemId);
    }));

    JMenuItem mnuCancelIncRoutes = new JMenuItem("Cancel Incoming Trade Routes");
    mnuCancelIncRoutes.addActionListener(new CBActionListener(evt -> {
      sgif.systemTools().cancelIncomingTradeRoutes(systemId);
    }));

    JMenuItem mnuCancelOutRoutes = new JMenuItem("Cancel Outgoing Trade Routes");
    mnuCancelOutRoutes.addActionListener(new CBActionListener(evt -> {
      sgif.systemTools().cancelOutgoingTradeRoutes(systemId);
    }));

    JMenuItem mnuCancelAllRoutes = new JMenuItem("Cancel All Trade Routes");
    mnuCancelAllRoutes.addActionListener(new CBActionListener(evt -> {
      sgif.systemTools().cancelSystemTradeRoutes(systemId);
    }));

    JMenu mnuAI = new JMenu("Maintenance");
    mnuAI.add(mnuRemoveTasks);
    mnuAI.add(mnuResetSubTasks);
    mnuAI.add(mnuCancelIncRoutes);
    mnuAI.add(mnuCancelOutRoutes);
    mnuAI.add(mnuCancelAllRoutes);
    items.add(mnuAI);
  }

  private static void addMoveMenu(List<JMenuItem> items, MapPanel map, short systemId, String label) {
    JMenuItem mnuMove = new JMenuItem(label);
    mnuMove.addActionListener(new CBActionListener(evt -> map.moveSystem(systemId)));
    items.add(mnuMove);
  }

  private static void addEliminateMenu(List<JMenuItem> items, MapPanel map,
    SavGameInterface sgif, short systemId, String label) throws IOException {

    JMenuItem mnuEliminate = new JMenuItem(label);
    mnuEliminate.addActionListener(new CBActionListener(evt -> {
      sgif.systemTools().eliminateSystem(systemId);
      map.refreshMap();
    }));
    items.add(mnuEliminate);
  }

  private static void addRemoveMenu(List<JMenuItem> items, MapPanel map,
    SavGameInterface sgif, short systemId, String label) throws IOException {

    JMenuItem mnuRemove = new JMenuItem(label);
    mnuRemove.addActionListener(new CBActionListener(evt -> {
      sgif.systemTools().removeSystemAsync(systemId);
      map.refreshMap();
    }));
    items.add(mnuRemove);
  }

  // #endregion menus

  // #region sub-menus

  private static JMenuItem createNameItem(MapPanel map, SavGameInterface sgif, short systemId)
    throws IOException {

    JMenuItem mnuName = new JMenuItem("Name");
    mnuName.addActionListener(new CBActionListener(evt -> {
      val system = sgif.systInfo().getSystem(systemId);
      Object retVal = JOptionPane.showInputDialog(UE.WINDOW, "Enter new name:", system.getName());
      if (retVal != null) {
        system.setName(retVal.toString());
        map.refreshMap();
      }
    }));

    return mnuName;
  }

  private static JMenu createTypeMenu(MapPanel map, StbofFileInterface stif, SystInfo systInfo, short systemId)
    throws IOException {

    val system = systInfo.getSystem(systemId);
    StellarTools stTools = stif.stellarTools();
    JMenu mnuType = new JMenu("Type");

    val editType = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      String text = mnu.getText();
      short id = Short.parseShort(text.substring(0, text.indexOf(":")));

      system.setStarType(id);
      system.setAni(stif.objStruc().getGraph(id) + ".ani");
      map.refreshMap();
    });

    // add system type menu selections
    ArrayList<ID> ids = stTools.listSystemTypes();
    for (int i = 0; i < ids.size(); i++) {
      ID id = ids.get(i);
      JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(id.ID + ": " + id.toString());
      mnuItem.setSelected(system.getStarType() == id.ID);
      mnuItem.addActionListener(editType);
      mnuType.add(mnuItem);
    }

    return mnuType;
  }

  private static JMenu createDilithiumMenu(MapPanel map, SystInfo systInfo, short systemId) {

    val system = systInfo.getSystem(systemId);
    JMenu mnuDil = new JMenu("Dilithium");

    JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem("Yes");
    mnuItem.setSelected(system.hasDilithium());
    mnuItem.addActionListener(new CBActionListener(evt -> {
      system.setDilithium(true);
      map.refreshMap();
    }));
    mnuDil.add(mnuItem);

    mnuItem = new JCheckBoxMenuItem("No");
    mnuItem.setSelected(!system.hasDilithium());
    mnuItem.addActionListener(new CBActionListener(evt -> {
      system.setDilithium(false);
      map.refreshMap();
    }));
    mnuDil.add(mnuItem);

    return mnuDil;
  }

  private static JMenu createGraphicsMenu(MapPanel map, Stbof stbof, SystInfo systInfo, short systemId) {

    val system = systInfo.getSystem(systemId);
    JMenu mnuGraph = new JMenu("Graphics");

    val editGraph = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      system.setAni(mnu.getText() + ".ani");
      map.refreshMap();
    });

    Collection<String> anis = stbof.getFileNames(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (name.endsWith(".ani")) {
          name = name.substring(0, name.lastIndexOf("."));
          return stbof.hasInternalFile(name + "map.tga") &&
            stbof.hasInternalFile(name + "maps.tga");
        }
        return false;
      }
    });

    ArrayList<JMenuItem> graphItems = new ArrayList<JMenuItem>();

    for (String ani : anis) {
      ani = ani.substring(0, ani.lastIndexOf("."));
      val mnuItem = new JCheckBoxMenuItem(ani);
      mnuItem.setSelected(system.getStarAni().equalsIgnoreCase(ani));
      mnuItem.addActionListener(editGraph);
      graphItems.add(mnuItem);
    }

    Collections.sort(graphItems, new Comparator<JMenuItem>() {
      @Override
      public int compare(JMenuItem o1, JMenuItem o2) {
        return o1.getText().compareTo(o2.getText());
      }
    });

    for (int i = 0; i < graphItems.size(); i++) {
      mnuGraph.add(graphItems.get(i));
    }

    return mnuGraph;
  }

  private static void addStrcListItems(JMenu menu, SavGame sav,
    Stbof stbof, Trek trek, short systemId) throws IOException {

    val systInfo = sav.files().systInfo();
    val system = systInfo.getSystem(systemId);

    if (system.getPopulation() > 0 && system.getResidentRace() >= 0) {
      JMenuItem mItem = new JMenuItem("Structure list");

      mItem.addActionListener(new CBActionListener(evt ->
        // display system structures view for selected system
        // TODO: add some showPanel with index helper routine
        UE.WINDOW.showPanel(new StrcInfoGUI(sav, stbof, trek, systemId))
      ));

      menu.add(mItem);
    }
  }

  private static JMenuItem createSysEditItem(MapPanel map, SavGame sav,
    Stbof stbof, short systemId) throws IOException {

    JMenuItem mnuSysEdit = new JMenuItem("Open System Edit");
    mnuSysEdit.addActionListener(new CBActionListener(evt -> {
      UE.WINDOW.showPanel(new SystemsInfoGUI(sav, stbof, systemId));
    }));

    return mnuSysEdit;
  }

  // #endregion sub-menus

}
