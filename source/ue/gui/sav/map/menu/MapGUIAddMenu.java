package ue.gui.sav.map.menu;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import lombok.val;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.tools.RaceTools;
import ue.edit.res.stbof.tools.StellarTools;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.tools.SystemGen.SysGenOption;
import ue.exception.KeyNotFoundException;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;
import ue.util.data.ID;

public class MapGUIAddMenu extends JMenu {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public MapGUIAddMenu(MapPanel map, SavGameInterface sgif, StbofFileInterface stif,
    int sectorIndex) throws KeyNotFoundException, IOException {
    super("Add");

    val items = getMenuItems(map, sgif, stif, sectorIndex);
    for (val item : items)
      add(item);
  }

  /**
   * Returns the menu shown when a sector is right-clicked
   *
   * @param map
   * @param empire
   * @param sector
   * @return
   * @throws IOException
   * @throws KeyNotFoundException
   */
  public static ArrayList<JMenuItem> getMenuItems(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sector) throws IOException, KeyNotFoundException {

    ArrayList<JMenuItem> addItems = new ArrayList<JMenuItem>();

    // systems & stellar objects
    if (sgif.sectorLst().getStellarObject(sector) < 0 && sgif.sectorLst().getSystem(sector) < 0) {
      addItems.add(createSystemMenu(map, sgif, sector));
      addItems.add(createStellarObjectMenu(map, sgif, stif, sector));
    }

    // outposts
    if (sgif.sectorLst().getStarbase(sector) < 0)
      addItems.add(createOutpostsMenu(map, sgif, stif, sector));

    // task forces
    addItems.add(createTaskForceMenu(map, sgif, stif, sector));

    return addItems;
  }

  private static JMenu createSystemMenu(MapPanel map, SavGameInterface sgif, int sector) {
    JMenu mnuSystem = new JMenu("System");

    ArrayList<ID> ids = new ArrayList<>();
    ids.add(new ID("Empty", SysGenOption.Empty));
    ids.add(new ID("Dreadful", SysGenOption.Dreadful));
    ids.add(new ID("Miserable", SysGenOption.Miserable));
    ids.add(new ID("Inhabitable", SysGenOption.Inhabitable));
    ids.add(new ID("Peculiar", SysGenOption.Peculiar));
    ids.add(new ID("Acceptable", SysGenOption.Acceptable));
    ids.add(new ID("Average", SysGenOption.Average));
    ids.add(new ID("Productive", SysGenOption.Productive));
    ids.add(new ID("Remarkable", SysGenOption.Remarkable));
    ids.add(new ID("Energy Hub", SysGenOption.EnergyHub));
    ids.add(new ID("Superior", SysGenOption.Superior));
    ids.add(new ID("Inconceivable", SysGenOption.Inconceivable));
    ids.add(new ID("Random", SysGenOption.Random));

    if (ids.size() > 0) {
      val addObject = new CBActionListener(evt -> {
        String text = ((JMenuItem) evt.getSource()).getText();
        int option = Integer.parseInt(text.substring(0, text.indexOf(":")));
        sgif.systemTools().addSystem(sector, option);
        map.refreshMap();
      });

      // add system menu selections
      for (int i = 0; i < ids.size(); i++) {
        ID id = ids.get(i);
        JMenuItem mnuItem = new JMenuItem(id.ID + ": " + id.toString());
        mnuItem.addActionListener(addObject);
        mnuSystem.add(mnuItem);
      }
    } else {
      JMenuItem mnuNone = new JMenuItem("None");
      mnuNone.setEnabled(false);
      mnuSystem.add(mnuNone);
    }

    return mnuSystem;
  }

  private static JMenuItem createStellarObjectMenu(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sector) throws IOException {

    JMenu mnuObject = new JMenu("Stellar object");
    StellarTools stTools = stif.stellarTools();
    ArrayList<ID> ids = stTools.listStellarTypes();

    if (ids.size() > 0) {
      Collections.sort(ids);

      val addObject = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        short id = Short.parseShort(text.substring(0, text.indexOf(":")));
        sgif.stellarTools().addStellarObject(stif.objStruc(), sector,
          id, text.substring(text.indexOf(":") + 2), false);
        map.refreshMap();
      });

      // add stellar type menu selections
      for (int i = 0; i < ids.size(); i++) {
        ID id = ids.get(i);
        JMenuItem mnuItem = new JMenuItem(id.ID + ": " + id.toString());
        mnuItem.addActionListener(addObject);
        mnuObject.add(mnuItem);
      }
    } else {
      JMenuItem mnuNone = new JMenuItem("None");
      mnuNone.setEnabled(false);
      mnuObject.add(mnuNone);
    }

    return mnuObject;
  }

  private static JMenuItem createOutpostsMenu(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sector) throws IOException {

    JMenu mnuOutpost = new JMenu("Outpost/Starbase");

    // get all outpost (6) and starbase (7) model ids
    ID[] ids = stif.shipList().getIDs(ShipList.FILTER_ALL, new int[] { ShipRole.Outpost, ShipRole.Starbase }, null);
    if (ids.length > 0) {
      val addOutpost = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        short id = Short.parseShort(text.substring(0, text.indexOf(":")));
        sgif.starbaseTools().addStarbase(stif.shipList(), sector, id, false);
        map.refreshMap();
      });

      for (ID id : ids) {
        ShipDefinition shipDef = stif.shipList().getShipDefinition(id.ID);
        String raceName = stif.raceTools().mapRace(shipDef.getRace());
        JMenuItem mnuBase = new JMenuItem(id.ID + ": " + raceName + ": " + id.toString());
        mnuBase.addActionListener(addOutpost);
        mnuOutpost.add(mnuBase);
      }
    } else {
      JMenuItem mnuNone = new JMenuItem("None");
      mnuNone.setEnabled(false);
      mnuOutpost.add(mnuNone);
    }

    return mnuOutpost;
  }

  private static JMenu createTaskForceMenu(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sector) throws IOException {
    ShipList shipList = stif.shipList();
    RaceTools raceTools = stif.raceTools();

    JMenu mnuTaskForce = new JMenu("Task Force");

    val addTaskForce = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      String text = mnu.getText();
      short shipType = Short.parseShort(text.substring(0, text.indexOf(":")));
      ShipDefinition shipDef = stif.shipList().getShipDefinition(shipType);
      short raceId = shipDef.getRace();

      sgif.taskForceTools().addTaskForce(shipList, sector, raceId, shipType, map.getShipCount());
      map.refreshMap();
    });

    // get all ship model ids, except outposts & starbases
    ID[] cardSmIds = shipList.getIDs(Race.Card, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] fedSmIds = shipList.getIDs(Race.Fed, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] fergSmIds = shipList.getIDs(Race.Ferg, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] klngSmIds = shipList.getIDs(Race.Kling, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] romSmIds = shipList.getIDs(Race.Rom, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] minSmIds = shipList.getIDs(ShipList.FILTER_MINORS, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] monSmIds = shipList.getIDs(Race.Monster, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[][] smTable = new ID[][] { cardSmIds, fedSmIds, fergSmIds, klngSmIds, romSmIds, minSmIds, monSmIds };

    // add sub-menus for all major races plus minors and monsters
    for (short r = 0; r < smTable.length; ++r) {
      String raceName = r == NUM_EMPIRES ? "5+: Minors" : r == 6 ? Race.Monster + ": Monsters"
        : (r + ": " + raceTools.mapRace(r));

      JMenu mnuRace = new JMenu(raceName);
      mnuTaskForce.add(mnuRace);

      // ship model race selector: 0-4 = major races, -3 = minors, 36 = monsters
      final short raceSelector = r == 6 ? Race.Monster : r == NUM_EMPIRES ? -3 : r;

      ActionListener addOtherTaskForce = null;
      if (raceSelector < NUM_EMPIRES) {
        addOtherTaskForce = new CBActionListener(evt -> {
          JMenuItem mnu = (JMenuItem) evt.getSource();
          String text = mnu.getText();
          short shipType = Short.parseShort(text.substring(0, text.indexOf(":")));

          sgif.taskForceTools().addTaskForce(shipList, sector, raceSelector, shipType, map.getShipCount());
          map.refreshMap();
        });
      }

      // add race ship model selections
      ID[] raceShipModelIds = smTable[r];
      if (raceShipModelIds.length > 0) {

        for (ID id : raceShipModelIds) {
          ShipDefinition shipDef = stif.shipList().getShipDefinition(id.ID);
          String shipName = shipDef.getName();
          JMenuItem mnuShp = new JMenuItem(id.ID + ": " + shipName);
          mnuShp.addActionListener(addTaskForce);
          mnuRace.add(mnuShp);
        }
      } else if (r > 4) {
        JMenuItem mnuNone = new JMenuItem("None");
        mnuNone.setEnabled(false);
        mnuRace.add(mnuNone);
      }

      // add another group for other race ships
      if (r < NUM_EMPIRES) {
        JMenu mnuOther = new JMenu("Other");
        mnuRace.add(mnuOther);

        // add sub-menus for all major races plus minors and monsters
        for (short other = 0; other < smTable.length; ++other) {
          if (other == r)
            continue;

          String otherRaceName = other == NUM_EMPIRES ? "5+: Minors"
            : other == 6 ? Race.Monster + ": Monsters"
            : (other + ": " + raceTools.mapRace(other));

          JMenu mnuOtherRace = new JMenu(otherRaceName);
          mnuOther.add(mnuOtherRace);

          // add race ship model selections
          ID[] otherIds = smTable[other];
          for (ID id : otherIds) {
            ShipDefinition otherShipDef = stif.shipList().getShipDefinition(id.ID);
            String shipName = otherShipDef.getName();
            JMenuItem mnuShp = new JMenuItem(id.ID + ": " + shipName);
            mnuShp.addActionListener(addOtherTaskForce);
            mnuOtherRace.add(mnuShp);
          }
        }
      }
    }

    return mnuTaskForce;
  }

}
