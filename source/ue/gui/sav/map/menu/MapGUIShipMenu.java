package ue.gui.sav.map.menu;

import java.util.ArrayList;
import java.util.Optional;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import lombok.val;
import ue.UE;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.tools.RaceTools;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.data.Ship;
import ue.gui.sav.map.MapGUI;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;
import ue.util.data.ID;

public class MapGUIShipMenu extends JMenu {

  private static int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public MapGUIShipMenu(MapGUI gui,SavGameInterface sgif, Optional<StbofFileInterface> stif,
    short taskForceId, short shipId) {
    super("Ship");

    val items = getMenuItems(gui, sgif, stif, taskForceId, shipId);
    for (val item : items)
      this.add(item);
  }

  /**
   * Returns the menu shown when a ship is right-clicked.
   */
  public static ArrayList<JMenuItem> getMenuItems(MapGUI gui, SavGameInterface sgif,
    Optional<StbofFileInterface> stif, short taskForceId, short shipId) {

    ArrayList<JMenuItem> items = new ArrayList<JMenuItem>();
    items.add(createNameItem(gui, sgif, shipId));
    items.add(createTypeMenu(gui, sgif, stif, taskForceId, shipId));
    items.add(createRemoveItem(gui.map(), sgif, taskForceId, shipId));

    return items;
  }

  private static JMenuItem createNameItem(MapGUI gui, SavGameInterface sgif, short shipId) {
    JMenuItem mnuName = new JMenuItem("Name");

    mnuName.addActionListener(new CBActionListener(evt -> {
      Ship ship = sgif.gShipList().getShip(shipId);
      Object retVal = JOptionPane.showInputDialog(UE.WINDOW, "Enter new name:", ship.getName());
      if (retVal != null) {
        ship.setName(retVal.toString());
        gui.updateMilitaryList();
      }
    }));

    return mnuName;
  }

  private static JMenu createTypeMenu(MapGUI gui, SavGameInterface sgif, Optional<StbofFileInterface> stif, short taskForceId, short shipId) {
    JMenu mnuType = new JMenu("Type");
    if (!stif.isPresent()) {
      mnuType.setEnabled(false);
      return mnuType;
    }

    ShipList shipList = stif.get().findShipList().orElse(null);
    if (shipList == null) {
      mnuType.setEnabled(false);
      return mnuType;
    }

    val changeShipType = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      String text = mnu.getText();
      short shipType = Short.parseShort(text.substring(0, text.indexOf(":")));
      sgif.gShipList().getShip(shipId).setShipType(shipType);
      // don't care on updating GTForceList for the ship type
      sgif.gwtForce().getTaskForce(taskForceId).updateAbilities();
      gui.updateMilitaryList();
    });

    // get all ship model ids, except outposts & starbases
    ID[] cardSmIds = shipList.getIDs(Race.Card, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] fedSmIds = shipList.getIDs(Race.Fed, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] fergSmIds = shipList.getIDs(Race.Ferg, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] klngSmIds = shipList.getIDs(Race.Kling, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] romSmIds = shipList.getIDs(Race.Rom, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] minSmIds = shipList.getIDs(ShipList.FILTER_MINORS, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[] monSmIds = shipList.getIDs(Race.Monster, null, new int[] { ShipRole.Outpost, ShipRole.Starbase });
    ID[][] smTable = new ID[][] { cardSmIds, fedSmIds, fergSmIds, klngSmIds, romSmIds, minSmIds, monSmIds };
    RaceTools raceTools = stif.get().raceTools();

    // add sub-menus for all major races plus minors and monsters
    for (short r = 0; r < smTable.length; ++r) {
      String raceName = r == NUM_EMPIRES ? "5+: Minors" : r == 6 ? Race.Monster + ": Monsters"
        : (r + ": " + raceTools.mapRace(r));

      JMenu mnuRace = new JMenu(raceName);
      mnuType.add(mnuRace);

      // add race ship model selections
      ID[] raceShipModelIds = smTable[r];
      if (raceShipModelIds.length > 0) {
        for (ID id : raceShipModelIds) {
          ShipDefinition shipDef = shipList.getShipDefinition(id.ID);
          String shipName = shipDef.getName();
          JMenuItem mnuShp = new JMenuItem(id.ID + ": " + shipName);
          mnuShp.addActionListener(changeShipType);
          mnuRace.add(mnuShp);
        }
      } else {
        JMenuItem mnuNone = new JMenuItem("None");
        mnuNone.setEnabled(false);
        mnuRace.add(mnuNone);
      }
    }

    return mnuType;
  }

  private static JMenuItem createRemoveItem(MapPanel map, SavGameInterface sgif, short taskForceId, short shipId) {
    JMenuItem mnuRemove = new JMenuItem("Remove");
    mnuRemove.addActionListener(new CBActionListener(evt -> {
      sgif.taskForceTools().removeShip(taskForceId, shipId);
      map.refreshMap();
    }));
    return mnuRemove;
  }

}
