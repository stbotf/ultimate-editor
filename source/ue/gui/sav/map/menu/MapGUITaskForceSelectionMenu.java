package ue.gui.sav.map.menu;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import lombok.val;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.sav.SavGameInterface;
import ue.exception.KeyNotFoundException;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;

public class MapGUITaskForceSelectionMenu extends JMenu {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public MapGUITaskForceSelectionMenu(MapPanel map, SavGameInterface sgif, StbofFileInterface stif,
    int sectorIndex) throws KeyNotFoundException, IOException {
    super("Task Force");

    ArrayList<JMenuItem> items = MapGUITaskForceSelectionMenu.getMenuItems(map, sgif, stif, sectorIndex);
    for (val item : items)
      add(item);
  }

  /**
   * Returns the menu show when a sector with a task force is right-clicked
   *
   * @param map
   * @param sectorIndex
   * @return the menu items
   * @throws FileNotFoundException
   * @throws KeyNotFoundException
   * @throws IOException
   */
  public static ArrayList<JMenuItem> getMenuItems(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, int sectorIndex) throws KeyNotFoundException, IOException {

    ArrayList<JMenuItem> items = new ArrayList<JMenuItem>();
    JMenu mnuSelect = new JMenu("Select");
    items.add(mnuSelect);

    val changeOwner = new CBActionListener(evt -> {
      JCheckBoxMenuItem cb = (JCheckBoxMenuItem) evt.getSource();
      String text = cb.getText();
      short raceId = Short.parseShort(text.substring(0, text.indexOf(":")));

      // get "Change Owner" parent
      JPopupMenu mnuPopup = (JPopupMenu) cb.getParent();
      JMenu mnuParent = (JMenu) mnuPopup.getInvoker();
      // then get the task force selection parent
      mnuPopup = (JPopupMenu) mnuParent.getParent();
      mnuParent = (JMenu) mnuPopup.getInvoker();
      // read back the task force id
      text = mnuParent.getText();
      short taskForceId = Short.parseShort(text.substring(0, text.indexOf(":")));

      sgif.taskForceTools().setTaskForceOwner(taskForceId, raceId);
      map.refreshMap();
    });

    val clearOrders = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();

      // get the task force selection parent
      JPopupMenu mnuPopup = (JPopupMenu) mnu.getParent();
      JMenu mnuParent = (JMenu) mnuPopup.getInvoker();

      String text = mnuParent.getText();
      short taskForceId = Short.parseShort(text.substring(0, text.indexOf(":")));
      sgif.taskForceTools().clearTaskForceOrders(taskForceId);
      map.refreshMap();
    });

    val clearResults = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();

      // get the task force selection parent
      JPopupMenu mnuPopup = (JPopupMenu) mnu.getParent();
      JMenu mnuParent = (JMenu) mnuPopup.getInvoker();

      String text = mnuParent.getText();
      short taskForceId = Short.parseShort(text.substring(0, text.indexOf(":")));
      sgif.taskForceTools().clearTaskForceResults(taskForceId);
    });

    val moveTaskForce = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();

      // get the task force selection parent
      JPopupMenu mnuPopup = (JPopupMenu) mnu.getParent();
      JMenu mnuParent = (JMenu) mnuPopup.getInvoker();

      String text = mnuParent.getText();
      short taskForceId = Short.parseShort(text.substring(0, text.indexOf(":")));

      map.moveTaskForce(taskForceId);
    });

    val removeTaskForce = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();

      // get the task force selection parent
      JPopupMenu mnuPopup = (JPopupMenu) mnu.getParent();
      JMenu mnuParent = (JMenu) mnuPopup.getInvoker();

      String text = mnuParent.getText();
      short taskForceId = Short.parseShort(text.substring(0, text.indexOf(":")));
      sgif.taskForceTools().removeTaskForce(taskForceId);
      map.refreshMap();
    });

    short[] taskForceIds = sgif.sectorLst().getTaskForces(sectorIndex);
    for (short taskForceId : taskForceIds) {
      String tfName = map.getTaskForceIdentifier(taskForceId, true);
      int tfOwner = sgif.gwtForce().taskForce(taskForceId).getOwnerId();
      JMenu mnuTaskForce = new JMenu(tfName);
      mnuSelect.add(mnuTaskForce);

      JMenu mnuChangeOwner = new JMenu("Change Owner");
      for (int r = 0; r < NUM_EMPIRES; ++r) {
        JCheckBoxMenuItem mnuCheckItem = new JCheckBoxMenuItem(r + ": " + stif.raceTools().mapRace(r));

        mnuCheckItem.setSelected(tfOwner == r);
        mnuCheckItem.addActionListener(changeOwner);

        mnuChangeOwner.add(mnuCheckItem);
      }
      mnuTaskForce.add(mnuChangeOwner);

      JMenuItem mnuClearOrders = new JMenuItem("Clear Orders");
      mnuClearOrders.addActionListener(clearOrders);
      mnuTaskForce.add(mnuClearOrders);

      JMenuItem mnuClearResults = new JMenuItem("Clear Results");
      mnuClearResults.addActionListener(clearResults);
      mnuTaskForce.add(mnuClearResults);

      JMenuItem mnuMove = new JMenuItem("Move");
      mnuMove.addActionListener(moveTaskForce);
      mnuTaskForce.add(mnuMove);

      JMenuItem mnuRemove = new JMenuItem("Remove");
      mnuRemove.addActionListener(removeTaskForce);
      mnuTaskForce.add(mnuRemove);
    }

    val changeAllOwners = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      String text = mnu.getText();
      short raceId = Short.parseShort(text.substring(0, text.indexOf(":")));

      sgif.taskForceTools().setAllSectorTaskForceOwners(sectorIndex, raceId);
      map.refreshMap();
    });

    JMenu mnuChangeAllOwners = new JMenu("Change All Owners");
    items.add(mnuChangeAllOwners);

    for (int r = 0; r < NUM_EMPIRES; ++r) {
      JMenuItem mnuOwnerItem = new JMenuItem(r + ": " + stif.raceTools().mapRace(r));
      mnuOwnerItem.addActionListener(changeAllOwners);
      mnuChangeAllOwners.add(mnuOwnerItem);
    }

    JMenuItem mnuClearAllOrders = new JMenuItem("Clear All Orders");
    items.add(mnuClearAllOrders);
    mnuClearAllOrders.addActionListener(new CBActionListener(evt -> {
      sgif.taskForceTools().clearAllSectorTaskForceOrders(sectorIndex);
      map.refreshMap();
    }));

    JMenuItem mnuClearAllResults = new JMenuItem("Clear All Results");
    items.add(mnuClearAllResults);
    mnuClearAllResults.addActionListener(new CBActionListener(evt -> {
      sgif.taskForceTools().clearAllSectorTaskForceResults(sectorIndex);
    }));

    JMenuItem mnuMoveAll = new JMenuItem("Move All");
    mnuMoveAll.addActionListener(new CBActionListener(evt -> map.moveAllSectorTaskForces(sectorIndex)));
    items.add(mnuMoveAll);

    JMenuItem mnuRemoveAll = new JMenuItem("Remove All");
    mnuRemoveAll.addActionListener(new CBActionListener(evt -> {
      sgif.taskForceTools().removeAllSectorTaskForces(sectorIndex);
      map.refreshMap();
    }));
    items.add(mnuRemoveAll);

    return items;
  }

}
