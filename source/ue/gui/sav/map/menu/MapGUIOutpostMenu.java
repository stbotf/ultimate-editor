package ue.gui.sav.map.menu;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import lombok.val;
import ue.UE;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGameInterface;
import ue.exception.KeyNotFoundException;
import ue.gui.sav.map.MapPanel;
import ue.gui.util.event.CBActionListener;
import ue.util.data.ID;

public class MapGUIOutpostMenu extends JMenu {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public MapGUIOutpostMenu(MapPanel map, SavGameInterface sgif, StbofFileInterface stif,
    short outpostId) throws KeyNotFoundException, IOException {
    super("Outpost/Starbase");

    val items = MapGUIOutpostMenu.getMenuItems(map, sgif, stif, outpostId);
    for (val item : items)
      add(item);
  }

  /**
   * Returns the menu show when an outpost is right-clicked
   *
   * @param map
   * @param outpostId
   * @return
   * @throws FileNotFoundException
   * @throws KeyNotFoundException
   * @throws IOException
   */
  public static ArrayList<JMenuItem> getMenuItems(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, short outpostId) throws KeyNotFoundException, IOException {

    ArrayList<JMenuItem> items = new ArrayList<JMenuItem>();

    JMenu mnuEdit = new JMenu("Edit");
    mnuEdit.add(createNameItem(map, sgif, outpostId));
    mnuEdit.add(createOwnerMenu(map, sgif, stif, outpostId));
    mnuEdit.add(createClassMenu(map, sgif, stif, outpostId));
    mnuEdit.add(createHitPointsItem(map, sgif, stif, outpostId));
    mnuEdit.add(createConstructionMenu(map, sgif, stif, outpostId));
    items.add(mnuEdit);

    addMoveMenu(items, map, outpostId);
    addRemoveMenu(items, map, sgif, stif, outpostId);

    return items;
  }

  private static JMenuItem createNameItem(MapPanel map, SavGameInterface sgif, short outpostId)
    throws IOException {

    JMenuItem mnuName = new JMenuItem("Name");
    mnuName.addActionListener(new CBActionListener(evt -> {
      Object retVal = JOptionPane.showInputDialog(UE.WINDOW,
        "Enter new name:", sgif.sbInfo().getName(outpostId));

      if (retVal != null) {
        sgif.sbInfo().setName(outpostId, retVal.toString());
        map.refreshMap();
      }
    }));

    return mnuName;
  }

  private static JMenu createOwnerMenu(MapPanel map, SavGameInterface sgif, StbofFileInterface stif,
    short outpostId) throws KeyNotFoundException, IOException {

    JMenu mnuOwner = new JMenu("Owner");
    int owner = sgif.sbInfo().getOwner(outpostId);

    val editOwner = new CBActionListener(evt -> {
      JMenuItem mnu = (JMenuItem) evt.getSource();
      String text = mnu.getText();
      short ownerId = Short.parseShort(text.substring(0, text.indexOf(":")));
      sgif.sbInfo().setOwner(outpostId, ownerId);
      map.refreshMap();
    });

    for (int r = 0; r < NUM_EMPIRES; r++) {
      JCheckBoxMenuItem mnuCheckItem = new JCheckBoxMenuItem(r + ": " + stif.raceTools().mapRace(r));
      mnuCheckItem.setSelected(owner == r);
      mnuCheckItem.addActionListener(editOwner);

      mnuOwner.add(mnuCheckItem);
    }

    return mnuOwner;
  }

  private static JMenu createClassMenu(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, short outpostId) throws KeyNotFoundException, IOException {

    // class/type
    JMenu mnuClass = new JMenu("Class");

    ArrayList<ID> ids = new ArrayList<ID>();
    ID[] outposts = stif.shipList().getIDs(43); // 37 + 6
    for (int i = 0; i < outposts.length; i++) {
      ids.add(outposts[i]);
    }
    outposts = stif.shipList().getIDs(44); // 37 + 7
    for (int i = 0; i < outposts.length; i++) {
      ids.add(outposts[i]);
    }

    if (ids.size() > 0) {
      JCheckBoxMenuItem mnuBase;
      ID id;
      Collections.sort(ids);
      short shipID = sgif.sbInfo().getShipID(outpostId);

      val editClass = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        short shipId = Short.parseShort(text.substring(0, text.indexOf(":")));
        ShipDefinition shipDef = stif.shipList().getShipDefinition(shipId);

        sgif.sbInfo().setShipID(outpostId, shipId);
        sgif.sbInfo().setRole(outpostId, shipDef.getShipRole());
        sgif.sbInfo().setHull(outpostId, shipDef.getHullStrength());
        sgif.sbInfo().setScanRange(outpostId, shipDef.getScanRange());

        map.refreshMap();
      });

      for (int i = 0; i < ids.size(); i++) {
        id = ids.get(i);
        ShipDefinition shipModel = stif.shipList().getShipDefinition(id.ID);
        String strRace = stif.raceTools().mapRace(shipModel.getRace());
        mnuBase = new JCheckBoxMenuItem(id.ID + ": " + strRace + ": " + id);

        mnuBase.setSelected(shipID == id.ID);

        mnuBase.addActionListener(editClass);
        mnuClass.add(mnuBase);
      }
    }

    return mnuClass;
  }

  private static JMenuItem createHitPointsItem(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, short outpostId) throws IOException {

    JMenuItem mnuHitPoints = new JMenuItem("Hit Points");
    mnuHitPoints.addActionListener(new CBActionListener(evt -> {
      JPanel pnl = new JPanel(new BorderLayout());
      short shipType = sgif.sbInfo().getShipID(outpostId);
      ShipDefinition shipDef = stif.shipList().getShipDefinition(shipType);

      int max = shipDef.getHullStrength();
      JSpinner spiValue = new JSpinner(new SpinnerNumberModel(
        sgif.sbInfo().getHull(outpostId), 0, max, 1));
      pnl.add(spiValue, BorderLayout.CENTER);
      pnl.add(new JLabel("Edit hit points:"), BorderLayout.NORTH);
      pnl.add(new JLabel(" / " + max + " "), BorderLayout.EAST);

      int retVal = JOptionPane.showConfirmDialog(UE.WINDOW, pnl, UE.APP_NAME,
        JOptionPane.OK_CANCEL_OPTION);

      if (retVal == JOptionPane.OK_OPTION) {
        sgif.sbInfo().setHull(outpostId, (Integer) spiValue.getValue());
        map.refreshMap();
      }
    }));

    return mnuHitPoints;
  }

  private static JMenu createConstructionMenu(MapPanel map, SavGameInterface sgif,
    StbofFileInterface stif, short outpostId) throws KeyNotFoundException, IOException {

    JMenu mnuConstruction = new JMenu("Contruction status");

    JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem("Just started");
    mnuItem.addActionListener(new CBActionListener(evt -> {
      short shipType = sgif.sbInfo().getShipID(outpostId);
      ShipDefinition shipDef = stif.shipList().getShipDefinition(shipType);
      sgif.sbInfo().setProductionInvested(outpostId, 0);
      sgif.sbInfo().setProductionCost(outpostId, shipDef.getBuildCost());

      map.refreshMap();
    }));
    mnuItem.setSelected(sgif.sbInfo().getProductionCost(outpostId) > 0 &&
      sgif.sbInfo().getProductionInvested(outpostId) == 0);

    mnuConstruction.add(mnuItem);

    mnuItem = new JCheckBoxMenuItem("Partialy complete");
    mnuItem.addActionListener(new CBActionListener(evt -> {
      JPanel pnl = new JPanel(new BorderLayout());
      short shipType = sgif.sbInfo().getShipID(outpostId);
      ShipDefinition shipDef = stif.shipList().getShipDefinition(shipType);

      int max = shipDef.getBuildCost();
      int val = (int) sgif.sbInfo().getProductionInvested(outpostId);
      val = Math.min(val, max);
      val = Math.max(val, 0);

      JSpinner spiValue = new JSpinner(new SpinnerNumberModel(val, 0, max, 1));
      pnl.add(spiValue, BorderLayout.CENTER);
      pnl.add(new JLabel("Edit production invested:"), BorderLayout.NORTH);
      pnl.add(new JLabel(" / " + max + " "), BorderLayout.EAST);

      int retVal = JOptionPane.showConfirmDialog(UE.WINDOW, pnl, UE.APP_NAME,
        JOptionPane.OK_CANCEL_OPTION);

      if (retVal == JOptionPane.OK_OPTION) {
        sgif.sbInfo().setProductionInvested(outpostId,
          ((Integer) spiValue.getValue()).intValue());
        map.refreshMap();
      }
    }));
    mnuItem.setSelected(sgif.sbInfo().getProductionCost(outpostId) >
      sgif.sbInfo().getProductionInvested(outpostId) &&
      sgif.sbInfo().getProductionInvested(outpostId) > 0);
    mnuConstruction.add(mnuItem);

    mnuItem = new JCheckBoxMenuItem("Completed");
    mnuItem.addActionListener(new CBActionListener(evt -> {
      short shipType = sgif.sbInfo().getShipID(outpostId);
      ShipDefinition shipDef = stif.shipList().getShipDefinition(shipType);
      int buildCost = shipDef.getBuildCost();
      sgif.sbInfo().setProductionInvested(outpostId, buildCost);
      sgif.sbInfo().setProductionCost(outpostId, buildCost);

      map.refreshMap();
    }));
    mnuItem.setSelected(sgif.sbInfo().getProductionCost(outpostId) <=
      sgif.sbInfo().getProductionInvested(outpostId));
    mnuConstruction.add(mnuItem);

    return mnuConstruction;
  }

  private static void addMoveMenu(ArrayList<JMenuItem> items, MapPanel map, short outpostId) {
    JMenuItem mnuMove = new JMenuItem("Move");
    mnuMove.addActionListener(new CBActionListener(evt -> map.moveOutpost(outpostId)));
    items.add(mnuMove);
  }

  private static void addRemoveMenu(ArrayList<JMenuItem> items, MapPanel map,
    SavGameInterface sgif, StbofFileInterface stif, short outpostId) {

    JMenuItem mnuRemove = new JMenuItem("Remove");
    mnuRemove.addActionListener(new CBActionListener(evt -> {
      int[] pos = sgif.sbInfo().getPosition(outpostId);
      int sector = pos[1] * sgif.sectorLst().getHSectors() + pos[0];
      sgif.starbaseTools().removeStarbase(stif.shipList(), sector);
      map.refreshMap();
    }));

    items.add(mnuRemove);
  }

}
