package ue.gui.sav.map.menu;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import lombok.val;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.BonusLevel;
import ue.edit.res.stbof.common.CStbof.PlanetSize;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.pst.data.PlanetEntry;
import ue.edit.res.stbof.files.pst.data.PlanetKey;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.gui.sav.map.MapGUI;
import ue.gui.util.event.CBActionListener;
import ue.util.data.ID;
import ue.util.data.StringTools;

/**
 * The context menu shown when a planet is right-clicked.
 */
public class MapGUIPlanetMenu extends JMenu {

  public MapGUIPlanetMenu(MapGUI gui, SavGame sav, Optional<Stbof> stbof,
    short systemId, short planetId) throws IOException {
    super("Planet");

    val items = getMenuItems(gui, sav, stbof, systemId, planetId);
    for (val item : items)
      this.add(item);
  }

  public static List<JMenuItem> getMenuItems(MapGUI gui, SavGame sav,
    Optional<Stbof> stbof, short systemId, short planetId) {

    val sgif = sav.files();
    ArrayList<JMenuItem> items = new ArrayList<>();

    addEditMenu(items, gui, sgif, stbof, systemId, planetId);
    addRemoveMenu(items, gui, sgif, systemId, planetId);

    return items;
  }

  // #region menus

  private static void addEditMenu(ArrayList<JMenuItem> items, MapGUI gui, SavGameInterface sgif,
    Optional<Stbof> stbof, short systemId, short planetId) {

    SystemEntry system = null;
    PlanetEntry planet = null;
    try {
      system = sgif.systInfo().getSystem(systemId);
      planet = system.getPlanet(planetId);
    } catch(Exception e) {
      e.printStackTrace();
    }

    JMenu mnuEdit = new JMenu("Edit Planet");
    mnuEdit.add(createNameItem(gui, system, planet));
    mnuEdit.add(createTypeMenu(gui, stbof, system, planet));
    mnuEdit.add(createAtmosphereMenu(gui, stbof, system, planet));
    mnuEdit.add(createGraphicsMenu(gui, stbof, system, planet));
    mnuEdit.add(createSizeMenu(gui, stbof, system, planet));
    mnuEdit.add(createEnergyBonusMenu(gui, stbof, system, planet));
    mnuEdit.add(createFoodBonusMenu(gui, stbof, system, planet));
    mnuEdit.add(createGrowthBonusMenu(gui, stbof, system, planet));
    items.add(mnuEdit);
  }

  private static void addRemoveMenu(List<JMenuItem> items, MapGUI gui,
    SavGameInterface sgif, short systemId, short planetId) {

    JMenuItem mnuRemove = new JMenuItem("Remove Planet");
    mnuRemove.addActionListener(new CBActionListener(evt -> {
      sgif.systemTools().removePlanet(systemId, planetId);
      gui.updateSystemDetails();
    }));
    items.add(mnuRemove);
  }

  // #endregion menus

  // #region sub-menus

  private static JMenuItem createNameItem(MapGUI gui, SystemEntry system, PlanetEntry planet) {

    JMenuItem mnuName = new JMenuItem("Name");
    mnuName.addActionListener(new CBActionListener(evt -> {
      Object retVal = JOptionPane.showInputDialog(UE.WINDOW, "Enter new name:", planet.getName());
      if (retVal != null) {
        planet.setName(retVal.toString());
        gui.updateSystemDetails();
      }
    }));

    return mnuName;
  }

  private static JMenu createTypeMenu(MapGUI gui, Optional<Stbof> stbof, SystemEntry system, PlanetEntry planet) {
    JMenu mnuType = new JMenu("Type");
    if (!stbof.isPresent() || system == null || planet == null) {
      mnuType.setEnabled(false);
      return mnuType;
    }

    try {
      val stif = stbof.get().files();
      val planetPst = stif.planetPst();
      ID[] planetGfx = stif.environ().planetGfx();

      if (planetGfx.length == 0) {
        mnuType.setEnabled(false);
        return mnuType;
      }

      val editType = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        int split = text.indexOf(":");
        short id = Short.parseShort(text.substring(0, split));
        PlanetKey searchKey = new PlanetKey(planet);
        searchKey.setPlanetType(id);

        planet.setPlanetType(id);
        planet.setAnimation(planetPst.getAnimation(searchKey));
        gui.updateSystemDetails();
      });

      // add planet type menu selections
      for (ID id : planetGfx) {
        JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(id.ID + ": " + id.toString());
        mnuItem.setSelected(planet.getPlanetType() == id.ID);
        mnuItem.addActionListener(editType);
        mnuType.add(mnuItem);
      }
    } catch(Exception e) {
      e.printStackTrace();
      mnuType.setEnabled(false);
    }

    return mnuType;
  }

  private static JMenu createAtmosphereMenu(MapGUI gui, Optional<Stbof> stbof, SystemEntry system, PlanetEntry planet) {
    JMenu mnuAtmo = new JMenu("Atmosphere");
    if (!stbof.isPresent() || system == null || planet == null) {
      mnuAtmo.setEnabled(false);
      return mnuAtmo;
    }

    try {
      val stif = stbof.get().files();
      val planetPst = stif.planetPst();
      ID[] atmoGfx = stif.environ().atmosphereGfx();

      if (atmoGfx.length == 0) {
        mnuAtmo.setEnabled(false);
        return mnuAtmo;
      }

      val editType = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        int split = text.indexOf(":");
        short id = Short.parseShort(text.substring(0, split));
        PlanetKey searchKey = new PlanetKey(planet);
        searchKey.setAtmosphere(id);

        planet.setAtmosphere(id);
        planet.setAnimation(planetPst.getAnimation(searchKey));
        gui.updateSystemDetails();
      });

      // add atmosphere type menu selections
      for (ID id : atmoGfx) {
        JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(id.ID + ": " + id.toString());
        mnuItem.setSelected(planet.getAtmosphere() == id.ID);
        mnuItem.addActionListener(editType);
        mnuAtmo.add(mnuItem);
      }
    } catch(Exception e) {
      e.printStackTrace();
      mnuAtmo.setEnabled(false);
    }

    return mnuAtmo;
  }

  private static JMenu createGraphicsMenu(MapGUI gui, Optional<Stbof> stbof, SystemEntry system, PlanetEntry planet) {
    JMenu mnuGraph = new JMenu("Graphics");
    if (!stbof.isPresent() || system == null || planet == null) {
      mnuGraph.setEnabled(false);
      return mnuGraph;
    }

    try {
      val editGraph = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        planet.setAnimation(mnu.getText() + ".ani");
        gui.updateSystemDetails();
      });

      List<String> anis = stbof.get().getFileNames(new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
          if (name.endsWith(".ani")) {
            name = name.substring(0, name.lastIndexOf("."));
            return !stbof.get().hasInternalFile(name + "map.tga") &&
              !stbof.get().hasInternalFile(name + "maps.tga");
          }
          return false;
        }
      });

      // sort by name
      anis.sort(null);

      JMenu mnuOther = new JMenu("other");
      String planetAni = StringTools.beforeLast(planet.getAnimation(), ".");

      // add ani graphics menu selections
      for (int i = 0; i < anis.size(); ++i) {
        String ani = StringTools.beforeLast(anis.get(i), ".");

        if (i + 2 < anis.size() && ani.endsWith("l")) {
          String prefix = ani.substring(0, ani.length() - 1);

          if (anis.get(i + 1).equals(prefix + "m.ani")
            && anis.get(i + 2).equals(prefix + "s.ani")) {
            JMenu mnuAni = new JMenu(prefix);

            String[] aniSizes = new String[] { ani, prefix + "m", prefix + "s" };
            for (String gfx : aniSizes) {
              JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(gfx);
              mnuItem.setSelected(gfx.equalsIgnoreCase(planetAni));
              mnuItem.addActionListener(editGraph);
              mnuAni.add(mnuItem);
            }

            mnuGraph.add(mnuAni);
            i += 2;
            continue;
          }
        }

        JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(ani);
        mnuItem.setSelected(ani.equalsIgnoreCase(planetAni));
        mnuItem.addActionListener(editGraph);
        mnuOther.add(mnuItem);
      }

      mnuGraph.add(mnuOther);
    } catch(Exception e) {
      e.printStackTrace();
      mnuGraph.setEnabled(false);
    }

    return mnuGraph;
  }

  private static JMenu createSizeMenu(MapGUI gui, Optional<Stbof> stbof, SystemEntry system, PlanetEntry planet) {
    JMenu mnuSize = new JMenu("Size");
    if (!stbof.isPresent() || system == null || planet == null) {
      mnuSize.setEnabled(false);
      return mnuSize;
    }

    try {
      val stif = stbof.get().files();
      val planetPst = stif.planetPst();

      val editType = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        int split = text.indexOf(":");
        short size = Short.parseShort(text.substring(0, split));
        PlanetKey searchKey = new PlanetKey(planet);
        searchKey.setPlanetSize(size);

        planet.setPlanetSize(size);
        planet.setAnimation(planetPst.getAnimation(searchKey));
        gui.updateSystemDetails();
      });

      // add planet bonus menu selections
      for (int bonus = PlanetSize.Small; bonus <= PlanetSize.Large; ++bonus) {
        String desc = LexHelper.mapPlanetSize(bonus);
        JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(bonus + ": " + desc);
        mnuItem.setSelected(planet.getPlanetSize() == bonus);
        mnuItem.addActionListener(editType);
        mnuSize.add(mnuItem);
      }
    } catch(Exception e) {
      e.printStackTrace();
      mnuSize.setEnabled(false);
    }

    return mnuSize;
  }

  private static JMenu createEnergyBonusMenu(MapGUI gui, Optional<Stbof> stbof, SystemEntry system, PlanetEntry planet) {
    JMenu mnuBonus = new JMenu("Energy Bonus");
    if (!stbof.isPresent() || system == null || planet == null) {
      mnuBonus.setEnabled(false);
      return mnuBonus;
    }

    try {
      val stif = stbof.get().files();
      val planetPst = stif.planetPst();

      val editType = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        int split = text.indexOf(":");
        short bonus = Short.parseShort(text.substring(0, split));
        PlanetKey searchKey = new PlanetKey(planet);
        searchKey.setEnergyBonusLvl(bonus);

        planet.setEnergyBonusLvl(bonus);
        planet.setAnimation(planetPst.getAnimation(searchKey));
        gui.updateSystemDetails();
      });

      // add energy bonus menu selections
      for (int bonus = BonusLevel.Few; bonus <= BonusLevel.Much; ++bonus) {
        String desc = LexHelper.mapBonusLevel(bonus);
        JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(bonus + ": " + desc);
        mnuItem.setSelected(planet.getEnergyBonusLvl() == bonus);
        mnuItem.addActionListener(editType);
        mnuBonus.add(mnuItem);
      }
    } catch(Exception e) {
      e.printStackTrace();
      mnuBonus.setEnabled(false);
    }

    return mnuBonus;
  }

  private static JMenu createFoodBonusMenu(MapGUI gui, Optional<Stbof> stbof, SystemEntry system, PlanetEntry planet) {
    JMenu mnuBonus = new JMenu("Food Bonus");
    if (!stbof.isPresent() || system == null || planet == null) {
      mnuBonus.setEnabled(false);
      return mnuBonus;
    }

    try {
      val stif = stbof.get().files();
      val planetPst = stif.planetPst();

      val editType = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        int split = text.indexOf(":");
        short bonus = Short.parseShort(text.substring(0, split));
        PlanetKey searchKey = new PlanetKey(planet);
        searchKey.setFoodBonusLvl(bonus);

        planet.setFoodBonusLvl(bonus);
        planet.setAnimation(planetPst.getAnimation(searchKey));
        gui.updateSystemDetails();
      });

      // add food bonus menu selections
      for (int bonus = BonusLevel.Few; bonus <= BonusLevel.Much; ++bonus) {
        String desc = LexHelper.mapBonusLevel(bonus);
        JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(bonus + ": " + desc);
        mnuItem.setSelected(planet.getFoodBonusLvl() == bonus);
        mnuItem.addActionListener(editType);
        mnuBonus.add(mnuItem);
      }
    } catch(Exception e) {
      e.printStackTrace();
      mnuBonus.setEnabled(false);
    }

    return mnuBonus;
  }

  private static JMenu createGrowthBonusMenu(MapGUI gui, Optional<Stbof> stbof, SystemEntry system, PlanetEntry planet) {
    JMenu mnuBonus = new JMenu("Growth Bonus");
    if (!stbof.isPresent() || system == null || planet == null) {
      mnuBonus.setEnabled(false);
      return mnuBonus;
    }

    try {
      val stif = stbof.get().files();
      val planetPst = stif.planetPst();

      val editType = new CBActionListener(evt -> {
        JMenuItem mnu = (JMenuItem) evt.getSource();
        String text = mnu.getText();
        int split = text.indexOf(":");
        short bonus = Short.parseShort(text.substring(0, split));
        PlanetKey searchKey = new PlanetKey(planet);
        searchKey.setGrowthBonusLvl(bonus);

        planet.setGrowthBonusLvl(bonus);
        planet.setAnimation(planetPst.getAnimation(searchKey));
        gui.updateSystemDetails();
      });

      // add growth bonus menu selections
      for (int bonus = BonusLevel.Few; bonus <= BonusLevel.Much; ++bonus) {
        String desc = LexHelper.mapBonusLevel(bonus);
        JCheckBoxMenuItem mnuItem = new JCheckBoxMenuItem(bonus + ": " + desc);
        mnuItem.setSelected(planet.getGrowthBonusLvl() == bonus);
        mnuItem.addActionListener(editType);
        mnuBonus.add(mnuItem);
      }
    } catch(Exception e) {
      e.printStackTrace();
      mnuBonus.setEnabled(false);
    }

    return mnuBonus;
  }

  // #endregion sub-menus

}
