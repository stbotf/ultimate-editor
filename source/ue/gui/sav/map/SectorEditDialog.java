package ue.gui.sav.map;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.Collection;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.files.map.GalInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.data.StellEntry;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.file.FileFilters;

/**
 * Edits sector info.
 */
public class SectorEditDialog extends JDialog implements WindowListener {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private Sector SECTOR;
  private StellInfo STELLAR;
  private GalInfo GALINFO;
  private StarBaseInfo BASEINFO;
  private Stbof STBOF;
  private Objstruc OBJ;
  private ShipList shipList;
  private RaceRst RACES;

  private int SECTOR_INDEX;

  private JLabel[] lblLabel;
  private JComboBox<ID> cmbObjectType;
  private JComboBox<String> cmbObjectAni;
  private JComboBox<ID> cmbBaseFunc;
  private JComboBox<ID> cmbBaseOwner;
  private JComboBox<ID> cmbBaseClass;
  private JTextField txtObjectName;
  private JTextField txtBaseName;

  private final String strStellarObject = Language.getString("SectorEditDialog.0"); //$NON-NLS-1$
  private final String strObjectType = Language.getString("SectorEditDialog.1"); //$NON-NLS-1$
  private final String strObjectName = Language.getString("SectorEditDialog.2"); //$NON-NLS-1$
  private final String strObjectAni = Language.getString("SectorEditDialog.3"); //$NON-NLS-1$
  private final String strBaseFunc = Language.getString("SectorEditDialog.17"); //$NON-NLS-1$
  private final String strBase = Language.getString("SectorEditDialog.18"); //$NON-NLS-1$
  private final String strBaseOwner = Language.getString("SectorEditDialog.19"); //$NON-NLS-1$
  private final String strBaseClass = Language.getString("SectorEditDialog.20"); //$NON-NLS-1$
  private final String strBaseName = Language.getString("SectorEditDialog.21"); //$NON-NLS-1$
  private final String strOutpost = Language.getString("SectorEditDialog.22"); //$NON-NLS-1$
  private final String strStarbase = Language.getString("SectorEditDialog.23"); //$NON-NLS-1$

  public SectorEditDialog(int sector_index, Sector sector, StellInfo stell,
      GalInfo gal, StarBaseInfo sbinfo, Stbof stbof) throws IOException, KeyNotFoundException {
    super();

    SECTOR = sector;
    STELLAR = stell;
    GALINFO = gal;
    BASEINFO = sbinfo;
    STBOF = stbof;

    if (STBOF != null) {
      try {
        OBJ = (Objstruc) STBOF.getInternalFile(CStbofFiles.ObjStrucSmt, true);
        shipList = (ShipList) STBOF.getInternalFile(CStbofFiles.ShipListSst, true);
        RACES = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);
      } catch (Exception e) {
        OBJ = null;
        shipList = null;
      }
    }

    SECTOR_INDEX = sector_index;

    setupComponents();
    placeComponents();

    // stellar object
    int objID = SECTOR.getStellarObject(SECTOR_INDEX);

    if (objID < 0) {
      cmbObjectType.setSelectedIndex(0);
    } else {
      StellEntry stellObj = STELLAR.getStellarObject(objID);
      cmbObjectType.setSelectedIndex(stellObj.getType() + 1);
      cmbObjectAni.setSelectedItem(stellObj.getAnimation());
    }

    cmbObjectAni.setEnabled(objID >= 0);
    txtObjectName.setEnabled(objID >= 0);
    cmbObjectType.setEnabled(SECTOR.getSystem(SECTOR_INDEX) < 0);

    // starbase
    short outID = SECTOR.getStarbase(SECTOR_INDEX);
    if (outID < 0) {
      cmbBaseFunc.setSelectedIndex(0);
    } else {
      cmbBaseFunc.setSelectedIndex(BASEINFO.getRole(outID) - 5);
      cmbBaseOwner.setSelectedIndex(BASEINFO.getOwner(outID));
      cmbBaseClass.setSelectedIndex(BASEINFO.getShipID(outID));
    }

    cmbBaseOwner.setEnabled(outID >= 0);
    cmbBaseClass.setEnabled(outID >= 0);
    txtBaseName.setEnabled(outID >= 0);
    cmbBaseFunc.setEnabled(true);
    setTitle(UE.APP_NAME + " - " + SECTOR.getDescription(SECTOR_INDEX, false)); //$NON-NLS-1$
    addWindowListener(this);
    pack();

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension pref = getPreferredSize();
    setLocation(
      new Point(
        (int) (screenSize.getWidth() - pref.getWidth()) / 2,
        (int) (screenSize.getHeight() - pref.getHeight()) / 2
      )
    );

    setIconImage(UE.WINDOW.getIconImage());
  }

  private void setupComponents() throws IOException {
    Font def = UE.SETTINGS.getDefaultFont();

    // labels
    lblLabel = new JLabel[9];
    lblLabel[0] = new JLabel(strStellarObject);
    lblLabel[1] = new JLabel(strObjectType);
    lblLabel[2] = new JLabel(strObjectName);
    lblLabel[3] = new JLabel(strObjectAni);
    lblLabel[4] = new JLabel(strBase);
    lblLabel[5] = new JLabel(strBaseFunc);
    lblLabel[6] = new JLabel(strBaseOwner);
    lblLabel[7] = new JLabel(strBaseClass);
    lblLabel[8] = new JLabel(strBaseName);

    for (int i = 0; i < lblLabel.length; i++) {
      lblLabel[i].setFont(def);
    }

    // object animation
    cmbObjectAni = new JComboBox<String>();
    cmbObjectAni.setFont(def);
    cmbObjectAni.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (cmbObjectAni.getSelectedIndex() < 0
            || !cmbObjectAni.isEnabled()) {
          return;
        }
        try {
          String ani = cmbObjectAni.getSelectedItem().toString();
          int objID = SECTOR.getStellarObject(SECTOR_INDEX);

          if (objID >= 0) {
            STELLAR.getStellarObject(objID).setAnimation(ani);
          }
        } catch (Exception x) {
          Dialogs.displayError(x);
        }
      }
    });

    // object type
    cmbObjectType = new JComboBox<ID>();
    cmbObjectType.setFont(def);
    cmbObjectType.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          if (cmbObjectType.getSelectedIndex() < 0
              || !cmbObjectType.isEnabled()) {
            return;
          }

          int type = cmbObjectType.getSelectedIndex() - 1;

          if (type >= -1) {
            int stellId = SECTOR.getStellarObject(SECTOR_INDEX);
            int oldType = stellId >= 0 ? STELLAR.getStellarObject(stellId).getType() : -1;

            if (type != oldType) {
              if (stellId < 0) {
                String loc = SECTOR.getDescription(SECTOR_INDEX, false).substring(7);
                txtObjectName.setText(cmbObjectType.getSelectedItem().toString() +
                  " " + loc); //$NON-NLS-1$

                int h = SECTOR.getHorizontal(SECTOR_INDEX);
                int v = SECTOR.getVertical(SECTOR_INDEX);
                stellId = STELLAR.add(h, v);

                StellEntry stellObj = STELLAR.getStellarObject(stellId);
                stellObj.setType(type);

                if (OBJ != null) {
                  cmbObjectAni.setSelectedItem(OBJ.getGraph(type));
                  stellObj.setAnimation(OBJ.getGraph(type));
                }
              } else {
                if (type < 0) {
                  STELLAR.remove(stellId);

                  for (int i = 0; i < SECTOR.getNumberOfSectors(); i++) {
                    int objId = SECTOR.getStellarObject(i);
                    if (objId >= 0 && objId > stellId)
                      SECTOR.setStellarObject(i, (short) (objId - 1));
                  }

                  txtObjectName.setText(""); //$NON-NLS-1$
                  stellId = -1;
                } else {
                  STELLAR.getStellarObject(stellId).setType(type);

                  String loc = SECTOR.getDescription(SECTOR_INDEX, false).substring(7);
                  if (txtObjectName.getText().length() == 0
                      || txtObjectName.getText().endsWith(loc)) {
                    txtObjectName.setText(cmbObjectType.getSelectedItem().toString() +
                        " " + loc); //$NON-NLS-1$
                  }

                  if (OBJ != null)
                    cmbObjectAni.setSelectedItem(OBJ.getGraph(type));
                }
              }

              GALINFO.setAnomalyCount(STELLAR.getNumberOfEntries());
              cmbObjectAni.setEnabled(stellId >= 0);
              txtObjectName.setEnabled(stellId >= 0);
              SECTOR.setStellarObject(SECTOR_INDEX, (short) stellId);
            }
          }
        } catch (Exception x) {
          Dialogs.displayError(x);
        }
      }
    });

    txtObjectName = new JTextField(24);
    txtObjectName.setFont(def);
    try {
      int objID = SECTOR.getStellarObject(SECTOR_INDEX);
      if (objID >= 0) {
        txtObjectName.setText(STELLAR.getStellarObject(objID).getName());
      }
    } catch (Exception e) {
      Dialogs.displayError(e);
    }

    // base func
    cmbBaseFunc = new JComboBox<ID>();
    cmbBaseFunc.setFont(def);
    cmbBaseFunc.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          if (cmbBaseFunc.getSelectedIndex() < 0
              || !cmbBaseFunc.isEnabled()) {
            return;
          }

          short role = (short) (cmbBaseFunc.getSelectedIndex() + 5);

          if (role >= 5) {
            short outID = SECTOR.getStarbase(SECTOR_INDEX);
            short oldRole = 5;

            if (outID >= 0) {
              oldRole = BASEINFO.getRole(outID);
            }

            if (role != oldRole) {
              if (outID < 0) {
                int hori = SECTOR.getHorizontal(SECTOR_INDEX);
                int vert = SECTOR.getVertical(SECTOR_INDEX);

                outID = (short) SECTOR_INDEX;
                BASEINFO.add(outID, role, vert, hori);

                int shipID = 0;
                int race = 0;

                if (shipList != null) {
                  ID[] ids = shipList.getIDs(37 + role);

                  if (ids.length > 0) {
                    shipID = ids[0].ID;
                    ShipDefinition shipDef = shipList.getShipDefinition(shipID);
                    race = shipDef.getRace();
                    BASEINFO.setHull(outID, shipDef.getHullStrength());
                  }
                }

                txtBaseName.setText(cmbBaseFunc.getSelectedItem().toString());
                BASEINFO.setName(txtBaseName.getText());
                cmbBaseOwner.setSelectedIndex(race);
                cmbBaseClass.setSelectedIndex(shipID);
              } else {
                if (role < 6) {
                  BASEINFO.remove(outID);

                  txtBaseName.setText(""); //$NON-NLS-1$

                  outID = -1;
                } else {
                  BASEINFO.setRole(outID, role);

                  int shipID = 0;
                  short race = (short) cmbBaseOwner.getSelectedIndex();

                  if (shipList != null) {
                    ID[] ids = shipList.getIDs(37 + cmbBaseFunc.getSelectedIndex() + 5);

                    for (ID id : ids) {
                      shipID = id.ID;
                      ShipDefinition shipDef = shipList.getShipDefinition(shipID);

                      if (shipDef.getRace() == race) {
                        cmbBaseClass.setSelectedIndex(shipID);
                        BASEINFO.setHull(outID, shipDef.getHullStrength());
                        break;
                      }
                    }
                  }

                  String name = BASEINFO.getName(outID);

                  if (name.equals(strOutpost)) {
                    name = strStarbase;
                  } else if (name.equals(strStarbase)) {
                    name = strOutpost;
                  }

                  txtBaseName.setText(name);
                  BASEINFO.setName(outID, name);
                }
              }

              GALINFO.setBaseCount((short) BASEINFO.getNumberOfEntries());
              cmbBaseOwner.setEnabled(outID >= 0);
              cmbBaseClass.setEnabled(outID >= 0);
              txtBaseName.setEnabled(outID >= 0);
              if (outID < 0) {
                SECTOR.setStarbase(SECTOR_INDEX, (short) -1);
              } else {
                SECTOR.setStarbase(SECTOR_INDEX, (short) SECTOR_INDEX);
              }
            }
          }
        } catch (Exception x) {
          Dialogs.displayError(x);
        }
      }
    });

    // base owner
    cmbBaseOwner = new JComboBox<ID>();
    cmbBaseOwner.setFont(def);
    cmbBaseOwner.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (cmbBaseOwner.getSelectedIndex() < 0
            || !cmbBaseOwner.isEnabled()) {
          return;
        }

        try {
          short outID = SECTOR.getStarbase(SECTOR_INDEX);
          if (outID >= 0) {
            int shipID = 0;
            short race = (short) cmbBaseOwner.getSelectedIndex();

            BASEINFO.setOwner(outID, race);

            if (shipList != null) {
              ID[] ids = shipList.getIDs(37 + cmbBaseFunc.getSelectedIndex() + 5);

              for (ID id : ids) {
                shipID = id.ID;
                ShipDefinition shipDef = shipList.getShipDefinition(shipID);

                if (shipDef.getRace() == race) {
                  cmbBaseClass.setSelectedIndex(shipID);
                  BASEINFO.setHull(outID, shipDef.getHullStrength());
                  break;
                }
              }
            }
          }
        } catch (Exception x) {
          Dialogs.displayError(x);
        }
      }
    });

    // base class
    cmbBaseClass = new JComboBox<ID>();
    cmbBaseClass.setFont(def);
    cmbBaseClass.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // ships is required for cmbBaseClass
        if (cmbBaseOwner.getSelectedIndex() < 0
            || !cmbBaseOwner.isEnabled()
            || shipList == null) {
          return;
        }

        try {
          short outID = SECTOR.getStarbase(SECTOR_INDEX);
          if (outID >= 0) {
            BASEINFO.setShipID(outID, (short) cmbBaseClass.getSelectedIndex());
          }
        } catch (Exception x) {
          Dialogs.displayError(x);
        }
      }
    });

    txtBaseName = new JTextField(24);
    txtBaseName.setFont(def);
    try {
      short outID = SECTOR.getStarbase(SECTOR_INDEX);
      if (outID >= 0) {
        txtBaseName.setText(BASEINFO.getName(outID));
      }
    } catch (Exception e) {
      Dialogs.displayError(e);
    }

    // fill base class
    cmbBaseClass.setEnabled(false);
    if (shipList != null) {
      ID[] ids = shipList.getIDs(35);

      for (int s = 0; s < ids.length; s++) {
        cmbBaseClass.addItem(new ID(s + ": " + ids[s].toString(), s)); //$NON-NLS-1$
      }
    }

    // fill base owner
    cmbBaseOwner.setEnabled(false);
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      if (RACES == null) {
        cmbBaseOwner.addItem(new ID(Integer.toString(emp), emp));
      } else {
        try {
          cmbBaseOwner.addItem(new ID(RACES.getName(emp), emp));
        } catch (Exception e) {
          cmbBaseOwner.addItem(new ID(Integer.toString(emp), emp));
        }
      }
    }

    // fill base function
    cmbBaseFunc.setEnabled(false);
    cmbBaseFunc.addItem(new ID(Language.getString("SectorEditDialog.4"), 0)); //$NON-NLS-1$
    cmbBaseFunc.addItem(new ID(strOutpost, 1));
    cmbBaseFunc.addItem(new ID(strStarbase, 2));

    // fill animation
    Collection<String> fileNames = STBOF.getFileNames(FileFilters.TgaMaps);

    cmbObjectAni.setEnabled(false);
    for (String fileName : fileNames) {
      fileName = fileName.substring(0, fileName.lastIndexOf("maps.tga")); //$NON-NLS-1$
      cmbObjectAni.addItem(fileName);
    }

    // fill type
    cmbObjectType.setEnabled(false);
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.4"), 0)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.5"), 1)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.6"), 2)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.7"), 3)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.8"), 4)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.9"), 5)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.10"), 6)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.11"), 7)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.12"), 8)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.13"), 9)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.14"), 10)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.15"), 11)); //$NON-NLS-1$
    cmbObjectType.addItem(new ID(Language.getString("SectorEditDialog.16"), 12)); //$NON-NLS-1$
  }

  public void placeComponents() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.NORTH;
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    add(lblLabel[0], c);
    c.gridwidth = 1;
    for (int i = 1; i < 4; i++) {
      c.gridy = i;
      add(lblLabel[i], c);
    }
    c.gridwidth = 2;
    c.gridy = 4;
    add(lblLabel[4], c);
    c.gridwidth = 1;
    for (int i = 5; i < 9; i++) {
      c.gridy = i;
      add(lblLabel[i], c);
    }
    c.gridx = 1;
    c.gridy = 1;
    add(cmbObjectType, c);
    c.gridy = 2;
    add(txtObjectName, c);
    c.gridy = 3;
    add(cmbObjectAni, c);
    c.gridy = 5;
    add(cmbBaseFunc, c);
    c.gridy = 6;
    add(cmbBaseOwner, c);
    c.gridy = 7;
    if (shipList != null) {
      add(cmbBaseClass, c);
    }
    c.gridy = 8;
    add(txtBaseName, c);
  }

  @Override
  public void windowActivated(WindowEvent e) {
  }

  @Override
  public void windowClosed(WindowEvent evt) {
    try {
      int stellarId = SECTOR.getStellarObject(SECTOR_INDEX);
      if (stellarId >= 0)
        STELLAR.getStellarObject(stellarId).setName(txtObjectName.getText());

      short outpostId = SECTOR.getStarbase(SECTOR_INDEX);
      if (outpostId >= 0)
        BASEINFO.setName(outpostId, txtBaseName.getText());
    } catch (InvalidArgumentsException e) {
      Dialogs.displayError(e);
    }
  }

  @Override
  public void windowClosing(WindowEvent evt) {
    try {
      int stellarId = SECTOR.getStellarObject(SECTOR_INDEX);
      if (stellarId >= 0) {
        StellEntry stellObj = STELLAR.getStellarObject(stellarId);
        stellObj.setName(txtObjectName.getText());
      }

      short outpostId = SECTOR.getStarbase(SECTOR_INDEX);
      if (outpostId >= 0) {
        BASEINFO.setName(outpostId, txtBaseName.getText());
      }
    } catch (InvalidArgumentsException e) {
      Dialogs.displayError(e);
    }
  }

  @Override
  public void windowDeactivated(WindowEvent e) {
  }

  @Override
  public void windowDeiconified(WindowEvent e) {
  }

  @Override
  public void windowIconified(WindowEvent e) {
  }

  @Override
  public void windowOpened(WindowEvent e) {
  }

}
