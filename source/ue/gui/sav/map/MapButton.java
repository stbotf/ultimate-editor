

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ue.gui.sav.map;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.border.Border;
import lombok.val;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexEventDetailsIdx;
import ue.edit.res.stbof.files.dic.idx.LexMenuIdx;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.sav.files.map.data.StellEntry;
import ue.edit.sav.files.map.data.TaskForce;
import ue.edit.sav.files.map.orders.MilitaryOrder;
import ue.edit.sav.files.map.orders.Order;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.service.Language;

/**
 * @author Alan
 */
public class MapButton extends JButton {

  public static final int DEFAULT_WIDTH = 50;
  public static final int DEFAULT_HEIGHT = 50;

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private static final Color DEFAULT_COLOR = new Color(255,189,58);     // golden orange
  private static final Color CARDASSIAN_COLOR = new Color(107,140,181); // steel blue
  private static final Color FEDERATION_COLOR = Color.CYAN;
  private static final Color FERENGI_COLOR = new Color(230,230,0);      // yellow
  private static final Color KLINGON_COLOR = new Color(230,16,0);       // red
  private static final Color ROMULAN_COLOR = new Color(0,173,16);       // dark green
  private static final Color HIDDEN_COLOR = new Color(130,130,130);     // gray
  private static final Color ERROR_FG_COLOR = Color.WHITE;              // error text
  private static final Color ERROR_BG_COLOR = new Color(180,0,80);      // error background

  private static final Border NORMAL_BORDER = BorderFactory.createLineBorder(new Color(65,65,65));
  private static final Border SELECTION_BORDER = BorderFactory.createLineBorder(Color.WHITE, 2);
  private static final Border CONNECTED_WORMHOLE_BORDER = BorderFactory.createLineBorder(Color.YELLOW, 2);
  private static final Border HL_BORDER = BorderFactory.createLineBorder(Color.CYAN, 2);

  private MapPanel map;
  private int x, y, objType = -1, index;
  private Point link = null;

  private TargaImage check = null;
  private TargaImage main = null;
  private TargaImage base = null;
  private TargaImage[] tf_icon = new TargaImage[7];
  private Font labelFont = new Font("Verdana", Font.PLAIN, 9);
  private JLabel label = null;

  private boolean[][] mov_icon = new boolean[6][9];
  private boolean[][] movs_icon = new boolean[6][9];

  private String sector = null;
  private String hexPos = null;
  private String numPos = null;
  private String title = null;
  private String objdesc = null;
  private String shipdesc = null;
  private int systemId = -1;
  private int stellarId = -1;
  private boolean isHovered = false;
  private boolean isSelected = false;
  private boolean isWormholeSelected = false;

  public MapButton(MapPanel pnl, int x, int y) {
    this.map = pnl;
    this.x = x;
    this.y = y;

    int h = map.sectors.getHSectors();
    this.index = y * h + x;

    setFocusPainted(false);
    setOpaque(true);
    setForeground(Color.WHITE);
    setIconTextGap(0);
    setPreferredSize(new Dimension(MapButton.DEFAULT_WIDTH, MapButton.DEFAULT_HEIGHT));
    setMinimumSize(new Dimension(20, 20));
    setBorder(NORMAL_BORDER);

    sector = map.sectors.getPositionCode(index);
    hexPos = map.sectors.getHexPosition(index, "/");
    numPos = map.sectors.getNumericPosition(index);

    this.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseEntered(MouseEvent e) {
        isHovered = true;
        updateBorder();
      }

      @Override
      public void mouseExited(MouseEvent e) {
        isHovered = false;
        updateBorder();
      }
    });
  }

  public static Dimension getDefaultSize() {
    return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
  }

  private static Color getRaceColor(int raceId) {
    switch (raceId) {
      case 0:
        return CARDASSIAN_COLOR;
      case 1:
        return FEDERATION_COLOR;
      case 2:
        return FERENGI_COLOR;
      case 3:
        return KLINGON_COLOR;
      case 4:
        return ROMULAN_COLOR;
      default:
        return DEFAULT_COLOR;
    }
  }

  public void clearMovementFlags(boolean selection_only) {
    for (int r = 0; r < 6; r++) {
      if (!selection_only) {
        Arrays.fill(movs_icon[r], false);
      }

      Arrays.fill(mov_icon[r], false);
    }
  }

  @Override
  public void setSelected(boolean selected) {
    this.isSelected = selected;
    Point link = this.getLink();
    if (link != null)
      map.map[link.x][link.y].setWormholeSelected(selected);

    updateBorder();
  }

  public void setWormholeSelected(boolean selected) {
    this.isWormholeSelected = selected;
    updateBorder();
  }

  public void updateBorder() {
    this.setBorder(
      isHovered && map.isTargetSelection() ? HL_BORDER
        : isSelected ? SELECTION_BORDER
        : isWormholeSelected ? CONNECTED_WORMHOLE_BORDER
        : NORMAL_BORDER);
  }

  public void refresh(int viewMask, MapDrawOptions drawOptions)
    throws IOException, InvalidArgumentsException {

    if (drawOptions.drawOwnership) {
      check = null;

      // ownership
      int owner = map.sectors.getOwnerMask(index);

      switch (owner) {
        case 0: {
          title = map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.UnclaimedSector).replace("%s", sector);
          break;
        }
        case 1:
        case 2:
        case 4:
        case 8:
        case 16: {
          owner = map.sectors.getOwnerRace(index);

          // sector map empire ownership marking
          String pic = map.empImgPrefixes.getEmpirePrefix(owner) + "_check.tga";
          if (map.res.hasInternalFile(pic))
            this.check = map.res.getTargaImage(pic, LoadFlags.CACHE);

          pic = map.races.getName(owner);
          this.title = map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.SectorX).replace("%s", sector) + " (" + pic + ")";

          break;
        }
        default: {
          String pic = "check.tga";
          if (map.res.hasInternalFile(pic))
            this.check = map.res.getTargaImage(pic, LoadFlags.CACHE);

          this.title = map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.ContestedSector).replace("%s", sector);
        }
      }
    }

    if (drawOptions.drawObjects) {
      main = null;
      link = null;

      // draw star if present
      int item_index = map.sectors.getSystem(index);

      if (item_index >= 0) {
        val system = map.systems.getSystem(item_index);
        int ctrlRace = system.getControllingRace();

        // map sector star icon
        String pic = system.getStarAni() + "map.tga"; //$NON-NLS-1$
        main = map.res.getTargaImage(pic, LoadFlags.CACHE);

        if (ctrlRace >= 0 && ctrlRace < map.races.getNumberOfEntries()) {
          pic = map.races.getName(ctrlRace);
          title = map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.SectorX).replace("%s", sector) + " (" + pic + ")";
        }

        objdesc = "<b>" + system.getName() + "</b>";
        objdesc += "<br>System Index: " + Integer.toHexString(item_index);
        objType = system.getStarType();
        objdesc += "<br>" + getObjType(objType);

        int numPlanets = system.getNumPlanets();
        objdesc += "<br>" + numPlanets;
        objdesc += " " + map.lexicon.getEntry(numPlanets == 1 ?
          LexMenuIdx.Galaxy.Map.Planet : LexMenuIdx.Galaxy.Map.Planets);

        int pop = system.getPopulation();
        if (pop > 0) {
          objdesc = objdesc + "<br>" + map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.Population) + " " + pop;

          // ground combat
          int residentRace = system.getResidentRace();

          if (residentRace >= 0 && residentRace < map.races.getNumberOfEntries()) {
            double techMulti = map.groundCombatTechMulti.getDouble();
            techMulti = techMulti * map.techInfo.getTechLevel((short) residentRace, 6);
            double defense = pop * map.races.getCombatMulti(residentRace) * (1.0 + techMulti);

            if (ctrlRace >= 0 && ctrlRace < NUM_EMPIRES)
              defense = defense * (1.0 + map.empireInfo.getGroundCombatBonus(ctrlRace) / 100.0);

            if (ctrlRace < 0 || ctrlRace > 4)
              ctrlRace = NUM_EMPIRES;

            for (short bid : map.strcInfo.getBuildings((short) item_index)) {
              int num = map.strcInfo.getPoweredOnCount((short) item_index, bid);
              if (num <= 0)
                continue;

              Building bld = map.edifice.building(bid);
              if (bld.getBonusType() == 37)
                defense = defense * (1.0 + num * bld.getBonusValue(ctrlRace) / 100.0);
            }

            objdesc = objdesc + "<br>" + map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.GroundCombat)
              + " " + (int) Math.floor(defense);
          }

          // defense
          int num = 0;

          objdesc = objdesc + "<br><b>" + map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.Defense) + "</b>";

          for (short bid : map.strcInfo.getBuildings((short) item_index)) {
            Building bld = map.edifice.building(bid);
            byte boniType = bld.getBonusType();

            if (boniType == 19 || boniType == 17) {
              num = map.strcInfo.getCount((short) item_index, bid);
              String bldName = num > 1 ? bld.getPluralName() : bld.getName();
              objdesc = objdesc + "<br>" + num + " " + bldName;
            }
          }

          if (num == 0) {
            objdesc = objdesc + "<br><i>" + map.lexicon.getEntry(LexDataIdx.Shared.None) + "</i>";
          }
        }
      } else {
        // stellar object image
        item_index = map.sectors.getStellarObject(index);

        if (item_index >= 0) {
          StellEntry stellObj = map.stellarObjects.getStellarObject(item_index);

          // image
          String pic = stellObj.getAnimation() + "map.tga"; //$NON-NLS-1$

          // map sector stellar icon
          main = map.res.getTargaImage(pic, LoadFlags.CACHE);

          objdesc = "<b>" + stellObj.getName() + "</b>";
          objType = stellObj.getType();
          objdesc = objdesc + "<br>" + getObjType(objType);

          if (stellObj.getType() == 10) {
            objdesc = objdesc + "<br>" + map.lexicon.getEntry(LexEventDetailsIdx.Intel.Target) + ": ";

            int linkId = stellObj.getLink();
            if (linkId >= 0) {
              StellEntry stellLink = map.stellarObjects.getStellarObject(linkId);
              objdesc = objdesc + stellLink.getName();
              int[] p = stellLink.getPosition();
              link = new Point(p[0], p[1]);
            } else {
              objdesc = objdesc + "<i>" + map.lexicon.getEntry(LexDataIdx.Shared.None) + "</i>";
            }
          }
        }
      }
    }

    String outpostText = "";
    String shipText = "";

    if (drawOptions.drawOutposts) {
      this.base = null;
      short item_index = map.sectors.getStarbase(index);

      if (item_index >= 0) {
        int shipType = map.outposts.getShipID(item_index);
        int raceId = map.outposts.getOwner(item_index);
        char raceChar = Character.toLowerCase(map.empImgPrefixes.getEmpirePrefix(raceId));

        String picName = map.outposts.getRole(item_index) == 6
          ? "i_" + raceChar + "o11.tga"
          : "i_" + raceChar + "s.tga";

        if (map.res.hasInternalFile(picName)) {
          // map sector outpost or starbase icon
          this.base = map.res.getTargaImage(picName, LoadFlags.CACHE);
        }

        ShipDefinition shipDef = map.shipModels.getShipDefinition(shipType);
        outpostText = "<br>" + map.outposts.getName(item_index) + " (" + shipDef.getShipClass() + ")";
      }
    }

    if (drawOptions.drawShips) {
      Arrays.fill(this.tf_icon, null);

      int[] tf = map.gWTForce.getSectorTaskForceNumbersByRace(y, x);
      int n, num = map.races.getNumberOfEntries();
      boolean minor = false, alien = false;
      int tf_index = 0;

      for (int r = 0; r < tf.length; r++) {
        n = tf[r];

        if (n > 0) {
          shipText += "<br>" + n + " " + (r < num
            ? map.races.getName(r) + " " + map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.TaskForce)
            : map.shipModels.getIDs(Race.Monster)[r - num - 1]);

          // icon
          String pic = null;
          if (r < NUM_EMPIRES) {
            // major empire
            pic = "i_" + map.empImgPrefixes.getEmpirePrefix(r) + "26.tga";
          } else if (r < num && !minor) {
            // minor race
            pic = "i_m26.tga";
            minor = true;
          } else if (!alien) {
            // alien
            pic = "i_t26.tga";
            alien = true;
          }

          // map sector task force icon
          tf_icon[tf_index] = map.res.getTargaImage(pic, LoadFlags.CACHE);
          tf_index++;
        }

        this.shipdesc = objdesc != null ? "<br><br>" : "";
        this.shipdesc += "<b>" + map.lexicon.getEntry(LexMenuIdx.IntelScreen.DetailReport.Military) + "</b>";
        this.shipdesc += (outpostText.length() > 0 || shipText.length() > 0) ?
          outpostText + shipText : "<br><i>" + map.lexicon.getEntry(LexDataIdx.Shared.None) + "</i>";
      }
    }

    if (drawOptions.drawPaths) {
      markPaths(true);
    }

    // refresh labels
    this.systemId = map.sectors.getSystem(index);
    this.stellarId = map.sectors.getStellarObject(index);
    if (this.systemId == -1 && this.stellarId == -1) {
      // remove obsolete label
      if (this.label != null) {
        map.sysLblPanel.remove(this.label);
        this.label = null;
      }
    }
    else if (this.systemId != -1 && drawOptions.drawSystemLabels
      || this.stellarId != -1 && drawOptions.drawStellarLabels) {

      // add missing label
      if (this.label == null) {
        this.label = new JLabel();
        map.sysLblPanel.add(this.label);
      }

      // check whether they are actually meant to be rendered
      // for the other components this is checked in the paint routine
      boolean visible = viewMask != 0 && this.systemId != -1 ? map.drawOptions.drawSystemLabels : map.drawOptions.drawStellarLabels;

      // check for revealed sectors
      if (viewMask != -1) {
        visible &= (map.sectors.getRevealed(index) & viewMask) != 0
          || (map.sectors.getExplored(index) & viewMask) != 0;
      }

      if (visible) {
        val system = systemId != -1 ? map.systems.getSystem(systemId) : null;
        val stellObj = stellarId != -1 ? map.stellarObjects.getStellarObject(stellarId) : null;
        String name = system != null ? system.getName()
          : stellObj != null ? stellObj.getName() : "???"; //$NON-NLS-1$
        Color col = getRaceColor(system != null ? system.getControllingRace() : -1);

        this.label.setFont(labelFont);
        this.label.setText(name);
        this.label.setForeground(col);

        Dimension lblDim = label.getPreferredSize();
        int cellWidth = (int)(MapButton.DEFAULT_WIDTH * map.zoomFactor);
        int cellHeight = (int)(MapButton.DEFAULT_HEIGHT * map.zoomFactor);

        // center label
        int xpos = x * cellWidth + cellWidth / 2 - lblDim.width / 2;

        // fit to screen
        if (xpos < 0) {
          xpos = 0;
        } else {
          // initial width is 0, therefore check the preferred width
          int maxWidth = map.getPreferredSize().width;
          if (maxWidth > 0 && (xpos + lblDim.width) > maxWidth)
            xpos = maxWidth - lblDim.width;
        }

        label.setBounds(xpos, y * cellHeight + (int)(cellHeight/1.5f), lblDim.width, lblDim.height);
      }
      this.label.setVisible(visible);
    }
  }

  public void markPaths(boolean select) throws KeyNotFoundException {
    // ship movements
    short[] i_tf = map.gWTForce.getSectorTaskForces(x, y);
    boolean not_first, selected;

    for (short tfIndex : i_tf) {
      selected = tfIndex == map.selected_taskforce;
      TaskForce tf = map.gWTForce.taskForce(tfIndex);

      int orderId = tf.getMilitaryOrderId();
      if (orderId == 0)
        continue;

      Order order = map.ordInfo.tryGetTaskForceOrder(tfIndex);
      if (order == null)
        continue;

      MilitaryOrder mil = order.militaryOrder();
      if (mil != null) {
        int mx = 0, my = 0;
        int w = mil.getTargetSectorColumn() - x;
        int h = mil.getTargetSectorRow() - y;
        not_first = false;

        if (w == 0 && h == 0) // not moving
        {
          continue;
        }

        int r = map.gWTForce.taskForce(tfIndex).getOwnerId();
        if (r > 4) {
          r = NUM_EMPIRES;
        }

        while (w - mx != 0 || h - my != 0) {
          if (Math.abs(w - mx) > Math.abs(h - my)) {
            if (w < 0) {
              if (not_first) {
                map.map[x + mx][y + my].movs_icon[r][6] = true;

                if (selected) {
                  map.map[x + mx][y + my].mov_icon[r][6] = select;
                }
              }
              mx--;
            } else {
              if (not_first) {
                map.map[x + mx][y + my].movs_icon[r][2] = true;
                if (selected) {
                  map.map[x + mx][y + my].mov_icon[r][2] = select;
                }
              }
              mx++;
            }
          } else if (Math.abs(w - mx) < Math.abs(h - my)) {
            if (h < 0) {
              if (not_first) {
                map.map[x + mx][y + my].movs_icon[r][0] = true;
                if (selected) {
                  map.map[x + mx][y + my].mov_icon[r][0] = select;
                }
              }
              my--;
            } else {
              if (not_first) {
                map.map[x + mx][y + my].movs_icon[r][4] = true;
                if (selected) {
                  map.map[x + mx][y + my].mov_icon[r][4] = select;
                }
              }
              my++;
            }
          } else {
            if (w < 0) {
              if (h < 0) {
                if (not_first) {
                  map.map[x + mx][y + my].movs_icon[r][7] = true;
                  if (selected) {
                    map.map[x + mx][y + my].mov_icon[r][7] = select;
                  }
                }
                my--;
              } else {
                if (not_first) {
                  map.map[x + mx][y + my].movs_icon[r][5] = true;
                  if (selected) {
                    map.map[x + mx][y + my].mov_icon[r][5] = select;
                  }
                }
                my++;
              }

              mx--;
            } else {
              if (h < 0) {
                if (not_first) {
                  map.map[x + mx][y + my].movs_icon[r][1] = true;
                  if (selected) {
                    map.map[x + mx][y + my].mov_icon[r][1] = select;
                  }
                }
                my--;
              } else {
                if (not_first) {
                  map.map[x + mx][y + my].movs_icon[r][3] = true;
                  if (selected) {
                    map.map[x + mx][y + my].mov_icon[r][3] = select;
                  }
                }
                my++;
              }
              mx++;
            }
          }

          not_first = true;
        }

        map.map[x + mx][y + my].movs_icon[r][8] = true;
        if (selected) {
          map.map[x + mx][y + my].mov_icon[r][8] = select;
        }
      }
    }
  }

  @Override
  protected void paintComponent(Graphics g) {
    // fill background by our own, to avoid pressed JButton background color
    g.setColor(getBackground());
    g.fillRect(0, 0, getWidth(), getHeight());

    int viewMode = map.game.getEmpireViewMode();

    if (viewMode >= -1) {
      String tip = "<html>";

      try {
        int viewMask = map.game.getEmpireViewMask();
        boolean revealed = viewMode == -1 || (map.sectors.getRevealed(index) & viewMask) == viewMask;
        boolean explored = viewMode == -1 || (map.sectors.getExplored(index) & viewMask) == viewMask;

        if (revealed || explored) {
          this.setEnabled(true);

          // color sector
          setBackground(Color.BLACK);

          int owner = map.sectors.getOwnerMask(index);

          switch (owner) {
            case 0: {
              if (explored) {
                tip = tip + "<h3>" + title + "</h3>";
              } else {
                String str = map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.UnexploredSector).replace("%s", sector);
                tip = tip + "<h3>" + str + "</h3>";
              }

              break;
            }
            case 1:
            case 2:
            case 4:
            case 8:
            case 16: {
              owner = map.sectors.getOwnerRace(index);

              if (explored || viewMode == owner || map.empireInfo.isRaceKnown(viewMode, owner)) {
                if (check != null && map.drawOptions.drawOwnership) {
                  Image tga = check.getImage(AlphaMode.AlphaChannel);

                  BufferedImage img = new BufferedImage(tga.getWidth(null), tga.getHeight(null),
                    BufferedImage.TYPE_INT_RGB);

                  Graphics2D g2 = img.createGraphics();
                  g2.drawImage(tga, 0, 0, null);
                  g2.dispose();

                  float scaleFactor = 2.5f;
                  RescaleOp op = new RescaleOp(scaleFactor, -5, null);
                  img = op.filter(img, null);

                  g.drawImage(img.getScaledInstance(getHeight(), getHeight(),
                    Image.SCALE_AREA_AVERAGING), 0, 0, null);
                }

                tip = tip + "<h3>" + title + "</h3>";
              } else {
                String str = map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.UnexploredSector).replace("%s", sector);
                tip = tip + "<h3>" + str + "</h3>";
              }

              break;
            }
            default: {
              if (check != null && map.drawOptions.drawOwnership) {
                Image tga = check.getImage(AlphaMode.AlphaChannel);

                BufferedImage img = new BufferedImage(tga.getWidth(null), tga.getHeight(null),
                    BufferedImage.TYPE_INT_RGB);

                Graphics2D g2 = img.createGraphics();
                g2.drawImage(tga, 0, 0, null);
                g2.dispose();

                float scaleFactor = 2.5f;
                RescaleOp op = new RescaleOp(scaleFactor, -5, null);
                img = op.filter(img, null);

                g.drawImage(
                    img.getScaledInstance(getHeight(), getHeight(), Image.SCALE_AREA_AVERAGING),
                    0, 0, null);
              }

              tip = tip + "<h3>" + title + "</h3>";
            }
          }

          tip = tip + "<table style=\"margin: -12 0 10 -2\" width=\"100%\">"
              + "<td style=\"padding: 0\">"
              + "Position: " + numPos
              + "</td>"
              + "<td style=\"padding: 0; text-align: right\">"
              + " hex: " + hexPos
              + "</td>"
              + "<td style=\"padding: 0; text-align: right\">"
              + " idx: " + Integer.toHexString(index)
              + "</td>"
              + "</table>";

          // draw star/object if present
          if (main != null) {
            int size = getHeight() / 3;

            if (map.drawOptions.drawObjects) {
              Image tga = main.getImage(AlphaMode.FirstPixelAlpha);

              BufferedImage img = new BufferedImage(tga.getWidth(null), tga.getHeight(null),
                  BufferedImage.TYPE_INT_ARGB);

              Graphics2D g2 = img.createGraphics();
              g2.drawImage(tga, 0, 0, null);
              g2.dispose();

              g.drawImage(img.getScaledInstance(size, size, Image.SCALE_AREA_AVERAGING), size, size,
                  null);
            }

            if (objdesc != null && explored) {
              tip = tip + objdesc;
            } else if (objType >= 0) {
              String str = getObjType(objType);
              tip = tip + str;
            }
          }

          // draw base
          if (base != null && map.drawOptions.drawOutposts) {
            Image tga = base.getImage(AlphaMode.FirstPixelAlpha);

            int size = getHeight() / 4;

            BufferedImage img = new BufferedImage(tga.getWidth(null), tga.getHeight(null),
                BufferedImage.TYPE_INT_ARGB);

            Graphics2D g2 = img.createGraphics();
            g2.drawImage(tga, 0, 0, null);
            g2.dispose();

            g.drawImage(img.getScaledInstance(size, size, Image.SCALE_AREA_AVERAGING), 0, 3 * size,
                null);
          }

          // draw task forces
          int w = getWidth() / 4;

          if (w > 0 && map.drawOptions.drawShips) {
            int offset = 0;
            int h_offset = 0;
            int h;

            for (int i = 0; i < 7 && tf_icon[i] != null; i++) {
              Image tga = tf_icon[i].getImage(AlphaMode.FirstPixelAlpha);

              h = (int) (w / (double) tga.getWidth(null) * tga.getHeight(null));

              if (i == 4) {
                offset = 0;
                h_offset += h;
              }

              BufferedImage img = new BufferedImage(tga.getWidth(null), tga.getHeight(null),
                  BufferedImage.TYPE_INT_ARGB);

              Graphics2D g2 = img.createGraphics();
              g2.drawImage(tga, 0, 0, null);
              g2.dispose();

              g.drawImage(img.getScaledInstance(w, h, Image.SCALE_AREA_AVERAGING), offset, h_offset,
                  null);

              offset += w;
            }
          }

          if (shipdesc != null) {
            tip = tip + shipdesc;
          }

          // ship paths
          if (map.drawOptions.drawPaths) {
            if (viewMode == -1) {
              for (int race = 0; race < 6; race++) {
                this.drawPaths(race, movs_icon, map.movs_icon, g);
              }
            } else {
              this.drawPaths(viewMode, movs_icon, map.movs_icon, g);
            }
            if (viewMode == -1) {
              for (int race = 0; race < 6; race++) {
                this.drawPaths(race, mov_icon, map.mov_icon, g);
              }
            } else {
              this.drawPaths(viewMode, mov_icon, map.mov_icon, g);
            }
          }
        } else {
          setBackground(HIDDEN_COLOR);
          // String str = map.lexicon.getEntry(Menu.Galaxy.Map.UnexploredSector).replace("%s", sector);
          // tip = tip + "<h3>" + str + "</h3>";
          setToolTipText(null);
          setEnabled(false);
          return;
        }
      } catch (Exception ex) {
        // don't throw but render error text and set tooltip
        ex.printStackTrace();
        setBackground(ERROR_BG_COLOR);
        g.setFont(labelFont);
        g.setColor(ERROR_FG_COLOR);
        g.drawString("error", 3, 9);
        tip += ex.toString();
        setEnabled(false);
      }

      tip = tip + "</html>";
      this.setToolTipText(tip);
    } else {
      setBackground(HIDDEN_COLOR);
      setToolTipText(null);
      setEnabled(false);
    }
  }

  private void drawPaths(int race, boolean[][] flags, Image[] icons, Graphics g) {
    int w = getWidth();
    int h = getHeight();
    int iw, ih, ix, iy;

    double scale = w / 5.0 / icons[8].getWidth(null);

    for (int i = 0; i < 9; i++) {
      if (flags[race][i]) {
        iw = (int) (icons[i].getWidth(null) * scale);
        ih = (int) (icons[i].getHeight(null) * scale);

        if (i == 0 || i == 4 || i == 8) {
          ix = (w - iw) / 2;
        } else if (i >= 1 && i <= 3) {
          ix = w - iw;
        } else {
          ix = 0;
        }

        if (i == 6 || i == 2 || i == 8) {
          iy = (h - ih) / 2;
        } else if (i >= 3 && i <= 5) {
          iy = h - ih;
        } else {
          iy = 0;
        }

        BufferedImage img = new BufferedImage(icons[i].getWidth(null), icons[i].getHeight(null),
            BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = img.createGraphics();
        g2.drawImage(icons[i], 0, 0, null);
        g2.dispose();

        g.drawImage(img.getScaledInstance(iw, ih, Image.SCALE_AREA_AVERAGING), ix, iy, null);
      }
    }
  }

  private String getObjType(int type) {
    switch (type) {
      case 0:
        return Language.getString("SystemsInfoGUI.13");
      case 1:
        return Language.getString("SystemsInfoGUI.14");
      case 2:
        return Language.getString("SystemsInfoGUI.15");
      case 3:
        return Language.getString("SystemsInfoGUI.16");
      case 4:
        return Language.getString("SystemsInfoGUI.17");
      case 5:
        return Language.getString("SystemsInfoGUI.18");
      case 6:
        return Language.getString("SystemsInfoGUI.19");
      case 7:
        return Language.getString("SystemsInfoGUI.20");
      case 8:
        return Language.getString("SystemsInfoGUI.21");
      case 9:
        return Language.getString("SystemsInfoGUI.22");
      case 10:
        return Language.getString("SystemsInfoGUI.23");
      default:
        return Language.getString("SystemsInfoGUI.24");
    }
  }

  public Point getLink() {
    return link;
  }
}
