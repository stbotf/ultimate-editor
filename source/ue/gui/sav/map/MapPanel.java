package ue.gui.sav.map;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.function.Consumer;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.seg.prim.LongValueSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SD_GroundCombat;
import ue.edit.exe.trek.seg.emp.EmpireImgPrefixes;
import ue.edit.exe.trek.seg.map.PlanetBonusTypes;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.emp.EmpsInfo;
import ue.edit.sav.files.emp.StrcInfo;
import ue.edit.sav.files.emp.TechInfo;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.OrderInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.data.Ship;
import ue.edit.sav.files.map.data.TaskForce;
import ue.edit.sav.tools.SavFileFilters;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.gui.common.FileChanges;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.SelectionEvent;
import ue.gui.util.event.SelectionListener;
import ue.service.Language;

public class MapPanel extends JPanel {

  static final String Prefix = "mapPanel";

  // read
  SavGame game;
  Stbof res;
  Trek exe;
  // stbof.res
  ShipList shipModels;
  Edifice edifice;
  Lexicon lexicon;
  RaceRst races;
  Environ environ;
  Planspac planspac;
  Planboni planboni;
  Objstruc objstruc;
  // trek.exe
  EmpireImgPrefixes empImgPrefixes;
  PlanetBonusTypes planBoniTypes;
  LongValueSegment groundCombatTechMulti;
  // saved game
  SavGameInterface sgif;
  EmpsInfo empireInfo;
  TechInfo techInfo;

  // edited
  // directly
  Sector sectors;
  SystInfo systems;
  StellInfo stellarObjects;
  StarBaseInfo outposts;
  StrcInfo strcInfo;
  // by MapTools
  TaskForceList gWTForce;
  OrderInfo ordInfo;
  GShipList gShipList;

  // ui components
  Image[] mov_icon = new Image[9];
  Image[] movs_icon = new Image[9];
  MapButton[][] map;
  JPanel sysLblPanel = new JPanel();
  private JLayeredPane mapPanel = new JLayeredPane();
  private JPanel sectorPanel = new JPanel();

  // data
  MapDrawOptions drawOptions = MapDrawOptions.fromSettings();
  @Getter @Setter private int shipCount = 1;
  int selected_taskforce = -1;
  Point selection1 = null;
  float zoomFactor = 1.0f;
  private Point prev_selection1 = null;

  // listeners
  private ArrayList<ActionListener> listeners;
  private ArrayList<SelectionListener> right_click_listeners;
  private Consumer<Point> onTargetSelection = null;

  public MapPanel(SavGame game, Stbof stbof, Trek trek) throws IOException {
    this(game, stbof, trek, -1);
  }

  public MapPanel(SavGame game, Stbof stbof, Trek trek, int sectorIndex) throws IOException {
    this.game = game;
    this.res = stbof;
    this.exe = trek;
    sgif = game.files();

    // use GridBagLayout to center the map both horizontal and vertical
    setLayout(new GridBagLayout());
    setBackground(Color.BLACK);

    // wrap sector panel for the overlays
    mapPanel.setLayout(new GridBagLayout());

    sysLblPanel.setLayout(null);
    sysLblPanel.setOpaque(false);

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    mapPanel.add(sectorPanel, c);
    mapPanel.setLayer(sectorPanel, JLayeredPane.DEFAULT_LAYER);
    mapPanel.add(sysLblPanel, c);
    mapPanel.setLayer(sysLblPanel, JLayeredPane.PALETTE_LAYER);
    add(mapPanel);


    ToolTipManager.sharedInstance().setDismissDelay(10000);

    shipModels = (ShipList) res.getInternalFile(CStbofFiles.ShipListSst, true);
    edifice = (Edifice) res.getInternalFile(CStbofFiles.EdificeBst, true);
    lexicon = (Lexicon) res.getInternalFile(CStbofFiles.LexiconDic, true);
    races = (RaceRst) res.getInternalFile(CStbofFiles.RaceRst, true);
    environ = (Environ) res.getInternalFile(CStbofFiles.EnvironEst, true);
    planspac = (Planspac) res.getInternalFile(CStbofFiles.PlanSpacBin, true);
    planboni = (Planboni) res.getInternalFile(CStbofFiles.PlanBoniBin, true);
    objstruc = (Objstruc) res.getInternalFile(CStbofFiles.ObjStrucSmt, true);

    empImgPrefixes = (EmpireImgPrefixes) exe
        .getInternalFile(CTrekSegments.EmpireImagePrefixes, true);
    planBoniTypes = (PlanetBonusTypes) exe.getInternalFile(CTrekSegments.PlanetBonusTypes, true);
    groundCombatTechMulti = (LongValueSegment) exe.getSegment(SD_GroundCombat.TechMultiplier, false);

    sectors = (Sector) game.getInternalFile(CSavFiles.SectorLst, true);
    systems = (SystInfo) game.getInternalFile(CSavFiles.SystInfo, true);
    stellarObjects = (StellInfo) game.getInternalFile(CSavFiles.StellInfo, true);
    outposts = (StarBaseInfo) game.getInternalFile(CSavFiles.StarBaseInfo, true);
    strcInfo = (StrcInfo) game.getInternalFile(CSavFiles.StrcInfo, true);
    empireInfo = (EmpsInfo) game.getInternalFile(CSavFiles.EmpsInfo, true);
    gWTForce = (TaskForceList) game.getInternalFile(CSavFiles.GWTForce, true);
    ordInfo = (OrderInfo) game.getInternalFile(CSavFiles.OrdInfo, true);
    gShipList = (GShipList) game.getInternalFile(CSavFiles.GShipList, true);
    techInfo = (TechInfo) game.getInternalFile(CSavFiles.TechInfo, true);

    mov_icon[0] = res.getTargaImage("mov_n.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[1] = res.getTargaImage("mov_ne.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[2] = res.getTargaImage("mov_e.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[3] = res.getTargaImage("mov_se.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[4] = res.getTargaImage("mov_s.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[5] = res.getTargaImage("mov_sw.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[6] = res.getTargaImage("mov_w.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[7] = res.getTargaImage("mov_nw.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    mov_icon[8] = res.getTargaImage("mov.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);

    movs_icon[0] = res.getTargaImage("mov_ns.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[1] = res.getTargaImage("mov_nes.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[2] = res.getTargaImage("mov_es.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[3] = res.getTargaImage("mov_ses.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[4] = res.getTargaImage("mov_ss.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[5] = res.getTargaImage("mov_sws.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[6] = res.getTargaImage("mov_ws.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[7] = res.getTargaImage("mov_nws.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);
    movs_icon[8] = res.getTargaImage("movs.tga", LoadFlags.UNCACHED).getImage(AlphaMode.FirstPixelAlpha);

    listeners = new ArrayList<ActionListener>();
    right_click_listeners = new ArrayList<SelectionListener>();

    // auto-select sector
    if (sectorIndex != -1) {
      int[] pos = sgif.sectorLst().getPosition(sectorIndex);
      selection1 = new Point(pos[0], pos[1]);
    }

    createMap();
  }

  public boolean isTargetSelection() {
    return onTargetSelection != null;
  }

  public void selectTaskForcePath(short tf_index, boolean select) throws KeyNotFoundException {
    if (selection1 != null) {
      // deselct
      if (prev_selection1 != null)
        map[prev_selection1.x][prev_selection1.y].markPaths(false);

      map[selection1.x][selection1.y].markPaths(false);

      // set new index and select
      if (select) {
        selected_taskforce = tf_index;
        map[selection1.x][selection1.y].markPaths(true);
      } else {
        selected_taskforce = -1;
      }

      refreshMap(MapDrawOptions.BASIC);
    }
  }

  public void addActionListener(ActionListener l) {
    listeners.add(l);
  }

  public void addRightClickListener(SelectionListener l) {
    right_click_listeners.add(l);
  }

  private void createMap() {
    int vert = sectors.getVSectors();
    int hor = sectors.getHSectors();

    sectorPanel.removeAll();
    sectorPanel.setLayout(new GridLayout(vert, hor, 0, 0));
    map = new MapButton[hor][vert];

    for (int y = 0; y < vert; y++) {
      for (int x = 0; x < hor; x++) {
        map[x][y] = new MapButton(this, x, y);
        map[x][y].addActionListener(new MapSelectionListener(x, y));
        map[x][y].addMouseListener(new MapRightClickSelectionListener(x, y, y * hor + x));
        sectorPanel.add(map[x][y]);
      }
    }

    refreshMap(MapDrawOptions.FULL);
  }

  public void refreshMap(MapDrawOptions drawOptions) {
    setVisible(false);

    if (drawOptions.drawShips) {
      drawOptions.drawPaths = true;
    }

    if (drawOptions.drawPaths) {
      for (int y = 0; y < map[0].length; y++) {
        for (int x = 0; x < map.length; x++) {
          map[x][y].clearMovementFlags(false);
        }
      }
    }

    HashSet<String> reported = new HashSet<>();
    ArrayList<String> errors = new ArrayList<>();
    int viewMask = game.getEmpireViewMask();

    for (int y = 0; y < map[0].length; y++) {
      for (int x = 0; x < map.length; x++) {
        try {
          map[x][y].refresh(viewMask, drawOptions);
        } catch (Exception ex) {
          String error = ex.getMessage();
          // check whether the error was already added
          // to not report them twice
          if (reported.add(error))
            errors.add(error);
        }
      }
    }

    if (!errors.isEmpty()) {
      String errorMsg = String.join("\n", errors);
      Dialogs.displayError(new Exception(errorMsg));
    }

    if (selection1 != null) {
      setSelected(selection1, true);
    }
    setVisible(true);
  }

  public void setViewMode(int viewMode) {
    game.setEmpireViewMode(viewMode);

    // clear selection
    if (selection1 != null) {
      setSelected(selection1, false);
      selection1 = null;
    }

    refreshMap(MapDrawOptions.RACE_SPECIFIC);
  }

  private void setSelected(Point pnt, boolean selected) {
    if (pnt != null && pnt.x < map.length && pnt.y < map[0].length)
      map[pnt.x][pnt.y].setSelected(selected);
  }

  public void zoomIn() {
    zoomFactor = snapZoom(zoomFactor * 1.1f);
    updateZoom();
  }

  public void zoomOut() {
    zoomFactor = snapZoom(zoomFactor / 1.1f);
    updateZoom();
  }

  private void updateZoom() {
    Dimension dim = MapButton.getDefaultSize();

    dim.height *= zoomFactor;
    dim.width = dim.height;
    map[0][0].setPreferredSize(dim);

    Dimension dp = sectorPanel.getPreferredSize();
    dp.height = map[0].length * dim.height;
    dp.width = map.length * dim.width;

    sectorPanel.setPreferredSize(dp);
    sectorPanel.invalidate();

    refreshMap(MapDrawOptions.ZOOM);
  }

  private static float snapZoom(float zoomFactor) {
    // limit
    if (zoomFactor < 0.2f)
      return 0.2f;
    // snap
    if (zoomFactor < 0.95f)
      return (float) Math.ceil(zoomFactor * 100) / 100;
    else if (zoomFactor > 1.05f)
      return (float) Math.floor(zoomFactor * 10) / 10;
    return 1.0f;
  }

  protected void fireAction(ActionEvent evt) {
    for (ActionListener l : listeners) {
      l.actionPerformed(evt);
    }
  }

  private void fireRightClick(int x, int y, SelectionEvent selectionEvent) {
    Point pnt = new Point(x, y);

    if (selection1 == null || !selection1.equals(pnt)) {
      map[x][y].doClick();
    }

    for (SelectionListener l : right_click_listeners) {
      l.selected(selectionEvent);
    }
  }

  public boolean hasChanges() {
    // edited directly
    if (sectors.madeChanges()
      || systems.madeChanges()
      || stellarObjects.madeChanges()
      || outposts.madeChanges()
      || strcInfo.madeChanges())
      return true;

    // edited by MapTools
    if (gWTForce.madeChanges()
      || ordInfo.madeChanges()
      || gShipList.madeChanges()
      || game.isChanged(CSavFiles.GalInfo)
      || game.isChanged(CSavFiles.GTForceList)
      || game.isChanged(CSavFiles.AIShpUnt)
      || game.isChanged(CSavFiles.AIShpUntCnt)
      || game.isChanged(CSavFiles.MonsterLst)
      || game.isChanged(CSavFiles.ResultLst)
      || game.hasBeenModified(SavFileFilters.AlienTF)
      || game.hasBeenModified(SavFileFilters.AgtSh)
      || game.hasBeenModified(SavFileFilters.AgtShCnt)
      || game.hasBeenModified(SavFileFilters.TskSh)
      || game.hasBeenModified(SavFileFilters.TskShCnt)
      || game.hasBeenModified(SavFileFilters.AIAgent)
      || game.hasBeenModified(SavFileFilters.AITask))
      return true;

    // edited by SystemTools add/remove system
    if (game.isChanged(CSavFiles.AIAgtIdCtr)
      || game.isChanged(CSavFiles.AINumAgents)
      || game.isChanged(CSavFiles.AINumTasks)
      || game.isChanged(CSavFiles.AISysUnt)
      || game.isChanged(CSavFiles.AISysUntCnt)
      || game.isChanged(CSavFiles.AlienInfo)
      || game.hasBeenModified(CSavFiles.ProvInfos)
      || game.isChanged(CSavFiles.RToSInfo)
      || game.hasBeenModified(CSavFiles.TrdeInfos)
      || game.hasBeenModified(CSavFiles.TrdeInfoCnts)
      || game.hasBeenModified(SavFileFilters.AgtDp)
      || game.hasBeenModified(SavFileFilters.AgtDpCnt)
      || game.hasBeenModified(SavFileFilters.AgtSy)
      || game.hasBeenModified(SavFileFilters.AgtSyCnt)
      || game.hasBeenModified(SavFileFilters.AgtTk)
      || game.hasBeenModified(SavFileFilters.AgtTkCnt)
      || game.hasBeenModified(SavFileFilters.TskSy)
      || game.hasBeenModified(SavFileFilters.TskSyCnt))
      return true;

    return false;
  }

  public void listChangedFiles(FileChanges changes) {
    // edited directly
    game.checkChanged(sectors, changes);
    game.checkChanged(systems, changes);
    game.checkChanged(stellarObjects, changes);
    game.checkChanged(outposts, changes);
    game.checkChanged(strcInfo, changes);

    // edited by MapTools
    game.checkChanged(gWTForce, changes);
    game.checkChanged(ordInfo, changes);
    game.checkChanged(gShipList, changes);
    game.checkChanged(CSavFiles.GalInfo, changes);
    game.checkChanged(CSavFiles.GTForceList, changes);
    game.checkChanged(CSavFiles.AIShpUnt, changes);
    game.checkChanged(CSavFiles.AIShpUntCnt, changes);
    game.checkChanged(CSavFiles.MonsterLst, changes);
    game.checkChanged(CSavFiles.ResultLst, changes);
    game.checkChanged(SavFileFilters.AlienTF,changes);
    game.checkChanged(SavFileFilters.AgtSh, changes);
    game.checkChanged(SavFileFilters.AgtShCnt, changes);
    game.checkChanged(SavFileFilters.TskSh, changes);
    game.checkChanged(SavFileFilters.TskShCnt, changes);
    game.checkChanged(SavFileFilters.AIAgent, changes);
    game.checkChanged(SavFileFilters.AITask, changes);

    // edited by SystemTools add/remove system
    game.checkChanged(CSavFiles.AIAgtIdCtr, changes);
    game.checkChanged(CSavFiles.AINumAgents, changes);
    game.checkChanged(CSavFiles.AINumTasks, changes);
    game.checkChanged(CSavFiles.AISysUnt, changes);
    game.checkChanged(CSavFiles.AISysUntCnt, changes);
    game.checkChanged(CSavFiles.AlienInfo, changes);
    game.checkChanged(CSavFiles.ProvInfos, changes);
    game.checkChanged(CSavFiles.RToSInfo, changes);
    game.checkChanged(CSavFiles.TrdeInfos, changes);
    game.checkChanged(CSavFiles.TrdeInfoCnts, changes);
    game.checkChanged(SavFileFilters.AgtDp, changes);
    game.checkChanged(SavFileFilters.AgtDpCnt, changes);
    game.checkChanged(SavFileFilters.AgtSy, changes);
    game.checkChanged(SavFileFilters.AgtSyCnt, changes);
    game.checkChanged(SavFileFilters.AgtTk, changes);
    game.checkChanged(SavFileFilters.AgtTkCnt, changes);
    game.checkChanged(SavFileFilters.TskSy, changes);
    game.checkChanged(SavFileFilters.TskSyCnt, changes);
  }

  public void reload() throws IOException {
    // reload cached files
    sectors = (Sector) game.getInternalFile(CSavFiles.SectorLst, true);
    systems = (SystInfo) game.getInternalFile(CSavFiles.SystInfo, true);
    stellarObjects = (StellInfo) game.getInternalFile(CSavFiles.StellInfo, true);
    outposts = (StarBaseInfo) game.getInternalFile(CSavFiles.StarBaseInfo, true);
    strcInfo = (StrcInfo) game.getInternalFile(CSavFiles.StrcInfo, true);
    gWTForce = (TaskForceList) game.getInternalFile(CSavFiles.GWTForce, true);
    ordInfo = (OrderInfo) game.getInternalFile(CSavFiles.OrdInfo, true);
    gShipList = (GShipList) game.getInternalFile(CSavFiles.GShipList, true);

    refreshMap();
  }

  public void refreshMap() {
    // refresh the currently rendered components
    refreshMap(drawOptions);

    if (selection1 != null) {
      val button = map[selection1.x][selection1.y];
      fireAction(new ActionEvent(button, ActionEvent.ACTION_PERFORMED, null));
    }
  }

  private class MapSelectionListener implements ActionListener {

    int x, y;

    public MapSelectionListener(int x, int y) {
      this.x = x;
      this.y = y;
    }

    @Override
    public void actionPerformed(ActionEvent arg) {
      if (onTargetSelection != null) {
        onTargetSelection.accept(new Point(x, y));
        return;
      }

      // update selection
      prev_selection1 = selection1;
      selection1 = new Point(x, y);

      // reset previous selection
      if (prev_selection1 != null) {
        setSelected(prev_selection1, false);
        if (prev_selection1.equals(selection1))
          selection1 = null;
      }

      // apply new selection
      if (selection1 != null)
        setSelected(selection1, true);

      fireAction(new ActionEvent(map[x][y], ActionEvent.ACTION_PERFORMED, null));
    }
  }

  private class MapRightClickSelectionListener extends MouseAdapter {

    int x, y;
    int index;

    public MapRightClickSelectionListener(int x, int y, int index) {
      this.x = x;
      this.y = y;
      this.index = index;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      // do not depend on the right click event,
      // it doesn't fire all the time, but only on certain clicks
      // this can be time or layout dependent or whatsoever

      if (e.getButton() == MouseEvent.BUTTON3) {
        // cancel target selection
        if (onTargetSelection != null) {
          onTargetSelection = null;
          map[x][y].updateBorder();
          return;
        }

        fireRightClick(x, y, new SelectionEvent(new int[]{-1, index}, e.getLocationOnScreen()));
      }
    }
  }

  public String getTaskForceTitle(short taskForceId) throws InvalidArgumentsException {
    TaskForce gtf = gWTForce.taskForce(taskForceId);
    short[] shipIDs = gtf.getShipIDs();
    int tfOwner = gtf.getOwnerId();
    return getShipTitleString(tfOwner, shipIDs);
  }

  public String getTaskForceIdentifier(short taskForceId, boolean withRace) throws KeyNotFoundException {
    TaskForce gtf = gWTForce.taskForce(taskForceId);
    int tfOwner = gtf.getOwnerId();

    // check size for monsters that have no race entry
    String race = withRace && tfOwner < races.getNumberOfEntries() ?
        races.getName(tfOwner) + ": " : "";

    short[] shipIDs = gtf.getShipIDs();
    String title = getShipTitleString(tfOwner, shipIDs);
    return taskForceId + ": " + race + title;
  }

  public String getShipTitleString(int race, short[] shipIDs) throws KeyNotFoundException {
    if (shipIDs.length == 0) {
      return "Error: No ships in this task force";
    } else if (shipIDs.length == 1) {
      Ship ship = gShipList.getShip(shipIDs[0]);
      ShipDefinition shipDef = shipModels.getShipDefinition(ship.getShipType());
      return "1 %1".replace("%1", shipDef.getName());
    } else {
      boolean support = false;
      boolean military = false;
      boolean mixed = false;
      boolean sameType = true;
      int f1, f2, shID1, shID2;

      Ship ship1 = gShipList.getShip(shipIDs[0]);
      shID1 = ship1.getShipType();
      ShipDefinition shDef1 = shipModels.getShipDefinition(shID1);
      f1 = shDef1.getShipRole();

      if (f1 == 5 || f1 == 9) {
        support = true;
      } else {
        military = true;
      }

      for (int i = 1; i < shipIDs.length; i++) {
        Ship ship2 = gShipList.getShip(shipIDs[i]);
        shID2 = ship2.getShipType();
        ShipDefinition shDef2 = shipModels.getShipDefinition(shID2);
        f2 = shDef2.getShipRole();

        if (f1 != f2) {
          mixed = true;
          sameType = false;
        } else if (shID1 != shID2) {
          sameType = false;
        }

        if (f2 == 5 || f2 == 9) {
          support = true;
        } else {
          military = true;
        }
      }

      if (sameType) {
        return "%1 %2s".replace("%1", Integer.toString(shipIDs.length))
          .replace("%2", shDef1.getName());
      } else if (mixed) {
        if (support) {
          if (military) {
            return "%1 ships".replace("%1", Integer.toString(shipIDs.length));
          } else {
            return "%1 support ships".replace("%1", Integer.toString(shipIDs.length));
          }
        } else {
          if (race == 1) {
            return "%1 starships".replace("%1", Integer.toString(shipIDs.length));
          } else {
            return "%1 warships".replace("%1", Integer.toString(shipIDs.length));
          }
        }
      } else {
        String func;

        switch (f1) {
          case 0:
            func = lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.ScoutShip);
            break;
          case 1:
            func = lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.Destroyer);
            break;
          case 2:
            func = lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.Cruiser);
            break;
          case 3:
            func = lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.StrikeCruiser);
            break;
          case 4:
            func = lexicon.getEntry(LexDataIdx.Other.ShipRole.CommandShip);
            break;
          case 5:
            func = lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.ColonyShip);
            break;
          case 6:
            func = lexicon.getEntry(LexDataIdx.Combat.ShipGroup.Outpost);
            break;
          case 7:
            func = lexicon.getEntry(LexDataIdx.Combat.ShipGroup.Starbase);
            break;
          case 9:
            func = lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.Transport);
            break;
          default:
            func = Language.getString("ShipGUI.1" /*Alien*/);
            break;
        }

        return "%1 %2s".replace("%1", Integer.toString(shipIDs.length))
            .replace("%2", func);
      }
    }
  }

  public void moveStellarObject(short stellId) {
    onTargetSelection = (target) -> performStellarObjectMove(stellId, target);
  }

  public void performStellarObjectMove(short stellId, Point target) {
    try {
      onTargetSelection = null;
      sgif.stellarTools().moveStellarObject(stellId, target.x, target.y);
      refreshMap();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }

  public void moveOutpost(short outpostId) {
    onTargetSelection = (target) -> performOutpostMove(outpostId, target);
  }

  public void performOutpostMove(short outpostId, Point target) {
    try {
      onTargetSelection = null;
      sgif.starbaseTools().moveStarbase(outpostId, target.x, target.y, false);
      refreshMap();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }

  public void switchSectors(int sectorId) {
    onTargetSelection = (target) -> performSectorSwitch(sectorId, target);
  }

  public void performSectorSwitch(int sectorId, Point target) {
    try {
      onTargetSelection = null;
      sgif.mapTools().switchSectors(sectorId, target.x, target.y);
      refreshMap();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }

  public void moveTaskForce(short taskForceId) {
    onTargetSelection = (target) -> performTaskForceMove(taskForceId, target);
  }

  public void performTaskForceMove(short taskForceId, Point target) {
    try {
      onTargetSelection = null;
      sgif.taskForceTools().moveTaskForce(taskForceId, target.x, target.y, false);
      refreshMap();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }

  public void moveAllSectorTaskForces(int sectorIndex) {
    onTargetSelection = (target) -> performAllSectorTaskForceMove(sectorIndex, target);
  }

  public void performAllSectorTaskForceMove(int sectorIndex, Point target) {
    try {
      onTargetSelection = null;
      sgif.taskForceTools().moveAllSectorTaskForces(sectorIndex, target.x, target.y);
      refreshMap();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }

  public void moveSystem(short systemId) {
    onTargetSelection = (target) -> performSystemMove(systemId, target);
  }

  public void performSystemMove(short systemId, Point target) {
    try {
      onTargetSelection = null;
      sgif.mapTools().moveSystem(systemId, target.x, target.y);
      refreshMap();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }
}
