package ue.gui.sav.map;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import org.kordamp.ikonli.materialdesign.MaterialDesign;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.dic.idx.LexMenuIdx;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.sav.SavGame;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.gui.common.InfoPanel;
import ue.gui.sav.emp.SystemsInfoGUI;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.util.component.IconButton;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;

public class StarPanel extends InfoPanel {

  private IconButton btnSystemEdit = IconButton.CreateSmallButton(MaterialDesign.MDI_ADJUST);
  private AniLabel aniLbl;

  public StarPanel(int key, SavGame sav, Stbof stbof, SystemEntry system, int imgSize) throws IOException {
    this(key, sav, stbof, system, getAnimation(stbof, system), imgSize);
  }

  public StarPanel(int key, SavGame sav, Stbof stbof, SystemEntry system, AniOrCur ani, int imgSize) throws IOException {
    super(new int[] { key, system.index() });
    aniLbl = new AniLabel();

    // first pixel alpha must be set before loading the frames
    aniLbl.setForceFirstPixelAlpha(true);

    // frame size must be adjusted before loading the frames
    Dimension frameSize = ani.getFrameDim();
    if (frameSize.height > imgSize) {
      // compute scaled frame width by image height
      double ratio = imgSize / (double) frameSize.height;
      int scaledWidth = (int) (ratio * frameSize.width);
      // limit by the larger size
      int limit = Integer.max(scaledWidth, imgSize);
      aniLbl.setMaxFrameSize(limit);
    }

    // load animation frames
    aniLbl.setAnimation(ani, true);

    // add dilithium sign
    TargaImage dilIcon = system.hasDilithium() ? stbof.getTargaImage("bid.tga", LoadFlags.CACHE) : null; //$NON-NLS-1$
    aniLbl.setOverlayImg(0, 0, dilIcon);

    String stellType = stbof.files().stellarTools().mapStellarType(system.getStarType());
    initComponents(aniLbl, system.getName(), stellType, null);

    btnSystemEdit.setToolTipText(Language.getString("StarPanel.0")); //$NON-NLS-1$

    btnSystemEdit.addActionListener(new CBActionListener(evt -> {
      UE.WINDOW.showPanel(new SystemsInfoGUI(sav, stbof, system.index()));
    }));

    if (system.hasDilithium()) {
      int lexId = LexMenuIdx.Galaxy.SystemPanel.DilithiumPresent;
      setToolTipText(LexHelper.mapEntry(lexId, Language.getString("StarPanel.1"))); //$NON-NLS-1$
    }
  }

  public double getRatio(int imgSize) {
    AniOrCur ani = getAnimation();
    if (ani != null) {
      Dimension size = ani.getFrameDim();
      if (imgSize < size.height)
        return imgSize / (double) size.height;
    }
    return 1;
  }

  public AniOrCur getAnimation() {
    return aniLbl != null ? aniLbl.getAnimation() : null;
  }

  private static AniOrCur getAnimation(Stbof stbof, SystemEntry system) throws IOException {
    String pic = system.getAni();
    return pic.toLowerCase().endsWith(".ani") && stbof.hasInternalFile(pic)
      ? stbof.getAnimation(pic, LoadFlags.CACHE) : null;
  }

  @Override
  protected void placeComponents() {
    removeAll();
    setLayout(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    c.weighty = 1;
    add(createItemPanel(), c);
    c.anchor = GridBagConstraints.NORTHEAST;
    c.fill = GridBagConstraints.NONE;
    c.insets = new Insets(5, 5, 5, 5);
    c.weighty = 0;
    add(btnSystemEdit, c);
  }

}
