package ue.gui.sav.map;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.exception.KeyNotFoundException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.func.FunctionTools;

/**
 * Used to edit stellar objects info in a saved game.
 */
public class StarBaseInfoGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final String STR_UNKOWN = "???"; //$NON-NLS-1$

  private String strCoords = Language.getString("StarBaseInfoGUI.4"); //$NON-NLS-1$

  // read
  private SavGame savGame;
  private RaceRst RACE;
  private ShipList shipList;

  // edited
  private StarBaseInfo BASE;

  // ui components
  private JComboBox<ID> jcbBASE = new JComboBox<ID>();
  private JComboBox<ID> jcbSHIP_ID = new JComboBox<ID>();
  private JComboBox<String> jcbROLE = new JComboBox<String>();
  private JComboBox<ID> jcbRACE = new JComboBox<ID>();
  private JLabel[] lblLABEL = new JLabel[6];
  private JLabel lblMaxHull = new JLabel();
  private JTextField txtHull = new JTextField();
  private JButton btnEDIT = new JButton(Language.getString("StarBaseInfoGUI.0")); //$NON-NLS-1$

  // data
  private short SELECTED = -1;

  /**
   * Constructor.
   * @throws IOException
   */
  public StarBaseInfoGUI(SavGame game, Stbof stbof) throws IOException {
    this.savGame = game;

    BASE = (StarBaseInfo) game.getInternalFile(CSavFiles.StarBaseInfo, true);
    RACE = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    shipList = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);

    setupComponents();
    setupLayout();
    loadData();
    setupListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Starbases;
  }

  @Override
  public boolean hasChanges() {
    return BASE.madeChanges();
  }

  @Override
  public void reload() throws IOException {
    BASE = (StarBaseInfo) savGame.getInternalFile(CSavFiles.StarBaseInfo, true);
    fillBase();
  }

  @Override
  public void finalWarning() throws KeyNotFoundException {
    if (SELECTED >= 0) {
      int hull = Integer.parseInt(txtHull.getText());
      short shipType = (short) ((ID) jcbSHIP_ID.getSelectedItem()).ID;
      ShipDefinition shipDef = shipList.getShipDefinition(shipType);
      int maxHull = shipDef.getHullStrength();

      if (maxHull < hull)
        hull = maxHull;
      if (hull <= 0)
        hull = 1;

      BASE.setHull(SELECTED, hull);
    }
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    savGame.checkChanged(BASE, changes);
  }

  private void setupComponents() {
    lblLABEL[0] = new JLabel(Language.getString("StarBaseInfoGUI.7")); //$NON-NLS-1$
    lblLABEL[1] = new JLabel(Language.getString("StarBaseInfoGUI.1")); //$NON-NLS-1$
    lblLABEL[2] = new JLabel(Language.getString("StarBaseInfoGUI.2")); //$NON-NLS-1$
    lblLABEL[3] = new JLabel(Language.getString("StarBaseInfoGUI.8")); //$NON-NLS-1$
    lblLABEL[4] = new JLabel(Language.getString("StarBaseInfoGUI.9")); //$NON-NLS-1$
    lblLABEL[5] = new JLabel();

    Font def = UE.SETTINGS.getDefaultFont();
    jcbBASE.setFont(def);
    btnEDIT.setFont(def);
    jcbSHIP_ID.setFont(def);
    jcbROLE.setFont(def);
    jcbRACE.setFont(def);
    lblMaxHull.setFont(def);
    txtHull.setFont(def);
    for (int i = 0; i < 6; i++) {
      lblLABEL[i].setFont(def);
    }
  }

  private void setupLayout() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    add(jcbBASE, c);
    c.gridwidth = 1;
    c.gridx = 2;
    add(btnEDIT, c);
    c.gridx = 0;
    c.gridheight = 1;
    for (int i = 0; i < 2; i++) {
      c.gridy = 1 + i;
      add(lblLABEL[i], c);
    }
    c.gridy = 3;
    add(lblLABEL[2], c);
    c.gridwidth = 3;
    c.gridy = 4;
    add(lblLABEL[3], c);
    c.gridy = 5;
    add(lblLABEL[4], c);
    c.gridy = 6;
    add(lblLABEL[5], c);
    c.gridwidth = 2;
    c.gridx = 1;
    c.gridy = 1;
    add(jcbSHIP_ID, c);
    c.gridy = 2;
    add(jcbROLE, c);
    c.gridy = 3;
    add(jcbRACE, c);
    c.gridy = 4;
    add(lblMaxHull, c);
    c.gridy = 5;
    add(txtHull, c);
  }

  private void loadData() {
    jcbROLE.removeAllItems();
    jcbROLE.addItem(Language.getString("StarBaseInfoGUI.5")); //$NON-NLS-1$
    jcbROLE.addItem(Language.getString("StarBaseInfoGUI.6")); //$NON-NLS-1$
    jcbRACE.removeAllItems();

    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      jcbRACE.addItem(new ID(RACE.getName(emp), emp));
    }

    jcbSHIP_ID.removeAllItems();
    ID[] ids = shipList.getIDs(35);
    for (ID id : ids) {
      jcbSHIP_ID.addItem(new ID(id.ID + ": " + id.toString(), id.ID)); //$NON-NLS-1$
    }

    fillBase();
    // load initial selection data
    updateOutpost();
    updateBaseType();
  }

  private void fillBase() {
    // remember selected for item removal
    int prev = SELECTED;
    // unset selected to skip save on changes
    SELECTED = -1;

    ID[] entries = BASE.listEntries();
    if (entries.length == 0) {
      jcbBASE.removeAllItems();
      jcbBASE.setEnabled(false);
      jcbROLE.setEnabled(false);
      jcbRACE.setEnabled(false);
      btnEDIT.setEnabled(false);
      jcbSHIP_ID.setEnabled(false);
    } else {
      jcbSHIP_ID.setEnabled(true);
      jcbBASE.setEnabled(true);
      jcbROLE.setEnabled(true);
      jcbRACE.setEnabled(true);
      btnEDIT.setEnabled(true);
      jcbBASE.removeAllItems();
      for (ID entry : entries) {
        jcbBASE.addItem(entry);
      }
    }

    // restore selected
    if (prev > -1 && prev < entries.length)
      jcbBASE.setSelectedIndex(prev);
  }

  private void updateOutpost() {
    ID sel = (ID) jcbBASE.getSelectedItem();
    if (sel != null) {
      try {
        SELECTED = (short) (sel != null ? sel.ID : -1);

        int lu = BASE.getRole(SELECTED);
        lu = lu < 7 ? 0 : 1;
        jcbROLE.setSelectedIndex(lu);
        jcbRACE.setSelectedIndex(BASE.getOwner(SELECTED));

        updateCoords();

        jcbSHIP_ID.setSelectedItem(new ID("", BASE.getShipID(SELECTED))); //$NON-NLS-1$
        txtHull.setText(Integer.toString(BASE.getHull(SELECTED)));
        return;
      } catch(Exception e) {
        Dialogs.displayError(e);
      }
    }

    // fallback to defaults
    jcbROLE.setSelectedIndex(-1);
    jcbRACE.setSelectedIndex(-1);
    jcbSHIP_ID.setSelectedIndex(-1);
    updateCoords();
    txtHull.setText("");
    return;
  }

  private void updateCoords() {
    String desc = SELECTED >= 0 ? FunctionTools.defaultIfThrown(
      () -> BASE.getDescription(SELECTED), STR_UNKOWN) : STR_UNKOWN;
    String msg = strCoords.replace("%1", desc); //$NON-NLS-1$
    lblLABEL[5].setText(msg);
  }

  private void updateBaseType() {
    ID sel = (ID) jcbSHIP_ID.getSelectedItem();
    if (sel == null) {
      lblMaxHull.setText("");
      return;
    }

    short shipType = (short) sel.ID;
    ShipDefinition shipDef = shipList.getShipDefinition(shipType);
    int max = shipDef.getHullStrength();
    lblMaxHull.setText(Integer.toString(max));

    // limit hull value
    int hull = DataTools.tryParseInt(txtHull.getText()).orElse(-1);
    if (hull > max || hull <= 0)
      txtHull.setText(Integer.toString(max));
  }

  private void setupListeners() {
    btnEDIT.addActionListener(new CBActionListener(evt -> {
      if (SELECTED >= 0) {
        String msg = Language.getString("StarBaseInfoGUI.3"); //$NON-NLS-1$
        String n = JOptionPane.showInputDialog(msg, BASE.getName(SELECTED));
        if (n != null) {
          BASE.setName(SELECTED, n);
          fillBase();
        }
      }
    }));

    jcbBASE.addActionListener(new CBActionListener(evt -> {
      finalWarning();
      updateOutpost();
    }));

    jcbSHIP_ID.addActionListener(new CBActionListener(evt -> {
      ID sel = (ID) jcbSHIP_ID.getSelectedItem();
      if (SELECTED >= 0 && sel != null) {
        short shipType = (short) sel.ID;
        BASE.setShipID(SELECTED, shipType);
        updateBaseType();
      }
    }));

    jcbROLE.addActionListener(new CBActionListener(evt -> {
      if (SELECTED >= 0 && jcbROLE.getSelectedIndex() >= 0)
        BASE.setRole(SELECTED, (short) (jcbROLE.getSelectedIndex() + 6));
    }));

    jcbRACE.addActionListener(new CBActionListener(evt -> {
      if (SELECTED >= 0)
        BASE.setOwner(SELECTED, (short) jcbRACE.getSelectedIndex());
    }));
  }

}
