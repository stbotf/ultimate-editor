package ue.gui.sav.map;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.data.StellEntry;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.util.data.ID;

/**
 * Used to edit stellar objects info in a saved game.
 */
public class StellInfoGUI extends MainPanel {

  // read
  private SavGame savGame;

  // edited
  private StellInfo STELLAR;

  // ui components
  private JComboBox<ID> jcbSTELL = new JComboBox<ID>();
  private JComboBox<String> jcbTYPE = new JComboBox<String>();
  private JTextField txtANI = new JTextField();
  private JComboBox<ID> jcbLINK = new JComboBox<ID>();
  private JLabel[] lblLABEL = new JLabel[3];
  private JButton btnEDIT = new JButton(Language.getString("StellInfoGUI.0")); //$NON-NLS-1$

  // data
  private int SELECTED = -1;

  /**
   * Constructor.
   * @throws IOException
   */
  public StellInfoGUI(SavGame game) throws IOException {
    this.savGame = game;

    //associate
    STELLAR = (StellInfo) game.getInternalFile(CSavFiles.StellInfo, true);

    //build
    lblLABEL[0] = new JLabel(Language.getString("StellInfoGUI.1")); //$NON-NLS-1$
    lblLABEL[1] = new JLabel(Language.getString("StellInfoGUI.2")); //$NON-NLS-1$
    lblLABEL[2] = new JLabel(Language.getString("StellInfoGUI.3")); //$NON-NLS-1$

    //set up
    Font def = UE.SETTINGS.getDefaultFont();
    jcbSTELL.setFont(def);
    btnEDIT.setFont(def);
    jcbTYPE.setFont(def);
    txtANI.setFont(def);
    jcbLINK.setFont(def);
    for (int i = 0; i < 3; i++) {
      lblLABEL[i].setFont(def);
    }

    //edit
    btnEDIT.addActionListener(new CBActionListener(evt -> {
      if (SELECTED >= 0) {
        StellEntry stellObj = STELLAR.getStellarObject(SELECTED);
        String msg = Language.getString("StellInfoGUI.4"); //$NON-NLS-1$
        String n = JOptionPane.showInputDialog(msg, stellObj.getName());
        if (n != null) {
          stellObj.setName(n);
          fillObjects();
        }
      }
    }));

    //combos
    jcbSTELL.addActionListener(new CBActionListener(evt -> {
      finalWarning();
      SELECTED = jcbSTELL.getSelectedIndex();

      if (SELECTED >= 0) {
        StellEntry stellObj = STELLAR.getStellarObject(SELECTED);
        jcbTYPE.setSelectedIndex(stellObj.getType());
        txtANI.setText(stellObj.getAnimation());

        short al = stellObj.getLink();
        if (al == -1) {
          jcbLINK.setSelectedIndex(jcbLINK.getItemCount() - 1);
        } else {
          jcbLINK.setSelectedIndex(al);
        }
      }
    }));

    jcbTYPE.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1 && jcbTYPE.getSelectedIndex() > -1) {
        StellEntry stellObj = STELLAR.getStellarObject(SELECTED);
        stellObj.setType(jcbTYPE.getSelectedIndex());
        jcbTYPE.setSelectedIndex(stellObj.getType());
      }
    }));

    jcbLINK.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1 && jcbLINK.getSelectedIndex() > -1) {
        StellEntry stellObj = STELLAR.getStellarObject(SELECTED);
        int linkId = jcbLINK.getSelectedIndex() < jcbLINK.getItemCount() - 1
          ? jcbLINK.getSelectedIndex() : -1;
        stellObj.setLink((short) linkId);
      }
    }));

    //layout
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    //up
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 2;
    add(jcbSTELL, c);
    c.gridwidth = 1;
    c.gridx = 2;
    add(btnEDIT, c);
    c.gridx = 0;
    c.gridheight = 1;
    for (int i = 0; i < 3; i++) {
      c.gridy = 1 + i;
      add(lblLABEL[i], c);
    }
    c.gridwidth = 2;
    c.gridx = 1;
    c.gridy = 1;
    add(jcbTYPE, c);
    c.gridy = 2;
    add(txtANI, c);
    c.gridy = 3;
    add(jcbLINK, c);
    //fill combo
    //type
    jcbTYPE.removeAllItems();
    jcbTYPE.addItem(Language.getString("StellInfoGUI.5")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.6")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.7")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.8")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.9")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.10")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.11")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.12")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.13")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.14")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.15")); //$NON-NLS-1$
    jcbTYPE.addItem(Language.getString("StellInfoGUI.16")); //$NON-NLS-1$
    fillObjects();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.StellInfo;
  }

  //fill objects
  private void fillObjects() {
    // remember selected for item removal
    int prev = jcbSTELL.getSelectedIndex();
    // unset selected to skip save on changes
    SELECTED = -1;

    String[] n = STELLAR.getNames();
    jcbSTELL.removeAllItems();
    jcbLINK.removeAllItems();
    //link first
    for (int i = 0; i < n.length; i++) {
      jcbLINK.addItem(new ID(n[i], i));
    }
    jcbLINK.addItem(
        new ID(Language.getString("StellInfoGUI.17"), jcbLINK.getItemCount())); //$NON-NLS-1$
    for (int i = 0; i < n.length; i++) {
      jcbSTELL.addItem(new ID(n[i], i));
    }

    // restore selected
    if (prev > -1 && prev < n.length)
      jcbSTELL.setSelectedIndex(prev);
  }

  @Override
  public boolean hasChanges() {
    return STELLAR.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    savGame.checkChanged(STELLAR, changes);
  }

  @Override
  public void reload() throws IOException {
    STELLAR = (StellInfo) savGame.getInternalFile(CSavFiles.StellInfo, true);
    fillObjects();
  }

  //final warning
  @Override
  public void finalWarning() {
    if (SELECTED >= 0) {
      STELLAR.getStellarObject(SELECTED).setAnimation(txtANI.getText());
    }
  }
}
