package ue.gui.sav.map;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import ue.UE;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.common.CSavGame.ControlFlags;
import ue.edit.sav.tools.TaskForceTools;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;

public class MapCleanupDialog extends JPanel {

  private final String title = "Cleanup the galactic map";
  private JLabel lblClean = new JLabel("Select elements to remove:");
  // stellar objects & systems
  private JCheckBox chkStellar = new JCheckBox();
  private JCheckBox chkUninhabited = new JCheckBox();
  private JCheckBox chkMinorSystems = new JCheckBox();
  private JCheckBox chkProvinces = new JCheckBox();
  private JCheckBox chkHomeSystems = new JCheckBox();
  // tasks & events
  private JCheckBox chkAITasks = new JCheckBox();
  private JCheckBox chkShpAITasks = new JCheckBox();
  private JCheckBox chkSysAITasks = new JCheckBox();
  private JCheckBox chkDiploAITasks = new JCheckBox();
  private JCheckBox chkAISubTasks = new JCheckBox();
  private JCheckBox chkOrders = new JCheckBox();
  private JCheckBox chkResults = new JCheckBox();
  // systems
  private JCheckBox chkMainStructures = new JCheckBox();
  private JCheckBox chkEnergyStructures = new JCheckBox();
  private JCheckBox chkTradeRoutes = new JCheckBox();
  // ships
  private JCheckBox chkStarbases = new JCheckBox();
  private JCheckBox chkAllShips = new JCheckBox();
  private JCheckBox chkPlayerShips = new JCheckBox();
  private JCheckBox chkAIEmpireShips = new JCheckBox();
  private JCheckBox chkMinorShips = new JCheckBox();
  private JCheckBox chkMonsterShips = new JCheckBox();
  private JCheckBox chkMonsters = new JCheckBox();

  private MapGUI gui;
  private SavGameInterface sgif;

  public MapCleanupDialog(MapGUI gui, SavGameInterface sgif) {
    this.gui = gui;
    this.sgif = sgif;
    setupComponents();
    setupLayout();
    setupListeners();
  }

  public static void show(MapGUI gui, SavGameInterface sgif) {
    MapCleanupDialog pnlClean = new MapCleanupDialog(gui, sgif);
    pnlClean.show();
  }

  public void show() {
    int response = JOptionPane.showConfirmDialog(UE.WINDOW, this, title, JOptionPane.OK_CANCEL_OPTION);
    if (response != JOptionPane.OK_OPTION)
      return;

    MapPanel map = gui.map();
    boolean refreshMap = cleanupMap();

    if (refreshMap) {
      map.refreshMap(MapDrawOptions.FULL);
      if (map.selection1 != null) {
        gui.updateMilitaryList();
        gui.updateSystemDetails();
      }
    }
  }

  private void setupComponents() {
    lblClean.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    Border indentBorder = BorderFactory.createEmptyBorder(5, 20, 5, 5);
    chkStellar.setText("all stellar objects");
    chkUninhabited.setText("all uninhabited systems");
    chkMinorSystems.setText("all minor race systems");
    chkProvinces.setText("all empire provinces");
    chkHomeSystems.setText("all empire home systems");

    chkAITasks.setText("all AI tasks");
    chkShpAITasks.setText("all ship AI tasks");
    chkShpAITasks.setBorder(indentBorder);
    chkSysAITasks.setText("all system AI tasks");
    chkSysAITasks.setBorder(indentBorder);
    chkDiploAITasks.setText("all diplomatic AI tasks");
    chkDiploAITasks.setBorder(indentBorder);
    chkAISubTasks.setText("all TskSh & TskSy files");
    chkAISubTasks.setBorder(indentBorder);

    chkOrders.setText("all current orders");
    chkResults.setText("all turn results");
    chkMainStructures.setText("all main structures");
    chkEnergyStructures.setText("all energy structures");
    chkTradeRoutes.setText("all trade routes");

    chkStarbases.setText("all star bases");
    chkAllShips.setText("all ships (but one)");
    chkAllShips.setToolTipText("One single task force and ship must be kept to not crash the game.");
    chkPlayerShips.setText("all player ships");
    chkPlayerShips.setBorder(indentBorder);
    chkAIEmpireShips.setText("all ai empire ships");
    chkAIEmpireShips.setBorder(indentBorder);
    chkMinorShips.setText("all minor ships");
    chkMinorShips.setBorder(indentBorder);
    chkMonsterShips.setText("all monster ships");
    chkMonsterShips.setBorder(indentBorder);

    chkMonsters.setText("all monster agents");
  }

  private void setupLayout() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.WEST;
    c.gridx = 0;
    c.gridy = 0;

    // cleanup description
    c.gridwidth = 2;
    add(lblClean, c);
    c.gridwidth = 1;

    // checkbox row 1
    c.gridy++;
    add(chkStellar, c);
    c.gridy++;
    add(chkUninhabited, c);
    c.gridy++;
    add(chkMinorSystems, c);
    c.gridy++;
    add(chkProvinces, c);
    c.gridy++;
    add(chkHomeSystems, c);
    c.gridy++;
    add(chkMainStructures, c);
    c.gridy++;
    add(chkEnergyStructures, c);
    c.gridy++;
    add(chkTradeRoutes, c);
    c.gridy++;
    add(chkStarbases, c);
    c.gridy++;
    add(chkOrders, c);
    c.gridy++;
    add(chkResults, c);

    // checkbox row 2
    c.insets.left = 10;
    c.gridx++;
    c.gridy = 1;
    add(chkAITasks, c);
    c.gridy++;
    add(chkShpAITasks, c);
    c.gridy++;
    add(chkSysAITasks, c);
    c.gridy++;
    add(chkDiploAITasks, c);
    c.gridy++;
    add(chkAISubTasks, c);
    c.gridy++;
    add(chkAllShips, c);
    c.gridy++;
    add(chkPlayerShips, c);
    c.gridy++;
    add(chkAIEmpireShips, c);
    c.gridy++;
    add(chkMinorShips, c);
    c.gridy++;
    add(chkMonsterShips, c);
    c.gridy++;
    add(chkMonsters, c);
  }

  private void setupListeners() {
    chkAllShips.addActionListener(new CBActionListener(evt -> {
      boolean checked = chkAllShips.isSelected();
      chkPlayerShips.setSelected(checked);
      chkAIEmpireShips.setSelected(checked);
      chkMinorShips.setSelected(checked);
      chkMonsterShips.setSelected(checked);
    }));

    chkAITasks.addActionListener(new CBActionListener(evt -> {
      boolean checked = chkAITasks.isSelected();
      chkShpAITasks.setSelected(checked);
      chkSysAITasks.setSelected(checked);
      chkDiploAITasks.setSelected(checked);
      chkAISubTasks.setSelected(checked);
    }));
  }

  private boolean cleanupMap() {
    boolean refreshMap = false;
    MapPanel map = gui.map();

    try {
      // stellar objects & systems
      if (chkStellar.isSelected()) {
        sgif.stellarTools().removeAllStellarObjects();
        refreshMap = true;
      }
      if (chkUninhabited.isSelected()) {
        if (!sgif.systemTools().removeAllUninhabitedSystems())
          return true;
        refreshMap = true;
      }
      if (chkMinorSystems.isSelected()) {
        if (!sgif.systemTools().removeAllMinorRaceSystems())
          return true;
        refreshMap = true;
      }
      if (chkProvinces.isSelected()) {
        if (!sgif.systemTools().removeAllEmpireProvinces())
          return true;
        refreshMap = true;
      }
      if (chkHomeSystems.isSelected()) {
        if (!sgif.systemTools().removeAllEmpireHomeSystems())
          return true;
        refreshMap = true;
      }

      // AI tasks
      if (chkAITasks.isSelected()) {
        sgif.aiTaskTools().removeAllTasks();
      }
      else {
        if (chkShpAITasks.isSelected())
          sgif.aiTaskTools().removeAllShipTasks();
        if (chkSysAITasks.isSelected())
          sgif.aiTaskTools().removeAllSystemTasks();
        if (chkDiploAITasks.isSelected())
          sgif.aiTaskTools().removeAllDiplomaticTasks();
        if (chkAISubTasks.isSelected())
          sgif.aiTaskTools().resetSubTasks();
      }

      // orders & results
      if (chkOrders.isSelected())
        sgif.ordInfo().clear();
      if (chkResults.isSelected())
        sgif.resultList().clear();

      // system related
      if (chkMainStructures.isSelected())
        map.strcInfo.removeAllMainStructures();
      if (chkEnergyStructures.isSelected())
        map.strcInfo.removeAllEnergyStructures();
      if (chkTradeRoutes.isSelected())
        sgif.systemTools().cancelAllTradeRoutes();

      // starbases
      if (chkStarbases.isSelected()) {
        sgif.starbaseTools().removeAllStarbases();
        refreshMap = true;
      }

      // ships
      int controlFlags = 0;
      if (chkPlayerShips.isSelected())
        controlFlags |= ControlFlags.Player;
      if (chkAIEmpireShips.isSelected())
        controlFlags |= ControlFlags.AIEmpire;
      if (chkMinorShips.isSelected())
        controlFlags |= ControlFlags.Minor;
      if (chkMonsterShips.isSelected())
        controlFlags |= ControlFlags.Monster;

      if (controlFlags != 0) {
        TaskForceTools tfTools = sgif.taskForceTools();
        tfTools.removeAllTaskForces(controlFlags);

        // to not crash the game, add back one player task force
        if (map.gWTForce.getEntryCount() == 0) {
          try {
            tfTools.addPlayerTaskForce(ShipRole.Destroyer, 1);
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
        refreshMap = true;
      }

      if (chkMonsters.isSelected())
        sgif.aiAgentMgr().removeMonsters();
    }
    catch (Exception e) {
      Dialogs.displayError(e);
    }

    return refreshMap;
  }

}
