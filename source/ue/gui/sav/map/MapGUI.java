

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MapGUIv2.java
 *
 * Created on 23.7.2010, 13:04:15
 */
package ue.gui.sav.map;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.commons.text.WordUtils;
import org.kordamp.ikonli.materialdesign.MaterialDesign;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.seg.map.PlanetBonusTypes;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.StructureType;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.dic.idx.LexMenuIdx;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.data.Ship;
import ue.edit.sav.files.map.data.StellEntry;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.edit.sav.files.map.data.TaskForce;
import ue.edit.sav.tools.SystemGen.SysGenOption;
import ue.gui.common.FileChanges;
import ue.gui.common.InfoList;
import ue.gui.common.InfoPanel.Orientation;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.sav.map.menu.MapGUIOutpostMenu;
import ue.gui.sav.map.menu.MapGUIPlanetMenu;
import ue.gui.sav.map.menu.MapGUISectorMenu;
import ue.gui.sav.map.menu.MapGUIShipMenu;
import ue.gui.sav.map.menu.MapGUIStellarMenu;
import ue.gui.sav.map.menu.MapGUISystemMenu;
import ue.gui.sav.map.menu.MapGUITaskForceMenu;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.util.component.IconButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.gui.util.event.SelectionEvent;
import ue.gui.util.event.SelectionListener;
import ue.gui.util.gfx.Icons;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.data.StringTools;
import ue.util.func.FunctionTools;

/**
 * @author Alan
 */
public class MapGUI extends MainPanel {

  // map objects
  private interface MapObject {
    static final int outpost = 0;
    static final int taskforce = 1;
    static final int stellar_object = 2;
    static final int system = 3;
    static final int planet = 4;
    static final int ship = 5;
  }

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private static final Color SELECTION_COLOR = Color.getHSBColor(0.04f, 1f, 0.25f);
  private static final int detailsImgSize = 50;
  private static final ImageIcon icoMilitaryLst = new ImageIcon(new BufferedImage(30, 30, BufferedImage.TYPE_INT_ARGB));
  private static final ImageIcon icoDetailsLst = new ImageIcon(new BufferedImage(detailsImgSize, detailsImgSize, BufferedImage.TYPE_INT_ARGB));

  // ui components
  /* LEFT SHIP LIST */
  private JPanel pnlLeft = new JPanel();
  private InfoList lstMilitary = new InfoList(icoMilitaryLst, false, 0, 1);
  private JPanel pnlMilitary = new JPanel(new BorderLayout());
  private JScrollPane jspMilitary = new JScrollPane(pnlMilitary);
  /* CENTER MAP PANEL */
  private JPanel pnlCenter = new JPanel();
  // map race selection
  private JPanel pnlView = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
  // map
  private JPanel pnlCenterView = new JPanel();
  @Getter @Accessors(fluent = true)
  private MapPanel map;
  private JScrollPane scrMap = new JScrollPane();
  // view options
  private JPanel pnlCenterOptions = new JPanel(new BorderLayout(0, 0));
  private JPanel pnlViewOptions = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
  private JButton btnZoomIn = new JButton();
  private JButton btnZoomOut = new JButton();
  private JPanel pnlSettings = new JPanel();
  private JCheckBox chkObjects = new JCheckBox();
  private JCheckBox chkOutposts = new JCheckBox();
  private JCheckBox chkSectorOwnership = new JCheckBox();
  private JCheckBox chkShips = new JCheckBox();
  private JCheckBox chkShipPaths = new JCheckBox();
  private JCheckBox chkSysLabels = new JCheckBox();
  private JCheckBox chkStellLabels = new JCheckBox();
  private JSpinner spiShipCount = new JSpinner(new SpinnerNumberModel(1, 1, Short.MAX_VALUE, 1));
  private IconButton btnCleanup = IconButton.CreateMediumButton(Icons.DELETE);
  /* BOTTOM SECTOR DETAILS */
  private JPanel pnlBottom = new JPanel();
  private InfoList lstDetails = new InfoList(icoDetailsLst, false, 1, 0);
  private JPanel pnlDetailItems = new JPanel(new BorderLayout());
  private JScrollPane jspDetails = new JScrollPane(pnlDetailItems);
  private JEditorPane txtDetailsDesc;
  private JPanel pnlDetails = new JPanel();
  private JScrollPane jspDetailsText = new JScrollPane(pnlDetails);
  private IconButton btnAddPlanet = new IconButton(MaterialDesign.MDI_PLUS_CIRCLE_OUTLINE, 20, Color.YELLOW, Color.CYAN);

  // data
  private Point prev_sector = null;
  private SystemEntry selectedSystem = null;
  private Font font;

  private SavGame sav;
  private Stbof stbof;
  private Trek trek;
  private SavGameInterface sgif;
  private StbofFileInterface stif;

  public MapGUI(SavGame sav, Stbof stbof, Trek trek) throws IOException {
    this(sav, stbof, trek, -1);
  }

  public MapGUI(SavGame sav, Stbof stbof, Trek trek, int sectorIndex) throws IOException {
    this.sav = sav;
    this.stbof = stbof;
    this.trek = trek;
    sgif = sav.files();
    stif = stbof.files();

    map = new MapPanel(sav, stbof, trek, sectorIndex);
    scrMap.setViewportView(map);
    font = UE.SETTINGS.getDefaultFont();

    initComponents();
    setupComponents();

    // update auto-selected sector details
    if (sectorIndex != -1)
      updateSystemDetails();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.MapV2;
  }

  public void updateSystemDetails() {
    updateDetailsList(map.selection1, (short)-1, (short)-1);
  }

  public void updateMilitaryList() {
    updateMilitaryList(map.selection1);
  }

  @Override
  public boolean hasChanges() {
    return map.hasChanges();
  }

  @Override
  public void reload() throws IOException {
    map.reload();
  }

  @Override
  public void finalWarning() {
    // nothing to do
  }

  private void setupComponents() {
    RightClickSelectionListener rightClickListener = new RightClickSelectionListener();

    setupHeaderPanel();
    setupLeftPanel(rightClickListener);
    setupCenterPanel(rightClickListener);
    setupBottomPanel(rightClickListener);
  }

  private void setupHeaderPanel() {
    // setup race view selections
    ButtonGroup grp = new ButtonGroup();
    JToggleButton[] btnView = new JToggleButton[6];

    btnView[0] = new JToggleButton(Language.getString("MapGUI.0")); //$NON-NLS-1$
    btnView[0].setFont(font);
    btnView[0].addActionListener(new ViewListener(-1));
    grp.add(btnView[0]);
    pnlView.add(btnView[0]);

    for (int r = 0; r < NUM_EMPIRES; r++) {
      btnView[r + 1] = new JToggleButton(map.races.getName(r));
      btnView[r + 1].setFont(font);
      btnView[r + 1].addActionListener(new ViewListener(r));
      grp.add(btnView[r + 1]);
      pnlView.add(btnView[r + 1]);
    }
  }

  private void setupLeftPanel(RightClickSelectionListener rightClickListener) {
    pnlMilitary.setBackground(Color.BLACK);
    lstMilitary.setFont(font);
    lstMilitary.setBackground(Color.BLACK);
    lstMilitary.setForeground(Color.WHITE);
    lstMilitary.setSelectionBackground(SELECTION_COLOR);
    lstMilitary.addListSelectionListener(new MilitarySelectionListener());
    pnlMilitary.add(lstMilitary, BorderLayout.NORTH);

    JScrollBar jsp = jspMilitary.getVerticalScrollBar();
    jsp.setUnitIncrement(30);
    jspMilitary.setPreferredSize(new Dimension(200, 250));
    pnlLeft.add(jspMilitary);
    pnlLeft.setLayout(new BoxLayout(pnlLeft, BoxLayout.Y_AXIS));

    lstMilitary.addRightClickListener(rightClickListener);
  }

  private void setupCenterPanel(RightClickSelectionListener rightClickListener) {
    int scr = Math.max(1, map.map[0][0].getPreferredSize().height / 2);
    scrMap.getVerticalScrollBar().setUnitIncrement(scr);
    scrMap.getVerticalScrollBar().setBlockIncrement(scr);
    scrMap.setPreferredSize(new Dimension(500, 380));

    map.addActionListener(new SectorSelectionListener());
    map.addRightClickListener(rightClickListener);
  }

  private void setupBottomPanel(RightClickSelectionListener rightClickListener) {
    JPanel pnlDetailedView = new JPanel(new BorderLayout());
    pnlDetailItems.setOpaque(false);
    pnlDetailItems.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

    lstDetails.setItemOrientation(Orientation.Vertical);
    lstDetails.setFont(font);
    // set all contained component backgrounds
    lstDetails.setBackground(Color.BLACK);
    lstDetails.setForeground(Color.WHITE);
    lstDetails.setSelectionBackground(SELECTION_COLOR);
    lstDetails.setSelectionActivator(InfoList.MOUSE_MOVEMENT);
    lstDetails.addListSelectionListener(new DetailsSelectionListener());

    JScrollBar jsp = jspDetails.getHorizontalScrollBar();
    jsp.setUnitIncrement(50);
    jspDetails.getViewport().setBackground(Color.BLACK);
    jspDetails.setPreferredSize(new Dimension(100, 100));
    jspDetails.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
    jspDetails.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    UIDefaults defaults = UIManager.getDefaults();
    int width = defaults.getInt("InternalFrame.borderWidth");
    if (width <= 0)
      width = 1;

    txtDetailsDesc = new JEditorPane("text/html", "");
    txtDetailsDesc.setBorder(BorderFactory.createEmptyBorder(0, 2, 2, 2));
    txtDetailsDesc.setOpaque(false);
    txtDetailsDesc.setEditable(false);
    txtDetailsDesc.setFocusable(false);
    txtDetailsDesc.setFont(font);

    btnAddPlanet.addActionListener(new CBActionListener(evt -> {
      sgif.systemGen().genPlanet(selectedSystem, SysGenOption.Random);
      updateSystemDetails();
    }));

    pnlDetails.setLayout(new BoxLayout(pnlDetails, BoxLayout.Y_AXIS));
    pnlDetails.add(txtDetailsDesc);
    pnlDetails.setPreferredSize(new Dimension(200, 140));

    // making it not opaque for some reason doesn't work when added to a JScrollPane
    pnlDetails.setBackground(Color.BLACK);

    jsp = jspDetailsText.getHorizontalScrollBar();
    jsp.setUnitIncrement(50);

    Color color = defaults.getColor("InternalFrame.borderColor");
    if (color == null)
      color = getBackground();

    jspDetails.setBorder(BorderFactory.createMatteBorder(
        0, 0, width, width, color));
    jspDetailsText.setBorder(BorderFactory.createMatteBorder(
        0, width, width, 0, color));
    jspDetailsText.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    jspDetailsText.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

    pnlDetailedView.add(jspDetails, BorderLayout.CENTER);
    pnlDetailedView.add(jspDetailsText, BorderLayout.WEST);

    pnlBottom.setLayout(new BorderLayout());
    pnlBottom.add(pnlDetailedView, BorderLayout.CENTER);
    pnlBottom.setBorder(BorderFactory.createEmptyBorder(0, 5, 2, 5));

    lstDetails.addRightClickListener(rightClickListener);
  }

  private void initComponents() {
    setLayout(new BorderLayout());

    // left military panel
    pnlLeft.setBorder(BorderFactory
        .createTitledBorder(BorderFactory.createEmptyBorder(20, 2, 1, 1), "Military:"));
    add(pnlLeft, BorderLayout.LINE_START);

    // map panel
    pnlCenterView.setLayout(new BoxLayout(pnlCenterView, BoxLayout.LINE_AXIS));
    scrMap.setDoubleBuffered(true);
    scrMap.setFocusable(false);
    pnlCenterView.setBorder(new EmptyBorder(0, 0, 0, 5));
    pnlCenterView.add(scrMap);

    // map panel options
    btnZoomIn.setText("+");
    btnZoomIn.addActionListener(new CBActionListener(MapGUI.this::btnZoomInActionPerformed));
    pnlViewOptions.add(btnZoomIn);
    pnlViewOptions.add(Box.createHorizontalStrut(3));

    btnZoomOut.setText("-");
    btnZoomOut.addActionListener(new CBActionListener(MapGUI.this::btnZoomOutActionPerformed));
    pnlViewOptions.add(btnZoomOut);
    pnlViewOptions.add(Box.createHorizontalStrut(5));

    // draw options
    JPanel drawOptions = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

    chkObjects.setSelected(map.drawOptions.drawObjects);
    chkObjects.setText("sys");
    chkObjects.setToolTipText("draw stars & stellar objects");
    chkObjects.setFocusable(false);
    chkObjects.setBorder(null);
    chkObjects.setPreferredSize(new Dimension(42, 14));
    chkObjects.addActionListener(new CBActionListener(MapGUI.this::chkObjectsActionPerformed));
    drawOptions.add(chkObjects);

    chkOutposts.setSelected(map.drawOptions.drawOutposts);
    chkOutposts.setText("op");
    chkOutposts.setToolTipText("draw outposts");
    chkOutposts.setFocusable(false);
    chkOutposts.setBorder(null);
    chkOutposts.setPreferredSize(new Dimension(42, 14));
    chkOutposts.addActionListener(new CBActionListener(MapGUI.this::chkOutpostsActionPerformed));
    drawOptions.add(chkOutposts);

    chkShips.setSelected(map.drawOptions.drawShips);
    chkShips.setText("tf");
    chkShips.setToolTipText("draw task forces");
    chkShips.setFocusable(false);
    chkShips.setBorder(null);
    chkShips.setPreferredSize(new Dimension(42, 14));
    chkShips.addActionListener(new CBActionListener(MapGUI.this::chkShipsActionPerformed));
    drawOptions.add(chkShips);

    chkShipPaths.setSelected(map.drawOptions.drawPaths);
    chkShipPaths.setText("mov");
    chkShipPaths.setToolTipText("draw movement paths");
    chkShipPaths.setBorder(null);
    chkShipPaths.setPreferredSize(new Dimension(42, 14));
    chkShipPaths.addActionListener(new CBActionListener(MapGUI.this::chkShipPathsActionPerformed));
    drawOptions.add(chkShipPaths);

    chkSectorOwnership.setSelected(map.drawOptions.drawOwnership);
    chkSectorOwnership.setText("own");
    chkSectorOwnership.setToolTipText("draw sector ownership");
    chkSectorOwnership.setFocusable(false);
    chkSectorOwnership.setBorder(null);
    chkSectorOwnership.setPreferredSize(new Dimension(42, 14));
    chkSectorOwnership.addActionListener(new CBActionListener(MapGUI.this::chkSectorOwnershipActionPerformed));
    drawOptions.add(chkSectorOwnership);

    chkSysLabels.setSelected(map.drawOptions.drawSystemLabels);
    chkSysLabels.setText("syn");
    chkSysLabels.setToolTipText("system labels");
    chkSysLabels.setBorder(null);
    chkSysLabels.setPreferredSize(new Dimension(42, 14));
    chkSysLabels.addActionListener(new CBActionListener(MapGUI.this::chkSystemLabelsActionPerformed));
    drawOptions.add(chkSysLabels);

    chkStellLabels.setSelected(map.drawOptions.drawStellarLabels);
    chkStellLabels.setText("stn");
    chkStellLabels.setToolTipText("stellar labels");
    chkStellLabels.setBorder(null);
    chkStellLabels.setPreferredSize(new Dimension(42, 14));
    chkStellLabels.addActionListener(new CBActionListener(MapGUI.this::chkStellarLabelsActionPerformed));
    drawOptions.add(chkStellLabels);

    drawOptions.setPreferredSize(new Dimension(168, 30));
    pnlViewOptions.add(drawOptions);
    pnlCenterOptions.add(pnlViewOptions, BorderLayout.LINE_START);

    // settings
    JLabel shpCntLabel = new JLabel("ship count:");
    pnlSettings.add(shpCntLabel);

    spiShipCount.setFont(font);
    spiShipCount.addChangeListener(new CBChangeListener(
      evt -> map.setShipCount((Integer) spiShipCount.getValue())
    ));
    pnlSettings.add(spiShipCount);

    // icon only buttons
    btnCleanup.addActionListener(new CBActionListener(x -> MapCleanupDialog.show(this, sgif)));
    pnlSettings.add(btnCleanup);

    pnlCenterOptions.add(pnlSettings, BorderLayout.LINE_END);
    pnlCenterOptions.setBorder(new EmptyBorder(0, 0, 0, 1));

    // combined center panel
    pnlCenter.setLayout(new BorderLayout(0, 0));
    pnlCenter.add(pnlView, BorderLayout.PAGE_START);
    pnlCenter.add(pnlCenterView, BorderLayout.CENTER);
    pnlCenter.add(pnlCenterOptions, BorderLayout.PAGE_END);

    add(pnlCenter, BorderLayout.CENTER);

    // bottom panel
    add(pnlBottom, BorderLayout.PAGE_END);
  }

  private void btnZoomInActionPerformed(ActionEvent evt) {
    final Dimension dim = new Dimension(scrMap.getVerticalScrollBar().getMaximum(),
        scrMap.getHorizontalScrollBar().getMaximum());

    map.zoomIn();

    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        int window = scrMap.getHeight();

        Dimension dim2 = new Dimension(scrMap.getVerticalScrollBar().getMaximum(),
            scrMap.getHorizontalScrollBar().getMaximum());

        dim.height =
            window - (int) Math.round((dim.height / (double) dim2.height) * window) + scrMap
                .getVerticalScrollBar().getValue();

        window = scrMap.getWidth();

        dim.width = window - (int) Math.round((dim.width / (double) dim2.width) * window) + scrMap
            .getHorizontalScrollBar().getValue();

        scrMap.getVerticalScrollBar().setValue(dim.height);
        scrMap.getHorizontalScrollBar().setValue(dim.width);

        int scr = Math.max(1, map.map[0][0].getPreferredSize().height / 2);
        scrMap.getVerticalScrollBar().setUnitIncrement(scr);
        scrMap.getVerticalScrollBar().setBlockIncrement(scr);
      }
    });
  }

  private void btnZoomOutActionPerformed(ActionEvent evt) {
    val oldDim = map.getPreferredSize();
    val vBar = scrMap.getVerticalScrollBar();
    val hBar = scrMap.getHorizontalScrollBar();
    int vPos = vBar.getValue();
    int hPos = hBar.getValue();

    map.zoomOut();
    val newDim = map.getPreferredSize();

    int vZoom = (oldDim.height - newDim.height) / 2;
    int vScroll = Integer.max(0, vPos - vZoom);
    vBar.setValue(vScroll);

    int hZoom = (oldDim.width - newDim.width) / 2;
    int hScroll = Integer.max(0, hPos - hZoom);
    hBar.setValue(hScroll);

    int scr = Math.max(1, 2 * newDim.height / map.sectors.getHSectors());
    vBar.setUnitIncrement(scr);
  }

  private void chkSectorOwnershipActionPerformed(ActionEvent evt) {
    map.drawOptions.drawOwnership = chkSectorOwnership.isSelected();
    UE.SETTINGS.setProperty(SettingsManager.MAP_HIDE_OWNERSHIP, !map.drawOptions.drawOwnership);
    map.setVisible(false);
    map.setVisible(true);
  }

  private void chkObjectsActionPerformed(ActionEvent evt) {
    map.drawOptions.drawObjects = chkObjects.isSelected();
    UE.SETTINGS.setProperty(SettingsManager.MAP_HIDE_OBJECTS, !map.drawOptions.drawObjects);
    map.setVisible(false);
    map.setVisible(true);
  }

  private void chkOutpostsActionPerformed(ActionEvent evt) {
    map.drawOptions.drawOutposts = chkOutposts.isSelected();
    UE.SETTINGS.setProperty(SettingsManager.MAP_HIDE_OUTPOSTS, !map.drawOptions.drawOutposts);
    map.setVisible(false);
    map.setVisible(true);
  }

  private void chkShipsActionPerformed(ActionEvent evt) {
    map.drawOptions.drawShips = chkShips.isSelected();
    UE.SETTINGS.setProperty(SettingsManager.MAP_HIDE_SHIPS, !map.drawOptions.drawShips);
    map.setVisible(false);
    map.setVisible(true);
  }

  private void chkShipPathsActionPerformed(ActionEvent evt) {
    map.drawOptions.drawPaths = chkShipPaths.isSelected();
    UE.SETTINGS.setProperty(SettingsManager.MAP_HIDE_PATHS, !map.drawOptions.drawPaths);
    map.setVisible(false);
    map.setVisible(true);
  }

  private void chkSystemLabelsActionPerformed(ActionEvent evt) {
    map.drawOptions.drawSystemLabels = chkSysLabels.isSelected();
    UE.SETTINGS.setProperty(SettingsManager.MAP_HIDE_SYSTEM_LABELS, !map.drawOptions.drawSystemLabels);
    map.refreshMap(MapDrawOptions.SYSTEM_LABELS);
  }

  private void chkStellarLabelsActionPerformed(ActionEvent evt) {
    map.drawOptions.drawStellarLabels = chkStellLabels.isSelected();
    UE.SETTINGS.setProperty(SettingsManager.MAP_HIDE_STELLAR_LABELS, !map.drawOptions.drawStellarLabels);
    map.refreshMap(MapDrawOptions.STELLAR_LABELS);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    map.listChangedFiles(changes);
  }

  private String getPlanetTypeString(int type) {
    return map.environ != null
      ? StringTools.toUpperFirstChar(map.environ.getName(type))
      : LexHelper.mapPlanetType(type);
  }

  private String getAtmoTypeString(int type) {
    return map.environ != null
      ? StringTools.toUpperFirstChar(map.environ.getName(type))
      : LexHelper.mapPlanetAtmosphere(type);
  }

  private synchronized void updateMilitaryList(Point sector) {
    final int[] selected = lstMilitary.getSelectedItemID();

    lstMilitary.removeAllItems();

    if (sector != null) {
      int h = map.sectors.getHSectors();
      int index = sector.y * h + sector.x;

      short sbIndex = map.sectors.getStarbase(index);

      // update military list
      // base
      if (sbIndex >= 0) {
        int[] id = new int[]{MapObject.outpost, sbIndex};
        short shipRole = FunctionTools.defaultIfThrown(() -> map.outposts.getRole(sbIndex), (short)-1);
        short owner = FunctionTools.defaultIfThrown(() -> map.outposts.getOwner(sbIndex), (short)-1);
        String strShipRole = LexHelper.mapShipRole(shipRole);
        String raceName = map.races.getName(owner, true);
        String line2 = raceName + " " + strShipRole;

        try {
          int shipID = map.outposts.getShipID(sbIndex);
          ShipDefinition shipDef = map.shipModels.getShipDefinition(shipID);
          String pic = shipDef.getGraphics();

          AniLabel icon = null;
          if (map.res.hasInternalFile("i_" + pic + "30.tga")) {
            // military list outpost & starbase icons
            TargaImage tga = map.res.getTargaImage("i_" + pic + "30.tga", LoadFlags.CACHE);
            icon = new AniLabel(new ImageIcon(tga.getImage(AlphaMode.FirstPixelAlpha)));
            icon.setForceFirstPixelAlpha(true);
          }

          double cost = map.outposts.getProductionCost(sbIndex);
          double invested = map.outposts.getProductionInvested(sbIndex);

          String strCondition;
          if (cost <= invested) {
            strCondition = map.lexicon.getEntry(LexDataIdx.SpaceCraft.Condition.OPERATIONAL).toUpperCase();
          } else {
            int percent = (int) Math.floor(invested / cost * 100);
            strCondition = map.lexicon.getEntry(LexMenuIdx.Other.X_COMPLETE).toUpperCase()
                .replace("%D", Integer.toString(percent))
                .replace("%%", "%");
          }

          lstMilitary.addItem(icon, map.outposts.getName(sbIndex), line2, strCondition, id);
        } catch (Exception e) {
          lstMilitary.addItem(null, "SB " + Integer.toString(sbIndex), line2, null, id);
        }
      }

      //  task forces
      short[] taskForceIds = map.gWTForce.getSectorTaskForces(sector.x, sector.y);
      int numRaces = map.races.getNumberOfEntries();

      for (short taskForceId : taskForceIds) {
        int[] id = new int[]{MapObject.taskforce, taskForceId};
        String taskForceName = FunctionTools.defaultIfThrown(() -> map.getTaskForceTitle(taskForceId), "???");
        TaskForce gtf = FunctionTools.defaultIfThrown(() -> map.gWTForce.taskForce(taskForceId), null);
        String commandName = FunctionTools.defaultIfThrown(() -> getTaskForceCommand(gtf), "<?>");
        AniLabel lbl = null;

        try {
          int race = gtf.getOwnerId();

          // task force icons
          String icon = null;
          if (race < NUM_EMPIRES) {
            // empire task force icon
            icon = "i_" + map.empImgPrefixes.getEmpirePrefix(race) + "26.tga";
          } else if (race < numRaces) {
            // minor race task force icon
            icon = "i_m26.tga";
          } else {
            // alien task force icon
            icon = "i_t26.tga";
          }

          TargaImage tga = map.res.getTargaImage(icon, LoadFlags.CACHE);
          lbl = new AniLabel(new ImageIcon(tga.getImage(AlphaMode.FirstPixelAlpha)));
        } catch (Exception e) {
          // show default image
          e.printStackTrace();
        }

        lstMilitary.addItem(lbl, commandName, taskForceName, null, id);
      }
    }

    lstMilitary.setSelectedItemID(selected);
    lstMilitary.setVisible(false);
    lstMilitary.setVisible(true);
  }

  private String getTaskForceCommand(TaskForce gtf) {
    int commandId = gtf.getMission();
    String commandName = LexHelper.mapTFMission(commandId);

    // eta
    int distance = gtf.targetDistance();
    if (distance > 0) {
      int turns = (int) Math.max(1, distance / gtf.abilities().getMapSpeed());

      commandName += " " + map.lexicon.getEntry(LexMenuIdx.Galaxy.Fleet.MoveProgress);
      commandName += " " + map.lexicon.getEntry(LexMenuIdx.Galaxy.Summary.XTurns)
        .replace("%d", Integer.toString(turns)).toUpperCase();
    }

    return commandName;
  }

  private synchronized void updateDetailsList(Point sector, short taskforce, short outpost) {
    pnlDetailItems.removeAll();
    lstDetails.removeAllItems();
    lstDetails.setRowsAndColumnsNumber(1, 0);
    lstDetails.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

    if (sector != null) {
      int h = map.sectors.getHSectors();
      int index = sector.y * h + sector.x;
      int item = -1;

      // sector details
      try {
        item = map.sectors.getStellarObject(index);

        if (item >= 0) {
          // anomaly
          StellEntry stellObj = map.stellarObjects.getStellarObject(item);
          String name = stellObj.getName();
          String type = stif.stellarTools().mapStellarType(stellObj.getType());
          String pic = stellObj.getAnimation();
          AniLabel lbl = null;

          if (map.res.hasInternalFile(pic + ".ani")) {
            // stellar object animation
            AniOrCur ani = map.res.getAnimation(pic + ".ani", LoadFlags.CACHE);
            Dimension size = ani.getFrameDim();
            lbl = new AniLabel();
            lbl.setForceFirstPixelAlpha(true);

            if (size.height > detailsImgSize) {
              // accept larger width
              double ratio = detailsImgSize / (double) size.height;
              int scaledWidth = (int) (ratio * size.width);
              int scale = Integer.max(scaledWidth, detailsImgSize);
              lbl.setMaxFrameSize(scale);
            }

            lbl.setAnimation(ani, true);
          } else if (map.res.hasInternalFile(pic + ".tga")) {
            // nebula image
            TargaImage tga = map.res.getTargaImage(pic + ".tga", LoadFlags.CACHE);
            Dimension size = tga.getImageSize();
            Image img;

            if (size.height > detailsImgSize) {
              // accept larger width
              double ratio = detailsImgSize / (double) size.height;
              int scaledWidth = (int) (ratio * size.width);
              int scale = Integer.max(scaledWidth, detailsImgSize);
              img = tga.getThumbnail(scale, false, AlphaMode.FirstPixelAlpha);
            } else {
              img = tga.getImage(AlphaMode.FirstPixelAlpha);
            }

            lbl = new AniLabel(new ImageIcon(img));
          }

          lstDetails.addItem(lbl, name, type, null,
            new int[]{MapObject.stellar_object, item});

          pnlDetailItems.add(lstDetails, BorderLayout.CENTER);
        } else {
          item = map.sectors.getSystem(index);

          if (item >= 0) {
            val system = map.systems.getSystem(item);

            // update system reference
            selectedSystem = system;

            // star
            StarPanel starPanel = new StarPanel(MapObject.system, sav, map.res, system, detailsImgSize);
            lstDetails.addItem(starPanel);

            double ratio = starPanel.getRatio(detailsImgSize);
            AniLabel lbl = null;
            int boni, btype;
            String cachestr;

            // planets
            for (int planetIndex = 0; planetIndex < system.getNumPlanets(); ++planetIndex) {
              val planet = system.getPlanet(planetIndex);
              String pic = planet.getAnimation();
              cachestr = pic;
              String name = planet.getName();
              String type = map.environ.getName(planet.getPlanetType());
              type = Character.toString(type.charAt(0)).toUpperCase() + type.substring(1);
              String tip = null;

              // get bonus
              boni = map.planboni.get(planet.getPlanetType(), planet.getPlanetSize(),
                planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());

              if (boni != 0) {
                btype = map.planBoniTypes.getBonusType(planet.getPlanetType());
                tip = "+%1% %2";
                tip = tip.replace("%1", Integer.toString(boni * 5));

                if (btype == PlanetBonusTypes.energy) {
                  btype = LexDataIdx.System.BonusType.Energy;
                  boni = 0;
                  cachestr = cachestr + "bie.tga";
                } else {
                  btype = LexDataIdx.System.BonusType.Food;
                  boni = 1;
                  cachestr = cachestr + "bif1.tga";
                }

                tip = tip.replace("%2", map.lexicon.getEntry(btype));
              }

              if (map.res.hasInternalFile(pic)) {
                if (pic.toLowerCase().endsWith(".ani")) {
                  // planet animation
                  lbl = new AniLabel();
                  lbl.setForceFirstPixelAlpha(true);
                  lbl.setMaxFrameSize(detailsImgSize);
                  // scale planet icons by the sun size
                  int bksize = (int) (detailsImgSize / ratio);

                  lbl.setBackgroundImg(new TargaImage(bksize, bksize, 16, true));

                  if (tip != null) {
                    // planet bonus icon
                    String bonusIconName = boni == 0 ? "bie.tga" : "bif1.tga";
                    TargaImage pbi = map.res.getTargaImage(bonusIconName, LoadFlags.CACHE);
                    lbl.setOverlayImg(0, 0, pbi);
                  }

                  AniOrCur ani = map.res.getAnimation(pic, LoadFlags.CACHE);
                  lbl.setAnimation(ani, true);
                }
              }

              int[] items = new int[] { MapObject.planet, item, planetIndex };
              lstDetails.addItem(lbl, name, type, null, items);
              lstDetails.setTooltip(planetIndex + 1, tip);

              boni = planet.getTerraformPoints();
              btype = map.planspac.get(
                  planet.getPlanetType(),
                  (byte) planet.getPlanetSize(),
                  planet.getEnergyBonusLvl(),
                  planet.getGrowthBonusLvl());

              if (btype == 0) {
                lstDetails.setItemLine2Foreground(planetIndex + 1, Color.BLUE);
              } else if (boni == 0) {
                lstDetails.setItemLine2Foreground(planetIndex + 1, Color.GREEN);
              } else if (planet.getTerraformed() == 0) {
                lstDetails.setItemLine2Foreground(planetIndex + 1, Color.RED);
              } else {
                lstDetails.setItemLine2Foreground(planetIndex + 1, Color.YELLOW);
              }
            }

            // add new planet button
            if (system.getNumPlanets() < CStbof.MAX_PLANETS)
              lstDetails.addCustomItem(btnAddPlanet, true, 0, -37);

            // right align system items to not introduce any right GridLayout spacing
            // by default the GridLayout list is aligned to the left
            pnlDetailItems.add(lstDetails, BorderLayout.EAST);
          }
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
      }

      if (item >= 0) {
        txtDetailsDesc.setText("<html><br><center><font size=\"4\" face=\""
            + font.getFamily() + "\" color=\"white\">" +
            "Hover the cursor<br>" +
            "over an item for details<br>" +
            "and right click on it to edit.</center></font></html>");
      } else {
        txtDetailsDesc.setText("");
      }
    } else if (outpost >= 0) {
      txtDetailsDesc.setText("<html><br><center><font size=\"4\" face=\""
          + font.getFamily() + "\" color=\"white\">" +
          "Hover the cursor<br>" +
          "over an item for details<br>" +
          "and right click on it to edit.</center></font></html>");

      try {
        short item = map.outposts.getShipID(outpost);
        String name = map.outposts.getName(outpost);

        if (item >= 0) {
          ShipDefinition shipDef = map.shipModels.getShipDefinition(item);
          String pic = shipDef.getGraphics();
          double cost = map.outposts.getProductionCost(outpost);
          double invested = map.outposts.getProductionInvested(outpost);
          pic = "i_" + pic + (cost <= invested ? "120.tga" : "w.tga");
          AniLabel lbl = null;

          if (map.res.hasInternalFile(pic)) {
            // starbase & outpost icon
            TargaImage tga = map.res.getTargaImage(pic, LoadFlags.CACHE);
            Dimension size = tga.getImageSize();
            Image img = null;

            if (size.height > detailsImgSize) {
              // accept larger width
              double ratio = detailsImgSize / (double) size.height;
              int scaledWidth = (int) (ratio * size.width);
              int scale = Integer.max(scaledWidth, detailsImgSize);
              img = tga.getThumbnail(scale, false, AlphaMode.FirstPixelAlpha);
            } else {
              img = tga.getImage(AlphaMode.FirstPixelAlpha);
            }

            lbl = new AniLabel(new ImageIcon(img));
          }

          int[] items = new int[] { MapObject.outpost, outpost };
          lstDetails.addItem(lbl, name, null, null, items);
          pnlDetailItems.add(lstDetails, BorderLayout.CENTER);
        }
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }
    } else if (taskforce >= 0) {
      // details
      try {
        TaskForce gtf = map.gWTForce.taskForce(taskforce);

        int columns = jspDetails.getWidth() / (detailsImgSize + 30);
        int shipNum = gtf.shipCount();
        int rows = (int) Math.ceil(((double) shipNum) / columns);

        lstDetails.setRowsAndColumnsNumber(rows, columns);
        lstDetails.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

        String strSpeed = Integer.toString((int) gtf.abilities().getMapSpeed());
        int range = (int) gtf.abilities().getMapRange();

        String strRange = null;
        switch (range) {
          case 0: {
            strRange = map.lexicon.getEntry(LexDataIdx.SpaceCraft.Range.Short);
            break;
          }
          case 1: {
            strRange = map.lexicon.getEntry(LexDataIdx.Other.Amount.Medium_2);
            break;
          }
          default: {
            strRange = map.lexicon.getEntry(LexDataIdx.SpaceCraft.Range.Long);
          }
        }

        String scanRange = Integer.toString(gtf.abilities().getScanRange()+1);
        int maintenance = 0, beams = 0, torp = 0;
        int typeID;

        short[] shpIds = gtf.getShipIDs();
        for (short i = 0; i < shpIds.length; i++) {
          typeID = map.gShipList.getShip(shpIds[i]).getShipType();
          ShipDefinition shipDef = map.shipModels.getShipDefinition(typeID);
          maintenance += shipDef.getMaintenance();
          beams += shipDef.getBeamNumber() * shipDef.getBeamDamage();
          torp += shipDef.getTorpedoNumber() * shipDef.getTorpedoDamage();
        }

        txtDetailsDesc.setText("<html><center><table cellpadding=0><font size=\"4\" face=\""
            + font.getFamily() + "\" color=\"white\">"
            + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
            + map.lexicon.getEntry(LexMenuIdx.SystemScreen.ShipBuild.GalSpeed).toUpperCase() + "</td><td><font size=\"3\" face=\"" + font
            .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + strSpeed + "</pre></td></tr>"
            + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
            + map.lexicon.getEntry(LexMenuIdx.SystemScreen.ShipBuild.GalRange).toUpperCase() + "</td><td><font size=\"3\" face=\"" + font
            .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + strRange + "</pre></td></tr>"
            + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
            + map.lexicon.getEntry(LexMenuIdx.ShipStats.SCANNER_RANGE).toUpperCase() + "</td><td><font size=\"3\" face=\"" + font
            .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + scanRange + "</td></tr>"
            + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
            + map.lexicon.getEntry(LexMenuIdx.ShipStats.MaintenanceCost).toUpperCase() + "</td><td><font size=\"3\" face=\"" + font
            .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + maintenance + "</td></tr>"
            + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
            + "\" color=\"teal\">BEAM FIREPOWER</td><td><font size=\"3\" face=\"" + font.getFamily()
            + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + beams + "</td></tr>"
            + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
            + "\" color=\"teal\">TORPEDO FIREPOWER</td><td><font size=\"3\" face=\"" + font
            .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + torp + "</td></tr>"
            + "</font></table></center></html>");

        short[] shipIds = gtf.getShipIDs();
        for (short shipID : shipIds) {
          int shipType = map.gShipList.getShip(shipID).getShipType();
          ShipDefinition shipDef = map.shipModels.getShipDefinition(shipType);
          String pic = shipDef.getGraphics();
          pic = "i_" + pic + "120.tga";
          AniLabel lbl = null;

          if (map.res.hasInternalFile(pic)) {
            // details list ship icon
            TargaImage tga = map.res.getTargaImage(pic, LoadFlags.CACHE);
            Dimension size = tga.getImageSize();
            Image img = null;

            if (size.height > detailsImgSize) {
              // accept larger width
              double ratio = detailsImgSize / (double) size.height;
              int scaledWidth = (int) (ratio * size.width);
              int scale = Integer.max(scaledWidth, detailsImgSize);
              img = tga.getThumbnail(scale, false, AlphaMode.FirstPixelAlpha);
            } else {
              img = tga.getImage(AlphaMode.FirstPixelAlpha);
            }

            lbl = new AniLabel(new ImageIcon(img));
          }

          int[] items = new int[] { MapObject.ship, taskforce, shipID };
          lstDetails.addItem(lbl, null, null, null, items);
          pnlDetailItems.add(lstDetails, BorderLayout.CENTER);
        }
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }

    } else {
      txtDetailsDesc.setText("");
    }

    pnlDetailItems.setVisible(false);
    pnlDetailItems.setVisible(true);
  }

  private class ViewListener implements ActionListener {

    private int index;

    public ViewListener(int index) {
      this.index = index;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
      map.setViewMode(index);
    }
  }

  private class SectorSelectionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent evt) {
      // check selection
      Point sector = map.selection1;
      updateMilitaryList(sector);

      if (prev_sector == null || sector == null || !sector.equals(prev_sector))
        updateSystemDetails();

      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          if (lstMilitary.getWidth() >= jspMilitary.getWidth()) {
            Dimension dim = jspMilitary.getPreferredSize();
            dim.width = lstMilitary.getWidth() + 5;
            jspMilitary.setPreferredSize(dim);
            setVisible(false);
            setVisible(true);
          }
          if (jspDetails.getVerticalScrollBar().isShowing()) {
            Dimension dim = jspDetails.getPreferredSize();
            dim.height = lstDetails.getHeight() + 5 + jspDetails.getHorizontalScrollBar().getHeight();
            jspDetails.setPreferredSize(dim);
            setVisible(false);
            setVisible(true);
          }
        }
      });

      prev_sector = sector;
    }
  }

  private class RightClickSelectionListener implements SelectionListener {

    @Override
    public void selected(SelectionEvent event) {
      int[] index = event.getSelectedIndex();
      List<JMenuItem> items = getMenuItems(index);

      if (items != null) {
        JPopupMenu menu = new JPopupMenu();

        for (int i = 0; i < items.size(); i++) {
          JMenuItem mnuItem = items.get(i);
          mnuItem.setFont(UE.SETTINGS.getDefaultFont());
          menu.add(mnuItem);
        }

        menu.pack();
        Point loc = event.getLocation();
        loc.x = loc.x - UE.WINDOW.getX() - menu.getPreferredSize().width;
        loc.y = loc.y - UE.WINDOW.getY();

        menu.show(UE.WINDOW, loc.x, loc.y);
      }
    }

    private List<JMenuItem> getMenuItems(int[] index) {
      List<JMenuItem> items = null;

      try {
        switch (index[0]) {
          case MapObject.outpost: {
            short outpostId = (short) index[1];
            items = MapGUIOutpostMenu.getMenuItems(map, sgif, stif, outpostId);
            break;
          }
          case MapObject.taskforce: {
            short taskforceId = (short) index[1];
            items = MapGUITaskForceMenu.getMenuItems(map, sgif, stif, taskforceId);
            break;
          }
          case MapObject.stellar_object: {
            short stellId = (short) index[1];
            items = MapGUIStellarMenu.getMenuItems(map, sgif, stbof, trek, stellId);
            break;
          }
          case MapObject.system: {
            short systemId = (short) index[1];
            items = MapGUISystemMenu.getMenuItems(map, sav, stbof, trek, systemId, true);
            break;
          }
          case MapObject.planet: {
            short systemId = (short) index[1];
            short planetId = (short) index[2];
            items = MapGUIPlanetMenu.getMenuItems(MapGUI.this, sav, Optional.ofNullable(stbof), systemId, planetId);
            break;
          }
          case MapObject.ship: {
            short taskforceId = (short) index[1];
            short shipId = (short) index[2];
            items = MapGUIShipMenu.getMenuItems(MapGUI.this, sgif, Optional.ofNullable(stif), taskforceId, shipId);
            break;
          }
          default: {
            items = MapGUISectorMenu.getMenuItems(map, sav, stbof, trek, index[1]);
          }
        }
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }

      return items;
    }
  }

  private class MilitarySelectionListener implements ListSelectionListener {

    private int[] militaryID = null;

    @Override
    public void valueChanged(ListSelectionEvent e) {
      try {
        if (lstMilitary.getSelectedItemIndex() >= 0) {
          militaryID = lstMilitary.getSelectedItemID();

          // draw path
          if (militaryID[0] == MapObject.taskforce) {   // taskforce
            short tfId = (short) militaryID[1];
            map.selectTaskForcePath(tfId, true);
            updateDetailsList(null, tfId, (short) -1);
          } else {   // outpost
            short outpostId = (short) militaryID[1];
            map.selectTaskForcePath((short)-1, false);
            updateDetailsList(null, (short)-1, outpostId);
          }
        } else {
          map.selectTaskForcePath((short)-1, false);
          updateSystemDetails();
        }
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }
    }
  }

  private class DetailsSelectionListener implements ListSelectionListener {

    private int[] itemID = null;

    @Override
    public void valueChanged(ListSelectionEvent e) {
      try {
        if (lstDetails.getSelectedItemIndex() >= 0) {
          itemID = lstDetails.getSelectedItemID();

          if (itemID[0] == MapObject.stellar_object) {
            int type = map.stellarObjects.getStellarObject(itemID[1]).getType();
            txtDetailsDesc.setText(
              "<html><div width: 100px word-wrap: break-word><center><font size=\"3\" face=\""
                + font.getFamily() + "\" color=\"white\"><br>"
                + stif.stellarTools().mapStellarDesc(type) + "</center></div></html>");
          } else if (itemID[0] == MapObject.system) {
            val system = map.systems.getSystem(itemID[1]);

            // name, star type, race, population, max pop, orbitals, ground combat
            int fsize = 3;
            String type = stif.stellarTools().mapStellarType(system.getStarType());

            int pop = system.getPopulation();
            String popStr = "";
            String race = "";

            if (pop > 0) {
              popStr = Integer.toString(pop);
              race = map.races.getName(system.getResidentRace());
            }

            pop = 0;
            int mPop = 0;

            for (int p = 0; p < system.getNumPlanets(); ++p) {
              val planet = system.getPlanet(p);

              int val = map.planspac.get(planet.getPlanetType(), planet.getPlanetSize(),
                planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());

              if (val > 0) {
                if (planet.getTerraformPoints() <= planet.getTerraformed())
                  pop = pop + val;

                mPop += val;
              }
            }

            String maxPop = pop + " / " + mPop;
            String groundCombat = "";
            int r = system.getResidentRace();

            if (r >= 0 && r < map.races.getNumberOfEntries()) {
              double techMulti = map.groundCombatTechMulti.getDouble();
              techMulti = techMulti * map.techInfo.getTechLevel((short) r, 6);

              double defense = pop * map.races.getCombatMulti(r) * (1.0 + techMulti);
              int ctrlRace = system.getControllingRace();

              if (ctrlRace >= 0 && ctrlRace < NUM_EMPIRES) {
                int grndBonus = map.empireInfo.getGroundCombatBonus(ctrlRace);
                defense = defense * (1.0 + grndBonus / 100.0);
              }

              int num;
              if (ctrlRace < 0 || ctrlRace > 4)
                ctrlRace = NUM_EMPIRES;

              for (short bid : map.strcInfo.getBuildings((short) itemID[1])) {
                num = map.strcInfo.getPoweredOnCount((short) itemID[1], bid);
                if (num <= 0)
                  continue;

                Building bld = map.edifice.building(bid);
                if (bld.getBonusType() == 37)
                  defense = defense * (1.0 + num * bld.getBonusValue(ctrlRace) / 100.0);
              }

              groundCombat = Integer.toString((int) Math.floor(defense));
            }

            int orbid = map.edifice.getTrekExeID(StructureType.OrbitalBattery);
            String orbs = "";

            if (orbid >= 0 && popStr.length() > 0) {
              orbs = map.strcInfo.getPoweredOnCount((short) itemID[1], (short) orbid) +
                " / " + map.strcInfo.getCount((short) itemID[1], (short) orbid);
            }

            String desc =
                "<html><center><table cellpadding=0><tr><td>"
                    + "<font size=\"" + fsize + "\" face=\"" + font.getFamily()
                    + "\" color=\"yellow\">" + system.getName()
                    + "</td></tr><tr><td><table cellpadding=0><font size=\"" + fsize + "\" face=\""
                    + font.getFamily() + "\" color=\"white\">"
                    + "<tr><td><font size=\"" + fsize + "\" face=\"" + font.getFamily()
                    + "\" color=\"teal\">" + map.lexicon.getEntry(LexMenuIdx.Other.Type) + "</td><td><font size=\""
                    + fsize + "\" face=\"" + font.getFamily()
                    + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + type + "</pre></td></tr>";

            if (race.length() > 0) {
              desc = desc + "<tr><td><font size=\"" + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"teal\">" + map.lexicon.getEntry(LexMenuIdx.DiplomacyScreen.TreatyList.Race) + "</td><td><font size=\""
                  + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + race + "</td></tr>"
                  + "<tr><td><font size=\"" + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"teal\">" + map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.Population) + "</td><td><font size=\""
                  + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + popStr + "</td></tr>";
            }

            desc = desc + "<tr><td><font size=\"" + fsize + "\" face=\"" + font.getFamily()
                + "\" color=\"teal\">" + map.lexicon.getEntry(LexMenuIdx.Galaxy.SystemPanel.MaxPop) + "</td><td><font size=\""
                + fsize + "\" face=\"" + font.getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;"
                + maxPop + "</td></tr>";

            if (groundCombat.length() > 0) {
              desc = desc + "<tr><td><font size=\"" + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"teal\">" + map.lexicon.getEntry(LexMenuIdx.Galaxy.Map.GroundCombat) + "</td><td><font size=\""
                  + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + groundCombat + "</td></tr>";
            }

            if (orbs.length() > 0) {
              desc = desc + "<tr><td><font size=\"" + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"teal\">" + map.lexicon.getEntry(LexMenuIdx.Galaxy.SystemPanel.Orbitals) + "</td><td><font size=\""
                  + fsize + "\" face=\"" + font.getFamily()
                  + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + orbs + "</td></tr>";
            }

            desc = desc + "</font></table></td></tr></table></center></html>";

            txtDetailsDesc.setText(desc);
          } else if (itemID[0] == MapObject.outpost) {
            short outpostId = (short) itemID[1];

            // type, hull (hit points), shield strength, maintenance, weapons
            String type;
            int tmp = map.outposts.getRole(outpostId);

            if (tmp == 6) {
              type = map.lexicon.getEntry(LexDataIdx.Combat.ShipGroup.Outpost);
            } else if (tmp == 7) {
              type = map.lexicon.getEntry(LexDataIdx.Combat.ShipGroup.Starbase);
            } else {
              type = map.lexicon.getEntry(LexDataIdx.Shared.Unknown);
            }

            int shipID = map.outposts.getShipID(outpostId);
            ShipDefinition shipDef = map.shipModels.getShipDefinition(shipID);

            String hitpoints = "%1 / %2";
            int hull = map.outposts.getHull(outpostId);
            hitpoints = hitpoints.replace("%1", Integer.toString(hull));
            hull = shipDef.getHullStrength();
            hitpoints = hitpoints.replace("%2", Integer.toString(hull));

            String shieldStr = Integer.toString(shipDef.getShieldStrength());
            String maintenance = Integer.toString(shipDef.getMaintenance());

            String beams = "%1 x %2";
            beams = beams.replace("%1", Integer.toString(shipDef.getBeamNumber()));
            beams = beams.replace("%2", Integer.toString(shipDef.getBeamDamage()));

            String torp = "%1 x %2";
            torp = torp.replace("%1", Integer.toString(shipDef.getTorpedoNumber()));
            torp = torp.replace("%2", Integer.toString(shipDef.getTorpedoDamage()));

            txtDetailsDesc.setText(
                "<html><center><table cellpadding=0><tr><td>"
                    + "<font size=\"3\" face=\"" + font.getFamily() + "\" color=\"yellow\">"
                    + map.outposts.getName(outpostId)
                    + "</td></tr><tr><td><table cellpadding=0><font size=\"3\" face=\""
                    + font.getFamily() + "\" color=\"white\">"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.Other.Type) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + type
                    + "</pre></td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.ShipStats.HitPoints) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + hitpoints
                    + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.ShipStats.ShieldStrength) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + shieldStr
                    + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.ShipStats.MaintenanceCost) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + maintenance
                    + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"teal\">Beam Weapons</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + beams + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"teal\">Torpedo Weapons</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + torp + "</td></tr>"
                    + "</font></table></td></tr></table></center></html>");
          } else if (itemID[0] == MapObject.planet) {
            val system = map.systems.getSystem(itemID[1]);
            val planet = system.getPlanet(itemID[2]);

            // type, atmo, uninhabitable / (maxpop, race/terraform)
            String type = getPlanetTypeString(planet.getPlanetType());
            String atmo = getAtmoTypeString(planet.getAtmosphere());

            int pop = map.planspac.get(
                planet.getPlanetType(),
                (byte) planet.getPlanetSize(),
                planet.getEnergyBonusLvl(),
                planet.getGrowthBonusLvl());

            String maxPop = Integer.toString(pop);
            String race = "";
            if (pop > 0) {
              int r = system.getResidentRace();
              if (r >= 0 && r < map.races.getNumberOfEntries()) {
                race = map.races.getName(r);
              }
            }

            String terra = "";
            if (planet.getTerraformPoints() > planet.getTerraformed()) {
              terra = planet.getTerraformed() <= 0 ? "0 / " + planet.getTerraformPoints()
                : ((100 * planet.getTerraformed()) / planet.getTerraformPoints()) + "%";
            }

            String sizeStr = "";
            switch (planet.getPlanetSize()) {
              case 0: {
                sizeStr = map.lexicon.getEntry(LexDataIdx.GameSettings.GalaxySize.Small);
                break;
              }
              case 1: {
                sizeStr = map.lexicon.getEntry(LexDataIdx.GameSettings.GalaxySize.Medium);
                break;
              }
              default: {
                sizeStr = map.lexicon.getEntry(LexDataIdx.GameSettings.GalaxySize.Large);
              }
            }

            String desc =
                "<html><center><table cellpadding=0><tr><td>"
                    + "<font size=\"3\" face=\"" + font.getFamily() + "\" color=\"yellow\">"
                    + planet.getName()
                    + "</td></tr><tr><td><table cellpadding=0><font size=\"3\" face=\""
                    + font.getFamily() + "\" color=\"white\">"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.Other.Type) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + type
                    + "</pre></td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + "Size" + "</td><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + sizeStr + "</pre></td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + "Atmosphere" + "</td><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + atmo + "</pre></td></tr>";

            if (pop > 0) {
              desc =
                  desc + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                      + map.lexicon.getEntry(LexMenuIdx.Galaxy.SystemPanel.MaxPop) + "</td><td><font size=\"3\" face=\"" + font
                      .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + maxPop
                      + "</td></tr>";

              if (terra.length() > 0) {
                desc = desc + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"teal\">" + map.lexicon.getEntry(LexDataIdx.Other.ShipCommand.Terraform)
                    + "</td><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + terra + "</td></tr>";
              } else {
                desc = desc + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"teal\"></td><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + race + "</td></tr>";
              }
            } else {
              desc = desc + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                  + "\" color=\"teal\"></td><td><font size=\"3\" face=\"" + font.getFamily()
                  + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + map.lexicon.getEntry(LexDataIdx.System.Population.Uninhabitable)
                  + "</td></tr>";
            }

            desc = desc + "</font></table></td></tr></table></center></html>";

            txtDetailsDesc.setText(desc);
          } else if (itemID[0] == MapObject.ship) {
            // name, class, type, hull (hit points), shield strength, maintenance, weapons
            short shipID = (short) itemID[2];
            Ship ship = map.gShipList.getShip(shipID);
            int typeID = ship.getShipType();
            ShipDefinition shipDef = map.shipModels.getShipDefinition(typeID);

            String name = ship.getName();
            String strClass = shipDef.getShipClass();
            String strFunc = LexHelper.mapShipRole(shipDef.getShipRole());

            String hitpoints = "%1 / %2";
            int val = ship.getHullStr();
            hitpoints = hitpoints.replace("%1", Integer.toString(val));
            val = shipDef.getHullStrength();
            hitpoints = hitpoints.replace("%2", Integer.toString(val));

            String shieldStr = Integer.toString(shipDef.getShieldStrength());
            String maintenance = Integer.toString(shipDef.getMaintenance());

            String beams = "%1 x %2";
            beams = beams.replace("%1", Integer.toString(shipDef.getBeamNumber()));
            beams = beams.replace("%2", Integer.toString(shipDef.getBeamDamage()));

            String torp = "%1 x %2";
            torp = torp.replace("%1", Integer.toString(shipDef.getTorpedoNumber()));
            torp = torp.replace("%2", Integer.toString(shipDef.getTorpedoDamage()));

            txtDetailsDesc.setText(
                "<html><center><table cellpadding=0><tr><td>"
                    + "<font size=\"3\" face=\"" + font.getFamily() + "\" color=\"yellow\">" + name
                    + "</td></tr><tr><td><table cellpadding=0><font size=\"3\" face=\""
                    + font.getFamily() + "\" color=\"white\">"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.Other.Type) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + strFunc
                    + "</pre></td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + WordUtils.capitalize(map.lexicon.getEntry(LexMenuIdx.Other.SHIP_CLASS).toLowerCase())
                    + "</td><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + strClass + "</pre></td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.ShipStats.HitPoints) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + hitpoints
                    + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.ShipStats.ShieldStrength) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + shieldStr
                    + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                    + map.lexicon.getEntry(LexMenuIdx.ShipStats.MaintenanceCost) + "</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + maintenance
                    + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"teal\">Beam Weapons</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + beams + "</td></tr>"
                    + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                    + "\" color=\"teal\">Torpedo Weapons</td><td><font size=\"3\" face=\"" + font
                    .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + torp + "</td></tr>"
                    + "</font></table></td></tr></table></center></html>");
          } else {
            txtDetailsDesc.setText("<html><br><br><br><center><font size=\"4\" face=\""
                + font.getFamily() + "\" color=\"white\">" +
                "TODO: List details for item type " + itemID[0]);
          }
        } else {
          if (lstDetails.getNumberOfItems() > 0) {
            int[] milItemID = lstMilitary.getSelectedItemID();

            if (milItemID != null && milItemID[0] == MapObject.taskforce) {
              short tfId = (short)milItemID[1];
              TaskForce gtf = map.gWTForce.taskForce(tfId);

              String strSpeed = Integer.toString((int) gtf.abilities().getMapSpeed());
              int range = (int) gtf.abilities().getMapRange();

              String strRange = null;
              switch (range) {
                case 0: {
                  strRange = map.lexicon.getEntry(LexDataIdx.SpaceCraft.Range.Short);
                  break;
                }
                case 1: {
                  strRange = map.lexicon.getEntry(LexDataIdx.Other.Amount.Medium_2);
                  break;
                }
                default: {
                  strRange = map.lexicon.getEntry(LexDataIdx.SpaceCraft.Range.Long);
                }
              }

              String scanRange = Integer.toString(gtf.abilities().getScanRange() + 1);
              int maintenance = 0;
              int beams = 0;
              int torp = 0;

              short[] shpIds = gtf.getShipIDs();
              for (int i = 0; i < shpIds.length; i++) {
                int typeID = map.gShipList.getShip(shpIds[i]).getShipType();
                ShipDefinition shipDef = map.shipModels.getShipDefinition(typeID);
                maintenance += shipDef.getMaintenance();
                beams += shipDef.getBeamNumber() * shipDef.getBeamDamage();
                torp += shipDef.getTorpedoNumber() * shipDef.getTorpedoDamage();
              }

              txtDetailsDesc.setText("<html><center><table cellpadding=0><font size=\"4\" face=\""
                  + font.getFamily() + "\" color=\"white\">"
                  + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                  + map.lexicon.getEntry(LexMenuIdx.SystemScreen.ShipBuild.GalSpeed).toUpperCase() + "</td><td><font size=\"3\" face=\""
                  + font.getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + strSpeed
                  + "</pre></td></tr>"
                  + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                  + map.lexicon.getEntry(LexMenuIdx.SystemScreen.ShipBuild.GalRange).toUpperCase() + "</td><td><font size=\"3\" face=\""
                  + font.getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + strRange
                  + "</pre></td></tr>"
                  + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                  + map.lexicon.getEntry(LexMenuIdx.ShipStats.SCANNER_RANGE).toUpperCase() + "</td><td><font size=\"3\" face=\""
                  + font.getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + scanRange
                  + "</td></tr>"
                  + "<tr><td><font size=\"3\" face=\"" + font.getFamily() + "\" color=\"teal\">"
                  + map.lexicon.getEntry(LexMenuIdx.ShipStats.MaintenanceCost).toUpperCase() + "</td><td><font size=\"3\" face=\""
                  + font.getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + maintenance
                  + "</td></tr>"
                  + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                  + "\" color=\"teal\">BEAM FIREPOWER</td><td><font size=\"3\" face=\"" + font
                  .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + beams + "</td></tr>"
                  + "<tr><td><font size=\"3\" face=\"" + font.getFamily()
                  + "\" color=\"teal\">TORPEDO FIREPOWER</td><td><font size=\"3\" face=\"" + font
                  .getFamily() + "\" color=\"white\"> &nbsp;&nbsp;&nbsp;" + torp + "</td></tr>"
                  + "</font></table></center></html>");
            } else {
              txtDetailsDesc.setText("<html><br><center><font size=\"4\" face=\""
                  + font.getFamily() + "\" color=\"white\">" +
                  "Hover the cursor<br>" +
                  "over an item for details<br>" +
                  "and right click on it to edit.</center></font></html>");
            }
          } else {
            txtDetailsDesc.setText("");
          }
        }
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }
    }
  }

}
