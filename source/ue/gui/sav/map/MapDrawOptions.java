package ue.gui.sav.map;

import ue.UE;
import ue.service.SettingsManager;

public class MapDrawOptions {
  public boolean drawOwnership = false;
  public boolean drawObjects = false;
  public boolean drawOutposts = false;
  public boolean drawShips = false;
  public boolean drawPaths = false;
  public boolean drawSystemLabels = false;
  public boolean drawStellarLabels = false;

  public static final MapDrawOptions BASIC = basic();
  public static final MapDrawOptions FULL = full();
  public static final MapDrawOptions RACE_SPECIFIC = raceSpecific();
  public static final MapDrawOptions ZOOM = zoom();
  public static final MapDrawOptions SYSTEM_LABELS = systemLabels();
  public static final MapDrawOptions STELLAR_LABELS = stellarLabels();

  public static MapDrawOptions basic() {
    return new MapDrawOptions();
  }

  public static MapDrawOptions full() {
    MapDrawOptions drawOptions = new MapDrawOptions();
    drawOptions.drawOwnership = true;
    drawOptions.drawObjects = true;
    drawOptions.drawOutposts = true;
    drawOptions.drawShips = true;
    drawOptions.drawPaths = true;
    drawOptions.drawSystemLabels = true;
    drawOptions.drawStellarLabels = true;
    return drawOptions;
  }

  public static MapDrawOptions raceSpecific() {
    MapDrawOptions drawOptions = new MapDrawOptions();
    drawOptions.drawOutposts = true;
    drawOptions.drawShips = true;
    drawOptions.drawPaths = true;
    drawOptions.drawSystemLabels = true;
    drawOptions.drawStellarLabels = true;
    return drawOptions;
  }

  public static MapDrawOptions zoom() {
    MapDrawOptions drawOptions = new MapDrawOptions();
    drawOptions.drawSystemLabels = true;
    drawOptions.drawStellarLabels = true;
    return drawOptions;
  }

  public static MapDrawOptions systemLabels() {
    MapDrawOptions drawOptions = new MapDrawOptions();
    drawOptions.drawSystemLabels = true;
    return drawOptions;
  }

  public static MapDrawOptions stellarLabels() {
    MapDrawOptions drawOptions = new MapDrawOptions();
    drawOptions.drawStellarLabels = true;
    return drawOptions;
  }

  public static MapDrawOptions fromSettings() {
    MapDrawOptions drawOptions = new MapDrawOptions();
    drawOptions.drawOwnership = !UE.SETTINGS.getBooleanProperty(SettingsManager.MAP_HIDE_OWNERSHIP);
    drawOptions.drawObjects = !UE.SETTINGS.getBooleanProperty(SettingsManager.MAP_HIDE_OBJECTS);
    drawOptions.drawOutposts = !UE.SETTINGS.getBooleanProperty(SettingsManager.MAP_HIDE_OUTPOSTS);
    drawOptions.drawShips = !UE.SETTINGS.getBooleanProperty(SettingsManager.MAP_HIDE_SHIPS);
    drawOptions.drawPaths = !UE.SETTINGS.getBooleanProperty(SettingsManager.MAP_HIDE_PATHS);
    drawOptions.drawSystemLabels = !UE.SETTINGS.getBooleanProperty(SettingsManager.MAP_HIDE_SYSTEM_LABELS);
    drawOptions.drawStellarLabels = !UE.SETTINGS.getBooleanProperty(SettingsManager.MAP_HIDE_STELLAR_LABELS);
    return drawOptions;
  }
}
