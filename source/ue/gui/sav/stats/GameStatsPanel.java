package ue.gui.sav.stats;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.dic.idx.LexMenuIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.files.GameInfo;
import ue.gui.common.CGui;
import ue.service.Language;
import ue.util.func.FunctionTools;

/**
 * Used by the GameInfoGUI to display general save game statistics.
 */
public class GameStatsPanel extends JPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private GameInfo GAME;
  private RaceRst RACE;
  private Lexicon LEX;
  private String invalid;

  //items
  private JPanel pnlConfigBg = new JPanel();
  private JPanel pnlStatsBg = new JPanel();
  private JPanel pnlRacesBg = new JPanel();
  private JTextPane lblGameConfig = new JTextPane();
  private JTextPane lblGameStats = new JTextPane();
  private JTextPane lblRaces = new JTextPane();

  /**
   * Constructor.
   * @throws IOException
   */
  public GameStatsPanel(GameInfo gameInfo, Stbof stbof) throws IOException {
    init(stbof);
    setupControls();
    setupLayout();
    load(gameInfo);
  }

  private void init(Stbof stbof) throws IOException {
    RACE = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    LEX = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);
    invalid = LEX.getEntryOrDefault(LexDataIdx.Shared.Invalid, "Invalid"); //$NON-NLS-1$
  }

  private void setupControls() {
    this.setOpaque(false);
    pnlConfigBg.setBackground(new Color(0,0,0,100));
    pnlStatsBg.setBackground(new Color(0,0,0,100));
    pnlRacesBg.setBackground(new Color(0,0,0,100));
    Font defFont = UE.SETTINGS.getDefaultFont();
    lblGameConfig.setContentType("text/html");
    lblGameConfig.setOpaque(false);
    lblGameConfig.setEditable(false);
    lblGameConfig.setFont(defFont);
    lblGameStats.setContentType("text/html");
    lblGameStats.setOpaque(false);
    lblGameStats.setEditable(false);
    lblGameStats.setFont(defFont);
    lblRaces.setContentType("text/html");
    lblRaces.setOpaque(false);
    lblRaces.setEditable(false);
    lblRaces.setFont(defFont);
  }

  private void setupLayout() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(10, 5, 5, 5);
    c.gridwidth = 3;
    c.weightx = 0;
    c.weighty = 0;
    c.gridx = 0;
    c.gridy = 0;
    add(lblGameConfig, c);
    add(pnlConfigBg, c);
    c.gridwidth = 2;
    c.gridy++;
    add(lblGameStats, c);
    add(pnlStatsBg, c);
    c.gridwidth = 1;
    c.weighty = 1.0;
    c.gridy++;
    add(Box.createVerticalGlue(), c);
    c.insets = new Insets(5, 5, 25, 5);
    c.weighty = 0;
    c.weighty = 0;
    c.gridy++;
    add(lblRaces, c);
    add(pnlRacesBg, c);
    c.weightx = 1.0;
    c.gridx = 4;
    add(Box.createHorizontalGlue(), c);
  }

  public void load(GameInfo gameInfo) {
    GAME = gameInfo;
    lblGameConfig.setText(getConfig());
    lblGameStats.setText(getStats());
    lblRaces.setText(getRaces());
  }

  private String getConfig() {
    StringBuffer config = new StringBuffer();
    int playerEmpire = GAME.getPlayerEmpire();
    // show last turn to match the BotF savegame names
    int turn = GAME.getLastTurn();

    config.append("<div color='white' style='font-size:10px'><b>");
    config.append("<tr><td style='padding: -1 2'>");
    config.append(FunctionTools.logIfThrown(() -> RACE.getName(playerEmpire), invalid));
    config.append(' ');
    config.append(turn);
    config.append(' ');
    config.append(LEX.getEntryOrDefault(LexHelper.lexGalSize(GAME.getGalaxySize()), invalid));
    config.append(' ');
    config.append(LEX.getEntryOrDefault(LexHelper.lexGalShape(GAME.getGalaxyShape()), invalid));
    config.append(' ');
    config.append(LEX.getEntryOrDefault(LexMenuIdx.GameSetup.LoadGame.Galaxy, invalid));
    config.append("<br>");
    config.append(GAME.isMultiplayer() ? Language.getString("GameStatsPanel.1") : Language.getString("GameStatsPanel.0"));
    if (GAME.isRandomEvents()) {
      config.append("  &  ");
      config.append(Language.getString("GameStatsPanel.2"));
    }
    config.append("</b></td><td style='padding: -1 2 -1 20'>");
    config.append("<div color='silver'>");
    config.append(LEX.getEntryOrDefault(LexHelper.lexEvoLvl(GAME.getStartEvoLvl(playerEmpire)), invalid));
    config.append(' ');
    config.append(LEX.getEntryOrDefault(LexHelper.lexMinorAmount(GAME.getMinorRaceAmount()), invalid));
    config.append(' ');
    config.append(LEX.getEntryOrDefault(LexHelper.lexDifficulty(GAME.getDifficulty()), invalid));
    config.append("<br>");
    config.append(LEX.getEntryOrDefault(LexHelper.lexTacticalCombat(GAME.getTacticalMode()), invalid));
    config.append(' ');
    config.append(LEX.getEntryOrDefault(LexHelper.lexVictoryConditions(GAME.getVictoryConditions()), invalid));
    config.append("</td></tr></div></div>");

    return config.toString();
  }

  private String getStats() {
    StringBuffer stats = new StringBuffer();
    int lastTurn = GAME.getLastTurn();

    // stats: score, intel, military, economy, research, minors, systems, dilithium
    stats.append("<div color='white' style='font-size:10px'>");
    stats.append("<table cellspacing='0' cellpadding='0'><tr>");
    stats.append("<th style='padding: -1 2; font-size:11px' align='right'><u><b>");
    stats.append(Language.getString("GameStatsPanel.3"));
    stats.append("</b></u></th>");
    for (int i = 4; i < 11; ++i) {
      stats.append("<th style='padding: -1 2' align='right'><u>");
      // listed to exclude from clean_properties.sh:
      // "GameStatsPanel.4", "GameStatsPanel.5", "GameStatsPanel.6", "GameStatsPanel.7",
      // "GameStatsPanel.8", "GameStatsPanel.9", "GameStatsPanel.10"
      stats.append(Language.getString("GameStatsPanel." + Integer.toString(i)));
      stats.append("</u></th>");
    }
    stats.append("</tr>");
    for (int empire = 0; empire < NUM_EMPIRES; ++empire) {
      stats.append("<tr color=");
      stats.append(CGui.RACE_COLOR_CODES[empire]);
      stats.append("><td style='padding: -2 2; font-size:11px' align='right'><b>");
      stats.append(GAME.getEmpireScore(empire, lastTurn));
      stats.append("</b></td><td style='padding: -2 2' align='right'>");
      stats.append(GAME.getEmpireStartScore(empire));
      stats.append("</td><td style='padding: -2 2' align='right'>");
      stats.append(GAME.getEmpireMilitary(empire));
      stats.append("</td><td style='padding: -2 2' align='right'>");
      stats.append(GAME.getEmpireEconomy(empire));
      stats.append("</td><td style='padding: -2 2' align='right'>");
      stats.append(GAME.getEmpireScience(empire));
      stats.append("</td><td style='padding: -2 2' align='right'>");
      stats.append(GAME.getEmpireMembers(empire));
      stats.append("</td><td style='padding: -2 2' align='right'>");
      stats.append(GAME.getEmpireSystems(empire));
      stats.append("</td><td style='padding: -2 2' align='right'>");
      stats.append(GAME.getEmpireDilithium(empire));
      stats.append("</td></tr>");
    }
    stats.append("</table></div>");

    return stats.toString();
  }

  private String getRaces() {
    StringBuffer races = new StringBuffer();

    races.append("<table cellspacing='0' cellpadding='0' style='font-size:12px; font-weight: bold'>");
    for (int empire = 0; empire < NUM_EMPIRES; ++empire) {
      races.append("<tr color=");
      races.append(CGui.RACE_COLOR_CODES[empire]);
      races.append("><td style='padding: -1 2'>");
      races.append(FunctionTools.logIfThrown(emp -> RACE.getName(emp), invalid, empire));
      races.append("</td></tr>");
    }
    races.append("</table>");

    return races.toString();
  }
}
