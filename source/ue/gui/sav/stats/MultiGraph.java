package ue.gui.sav.stats;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import lombok.Getter;
import lombok.Setter;

public class MultiGraph extends JPanel {
  private static final Color DEFAULT_COLOR = Color.BLACK;

  @Getter @Setter Color[] colors;
  @Getter @Setter int end = -1;
  @Getter @Setter int thickness = 1;
  @Getter @Setter boolean shadow = false;
  private int[][] values;
  private int max = 0;

  public MultiGraph() {
  }

  public MultiGraph(int[][] values) {
    setValues(values);
  }

  public MultiGraph(int[][] values, Color[] colors) {
    setValues(values);
    setColors(colors);
  }

  public void setValues(int[][] values) {
    this.values = values;
    this.max = determineMaxValue(values);
  }

  @Override
  protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      int width = this.getWidth();
      int height = this.getHeight();
      if (width <= 0 || height <= 0)
        return;

      Graphics2D g2d = (Graphics2D) g;
      if (thickness != 1)
        g2d.setStroke(new BasicStroke(thickness));

      int graphCount = values.length;

      for (int i = 0; i < graphCount; ++i) {
        int[] range = values[i];
        int valueCount = range.length;
        if (valueCount < 1)
          continue;

        if (end >= 0 && end < valueCount)
          valueCount = end;

        Color lineColor = getGraphColor(i);
        if (!shadow)
          g2d.setColor(lineColor);

        double segWidth = width / (double)valueCount;
        double startX = 0;
        double startY = height;

        for (int j = 0; j < valueCount; ++j) {
          int value = range[j];
          double endX = startX + segWidth;
          double endY = height - value * (height / (double) max);
          if (shadow) {
            g2d.setColor(lineColor.darker());
            g2d.drawLine((int)startX, (int)startY+thickness, (int)endX, (int)endY+thickness);
            g2d.setColor(lineColor);
          }
          g2d.drawLine((int)startX, (int)startY, (int)endX, (int)endY);
          startX = endX;
          startY = endY;
        }
      }
  }

  private Color getGraphColor(int idx) {
    return this.colors != null && this.colors.length > idx ? this.colors[idx] : DEFAULT_COLOR;
  }

  private int determineMaxValue(int[][] values) {
    int max = 0;
    for (int[] range : values)
      for (int value : range)
        max = Integer.max(max, value);
    return max;
  }
}
