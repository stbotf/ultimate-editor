package ue.gui.sav.stats;

import ue.gui.common.CGui;

public class ScorePanel extends MultiGraph {

  public ScorePanel() {
    init();
  }

  public ScorePanel(int[][] values) {
    super(values);
    init();
  }

  private void init() {
    this.setColors(CGui.RACE_COLORS);
    this.setOpaque(false);
    this.setShadow(false);
    this.setThickness(3);
  }
}
