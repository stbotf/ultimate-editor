package ue.gui.sav.stats;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.basic.BasicScrollBarUI;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.dic.idx.LexMenuIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.GameInfo.GameStats;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.AlienInfo.AlienStats;
import ue.edit.sav.files.emp.IntelInfo;
import ue.edit.sav.files.emp.IntelInfo.IntelStats;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.emp.ResultList.ResultStats;
import ue.edit.sav.files.emp.StrcInfo;
import ue.edit.sav.files.emp.StrcInfo.StrcStatEntry;
import ue.edit.sav.files.emp.StrcInfo.StrcStats;
import ue.edit.sav.files.emp.TechInfo;
import ue.edit.sav.files.emp.TechInfo.TechStats;
import ue.edit.sav.files.emp.Treaty;
import ue.edit.sav.files.emp.Treaty.TreatyStats;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.GShipList.ShipStats;
import ue.edit.sav.files.map.GalInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.Sector.SectorStats;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.edit.sav.files.map.StarBaseInfo.StarbaseStats;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.StellInfo.StellarStats;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.SystInfo.PlanetStats;
import ue.edit.sav.files.map.SystInfo.SystemStats;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.TaskForceList.TaskForceStats;
import ue.service.Language;
import ue.util.data.StringTools;
import ue.util.func.FunctionTools;

/**
 * Used by the GameInfoGUI to display general savegame statistics.
 */
public class GameDetailsPanel extends JPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private SavGame SAV;
  private GameInfo GAME;
  private RaceRst RACE;
  private ShipList SHIP_MODELS;
  private Lexicon LEX;
  private String[] races;
  private String invalid;

  private JPanel pnlDetailsBg = new JPanel();
  private JTextPane txtDetails = new JTextPane();
  private JScrollPane jspDetails = new JScrollPane(txtDetails);

  /**
   * Constructor.
   * @throws IOException
   */
  public GameDetailsPanel(SavGame sav, GameInfo gameInfo, Stbof stbof) throws IOException {
    init(stbof);
    setupControls();
    setupLayout();
    load(sav, gameInfo);
  }

  private void init(Stbof stbof) throws IOException {
    RACE = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    SHIP_MODELS = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
    LEX = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);

    // pre-load the race names
    races = new String[6];
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      races[emp] = FunctionTools.logIfThrown(x -> RACE.getName(x), invalid, emp);
    races[NUM_EMPIRES] = LEX.getEntryOrDefault(LexMenuIdx.IntelScreen.DetailReport.MinorRace, "Minors"); //$NON-NLS-1$

    invalid = LEX.getEntryOrDefault(LexDataIdx.Shared.Invalid, "Invalid"); //$NON-NLS-1$
  }

  private void setupControls() {
    this.setOpaque(false);
    pnlDetailsBg.setBackground(new Color(0,0,0,100));
    Font defFont = UE.SETTINGS.getDefaultFont();
    txtDetails.setContentType("text/html");
    txtDetails.setOpaque(false);
    txtDetails.setEditable(false);
    txtDetails.setFont(defFont);

    jspDetails.getViewport().setOpaque(false);
    jspDetails.setOpaque(false);
    UIManager.put("ScrollBar.thumb", new ColorUIResource(new Color(0,0,0,0)));
    UIManager.put("ScrollBar.track", new ColorUIResource(new Color(0,0,0,0)));
    jspDetails.getVerticalScrollBar().setUI(new BasicScrollBarUI());
    jspDetails.getHorizontalScrollBar().setUI(new BasicScrollBarUI());
    jspDetails.getVerticalScrollBar().setOpaque(false);
    jspDetails.getHorizontalScrollBar().setOpaque(false);
    jspDetails.setPreferredSize(new Dimension(400, 200));
  }

  private void setupLayout() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0;
    c.weighty = 1.0;
    c.gridx = 0;
    c.gridy = 0;
    add(jspDetails, c);
    add(pnlDetailsBg, c);
  }

  public void load(SavGame sav, GameInfo gameInfo) {
    SAV = sav;
    GAME = gameInfo;

    try {
      SwingWorker<Boolean, Void> loadWorker = new SwingWorker<Boolean, Void>() {
        ArrayList<String[]> res = new ArrayList<String[]>();

        @Override
        protected Boolean doInBackground() throws Exception {
          loadGameStats(res);
          loadAlienStats(res);
          loadSectorStats(res);
          loadSystemStats(res);
          loadStellarStats(res);
          loadShipStats(res);
          loadStarbaseStats(res);
          loadFleetStats(res);
          loadStructureStats(res);
          loadTechStats(res);
          loadIntelStats(res);
          loadEventStats(res);
          return true;
        }

        @Override
        // update ui on published events
        protected void done() {
          updateStats(res);
        }
      };

      loadWorker.execute();
    }
    catch (Throwable e) {
      e.printStackTrace();
      return;
    }
  }

  private void updateStats(ArrayList<String[]> entries) {
    StringBuffer stats = new StringBuffer();
    stats.append("<div color='white' style='font-size:10px'>");
    stats.append("<table cellspacing='0' cellpadding='0'>");
    for (String[] entry : entries) {
      stats.append("<tr><td nowrap style='padding: -2 2; font-size:11px'><b>");
      stats.append(entry[0]);
      stats.append("</b></td><td nowrap style='padding: -8 2'><pre>");
      stats.append(entry[1]);
      stats.append("</pre></td></tr>");
    }
    stats.append("</table></div>");

    txtDetails.setText(stats.toString());
  }

  private void loadGameStats(ArrayList<String[]> res) throws IOException {
    GalInfo galInfo = (GalInfo) SAV.getInternalFile(CSavFiles.GalInfo, true);
    int systemsTotal = galInfo.getSystemCount();

    GameStats gameStats = GAME.getStats();

    float domination = 100.0f * gameStats.majorSystemsMax / systemsTotal;
    String dominationStr = String.format("%.2f", domination);
    res.add(new String[] {Language.getString("GameDetailsPanel.143"), pad(dominationStr) + "%"});
    res.add(new String[] {Language.getString("GameDetailsPanel.84"), padIntVal(gameStats.majorRaces)});
    res.add(new String[] {Language.getString("GameDetailsPanel.85"), padIntVal(gameStats.majorsAlive())});
    res.add(new String[] {Language.getString("GameDetailsPanel.86"), padIntVal(gameStats.majorsBeaten)});
    res.add(new String[] {Language.getString("GameDetailsPanel.87"), padIntVal(gameStats.minorRaces)});
    res.add(new String[] {Language.getString("GameDetailsPanel.88"), padIntVal(gameStats.minorsAlive())});
    res.add(new String[] {Language.getString("GameDetailsPanel.89"), padIntVal(gameStats.minorsBeaten)});
    res.add(new String[] {Language.getString("GameDetailsPanel.90"), padIntVal(gameStats.minorsMembered)});
    res.add(new String[] {Language.getString("GameDetailsPanel.91"), padIntVal(gameStats.minorsIndependent())});
    res.add(new String[] {Language.getString("GameDetailsPanel.92"), padIntVal(gameStats.minorsSpaceFaring)});
  }

  private void loadAlienStats(ArrayList<String[]> res) throws IOException {
    AlienInfo aliens = (AlienInfo) SAV.getInternalFile(CSavFiles.AlienInfo, true);
    AlienStats alienStats = aliens.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.93"), padIntVal(alienStats.racesTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.94"), padIntVal(alienStats.racesAlive)});

    for (int i = 0; i < alienStats.relationships.length; ++i) {
      String rsName = LexHelper.mapAlienRelationship(i);
      String lbl = Language.getString("GameDetailsPanel.95").replace("%", rsName);
      res.add(new String[] {lbl, padIntVal(alienStats.relationships[i])});
    }

    for (Entry<Integer, Integer> entry : alienStats.status.entrySet()) {
      String statusName = LexHelper.mapMinorRaceStatus(entry.getKey());
      String lbl = Language.getString("GameDetailsPanel.96").replace("%", statusName);
      res.add(new String[] {lbl, padIntVal(entry.getValue())});
    }

    Treaty treaties = (Treaty) SAV.getInternalFile(CSavFiles.Treaty, true);

    TreatyStats treatyStats = treaties.getStats();
    for (Entry<Integer, Integer> treatyType : treatyStats.treatyTypes.entrySet()) {
      String treatyName = LexHelper.mapTreatyType(treatyType.getKey());
      String lbl = Language.getString("GameDetailsPanel.97").replace("%", treatyName);
      res.add(new String[] {lbl, padIntVal(treatyType.getValue())});
    }
    for (Entry<Integer, Integer> peaceType : treatyStats.peaceTypes.entrySet()) {
      String peaceName = LexHelper.mapPeaceType(peaceType.getKey());
      String lbl = Language.getString("GameDetailsPanel.98").replace("%", peaceName);
      res.add(new String[] {lbl, padIntVal(peaceType.getValue())});
    }
  }

  private void loadSectorStats(ArrayList<String[]> res) throws IOException {
    Sector sectors = (Sector) SAV.getInternalFile(CSavFiles.SectorLst, true);
    SectorStats secStats = sectors.getSectorStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.0"), padIntVal(secStats.sectorsTotal())});
    res.add(new String[] {Language.getString("GameDetailsPanel.1"), padIntVal(secStats.sectorsHorizontal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.2"), padIntVal(secStats.sectorsVertical)});
    res.add(new String[] {Language.getString("GameDetailsPanel.3"), padIntVal(secStats.sectorsOwned)});

    for (int i = 0; i < secStats.sectorOwns.length; ++i)
      res.add(new String[] {Language.getString("GameDetailsPanel.4").replace("%", races[i]),
        padIntVal(secStats.sectorOwns[i])});

    res.add(new String[] {Language.getString("GameDetailsPanel.5"), padIntVal(secStats.sectorsClaimed)});

    for (int i = 0; i < secStats.sectorClaims.length; ++i)
      res.add(new String[] {Language.getString("GameDetailsPanel.6").replace("%", races[i]),
        padIntVal(secStats.sectorClaims[i])});

    res.add(new String[] {Language.getString("GameDetailsPanel.7"), padIntVal(secStats.sectorsDisputed)});
    res.add(new String[] {Language.getString("GameDetailsPanel.8"), padIntVal(secStats.sectorsUnclaimed())});
  }

  private void loadSystemStats(ArrayList<String[]> res) throws IOException {
    SystInfo systems = (SystInfo) SAV.getInternalFile(CSavFiles.SystInfo, true);
    SystemStats sysStats = systems.getSystemStats();

    // count & status
    res.add(new String[] {Language.getString("GameDetailsPanel.9"), padIntVal(sysStats.systemsTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.10"), padIntVal(sysStats.systemsOwned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.11"), padIntVal(sysStats.systemsInhabitable)});
    res.add(new String[] {Language.getString("GameDetailsPanel.12"), padIntVal(sysStats.systemsColonized)});
    res.add(new String[] {Language.getString("GameDetailsPanel.13"), padIntVal(sysStats.systemsSubjugated)});
    res.add(new String[] {Language.getString("GameDetailsPanel.14"), padIntVal(sysStats.systemsRebelled)});
    res.add(new String[] {Language.getString("GameDetailsPanel.122"), padIntVal(sysStats.systemsStarving)});
    res.add(new String[] {Language.getString("GameDetailsPanel.123"), padIntVal(sysStats.systemsIdle)});

    // star types
    for (Entry<Integer, Integer> starType : sysStats.starTypes.entrySet()) {
      String starName = LexHelper.mapStellarType(starType.getKey());
      String lbl = Language.getString("GameDetailsPanel.15").replace("%", starName);
      res.add(new String[] {lbl, padIntVal(starType.getValue())});
    }

    // planets
    res.add(new String[] {Language.getString("GameDetailsPanel.16"), padIntVal(sysStats.planetsTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.17"), padIntVal(sysStats.planetsOwned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.18"), padIntVal(sysStats.planetsInhabitable)});
    res.add(new String[] {Language.getString("GameDetailsPanel.19"), padIntVal(sysStats.planetsTerraformed)});
    res.add(new String[] {Language.getString("GameDetailsPanel.20"), padIntVal(sysStats.planetsColonized)});
    res.add(new String[] {Language.getString("GameDetailsPanel.21"), padIntVal(sysStats.planetsLarge)});
    res.add(new String[] {Language.getString("GameDetailsPanel.22"), padIntVal(sysStats.planetsMedium)});
    res.add(new String[] {Language.getString("GameDetailsPanel.23"), padIntVal(sysStats.planetsSmall)});

    // planet types
    for (Entry<Integer, PlanetStats> entry : sysStats.planets.entrySet()) {
      PlanetStats planetStats = entry.getValue();
      char planetCategory = CStbof.PlanetCategory.length > planetStats.planetType
        ? CStbof.PlanetCategory[planetStats.planetType] : '?';
      String planetTypeName = LexHelper.mapPlanetType(planetStats.planetType)
        + " (" + planetCategory + ")";
      String lbl = Language.getString("GameDetailsPanel.24").replace("%", planetTypeName);
      String values = padIntVal(planetStats.size[0] + planetStats.size[1] + planetStats.size[2]) + " ["
        + padIntVal(planetStats.size[0], 3) + "|"
        + padIntVal(planetStats.size[1], 3) + "|"
        + padIntVal(planetStats.size[2], 3) + "]";
      res.add(new String[] {lbl, values});
    }

    // resources & bonuses
    res.add(new String[] {Language.getString("GameDetailsPanel.28"), padIntVal(sysStats.dilithiumTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.29"), padIntVal(sysStats.dilithiumOwned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.30"), padIntVal(sysStats.dilithiumProduction)});

    for (int i = 0; i < sysStats.foodBonus.length; ++i) {
      // 0, 0+, 0.1+, 0.2+, 0.3+, 0.4+, 0.5+
      String amount = i == 0 ? "0%" : Integer.toString((i-1) * 10) + "%+";
      String lbl = Language.getString("GameDetailsPanel.31").replace("%", amount);
      res.add(new String[] {lbl, padIntVal(sysStats.foodBonus[i])});
    }

    for (int i = 0; i < sysStats.energyBonus.length; ++i) {
      // 0, 0+, 0.25+, 0.5+, 0.75+, 1.0+
      String amount = i == 0 ? "0%" : Integer.toString((i-1) * 25) + "%+";
      String lbl = Language.getString("GameDetailsPanel.32").replace("%", amount);
      res.add(new String[] {lbl, padIntVal(sysStats.energyBonus[i])});
    }

    // population
    res.add(new String[] {Language.getString("GameDetailsPanel.33"), padIntVal(sysStats.populationMax)});
    res.add(new String[] {Language.getString("GameDetailsPanel.34"), padIntVal(sysStats.populationTerraformed)});
    res.add(new String[] {Language.getString("GameDetailsPanel.35"), padIntVal(sysStats.populationTotal)});

    // production (raw = cumulated edifice.bst output, total = with bonuses)
    res.add(new String[] {Language.getString("GameDetailsPanel.36"), padIntVal(sysStats.foodProductionTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.37"), padIntVal(sysStats.foodProductionMaxRaw)});
    res.add(new String[] {Language.getString("GameDetailsPanel.38"), padIntVal(sysStats.foodProductionAssigned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.39"), padIntVal(sysStats.foodProductionExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.40"), padIntVal(sysStats.foodProductionBonus)});
    res.add(new String[] {Language.getString("GameDetailsPanel.41"), padIntVal(sysStats.foodConsumption)});
    res.add(new String[] {Language.getString("GameDetailsPanel.42"), padIntVal(sysStats.foodExcess)});
    res.add(new String[] {Language.getString("GameDetailsPanel.43"), padIntVal(sysStats.industryProductionTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.44"), padIntVal(sysStats.industryProductionMaxRaw)});
    res.add(new String[] {Language.getString("GameDetailsPanel.45"), padIntVal(sysStats.industryProductionAssigned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.46"), padIntVal(sysStats.industryProductionExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.47"), padIntVal(sysStats.industryProductionBonus)});
    res.add(new String[] {Language.getString("GameDetailsPanel.48"), padIntVal(sysStats.industryInvested)});
    res.add(new String[] {Language.getString("GameDetailsPanel.49"), padIntVal(sysStats.industryUsage)});
    res.add(new String[] {Language.getString("GameDetailsPanel.50"), padIntVal(sysStats.industryExcess())});
    res.add(new String[] {Language.getString("GameDetailsPanel.51"), padIntVal(sysStats.energyProductionTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.52"), padIntVal(sysStats.energyProductionMaxRaw)});
    res.add(new String[] {Language.getString("GameDetailsPanel.53"), padIntVal(sysStats.energyProductionAssigned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.54"), padIntVal(sysStats.energyProductionExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.55"), padIntVal(sysStats.energyProductionBonus)});
    res.add(new String[] {Language.getString("GameDetailsPanel.56"), padIntVal(sysStats.energyConsumption)});
    res.add(new String[] {Language.getString("GameDetailsPanel.57"), padIntVal(sysStats.energyExcess)});
    res.add(new String[] {Language.getString("GameDetailsPanel.58"), padIntVal(sysStats.intelTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.59"), padIntVal(sysStats.intelMaxRaw)});
    res.add(new String[] {Language.getString("GameDetailsPanel.60"), padIntVal(sysStats.intelAssigned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.61"), padIntVal(sysStats.intelExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.62"), padIntVal(sysStats.intelBonus)});
    res.add(new String[] {Language.getString("GameDetailsPanel.63"), padIntVal(sysStats.researchTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.64"), padIntVal(sysStats.researchMaxRaw)});
    res.add(new String[] {Language.getString("GameDetailsPanel.65"), padIntVal(sysStats.researchAssigned)});
    res.add(new String[] {Language.getString("GameDetailsPanel.66"), padIntVal(sysStats.researchExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.67"), padIntVal(sysStats.researchBonus)});
    res.add(new String[] {Language.getString("GameDetailsPanel.68"), padIntVal(sysStats.incomeTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.69"), padIntVal(sysStats.incomeRaw)});
    res.add(new String[] {Language.getString("GameDetailsPanel.70"), padIntVal(sysStats.incomeExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.71"), padIntVal(sysStats.incomeBonus)});
    res.add(new String[] {Language.getString("GameDetailsPanel.72"), padIntVal(sysStats.incomeRemnant)});
    res.add(new String[] {Language.getString("GameDetailsPanel.73"), padIntVal(sysStats.shipBuildExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.74"), padIntVal(sysStats.shipsInBuild)});

    // trade routes
    res.add(new String[] {Language.getString("GameDetailsPanel.75"), padIntVal(sysStats.tradeRoutesIncoming)});
    res.add(new String[] {Language.getString("GameDetailsPanel.76"), padIntVal(sysStats.tradeRoutesLocal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.77"), padIntVal(sysStats.tradeRoutesExtra)});
    res.add(new String[] {Language.getString("GameDetailsPanel.78"), padIntVal(sysStats.averageMorale())});

    for (int i = 0; i < sysStats.moraleLvls.length; ++i) {
      int moraleLvl = i-4; // effectively final variable for the lambda call
      String moraleName = LexHelper.mapMoraleLvl(moraleLvl);
      res.add(new String[] {Language.getString("GameDetailsPanel.79").replace("%", moraleName), padIntVal(sysStats.moraleLvls[i])});
    }
  }

  private void loadStellarStats(ArrayList<String[]> res) throws IOException {
    StellInfo stellar = (StellInfo) SAV.getInternalFile(CSavFiles.StellInfo, true);
    StellarStats stellStats = stellar.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.80"), padIntVal(stellStats.stellarObjectsTotal)});

    for (Entry<Integer, Integer> stellType : stellStats.stellarTypes.entrySet()) {
      String stellName = LexHelper.mapStellarType(stellType.getKey());
      String lbl = Language.getString("GameDetailsPanel.81").replace("%", stellName);
      res.add(new String[] {lbl, padIntVal(stellType.getValue())});
    }

    res.add(new String[] {Language.getString("GameDetailsPanel.82"), padIntVal(stellStats.wormholesStable)});
    res.add(new String[] {Language.getString("GameDetailsPanel.83"), padIntVal(stellStats.wormholesInstable)});
  }

  private void loadShipStats(ArrayList<String[]> res) throws IOException {
    GShipList shipList = (GShipList) SAV.getInternalFile(CSavFiles.GShipList, true);
    ShipStats shipStats = shipList.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.99"), padIntVal(shipStats.shipsTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.100"), padIntVal(shipStats.shipsCloaked)});

    for (Entry<Short, Integer> shipCat : shipStats.shipCategories.entrySet()) {
      if (shipCat.getKey() != ShipRole.Monster) {
        String catName = LexHelper.mapShipRole(shipCat.getKey());
        String lbl = Language.getString("GameDetailsPanel.101").replace("%", catName);
        res.add(new String[] {lbl, padIntVal(shipCat.getValue())});
      }
    }

    // 'Manifestation' has been translated to 'Ausdruck' = 'expression' in german, which is complete non-sense
    // given there is no other matching description in the lexicon.dic, handle the monsters separately
    res.add(new String[] {Language.getString("GameDetailsPanel.102"), padIntVal(shipStats.monsterTypes.size())});
    // further list the different monster types
    for (Entry<Integer, Integer> monsterType : shipStats.monsterTypes.entrySet()) {
      int shipType = monsterType.getKey();
      ShipDefinition shipDef = SHIP_MODELS.getShipDefinition(shipType);

      String name = shipDef.getName();
      String className = shipDef.getShipClass();
      if (!StringTools.isNullOrEmpty(className))
        name = name + " / " + className;

      String lbl = Language.getString("GameDetailsPanel.103").replace("%", name);
      res.add(new String[] {lbl, padIntVal(monsterType.getValue())});
    }

    for (Entry<Integer, Integer> range : shipStats.range.entrySet()) {
      String lbl = Language.getString("GameDetailsPanel.104").replace("%", Integer.toString(range.getKey()));
      res.add(new String[] {lbl, padIntVal(range.getValue())});
    }
    for (Entry<Integer, Integer> speed : shipStats.speed.entrySet()) {
      String lbl = Language.getString("GameDetailsPanel.105").replace("%", Integer.toString(speed.getKey()));
      res.add(new String[] {lbl, padIntVal(speed.getValue())});
    }
    for (Entry<Integer, Integer> scanRange : shipStats.scanRange.entrySet()) {
      String lbl = Language.getString("GameDetailsPanel.106").replace("%", Integer.toString(scanRange.getKey()));
      res.add(new String[] {lbl, padIntVal(scanRange.getValue())});
    }
    for (Entry<Integer, Integer> experience : shipStats.experienceLvl.entrySet()) {
      String expName = LexHelper.mapCrewExperience(experience.getKey());
      String lbl = Language.getString("GameDetailsPanel.121").replace("%", expName);
      res.add(new String[] {lbl, padIntVal(experience.getValue())});
    }
  }

  private void loadStarbaseStats(ArrayList<String[]> res) throws IOException {
    StarBaseInfo sbInfo = (StarBaseInfo) SAV.getInternalFile(CSavFiles.StarBaseInfo, true);
    StarbaseStats sbStats = sbInfo.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.107"), padIntVal(sbStats.starbasesTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.108"), padIntVal(sbStats.starbasesInBuild)});
    res.add(new String[] {Language.getString("GameDetailsPanel.109"), padIntVal(sbStats.starbasesDamaged)});
    res.add(new String[] {Language.getString("GameDetailsPanel.110"), padIntVal(sbStats.outpostsTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.111"), padIntVal(sbStats.outpostsInBuild)});
    res.add(new String[] {Language.getString("GameDetailsPanel.112"), padIntVal(sbStats.outpostsDamaged)});
  }

  private void loadFleetStats(ArrayList<String[]> res) throws IOException {
    TaskForceList tfList = (TaskForceList) SAV.getInternalFile(CSavFiles.GWTForce, true);
    TaskForceStats tfStats = tfList.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.113"), padIntVal(tfStats.taskForcesTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.114"), padIntVal(tfStats.taskForcesCloaked)});

    for (Entry<Integer, Integer> range : tfStats.range.entrySet()) {
      String lbl = Language.getString("GameDetailsPanel.115").replace("%", Integer.toString(range.getKey()));
      res.add(new String[] {lbl, padIntVal(range.getValue())});
    }
    for (Entry<Integer, Integer> speed : tfStats.speed.entrySet()) {
      String lbl = Language.getString("GameDetailsPanel.116").replace("%", Integer.toString(speed.getKey()));
      res.add(new String[] {lbl, padIntVal(speed.getValue())});
    }
    for (Entry<Integer, Integer> scanRange : tfStats.scanRange.entrySet()) {
      String lbl = Language.getString("GameDetailsPanel.117").replace("%", Integer.toString(scanRange.getKey()));
      res.add(new String[] {lbl, padIntVal(scanRange.getValue())});
    }

    for (Entry<Integer, Integer> mission : tfStats.missions.entrySet()) {
      String missionName = LexHelper.mapTFMission(mission.getKey());
      if (mission.getKey() == Mission.Intercept)
        missionName = missionName.replace("%d %% ", "");

      String lbl = Language.getString("GameDetailsPanel.118").replace("%", missionName);
      res.add(new String[] {lbl, padIntVal(mission.getValue())});
    }
    res.add(new String[] {Language.getString("GameDetailsPanel.119"), padIntVal(tfStats.systemsAttacked)});
  }

  private void loadStructureStats(ArrayList<String[]> res) throws IOException {
    StrcInfo strcInfo = (StrcInfo) SAV.getInternalFile(CSavFiles.StrcInfo, true);
    StrcStats strcStats = strcInfo.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.124"), padIntVal(strcStats.strcTotal)});
    res.add(new String[] {Language.getString("GameDetailsPanel.125"), padIntVal(strcStats.powered)});
    res.add(new String[] {Language.getString("GameDetailsPanel.126"), padIntVal(strcStats.strcMainFood)});
    res.add(new String[] {Language.getString("GameDetailsPanel.127"), padIntVal(strcStats.strcMainIndustry)});
    res.add(new String[] {Language.getString("GameDetailsPanel.128"), padIntVal(strcStats.strcMainEnergy)});
    res.add(new String[] {Language.getString("GameDetailsPanel.129"), padIntVal(strcStats.strcMainIntel)});
    res.add(new String[] {Language.getString("GameDetailsPanel.130"), padIntVal(strcStats.strcMainResearch)});
    res.add(new String[] {Language.getString("GameDetailsPanel.131"), padIntVal(strcStats.strcScanners)});
    res.add(new String[] {Language.getString("GameDetailsPanel.132"), padIntVal(strcStats.strcOther)});

    for (Entry<Short, StrcStatEntry> entry : strcStats.strcTypes.entrySet()) {
      StrcStatEntry strc = entry.getValue();
      String lbl = Language.getString("GameDetailsPanel.133")
        .replace("%1", Integer.toString(strc.race))
        .replace("%2", strc.name);
      String value = padIntVal(strc.count) + (strc.powered > 0 ? " (" + strc.powered + ")" : "");
      res.add(new String[] {lbl, value});
    }
  }

  private void loadTechStats(ArrayList<String[]> res) throws IOException {
    TechInfo techInfo = (TechInfo) SAV.getInternalFile(CSavFiles.TechInfo, true);
    TechStats techStats = techInfo.getStats();

    for (int i = 0; i < techStats.majorTechLevels.length; ++i) {
      String techField = LexHelper.mapTechField(i);
      String level = techStats.majorTechLevels[i] == null ? pad("-")
        : padIntVal(techStats.majorTechLevels[i].min)
        + " /" + padIntVal(techStats.majorTechLevels[i].max, 3)
        + " /" + padIntVal(techStats.majorTechLevels[i].average, 3);
      String lbl = Language.getString("GameDetailsPanel.141").replace("%", techField);
      res.add(new String[] {lbl, level});
    }
    for (int i = 0; i < techStats.minorTechLevels.length; ++i) {
      String tech = LexHelper.mapTechField(i);
      String level = techStats.minorTechLevels[i] == null ? pad("-")
        : padIntVal(techStats.minorTechLevels[i].min)
        + " /" + padIntVal(techStats.minorTechLevels[i].max, 3)
        + " /" + padIntVal(techStats.minorTechLevels[i].average, 3);
      String lbl = Language.getString("GameDetailsPanel.142").replace("%", tech);
      res.add(new String[] {lbl, level});
    }
  }

  private void loadIntelStats(ArrayList<String[]> res) throws IOException {
    IntelInfo intelInfo = (IntelInfo) SAV.getInternalFile(CSavFiles.IntelInfo, true);
    IntelStats intelStats = intelInfo.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.134"), padIntVal(intelStats.security)});
    res.add(new String[] {Language.getString("GameDetailsPanel.135"), padIntVal(intelStats.espionage)});
    res.add(new String[] {Language.getString("GameDetailsPanel.136"), padIntVal(intelStats.sabotage)});
    res.add(new String[] {Language.getString("GameDetailsPanel.137"), padIntVal(intelStats.averageSecurity)});
    res.add(new String[] {Language.getString("GameDetailsPanel.138"), padIntVal(intelStats.averageEspionage)});
    res.add(new String[] {Language.getString("GameDetailsPanel.139"), padIntVal(intelStats.averageSabotage)});
    res.add(new String[] {Language.getString("GameDetailsPanel.140"), padIntVal(intelStats.reportCount)});
  }

  private void loadEventStats(ArrayList<String[]> res) throws IOException {
    ResultList resultList = (ResultList) SAV.getInternalFile(CSavFiles.ResultLst, true);
    ResultStats resultStats = resultList.getStats();

    res.add(new String[] {Language.getString("GameDetailsPanel.120"), padIntVal(resultStats.resultsTotal)});
  }

  private String pad(String val) {
    return pad(val, 6);
  }

  private String pad(String val, int len) {
    return StringTools.padLeft(val, len);
  }

  private String padIntVal(int val) {
    return padIntVal(val, 6);
  }

  private String padIntVal(int val, int len) {
    return pad(Integer.toString(val), len);
  }
}
