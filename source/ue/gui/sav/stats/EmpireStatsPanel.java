package ue.gui.sav.stats;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.basic.BasicScrollBarUI;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.dic.idx.LexMenuIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.GameInfo.EmpireGameStats;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.AlienInfo.EmpireAlienStats;
import ue.edit.sav.files.emp.EmpsInfo;
import ue.edit.sav.files.emp.EmpsInfo.EmpireStats;
import ue.edit.sav.files.emp.IntelInfo;
import ue.edit.sav.files.emp.IntelInfo.EmpireIntelStats;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.emp.ResultList.ResultStats;
import ue.edit.sav.files.emp.StrcInfo;
import ue.edit.sav.files.emp.StrcInfo.StrcStatEntry;
import ue.edit.sav.files.emp.StrcInfo.StrcStats;
import ue.edit.sav.files.emp.TechInfo;
import ue.edit.sav.files.emp.TechInfo.EmpireTechStats;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.GShipList.EmpireShipStats;
import ue.edit.sav.files.map.GalInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.Sector.EmpireSectorStats;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.StarBaseInfo.StarbaseStats;
import ue.edit.sav.files.map.SystInfo.EmpireSystemStats;
import ue.edit.sav.files.map.SystInfo.PlanetStats;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.TaskForceList.TaskForceStats;
import ue.service.Language;
import ue.util.data.StringTools;
import ue.util.func.FunctionTools;

/**
 * Used by the GameInfoGUI to display empire statistics.
 */
public class EmpireStatsPanel extends JPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private SavGame SAV;
  private GameInfo GAME;
  private RaceRst RACE;
  private Lexicon LEX;
  private String[] races;
  private String invalid;
  private int[][] empireSystemIds;

  private JPanel pnlStatsBg = new JPanel();
  private JTextPane txtStats = new JTextPane();
  private JScrollPane jspStats = new JScrollPane(txtStats);

  /**
   * Constructor.
   * @throws IOException
   */
  public EmpireStatsPanel(SavGame sav, GameInfo gameInfo, Stbof stbof) throws IOException {
    init(stbof);
    setupControls();
    setupLayout();
    load(sav, gameInfo);
  }

  private void init(Stbof stbof) throws IOException {
    RACE = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    LEX = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);

    // pre-load the race names
    races = new String[6];
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      races[emp] = FunctionTools.logIfThrown(x -> RACE.getName(x), invalid, emp);
    races[NUM_EMPIRES] = LEX.getEntryOrDefault(LexMenuIdx.IntelScreen.DetailReport.MinorRace, "Minors"); //$NON-NLS-1$

    invalid = LEX.getEntryOrDefault(LexDataIdx.Shared.Invalid, "Invalid"); //$NON-NLS-1$
  }

  private void setupControls() {
    this.setOpaque(false);
    pnlStatsBg.setBackground(new Color(0,0,0,100));
    Font defFont = UE.SETTINGS.getDefaultFont();
    txtStats.setContentType("text/html");
    txtStats.setOpaque(false);
    txtStats.setEditable(false);
    txtStats.setFont(defFont);
    txtStats.setPreferredSize(new Dimension(600, 200));

    jspStats.getViewport().setOpaque(false);
    jspStats.setOpaque(false);
    UIManager.put("ScrollBar.thumb", new ColorUIResource(new Color(0,0,0,0)));
    UIManager.put("ScrollBar.track", new ColorUIResource(new Color(0,0,0,0)));
    jspStats.getVerticalScrollBar().setUI(new BasicScrollBarUI());
    jspStats.getHorizontalScrollBar().setUI(new BasicScrollBarUI());
    jspStats.getVerticalScrollBar().setOpaque(false);
    jspStats.getHorizontalScrollBar().setOpaque(false);
    jspStats.setPreferredSize(new Dimension(400, 200));
  }

  private void setupLayout() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0;
    c.weighty = 1.0;
    c.gridx = 0;
    c.gridy = 0;
    add(jspStats, c);
    add(pnlStatsBg, c);
  }

  public void load(SavGame sav, GameInfo gameInfo) {
    SAV = sav;
    GAME = gameInfo;

    try {
      SwingWorker<Boolean, Void> loadWorker = new SwingWorker<Boolean, Void>() {
        ArrayList<String[]> res = new ArrayList<String[]>();

        @Override
        protected Boolean doInBackground() throws Exception {
          loadEmpireStats(res);
          loadGameStats(res);
          loadAlienStats(res);
          loadSectorStats(res);
          loadSystemStats(res);
          loadShipStats(res);
          loadStarbaseStats(res);
          loadFleetStats(res);
          loadStructureStats(res);
          loadTechStats(res);
          loadIntelStats(res);
          loadEventStats(res);
          return true;
        }

        @Override
        // update ui on published events
        protected void done() {
          updateStats(res);
        }
      };

      loadWorker.execute();
    }
    catch (Throwable e) {
      e.printStackTrace();
      return;
    }
  }

  private void updateStats(ArrayList<String[]> entries) {
    StringBuffer stats = new StringBuffer();
    stats.append("<html><head><style>"
      + "table{"
      + " cellspacing: 0; cellpadding: 0;}"
      + "th, td{"
      + " white-space: nowrap;"
      + " margin: -2 0 0 -2;"
      + " border-right: 1px solid #A0A0A0;"
      + " border-bottom: 1px solid #A0A0A0;}"
      + "</style></head>");
    stats.append("<div color='white' style='font-size:10px'>");
    stats.append("<table>");

    // header
    stats.append("<tr><th></th>");
    for (String race : races ) {
      stats.append("<th nowrap style='padding: 0 4'>" + race + "</th>");
    }
    stats.append("</tr>");

    // values
    for (String[] entry : entries) {
      stats.append("<tr><td style='padding: 0 2; font-size:11px'><b>");
      stats.append(entry[0]);
      stats.append("</b></td>");
      for (int i = 1; i < entry.length; ++i) {
        stats.append("<td style='padding: 0 2; text-align: right'>");
        stats.append(entry[i]);
        stats.append("</td>");
      }
      stats.append("</tr>");
    }
    stats.append("</table></div>");

    txtStats.setText(stats.toString());
  }

  private void loadEmpireStats(ArrayList<String[]> res) throws IOException {
    EmpsInfo empsInfo = (EmpsInfo) SAV.getInternalFile(CSavFiles.EmpsInfo, true);
    EmpireStats[] empsStats = empsInfo.getStats();

    // collect relationship types for indexing
    HashSet<Integer> relationships = new HashSet<Integer>();
    for (EmpireStats empStats : empsStats) {
      relationships.addAll(empStats.relationships.keySet());
    }

    final int pos = res.size();
    final int entryCount = 11 + relationships.size();
    res.ensureCapacity(pos + entryCount);

    // add headers
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.0"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.1"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.2"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.3"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.4"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.5"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.6"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.7"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.8"));
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.9"));
    for (int relationship : relationships) {
      String rsName = LexHelper.mapRelationship(relationship);
      addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.10").replace("%", rsName));
    }
    addEntry(res, empsStats.length, Language.getString("EmpireStatsPanel.11"));

    // add values
    for (int i = 0; i < empsStats.length; ++i) {
      EmpireStats empStats = empsStats[i];
      res.get(pos+0)[1+i] = empStats.empireName;
      res.get(pos+1)[1+i] = empStats.emperor;
      res.get(pos+2)[1+i] = padIntVal(empStats.credits);
      res.get(pos+3)[1+i] = padIntVal(empStats.income);
      res.get(pos+4)[1+i] = padIntVal(empStats.dilithium);
      res.get(pos+5)[1+i] = padIntVal(empStats.supportCost);
      res.get(pos+6)[1+i] = padIntVal(empStats.populationSupport);
      res.get(pos+7)[1+i] = padIntVal(empStats.empireMorale);
      res.get(pos+8)[1+i] = padIntVal(empStats.combatBonus);
      res.get(pos+9)[1+i] = padIntVal(empStats.shipsInBuild);
      // relationships
      int j = 0;
      for (int relationship : relationships) {
        int rsCount = empStats.relationships.getOrDefault(relationship, 0);
        res.get(pos+10+j++)[1+i] = padIntVal(rsCount);
      }
      res.get(pos+10+j)[1+i] = padIntVal(empStats.knownRaces);
    }
  }

  private void loadGameStats(ArrayList<String[]> res) throws IOException {
    GalInfo galInfo = (GalInfo) SAV.getInternalFile(CSavFiles.GalInfo, true);
    int systemsTotal = galInfo.getSystemCount();

    EmpireGameStats[] gameStats = GAME.getEmpireStats();

    final int pos = res.size();
    final int entryCount = 9;
    res.ensureCapacity(pos + entryCount);

    // add headers
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.143"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.12"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.13"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.14"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.15"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.16"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.17"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.18"));
    addEntry(res, gameStats.length, Language.getString("EmpireStatsPanel.19"));

    // add values
    for (int i = 0; i < gameStats.length; ++i) {
      EmpireGameStats empStats = gameStats[i];
      int r = 0;
      float domination = 100.0f * empStats.systems / systemsTotal;
      String dominationStr = String.format("%.2f", domination) + "%";
      res.get(pos+r++)[1+i] = pad(dominationStr);
      res.get(pos+r++)[1+i] = padIntVal(empStats.score);
      res.get(pos+r++)[1+i] = padIntVal(empStats.intel);
      res.get(pos+r++)[1+i] = padIntVal(empStats.military);
      res.get(pos+r++)[1+i] = padIntVal(empStats.economy);
      res.get(pos+r++)[1+i] = padIntVal(empStats.research);
      res.get(pos+r++)[1+i] = padIntVal(empStats.minors);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systems);
      res.get(pos+r++)[1+i] = padIntVal(empStats.dilithium);
    }
  }

  private void loadAlienStats(ArrayList<String[]> res) throws IOException {
    AlienInfo aliens = (AlienInfo) SAV.getInternalFile(CSavFiles.AlienInfo, true);
    EmpireAlienStats[] alienStats = aliens.getEmpireStats();

    final int pos = res.size();
    final int entryCount = NUM_EMPIRES + 3;
    res.ensureCapacity(pos + entryCount);

    // add headers
    for (int rs = 0; rs < NUM_EMPIRES; ++rs) {
      String rsName = LexHelper.mapAlienRelationship(rs);
      addEntry(res, alienStats.length, Language.getString("EmpireStatsPanel.20").replace("%", rsName));
    }
    addEntry(res, alienStats.length, Language.getString("EmpireStatsPanel.21"));
    addEntry(res, alienStats.length, Language.getString("EmpireStatsPanel.22"));
    addEntry(res, alienStats.length, Language.getString("EmpireStatsPanel.23"));

    // add values
    for (int i = 0; i < alienStats.length; ++i) {
      EmpireAlienStats empStats = alienStats[i];

      int j = 0;
      for (int rs = 0; rs < NUM_EMPIRES; ++rs) {
        int rsCount = empStats.relationships[rs];
        res.get(pos+j++)[1+i] = padIntVal(rsCount);
      }
      res.get(pos+j++)[1+i] = padIntVal(empStats.membered);
      res.get(pos+j++)[1+i] = padIntVal(empStats.subjugated);
      res.get(pos+j++)[1+i] = padIntVal(empStats.met);
    }
  }

  private void loadSectorStats(ArrayList<String[]> res) throws IOException {
    Sector sectors = (Sector) SAV.getInternalFile(CSavFiles.SectorLst, true);
    EmpireSectorStats[] sectorStats = sectors.getEmpireStats();

    final int pos = res.size();
    final int entryCount = 8;
    res.ensureCapacity(pos + entryCount);

    // add headers
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.24"));
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.25"));
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.26"));
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.27"));
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.28"));
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.29"));
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.30"));
    addEntry(res, sectorStats.length, Language.getString("EmpireStatsPanel.31"));

    // add values
    for (int i = 0; i < sectorStats.length; ++i) {
      EmpireSectorStats empStats = sectorStats[i];
      res.get(pos+0)[1+i] = padIntVal(empStats.explored);
      res.get(pos+1)[1+i] = padIntVal(empStats.revealed);
      res.get(pos+2)[1+i] = padIntVal(empStats.sectorsOwned);
      res.get(pos+3)[1+i] = padIntVal(empStats.sectorsClaimed);
      res.get(pos+4)[1+i] = padIntVal(empStats.sectorsDisputed);
      res.get(pos+5)[1+i] = padIntVal(empStats.systems);
      res.get(pos+6)[1+i] = padIntVal(empStats.stellarobjects);
      res.get(pos+7)[1+i] = padIntVal(empStats.starbases);
    }
  }

  private void loadSystemStats(ArrayList<String[]> res) throws IOException {
    SystInfo systems = (SystInfo) SAV.getInternalFile(CSavFiles.SystInfo, true);
    EmpireSystemStats[] sysStats = systems.getEmpireStats();
    empireSystemIds = Arrays.stream(sysStats).map(x -> x.systemIds).toArray(int[][]::new);

    // collect types for indexing
    HashSet<Integer> starTypes = new HashSet<Integer>();
    HashSet<Integer> planetTypes = new HashSet<Integer>();
    for (EmpireSystemStats empStats : sysStats) {
      starTypes.addAll(empStats.starTypes.keySet());
      planetTypes.addAll(empStats.planets.keySet());
    }

    final int pos = res.size();
    final int entryCount = 63 + starTypes.size() + planetTypes.size()
      + sysStats[0].foodBonus.length + sysStats[0].energyBonus.length
      + sysStats[0].moraleLvls.length;
    res.ensureCapacity(pos + entryCount);

    // add headers
    // count & status
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.32"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.33"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.34"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.35"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.36"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.118"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.119"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.120"));

    // star types
    for (Integer starType : starTypes) {
      String starName = LexHelper.mapStellarType(starType);
      addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.37").replace("%", starName));
    }

    // planets
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.38"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.39"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.40"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.41"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.42"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.43"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.44"));

    // planet types
    for (Integer planetType : planetTypes) {
      char planetCategory = CStbof.PlanetCategory.length > planetType
        ? CStbof.PlanetCategory[planetType] : '?';
      String planetTypeName = LexHelper.mapPlanetType(planetType)
        + " (" + planetCategory + ")";
      addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.45").replace("%", planetTypeName));
    }

    // resources & bonuses
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.46"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.47"));
    for (int i = 0; i < sysStats[0].foodBonus.length; ++i) {
      // 0, 0+, 0.1+, 0.2+, 0.3+, 0.4+, 0.5+
      String amount = i == 0 ? "0%" : padIntVal((i-1) * 10) + "%+";
      addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.48").replace("%", amount));
    }
    for (int i = 0; i < sysStats[0].energyBonus.length; ++i) {
      // 0, 0+, 0.25+, 0.5+, 0.75+, 1.0+
      String amount = i == 0 ? "0%" : padIntVal((i-1) * 25) + "%+";
      addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.49").replace("%", amount));
    }

    // population
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.50"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.51"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.52"));

    // production (raw = cumulated edifice.bst output, total = with bonuses)
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.53"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.54"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.55"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.56"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.57"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.58"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.59"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.60"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.61"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.62"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.63"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.64"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.65"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.66"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.67"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.68"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.69"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.70"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.71"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.72"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.73"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.74"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.75"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.76"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.77"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.78"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.79"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.80"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.81"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.82"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.83"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.84"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.85"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.86"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.87"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.88"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.89"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.90"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.91"));

    // trade routes
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.92"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.93"));
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.94"));

    // morale
    addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.95"));
    for (int i = 0; i < sysStats[0].moraleLvls.length; ++i) {
      int moraleLvl = i-4; // effectively final variable for the lambda call
      String moraleName = LexHelper.mapMoraleLvl(moraleLvl);
      addEntry(res, sysStats.length, Language.getString("EmpireStatsPanel.96").replace("%", moraleName));
    }

    // add values
    for (int i = 0; i < sysStats.length; ++i) {
      EmpireSystemStats empStats = sysStats[i];

      // count & status
      int r = 0;
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsOwned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsInhabitable);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsColonized);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsSubjugated);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsRebelled);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsStarving);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsIdle);
      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsLost);

      // star types
      for (int starType : starTypes) {
        int stCount = empStats.starTypes.getOrDefault(starType, 0);
        res.get(pos+r++)[1+i] = padIntVal(stCount);
      }

      // planets
      res.get(pos+r++)[1+i] = padIntVal(empStats.planetsOwned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.planetsInhabitable);
      res.get(pos+r++)[1+i] = padIntVal(empStats.planetsTerraformed);
      res.get(pos+r++)[1+i] = padIntVal(empStats.planetsColonized);
      res.get(pos+r++)[1+i] = padIntVal(empStats.planetsLarge);
      res.get(pos+r++)[1+i] = padIntVal(empStats.planetsMedium);
      res.get(pos+r++)[1+i] = padIntVal(empStats.planetsSmall);

      // planet types
      for (int planetType : planetTypes) {
        PlanetStats planetStats = empStats.planets.get(planetType);
        String values =
            planetStats == null ? "0 [" + pad("0|", 4) + pad("0|", 4) + pad("0", 3) + "]"
                : padIntVal(planetStats.size[0] + planetStats.size[1] + planetStats.size[2]) + " ["
                    + padIntVal(planetStats.size[0], 3) + "|"
                    + padIntVal(planetStats.size[1], 3) + "|"
                    + padIntVal(planetStats.size[2], 3) + "]";
        res.get(pos+r++)[1+i] = values;
      }

      // resources & bonuses
      res.get(pos+r++)[1+i] = padIntVal(empStats.dilithiumOwned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.dilithiumProduction);
      for (int b = 0; b < empStats.foodBonus.length; ++b)
        res.get(pos+r++)[1+i] = padIntVal(empStats.foodBonus[b]);
      for (int b = 0; b < empStats.energyBonus.length; ++b)
        res.get(pos+r++)[1+i] = padIntVal(empStats.energyBonus[b]);

      // population
      res.get(pos+r++)[1+i] = padIntVal(empStats.populationMax);
      res.get(pos+r++)[1+i] = padIntVal(empStats.populationTerraformed);
      res.get(pos+r++)[1+i] = padIntVal(empStats.populationTotal);

      // production (raw = cumulated edifice.bst output, total = with bonuses)
      res.get(pos+r++)[1+i] = padIntVal(empStats.foodProductionTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.foodProductionMaxRaw);
      res.get(pos+r++)[1+i] = padIntVal(empStats.foodProductionAssigned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.foodProductionExtra);
      res.get(pos+r++)[1+i] = padIntVal(empStats.foodProductionBonus);
      res.get(pos+r++)[1+i] = padIntVal(empStats.foodConsumption);
      res.get(pos+r++)[1+i] = padIntVal(empStats.foodExcess);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryProductionTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryProductionMaxRaw);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryProductionAssigned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryProductionExtra);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryProductionBonus);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryInvested);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryUsage);
      res.get(pos+r++)[1+i] = padIntVal(empStats.industryExcess());
      res.get(pos+r++)[1+i] = padIntVal(empStats.energyProductionTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.energyProductionMaxRaw);
      res.get(pos+r++)[1+i] = padIntVal(empStats.energyProductionAssigned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.energyProductionExtra);
      res.get(pos+r++)[1+i] = padIntVal(empStats.energyProductionBonus);
      res.get(pos+r++)[1+i] = padIntVal(empStats.energyConsumption);
      res.get(pos+r++)[1+i] = padIntVal(empStats.energyExcess);
      res.get(pos+r++)[1+i] = padIntVal(empStats.intelTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.intelMaxRaw);
      res.get(pos+r++)[1+i] = padIntVal(empStats.intelAssigned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.intelExtra);
      res.get(pos+r++)[1+i] = padIntVal(empStats.intelBonus);
      res.get(pos+r++)[1+i] = padIntVal(empStats.researchTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.researchMaxRaw);
      res.get(pos+r++)[1+i] = padIntVal(empStats.researchAssigned);
      res.get(pos+r++)[1+i] = padIntVal(empStats.researchExtra);
      res.get(pos+r++)[1+i] = padIntVal(empStats.researchBonus);
      res.get(pos+r++)[1+i] = padIntVal(empStats.incomeTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.incomeRaw);
      res.get(pos+r++)[1+i] = padIntVal(empStats.incomeExtra);
      res.get(pos+r++)[1+i] = padIntVal(empStats.incomeBonus);
      res.get(pos+r++)[1+i] = padIntVal(empStats.incomeRemnant);
      res.get(pos+r++)[1+i] = padIntVal(empStats.shipBuildExtra);
      res.get(pos+r++)[1+i] = padIntVal(empStats.shipsInBuild);

      // trade routes
      res.get(pos+r++)[1+i] = padIntVal(empStats.tradeRoutesIncoming);
      res.get(pos+r++)[1+i] = padIntVal(empStats.tradeRoutesLocal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.tradeRoutesExtra);

      // morale
      res.get(pos+r++)[1+i] = padIntVal(empStats.averageMorale());
      for (int lvl = 0; lvl < empStats.moraleLvls.length; ++lvl)
        res.get(pos+r++)[1+i] = padIntVal(empStats.moraleLvls[lvl]);
    }
  }

  private void loadShipStats(ArrayList<String[]> res) throws IOException {
    GShipList shipList = (GShipList) SAV.getInternalFile(CSavFiles.GShipList, true);
    EmpireShipStats[] shipStats = shipList.getEmpireStats();

    // collect types for indexing
    HashSet<Short>   shipCategories = new HashSet<Short>();
    HashSet<Integer> ranges = new HashSet<Integer>();
    HashSet<Integer> speeds = new HashSet<Integer>();
    HashSet<Integer> scanRanges = new HashSet<Integer>();
    HashSet<Integer> experiences = new HashSet<Integer>();
    for (EmpireShipStats empStats : shipStats) {
      shipCategories.addAll(empStats.shipCategories.keySet());
      ranges.addAll(empStats.range.keySet());
      speeds.addAll(empStats.speed.keySet());
      scanRanges.addAll(empStats.scanRange.keySet());
      experiences.addAll(empStats.experienceLvl.keySet());
    }

    final int pos = res.size();
    final int entryCount = 2 + shipCategories.size() + ranges.size() + speeds.size() + scanRanges.size()
      + experiences.size();
    res.ensureCapacity(pos + entryCount);

    // add headers
    // count & status
    addEntry(res, shipStats.length, Language.getString("EmpireStatsPanel.97"));
    addEntry(res, shipStats.length, Language.getString("EmpireStatsPanel.98"));

    // ship categories
    for (Short shipCategory : shipCategories) {
      String catName = LexHelper.mapShipRole(shipCategory);
      addEntry(res, shipStats.length, Language.getString("EmpireStatsPanel.99")
        .replace("%", catName));
    }

    // range
    for (Integer range : ranges) {
      addEntry(res, shipStats.length, Language.getString("EmpireStatsPanel.100")
        .replace("%", Integer.toString(range)));
    }

    // speed
    for (Integer speed : speeds) {
      addEntry(res, shipStats.length, Language.getString("EmpireStatsPanel.101")
        .replace("%", Integer.toString(speed)));
    }

    // scan range
    for (Integer scanRange : scanRanges) {
      addEntry(res, shipStats.length, Language.getString("EmpireStatsPanel.102")
        .replace("%", Integer.toString(scanRange)));
    }

    // crew experience
    for (Integer experience : experiences) {
      String expName = LexHelper.mapCrewExperience(experience);
      addEntry(res, shipStats.length, Language.getString("EmpireStatsPanel.117")
        .replace("%", expName));
    }

    // add values
    for (int i = 0; i < shipStats.length; ++i) {
      EmpireShipStats empStats = shipStats[i];

      // count & status
      int r = 0;
      res.get(pos+r++)[1+i] = padIntVal(empStats.shipsTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.shipsCloaked);

      // ship categories
      for (short shipCategory : shipCategories) {
        int count = empStats.shipCategories.getOrDefault(shipCategory, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      // range
      for (int range : ranges) {
        int count = empStats.range.getOrDefault(range, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      // speed
      for (int speed : speeds) {
        int count = empStats.speed.getOrDefault(speed, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      // scan range
      for (int scanRange : scanRanges) {
        int count = empStats.scanRange.getOrDefault(scanRange, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      // crew experience
      for (int experience : experiences) {
        int count = empStats.experienceLvl.getOrDefault(experience, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }
    }
  }

  private void loadStarbaseStats(ArrayList<String[]> res) throws IOException {
    StarBaseInfo sbInfo = (StarBaseInfo) SAV.getInternalFile(CSavFiles.StarBaseInfo, true);
    StarbaseStats[] sbStats = sbInfo.getEmpireStats();

    final int pos = res.size();
    final int entryCount = 6;
    res.ensureCapacity(pos + entryCount);

    // add headers
    addEntry(res, sbStats.length, Language.getString("EmpireStatsPanel.103"));
    addEntry(res, sbStats.length, Language.getString("EmpireStatsPanel.104"));
    addEntry(res, sbStats.length, Language.getString("EmpireStatsPanel.105"));
    addEntry(res, sbStats.length, Language.getString("EmpireStatsPanel.106"));
    addEntry(res, sbStats.length, Language.getString("EmpireStatsPanel.107"));
    addEntry(res, sbStats.length, Language.getString("EmpireStatsPanel.108"));

    // add values
    for (int i = 0; i < sbStats.length; ++i) {
      StarbaseStats empStats = sbStats[i];

      // count & status
      int r = 0;
      res.get(pos+r++)[1+i] = padIntVal(empStats.starbasesTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.starbasesInBuild);
      res.get(pos+r++)[1+i] = padIntVal(empStats.starbasesDamaged);
      res.get(pos+r++)[1+i] = padIntVal(empStats.outpostsTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.outpostsInBuild);
      res.get(pos+r++)[1+i] = padIntVal(empStats.outpostsDamaged);
    }
  }

  private void loadFleetStats(ArrayList<String[]> res) throws IOException {
    TaskForceList tfList = (TaskForceList) SAV.getInternalFile(CSavFiles.GWTForce, true);
    TaskForceStats[] tfStats = tfList.getEmpireStats();

    // collect types for indexing
    HashSet<Integer> ranges = new HashSet<Integer>();
    HashSet<Integer> speeds = new HashSet<Integer>();
    HashSet<Integer> scanRanges = new HashSet<Integer>();
    HashSet<Integer> missions = new HashSet<Integer>();
    for (TaskForceStats empStats : tfStats) {
      ranges.addAll(empStats.range.keySet());
      speeds.addAll(empStats.speed.keySet());
      scanRanges.addAll(empStats.scanRange.keySet());
      missions.addAll(empStats.missions.keySet());
    }

    final int pos = res.size();
    final int entryCount = 3 + ranges.size() + speeds.size() + scanRanges.size() + missions.size();
    res.ensureCapacity(pos + entryCount);

    // add headers
    // count & status
    addEntry(res, tfStats.length, Language.getString("EmpireStatsPanel.109"));
    addEntry(res, tfStats.length, Language.getString("EmpireStatsPanel.110"));

    // range
    for (Integer range : ranges) {
      addEntry(res, tfStats.length, Language.getString("EmpireStatsPanel.111").replace("%", Integer.toString(range)));
    }

    // speed
    for (Integer speed : speeds) {
      addEntry(res, tfStats.length, Language.getString("EmpireStatsPanel.112").replace("%", Integer.toString(speed)));
    }

    // scan range
    for (Integer scanRange : scanRanges) {
      addEntry(res, tfStats.length, Language.getString("EmpireStatsPanel.113").replace("%", Integer.toString(scanRange)));
    }

    // missions
    for (Integer mission : missions) {
      String missionName = LexHelper.mapTFMission(mission);
      if (mission == Mission.Intercept)
        missionName = missionName.replace("%d %% ", "");
      addEntry(res, tfStats.length, Language.getString("EmpireStatsPanel.114").replace("%", missionName));
    }

    // systems attacked
    addEntry(res, tfStats.length, Language.getString("EmpireStatsPanel.115"));

    // add values
    for (int i = 0; i < tfStats.length; ++i) {
      TaskForceStats empStats = tfStats[i];

      // count & status
      int r = 0;
      res.get(pos+r++)[1+i] = padIntVal(empStats.taskForcesTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.taskForcesCloaked);

      // range
      for (int range : ranges) {
        int count = empStats.range.getOrDefault(range, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      // speed
      for (int speed : speeds) {
        int count = empStats.speed.getOrDefault(speed, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      // scan range
      for (int scanRange : scanRanges) {
        int count = empStats.scanRange.getOrDefault(scanRange, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      // missions
      for (int mission : missions) {
        int count = empStats.missions.getOrDefault(mission, 0);
        res.get(pos+r++)[1+i] = padIntVal(count);
      }

      res.get(pos+r++)[1+i] = padIntVal(empStats.systemsAttacked);
    }
  }

  private void loadStructureStats(ArrayList<String[]> res) throws IOException {
    if (empireSystemIds == null)
      return;

    StrcInfo strcInfo = (StrcInfo) SAV.getInternalFile(CSavFiles.StrcInfo, true);
    StrcStats[] strcStats = Arrays.stream(empireSystemIds).map(starIds ->
      FunctionTools.logIfThrown(() -> strcInfo.getEmpireStats(starIds), null)
    ).toArray(StrcStats[]::new);

    // collect types for indexing
    HashMap<Short, StrcStatEntry> strcTypes = new HashMap<Short, StrcStatEntry>();
    for (StrcStats empStats : strcStats)
      empStats.strcTypes.entrySet().stream().forEachOrdered(x -> strcTypes.putIfAbsent(x.getKey(), x.getValue()));

    final int pos = res.size();
    final int entryCount = 9 + strcTypes.size();
    res.ensureCapacity(pos + entryCount);

    // add headers
    // count & status
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.121"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.122"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.123"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.124"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.125"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.126"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.127"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.128"));
    addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.129"));

    // structure types
    for (StrcStatEntry entry : strcTypes.values()) {
      addEntry(res, strcStats.length, Language.getString("EmpireStatsPanel.130")
        .replace("%1", Integer.toString(entry.race))
        .replace("%2", entry.name));
    }

    // add values
    for (int i = 0; i < strcStats.length; ++i) {
      StrcStats empStats = strcStats[i];

      // count & status
      int r = 0;
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcTotal);
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcMainFood);
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcMainIndustry);
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcMainEnergy);
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcMainIntel);
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcMainResearch);
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcScanners);
      res.get(pos+r++)[1+i] = padIntVal(empStats.strcOther);
      res.get(pos+r++)[1+i] = padIntVal(empStats.powered);

      // structure types
      for (short strcType : strcTypes.keySet()) {
        StrcStatEntry strc = empStats.strcTypes.get(strcType);
        res.get(pos+r++)[1+i] = strc != null ? padIntVal(strc.count)
          + (strc.powered > 0 ? " (" + strc.powered + ")" : "") : "-";
      }
    }
  }

  private void loadTechStats(ArrayList<String[]> res) throws IOException {
    TechInfo techInfo = (TechInfo) SAV.getInternalFile(CSavFiles.TechInfo, true);
    EmpireTechStats[] techStats = techInfo.getEmpireStats();
    final int techFields = techStats[0].techLevels.length;

    final int pos = res.size();
    res.ensureCapacity(pos + techFields);

    // add headers
    for (int i = 0; i < techFields; ++i) {
      String techField = LexHelper.mapTechField(i);
      addEntry(res, techStats.length, Language.getString("EmpireStatsPanel.142").replace("%", techField));
    }

    // add values
    for (int i = 0; i < techStats.length; ++i) {
      EmpireTechStats empStats = techStats[i];
      for (int j = 0; j < empStats.techLevels.length; ++j)
        res.get(pos+j)[1+i] = padIntVal(empStats.techLevels[j]);
    }
  }

  private void loadIntelStats(ArrayList<String[]> res) throws IOException {
    IntelInfo intelInfo = (IntelInfo) SAV.getInternalFile(CSavFiles.IntelInfo, true);
    EmpireIntelStats[] intelStats = intelInfo.getEmpireStats();

    final int pos = res.size();
    final int entryCount = 27;
    res.ensureCapacity(pos + entryCount);

    // add headers
    // last turn output
    addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.131"));
    addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.132"));
    addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.133"));
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.134").replace("%", races[emp]));
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.135").replace("%", races[emp]));
    // average output
    addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.136"));
    addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.137"));
    addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.138"));
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.139").replace("%", races[emp]));
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.140").replace("%", races[emp]));
    // reports
    addEntry(res, intelStats.length, Language.getString("EmpireStatsPanel.141"));

    // add values
    for (int i = 0; i < intelStats.length; ++i) {
      EmpireIntelStats empStats = intelStats[i];

      int r = 0;
      // last turn output
      res.get(pos+r++)[1+i] = padIntVal(empStats.security);
      res.get(pos+r++)[1+i] = padIntVal(empStats.espionageImpact);
      res.get(pos+r++)[1+i] = padIntVal(empStats.sabotageImpact);
      for (int j = 0; j < empStats.espionage.length; ++j)
        res.get(pos+r++)[1+i] = padIntVal(empStats.espionage[j]);
      for (int j = 0; j < empStats.sabotage.length; ++j)
        res.get(pos+r++)[1+i] = padIntVal(empStats.sabotage[j]);
      // average output
      res.get(pos+r++)[1+i] = padIntVal(empStats.averageSecurity);
      res.get(pos+r++)[1+i] = padIntVal(empStats.averageEspionageImpact);
      res.get(pos+r++)[1+i] = padIntVal(empStats.averageSabotageImpact);
      for (int j = 0; j < empStats.averageEspionage.length; ++j)
        res.get(pos+r++)[1+i] = padIntVal(empStats.averageEspionage[j]);
      for (int j = 0; j < empStats.averageSabotage.length; ++j)
        res.get(pos+r++)[1+i] = padIntVal(empStats.averageSabotage[j]);
      // reports
      res.get(pos+r++)[1+i] = padIntVal(empStats.reportCount);
    }
  }

  private void loadEventStats(ArrayList<String[]> res) throws IOException {
    ResultList resultList = (ResultList) SAV.getInternalFile(CSavFiles.ResultLst, true);
    ResultStats[] resultStats = resultList.getEmpireStats();

    final int pos = res.size();
    final int entryCount = 1;
    res.ensureCapacity(pos + entryCount);

    // add headers
    addEntry(res, resultStats.length, Language.getString("EmpireStatsPanel.116"));

    // add values
    for (int i = 0; i < resultStats.length; ++i) {
      ResultStats empStats = resultStats[i];
      res.get(pos)[1+i] = padIntVal(empStats.resultsTotal);
    }
  }

  private String[] createEntry(int dataColumns, String label) {
    String[] values = new String[1+dataColumns];
    values[0] = label;
    return values;
  }

  private void addEntry(ArrayList<String[]> res, int dataColumns, String label) {
    res.add(createEntry(dataColumns, label));
  }

  private String pad(String val) {
    return val;
  }

  private String pad(String val, int len) {
    return StringTools.padLeft(val, len);
  }

  private String padIntVal(int val) {
    // no textual padding since it is right aligned
    return Integer.toString(val);
  }

  private String padIntVal(int val, int len) {
    return pad(Integer.toString(val), len);
  }
}
