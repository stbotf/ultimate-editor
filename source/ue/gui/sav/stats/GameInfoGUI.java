package ue.gui.sav.stats;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.GameInfo;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.component.IconButton;
import ue.gui.util.component.ImagePanel;
import ue.gui.util.gfx.Icons;
import ue.service.SettingsManager;
import ue.util.data.ValueCast;

/**
 * Used to edit game info of a saved game.
 */
public class GameInfoGUI extends MainPanel {

  private static final String bgImageName = "graham-holtshausen.jpg";
  private static ImagePanel bgImgPanel = null;

  // icons
  private static final Color BTN_COLOR = new Color(180, 180, 180);
  private static final Color HL_COLOR = new Color(50, 130, 220);

  // read
  private SavGame savGame;

  // ui components
  private JPanel headerPanel = new JPanel();
  private JTextPane lblWarn = new JTextPane();
  private JPanel infoPanel = new JPanel();
  private CardLayout cards = new CardLayout();
  private JPanel pnlCards = new JPanel();
  private GameStatsPanel statsPanel;
  private EmpireStatsPanel empiresPanel;
  private GameDetailsPanel detailsPanel;
  private JPanel emptyPanel = new JPanel();
  private ScorePanel scorePanel = new ScorePanel();
  private JPanel pathInfo = new JPanel();
  private JLabel lblTrekPath = new JLabel();
  private JLabel lblStBofPath = new JLabel();
  private JLabel lblSeparator = new JLabel("  •  ");
  private JPanel pnlNav = new JPanel();
  private IconButton btnPrev = new IconButton(Icons.PAGE_LEFT, 24, BTN_COLOR, HL_COLOR);
  private IconButton btnNext = new IconButton(Icons.PAGE_RIGHT, 24, BTN_COLOR, HL_COLOR);

  /**
   * Constructor.
   * @throws IOException
   */
  public GameInfoGUI(SavGame sav, Stbof stbof) throws IOException {
    savGame = sav;

    /**
     * loaded game statistic files:
     * CSavFiles.GameInfo
     * CStbofFiles.RaceRst
     * CStbofFiles.LexiconDic
     */
    GameInfo gameInfo = (GameInfo) sav.getInternalFile(CSavFiles.GameInfo, true);
    statsPanel = new GameStatsPanel(gameInfo, stbof);

    /**
     * additionally loaded empire statistic files:
     * CSavFiles.AlienInfo
     * CSavFiles.EmpsInfo
     * CSavFiles.GalInfo
     * CSavFiles.GShipList
     * CSavFiles.GWTForce
     * CSavFiles.IntelInfo
     * CSavFiles.ResultLst
     * CSavFiles.SectorLst
     * CSavFiles.StarBaseInfo
     * CSavFiles.StrcInfo
     * CSavFiles.SystInfo
     * CSavFiles.TechInfo
     */
    empiresPanel = new EmpireStatsPanel(sav, gameInfo, stbof);

    /**
     * additionally loaded galaxy details:
     * CSavFiles.StellInfo
     * CSavFiles.Treaty
     */
    detailsPanel = new GameDetailsPanel(sav, gameInfo, stbof);

    setupControls();
    setupLayout();
    loadBackgroundImage();
    load(gameInfo);
    addActionListeners();
  }

  private void setupControls() {
    // setup sav file corruption warning
    lblWarn.setEditable(false);
    lblWarn.setContentType("text/html");
    lblWarn.setBackground(Color.DARK_GRAY);
    lblWarn.setForeground(Color.WHITE);
    lblWarn.setBorder(new EmptyBorder(4, 8, 4, 8));
    lblWarn.setPreferredSize(new Dimension(199, 50));
    lblWarn.setFont(new Font("Dialog", Font.BOLD, 12));
    lblWarn.addHyperlinkListener(new HyperlinkListener() {
      @Override
      public void hyperlinkUpdate(HyperlinkEvent event) {
        if(event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
          try {
            if(Desktop.isDesktopSupported()) {
              URI uri = event.getURL().toURI();
              Desktop.getDesktop().browse(uri);
            }
          } catch (Exception err) {
              err.printStackTrace();
          }
        }
      }
    });

    pnlCards.setOpaque(false);
    emptyPanel.setOpaque(false);
    Font defFont = UE.SETTINGS.getDefaultFont();
    infoPanel.setBackground(Color.BLACK);
    pathInfo.setBackground(Color.DARK_GRAY);
    pathInfo.setFont(defFont);
    pathInfo.setBorder(new EmptyBorder(2, 8, 2, 8));
    lblTrekPath.setFont(defFont);
    lblTrekPath.setForeground(Color.WHITE);
    lblStBofPath.setFont(defFont);
    lblStBofPath.setForeground(Color.WHITE);
    lblSeparator.setFont(defFont);
    lblSeparator.setForeground(Color.WHITE);

    pnlNav.setOpaque(false);
  }

  private void setupLayout() {
    setLayout(new GridBagLayout());
    setupHeaderLayout();
    setupInfoLayout();
    setupFooterLayout();

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(0, 0, 0, 0);
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1.0;
    add(headerPanel, c);
    c.weighty = 1.0;
    c.gridy++;
    add(infoPanel, c);
    c.weighty = 0;
    c.gridy++;
    add(pathInfo, c);
  }

  private void setupHeaderLayout() {
    headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.Y_AXIS));
    headerPanel.add(Box.createHorizontalStrut(470));
    headerPanel.add(lblWarn);
  }

  private void setupInfoLayout() {
    // cards
    pnlCards.setLayout(cards);
    pnlCards.add(statsPanel, "0"); //$NON-NLS-1$
    pnlCards.add(detailsPanel, "1"); //$NON-NLS-1$
    pnlCards.add(empiresPanel, "2"); //$NON-NLS-1$
    pnlCards.add(emptyPanel, "3"); //$NON-NLS-1$
    cards.first(pnlCards);
    // score
    infoPanel.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(0, 0, 0, 0);
    c.weighty = 1.0;
    c.weightx = 1.0;
    c.gridx = 0;
    c.gridy = 0;
    infoPanel.add(pnlCards, c);
    c.insets = new Insets(5, 5, 5, 5);
    infoPanel.add(Box.createVerticalStrut(100), c);
    infoPanel.add(scorePanel, c);
  }

  private void loadBackgroundImage() {
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.weighty = 1.0;
    c.weightx = 1.0;
    c.gridx = 0;
    c.gridy = 0;

    if (bgImgPanel != null) {
      infoPanel.add(bgImgPanel, c);
    }
    else {
      SwingWorker<Boolean, Void> loadWorker = new SwingWorker<Boolean, Void>() {

        @Override
        protected Boolean doInBackground() throws Exception {
          bgImgPanel = new ImagePanel(bgImageName);
          return true;
        }

        @Override
        // update ui on published events
        protected void done() {
          infoPanel.add(bgImgPanel, c);
          infoPanel.revalidate();
        }
      };

      loadWorker.execute();
    }
  }

  private void setupFooterLayout() {
    setupNavLayout();
    pathInfo.setLayout(new BoxLayout(pathInfo, BoxLayout.X_AXIS));
    pathInfo.add(lblTrekPath);
    pathInfo.add(lblSeparator);
    pathInfo.add(lblStBofPath);
    pathInfo.add(Box.createHorizontalStrut(10));
    pathInfo.add(Box.createHorizontalGlue());
    pathInfo.add(pnlNav);
  }

  private void setupNavLayout() {
    pnlNav.setLayout(new BoxLayout(pnlNav, BoxLayout.X_AXIS));
    pnlNav.add(btnPrev);
    pnlNav.add(btnNext);
  }

  private void load(GameInfo gameInfo) {
    // setup trek & stbof path info
    String trekPath = UE.SETTINGS.getProperty(SettingsManager.EXE);
    String stBofPath = UE.SETTINGS.getProperty(SettingsManager.STBOF);
    lblTrekPath.setText(trekPath);
    lblStBofPath.setText(stBofPath);

    lblWarn.setText("<div color='white'><b>Attention, editing save games is at your risk!</b>"
      + " Specially for modded games, chance is good to corrupt your saves!"
      + " Check <a href='https://www.armadafleetcommand.com/onscreen/botf/viewforum.php?f=125' color='aqua'>AFC forums</a> for known issues.</div>");

    scorePanel.setEnd(gameInfo.getEmpireStatsIdx());
    scorePanel.setValues(ValueCast.castUIntArray(gameInfo.getEmpireStatistics()));
  }

  private void addActionListeners() {
    btnPrev.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        cards.previous(pnlCards);
      }
    });
    btnNext.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        cards.next(pnlCards);
      }
    });
  }

  @Override
  public String menuCommand() {
    return MenuCommand.GameInfo;
  }

  //final warning
  @Override
  public void finalWarning() {
  }

  @Override
  public boolean hasChanges() {
    // no changes applied by GameInfo
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  @Override
  public void reload() throws IOException {
    GameInfo gameInfo = (GameInfo) savGame.getInternalFile(CSavFiles.GameInfo, true);
    load(gameInfo);
    statsPanel.load(gameInfo);
    empiresPanel.load(savGame, gameInfo);
    detailsPanel.load(savGame, gameInfo);
  }
}
