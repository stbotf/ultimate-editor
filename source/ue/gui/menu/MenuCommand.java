package ue.gui.menu;

import java.io.IOException;
import ue.UE;
import ue.service.Language;

/**
 * This class is a middle-man for GUI menu command execution.
 */
public interface MenuCommand {

  // shared commands
  public static final String Check                = "Check Integrity"; //$NON-NLS-1$
  public static final String Files                = "Files"; //$NON-NLS-1$
  public static final String ModList              = "List Of Modified Files"; //$NON-NLS-1$
  // MainMenu commands
  public static final String About                = "About..."; //$NON-NLS-1$
  public static final String ChangeLog            = "Change Log"; //$NON-NLS-1$
  public static final String Close                = "Close"; //$NON-NLS-1$
  public static final String Exit                 = "Exit"; //$NON-NLS-1$
  public static final String FAQ                  = "FAQ"; //$NON-NLS-1$
  public static final String Help                 = "Help"; //$NON-NLS-1$
  public static final String Open                 = "Open"; //$NON-NLS-1$
  public static final String Readme               = "Readme"; //$NON-NLS-1$
  public static final String Save                 = "Save"; //$NON-NLS-1$
  public static final String SaveAs               = "Save as..."; //$NON-NLS-1$
  public static final String Settings             = "Settings"; //$NON-NLS-1$
  public static final String StartPage            = "Start Page"; //$NON-NLS-1$
  public static final String VisitAFC             = "Visit AFC"; //$NON-NLS-1$
  public static final String VisitGitLab          = "Visit GitLab"; //$NON-NLS-1$
  // TrekMenu commands
  public static final String AiShpBldSets         = "AI Ship Building Sets"; //$NON-NLS-1$
  public static final String CdProtection         = "CD Protection"; //$NON-NLS-1$
  public static final String Construction         = "Construction & Upgrades"; //$NON-NLS-1$
  public static final String GalaxyGen            = "Galaxy Generation"; //$NON-NLS-1$
  public static final String Graphics             = "Graphics"; //$NON-NLS-1$
  public static final String MapRanges            = "Ship Map Ranges"; //$NON-NLS-1$
  public static final String Segments             = "Trek Segments"; //$NON-NLS-1$
  public static final String StartCond            = "Starting Conditions"; //$NON-NLS-1$
  // StbofMenu commands
  public static final String AiBldReq             = "AI Building Requirements"; //$NON-NLS-1$
  public static final String Animations           = "Animations"; //$NON-NLS-1$
  public static final String BuildingStats        = "Building Stats"; //$NON-NLS-1$
  public static final String BuildingGroupEdit    = "Group Edit Of Building Stats"; //$NON-NLS-1$
  public static final String ColonyVal            = "Colony Value"; //$NON-NLS-1$
  public static final String EdificeReport        = "Buildings Overview"; //$NON-NLS-1$
  public static final String Environments         = "Environments"; //$NON-NLS-1$
  public static final String GameText             = "In-Game Text"; //$NON-NLS-1$
  public static final String HobFiles             = "HOB Files"; //$NON-NLS-1$
  public static final String InstallModels        = "Add/Remove Ship Models"; //$NON-NLS-1$
  public static final String IntelMult            = "Intel multipliers"; //$NON-NLS-1$
  public static final String Lexicon              = "Lexicon"; //$NON-NLS-1$
  public static final String MaxPlanetPop         = "Maximum Planet Populations"; //$NON-NLS-1$
  public static final String MeSystems            = "Major Empire Home Systems"; //$NON-NLS-1$
  public static final String MinorAttitudes       = "Minor Race Attitudes"; //$NON-NLS-1$
  public static final String Modlist              = "List Of Modified Files"; //$NON-NLS-1$
  public static final String MoralePenalties      = "Morale Penalties"; //$NON-NLS-1$
  public static final String PlanetBonuses        = "Planet Bonuses"; //$NON-NLS-1$
  public static final String Planets              = "Planets"; //$NON-NLS-1$
  public static final String RacesInfo            = "Races Info"; //$NON-NLS-1$
  public static final String ShipGroupEdit        = "Group Edit Of Ship Stats"; //$NON-NLS-1$
  public static final String ShipNames            = "Ship Names"; //$NON-NLS-1$
  public static final String ShipStats            = "Ship Stats"; //$NON-NLS-1$
  public static final String ShiplistReport       = "Ships Overview"; //$NON-NLS-1$
  public static final String StarNames            = "Star Names"; //$NON-NLS-1$
  public static final String StartBuildings       = "Starting Buildings"; //$NON-NLS-1$
  public static final String StartTech            = "Starting Technology"; //$NON-NLS-1$
  public static final String StellarObj           = "Stellar Objects"; //$NON-NLS-1$
  public static final String TechFields           = "Technology Fields"; //$NON-NLS-1$
  public static final String TechFieldsGroupEdit  = "Group Edit Of Technology Fields"; //$NON-NLS-1$
  public static final String Textures             = "Textures"; //$NON-NLS-1$
  public static final String WdfEditor            = "Graphical User Interface"; //$NON-NLS-1$
  // SavMenu commands
  public static final String Empires              = "Empires"; //$NON-NLS-1$
  public static final String GameConfig           = "Game Configuration"; //$NON-NLS-1$
  public static final String GameInfo             = "Game Info"; //$NON-NLS-1$
  public static final String MapV2                = "Galactic Map"; //$NON-NLS-1$
  public static final String Ships                = "Ships"; //$NON-NLS-1$
  public static final String Starbases            = "Starbases"; //$NON-NLS-1$
  public static final String StellInfo            = "Stellar Info"; //$NON-NLS-1$
  public static final String Strcinfo             = "Lists of built structures"; //$NON-NLS-1$
  public static final String SystemInfo           = "System Info"; //$NON-NLS-1$
  // EnglishMenu commands
  public static final String SwitchRaces          = "Switch Race Voices"; //$NON-NLS-1$
  public static final String Voices               = "Voices"; //$NON-NLS-1$
  // MusicMenu commands
  public static final String Music                = "Music"; //$NON-NLS-1$
  // SfxMenu commands
  public static final String GameSounds           = "Game Sounds"; //$NON-NLS-1$
  // SpecialMenu commands
  public static final String CreateAniCur         = "Create animation/cursor"; //$NON-NLS-1$
  public static final String ConvertImage         = "Convert image to 16-bit TGA"; //$NON-NLS-1$
  public static final String SuperviseMulti       = "Supervise a multiplayer game"; //$NON-NLS-1$
  public static final String Calculator           = "Calculator"; //$NON-NLS-1$
  public static final String ConvertAddresses     = "Convert addresses";

  /**
   * Looks up the translated menu or command display name.
   * With static access for panel name lookup.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  public static String mapCommandName(String command) {
    switch (command) {
      // shared commands
      case Check:               return Language.getString("MenuCommand.0"); //$NON-NLS-1$
      case Files:               return Language.getString("MenuCommand.1"); //$NON-NLS-1$
      case ModList:             return Language.getString("MenuCommand.2"); //$NON-NLS-1$
      // MainMenu commands
      case About:               return Language.getString("MenuCommand.3"); //$NON-NLS-1$
      case ChangeLog:           return Language.getString("MenuCommand.4"); //$NON-NLS-1$
      case Close:               return Language.getString("MenuCommand.5"); //$NON-NLS-1$
      case Exit:                return Language.getString("MenuCommand.6"); //$NON-NLS-1$
      case FAQ:                 return Language.getString("MenuCommand.7"); //$NON-NLS-1$
      case Help:                return Language.getString("MenuCommand.8"); //$NON-NLS-1$
      case Open:                return Language.getString("MenuCommand.9"); //$NON-NLS-1$
      case Readme:              return Language.getString("MenuCommand.10"); //$NON-NLS-1$
      case Save:                return Language.getString("MenuCommand.12"); //$NON-NLS-1$
      case SaveAs:              return Language.getString("MenuCommand.13"); //$NON-NLS-1$
      case Settings:            return Language.getString("MenuCommand.14"); //$NON-NLS-1$
      case StartPage:           return Language.getString("MenuCommand.15"); //$NON-NLS-1$
      case VisitAFC:            return Language.getString("MenuCommand.16"); //$NON-NLS-1$
      case VisitGitLab:         return Language.getString("MenuCommand.17"); //$NON-NLS-1$
      // TrekMenu commands
      case AiShpBldSets:        return Language.getString("MenuCommand.18"); //$NON-NLS-1$
      case CdProtection:        return Language.getString("MenuCommand.19"); //$NON-NLS-1$
      case Construction:        return Language.getString("MenuCommand.20"); //$NON-NLS-1$
      case GalaxyGen:           return Language.getString("MenuCommand.21"); //$NON-NLS-1$
      case Graphics:            return Language.getString("MenuCommand.22"); //$NON-NLS-1$
      case MapRanges:           return Language.getString("MenuCommand.23"); //$NON-NLS-1$
      case Segments:            return Language.getString("MenuCommand.24"); //$NON-NLS-1$
      case StartCond:           return Language.getString("MenuCommand.25"); //$NON-NLS-1$
      // StbofMenu commands
      case AiBldReq:            return Language.getString("MenuCommand.26"); //$NON-NLS-1$
      case Animations:          return Language.getString("MenuCommand.27"); //$NON-NLS-1$
      case BuildingGroupEdit:   return Language.getString("MenuCommand.28"); //$NON-NLS-1$
      case BuildingStats:       return Language.getString("MenuCommand.29"); //$NON-NLS-1$
      case ColonyVal:           return Language.getString("MenuCommand.30"); //$NON-NLS-1$
      case EdificeReport:       return Language.getString("MenuCommand.31"); //$NON-NLS-1$
      case Environments:        return Language.getString("MenuCommand.32"); //$NON-NLS-1$
      case GameText:            return Language.getString("MenuCommand.33"); //$NON-NLS-1$
      case HobFiles:            return Language.getString("MenuCommand.34"); //$NON-NLS-1$
      case InstallModels:       return Language.getString("MenuCommand.35"); //$NON-NLS-1$
      case IntelMult:           return Language.getString("MenuCommand.36"); //$NON-NLS-1$
      case Lexicon:             return Language.getString("MenuCommand.37"); //$NON-NLS-1$
      case MaxPlanetPop:        return Language.getString("MenuCommand.38"); //$NON-NLS-1$
      case MeSystems:           return Language.getString("MenuCommand.39"); //$NON-NLS-1$
      case MinorAttitudes:      return Language.getString("MenuCommand.40"); //$NON-NLS-1$
      case MoralePenalties:     return Language.getString("MenuCommand.41"); //$NON-NLS-1$
      case PlanetBonuses:       return Language.getString("MenuCommand.42"); //$NON-NLS-1$
      case Planets:             return Language.getString("MenuCommand.43"); //$NON-NLS-1$
      case RacesInfo:           return Language.getString("MenuCommand.44"); //$NON-NLS-1$
      case ShipGroupEdit:       return Language.getString("MenuCommand.45"); //$NON-NLS-1$
      case ShipNames:           return Language.getString("MenuCommand.46"); //$NON-NLS-1$
      case ShipStats:           return Language.getString("MenuCommand.47"); //$NON-NLS-1$
      case ShiplistReport:      return Language.getString("MenuCommand.48"); //$NON-NLS-1$
      case StarNames:           return Language.getString("MenuCommand.49"); //$NON-NLS-1$
      case StartBuildings:      return Language.getString("MenuCommand.50"); //$NON-NLS-1$
      case StartTech:           return Language.getString("MenuCommand.51"); //$NON-NLS-1$
      case StellarObj:          return Language.getString("MenuCommand.52"); //$NON-NLS-1$
      case TechFields:          return Language.getString("MenuCommand.53"); //$NON-NLS-1$
      case TechFieldsGroupEdit: return Language.getString("MenuCommand.54"); //$NON-NLS-1$
      case Textures:            return Language.getString("MenuCommand.55"); //$NON-NLS-1$
      case WdfEditor:           return Language.getString("MenuCommand.56"); //$NON-NLS-1$
      // SavMenu commands
      case Empires:             return Language.getString("MenuCommand.57"); //$NON-NLS-1$
      case GameConfig:          return Language.getString("MenuCommand.58"); //$NON-NLS-1$
      case GameInfo:            return Language.getString("MenuCommand.59"); //$NON-NLS-1$
      case MapV2:               return Language.getString("MenuCommand.60"); //$NON-NLS-1$
      case Ships:               return Language.getString("MenuCommand.61"); //$NON-NLS-1$
      case Starbases:           return Language.getString("MenuCommand.62"); //$NON-NLS-1$
      case StellInfo:           return Language.getString("MenuCommand.63"); //$NON-NLS-1$
      case Strcinfo:            return Language.getString("MenuCommand.64"); //$NON-NLS-1$
      case SystemInfo:          return Language.getString("MenuCommand.65"); //$NON-NLS-1$
      // EnglishMenu commands
      case SwitchRaces:         return Language.getString("MenuCommand.66"); //$NON-NLS-1$
      case Voices:              return Language.getString("MenuCommand.67"); //$NON-NLS-1$
      // MusicMenu commands
      case Music:               return Language.getString("MenuCommand.68"); //$NON-NLS-1$
      // SfxMenu commands
      case GameSounds:          return Language.getString("MenuCommand.69"); //$NON-NLS-1$
      // SpecialMenu commands
      case Calculator:          return Language.getString("MenuCommand.70"); //$NON-NLS-1$
      case ConvertAddresses:    return Language.getString("MenuCommand.71"); //$NON-NLS-1$
      case ConvertImage:        return Language.getString("MenuCommand.72"); //$NON-NLS-1$
      case CreateAniCur:        return Language.getString("MenuCommand.73"); //$NON-NLS-1$
      case SuperviseMulti:      return Language.getString("MenuCommand.74"); //$NON-NLS-1$
      default:                  return command;
    }
  }

  /**
   * The execute function is called to perform some menu action.
   * Available commands are listed above.
   *
   * @param command Identifier for the panel to show.
   * @throws IOException
   */
  public static void execute(String command) throws IOException {
    switch (command) {
      case Exit:        UE.WINDOW.exit();             break;
      case Open:        UE.WINDOW.openFile();         break;
      case Close:       UE.WINDOW.close();            break;
      case Save:        UE.WINDOW.save();             break;
      case SaveAs:      UE.WINDOW.saveAs();           break;
      case Help:        UE.WINDOW.showHelp();         break;
      case FAQ:         UE.WINDOW.showFAQ();          break;
      case Readme:      UE.WINDOW.showReadme();       break;
      case ChangeLog:   UE.WINDOW.showChangelog();    break;
      case VisitAFC:    UE.WINDOW.openAfcWebsite();   break;
      case VisitGitLab: UE.WINDOW.openGitWebsite();   break;
      case About:       UE.WINDOW.showAbout();        break;
      default:          UE.WINDOW.showPanel(command); break;
    }
  }

}
