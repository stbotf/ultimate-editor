package ue.gui.menu;

import ue.UE;
import ue.edit.snd.Music;
import ue.service.Language;

/**
 * This class is a middle-man between the GUI and music.snd musics.
 * Opens, edits, saves sounds of music.snd.
 */
public class MusicMenu implements EditMenu {

  // list of command menus
  public static final String mnuOTHER = "Other"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    Music music = UE.FILES.music();
    if (music == null || music.getSourceFile() == null)
      return null;

    StringGroup[] ret = new StringGroup[4];
    ret[0] = new StringGroup(MenuCommand.Music);
    ret[1] = null;  // separator
    ret[2] = new StringGroup(MenuCommand.Check);
    ret[3] = new StringGroup(MenuCommand.Modlist);

    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuOTHER:        return Language.getString("MusicMenu.3"); //$NON-NLS-1$
      default:              return MenuCommand.mapCommandName(command);
    }
  }

}
