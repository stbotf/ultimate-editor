package ue.gui.menu;

import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.service.Language;

/**
 * This class is a middle-man between the GUI and trek files.
 * Opens, edits, saves trek.exe segments.
 */
public class TrekMenu implements EditMenu {

  // list of command menus
  public static final String mnuAI = "AI"; //$NON-NLS-1$
  public static final String mnuGame = "Game"; //$NON-NLS-1$
  public static final String mnuOther = "Other"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    Trek trek = UE.FILES.trek();
    if (trek == null || trek.getSourceFile() == null)
      return null;

    StringGroup[] ret = new StringGroup[11];
    ret[0] = new StringGroup(MenuCommand.AiShpBldSets);
    ret[1] = new StringGroup(MenuCommand.StartCond);
    ret[2] = null;  // separator
    ret[3] = new StringGroup(MenuCommand.GalaxyGen);
    ret[4] = new StringGroup(MenuCommand.Construction);
    ret[5] = new StringGroup(MenuCommand.MapRanges);
    ret[6] = null;  // separator
    ret[7] = new StringGroup(MenuCommand.CdProtection);
    ret[8] = new StringGroup(MenuCommand.Graphics);
    ret[9] = new StringGroup(MenuCommand.Segments);
    ret[10] = new StringGroup(MenuCommand.Check);
    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuAI:     return Language.getString("TrekMenu.0"); //$NON-NLS-1$
      case mnuGame:   return Language.getString("TrekMenu.1"); //$NON-NLS-1$
      case mnuOther:  return Language.getString("TrekMenu.2"); //$NON-NLS-1$
      default:        return MenuCommand.mapCommandName(command);
    }
  }

}
