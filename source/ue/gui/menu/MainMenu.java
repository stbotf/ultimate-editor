package ue.gui.menu;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.gui.common.MainPanel;
import ue.gui.util.component.IconButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.gfx.Icons;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.SettingsManager;

/**
 * The main menu bar.
 *
 * Acts as a link between the MainWindow and the other parts of the editor.
 */
public class MainMenu extends JMenuBar {

  // ui components
  private JMenu mFile = new JMenu();
  private JMenu mEdit = new JMenu();
  private JMenu mSpecial = new JMenu();
  private JMenu mHelp = new JMenu();
  //FILE items
  private JMenuItem fileStartPage = new JMenuItem();
  private JMenuItem fileOpen = new JMenuItem();
  private JMenu fileRecentList = new JMenu();
  private JMenu fileRestore = new JMenu();
  private JMenuItem fileSave = new JMenuItem();
  private JMenuItem fileSaveAs = new JMenuItem();
  private JMenuItem fileClose = new JMenuItem();
  private JMenuItem fileExit = new JMenuItem();
  //RECENT items
  private JMenuItem[] fileRecent = new JMenuItem[10];
  //EDIT items
  private JMenuItem editSettings = new JMenuItem();
  //HELP items
  private JMenuItem helpHelp = new JMenuItem();
  private JMenuItem helpFAQ = new JMenuItem();
  private JMenuItem helpReadme = new JMenuItem();
  private JMenuItem helpChangeLog = new JMenuItem();
  private JMenuItem helpVisitAFC = new JMenuItem();
  private JMenuItem helpVisitGitLab = new JMenuItem();
  private JMenuItem helpAbout = new JMenuItem();
  //Buttons
  private IconButton btnReload = IconButton.CreateSmallButton(Icons.REFRESH);

  // data
  private static final AltMenu altMenu = new AltMenu();
  private static final EnglishMenu engMenu = new EnglishMenu();
  private static final MusicMenu musicMenu = new MusicMenu();
  private static final SavMenu savMenu = new SavMenu();
  private static final SfxMenu sfxMenu = new SfxMenu();
  private static final StbofMenu stbofMenu = new StbofMenu();
  private static final TrekMenu trekMenu = new TrekMenu();
  @Getter @Accessors(fluent = true)
  private static final SpecialMenu specialMenu = new SpecialMenu();

  /**
   * Constructor.
   */
  public MainMenu() {
    setupComponents();
    placeComponents();
    addListerners();

    setVisible(true);
  }

  private void setupComponents() {
    //setup
    btnReload.setMargin(new Insets(2, 0, 0, 0));
    btnReload.setEnabled(false);
    fileSave.setEnabled(false);
    fileRestore.setEnabled(false);
    fileClose.setEnabled(false);
    fileSaveAs.setEnabled(false);
    resetMenu();
  }

  private void placeComponents() {
    //add items to menus
    //file
    mFile.add(fileStartPage);
    mFile.addSeparator();
    mFile.add(fileOpen);
    mFile.add(fileRecentList);
    mFile.addSeparator();
    mFile.add(fileSave);
    mFile.add(fileSaveAs);
    mFile.add(fileRestore);
    mFile.addSeparator();
    mFile.add(fileClose);
    mFile.addSeparator();
    mFile.add(fileExit);
    //edit
    mEdit.add(editSettings);
    //help
    mHelp.add(helpHelp);
    mHelp.add(helpFAQ);
    mHelp.add(helpReadme);
    mHelp.add(helpChangeLog);
    mHelp.addSeparator();
    mHelp.add(helpVisitAFC);
    mHelp.add(helpVisitGitLab);
    mHelp.addSeparator();
    mHelp.add(helpAbout);

    //add to frame
    add(mFile);
    add(mEdit);
    add(mSpecial);
    add(mHelp);
    add(Box.createHorizontalGlue());
    add(btnReload);
  }

  private void addListerners() {
    // set action commands
    fileStartPage.setActionCommand(MenuCommand.StartPage);
    // 'File->Open...'      closes all files, then prompts the user to choose a file to open.
    fileOpen.setActionCommand(MenuCommand.Open);
    // 'File->Save'         saves all changes of the open and all loaded secondary files.
    fileSave.setActionCommand(MenuCommand.Save);
    // 'File->Save As..'    prompts the user where to save to.
    fileSaveAs.setActionCommand(MenuCommand.SaveAs);
    // 'File->Close'        closes all files, the current open panel and resets the menu.
    fileClose.setActionCommand(MenuCommand.Close);
    // 'File->Exit'         closes all files and save settings, then exits the program.
    fileExit.setActionCommand(MenuCommand.Exit);
    // 'Edit->Settings'     displays the settings panel.
    editSettings.setActionCommand(MenuCommand.Settings);
    // 'Help->About UE...'  displays the about window.
    helpHelp.setActionCommand(MenuCommand.Help);
    // 'Help->F.A.Q.'       displays FAQ.
    helpFAQ.setActionCommand(MenuCommand.FAQ);
    helpReadme.setActionCommand(MenuCommand.Readme);
    helpChangeLog.setActionCommand(MenuCommand.ChangeLog);
    helpVisitAFC.setActionCommand(MenuCommand.VisitAFC);
    helpVisitGitLab.setActionCommand(MenuCommand.VisitGitLab);
    // 'Help->Screen Help'  displays the panel help.
    helpAbout.setActionCommand(MenuCommand.About);

    addListeners();

    // listen for language and font changes
    PropertyChangeListener pcl = new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent e) {
        if (e.getSource().equals(Language.class)) {
          resetMenu();
        } else if (e.getSource().equals(UE.SETTINGS) && e.getPropertyName()
            .equals(SettingsManager.FONT_NAME)) {
          resetMenu();
        }
      }
    };
    Language.addPropertyChangeListener(pcl);
    UE.SETTINGS.addPropertyChangeListener(pcl);

    //buttons
    btnReload.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          UE.WINDOW.reload();
        }
        catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });
  }

  /**
   * @return the current open primary file edit menu
   */
  public static EditMenu editMenu() {
    switch (FileManager.getPRIMARY()) {
      case FileManager.P_ALT:       return altMenu;
      case FileManager.P_ENGLISH:   return engMenu;
      case FileManager.P_MUSIC:     return musicMenu;
      case FileManager.P_SAVE_GAME: return savMenu;
      case FileManager.P_SFX:       return sfxMenu;
      case FileManager.P_STBOF:     return stbofMenu;
      case FileManager.P_TREK:      return trekMenu;
      default:                      return null;
    }
  }

  public static void showFileChanges(int fileType) throws IOException {
    switch (fileType) {
      case FileManager.P_ALT:       UE.WINDOW.showPanel(MenuCommand.Files);       break;
      case FileManager.P_ENGLISH:   UE.WINDOW.showPanel(MenuCommand.Voices);      break;
      case FileManager.P_MUSIC:     UE.WINDOW.showPanel(MenuCommand.Music);       break;
      case FileManager.P_SAVE_GAME: UE.WINDOW.showPanel(MenuCommand.Files);       break;
      case FileManager.P_SFX:       UE.WINDOW.showPanel(MenuCommand.GameSounds);  break;
      case FileManager.P_STBOF:     UE.WINDOW.showPanel(MenuCommand.Files);       break;
      case FileManager.P_TREK:      UE.WINDOW.showPanel(MenuCommand.Segments);    break;
    }
  }

  public void onClosed() {
    mEdit.add(editSettings);
    refreshRecentFilesAndBackups();
  }

  public void viewChanged(MainPanel panel) {
    btnReload.setEnabled(panel != null);
  }

  /**
   * Returns the edit commands for the opened file.
   * @return valid edit commands or null if none.
   */
  private StringGroup[] getPrimaryCommands() {
    EditMenu editMenu = editMenu();
    return editMenu != null ? editMenu.getCommands(): null;
  }

  /**
   * Looks up the translated display name for the given primary panel menu or command.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  private String mapPrimaryCommandName(String command) {
    EditMenu editMenu = editMenu();
    return editMenu != null ? editMenu.mapCommandName(command): command;
  }

  private void addMenuOptions(JMenu menu, StringGroup[] menuOptions, ActionListener act) {
    Font def = UE.SETTINGS.getDefaultBigFont();

    for (StringGroup group : menuOptions) {
      if (group == null) {
        menu.addSeparator();
        continue;
      }

      String command = group.getName();
      String commandName = mapPrimaryCommandName(command);
      String[] subCommands = group.getStrings();
      JMenuItem tmi;

      if (subCommands != null) {
        JMenu subMenu = new JMenu(commandName);
        tmi = subMenu;

        for (int j = 0; j < subCommands.length; ++j) {
          String subCommand = subCommands[j];
          if (subCommand == null) {
            subMenu.addSeparator();
          } else {
            String subCommandName = mapPrimaryCommandName(subCommand);
            JMenuItem smi = new JMenuItem(subCommandName);
            smi.setActionCommand(subCommand);
            smi.addActionListener(act);
            smi.setFont(def);
            subMenu.add(smi);
          }
        }
      } else {
        tmi = new JMenuItem(commandName);
        tmi.setActionCommand(command);
        tmi.addActionListener(act);
      }

      tmi.setFont(def);
      menu.add(tmi);
    }
  }

  // call this when language or font changes and the menu needs updating
  private void resetMenu() {
    Font def = UE.SETTINGS.getDefaultBigFont();

    // menus
    mFile.setText(Language.getString("MainMenu.7")); //$NON-NLS-1$
    mEdit.setText(Language.getString("MainMenu.8")); //$NON-NLS-1$
    mSpecial.setText(Language.getString("MainMenu.9")); //$NON-NLS-1$
    mHelp.setText(Language.getString("MainMenu.10")); //$NON-NLS-1$

    //FILE items
    fileStartPage.setText(MenuCommand.mapCommandName(MenuCommand.StartPage));
    fileOpen.setText(MenuCommand.mapCommandName(MenuCommand.Open));
    fileRecentList.setText(Language.getString("MainMenu.11")); //$NON-NLS-1$
    fileRestore.setText(Language.getString("MainMenu.12")); //$NON-NLS-1$
    fileSave.setText(MenuCommand.mapCommandName(MenuCommand.Save));
    fileSaveAs.setText(MenuCommand.mapCommandName(MenuCommand.SaveAs));
    fileClose.setText(MenuCommand.mapCommandName(MenuCommand.Close));
    fileExit.setText(MenuCommand.mapCommandName(MenuCommand.Exit));
    //EDIT items
    editSettings.setText(MenuCommand.mapCommandName(MenuCommand.Settings));
    //HELP items
    helpHelp.setText(MenuCommand.mapCommandName(MenuCommand.Help));
    helpFAQ.setText(MenuCommand.mapCommandName(MenuCommand.FAQ));
    helpReadme.setText(MenuCommand.mapCommandName(MenuCommand.Readme));
    helpChangeLog.setText(MenuCommand.mapCommandName(MenuCommand.ChangeLog));
    helpVisitAFC.setText(MenuCommand.mapCommandName(MenuCommand.VisitAFC));
    helpVisitGitLab.setText(MenuCommand.mapCommandName(MenuCommand.VisitGitLab));
    helpAbout.setText(MenuCommand.mapCommandName(MenuCommand.About));

    btnReload.setToolTipText(Language.getString("MainMenu.17"));

    // update commands
    refreshEditOptions();

    // in case there are no recent files
    refreshRecentFilesAndBackups();

    // update special commands
    refreshSpecialOptions();

    // update font
    mFile.setFont(def);
    mEdit.setFont(def);
    mSpecial.setFont(def);
    mHelp.setFont(def);
    fileStartPage.setFont(def);
    fileOpen.setFont(def);
    fileRecentList.setFont(def);
    fileSave.setFont(def);
    fileSaveAs.setFont(def);
    fileRestore.setFont(def);
    fileClose.setFont(def);
    fileExit.setFont(def);
    editSettings.setFont(def);
    helpHelp.setFont(def);
    helpFAQ.setFont(def);
    helpReadme.setFont(def);
    helpChangeLog.setFont(def);
    helpVisitAFC.setFont(def);
    helpVisitGitLab.setFont(def);
    helpAbout.setFont(def);

    if (UE.WINDOW != null)
      UE.WINDOW.updateWindowTitle(MenuCommand.mapCommandName(MenuCommand.Settings));
  }

  public void refreshEditOptions() {
    StringGroup[] editOptions = getPrimaryCommands();
    mEdit.removeAll();
    mEdit.add(editSettings);

    //add
    if (editOptions != null) {
      // activate file options
      // skip reload, which is activated by panel change
      // @see viewChanged
      fileSave.setEnabled(true);
      fileRestore.setEnabled(true);
      fileSaveAs.setEnabled(true);
      fileClose.setEnabled(true);
      mEdit.addSeparator();

      addMenuOptions(mEdit, editOptions, new CBActionListener(evt ->
        MenuCommand.execute(evt.getActionCommand())
      ));
    }
  }

  public void refreshSpecialOptions() {
    StringGroup[] specials = specialMenu.getCommands();
    mSpecial.removeAll();

    if (specials != null) {
      //ActionListener for special commands buttons
      addMenuOptions(mSpecial, specials, new CBActionListener(evt ->
        MenuCommand.execute(evt.getActionCommand())
      ));
    }
  }

  /**
   * Refreshes the recent files list in the File menu.
   *
   * Uses SettingsManager's getRecentFiles() function to get the list of recent files. Should the
   * list be empty, a disabled JMenuItem("None") will be added to the menu.
   */
  public void refreshRecentFilesAndBackups() {
    // font
    Font def = UE.SETTINGS.getDefaultBigFont();
    //trash old ones
    fileRecentList.removeAll();
    fileRestore.removeAll();

    //get new ones
    String[] file = UE.SETTINGS.getRecentFiles();
    if (file == null) {
      fileRecent[0] = new JMenuItem(Language.getString("MainMenu.13")); //$NON-NLS-1$
      fileRecent[0].setFont(def);
      fileRecent[0].setEnabled(false);
      fileRecentList.add(fileRecent[0]);
    } else {
      ActionListener ore = new CBActionListener(evt -> UE.WINDOW.openFile(evt.getActionCommand()));
      for (int i = 0; i < file.length; i++) {
        fileRecent[i] = new JMenuItem(file[i]);
        fileRecent[i].setFont(def);
        fileRecent[i].addActionListener(ore);
        fileRecentList.add(fileRecent[i]);
      }
    }

    Collection<Date> bkpDates = UE.FILES.getBackupDates();

    if (bkpDates.isEmpty()) {
      JMenuItem item = new JMenuItem(Language.getString("MainMenu.13")); //$NON-NLS-1$
      item.setFont(def);
      item.setEnabled(false);
      fileRestore.add(item);
    } else {
      val dateFormat = FileManager.dateFormat();

      bkpDates.stream().sorted(Collections.reverseOrder()).forEach(bkpDate -> {
        String date = dateFormat.format(bkpDate);
        JMenuItem item = new JMenuItem(date);
        item.setFont(def);
        item.addActionListener(new CBActionListener(evt -> restoreBackups(evt.getActionCommand())));
        fileRestore.add(item);
      });
    }

    // update view
    validate();
  }

  public void reset() {
    fileSave.setEnabled(false);
    fileRestore.setEnabled(false);
    fileSaveAs.setEnabled(false);
    fileClose.setEnabled(false);
    btnReload.setEnabled(false);
    // reset edit menu
    mEdit.removeAll();
  }

  private void addListeners() {
    // Invoked when the user clicks on buttons in the File menu
    // or the 'Settings' button in the Edit menu.
    ActionListener acl = new CBActionListener(evt -> MenuCommand.execute(evt.getActionCommand()));
    fileStartPage.addActionListener(acl);
    fileOpen.addActionListener(acl);
    fileSave.addActionListener(acl);
    fileSaveAs.addActionListener(acl);
    fileClose.addActionListener(acl);
    fileExit.addActionListener(acl);
    editSettings.addActionListener(acl);
    helpAbout.addActionListener(acl);
    helpHelp.addActionListener(acl);
    helpReadme.addActionListener(acl);
    helpChangeLog.addActionListener(acl);
    helpVisitAFC.addActionListener(acl);
    helpVisitGitLab.addActionListener(acl);
    helpFAQ.addActionListener(acl);
  }

  private void restoreBackups(String timestamp) {
    // first apply final panel changes
    UE.WINDOW.finalWarning();

    // check to save and abort if cancelled
    // leave files open and skip removal of old backup files
    if (!UE.FILES.checkSaveChanges())
      return;

    MainPanel prev = UE.WINDOW.closePanel();
    reset();

    // restore
    boolean restored = UE.FILES.restoreBackups(timestamp);
    refreshEditOptions();
    refreshRecentFilesAndBackups();

    if (restored) {
      // attempt to restore the previous view
      if (!restorePanelView(prev))
        UE.WINDOW.showDefaultFileView();
      // notify backup restore completion
      Dialogs.notify(Language.getString("MainMenu.14")); //$NON-NLS-1$
    }
    else {
      // show initial file view
      UE.WINDOW.showDefaultFileView();
    }
  }

  private boolean restorePanelView(MainPanel prev) {
    if (prev != null) {
      try {
        UE.WINDOW.showPanel(prev.menuCommand());
        return true;
      }
      catch (Exception e) {
        Dialogs.displayError(e);
      }
    }
    return false;
  }

}
