package ue.gui.menu;

import ue.UE;
import ue.edit.snd.English;
import ue.service.Language;

/**
 * This class is a middle-man between the GUI and english/german.snd voices.
 * Opens, edits, saves sounds of english/german.snd.
 */
public class EnglishMenu implements EditMenu {

  // list of command menus
  public static final String mnuOTHER = "Other"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    English eng = UE.FILES.english();
    if (eng == null || eng.getSourceFile() == null)
      return null;

    StringGroup[] ret = new StringGroup[5];
    ret[0] = new StringGroup(MenuCommand.Voices);
    ret[1] = new StringGroup(MenuCommand.SwitchRaces);
    ret[2] = null;  // separator
    ret[3] = new StringGroup(MenuCommand.Check);
    ret[4] = new StringGroup(MenuCommand.ModList);

    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuOTHER:  return Language.getString("EnglishMenu.4"); //$NON-NLS-1$
      default:        return MenuCommand.mapCommandName(command);
    }
  }

}
