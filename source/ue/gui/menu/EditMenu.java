package ue.gui.menu;

/**
 * This class is a middle-man between the GUI and sav files. Opens, edits, saves saved games.
 */
public interface EditMenu {

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  public StringGroup[] getCommands();

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  public default String mapCommandName(String command) {
    return MenuCommand.mapCommandName(command);
  }

}
