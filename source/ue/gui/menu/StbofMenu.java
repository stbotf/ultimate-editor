package ue.gui.menu;

import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;

/**
 * This class is a middle-man between the GUI and stbof files.
 * Opens, edits, saves stbof.res files.
 */
public class StbofMenu implements EditMenu {

  // list of command menus
  public static final String mnuAI = "AI"; //$NON-NLS-1$
  public static final String mnuBuildings = "Buildings"; //$NON-NLS-1$
  public static final String mnuGalaxy = "Galaxy"; //$NON-NLS-1$
  public static final String mnuGraphics = "Graphics"; //$NON-NLS-1$
  public static final String mnuOther = "Other"; //$NON-NLS-1$
  public static final String mnuRaces = "Races"; //$NON-NLS-1$
  public static final String mnuShips = "Ships"; //$NON-NLS-1$
  public static final String mnuStartConditions = "StartConditions"; //$NON-NLS-1$
  public static final String mnuTechnology = "Technology"; //$NON-NLS-1$
  public static final String mnuText = "Text"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    Stbof stbof = UE.FILES.stbof();
    if (stbof == null || stbof.getSourceFile() == null)
      return null;

    StringGroup[] ret = new StringGroup[13];

    // AI
    ret[0] = new StringGroup(mnuAI, new String[] {
      MenuCommand.AiBldReq, MenuCommand.MinorAttitudes, MenuCommand.ColonyVal
    });
    // Starting Conditions
    ret[1] = new StringGroup(mnuStartConditions, new String[] {
      MenuCommand.MeSystems, MenuCommand.StartTech, MenuCommand.StartBuildings
    });

    // Galaxy
    ret[2] = null;  // separator
    ret[3] = new StringGroup(mnuGalaxy, new String[] {
      MenuCommand.Environments, MenuCommand.StellarObj, MenuCommand.StarNames,
      MenuCommand.Planets, MenuCommand.PlanetBonuses, MenuCommand.MaxPlanetPop
    });
    // Races
    ret[4] = new StringGroup(mnuRaces, new String[] {
      MenuCommand.RacesInfo, MenuCommand.MoralePenalties, MenuCommand.IntelMult
    });
    // Ships
    ret[5] = new StringGroup(mnuShips, new String[] {
      MenuCommand.ShipStats, MenuCommand.ShipGroupEdit, MenuCommand.ShipNames, null, MenuCommand.ShiplistReport
    });
    // Buildings
    ret[6] = new StringGroup(mnuBuildings, new String[] {
      MenuCommand.BuildingStats, MenuCommand.BuildingGroupEdit, null, MenuCommand.EdificeReport
    });
    // Technology
    ret[7] = new StringGroup(mnuTechnology, new String[] {
      MenuCommand.TechFields, MenuCommand.TechFieldsGroupEdit
    });

    // Text
    ret[8] = null;  // separator
    ret[9] = new StringGroup(mnuText, new String[] {
      MenuCommand.Lexicon, MenuCommand.GameText
    });
    // Graphics
    ret[10] = new StringGroup(mnuGraphics, new String[] {
      MenuCommand.WdfEditor, MenuCommand.Textures, MenuCommand.Animations, MenuCommand.HobFiles, MenuCommand.InstallModels
    });

    // Other
    ret[11] = null;  // separator
    ret[12] = new StringGroup(mnuOther, new String[] {
      MenuCommand.Check, MenuCommand.Files, MenuCommand.Segments, MenuCommand.Modlist
    });

    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuAI:               return Language.getString("StbofMenu.38"); //$NON-NLS-1$
      case mnuBuildings:        return Language.getString("StbofMenu.41"); //$NON-NLS-1$
      case mnuGalaxy:           return Language.getString("StbofMenu.37"); //$NON-NLS-1$
      case mnuGraphics:         return Language.getString("StbofMenu.34"); //$NON-NLS-1$
      case mnuOther:            return Language.getString("StbofMenu.43"); //$NON-NLS-1$
      case mnuRaces:            return Language.getString("StbofMenu.42"); //$NON-NLS-1$
      case mnuShips:            return Language.getString("StbofMenu.40"); //$NON-NLS-1$
      case mnuStartConditions:  return Language.getString("StbofMenu.35"); //$NON-NLS-1$
      case mnuTechnology:       return Language.getString("StbofMenu.39"); //$NON-NLS-1$
      case mnuText:             return Language.getString("StbofMenu.36"); //$NON-NLS-1$
      default:                  return MenuCommand.mapCommandName(command);
    }
  }

}
