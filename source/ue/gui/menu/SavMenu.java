package ue.gui.menu;

import ue.UE;
import ue.edit.sav.SavGame;
import ue.service.Language;

/**
 * This class is a middle-man between the GUI and sav files.
 * Opens, edits, saves saved games.
 */
public class SavMenu implements EditMenu {

  // list of command menus
  public static final String mnuGALAXY = "Galaxy"; //$NON-NLS-1$
  public static final String mnuGAME = "Game"; //$NON-NLS-1$
  public static final String mnuOther = "Other"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    SavGame sav = UE.FILES.savGame();
    if (sav == null || sav.getSourceFile() == null)
      return null;

    StringGroup[] ret = new StringGroup[13];
    ret[0] = new StringGroup(MenuCommand.GameInfo);
    ret[1] = new StringGroup(MenuCommand.GameConfig);
    ret[2] = null;  // separator
    ret[3] = new StringGroup(MenuCommand.MapV2);
    ret[4] = new StringGroup(MenuCommand.StellarObj);
    ret[5] = new StringGroup(MenuCommand.Starbases);
    ret[6] = new StringGroup(MenuCommand.SystemInfo);
    ret[7] = null;  // separator
    ret[8] = new StringGroup(MenuCommand.Empires);
    ret[9] = new StringGroup(MenuCommand.Strcinfo);
    ret[10] = null;  // separator
    ret[11] = new StringGroup(MenuCommand.Check);
    ret[12] = new StringGroup(MenuCommand.Files);

    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuGALAXY:       return Language.getString("SavMenu.8"); //$NON-NLS-1$
      case mnuGAME:         return Language.getString("SavMenu.9"); //$NON-NLS-1$
      case mnuOther:        return Language.getString("SavMenu.10"); //$NON-NLS-1$
      default:              return MenuCommand.mapCommandName(command);
    }
  }

}
