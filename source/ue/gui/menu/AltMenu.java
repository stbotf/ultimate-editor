package ue.gui.menu;

import ue.UE;
import ue.edit.res.alt.Alt;
import ue.service.Language;

/**
 * This class is a middle-man between the GUI and alt.res files.
 * Opens, edits, saves alt.res files.
 */
public class AltMenu implements EditMenu {

  // list of command menus
  public static final String mnuOther = "Other"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    Alt alt = UE.FILES.alt();
    if (alt == null || alt.getSourceFile() == null)
      return null;

    StringGroup[] ret = new StringGroup[3];
    ret[0] = new StringGroup(MenuCommand.Check);
    ret[1] = new StringGroup(MenuCommand.Files);
    ret[2] = new StringGroup(MenuCommand.ModList);

    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuOther:  return Language.getString("AltMenu.3"); //$NON-NLS-1$
      default:        return MenuCommand.mapCommandName(command);
    }
  }

}
