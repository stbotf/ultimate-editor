package ue.gui.menu;

/**
 * A group of strings.
 */
public class StringGroup implements Comparable<StringGroup> {

  private String NAME; //name of the group
  private String[] STRINGS; //the strings in this group

  public StringGroup(String name) {
    NAME = name;
  }

  public StringGroup(String name, String[] str) {
    NAME = name;
    STRINGS = str;
  }

  public String getName() {
    return NAME;
  }

  public String[] getStrings() {
    return STRINGS;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof StringGroup)) {
      return false;
    }
    return (hashCode() == o.hashCode());
  }

  /**
   * Returns the file's hashcode.
   */
  @Override
  public int hashCode() {
    return NAME.hashCode();
  }

  @Override
  public int compareTo(StringGroup obj) {
    return NAME.compareTo(obj.getName());
  }
}
