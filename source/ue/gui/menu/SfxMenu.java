package ue.gui.menu;

import ue.UE;
import ue.edit.snd.Sfx;
import ue.service.Language;

/**
 * This class is a middle-man between the GUI and sfx.snd sound effects.
 * Opens, edits, saves sounds of sfx.snd.
 */
public class SfxMenu implements EditMenu {

  // list of command menus
  public static final String mnuOTHER = "Other"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    Sfx sfx = UE.FILES.sfx();
    if (sfx == null || sfx.getSourceFile() == null)
      return null;

    StringGroup[] ret = new StringGroup[4];
    ret[0] = new StringGroup(MenuCommand.GameSounds);
    ret[1] = null;  // separator
    ret[2] = new StringGroup(MenuCommand.Check);
    ret[3] = new StringGroup(MenuCommand.Modlist);

    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuOTHER:  return Language.getString("SfxMenu.3"); //$NON-NLS-1$
      default:        return MenuCommand.mapCommandName(command);
    }
  }

}
