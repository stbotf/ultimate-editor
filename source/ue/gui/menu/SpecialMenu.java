package ue.gui.menu;

import ue.service.Language;

/**
 * This class is a middle-man between the GUI and special tools.
 *
 * It provides a list of available special commands which are reachable
 * under the Tools menu and returns the corresponding panels if demanded.
 */
public class SpecialMenu implements EditMenu {

  // list of command menus
  public static final String mnuGraphics = "Graphics"; //$NON-NLS-1$
  public static final String mnuNetwork = "Network"; //$NON-NLS-1$
  public static final String mnuOther = "Other"; //$NON-NLS-1$

  /**
   * @return a String array of available commands that can be used with the edit function.
   */
  @Override
  public StringGroup[] getCommands() {
    StringGroup[] ret = new StringGroup[7];
    ret[0] = new StringGroup(MenuCommand.Calculator);
    ret[1] = new StringGroup(MenuCommand.ConvertAddresses);
    ret[2] = null;  // separator
    ret[3] = new StringGroup(MenuCommand.ConvertImage);
    ret[4] = new StringGroup(MenuCommand.CreateAniCur);
    ret[5] = null;  // separator
    ret[6] = new StringGroup(MenuCommand.SuperviseMulti);

    return ret;
  }

  /**
   * Looks up the translated menu or command display name.
   *
   * @param command the menu or command identifier to translate
   * @return the translated display name
   */
  @Override
  public String mapCommandName(String command) {
    switch (command) {
      case mnuGraphics: return Language.getString("SpecialMenu.0"); //$NON-NLS-1$
      case mnuNetwork:  return Language.getString("SpecialMenu.1"); //$NON-NLS-1$
      case mnuOther:    return Language.getString("SpecialMenu.2"); //$NON-NLS-1$
      default:          return MenuCommand.mapCommandName(command);
    }
  }

}
