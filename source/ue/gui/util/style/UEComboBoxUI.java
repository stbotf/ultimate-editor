package ue.gui.util.style;

import java.awt.Rectangle;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.metal.MetalComboBoxUI;
import lombok.Getter;
import lombok.Setter;

public class UEComboBoxUI extends MetalComboBoxUI {

  @Getter @Setter private int width;

  public UEComboBoxUI() {
    super();
  }

  public UEComboBoxUI(int width) {
    super();
    this.width = width;
  }

  @Override
  protected ComboPopup createPopup() {
    BasicComboPopup pop = new BasicComboPopup(comboBox) {
      private static final long serialVersionUID = 1484056352729892488L;

      @Override
      protected Rectangle computePopupBounds(int px, int py, int pw, int ph) {
        int w = Math.max(width, pw);
        px = px - (w - pw);

        return super.computePopupBounds(px, py, w, ph);
      }
    };
    pop.getAccessibleContext().setAccessibleParent(comboBox);
    return pop;
  }
}
