package ue.gui.util.style;

public class CheckItem {

  private String name;
  private boolean selected = false;
  private boolean editable = false;

  public CheckItem(String name, boolean selected, boolean editable) {
    this.name = name;
    this.selected = selected;
    this.editable = editable;
  }

  public boolean isSelected() {
    return selected;
  }

  public boolean isEditable() {
    return editable;
  }

  public void setSelected(boolean sel) {
    if (editable) {
      selected = sel;
    }
  }

  public void toggleSelected() {
    if (editable) {
      selected = !selected;
    }
  }

  @Override
  public String toString() {
    return name;
  }
}
