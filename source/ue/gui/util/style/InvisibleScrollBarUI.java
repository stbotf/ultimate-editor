package ue.gui.util.style;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalScrollBarUI;


public class InvisibleScrollBarUI extends MetalScrollBarUI {

  public InvisibleScrollBarUI() {
    super();
  }

  @Override
  public void paint(Graphics g, JComponent c) {
    c.removeAll();
    c.setOpaque(false);
    c.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.GRAY));
  }

  public static ComponentUI createUI(JComponent c) {
    return new InvisibleScrollBarUI();
  }

  @Override
  public void update(Graphics g, JComponent c) {
    c.removeAll();
    c.setOpaque(false);
    c.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.GRAY));
  }
}
