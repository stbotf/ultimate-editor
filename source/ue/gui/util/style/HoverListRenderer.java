package ue.gui.util.style;

import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import lombok.Getter;

public class HoverListRenderer extends DefaultListCellRenderer {

  public static Color DEFAULT_HL_COLOR = Color.CYAN;

  @Getter
  private int hoverIndex = -1;

  public boolean updateHoverIndex(int index) {
    if (hoverIndex != index) {
      hoverIndex = index;
      return true;
    }
    return false;
  }

  @Override
  public Component getListCellRendererComponent(JList<?> list, Object value, int index,
      boolean isSelected, boolean hasFocus) {

    // get default JLabel render component
    JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, hasFocus);

    Color col = hoverIndex == index ? DEFAULT_HL_COLOR : Color.white;
    label.setBackground(col);

    return label;
  }
}
