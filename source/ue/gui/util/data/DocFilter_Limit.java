package ue.gui.util.data;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * DocumentFilter to limit document input for e.g. the JTextBox.
 */
public class DocFilter_Limit extends DocumentFilter {
  private int limit;

  public DocFilter_Limit(int limit) {
    if (limit <= 0)
      throw new IllegalArgumentException("Limit must be greater than 0.");
    this.limit = limit;
  }

  @Override
  public void replace(FilterBypass fb, int offset, int remLength, String text, AttributeSet attr) throws BadLocationException {
    // text parameter is null when full text is set to null
    if (text == null)
      return;

    int currentLen = fb.getDocument().getLength();
    int newLen = currentLen + text.length() - remLength;
    int excess = newLen - limit;

    // strip excess
    if (excess > 0)
      text = text.substring(0, text.length() - excess);

    // update text
    if (text.length() > 0)
      super.replace(fb, offset, remLength, text, attr);
  }
}
