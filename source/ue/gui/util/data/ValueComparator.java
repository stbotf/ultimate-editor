package ue.gui.util.data;

import java.util.Comparator;

// comparator for sorting arbitrary values
public class ValueComparator implements Comparator<Object> {
  @Override
  public int compare(Object val1, Object val2) {
    boolean isNumber1 = val1 instanceof Number;
    boolean isNumber2 = val2 instanceof Number;

    if (isNumber1 && isNumber2) {
      if (val1 instanceof Double || val1 instanceof Float
        || val2 instanceof Double || val2 instanceof Float)
        return Double.compare(((Number)val1).doubleValue(), ((Number)val2).doubleValue());
      return Long.compare(((Number)val1).longValue(), ((Number)val2).longValue());
    }

    if (isNumber1)
      return -1;
    if (isNumber2)
      return +1;

    return val1.toString().compareTo(val2.toString());
  }
}
