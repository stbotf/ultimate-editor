package ue.gui.util.component;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;
import lombok.val;

/**
 * ScrollablePanel is a convenience class to allow inner components to adapt to an outer JScrollPane.
 * This e.g. can be utilized to scale inner JTextPane components and update the text wrapping.
 * It however forces all the subcomponents to adapt size and effectively disables the horizontal scrollbar.
 */
public class ScrollablePanel extends JPanel implements Scrollable {
  public ScrollablePanel() {
  }

  public ScrollablePanel(LayoutManager lm) {
    super(lm);
  }

  public Dimension getPreferredScrollableViewportSize() {
    val preferred = super.getPreferredSize();
    val min = super.getMinimumSize();
    return new Dimension(Integer.max(preferred.width, min.width),
        Integer.max(preferred.height, min.height));
  }

  public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
    return 16;
  }

  public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
    return 16;
  }

  public boolean getScrollableTracksViewportWidth() {
    Container parent = SwingUtilities.getUnwrappedParent(this);
    if (!(parent instanceof JViewport))
      return false;

    JViewport port = (JViewport) parent;
    int x = port.getWidth();
    Dimension min = getMinimumSize();

    // adapt to viewport width but regard min for horizontal scroll
    return x >= min.width;
  }

  public boolean getScrollableTracksViewportHeight() {
    Container parent = SwingUtilities.getUnwrappedParent(this);
    if (!(parent instanceof JViewport))
      return false;

    JViewport port = (JViewport) parent;
    int y = port.getHeight();
    Dimension pref = super.getPreferredSize();

    // adapt to viewport height but regard min & preferred height for vertical scroll
    return y >= pref.height;
  }

  public Dimension getPreferredSize() {
    Dimension pref = super.getPreferredSize();
    Container parent = SwingUtilities.getUnwrappedParent(this);
    if (!(parent instanceof JViewport))
      return pref;

    boolean trackWidth = getScrollableTracksViewportWidth();
    boolean trackHeight = getScrollableTracksViewportHeight();
    if (trackWidth && trackHeight)
      return pref;

    JViewport port = (JViewport) parent;
    Dimension min = getMinimumSize();

    if (!trackWidth) {
      int w = port.getWidth();
      if (w != 0 && w < min.width)
        pref.width = min.width;
    }
    if (!trackHeight) {
      int h = port.getHeight();
      if (h != 0 && h < min.height)
        pref.height = min.height;
    }

    return pref;
  }
}
