package ue.gui.util.component;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import lombok.Getter;
import lombok.Setter;
import ue.UE;

public class ImagePanel extends JPanel {

  private Image image;
  @Getter @Setter float alpha = 1.0f;

  public ImagePanel(String resourceFile) {
    this(UE.class.getResource(resourceFile));
  }

  public ImagePanel(URL imgUrl) {
    this(readImage(imgUrl));
  }

  public ImagePanel(Image image) {
    this.setOpaque(false);
    this.image = image;
  }

  private static Image readImage(URL imgUrl) {
    if (imgUrl != null) {
      try {
        return ImageIO.read(imgUrl);
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  @Override
  public void paintComponent(Graphics g) {
    Dimension panelSize = this.getSize();
    int imgHeight = image.getHeight(null);
    int imgWidth = image.getWidth(null);
    double aspect = panelSize.width / (double)panelSize.height;
    double relY = panelSize.height / (double)imgHeight;
    double relX = panelSize.width / (double)imgWidth;

    // scale to the lowest relation aspect
    int startX = 0;
    int startY = 0;
    int endX = 0;
    int endY = 0;
    if (relX < relY) {
      int width = (int)(imgHeight * aspect);
      startX = (imgWidth - width) / 2;
      endX = startX + width;
      endY = imgHeight;
    }
    else {
      int height = (int)(imgWidth / aspect);
      startY = (imgHeight - height) / 2;
      endX = imgWidth;
      endY = startY + height;
    }

    Graphics2D g2d = (Graphics2D) g;
    if (alpha != 1.0f) {
      AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
      g2d.setComposite(ac);
    }
    g2d.drawImage(image, 0, 0, panelSize.width, panelSize.height, startX, startY, endX, endY, null);
    if (alpha != 1.0f) {
      AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC);
      g2d.setComposite(ac);
    }

    super.paintComponent(g);
  }
}
