package ue.gui.util.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.JButton;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.swing.FontIcon;
import ue.UE;
import ue.gui.util.gfx.Icons;

/**
 * This class is used to display the about window.
 */
public class IconButton extends JButton {

  public static final int SMALL_ICON_SIZE = 18;
  public static final int MED_ICON_SIZE = 24;
  public static final Color DEFAULT_BTN_COLOR = new Color(120, 120, 120);
  public static final Color DEFAULT_BTN_HLCOLOR = new Color(50, 130, 220);
  public static final Color ALERT_COLOR = new Color(230, 85, 0);
  public static final Color ALERT_HLCOLOR = new Color(255, 138, 32);
  public static final Color ALIEN_COLOR = new Color(0, 168, 191);
  public static final Color ALIEN_HLCOLOR = new Color(0, 224, 255);
  public static final Color STARBASE_COLOR = new Color(210, 210, 210);
  public static final Color STARBASE_HLCOLOR = new Color(255, 255, 255);

  public static IconButton CreateUpButton() {
    IconButton btn = new IconButton(Icons.UP, SMALL_ICON_SIZE);
    btn.setMargin(new Insets(2,-4,0,-4));
    return btn;
  }

  public static IconButton CreateDownButton() {
    IconButton btn = new IconButton(Icons.DOWN, SMALL_ICON_SIZE);
    btn.setMargin(new Insets(4,-4,0,-4));
    return btn;
  }

  public static IconButton CreateAlertAlienButton() {
    IconButton btn = new IconButton(Icons.ALIEN, SMALL_ICON_SIZE, ALIEN_COLOR, ALIEN_HLCOLOR);
    btn.setMargin(new Insets(0,-4,-2,0));
    return btn;
  }

  public static IconButton CreateAlertStarbaseButton() {
    IconButton btn = new IconButton(Icons.STARBASE, SMALL_ICON_SIZE, STARBASE_COLOR, STARBASE_HLCOLOR);
    btn.setMargin(new Insets(0,-4,-2,0));
    return btn;
  }

  public static IconButton CreateAlertErrorButton() {
    IconButton btn = new IconButton(Icons.ALERT, SMALL_ICON_SIZE, ALERT_COLOR, ALERT_HLCOLOR);
    btn.setMargin(new Insets(0,0,-2,0));
    return btn;
  }

  public static IconButton CreateAlertButton(Color color, Color hlColor) {
    IconButton btn = new IconButton(Icons.ALERT, SMALL_ICON_SIZE, color, hlColor);
    btn.setMargin(new Insets(0,0,-2,0));
    return btn;
  }

  public static IconButton CreateEditButton() {
    IconButton btn = new IconButton(Icons.EDIT, SMALL_ICON_SIZE);
    btn.setMargin(new Insets(2,-4,0,-4));
    return btn;
  }

  public static IconButton CreateCalcButton() {
    IconButton btn = new IconButton(Icons.CALC, SMALL_ICON_SIZE);
    btn.setMargin(new Insets(2,-4,0,-4));
    return btn;
  }

  public static IconButton CreateSettingsButton() {
    IconButton btn = new IconButton(Icons.SETTINGS, SMALL_ICON_SIZE);
    btn.setMargin(new Insets(-2,-2,-5,-5));
    return btn;
  }

  public static IconButton CreateDescriptionButton() {
    IconButton btn = new IconButton(Icons.DESCRIPTIONS, SMALL_ICON_SIZE);
    btn.setMargin(new Insets(-2,-2,-5,-5));
    return btn;
  }

  public static IconButton CreateSmallButton(Ikon icon) {
    return new IconButton(icon, SMALL_ICON_SIZE);
  }

  public static IconButton CreateMediumButton(Ikon icon) {
    return new IconButton(icon, MED_ICON_SIZE);
  }

  public IconButton(String text) {
    setFont(UE.SETTINGS.getDefaultFont());
    setText(text);
  }

  public IconButton(String text, Ikon icon) {
    this(text);
    setIcon(icon, SMALL_ICON_SIZE, DEFAULT_BTN_COLOR);
    setRolloverIcon(icon, SMALL_ICON_SIZE, DEFAULT_BTN_HLCOLOR);
  }

  public IconButton(Ikon icon, int size) {
    this(icon, size, DEFAULT_BTN_COLOR, DEFAULT_BTN_HLCOLOR);
  }

  public IconButton(Ikon icon, int size, Color color, Color hlColor) {
    this(null);
    setIcon(icon, size, color);
    setIconOnly(true);
    setRolloverIcon(icon, size, hlColor);
    setDisabledIcon(icon, size, color.brighter());
    setPreferredSize(new Dimension(size, size));
  }

  public void setIcon(Ikon icon, int size, Color color) {
    FontIcon fntIcon = FontIcon.of(icon, size, color);
    setIcon(fntIcon);
  }

  public void setRolloverIcon(Ikon icon, int size, Color color) {
    FontIcon fntIcon = FontIcon.of(icon, size, color);
    setRolloverIcon(fntIcon);
  }

  public void setDisabledIcon(Ikon icon, int size, Color color) {
    FontIcon fntIcon = FontIcon.of(icon, size, color);
    this.setDisabledIcon(fntIcon);
  }

  public void setIconOnly(boolean enable) {
    // reset button spacing
    setBorderPainted(!enable);
    setContentAreaFilled(!enable);
    setMargin(enable ? new Insets(0,0,0,0) : new Insets(0,5,0,5));
  }

}
