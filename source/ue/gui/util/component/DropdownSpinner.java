package ue.gui.util.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.event.ChangeListener;
import ue.gui.util.style.UEComboBoxUI;
import ue.util.data.ID;

public class DropdownSpinner extends JPanel {

  private static final long serialVersionUID = -2860155380295398858L;

  // ui components
  private JButton[] btnCtrl = new JButton[2];
  private JTextField txtField = new JTextField(10);
  private JComboBox<ID> cmbItems = new JComboBox<ID>();
  private UEComboBoxUI cmbUI = new UEComboBoxUI();

  // data
  private SpinnerListModel model = new SpinnerListModel();
  private boolean editable = false;

  public DropdownSpinner() {
    btnCtrl[0] = new JButton("<");
    btnCtrl[1] = new JButton(">");
    txtField.setEditable(false);
    txtField.setBorder(null);

    Insets btnMargin = btnCtrl[0].getMargin();
    btnMargin.left = 5;
    btnMargin.right = 5;
    btnCtrl[0].setMargin(btnMargin);
    btnCtrl[1].setMargin(btnMargin);

    UIDefaults uidef = UIManager.getDefaults();
    Color bcolor = uidef.getColor("Button.toolBarBorderBackground");

    if (bcolor == null) {
      bcolor = new Color(-6710887);
    }
    txtField.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, bcolor));
    txtField.setHorizontalAlignment(JTextField.CENTER);

    // actions
    // prev
    btnCtrl[0].addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ID val = getPreviousValue();
        setValue(val);
      }
    });
    // next
    btnCtrl[1].addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ID val = getNextValue();
        setValue(val);
      }
    });
    // selected
    cmbItems.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED && cmbItems.isEnabled()) {
          ID selected = (ID) cmbItems.getSelectedItem();
          setValue(selected);
        }
      }
    });

    // layout
    this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    this.add(btnCtrl[0]);
    this.add(txtField);
    this.add(btnCtrl[1]);
    this.add(cmbItems);

    cmbItems.setUI(cmbUI);
  }

  public DropdownSpinner(SpinnerListModel listModel) {
    this();

    setModel(listModel);
  }

  public void setEditable(boolean val) {
    editable = val;
  }

  public synchronized ID getNextValue() {
    Object val = model.getNextValue();

    if (val == null) {
      // the model list must have at least one element
      List<?> items = model.getList();
      val = items.get(0);
    }

    return (ID) val;
  }

  public synchronized ID getPreviousValue() {
    Object val = model.getPreviousValue();

    if (val == null) {
      // the model list must have at least one element
      List<?> items = model.getList();
      val = items.get(items.size()-1);
    }

    return (ID) val;
  }

  public synchronized ID getValue() {
    Object val = model.getValue();

    if (val instanceof ID) {
      return (ID) val;
    } else {
      return null;
    }
  }

  public void addChangeListener(ChangeListener listener) {
    model.addChangeListener(listener);
  }

  public void removeChangeListener(ChangeListener listener) {
    model.removeChangeListener(listener);
  }

  public void setModel(SpinnerListModel model) {
    sortList(model);

    Object selected = model.getValue();
    if (selected != null) {
      txtField.setEditable(editable);
      txtField.setText(selected.toString());
    }

    // re-register change listeners
    if (this.model != null) {
      ChangeListener[] listeners = this.model.getChangeListeners();

      if (listeners != null) {
        for (ChangeListener l : listeners) {
          this.model.removeChangeListener(l);
          model.addChangeListener(l);
        }
      }
    }

    this.model = model;
    setCmbItems( model.getList());
  }

  public void setItems(ID[] ids) {
    setEnabled(false);
    model.setList(Arrays.asList(ids));
    setEnabled(true);

    setCmbItems(Arrays.asList(ids));
  }

  private void setCmbItems(List<?> ids) {
    cmbItems.setEnabled(false);
    cmbItems.removeAllItems();
    cmbItems.setEnabled(true);

    // first added item updates the selection
    for (Object id : ids) {
      cmbItems.addItem((ID)id);
    }

    fixCmbWidth();
  }

  public synchronized void setValue(ID val) {
    commitEdit();
    model.setValue(val);

    if (val != null) {
      txtField.setEditable(editable);
      txtField.setText(model.getValue().toString());
    }

    cmbItems.setEnabled(false);
    cmbItems.setSelectedItem(val);
    cmbItems.setEnabled(true);
  }

  public void commitEdit() {
    if (!editable)
      return;

    ID current = this.getValue();

    if (current != null) {
      String newname = txtField.getText();

      if (!newname.equals(current.toString())) {
        if (newname.length() > 0) {
          current.NAME = newname;
          sortList(model);
        }
      }
    }
  }

  private void sortList(SpinnerListModel model) {
    if (model == null)
      return;

    List<?> list = model.getList();

    Collections.sort(list, new Comparator<Object>() {
      @Override
      public int compare(Object o1, Object o2) {
        return o1.toString().compareToIgnoreCase(o2.toString());
      }
    });

    model.setList(list);
  }

  public SpinnerListModel getModel() {
    return model;
  }

  @Override
  public void setFont(Font font) {
    super.setFont(font);

    if (btnCtrl != null) {
      btnCtrl[0].setFont(font);
      btnCtrl[1].setFont(font);
      txtField.setFont(font);
    }
    if (cmbItems != null) {
      cmbItems.setFont(font);
    }
  }

  private void fixCmbWidth() {
    // reduce selection to the drop down button width
    Dimension pref = this.getPreferredSize();
    pref.width = 20;
    cmbItems.setPreferredSize(pref);

    // re-calculate and override the preferred popup items width
    int width = cmbItems.getUI().getPreferredSize(cmbItems).width;
    cmbUI.setWidth(width);
  }
}