package ue.gui.util.component;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.UE;

public class ImageButton extends JButton {

  public static final Dimension TINY_BUTTON_DIM = new Dimension(18, 18);

  @Getter @Accessors(fluent = true)
  private boolean hasIcon = false;
  private String name;

  public static ImageButton CreatePlusButton() {
    ImageButton btn = new ImageButton("+", "plus.png", "plus_hl.png"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    if (!btn.hasIcon()) {
      btn.setFont(new Font("Impact",Font.BOLD,18));
      btn.setPreferredSize(TINY_BUTTON_DIM);
    }
    return btn;
  }

  public static ImageButton CreateMinusButton() {
    ImageButton btn = new ImageButton("-", "minus.png", "minus_hl.png"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    if (!btn.hasIcon()) {
      btn.setFont(new Font("Impact",Font.BOLD,18));
      btn.setPreferredSize(TINY_BUTTON_DIM);
    }
    return btn;
  }

  public ImageButton(String name) {
    this.name = name;
    setFont(UE.SETTINGS.getDefaultFont());
    setMargin(new Insets(0,0,0,0));
    setFocusable(false);
  }

  public ImageButton(String name, String img) {
    this(name);
    setIcon(img);
  }

  public ImageButton(String name, String img, String imgHl) {
    this(name, img);
    setRolloverIcon(imgHl);
  }

  private void setIcon(String img) {
    URL imgUrl = UE.class.getResource(img);
    ImageIcon icon = imgUrl != null ? new ImageIcon(imgUrl) : null;
    hasIcon = icon != null;
    setIcon(icon);

    setBorderPainted(!hasIcon);
    setContentAreaFilled(!hasIcon);
    setText(hasIcon ? null : name);

    if (!hasIcon)
      super.setRolloverIcon(null);
  }

  public void setRolloverIcon(String img) {
    ImageIcon icon = null;
    if (hasIcon) {
      URL imgUrl = img != null ? UE.class.getResource(img) : null;
      icon = imgUrl != null ? new ImageIcon(imgUrl) : null;
    }
    super.setRolloverIcon(icon);
  }
}
