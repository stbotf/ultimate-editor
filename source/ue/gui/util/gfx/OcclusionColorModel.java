package ue.gui.util.gfx;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.ColorModel;
import java.awt.image.PackedColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import ue.util.img.ImageConversion;

/**
 * This color model allows to specify a defined color for transparency.
 */
public class OcclusionColorModel extends PackedColorModel {

  // bit masks
  private int red_mask;
  private int green_mask;
  private int blue_mask;
  private int rgb_mask;
  // bit mask offsets
  private int red_offset;
  private int green_offset;
  private int blue_offset;
  // bit mask counts
  private int red_cnt;
  private int green_cnt;
  private int blue_cnt;
  // transparency color
  private int alphaColor;

  public OcclusionColorModel(int bits, int rmask, int gmask, int bmask, int alphaColor) {
    super(ColorSpace.getInstance(ColorSpace.CS_sRGB), bits, rmask, gmask, bmask, 1 << (bits-1), true,
      Transparency.BITMASK, GraphicTools.pixelTransferType(bits));

    this.red_mask = rmask;
    this.green_mask = gmask;
    this.blue_mask = bmask;
    rgb_mask = rmask | gmask | bmask;
    red_offset   = GraphicTools.computeMaskOffset(rmask);
    green_offset = GraphicTools.computeMaskOffset(gmask);
    blue_offset  = GraphicTools.computeMaskOffset(bmask);
    red_cnt = Integer.bitCount(rmask);
    green_cnt = Integer.bitCount(gmask);
    blue_cnt = Integer.bitCount(bmask);
    this.alphaColor = alphaColor & rgb_mask;
  }

  public static OcclusionColorModel rgbModel(int bitDepth, int alphaColor) {
    switch (bitDepth) {
      case 15:
      case 16:
        return rgb16Model(alphaColor);
      case 24:
        return rgb24Model(alphaColor);
      case 32:
        return rgb32Model(alphaColor);
    }

    String err = "Invalid bit depth: " + bitDepth;
    throw new IllegalArgumentException(err);
  }

  public static OcclusionColorModel rgb16Model(int alphaColor) {
    return new OcclusionColorModel(16, ImageConversion.COLOR_MASK_16BIT_RED, ImageConversion.COLOR_MASK_16BIT_GREEN,
      ImageConversion.COLOR_MASK_16BIT_BLUE, alphaColor);
  }

  public static OcclusionColorModel rgb24Model(int alphaColor) {
    return new OcclusionColorModel(24, ImageConversion.COLOR_MASK_32BIT_RED, ImageConversion.COLOR_MASK_32BIT_GREEN,
      ImageConversion.COLOR_MASK_32BIT_BLUE, alphaColor);
  }

  public static OcclusionColorModel rgb32Model(int alphaColor) {
    return new OcclusionColorModel(32, ImageConversion.COLOR_MASK_32BIT_RED, ImageConversion.COLOR_MASK_32BIT_GREEN,
      ImageConversion.COLOR_MASK_32BIT_BLUE, alphaColor);
  }

  @Override
  final public int getRed(int pixel) {
    return ImageConversion.pixelColor(pixel, red_mask, red_offset, red_cnt);
  }

  @Override
  final public int getGreen(int pixel) {
    return ImageConversion.pixelColor(pixel, green_mask, green_offset, green_cnt);
  }

  @Override
  final public int getBlue(int pixel) {
    return ImageConversion.pixelColor(pixel, blue_mask, blue_offset, blue_cnt);
  }

  @Override
  final public int getAlpha(int pixel) {
    return (pixel & rgb_mask) == alphaColor ? 0 : 0xFF;
  }

  // override raster compatibility for BufferedImage rendering
  public boolean isCompatibleRaster(Raster raster) { return true; }
  public ColorModel coerceData (WritableRaster raster, boolean isAlphaPremultiplied) { return this; }

}
