package ue.gui.util.gfx;

import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import lombok.val;
import ue.util.data.DataTools;
import ue.util.img.PixelBuffer;

/**
 * This class provides graphic related helper routines.
 */
public abstract class GraphicTools {

  public static int computeMaskOffset(int bit_mask) {
    int off = 0;
    while ((bit_mask & 1) != 1) {
      bit_mask >>>= 1;
      off++;
    }
    return off;
  }

  public static int pixelTransferType(int pixel_bits) {
    if (pixel_bits <= 8) {
        return DataBuffer.TYPE_BYTE;
    } else if (pixel_bits <= 16) {
        return DataBuffer.TYPE_USHORT;
    } else if (pixel_bits <= 32) {
        return DataBuffer.TYPE_INT;
    } else {
        return DataBuffer.TYPE_UNDEFINED;
    }
  }

  public static Image createImage(byte[] pixelData, int width, int height, int pixelBytes, ColorModel colorModel) {
    int limit = Integer.min(pixelData.length, width * height * pixelBytes);
    val sm = colorModel.createCompatibleSampleModel(width, height);

    DataBuffer db = createDataBuffer(pixelData, limit, pixelBytes);
    WritableRaster wr = Raster.createWritableRaster(sm, db, new Point());
    return new BufferedImage(colorModel, wr, false, null);
  }

  public static Image createImage(short[] pixels, int width, int height, ColorModel colorModel) {
    val sm = colorModel.createCompatibleSampleModel(width, height);
    DataBuffer db = new DataBufferShort(pixels, pixels.length);
    WritableRaster wr = Raster.createWritableRaster(sm, db, new Point());
    return new BufferedImage(colorModel, wr, false, null);
  }

  public static Image createImage(int[] pixels, int width, int height, ColorModel colorModel) {
    val sm = colorModel.createCompatibleSampleModel(width, height);
    DataBuffer db = new DataBufferInt(pixels, pixels.length);
    WritableRaster wr = Raster.createWritableRaster(sm, db, new Point());
    return new BufferedImage(colorModel, wr, false, null);
  }

  public static Image createImage(PixelBuffer pixels, ColorModel colorModel) {
    val sm = colorModel.createCompatibleSampleModel(pixels.getWidth(), pixels.getHeight());
    DataBuffer db;

    switch (colorModel.getTransferType()) {
      case DataBuffer.TYPE_INT:
        db = new DataBufferInt(pixels.toIntArray(), pixels.getPixelCount());
        break;
      case DataBuffer.TYPE_USHORT:
        db = new DataBufferUShort(pixels.toShortArray(), pixels.getPixelCount());
        break;
      case DataBuffer.TYPE_BYTE:
        db = new DataBufferByte(pixels.toByteArray(true), pixels.getPixelCount());
        break;
      default:
        throw new RuntimeException("Illegal transfer type.");
    }

    WritableRaster wr = Raster.createWritableRaster(sm, db, new Point());
    return new BufferedImage(colorModel, wr, false, null);
  }

  private static DataBuffer createDataBuffer(byte[] pixelData, int limit, int pixelBytes) {

    switch (pixelBytes) {
      case 2: {
        short[] data = DataTools.toShortArray(pixelData, 0, limit, true);
        return new DataBufferUShort(data, data.length);
      }
      case 4: {
        int[] data = DataTools.toIntArray(pixelData, 0, limit, true);
        return new DataBufferInt(data, data.length);
      }
    }

    throw new IllegalArgumentException("Invalid pixel bytes: " + pixelBytes);
  }

}
