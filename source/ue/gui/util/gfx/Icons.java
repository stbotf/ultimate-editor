package ue.gui.util.gfx;

import java.awt.Color;
import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.materialdesign.MaterialDesign;
import org.kordamp.ikonli.swing.FontIcon;

/**
 * This class is provides some common icon definitions.
 */
public interface Icons {

  public static final int SMALL_ICON_SIZE = 18;
  public static final int MED_ICON_SIZE = 24;
  public static final Color ALERT_COLOR = new Color(230, 85, 0);

  // font icons
  // refer https://kordamp.org/ikonli/cheat-sheet-materialdesign.html
  // ui components
  public static final Ikon BUTTON = MaterialDesign.MDI_FORMAT_BOLD;
  public static final Ikon IMAGE = MaterialDesign.MDI_IMAGE;
  public static final Ikon LIST = MaterialDesign.MDI_FORMAT_LIST_BULLETED;
  public static final Ikon MENU = MaterialDesign.MDI_MENU;
  public static final Ikon SCROLL = MaterialDesign.MDI_UNFOLD_MORE;
  public static final Ikon SLIDER = MaterialDesign.MDI_RAY_VERTEX;
  public static final Ikon TEXT = MaterialDesign.MDI_FORMAT_TEXT;
  // load & save
  public static final Ikon ALERT = MaterialDesign.MDI_ALERT;
  public static final Ikon DELETE = MaterialDesign.MDI_DELETE;
  public static final Ikon DISCARD = MaterialDesign.MDI_REPLAY;
  public static final Ikon DISCARD_ALL = MaterialDesign.MDI_BACKUP_RESTORE;
  public static final Ikon EXPORT = MaterialDesign.MDI_EXPORT;
  public static final Ikon IMPORT = MaterialDesign.MDI_IMPORT;
  public static final Ikon LOAD = MaterialDesign.MDI_UPLOAD;
  public static final Ikon LOAD_ALL = MaterialDesign.MDI_AUTO_UPLOAD;
  // edit
  public static final Ikon DESCRIPTIONS = MaterialDesign.MDI_FILE_DOCUMENT_BOX;
  public static final Ikon EDIT = MaterialDesign.MDI_PENCIL;
  public static final Ikon CALC = MaterialDesign.MDI_CALCULATOR;
  public static final Ikon NUM = MaterialDesign.MDI_NUMERIC;
  public static final Ikon PATCH = MaterialDesign.MDI_FLASK_OUTLINE;
  public static final Ikon RESET = MaterialDesign.MDI_UNDO_VARIANT;
  public static final Ikon SETTINGS = MaterialDesign.MDI_SETTINGS;
  public static final Ikon ALIEN = MaterialDesign.MDI_GHOST;
  public static final Ikon STARBASE = MaterialDesign.MDI_UBUNTU;
  // navigation
  public static final Ikon UP = MaterialDesign.MDI_ARROW_UP_BOLD;
  public static final Ikon DOWN = MaterialDesign.MDI_ARROW_DOWN_BOLD;
  public static final Ikon PAGE_LEFT = MaterialDesign.MDI_ARROW_LEFT_BOLD_CIRCLE;
  public static final Ikon PAGE_RIGHT = MaterialDesign.MDI_ARROW_RIGHT_BOLD_CIRCLE;
  public static final Ikon PREV = MaterialDesign.MDI_ARROW_UP_BOLD;
  public static final Ikon REFRESH = MaterialDesign.MDI_REFRESH;
  public static final Ikon SEARCH = MaterialDesign.MDI_MAGNIFY;
  // pages
  public static final Ikon AFC = MaterialDesign.MDI_STAR;
  public static final Ikon CHANGELOG = MaterialDesign.MDI_CODE_NOT_EQUAL;
  public static final Ikon FAQ = MaterialDesign.MDI_COMMENT_QUESTION_OUTLINE;
  public static final Ikon GIT = MaterialDesign.MDI_GIT;
  public static final Ikon HELP = MaterialDesign.MDI_HELP_CIRCLE_OUTLINE;
  public static final Ikon README = MaterialDesign.MDI_CLIPBOARD_TEXT;

  public static FontIcon CreateAlertIcon() {
    return FontIcon.of(ALERT, SMALL_ICON_SIZE, ALERT_COLOR);
  }

  public static FontIcon CreateAlertIcon(Color color) {
    return FontIcon.of(ALERT, SMALL_ICON_SIZE, color);
  }

}
