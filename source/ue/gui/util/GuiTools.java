package ue.gui.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import org.apache.commons.lang3.SystemUtils;
import lombok.val;
import ue.UE;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.service.UEWorker;

public class GuiTools {

  public static Container findParentComponent(Class<?> classType, Component component, int maxDepth) {
    int depth = 0;
    Container parent = component.getParent();

    do {
      if (parent == null || classType.isInstance(parent))
        return parent;

      parent = parent.getParent();
    }
    while (maxDepth <= 0 || ++depth < maxDepth);

    return null;
  }

  public static void scrollToTableRow(JTable jTable, int row) {
    jTable.scrollRectToVisible(new Rectangle(jTable.getCellRect(row, 0, true)));
  }

  public static void addTextChangeListeners(JTextField txt, Consumer<String> callback) {
    txt.addActionListener(new CBActionListener(evt -> {
      // update value on enter
      callback.accept(txt.getText());
    }));
    txt.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {
      }
      @Override
      public void focusLost(FocusEvent event) {
        // update value on enter
        callback.accept(txt.getText());
      }
    });
  }

  public static void drawImage(Image image, int x, int y, int w, int h, Graphics g) {
    val imageW = Math.min(image.getWidth(null), w);
    val imageH = Math.min(image.getHeight(null), h);

    g.drawImage(image, x, y, x + imageW, y + imageH, 0, 0,
        imageW, imageH, null);
  }

  /**
   * Attempts to open the given URL in the system's default browser.
   *
   * @param url the URL to open.
   * @throws IOException when an error occurs while opening the browser.
   */
  public static void openInDefaultBrowser(String url) throws IOException {
    val runtime = Runtime.getRuntime();

    if (SystemUtils.IS_OS_WINDOWS) {
      runtime.exec("rundll32 url.dll,FileProtocolHandler \"" + url + "\""); //$NON-NLS-1$ //$NON-NLS-2$
    } else if (SystemUtils.IS_OS_MAC_OSX) {
      runtime.exec("open " + url); //$NON-NLS-1$
    } else {
      val browsers = Arrays.asList(
        "firefox", "iceweasel", "mozilla",  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        "chrome", "chromium", "konqueror",  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        "opera", "epiphany", "links"        //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      );

      val command = browsers.stream().map(browser -> browser + " \"" + url //$NON-NLS-1$
          + "\"").collect(Collectors.joining(" || ")); //$NON-NLS-1$ //$NON-NLS-2$

      runtime.exec(new String[]{"sh", "-c", command}); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  /**
   * Starts the worker and opens a dialog to display task status updates.
   *
   * @param worker  the worker task to be executed
   * @returns the result of the worker or null
   */
  public static <T> T runUEWorker(UEWorker<T> worker) {
    val optionPane = new JOptionPane(Language.getString("GuiTools.0"), JOptionPane.PLAIN_MESSAGE); //$NON-NLS-1$
    val cancelOption = Language.getString("GuiTools.1"); //$NON-NLS-1$
    optionPane.setOptions(new String[]{cancelOption});
    JDialog dialog = optionPane.createDialog(UE.WINDOW, UE.APP_NAME);

    worker.addPropertyChangeListener(changeEvent -> {
      String eventType = changeEvent.getPropertyName();

      switch (eventType) {
        case UEWorker.VISIBLE: {
          dialog.setVisible((Boolean) changeEvent.getNewValue());
          break;
        }
        case UEWorker.MESSAGE: {
          optionPane.setMessage(changeEvent.getNewValue());
          if (dialog.getPreferredSize().width > dialog.getSize().width)
            dialog.pack();
          break;
        }
        case UEWorker.STATUS: {
          val status = (Integer)changeEvent.getNewValue();
          switch (status) {
            case UEWorker.STATUS_DONE:
            case UEWorker.STATUS_CANCEL:
            case UEWorker.STATUS_FAILED:
              // close dialog
              dialog.dispose();
              break;
            case UEWorker.STATUS_ERROR_MSG:
            case UEWorker.STATUS_DONE_MSG:
              // add ok button to close the dialog
              // message is set via sendMessage
              val okOption = Language.getString("GuiTools.2"); //$NON-NLS-1$
              optionPane.setOptions(new String[]{okOption});
              break;
          }
          break;
        }
      }
    });

    // to ensure that the dialog is not left open without work,
    // delay worker execution for the dialog to become visible
    dialog.addComponentListener( new ComponentAdapter () {
      private boolean started = false;

      @Override
      public void componentShown(ComponentEvent e) {
        if (!started) {
          started = true;
          worker.execute();
        }
      }
    });

    // show dialog and run the blocking worker task
    dialog.setVisible(true);

    if (cancelOption.equals(optionPane.getValue()))
      worker.cancel(true);

    return worker.getResult();
  }

}
