package ue.gui.util.dialog;

import java.io.File;
import javax.swing.JOptionPane;
import lombok.val;
import lombok.experimental.UtilityClass;
import ue.service.Language;

/**
 * Contains static helper methods for displaying file dialogs.
 */
@UtilityClass
public class FileDialogs {

  // result values
  public static final int YES = 0;
  public static final int NO = 1;
  public static final int ALL = 2;
  public static final int CANCEL = -1;

  public static int confirmOverwrite(File file, boolean many) {
    String title = Language.getString("FileDialogs.0"); //$NON-NLS-1$
    String msg = Language.getString("FileDialogs.1") //$NON-NLS-1$
      .replace("%1", file.getAbsolutePath());

    int optionType = many ? JOptionPane.YES_NO_CANCEL_OPTION : JOptionPane.YES_NO_OPTION;
    String[] options = many ? new String[] {
      // overwrite all
      Language.getString("FileDialogs.2") //$NON-NLS-1$
    } : null;

    val dialog = new ConfirmDialog(title, msg, optionType, options, null);

    int res = dialog.show();
    if (res == ConfirmDialog.NO) {
      // skip if rejected to overwrite
      return NO;
    }
    else if (res != ConfirmDialog.YES) {
      // cancelled
      return CANCEL;
    }

    // check overwrite all option
    return many && dialog.getChkOptions()[0].isSelected() ? ALL : YES;
  }

}
