package ue.gui.util.dialog;

import static ue.common.FormattingConstants.EU_DATE_TIME_FORMAT;
import java.awt.Dimension;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.zip.ZipException;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import lombok.val;
import lombok.experimental.UtilityClass;
import ue.UE;
import ue.exception.InvalidArgumentsException;
import ue.exception.SavException;
import ue.exception.StackTracePrinter;
import ue.exception.UnsupportedImage;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.app.AppVersion;

/**
 * Contains static helper methods for displaying dialogs.
 */
@UtilityClass
public class Dialogs {

  public static final String DEFAULT_APP_TITLE = UE.APP_NAME;
  public static final String DEFAULT_CONFIRM_TITLE = "Please confirm";
  public final String DEFAULT_ERROR_TITLE = Language.getString("Dialogs.13"); //$NON-NLS-1$

  // confirm result values
  public static final int OK = JOptionPane.OK_OPTION; // 0
  public static final int YES = JOptionPane.YES_OPTION; // 0
  public static final int NO = JOptionPane.NO_OPTION; // 1
  public static final int CANCEL = JOptionPane.CANCEL_OPTION; // 2
  public static final int CLOSED = JOptionPane.CLOSED_OPTION; // -1

  /**
   * Used to display application errors.
   */
  public static void displayError(String message) {
    displayError(DEFAULT_ERROR_TITLE, message);
  }

  public static void displayError(String title, String message) {
    JTextArea textArea = new JTextArea(message);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);

    // wrap to a ScrollPane
    JScrollPane scrollPane = new JScrollPane(textArea);
    scrollPane.setPreferredSize(new Dimension(280,100));

    // display message box
    JOptionPane.showMessageDialog(UE.WINDOW, scrollPane, title, JOptionPane.ERROR_MESSAGE);
  }

  public static void displayError(Throwable throwable) {
    printStackTrace(throwable);
    String title = exceptionTitle(throwable);
    val message = throwable.toString();
    displayError(title, message);
  }

  /**
   * Used to display application notifications.
   */
  public static void notify(String message) {
    notify(DEFAULT_APP_TITLE, message);
  }

  public static void notify(String title, String message) {
    JOptionPane.showMessageDialog(UE.WINDOW, message, title, JOptionPane.INFORMATION_MESSAGE);
  }

  public static boolean showConfirm(String msg) {
    return showConfirm(null, msg, JOptionPane.QUESTION_MESSAGE);
  }

  public static boolean showConfirm(String title, String msg) {
    return JOptionPane.YES_OPTION == showConfirm(title, msg, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
  }

  public static boolean showConfirm(String title, String msg, int messageType) {
    return JOptionPane.YES_OPTION == showConfirm(title, msg, JOptionPane.YES_NO_OPTION, messageType);
  }

  public static int showConfirm(String title, String msg, int optionType, int messageType) {
    if (title == null)
      title = DEFAULT_CONFIRM_TITLE;

    JTextArea textArea = new JTextArea(msg);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);

    // wrap to a ScrollPane
    JScrollPane scrollPane = new JScrollPane(textArea);
    scrollPane.setPreferredSize(new Dimension(280,100));

    return JOptionPane.showConfirmDialog(UE.WINDOW, scrollPane, title, optionType, messageType);
  }

  public static int confirmError(String msg) {
    return confirmError(DEFAULT_ERROR_TITLE, msg);
  }

  public static int confirmError(String title, String msg) {
    JTextArea textArea = new JTextArea(msg);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);

    // wrap to a ScrollPane
    JScrollPane scrollPane = new JScrollPane(textArea);
    scrollPane.setPreferredSize(new Dimension(280,100));

    return JOptionPane.showConfirmDialog(UE.WINDOW, scrollPane, title, JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
  }

  public static int confirmError(Throwable throwable) {
    printStackTrace(throwable);
    String title = exceptionTitle(throwable);
    val message = throwable.toString();
    return confirmError(title, message);
  }

  public static boolean confirmOverwrite(String fileName) {
    String msg = Language.getString("Dialogs.4"); //$NON-NLS-1$
    msg = msg.replace("%1", fileName); //$NON-NLS-1$

    int respon = JOptionPane.showConfirmDialog(UE.WINDOW, msg, DEFAULT_APP_TITLE,
      JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

    return respon == JOptionPane.YES_OPTION;
  }

  private void printStackTrace(Throwable throwable) {
    AppVersion version = UE.getVersion();

    System.err.println("\n" + UE.APP_NAME + " " + version.getVersionString() //$NON-NLS-1$ //$NON-NLS-2$
      + " - " + EU_DATE_TIME_FORMAT.format(LocalDateTime.now())); //$NON-NLS-1$

    // when the SettingsManager initialization fails, UE.SETTINGS is null
    boolean filter = UE.SETTINGS != null ? UE.SETTINGS.getBooleanProperty(SettingsManager.FILTER_STACK_TRACE) : false;
    StackTracePrinter.printStackTrace(throwable, System.err, filter);
  }

  private static String exceptionTitle(Throwable throwable) {
    if (throwable instanceof InvalidArgumentsException) {
      return Language.getString("Dialogs.5"); //$NON-NLS-1$
    } else if (throwable instanceof UnsupportedImage) {
      return Language.getString("Dialogs.6"); //$NON-NLS-1$
    } else if (throwable instanceof FileNotFoundException) {
      return Language.getString("Dialogs.7"); //$NON-NLS-1$
    } else if (throwable instanceof EOFException) {
      return Language.getString("Dialogs.8"); //$NON-NLS-1$
    } else if (throwable instanceof ZipException) {
      return Language.getString("Dialogs.9"); //$NON-NLS-1$
    } else if (throwable instanceof IOException) {
      return Language.getString("Dialogs.10"); //$NON-NLS-1$
    } else if (throwable instanceof SavException) {
      return Language.getString("Dialogs.11"); //$NON-NLS-1$
    } else if (throwable instanceof NullPointerException) {
      return Language.getString("Dialogs.12"); //$NON-NLS-1$
    } else {
      return DEFAULT_ERROR_TITLE; //$NON-NLS-1$
    }
  }
}
