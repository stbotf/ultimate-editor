package ue.gui.util.dialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Arrays;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.annotation.Nullable;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import lombok.Getter;
import ue.UE;

/**
 * Contains static helper methods for displaying checkbox dialogs.
 */
public class ConfirmDialog {

  // yes or no option selected
  public static final int YES = 0;
  public static final int NO = 1;
  // abort, menu got closed or cancelled
  public static final int CANCEL = -1;

  private String title;
  private JPanel pnlOptions = new JPanel(new GridBagLayout());
  private JTextArea descOptions = new JTextArea();
  private JLabel lblFooter = new JLabel();
  private int optionType;

  @Getter
  private JCheckBox[] chkOptions;

  public ConfirmDialog(String title, String desc, int optionType, @Nullable String[] options, @Nullable String footer) {
    this.title = title;
    this.optionType = optionType;

    setup(options);
    placeComponents();

    descOptions.setText(desc);
    lblFooter.setText(footer);
  }

  public int show() {
    int response = JOptionPane.showConfirmDialog(UE.WINDOW, pnlOptions, title, optionType);
    return response == JOptionPane.YES_OPTION ? YES :
      response == JOptionPane.NO_OPTION ? NO : CANCEL;
  }

  private void setup(@Nullable String[] options) {
    descOptions.setLineWrap(true);
    descOptions.setWrapStyleWord(true);
    descOptions.setEditable(false);
    descOptions.setBackground(null);
    descOptions.setColumns(40);
    descOptions.setSize(descOptions.getPreferredSize().width, 1);

    chkOptions = options != null ? Arrays.stream(options)
      .map(x -> new JCheckBox(x)).toArray(JCheckBox[]::new) : null;
  }

  private void placeComponents() {
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;

    c.insets.set(5, 5, 5, 5);
    pnlOptions.add(descOptions, c);

    if (chkOptions != null) {
      c.insets.set(0, 0, 0, 0);
      for (JCheckBox chk : chkOptions) {
        c.gridy++;
        pnlOptions.add(chk, c);
      }
    }

    if (lblFooter != null) {
      c.insets.set(5, 5, 0, 5);
      c.gridy++;
      pnlOptions.add(lblFooter, c);
    }
  }

}
