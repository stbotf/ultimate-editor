package ue.gui.util.dialog;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.RowSorter;
import javax.swing.Timer;
import javax.swing.table.TableModel;
import lombok.Getter;
import lombok.Setter;
import ue.gui.util.GuiTools;
import ue.gui.util.event.CBActionListener;
import ue.util.data.StringTools;

public class SearchBox extends JTextField {
  public final KeyAdapter SearchKeyListener = new KeyAdapter() {
    @Override
    public void keyTyped(KeyEvent e) {
      onKeyEvent(e);
    }
  };

  private final Timer searchReset = new Timer(2000, new CBActionListener(x -> hidePopup()));

  private Popup popSearch;
  private boolean popupShowing = false;
  private String searchStr = "";
  private Component owner;
  private Component displayParent;

  // used with JTables
  @Getter @Setter
  private int searchColumn = 0;

  public SearchBox(Component owner, int columns) {
    super(columns);
    this.owner = owner;

    // find scroll panel container if any
    displayParent = GuiTools.findParentComponent(JScrollPane.class, owner, 2);
    if (displayParent == null)
      displayParent = owner;

    addListeners();
  }

  protected void addListeners() {
    owner.addKeyListener(SearchKeyListener);
    if (displayParent != owner)
      displayParent.addKeyListener(SearchKeyListener);

    addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {
        searchReset.stop();
        searchStr = getText();

        // initial search
        selectNextTableItem();
        searchReset.start();
      }

      @Override
      public void focusLost(FocusEvent e) {
        searchReset.stop();
        hidePopup();
      }
    });

    addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent e) {
        searchReset.stop();

        if (e.getKeyChar() == KeyEvent.CHAR_UNDEFINED) {
          hidePopup();
        } else {
          char ch = e.getKeyChar();
          searchStr = searchStr + ch;

          // find string
          selectNextTableItem();
          searchReset.start();
        }
      }
    });
  }

  public boolean onKeyEvent(KeyEvent e) {
    int modifiers = e.getModifiers();

    if (!popupShowing && (modifiers & (KeyEvent.CTRL_MASK|KeyEvent.ALT_MASK|KeyEvent.META_MASK)) == 0) {
      char ch = e.getKeyChar();

      // whenever a valid key is pressed, open the search box
      // but ignore tab key to allow move the focus
      if (ch != '\t' && StringTools.isTextCharacter(ch)) {
        setText(Character.toString(ch));

        Point pt = displayParent.getLocationOnScreen();
        Dimension d = getPreferredSize();
        int x = pt.x + displayParent.getWidth() - d.width;
        int y = pt.y + displayParent.getHeight() - d.height;

        popSearch = PopupFactory.getSharedInstance().getPopup(displayParent, this, x, y);
        popSearch.show();
        popupShowing = true;

        requestFocus();
        return true;
      }
    }

    hidePopup();
    return false;
  }

  public void hidePopup() {
    searchStr = "";
    popupShowing = false;
    if (popSearch != null)
      popSearch.hide();
  }

  private void selectNextTableItem() {
    if (owner instanceof JTable) {
      JTable table = (JTable) owner;

      // find string
      RowSorter<? extends TableModel> sorter = table.getRowSorter();
      int i = Math.max(0, table.getSelectedRow());

      for (int k = 0; k < table.getRowCount(); k++) {
        if (i == sorter.convertRowIndexToModel(k)) {
          i = k;
          break;
        }
      }

      for (int k = 1; k < table.getRowCount(); k++) {
        int index = sorter.convertRowIndexToModel((k + i) % table.getRowCount());
        String item = table.getValueAt(index, searchColumn).toString();

        if (item.startsWith(searchStr)) {
          table.changeSelection(index, searchColumn, false, false);
          break;
        }
      }
    }
  }
}
