package ue.gui.util.dialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Arrays;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import lombok.Getter;
import ue.UE;

/**
 * Contains static helper methods for displaying radio button dialogs.
 */
public class OptionsDialog {

  private String title;
  private JPanel pnlOptions = new JPanel(new GridBagLayout());
  private JTextArea descOptions = new JTextArea();
  private JLabel lblFooter = new JLabel();

  @Getter
  private JRadioButton[] rbnOptions;

  public OptionsDialog(String title, String desc, String[] options, String footer) {
    this.title = title;

    setup(options);
    placeComponents();

    descOptions.setText(desc);
    lblFooter.setText(footer);

    // default to first option
    rbnOptions[0].setSelected(true);
  }

  public int show() {
    int response = JOptionPane.showConfirmDialog(UE.WINDOW, pnlOptions, title, JOptionPane.OK_CANCEL_OPTION);
    if (response != JOptionPane.OK_OPTION) {
      // abort, menu got closed or cancelled
      return -1;
    }

    for (int i = 0; i < rbnOptions.length; ++i)
      if (rbnOptions[i].isSelected())
        return i;

    // no option selected
    return -1;
  }

  private void setup(String[] options) {
    descOptions.setLineWrap(true);
    descOptions.setWrapStyleWord(true);
    descOptions.setEditable(false);
    descOptions.setBackground(null);
    descOptions.setColumns(40);
    descOptions.setSize(descOptions.getPreferredSize().width, 1);

    rbnOptions = Arrays.stream(options)
      .map(x -> new JRadioButton(x)).toArray(JRadioButton[]::new);

    ButtonGroup rbgGroup = new ButtonGroup();
    for (JRadioButton rbn : rbnOptions)
      rbgGroup.add(rbn);
  }

  private void placeComponents() {
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;

    c.insets.set(5, 5, 5, 5);
    pnlOptions.add(descOptions, c);
    c.insets.set(0, 0, 0, 0);
    for (JRadioButton rbn : rbnOptions) {
      c.gridy++;
      pnlOptions.add(rbn, c);
    }
    c.insets.set(5, 5, 0, 5);
    c.gridy++;
    pnlOptions.add(lblFooter, c);
  }

}
