package ue.gui.util.event;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import ue.gui.util.dialog.Dialogs;
import ue.util.func.FunctionTools;
import ue.util.func.FunctionTools.CheckedConsumer;

/**
 * Wrapper class for the ListSelectionListener.
 * Allows for lambda callbacks that throw checked exceptions.
 */
public class CBListSelectionListener implements ListSelectionListener {
  private FunctionTools.CheckedConsumer<ListSelectionEvent, Exception> cb;

  public CBListSelectionListener(CheckedConsumer<ListSelectionEvent, Exception> cb) {
    this.cb = cb;
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
    try {
      cb.accept(e);
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }
}
