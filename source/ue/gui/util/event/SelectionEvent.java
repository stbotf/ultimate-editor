package ue.gui.util.event;

import java.awt.Point;

public class SelectionEvent {

  private int[] index;
  private Point location;

  public SelectionEvent(int[] index, Point location) {
    this.index = index;
    this.location = location;
  }

  public int[] getSelectedIndex() {
    return index;
  }

  public Point getLocation() {
    return location;
  }
}
