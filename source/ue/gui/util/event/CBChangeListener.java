package ue.gui.util.event;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import ue.gui.util.dialog.Dialogs;
import ue.util.func.FunctionTools;
import ue.util.func.FunctionTools.CheckedConsumer;

/**
 * Wrapper class for the ChangeListener.
 * Allows for lambda callbacks that throw checked exceptions.
 */
public class CBChangeListener implements ChangeListener {
  private FunctionTools.CheckedConsumer<ChangeEvent, Exception> cb;

  public CBChangeListener(CheckedConsumer<ChangeEvent, Exception> cb) {
    this.cb = cb;
  }

  @Override
  public void stateChanged(ChangeEvent e) {
    try {
      cb.accept(e);
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }
}
