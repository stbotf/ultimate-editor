package ue.gui.util.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ue.gui.util.dialog.Dialogs;
import ue.util.func.FunctionTools;
import ue.util.func.FunctionTools.CheckedConsumer;

/**
 * Wrapper class for the ActionListener.
 * Allows for lambda callbacks that throw checked exceptions.
 */
public class CBActionListener implements ActionListener {
  private FunctionTools.CheckedConsumer<ActionEvent, Exception> cb;

  public CBActionListener(CheckedConsumer<ActionEvent, Exception> cb) {
    this.cb = cb;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    try {
      cb.accept(e);
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }
}
