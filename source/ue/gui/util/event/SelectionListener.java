package ue.gui.util.event;

public interface SelectionListener {

  public void selected(SelectionEvent event);
}
