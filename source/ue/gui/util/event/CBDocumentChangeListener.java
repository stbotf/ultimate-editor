package ue.gui.util.event;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import ue.gui.util.dialog.Dialogs;
import ue.util.func.FunctionTools;
import ue.util.func.FunctionTools.CheckedConsumer;

/**
 * Wrapper class for the DocumentListener.
 * Allows for lambda callbacks that throw checked exceptions.
 */
public class CBDocumentChangeListener implements DocumentListener {
  private FunctionTools.CheckedConsumer<DocumentEvent, Exception> cb;

  public CBDocumentChangeListener(CheckedConsumer<DocumentEvent, Exception> cb) {
    this.cb = cb;
  }

  @Override
  public void insertUpdate(DocumentEvent e) {
    try {
      cb.accept(e);
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }

  @Override
  public void removeUpdate(DocumentEvent e) {
    try {
      cb.accept(e);
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    }
  }

  @Override
  public void changedUpdate(DocumentEvent e) {
    // called on attribute changes like font and size
  }
}
