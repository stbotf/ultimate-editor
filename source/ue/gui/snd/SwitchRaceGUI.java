package ue.gui.snd;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Vector;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Converse;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.snd.English;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

/**
 * @author Alan Podlesek
 */
public class SwitchRaceGUI extends MainPanel {

  // read
  private Stbof stbof;
  private RaceRst RACE;

  // edited
  private Converse converse;

  // ui components
  private Vector<JComboBox<ID>> cmbRace = new Vector<JComboBox<ID>>(4);
  private JButton btnSwitch = new JButton(Language.getString("SwitchRaceGUI.0")); //$NON-NLS-1$
  private JButton btnSet = new JButton(Language.getString("SwitchRaceGUI.1")); //$NON-NLS-1$
  private JLabel lblUse = new JLabel(Language.getString("SwitchRaceGUI.2")); //$NON-NLS-1$
  private JLabel lblSet = new JLabel(Language.getString("SwitchRaceGUI.1")); //$NON-NLS-1$
  private JLabel lblSwitch = new JLabel(Language.getString("SwitchRaceGUI.3")); //$NON-NLS-1$

  // data
  private Timer tReEnableButtons;

  public SwitchRaceGUI(English english, Stbof stbof) throws IOException {
    this.stbof = stbof;
    converse = (Converse) stbof.getInternalFile(CStbofFiles.ConverseBin, true);

    if (stbof != null) {
      RACE = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    }

    tReEnableButtons = new Timer(2000, new ActionListener() // 2 sec cool-off time
    {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        btnSwitch.setEnabled(true);
        btnSet.setEnabled(true);
      }
    });
    tReEnableButtons.setRepeats(false);

    setupComponents();
    placeComponents();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.SwitchRaces;
  }

  private void placeComponents() {
    this.setLayout(new BorderLayout());
    JPanel mothership = new JPanel(new GridLayout(0, 1));
    JPanel line1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
    JPanel line2 = new JPanel(new FlowLayout(FlowLayout.CENTER));

    line1.add(lblSwitch);
    line1.add(cmbRace.elementAt(0));
    line1.add(cmbRace.elementAt(1));
    line1.add(btnSwitch);
    line2.add(lblSet);
    line2.add(cmbRace.elementAt(2));
    line2.add(lblUse);
    line2.add(cmbRace.elementAt(3));
    line2.add(btnSet);

    mothership.add(line1);
    mothership.add(line2);

    add(mothership, BorderLayout.NORTH);
    add(Box.createVerticalGlue(), BorderLayout.CENTER);
  }

  private void setupComponents() {
    Font fnt = UE.SETTINGS.getDefaultFont();

    // combos
    for (int i = 0; i < 4; i++) {
      JComboBox<ID> cmb = new JComboBox<ID>();
      cmb.setFocusable(false);
      cmb.setFont(fnt);

      for (int r = 0; r < 5; r++) {
        cmb.addItem(new ID(RACE.getName(r), r));
      }

      cmbRace.add(cmb);
    }

    // button
    btnSwitch.setFont(fnt);
    btnSwitch.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // prevent click happyness
        btnSwitch.setEnabled(false);
        btnSet.setEnabled(false);
        tReEnableButtons.start();

        if (cmbRace.elementAt(0).getSelectedIndex() >= 0
          || cmbRace.elementAt(1).getSelectedIndex() >= 0) {
          try {
            converse.switchRaces(
              (short) (cmbRace.elementAt(0).getSelectedIndex()),
              (short) (cmbRace.elementAt(1).getSelectedIndex()),
              true);
          } catch (Exception f) {
            Dialogs.displayError(f);
          }
        }
      }
    });

    // button 2
    btnSet.setFont(fnt);
    btnSet.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // prevent click happyness
        btnSwitch.setEnabled(false);
        btnSet.setEnabled(false);
        tReEnableButtons.start();

        if (cmbRace.elementAt(2).getSelectedIndex() >= 0
          || cmbRace.elementAt(3).getSelectedIndex() >= 0) {
          try {
            converse.switchRaces(
              (short) (cmbRace.elementAt(2).getSelectedIndex()),
              (short) (cmbRace.elementAt(3).getSelectedIndex()),
              false);
          } catch (Exception f) {
            Dialogs.displayError(f);
          }
        }
      }
    });

    // labels
    lblUse.setFont(fnt);
    lblSet.setFont(fnt);
    lblSwitch.setFont(fnt);

    // clean up
    // is supposed to hint GC it's ok to remove the item
    // in any case it can't hurt
    fnt = null;
  }

  @Override
  public boolean hasChanges() {
    return converse.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(converse, changes);
  }

  @Override
  public void reload() throws IOException {
    converse = (Converse) stbof.getInternalFile(CStbofFiles.ConverseBin, true);
  }

  /* (non-Javadoc)
   * @see ue.gui.MainPanel#finalWarning()
   */
  @Override
  public void finalWarning() {
  }
}
