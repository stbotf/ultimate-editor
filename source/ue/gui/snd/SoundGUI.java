package ue.gui.snd;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.snd.SndFile;
import ue.exception.KeyNotFoundException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.data.ID;
import ue.util.file.PathHelper;

public class SoundGUI extends MainPanel {

  // strings
  private final String strSTARTING_TASK = Language.getString("SoundGUI.0"); //$NON-NLS-1$
  private final String strCANCEL = Language.getString("SoundGUI.1"); //$NON-NLS-1$
  private final String strEXTRACT = Language.getString("SoundGUI.2"); //$NON-NLS-1$
  private final String strSAVE_AS = Language.getString("SoundGUI.3"); //$NON-NLS-1$
  private final String strSELECT_DESTINATION = Language.getString("SoundGUI.4"); //$NON-NLS-1$

  interface RaceSelections {
    static final int Unknown = 0;
    static final int Neutral = 1;
    static final int Cardassians = 2;
    static final int Federation = 3;
    static final int Ferengi = 4;
    static final int Klingons = 5;
    static final int Romulans = 6;
    static final int Unused = 7;
  }

  // read
  private RaceRst RACE;

  // edited
  private SndFile SND;

  // ui components
  private JList<ID> lstSND = new JList<ID>();
  private JTextArea txtDESC = new JTextArea();
  private JComboBox<ID> cmbRACE = new JComboBox<ID>();
  private JComboBox<ID> cmbTYPE = new JComboBox<ID>();
  private JComboBox<ID> cmbSRACE = new JComboBox<ID>();
  private JComboBox<ID> cmbSTYPE = new JComboBox<ID>();
  private JTextField txtSEARCH = new JTextField();
  private JLabel lblINDEX = new JLabel();
  private JButton btnPLAY = new JButton(Language.getString("SoundGUI.8")); //$NON-NLS-1$
  private JButton btnEXTRACT = new JButton(strEXTRACT);
  private JButton btnEXTRACT_ALL = new JButton(Language.getString("SoundGUI.5")); //$NON-NLS-1$
  private JButton btnREPLACE = new JButton(Language.getString("SoundGUI.6")); //$NON-NLS-1$
  private JButton btnCREATE = new JButton(Language.getString("SoundGUI.7")); //$NON-NLS-1$
  private JButton btnDESC = new JButton(Language.getString("SoundGUI.9")); //$NON-NLS-1$
  private JButton btnFIND = new JButton(Language.getString("SoundGUI.10")); //$NON-NLS-1$
  private JButton btnFIND_NEXT = new JButton(Language.getString("SoundGUI.11")); //$NON-NLS-1$
  private JLabel lblSEARCH = new JLabel(Language.getString("SoundGUI.12")); //$NON-NLS-1$

  // data
  @Getter @Accessors(fluent = true)
  private String menuCommand;
  private int SELECTED = -1;
  private boolean IGNORE = false;
  private int[] SEARCH = null;
  private int CURSEA = 0;

  public SoundGUI(SndFile snd, Stbof st, String command) throws IOException {
    RACE = (RaceRst) st.getInternalFile(CStbofFiles.RaceRst, true);
    SND = snd;
    menuCommand = command;

    setupComponents();
    placeComponents();
    addListeners();
    fillList();

    lstSND.setSelectedIndex(0);
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();
    btnEXTRACT.setFont(def);
    btnEXTRACT_ALL.setFont(def);
    btnREPLACE.setFont(def);
    btnCREATE.setFont(def);
    btnPLAY.setFont(def);
    btnDESC.setFont(def);
    btnFIND.setFont(def);
    btnFIND_NEXT.setFont(def);
    lstSND.setFont(def);
    txtDESC.setFont(def);
    cmbTYPE.setFont(def);
    cmbRACE.setFont(def);
    cmbSTYPE.setFont(def);
    cmbSRACE.setFont(def);
    txtSEARCH.setFont(def);
    lblSEARCH.setFont(def);
    lblINDEX.setFont(def);

    lstSND.setFixedCellWidth(50);
    txtSEARCH.setColumns(20);
    txtDESC.setLineWrap(true);
    txtDESC.setWrapStyleWord(true);

    //races
    String strUnknown = Language.getString("SoundGUI.18"); //$NON-NLS-1$
    String strNeutral = Language.getString("SoundGUI.19"); //$NON-NLS-1$
    cmbSRACE.addItem(new ID(strUnknown, 0));
    cmbSRACE.addItem(new ID(strNeutral, 1));
    cmbRACE.addItem(new ID(strUnknown, RaceSelections.Unknown));
    cmbRACE.addItem(new ID(strNeutral, RaceSelections.Neutral));
    for (int i = 0; i < 5; i++) {
      cmbSRACE.addItem(new ID(RACE.getName(i), i + 2));
      cmbRACE.addItem(new ID(RACE.getName(i), i + 2));
    }
    String strUnused = Language.getString("SoundGUI.31"); //$NON-NLS-1$
    cmbRACE.addItem(new ID(strUnused, RaceSelections.Unused));
    cmbSRACE.addItem(new ID(Language.getString("SoundGUI.31"), 7)); //$NON-NLS-1$

    //type
    cmbSTYPE.addItem(new ID(strUnknown, 0));
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.20"), 1)); //$NON-NLS-1$
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.21"), 2)); //$NON-NLS-1$
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.22"), 3)); //$NON-NLS-1$
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.23"), 4)); //$NON-NLS-1$
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.24"), 5)); //$NON-NLS-1$
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.25"), 6)); //$NON-NLS-1$
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.26"), 7)); //$NON-NLS-1$
    cmbSTYPE.addItem(new ID(Language.getString("SoundGUI.27"), 8)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(strUnknown, 0));
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.20"), 1)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.21"), 2)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.22"), 3)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.23"), 4)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.24"), 5)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.25"), 6)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.26"), 7)); //$NON-NLS-1$
    cmbTYPE.addItem(new ID(Language.getString("SoundGUI.27"), 8)); //$NON-NLS-1$
  }

  private void placeComponents() {
    JScrollPane scrSND = new JScrollPane(lstSND);
    scrSND.setPreferredSize(new Dimension(80, 240));
    JScrollPane scrDESC = new JScrollPane(txtDESC);
    scrDESC.setPreferredSize(new Dimension(160, 200));

    JPanel sndOptions = new JPanel(new GridBagLayout());
    {
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.anchor = GridBagConstraints.NORTH;
      c.insets.left = 5;
      c.gridx = 0;
      c.gridy = 0;

      sndOptions.add(btnEXTRACT, c);
      c.insets.top = 5;
      c.gridy++;
      sndOptions.add(btnREPLACE, c);
      c.gridy++;
      sndOptions.add(btnEXTRACT_ALL, c);
      c.gridy++;
      sndOptions.add(btnCREATE, c);
      c.gridy++;
      sndOptions.add(btnPLAY, c);
      c.gridy++;
      sndOptions.add(btnDESC, c);
      c.gridy++;
      sndOptions.add(lblINDEX, c);
      c.gridy++;
      c.weighty = 1;
      sndOptions.add(Box.createVerticalGlue(), c);
    }

    JPanel descOptions = new JPanel(new GridBagLayout());
    {
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.insets.right = 5;
      c.gridx = 0;
      c.gridy = 0;

      descOptions.add(cmbRACE, c);
      c.gridx++;
      descOptions.add(cmbTYPE, c);
      c.insets.top = 5;
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 2;
      c.weighty = 1;
      descOptions.add(scrDESC, c);
    }

    JPanel searchPanel = new JPanel(new GridBagLayout());
    {
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.insets.top = 5;
      c.gridx = 0;
      c.gridy = 0;

      searchPanel.add(lblSEARCH, c);
      c.insets.top = 0;
      c.gridy++;
      c.weightx = 1;
      searchPanel.add(txtSEARCH, c);
      c.insets.left = 5;
      c.gridx++;
      c.weightx = 0;
      searchPanel.add(cmbSRACE, c);
      c.gridx++;
      searchPanel.add(cmbSTYPE, c);
      c.gridx++;
      searchPanel.add(btnFIND, c);
      c.gridx++;
      searchPanel.add(btnFIND_NEXT, c);
    }

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weighty = 1;

    // main panel
    setLayout(new GridBagLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add(descOptions, c);
    c.gridx++;
    c.weightx = 1;
    // set minimum width to list english/german.snd sound details
    add(Box.createHorizontalStrut(200), c);
    add(scrSND, c);
    c.gridx++;
    c.weightx = 0;
    add(sndOptions, c);
    c.gridx = 0;
    c.gridy++;
    c.gridwidth = 3;
    c.weightx = 1;
    c.weighty = 0;
    add(searchPanel, c);
  }

  private void addListeners() {
    lstSND.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (IGNORE)
          return;
        if (lstSND.getSelectedIndex() < 0)
          return;

        //save
        try {
          finalWarning();
        } catch (Exception ex) {
          // really bad stuff happening - bail
          Dialogs.displayError(ex);
          return;
        }

        // cache & unset selected to skip change events
        int sel = lstSND.getSelectedIndex();
        SELECTED = -1;

        String strIndex = Language.getString("SoundGUI.13") //$NON-NLS-1$
          .replace("%1", Integer.toString(sel)); //$NON-NLS-1$
        int race = SND.getRaceDescriptor(sel);
        int type = SND.getTypeDescriptor(sel);
        String desc = SND.getDescription(sel);

        lblINDEX.setText(strIndex);
        cmbRACE.setSelectedIndex(race);
        cmbTYPE.setSelectedIndex(type);
        txtDESC.setText(desc);

        // restore selected
        SELECTED = sel;
      }
    });

    cmbTYPE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (IGNORE || SELECTED < 0 || cmbTYPE.getSelectedIndex() < 0)
          return;

        SND.setTypeDescriptor(SELECTED, (short) cmbTYPE.getSelectedIndex());
      }
    });

    cmbRACE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (IGNORE || SELECTED < 0 || cmbRACE.getSelectedIndex() < 0)
          return;

        try {
          SND.setRaceDescriptor(SELECTED, (short) cmbRACE.getSelectedIndex());
        }
        catch (KeyNotFoundException e) {
          cmbRACE.setSelectedIndex(RaceSelections.Unused);
          String title = "Can't change the voice race";
          String msg = "Setting the race on unused descriptions is not supported,"
            + " since they lack the voice group entry and there is no support"
            + " yet to do any group assignments.";
          Dialogs.displayError(title, msg);
        }
        catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    btnFIND.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        SEARCH = null;
        CURSEA = 0;

        ArrayList<Integer> into = new ArrayList<Integer>();
        String str = txtSEARCH.getText().toLowerCase();
        int rac = cmbSRACE.getSelectedIndex();
        int typ = cmbSTYPE.getSelectedIndex();
        int numbeOfSounds = SND.getNumberOfSounds();

        for (int i = 0; i < numbeOfSounds; i++) {
          //race
          if (SND.getRaceDescriptor(i) == rac || rac == 0) {
            if (SND.getTypeDescriptor(i) == typ || typ == 0) {
              String desc = SND.getDescription(i);
              if (str.isEmpty() || desc != null && desc.toLowerCase().contains(str)) {
                into.add(i);
              }
            }
          }
        }

        if (into.size() > 0) {
          SEARCH = into.stream().mapToInt(x->x).toArray();
          lstSND.setSelectedIndex(SEARCH[0]);
          lstSND.ensureIndexIsVisible(SEARCH[0]);
        }
      }
    });

    btnFIND_NEXT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SEARCH == null)
          return;

        CURSEA++;
        if (CURSEA >= SEARCH.length)
          CURSEA = 0;

        lstSND.setSelectedIndex(SEARCH[CURSEA]);
        lstSND.ensureIndexIsVisible(SEARCH[CURSEA]);
      }
    });

    btnEXTRACT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        fc.setDialogTitle(UE.APP_NAME + " - " + strSELECT_DESTINATION); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);
        int returnVal = fc.showDialog(UE.WINDOW, strEXTRACT);
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File fil = fc.getSelectedFile();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, fil.getPath());

        JOptionPane opti = new JOptionPane(strSTARTING_TASK, JOptionPane.PLAIN_MESSAGE);
        opti.setOptions(new String[]{strCANCEL});

        ExtractThread ext = new ExtractThread(opti, lstSND.getSelectedIndices(), fil);
        ext.setPriority(Thread.MIN_PRIORITY);
        ext.setDaemon(true);
        ext.start();

        JDialog dia = opti.createDialog(UE.WINDOW, UE.APP_NAME);
        dia.setVisible(true);

        if ((opti.getValue()).equals(strCANCEL)) {
          ext.endNow();
          ext = null;
          System.gc();
        }
      }
    });

    btnDESC.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        fc.setDialogTitle(UE.APP_NAME + " - " + strSAVE_AS); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(false);
        int returnVal = fc.showDialog(UE.WINDOW, Language.getString("SoundGUI.14")); //$NON-NLS-1$
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File fil = fc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(fil).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        try (FileOutputStream out = new FileOutputStream(fil)) {
          SND.exportDescriptions(out, SND.getNumberOfSounds(), RACE);
        } catch (Exception z) {
          Dialogs.displayError(z);
        }
      }
    });

    btnEXTRACT_ALL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setApproveButtonText(Language.getString("SoundGUI.2")); //$NON-NLS-1$
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        fc.setDialogTitle(UE.APP_NAME +
            " - " + strSELECT_DESTINATION); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);
        int returnVal = fc.showDialog(UE.WINDOW, strEXTRACT);
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File fil = fc.getSelectedFile();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, fil.getPath());

        JOptionPane opti = new JOptionPane(strSTARTING_TASK, JOptionPane.PLAIN_MESSAGE);
        opti.setOptions(new String[]{strCANCEL});

        int[] ij = new int[SND.getNumberOfSounds()];
        for (int i = 0; i < ij.length; i++) {
          ij[i] = i;
        }

        ExtractThread ext = new ExtractThread(opti, ij, fil);
        ext.setPriority(Thread.MIN_PRIORITY);
        ext.setDaemon(true);
        ext.start();

        JDialog dia = opti.createDialog(UE.WINDOW, UE.APP_NAME);
        dia.setVisible(true);

        if ((opti.getValue()).equals(strCANCEL)) {
          ext.endNow();
          ext = null;
          System.gc();
        }
      }
    });

    btnCREATE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        fc.setDialogTitle(UE.APP_NAME + " - " + strSAVE_AS); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(false);
        FileFilter filter = new FileFilter() {

          @Override
          public boolean accept(File f) {
            if (f.isDirectory())
              return true;
            String name = f.getName();
            name = name.toLowerCase();
            return name.endsWith(".wav"); //$NON-NLS-1$
          }

          @Override
          public String getDescription() {
            return Language.getString("SoundGUI.15"); //$NON-NLS-1$
          }
        };

        fc.setFileFilter(filter);
        int returnVal = fc.showDialog(UE.WINDOW, Language.getString("SoundGUI.16")); //$NON-NLS-1$
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File fil = fc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(fil).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        try {
          SND.thinkBig(fil);
        } catch (Exception zu) {
          Dialogs.displayError(zu);
        }
      }
    });

    btnREPLACE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (lstSND.getSelectedIndex() < 0)
          return;

        JFileChooser fc = new JFileChooser();
        fc.setApproveButtonText(Language.getString("SoundGUI.6")); //$NON-NLS-1$
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        String title = Language.getString("SoundGUI.17"); //$NON-NLS-1$
        fc.setDialogTitle(UE.APP_NAME + " - " + title); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(false);
        FileFilter filter = new FileFilter() {

          @Override
          public boolean accept(File f) {
            if (f.isDirectory())
              return true;

            String name = f.getName();
            name = name.toLowerCase();
            return name.endsWith(".wav"); //$NON-NLS-1$
          }

          @Override
          public String getDescription() {
            return Language.getString("SoundGUI.15"); //$NON-NLS-1$
          }
        };
        fc.setFileFilter(filter);
        int returnVal = fc.showDialog(UE.WINDOW, Language.getString("SoundGUI.6")); //$NON-NLS-1$
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File fil = fc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(fil).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        try {
          SND.replace(SELECTED, fil);
        } catch (Exception zu) {
          Dialogs.displayError(zu);
        }
      }
    });

    btnPLAY.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (lstSND.getSelectedIndex() < 0)
          return;

        try {
          SND.play(lstSND.getSelectedIndex());
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });
  }

  private void fillList() {
    btnPLAY.setEnabled(SND.isPlayable());

    int h = SND.getNumberOfSounds();
    ID[] str = new ID[h];
    for (int i = 0; i < h; i++) {
      String tut = SND.getShortName(i);
      str[i] = new ID(tut, i);
    }

    // restore selection
    int prev = lstSND.getSelectedIndex();
    lstSND.setListData(str);
    if (prev >= 0 && prev < str.length)
      lstSND.setSelectedIndex(prev);
  }

  @Override
  public boolean hasChanges() {
    return SND.isChanged();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    if (SND.isChanged())
      changes.add(null, SND.getName());
  }

  @Override
  public void reload() throws IOException {
    UE.WINDOW.openFile(SND.getSourceFile());
    SND = (SndFile) UE.FILES.getPrimaryFile();
    fillList();
  }

  @Override
  public void finalWarning() {
    if (SELECTED < 0)
      return;

    IGNORE = true;
    String str = txtDESC.getText();
    SND.setDescription(SELECTED, str);
    int j = lstSND.getSelectedIndex();
    fillList();
    lstSND.setSelectedIndex(j);
    IGNORE = false;
  }

  private class ExtractThread extends Thread {

    private JOptionPane dialog;
    private boolean endNow = false;
    private int[] gug;
    private File path;

    public ExtractThread(JOptionPane dialog, int[] lol, File fil) {
      this.dialog = dialog;
      gug = lol;
      path = fil;
    }

    public void endNow() {
      endNow = true;
    }

    @Override
    public void run() {
      try {
        for (int i = 0; i < gug.length; i++) {
          File dest = new File(path, gug[i] + ".wav"); //$NON-NLS-1$
          if (dest.exists()) {
            String msg = Language.getString("SoundGUI.28"); //$NON-NLS-1$
            msg = msg.replace("%1", gug[i] + ".wav"); //$NON-NLS-1$ //$NON-NLS-2$
            int response = JOptionPane.showConfirmDialog(UE.WINDOW,
              msg, UE.APP_NAME, JOptionPane.YES_NO_OPTION);

            if (response == JOptionPane.NO_OPTION)
              continue;
          }

          String msg = Language.getString("SoundGUI.29"); //$NON-NLS-1$
          msg = msg.replace("%1", gug[i] + ".wav"); //$NON-NLS-1$ //$NON-NLS-2$
          dialog.setMessage(msg);
          if (endNow)
            return;

          SND.extract(gug[i], dest);
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
      }

      dialog.setValue(Language.getString("SoundGUI.30")); //$NON-NLS-1$
    }
  }
}
