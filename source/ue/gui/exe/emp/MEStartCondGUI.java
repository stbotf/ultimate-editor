package ue.gui.exe.emp;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.emp.MEStartingConditions;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.service.Language;

/**
 * Used for setting major empire starting conditions.
 */
public class MEStartCondGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_ERAS = CStbof.NUM_ERAS;

  // read
  private Trek trek;
  private RaceRst RACE;
  private Lexicon LEXICON;

  // edited
  private MEStartingConditions CONDITIONS;

  // ui components
  private JTextField[] txtPOP = new JTextField[NUM_EMPIRES * NUM_ERAS];
  private JTextField[] txtCRED = new JTextField[NUM_EMPIRES * NUM_ERAS];
  private JLabel lblPOP = new JLabel(Language.getString("MEStartCondGUI.0")); //$NON-NLS-1$
  private JLabel lblCRED = new JLabel(Language.getString("MEStartCondGUI.1")); //$NON-NLS-1$
  private JLabel[] lblERA = new JLabel[NUM_ERAS*2];
  private JLabel[] lblRACE = new JLabel[NUM_EMPIRES*2];

  public MEStartCondGUI(Trek trek, Stbof st) throws IOException {
    this.trek = trek;
    CONDITIONS = (MEStartingConditions) trek.getInternalFile(CTrekSegments.MEStartCond, true);

    RACE = (RaceRst) st.getInternalFile(CStbofFiles.RaceRst, true);
    LEXICON = (Lexicon) st.getInternalFile(CStbofFiles.LexiconDic, true);

    // CREATE
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      lblRACE[emp] = new JLabel(RACE.getName(emp));
      lblRACE[emp + NUM_EMPIRES] = new JLabel(RACE.getName(emp));
      for (int era = 0; era < NUM_ERAS; era++) {
        txtPOP[(emp * NUM_ERAS) + era] = new JTextField();
        txtCRED[(emp * NUM_ERAS) + era] = new JTextField();
      }
    }

    lblERA[0] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.BEGINNING));
    lblERA[1] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.EARLY));
    lblERA[2] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.DEVELOPED));
    lblERA[3] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.EXPANDED));
    lblERA[4] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.ADVANCED));

    lblERA[5] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.BEGINNING));
    lblERA[6] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.EARLY));
    lblERA[7] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.DEVELOPED));
    lblERA[8] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.EXPANDED));
    lblERA[9] = new JLabel(LEXICON.getEntry(LexDataIdx.GameSettings.StartCondition.ADVANCED));

    // SETUP

    Font def = UE.SETTINGS.getDefaultFont();

    for (int emp = 0; emp < NUM_EMPIRES * NUM_ERAS; emp++) {
      txtPOP[emp].setFont(def);
      txtCRED[emp].setFont(def);
      if (emp < 10) {
        lblRACE[emp].setFont(def);
        lblERA[emp].setFont(def);
      }
    }

    // ADD
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(5, 5, 5, 5);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;

    // pnlPOP
    JPanel pnlPOP = new JPanel(new GridBagLayout());

    c.gridwidth = 6;
    pnlPOP.add(lblPOP, c);
    c.gridwidth = 1;
    c.gridy = 1;
    for (int era = 0; era < NUM_ERAS; era++) {
      c.gridx = era + 1;
      pnlPOP.add(lblERA[era], c);
    }

    c.gridy = 2;
    c.gridx = 0;
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      c.gridy = emp + 2;
      pnlPOP.add(lblRACE[emp], c);
    }

    c.gridy = 2;
    c.gridx = 1;

    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int era = 0; era < NUM_ERAS; era++) {
        c.gridy = emp + 2;
        c.gridx = era + 1;
        pnlPOP.add(txtPOP[emp * NUM_ERAS + era], c);
      }
    }

    c.gridx = 0;
    c.gridy = 0;
    add(pnlPOP, c);
    // pnlCRED
    JPanel pnlCRED = new JPanel(new GridBagLayout());

    c.gridwidth = 6;
    pnlCRED.add(lblCRED, c);
    c.gridwidth = 1;
    c.gridy = 1;
    for (int era = 0; era < NUM_ERAS; era++) {
      c.gridx = era + 1;
      pnlCRED.add(lblERA[era + NUM_ERAS], c);
    }

    c.gridy = 2;
    c.gridx = 0;
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      c.gridy = emp + 2;
      pnlCRED.add(lblRACE[emp + NUM_EMPIRES], c);
    }

    c.gridy = 2;
    c.gridx = 1;

    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int era = 0; era < NUM_ERAS; era++) {
        c.gridy = emp + 2;
        c.gridx = era + 1;
        pnlCRED.add(txtCRED[emp * NUM_ERAS + era], c);
      }
    }

    c.gridx = 0;
    c.gridy = 1;
    add(pnlCRED, c);

    loadStartConditions();
  }

  private void loadStartConditions() {
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int era = 0; era < NUM_ERAS; era++) {
        int pop = CONDITIONS.getPopulation(emp, era);
        txtPOP[(emp * NUM_ERAS) + era].setText(Integer.toString(pop));
        int cred = CONDITIONS.getCredits(emp, era);
        txtCRED[(emp * NUM_ERAS) + era].setText(Integer.toString(cred));
      }
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.StartCond;
  }

  @Override
  public boolean hasChanges() {
    return CONDITIONS.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    trek.checkChanged(CONDITIONS, changes);
  }

  @Override
  public void reload() throws IOException {
    CONDITIONS = (MEStartingConditions) trek.getInternalFile(CTrekSegments.MEStartCond, true);
    loadStartConditions();
  }

  @Override
  public void finalWarning() {
    int val;
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int era = 0; era < NUM_ERAS; era++) {
        val = Integer.parseInt(txtPOP[(emp * NUM_ERAS) + era].getText());
        CONDITIONS.setPopulation(emp, era, val);

        val = Integer.parseInt(txtCRED[(emp * NUM_ERAS) + era].getText());
        CONDITIONS.setCredits(emp, era, val);
      }
    }
  }
}
