package ue.gui.exe.emp;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.emp.BaseMapRanges;
import ue.edit.exe.trek.seg.emp.MapRangeTechBonuses;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;

/**
 * Used for setting ship map ranges.
 */
public class ShipMapRangesGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_RANGES = 3;
  private static final int NUM_TECHLVLS = CStbof.DEFAULT_NUM_TECHLVLS;

  // read
  private Trek trek;
  private BaseMapRanges base;

  // edited
  private MapRangeTechBonuses bonus;

  // ui components
  private ButtonGroup gEmpires = new ButtonGroup();
  private JToggleButton[] btnEmpire = new JToggleButton[NUM_EMPIRES];
  private JTextField[] txtValue = new JTextField[NUM_RANGES * NUM_TECHLVLS];
  private JLabel[] lblHeader = new JLabel[4];
  private JLabel[] lblLevel = new JLabel[NUM_TECHLVLS];

  // data
  private int selected_empire = -1;

  public ShipMapRangesGUI(Trek trek, Stbof stbof) throws IOException {
    this.trek = trek;
    base = (BaseMapRanges) trek.getInternalFile(CTrekSegments.BaseMapRanges, true);
    bonus = (MapRangeTechBonuses) trek.getInternalFile(CTrekSegments.MapRangeBonuses, true);
    RaceRst race = stbof != null ? (RaceRst) stbof.tryGetInternalFile(CStbofFiles.RaceRst, true) : null;

    setupComponents(race);
    placeComponents();

    btnEmpire[0].doClick();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.MapRanges;
  }

  private void setupComponents(RaceRst race) {
    Font def = UE.SETTINGS.getDefaultFont();
    String str = Language.getString("ShipMapRangesGUI.0"); //$NON-NLS-1$
    int maxLen = 0;

    // buttons
    for (int i = 0; i < NUM_EMPIRES; i++) {
      if (race == null) {
        btnEmpire[i] = new JToggleButton(str.replace("%1", Integer.toString(i + 1))); //$NON-NLS-1$
      } else {
        btnEmpire[i] = new JToggleButton(race.getName(i));
      }

      gEmpires.add(btnEmpire[i]);

      if (btnEmpire[i].getMinimumSize().width > maxLen) {
        maxLen = btnEmpire[i].getMinimumSize().width;
      } else {
        btnEmpire[i].setMaximumSize(new Dimension(maxLen, btnEmpire[i].getMinimumSize().height));
      }

      btnEmpire[i].setAlignmentX(Component.CENTER_ALIGNMENT);
      btnEmpire[i].addActionListener(new BtnListener(i));
      btnEmpire[i].setFont(def);
    }

    // range labels
    lblHeader[0] = new JLabel(Language.getString("ShipMapRangesGUI.1")); //$NON-NLS-1$
    lblHeader[1] = new JLabel(Language.getString("ShipMapRangesGUI.2")); //$NON-NLS-1$
    lblHeader[2] = new JLabel(Language.getString("ShipMapRangesGUI.3")); //$NON-NLS-1$
    lblHeader[3] = new JLabel(Language.getString("ShipMapRangesGUI.4")); //$NON-NLS-1$

    maxLen = 0;

    for (int i = 0; i < 4; i++) {
      if (lblHeader[i].getMinimumSize().width > maxLen) {
        maxLen = lblHeader[i].getMinimumSize().width;
      } else {
        lblHeader[i].setMaximumSize(new Dimension(maxLen, lblHeader[i].getMinimumSize().height));
      }

      lblHeader[i].setAlignmentX(Component.CENTER_ALIGNMENT);
      if (i > 0)
        lblHeader[i].setHorizontalAlignment(JLabel.CENTER);

      lblHeader[i].setFont(def);
    }

    // level labels
    str = Language.getString("ShipMapRangesGUI.5"); //$NON-NLS-1$

    for (int i = 0; i < NUM_TECHLVLS; i++) {
      lblLevel[i] = new JLabel(str.replace("%1", Integer.toString(i + 1))); //$NON-NLS-1$

      if (lblLevel[i].getMinimumSize().width > maxLen) {
        maxLen = lblLevel[i].getMinimumSize().width;
      } else {
        lblLevel[i].setMaximumSize(new Dimension(maxLen, lblLevel[i].getMinimumSize().height));
      }

      lblLevel[i].setAlignmentX(Component.CENTER_ALIGNMENT);
      lblLevel[i].setFont(def);
    }

    // values
    for (int i = 0; i < NUM_RANGES * NUM_TECHLVLS; i++) {
      txtValue[i] = new JTextField();
      txtValue[i].setHorizontalAlignment(JTextField.RIGHT);
      txtValue[i].setMaximumSize(new Dimension(maxLen, txtValue[i].getMinimumSize().height));
      txtValue[i].setFont(def);
    }
  }

  private void placeComponents() {
    this.setLayout(new BorderLayout());

    // menu
    JPanel pnlMenu = new JPanel();
    BoxLayout blayout = new BoxLayout(pnlMenu, BoxLayout.Y_AXIS);

    pnlMenu.setLayout(blayout);
    pnlMenu.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    for (JToggleButton btn : btnEmpire) {
      pnlMenu.add(btn);
    }

    // table
    JPanel pnlTable = new JPanel();
    GridLayout glayout = new GridLayout(12, 4, 0, 0);

    pnlTable.setLayout(glayout);
    pnlTable.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    for (int l = 0; l < 4; l++) {
      pnlTable.add(lblHeader[l]);
    }

    for (int i = 0; i < NUM_TECHLVLS; i++) {
      int k = i * NUM_RANGES;
      pnlTable.add(lblLevel[i]);
      pnlTable.add(txtValue[k]);
      pnlTable.add(txtValue[k + 1]);
      pnlTable.add(txtValue[k + 2]);
    }

    // add to main
    this.add(pnlMenu, BorderLayout.WEST);
    this.add(pnlTable, BorderLayout.CENTER);
  }

  @Override
  public boolean hasChanges() {
    return bonus.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    trek.checkChanged(bonus, changes);
  }

  @Override
  public void reload() throws IOException {
    bonus = (MapRangeTechBonuses) trek.getInternalFile(CTrekSegments.MapRangeBonuses, true);
    loadShipRanges();
  }

  /* (non-Javadoc)
   * @see ue.gui.MainPanel#finalWarning()
   */
  @Override
  public void finalWarning() {
    for (int lvl = 0; lvl < NUM_TECHLVLS; lvl++) {
      for (int range = 0; range < NUM_RANGES; range++) {
        int empRange = Integer.parseInt(txtValue[lvl * NUM_RANGES + range].getText());
        if (empRange > 0) {
          int b = base.getBaseRange(selected_empire, range);
          bonus.setRangeBonus(lvl, selected_empire, range, empRange - b);
        }
      }
    }
  }

  private void loadShipRanges() {
    if (selected_empire < 0) {
      for (JTextField txtField : txtValue)
        txtField.setText(null);
    } else {
      for (int lvl = 0; lvl < NUM_TECHLVLS; lvl++) {
        for (int range = 0; range < NUM_RANGES; range++) {
          int empRange = empireRange(selected_empire, lvl, range);
          txtValue[lvl * NUM_RANGES + range].setText(Integer.toString(empRange));
        }
      }
    }
  }

  private int empireRange(int empire, int lvl, int range) {
    return base.getBaseRange(empire, range) + bonus.getRangeBonus(lvl, empire, range);
  }

  private class BtnListener implements ActionListener {

    private int empire;

    public BtnListener(int index) {
      empire = index;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
      try {
        if (selected_empire >= 0)
          finalWarning();

        selected_empire = empire;
        loadShipRanges();
      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }
  }
}
