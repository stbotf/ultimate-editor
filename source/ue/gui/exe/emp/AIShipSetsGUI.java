package ue.gui.exe.emp;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import lombok.val;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.emp.AIShpSets;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.service.Language;
import ue.util.data.ID;

/**
 * @author Alan Podlesek
 */
public class AIShipSetsGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_MAJOR_SHIP_ROLES = CStbof.NUM_MAJOR_SHIP_ROLES;
  private static final int BuildQueueSlots = 36;

  private JComboBox<ID> cmbAgenda = new JComboBox<ID>();
  private JComboBox<ID> cmbEmpire = new JComboBox<ID>();
  private ArrayList<JComboBox<ID>> cmbShipType = new ArrayList<JComboBox<ID>>(BuildQueueSlots);
  private JLabel lblEmpire = new JLabel();
  private JLabel lblAgenda = new JLabel();

  private Trek trekExe;
  private Stbof stbof;
  private AIShpSets shipSets;

  public AIShipSetsGUI(Trek exe, Stbof stbof) throws IOException {
    this.trekExe = exe;
    this.stbof = stbof;

    shipSets = (AIShpSets) exe.getInternalFile(CTrekSegments.AIShipSets, true);
    setupComponents();
    placeComponents();
    updateShipTypes();
    addListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.AiShpBldSets;
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();

    // labels
    lblEmpire.setFont(def);
    lblAgenda.setFont(def);

    // combo boxes
    cmbAgenda.setFont(def);
    cmbEmpire.setFont(def);
    for (int i = 0; i < BuildQueueSlots; ++i) {
      JComboBox<ID> comboBox = new JComboBox<ID>();
      cmbShipType.add(comboBox);
      comboBox.setFont(def);
    }

    updateLanguage();
  }

  void updateLanguage() {
    final String strEmpire = Language.getString("AIShipSetsGUI.0"); //$NON-NLS-1$
    final String strNothing = Language.getString("AIShipSetsGUI.10"); //$NON-NLS-1$
    final String strShipType = Language.getString("AIShipSetsGUI.11"); //$NON-NLS-1$
    final String[] strAgendas = new String[] {
      Language.getString("AIShipSetsGUI.1"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.2"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.3"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.4"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.5"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.6"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.7"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.8"), //$NON-NLS-1$
      Language.getString("AIShipSetsGUI.9") //$NON-NLS-1$
    };

    RaceRst races = null;
    Lexicon lexicon = null;
    if (stbof != null) {
      races = (RaceRst) stbof.tryGetInternalFile(CStbofFiles.RaceRst, true);
      lexicon = (Lexicon) stbof.tryGetInternalFile(CStbofFiles.LexiconDic, true);
    }

    lblEmpire.setText(Language.getString("AIShipSetsGUI.12")); //$NON-NLS-1$
    lblAgenda.setText(Language.getString("AIShipSetsGUI.13")); //$NON-NLS-1$

    // combo boxes
    for (val comboBox : cmbShipType) {
      comboBox.removeAllItems();

      if (lexicon != null) {
        comboBox.addItem(new ID(strNothing, -1));
        comboBox.addItem(new ID(lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.ScoutShip), 0));
        comboBox.addItem(new ID(lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.Destroyer), 1));
        comboBox.addItem(new ID(lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.Cruiser), 2));
        comboBox.addItem(new ID(lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.StrikeCruiser), 3));
        comboBox.addItem(new ID(lexicon.getEntry(LexDataIdx.Other.ShipRole.CommandShip), 4));
        comboBox.addItem(new ID(lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.ColonyShip), 5));
        comboBox.addItem(new ID(lexicon.getEntry(LexDataIdx.SpaceCraft.Categories.Transport), 9));
      }

      for (int j = comboBox.getItemCount(); j < NUM_MAJOR_SHIP_ROLES; j++) {
        if (j == 6)
          j = 9;
        comboBox.addItem(new ID(strShipType.replace("%1", Integer.toString(j)), j));
      }
    }

    cmbAgenda.removeAllItems();
    for (int i = 0; i < strAgendas.length; i++) {
      cmbAgenda.addItem(new ID(strAgendas[i], i));
    }

    cmbEmpire.removeAllItems();
    for (int r = 0; r < NUM_EMPIRES; r++) {
      if (races != null) {
        cmbEmpire.addItem(new ID(races.getName(r), r));
        continue;
      }
      cmbEmpire.addItem(new ID(strEmpire.replace("%1", Integer.toString(r)), r)); //$NON-NLS-1$
    }
  }

  private void placeComponents() {
    Font def = UE.SETTINGS.getDefaultFont();

    JPanel pnlTop = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 5));
    {
      pnlTop.add(lblEmpire);
      pnlTop.add(cmbEmpire);
      pnlTop.add(lblAgenda);
      pnlTop.add(cmbAgenda);
    }

    JPanel pnlSet = new JPanel();
    {
      GroupLayout layout = new GroupLayout(pnlSet);
      layout.setAutoCreateGaps(true);
      layout.setAutoCreateContainerGaps(true);
      pnlSet.setLayout(layout);

      GroupLayout.ParallelGroup[] hpg = new GroupLayout.ParallelGroup[12];
      for (int i = 0; i < 12; i++) {
        hpg[i] = layout.createParallelGroup(GroupLayout.Alignment.BASELINE);
      }

      GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
      GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
      int n = 0;

      for (int i = 0; i < 3; i++) {
        GroupLayout.ParallelGroup vpg1 = layout.createParallelGroup();
        GroupLayout.ParallelGroup vpg2 = layout.createParallelGroup();

        for (int j = 0; j < 12; j++) {
          JLabel lbl = new JLabel(Language.getString("AIShipSetsGUI.14")
            .replace("%1", Integer.toString(n + 1))); //$NON-NLS-1$ //$NON-NLS-2$
          lbl.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbl.setFont(def);

          // vertically group labels in i columns
          vpg1.addComponent(lbl);
          // vertically group combo boxes in i columns
          vpg2.addComponent(cmbShipType.get(n));
          // horizontally group labels and combo boxes in j rows
          hpg[j].addComponent(lbl);
          hpg[j].addComponent(cmbShipType.get(n));

          n++;
        }

        hGroup.addGroup(vpg1);
        hGroup.addGroup(vpg2);
      }

      for (int i = 0; i < 12; i++) {
        vGroup.addGroup(hpg[i]);
      }

      layout.setHorizontalGroup(hGroup);
      layout.setVerticalGroup(vGroup);
    }

    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(pnlTop);
    add(pnlSet);
    add(Box.createVerticalGlue());
  }

  private ActionListener createShipTypeListener(int i) {
    return new ActionListener() {
      final int idx = i;

      @Override
      public void actionPerformed(ActionEvent e) {
        setShipType(idx);
      }
    };
  }

  private void addListeners() {
    // combo boxes
    for (int i = 0; i < cmbShipType.size(); ++i) {
      cmbShipType.get(i).addActionListener(createShipTypeListener(i));
    }

    ActionListener update = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateShipTypes();
      }
    };

    cmbAgenda.addActionListener(update);
    cmbEmpire.addActionListener(update);
  }

  @Override
  public boolean hasChanges() {
    return shipSets.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    trekExe.checkChanged(shipSets, changes);
  }

  @Override
  public void reload() throws IOException {
    shipSets = (AIShpSets) trekExe.getInternalFile(CTrekSegments.AIShipSets, true);
    updateShipTypes();
  }

  /* (non-Javadoc)
   * @see ue.gui.MainPanel#finalWarning()
   */
  @Override
  public void finalWarning() {
    // nothing to do
  }

  private void updateShipTypes() {
    int emp = cmbEmpire.getSelectedIndex();
    int agenda = cmbAgenda.getSelectedIndex();

    if (emp >= 0 && agenda >= 0) {
      boolean listEnd = false;

      for (int i = 0; i < BuildQueueSlots; i++) {
        val cmb = cmbShipType.get(i);
        int type = shipSets.getShipType(emp, agenda, i);
        cmb.setSelectedItem(new ID(null, type));

        if (type == ShipRole.None) {
          cmb.setEnabled(!listEnd);
          listEnd = true;
        }
        else {
          cmb.setEnabled(true);
        }
      }
    }
  }

  private void setShipType(int index) {
    int emp = cmbEmpire.getSelectedIndex();
    int agenda = cmbAgenda.getSelectedIndex();
    ID id = (ID) cmbShipType.get(index).getSelectedItem();
    if (emp < 0 || agenda < 0 || id == null)
      return;

    short shipRole = (short)id.ID;

    // on ship type change, update the next combo box
    if (shipSets.setShipType(emp, agenda, index, shipRole)) {
      if (++index < cmbShipType.size()) {
        val cmb = cmbShipType.get(index);

        if (shipRole == ShipRole.None) {
          // unset all combo boxes that follow
          cmb.setSelectedItem(new ID(null, -1));
          cmb.setEnabled(false);
        } else {
          cmb.setEnabled(true);
        }
      }
    }
  }
}
