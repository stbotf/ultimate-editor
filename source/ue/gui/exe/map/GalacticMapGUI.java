package ue.gui.exe.map;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import lombok.val;
import ue.UE;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.LongValueSegment;
import ue.edit.exe.seg.prim.MovDwordPtrInt;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.patch.map.GalaxyShapesPatch;
import ue.edit.exe.trek.patch.map.MapSizePatch;
import ue.edit.exe.trek.patch.map.MinEmpireDistancePatch;
import ue.edit.exe.trek.patch.map.NumericSectorIdPatch;
import ue.edit.exe.trek.patch.map.SectorSizePatch;
import ue.edit.exe.trek.patch.map.SystemAlignmentPatch;
import ue.edit.exe.trek.sd.SD_AvgSpaceObjNum;
import ue.edit.exe.trek.sd.SD_GalDensity;
import ue.edit.exe.trek.seg.emp.MinorRacesNumbers;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.calc.Calc;

/**
 * @author Alan Podlesek
 */
public class GalacticMapGUI extends MainPanel {

  static final String HELP_FILE_NAME = "GalaxyGeneration.html"; //$NON-NLS-1$

  static final int    GAL_ARCS = 3;                        // inner, middle, outer
  static final int    GAL_AXIS = 2;                        // x/y or major/minor
  static final int    GAL_MINORS = 3;                      // few, some, many
  static final int    GAL_SHAPES = 4;                      // irregular, elliptic, ring, spiral
  static final int    GAL_SIZE_MODES = 3;                  // small, medium, large
  static final int    GAL_ZOOM_MODES = GAL_SIZE_MODES + 1; // few, some, many, zoomed in

  static final int    MAP_WIDTH_MIN       = 3;
  static final int    MAP_WIDTH_MAX       = 209;
  static final int    MAP_HEIGHT_MIN      = 2;
  static final int    MAP_HEIGHT_MAX      = 156;
  static final int    SECTOR_SIZE_MIN     = 5;
  static final int    SECTOR_SIZE_MAX     = 380;
  static final double ALIGN_FACTOR_MIN    = 0.1;
  static final double ALIGN_FACTOR_MAX    = 1000;
  static final double DENSITY_MIN         = 0.01;
  static final double DENSITY_MAX         = 1;
  static final int    MAX_MINORS_MIN      = 1;
  static final int    MAX_MINORS_MAX      = 30;
  static final int    SPACE_OBJECT_MIN    = 5;
  static final int    SPACE_OBJECT_MAX    = 1000;
  static final double STAR_ANOMALY_MIN    = 0.01;
  static final double STAR_ANOMALY_MAX    = 1;
  static final int    EMPIRE_DISTANCE_MIN = 0;
  static final int    ELLIPSE_AXIS_MIN    = 1;
  static final int    ELLIPSE_AXIS_MAX    = 999;
  static final int    SPIRAL_AXIS_MIN     = 1;
  static final int    SPIRAL_AXIS_MAX     = 999;
  static final int    SPIRAL_ARM_MIN      = 1;
  static final int    SPIRAL_ARM_MAX      = 999;
  static final int    RING_WIDTH_MIN      = 1;
  static final int    CORE_SIZE_MIN       = 0;
  static final double SPIRAL_ARC_MIN      = 0.0;
  static final double SPIRAL_ARC_MAX      = 999.0;

  private Trek TREK;

// Gowron's starting pop codes
//
// Hex location    suggested   vanilla
// Free Systems
// 0xb3cc1         A6 0E       A0 0F
// 0xb3c4d         C4 09       20 03
// 0xb3c60         C4 09       20 03
// 0xb3d30         07 00       07 00
//
// Minor Race Systems
// 0xb3f0f         A0 0F       A0 0F
// 0xb3e47         B8 0B       14 05
// 0xb3e57         B8 0B       14 05
// 0xb3f80         07 00       07 00
//
// Additional Major Empire Starting Systems
// 0xb4141         94 11       A0 0F
// 0xb4071         A0 0F       14 05
// 0xb4081         A0 0F       14 05
// 0xb41fb         07 00       07 00

  // strings
  private final String strMapSize = Language.getString("GalacticMapGUI.0"); //$NON-NLS-1$
  private final String strSystemAlignment = Language.getString("GalacticMapGUI.1"); //$NON-NLS-1$
  private final String strIrregularGalaxy = Language.getString("GalacticMapGUI.2"); //$NON-NLS-1$
  private final String strMinorRaces = Language.getString("GalacticMapGUI.3"); //$NON-NLS-1$
  private final String strMapWidth = Language.getString("GalacticMapGUI.4"); //$NON-NLS-1$
  private final String strMapHeight = Language.getString("GalacticMapGUI.5"); //$NON-NLS-1$
  private final String strDensity_ = Language.getString("GalacticMapGUI.6"); //$NON-NLS-1$
  private final String strSmall_ = Language.getString("GalacticMapGUI.7"); //$NON-NLS-1$
  private final String strMedium_ = Language.getString("GalacticMapGUI.8"); //$NON-NLS-1$
  private final String strLarge_ = Language.getString("GalacticMapGUI.9"); //$NON-NLS-1$
  private final String strZoomIn_ = Language.getString("GalacticMapGUI.10"); //$NON-NLS-1$
  private final String strFewMinors = Language.getString("GalacticMapGUI.11"); //$NON-NLS-1$
  private final String strSomeMinors = Language.getString("GalacticMapGUI.12"); //$NON-NLS-1$
  private final String strManyMinors = Language.getString("GalacticMapGUI.13"); //$NON-NLS-1$
  private final String strSectorSize = Language.getString("GalacticMapGUI.15"); //$NON-NLS-1$
  private final String strNumSpaceObjects = Language.getString("GalacticMapGUI.16"); //$NON-NLS-1$
  private final String strStarAnomalyRatio = Language.getString("GalacticMapGUI.17"); //$NON-NLS-1$
  private final String strStarAnomalyRatio_ = Language.getString("GalacticMapGUI.18"); //$NON-NLS-1$
  private final String strIrregular = Language.getString("GalacticMapGUI.19"); //$NON-NLS-1$
  private final String strElliptic = Language.getString("GalacticMapGUI.20"); //$NON-NLS-1$
  private final String strRing = Language.getString("GalacticMapGUI.21"); //$NON-NLS-1$
  private final String strSpiral = Language.getString("GalacticMapGUI.22"); //$NON-NLS-1$
  private final String strMinEmpDist = Language.getString("GalacticMapGUI.23"); //$NON-NLS-1$
  private final String strEllipseAxes = Language.getString("GalacticMapGUI.24"); //$NON-NLS-1$
  private final String strMajor = Language.getString("GalacticMapGUI.25"); //$NON-NLS-1$
  private final String strMinor = Language.getString("GalacticMapGUI.26"); //$NON-NLS-1$
  private final String strSpiralGalSettings = Language.getString("GalacticMapGUI.27"); //$NON-NLS-1$
  private final String strArmWidth = Language.getString("GalacticMapGUI.28"); //$NON-NLS-1$
  private final String strRingGalSettings = Language.getString("GalacticMapGUI.29"); //$NON-NLS-1$
  private final String strRingWidth = Language.getString("GalacticMapGUI.30"); //$NON-NLS-1$
  private final String strCoreSize = Language.getString("GalacticMapGUI.31"); //$NON-NLS-1$
  private final String strSpiralArmArcInner = Language.getString("GalacticMapGUI.32"); //$NON-NLS-1$
  private final String strSpiralArmArcMiddle = Language.getString("GalacticMapGUI.33"); //$NON-NLS-1$
  private final String strSpiralArmArcOuter = Language.getString("GalacticMapGUI.34"); //$NON-NLS-1$
  private final String strSpiralArmArcMultiplier = Language.getString("GalacticMapGUI.35"); //$NON-NLS-1$
  private final String strStandardSectorID = Language.getString("GalacticMapGUI.36"); //$NON-NLS-1$
  private final String strNumericSectorID = Language.getString("GalacticMapGUI.37"); //$NON-NLS-1$
  private final String strBracedSectorID = Language.getString("GalacticMapGUI.38"); //$NON-NLS-1$

  // Labels
  private JLabel lblMapWidth = new JLabel(strMapWidth);
  private JLabel lblMapHeight = new JLabel(strMapHeight);
  private JLabel[] lblZoomIn = new JLabel[2];
  private JLabel[] lblSmall = new JLabel[9];
  private JLabel[] lblMedium = new JLabel[9];
  private JLabel[] lblLarge = new JLabel[9];
  private JLabel[] lblMinorsSetting = new JLabel[GAL_MINORS];
  private JLabel lblDensity = new JLabel(strDensity_);
  private JLabel lblStarAnomalyRatio = new JLabel(strStarAnomalyRatio_);
  private JLabel[] lblGalaxyShape = new JLabel[GAL_SHAPES];
  private JLabel[] lblAxis = new JLabel[GAL_AXIS];
  private JLabel[] lblSpiralGalSettings = new JLabel[GAL_AXIS+1];
  private JLabel[] lblRingGalSettings = new JLabel[2];
  private JLabel[] lblSpiralArmArc = new JLabel[GAL_ARCS];

  // Spinners
  // map size
  private JSpinner[] spiMapWidth = new JSpinner[GAL_SIZE_MODES];
  private JSpinner[] spiMapHeight = new JSpinner[GAL_SIZE_MODES];
  // sector size (in pixels)
  private JSpinner[] spiSectorSize = new JSpinner[GAL_ZOOM_MODES];
  // alignment multipliers
  private JSpinner[] spiAlignmentMultiplier = new JSpinner[GAL_ZOOM_MODES];
  // minimum empire distance
  private JSpinner[] spiMinEmpireDistance = new JSpinner[GAL_SIZE_MODES * GAL_SHAPES];
  // average number of space objects
  private JSpinner[] spiSpaceObjectsNum = new JSpinner[GAL_SIZE_MODES];
  // irregular galaxy density
  private JSpinner spiDensity;
  // maximum number of minor races
  private JSpinner[] spiMaxMinors = new JSpinner[GAL_SIZE_MODES * GAL_MINORS];
  // ellipse axes for ellipse galaxy
  private JSpinner[] spiEllipseAxis = new JSpinner[GAL_SIZE_MODES * GAL_AXIS];
  // spiral galaxy ellipse axes and arm width
  private JSpinner[] spiSpiralEllipseAxis = new JSpinner[GAL_SIZE_MODES * GAL_AXIS];
  private JSpinner[] spiSpiralArmWidth = new JSpinner[GAL_SIZE_MODES];
  // spiral arm arc multipliers
  private JSpinner[] spiSpiralArmArc = new JSpinner[GAL_ARCS];
  // star / anomaly ratio
  private JSpinner spiStarAnomalyRatio;
  // ring galaxy
  private JSpinner[] spiRingWidth = new JSpinner[GAL_SIZE_MODES];
  private JSpinner[] spiCoreSize = new JSpinner[GAL_SIZE_MODES];

  // other ui components
  private JRadioButton rbnStandardSectorID;
  private JRadioButton rbnNumericSectorID;
  private JRadioButton rbnBracedSectorID;
  private JPanel cards;
  private JButton[] btnSwitchCard = new JButton[2];

  // data
  private MovRegInt               seg_irrDensity;
  private MinorRacesNumbers       seg_maxMinors;
  private LongValueSegment        seg_starAnomalyRatio;
  private MinEmpireDistancePatch  patch_empireDistance;
  private GalaxyShapesPatch       patch_galaxyShapes;
  private MapSizePatch            patch_mapSize;
  private NumericSectorIdPatch    patch_numericSectorID;
  private SectorSizePatch         patch_sectorSize;
  private SystemAlignmentPatch    patch_systemAlignment;

  public GalacticMapGUI(Trek trek) throws IOException {
    TREK = trek;
    patch_numericSectorID = new NumericSectorIdPatch(trek);
    patch_mapSize = new MapSizePatch(trek);
    patch_empireDistance = new MinEmpireDistancePatch(trek);
    patch_galaxyShapes = new GalaxyShapesPatch(trek);
    patch_sectorSize = new SectorSizePatch(trek);
    patch_systemAlignment = new SystemAlignmentPatch(trek);

    createComponents();
    placeComponents();
    loadValues(false);
    setupListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.GalaxyGen;
  }

  private void createComponents() {
    Font font = UE.SETTINGS.getDefaultFont();

    // labels
    lblMapWidth.setFont(font);
    lblMapHeight.setFont(font);
    lblDensity.setFont(font);
    lblStarAnomalyRatio.setFont(font);

    createAndSetupLabels(lblMinorsSetting, font,
      new String[]{strFewMinors, strSomeMinors, strManyMinors}, SwingConstants.CENTER);
    createAndSetupLabels(lblZoomIn, font, new String[]{strZoomIn_}, SwingConstants.RIGHT);
    createAndSetupLabels(lblSmall, font, new String[]{strSmall_}, SwingConstants.RIGHT);
    createAndSetupLabels(lblMedium, font, new String[]{strMedium_}, SwingConstants.RIGHT);
    createAndSetupLabels(lblLarge, font, new String[]{strLarge_}, SwingConstants.RIGHT);
    createAndSetupLabels(lblGalaxyShape, font,
      new String[]{strIrregular, strElliptic, strRing, strSpiral}, SwingConstants.CENTER);
    createAndSetupLabels(lblAxis, font, new String[]{strMajor, strMinor}, SwingConstants.CENTER);
    createAndSetupLabels(lblSpiralGalSettings, font,
      new String[]{strMajor, strMinor, strArmWidth}, SwingConstants.CENTER);
    createAndSetupLabels(lblRingGalSettings, font, new String[]{strRingWidth, strCoreSize},
      SwingConstants.CENTER);
    createAndSetupLabels(lblSpiralArmArc, font,
      new String[]{strSpiralArmArcInner, strSpiralArmArcMiddle, strSpiralArmArcOuter},
      SwingConstants.RIGHT);

    for (int i = 0; i < spiMapWidth.length; ++i) {
      spiMapWidth[i] = new JSpinner(new SpinnerNumberModel(MAP_WIDTH_MIN, MAP_WIDTH_MIN, MAP_WIDTH_MAX, 1));
      spiMapWidth[i].setFont(font);
    }

    for (int i = 0; i < spiMapHeight.length; ++i) {
      spiMapHeight[i] = new JSpinner(new SpinnerNumberModel(MAP_HEIGHT_MIN, MAP_HEIGHT_MIN, MAP_HEIGHT_MAX, 1));
      spiMapHeight[i].setFont(font);
    }

    for (int i = 0; i < spiSectorSize.length; ++i) {
      spiSectorSize[i] = new JSpinner(new SpinnerNumberModel(SECTOR_SIZE_MIN, SECTOR_SIZE_MIN, SECTOR_SIZE_MAX, 1));
      spiSectorSize[i].setFont(font);
    }

    for (int i = 0; i < spiAlignmentMultiplier.length; ++i) {
      spiAlignmentMultiplier[i] = new JSpinner(new SpinnerNumberModel(ALIGN_FACTOR_MIN, ALIGN_FACTOR_MIN, ALIGN_FACTOR_MAX, 0.1));
      spiAlignmentMultiplier[i].setFont(font);
    }

    spiDensity = new JSpinner(new SpinnerNumberModel(DENSITY_MIN, DENSITY_MIN, DENSITY_MAX, 0.01));

    for (int i = 0; i < spiMaxMinors.length; ++i) {
      spiMaxMinors[i] = new JSpinner(new SpinnerNumberModel(MAX_MINORS_MIN, MAX_MINORS_MIN, MAX_MINORS_MAX, 1));
      spiMaxMinors[i].setFont(font);
    }

    for (int i = 0; i < spiSpaceObjectsNum.length; ++i) {
      // set SPACE_OBJECT_MAX to initialize the min spinner width
      spiSpaceObjectsNum[i] = new JSpinner(new SpinnerNumberModel(SPACE_OBJECT_MIN, SPACE_OBJECT_MIN, SPACE_OBJECT_MAX, 1));
      spiSpaceObjectsNum[i].setFont(font);
    }

    spiStarAnomalyRatio = new JSpinner(new SpinnerNumberModel(STAR_ANOMALY_MIN, STAR_ANOMALY_MIN, STAR_ANOMALY_MAX, 0.01));

    int maxEmpDistance = (int) Math.sqrt(Math.pow(MAP_WIDTH_MAX, 2) + Math.pow(MAP_HEIGHT_MAX, 2)) / 2;
    for (int i = 0; i < spiMinEmpireDistance.length; ++i) {
      // set maxEmpDistance to initialize the min spinner width
      spiMinEmpireDistance[i] = new JSpinner(new SpinnerNumberModel(EMPIRE_DISTANCE_MIN, EMPIRE_DISTANCE_MIN, maxEmpDistance, 1));
      spiMinEmpireDistance[i].setFont(font);
    }

    for (int i = 0; i < spiEllipseAxis.length; ++i) {
      spiEllipseAxis[i] = new JSpinner(new SpinnerNumberModel(ELLIPSE_AXIS_MIN, ELLIPSE_AXIS_MIN, ELLIPSE_AXIS_MAX, 1));
      spiEllipseAxis[i].setFont(font);
    }

    for (int i = 0; i < spiSpiralEllipseAxis.length; ++i) {
      spiSpiralEllipseAxis[i] = new JSpinner(new SpinnerNumberModel(SPIRAL_AXIS_MIN, SPIRAL_AXIS_MIN, SPIRAL_AXIS_MAX, 1));
      spiSpiralEllipseAxis[i].setFont(font);
    }

    for (int i = 0; i < spiSpiralArmWidth.length; ++i) {
      spiSpiralArmWidth[i] = new JSpinner(new SpinnerNumberModel(SPIRAL_ARM_MIN, SPIRAL_ARM_MIN, SPIRAL_ARM_MAX, 1));
      spiSpiralArmWidth[i].setFont(font);
    }

    for (int i = 0; i < spiRingWidth.length; ++i) {
      int max = Math.min(MAP_HEIGHT_MIN, MAP_WIDTH_MIN);
      spiRingWidth[i] = new JSpinner(new SpinnerNumberModel(RING_WIDTH_MIN, RING_WIDTH_MIN, max, 1));
      spiRingWidth[i].setFont(font);
    }

    for (int i = 0; i < spiCoreSize.length; ++i) {
      int max = Math.min(MAP_HEIGHT_MIN, MAP_WIDTH_MIN);
      spiCoreSize[i] = new JSpinner(new SpinnerNumberModel(CORE_SIZE_MIN, CORE_SIZE_MIN, max, 1));
      spiCoreSize[i].setFont(font);
    }

    for (int i = 0; i < spiSpiralArmArc.length; ++i) {
      spiSpiralArmArc[i] = new JSpinner(new SpinnerNumberModel(SPIRAL_ARC_MIN, SPIRAL_ARC_MIN, SPIRAL_ARC_MAX, 0.01));
      spiSpiralArmArc[i].setFont(font);
    }

    // numeric sector id radio buttons
    rbnStandardSectorID = new JRadioButton(strStandardSectorID);
    rbnStandardSectorID.setFont(font);
    rbnNumericSectorID = new JRadioButton(strNumericSectorID);
    rbnNumericSectorID.setFont(font);
    rbnBracedSectorID = new JRadioButton(strBracedSectorID);
    rbnBracedSectorID.setFont(font);

    ButtonGroup group = new ButtonGroup();
    group.add(rbnStandardSectorID);
    group.add(rbnNumericSectorID);
    group.add(rbnBracedSectorID);

    // buttons
    btnSwitchCard[0] = new JButton("<<<"); //$NON-NLS-1$
    btnSwitchCard[0].setFont(font);

    btnSwitchCard[1] = new JButton(">>>"); //$NON-NLS-1$
    btnSwitchCard[1].setFont(font);
  }

  private void setupListeners() {
    // spinners
    ChangeListener lstMapWidth = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        int i = 0;
        for (; i < spiMapWidth.length; i++) {
          if ((Integer) spiMapWidth[i].getValue() > 27) {
            rbnStandardSectorID.setEnabled(false);
            if (rbnStandardSectorID.isSelected())
              rbnNumericSectorID.setSelected(true);
            break;
          }
        }

        // re-enable standard selection if no limiting size is found
        if (i >= spiMapWidth.length) {
          rbnStandardSectorID.setEnabled(true);
        }

        try {
          for (i = 0; i < GAL_SIZE_MODES; i++) {
            if (i < 2) {
              if (((Number) spiMapWidth[i].getValue()).intValue() >
                  ((Number) spiMapWidth[i + 1].getValue()).intValue()) {
                spiMapWidth[i + 1].setValue(spiMapWidth[i].getValue());
              }
            }

            updateMaxMapValues(i);
          }
        } catch (Exception f) {
          Dialogs.displayError(f);
        }
      }
    };

    ChangeListener lstMapHeight = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        try {
          for (int i = 0; i < GAL_SIZE_MODES; i++) {
            if (i < 2) {
              if (((Number) spiMapHeight[i].getValue()).intValue() >
                  ((Number) spiMapHeight[i + 1].getValue()).intValue()) {
                spiMapHeight[i + 1].setValue(spiMapHeight[i].getValue());
              }
            }

            updateMaxMapValues(i);
          }
        } catch (Exception f) {
          Dialogs.displayError(f);
        }
      }
    };

    ChangeListener lstSectorSize = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        for (int i = 0; i < spiSectorSize.length; i++) {
          spiAlignmentMultiplier[i]
              .setValue(0.2 * ((Number) spiSectorSize[i].getValue()).intValue());
        }
      }
    };

    for (int i = 0; i < spiMapWidth.length; ++i)
      spiMapWidth[i].addChangeListener(lstMapWidth);
    for (int i = 0; i < spiMapHeight.length; ++i)
      spiMapHeight[i].addChangeListener(lstMapHeight);
    for (int i = 0; i < spiSectorSize.length; i++)
      spiSectorSize[i].addChangeListener(lstSectorSize);

    btnSwitchCard[0].addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        ((CardLayout) cards.getLayout()).previous(cards);
      }
    });

    btnSwitchCard[1].addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        ((CardLayout) cards.getLayout()).next(cards);
      }
    });
  }

  private void placeComponents() {
    this.setLayout(new BorderLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;

    cards = new JPanel(new CardLayout());

    JPanel[] card = new JPanel[3];

    // card 0
    int index = 0;
    card[index] = new JPanel(new GridBagLayout());

    // map size
    JPanel pnlMapSize = new JPanel(new GridBagLayout());
    pnlMapSize.setBorder(BorderFactory.createTitledBorder(strMapSize));
    c.gridy = 0;
    c.gridx = 1;
    pnlMapSize.add(lblMapWidth, c);
    c.gridx++;
    pnlMapSize.add(lblMapHeight, c);

    c.gridy++;
    c.gridx = 0;
    pnlMapSize.add(lblSmall[0], c);
    c.gridy++;
    pnlMapSize.add(lblMedium[0], c);
    c.gridy++;
    pnlMapSize.add(lblLarge[0], c);

    c.gridy = 1;
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      c.gridx = 1;
      pnlMapSize.add(spiMapWidth[i], c);
      c.gridx = 2;
      pnlMapSize.add(spiMapHeight[i], c);
      c.gridy++;
    }

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlMapSize, c);

    // sector size
    JPanel pnlSectorSize = new JPanel(new GridBagLayout());
    pnlSectorSize.setBorder(BorderFactory.createTitledBorder(strSectorSize));
    c.gridx = 0;
    c.gridy = 0;
    pnlSectorSize.add(lblSmall[1], c);
    c.gridy++;
    pnlSectorSize.add(lblMedium[1], c);
    c.gridy++;
    pnlSectorSize.add(lblLarge[1], c);
    c.gridy++;
    pnlSectorSize.add(lblZoomIn[0], c);

    c.gridy = 0;
    c.gridx = 1;
    for (int i = 0; i < spiSectorSize.length; i++) {
      pnlSectorSize.add(spiSectorSize[i], c);
      c.gridy++;
    }

    c.gridx = 1;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlSectorSize, c);

    // system alignment
    JPanel pnlSystemAlignment = new JPanel(new GridBagLayout());
    pnlSystemAlignment.setBorder(BorderFactory.createTitledBorder(strSystemAlignment));
    c.gridx = 0;
    c.gridy = 0;
    pnlSystemAlignment.add(lblSmall[2], c);
    c.gridy++;
    pnlSystemAlignment.add(lblMedium[2], c);
    c.gridy++;
    pnlSystemAlignment.add(lblLarge[2], c);
    c.gridy++;
    pnlSystemAlignment.add(lblZoomIn[1], c);

    c.gridy = 0;
    c.gridx = 1;
    for (int i = 0; i < spiAlignmentMultiplier.length; i++) {
      pnlSystemAlignment.add(spiAlignmentMultiplier[i], c);
      c.gridy++;
    }

    c.gridx = 2;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlSystemAlignment, c);

    // irregular gal
    JPanel pnlIrregularGalaxy = new JPanel(new GridBagLayout());
    pnlIrregularGalaxy.setBorder(BorderFactory.createTitledBorder(strIrregularGalaxy));
    c.gridx = 0;
    c.gridy = 0;
    pnlIrregularGalaxy.add(lblDensity, c);
    c.gridx = 1;
    pnlIrregularGalaxy.add(spiDensity, c);

    c.gridx = 2;
    c.gridy = 2;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlIrregularGalaxy, c);

    // num of space objects
    JPanel pnlSpaceObjectsNum = new JPanel(new GridBagLayout());
    pnlSpaceObjectsNum.setBorder(BorderFactory.createTitledBorder(strNumSpaceObjects));
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    pnlSpaceObjectsNum.add(lblSmall[3], c);
    c.gridy++;
    pnlSpaceObjectsNum.add(lblMedium[3], c);
    c.gridy++;
    pnlSpaceObjectsNum.add(lblLarge[3], c);

    c.gridy = 0;
    c.gridx = 1;
    for (int i = 0; i < spiSpaceObjectsNum.length; i++) {
      pnlSpaceObjectsNum.add(spiSpaceObjectsNum[i], c);
      c.gridy++;
    }

    c.gridx = 2;
    c.gridy = 1;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlSpaceObjectsNum, c);

    // numeric sector id
    JPanel pnlNumeric = new JPanel(new GridBagLayout());
    c.insets.top = 0;
    c.insets.bottom = 0;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    pnlNumeric.add(rbnStandardSectorID, c);
    c.gridy++;
    pnlNumeric.add(rbnNumericSectorID, c);
    c.gridy++;
    pnlNumeric.add(rbnBracedSectorID, c);

    c.weightx = 0;
    c.insets.top = 5;
    c.insets.bottom = 5;
    c.gridwidth = 2;
    c.gridx = 0;
    c.gridy = 2;
    card[index].add(pnlNumeric, c);

    // minimum empire distances
    JPanel pnlMinEmpDist = new JPanel(new GridBagLayout());
    pnlMinEmpDist.setBorder(BorderFactory.createTitledBorder(strMinEmpDist));
    c.gridwidth = 1;
    c.gridx = 0;
    c.gridy = 1;
    pnlMinEmpDist.add(lblSmall[4], c);
    c.gridy++;
    pnlMinEmpDist.add(lblMedium[4], c);
    c.gridy++;
    pnlMinEmpDist.add(lblLarge[4], c);

    c.gridy = 0;
    c.gridx = 1;
    for (int i = 0; i < lblGalaxyShape.length; i++) {
      pnlMinEmpDist.add(lblGalaxyShape[i], c);
      c.gridx++;
    }

    // max
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      c.gridy = i + 1;

      for (int j = 0; j < GAL_SHAPES; j++) {
        c.gridx = j + 1;
        pnlMinEmpDist.add(spiMinEmpireDistance[i * GAL_SHAPES + j], c);
      }
    }

    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 2;
    c.gridheight = 1;
    card[index].add(pnlMinEmpDist, c);

    // card 1
    index++;
    card[index] = new JPanel(new GridBagLayout());

    // minor races
    JPanel pnlMinors = new JPanel(new GridBagLayout());
    pnlMinors.setBorder(BorderFactory.createTitledBorder(strMinorRaces));
    c.gridwidth = 1;
    c.gridx = 0;
    c.gridy = 1;
    pnlMinors.add(lblSmall[5], c);
    c.gridy++;
    pnlMinors.add(lblMedium[5], c);
    c.gridy++;
    pnlMinors.add(lblLarge[5], c);

    c.gridy = 0;
    c.gridx = 1;

    for (int i = 0; i < lblMinorsSetting.length; i++) {
      pnlMinors.add(lblMinorsSetting[i], c);
      c.gridx++;
    }

    // max
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      c.gridy = i + 1;

      for (int j = 0; j < GAL_MINORS; j++) {
        c.gridx = j + 1;
        pnlMinors.add(spiMaxMinors[i * GAL_MINORS + j], c);
      }
    }

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 2;
    card[index].add(pnlMinors, c);

    // ellipse axes
    JPanel pnlEllipseAxes = new JPanel(new GridBagLayout());
    pnlEllipseAxes.setBorder(BorderFactory.createTitledBorder(strEllipseAxes));
    c.gridheight = 1;
    c.gridx = 0;
    c.gridy = 1;
    pnlEllipseAxes.add(lblSmall[6], c);
    c.gridy++;
    pnlEllipseAxes.add(lblMedium[6], c);
    c.gridy++;
    pnlEllipseAxes.add(lblLarge[6], c);

    c.gridy = 0;
    c.gridx = 1;

    for (int i = 0; i < lblAxis.length; i++) {
      pnlEllipseAxes.add(lblAxis[i], c);
      c.gridx++;
    }

    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      c.gridy = i + 1;

      for (int j = 0; j < GAL_AXIS; j++) {
        c.gridx = j + 1;
        pnlEllipseAxes.add(spiEllipseAxis[i * GAL_AXIS + j], c);
      }
    }

    c.gridx = 1;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 2;
    card[index].add(pnlEllipseAxes, c);

    // star anomaly ratio
    JPanel pnlStarAnomalyRatio = new JPanel(new GridBagLayout());
    pnlStarAnomalyRatio.setBorder(BorderFactory.createTitledBorder(strStarAnomalyRatio));
    c.gridheight = 1;
    c.gridx = 0;
    c.gridy = 0;
    pnlStarAnomalyRatio.add(lblStarAnomalyRatio, c);
    c.gridx = 1;
    pnlStarAnomalyRatio.add(spiStarAnomalyRatio, c);

    c.gridx = 1;
    c.gridy = 3;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlStarAnomalyRatio, c);

    // spiral settings
    JPanel pnlSpiralSettings = new JPanel(new GridBagLayout());
    pnlSpiralSettings.setBorder(BorderFactory.createTitledBorder(strSpiralGalSettings));
    c.gridx = 0;
    c.gridy = 1;
    pnlSpiralSettings.add(lblSmall[7], c);
    c.gridy++;
    pnlSpiralSettings.add(lblMedium[7], c);
    c.gridy++;
    pnlSpiralSettings.add(lblLarge[7], c);

    c.gridy = 0;
    c.gridx = 1;

    for (int i = 0; i < GAL_AXIS + 1; i++) {
      pnlSpiralSettings.add(lblSpiralGalSettings[i], c);
      c.gridx++;
    }

    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      c.gridy = i + 1;

      for (int j = 0; j < GAL_AXIS + 1; j++) {
        c.gridx = j + 1;
        if (j < GAL_AXIS) {
          pnlSpiralSettings.add(spiSpiralEllipseAxis[i * GAL_AXIS + j], c);
        } else {
          pnlSpiralSettings.add(spiSpiralArmWidth[i], c);
        }
      }
    }

    c.gridx = 0;
    c.gridy = 2;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlSpiralSettings, c);

    // spiral arm arc multipliers
    JPanel pnlSpiralArmArc = new JPanel(new GridBagLayout());
    pnlSpiralArmArc.setBorder(BorderFactory.createTitledBorder(strSpiralArmArcMultiplier));
    c.gridy = 0;
    c.gridx = 0;

    for (int i = 0; i < spiSpiralArmArc.length; i++) {
      c.gridx = 0;
      pnlSpiralArmArc.add(lblSpiralArmArc[i], c);
      c.gridx = 1;
      pnlSpiralArmArc.add(spiSpiralArmArc[i], c);
      c.gridy++;
    }

    c.gridx = 1;
    c.gridy = 2;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlSpiralArmArc, c);

    // card 2
    index = 2;
    card[index] = new JPanel(new GridBagLayout());

    // ring gal settings
    JPanel pnlRingSettings = new JPanel(new GridBagLayout());
    pnlRingSettings.setBorder(BorderFactory.createTitledBorder(strRingGalSettings));
    c.gridx = 0;
    c.gridy = 1;
    pnlRingSettings.add(lblSmall[8], c);
    c.gridy++;
    pnlRingSettings.add(lblMedium[8], c);
    c.gridy++;
    pnlRingSettings.add(lblLarge[8], c);

    c.gridy = 0;
    c.gridx = 1;

    for (int i = 0; i < 2; i++) {
      pnlRingSettings.add(lblRingGalSettings[i], c);
      c.gridx++;
    }

    for (int i = 0; i < 3; i++) {
      c.gridy = i + 1;

      for (int j = 0; j < 2; j++) {
        c.gridx = j + 1;
        if (j == 0) {
          pnlRingSettings.add(spiRingWidth[i], c);
        } else {
          pnlRingSettings.add(spiCoreSize[i], c);
        }
      }
    }

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    card[index].add(pnlRingSettings, c);

    // finish
    for (index = 0; index < card.length; index++) {
      cards.add(card[index], Integer.toString(index));
    }

    add(cards, BorderLayout.CENTER);

    JPanel pnlSwitchCards = new JPanel();
    pnlSwitchCards.setLayout(new BoxLayout(pnlSwitchCards, BoxLayout.X_AXIS));

    Dimension dim = new Dimension(10, btnSwitchCard[0].getPreferredSize().height + 10);

    pnlSwitchCards.add(Box.createRigidArea(dim));
    pnlSwitchCards.add(btnSwitchCard[0]);
    pnlSwitchCards.add(Box.createHorizontalGlue());
    pnlSwitchCards.add(btnSwitchCard[1]);
    pnlSwitchCards.add(Box.createRigidArea(dim));

    add(pnlSwitchCards, BorderLayout.SOUTH);

    ((CardLayout) cards.getLayout()).first(cards);
  }

  private void loadValues(boolean reload) throws IOException {
    // PAGE 1
    // map width
    patch_mapSize.load(reload);
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      int val = patch_mapSize.getMapWidth(i, true);
      val = Calc.getValueWithinBounds(val, MAP_WIDTH_MIN, MAP_WIDTH_MAX).intValue();
      spiMapWidth[i].getModel().setValue(val);
    }

    // map height
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      int val = patch_mapSize.getMapHeight(i, true);
      val = Calc.getValueWithinBounds(val, MAP_HEIGHT_MIN, MAP_HEIGHT_MAX).intValue();
      spiMapHeight[i].getModel().setValue(val);
    }

    // sector size
    patch_sectorSize.load(reload);
    for (int i = 0; i < GAL_ZOOM_MODES; ++i) {
      int val = patch_sectorSize.getSectorSize(i, true);
      val = Calc.getValueWithinBounds(val, SECTOR_SIZE_MIN, SECTOR_SIZE_MAX).intValue();
      spiSectorSize[i].getModel().setValue(val);
    }

    // alignment multipliers
    patch_systemAlignment.load(reload);
    for (int i = 0; i < GAL_ZOOM_MODES; ++i) {
      double val = patch_systemAlignment.getSystemAlignment(i, true);
      val = Calc.getValueWithinBounds(val, ALIGN_FACTOR_MIN, ALIGN_FACTOR_MAX).doubleValue();
      spiAlignmentMultiplier[i].getModel().setValue(val);
    }

    // minimum empire distance
    patch_empireDistance.load(reload);
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      int mapWidth = ((Number) spiMapWidth[i].getValue()).intValue();
      int mapHeight = ((Number) spiMapHeight[i].getValue()).intValue();
      int max = (int) Math.sqrt(Math.pow(mapWidth, 2) + Math.pow(mapHeight, 2)) / 2;

      for (int j = 0; j < GAL_SHAPES; ++j) {
        int dist = patch_empireDistance.getMinEmpireDistance(i, j);
        dist = Calc.getValueWithinBounds(dist , EMPIRE_DISTANCE_MIN, max).intValue();
        int k = i * GAL_SHAPES + j;
        SpinnerNumberModel model = (SpinnerNumberModel)spiMinEmpireDistance[k].getModel();
        model.setMaximum(max);
        model.setValue(dist);
      }
    }

    // average number of space objects
    for (int i = 0; i < spiSpaceObjectsNum.length; ++i) {
      SpinnerNumberModel model = (SpinnerNumberModel) spiSpaceObjectsNum[i].getModel();
      int max = patch_mapSize.getMapHeight(i, true) * patch_mapSize.getMapWidth(i, true);
      int val = Calc.getValueWithinBounds(getSpaceObjectsNum(i), SPACE_OBJECT_MIN, max).intValue();
      model.setMaximum(max);
      model.setValue(val);
    }

    // irregular galaxy density
    seg_irrDensity = (MovRegInt) TREK.getSegment(SD_GalDensity.Irregular, true);
    double dval = Calc.getValueWithinBounds(seg_irrDensity.getFloat(), DENSITY_MIN, DENSITY_MAX).doubleValue();
    spiDensity.getModel().setValue(dval);

    // make sector ID show as numeric (e.g. "Sector 1.1")
    patch_numericSectorID.load(reload);
    if (patch_numericSectorID.isBraced())
      rbnBracedSectorID.setSelected(true);
    else if (patch_numericSectorID.isPatched())
      rbnNumericSectorID.setSelected(true);
    else
      rbnStandardSectorID.setSelected(true);

    // PAGE 2
    // max minor races
    seg_maxMinors = (MinorRacesNumbers) TREK.getInternalFile(CTrekSegments.MaxMinorsNumber, true);
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      for (int j = 0; j < GAL_MINORS; ++j) {
        int val = seg_maxMinors.getMinorsNumber(i, j) - 1;
        val = Calc.getValueWithinBounds(val, MAX_MINORS_MIN, MAX_MINORS_MAX).intValue();
        spiMaxMinors[i * GAL_MINORS + j].getModel().setValue(val);
      }
    }

    patch_galaxyShapes.load(reload);

    // ellipse axes
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      for (int j = 0; j < GAL_AXIS; ++j) {
        int val = patch_galaxyShapes.getEllipseGalEllipseAxis(i, j);
        val = Calc.getValueWithinBounds(val, ELLIPSE_AXIS_MIN, ELLIPSE_AXIS_MAX).intValue();
        spiEllipseAxis[i * GAL_AXIS + j].getModel().setValue(val);
      }
    }

    // spiral axes
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      for (int j = 0; j < GAL_AXIS; ++j) {
        int val = patch_galaxyShapes.getSpiralGalEllipseAxis(i, j);
        val = Calc.getValueWithinBounds(val, SPIRAL_AXIS_MIN, SPIRAL_AXIS_MAX).intValue();
        spiSpiralEllipseAxis[i * GAL_AXIS + j].getModel().setValue(val);
      }
    }

    // spiral arm width
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      int val = patch_galaxyShapes.getSpiralGalArmWidth(i);
      val = Calc.getValueWithinBounds(val, SPIRAL_ARM_MIN, SPIRAL_ARM_MAX).intValue();
      spiSpiralArmWidth[i].getModel().setValue(val);
    }

    // spiral arm arc
    for (int i = 0; i < GAL_ARCS; ++i) {
      double armArc = patch_galaxyShapes.getSpiralGalArmArc(i);
      spiSpiralArmArc[i].getModel().setValue(armArc);
    }

    // star / anomaly ratio
    seg_starAnomalyRatio = (LongValueSegment) TREK.getSegment(SD_GalDensity.StarAnomalyRatio, true);
    dval = Calc.getValueWithinBounds(seg_starAnomalyRatio.getDouble(), STAR_ANOMALY_MIN, STAR_ANOMALY_MAX).doubleValue();
    spiStarAnomalyRatio.getModel().setValue(dval);

    // PAGE 3
    // ring width
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      int max = Math.min(patch_mapSize.getMapHeight(i, false), patch_mapSize.getMapWidth(i, false));
      int val = Calc.getValueWithinBounds(patch_galaxyShapes.getRingGalWidth(i), RING_WIDTH_MIN, max).intValue();
      SpinnerNumberModel model = (SpinnerNumberModel)spiRingWidth[i].getModel();
      model.setMaximum(max);
      model.setValue(val);
    }

    // ring core size
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      int max = Math.min(patch_mapSize.getMapHeight(i, false), patch_mapSize.getMapWidth(i, false));
      int val = Calc.getValueWithinBounds(patch_galaxyShapes.getRingGalCoreSize(i), CORE_SIZE_MIN, max).intValue();
      SpinnerNumberModel model = (SpinnerNumberModel)spiCoreSize[i].getModel();
      model.setMaximum(max);
      model.setValue(val);
    }
  }

  private void updateMaxMapValues(int galSize) {
    spiSectorSize[galSize].setValue((int) getExpectedSectorSize(galSize));

    int mapWidth = ((Number) spiMapWidth[galSize].getValue()).intValue();
    int mapHeight = ((Number) spiMapHeight[galSize].getValue()).intValue();

    // space objects
    {
      int max = mapWidth * mapHeight;
      int val = Calc.getValueWithinBounds((Number) spiSpaceObjectsNum[galSize].getValue(), 5, max).intValue();
      val model = (SpinnerNumberModel) spiSpaceObjectsNum[galSize].getModel();
      model.setMaximum(max);
      model.setValue(val);
    }

    int max = (int) Math.sqrt(Math.pow(mapWidth, 2) + Math.pow(mapHeight, 2)) / 2;

    // min empire distance
    {
      int k = galSize * GAL_SHAPES;

      for (int j = 0; j < GAL_SHAPES; j++) {
        int val = Calc.getValueWithinBounds((Number) spiMinEmpireDistance[k].getValue(), 0, max).intValue();
        val model = (SpinnerNumberModel) spiMinEmpireDistance[k].getModel();
        model.setMaximum(max);
        model.setValue(val);
        k++;
      }
    }

    // ring width
    {
      int val = Calc.getValueWithinBounds((Number) spiRingWidth[galSize].getValue(), 1, max).intValue();
      val model = (SpinnerNumberModel) spiRingWidth[galSize].getModel();
      model.setMaximum(max);
      model.setValue(val);
    }

    // ring core size
    {
      int val = Calc.getValueWithinBounds((Number) spiCoreSize[galSize].getValue(), 0, max).intValue();
      val model = (SpinnerNumberModel) spiCoreSize[galSize].getModel();
      model.setMaximum(max);
      model.setValue(val);
    }
  }

  @Override
  public boolean hasChanges() {
    val segments = listSegments();
    for (val seg : segments) {
      if (seg.madeChanges())
        return true;
    }

    return TREK.isChanged(SD_AvgSpaceObjNum.SGalName)
      || TREK.isChanged(SD_AvgSpaceObjNum.MGalName)
      || TREK.isChanged(SD_AvgSpaceObjNum.LGalName);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) throws IOException {
    listSegments().stream().forEach(x -> TREK.checkChanged(x, changes));
    TREK.checkChanged(SD_AvgSpaceObjNum.Names, changes);
  }

  private List<InternalSegment> listSegments() {
    return Stream.of(
      patch_mapSize.listSegments().stream(),
      patch_empireDistance.listSegments().stream(),
      patch_galaxyShapes.listSegments().stream(),
      patch_sectorSize.listSegments().stream(),
      patch_systemAlignment.listSegments().stream(),
      patch_numericSectorID.listSegments().stream(),
      Stream.of(
        seg_irrDensity,
        seg_maxMinors,
        seg_starAnomalyRatio
      )
    ).flatMap(x -> x).collect(Collectors.toList());
  }

  @Override
  public void reload() throws IOException {
    loadValues(true);
  }

  /* (non-Javadoc)
   * @see ue.gui.MainPanel#finalWarning()
   */
  @Override
  public void finalWarning() throws Exception {
    // map size
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      patch_mapSize.setMapWidth(i, (Integer) spiMapWidth[i].getValue());
      patch_mapSize.setMapHeight(i, (Integer) spiMapHeight[i].getValue());
    }

    // numeric sector id
    if (rbnBracedSectorID.isSelected())
      patch_numericSectorID.patchBraced();
    else if (rbnNumericSectorID.isSelected())
      patch_numericSectorID.patch();
    else
      patch_numericSectorID.unpatch();

    // sector size
    for (int i = 0; i < spiSectorSize.length; i++) {
      patch_sectorSize.setSectorSize(i, ((Number) spiSectorSize[i].getValue()).shortValue());
    }

    // alignment multipliers
    for (int i = 0; i < spiAlignmentMultiplier.length; i++) {
      float align = ((Number) spiAlignmentMultiplier[i].getValue()).floatValue();
      patch_systemAlignment.setSystemAlignment(i, align);
    }

    // irregular galaxy density
    float density = ((Number) spiDensity.getValue()).floatValue();
    seg_irrDensity.setFloat(density);

    // minors
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      for (int j = 0; j < GAL_MINORS; j++) {
        int value = ((Number) spiMaxMinors[i * GAL_MINORS + j].getValue()).intValue();
        seg_maxMinors.setMinorsNumber(i, j, value + 1);
      }
    }

    // number of objects
    for (int i = 0; i < spiSpaceObjectsNum.length; i++) {
      setSpaceObjectsNum(i, ((Number) spiSpaceObjectsNum[i].getValue()).intValue());
    }

    // star anomaly ratio
    double starRatio = ((Number) spiStarAnomalyRatio.getValue()).doubleValue();
    seg_starAnomalyRatio.setDouble(starRatio);

    // min empire distance
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      for (int j = 0; j < GAL_SHAPES; j++) {
        int dist = ((Number) spiMinEmpireDistance[i * GAL_SHAPES + j].getValue()).intValue();
        patch_empireDistance.setMinEmpireDistance(i, j, dist);
      }
    }

    // ellipse axes
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      for (int j = 0; j < GAL_AXIS; j++) {
        int value = ((Number) spiEllipseAxis[i * GAL_AXIS + j].getValue()).intValue();
        patch_galaxyShapes.setEllipseGalEllipseAxis(i, j, value);
      }
    }

    // spiral gal axes
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      for (int j = 0; j < GAL_AXIS; j++) {
        int value = ((Number) spiSpiralEllipseAxis[i * GAL_AXIS + j].getValue()).intValue();
        patch_galaxyShapes.setSpiralGalEllipseAxis(i, j, value);
      }
    }

    // spiral arm width
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      spiSpiralArmWidth[i].commitEdit();
      int armWidth = ((Number) spiSpiralArmWidth[i].getValue()).intValue();
      patch_galaxyShapes.setSpiralGalArmWidth(i, armWidth);
    }

    // ring width
    for (int i = 0; i < spiRingWidth.length; i++) {
      int ringWidth = ((Number) spiRingWidth[i].getValue()).intValue();
      patch_galaxyShapes.setRingGalWidth(i, ringWidth);
    }

    // ring core size
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      int coreSize = ((Number) spiCoreSize[i].getValue()).intValue();
      patch_galaxyShapes.setRingGalCoreSize(i, coreSize);
    }

    // spiral arm arc multipliers
    for (int i = 0; i < GAL_ARCS; i++) {
      spiSpiralArmArc[i].commitEdit();
      double armArc = ((Number) spiSpiralArmArc[i].getValue()).doubleValue();
      patch_galaxyShapes.setSpiralGalArmArc(i, armArc);
    }
  }

  private short getExpectedSectorSize(int gsize) {
    short x = (short) (500 / (Integer) spiMapWidth[gsize].getValue());
    short y = (short) (380 / (Integer) spiMapHeight[gsize].getValue());

    if (x <= 5 || y <= 5) {
      x = y = 5;
    }

    if (x < y) {
      return x;
    } else {
      return y;
    }
  }

  private int getSpaceObjectsNum(int i) throws IOException {
    MovDwordPtrInt anso = (MovDwordPtrInt) TREK.getSegment(SD_AvgSpaceObjNum.All[i], true);
    return anso.intValue();
  }

  private void setSpaceObjectsNum(int i, int num) throws Exception {
    MovDwordPtrInt anso = (MovDwordPtrInt) TREK.getSegment(SD_AvgSpaceObjNum.All[i], true);
    anso.setValue(num);
  }

  @Override
  public String getHelpFileName() {
    return HELP_FILE_NAME; //$NON-NLS-1$
  }
}
