package ue.gui.exe;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import ue.UE;
import ue.edit.common.GenericFile;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.emp.EmpireImgPrefixes;
import ue.edit.exe.trek.seg.map.StellTypeDetailsDescMap;
import ue.edit.exe.trek.seg.shp.SelectPhaserHobFile;
import ue.edit.exe.trek.seg.shp.SelectPlasmaHobFile;
import ue.edit.exe.trek.seg.shp.SelectTorpedoHobFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.event.CBActionListener;
import ue.util.img.Tga2Ani;

/**
 * @author Alan
 */
public class TrekPatchesGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // read
  private Trek TREK;
  private Stbof stbof;

  // edited
  private StellTypeDetailsDescMap stellObjDescMap;
  private SelectPlasmaHobFile plasmaHobLoading;
  private SelectPhaserHobFile phaserHobLoading;
  private SelectTorpedoHobFile torpedoHobLoading;

  // ui components
  private JCheckBox chkNebulaHCGraphic = new JCheckBox("Disable hardcoded nebula image."
    + " (creates animation from nebula image and adds it to stbof.res if missing)");
  private JCheckBox chkExtSpObj = new JCheckBox("Enable extended space object description mappings."
    + " (allows to specify up to 30 individual space object descriptions)");
  private JCheckBox chkPlasmaPhaserHobLoading = new JCheckBox(
    "Enable per ship plasma/phaser hob file selection. (modifies stbof.res/shiplist.sst)");
  private JCheckBox chkTorpedoHobLoading = new JCheckBox(
    "Enable per ship torpedo hob file selection. (modifies stbof.res/shiplist.sst)");

  // data
  private HashSet<String> filesChanged = new HashSet<String>();

  public TrekPatchesGUI(Trek exe, Stbof stbof) throws IOException {
    this.TREK = exe;
    this.stbof = stbof;

    // load required files
    stellObjDescMap = (StellTypeDetailsDescMap) TREK.getSegment(StellTypeDetailsDescMap.SEGMENT_DEFINITION, true);
    plasmaHobLoading = (SelectPlasmaHobFile) TREK.getInternalFile(CTrekSegments.SelectPlasmaHobFile, true);
    phaserHobLoading = (SelectPhaserHobFile) TREK.getInternalFile(CTrekSegments.SelectPhaserHobFile, true);
    torpedoHobLoading = (SelectTorpedoHobFile) TREK.getInternalFile(CTrekSegments.SelectTorpedoHobFile, true);

    setupComponents();
    placeComponents();
    addListeners();
    loadPatches();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Graphics;
  }

  private void setupComponents() {
    Font font = UE.SETTINGS.getDefaultFont();
    chkNebulaHCGraphic.setFont(font);
    chkExtSpObj.setFont(font);
    chkPlasmaPhaserHobLoading.setFont(font);
    chkTorpedoHobLoading.setFont(font);
  }

  private void placeComponents() {
    Font font = UE.SETTINGS.getDefaultBigFont();

    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    JPanel pnlOptions = new JPanel(new GridLayout(0, 1));
    pnlOptions.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    JLabel lbl = new JLabel("Graphics");
    lbl.setFont(font);
    pnlOptions.add(lbl);

    pnlOptions.add(chkNebulaHCGraphic);
    pnlOptions.add(chkExtSpObj);
    pnlOptions.add(chkPlasmaPhaserHobLoading);
    pnlOptions.add(chkTorpedoHobLoading);

    JScrollPane pane = new JScrollPane(pnlOptions);

    add(pane, BorderLayout.NORTH);
  }

  private void addListeners() {
    chkNebulaHCGraphic.addActionListener(new CBActionListener(TrekPatchesGUI.this::onChkNebHC));
    chkExtSpObj.addActionListener(new CBActionListener(TrekPatchesGUI.this::onChkExtSpObj));
    chkPlasmaPhaserHobLoading.addActionListener(new CBActionListener(TrekPatchesGUI.this::onChkPhaserHob));
    chkTorpedoHobLoading.addActionListener(new CBActionListener(TrekPatchesGUI.this::onChkTorpedoHob));
  }

  private void loadPatches() {
    chkNebulaHCGraphic.setSelected(!stellObjDescMap.isNebulaHardCoded());
    chkExtSpObj.setSelected(stellObjDescMap.isExtended());
    chkPlasmaPhaserHobLoading.setSelected(
      plasmaHobLoading.isPatched() && phaserHobLoading.isPatched());
    chkTorpedoHobLoading.setSelected(torpedoHobLoading.isPatched());
  }

  @Override
  public boolean hasChanges() {
    return !filesChanged.isEmpty()
      || stellObjDescMap.madeChanges()
      || plasmaHobLoading.madeChanges()
      || phaserHobLoading.madeChanges()
      || torpedoHobLoading.madeChanges()
      || stbof != null && stbof.isChanged(CStbofFiles.ShipListSst);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    changes.addAll(TREK, filesChanged);
    TREK.checkChanged(stellObjDescMap, changes);
    TREK.checkChanged(plasmaHobLoading, changes);
    TREK.checkChanged(phaserHobLoading, changes);
    TREK.checkChanged(torpedoHobLoading, changes);
    if (stbof != null)
      stbof.checkChanged(CStbofFiles.ShipListSst, changes);
  }

  @Override
  public void reload() throws IOException {
    filesChanged.clear();
    stellObjDescMap = (StellTypeDetailsDescMap) TREK.getSegment(StellTypeDetailsDescMap.SEGMENT_DEFINITION, true);
    plasmaHobLoading = (SelectPlasmaHobFile) TREK.getInternalFile(CTrekSegments.SelectPlasmaHobFile, true);
    phaserHobLoading = (SelectPhaserHobFile) TREK.getInternalFile(CTrekSegments.SelectPhaserHobFile, true);
    torpedoHobLoading = (SelectTorpedoHobFile) TREK.getInternalFile(CTrekSegments.SelectTorpedoHobFile, true);

    loadPatches();
  }

  @Override
  public void finalWarning() {
  }

  private AniOrCur createAniFromTga(String tgaName) throws IOException, InvalidArgumentsException {
    TargaImage tga = stbof.getTargaImage(tgaName, LoadFlags.UNCACHED);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    tga.save(baos);

    Tga2Ani T2A = new Tga2Ani();
    T2A.addFrame(tgaName, baos.toByteArray());

    return T2A.getAniOrCur(false);
  }

  private byte[] createAniDataFromTga(String tgaName) throws IOException, InvalidArgumentsException {
    AniOrCur aoc = createAniFromTga(tgaName);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    aoc.save(baos);
    return baos.toByteArray();
  }

  private byte[] createTgaDataFromAni(String aniName) throws IOException {
    AniOrCur aoc = stbof.getAnimation(aniName, LoadFlags.UNCACHED);
    TargaImage tga = aoc.getFrame(0);
    // BotF ani files seem to disregard the overlay bit
    tga.convertFirstPixelAlpha(true);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    tga.save(baos);

    return baos.toByteArray();
  }

  private void onChkNebHC(ActionEvent evt) throws IOException, InvalidArgumentsException {
    boolean disableNebHC = chkNebulaHCGraphic.isSelected();
    stellObjDescMap.setHardCodedNebula(!disableNebHC);

    if (stbof != null) {
      Objstruc objstruc = (Objstruc) stbof.getInternalFile(CStbofFiles.ObjStrucSmt, false);
      String graph = objstruc.getGraph(2);
      String aniName = graph + ".ani";
      String tgaName = graph + ".tga";
      boolean hasAni = stbof.hasInternalFile(aniName);
      boolean hasTga = stbof.hasInternalFile(tgaName);

      // convert missing ani or tga
      if ((hasTga != hasAni) && (hasAni != disableNebHC)) {
        byte[] data = hasTga ? createAniDataFromTga(tgaName) : createTgaDataFromAni(aniName);
        String convertedName = hasTga ? aniName : tgaName;
        stbof.addInternalFile(new GenericFile(convertedName, data));
        filesChanged.add(convertedName);
      }
    }
  }

  private void onChkExtSpObj(ActionEvent evt) {
    boolean extended = chkExtSpObj.isSelected();
    stellObjDescMap.setExtended(extended);
  }

  private void onChkPhaserHob(ActionEvent x) throws IOException {
    boolean enabled = !chkPlasmaPhaserHobLoading.isSelected();
    plasmaHobLoading.setPatched(enabled);
    phaserHobLoading.setPatched(enabled);

    // fix prefixes
    if (enabled && stbof != null) {
      EmpireImgPrefixes prefixes = (EmpireImgPrefixes) TREK
        .getInternalFile(CTrekSegments.EmpireImagePrefixes, false);

      ShipList shipList = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);

      for (ShipDefinition shipDef : shipList.listShips()) {
        int r = shipDef.getRace();
        char empPrefix = r < NUM_EMPIRES ? prefixes.getEmpirePrefix(r) : 'M';
        shipDef.setPhaserPrefix(empPrefix);
      }
    }
  }

  private void onChkTorpedoHob(ActionEvent x) throws IOException {
    boolean enabled = !chkTorpedoHobLoading.isSelected();
    torpedoHobLoading.setPatched(enabled);

    // fix prefixes
    if (enabled && stbof != null) {
      EmpireImgPrefixes prefixes = (EmpireImgPrefixes) TREK
        .getInternalFile(CTrekSegments.EmpireImagePrefixes, false);

      ShipList shipList = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);

      for (ShipDefinition shipDef : shipList.listShips()) {
        int r = shipDef.getRace();
        char empPrefix = r < NUM_EMPIRES ? prefixes.getEmpirePrefix(r) : 'M';
        shipDef.setTorpedoPrefix(empPrefix);
      }
    }
  }

}
