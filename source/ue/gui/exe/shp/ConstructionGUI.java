package ue.gui.exe.shp;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import lombok.val;
import ue.UE;
import ue.edit.exe.seg.common.DataPatchSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SD_AutoUpgrade;
import ue.edit.exe.trek.sd.SD_MandatoryDilFix;
import ue.edit.exe.trek.seg.bld.MandatoryDilithiumFix;
import ue.edit.exe.trek.seg.shp.SpecialShips;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;

/**
 * @author Alan Podlesek
 */
public class ConstructionGUI extends MainPanel {

  // read
  private Trek TREK;

  // edited
  private SpecialShips SP;
  private DataPatchSegment segAutoUpgradeShips;
  private DataPatchSegment segAutoUpgradeBase;
  private MandatoryDilithiumFix segMandatoryDilFix;

  // ui components
  private JPanel pnlSBMod = new JPanel();
  private JRadioButton rbnSBMod_None = new JRadioButton(Language.getString("ConstructionGUI.2")); //$NON-NLS-1$
  private JRadioButton rbnSBMod_ResidentShips = new JRadioButton(Language.getString("ConstructionGUI.3")); //$NON-NLS-1$
  private JRadioButton rbnSBMod_MinorShips = new JRadioButton(Language.getString("ConstructionGUI.4")); //$NON-NLS-1$
  private JCheckBox chkNoSpecial = new JCheckBox(Language.getString("ConstructionGUI.0")); //$NON-NLS-1$
  private JCheckBox chkNoAutoUpgradeShips = new JCheckBox(Language.getString("ConstructionGUI.5")); //$NON-NLS-1$
  private JCheckBox chkNoAutoUpgradeBase = new JCheckBox(Language.getString("ConstructionGUI.6")); //$NON-NLS-1$
  private JCheckBox chkNoMandatoryDil = new JCheckBox(Language.getString("ConstructionGUI.7")); //$NON-NLS-1$

  public ConstructionGUI(Trek trek) throws IOException {
    TREK = trek;
    SP = (SpecialShips) TREK.getInternalFile(CTrekSegments.SpecialShips, true);

    setupComponents();
    placeComponents();
    loadSpecials();
    addListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Construction;
  }

  @Override
  public String getHelpFileName() {
    return "ShipBuilding.html"; //$NON-NLS-1$
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();
    rbnSBMod_None.setFont(def);
    rbnSBMod_ResidentShips.setFont(def);
    rbnSBMod_MinorShips.setFont(def);
    chkNoSpecial.setFont(def);
    chkNoAutoUpgradeShips.setFont(def);
    chkNoAutoUpgradeBase.setFont(def);
    chkNoMandatoryDil.setFont(def);

    pnlSBMod.setBorder(BorderFactory.createTitledBorder(Language.getString("ConstructionGUI.1")));

    ButtonGroup group = new ButtonGroup();
    group.add(rbnSBMod_None);
    group.add(rbnSBMod_ResidentShips);
    group.add(rbnSBMod_MinorShips);
  }

  private void placeComponents() {
    setLayout(new GridBagLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    // ship building mod
    pnlSBMod.setLayout(new BoxLayout(pnlSBMod, BoxLayout.Y_AXIS));
    pnlSBMod.add(rbnSBMod_None);
    pnlSBMod.add(rbnSBMod_ResidentShips);
    pnlSBMod.add(rbnSBMod_MinorShips);

    val c = new GridBagConstraints();
    c.anchor = GridBagConstraints.WEST;
    c.gridx = 0;
    c.gridy = 0;
    add(pnlSBMod, c);
    c.insets.top = 5;
    c.insets.left = 5;
    c.gridy++;
    add(chkNoSpecial, c);
    c.insets.top = 0;
    c.gridy++;
    add(chkNoAutoUpgradeShips, c);
    c.gridy++;
    add(chkNoAutoUpgradeBase, c);
    c.gridy++;
    add(chkNoMandatoryDil, c);
  }

  private void addListeners() {
    rbnSBMod_None.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SP.setMinorShipsSimpleMod(false);
        SP.setMinorShipsMod(false);
        // re-enable the special ship selection
        chkNoSpecial.setEnabled(true);
      }
    });

    rbnSBMod_ResidentShips.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SP.setMinorShipsSimpleMod(true);
        // re-enable the special ship selection,
        // which is compatible to this mod
        chkNoSpecial.setEnabled(true);
      }
    });

    rbnSBMod_MinorShips.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SP.setMinorShipsMod(true);
        // disable the special ship building selection
        // which is overridden by this mod
        chkNoSpecial.setEnabled(false);
        chkNoSpecial.setSelected(true);
      }
    });

    chkNoSpecial.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SP.setSPEnabled(!chkNoSpecial.isSelected());
      }
    });

    chkNoAutoUpgradeShips.addActionListener(new CBActionListener(
      x -> segAutoUpgradeShips.setPatched(chkNoAutoUpgradeShips.isSelected())));

    chkNoAutoUpgradeBase.addActionListener(new CBActionListener(
      x -> segAutoUpgradeBase.setPatched(chkNoAutoUpgradeBase.isSelected())));

    chkNoMandatoryDil.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          segMandatoryDilFix.setPatched(chkNoMandatoryDil.isSelected());
        }
        catch (IOException err) {
          Dialogs.displayError(err);
        }
      }
    });
  }

  private void loadSpecials() throws IOException {
    boolean spDisabled = !SP.areSPEnabled();
    boolean simpleEnabled = SP.areMinorShipsSimpleEnabled();
    boolean minEnabled = SP.areMinorShipsEnabled();
    rbnSBMod_None.setSelected(!simpleEnabled && !minEnabled);
    rbnSBMod_ResidentShips.setSelected(simpleEnabled);
    rbnSBMod_MinorShips.setSelected(minEnabled);
    chkNoSpecial.setSelected(spDisabled);
    chkNoSpecial.setEnabled(!spDisabled || !minEnabled);

    segAutoUpgradeShips = (DataPatchSegment) TREK.getSegment(SD_AutoUpgrade.Ships, true);
    chkNoAutoUpgradeShips.setSelected(segAutoUpgradeShips.isPatched());

    segAutoUpgradeBase = (DataPatchSegment) TREK.getSegment(SD_AutoUpgrade.Base, true);
    chkNoAutoUpgradeBase.setSelected(segAutoUpgradeBase.isPatched());

    segMandatoryDilFix = (MandatoryDilithiumFix) TREK.getSegment(SD_MandatoryDilFix.DilithiumFix, true);
    chkNoMandatoryDil.setSelected(segMandatoryDilFix.isPatched());
  }

  @Override
  public boolean hasChanges() {
    return SP.madeChanges()
      || segAutoUpgradeShips.madeChanges()
      || segAutoUpgradeBase.madeChanges()
      || segMandatoryDilFix.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    TREK.checkChanged(SP, changes);
    TREK.checkChanged(segAutoUpgradeShips, changes);
    TREK.checkChanged(segAutoUpgradeBase, changes);
    TREK.checkChanged(segMandatoryDilFix, changes);
  }

  @Override
  public void reload() throws IOException {
    SP = (SpecialShips) TREK.getInternalFile(CTrekSegments.SpecialShips, true);
    segAutoUpgradeShips = (DataPatchSegment) TREK.getSegment(SD_AutoUpgrade.Ships, true);
    segAutoUpgradeBase = (DataPatchSegment) TREK.getSegment(SD_AutoUpgrade.Base, true);
    segMandatoryDilFix = (MandatoryDilithiumFix) TREK.getSegment(SD_MandatoryDilFix.DilithiumFix, true);
    loadSpecials();
  }

  /* (non-Javadoc)
   * @see ue.gui.MainPanel#finalWarning()
   */
  @Override
  public void finalWarning() {
    SP.setSPEnabled(!chkNoSpecial.isSelected());
    SP.setMinorShipsSimpleMod(rbnSBMod_ResidentShips.isSelected());
    SP.setMinorShipsMod(rbnSBMod_MinorShips.isSelected());
  }
}
