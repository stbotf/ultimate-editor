package ue.gui.exe;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_App;
import ue.edit.exe.trek.seg.CDprotection;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

/**
 * A gui used to enable/disable BotF CD protection.
 */
public class CDProtectionGUI extends MainPanel {

  // %1 - the name of the updated file
  private final String strFILE_UPDATED = Language.getString("CDProtectionGUI.5"); //$NON-NLS-1$
  private final String strSELECT_FILE = Language.getString("CDProtectionGUI.6"); //$NON-NLS-1$
  private final String strSELECT = Language.getString("CDProtectionGUI.7"); //$NON-NLS-1$
  private final String strINFO = Language.getString("CDProtectionGUI.9"); //$NON-NLS-1$

  // read
  private Trek trek;

  // edited
  private CDprotection PROTECTION;

  // ui components
  private JLabel lblCDProtection = new JLabel(Language.getString("CDProtectionGUI.0")); //$NON-NLS-1$
  private JLabel lblModifyIni = new JLabel(Language.getString("CDProtectionGUI.1")); //$NON-NLS-1$
  private JLabel lblPath = new JLabel(Language.getString("CDProtectionGUI.2")); //$NON-NLS-1$
  private JCheckBox chkCDProtection = new JCheckBox(""); //$NON-NLS-1$
  private JButton btnModify = new JButton(Language.getString("CDProtectionGUI.3")); //$NON-NLS-1$
  private JButton btnBrowse = new JButton(Language.getString("CDProtectionGUI.4")); //$NON-NLS-1$
  private JButton btnEdit = new JButton(Language.getString("CDProtectionGUI.10")); //$NON-NLS-1$
  private JTextField txtPath = new JTextField(40);
  private JComboBox<ID> cmbRoots = new JComboBox<ID>();
  private JLabel lblDrives = new JLabel(Language.getString("CDProtectionGUI.8")); //$NON-NLS-1$
  private JTextArea txtINFO = new JTextArea();

  public CDProtectionGUI(Trek trek) throws IOException {
    if (trek == null)
      return;

    this.trek = trek;
    PROTECTION = (CDprotection) trek.getSegment(SD_App.CDProtection, true);

    setupComponents();
    placeComponents();
    addListeners();
    loadValues();
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();
    lblCDProtection.setFont(def);
    lblModifyIni.setFont(def);
    lblPath.setFont(def);
    chkCDProtection.setFont(def);
    btnModify.setFont(def);
    btnBrowse.setFont(def);
    txtPath.setFont(def);
    lblDrives.setFont(def);
    cmbRoots.setFont(def);

    txtINFO.setFont(def);
    txtINFO.setEditable(false);
    txtINFO.setFocusable(false);
    txtINFO.setLineWrap(true);
    txtINFO.setOpaque(false);
    txtINFO.setWrapStyleWord(true);
  }

  private void placeComponents() {
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(5, 5, 5, 5);
    c.gridx = 0;
    c.gridy = 0;

    // pnlOne
    JPanel pnlOne = new JPanel(new GridBagLayout());
    {
      pnlOne.add(lblCDProtection, c);
      c.gridx = 1;
      pnlOne.add(chkCDProtection, c);
    }

    // pnlTwo
    JPanel pnlTwo = new JPanel(new GridBagLayout());
    {
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 2;
      pnlTwo.add(lblModifyIni, c);
      c.gridx = 2;
      c.gridwidth = 1;
      pnlTwo.add(btnModify, c);
      c.gridx = 0;
      c.gridy++;
      pnlTwo.add(lblDrives, c);
      c.gridx++;
      pnlTwo.add(cmbRoots, c);
      c.gridx++;
      pnlTwo.add(btnEdit, c);
      c.gridx = 0;
      c.gridy++;
      pnlTwo.add(lblPath, c);
      c.gridx++;
      pnlTwo.add(txtPath, c);
      c.gridx++;
      pnlTwo.add(btnBrowse, c);
    }

    // main panel
    setLayout(new GridBagLayout());
    c.gridx = 0;
    c.gridy = 0;
    add(pnlOne, c);
    c.gridy++;
    add(pnlTwo, c);
    c.insets.top = 10;
    c.gridy++;
    add(txtINFO, c);
  }

  private void addListeners() {
    btnModify.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent a) {
        try {
          File path = new File(txtPath.getText());
          ArrayList<String> lines = new ArrayList<String>();

          // read stbof.ini
          try (
            FileInputStream fs = new FileInputStream(path);
            InputStreamReader sr = new InputStreamReader(fs, "ISO-8859-1"); //$NON-NLS-1$
            BufferedReader reader = new BufferedReader(sr)
          ) {
            while (reader.ready()) {
              lines.add(reader.readLine());
          }}

          String cdpath = "C:\\botf\\"; //$NON-NLS-1$

          // copy hdpath to cdpath
          if (!chkCDProtection.isSelected()) {
            for (int i = 0; i < lines.size(); i++) {
              String str = lines.get(i);
              if (str.startsWith("HDPATH")) //$NON-NLS-1$
                cdpath = str.substring(7);
            }
          } else {
            cdpath = ((ID) cmbRoots.getSelectedItem()).toString();
            if (!cdpath.endsWith(File.separator)) {
              cdpath = cdpath + File.separator;
            }
          }

          // write to stbof.ini
          try (
            FileOutputStream fs = new FileOutputStream(path);
            OutputStreamWriter sw = new OutputStreamWriter(fs, "ISO-8859-1"); //$NON-NLS-1$
            BufferedWriter writer = new BufferedWriter(sw);
          ) {
            for (int i = 0; i < lines.size(); i++) {
              String str = lines.get(i);
              if (str.startsWith("CDPATH")) { //$NON-NLS-1$
                str = "CDPATH=" + cdpath; //$NON-NLS-1$
              }

              writer.write(str);
              writer.newLine();
            }
          }

          // show message
          String msg = strFILE_UPDATED.replace("%1", path.getName()); //$NON-NLS-1$
          JOptionPane.showMessageDialog(UE.WINDOW, msg,
            UE.APP_NAME, JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    chkCDProtection.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        PROTECTION.setEnabled(!PROTECTION.isEnabled());
      }
    });

    btnBrowse.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        File path = new File(txtPath.getText());

        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(path.getParentFile());
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle(UE.APP_NAME + " - " + strSELECT_FILE); //$NON-NLS-1$
        fc.setMultiSelectionEnabled(false);

        FileFilter filter = new FileFilter() {
          @Override
          public boolean accept(File f) {
            if (f.isDirectory()) {
              return true;
            }
            String name = f.getName();
            name = name.toLowerCase();
            if (name.endsWith(".ini")) { //$NON-NLS-1$
              return true;
            } else {
              return false;
            }
          }

          @Override
          public String getDescription() {
            return "stbof.ini (*.ini)"; //$NON-NLS-1$
          }
        };
        fc.setFileFilter(filter);

        int retValue = fc.showDialog(UE.WINDOW, strSELECT);

        if (retValue == JFileChooser.APPROVE_OPTION) {
          File fil = fc.getSelectedFile();
          if (fil != null) {
            txtPath.setText(fil.getPath());
          }
        }
      }
    });

    btnEdit.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        String ret = JOptionPane
            .showInputDialog(null, Language.getString("CDProtectionGUI.11"), //$NON-NLS-1$
                UE.APP_NAME, JOptionPane.PLAIN_MESSAGE);
        int i = cmbRoots.getItemCount();
        cmbRoots.addItem(new ID(ret, i));
        cmbRoots.setSelectedIndex(i);
      }
    });
  }

  @Override
  public String menuCommand() {
    return MenuCommand.CdProtection;
  }

  @Override
  public boolean hasChanges() {
    return PROTECTION.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    trek.checkChanged(PROTECTION, changes);
  }

  @Override
  public void reload() throws IOException {
    PROTECTION = (CDprotection) trek.getSegment(SD_App.CDProtection, true);
    loadValues();
  }

  private void loadValues() {
    chkCDProtection.setSelected(PROTECTION.isEnabled());
    chkCDProtection.setEnabled(!PROTECTION.isRemoved());

    File[] roots = File.listRoots();
    cmbRoots.removeAllItems();

    for (int i = 0; i < roots.length; ++i) {
      cmbRoots.addItem(new ID(roots[i].getAbsolutePath(), i));
    }
    cmbRoots.setSelectedIndex(cmbRoots.getItemCount() - 1);

    File path = trek.getSourceFile().getParentFile();
    txtPath.setText(path.getAbsolutePath() + File.separator + "stbof.ini"); //$NON-NLS-1$ //$NON-NLS-2$
    txtINFO.setText(strINFO.replace("%1", path.getAbsolutePath() + File.separator)); //$NON-NLS-1$
  }

  @Override
  public void finalWarning() {
  }
}
