package ue.gui.exe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import lombok.val;
import lombok.var;
import ue.edit.common.FileInfo;
import ue.edit.common.InternalFile;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.SegmentInfo;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.value.ByteValue;
import ue.edit.value.DoubleValue;
import ue.edit.value.FloatValue;
import ue.edit.value.IntValue;
import ue.edit.value.LongValue;
import ue.edit.value.SByteValue;
import ue.edit.value.ShortValue;
import ue.edit.value.StringValue;
import ue.edit.value.ValueType;
import ue.gui.common.FileChanges;
import ue.gui.file.ChangesBaseGUI;
import ue.gui.file.FileListModel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.component.IconButton;
import ue.gui.util.data.ValueComparator;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.gfx.Icons;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.HexTools;

public class SegmentsGUI extends ChangesBaseGUI {

  private static final int DIVIDER_LOCATION = 285;

  private static final int NAME_COLUMN = 0;
  private static final int ADDRESS_COLUMN = 1;
  private static final int TYPE_COLUMN = 2;
  private static final int VALUE_COLUMN = 3;
  private static final int SIZE_COLUMN = 4;
  private static final int STATUS_COLUMN = 5;

  // button tooltips
  private final String TOOLTIP_LoadAll     = Language.getString("SegmentsGUI.23"); //$NON-NLS-1$
  private final String TOOLTIP_DiscardAll  = Language.getString("SegmentsGUI.24"); //$NON-NLS-1$
  // segment description
  private final String DESC_Selected       = Language.getString("SegmentsGUI.12"); //$NON-NLS-1$

  @Override
  protected String descSelected() {
    return DESC_Selected;
  }

  // edited
  private Trek trek;

  // ui components
  private IconButton btnLoadAll = new IconButton("", Icons.LOAD_ALL);
  private IconButton btnDiscardAll = new IconButton("", Icons.DISCARD_ALL);
  private IconButton btnLoad = new IconButton(Language.getString("SegmentsGUI.25"), Icons.LOAD); //$NON-NLS-1$
  private IconButton btnDiscard = new IconButton(Language.getString("SegmentsGUI.26"), Icons.DISCARD); //$NON-NLS-1$
  private IconButton btnReset = new IconButton(Language.getString("SegmentsGUI.27"), Icons.RESET); //$NON-NLS-1$
  private IconButton btnPatch = new IconButton(Language.getString("SegmentsGUI.28"), Icons.PATCH); //$NON-NLS-1$
  private IconButton btnSet = new IconButton(Language.getString("SegmentsGUI.29"), Icons.EDIT); //$NON-NLS-1$

  // create a custom row comparator to sort the value column
  // set by loadValues when the table data is loaded
  private ValueComparator rowCompare = new ValueComparator();

  public SegmentsGUI(Trek trek) throws IOException {
    super(trek);
    this.trek = trek;
    init(NAME_COLUMN, STATUS_COLUMN);
  }

  @Override
  protected String[] entryNames() {
    TableData model = (TableData) tblFILES.getModel();
    return model.entryNames();
  }

  @Override
  protected InternalFile entryFile(FileInfo info) {
    return (InternalFile) trek.getCachedFile(info.getName());
  }

  @Override
  protected String listInfo() {
    return null;
  }

  @Override
  protected String entryDescription(Selection sel) {
    val seg = (InternalSegment) trek.getCachedFile(sel.info.getName());
    String desc = seg != null ? seg.description()
      : CTrekSegments.getSegmentDescription(sel.info.getName());
    return desc != null ? desc : DESC_None;
  }

  @Override
  protected void setupComponents() {
    super.setupComponents();

    // buttons
    btnLoadAll.setToolTipText(TOOLTIP_LoadAll);
    btnLoadAll.setMargin(new Insets(2, 2, 2, 2));
    btnDiscardAll.setToolTipText(TOOLTIP_DiscardAll);
    btnDiscardAll.setMargin(new Insets(2, 2, 2, 2));
  }

  @Override
  protected void placeComponents() {
    super.placeComponents();

    // segment info & status
    JSplitPane splitPane;
    {
      // description
      JScrollPane scrDesc = new JScrollPane(descPanel);
      // set preferred size to not auto-calculate a minimum size on initial window load
      scrDesc.setPreferredSize(new Dimension(0, 0));
      // split pane
      splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, tblScroll, scrDesc);
      splitPane.setDividerLocation(DIVIDER_LOCATION);
      splitPane.setOneTouchExpandable(true);
    }

    // buttons
    JPanel pnlButtons = new JPanel();
    {
      pnlButtons.setLayout(new BoxLayout(pnlButtons, BoxLayout.X_AXIS));
      pnlButtons.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
      pnlButtons.add(btnLoadAll);
      pnlButtons.add(btnLoad);
      pnlButtons.add(Box.createHorizontalStrut(5));
      pnlButtons.add(btnDiscardAll);
      pnlButtons.add(btnDiscard);
      pnlButtons.add(Box.createHorizontalStrut(5));
      pnlButtons.add(btnReset);
      pnlButtons.add(Box.createHorizontalStrut(5));
      pnlButtons.add(btnPatch);
      pnlButtons.add(Box.createHorizontalStrut(5));
      pnlButtons.add(btnSet);
    }

    // main panel
    setLayout(new BorderLayout(0, 0));
    setBorder(BorderFactory.createEmptyBorder(10, 10, 8, 10));
    add(splitPane, BorderLayout.CENTER);
    add(pnlButtons, BorderLayout.SOUTH);
  }

  @Override
  protected void addListeners() {
    super.addListeners();

    btnLoadAll.addActionListener(new CBActionListener(x -> {
      trek.loadReadableSegments();
      refreshFiles(true);
    }));

    btnLoad.addActionListener(new CBActionListener(x -> {
      val sel = getSelectedFiles();
      for (String segName : sel) {
        val sd = CTrekSegments.AllSegmentsByName.get(segName);
        if (sd != null)
          trek.getSegment(sd, true);
        else
          trek.getInternalFile(segName, true);
      }

      // keep selected segments in view
      refreshFiles(true);
    }));

    btnDiscardAll.addActionListener(new CBActionListener(x -> {
      boolean res = Dialogs.showConfirm(Language.getString("SegmentsGUI.31")); //$NON-NLS-1$
      if (res) {
        trek.discard();
        // keep selected segments in view
        refreshFiles(true);
      }
    }));

    btnDiscard.addActionListener(new CBActionListener(x -> {
      val sel = getSelectedFiles();
      for (String segName : sel) {
        trek.discard(segName);
      }

      // select next element
      refreshFiles(false);
    }));

    btnReset.addActionListener(new CBActionListener(x -> {
      val sel = getSelectedFiles();
      for (String segName : sel) {
        val sd = CTrekSegments.AllSegmentsByName.get(segName);
        val seg = sd != null ? trek.getSegment(sd, true)
          : trek.getInternalFile(segName, true);
        seg.reset();
      }

      // select next element
      refreshFiles(false);
    }));

    btnPatch.addActionListener(new CBActionListener(x -> {
      val sel = getSelectedFiles();
      for (String segName : sel) {
        val sd = CTrekSegments.AllSegmentsByName.get(segName);
        val seg = sd != null ? trek.getSegment(sd, true)
          : trek.getInternalFile(segName, true);
        if (seg instanceof Patch)
          ((Patch)seg).patch();
      }

      // keep edited segments in view
      refreshFiles(true);
    }));

    btnSet.addActionListener(new CBActionListener(x -> {
      val sel = getSelectedFiles();
      if (sel.isEmpty())
        return;

      String initialValue = null;
      if (sel.size() == 1) {
        String segName = sel.iterator().next();
        val seg = loadSegment(segName);
        if (seg instanceof ValueType)
          initialValue = seg.toString();
      }

      String msg = Language.getString("SegmentsGUI.30"); //$NON-NLS-1$
      String value = JOptionPane.showInputDialog(msg, initialValue);

      if (value != null) {
        val dval = DataTools.tryParseDouble(value).orElse(null);
        val lval = DataTools.tryParseLong(value).orElse(null);
        boolean isSByte = lval != null && lval >= Byte.MIN_VALUE && lval <= Byte.MAX_VALUE;
        boolean isByte  = lval != null && lval >= 0 && lval <= 255;
        boolean isShort = lval != null && lval >= Short.MIN_VALUE && lval <= Short.MAX_VALUE;
        boolean isInt   = lval != null && lval >= Integer.MIN_VALUE && lval <= Integer.MAX_VALUE;

        for (String segName : sel) {
          val seg = loadSegment(segName);
          if (lval != null) {
            if (isSByte && seg instanceof SByteValue)
              ((SByteValue)seg).setValue(lval.byteValue());
            else if (isByte && seg instanceof ByteValue)
              ((ByteValue)seg).setValue(lval.intValue());
            else if (isShort && seg instanceof ShortValue)
              ((ShortValue)seg).setValue(lval.shortValue());
            else if (isInt && seg instanceof IntValue)
              ((IntValue)seg).setValue(lval.intValue());
            else if (seg instanceof LongValue)
              ((LongValue)seg).setValue(lval);
          } else if (dval != null) {
            if (seg instanceof FloatValue)
              ((FloatValue)seg).setValue(dval.floatValue());
            else if (seg instanceof DoubleValue)
              ((DoubleValue)seg).setValue(dval.doubleValue());
            else if (seg instanceof IntValue)
              ((IntValue)seg).setFloat(dval.floatValue());
            else if (seg instanceof LongValue)
              ((LongValue)seg).setDouble(dval.doubleValue());
          }

          if (seg instanceof StringValue)
            ((StringValue)seg).setValue(value);
        }
      }

      // keep edited segments in view
      refreshFiles(true);
    }));

    // open search even when some button has focus
    addKeyListener(searchBox.SearchKeyListener);
    btnLoadAll.addKeyListener(searchBox.SearchKeyListener);
    btnDiscardAll.addKeyListener(searchBox.SearchKeyListener);
    btnLoad.addKeyListener(searchBox.SearchKeyListener);
    btnDiscard.addKeyListener(searchBox.SearchKeyListener);
    btnReset.addKeyListener(searchBox.SearchKeyListener);
    btnPatch.addKeyListener(searchBox.SearchKeyListener);
    btnPatch.addKeyListener(searchBox.SearchKeyListener);
    btnSet.addKeyListener(searchBox.SearchKeyListener);
  }

  @Override
  protected void loadValues() throws IOException {
    val files = trek.listSegments(true);
    tblFILES.setModel(new TableData(files));

    // setup columns
    val columnModel = tblFILES.getColumnModel();
    val sizeColumn = columnModel.getColumn(SIZE_COLUMN);
    val valueColumn = columnModel.getColumn(VALUE_COLUMN);
    val rightRenderer = new DefaultTableCellRenderer();
    rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
    columnModel.getColumn(ADDRESS_COLUMN).setMaxWidth(100);
    // limit value to trek.exe rounded nine decimal places of PI
    // @see galaxyShape_spiralArmArc at 0x0017D5A8
    valueColumn.setMaxWidth(65);
    valueColumn.setCellRenderer(rightRenderer);
    sizeColumn.setMaxWidth(50);
    sizeColumn.setCellRenderer(rightRenderer);
    columnModel.getColumn(STATUS_COLUMN).setMaxWidth(50);

    if (files.size() > 0) {
      updateListRenderer();
      var rowSorter = updateRowSorter();
      // set custom value compare
      rowSorter.setComparator(VALUE_COLUMN, rowCompare);
    }

    updateDescription();
  }

  private InternalSegment loadSegment(String segName) throws IOException {
    val sd = CTrekSegments.AllSegmentsByName.get(segName);
    return sd != null ? trek.getSegment(sd, true)
      : trek.getInternalFile(segName, true);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Segments;
  }

  @Override
  public boolean hasChanges() {
    return false;
  }

  @Override
  public void reload() throws IOException {
    refreshFiles(true);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  private void refreshFiles(boolean selByName) {
    Set<String> sel = selByName ? getSelectedFiles() : null;
    int selIdx = tblFILES.getSelectedRow();

    // update file list
    val seg = trek.listSegments(true);
    ((TableData)tblFILES.getModel()).setData(seg);

    // try to restore previous selection
    if (sel != null)
      selectFiles(sel);
    else if (selIdx >= 0)
      tblFILES.addRowSelectionInterval(selIdx, selIdx);

    updateDescription();
  }

  @Override
  public void finalWarning() {
  }

  @Override
  protected Optional<byte[]> loadUnchanged(InternalFile file) throws IOException {
    InternalSegment seg = (InternalSegment)file;
    return Optional.of(trek.getRawData(seg.address(), seg.getSize()));
  }

  @Override
  protected byte[] loadDefault(InternalFile file) throws IOException {
    InternalSegment seg = (InternalSegment)file;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    seg.saveDefault(out);
    return out.toByteArray();
  }

  private class TableData extends AbstractTableModel implements FileListModel {
    public static final int COLUMNS = 6;

    private SegmentInfo[] segInfos;

    // the list must be sorted in order to index the row
    public TableData(Collection<SegmentInfo> data) {
      segInfos = data.toArray(new SegmentInfo[0]);
    }

    public void setData(Collection<SegmentInfo> data) {
      segInfos = data.toArray(new SegmentInfo[0]);
      fireTableDataChanged();
    }

    @Override
    public int getColumnCount() {
      return COLUMNS;
    }

    @Override
    public int getRowCount() {
      return segInfos.length;
    }

    public String[] entryNames() {
      return Arrays.stream(segInfos).map(SegmentInfo::getName).toArray(String[]::new);
    }

    public SegmentInfo getInfo(int row) {
      return segInfos[row];
    }

    @Override
    public Object getValueAt(int row, int col) {
      val info = segInfos[row];

      // for sorting of numbers, keep the data types
      switch (col) {
        case NAME_COLUMN:
          return info.getName();
        case ADDRESS_COLUMN:
          return "0x" + HexTools.toHex(info.getAddress());
        case TYPE_COLUMN:
          return info.getClassType().getSimpleName();
        case SIZE_COLUMN:
          return info.getSize();
        case STATUS_COLUMN:
          return info.getStatus().name();
        case VALUE_COLUMN:
          return info.getValue();
        default:
          return null;
      }
    }

    @Override
    public String getColumnName(int columnIndex) {
      switch (columnIndex) {
        case NAME_COLUMN:
          return Language.getString("SegmentsGUI.0"); //$NON-NLS-1$
        case ADDRESS_COLUMN:
          return Language.getString("SegmentsGUI.1"); //$NON-NLS-1$
        case TYPE_COLUMN:
          return Language.getString("SegmentsGUI.2"); //$NON-NLS-1$
        case VALUE_COLUMN:
          return Language.getString("SegmentsGUI.3"); //$NON-NLS-1$
        case SIZE_COLUMN:
          return Language.getString("SegmentsGUI.4"); //$NON-NLS-1$
        case STATUS_COLUMN:
          return Language.getString("SegmentsGUI.5"); //$NON-NLS-1$
        default:
          return "";
      }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      switch (columnIndex) {
        case NAME_COLUMN:
        case ADDRESS_COLUMN:
        case TYPE_COLUMN:
        case STATUS_COLUMN:
          return String.class;
        case SIZE_COLUMN:
          return Integer.class;
        default:
          return Object.class;
      }
    }
  }
}
