package ue.gui.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map.Entry;
import lombok.val;
import ue.edit.common.FileLookup;

/**
 * This class is used to list file changes by MainPanels.
 *
 * Attention, for settings, sound files and utilities like Tga2AniGUI,
 * FileLookup can be null!
 */
public class FileChangeList extends ArrayList<FileChangeList.FileEntry> {

  public class FileEntry {
    public FileLookup file;
    public ArrayList<String> entries;

    public FileEntry(){}
    public FileEntry(FileLookup fl, Collection<String> entries){
      this.file = fl;
      this.entries = new ArrayList<String> (entries);
    }
  }

  public FileChangeList() {
  }

  public FileChangeList(Collection<Entry<FileLookup, HashSet<String>>> fileChanges) {
    ensureCapacity(fileChanges.size());
    for (val fileChange : fileChanges) {
      this.add(new FileEntry(fileChange.getKey(), fileChange.getValue()));
    }
  }

  public void sort() {
    // sort main files
    sort(new Comparator<FileEntry>() {
      @Override
      public int compare(FileEntry entry1, FileEntry entry2) {
        if (entry1.file == entry2.file)
          return 0;
        if (entry1.file == null)
          return -1;
        if (entry2.file == null)
          return 1;
        return entry1.file.getName().compareTo(entry2.file.getName());
      }
    });

    // sort all file entries
    for (val entry : this) {
      entry.entries.sort(null);
    }
  }
}