package ue.gui.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import lombok.val;
import ue.edit.common.FileLookup;

/**
 * This class is used to list file changes by MainPanels.
 *
 * Attention, for settings, sound files and utilities like Tga2AniGUI,
 * FileLookup can be null!
 */
public class FileChanges extends HashMap<FileLookup, HashSet<String>> {
  public void add(FileLookup file, String entry) {
    val fileList = lookup(file);
    fileList.add(entry);
  }

  public void addAll(FileLookup file, Set<String> entries) {
    if (!entries.isEmpty()) {
      val fileList = lookup(file);
      fileList.addAll(entries);
    }
  }

  private HashSet<String> lookup(FileLookup file) {
    val existing = get(file);
    if (existing != null)
      return existing;

    val created = new HashSet<String>();
    put(file, created);
    return created;
  }

  public FileChangeList toList() {
    return new FileChangeList(entrySet());
  }
}