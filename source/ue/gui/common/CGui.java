package ue.gui.common;

import java.awt.Color;

public class CGui {
  public static final Color[] RACE_COLORS = new Color[] { Color.MAGENTA, Color.CYAN, Color.YELLOW, Color.RED, Color.GREEN };
  public static final String[] RACE_COLOR_CODES = new String[] { "fuchsia", "aqua", "yellow", "red", "lime" };
}
