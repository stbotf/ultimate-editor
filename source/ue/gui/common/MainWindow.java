package ue.gui.common;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.FilesInterface;
import ue.edit.common.CanBeEdited;
import ue.edit.common.FileArchive;
import ue.edit.common.FileLookup;
import ue.gui.exe.CDProtectionGUI;
import ue.gui.exe.SegmentsGUI;
import ue.gui.exe.TrekPatchesGUI;
import ue.gui.exe.emp.AIShipSetsGUI;
import ue.gui.exe.emp.MEStartCondGUI;
import ue.gui.exe.emp.ShipMapRangesGUI;
import ue.gui.exe.map.GalacticMapGUI;
import ue.gui.exe.shp.ConstructionGUI;
import ue.gui.file.CheckGUI;
import ue.gui.file.FilesGUI;
import ue.gui.file.ModifiedFilesGUI;
import ue.gui.main.AboutGUI;
import ue.gui.main.HelpWindow;
import ue.gui.main.SettingsPanel;
import ue.gui.main.StartPagePanel;
import ue.gui.menu.MainMenu;
import ue.gui.menu.MenuCommand;
import ue.gui.sav.GameConfigGUI;
import ue.gui.sav.emp.EmpireInfoGUI;
import ue.gui.sav.emp.StrcInfoGUI;
import ue.gui.sav.emp.SystemsInfoGUI;
import ue.gui.sav.map.MapGUI;
import ue.gui.sav.map.StarBaseInfoGUI;
import ue.gui.sav.map.StellInfoGUI;
import ue.gui.sav.stats.GameInfoGUI;
import ue.gui.snd.SoundGUI;
import ue.gui.snd.SwitchRaceGUI;
import ue.gui.special.CalcGUI;
import ue.gui.special.ConvertAddressGUI;
import ue.gui.special.Image2TgaGUI;
import ue.gui.special.MonitorGameGUI;
import ue.gui.special.Tga2AniGUI;
import ue.gui.stbof.bld.BldSetGUI;
import ue.gui.stbof.bld.BuildingsGUI;
import ue.gui.stbof.bld.BuildingsMultiSetGUI;
import ue.gui.stbof.bld.BuildingsReportGUI;
import ue.gui.stbof.emp.AIBldReqGUI;
import ue.gui.stbof.emp.AiminorGUI;
import ue.gui.stbof.emp.ColValGUI;
import ue.gui.stbof.emp.IntelGUI;
import ue.gui.stbof.emp.MeplanetGUI;
import ue.gui.stbof.emp.MoraleGUI;
import ue.gui.stbof.emp.RaceInfoGUI;
import ue.gui.stbof.emp.StartConGUI;
import ue.gui.stbof.gfx.AniGUI;
import ue.gui.stbof.gfx.HobGUI;
import ue.gui.stbof.gfx.TextureGUI;
import ue.gui.stbof.lex.InGameTextGUI;
import ue.gui.stbof.lex.LexiconGUI;
import ue.gui.stbof.map.EnvironGUI;
import ue.gui.stbof.map.MaxPopulationGUI;
import ue.gui.stbof.map.ObjstrucGUI;
import ue.gui.stbof.map.PlanetBonusGUI;
import ue.gui.stbof.map.PlanetGUI;
import ue.gui.stbof.map.StarnameGUI;
import ue.gui.stbof.shp.ShipGUI;
import ue.gui.stbof.shp.ShipModelGUI;
import ue.gui.stbof.shp.ShipMultiGUI;
import ue.gui.stbof.shp.ShipnameGUI;
import ue.gui.stbof.shp.ShipsReportGUI;
import ue.gui.stbof.tec.TechGUI;
import ue.gui.stbof.tec.TechMultiSetGUI;
import ue.gui.stbof.wdf.WDFEditorGUI;
import ue.gui.util.dialog.Dialogs;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.app.AppVersion;

/**
 * The main window.
 *
 * This is a subclass of JFrame. It shows demanded panels and contains a menu on top.
 **/
public class MainWindow extends JFrame {

  public static final URI AFC_URL = URI.create("https://www.armadafleetcommand.com/onscreen/botf/viewforum.php?f=125"); //$NON-NLS-1$
  public static final URI GIT_URL = URI.create("https://gitlab.com/stbotf/ultimate-editor"); //$NON-NLS-1$
  public static final String DefaultSHFile = "blank.html"; //$NON-NLS-1$
  public static final String ReadmeFile = "Readme.txt"; //$NON-NLS-1$
  public static final String ChangelogFile = "Changelog.txt"; //$NON-NLS-1$

  @Getter @Accessors(fluent = true)
  private MainPanel currentPanel = null;
  @Getter @Accessors(fluent = true)
  private MainMenu mainMenu = new MainMenu();

  private String curSHFile = null;

  /**
   * Constructs an instance of MainWindow.
   *
   * Title is "Ultimate Editor" Default size is 600x480 pixels. Loads in the middle of the screen.
   */
  public MainWindow() {
    setupComponents();
    setupLayout();
    addListeners();
    setVisible(true);
  }

  public void showFileChanges(int fileType) {
    try {
      MainMenu.showFileChanges(fileType);
    } catch (Exception p) {
      Dialogs.displayError(p);
    }
  }

  /**
   * Displays the provided panel in window.
   *
   * Calls the currently shown panel's finalWarning() function, then removes it
   * and adds the specified one. In case of unsaved changes or errors,
   * the user is asked whether to abort.
   *
   * If the new panel is larger than window space, the window sets it's size to
   * provide enough space. Panels size is read with the getPreferredSize() function.
   *
   * @param panel     The MainPanel to display. Accepts null for no panel.
   * @returns whether the panel got changed (true) or whether it was aborted (false)
   */
  public boolean showPanel(MainPanel panel) {
    // first finalize the current loaded panel
    // to make sure all changes are applied when the new panel is loaded
    // keep current panel to allow for manual fixes
    if (!finalWarning())
      return false;

    displayPanelImpl(panel);
    return true;
  }

  /**
   * This function creates and displays a MainPanel.
   *
   * @param command Identifier for the panel to show.
   * @throws IOException
   */
  public boolean showPanel(String command) throws IOException {
    MainPanel panel = createPanel(command);
    return panel != null ? UE.WINDOW.showPanel(panel) : false;
  }

  public void showStartPage() {
    showPanel(new StartPagePanel());
  }

  public void showHelp() {
    String title = Language.getString("MainWindow.3"); //$NON-NLS-1$
    String fileName = curSHFile != null ? curSHFile: DefaultSHFile;
    new HelpWindow(title, FileManager.SHPath, fileName);
  }

  public void showFAQ() {
    String title = Language.getString("MainWindow.4"); //$NON-NLS-1$
    new HelpWindow(title, FileManager.FAQPath, "faq.html"); //$NON-NLS-1$
  }

  public void showReadme() throws IOException {
    FileManager.openLocalFile(ReadmeFile);
  }

  public void showChangelog() throws IOException {
    FileManager.openLocalFile(ChangelogFile);
  }

  public void openAfcWebsite() throws IOException {
    Desktop.getDesktop().browse(AFC_URL);
  }

  public void openGitWebsite() throws IOException {
    Desktop.getDesktop().browse(GIT_URL);
  }

  public void showAbout() {
    new AboutGUI();
  }

  /**
   * Closes the current panel if any.
   *
   * Calls the currently shown panel's finalWarning() function, before removing it.
   * In case of unsaved changes or errors, the user is asked whether to abort.
   */
  public MainPanel closePanel() {
    MainPanel prev = currentPanel;
    showPanel((MainPanel)null);
    return prev;
  }

  private void setupComponents() {
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setIconImage(UE.ICON);
    setTitle(null);
    setJMenuBar(mainMenu);
  }

  private void setupLayout() {
    setLayout(new BorderLayout());
    setSize(600, 480);
    setMinimumSize(new Dimension(300, 60));

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation(new Point((int) (screenSize.getWidth() / 2 - 300),
        (int) (screenSize.getHeight() / 2 - 240)));
  }

  private MainPanel createPanel(String command) throws IOException {
    MainPanel panel = null;

    switch (command) {
      // shared
      case MenuCommand.Check:               panel = new CheckGUI(UE.FILES.getPrimaryFile());                  break;
      case MenuCommand.Files: {
        CanBeEdited file = UE.FILES.getPrimaryFile();
        panel = new FilesGUI(new FilesInterface((FileArchive)file));
        break;
      }
      case MenuCommand.ModList: {
        CanBeEdited file = UE.FILES.getPrimaryFile();
        // update modified files list first
        file.updateModList();
        // disable main window
        UE.WINDOW.setEnabled(false);
        // this is a separate window, not a panel
        new ModifiedFilesGUI(file);
        break;
      }
      // MainMenu
      case MenuCommand.StartPage:           panel = new StartPagePanel();                                     break;
      case MenuCommand.Settings:            panel = new SettingsPanel();                                      break;
      // TrekMenu
      case MenuCommand.AiShpBldSets:        panel = new AIShipSetsGUI(UE.FILES.trek(), UE.FILES.stbof());     break;
      case MenuCommand.StartCond:           panel = new MEStartCondGUI(UE.FILES.trek(), UE.FILES.stbof());    break;
      case MenuCommand.GalaxyGen:           panel = new GalacticMapGUI(UE.FILES.trek());                      break;
      case MenuCommand.Construction:        panel = new ConstructionGUI(UE.FILES.trek());                     break;
      case MenuCommand.MapRanges:           panel = new ShipMapRangesGUI(UE.FILES.trek(), UE.FILES.stbof());  break;
      case MenuCommand.CdProtection:        panel = new CDProtectionGUI(UE.FILES.trek());                     break;
      case MenuCommand.Graphics:            panel = new TrekPatchesGUI(UE.FILES.trek(), UE.FILES.stbof());    break;
      case MenuCommand.Segments:            panel = new SegmentsGUI(UE.FILES.trek());                         break;
      // StbofMenu
      case MenuCommand.AiBldReq:            panel = new AIBldReqGUI(UE.FILES.stbof());                        break;
      case MenuCommand.Animations:          panel = new AniGUI(UE.FILES.stbof());                             break;
      case MenuCommand.BuildingGroupEdit:   panel = new BuildingsMultiSetGUI(UE.FILES.stbof());               break;
      case MenuCommand.BuildingStats:       panel = new BuildingsGUI(UE.FILES.stbof(), UE.FILES.trek());      break;
      case MenuCommand.ColonyVal:           panel = new ColValGUI(UE.FILES.stbof());                          break;
      case MenuCommand.Environments:        panel = new EnvironGUI(UE.FILES.stbof());                         break;
      case MenuCommand.GameText:            panel = new InGameTextGUI(UE.FILES.stbof());                      break;
      case MenuCommand.HobFiles:            panel = new HobGUI(UE.FILES.stbof());                             break;
      case MenuCommand.InstallModels:       panel = new ShipModelGUI(UE.FILES.stbof(), UE.FILES.trek());      break;
      case MenuCommand.IntelMult:           panel = new IntelGUI(UE.FILES.stbof());                           break;
      case MenuCommand.EdificeReport:       panel = new BuildingsReportGUI(UE.FILES.stbof());                 break;
      case MenuCommand.Lexicon:             panel = new LexiconGUI(UE.FILES.stbof());                         break;
      case MenuCommand.MaxPlanetPop:        panel = new MaxPopulationGUI(UE.FILES.stbof());                   break;
      case MenuCommand.MeSystems:           panel = new MeplanetGUI(UE.FILES.stbof());                        break;
      case MenuCommand.MinorAttitudes:      panel = new AiminorGUI(UE.FILES.stbof(), UE.FILES.trek());        break;
      case MenuCommand.MoralePenalties:     panel = new MoraleGUI(UE.FILES.stbof());                          break;
      case MenuCommand.PlanetBonuses:       panel = new PlanetBonusGUI(UE.FILES.stbof());                     break;
      case MenuCommand.Planets:             panel = new PlanetGUI(UE.FILES.stbof());                          break;
      case MenuCommand.RacesInfo:           panel = new RaceInfoGUI(UE.FILES.stbof(), UE.FILES.trek());       break;
      case MenuCommand.ShipGroupEdit:       panel = new ShipMultiGUI(UE.FILES.stbof());                       break;
      case MenuCommand.ShipNames:           panel = new ShipnameGUI(UE.FILES.stbof(), UE.FILES.trek());       break;
      case MenuCommand.ShipStats:           panel = new ShipGUI(UE.FILES.stbof(), UE.FILES.trek());           break;
      case MenuCommand.ShiplistReport:      panel = new ShipsReportGUI(UE.FILES.stbof());                     break;
      case MenuCommand.StarNames:           panel = new StarnameGUI(UE.FILES.stbof());                        break;
      case MenuCommand.StartBuildings:      panel = new BldSetGUI(UE.FILES.stbof());                          break;
      case MenuCommand.StartTech:           panel = new StartConGUI(UE.FILES.stbof());                        break;
      case MenuCommand.StellarObj:          panel = new ObjstrucGUI(UE.FILES.stbof(), UE.FILES.trek());       break;
      case MenuCommand.TechFields:          panel = new TechGUI(UE.FILES.stbof(), UE.FILES.trek());           break;
      case MenuCommand.TechFieldsGroupEdit: panel = new TechMultiSetGUI(UE.FILES.stbof());                    break;
      case MenuCommand.Textures:            panel = new TextureGUI(UE.FILES.stbof());                         break;
      case MenuCommand.WdfEditor:           panel = new WDFEditorGUI(UE.FILES.stbof());                       break;
      // SavMenu
      case MenuCommand.SystemInfo:          panel = new SystemsInfoGUI(UE.FILES.savGame(), UE.FILES.stbof());               break;
      case MenuCommand.StellInfo:           panel = new StellInfoGUI(UE.FILES.savGame());                                   break;
      case MenuCommand.Starbases:           panel = new StarBaseInfoGUI(UE.FILES.savGame(), UE.FILES.stbof());              break;
      case MenuCommand.GameConfig:          panel = new GameConfigGUI(UE.FILES.savGame(), UE.FILES.stbof());                break;
      case MenuCommand.GameInfo:            panel = new GameInfoGUI(UE.FILES.savGame(), UE.FILES.stbof());                  break;
      case MenuCommand.Empires:             panel = new EmpireInfoGUI(UE.FILES.savGame(), UE.FILES.stbof());                break;
      case MenuCommand.MapV2:               panel = new MapGUI(UE.FILES.savGame(), UE.FILES.stbof(), UE.FILES.trek());      break;
      case MenuCommand.Strcinfo:            panel = new StrcInfoGUI(UE.FILES.savGame(), UE.FILES.stbof(), UE.FILES.trek()); break;
      // EnglishMenu
      case MenuCommand.Voices:              panel = new SoundGUI(UE.FILES.english(), UE.FILES.stbof(), MenuCommand.Voices); break;
      case MenuCommand.SwitchRaces:         panel = new SwitchRaceGUI(UE.FILES.english(), UE.FILES.stbof());                break;
      // MusicMenu
      case MenuCommand.Music:               panel = new SoundGUI(UE.FILES.music(), UE.FILES.stbof(), MenuCommand.Music);    break;
      // SfxMenu
      case MenuCommand.GameSounds:          panel = new SoundGUI(UE.FILES.sfx(), UE.FILES.stbof(), MenuCommand.GameSounds); break;
      // SpecialMenu
      case MenuCommand.CreateAniCur:        panel = new Tga2AniGUI();                                         break;
      case MenuCommand.ConvertImage:        panel = new Image2TgaGUI();                                       break;
      case MenuCommand.SuperviseMulti:      panel = new MonitorGameGUI();                                     break;
      case MenuCommand.Calculator:          panel = new CalcGUI();                                            break;
      case MenuCommand.ConvertAddresses:    panel = new ConvertAddressGUI();                                  break;
    }

    return panel;
  }

  /**
   * Displays the provided panel in window.
   *
   * Unlike showPanel it skips the finalWarning call but forcefully replaces
   * any current panel with the specified one. Any left panel changes are lost!
   *
   * If the new panel is larger than window space, the window sets it's size to
   * provide enough space. Panels size is read with the getPreferredSize() function.
   *
   * @param panel     The MainPanel to display. Accepts null for no panel.
   */
  private void displayPanelImpl(MainPanel panel) {
    // remove current panel if any
    if (currentPanel != null) {
      remove(currentPanel);
      currentPanel = null;
    }

    if (panel != null) {
      Dimension pan = panel.getPreferredSize();
      Dimension win = getSize();
      double w;
      double h;

      if (pan.getWidth() > this.getContentPane().getWidth()) {
        w = pan.getWidth() + this.getContentPane().getWidth() - pan.getWidth();
      } else {
        w = win.getWidth();
      }

      if (pan.getHeight() > this.getContentPane().getHeight()) {
        h = win.getHeight() + this.getContentPane().getHeight() - pan.getHeight();
      } else {
        h = win.getHeight();
      }

      setMinimumSize(
          new Dimension((int) pan.getWidth() + getWidth() - this.getContentPane().getWidth(),
              (int) pan.getHeight() + getHeight() - this.getContentPane().getHeight()));
      win.setSize(w, h);
      setSize(win);
      add(panel, BorderLayout.CENTER);
    } else {
      setMinimumSize(new Dimension(300, 60));
    }

    if ((this.getExtendedState() & MAXIMIZED_BOTH) != MAXIMIZED_BOTH) {
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

      if (this.getX() + this.getWidth() > screenSize.width
          || this.getY() + this.getHeight() > screenSize.height) {
        setLocation(new Point(
            (int) ((screenSize.getWidth() - getWidth()) / 2),
            (int) ((screenSize.getHeight() - getHeight()) / 2)));
      }
    }

    currentPanel = panel;
    updateWindowTitle();
    // update displayed panel help file
    curSHFile = panel != null ? panel.getHelpFileName() : null;
    mainMenu.viewChanged(panel);
    repaint();
    validate();
  }

  public void updateWindowTitle() {
    updateWindowTitle(currentPanel != null ? currentPanel.getTitleName() : null);
  }

  public void updateWindowTitle(String pageTitle) {
    String fileName = UE.FILES.getOpenedPrimaryFileName();
    Boolean hasFileName = fileName != null && fileName.length() > 0;
    Boolean hasPageName = pageTitle != null && pageTitle.length() > 0;
    String title = hasFileName ? hasPageName ?
        fileName + " - " + pageTitle : fileName : pageTitle;
    setTitle(title);
  }

  @Override
  public void setTitle(String title) {
    title = title != null && title.length() > 0 ? UE.APP_NAME + " - " + title : UE.APP_NAME;
    super.setTitle(title);
  }

  /**
   * Calls the currently showed panel's finalWarning() function if any.
   * @returns true if successful or dismissed, false if cancelled
   */
  public boolean finalWarning() {
    if (currentPanel != null) {
      try {
        currentPanel.finalWarning();
      } catch (Exception ex) {
        ex.printStackTrace();
        String msg = Language.getString("MainWindow.0") + ex;
        // prevent panel switch if cancelled
        return Dialogs.OK == Dialogs.confirmError(msg);
      }
    }
    return true;
  }

  public void onSaved() {
    // update window title name for in case the file name changed
    updateWindowTitle();

    if (currentPanel instanceof SegmentsGUI || currentPanel instanceof FilesGUI) {
      try {
        currentPanel.reload();
      }
      catch (IOException e) {
        Dialogs.displayError(e);
      }
    }

    // update backups menu
    mainMenu.refreshRecentFilesAndBackups();
  }

  // 'File->Open...'
  // Closes all files, then prompts the user to choose a file to open.
  // The file is opened using the FileManager's open(File file, boolean addToRecent) method
  // which returns an array of commands. Those commands are then added to the 'Edit' menu.
  public void openFile() {
    // close files, clear panel and reset the menu
    if (!closeAllFiles())
      return;

    File dir = new File(UE.SETTINGS.getProperty(SettingsManager.LAST));
    JFileChooser jfc = new JFileChooser(dir);

    FileFilter filter = new FileFilter() {
      @Override
      public boolean accept(File f) {
        if (f.isDirectory()) {
          return true;
        }
        String name = f.getName();
        name = name.toLowerCase();
        if (name.endsWith(".res") || name.endsWith(".sav") //$NON-NLS-1$ //$NON-NLS-2$
          || name.endsWith(".snd") || name.endsWith(".exe")) { //$NON-NLS-1$ //$NON-NLS-2$
          return true; //$NON-NLS-1$
        } else {
          return false;
        }
      }

      @Override
      public String getDescription() {
        return "*.res, *.sav, *.snd, *.exe"; //$NON-NLS-1$
      }
    };

    jfc.setFileFilter(filter);
    int returnVal = jfc.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION)
      openFileImpl(jfc.getSelectedFile());
  }

  public void openFile(String filePath) {
    openFile(new File(filePath));
  }

  public void openFile(File file) {
    // abort if current page or files have changes
    // and the user selected to cancel the request
    if (!closeAllFiles())
      return;

    openFileImpl(file);
  }

  private void openFileImpl(File file) {
    try {
      UE.FILES.open(file);
      // show initial file view
      showDefaultFileView();
    }
    catch (Exception e) {
      Dialogs.displayError(e);
      showStartPage();
    }

    // since files were closed for sure,
    // make sure to refresh the menu
    mainMenu.refreshEditOptions();
    mainMenu.refreshRecentFilesAndBackups();
  }

  // 'File->Save'
  // Saves all changes of the open and all loaded secondary files.
  public void save() {
    // first finalize the current loaded panel
    // to make sure all changes are applied
    if (!finalWarning())
      return;

    // save if changed only
    if (UE.FILES.save(null, true)) {
      // cleanup backups if successful
      UE.FILES.deleteOldBackups();
    }

    // refresh segment & file GUI
    onSaved();
  }

  // 'File->Save As..'
  // Prompts the user where to save to.
  public void saveAs() {
    // first finalize the current loaded panel
    // to make sure all changes are applied
    if (!finalWarning())
      return;

    JFileChooser jfc = new JFileChooser(new File(UE.SETTINGS.getProperty(SettingsManager.LAST)));
    int returnVal = jfc.showSaveDialog(this);
    if (returnVal != JFileChooser.APPROVE_OPTION)
      return;

    File filo = jfc.getSelectedFile();
    if (filo.exists() && !Dialogs.confirmOverwrite(filo.getName()))
      return;

    // save file to target even if unchanged
    if (UE.FILES.save(filo, false)) {
      // cleanup backups if successful
      UE.FILES.deleteOldBackups();
    }

    // refresh segment & file GUI
    onSaved();
  }

  // 'File->Close'
  // Closes all files, the current panel and resets the menu.
  public void close() {
    if (closeAllFiles()) {
      mainMenu.onClosed();
      showStartPage();
    }
  }

  // 'File->Exit'
  // Closes all files, the current panel, then saves settings and exits the program.
  public void exit() {
    if (closeAllFiles()) {
      UE.SETTINGS.saveSettings();
      // say bye bye
      System.exit(0);
    }
  }

  public void showDefaultFileView() {
    try {
      // reset the view for default panels
      // this is required to empty the screen for when no new panel is shown
      if (isDefaultPanel())
        resetView();

      // open default file panel
      switch (FileManager.getPRIMARY()) {
        case FileManager.P_TREK:
          showPanel(MenuCommand.Segments);
          break;
        case FileManager.P_STBOF:
        case FileManager.P_ALT:
          showPanel(MenuCommand.Files);
          break;
        case FileManager.P_SAVE_GAME:
          showPanel(MenuCommand.GameInfo);
          break;
        case FileManager.P_ENGLISH:
          showPanel(MenuCommand.Voices);
          break;
        case FileManager.P_MUSIC:
          showPanel(MenuCommand.Music);
          break;
        case FileManager.P_SFX:
          showPanel(MenuCommand.GameSounds);
          break;
      }
    }
    catch (Exception e) {
      Dialogs.displayError(e);
      showStartPage();
    }
  }

  public void reload() throws IOException {
    if (currentPanel == null)
      return;

    val fileChanges = currentPanel.filesChanged();
    if (!fileChanges.isEmpty()) {
      String title = Language.getString("MainWindow.1");
      val sb = new StringBuilder(Language.getString("MainWindow.2"));

      // convert to change list to sort by file archive name
      val changeList = fileChanges.toList();
      changeList.sort();

      for (val change : changeList) {
        FileLookup fl = change.file;
        sb.append("\n");
        if (fl != null)
          sb.append("\n" + fl.getName() + ":");
        for (String file : change.entries)
          sb.append("\n" + file);
      }

      if (!Dialogs.showConfirm(title, sb.toString()))
        return;

      // make sure to discard all the listed entry changes
      for (val change : changeList) {
        FileLookup fl = change.file;
        if (fl != null)
          fl.discard(change.entries);
      }
    }

    // Reload the panel view and JUST the panel view!
    // To make sure all reloaded changes have been listed by the user confirm,
    // actual file changes must be reloaded or discarded on the archive itself.
    currentPanel.reload();
  }

  // @returns false if cancelled
  public boolean closeAllFiles() {
    if (!closeFilesOnly())
      return false;

    // reset the menus, including edit menu and help file
    resetMenu();

    // make sure to reset the view when the current panel might depend on closed files
    // that is all trek, stbof, alt, and snd edit panels
    if (!isDefaultPanel())
      resetView();

    return true;
  }

  public void notifyUpdate(AppVersion updateInfo) {
    if (currentPanel instanceof StartPagePanel)
      ((StartPagePanel)currentPanel).notifyUpdate(updateInfo);
    else if (currentPanel instanceof SettingsPanel)
      ((SettingsPanel)currentPanel).notifyUpdate(updateInfo);
  }

  private void addListeners() {
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        // close window and exit program
        UE.WINDOW.exit();
      }
    });
  }

  private boolean isDefaultPanel() {
    return currentPanel instanceof DefaultPanel;
  }

  // @returns false if cancelled
  private boolean closeFilesOnly() {
    // first warn to apply final panel changes
    if (!finalWarning())
      return false;

    // close all files, abort if unsaved
    if (UE.FILES.closeAll()) {
      // cleanup backups if successful
      UE.FILES.deleteOldBackups();
      return true;
    }
    return false;
  }

  // remove current panel and reset the menu and window title
  private void resetView() {
    // remove current panel
    if (currentPanel != null) {
      remove(currentPanel);
      currentPanel = null;
    }

    // reset window title
    updateWindowTitle();

    // refresh view, required to empty the screen when no new panel is shown
    repaint();
    validate();
  }

  // reset the menus, including edit menu and help file
  private void resetMenu() {
    mainMenu.reset();
    // reset help file
    curSHFile = null;
  }

}
