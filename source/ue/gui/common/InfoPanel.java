package ue.gui.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import lombok.val;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.stbof.gfx.AniLabel.AlertType;

/**
 * A simple panel with an image and three lines of text.
 */
public class InfoPanel extends JPanel {

  public enum Orientation {Horizontal, Vertical}


  private AniLabel lblIMG;
  private JLabel lblLINE1;
  private JLabel lblLINE2;
  private JLabel lblLINE3;
  protected JPanel pnlText;
  private Orientation orientation = Orientation.Horizontal;
  private int[] ID;

  public InfoPanel(AniLabel lbl, String line1, String line2, String line3, int[] id) {
    this(id);
    initComponents(lbl, line1, line2, line3);
    placeComponents();
  }

  protected InfoPanel(int[] id) {
    this.ID = id;
  }

  protected void initComponents(AniLabel lbl, String line1, String line2, String line3) {
    lblIMG = lbl;

    if (line1 != null) {
      lblLINE1 = new JLabel(line1);
      lblLINE1.setAlignmentY(InfoPanel.CENTER_ALIGNMENT);
      lblLINE1.setOpaque(false);
    }
    if (line2 != null) {
      lblLINE2 = new JLabel(line2);
      lblLINE2.setAlignmentY(InfoPanel.CENTER_ALIGNMENT);
      lblLINE2.setOpaque(false);
    }
    if (line3 != null) {
      lblLINE3 = new JLabel(line3);
      lblLINE3.setAlignmentY(InfoPanel.CENTER_ALIGNMENT);
      lblLINE3.setOpaque(false);
    }

    if (lblIMG != null) {
      lblIMG.setBackground(Color.BLACK);
      lblIMG.setOpaque(false);
      lblIMG.setAlignmentY(InfoPanel.CENTER_ALIGNMENT);
    }

    setupComponents();
    setOrientation(orientation);
  }

  public void setOrientation(InfoPanel.Orientation value) {
    if (orientation == value)
      return;

    orientation = value;
    float align = (orientation == Orientation.Horizontal)
      ? InfoPanel.LEFT_ALIGNMENT : InfoPanel.CENTER_ALIGNMENT;
    int textAlign = (orientation == Orientation.Horizontal)
      ? SwingConstants.LEFT : SwingConstants.CENTER;

    if (lblIMG != null)
      lblIMG.setAlignmentX(align);

    if (lblLINE1 != null) {
      lblLINE1.setAlignmentX(align);
      lblLINE1.setHorizontalTextPosition(textAlign);
    }
    if (lblLINE2 != null) {
      lblLINE2.setAlignmentX(align);
      lblLINE2.setHorizontalTextPosition(textAlign);
    }
    if (lblLINE3 != null) {
      lblLINE3.setAlignmentX(align);
      lblLINE3.setHorizontalTextPosition(textAlign);
    }

    // update horizontal / vertical placement
    placeComponents();
  }

  public void showAlert(AlertType type, String tooltip) {
    lblIMG.showAlert(type, tooltip);
  }

  private void setupComponents() {
    if (lblIMG != null)
      lblIMG.setOpaque(false);

    pnlText = new JPanel();
    pnlText.setOpaque(false);
    pnlText.setLayout(new BoxLayout(pnlText, BoxLayout.Y_AXIS));
    pnlText.add(Box.createVerticalGlue());

    if (lblLINE1 != null) {
      pnlText.add(lblLINE1);
      pnlText.add(Box.createVerticalGlue());
    }
    if (lblLINE2 != null) {
      pnlText.add(lblLINE2);
      pnlText.add(Box.createVerticalGlue());
    }
    if (lblLINE3 != null) {
      pnlText.add(lblLINE3);
      pnlText.add(Box.createVerticalGlue());
    }
  }

  protected void placeComponents() {
    removeAll();
    setLayout(new BorderLayout());
    add(createItemPanel());
  }

  protected JPanel createItemPanel() {
    JPanel pnl = new JPanel(new GridBagLayout());
    pnl.setOpaque(false);
    val c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(3, 5, 3, 5);
    c.gridx = 0;
    c.gridy = 0;
    c.weighty = 1;

    pnl.add(lblIMG, c);
    c.insets.top = 5;
    c.insets.bottom = 5;
    c.weightx = 1;
    c.weighty = 0;

    if (orientation == Orientation.Horizontal) {
      c.gridx++;
      pnl.add(pnlText, c);
    } else {
      c.gridy++;
      pnl.add(pnlText, c);
    }
    return pnl;
  }

  @Override
  public void setForeground(Color foreground) {
    super.setForeground(foreground);

    if (lblLINE1 != null)
      lblLINE1.setForeground(foreground);
    if (lblLINE2 != null)
      lblLINE2.setForeground(foreground);
    if (lblLINE3 != null)
      lblLINE3.setForeground(foreground);
  }

  public void setLine2Foreground(Color foreground) {
    if (lblLINE2 != null)
      lblLINE2.setForeground(foreground);
  }

  public String getLine1() {
    return lblLINE1 != null ? lblLINE1.getText() : null;
  }

  public String getLine2() {
    return lblLINE2 != null ? lblLINE2.getText() : null;
  }

  public String getLine3() {
    return lblLINE3 != null ? lblLINE3.getText() : null;
  }

  public int[] getID() {
    return ID;
  }

  public void setImageBackgroundOpaque(boolean opaque) {
    if (lblIMG != null)
      lblIMG.setOpaque(opaque);
  }

  @Override
  public synchronized void addKeyListener(KeyListener m) {
    super.addKeyListener(m);

    if (lblIMG != null)
      lblIMG.addKeyListener(m);
    if (lblLINE1 != null)
      lblLINE1.addKeyListener(m);
    if (lblLINE2 != null)
      lblLINE2.addKeyListener(m);
    if (lblLINE3 != null)
      lblLINE3.addKeyListener(m);

    pnlText.addKeyListener(m);
    lblIMG.addKeyListener(m);
  }

  @Override
  public void setFont(Font font) {
    Font oldFont = getFont();
    super.setFont(font);

    if (font != oldFont) {
      revalidate();
      repaint();
    }

    if (lblIMG != null)
      lblIMG.setFont(font);
    if (lblLINE1 != null)
      lblLINE1.setFont(font);
    if (lblLINE2 != null)
      lblLINE2.setFont(font);
    if (lblLINE3 != null)
      lblLINE3.setFont(font);
  }

}
