package ue.gui.common;

/**
 * The DefaultPanel base class is for panels that are not open file dependent,
 * and therefore can be kept open on file close.
 */
public abstract class DefaultPanel extends MainPanel {
}
