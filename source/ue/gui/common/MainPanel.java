package ue.gui.common;

import java.awt.Font;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JPanel;
import lombok.val;
import ue.gui.menu.MenuCommand;

/**
 * This is the abstract class MainPanel. All panels to be displayed in main window must be
 * subclasses of this class.
 */
public abstract class MainPanel extends JPanel {

  public abstract String menuCommand();

  /**
   * Dynamic title name lookup.
   * @return the translated panel display name
   */
  public String getTitleName() {
    // dynamically lookup the translated name
    return MenuCommand.mapCommandName(menuCommand());
  }

  public abstract boolean hasChanges();

  public FileChanges filesChanged() throws IOException {
    val changes = new FileChanges();
    listChangedFiles(changes);
    return changes;
  }

  /**
   * Reload the panel data and refresh view.
   *
   * This call shall not reload actual files changes!
   * To discard actual file changes, instead list changes
   * and call discard on the related archive.
   * @throws IOException
   */
  public abstract void reload() throws IOException;

  /**
   * Right before the panel is removed by the MainWindow, this method is called to let the panel do
   * any unfinished tasks it has before it becomes gc() fodder.
   */
  public abstract void finalWarning() throws Exception;

  /**
   * Returns instructions/help for this panel. Returns null if no help is available.
   */
  public String getHelpFileName() {
    return null;
  }

  public void createAndSetupLabels(JLabel[] lbl, Font font, String[] text, int horizontal_Align) {
    for (int i = 0; i < lbl.length; i++) {
      lbl[i] = new JLabel();
      lbl[i].setFont(font);
      lbl[i].setHorizontalAlignment(horizontal_Align);

      if (text != null && text.length > 0) {
        lbl[i].setText(text[i % text.length]);
      }
    }
  }

  protected abstract void listChangedFiles(FileChanges changes) throws IOException;
}