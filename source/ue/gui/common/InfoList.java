package ue.gui.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.Vector;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import lombok.Getter;
import lombok.val;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.util.event.SelectionEvent;
import ue.gui.util.event.SelectionListener;

public class InfoList extends JPanel {

  public static final int MOUSE_CLICK = 0;
  public static final int MOUSE_MOVEMENT = 1;

  @Getter
  private Icon defaultIcon;
  private Color SEL_COLOR;
  private boolean imgOpaque = false;
  private InfoPanel.Orientation orientation;

  private Vector<InfoPanel> DATA = new Vector<InfoPanel>();
  private Vector<ListSelectionListener> LISTENERS = new Vector<ListSelectionListener>();
  private Vector<SelectionListener> RIGHT_CLICK_LISTENERS = new Vector<SelectionListener>();
  private int selection_activator;
  private int SELECTED = -1;

  public InfoList(Icon def, boolean imgOpaque, int rows, int columns) {
    this.defaultIcon = def;
    this.imgOpaque = imgOpaque;

    GridLayout layout = new GridLayout(rows, columns, 0, 0);
    setLayout(layout);
    setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

    JList<String> list = new JList<String>();
    SEL_COLOR = list.getSelectionBackground();
    orientation = InfoPanel.Orientation.Horizontal;
  }

  public void setRowsAndColumnsNumber(int rows, int columns) {
    GridLayout layout = new GridLayout(rows, columns, 0, 0);
    setLayout(layout);
  }

  public void setSelectionBackground(Color background) {
    SEL_COLOR = background;
  }

  @Override
  public void setBackground(Color background) {
    super.setBackground(background);
    for (Component c : getComponents()) {
      c.setBackground(background);
    }
  }

  @Override
  public void setForeground(Color foreground) {
    super.setForeground(foreground);
    for (Component c : getComponents()) {
      c.setForeground(foreground);
    }
  }

  public void setItemLine2Foreground(int index, Color foreground) {
    DATA.get(index).setLine2Foreground(foreground);
  }

  public void addItem(AniLabel lbl, String line1, String line2, String line3) {
    addItem(lbl, line1, line2, line3, null);
  }

  public void addItem(AniLabel lbl, String line1, String line2, String line3, int[] id) {
    if (lbl == null) {
      // default icon must be instantiated, else it keeps blank
      lbl = new AniLabel(defaultIcon);
    }

    addItem(new InfoPanel(lbl, line1, line2, line3, id));
  }

  public void addItem(InfoPanel pnl) {
    pnl.setBackground(getBackground());
    pnl.setForeground(getForeground());
    pnl.setAlignmentX(getAlignmentX());
    pnl.addMouseListener(new SelectListener(DATA.size()));

    Dimension maxSize = pnl.getPreferredSize();
    maxSize.width = Short.MAX_VALUE;
    pnl.setMaximumSize(maxSize);
    pnl.setImageBackgroundOpaque(imgOpaque);
    pnl.setOrientation(orientation);
    pnl.setFont(getFont());
    DATA.add(pnl);

    add(pnl);
    invalidate();
  }

  // add custom item that is ignored by the list
  // wrap to not stretch the item
  public void addCustomItem(JComponent item, boolean wrap, int alignX, int alignY) {
    if (wrap) {
      JPanel pnl = new JPanel(new GridBagLayout());
      pnl.setBackground(Color.BLACK);
      val c = new GridBagConstraints();
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1;
      c.weighty = 1;
      pnl.add(Box.createGlue(), c);
      c.insets.top = alignY > 0 ? alignY : 0;
      c.insets.left = alignX > 0 ? alignX : 0;
      c.insets.bottom = alignY < 0 ? -alignY : 0;
      c.insets.right = alignX < 0 ? -alignX : 0;
      c.gridx++;
      c.gridy++;
      c.weightx = 0;
      c.weighty = 0;
      pnl.add(item, c);
      c.insets.top = 0;
      c.insets.left = 0;
      c.insets.bottom = 0;
      c.insets.right = 0;
      c.gridx++;
      c.gridy++;
      c.weightx = 1;
      c.weighty = 1;
      pnl.add(Box.createGlue(), c);
      add(pnl);
    } else {
      add(item);
    }

    invalidate();
  }

  public void removeAllItems() {
    removeAll();
    DATA.clear();

    if (SELECTED != -1) {
      SELECTED = -1;
      fireSelectionChanged();
    }

    invalidate();
  }

  public synchronized int getSelectedItemIndex() {
    return SELECTED;
  }

  public int[] getSelectedItemID() {
    if (SELECTED < 0)
      return null;

    InfoPanel pnl = DATA.get(SELECTED);
    return pnl.getID();
  }

  public String getSelectedItemLine1() {
    if (SELECTED < 0)
      return null;

    InfoPanel pnl = DATA.get(SELECTED);
    return pnl.getLine1();
  }

  public String getSelectedItemLine2() {
    if (SELECTED < 0)
      return null;

    InfoPanel pnl = DATA.get(SELECTED);
    return pnl.getLine2();
  }

  public String getSelectedItemLine3() {
    if (SELECTED < 0)
      return null;

    InfoPanel pnl = DATA.get(SELECTED);
    return pnl.getLine3();
  }

  public synchronized void setSelectedItemIndex(int index) {
    if (SELECTED == index) {
      if (SELECTED >= 0) {
        InfoPanel pnl = DATA.get(SELECTED);
        pnl.setBackground(getBackground());
      }

      SELECTED = -1;

      setVisible(false);
      setVisible(true);
      fireSelectionChanged();
    } else {
      if (SELECTED >= 0) {
        InfoPanel pnl = DATA.get(SELECTED);
        pnl.setBackground(getBackground());
      }

      SELECTED = -1;

      if (index < DATA.size() && index >= 0) {
        SELECTED = index;
        InfoPanel pnl = DATA.get(SELECTED);
        pnl.setBackground(SEL_COLOR);

        setVisible(false);
        setVisible(true);
      } else {
        setVisible(false);
        setVisible(true);
      }

      fireSelectionChanged();
    }
  }

  public synchronized void clearSelection() {
    if (SELECTED >= 0) {
      InfoPanel pnl = DATA.get(SELECTED);
      pnl.setBackground(getBackground());
    }

    SELECTED = -1;

    setVisible(false);
    setVisible(true);

    fireSelectionChanged();
  }

  /**
   * @param listener
   */
  public void addListSelectionListener(ListSelectionListener listener) {
    LISTENERS.add(listener);
  }

  public void addRightClickListener(SelectionListener listener) {
    RIGHT_CLICK_LISTENERS.add(listener);
  }

  public void setSelectionActivator(int activator) {
    selection_activator = activator;
  }

  protected void fireSelectionChanged() {
    for (int i = 0; i < LISTENERS.size(); i++) {
      LISTENERS.get(i).valueChanged(new ListSelectionEvent(this, SELECTED, SELECTED, true));
    }
  }

  protected void fireRightClick(SelectionEvent evt) {
    for (int i = 0; i < RIGHT_CLICK_LISTENERS.size(); i++) {
      RIGHT_CLICK_LISTENERS.get(i).selected(evt);
    }
  }

  public void setTooltip(int index, String tip) {
    if (index < 0 || index >= DATA.size())
      return;

    DATA.get(index).setToolTipText(tip);
  }

  public void setSelectedItemID(int[] id) {
    for (int i = 0; i < DATA.size(); i++) {
      if (Arrays.equals(DATA.get(i).getID(), id)) {
        setSelectedItemIndex(i);
        fireSelectionChanged();
        return;
      }
    }
    fireSelectionChanged();
  }

  private class SelectListener implements MouseListener {

    private int INDEX;

    public SelectListener(int index) {
      INDEX = index;
    }

    @Override
    public void mouseClicked(MouseEvent evt) {
      // do not care for the right click event,
      // it doesn't fire all the time, but only on certain clicks
      // this can be time or layout dependent or whatsoever
    }

    @Override
    public void mouseEntered(MouseEvent evt) {
      if (SELECTED != INDEX) {
        if ((evt.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) != 0
            || selection_activator == MOUSE_MOVEMENT) {
          setSelectedItemIndex(INDEX);
        }
      }
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
      if (selection_activator == MOUSE_MOVEMENT) {
        if (SELECTED == INDEX) {
          setSelectedItemIndex(-1);
        }
      }
    }

    @Override
    public void mousePressed(MouseEvent evt) {
      if (selection_activator == MOUSE_CLICK) {
        if ((evt.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) != 0) {
          setSelectedItemIndex(INDEX);
        }
      }
    }

    @Override
    public void mouseReleased(MouseEvent evt) {
      if (evt.getButton() == MouseEvent.BUTTON3) {
        if (selection_activator == MOUSE_CLICK) {
          if (SELECTED != INDEX)
            setSelectedItemIndex(INDEX);
        }

        InfoPanel pnl = DATA.get(INDEX);
        fireRightClick(new SelectionEvent(pnl.getID(), evt.getLocationOnScreen()));
      }
    }
  }

  /**
   * @return number of items in this list
   */
  public int getNumberOfItems() {
    return DATA.size();
  }

  /**
   * @param value either horizontal or vertival
   */
  public void setItemOrientation(InfoPanel.Orientation value) {
    orientation = value;
    for (InfoPanel pnl : DATA)
      pnl.setOrientation(value);
  }

  @Override
  public void setFont(Font font) {
    Font oldFont = getFont();
    super.setFont(font);

    if (font != oldFont) {
      revalidate();
      repaint();
    }

    if (DATA != null) {
      for (InfoPanel pnl : DATA)
        pnl.setFont(font);
    }
  }

}
