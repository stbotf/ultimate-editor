package ue.gui.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import ue.UE;
import ue.edit.common.FileInfo;
import ue.edit.common.FileLookup;
import ue.edit.common.IDataFile.PatchStatus;
import ue.edit.common.InternalFile;
import ue.gui.common.MainPanel;
import ue.gui.util.GuiTools;
import ue.gui.util.component.ScrollablePanel;
import ue.gui.util.dialog.SearchBox;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.util.data.HexTools;
import ue.util.stream.chunk.ChunkWriterDummy;

public abstract class ChangesBaseGUI extends MainPanel {

  protected static final Color COLOR_Default      = Color.WHITE;
  // loaded segment status
  protected static final Color COLOR_Orig         = Color.LIGHT_GRAY;
  protected static final Color COLOR_Orig_HL      = new Color(230, 230, 230);
  protected static final Color COLOR_Patched      = Color.GREEN;
  protected static final Color COLOR_Patched_HL   = new Color(200, 255, 200);
  protected static final Color COLOR_Changed      = Color.CYAN;
  protected static final Color COLOR_Changed_HL   = new Color(200, 255, 255);
  protected static final Color COLOR_Sneaked      = new Color(255, 8, 92);
  protected static final Color COLOR_Sneaked_HL   = new Color(255, 207, 227);
  protected static final Color COLOR_Invalid      = Color.RED;
  protected static final Color COLOR_Invalid_HL   = new Color(255, 215, 215);
  protected static final Color COLOR_Loaded       = Color.LIGHT_GRAY;
  protected static final Color COLOR_Loaded_HL    = new Color(240, 230, 230);
  protected static final Color COLOR_Unloaded     = Color.WHITE;
  protected static final Color COLOR_Unloaded_HL  = new Color(230, 242, 253);
  // not yet saved segment changes
  protected static final Color COLOR_Patch        = Color.YELLOW;
  protected static final Color COLOR_Patch_HL     = new Color(255, 240, 176);
  protected static final Color COLOR_Unpatch      = new Color(100, 100, 100);
  protected static final Color COLOR_Unpatch_HL   = new Color(210, 210, 210);
  protected static final Color COLOR_Change       = Color.MAGENTA;
  protected static final Color COLOR_Change_HL    = new Color(255, 220, 255);
  protected static final Color COLOR_Modify       = Color.ORANGE;
  protected static final Color COLOR_Modify_HL    = new Color(255, 230, 204);
  protected static final Color COLOR_Reset        = new Color(177, 73, 255);
  protected static final Color COLOR_Reset_HL     = new Color(231, 199, 255);

  protected static final String HexFontName                 = "Courier";
  protected static final Color COLOR_Diff               = new Color(255, 176, 176);
  protected static final SimpleAttributeSet AttrHexDiff = new SimpleAttributeSet();
  protected static final SimpleAttributeSet AttrHexMark = new SimpleAttributeSet();

  // loaded status
  protected final String TOOLTIP_Orig        = Language.getString("ChangesBaseGUI.0"); //$NON-NLS-1$
  protected final String TOOLTIP_Patched     = Language.getString("ChangesBaseGUI.1"); //$NON-NLS-1$
  protected final String TOOLTIP_Changed     = Language.getString("ChangesBaseGUI.2"); //$NON-NLS-1$
  protected final String TOOLTIP_Sneaked     = Language.getString("ChangesBaseGUI.3"); //$NON-NLS-1$
  protected final String TOOLTIP_Invalid     = Language.getString("ChangesBaseGUI.4"); //$NON-NLS-1$
  // not yet saved changes
  protected final String TOOLTIP_Patch       = Language.getString("ChangesBaseGUI.5"); //$NON-NLS-1$
  protected final String TOOLTIP_Unpatch     = Language.getString("ChangesBaseGUI.6"); //$NON-NLS-1$
  protected final String TOOLTIP_Change      = Language.getString("ChangesBaseGUI.7"); //$NON-NLS-1$
  protected final String TOOLTIP_Modify      = Language.getString("ChangesBaseGUI.8"); //$NON-NLS-1$
  protected final String TOOLTIP_Reset       = Language.getString("ChangesBaseGUI.9"); //$NON-NLS-1$
  // segment description
  protected final String DESC_Title          = Language.getString("ChangesBaseGUI.10"); //$NON-NLS-1$
  protected final String DESC_None           = Language.getString("ChangesBaseGUI.11"); //$NON-NLS-1$
  protected final String DESC_Changed        = Language.getString("ChangesBaseGUI.12"); //$NON-NLS-1$
  protected final String DESC_Loaded         = Language.getString("ChangesBaseGUI.13"); //$NON-NLS-1$
  protected final String DESC_Default        = Language.getString("ChangesBaseGUI.14"); //$NON-NLS-1$
  protected final String DESC_Error          = Language.getString("ChangesBaseGUI.15"); //$NON-NLS-1$
  protected final String DESC_Selected       = Language.getString("ChangesBaseGUI.16"); //$NON-NLS-1$

  protected static final int BytesPerLine = 16;

  @Getter @Setter
  private boolean compareHex = true;

  protected class Selection {
    public int rowIdx = -1;
    public FileInfo info = null;
    public InternalFile file = null;
  }

  // ui components
  protected JTable tblFILES = new JTable();
  protected ScrollablePanel descPanel = new ScrollablePanel(new GridBagLayout());
  protected JPanel diffPanel = new JPanel(new GridBagLayout());
  protected JTextPane txtDesc = new JTextPane();
  protected JLabel lblDiffOrig = new JLabel();
  protected JLabel lblDiffChange = new JLabel(DESC_Changed);
  protected JTextPane txtDiffLines = new JTextPane();
  protected JTextPane txtDiffOrig = new JTextPane();
  protected JTextPane txtDiffChange = new JTextPane();
  protected JScrollPane tblScroll = new JScrollPane(tblFILES);
  protected SearchBox searchBox = new SearchBox(tblFILES, 12);

  // data
  private int nameColumn = -1;
  private int statusColumn = -1;
  protected Selection selection = null;

  protected FileLookup fileLookup = null;

  public ChangesBaseGUI(FileLookup lookup) {
    fileLookup = lookup;
  }

  protected String descSelected() {
    return DESC_Selected;
  }

  protected int numEntries() {
    return tblFILES.getModel().getRowCount();
  }

  protected abstract String[] entryNames();
  protected abstract InternalFile entryFile(FileInfo info);
  protected abstract String listInfo();
  protected abstract String entryDescription(Selection selection);

  protected void init(int nameColumn, int statusColumn) throws IOException {
    this.nameColumn = nameColumn;
    this.statusColumn = statusColumn;

    setupComponents();
    placeComponents();
    addListeners();
    loadValues();
  }

  protected void setupComponents() {
    // list
    tblFILES.setAutoCreateRowSorter(true);
    tblFILES.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    tblFILES.setShowGrid(false);
    tblFILES.setIntercellSpacing(new Dimension(0, 0));
    tblFILES.setRowSelectionAllowed(true);
    tblFILES.setColumnSelectionAllowed(false);
    tblFILES.setUpdateSelectionOnSort(true);
    tblScroll.setPreferredSize(new Dimension(400, 250));

    // description
    descPanel.setBackground(Color.WHITE);
    diffPanel.setBackground(Color.WHITE);
    txtDesc.setContentType("text/html");
    txtDesc.setEditable(false);
    txtDiffLines.setBackground(Color.LIGHT_GRAY);
    txtDiffLines.setEditable(false);
    txtDiffChange.setEditable(false);
    txtDiffOrig.setEditable(false);
    StyleConstants.setBackground(AttrHexMark, COLOR_Diff);

    // StyledDocument doc = txtDiffLines.getStyledDocument();
    // SimpleAttributeSet alignRight = new SimpleAttributeSet();
    // StyleConstants.setAlignment(alignRight, StyleConstants.ALIGN_RIGHT);
    // doc.setParagraphAttributes(0, Integer.MAX_VALUE, alignRight, true);
    Style style = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
    Style body = txtDiffLines.getStyledDocument().addStyle("body", style);
    StyleConstants.setAlignment(body, StyleConstants.ALIGN_RIGHT);
    txtDiffLines.setLogicalStyle(body);

    // font
    Font def = UE.SETTINGS.getDefaultFont();
    Font defBold = def.deriveFont(Font.BOLD);
    tblFILES.setFont(def);
    tblFILES.setRowHeight(UE.WINDOW.getGraphics().getFontMetrics(def).getHeight());
    txtDesc.setFont(def);
    searchBox.setFont(def);
    lblDiffOrig.setFont(defBold);
    lblDiffChange.setFont(defBold);

    Font hexFont = new Font(HexFontName, Font.PLAIN, def.getSize());
    txtDiffLines.setFont(hexFont);
    txtDiffChange.setFont(hexFont);
    txtDiffOrig.setFont(hexFont);

    // main panel
    setLayout(new BorderLayout(0, 0));
    setBorder(BorderFactory.createEmptyBorder(10, 10, 8, 10));
  }

  protected void placeComponents() {
    layoutDiffPanel();

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    c.gridwidth = 2;
    descPanel.add(txtDesc, c);
    c.insets.top = 5;
    c.gridy++;
    c.weightx = 0;
    c.gridwidth = 1;
    descPanel.add(diffPanel, c);
    c.insets.top = 0;
    c.gridy++;
    c.weighty = 1;
    descPanel.add(Box.createVerticalGlue(), c);
  }

  private void layoutDiffPanel() {
    GridBagConstraints c = new GridBagConstraints();
    c.anchor = GridBagConstraints.NORTHWEST;
    c.insets.left = 10;
    c.gridx = 1;
    c.gridy = 0;
    diffPanel.add(lblDiffChange, c);
    c.gridx++;
    diffPanel.add(lblDiffOrig, c);
    c.insets.left = 0;
    c.gridx = 0;
    c.gridy++;
    diffPanel.add(txtDiffLines, c);
    c.insets.left = 5;
    c.gridx++;
    diffPanel.add(txtDiffChange, c);
    c.insets.right = 5;
    c.gridx++;
    diffPanel.add(txtDiffOrig, c);
  }

  protected void addListeners() {
    tblFILES.getSelectionModel().addListSelectionListener(
      new CBListSelectionListener(x -> onListSelectionChanged(x)));

    addKeyListener(searchBox.SearchKeyListener);
    txtDesc.addKeyListener(searchBox.SearchKeyListener);
  }

  protected abstract void loadValues() throws IOException;

  protected void updateListRenderer() throws IOException {
    tblFILES.setDefaultRenderer(tblFILES.getModel().getColumnClass(nameColumn), new CellRenderer());
  }

  protected TableRowSorter<TableModel> updateRowSorter() throws IOException {
    // set custom row sorter to sort the value column
    val rowSorter = new TableRowSorter<>(tblFILES.getModel());
    tblFILES.setRowSorter(rowSorter);
    // sort table by name
    rowSorter.toggleSortOrder(0);
    return rowSorter;
  }

  public void updateDescription() {
    StringBuffer msg = new StringBuffer();
    boolean diffLoaded = false;

    String listInfo = listInfo();
    if (listInfo != null)
      msg.append(listInfo);

    if (selection != null) {
      String desc = entryDescription(selection);
      if (desc != null) {
        if (msg.length() > 0)
          msg.append("<p>"); //$NON-NLS-1$

        msg.append("<b>"); //$NON-NLS-1$
        msg.append(DESC_Title);
        msg.append("</b><br>"); //$NON-NLS-1$
        msg.append(desc);
      }

      val fileErrors = getFileErrors(selection.info);
      if (fileErrors != null && !fileErrors.isEmpty()) {
        if (msg.length() > 0)
          msg.append("<p>"); //$NON-NLS-1$

        msg.append("<b>"); //$NON-NLS-1$
        msg.append(DESC_Error);
        msg.append("</b>"); //$NON-NLS-1$
        for (String err : fileErrors) {
          msg.append("<br>");
          msg.append(err);
        }
      }

      try {
        diffLoaded = loadDescData(selection);
      } catch (Exception e) {
        e.printStackTrace();

        if (msg.length() > 0)
          msg.append("<p>"); //$NON-NLS-1$

        msg.append("<b>"); //$NON-NLS-1$
        msg.append(DESC_Error);
        msg.append("</b><br>"); //$NON-NLS-1$
        msg.append(e.toString());
      }
    } else {
      int selCount = tblFILES.getSelectedRowCount();
      msg.append(descSelected().replace("%1", Integer.toString(selCount)));
    }

    // add header last to skip adding spacings when there is no info text
    Font def = UE.SETTINGS.getDefaultFont();
    String fontSize = Integer.toString(def.getSize());
    msg.insert(0, "<html><head><style>"
      + "p{"
      + " margin: 5 0 0 0;}"
      + "</style></head>"
      + "<div style='font-size:" + fontSize + "'>");
    msg.append("</div></html>");

    txtDesc.setText(msg.toString());

    // prevent diff panel word wrap
    if (diffLoaded) {
      // force recalculate text pane sizes for unlimited width
      txtDiffChange.setSize(0, 0);
      txtDiffOrig.setSize(0, 0);
      txtDiffLines.setSize(0, 0);
      // set minimum diff panel extends to limit the descPanel auto resize
      diffPanel.setMinimumSize(diffLoaded ? diffPanel.getPreferredSize() : null);
    }
    diffPanel.setVisible(diffLoaded);
  }

  protected Set<String> getSelectedFiles() {
    final TableModel model = tblFILES.getModel();
    final int[] sel = tblFILES.getSelectedRows();
    return Arrays.stream(sel).map(x -> tblFILES.convertRowIndexToModel(x))
      .mapToObj(x -> model.getValueAt(x, 0).toString()).collect(Collectors.toSet());
  }

  protected void selectRow(int sel) {
    int rows = tblFILES.getRowCount();
    if (rows > 0) {
      sel = Integer.min(sel, rows - 1);
      tblFILES.addRowSelectionInterval(sel, sel);
    }
  }

  protected void selectFiles(Set<String> files) {
    if (files.isEmpty())
      return;

    String[] names = entryNames();
    int first = -1;

    for (int i = 0; i < names.length; ++i) {
      if (files.contains(names[i])) {
        int idx = tblFILES.convertRowIndexToView(i);
        if (first < 0)
          first = idx;

        tblFILES.addRowSelectionInterval(idx, idx);
      }
    }

    if (first >= 0)
      GuiTools.scrollToTableRow(tblFILES, first);
  }

  private boolean loadDescData(Selection sel) throws IOException {
    if(!compareHex || sel == null || sel.info == null || sel.file == null)
      return false;

    byte[] orig = null;
    byte[] changed = null;
    boolean loadedDataChange = false;

    switch (sel.info.getStatus()) {
      case Changed:
      case Patched:
        // the file is left unchanged
        // compare loaded data with defaults, which
        // should match the unmodified 'vanilla' code
        orig = loadDefault(sel.file);
        // load raw file data for comparison
        changed = loadUnchanged(sel.file).orElse(null);
        loadedDataChange = true;
        break;
      case Change:
      case Patch:
      case Modify:
      case Reset:
      case Unpatch:
      case Sneaked:
        // the file has been modified
        // compare changes with raw file data
        orig = loadUnchanged(sel.file).orElse(null);
        // load current file data
        changed = loadChanged(sel.file);
        break;
      case Orig:
      case Loaded:
        // no changes detected
        // load raw file data only
        orig = loadUnchanged(sel.file).orElse(null);
        break;
      default:
        // invalid and unloaded need no data comparison
        return false;
    }

    // compare diff
    int origLen = orig != null ? orig.length : 0;
    int changedLen = changed != null ? changed.length : 0;
    int numLines = (Integer.max(origLen, changedLen) + BytesPerLine - 1) / BytesPerLine;

    if (numLines <= 0)
      return false;

    // number the lines
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < numLines; ++i) {
      if (i > 0)
        sb.append("\n"); //$NON-NLS-1$
      sb.append(Integer.toHexString(i * BytesPerLine).toUpperCase() + ":");
    }
    txtDiffLines.setText(sb.toString());

    // clean string builder
    sb.setLength(0);

    val diff = HexTools.diff(orig, changed, BytesPerLine,
      " ", "\n", "", ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

    // changed
    String strChanged = diff.getChanged();
    txtDiffChange.setText(strChanged);
    txtDiffChange.setVisible(strChanged != null);
    lblDiffChange.setVisible(strChanged != null);
    if (strChanged != null) {
      StyledDocument doc = txtDiffChange.getStyledDocument();
      doc.setCharacterAttributes(0, Integer.MAX_VALUE, AttrHexDiff, true);

      for (val mark : diff.getMarkings()) {
        int start = mark.lowerEndpoint();
        int end = mark.upperEndpoint();
        doc.setCharacterAttributes(start, end - start, AttrHexMark, false);
      }
    }

    // original
    String strOrig = diff.getOrig();
    txtDiffOrig.setText(strOrig);
    txtDiffOrig.setVisible(strOrig != null);
    lblDiffOrig.setText(loadedDataChange ? DESC_Default : DESC_Loaded);
    lblDiffOrig.setVisible(strOrig != null);
    if (strOrig != null) {
      StyledDocument doc = txtDiffOrig.getStyledDocument();
      doc.setCharacterAttributes(0, Integer.MAX_VALUE, AttrHexDiff, true);

      for (val mark : diff.getMarkings()) {
        int start = mark.lowerEndpoint();
        int end = mark.upperEndpoint();
        doc.setCharacterAttributes(start, end - start, AttrHexMark, false);
      }
    }

    return true;
  }

  protected byte[] loadChanged(InternalFile file) throws IOException {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    // if chunked, use a dummy stream to not write any chunk headers
    // this is needed for binary comparison of the result.lst file
    file.save(file.isChunked() ? new ChunkWriterDummy(out) : out);
    return out.toByteArray();
  }

  protected abstract Optional<byte[]> loadUnchanged(InternalFile file) throws IOException;
  protected abstract byte[] loadDefault(InternalFile file) throws IOException;

  protected void onListSelectionChanged(ListSelectionEvent x) {
    if (x.getValueIsAdjusting())
      return;

    int selCount = tblFILES.getSelectedRowCount();
    if (selCount == 1) {
      val model = (FileListModel) tblFILES.getModel();
      int rowIdx = tblFILES.convertRowIndexToModel(tblFILES.getSelectedRow());
      val info = model.getInfo(rowIdx);

      selection = new Selection();
      selection.rowIdx = rowIdx;
      selection.info = info;
      selection.file = entryFile(info);
    } else {
      selection = null;
    }

    updateDescription();
  }

  private ArrayList<String> getFileErrors(FileInfo info) {
    ArrayList<String> issues = null;
    String loadError = selection.info.getError();

    if (loadError != null) {
      issues = new ArrayList<String>();
      issues.add(loadError);
    } else {
      val file = fileLookup.getCachedFile(selection.info.getName());
      if (file != null) {
        val errs = file.listErrors();
        val wrns = file.listWarnings();
        val infs = file.listNotes();
        if (!errs.isEmpty() || !wrns.isEmpty() || !infs.isEmpty()) {
          issues = new ArrayList<String>();
          issues.addAll(errs);
          issues.addAll(wrns);
          issues.addAll(infs);
        }
      }
    }

    return issues;
  }

  @Override
  public synchronized void addKeyListener(KeyListener l) {
    super.addKeyListener(l);
    descPanel.addKeyListener(l);
    diffPanel.addKeyListener(l);
    txtDesc.addKeyListener(l);

    // diff panel components
    lblDiffOrig.addKeyListener(l);
    lblDiffChange.addKeyListener(l);
    txtDiffLines.addKeyListener(l);
    txtDiffOrig.addKeyListener(l);
    txtDiffChange.addKeyListener(l);
  }

  private class CellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {

      // get default JLabel render component
      // plus disable cell selection focus
      JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, false, row, column);

      // color status column
      if (column == statusColumn && value != null) {
        // alternatively to the value, call:
        // ((TableData)table.getModel()).getStatus(table.convertRowIndexToModel(row));
        Color col;
        PatchStatus status = PatchStatus.valueOf((String)value);
        switch (status) {
          // loaded states
          case Orig:
            col = isSelected ? COLOR_Orig_HL : COLOR_Orig;
            label.setToolTipText(TOOLTIP_Orig);
            break;
          case Patched:
            col = isSelected ? COLOR_Patched_HL : COLOR_Patched;
            label.setToolTipText(TOOLTIP_Patched);
            break;
          case Changed:
            col = isSelected ? COLOR_Changed_HL : COLOR_Changed;
            label.setToolTipText(TOOLTIP_Changed);
            break;
          case Sneaked:
            col = isSelected ? COLOR_Sneaked_HL : COLOR_Sneaked;
            label.setToolTipText(TOOLTIP_Sneaked);
            break;
          case Invalid:
            col = isSelected ? COLOR_Invalid_HL : COLOR_Invalid;
            label.setToolTipText(TOOLTIP_Invalid);
            break;
          case Loaded:
            col = isSelected ? COLOR_Loaded_HL : COLOR_Loaded;
            label.setToolTipText(null);
            break;
          case Unloaded:
            col = isSelected ? COLOR_Unloaded_HL : COLOR_Unloaded;
            label.setToolTipText(null);
            break;
            // unsaved changes
          case Patch:
            col = isSelected ? COLOR_Patch_HL : COLOR_Patch;
            label.setToolTipText(TOOLTIP_Patch);
            break;
          case Unpatch:
            col = isSelected ? COLOR_Unpatch_HL : COLOR_Unpatch;
            label.setToolTipText(TOOLTIP_Unpatch);
            break;
          case Change:
            col = isSelected ? COLOR_Change_HL : COLOR_Change;
            label.setToolTipText(TOOLTIP_Change);
            break;
          case Modify:
            col = isSelected ? COLOR_Modify_HL : COLOR_Modify;
            label.setToolTipText(TOOLTIP_Modify);
            break;
          case Reset:
            col = isSelected ? COLOR_Reset_HL : COLOR_Reset;
            label.setToolTipText(TOOLTIP_Reset);
            break;
          default:
            col = COLOR_Default;
            label.setToolTipText(null);
            break;
        }

        label.setBackground(col);
      } else if (isSelected) {
        Color col = table.getSelectionBackground();
        label.setBackground(col);
        label.setToolTipText(null);
      } else {
        // unset cell color
        label.setBackground(COLOR_Default);
        label.setToolTipText(null);
      }

      return label;
    }
  }
}
