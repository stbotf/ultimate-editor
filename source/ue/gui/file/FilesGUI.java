package ue.gui.file;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import lombok.val;
import ue.UE;
import ue.edit.FilesInterface;
import ue.edit.common.FileInfo;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.fnt.TgaFont;
import ue.edit.res.stbof.files.gif.GifImage;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.gui.util.component.IconButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.gfx.Icons;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.file.PathHelper;

public class FilesGUI extends ChangesBaseGUI {

  private static final int DIVIDER_LOCATION = 300;

  private static final int NAME_COLUMN = 0;
  private static final int TYPE_COLUMN = 1;
  private static final int CLASS_COLUMN = 2;
  private static final int SIZE_COLUMN = 3;
  private static final int STATUS_COLUMN = 4;

  // button tooltips
  private final String TOOLTIP_LoadAll = Language.getString("FilesGUI.24"); //$NON-NLS-1$
  private final String TOOLTIP_DiscardAll = Language.getString("FilesGUI.26"); //$NON-NLS-1$
  private final String TOOLTIP_RawExport = Language.getString("FilesGUI.20"); //$NON-NLS-1$
  private final String TOOLTIP_Sneaks = Language.getString("FilesGUI.28"); //$NON-NLS-1$
  private final String TOOLTIP_HexCmp = Language.getString("FilesGUI.30"); //$NON-NLS-1$

  // edited
  private FilesInterface INTERFACE;

  // ui components
  private JButton btn_Add = new JButton(Language.getString("FilesGUI.0")); //$NON-NLS-1$
  private JButton btn_Remove = new JButton(Language.getString("FilesGUI.2")); //$NON-NLS-1$
  private JButton btn_Extract = new JButton(Language.getString("FilesGUI.4")); //$NON-NLS-1$
  private IconButton btn_LoadAll = new IconButton("", Icons.LOAD_ALL);
  private IconButton btn_Load = new IconButton(Language.getString("FilesGUI.23"), Icons.LOAD); //$NON-NLS-1$
  private IconButton btn_DiscardAll = new IconButton("", Icons.DISCARD_ALL);
  private IconButton btn_Discard = new IconButton(Language.getString("FilesGUI.25"), Icons.DISCARD); //$NON-NLS-1$
  private JLabel lblPREVIEW = new JLabel();
  private JCheckBox chkPREVIEW = new JCheckBox(Language.getString("FilesGUI.9"), true); //$NON-NLS-1$
  private JCheckBox chkRawExport = new JCheckBox(Language.getString("FilesGUI.19")); //$NON-NLS-1$
  private JCheckBox chkSneaks = new JCheckBox(Language.getString("FilesGUI.27")); //$NON-NLS-1$
  private JCheckBox chkHexCmp = new JCheckBox(Language.getString("FilesGUI.29")); //$NON-NLS-1$

  // data
  private HashSet<String> addedFiles = new HashSet<String>();
  private HashSet<String> removedFiles = new HashSet<String>();
  private Map<String, FileInfo> fileInfos;
  private PreviewLoader LOADER;
  private String statusText;

  public FilesGUI(FilesInterface f) throws IOException {
    super(f.archive());
    INTERFACE = f;
    init(NAME_COLUMN, STATUS_COLUMN);
  }

  @Override
  protected String[] entryNames() {
    TableData model = (TableData) tblFILES.getModel();
    return model.entryNames();
  }

  @Override
  protected InternalFile entryFile(FileInfo info) {
    return INTERFACE.getCachedFile(info.getName());
  }

  @Override
  protected String listInfo() {
    return statusText;
  }

  @Override
  protected String entryDescription(Selection sel) {
    return INTERFACE.archive().getFileDescription(sel.info.getName());
  }

  @Override
  protected void setupComponents() {
    super.setupComponents();

    // buttons
    btn_Remove.setEnabled(false);
    btn_Extract.setEnabled(false);
    btn_LoadAll.setToolTipText(TOOLTIP_LoadAll);
    btn_LoadAll.setMargin(new Insets(2, 2, 2, 2));
    btn_DiscardAll.setToolTipText(TOOLTIP_DiscardAll);
    btn_DiscardAll.setMargin(new Insets(2, 2, 2, 2));

    // preview
    lblPREVIEW.setHorizontalAlignment(JLabel.CENTER);
    lblPREVIEW.setVerticalAlignment(JLabel.CENTER);
    lblPREVIEW.setBackground(Color.WHITE);
    lblPREVIEW.setVerticalTextPosition(JLabel.CENTER);
    lblPREVIEW.setHorizontalTextPosition(JLabel.CENTER);
    lblPREVIEW.setBorder(BorderFactory.createEtchedBorder());

    // adjust preview width
    int width = btn_Extract.getPreferredSize().width;
    width = Integer.max(width, btn_Add.getPreferredSize().width);
    width = Integer.max(width, btn_Remove.getPreferredSize().width);
    width = Integer.max(width, btn_Load.getPreferredSize().width + btn_LoadAll.getPreferredSize().width);
    width = Integer.max(width, btn_Discard.getPreferredSize().width + btn_DiscardAll.getPreferredSize().width);
    lblPREVIEW.setPreferredSize(new Dimension(width, width));
    lblPREVIEW.setHorizontalAlignment(SwingConstants.CENTER);
    lblPREVIEW.setVerticalAlignment(SwingConstants.CENTER);

    // checkboxes
    chkRawExport.setToolTipText(TOOLTIP_RawExport);
    chkSneaks.setToolTipText(TOOLTIP_Sneaks);
    chkHexCmp.setToolTipText(TOOLTIP_HexCmp);
    chkPREVIEW.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.FILE_SHOW_PREVIEW));
    chkRawExport.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.FILE_RAW_EXPORT));
    chkSneaks.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.FILE_SNEAK_CHECKING));
    boolean hexComp = UE.SETTINGS.getBooleanProperty(SettingsManager.FILE_HEX_COMPARE);
    chkHexCmp.setSelected(hexComp);
    super.setCompareHex(hexComp);

    //font
    Font def = UE.SETTINGS.getDefaultFont();
    btn_Add.setFont(def);
    btn_Remove.setFont(def);
    btn_Load.setFont(def);
    btn_LoadAll.setFont(def);
    btn_Discard.setFont(def);
    btn_DiscardAll.setFont(def);
    btn_Extract.setFont(def);
    lblPREVIEW.setFont(def);
    chkPREVIEW.setFont(def);
    chkRawExport.setFont(def);
    chkSneaks.setFont(def);
    chkHexCmp.setFont(def);
  }

  @Override
  protected void placeComponents() {
    super.placeComponents();

    // file info & status
    JSplitPane splitPane;
    {
      // description
      JScrollPane scrDesc = new JScrollPane(descPanel);
      // set preferred size to not auto-calculate a minimum size on initial window load
      scrDesc.setPreferredSize(new Dimension(0, 0));
      // split pane
      splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, tblScroll, scrDesc);
      splitPane.setDividerLocation(DIVIDER_LOCATION);
      splitPane.setOneTouchExpandable(true);
    }

    JPanel pnlLoad = new JPanel();
    {
      pnlLoad.setLayout(new BorderLayout());
      pnlLoad.add(btn_LoadAll, BorderLayout.LINE_START);
      pnlLoad.add(btn_Load, BorderLayout.CENTER);
    }

    JPanel pnlDiscard = new JPanel();
    {
      pnlDiscard.setLayout(new BorderLayout());
      pnlDiscard.add(btn_DiscardAll, BorderLayout.LINE_START);
      pnlDiscard.add(btn_Discard, BorderLayout.CENTER);
    }

    // buttons
    JPanel pnlOptions = new JPanel();
    {
      GridBagConstraints c = new GridBagConstraints();
      c.anchor = GridBagConstraints.NORTH;
      c.fill = GridBagConstraints.HORIZONTAL;
      pnlOptions.setLayout(new GridBagLayout());
      c.insets = new Insets(0, 0, 5, 0);
      c.gridx = 0;
      c.gridy = 0;
      pnlOptions.add(btn_Add, c);
      c.insets.top = 5;
      c.gridy++;
      pnlOptions.add(btn_Remove, c);
      c.gridy++;
      pnlOptions.add(btn_Extract, c);
      c.insets.bottom = 0;
      c.gridy++;
      pnlOptions.add(pnlLoad, c);
      c.gridy++;
      pnlOptions.add(pnlDiscard, c);
      c.gridy++;
      pnlOptions.add(chkPREVIEW, c);
      c.insets.top = 0;
      c.gridy++;
      pnlOptions.add(chkRawExport, c);
      c.gridy++;
      pnlOptions.add(chkSneaks, c);
      c.gridy++;
      pnlOptions.add(chkHexCmp, c);
      c.insets.top = 5;
      c.gridy++;
      pnlOptions.add(lblPREVIEW, c);
    }

    // main panel
    setLayout(new BorderLayout(0, 0));
    setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    add(splitPane, BorderLayout.CENTER);
    add(pnlOptions, BorderLayout.EAST);
  }

  @Override
  protected void addListeners() {
    super.addListeners();

    //buttons
    btn_Add.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        fc.setDialogTitle(
            UE.APP_NAME + " - " + Language.getString("FilesGUI.1")); //$NON-NLS-1$ //$NON-NLS-2$
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(true);
        int returnVal = fc.showDialog(UE.WINDOW, Language.getString("FilesGUI.0")); //$NON-NLS-1$

        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File[] fil = fc.getSelectedFiles();
        if (fil.length > 0) {
          Set<String> added = Arrays.stream(fil).map(x -> x.getName()).collect(Collectors.toSet());

          try {
            // first remove extising files from cache to clear the status
            for (String fileName : added) {
              INTERFACE.archive().discard(fileName);
            }

            String workPath = PathHelper.toFolderPath(fil[0]).toString();
            UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);
            INTERFACE.add(fil);

            // remember added files for reload
            for (String file : added) {
              addedFiles.add(file);
            }

            // check files for sneaked changes by single
            if (chkSneaks.isSelected()) {
              for (String fileName : added) {
                val file = INTERFACE.archive().getCachedFile(fileName);
                if (file != null)
                  INTERFACE.archive().getFileStatus(file, true);
              }
            }

            updateFileList(Optional.of(added), false);
            selectFiles(added);
          } catch (Exception e) {
            Dialogs.displayError(e);
          }
        }
      }
    });

    btn_Remove.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        int sel = tblFILES.getSelectedRow();
        if (sel >= 0) {
          try {
            final int[] rows = tblFILES.getSelectedRows();
            final String[] files = Arrays.stream(rows).mapToObj(x -> tblFILES.getValueAt(x, 0).toString()).toArray(String[]::new);
            final String[] modified = Arrays.stream(files).filter(x -> INTERFACE.archive().isChanged(x)).toArray(String[]::new);

            if (modified.length > 0) {
              // confirm to remove modified files
              StringBuilder sb = new StringBuilder(Language.getString("FilesGUI.21"));
              for (String file : modified) {
                sb.append("\n" + file);
              }

              boolean res = Dialogs.showConfirm(sb.toString());
              if (!res)
                return;
            }

            for (String file : files) {
              INTERFACE.remove(file);
              removedFiles.add(file);
              fileInfos.remove(file);
            }

            // update the file list, status and description
            refreshFileList();

            // select next file
            selectRow(sel);
          } catch (Exception e) {
            Dialogs.displayError(e);
          }
        }
      }
    });

    btn_LoadAll.addActionListener(new CBActionListener(x -> {
      // load yet uncached files
      INTERFACE.loadAllFiles();
      refreshFiles(Optional.empty(), chkSneaks.isSelected());
    }));

    btn_Load.addActionListener(new CBActionListener(x -> {
      val sel = getSelectedFiles();
      INTERFACE.loadFiles(sel);
      refreshFiles(Optional.of(sel), chkSneaks.isSelected());
    }));


    btn_DiscardAll.addActionListener(new CBActionListener(x -> {
      boolean res = Dialogs.showConfirm(Language.getString("FilesGUI.22"));
      if (res) {
        try {
          INTERFACE.discard();
          addedFiles.clear();
          removedFiles.clear();
          refreshFiles(Optional.empty(), false);
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    }));

    btn_Discard.addActionListener(new CBActionListener(x -> {
      val sel = getSelectedFiles();
      for (String fileName : sel) {
        INTERFACE.discard(fileName);
        addedFiles.remove(fileName);
        removedFiles.remove(fileName);
      }
      refreshFiles(Optional.of(sel), false);
    }));

    btn_Extract.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (tblFILES.getSelectedRow() >= 0) {
          JFileChooser fc = new JFileChooser();
          fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
          fc.setDialogTitle(
              UE.APP_NAME + " - " + Language.getString("FilesGUI.5")); //$NON-NLS-1$ //$NON-NLS-2$
          fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
          fc.setMultiSelectionEnabled(false);
          int returnVal = fc.showDialog(UE.WINDOW, Language.getString("FilesGUI.4")); //$NON-NLS-1$

          if (returnVal != JFileChooser.APPROVE_OPTION) {
            return;
          }
          File fil = fc.getSelectedFile();
          if (fil != null) {
            UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, fil.getAbsolutePath());

            try {
              int[] s = tblFILES.getSelectedRows();

              String[] li = new String[s.length];

              for (int i = 0; i < s.length; i++) {
                li[i] = (String) tblFILES.getValueAt(s[i], NAME_COLUMN);
              }
              INTERFACE.extract(li, fil, chkRawExport.isSelected());
            } catch (InvalidArgumentsException e) {
              Dialogs.displayError(e);
            } catch (Exception e) {
              Dialogs.displayError(e);
            }
          }
        }
      }
    });

    chkPREVIEW.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        tblFILES.tableChanged(null);
        // remember preview option
        boolean activate = chkPREVIEW.isSelected();
        UE.SETTINGS.setProperty(SettingsManager.FILE_SHOW_PREVIEW, activate);
      }
    });

    chkRawExport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        // remember raw export option
        boolean activate = chkRawExport.isSelected();
        UE.SETTINGS.setProperty(SettingsManager.FILE_RAW_EXPORT, activate);
      }
    });

    chkSneaks.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        // remember sneak checking option
        boolean activate = chkSneaks.isSelected();
        UE.SETTINGS.setProperty(SettingsManager.FILE_SNEAK_CHECKING, activate);
      }
    });

    chkHexCmp.addActionListener(new CBActionListener(x -> {
      // remember hex compare option
      boolean activate = chkHexCmp.isSelected();
      UE.SETTINGS.setProperty(SettingsManager.FILE_HEX_COMPARE, activate);
      setCompareHex(activate);
      refreshFiles(Optional.empty(), false);
    }));

    // open search even when some button has focus
    addKeyListener(searchBox.SearchKeyListener);
    btn_Add.addKeyListener(searchBox.SearchKeyListener);
    btn_Remove.addKeyListener(searchBox.SearchKeyListener);
    btn_Extract.addKeyListener(searchBox.SearchKeyListener);
    btn_LoadAll.addKeyListener(searchBox.SearchKeyListener);
    btn_Load.addKeyListener(searchBox.SearchKeyListener);
    btn_DiscardAll.addKeyListener(searchBox.SearchKeyListener);
    btn_Discard.addKeyListener(searchBox.SearchKeyListener);
    lblPREVIEW.addKeyListener(searchBox.SearchKeyListener);
    chkPREVIEW.addKeyListener(searchBox.SearchKeyListener);
    chkRawExport.addKeyListener(searchBox.SearchKeyListener);
    chkSneaks.addKeyListener(searchBox.SearchKeyListener);
    chkHexCmp.addKeyListener(searchBox.SearchKeyListener);
  }

  @Override
  protected void loadValues() throws IOException {
    setPreview(null);

    fileInfos = INTERFACE.listFileInfos(Optional.empty(), true);
    tblFILES.setModel(new TableData(fileInfos.values()));

    // setup columns
    val columnModel = tblFILES.getColumnModel();
    val sizeColumn = columnModel.getColumn(SIZE_COLUMN);
    val rightRenderer = new DefaultTableCellRenderer();
    rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
    sizeColumn.setMaxWidth(65);
    sizeColumn.setCellRenderer(rightRenderer);
    columnModel.getColumn(STATUS_COLUMN).setMaxWidth(50);

    if (fileInfos.size() > 0) {
      updateListRenderer();
      updateRowSorter();
    }

    updateStatus();
    updateDescription();
  }

  public void updateStatus() {
    StringBuilder sb = new StringBuilder();
    sb.append("<table style='white-space: normal'>"); //$NON-NLS-1$

    // list removed files
    sb.append("<tr><td style='white-space: nowrap' valign='top'>"); //$NON-NLS-1$
    sb.append(Language.getString("FilesGUI.8")); //$NON-NLS-1$
    sb.append("</td><td>"); //$NON-NLS-1$

    Set<String> removedFiles = INTERFACE.removedFiles();
    if (removedFiles.isEmpty()) {
      sb.append(Language.getString("FilesGUI.7")); //$NON-NLS-1$
    } else {
      boolean first = true;
      for (String removed : removedFiles) {
        if (!first)
          sb.append(", "); //$NON-NLS-1$

        sb.append(removed);
        first = false;
      }
    }
    sb.append("</td></tr></table>"); //$NON-NLS-1$

    statusText = sb.toString();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Files;
  }

  @Override
  public boolean hasChanges() {
    return !addedFiles.isEmpty() || !removedFiles.isEmpty();
  }

  @Override
  public void reload() throws IOException {
    addedFiles.clear();
    removedFiles.clear();
    refreshFiles(Optional.empty(), chkSneaks.isSelected());
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    changes.addAll(INTERFACE.archive(), addedFiles);
    changes.addAll(INTERFACE.archive(), removedFiles);
  }

  private void refreshFiles(Optional<Collection<String>> fileNames, boolean checkData) throws IOException {
    Set<String> sel = getSelectedFiles();
    updateFileList(fileNames, checkData);
    // try to restore previous selection
    selectFiles(sel);
  }

  private void updateFileList(Optional<Collection<String>> fileNames, boolean checkData) throws IOException {
    val infos = INTERFACE.listFileInfos(fileNames, checkData);
    if (fileNames.isPresent())
      fileInfos.putAll(infos);
    else
      fileInfos = infos;

    refreshFileList();
  }

  private void refreshFileList() {
    ((TableData)tblFILES.getModel()).setData(fileInfos.values());
    updateStatus();
    updateDescription();
  }

  @Override
  public void finalWarning() {
  }

  @Override
  protected Optional<byte[]> loadUnchanged(InternalFile file) throws IOException {
    return INTERFACE.getRawData(file.getName());
  }

  @Override
  protected byte[] loadDefault(InternalFile file) throws IOException {
    return null;
  }

  @Override
  protected void onListSelectionChanged(ListSelectionEvent x) {
    super.onListSelectionChanged(x);
    if (x.getValueIsAdjusting())
      return;

    int rowCnt = tblFILES.getSelectedRow();
    btn_Extract.setEnabled(rowCnt >= 0);
    btn_Remove.setEnabled(rowCnt >= 0);

    if (selection != null && chkPREVIEW.isSelected()) {
      if (LOADER != null)
        LOADER.cancel();

      LOADER = new PreviewLoader(selection.info.getName());
      LOADER.start();
    } else {
      if (LOADER != null)
        LOADER.cancel();

      setPreview(null);
    }
  }

  private synchronized void setPreview(ImageIcon icon) {
    if (icon == null) {
      lblPREVIEW.setIcon(null);
      lblPREVIEW.setText(Language.getString("FilesGUI.11")); //$NON-NLS-1$
    } else {
      lblPREVIEW.setIcon(icon);
      lblPREVIEW.setText(null);
    }
  }

  private class PreviewLoader extends Thread {

    private String NAME;
    private boolean stop = false;

    public PreviewLoader(String name) {
      NAME = name;
    }

    public void cancel() {
      stop = true;
    }

    @Override
    public void run() {
      setPreview(null);

      if (NAME == null) {
        return;
      }

      lblPREVIEW.setText(Language.getString("FilesGUI.10")); //$NON-NLS-1$

      try {
        if (NAME.toLowerCase().endsWith(".tga") //$NON-NLS-1$
            || NAME.toLowerCase().endsWith(".gif") //$NON-NLS-1$
            || NAME.toLowerCase().endsWith(".cur") //$NON-NLS-1$
            || NAME.toLowerCase().endsWith(".fnt") //$NON-NLS-1$
            || NAME.toLowerCase().endsWith(".ani")) //$NON-NLS-1$
        {
          loadImage(NAME);
        } else {
          if (!stop) {
            setPreview(null);
          }
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
        if (!stop) {
          setPreview(null);
        }
      }
    }

    private void loadImage(String fileName) throws Exception {
      InternalFile f = INTERFACE.getInternalFile(fileName);

      if (f == null) {
        if (!stop)
          setPreview(null);
        return;
      }

      if (f instanceof GifImage) {
        loadGif((GifImage) f);
      } else if (f instanceof TargaImage) {
        loadTga((TargaImage) f);
      } else if (f instanceof TgaFont) {
        loadTgaFont((TgaFont) f);
      } else if (f instanceof AniOrCur) {
        loadAni((AniOrCur) f);
      }
    }

    private void loadGif(GifImage gif) throws IOException {
      Image img = gif.getImage();
      ImageIcon icon = new ImageIcon(img);

      if (lblPREVIEW.getWidth() < icon.getIconWidth()) {
        int width = lblPREVIEW.getWidth();
        int height = icon.getIconHeight();
        if (icon.getIconWidth() > lblPREVIEW.getWidth()) {
          height = (int) (((double) lblPREVIEW.getWidth()) * icon.getIconHeight() / icon
              .getIconWidth());

          icon = new ImageIcon(img.getScaledInstance(width,
              height, Image.SCALE_DEFAULT));
        }
      }

      if (!stop)
        setPreview(icon);
    }

    private void loadTga(TargaImage img) throws IOException {
      // test image
      if (img.getImageType() != 2) {
        String msg = Language.getString("FilesGUI.17"); //$NON-NLS-1$
        msg = msg.replace("%1", img.getName()); //$NON-NLS-1$
        Dialogs.displayError(new Exception(msg));
      }
      if (img.getPixelDepth() != 16) {
        String msg = Language.getString("FilesGUI.18"); //$NON-NLS-1$
        msg = msg.replace("%1", img.getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(img.getPixelDepth())); //$NON-NLS-1$
        Dialogs.displayError(new Exception(msg));
      }

      if (!stop) {
        ImageIcon icon;
        if (img.getImageSize().width > lblPREVIEW.getWidth()) {
          icon = new ImageIcon(img.getThumbnail(lblPREVIEW.getWidth(), false, AlphaMode.Opaque));
        } else {
          icon = new ImageIcon(img.getImage(AlphaMode.Opaque));
        }
        setPreview(icon);
      }
    }

    private void loadTgaFont(TgaFont fnt) throws IOException {
      int column = (int) Math.round((Math.random() * 4));
      int charind;

      if (column == 0) {
        charind = (int) Math.round((Math.random() * 9));
      } else if (column == 3) {
        charind = (int) Math.round((Math.random() * 31));
      } else {
        charind = (int) Math.round((Math.random() * 25));
      }

      if (!stop) {
        Image img = fnt.getCharImage(column, charind);
        ImageIcon icon = new ImageIcon(img);

        int height = icon.getIconHeight();
        int width = icon.getIconWidth();

        while (lblPREVIEW.getHeight() > height * 2) {
          height = height * 2;
          width = width * 2;
        }

        icon = new ImageIcon(img.getScaledInstance(width,
            height, Image.SCALE_DEFAULT));

        setPreview(icon);
      }
    }

    private void loadAni(AniOrCur a) throws IOException {
      TargaImage img = a.getFrame(0);

      if (!stop) {
        ImageIcon icon;
        if (img.getImageSize().width > lblPREVIEW.getWidth()) {
          icon = new ImageIcon(img.getThumbnail(lblPREVIEW.getWidth(), false, AlphaMode.Opaque));
        } else {
          icon = new ImageIcon(img.getImage(AlphaMode.Opaque));
        }
        setPreview(icon);
      }
    }
  }

  private class TableData extends AbstractTableModel implements FileListModel {
    public static final int COLUMNS = 5;

    private FileInfo[] fileInfos;

    // the list must be sorted in order to index the row
    public TableData(Collection<FileInfo> data) {
      fileInfos = data.toArray(new FileInfo[0]);
    }

    public void setData(Collection<FileInfo> data) {
      fileInfos = data.toArray(new FileInfo[0]);
      fireTableDataChanged();
    }

    @Override
    public int getColumnCount() {
      return COLUMNS;
    }

    @Override
    public int getRowCount() {
      return fileInfos.length;
    }

    public String[] entryNames() {
      return Arrays.stream(fileInfos).map(FileInfo::getName).toArray(String[]::new);
    }

    public FileInfo getInfo(int row) {
      return fileInfos[row];
    }

    @Override
    public Object getValueAt(int row, int col) {
      val info = fileInfos[row];

      // for sorting of numbers, keep the data types
      switch (col) {
        case NAME_COLUMN:
          return info.getName();
        case TYPE_COLUMN:
          return info.getFileType();
        case CLASS_COLUMN:
          return info.getClassType().getSimpleName();
        case SIZE_COLUMN:
          return info.getSize();
        case STATUS_COLUMN:
          return info.getStatus().name();
        default:
          return null;
      }
    }

    @Override
    public String getColumnName(int columnIndex) {
      switch (columnIndex) {
        case NAME_COLUMN:
          return Language.getString("FilesGUI.12"); //$NON-NLS-1$
        case TYPE_COLUMN:
          return Language.getString("FilesGUI.13"); //$NON-NLS-1$
        case CLASS_COLUMN:
          return Language.getString("FilesGUI.14"); //$NON-NLS-1$
        case SIZE_COLUMN:
          return Language.getString("FilesGUI.15"); //$NON-NLS-1$
        case STATUS_COLUMN:
          return Language.getString("FilesGUI.16"); //$NON-NLS-1$
        default:
          return null;
      }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      switch (columnIndex) {
        case NAME_COLUMN:
        case TYPE_COLUMN:
        case CLASS_COLUMN:
        case STATUS_COLUMN:
          return String.class;
        case SIZE_COLUMN:
          return Integer.class;
        default:
          return Object.class;
      }
    }
  }
}
