package ue.gui.file;

import ue.edit.common.FileInfo;

public interface FileListModel {

  FileInfo getInfo(int row);
}
