package ue.gui.file;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import lombok.Cleanup;
import lombok.val;
import ue.UE;
import ue.edit.common.CanBeEdited;
import ue.edit.common.FileArchive;
import ue.edit.common.InternalFile;
import ue.edit.mod.Mod;
import ue.edit.mod.ModList;
import ue.edit.res.stbof.Stbof;
import ue.edit.snd.SndFile;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.UEWorker;
import ue.util.data.StringTools;
import ue.util.func.FunctionTools;

/**
 * This class is used to display known modifications stored to mod.lst.
 */
public class ModifiedFilesGUI extends JFrame implements WindowListener {

  private JList<String> lstFILE = new JList<String>();
  private JButton btnADD = new JButton(Language.getString("ModifiedFilesGUI.0")); //$NON-NLS-1$
  private JButton btnREM = new JButton(Language.getString("ModifiedFilesGUI.1")); //$NON-NLS-1$
  private JButton btnEXT_ALL = new JButton(Language.getString("ModifiedFilesGUI.2")); //$NON-NLS-1$
  private JButton btnEXT = new JButton(Language.getString("ModifiedFilesGUI.17")); //$NON-NLS-1$
  private JButton btnEXP_ALL = new JButton(Language.getString("ModifiedFilesGUI.18")); //$NON-NLS-1$
  private JComboBox<String> cmbFILE = new JComboBox<String>();
  private CanBeEdited[] FILES;

  public ModifiedFilesGUI(CanBeEdited selected) {
    // open all game archives to load the mod.lst modifications
    FILES = UE.FILES.openAllGameArchives();

    setup();
    init(selected);
  }

  private void setup() {
    addWindowListener(this);

    /*SETUP ITEMS*/
    btnEXT.setEnabled(false);
    btnEXT_ALL.setEnabled(false);
    btnREM.setEnabled(false);
    Font def = UE.SETTINGS.getDefaultFont();

    btnEXT_ALL.setFont(def);
    btnEXP_ALL.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent ae) {
        // get destination
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        String title = Language.getString("ModifiedFilesGUI.19"); //$NON-NLS-1$
        fc.setDialogTitle(UE.APP_NAME + " - " + title); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);
        int returnVal = fc.showDialog(ModifiedFilesGUI.this, Language.getString("ModifiedFilesGUI.20")); //$NON-NLS-1$

        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File dest = fc.getSelectedFile();
        ArrayList<String> errors = new ArrayList<String>();

        // destination chosen
        if (dest != null) {
          UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, dest.getPath());

          for (CanBeEdited cbe : FILES) {
            try {
              if (!cbe.isModded())
                continue;

              if (cbe instanceof Stbof) {
                Vector<String> files = new Vector<String>();
                Mod[] mods = cbe.modList().getAll();

                //fill
                String str;
                for (int i = 0; i < mods.length; i++) {
                  Iterator<String> it = mods[i].iterator();
                  while (it.hasNext()) {
                    str = it.next();
                    if (!files.contains(str))
                      files.add(str);
                  }
                }

                File f = new File(dest, cbe.getSourceFile().getName());
                if (f.exists() && f.isFile())
                  f = new File(dest, cbe.getClass().getSimpleName());
                if (!f.exists())
                  f.mkdirs();

                String[] inFile = files.toArray(new String[0]);
                if (!extract(cbe, inFile, f)) {
                  // stop if cancelled
                  return;
                }
              } else {
                String name = cbe.getSourceFile().getName();
                File f = new File(dest, name.substring(name.lastIndexOf(".") + 1)); //$NON-NLS-1$

                if (!f.exists())
                  f.mkdirs();

                if (!copy(cbe, f)) {
                  // stop if cancelled
                  return;
                }
              }
            } catch (Exception e) {
              e.printStackTrace();
              errors.add(cbe.getName());
            }
          }
        }

        if (!errors.isEmpty()) {
          String msg = Language.getString("ModifiedFilesGUI.24"); //$NON-NLS-1$
          StringBuilder sb = new StringBuilder(msg);
          for (String file : errors)
            sb.append("\n" + file);
          Dialogs.displayError(sb.toString());
        }
      }
    });

    cmbFILE.setFont(def);
    cmbFILE.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        int sel = cmbFILE.getSelectedIndex();
        if (sel < 0)
          return;

        btnADD.setEnabled(FILES[sel] instanceof SndFile || FILES[sel] instanceof FileArchive);

        fillList();

        btnEXT_ALL.setEnabled(lstFILE.isEnabled());
      }
    });

    lstFILE.setFont(def);
    lstFILE.addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent arg0) {
        btnREM.setEnabled(!lstFILE.isSelectionEmpty());
        btnEXT.setEnabled(btnREM.isEnabled());
      }
    });
    // Add file to the list
    btnADD.setFont(def);
    btnADD.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent ae) {
        if (cmbFILE.getSelectedIndex() < 0)
          return;

        String title = Language.getString("ModifiedFilesGUI.3"); //$NON-NLS-1$
        String msg = Language.getString("ModifiedFilesGUI.4"); //$NON-NLS-1$
        String n = JOptionPane.showInputDialog(title, msg);

        if (n != null && n.length() > 0) {
          val file = FILES[cmbFILE.getSelectedIndex()];
          ModList mods = file.modList();

          if (mods != null) {
            mods.add(n);
            file.markChanged();
            fillList();
          }
        }
      }
    });
    // Remove from list
    btnREM.setFont(def);
    btnREM.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent ae) {
        if (lstFILE.isSelectionEmpty())
          return;

        val file = FILES[cmbFILE.getSelectedIndex()];
        ModList mods = file.modList();

        if (mods != null) {
          List<String> values = lstFILE.getSelectedValuesList();
          values.forEach(x -> mods.remove(x));
          file.markChanged();
          fillList();
        }
      }
    });

    // Extract all modified files
    btnEXT_ALL.setFont(def);
    btnEXT_ALL.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent ae) {
        // get destination
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        String title = Language.getString("ModifiedFilesGUI.5"); //$NON-NLS-1$
        fc.setDialogTitle(UE.APP_NAME + " - " + title);//$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);

        String msg = Language.getString("ModifiedFilesGUI.6"); //$NON-NLS-1$
        int returnVal = fc.showDialog(ModifiedFilesGUI.this, msg);
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File fil = fc.getSelectedFile();
        // destination chosen
        if (fil != null) {
          UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, fil.getPath());
          // go thru all modified files
          Vector<String> files = new Vector<String>();

          val file = FILES[cmbFILE.getSelectedIndex()];
          ModList ml = file.modList();
          Mod[] mods = ml != null ? ml.getAll() : new Mod[0];

          //fill
          for (int i = 0; i < mods.length; i++) {
            Iterator<String> it = mods[i].iterator();
            while (it.hasNext()) {
              String str = it.next();
              if (!files.contains(str)) {
                files.add(str);
              }
            }
          }

          try {
            String[] inFile = files.toArray(new String[0]);
            extract(FILES[cmbFILE.getSelectedIndex()], inFile, fil);
          } catch (Exception e) {
            Dialogs.displayError(e);
          }
        }
      }
    });

    // Extract selected files
    btnEXT.setFont(def);
    btnEXT.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent ae) {
        if (lstFILE.isSelectionEmpty()) {
          return;
        }

        // get destination
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        fc.setDialogTitle(UE.APP_NAME + " - " //$NON-NLS-1$
            + Language.getString("ModifiedFilesGUI.5")); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(false);

        String msg = Language.getString("ModifiedFilesGUI.6"); //$NON-NLS-1$
        int returnVal = fc.showDialog(ModifiedFilesGUI.this, msg);
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File fil = fc.getSelectedFile();

        if (fil != null) {
          UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, fil.getPath());
          List<String> values = lstFILE.getSelectedValuesList();
          Vector<String> files = new Vector<String>();

          for (String str : values) {
            if (!files.contains(str)) {
              files.add(str);
            }
          }
          try {
            String[] inFile = files.toArray(new String[0]);
            extract(FILES[cmbFILE.getSelectedIndex()], inFile, fil);
          } catch (Exception e) {
            Dialogs.displayError(e);
          }
        }
      }
    });

    /*ADD ITEMS TO FRAME*/
    //general layout settings
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    //adding items
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    add(cmbFILE, c);
    c.gridx = 1;
    add(btnEXP_ALL, c);
    c.gridx = 0;
    c.gridy = 1;
    c.gridheight = 5;
    JScrollPane scr = new JScrollPane(lstFILE);
    scr.setPreferredSize(new Dimension(200, 200));
    add(scr, c);
    c.gridheight = 1;
    c.gridx = 1;
    add(btnADD, c);
    c.gridy = 2;
    add(btnREM, c);
    c.gridy = 3;
    add(btnEXT, c);
    c.gridy = 4;
    add(btnEXT_ALL, c);

    /*FRAME SETUP*/
    //icon is the same as main
    setIconImage(UE.ICON);
    setTitle(Language.getString("ModifiedFilesGUI.7")); //$NON-NLS-1$
    pack();
    setResizable(false);    //this frame is not resizable
    //place at the center of screen
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation(new Point((int) (screenSize.getWidth() / 2 - 125),
        (int) (screenSize.getHeight() / 2 - 125)));
    setVisible(true);
  }

  private void fillList() {
    int sel = cmbFILE.getSelectedIndex();
    if (sel < 0) {
      resetFilesText();
      return;
    }

    try {
      if (!FILES[sel].isModded()) {
        setFilesText(Language.getString("ModifiedFilesGUI.22")); //$NON-NLS-1$
        return;
      }

      ModList ml = FILES[sel].modList();
      if (ml == null) {
        setFilesText(Language.getString("ModifiedFilesGUI.21")); //$NON-NLS-1$
        return;
      }

      // collect mod changes & eliminate duplicates
      Mod[] mods = ml.getAll();
      Set<String> fileChanges = Arrays.stream(mods)
        .flatMap(x -> x.list().stream())
        .collect(Collectors.toSet());

      // sort by file name
      String[] sorted = fileChanges.toArray(new String[0]);
      Arrays.sort(sorted);

      //refill
      lstFILE.setListData(sorted);
      lstFILE.setEnabled(true);
    } catch(Exception e) {
      resetFilesText();
      Dialogs.displayError(e);
    }
  }

  private void setFilesText(String text) {
    Font def = UE.SETTINGS.getDefaultFont();
    FontMetrics metrics = cmbFILE.getFontMetrics(def);

    // wrap text to multiple lines for the JTable
    // alternatively hide and replace the JTable by some line wrapped text box
    text = StringTools.wrapAtWidth(text, metrics, 175);
    String[] lines = text.split("\n"); //$NON-NLS-1$

    lstFILE.setListData(lines);
    lstFILE.setEnabled(false);
  }

  private void resetFilesText() {
    lstFILE.setListData(new String[0]);
  }

  private void init(CanBeEdited selected) {
    Font def = UE.SETTINGS.getDefaultFont();
    FontMetrics metrics = cmbFILE.getFontMetrics(def);
    int sel = 0;

    for (int i = 0; i < FILES.length; i++) {
      if (FILES[i].equals(selected))
        sel = i;

      String str = FILES[i].getSourceFile().getAbsolutePath();
      str = StringTools.shortenPath(str, metrics, 175);
      cmbFILE.addItem(str);
    }

    cmbFILE.setSelectedIndex(sel);
  }

  /**
   * Extracts files.
   *
   * @param edited
   * @param inFile the name of the file to extract.
   * @param to     the destination where to extract the file to.
   * @returns whether successfully processed and not cancelled
   */
  public boolean extract(CanBeEdited edited, String[] inFile, File to) throws Exception {
    File file;
    ArrayList<String> files = new ArrayList<String>();
    boolean overwrite_all = false;
    boolean skip_all = false;

    for (String name : inFile) {
      if (edited instanceof FileArchive) {
        file = new File(to, name);
      } else {
        file = new File(to, name + ".wav"); //$NON-NLS-1$
      }

      if (file.isFile() && file.exists()) {
        if (overwrite_all) {
          file.delete();
          files.add(name);
        } else if (skip_all) {
          continue;
        } else {
          String msg = Language.getString("ModifiedFilesGUI.10"); //$NON-NLS-1$

          int respon = JOptionPane.showOptionDialog(
              UE.WINDOW,
              msg.replace("%1", file.getName()), //$NON-NLS-1$
              UE.APP_NAME,
              JOptionPane.NO_OPTION,
              JOptionPane.QUESTION_MESSAGE,
              null,
              new String[]{Language.getString("ModifiedFilesGUI.11"), // yes //$NON-NLS-1$
                  Language.getString("ModifiedFilesGUI.12"), // yes to all //$NON-NLS-1$
                  Language.getString("ModifiedFilesGUI.13"), // no //$NON-NLS-1$
                  Language.getString("ModifiedFilesGUI.14"), // No to all //$NON-NLS-1$
              },
              Language.getString("ModifiedFilesGUI.13") // no //$NON-NLS-1$
          );

          switch (respon) {
            case 0: {
              file.delete();
              files.add(name);
              break;
            }
            case 1: {
              file.delete();
              files.add(name);
              overwrite_all = true;
              break;
            }
            case 2: {
              continue;
            }
            case 3: {
              skip_all = true;
            }
          }
        }
      } else {
        files.add(name);
      }
    }

    ExtractTask task = new ExtractTask(edited, to, files.toArray(new String[0]));
    return GuiTools.runUEWorker(task);
  }

  private boolean copy(CanBeEdited cbe, File f) {
    File dest = new File(f, cbe.getSourceFile().getName());

    if (dest.isFile() && dest.exists()) {
      String msg = Language.getString("ModifiedFilesGUI.10"); //$NON-NLS-1$

      int respon = JOptionPane.showOptionDialog(
          UE.WINDOW,
          msg.replace("%1", dest.getName()), //$NON-NLS-1$
          UE.APP_NAME,
          JOptionPane.OK_CANCEL_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          new String[]{
            Language.getString("ModifiedFilesGUI.11"), // yes //$NON-NLS-1$
            Language.getString("ModifiedFilesGUI.13") // no //$NON-NLS-1$
          },
          Language.getString("ModifiedFilesGUI.13") // no //$NON-NLS-1$
      );

      if (respon != JOptionPane.OK_OPTION)
        return false;

      dest.delete();
    }

    CopyTask task = new CopyTask(cbe, dest);
    return GuiTools.runUEWorker(task);
  }

  private class ExtractTask extends UEWorker<Boolean> {

    private CanBeEdited src;
    private File to;
    private String[] files;

    public ExtractTask(CanBeEdited edited, File fil, String[] p) {
      src = edited;
      to = fil;
      files = p;
    }

    @Override
    protected Boolean work() throws Exception {
      for (int i = 0; i < files.length; i++) {
        if (isCancelled())
          return false;

        boolean isArchive = src instanceof FileArchive;
        String fileName = isArchive ? files[i] : files[i] + ".wav"; //$NON-NLS-1$
        File tor = new File(to.getAbsolutePath(), fileName);

        String msg = Language.getString("ModifiedFilesGUI.16"); //$NON-NLS-1$
        msg = msg.replace("%1", tor.getName()); //$NON-NLS-1$
        feedMessage(msg);

        if (isArchive) {
          InternalFile fui = ((FileArchive) src).getInternalFile(files[i], false);
          tor.createNewFile();
          @Cleanup FileOutputStream out = new FileOutputStream(tor);
          fui.save(out);
        } else {
          String wavName = files[i];
          int index = FunctionTools.defaultIfThrown(() -> Integer.parseInt(wavName), -1);
          if (index > -1)
            ((SndFile) src).extract(index, tor);
        }
      }

      return true;
    }
  }

  private class CopyTask extends UEWorker<Boolean> {

    private CanBeEdited src;
    private File to;

    public CopyTask(CanBeEdited edited, File fil) {
      src = edited;
      to = fil;
    }

    @Override
    protected Boolean work() throws IOException {
      String msg = Language.getString("ModifiedFilesGUI.23")
        .replace("%1", src.getSourceFile().getName()); //$NON-NLS-1$ //$NON-NLS-2$
      sendMessage(msg);

      try (
        FileInputStream fis = new FileInputStream(src.getSourceFile());
        FileOutputStream fos = new FileOutputStream(to);
      ){
        byte[] b = new byte[1024];
        int len;

        while ((len = fis.read(b)) > 0) {
          if (isCancelled()) {
            to.delete();
            return false;
          }

          fos.write(b, 0, len);
        }
      }

      return true;
    }
  }

  // SPECIFIED BY WINDOW LISTENER
  @Override
  public void windowActivated(WindowEvent e) {
  }

  @Override
  public void windowClosed(WindowEvent e) {
    UE.WINDOW.setEnabled(true);
    UE.WINDOW.toFront();
  }

  @Override
  public void windowClosing(WindowEvent e) {
    UE.WINDOW.setEnabled(true);
    UE.WINDOW.toFront();
  }

  @Override
  public void windowDeactivated(WindowEvent e) {
  }

  @Override
  public void windowDeiconified(WindowEvent e) {
  }

  @Override
  public void windowIconified(WindowEvent e) {
  }

  @Override
  public void windowOpened(WindowEvent e) {
  }
}
