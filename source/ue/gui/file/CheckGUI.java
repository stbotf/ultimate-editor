package ue.gui.file;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import lombok.val;
import ue.UE;
import ue.edit.common.CanBeEdited;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.service.Language;

/**
 * This MainPanel is used to display results of the check() query.
 * Check() is specified by the CanBeEdited interface.
 */
public class CheckGUI extends MainPanel {

  private CanBeEdited file;

  //displays results
  private JTextArea res;

  /**
   * Constructor.
   *
   * @param results contains entries to be displayed.
   */
  public CheckGUI(CanBeEdited file) {
    this.file = file;
    createUI();
    check();
  }

  private void createUI() {
    // setup text area
    val scrollPane = createTextPane();

    // main panel
    setLayout(new BorderLayout());
    add(scrollPane, BorderLayout.CENTER);
    setVisible(true);
  }

  private JScrollPane createTextPane() {
    res = new JTextArea();
    res.setEditable(false);
    res.setLineWrap(true);
    res.setWrapStyleWord(true);
    res.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    Font def = UE.SETTINGS.getDefaultFont();
    res.setFont(def);

    JScrollPane scrollPane = new JScrollPane(res);
    scrollPane.setPreferredSize(new Dimension(560, 420));
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    return scrollPane;
  }

  private void check() {
    Vector<String> results = file.check();
    StringBuilder text = new StringBuilder(Language.getString("CheckGUI.0")); //$NON-NLS-2$
    text.append("\n"); //$NON-NLS-1$

    for (String result : results) {
      text.append("\n"); //$NON-NLS-1$
      text.append(result);
    }

    res.setText(text.toString());
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Check;
  }

  @Override
  public boolean hasChanges() {
    // none to incrementally keep checking same files over again
    // since potentially all the files are changed, to undo
    // better re-open the .sav file or use the files menu
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  @Override
  public void reload() {
    // incrementally keep checking same files over again
    check();
  }

  @Override
  public void finalWarning() {
  }
}
