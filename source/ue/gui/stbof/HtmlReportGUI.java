package ue.gui.stbof;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.UEWorker;
import ue.util.file.PathHelper;

/**
 * Base class to create HTML reports based on input files.
 */
public abstract class HtmlReportGUI extends MainPanel {

  // displays report
  private JEditorPane htmlReport;
  private JButton btnSave;
  private JScrollPane scrollPane;

  private String report = null;

  public HtmlReportGUI() {
    Font def = UE.SETTINGS.getDefaultFont();
    setupComponents(def);
    placeComponents();
  }

  private void setupComponents(Font def) {
    // editor pane
    this.htmlReport = new JEditorPane("text/html", null); //$NON-NLS-1$
    this.htmlReport.setEditable(false);
    htmlReport.setCaretPosition(0);

    // scrollpane
    scrollPane = new JScrollPane(this.htmlReport);
    scrollPane.setPreferredSize(new Dimension(560, 340));
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

    // save button
    btnSave = new JButton(Language.getString("HtmlReportGUI.0")); //$NON-NLS-1$
    btnSave.setFont(def);
    btnSave.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser jfc = new JFileChooser(new File(
          UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
        int returnVal = jfc.showSaveDialog(UE.WINDOW);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          File file = jfc.getSelectedFile();
          String workPath = PathHelper.toFolderPath(file).toString();
          UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

          if (!file.getName().endsWith(".html")) //$NON-NLS-1$
            file = new File(workPath, file.getName() + ".html"); //$NON-NLS-1$

          if (file.exists()) {
            String msg = Language.getString("HtmlReportGUI.1"); //$NON-NLS-1$
            msg = msg.replace("%1", file.getName()); //$NON-NLS-1$
            int respon = JOptionPane.showConfirmDialog(UE.WINDOW, msg, UE.APP_NAME,
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (respon != JOptionPane.YES_OPTION)
              return;
          }

          try (FileOutputStream out = new FileOutputStream(file)) {
            int i = 0;
            int j = 0;
            int len = 10000;
            int todo = report.length();

            while (todo > 0) {
              if (len > todo)
                len = todo;

              j += len;
              out.write(report.substring(i, j).getBytes());

              i += len;
              todo -= len;
            }
          } catch (Exception x) {
            Dialogs.displayError(x);
          }
        }
      }
    });
  }

  private void placeComponents() {
    JPanel pnl = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    pnl.add(btnSave);

    this.setLayout(new BorderLayout());
    this.add(scrollPane, BorderLayout.CENTER);
    this.add(pnl, BorderLayout.SOUTH);
  }

  protected void updateReport() {
    generateReport();
    htmlReport.setText(report);
  }

  /**
   * @return the generated report text
   * @throws InvalidArgumentsException
   */
  protected void generateReport() {
    ReportGenerator gen = new ReportGenerator();
    report = GuiTools.runUEWorker(gen);
  }

  @Override
  public boolean hasChanges() {
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  @Override
  public void reload() {
    updateReport();
  }

  @Override
  public void finalWarning() {
  }

  protected abstract InternalFile[] filesToCheck();
  protected abstract void genReport(StringBuffer report) throws Exception;

  private class ReportGenerator extends UEWorker<String> {

    public ReportGenerator() {
      RESULT = ""; //$NON-NLS-1$
    }

    @Override
    public String work() throws Exception {
      try {
        // check for errors first and abort if any are found
        Vector<String> test = new Vector<String>();
        String msg = Language.getString("HtmlReportGUI.2"); //$NON-NLS-1$

        InternalFile[] files = filesToCheck();
        for (InternalFile file : files) {
          feedMessage(msg.replace("%1", file.getName())); //$NON-NLS-1$
          test.addAll(file.check());

          if (isCancelled())
            return RESULT;
        }

        StringBuffer report = new StringBuffer();
        boolean errors = false;

        for (int i = 0; i < test.size(); i++) {
          if (test.get(i).startsWith(Language.getString("HtmlReportGUI.3"))) { //$NON-NLS-1$
            if (!errors)
              report.append(Language.getString("HtmlReportGUI.4") + "<br><br>"); //$NON-NLS-1$

            report.append(test.get(i) + "<br>"); //$NON-NLS-1$
            errors = true;
          }
        }

        if (!errors) {
          msg = Language.getString("HtmlReportGUI.5"); //$NON-NLS-1$
          sendMessage(msg);
          genReport(report);
        }

        return report.toString();
      } catch (Exception e) {
        RESULT = e.getLocalizedMessage();
        throw e;
      }
    }
  }
}
