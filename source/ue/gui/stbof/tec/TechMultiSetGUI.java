package ue.gui.stbof.tec;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.tec.TecField;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.edit.res.stbof.files.tec.data.TechTreeField;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.calc.Calc;
import ue.util.data.ID;
import ue.util.func.FunctionTools;

public class TechMultiSetGUI extends ue.gui.common.MainPanel {

  // read
  private Stbof STBOF;
  private TecField FIELD;

  // edited
  private TechTree TECH;

  // ui components
  private JButton btnCalc = new JButton();
  private JButton btnMathApply = new JButton();
  private JCheckBox chkAllFields = new JCheckBox();
  private JComboBox<String> cmbFields = new JComboBox<String>();
  private JPanel jPanel1 = new JPanel();
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JLabel lblAdd = new JLabel();
  private JLabel lblDiv = new JLabel();
  private JLabel lblMath = new JLabel();
  private JLabel lblMult = new JLabel();
  private JList<ID> lstTechs = new JList<ID>();
  private JTextField txtAdd = new JTextField();
  private JTextField txtDiv = new JTextField();
  private JTextField txtMath = new JTextField();
  private JTextField txtMult = new JTextField();

  // data
  private int sel_field = -1;
  private Thread task = null;
  private Timer tCOOLER;

  /**
   * Creates new form TechGUI2
   * @throws IOException
   */
  public TechMultiSetGUI(Stbof st) throws IOException {
    STBOF = st;
    FIELD = (TecField) STBOF.getInternalFile(CStbofFiles.TecFieldTec, true);
    TECH = (TechTree) STBOF.getInternalFile(CStbofFiles.TechTreeTec, true);

    tCOOLER = new Timer(1000, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        if (task == null || !task.isAlive()) {
          setControlsEnabled(true);
          tCOOLER.stop();
        }
      }
    });

    // gui
    initComponents();
    setFonts();
    loadTranslations();

    // magic
    initMagic();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.TechFieldsGroupEdit;
  }

  private void initMagic() {
    // load fields
    String[] fields = FIELD.getFields();

    cmbFields.removeAllItems();
    for (int i = 0; i < fields.length; i++) {
      cmbFields.addItem(fields[i]);
    }
  }

  private void setFonts() {
    Font def = UE.SETTINGS.getDefaultFont();

    btnMathApply.setFont(def);
    cmbFields.setFont(def);
    lstTechs.setFont(def);
    lblAdd.setFont(def);
    lblDiv.setFont(def);
    lblMath.setFont(def);
    lblMult.setFont(def);
    txtAdd.setFont(def);
    btnCalc.setFont(def);
    txtDiv.setFont(def);
    txtMath.setFont(def);
    txtMult.setFont(def);
    chkAllFields.setFont(def);
  }

  private void loadTranslations() {
    jPanel1.setBorder(BorderFactory.createTitledBorder("TechMultiSetGUI.0"));
    ((TitledBorder) jPanel1.getBorder()).setTitle(Language.getString("TechMultiSetGUI.1"));
    lblAdd.setText(Language.getString("TechMultiSetGUI.2"));
    lblDiv.setText(Language.getString("TechMultiSetGUI.3"));
    lblMath.setText(Language.getString("TechMultiSetGUI.4"));
    lblMult.setText(Language.getString("TechMultiSetGUI.5"));
    chkAllFields.setText(Language.getString("TechMultiSetGUI.6"));
    String apply = Language.getString("TechMultiSetGUI.7");
    btnCalc.setText(apply);
    btnMathApply.setText(apply);
    btnCalc.setText(apply);
  }

  private void fillTechList() {
    if (sel_field < 0)
      return;

    TechTreeField field = TECH.field(sel_field);
    ID[] tech = field.getTechList();

    for (ID lvl : tech) {
      long cost = FunctionTools.defaultIfThrown(() -> field.getEntry(lvl.ID).cost(), (long)0);
      lvl.NAME = "L" + lvl.ID + " " + lvl.NAME + ": " + cost;
    }

    // restore selection
    ID prev = lstTechs.getSelectedValue();
    lstTechs.setListData(tech);
    if (prev != null)
      lstTechs.setSelectedValue(prev, true);
  }

  private void setControlsEnabled(boolean enabled) {
    cmbFields.setEnabled(enabled);
    txtAdd.setEnabled(enabled);
    btnCalc.setEnabled(enabled);
    txtDiv.setEnabled(enabled);
    txtMath.setEnabled(enabled);
    txtMult.setEnabled(enabled);
    btnMathApply.setEnabled(enabled);
    lstTechs.setEnabled(enabled);
    chkAllFields.setEnabled(enabled);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated
  // Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    String[] itemList = new String[]{"Item 1", "Item 2", "Item 3", "Item 4"};
    cmbFields.setModel(new DefaultComboBoxModel<String>(itemList));
    cmbFields.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        cmbFieldsActionPerformed(evt);
      }
    });

    lblMult.setText("jLabel1");
    lblDiv.setText("jLabel1");
    lblAdd.setText("jLabel1");

    btnCalc.setText("Apply");
    btnCalc.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        btnCalcActionPerformed(evt);
      }
    });

    lblMath.setText("jLabel1");
    txtMath.setText("152.4158*((L-1)^4)+(L-1)*(10-L)*31");

    btnMathApply.setText("Apply");
    btnMathApply.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        btnMathApplyActionPerformed(evt);
      }
    });

    chkAllFields.setText("jCheckBox1");

    GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
          .addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
              .addComponent(lblAdd, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
              .addComponent(lblDiv, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
              .addComponent(lblMult, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              .addComponent(txtAdd, GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
              .addComponent(txtDiv, GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
              .addComponent(txtMult, GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE)
              .addComponent(btnCalc, GroupLayout.Alignment.TRAILING)))
          .addComponent(lblMath, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
          .addComponent(txtMath, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
          .addComponent(btnMathApply)
          .addComponent(chkAllFields, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE))
        .addContainerGap());
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
          .addComponent(lblMult)
          .addComponent(txtMult, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
          .addComponent(lblDiv)
          .addComponent(txtDiv, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
          .addComponent(lblAdd)
          .addComponent(txtAdd, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(btnCalc)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(lblMath)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(txtMath, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(btnMathApply)
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(chkAllFields)
        .addContainerGap());

    lstTechs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(lstTechs);

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
            .addComponent(jScrollPane1)
            .addComponent(cmbFields, 0, 165, Short.MAX_VALUE))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addContainerGap()));
    layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
          .addGroup(layout.createSequentialGroup()
            .addComponent(cmbFields, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
              .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)))
          .addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addContainerGap()));
  }// </editor-fold>//GEN-END:initComponents

  private void cmbFieldsActionPerformed(ActionEvent evt) {
    // save old
    try {
      finalWarning();

      // setup new
      sel_field = cmbFields.getSelectedIndex();
      fillTechList();
    } catch (Exception ex) {
      // really bad stuff happening - bail
      Dialogs.displayError(ex);
    }
  }

  private void btnCalcActionPerformed(ActionEvent evt) {
    try {
      finalWarning();
    } catch (Exception ex) {
      // really bad stuff happening - bail
      Dialogs.displayError(ex);
      return;
    }

    setControlsEnabled(false);

    task = new Thread(new Runnable() {
      @Override
      public void run() {
        double num;
        boolean filter = !chkAllFields.isSelected();

        try {
          for (int i = 0; i < TECH.getNumberOfEntries(); i++) {
            if (filter && TECH.getEntry(i).field() != sel_field)
              continue;

            // multiply
            try {
              num = Double.parseDouble(txtMult.getText());
              TECH.getEntry(i).setCost((int) Math.max(TECH.getEntry(i).cost() * num, 0));
            } catch (Exception ex) {
            }

            // divide
            try {
              num = Double.parseDouble(txtDiv.getText());
              TECH.getEntry(i).setCost((int) Math.max(TECH.getEntry(i).cost() / num, 0));
            } catch (Exception ex) {
            }

            // add
            try {
              num = Double.parseDouble(txtAdd.getText());
              TECH.getEntry(i).setCost((int) Math.max(TECH.getEntry(i).cost() + num, 0));
            } catch (Exception ex) {
            }
          }
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }

        fillTechList();
      }
    });

    task.start();
    tCOOLER.setRepeats(true);
    tCOOLER.start();
  }

  private void btnMathApplyActionPerformed(ActionEvent evt) {// GEN-FIRST:event_btnMathApplyActionPerformed
    try {
      finalWarning();
    } catch (Exception ex) {
      // really bad stuff happening - bail
      Dialogs.displayError(ex);
      return;
    }

    setControlsEnabled(false);

    task = new Thread(new Runnable() {
      @Override
      public void run() {
        String calc;
        byte level, field;
        boolean filter = !chkAllFields.isSelected();

        try {
          for (int i = 0; i < TECH.getNumberOfEntries(); i++) {
            level = (byte) TECH.getEntry(i).level();
            field = (byte) TECH.getEntry(i).field();

            if (filter && field != sel_field) {
              continue;
            }

            calc = txtMath.getText();
            calc = calc.replaceAll("L", Byte.toString(level));

            TECH.getEntry(i).setCost((int) Math.max(Calc.calculate(calc), 0));
          }
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }

        fillTechList();
      }
    });

    task.start();
    tCOOLER.setRepeats(true);
    tCOOLER.start();
  }// GEN-LAST:event_btnMathApplyActionPerformed

  @Override
  public boolean hasChanges() {
    return TECH.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(TECH, changes);
  }

  @Override
  public void reload() throws IOException {
    TECH = (TechTree) STBOF.getInternalFile(CStbofFiles.TechTreeTec, true);
    fillTechList();
  }

  @Override
  public void finalWarning() {
  }
}
