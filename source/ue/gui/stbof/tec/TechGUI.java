package ue.gui.stbof.tec;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.patch.tec.TechLevelsPatch;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.tec.TecField;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.edit.res.stbof.files.tec.data.TechTreeEntry;
import ue.edit.res.stbof.files.tec.data.TechTreeField;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.DescTools;
import ue.gui.util.component.IconButton;
import ue.gui.util.component.ImageButton;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.data.ID;
import ue.util.file.FileFilters;
import ue.util.func.FunctionTools;

/**
 * @author Alan
 */
public class TechGUI extends MainPanel {

  private static final Color ALERT_COLOR = Color.RED;
  private static final Color TEXT_COLOR = Color.BLACK;

  // read
  private Stbof STBOF;

  // edited
  private TecField FIELD;
  private TechTree TECH;
  private TechLevelsPatch techLvlPatch = null;

  // ui components
  // tech selection
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JComboBox<String> cmbFields = new JComboBox<String>();
  private JList<ID> lstTechs = new JList<ID>();
  private IconButton btnFieldName = IconButton.CreateEditButton();
  // details
  private JPanel jPanel1 = new JPanel();
  private JScrollPane jScrollPane2 = new JScrollPane();
  private JComboBox<String> cmbImageFile = new JComboBox<String>();
  private JLabel lblName = new JLabel();
  private JLabel lblDesc = new JLabel();
  private JLabel lblImageIcon = new JLabel();
  private JLabel lblReqResPoints = new JLabel();
  private JTextField txtName = new JTextField(40);
  private JTextArea txtDesc = new JTextArea();
  private JTextField txtReqResPoints = new JTextField();
  private ImageButton btnAddTech = ImageButton.CreatePlusButton();
  private ImageButton btnRemTech = ImageButton.CreateMinusButton();
  private IconButton btnDescTools = IconButton.CreateDescriptionButton();
  private JLabel lblTrekTechLvls = new JLabel();
  private JSpinner spiTrekTechLvls = new JSpinner(new SpinnerNumberModel(11, 0, 42, 1));

  // data
  private int sel_field = -1;
  private int sel_level = -1;
  private Thread task = null;
  private Timer tCOOLER;
  private boolean techUpdate = false;

  /**
   * Creates new form TechGUI2
   * @throws IOException
   */
  public TechGUI(Stbof st, Trek trek) throws IOException {
    this.STBOF = st;
    this.FIELD = (TecField) STBOF.getInternalFile(CStbofFiles.TecFieldTec, true);
    this.TECH = (TechTree) STBOF.getInternalFile(CStbofFiles.TechTreeTec, true);

    if (trek != null)
      this.techLvlPatch = new TechLevelsPatch(trek);

    tCOOLER = new Timer(1000, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        if (task == null || !task.isAlive()) {
          setControlsEnabled(true);
          tCOOLER.stop();
        }
      }
    });

    // gui
    initComponents();
    loadTranslations();
    initLayout();

    if (!UE.SETTINGS.getBooleanProperty(SettingsManager.EXPERIMENTAL))
      disableExperimental();

    // load tech data
    loadImages();
    loadData();
    addListeners();
    cmbFields.setSelectedIndex(0);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.TechFields;
  }

  private void disableExperimental() {
    btnAddTech.setVisible(false);
    btnRemTech.setVisible(false);
  }

  private void loadImages() throws IOException {
    // load image file names
    cmbImageFile.removeAllItems();
    Collection<String> images = STBOF.getFileNames(FileFilters.suffix("_b.tga"));
    for (String img : images) {
      img = img.substring(0, img.length() - 6);

      if (STBOF.hasInternalFile(img + "_60.tga") && STBOF.hasInternalFile(img + ".tga"))
        cmbImageFile.addItem(img + ".tga");
    }
  }

  private void loadData() throws IOException {
    int prev = sel_field;
    sel_field = -1;
    sel_level = -1;

    // load fields
    cmbFields.removeAllItems();
    String[] fields = FIELD.getFields();
    for (int i = 0; i < fields.length; i++) {
      cmbFields.addItem(fields[i]);
    }

    // restore selection
    if (prev >= 0 && prev < fields.length)
      cmbFields.setSelectedIndex(prev);
    else if (fields.length > 0)
      cmbFields.setSelectedIndex(0);

    // load trek.exe modifications
    if (techLvlPatch != null) {
      techLvlPatch.reload();
      spiTrekTechLvls.getModel().setValue(techLvlPatch.intValue());
    }
  }

  private void initComponents() {
    Font def = UE.SETTINGS.getDefaultFont();

    // tech selection
    cmbFields.setFont(def);
    lstTechs.setFont(def);
    btnFieldName.setFont(def);
    // details
    lblName.setFont(def);
    txtName.setFont(def);
    lblDesc.setFont(def);
    txtDesc.setFont(def);
    lblReqResPoints.setFont(def);
    txtReqResPoints.setFont(def);
    lblTrekTechLvls.setFont(def);
    spiTrekTechLvls.setFont(def);
    lblImageIcon.setFont(def);
    cmbImageFile.setFont(def);

    txtDesc.setColumns(20);
    txtDesc.setLineWrap(true);
    txtDesc.setRows(5);
    txtDesc.setWrapStyleWord(true);
    jScrollPane2.setViewportView(txtDesc);
    lblImageIcon.setVerticalAlignment(SwingConstants.TOP);
    lstTechs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(lstTechs);

    if (techLvlPatch == null)
      spiTrekTechLvls.setEnabled(false);
  }

  private void loadTranslations() {
    lblName.setText(Language.getString("TechGUI.2"));
    lblDesc.setText(Language.getString("TechGUI.3"));
    lblReqResPoints.setText(Language.getString("TechGUI.1"));
    lblTrekTechLvls.setText(Language.getString("TechGUI.7"));
  }

  private void fillTechList(boolean restoreSelection) {
    if (techUpdate || sel_field < 0)
      return;

    try {
      techUpdate = true;

      int idx = lstTechs.getSelectedIndex();
      ID sel = lstTechs.getSelectedValue();
      sel_level = -1;

      TechTreeField field = TECH.field(sel_field);
      ID[] tech = field.getTechList();

      for (ID lvl : tech) {
        long cost = FunctionTools.defaultIfThrown(() -> field.getEntry(lvl.ID).cost(), (long)0);
        lvl.NAME = "L" + lvl.ID + " " + lvl.NAME + ": " + cost;
      }

      lstTechs.setListData(tech);

      // restore selection
      if (restoreSelection) {
        if (sel != null) {
          lstTechs.setSelectedValue(sel, true);
          // fallback to select the previous index
          if (lstTechs.getSelectedValue() == null) {
            idx = Integer.min(idx, tech.length - 1);
            lstTechs.setSelectedIndex(idx);
          }
        } else {
          lstTechs.setSelectedIndex(0);
        }
      }
    } finally {
      techUpdate = false;
    }
  }

  private void updateTechEdit() {
    boolean enable = sel_level >= 0;

    // if nothing is select, disable and clear tech specific stuff
    txtName.setEnabled(enable);
    txtDesc.setEnabled(enable);
    txtReqResPoints.setEnabled(enable);
    cmbImageFile.setEnabled(enable);

    if (!enable) {
      txtName.setText(null);
      txtDesc.setText(null);
      txtReqResPoints.setText(null);
      lblImageIcon.setIcon(null);
    }
  }

  private void setControlsEnabled(boolean enabled) {
    if (sel_level >= 0) {
      txtName.setEnabled(enabled);
      txtDesc.setEnabled(enabled);
      txtReqResPoints.setEnabled(enabled);
      cmbImageFile.setEnabled(enabled);
    }

    cmbFields.setEnabled(enabled);
    btnFieldName.setEnabled(enabled);
    lstTechs.setEnabled(enabled);
  }

  private void initLayout() {
    GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING)
      .addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup()
        .addGroup(jPanel1Layout.createSequentialGroup()
          .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
            .addComponent(lblImageIcon, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(cmbImageFile, 0, 121, Short.MAX_VALUE))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(jPanel1Layout.createParallelGroup()
            .addGroup(jPanel1Layout.createSequentialGroup()
              .addComponent(lblReqResPoints, GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(txtReqResPoints, GroupLayout.DEFAULT_SIZE, 80, 80))
            .addGroup(jPanel1Layout.createSequentialGroup()
              .addComponent(lblTrekTechLvls, GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(spiTrekTechLvls, GroupLayout.DEFAULT_SIZE, 80, 80))))
        .addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(lblDesc, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(txtName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    jPanel1Layout.setVerticalGroup(jPanel1Layout.createSequentialGroup()
      .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
        .addGroup(jPanel1Layout.createSequentialGroup()
          .addComponent(txtName)))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(lblDesc)
      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
      .addComponent(jScrollPane2)
      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
      .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
        .addGroup(jPanel1Layout.createSequentialGroup()
          .addComponent(cmbImageFile, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
          .addComponent(lblImageIcon, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE))
        .addGroup(jPanel1Layout.createSequentialGroup()
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
            .addComponent(lblReqResPoints)
            .addComponent(txtReqResPoints, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
            .addComponent(lblTrekTechLvls)
            .addComponent(spiTrekTechLvls, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))));

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
      .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
          .addComponent(jScrollPane1)
          .addGroup(layout.createSequentialGroup()
            .addComponent(cmbFields, 0, 105, Short.MAX_VALUE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnFieldName))
          .addGroup(layout.createSequentialGroup()
            .addComponent(btnAddTech)
            .addComponent(btnRemTech)))
        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup()
          .addGroup(layout.createSequentialGroup()
            .addComponent(lblName)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnDescTools, GroupLayout.DEFAULT_SIZE, 18, 18))
          .addComponent(jPanel1))
        .addContainerGap()));
    layout.setVerticalGroup(layout.createSequentialGroup()
      .addContainerGap()
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
        .addComponent(cmbFields, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(btnFieldName)
        .addComponent(lblName)
        .addComponent(btnDescTools))
      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
          .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(btnAddTech)
            .addComponent(btnRemTech)))
        .addComponent(jPanel1))
      .addContainerGap());
  }

  private void addListeners() {
    cmbFields.addActionListener(new CBActionListener(x -> onFieldSelection(x)));
    btnFieldName.addActionListener(new CBActionListener(x -> onFieldRename(x)));
    cmbImageFile.addActionListener(new CBActionListener(x -> onImageSelection(x)));
    lstTechs.addListSelectionListener(new CBListSelectionListener(x -> onTechSelection(x)));
    btnAddTech.addActionListener(new CBActionListener(x -> onTechAdd(x)));
    btnRemTech.addActionListener(new CBActionListener(x -> onTechRemove(x)));
    btnDescTools.addActionListener(new CBActionListener(x -> onOpenTools(x)));
    spiTrekTechLvls.addChangeListener(new CBChangeListener(x -> onTrekTechLvlChange(x)));
  }

  private void onFieldRename(ActionEvent evt) throws InvalidArgumentsException {
    if (sel_field < 0)
      return;

    int i = sel_field;
    String msg = Language.getString("TechGUI.4"); //$NON-NLS-1$
    String response = JOptionPane.showInputDialog(msg, FIELD.getField(i));

    if (response != null) {
      FIELD.setField(i, response);

      cmbFields.setEnabled(false);
      cmbFields.removeAllItems();

      String[] fields = FIELD.getFields();
      for (int j = 0; j < fields.length; j++) {
        cmbFields.addItem(fields[j]);
      }

      cmbFields.setEnabled(true);
      cmbFields.setSelectedIndex(i);
    }
  }

  private void onFieldSelection(ActionEvent evt) throws InvalidArgumentsException {
    // save old
    finalWarning();

    // setup new
    sel_field = cmbFields.getSelectedIndex();
    fillTechList(true);
  }

  private void onTechSelection(ListSelectionEvent evt) throws IOException, InvalidArgumentsException {
    // skip duplicate events
    if (evt.getValueIsAdjusting())
      return;

    // save old
    if (applySelected()) {
      // update technology list for name and cost changes
      fillTechList(true);
      // skip redundant update
      return;
    }

    ID sel = lstTechs.getSelectedValue();
    sel_level = sel != null ? sel.ID : -1;
    updateTechEdit();

    // setup new
    if (sel_level >= 0) {
      val entry = TECH.getEntry(sel_field, sel_level).LEFT;
      txtName.setText(entry.getName());
      txtDesc.setText(entry.getDesc());

      TechTreeField field = TECH.field(sel_field);
      txtReqResPoints.setText(Long.toString(field.getEntry(sel_level).cost()));

      String img = field.getEntry(sel_level).image() + ".tga";
      cmbImageFile.setSelectedItem(img);
      lblImageIcon.setIcon(new ImageIcon(STBOF.getTargaImage(img, LoadFlags.UNCACHED).getImage(AlphaMode.Opaque)));
    }
  }

  private void onImageSelection(ActionEvent evt) throws IOException, KeyNotFoundException {
    if (cmbImageFile.getSelectedIndex() >= 0 && sel_field >= 0 && sel_level >= 0) {
      String img = cmbImageFile.getSelectedItem().toString();
      TechTreeField field = TECH.field(sel_field);

      Image iconImg = STBOF.getTargaImage(img, LoadFlags.UNCACHED).getImage(AlphaMode.Opaque);
      lblImageIcon.setIcon(new ImageIcon(iconImg));
      field.getEntry(sel_level).setImage(img.substring(0, img.length() - 4));
    }
  }// GEN-LAST:event_cmbImageFileActionPerformed

  private void onTechAdd(ActionEvent evt) throws InvalidArgumentsException {
    if (sel_field < 0)
      return;

    // save old
    finalWarning();

    String response = JOptionPane.showInputDialog(Language.getString("TechGUI.5"),
      Language.getString("TechGUI.6")); //$NON-NLS-1$

    if (response == null)
      return;

    String desc = response.toUpperCase() + ": " + "No description.";
    int index = TECH.add(response, (byte) sel_field, desc);

    // add description and update indices
    TECH.setDesc(index, response.toUpperCase() + ": " + "No description.");

    int lvl = TECH.getEntry(index).level();
    fillTechList(false);
    lstTechs.setSelectedValue(new ID(null, lvl), true);

    checkTrekTechLvlNum();
  }

  private void onTechRemove(ActionEvent evt) {
    if (sel_field < 0 || sel_level < 0)
      return;

    int sel = lstTechs.getSelectedIndex();
    if (sel < 0)
      return;

    TECH.removeEntry(sel_field, sel_level);
    fillTechList(false);

    // select next entry
    int techs = lstTechs.getModel().getSize();
    sel = Integer.min(sel, techs - 1);
    lstTechs.setSelectedIndex(sel);

    checkTrekTechLvlNum();
  }

  private void onOpenTools(ActionEvent x) throws KeyNotFoundException {
    if (DescTools.showToolsDialog(TECH)) {
      String name = null;
      String desc = null;
      if (sel_field >= 0 && sel_level >= 0) {
        val entry = TECH.getEntry(sel_field, sel_level).LEFT;
        name = entry.getName();
        desc = entry.getDesc();
      }
      txtName.setText(name);
      txtDesc.setText(desc);
    }
  }

  private void onTrekTechLvlChange(ChangeEvent x) throws InvalidArgumentsException {
    int lvls = ((Number) spiTrekTechLvls.getValue()).intValue();
    techLvlPatch.setValue(lvls);
    checkTrekTechLvlNum();
  }

  private void checkTrekTechLvlNum() {
    // alert trek.exe tech level mismatch by highlighting the label
    int numTL = lstTechs.getModel().getSize();
    lblTrekTechLvls.setForeground(numTL != techLvlPatch.intValue() ? ALERT_COLOR : TEXT_COLOR);
  }

  @Override
  public boolean hasChanges() {
    return FIELD.madeChanges() || TECH.madeChanges()
      || techLvlPatch != null && techLvlPatch.madeChanges()
      || STBOF.isChanged(CStbofFiles.TechDescTec);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(FIELD, changes);
    STBOF.checkChanged(TECH, changes);
    STBOF.checkChanged(CStbofFiles.TechDescTec, changes);
    if (techLvlPatch != null)
      techLvlPatch.listChangedFiles(changes);
  }

  @Override
  public void reload() throws IOException {
    FIELD = (TecField) STBOF.getInternalFile(CStbofFiles.TecFieldTec, true);
    TECH = (TechTree) STBOF.getInternalFile(CStbofFiles.TechTreeTec, true);
    loadData();
  }

  @Override
  public void finalWarning() throws InvalidArgumentsException {
    if (sel_field < 0 || sel_level < 0)
      return;

    applySelected();
    TECH.flushDescriptions();
  }

  private boolean applySelected() throws InvalidArgumentsException {
    if (sel_field < 0 || sel_level < 0)
      return false;

    boolean changed = false;
    TechTreeEntry techEntry = TECH.getEntry(sel_field, sel_level).LEFT;
    changed |= techEntry.setName(txtName.getText());
    changed |= techEntry.setDesc(txtDesc.getText());

    int points = Math.max(Integer.parseInt(txtReqResPoints.getText()), 0);
    TechTreeField field = TECH.field(sel_field);
    changed |= field.getEntry(sel_level).setCost(points);

    return changed;
  }
}
