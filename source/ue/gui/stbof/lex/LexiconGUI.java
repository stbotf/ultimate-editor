package ue.gui.stbof.lex;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.component.IconButton;
import ue.gui.util.component.ImageButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.gfx.Icons;
import ue.service.Language;
import ue.util.data.StringTools;

public class LexiconGUI extends MainPanel {

  private static final int DEFAULT_STBOF_LEC_SIZE = 1460;
  private final String strINDEX = Language.getString("LexiconGUI.2"); //$NON-NLS-1$
  private final String ASK_BOTF_LIMIT = Language.getString("LexiconGUI.5"); //$NON-NLS-1$
  private final String ASK_ENTRY_SHIFT = Language.getString("LexiconGUI.6"); //$NON-NLS-1$
  private final String NEW_ENTRY_TEXT = Language.getString("LexiconGUI.7"); //$NON-NLS-1$

  // read
  Stbof stbof;

  // edited
  private Lexicon LEC;

  // ui components
  DefaultListModel<String> listModel = new DefaultListModel<String>();
  private JList<String> LIST = new JList<String>(listModel);
  private JScrollPane scrolly = new JScrollPane(LIST);
  private JTextArea TEXT = new JTextArea(12, 30);
  private JScrollPane textScrollPane = new JScrollPane(TEXT);
  private JPanel pnlSelControl = new JPanel();
  private JPanel pnlSelFooter = new JPanel();
  private JPanel pnlSearch = new JPanel();
  private JPanel pnlCommands = new JPanel();
  private JLabel lblSelectedIndex = new JLabel();
  private JTextField txtFIND = new JTextField();

  // buttons
  private IconButton btnFindNext = IconButton.CreateSmallButton(Icons.SEARCH);
  private IconButton btnFindPrev = IconButton.CreateSmallButton(Icons.PREV);
  private ImageButton btnAdd = ImageButton.CreatePlusButton();
  private ImageButton btnRemove = ImageButton.CreateMinusButton();
  private IconButton btnImport = new IconButton(Language.getString("LexiconGUI.9"), Icons.IMPORT); //$NON-NLS-1$
  private IconButton btnExport = new IconButton(Language.getString("LexiconGUI.11"), Icons.EXPORT); //$NON-NLS-1$
  private IconButton btnNumberAll = new IconButton(Language.getString("LexiconGUI.3"), Icons.NUM); //$NON-NLS-1$

  // data
  private int INDEX = 0;  // default selection to first entry, which at least is the virtual add entry
  private boolean selectionChange = false;
  private boolean askOnBotfLimit = true;
  private boolean askOnEntryShift = true;
  private int loadedEntryCount = 0;

  public LexiconGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;
    LEC = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);

    setupControls();
    setupLayout();
    updateEntries();

    // update initial entry count to check on shifted entries
    loadedEntryCount = LEC.getNumEntries();

    updateIndexLabel();
    addActionListeners();
  }

  private void setupControls() {
    Font def = UE.SETTINGS.getDefaultFont();

    // selection scroll
    scrolly.setPreferredSize(new Dimension(210, 210));
    scrolly.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    LIST.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    LIST.setFont(def);
    // set fixed cell height for empty rows
    LIST.setFixedCellHeight(15);
    LIST.setFixedCellWidth(50);
    TEXT.setFont(def);
    lblSelectedIndex.setFont(def);

    // text box
    TEXT.setLineWrap(true);
    TEXT.setWrapStyleWord(true);

    // search & button controls
    Dimension iconDim = new Dimension(22, 18);
    txtFIND.setFont(def);
    btnFindNext.setPreferredSize(iconDim);
    btnFindPrev.setPreferredSize(iconDim);
  }

  private void setupLayout() {
    // set main layout
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    // add selection control
    pnlSelControl.setLayout(new BoxLayout(pnlSelControl, BoxLayout.Y_AXIS));
    pnlSelControl.add(scrolly);
    pnlSelFooter.setLayout(new BoxLayout(pnlSelFooter, BoxLayout.X_AXIS));
    pnlSelFooter.add(lblSelectedIndex);
    pnlSelFooter.add(Box.createHorizontalGlue());
    pnlSelFooter.add(btnAdd);
    pnlSelFooter.add(btnRemove);
    pnlSelControl.add(pnlSelFooter);
    c.gridwidth = 1;
    c.gridheight = 3;
    c.weighty = 1.0;
    c.gridx = 0;
    c.gridy = 0;
    add(pnlSelControl, c);
    // add text control
    c.weightx = 1.0;
    c.gridheight = 1;
    c.gridx = 1;
    add(textScrollPane, c);
    // add search text
    c.gridwidth = 1;
    c.weightx = 0;
    c.weighty = 0;
    c.gridy = 1;
    add(txtFIND, c);
    // add button control
    pnlSearch.setLayout(new BoxLayout(pnlSearch, BoxLayout.X_AXIS));
    pnlSearch.add(txtFIND);
    pnlSearch.add(btnFindNext);
    pnlSearch.add(btnFindPrev);
    add(pnlSearch, c);
    c.gridy = 2;
    c.gridx = 1;
    pnlCommands.setLayout(new BoxLayout(pnlCommands, BoxLayout.X_AXIS));
    pnlCommands.add(btnImport);
    pnlCommands.add(btnExport);
    pnlCommands.add(Box.createHorizontalGlue());
    pnlCommands.add(btnNumberAll);
    add(pnlCommands, c);
  }

  private void addActionListeners() {
    // add listeners
    LIST.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (LIST.getSelectedIndex() < 0) {
          return;
        }
        try {
          selectionChanged();
        } catch (Exception ex) {
          // really bad stuff happening - bail
          Dialogs.displayError(ex);
          return;
        }
      }
    });
    btnFindNext.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        findNext();
      }
    });
    btnFindPrev.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        findPrev();
      }
    });
    btnNumberAll.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        String msg = Language.getString("LexiconGUI.4"); //$NON-NLS-1$
        if (Dialogs.showConfirm(msg))
          NumberAll();
      }
    });
    btnAdd.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        int index = LIST.getSelectedIndex();
        try {
          if (askOnEntryShift && INDEX < loadedEntryCount) {
            if (!Dialogs.showConfirm(ASK_ENTRY_SHIFT))
              return;

            // remember we have asked
            askOnEntryShift = false;
          }

          LEC.addEntry(index, "");
          // update current index for the moved previous entry
          // to not detect false changes by the selection update
          INDEX = index+1;
          // adding entries raises a selection change
          // cause the selected index is getting updated
          // to keep the old entry selected
          listModel.add(index, "");
          // update selection for the new entry
          LIST.setSelectedIndex(--INDEX);
          // unset text, cause with no index change the update is skipped
          TEXT.setText("");
          // focus text area
          TEXT.requestFocusInWindow();
        }
        catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });
    btnRemove.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        int index = LIST.getSelectedIndex();

        // never remove the virtual add entry
        if (index + 1 == listModel.getSize())
          return;

        int lecCount = LEC.getNumEntries();
        boolean confirmBotfLimit = askOnBotfLimit && lecCount == DEFAULT_STBOF_LEC_SIZE;
        boolean confirmEntryShift = askOnEntryShift && INDEX + 1 < loadedEntryCount;

        if (confirmBotfLimit || confirmEntryShift) {
          String msg1 = confirmBotfLimit ? ASK_BOTF_LIMIT : null;
          String msg2 = confirmEntryShift ? ASK_ENTRY_SHIFT : null;
          String msg = StringTools.joinNotEmpty("\n", msg1, msg2);

          if (!Dialogs.showConfirm(msg))
            return;

          // remember we have asked
          if (confirmEntryShift)
            askOnEntryShift = false;
          if (confirmBotfLimit)
            askOnBotfLimit = false;
        }

        try {
          LEC.removeEntry(index);
          int entryCount = LEC.getNumEntries();

          // compute new selection index
          // when the last lexicon entry is removed, choose the previous entry index
          // if none is remaining, default to the virtual add entry
          int newIdx = INDEX < entryCount ? INDEX : entryCount > 0 ? entryCount - 1 : 0;

          // reset current selected index to not skip the selection update
          INDEX = -1;

          // remove the selected item, which not only for the last entry,
          // sometimes resets the selection index to -1 as well
          listModel.remove(index);

          // make sure to update the selection to the computed index
          LIST.setSelectedIndex(newIdx);
        }
        catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });
    btnImport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle(UE.APP_NAME + " - " + Language.getString("LexiconGUI.8")); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(false);

        int retValue = fc.showDialog(UE.WINDOW, Language.getString("LexiconGUI.9")); //$NON-NLS-1$
        if (retValue == JFileChooser.APPROVE_OPTION) {
          File file = fc.getSelectedFile();
          if (file != null) {
            ArrayList<String> lines = new ArrayList<String>();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"))){ //$NON-NLS-1$
              // read one entry per line
              while (reader.ready()) {
                String line = reader.readLine();
                // unescape any line breaks
                line = StringTools.unescapeLineBreaks(line);
                lines.add(line);
              }
            }
            catch (IOException e) {
              Dialogs.displayError(e);
            }

            LEC.setEntries(lines);
            INDEX = 0;
            updateEntries();
            LIST.ensureIndexIsVisible(INDEX);
          }
        }
      }
    });
    btnExport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle(UE.APP_NAME + " - " + Language.getString("LexiconGUI.10")); //$NON-NLS-1$
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(false);

        int retValue = fc.showDialog(UE.WINDOW, Language.getString("LexiconGUI.11")); //$NON-NLS-1$
        if (retValue == JFileChooser.APPROVE_OPTION) {
          File file = fc.getSelectedFile();
          if (file == null || file.exists() && !Dialogs.showConfirm(Language.getString("LexiconGUI.12")))
              return;

          List<String> entries = LEC.getEntries();

          try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "ISO-8859-1"))) { //$NON-NLS-1$
            for (String entry : entries) {
              // escape '\\', '\n' and '\0' characters, so each entry uses one single line
              entry = StringTools.escapeLineBreaks(entry);
              writer.write(entry);
              writer.newLine();
            }
          }
          catch (IOException e) {
            Dialogs.displayError(e);
          }
        }
      }
    });
    TEXT.getDocument().addDocumentListener(new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
          updateVirtualAdd();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
          updateVirtualAdd();
        }

        @Override
        public void changedUpdate(DocumentEvent arg0) {
        }

        private void updateVirtualAdd() {
          // skip selection changes
          if (selectionChange)
            return;

          // add a new one when the virtual add entry is edited
          // check on the listModel here to ensure it is the very last entry
          // so there aren't further ones added without being stored
          if (INDEX+1 == listModel.size()) {
            listModel.addElement(NEW_ENTRY_TEXT);
            LIST.ensureIndexIsVisible(INDEX+1);
          }
        }
    });
  }

  protected void findNext() {
    int idx = LIST.getSelectedIndex();
    String searchText = txtFIND.getText();
    idx = LEC.findNext(searchText, ++idx, true);
    if (idx >= 0) {
      LIST.setSelectedIndex(idx);
      LIST.ensureIndexIsVisible(idx);
    }
  }

  protected void findPrev() {
    int idx = LIST.getSelectedIndex();
    String searchText = txtFIND.getText();
    idx = LEC.findPrev(searchText, --idx, true);
    if (idx >= 0) {
      LIST.setSelectedIndex(idx);
      LIST.ensureIndexIsVisible(idx);
    }
  }

  /**
   * Updates the lexicon entries and cuts any entries larger than 20 characters.
   */
  private void updateEntries() {
    // add one extra element for modification
    // so we don't have to add another button for expanding the list
    List<String> entries = LEC.getEntries();
    entries.add(NEW_ENTRY_TEXT);

    // adjust selected index
    if (INDEX >= entries.size())
      INDEX = 0;

    // update current selection text
    TEXT.setText(entries.get(INDEX));

    // update the entry list
    listModel.clear();
    entries.forEach(s -> listModel.addElement(s));

    // update selection
    LIST.setSelectedIndex(INDEX);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Lexicon;
  }

  @Override
  public boolean hasChanges() {
    return LEC.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(LEC, changes);
  }

  @Override
  public void reload() throws IOException {
    LEC = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);
    // update initial entry count to check on shifted entries
    loadedEntryCount = LEC.getNumEntries();
    updateEntries();
  }

  @Override
  public void finalWarning() {
    selectionChanged();
  }

  private void selectionChanged() {
    int nextIdx = LIST.getSelectedIndex();

    // skip recursive update on updated entry list
    if (nextIdx == INDEX)
      return;

    boolean saved = false;

    // for the last element, on changes add a new entry
    if (INDEX == LEC.getNumEntries()) {
      String text = TEXT.getText();
      // the list is increased on any change to the virtual add entry
      if (listModel.size() > INDEX+1) {
        LEC.addEntry(INDEX, text);
        saved = true;
      }
    } else {
      // save previous entry
      saved = INDEX >= 0 && LEC.setEntry(INDEX, TEXT.getText());
    }

    // update index
    INDEX = nextIdx;

    // skip text edit events on update
    selectionChange = true;

    // update entry list & text
    if (saved)
      updateEntries();
    else if (INDEX == LEC.getNumEntries())
      TEXT.setText(NEW_ENTRY_TEXT);
    else if (INDEX >= 0)
      TEXT.setText(LEC.getEntry(INDEX));

    selectionChange = false;

    // update displayed index number
    updateIndexLabel();
  }

  private void updateIndexLabel() {
    lblSelectedIndex.setText(strINDEX.replace("%1", Integer.toString(INDEX))); //$NON-NLS-1$
  }

  protected void NumberAll() {
    try {
      LEC.NumberAll();
      updateEntries();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
      return;
    }
  }
}
