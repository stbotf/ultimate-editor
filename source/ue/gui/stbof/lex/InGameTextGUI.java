package ue.gui.stbof.lex;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.tdb.TextDataBase;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.file.FileFilters;

public class InGameTextGUI extends MainPanel {

  private final String strINDEX = Language.getString("InGameTextGUI.2"); //$NON-NLS-1$

  // read
  private Stbof STBOF;

  // ui components
  private JComboBox<String> cmbTDBS = new JComboBox<String>();
  private JList<String> LIST = new JList<String>();
  private JTextArea TEXT = new JTextArea(12, 30);
  private JLabel INT = new JLabel();
  private JTextField txtFIND = new JTextField();
  private JButton btnFIND = new JButton(Language.getString("InGameTextGUI.0")); //$NON-NLS-1$
  private JButton btnFINDNEXT = new JButton(Language.getString("InGameTextGUI.1")); //$NON-NLS-1$

  // data
  private int BIG_INDEX = -1;
  private int INDEX = -1;
  private int FIND_INDEX = -1;
  private ArrayList<Integer> intFIND = null;
  private ArrayList<TextDataBase> DATA = new ArrayList<>();

  public InGameTextGUI(Stbof st) throws IOException {
    STBOF = st;
    Font def = UE.SETTINGS.getDefaultFont();
    cmbTDBS.setFont(def);
    LIST.setFont(def);
    LIST.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    LIST.setFixedCellWidth(50);
    btnFIND.setFont(def);
    txtFIND.setFont(def);
    btnFINDNEXT.setFont(def);
    TEXT.setFont(def);
    TEXT.setLineWrap(true);
    TEXT.setWrapStyleWord(true);
    INT.setFont(def);
    JScrollPane scrolly = new JScrollPane(LIST);
    scrolly.setPreferredSize(new Dimension(200, 200));
    JScrollPane JSP = new JScrollPane(TEXT);

    LIST.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (BIG_INDEX < 0 || LIST.getSelectedIndex() < 0)
          return;

        try {
          finalWarning();
        } catch (Exception ex) {
          // really bad stuff happening - bail
          Dialogs.displayError(ex);
          return;
        }

        TextDataBase txt = DATA.get(BIG_INDEX);
        try {
          TEXT.setText(txt.getString(LIST.getSelectedIndex()));
        } catch (Exception ze) {
          Dialogs.displayError(ze);
        }

        INDEX = LIST.getSelectedIndex();
        INT.setText(strINDEX.replace("%1", Integer.toString(INDEX))); //$NON-NLS-1$
        int tmp = BIG_INDEX;
        BIG_INDEX = -1;
        LIST.setListData(txt.getStrings());
        LIST.setSelectedIndex(INDEX);
        BIG_INDEX = tmp;
      }
    });

    cmbTDBS.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (BIG_INDEX != -1) {
          try {
            finalWarning();
          } catch (Exception ex) {
            // really bad stuff happening - bail
            Dialogs.displayError(ex);
            return;
          }
        }

        BIG_INDEX = cmbTDBS.getSelectedIndex();
        if (BIG_INDEX >= 0) {
          TextDataBase txt = DATA.get(BIG_INDEX);
          INDEX = -1;
          TEXT.setText(""); //$NON-NLS-1$
          LIST.setListData(txt.getStrings());
          INT.setText(""); //$NON-NLS-1$
          txtFIND.setText(""); //$NON-NLS-1$
          intFIND = null;
        }
      }
    });

    btnFIND.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (BIG_INDEX == -1) {
          return;
        }
        TextDataBase txt = DATA.get(BIG_INDEX);
        String[] ser = txt.getStrings();
        String tmp = txtFIND.getText();
        intFIND = null;
        tmp = tmp.toLowerCase();
        for (int i = 0; i < ser.length; i++) {
          ser[i] = ser[i].toLowerCase();
          if (ser[i].indexOf(tmp) != -1) {
            if (intFIND == null) {
              intFIND = new ArrayList<Integer>();
            }
            intFIND.add(i);
          }
        }
        if (intFIND != null) {
          FIND_INDEX = 0;
          LIST.setSelectedIndex(intFIND.get(0));
          LIST.ensureIndexIsVisible(intFIND.get(0));
        }
      }
    });

    btnFINDNEXT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (intFIND != null) {
          if (BIG_INDEX == -1) {
            return;
          }
          FIND_INDEX += 1;
          if (intFIND.size() == FIND_INDEX) {
            FIND_INDEX = 0;
          }
          LIST.setSelectedIndex(intFIND.get(FIND_INDEX));
          LIST.ensureIndexIsVisible(intFIND.get(FIND_INDEX));
        }
      }
    });

    //lay
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridwidth = 4;
    c.gridheight = 1;
    c.gridx = 0;
    c.gridy = 0;
    add(cmbTDBS, c);
    c.gridwidth = 1;
    c.gridy = 1;
    c.weighty = 1;
    add(scrolly, c);
    c.gridx = 1;
    c.gridwidth = 3;
    c.weightx = 1;
    add(JSP, c);
    c.gridx = 0;
    c.gridy = 2;
    c.weightx = 0;
    c.weighty = 0;
    add(INT, c);
    c.gridy = 3;
    c.gridwidth = 2;
    add(txtFIND, c);
    c.gridwidth = 1;
    c.gridx = 2;
    add(btnFIND, c);
    c.gridx = 3;
    add(btnFINDNEXT, c);

    //fill
    load();

    INT.setText(strINDEX.replace("%1", Integer.toString(INDEX))); //$NON-NLS-1$
  }

  private void load() throws IOException {
    int prevTxt = INDEX;
    int prevDB = BIG_INDEX;
    BIG_INDEX = -1;
    INDEX = -1;
    FIND_INDEX = -1;
    intFIND = null;

    DATA.clear();
    cmbTDBS.removeAllItems();
    Collection<String> fileNames = STBOF.getFileNames(FileFilters.Tdb);
    ArrayList<String> errors = new ArrayList<String>();

    for (String fileName : fileNames) {
      try {
        DATA.add((TextDataBase) STBOF.getInternalFile(fileName, true));
        cmbTDBS.addItem(fileName);
      } catch (Exception ex) {
        ex.printStackTrace();
        errors.add(fileName);
      }
    }

    if (!errors.isEmpty()) {
      String msg = Language.getString("InGameTextGUI.3");
      for (String err : errors)
        msg += "\n" + err;
      Dialogs.displayError(msg);
    }

    // restore text database selection
    if (prevDB >= 0 && prevDB < fileNames.size()) {
      cmbTDBS.setSelectedIndex(prevDB);

      // restore text selection
      if (prevTxt >= 0 && prevTxt < LIST.getModel().getSize())
        LIST.setSelectedIndex(prevTxt);
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.GameText;
  }

  @Override
  public boolean hasChanges() {
    return DATA.stream().anyMatch(TextDataBase::madeChanges);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(DATA, changes);
  }

  @Override
  public void reload() throws IOException {
    load();
  }

  @Override
  public void finalWarning() throws IOException {
    if (BIG_INDEX != -1 && INDEX != -1) {
      TextDataBase txt = DATA.get(BIG_INDEX);
      txt.setString(INDEX, TEXT.getText());
    }
  }

}
