package ue.gui.stbof.map;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.est.Environ;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;

/**
 * Used to edit planet and atmosphere frequencies.
 */
public class EnvironGUI extends MainPanel {

  private final String strSUM = Language.getString("EnvironGUI.2"); //$NON-NLS-1$

  // read
  private Stbof stbof;

  // edited
  private Environ ENVIRON;

  // ui components
  private JComboBox<String> jcbTYPE = new JComboBox<String>();
  private JButton btnAPPLY = new JButton(Language.getString("EnvironGUI.0")); //$NON-NLS-1$
  private JLabel lblFREQ = new JLabel(Language.getString("EnvironGUI.1")); //$NON-NLS-1$
  private JTextField txtFREQ = new JTextField(5);
  private JLabel lblSUM = new JLabel();

  /**
   * Creates an instance of EnvironGUI.
   * @param stbof
   * @throws IOException
   */
  public EnvironGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;
    this.ENVIRON = (Environ)stbof.getInternalFile(CStbofFiles.EnvironEst, true);

    //set font
    Font def = UE.SETTINGS.getDefaultFont();
    jcbTYPE.setFont(def);
    lblFREQ.setFont(def);
    txtFREQ.setFont(def);
    btnAPPLY.setFont(def);
    lblSUM.setFont(def);
    //add listeners
    btnAPPLY.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        int indi = jcbTYPE.getSelectedIndex();
        if (indi != -1) {
          try {
            float flt = Float.parseFloat(txtFREQ.getText());
            ENVIRON.setFrequency(indi, flt);
          } catch (Exception gu) {
            try {
              float freq = ENVIRON.getFrequency(indi);
              txtFREQ.setText(Float.toString(freq));
            } catch (Exception ex) {
              Dialogs.displayError(ex);
              return;
            }
          }

          try {
            String txt = strSUM.replace("%1", Float.toString(ENVIRON.getFreqSum()));
            lblSUM.setText(txt); //$NON-NLS-1$
          } catch (Exception ex) {
            Dialogs.displayError(ex);
          }
        }
      }
    });
    jcbTYPE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        int indi = jcbTYPE.getSelectedIndex();
        if (indi != -1) {
          try {
            txtFREQ.setText(Float.toString(ENVIRON.getFrequency(indi)));
          } catch (Exception ex) {
            Dialogs.displayError(ex);
          }
        }
      }
    });
    //add items
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 3;
    c.gridheight = 1;
    add(jcbTYPE, c);
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 2;
    add(lblFREQ, c);
    c.gridx = 2;
    c.gridwidth = 1;
    add(txtFREQ, c);
    c.gridx = 3;
    add(btnAPPLY, c);
    c.gridx = 0;
    c.gridy = 2;
    add(lblSUM, c);
    fillList();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Environments;
  }

  //fill the type combo
  private void fillList() {
    int prev = jcbTYPE.getSelectedIndex();
    jcbTYPE.removeAllItems();
    for (short i = 0; i < ENVIRON.numEntries(); ++i) {
      jcbTYPE.addItem(ENVIRON.getName(i));
    }

    if (prev >= 0 && prev < ENVIRON.numEntries())
      jcbTYPE.setSelectedIndex(prev);

    lblSUM.setText(strSUM.replace("%1", Float.toString(ENVIRON.getFreqSum()))); //$NON-NLS-1$
  }

  @Override
  public boolean hasChanges() {
    return ENVIRON.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(ENVIRON, changes);
  }

  @Override
  public void reload() throws IOException {
    ENVIRON = (Environ) stbof.getInternalFile(CStbofFiles.EnvironEst, true);
    fillList();
  }

  /*this is used to apply recent changes before panel is removed
  no need for it in this case tho*/
  @Override
  public void finalWarning() {
  }
}
