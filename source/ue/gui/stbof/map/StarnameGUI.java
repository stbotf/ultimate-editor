package ue.gui.stbof.map;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileFilter;
import lombok.val;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Starname;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.file.PathHelper;

public class StarnameGUI extends MainPanel {

  // read
  private Stbof stbof;

  // edited
  private Starname STARS;

  // ui components
  private JList<String> NAMELIST = new JList<String>();
  private JButton btnAdd = new JButton(Language.getString("StarnameGUI.0")); //$NON-NLS-1$
  private JButton btnEdit = new JButton(Language.getString("StarnameGUI.1")); //$NON-NLS-1$
  private JButton btnRemove = new JButton(Language.getString("StarnameGUI.2")); //$NON-NLS-1$
  private JButton btnExport = new JButton("Export");
  private JButton btnImport = new JButton("Import");
  private JLabel lblSum = new JLabel();

  public StarnameGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;
    STARS = (Starname) stbof.getInternalFile(CStbofFiles.StarNameBin, true);

    //set font
    Font def = UE.SETTINGS.getDefaultFont();
    btnAdd.setFont(def);
    btnEdit.setFont(def);
    btnRemove.setFont(def);
    btnImport.setFont(def);
    btnExport.setFont(def);
    lblSum.setFont(def);
    NAMELIST.setFont(def);

    //add listeners
    btnAdd.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String n = JOptionPane.showInputDialog(Language.getString("StarnameGUI.3"),
            Language.getString("StarnameGUI.4")); //$NON-NLS-1$ //$NON-NLS-2$

        if (n != null) {
          STARS.add(n);
          updateData();
        }
      }
    });

    btnEdit.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (NAMELIST.getSelectedIndex() != -1) {
          String n = JOptionPane.showInputDialog(Language.getString("StarnameGUI.5"),
              NAMELIST.getSelectedValue()); //$NON-NLS-1$
          if (n != null) {
            STARS.set(NAMELIST.getSelectedValue(), n);
            updateData();
          }
        }
      }
    });

    btnRemove.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (NAMELIST.getSelectedIndex() != -1) {
          STARS.remove(NAMELIST.getSelectedValue());
          updateData();
        }
      }
    });

    btnImport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser jfc = new JFileChooser(new File(
          UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));

        FileFilter filter = new FileFilter() {
          @Override
          public boolean accept(File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".txt");
          }

          @Override
          public String getDescription() {
            return "*.txt"; //$NON-NLS-1$
          }
        };
        jfc.setFileFilter(filter);
        jfc.setApproveButtonText("Import");
        jfc.setDialogTitle("Import list of names...");
        jfc.setMultiSelectionEnabled(false);

        int returnVal = jfc.showOpenDialog(UE.WINDOW);
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File file = jfc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(file).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
          String str = in.readLine();

          while (str != null) {
            str = str.trim();
            if (str.length() > 0) {
              STARS.add(str);
            }
            str = in.readLine();
          }
        } catch (IOException ex) {
          Dialogs.displayError(ex);
        }

        updateData();
      }
    });
    btnExport.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser jfc = new JFileChooser(new File(
          UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));

        FileFilter filter = new FileFilter() {
          @Override
          public boolean accept(File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".txt");
          }

          @Override
          public String getDescription() {
            return "*.txt"; //$NON-NLS-1$
          }
        };
        jfc.setFileFilter(filter);
        jfc.setApproveButtonText("Export");
        jfc.setDialogTitle("Export list of names...");
        jfc.setMultiSelectionEnabled(false);

        int returnVal = jfc.showSaveDialog(UE.WINDOW);
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File file = jfc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(file).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        if (!file.getName().toLowerCase().endsWith(".txt"))
          file = new File(workPath, file.getName() + ".txt");

        if (file.exists() && !Dialogs.confirmOverwrite(file.getName()))
          return;

        try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
          val names = STARS.entries();

          for (String name : names) {
            out.write(name);
            out.newLine();
          }
        } catch (IOException ex) {
          Dialogs.displayError(ex);
        }
      }
    });

    //misc
    NAMELIST.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    updateData();

    //add items
    JScrollPane scrolly = new JScrollPane(NAMELIST);
    scrolly.setPreferredSize(new Dimension(200, 200));
    JPanel pnlCenter = new JPanel(new BorderLayout());
    pnlCenter.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnlCenter.add(scrolly, BorderLayout.CENTER);

    JPanel pnlOptions = new JPanel(new GridLayout(0, 1, 5, 5));
    pnlOptions.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnlOptions.add(btnAdd);
    pnlOptions.add(btnEdit);
    pnlOptions.add(btnRemove);
    pnlOptions.add(btnImport);
    pnlOptions.add(btnExport);

    JPanel pnlEast = new JPanel(new BorderLayout());
    pnlEast.add(pnlOptions, BorderLayout.NORTH);

    lblSum.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    setLayout(new BorderLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    add(pnlCenter, BorderLayout.CENTER);
    add(pnlEast, BorderLayout.EAST);
    add(lblSum, BorderLayout.SOUTH);

    updateSumString();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.StarNames;
  }

  /*this is used to apply recent changes before panel is removed
  no need for it in this case tho*/
  @Override
  public void finalWarning() {
    // nothing to do
  }

  private void updateData() {
    String sel = NAMELIST.getSelectedValue();
    NAMELIST.setListData(STARS.entries().toArray(new String[0]));
    if (sel != null)
      NAMELIST.setSelectedValue(sel, true);

    updateSumString();
  }

  private void updateSumString() {
    lblSum.setText("Star names: %1".replace("%1", Integer.toString(STARS.count())));
  }

  @Override
  public boolean hasChanges() {
    return STARS.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(STARS, changes);
  }

  @Override
  public void reload() throws IOException {
    STARS = (Starname) stbof.getInternalFile(CStbofFiles.StarNameBin, true);
    updateData();
  }
}
