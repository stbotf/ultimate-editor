package ue.gui.stbof.map;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.ListSelectionEvent;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_StellarTypes;
import ue.edit.exe.trek.seg.map.StellTypeDetailsDescMap;
import ue.edit.exe.trek.seg.map.StellTypeDetailsNameMap;
import ue.edit.exe.trek.seg.map.StellTypeDetailsNameMumFix;
import ue.edit.exe.trek.seg.map.StellTypeGen;
import ue.edit.exe.trek.seg.map.StellTypeGenGALMFix;
import ue.edit.exe.trek.seg.map.StellTypeTooltipGALMFix;
import ue.edit.exe.trek.seg.map.StellTypeTooltipGALMFix2;
import ue.edit.exe.trek.seg.map.StellTypeTooltipNameMap;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.value.ByteValue;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.util.component.ImageButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.gui.util.event.CBDocumentChangeListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.util.file.FileFilters;

// Objstruc lists the stellar objects of objstruc.smt.
public class ObjstrucGUI extends MainPanel {

  private final String strSUM = Language.getString("ObjstrucGUI.3"); //$NON-NLS-1$
  private final String strNoTrek = Language.getString("ObjstrucGUI.2"); //$NON-NLS-1$
  private final String strDefault = Language.getString("ObjstrucGUI.6"); //$NON-NLS-1$

  // read
  private Stbof stbof;
  private Trek trek;
  private StbofFileInterface stif;

  // edited
  private Objstruc OBJECT;
  private Lexicon lex;

  private StellTypeGen stellTypeGen;
  private StellTypeGenGALMFix genGALMFix;
  private StellTypeTooltipGALMFix tooltipGALMFix;
  private StellTypeTooltipGALMFix2 tooltipGALMFix2;
  private InternalSegment[] lastNameIds;
  private StellTypeTooltipNameMap tooltipNameMap;
  private StellTypeDetailsNameMumFix detailsNameMumFix;
  private StellTypeDetailsNameMap detailsNameMap;
  private StellTypeDetailsDescMap detailsDescMap;

  // ui components
  private DefaultListModel<String> lstModel = new DefaultListModel<String>();
  private JList<String> lstSpaceObj = new JList<String>(lstModel);
  private ImageButton btnADD = ImageButton.CreatePlusButton();
  private ImageButton btnREMOVE = ImageButton.CreateMinusButton();
  private JSpinner spiNameId = null; //lexicon id for the name
  private JSpinner spiDescId = null; //lexicon id for the description
  private JCheckBox chkSystem = new JCheckBox(Language.getString("ObjstrucGUI.7")); //$NON-NLS-1$
  private JComboBox<String> cobGfx = new JComboBox<String>(); //graphic file (e.g. blkh)
  private AniLabel aniStell = new AniLabel();
  private JTextField txtFreq = new JTextField(10); //frequency
  private JTextField txtName = new JTextField(10);
  private JTextArea txtDesc = new JTextArea(8, 10);
  private JLabel lblNameId = new JLabel(Language.getString("ObjstrucGUI.4")); //$NON-NLS-1$
  private JLabel lblDescId = new JLabel(Language.getString("ObjstrucGUI.5")); //$NON-NLS-1$
  private JLabel lblGfxFiles = new JLabel(Language.getString("ObjstrucGUI.0")); //$NON-NLS-1$
  private JTextArea  txtUsedFiles = new JTextArea(); //listed graphic files used
  private JLabel lblFreq = new JLabel(Language.getString("ObjstrucGUI.1")); //$NON-NLS-1$
  private JLabel lblSum = new JLabel();

  // data
  private int       INDEX = -1;
  private boolean   selectionChange = false;
  private boolean   keepValues = false;

  public ObjstrucGUI(Stbof stbof, Trek trek) throws IOException {
    this.stbof = stbof;
    this.trek = trek;
    stif = stbof.files();
    lex = stbof.files().lexicon();
    OBJECT = (Objstruc) stbof.getInternalFile(CStbofFiles.ObjStrucSmt, true);

    if (trek != null) {
      stellTypeGen = (StellTypeGen) trek.getSegment(SD_StellarTypes.Gen, true);
      // load GALM JGE patch for sneaked auto-fixture
      genGALMFix = (StellTypeGenGALMFix) trek.getSegment(SD_StellarTypes.GenGALMFix, true);
      // load GALM CALL patch for sneaked auto-fixture
      tooltipGALMFix = (StellTypeTooltipGALMFix) trek.getSegment(SD_StellarTypes.TooltipGALMFix, true);
      tooltipGALMFix2 = (StellTypeTooltipGALMFix2) trek.getSegment(SD_StellarTypes.TooltipGALMFix2, true);
      lastNameIds = trek.getSegments(SD_StellarTypes.LastNameIds, true);
      tooltipNameMap = (StellTypeTooltipNameMap) trek.getSegment(StellTypeTooltipNameMap.SEGMENT_DEFINITION, true);
      // load MUM details panel name mappings for sneaked auto-fixture
      detailsNameMumFix = (StellTypeDetailsNameMumFix) trek.getSegment(SD_StellarTypes.DetailsNameMumFix, true);
      detailsNameMap = (StellTypeDetailsNameMap) trek.getSegment(StellTypeDetailsNameMap.SEGMENT_DEFINITION, true);
      detailsDescMap = (StellTypeDetailsDescMap) trek.getSegment(StellTypeDetailsDescMap.SEGMENT_DEFINITION, true);
    }

    // set up
    setupComponents();
    setupLayout();
    addActionListeners();

    // load
    reload();
  }

  private void setupComponents() {
    //set font
    Font def = UE.SETTINGS.getDefaultFont();
    lstSpaceObj.setFont(def);
    chkSystem.setFont(def);
    cobGfx.setFont(def);
    txtFreq.setFont(def);
    txtName.setFont(def);
    txtDesc.setFont(def);
    lblNameId.setFont(def);
    lblDescId.setFont(def);
    lblGfxFiles.setFont(def);
    txtUsedFiles.setFont(def);
    lblFreq.setFont(def);
    lblSum.setFont(def);

    chkSystem.setToolTipText(Language.getString("ObjstrucGUI.8")); //$NON-NLS-1$
    txtDesc.setLineWrap(true);
    txtDesc.setWrapStyleWord(true);

    cobGfx.setEditable(true);
    aniStell.setBackground(Color.BLACK);
    aniStell.setOpaque(true);
    aniStell.setForceFirstPixelAlpha(true);
    txtUsedFiles.setEditable(false);
    txtUsedFiles.setLineWrap(true);
    txtUsedFiles.setWrapStyleWord(true);
    txtUsedFiles.setBackground(getBackground());
    txtUsedFiles.setText("( *.ani, *map.tga, *maps.tga )"); //$NON-NLS-1$

    // list of buildings
    lstSpaceObj.setFixedCellWidth(50);
    lstSpaceObj.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    int maxLexId = lex.getNumEntries() - 1;
    spiNameId = new JSpinner(new SpinnerNumberModel(0, -1, maxLexId, 1));
    spiDescId = new JSpinner(new SpinnerNumberModel(0, -1, maxLexId, 1));

    if (trek == null) {
      spiNameId.setEnabled(false);
      spiDescId.setEnabled(false);
      chkSystem.setEnabled(false);
      txtName.setEnabled(false);
      txtDesc.setEnabled(false);
      txtName.setText(strNoTrek);
      txtDesc.setText(strNoTrek);
    }
  }

  private void setupLayout() {
    // Buttons panel
    JPanel pnlButtons = new JPanel();
    pnlButtons.setLayout(new BoxLayout(pnlButtons, BoxLayout.X_AXIS));
    {
      pnlButtons.add(Box.createHorizontalGlue());
      pnlButtons.add(btnADD);
      pnlButtons.add(btnREMOVE);
    }

    // left panel
    JPanel pnlLEFT = new JPanel(new GridBagLayout());
    {
      JScrollPane JSP = new JScrollPane(lstSpaceObj);
      JSP.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1;
      c.weighty = 1;
      pnlLEFT.add(JSP, c);
      c.insets.top = 0;
      c.weighty = 0;
      c.gridy++;
      pnlLEFT.add(pnlButtons, c);
    }

    // details
    JPanel pnlMAIN = new JPanel(new GridBagLayout());
    {
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      pnlMAIN.add(lblNameId, c);
      c.gridx = 5;
      pnlMAIN.add(chkSystem, c);
      c.gridx = 6;
      pnlMAIN.add(spiNameId, c);
      c.insets.top = 0;
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 7;
      pnlMAIN.add(txtName, c);
      c.insets.top = 5;
      c.gridy++;
      c.gridwidth = 1;
      pnlMAIN.add(lblDescId, c);
      c.gridx = 6;
      pnlMAIN.add(spiDescId, c);
      c.insets.top = 0;
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 7;
      // fixture to prevent description text box from flickering
      pnlMAIN.add(Box.createVerticalStrut(120), c);
      pnlMAIN.add(txtDesc, c);
      c.insets.top = 10;
      c.insets.bottom = 2;
      c.gridy++;
      c.gridwidth = 1;
      pnlMAIN.add(lblGfxFiles, c);
      c.insets.top = 5;
      c.gridy++;
      c.gridwidth = 2;
      pnlMAIN.add(cobGfx, c);
      c.insets.bottom = 5;
      c.insets.top = 0;
      c.gridy++;
      pnlMAIN.add(txtUsedFiles, c);
      c.insets.top = 10;
      c.insets.bottom = 2;
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 1;
      pnlMAIN.add(lblFreq, c);
      c.insets.top = 5;
      c.gridy++;
      pnlMAIN.add(txtFreq, c);
      c.gridx++;
      pnlMAIN.add(lblSum, c);
      c.gridx = 3;
      c.gridy = 4;
      c.gridwidth = 4;
      c.gridheight = 6;
      c.weightx = 1;
      c.weighty = 1;
      pnlMAIN.add(aniStell, c);
      c.gridwidth = 1;
      pnlMAIN.add(Box.createHorizontalGlue(), c);
    }

    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weighty = 1;
    add(pnlLEFT, c);
    add(Box.createHorizontalStrut(150), c);
    c.gridx++;
    c.weightx = 1;
    add(pnlMAIN, c);
  }

  private void addActionListeners() {
    btnADD.addActionListener(new CBActionListener(x -> addSpaceObj()));
    btnREMOVE.addActionListener(new CBActionListener(x -> removeSpaceObj()));
    txtFreq.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateSum()));
    lstSpaceObj.addListSelectionListener(new CBListSelectionListener(ObjstrucGUI.this::onSelectionChange));

    spiNameId.addChangeListener(new CBChangeListener(x -> {
      // skip reload updates
      if (!selectionChange)
        reloadName();
    }));

    spiDescId.addChangeListener(new CBChangeListener(x -> {
      // skip reload updates
      if (!selectionChange)
        reloadDesc();
    }));

    txtName.getDocument().addDocumentListener(new CBDocumentChangeListener(ObjstrucGUI.this::onNameChange));

    //animation
    cobGfx.addActionListener(new CBActionListener(x -> {
      if (INDEX != -1) {
        try {
          String gfxName = (String) cobGfx.getSelectedItem();
          AniOrCur aniFile = (AniOrCur) stbof.tryGetInternalFile(gfxName + ".ani", LoadFlags.CACHE);
          if (aniFile != null) {
            aniStell.setAnimation(aniFile, true);
          } else {
            // for the nebula display the nebula.tga
            TargaImage tga = stbof.getTargaImage(gfxName + ".tga", LoadFlags.CACHE);
            aniStell.setImage(tga.getImage(AlphaMode.Opaque));
          }
        } catch (Exception ze) {
          // unset missing animation
          aniStell.setAnimation(null, true);
        }
      }
    }));
  }

  private void loadSelected() {
    String graphic = "";
    String freq = "";

    if (INDEX >= 0) {
      // disable spinner change event handling
      selectionChange = true;

      try {
        // graphics
        graphic = OBJECT.getGraph(INDEX);
        cobGfx.setSelectedItem(graphic);

        // frequency
        freq = Float.toString(OBJECT.getFreq(INDEX));

        if (trek != null) {
          // name
          int nameId = tooltipNameMap.getLexId(INDEX);
          spiNameId.getModel().setValue(nameId);
          // for reload button enforce to reload
          // even when the lexicon id is same
          reloadName();

          boolean isSystem = stellTypeGen.isSystem(INDEX);
          chkSystem.setSelected(isSystem);

          // description
          int descId = detailsDescMap.getLexId(INDEX);
          spiDescId.getModel().setValue(descId);
          // for reload button enforce to reload
          // even when the lexicon id is same
          reloadDesc();
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
      }

      selectionChange = false;
    }

    cobGfx.setSelectedItem(graphic);
    txtFreq.setText(freq);
  }

  private void reloadName() {
    if (trek == null) {
      txtName.setText(strNoTrek);
      return;
    }

    int nameId = (Integer) spiNameId.getModel().getValue();
    String name = nameId < 0 ? strDefault : lex.getEntry(nameId);
    txtName.setEnabled(nameId >= 0);
    txtName.setText(name);
  }

  private void reloadDesc() {
    if (trek == null) {
      txtDesc.setText(strNoTrek);
      return;
    }

    int descId = (Integer) spiDescId.getModel().getValue();
    String desc = descId < 0 ? strDefault : lex.getEntry(descId);
    txtDesc.setEnabled(descId >= 0);
    txtDesc.setText(desc);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.StellarObj;
  }

  @Override
  public boolean hasChanges() {
    if (trek != null) {
      if (stellTypeGen.madeChanges()
        || genGALMFix.madeChanges()
        || tooltipGALMFix.madeChanges()
        || tooltipGALMFix2.madeChanges()
        || tooltipNameMap.madeChanges()
        || detailsNameMumFix.madeChanges()
        || detailsNameMap.madeChanges()
        || detailsDescMap.madeChanges())
        return true;

      if (Arrays.stream(lastNameIds).anyMatch(x -> x.madeChanges()))
        return true;
    }
    return OBJECT.madeChanges() || lex.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(OBJECT, changes);
    stbof.checkChanged(lex, changes);

    if (trek != null) {
      for (val seg : lastNameIds)
        trek.checkChanged(seg, changes);

      trek.checkChanged(stellTypeGen, changes);
      trek.checkChanged(genGALMFix, changes);
      trek.checkChanged(tooltipGALMFix, changes);
      trek.checkChanged(tooltipGALMFix2, changes);
      trek.checkChanged(tooltipNameMap, changes);
      trek.checkChanged(detailsNameMumFix, changes);
      trek.checkChanged(detailsNameMap, changes);
      trek.checkChanged(detailsDescMap, changes);
    }
  }

  @Override
  public void reload() throws IOException {
    // reset selected index to discard current changes on reload
    int sel = INDEX;
    INDEX = -1;

    // reload stbof.res files
    lex = stbof.files().lexicon();
    OBJECT = (Objstruc) stbof.getInternalFile(CStbofFiles.ObjStrucSmt, true);

    // reload trek segments
    if (trek != null) {
      tooltipNameMap = (StellTypeTooltipNameMap) trek.getSegment(StellTypeTooltipNameMap.SEGMENT_DEFINITION, true);
      detailsNameMap = (StellTypeDetailsNameMap) trek.getSegment(StellTypeDetailsNameMap.SEGMENT_DEFINITION, true);
      detailsDescMap = (StellTypeDetailsDescMap) trek.getSegment(StellTypeDetailsDescMap.SEGMENT_DEFINITION, true);
    }

    // reload animations
    cobGfx.removeAllItems();
    Collection<String> anis = stbof.getFileNames(FileFilters.Ani);
    for (String ani : anis)
      cobGfx.addItem(ani.substring(0, ani.length() - 4));

    // reload objstruc.smt entries
    int num = OBJECT.numberOfEntries();
    lstModel.clear();
    lstModel.ensureCapacity(num);

    btnADD.setEnabled(num < SD_StellarTypes.MAX_NUM);
    btnREMOVE.setEnabled(num > 0);

    for (int i = 0; i < num; ++i) {
      String graph = OBJECT.getGraph(i);
      lstModel.addElement(stif.stellarTools().mapStellarType(i, graph));
    }

    // restore selection
    int idx = sel >= 0 && sel < num ? sel : 0;
    lstSpaceObj.setSelectedIndex(idx);
  }

  @Override
  public void finalWarning() throws InvalidArgumentsException {
    applyChanges();

    if (trek != null) {
      // unpatched vanilla names and descriptions are limited to 12 entries
      // further the star names and descriptions 4-9 are defaulted
      // therefore auto-patch if exceeded or any of the star entries got changed
      boolean exceeded = lstModel.getSize() > SD_StellarTypes.DEFAULT_NUM;
      if (exceeded)
        tooltipNameMap.setExtended(true);
      if (exceeded || detailsNameMap.starDefaultsChanged())
        detailsNameMap.setExtended(true);
      if (exceeded || detailsDescMap.starDefaultsChanged())
        detailsDescMap.setExtended(true);

      // update number of stellar types
      int numTypes = lstModel.getSize();

      for (val seg : lastNameIds)
        ((ByteValue)seg).setValue(numTypes-1);

      stellTypeGen.updateExtended(numTypes);
      tooltipNameMap.setNumStellTypes(numTypes);
      detailsDescMap.setNumStellTypes(numTypes);
    }
  }

  private void onSelectionChange(ListSelectionEvent e) throws InvalidArgumentsException {
    if (e.getValueIsAdjusting())
      return;

    applyChanges();
    INDEX = lstSpaceObj.getSelectedIndex();

    if (!keepValues)
      loadSelected();
  }

  private void onNameChange(DocumentEvent x) {
    if (!selectionChange)
      lstModel.set(INDEX, txtName.getText());
  }

  private void applyChanges() throws InvalidArgumentsException {
    if (INDEX > -1) {
      float freq = Float.parseFloat(txtFreq.getText());
      OBJECT.setFreq(INDEX, freq);
      String gfx = (String) cobGfx.getSelectedItem();
      OBJECT.setGraph(INDEX, gfx);

      if (trek != null) {
        int nameId = (Integer)spiNameId.getValue();
        tooltipNameMap.setLexId(INDEX, nameId);

        boolean isSystem = chkSystem.isSelected();
        stellTypeGen.setSystem(INDEX, isSystem);
        detailsNameMap.setLexId(INDEX, isSystem ? -1 : nameId);
        if (nameId >= 0)
          lex.setEntry(nameId, txtName.getText());

        int descId = (Integer)spiDescId.getValue();
        detailsDescMap.setLexId(INDEX, descId);
        if (descId >= 0)
          lex.setEntry(descId, txtDesc.getText());
      }
    }
  }

  private void addSpaceObj() {
    int idx = lstModel.size();

    // copy current name if any
    String name = txtName.getText();
    if (name == null || name.equals(strDefault))
      name = stif.stellarTools().mapStellarType(idx, (String)cobGfx.getSelectedItem());

    // append to the list
    OBJECT.add((short)idx);
    lstModel.add(idx, name);

    // enable remove button
    btnREMOVE.setEnabled(true);

    // if max is reached, disable add button
    if (idx + 1 >= SD_StellarTypes.MAX_NUM)
      btnADD.setEnabled(false);

    // keep values for copy
    keepValues = true;

    try {
      // select new entry
      lstSpaceObj.setSelectedIndex(idx);
    } finally {
      keepValues = false;
    }
  }

  private void removeSpaceObj() {
    int sel = lstSpaceObj.getSelectedIndex();
    if (sel < 0)
      return;

    // reset selection index
    // to skip apply changes on selection change
    INDEX = -1;

    OBJECT.remove((short)sel);
    lstModel.remove(sel);
    tooltipNameMap.remove(sel);
    detailsNameMap.remove(sel);
    detailsDescMap.remove(sel);

    // enable add button
    int num = lstModel.size();
    btnADD.setEnabled(num < SD_StellarTypes.MAX_NUM);

    // if min is reached, disable remove button
    if (num <= 0)
      btnREMOVE.setEnabled(false);

    // select next entry
    int idx = sel >= lstModel.getSize() ? sel - 1 : sel;
    lstSpaceObj.setSelectedIndex(idx);
  }

  private void updateSum() {
    float sum = 0;

    if (INDEX > -1) {
      try {
        // compute diff first to resolve float rounding issues
        float freq = Float.parseFloat(txtFreq.getText());
        float diff = freq - OBJECT.getFreq(INDEX);
        sum = OBJECT.allFreq() + diff;
      } catch (NumberFormatException e) {
        sum = Float.NaN;
      }
    }

    lblSum.setText(strSUM.replace("%1", Float.toString(sum))); //$NON-NLS-1$
  }
}
