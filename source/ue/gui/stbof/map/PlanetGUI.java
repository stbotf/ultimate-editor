package ue.gui.stbof.map;

//standard

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.util.Collection;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.bin.Meplanet;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.pst.Planet;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.file.FileFilters;

public class PlanetGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private final String strMAX_POP = Language.getString("PlanetGUI.2"); //$NON-NLS-1$
  private final String strBONUS = Language.getString("PlanetGUI.3"); //$NON-NLS-1$
  private final String strINDEX = Language.getString("PlanetGUI.4"); //$NON-NLS-1$

  // read
  private Stbof STBOF;
  private Planspac SPACE;
  private Planboni BONI;
  private RaceRst RACE;
  private Environ ENVIRONMENT;

  // edited
  private Planet planetPst;
  private Meplanet MEPLANET;

  // ui components
  private JLabel[] LABEL = new JLabel[6];
  private JButton btnEDIT = new JButton(Language.getString("PlanetGUI.0")); //$NON-NLS-1$
  private JButton btnADD = new JButton("Add");
  private JButton btnREMOVE = new JButton("Remove");
  private JComboBox<ID> jcbMASK = new JComboBox<ID>();
  private JComboBox<String> jcbANI = new JComboBox<String>();
  private JComboBox<ID> jcbTYPE = new JComboBox<ID>();
  private JComboBox<String> jcbSIZE = new JComboBox<String>();
  private Vector<JComboBox<String>> jcbBONUS = new Vector<JComboBox<String>>(3);
  private AniLabel alANIM = new AniLabel();
  private JLabel jlPOP = new JLabel();
  private JLabel jlBONI = new JLabel();
  private JLabel jlINDEX = new JLabel();
  private JTextField txtNAME = new JTextField(12);
  // For the listed planet.pst planets, the terraform points are all zero.
  // Since they are valid fields, list them anyway.
  private JTextField txtTerraformPoints = new JTextField(12);
  private JTextField txtTerraformed = new JTextField(12);
  private JList<ID> lstPLANET = new JList<ID>();
  private JComboBox<ID> jcbATMO = new JComboBox<ID>();

  // data
  private short SELECTED = -1;
  private boolean PAUSE = false;

  /**
   * constructor
   * @throws IOException
   */
  public PlanetGUI(Stbof stbof) throws IOException {
    STBOF = stbof;
    BONI = (Planboni) stbof.getInternalFile(CStbofFiles.PlanBoniBin, true);
    ENVIRONMENT = (Environ) stbof.getInternalFile(CStbofFiles.EnvironEst, true);
    MEPLANET = (Meplanet) stbof.getInternalFile(CStbofFiles.MEPlanetBin, true);
    planetPst = (Planet) stbof.getInternalFile(CStbofFiles.PlanetPst, true);
    RACE = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    SPACE = (Planspac) stbof.getInternalFile(CStbofFiles.PlanSpacBin, true);

    //build elements
    LABEL[0] = new JLabel(Language.getString("PlanetGUI.5")); //$NON-NLS-1$
    LABEL[1] = new JLabel(Language.getString("PlanetGUI.6")); //$NON-NLS-1$
    LABEL[2] = new JLabel(Language.getString("PlanetGUI.7")); //$NON-NLS-1$
    LABEL[3] = new JLabel(Language.getString("PlanetGUI.8")); //$NON-NLS-1$
    LABEL[4] = new JLabel(Language.getString("PlanetGUI.9")); //$NON-NLS-1$
    LABEL[5] = new JLabel(Language.getString("PlanetGUI.10")); //$NON-NLS-1$
    jcbBONUS.add(new JComboBox<String>());
    jcbBONUS.add(new JComboBox<String>());
    jcbBONUS.add(new JComboBox<String>());

    //set font
    Font def = UE.SETTINGS.getDefaultFont();
    for (int i = 0; i < 6; i++) {
      LABEL[i].setFont(def);
      if (i < 3) {
        jcbBONUS.elementAt(i).setFont(def);
      }
    }
    btnEDIT.setFont(def);
    jcbMASK.setFont(def);
    jcbANI.setFont(def);
    jcbTYPE.setFont(def);
    jcbSIZE.setFont(def);
    jlPOP.setFont(def);
    jlBONI.setFont(def);
    jlINDEX.setFont(def);
    lstPLANET.setFont(def);
    txtNAME.setFont(def);
    txtTerraformPoints.setFont(def);
    txtTerraformed.setFont(def);
    jcbATMO.setFont(def);
    btnADD.setFont(def);
    btnREMOVE.setFont(def);

    alANIM.setForceFirstPixelAlpha(true);

    //add listeners
    jcbANI.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1) {
        try {
          int aniSel = jcbANI.getSelectedIndex();
          if (aniSel == -1)
            return;

          int index = planetPst.getIndex(SELECTED);
          val planet = planetPst.getPlanet(index);
          planet.setAnimation((String) jcbANI.getSelectedItem());
          jcbANI.setSelectedItem(planet.getAnimation());
          AniOrCur ani = STBOF.getAnimation(planet.getAnimation(), LoadFlags.CACHE);
          alANIM.setAnimation(ani, true);
        } catch (Exception fnf) {
          Dialogs.displayError(fnf);
          alANIM.setAnimation(null, false);
        }
      }
    }));

    jcbTYPE.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1) {
        // apply planet name changes
        finalWarning();

        int planetType = ((ID)jcbTYPE.getSelectedItem()).ID;
        if (planetType == -1)
          return;

        int index = planetPst.getIndex(SELECTED);
        val planet = planetPst.getPlanet(index);
        if (planet.getPlanetType() != planetType) {
          planet.setPlanetType(planetType);
          planetPst.sortEntries();
          fillPlanets();
        }

        // update max population selection
        int maxPop = SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
        jlPOP.setText(strMAX_POP.replace("%1", Integer.toString(maxPop))); //$NON-NLS-1$
        // update max bonus selection
        float bonus = BONI.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());
        jlBONI.setText(strBONUS.replace("%1", Integer.toString((int) bonus * 5))); //$NON-NLS-1$
      }
    }));

    jcbATMO.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1) {
        // apply planet name changes
        finalWarning();

        int atmo = ((ID)jcbATMO.getSelectedItem()).ID;
        if (atmo == -1)
          return;

        int index = planetPst.getIndex(SELECTED);
        val planet = planetPst.getPlanet(index);
        if (planet.getAtmosphere() != atmo) {
          planet.setAtmosphere(atmo);
          planetPst.sortEntries();
          fillPlanets();
        }
      }
    }));

    jcbSIZE.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1) {
        // apply planet name changes
        finalWarning();

        short planetSize = (short) jcbSIZE.getSelectedIndex();
        if (planetSize == -1)
          return;

        int index = planetPst.getIndex(SELECTED);
        val planet = planetPst.getPlanet(index);
        if (planet.getPlanetSize() != planetSize) {
          planet.setPlanetSize(planetSize);
          planetPst.sortEntries();
          fillPlanets();
        }

        // update max population selection
        int maxPop = SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
        jlPOP.setText(strMAX_POP.replace("%1", Integer.toString(maxPop))); //$NON-NLS-1$
        // update max bonus selection
        float bonus = BONI.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());
        jlBONI.setText(strBONUS.replace("%1", Integer.toString((int) bonus * 5))); //$NON-NLS-1$
      }
    }));

    jcbBONUS.elementAt(0).addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1) {
        JComboBox<String> combobox = jcbBONUS.elementAt(0);
        int index = planetPst.getIndex(SELECTED);
        byte bonusLvl = (byte) combobox.getSelectedIndex();
        if (bonusLvl == -1)
          return;

        val planet = planetPst.getPlanet(index);
        if (planet.getFoodBonusLvl() != bonusLvl) {
          planet.setFoodBonusLvl(bonusLvl);
          planetPst.sortEntries();
          fillPlanets();
        }

        // update max population selection
        int pot = SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
        jlPOP.setText(strMAX_POP.replace("%1", Integer.toString(pot))); //$NON-NLS-1$
        // update max bonus selection
        float pov = BONI.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());
        jlBONI.setText(strBONUS.replace("%1", Integer.toString((int) pov * 5))); //$NON-NLS-1$
      }
    }));

    jcbBONUS.elementAt(1).addActionListener(new CBActionListener(evt -> {
      if (SELECTED != -1) {
        JComboBox<String> combobox = jcbBONUS.elementAt(1);
        int index = planetPst.getIndex(SELECTED);
        byte bonusLvl = (byte) combobox.getSelectedIndex();
        if (bonusLvl == -1) {
          return;
        }

        val planet = planetPst.getPlanet(index);
        if (planet.getEnergyBonusLvl() != bonusLvl) {
          planet.setEnergyBonusLvl(bonusLvl);
          planetPst.sortEntries();
          fillPlanets();
        }

        // update max population selection
        int pot = SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
        jlPOP.setText(strMAX_POP.replace("%1", Integer.toString(pot))); //$NON-NLS-1$
        // update max bonus selection
        float pov = BONI.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());
        jlBONI.setText(strBONUS.replace("%1", Integer.toString((int) pov * 5))); //$NON-NLS-1$
      }
    }));

    jcbBONUS.elementAt(2).addActionListener(new CBActionListener(evt -> {
      if (SELECTED >= 0) {
        JComboBox<String> combobox = jcbBONUS.elementAt(2);
        int index = planetPst.getIndex(SELECTED);
        byte bonusLvl = (byte) combobox.getSelectedIndex();
        if (bonusLvl == -1)
          return;

        val planet = planetPst.getPlanet(index);
        if (planet.getGrowthBonusLvl() != bonusLvl) {
          planet.setGrowthBonusLvl(bonusLvl);
          planetPst.sortEntries();
          fillPlanets();
        }

        // update max population selection
        int pot = SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
            planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
        jlPOP.setText(strMAX_POP.replace("%1", Integer.toString(pot))); //$NON-NLS-1$
        // update max bonus selection
        float pov = BONI.get(planet.getPlanetType(), planet.getPlanetSize(),
            planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());
        jlBONI.setText(strBONUS.replace("%1", Integer.toString((int) pov * 5))); //$NON-NLS-1$
      }
    }));

    jcbMASK.addActionListener(new CBActionListener(evt -> {
      // apply planet name changes
      finalWarning();
      fillPlanets();
    }));

    lstPLANET.addListSelectionListener(new CBListSelectionListener(evt -> {
      if (lstPLANET.getSelectedIndex() < 0)
        return;

      //save
      finalWarning();

      //show
      updatePlanetSelection();
      if (!PAUSE)
        fillPlanets();
    }));

    btnADD.addActionListener(new CBActionListener(evt -> {
      // apply planet name changes
      finalWarning();

      int type = ((ID)jcbMASK.getSelectedItem()).ID;
      if (type < 0)
        type = Integer.min(ENVIRONMENT.numEntries(), 7);

      planetPst.addPlanet(type);
      fillPlanets();

      ID[] hm = planetPst.getIDs(type);

      lstPLANET.setSelectedIndex(hm.length - 1);
      lstPLANET.ensureIndexIsVisible(hm.length - 1);
    }));

    btnREMOVE.addActionListener(new CBActionListener(evt -> {
      if (SELECTED >= 0) {
        // check minor race usage
        for (short r = NUM_EMPIRES; r < RACE.getNumberOfEntries(); r++) {
          if (RACE.getPlanet(r) == SELECTED) {
            String msg = "This is the home planet of the %1."
              .replace("%1", RACE.getName(r));
            JOptionPane.showMessageDialog(null, msg, "Can not remove planet",
              JOptionPane.INFORMATION_MESSAGE);
            return;
          }
        }

        //check major race usage
        String[] planetNames;
        int index = planetPst.getIndex(SELECTED);
        val planet = planetPst.getPlanet(index);
        String planetName = planet.getName();

        for (short r = 0; r < NUM_EMPIRES; r++) {
          planetNames = MEPLANET.getPlanets(r);

          if (planetNames != null) {
            for (String name : planetNames) {
              if (name.equals(planetName)) {
                String msg = "This planet belongs to the home system of the %1."
                  .replace("%1", RACE.getName(r));
                JOptionPane.showMessageDialog(null, msg, "Can not remove planet",
                  JOptionPane.INFORMATION_MESSAGE);
                return;
              }
            }
          }
        }

        planetPst.removePlanet(SELECTED);
        fillPlanets();
      }
    }));

    //fill combos
    Collection<String> aniFiles = STBOF.getFileNames(FileFilters.Ani);
    for (String ani : aniFiles)
      jcbANI.addItem(ani);

    ID[] planetGfx = ENVIRONMENT.planetGfx();
    for (ID gfx : planetGfx) {
      jcbTYPE.addItem(gfx);
    }
    ID[] atmoGfx = ENVIRONMENT.atmosphereGfx();
    for (ID gfx : atmoGfx) {
      jcbATMO.addItem(gfx);
    }
    jcbSIZE.addItem(Language.getString("PlanetGUI.12")); //$NON-NLS-1$
    jcbSIZE.addItem(Language.getString("PlanetGUI.13")); //$NON-NLS-1$
    jcbSIZE.addItem(Language.getString("PlanetGUI.14")); //$NON-NLS-1$

    for (int i = 0; i < 3; i++) {
      String str = null;
      switch (i) {
        case 0:
          str = Language.getString("PlanetGUI.15"); //$NON-NLS-1$
          break;
        case 1:
          str = Language.getString("PlanetGUI.16"); //$NON-NLS-1$
          break;
        case 2:
          str = Language.getString("PlanetGUI.17"); //$NON-NLS-1$
          break;
      }

      jcbBONUS.elementAt(0).addItem(str);
      jcbBONUS.elementAt(1).addItem(str);
      jcbBONUS.elementAt(2).addItem(str);
    }

    // add all planet types filter
    jcbMASK.addItem(new ID(Language.getString("PlanetGUI.18"), -1)); //$NON-NLS-1$
    for (ID gfx : planetGfx) {
      jcbMASK.addItem(gfx);
    }

    //place
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;

    //planet name
    c.gridx = 0;
    c.gridy = 0;
    add(jcbMASK, c);
    c.gridy = 1;
    c.gridwidth = 1;
    c.gridheight = 10;
    JScrollPane jpplan = new JScrollPane(lstPLANET);
    jpplan.setPreferredSize(new Dimension(150, 300));
    add(jpplan, c);
    c.gridheight = 1;
    c.gridy = 11;
    c.insets = new Insets(0, 5, 0, 5);
    add(btnADD, c);
    c.gridy = 12;
    c.insets = new Insets(0, 5, 5, 5);
    add(btnREMOVE, c);
    c.insets = new Insets(5, 5, 5, 5);
    c.gridy = 1;
    c.gridx = 1;
    JLabel lbl = new JLabel(Language.getString("PlanetGUI.19")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);

    //labels
    for (int i = 0; i < 6; i++) {
      c.gridy = i + 2;
      add(LABEL[i], c);
    }
    c.gridy = 8;
    lbl = new JLabel(Language.getString("PlanetGUI.20")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 9;
    lbl = new JLabel(Language.getString("PlanetGUI.21")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 10;
    lbl = new JLabel(Language.getString("PlanetGUI.22")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    //edit
    c.gridx = 2;
    c.gridy = 1;
    add(txtNAME, c);
    //ani
    c.gridy = 2;
    add(jcbANI, c);
    //type
    c.gridy = 3;
    add(jcbTYPE, c);
    //size
    c.gridy = 4;
    add(jcbSIZE, c);
    //bonuses
    c.gridy = 5;
    add(jcbBONUS.elementAt(0), c);
    c.gridy = 6;
    add(jcbBONUS.elementAt(1), c);
    c.gridy = 7;
    add(jcbBONUS.elementAt(2), c);
    c.gridy = 8;
    add(jcbATMO, c);
    c.gridy = 9;
    add(txtTerraformPoints, c);
    c.gridy = 10;
    add(txtTerraformed, c);
    //animation
    c.insets = new Insets(5, 50, 5, 5);
    c.gridheight = 2;
    c.gridx = 3;
    c.gridy = 1;
    add(alANIM, c);
    //descriptive labels
    c.gridheight = 1;
    c.gridy = 3;
    add(jlPOP, c);
    c.gridy = 4;
    add(jlBONI, c);
    c.gridy = 5;
    add(jlINDEX, c);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Planets;
  }

  private void fillPlanets() {
    int j = SELECTED;
    SELECTED = -1;
    PAUSE = true;
    ID[] hm = planetPst.getIDs(((ID)jcbMASK.getSelectedItem()).ID);
    lstPLANET.setListData(hm);

    if (j >= 0) {
      lstPLANET.setSelectedValue(new ID("", j), true); //$NON-NLS-1$

      while (lstPLANET.getSelectedIndex() < 0 && j > 0) {
        j--;
        lstPLANET.setSelectedValue(new ID("", j), true); //$NON-NLS-1$
      }

      if (lstPLANET.getSelectedIndex() < 0) {
        lstPLANET.setSelectedIndex(0);
      }
    } else {
      lstPLANET.setSelectedIndex(0);
    }

    btnREMOVE.setEnabled(SELECTED >= 0);
    PAUSE = false;
  }

  protected void updatePlanetSelection() {
    ID hm = lstPLANET.getSelectedValue();
    SELECTED = (short) hm.hashCode();

    if (SELECTED >= 0) {
      btnREMOVE.setEnabled(true);

      try {
        int index = planetPst.getIndex(SELECTED);
        val planet = planetPst.getPlanet(index);

        txtNAME.setText(planet.getName());
        jcbTYPE.setSelectedItem(new ID(null, planet.getPlanetType()));
        jcbSIZE.setSelectedIndex(planet.getPlanetSize());
        jlINDEX.setText(strINDEX.replace("%1", Integer.toString(SELECTED))); //$NON-NLS-1$
        jcbANI.setSelectedItem(planet.getAnimation());
        jcbBONUS.elementAt(0).setSelectedIndex(planet.getFoodBonusLvl());
        jcbBONUS.elementAt(1).setSelectedIndex(planet.getEnergyBonusLvl());
        jcbBONUS.elementAt(2).setSelectedIndex(planet.getGrowthBonusLvl());
        jcbATMO.setSelectedItem(new ID(null, planet.getAtmosphere()));
        txtTerraformPoints.setText(Integer.toString(planet.getTerraformPoints()));
        txtTerraformed.setText(Integer.toString(planet.getTerraformed()));

        AniOrCur ani = STBOF.getAnimation(planet.getAnimation(), LoadFlags.CACHE);
        alANIM.setAnimation(ani, true);

        int maxPop = SPACE.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
        jlPOP.setText(strMAX_POP.replace("%1", Integer.toString(maxPop))); //$NON-NLS-1$
        float bonus = BONI.get(planet.getPlanetType(), planet.getPlanetSize(),
           planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());
        jlBONI.setText(strBONUS.replace("%1", Integer.toString((int) bonus * 5))); //$NON-NLS-1$
      } catch (Exception fnf) {
        Dialogs.displayError(fnf);
        try {
          alANIM.setAnimation(null, false);
        } catch(Exception e) {
          // ignore 2nd error
        }
      }
    }
  }

  @Override
  public boolean hasChanges() {
    return planetPst.madeChanges() || MEPLANET.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(planetPst, changes);
    STBOF.checkChanged(MEPLANET, changes);
  }

  @Override
  public void reload() throws IOException {
    MEPLANET = (Meplanet) STBOF.getInternalFile(CStbofFiles.MEPlanetBin, true);
    planetPst = (Planet) STBOF.getInternalFile(CStbofFiles.PlanetPst, true);
    fillPlanets();
  }

  @Override
  public void finalWarning() {
    if (SELECTED < 0)
      return;

    int index = planetPst.getIndex(SELECTED);
    val planet = planetPst.getPlanet(index);
    String old = planet.getName();
    String planetName = txtNAME.getText();
    planet.setName(planetName);
    String terrPts = txtTerraformPoints.getText();
    planet.setTerraformPoints(DataTools.tryParseUInt(terrPts).orElse(0));
    String terrFmd = txtTerraformPoints.getText();
    planet.setTerraformed(DataTools.tryParseUInt(terrFmd).orElse(0));
    MEPLANET.renamePlanets(old, planetName);
  }
}
