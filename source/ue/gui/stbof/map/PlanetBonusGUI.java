package ue.gui.stbof.map;

import java.io.IOException;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.service.Language;

/**
 * Used to edit planet bonuses.
 */
public class PlanetBonusGUI extends MaxPopOrBonGUI {

  private final String description = Language.getString("PlanetBonusGUI.0"); //$NON-NLS-1$
  private final String strBonus1 = Language.getString("PlanetBonusGUI.1"); //$NON-NLS-1$
  private final String strBonus2 = Language.getString("PlanetBonusGUI.2"); //$NON-NLS-1$
  private final String strBonus3 = Language.getString("PlanetBonusGUI.3"); //$NON-NLS-1$

  // edited
  private Planboni BONI = null;

  /**
   * @param stbof The stbof.res archive to lookup and edit files.
   * @throws IOException
   */
  public PlanetBonusGUI(Stbof stbof) throws IOException {
    super(stbof);
    BONI = (Planboni) stbof.getInternalFile(CStbofFiles.PlanBoniBin, true);

    lblDesc.setText(description);
    lblBoni[0].setText(strBonus1);
    lblBoni[1].setText(strBonus2);
    lblBoni[2].setText(strBonus3);
    for (JSpinner spinner : jspValue)
      spinner.setModel(new SpinnerNumberModel(0d, -1000d, 1000d, 0.05d));

    updateSelected();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.PlanetBonuses;
  }

  @Override
  public boolean hasChanges() {
    return BONI.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(BONI, changes);
  }

  @Override
  public void reload() throws IOException {
    BONI = (Planboni) stbof.getInternalFile(CStbofFiles.PlanBoniBin, true);
    updateSelected();
  }

  @Override
  protected void loadValues(byte planetType, byte planetSize) throws InvalidArgumentsException {
    int[] values = BONI.getValues(planetType, planetSize);
    for (int i = 0; i < NUM_VALUES; ++i) {
      // default missing values
      int value = values != null ? values[i] : 0;
      jspValue[i].setValue(value * 0.05f);
    }
  }

  @Override
  protected void applyValues(byte planetType, byte planetSize) throws InvalidArgumentsException {
    for (int i = 0; i < NUM_VALUES; ++i) {
      int foodBonus = i / 3;
      int energyBonus = i % 3;
      Number value = (Number) jspValue[i].getValue();
      int bonus = (int) (value.floatValue() * 20f);
      BONI.set(planetType, planetSize, foodBonus, energyBonus, bonus);
    }
  }
}
