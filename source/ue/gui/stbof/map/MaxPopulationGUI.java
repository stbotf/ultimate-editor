package ue.gui.stbof.map;

import java.io.IOException;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.service.Language;

/**
 * Used to edit the max planet populations.
 */
public class MaxPopulationGUI extends MaxPopOrBonGUI {

  private final String description = Language.getString("MaxPopulationGUI.0"); //$NON-NLS-1$
  private final String strBonus1 = Language.getString("MaxPopulationGUI.1"); //$NON-NLS-1$
  private final String strBonus2 = Language.getString("MaxPopulationGUI.2"); //$NON-NLS-1$
  private final String strBonus3 = Language.getString("MaxPopulationGUI.3"); //$NON-NLS-1$

  // edited
  private Planspac SPACE = null;

  /**
   * @param stbof The stbof.res archive to lookup and edit files.
   * @throws IOException
   */
  public MaxPopulationGUI(Stbof stbof) throws IOException {
    super(stbof);
    SPACE = (Planspac) stbof.getInternalFile(CStbofFiles.PlanSpacBin, true);

    lblDesc.setText(description);
    lblBoni[0].setText(strBonus1);
    lblBoni[1].setText(strBonus2);
    lblBoni[2].setText(strBonus3);
    for (JSpinner spinner : jspValue)
      spinner.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));

    updateSelected();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.MaxPlanetPop;
  }

  @Override
  public boolean hasChanges() {
    return SPACE != null && SPACE.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(SPACE, changes);
  }

  @Override
  public void reload() throws IOException {
    SPACE = (Planspac) stbof.getInternalFile(CStbofFiles.PlanSpacBin, true);
    updateSelected();
  }

  @Override
  protected void loadValues(byte planetType, byte planetSize) throws InvalidArgumentsException {
    int[] values = SPACE.getValues(planetType, planetSize);
    for (int i = 0; i < NUM_VALUES; ++i) {
      // default missing values
      int value = values != null ? values[i] : 0;
      jspValue[i].setValue(value);
    }
  }

  @Override
  protected void applyValues(byte planetType, byte planetSize) throws InvalidArgumentsException {
    for (int i = 0; i < NUM_VALUES; ++i) {
      int growthBonus = i / 3;
      int energyBonus = i % 3;
      Integer value = (Integer) jspValue[i].getValue();
      SPACE.set(planetType, planetSize, growthBonus, energyBonus, value);
    }
  }
}
