package ue.gui.stbof.map;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.est.Environ;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.MainPanel;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

/**
 * Base class used to edit either max planet populations or planet bonuses.
 */
public abstract class MaxPopOrBonGUI extends MainPanel {

  public static final int NUM_VALUES = 9;

  private final String strPlanetType = Language.getString("MaxPopOrBonGUI.0"); //$NON-NLS-1$
  private final String strPlanetSize = Language.getString("MaxPopOrBonGUI.1"); //$NON-NLS-1$
  private final String strEnergyBonus1 = Language.getString("MaxPopOrBonGUI.10"); //$NON-NLS-1$
  private final String strEnergyBonus2 = Language.getString("MaxPopOrBonGUI.11"); //$NON-NLS-1$
  private final String strEnergyBonus3 = Language.getString("MaxPopOrBonGUI.12"); //$NON-NLS-1$
  private final String strPlanetSize1 = Language.getString("MaxPopOrBonGUI.13"); //$NON-NLS-1$
  private final String strPlanetSize2 = Language.getString("MaxPopOrBonGUI.14"); //$NON-NLS-1$
  private final String strPlanetSize3 = Language.getString("MaxPopOrBonGUI.15"); //$NON-NLS-1$

  // read
  protected Stbof stbof = null;
  private Environ ENVI = null;

  // ui components
  private JLabel lblPlanType = new JLabel(strPlanetType); //$NON-NLS-1$
  private JLabel lblPlanSize = new JLabel(strPlanetSize); //$NON-NLS-1$
  private JComboBox<ID> jcbPlanType = new JComboBox<ID>();
  private JComboBox<ID> jcbPlanSize = new JComboBox<ID>();
  protected JLabel lblDesc = new JLabel();
  protected JLabel[] lblBoni = new JLabel[3];
  protected JSpinner[] jspValue = new JSpinner[NUM_VALUES];
  private JLabel[] lblEnergyBoni = new JLabel[3];

  // data
  private ID SELECTED_TYPE = null;
  private byte SELECTED_SIZE = -1;

  /**
   * @param stbof The stbof.res archive, used to lookup 'Environ' for the planet types.
   * @throws IOException
   */
  public MaxPopOrBonGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;
    ENVI = (Environ) stbof.getInternalFile(CStbofFiles.EnvironEst, true);

    lblBoni[0] = new JLabel();
    lblBoni[1] = new JLabel();
    lblBoni[2] = new JLabel();
    lblEnergyBoni[0] = new JLabel(strEnergyBonus1);
    lblEnergyBoni[1] = new JLabel(strEnergyBonus2);
    lblEnergyBoni[2] = new JLabel(strEnergyBonus3);
    for (int i = 0; i < NUM_VALUES; i++)
      jspValue[i] = new JSpinner();

    setupComponents();
    placeComponents();
    loadSelections();
    addListeners();
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();
    lblDesc.setFont(def);
    lblBoni[0].setFont(def);
    lblBoni[1].setFont(def);
    lblBoni[2].setFont(def);
    lblEnergyBoni[0].setFont(def);
    lblEnergyBoni[1].setFont(def);
    lblEnergyBoni[2].setFont(def);
    lblPlanType.setFont(def);
    lblPlanSize.setFont(def);
    jcbPlanType.setFont(def);
    jcbPlanSize.setFont(def);
    for (JSpinner spinner : jspValue)
      spinner.setFont(def);
  }

  private void addListeners() {
    jcbPlanType.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        //save
        try {
          finalWarning();
        } catch (Exception ex) {
          Dialogs.displayError(ex);
          return;
        }
        updateSelected();
      }
    });

    jcbPlanSize.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        //save
        try {
          finalWarning();
        } catch (Exception ex) {
          Dialogs.displayError(ex);
          return;
        }
        updateSelected();
      }
    });
  }

  private void placeComponents() {
    setLayout(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.WEST;

    // labels & spinners
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    add(lblPlanType, c);
    c.gridx = 1;
    add(jcbPlanType, c);
    c.gridx = 2;
    add(lblPlanSize, c);
    c.gridx = 3;
    add(jcbPlanSize, c);
    c.gridy = 2;
    c.gridx = 0;
    add(lblDesc, c);
    c.gridx = 1;
    add(lblBoni[0], c);
    c.gridx = 2;
    add(lblBoni[1], c);
    c.gridx = 3;
    add(lblBoni[2], c);
    c.gridy = 3;
    c.gridx = 0;
    add(lblEnergyBoni[0], c);
    c.gridx = 1;
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    add(jspValue[0], c);
    c.gridx = 2;
    add(jspValue[3], c);
    c.gridx = 3;
    add(jspValue[6], c);
    c.anchor = GridBagConstraints.WEST;
    c.fill = GridBagConstraints.BOTH;
    c.gridy = 4;
    c.gridx = 0;
    add(lblEnergyBoni[1], c);
    c.gridx = 1;
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    add(jspValue[1], c);
    c.gridx = 2;
    add(jspValue[4], c);
    c.gridx = 3;
    add(jspValue[7], c);
    c.anchor = GridBagConstraints.WEST;
    c.fill = GridBagConstraints.BOTH;
    c.gridy = 5;
    c.gridx = 0;
    add(lblEnergyBoni[2], c);
    c.gridx = 1;
    c.anchor = GridBagConstraints.CENTER;
    c.fill = GridBagConstraints.BOTH;
    add(jspValue[2], c);
    c.gridx = 2;
    add(jspValue[5], c);
    c.gridx = 3;
    add(jspValue[8], c);
  }

  private void loadSelections() {
    //types
    ID[] planetTypes = ENVI.planetGfx();
    jcbPlanType.removeAllItems();

    for (ID type : planetTypes)
      jcbPlanType.addItem(type);

    //sizes
    jcbPlanSize.addItem(new ID(strPlanetSize1, 0));
    jcbPlanSize.addItem(new ID(strPlanetSize2, 1));
    jcbPlanSize.addItem(new ID(strPlanetSize3, 2));
  }

  protected void updateSelected() {
    SELECTED_TYPE = (ID) jcbPlanType.getSelectedItem();
    SELECTED_SIZE = (byte) jcbPlanSize.getSelectedIndex();
    if (SELECTED_TYPE == null || SELECTED_SIZE < 0)
      return;

    try {
      loadValues((byte) SELECTED_TYPE.ID, SELECTED_SIZE);
    } catch (Exception rf) {
      Dialogs.displayError(rf);
    }
  }

  @Override
  public void finalWarning() throws InvalidArgumentsException, ParseException {
    if (SELECTED_TYPE == null || SELECTED_SIZE < 0)
      return;

    for (JSpinner spinner : jspValue)
      spinner.commitEdit();

    applyValues((byte) SELECTED_TYPE.ID, SELECTED_SIZE);
  }

  protected abstract void loadValues(byte planetType, byte planetSize) throws InvalidArgumentsException;
  protected abstract void applyValues(byte planetType, byte planetSize) throws InvalidArgumentsException;
}
