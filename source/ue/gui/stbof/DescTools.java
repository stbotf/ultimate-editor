package ue.gui.stbof;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import ue.UE;
import ue.edit.res.stbof.files.desc.DescDataFile;
import ue.gui.util.dialog.Dialogs;

public class DescTools {

  public static boolean showToolsDialog(DescDataFile ddf) {
    String title = "Description fixture tools";
    JPanel pnlTools = new JPanel();
    JLabel lblTools = new JLabel("Select description fixture to apply:");
    JRadioButton rbnRemap = new JRadioButton();
    JRadioButton rbnResetOffsets = new JRadioButton();
    pnlTools.setLayout(new BoxLayout(pnlTools, BoxLayout.Y_AXIS));
    pnlTools.add(lblTools);
    pnlTools.add(rbnRemap);
    pnlTools.add(rbnResetOffsets);
    ButtonGroup rbgGroup = new ButtonGroup();
    rbgGroup.add(rbnRemap);
    rbgGroup.add(rbnResetOffsets);

    lblTools.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    rbnRemap.setText("remap by description index");
    rbnResetOffsets.setText("reset to description start address");

    int response = JOptionPane.showConfirmDialog(UE.WINDOW, pnlTools, title, JOptionPane.OK_CANCEL_OPTION);
    if (response == JOptionPane.OK_OPTION) {
      try {
        if (rbnRemap.isSelected()) {
          ddf.remapDescriptions();
          return true;
        }
        else if (rbnResetOffsets.isSelected()) {
          ddf.resetDescriptionOffsets();
          return true;
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }

    return false;
  }
}
