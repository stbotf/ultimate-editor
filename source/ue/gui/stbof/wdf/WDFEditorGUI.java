package ue.gui.stbof.wdf;

import java.awt.AWTException;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.fnt.TgaFont;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.edit.res.stbof.files.wdf.widgets.WDFButton;
import ue.edit.res.stbof.files.wdf.widgets.WDFListBox;
import ue.edit.res.stbof.files.wdf.widgets.WDFMenu;
import ue.edit.res.stbof.files.wdf.widgets.WDFPicture;
import ue.edit.res.stbof.files.wdf.widgets.WDFScrollBar;
import ue.edit.res.stbof.files.wdf.widgets.WDFSlider;
import ue.edit.res.stbof.files.wdf.widgets.WDFTextBox;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.wdf.nodes.WDFFileNode;
import ue.gui.stbof.wdf.nodes.WDFRootNode;
import ue.gui.stbof.wdf.nodes.data.WDFDataNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFButtonNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFListBoxNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFMenuNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFPictureNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFScrollBarNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFSliderNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFTextBoxNode;
import ue.gui.stbof.wdf.nodes.widgets.WDFWidgetNode;
import ue.gui.util.component.IconButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.gfx.Icons;
import ue.service.Language;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;
import ue.util.img.ImageFile;
import ue.util.img.ImageSource;

public class WDFEditorGUI extends MainPanel {

  private static final int DIVIDER_LOCATION = 250;

  // read
  private Stbof stbof;

  // edited
  private WDFFileNode wdfDrawn = null;

  // ui components
  private JTree tree;
  private JScrollPane scrTree;
  private JLabel display = new JLabel();
  private IconButton btnAddButton = new IconButton(Language.getString("WDFEditorGUI.1"), Icons.BUTTON); //$NON-NLS-1$
  private IconButton btnAddListBox = new IconButton(Language.getString("WDFEditorGUI.2"), Icons.LIST); //$NON-NLS-1$
  private IconButton btnAddMenu = new IconButton(Language.getString("WDFEditorGUI.3"), Icons.MENU); //$NON-NLS-1$
  private IconButton btnAddPicture = new IconButton(Language.getString("WDFEditorGUI.4"), Icons.IMAGE); //$NON-NLS-1$
  private IconButton btnAddScrollBar = new IconButton(Language.getString("WDFEditorGUI.5"), Icons.SCROLL); //$NON-NLS-1$
  private IconButton btnAddSlider = new IconButton(Language.getString("WDFEditorGUI.6"), Icons.SLIDER); //$NON-NLS-1$
  private IconButton btnAddTextBox = new IconButton(Language.getString("WDFEditorGUI.7"), Icons.TEXT); //$NON-NLS-1$
  private IconButton btnRemove = new IconButton(Language.getString("WDFEditorGUI.8"), Icons.DELETE); //$NON-NLS-1$

  // data
  private BufferedImage image;
  private Robot robot;
  private boolean ignoreNodeSelection = false;

  private ActionListener defaultChangeListener = new ActionListener() {
    // runs when a node reports a change in data
    @Override
    public void actionPerformed(ActionEvent e) {
      TreePath path = tree.getSelectionPath();
      if (path != null) {
        updateImage(path);
        tree.startEditingAtPath(path);
      }
    }
  };

  private ImageCache imgCache = new ImageCache(new ImageSource() {
    @Override
    public ImageFile getImage(String name) {
      ImageFile res = null;

      try {
        TargaImage tga = stbof.getTargaImage(name, LoadFlags.UNCACHED);
        res = new ImageFile(name, tga.getImage(AlphaMode.AlphaChannel), tga.getSize());
      } catch (IOException ex) {
        Dialogs.displayError(ex);
      }

      return res;
    }

    @Override
    public ImageFile getCharacterImage(String font, char character) {
      String name = font + File.pathSeparator + character;
      ImageFile res = null;

      try {
        TgaFont tgafont = stbof.getTgaFont(font, LoadFlags.UNCACHED);
        res = new ImageFile(name, tgafont.getChar(character), tgafont.getSize());
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }

      return res;
    }
  });


  public WDFEditorGUI(Stbof res) throws IOException {
    this.stbof = res;

    image = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
    clearImage();

    try {
      robot = new Robot();
    } catch (AWTException ex) {
      Dialogs.displayError(ex);
      return;
    }

    initComponents();
    placeComponents();
    addListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.WdfEditor;
  }

  private void initComponents() throws IOException {
    tree = new JTree() {
      @Override
      public void cancelEditing() {
        if (tree.isEditing()) {
          TreePath path = tree.getSelectionPath();
          if (path != null && path.getLastPathComponent() instanceof WDFDataNode)
            return;
        }

        super.cancelEditing();
      }
    };

    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    tree.setShowsRootHandles(true);
    tree.setRootVisible(false);
    tree.setEditable(true);
    tree.setToggleClickCount(0);
    tree.setRowHeight(-1);
    tree.setScrollsOnExpand(false);
    tree.setAutoscrolls(false);

    tree.setCellRenderer(new DefaultTreeCellRenderer() {
      private static final long serialVersionUID = 4004904842545117124L;

      @Override
      public Component getTreeCellRendererComponent(JTree tree, Object value,
          boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        TreePath path = tree.getPathForRow(row);
        Component com = null;

        if (path != null) {
          Object node = path.getLastPathComponent();
          if (node instanceof WDFDataNode)
            com = ((WDFDataNode) node).getTreeCellEditorComponent(sel);
        }

        if (com == null)
          com = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        return com;
      }
    });

    tree.setCellEditor(new DefaultTreeCellEditor(tree, (DefaultTreeCellRenderer) tree.getCellRenderer()) {
      @Override
      public Component getTreeCellEditorComponent(JTree tree, Object value,
          boolean isSelected, boolean expanded, boolean leaf, int row) {
        TreePath path = tree.getPathForRow(row);

        if (path != null) {
          Object node = path.getLastPathComponent();
          if (node instanceof WDFDataNode) {
            Component comp = ((WDFDataNode) node).getTreeCellEditorComponent(true);
            if (comp != null)
              return comp;
          }
        }

        return tree.getCellRenderer().getTreeCellRendererComponent(
          tree, value, isSelected, expanded, leaf, row, false);
      }
    });

    display.setIconTextGap(0);
    display.setHorizontalAlignment(SwingConstants.CENTER);
    display.setVerticalAlignment(SwingConstants.CENTER);
    display.setIcon(new ImageIcon(image));

    btnAddButton.setEnabled(false);
    btnAddListBox.setEnabled(false);
    btnAddMenu.setEnabled(false);
    btnAddPicture.setEnabled(false);
    btnAddScrollBar.setEnabled(false);
    btnAddSlider.setEnabled(false);
    btnAddTextBox.setEnabled(false);
    btnRemove.setEnabled(false);

    loadTree();
  }

  private void addListeners() {
    tree.addTreeSelectionListener(new TreeSelectionListener() {
      @Override
      public void valueChanged(TreeSelectionEvent e) {
        if (ignoreNodeSelection || !e.isAddedPath())
          return;

        final TreePath selPath = e.getNewLeadSelectionPath();
        if (selPath == null || selPath.getPathCount() < 2)
          return;

        // update selected wdf file node
        final TreePath path = updateFileSelection(selPath);

        // always update image for the red selection box
        updateImage(path);

        if (path.getLastPathComponent() instanceof WDFDataNode
            && !(tree.isPathSelected(path) && tree.isEditing())) {
          SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              // auto-start editing - sets cursor for text boxes
              tree.startEditingAtPath(path);

              Point loc = MouseInfo.getPointerInfo().getLocation();
              Point wloc = UE.WINDOW.getLocationOnScreen();
              loc.x -= wloc.x;
              loc.y -= wloc.y;

              // to prevent repetitive clicks on add/remove selection change,
              // check that the mouse pointer is located within the tree bounds
              Rectangle bounds = tree.getBounds();
              if (loc.x < bounds.x || loc.x > bounds.x + bounds.width
                || loc.y < bounds.y || loc.y > bounds.y + bounds.height)
                return;

              Component com = SwingUtilities.getDeepestComponentAt(UE.WINDOW, loc.x, loc.y);
              if (com instanceof JLabel || com instanceof JTree)
                return;

              // workaround to auto-select all text for integer nodes
              // listening for the focus gain event instead has a noticeable delay
              // @see WDFIntegerNode addFocusListener
              robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
              robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            }
          });
        }
      }
    });

    // hackedy hack hack hack because JTree is crap
    // and so is this hack
    // this is to prevent child nodes from being cut off - not painted fully
    tree.addTreeExpansionListener(new TreeExpansionListener() {
      @Override
      public void treeExpanded(TreeExpansionEvent event) {
        // resize nodes
        resizeNodes(event.getPath());
      }

      @Override
      public void treeCollapsed(TreeExpansionEvent event) {
      }
    });

    btnAddButton.addActionListener(new CBActionListener(x -> onAddButton(x)));
    btnAddListBox.addActionListener(new CBActionListener(x -> onAddListBox(x)));
    btnAddMenu.addActionListener(new CBActionListener(x -> onAddMenu(x)));
    btnAddPicture.addActionListener(new CBActionListener(x -> onAddPicture(x)));
    btnAddScrollBar.addActionListener(new CBActionListener(x -> onAddScrollBar(x)));
    btnAddSlider.addActionListener(new CBActionListener(x -> onAddSlider(x)));
    btnAddTextBox.addActionListener(new CBActionListener(x -> onAddTextBox(x)));
    btnRemove.addActionListener(new CBActionListener(x -> onRemove(x)));
  }

  private void onAddButton(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    val widget = new WDFButton(wdfDrawn.wdf());
    addWidget(new WDFButtonNode(widget, stbof, imgCache));
  }

  private void onAddListBox(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    val widget = new WDFListBox(wdfDrawn.wdf());
    addWidget(new WDFListBoxNode(widget, stbof, imgCache));
  }

  private void onAddMenu(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    val widget = new WDFMenu(wdfDrawn.wdf());
    addWidget(new WDFMenuNode(widget, stbof, imgCache));
  }

  private void onAddPicture(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    val widget = new WDFPicture(wdfDrawn.wdf());
    addWidget(new WDFPictureNode(widget, stbof, imgCache));
  }

  private void onAddScrollBar(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    val widget = new WDFScrollBar(wdfDrawn.wdf());
    addWidget(new WDFScrollBarNode(widget, stbof, imgCache));
  }

  private void onAddSlider(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    val widget = new WDFSlider(wdfDrawn.wdf());
    addWidget(new WDFSliderNode(widget, stbof, imgCache));
  }

  private void onAddTextBox(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    val widget = new WDFTextBox(wdfDrawn.wdf());
    addWidget(new WDFTextBoxNode(widget, stbof, imgCache));
  }

  private void onRemove(ActionEvent x) {
    if (wdfDrawn == null)
      return;

    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    TreePath sel = tree.getSelectionPath();
    WDFFileNode fileNode = sel.getPathCount() > 1 ?
      (WDFFileNode) sel.getPathComponent(1) : wdfDrawn;

    // don't remove file nodes
    if (sel.getPathCount() <= 2)
      return;

    // determine index to remove
    int index = fileNode.getIndex((TreeNode)sel.getPathComponent(2));

    // don't remove data nodes
    int dataNodes = fileNode.numDataNodes();
    if (index < dataNodes)
      return;

    // check node type
    TreeNode child = fileNode.getChildAt(index);
    if (!(child instanceof WDFWidgetNode))
      return;

    WDFWidgetNode node = (WDFWidgetNode) child;

    // get preceding node
    DefaultMutableTreeNode prev = (DefaultMutableTreeNode) fileNode.getChildBefore(node);
    sel = new TreePath(prev.getPath());
    // remove from tree
    ignoreNodeSelection = true;
    model.removeNodeFromParent(node);
    ignoreNodeSelection = false;
    // remove from wdf window
    fileNode.window().removeWidget(node.widget());

    // update selection and in turn the display image
    tree.setSelectionPath(sel);
  }

  private void addWidget(WDFWidgetNode node) {
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    TreePath sel = tree.getSelectionPath();
    WDFFileNode fileNode = sel.getPathCount() > 1 ?
      (WDFFileNode) sel.getPathComponent(1) : wdfDrawn;

    // determine selected index
    int index = sel.getPathCount() > 2
      ? fileNode.getIndex((TreeNode)sel.getPathComponent(2))
      : fileNode.getChildCount();

    // don't insert precedent to the data nodes
    int dataNodes = fileNode.numDataNodes();
    index = Integer.max(index, dataNodes);

    // adjust for next possible type group slot
    index = adjustForGroupType(fileNode, node.type(), index);

    // add to tree
    model.insertNodeInto(node, fileNode, index);

    // determine the type group index of the inserted node
    int typeIndex = determineTypeGroupIndex(fileNode, index);

    // add to wdf window
    fileNode.window().addWidget(node.widget(), typeIndex);

    // update selection and in turn the display image
    sel = new TreePath(node.getPath());
    tree.setSelectionPath(sel);
  }

  private int adjustForGroupType(WDFFileNode fileNode, int nodeType, int index) {
    int childCount = fileNode.getChildCount();
    index = Integer.min(index, childCount);
    int dataNodes = fileNode.numDataNodes();
    index = Integer.max(index, dataNodes);

    // find previous entry with type <= node.type
    while (index - 1 >= dataNodes) {
      WDFWidgetNode widget = (WDFWidgetNode) fileNode.getChildAt(index - 1);
      if (widget.type() <= nodeType)
        break;
      index--;
    }

    // find next entry with type >= node.type
    while (index < childCount) {
      WDFWidgetNode widget = (WDFWidgetNode) fileNode.getChildAt(index);
      if (widget.type() >= nodeType)
        break;
      index++;
    }

    return index;
  }

  // determines the position within same type node siblings
  private int determineTypeGroupIndex(WDFFileNode fileNode, int index) {
    int dataNodes = fileNode.numDataNodes();
    WDFWidgetNode widget = (WDFWidgetNode) fileNode.getChildAt(index);
    int nodeType = widget.type();

    int typeIndex = 0;
    for (int i = index - 1; i >= dataNodes; --i) {
      TreeNode pre = fileNode.getChildAt(i);
      if (!(pre instanceof WDFWidgetNode))
        break;

      widget = (WDFWidgetNode) pre;
      if (widget.type() != nodeType)
        break;

      typeIndex++;
    }

    return typeIndex;
  }

  protected TreePath updateFileSelection(TreePath path) {
    // update selected wdf file node
    TreeNode node = (TreeNode) path.getPathComponent(1);
    if (node != wdfDrawn) {
      if (node instanceof WDFFileNode) {
        wdfDrawn = (WDFFileNode) node;
      } else {
        try {
          wdfDrawn = loadAndExpandWDFFile((DefaultMutableTreeNode) path.getLastPathComponent());
          path = tree.getPathForRow(tree.getLeadSelectionRow());
        } catch (Exception ex) {
          wdfDrawn = null;
          Dialogs.displayError(ex);
        }
      }

      updateDisplayMenu();
    }

    return path;
  }

  private void loadTree() throws IOException {
    ArrayList<String> wdfNames = stbof.getFileNames(FileFilters.Wdf);
    Collections.sort(wdfNames);

    TreePath selPath = tree.getSelectionPath();
    wdfDrawn = null;

    DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
    treeModel.setRoot(new WDFRootNode(stbof.getSourceFile().getName(), wdfNames));

    // restore selection
    if (selPath != null) {
      val selNode = (DefaultMutableTreeNode) selPath.getLastPathComponent();
      Object[] path = selNode.getUserObjectPath();
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getModel().getRoot();

      // search for previous selected path
      if (path[0].equals(node.getUserObject())) {
        for (int i = 1; i < path.length; ++i) {
          Enumeration<?> childs = node.children();
          boolean childFound = false;
          while (childs.hasMoreElements()) {
            val child = childs.nextElement();
            if (child instanceof DefaultMutableTreeNode) {
              val mtn = (DefaultMutableTreeNode) child;
              if (path[i].equals(mtn.getUserObject())) {
                node = i == 1 ? loadAndExpandWDFFile(mtn) : mtn;
                childFound = true;
                break;
              }
            }
          }
          if (!childFound)
            break;
        }
      }

      selPath = new TreePath(node.getPath());
      tree.setSelectionPath(selPath);
    }
  }

  private void placeComponents() {
    setLayout(new GridBagLayout());
    scrTree = new JScrollPane(tree);

    JPanel pnlMenu = new JPanel();
    pnlMenu.setLayout(new BoxLayout(pnlMenu, BoxLayout.X_AXIS));
    pnlMenu.add(btnAddButton);
    pnlMenu.add(btnAddListBox);
    pnlMenu.add(btnAddMenu);
    pnlMenu.add(btnAddPicture);
    pnlMenu.add(btnAddScrollBar);
    pnlMenu.add(btnAddSlider);
    pnlMenu.add(btnAddTextBox);
    pnlMenu.add(Box.createHorizontalStrut(5));
    pnlMenu.add(btnRemove);

    JPanel displayPanel = new JPanel(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;

    c.weightx = 1.0;
    c.weighty = 1.0;
    c.gridwidth = 3;
    JScrollPane scrDisplay = new JScrollPane(display);
    displayPanel.add(scrDisplay, c);
    c.gridy++;
    c.weighty = 0;
    c.gridwidth = 1;
    displayPanel.add(Box.createHorizontalGlue(), c);
    c.gridx++;
    c.weightx = 0;
    displayPanel.add(pnlMenu, c);
    c.gridx++;
    c.weightx = 1.0;
    displayPanel.add(Box.createHorizontalGlue(), c);

    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrTree, displayPanel);
    splitPane.setDividerLocation(DIVIDER_LOCATION);
    splitPane.setOneTouchExpandable(true);

    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1.0;
    c.weighty = 1.0;
    add(splitPane, c);

    // set minimal window size for 800x600 screen rendering
    final int divSize = splitPane.getDividerSize();
    add(Box.createHorizontalStrut(800 + DIVIDER_LOCATION + divSize + 4), c);
    add(Box.createVerticalStrut(600 + 5), c);
  }

  private WDFFileNode loadAndExpandWDFFile(DefaultMutableTreeNode node) throws IOException {
    if (node instanceof WDFFileNode)
      return (WDFFileNode) node;

    ignoreNodeSelection = true;
    final int[] selected = tree.getSelectionRows();
    String file = node.getUserObject().toString();

    // cache wdf to detect changes on save
    WindowDefinitionFile wdf = stbof.getWDF(file, LoadFlags.CACHE);

    DefaultMutableTreeNode parent = (DefaultMutableTreeNode) node.getParent();
    int index = parent.getIndex(node);

    // replace WDFRootTreeNode
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    wdfDrawn = new WDFFileNode(stbof, wdf, defaultChangeListener, imgCache);
    model.insertNodeInto(wdfDrawn, parent, index);
    model.removeNodeFromParent(node);

    tree.setVisible(false);
    tree.setVisible(true);

    // restore selection
    if (selected.length > 0)
      tree.setSelectionRows(selected);

    ignoreNodeSelection = false;
    return wdfDrawn;
  }

  private void updateImage(TreePath path) {
    if (path.getPathCount() < 2) {
      clearImage();
      return;
    }

    TreeNode node = (TreeNode) path.getPathComponent(1);

    if (node instanceof WDFFileNode) {
      clearImage();
      image = wdfDrawn.paintOnImage(image);
      display.setIcon(new ImageIcon(image));

      // now paint selection
      Rectangle rect, rect2 = null, tmpRect;

      TreeNode leafNode = (TreeNode) path.getLastPathComponent();
      int index = path.getPathCount() - 1;

      while (!(leafNode instanceof WDFWidgetNode)) {
        leafNode = leafNode.getParent();
        index--;
      }

      WDFWidgetNode widget = (WDFWidgetNode) leafNode;
      rect = widget.getRectangle();

      for (int i = index - 1; i > 1; i--) {
        widget = (WDFWidgetNode) path.getPathComponent(i);
        tmpRect = widget.getRectangle();

        if (rect2 == null) {
          rect2 = tmpRect;
        } else {
          rect2.x += tmpRect.x;
          rect2.y += tmpRect.y;
        }

        rect.x += tmpRect.x;
        rect.y += tmpRect.y;
      }

      if (rect2 != null && (rect.x + rect.width > rect2.width + rect2.x
            || rect.y + rect.height > rect2.height + rect2.y)) {
        drawSelectionBox(rect2, Color.YELLOW);
      }

      drawSelectionBox(rect, Color.RED);
    }
  }

  private void clearImage() {
    Graphics g = image.getGraphics();
    g.setColor(Color.BLACK);
    g.fillRect(0, 0, image.getWidth(), image.getHeight());
  }

  private void drawSelectionBox(Rectangle rect, Color color) {
    Graphics2D g = image.createGraphics();
    g.setStroke(new BasicStroke(5));
    g.setColor(color);
    g.drawRect(rect.x - 3, rect.y - 3, rect.width + 6, rect.height + 6);
  }

  private void resizeNodes(TreePath path) {
    ignoreNodeSelection = true;
    tree.setVisible(false);

    TreePath selpath = tree.getSelectionPath();

    TreeNode node = (TreeNode) path.getLastPathComponent();
    Enumeration<?> en = node.children();

    // workaround to fix node heights and to not cut on the right side
    while (en.hasMoreElements()) {
      TreeNode next = (TreeNode) en.nextElement();
      tree.startEditingAtPath(path.pathByAddingChild(next));
    }

    if (selpath != null)
      tree.setSelectionPath(selpath);

    tree.setVisible(true);
    ignoreNodeSelection = false;
  }

  private void updateDisplayMenu() {
    boolean enabled = wdfDrawn != null;
    btnAddButton.setEnabled(enabled);
    btnAddListBox.setEnabled(enabled);
    btnAddMenu.setEnabled(enabled);
    btnAddPicture.setEnabled(enabled);
    btnAddScrollBar.setEnabled(enabled);
    btnAddSlider.setEnabled(enabled);
    btnAddTextBox.setEnabled(enabled);
    btnRemove.setEnabled(enabled);
  }

  @Override
  public boolean hasChanges() {
    return stbof.hasBeenModified(FileFilters.Wdf);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(FileFilters.Wdf, changes);
  }

  @Override
  public void reload() throws IOException {
    loadTree();
  }

  @Override
  public void finalWarning() {
    TreePath path = tree.getSelectionPath();
    if (path != null) {
      TreeNode node = ((TreeNode) path.getLastPathComponent());
      if (node instanceof WDFDataNode)
        ((WDFDataNode) node).commitChanges();
    }
  }
}
