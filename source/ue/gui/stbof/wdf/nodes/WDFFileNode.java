package ue.gui.stbof.wdf.nodes;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.gui.stbof.wdf.nodes.widgets.WDFWindowNode;
import ue.util.img.ImageCache;

/**
 * Base on the window node to list all included widgets and data nodes.
 */
public class WDFFileNode extends WDFWindowNode {

  private static final long serialVersionUID = -1609131564838960187L;

  public WDFFileNode(Stbof res, WindowDefinitionFile wdf, ActionListener listener,
      ImageCache imgCache) throws IOException {
    super(wdf.getWindow(), res, imgCache, wdf.getName());

    addActionListener(listener);
  }

  public BufferedImage paintOnImage(BufferedImage image) {
    Rectangle size = getRectangle();

    int w = Math.max(800, size.width + size.x);
    int h = Math.max(600, size.height + size.y);

    // resize black background rendering
    if (image.getWidth() != w || image.getHeight() != h) {
      BufferedImage tmp = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
      Graphics g = tmp.getGraphics();
      g.setColor(Color.BLACK);
      g.fillRect(0, 0, tmp.getWidth(), tmp.getHeight());
      g.drawImage(tmp, 0, 0, null);
      image = tmp;
    }

    // render window background & widgets
    paintOnImage(0, 0, image.getWidth(), image.getHeight(), image);

    return image;
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    return null;
  }

}
