package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.widgets.WDFButton;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.gui.stbof.wdf.nodes.data.WDFBooleanNode;
import ue.gui.stbof.wdf.nodes.data.WDFIntegerNode;
import ue.gui.stbof.wdf.nodes.data.WDFOptionNode;
import ue.gui.util.GuiTools;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;

public class WDFButtonNode extends WDFWidgetNode {

  private JPanel cellEditor;
  private JCheckBox display;
  private JLabel lbl;
  private WDFButton button;

  private WDFOptionNode normalOption;
  private WDFOptionNode selectedOption;
  private boolean normalImageVisible = true;
  private boolean selectedImageVisible = false;

  public WDFButtonNode(final WDFButton button,
      Stbof res, ImageCache imgCache) {
    super(button, res, imgCache, null);
    this.button = button;

    add(new WDFIntegerNode("Unknown", button.getUnknown(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        button.setUnknown(value);
      }
    });

    ArrayList<String> types = new ArrayList<String>();
    types.add("Push button");
    types.add("Unknown type (%1)".replace("%1", "1"));
    types.add("Toggle button");

    while (button.getButtonType() >= types.size()) {
      types.add("Unknown type (%1)".replace("%1", Integer.toString(types.size())));
    }

    add(new WDFOptionNode("Type",
        types.get(button.getButtonType()),
        types, false, true
    ) {
      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        button.setButtonType(index);
      }
    });

    add(new WDFBooleanNode("Repeat clicks", button.isRepeating()) {
      @Override
      public void setBoolean(boolean value) {
        button.setRepeatingClicks(value);
      }
    });

    add(new WDFIntegerNode("Repeat rate", button.getRepeatsPerSecond(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        button.setRepeatsPerSecond(value);
      }
    });

    ArrayList<String> images = res.getFileNames(FileFilters.Tga);
    images.add(0, WDFWidget.NO_IMAGE);

    normalOption = new WDFOptionNode("Image",
        button.getNormalImage(),
        images, true, normalImageVisible
    ) {
      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        button.setNormalImage(item);
        normalImageVisible = isEnabled;
        if (isEnabled)
          selectedOption.setOptionEnabled(false);
      }
    };
    add(normalOption);

    selectedOption = new WDFOptionNode("Selected image",
        button.getSelectedImage(),
        images, true, selectedImageVisible
    ) {
      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        button.setSelectedImage(item);
        selectedImageVisible = isEnabled;
        if (isEnabled)
          normalOption.setOptionEnabled(false);
      }
    };
    add(selectedOption);
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (this.visible) {
      Graphics gimg = image.getGraphics();
      Image img;

      x += button.getPosition().x;
      y += button.getPosition().y;
      w = Math.min(button.getDimension().width, w - button.getPosition().x);
      h = Math.min(button.getDimension().height, h - button.getPosition().y);

      if (this.backgroundVisible) {
        if (!button.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(button.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      if (normalImageVisible) {
        if (!button.getNormalImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(button.getNormalImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }
      if (selectedImageVisible) {
        if (!button.getSelectedImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(button.getSelectedImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(button.getPosition(), button.getDimension());
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (cellEditor == null) {
      cellEditor = new JPanel(new BorderLayout());
      cellEditor.setOpaque(false);

      display = new JCheckBox();
      display.setSelected(true);
      display.setOpaque(false);
      display.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          visible = display.isSelected();
          fireChangeNotification();
        }
      });

      cellEditor.add(display, BorderLayout.WEST);

      lbl = new JLabel(this.getUserObject().toString());
      cellEditor.add(lbl, BorderLayout.CENTER);
    }

    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }

}
