package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.data.WDFFont;
import ue.edit.res.stbof.files.wdf.widgets.WDFListBox;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.gui.stbof.wdf.nodes.data.WDFFontNode;
import ue.gui.stbof.wdf.nodes.data.WDFIntegerNode;
import ue.gui.util.GuiTools;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;

public class WDFListBoxNode extends WDFWidgetNode {

  private JPanel cellEditor;
  private JCheckBox display;
  private JLabel lbl;

  private WDFListBox listBox;

  private WDFFontNode normalOption;
  private WDFFontNode selectedOption;

  private boolean fontVisible = true;
  private boolean selectedFontVisible = false;

  public WDFListBoxNode(final WDFListBox listBox,
      Stbof res, ImageCache imgCache) {
    super(listBox, res, imgCache, null);
    this.listBox = listBox;

    add(new WDFIntegerNode("Unknown 1", listBox.getUnknown1(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        listBox.setUnknown1(value);
      }
    });

    ArrayList<String> files = res.getFileNames(FileFilters.Fnt);
    files.add(0, WDFWidget.NO_IMAGE);

    normalOption = new WDFFontNode("Font", listBox.getFont1(), true,
        true, files) {
      @Override
      public void setFont(boolean isEnabled, WDFFont font) {
        listBox.setFont1(font);
        fontVisible = isEnabled;
        if (fontVisible)
          selectedOption.setOptionEnabled(false);
      }
    };
    add(normalOption);

    selectedOption = new WDFFontNode("Selected text font", listBox.getFont2(), true,
        false, files) {
      @Override
      public void setFont(boolean isEnabled, WDFFont font) {
        listBox.setFont2(font);
        selectedFontVisible = isEnabled;
        if (selectedFontVisible)
          normalOption.setOptionEnabled(false);
      }
    };
    add(selectedOption);

    for (int i = 0; i < 10; i++) {
      final int index = i;
      add(new WDFIntegerNode("Columns Width " + (i + 1), listBox.getColumnWidth(i), 5,
          Integer.MIN_VALUE, Integer.MAX_VALUE) {
        @Override
        public void setInteger(int value) {
          listBox.setColumnWidth(index, value);
        }
      });
    }

    add(new WDFIntegerNode("Columns", listBox.getColumns(), 5, Integer.MIN_VALUE, Integer.MAX_VALUE) {
    @Override
    public void setInteger(int value) {
      listBox.setColumns(value);
    }
  });
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (this.visible) {
      Graphics gimg = image.getGraphics();
      Image img;

      x += listBox.getPosition().x;
      y += listBox.getPosition().y;
      w = Math.min(listBox.getDimension().width, w - listBox.getPosition().x);
      h = Math.min(listBox.getDimension().height, h - listBox.getPosition().y);

      if (this.backgroundVisible) {
        if (!listBox.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(listBox.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      // font
      WDFFont font = listBox.getFont1();
      WDFFont selfont = listBox.getFont2();
      int leftWidth = 0;

      if (fontVisible) {
        if (!font.getFontFile().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          int color = font.getRGBColor();
          int[] rgb = new int[]{(color >>> 16) & 0xFF, (color >>> 8) & 0xFF,
              (color & 0xFF)};
          float[] hsb = new float[3];
          Color.RGBtoHSB(rgb[0], rgb[1], rgb[2], hsb);

          Image character;
          BufferedImage bimg;

          String text = "Sample list text.";

          for (int i = 0; i < text.length(); i++) {
            character = imgCache.getCharacterImage(font.getFontFile(), text.charAt(i));
            bimg = null;

            if (character != null) {
              bimg = new BufferedImage(character.getWidth(null),
                  character.getHeight(null), BufferedImage.TYPE_INT_ARGB);

              GuiTools.drawImage(character, 0, 0, bimg.getWidth(),
                  bimg.getHeight(), bimg.getGraphics());

              if (font.overrideExeDefaults()) {
                int pixel;
                float[] ohsb = new float[3];

                for (int xi = 0; xi < bimg.getWidth(); xi++) {
                  for (int yi = 0; yi < bimg.getHeight(); yi++) {
                    pixel = bimg.getRGB(xi, yi);

                    if ((pixel & 0xFFFFFF) != 0) {
                      Color.RGBtoHSB((pixel >>> 16) & 0xFF,
                          (pixel >>> 8) & 0xFF, pixel & 0xFF, ohsb);

                      pixel = Color.HSBtoRGB(hsb[0], hsb[1], ohsb[2]);

                      bimg.setRGB(xi, yi, pixel);
                    }
                  }
                }
              }

              GuiTools.drawImage(bimg, x + leftWidth, y,
                  w - leftWidth, h, gimg);
              leftWidth += bimg.getWidth();
            }
          }
        }
      }

      // sel font
      if (selectedFontVisible) {
        if (!selfont.getFontFile().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          int color = selfont.getRGBColor();
          int[] rgb = new int[]{(color >>> 16) & 0xFF, (color >>> 8) & 0xFF,
              (color & 0xFF)};
          float[] hsb = new float[3];
          Color.RGBtoHSB(rgb[0], rgb[1], rgb[2], hsb);

          Image character;
          BufferedImage bimg;

          String text = "Sample list text.";

          for (int i = 0; i < text.length(); i++) {
            character = imgCache.getCharacterImage(selfont.getFontFile(), text.charAt(i));
            bimg = null;

            if (character != null) {
              bimg = new BufferedImage(character.getWidth(null),
                  character.getHeight(null), BufferedImage.TYPE_INT_ARGB);

              GuiTools.drawImage(character, 0, 0, bimg.getWidth(),
                  bimg.getHeight(), bimg.getGraphics());

              if (font.overrideExeDefaults()) {
                int pixel;
                float[] ohsb = new float[3];

                for (int xi = 0; xi < bimg.getWidth(); xi++) {
                  for (int yi = 0; yi < bimg.getHeight(); yi++) {
                    pixel = bimg.getRGB(xi, yi);

                    if ((pixel & 0xFFFFFF) != 0) {
                      Color.RGBtoHSB((pixel >>> 16) & 0xFF,
                          (pixel >>> 8) & 0xFF, pixel & 0xFF, ohsb);

                      pixel = Color.HSBtoRGB(hsb[0], hsb[1], ohsb[2]);

                      bimg.setRGB(xi, yi, pixel);
                    }
                  }
                }
              }

              GuiTools.drawImage(bimg, x + leftWidth, y,
                  w - leftWidth, h, gimg);
              leftWidth += bimg.getWidth();
            }
          }
        }
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(listBox.getPosition(), listBox.getDimension());
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (cellEditor == null) {
      cellEditor = new JPanel(new BorderLayout());
      cellEditor.setOpaque(false);

      display = new JCheckBox();
      display.setSelected(true);
      display.setOpaque(false);
      display.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          visible = display.isSelected();
          fireChangeNotification();
        }
      });

      cellEditor.add(display, BorderLayout.WEST);

      lbl = new JLabel(this.getUserObject().toString());
      cellEditor.add(lbl, BorderLayout.CENTER);
    }

    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }
}
