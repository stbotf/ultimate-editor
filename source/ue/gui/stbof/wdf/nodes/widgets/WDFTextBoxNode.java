package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.data.WDFFont;
import ue.edit.res.stbof.files.wdf.widgets.WDFTextBox;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.gui.stbof.wdf.nodes.data.WDFDimensionNode;
import ue.gui.stbof.wdf.nodes.data.WDFFontNode;
import ue.gui.stbof.wdf.nodes.data.WDFPointNode;
import ue.gui.util.GuiTools;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;

public class WDFTextBoxNode extends WDFWidgetNode {

  private static final long serialVersionUID = -8965105591494934914L;

  private JPanel cellEditor;
  private JCheckBox display;
  private JLabel lbl;

  private WDFTextBox textBox;

  private boolean fontVisible = true;

  public WDFTextBoxNode(final WDFTextBox textBox,
      Stbof res, ImageCache imgCache) {
    super(textBox, res, imgCache, null);
    this.textBox = textBox;

    add(new WDFPointNode("Text area position", textBox.getTextPosition()) {
      private static final long serialVersionUID = 2018509794219892882L;

      @Override
      public void setPoint(Point point) {
        textBox.setTextPosition(point);
      }
    });
    add(new WDFDimensionNode("Text area dimension", textBox.getTextDimension()) {
      private static final long serialVersionUID = 4758637339931025936L;

      @Override
      public void setDimension(Dimension dim) {
        textBox.setTextDimension(dim);
      }
    });

    ArrayList<String> files = res.getFileNames(FileFilters.Fnt);
    files.add(0, WDFWidget.NO_IMAGE);

    add(new WDFFontNode("Font", textBox.getFont(), true,
        true, files) {
      private static final long serialVersionUID = -7373475242195419408L;

      @Override
      public void setFont(boolean isEnabled, WDFFont font) {
        textBox.setFont(font);
        fontVisible = isEnabled;
      }
    });
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (this.visible) {
      Image img;
      Graphics gimg = image.getGraphics();

      x += textBox.getPosition().x;
      y += textBox.getPosition().y;
      w = Math.min(textBox.getDimension().width, w - textBox.getPosition().x);
      h = Math.min(textBox.getDimension().height, h - textBox.getPosition().y);

      if (this.backgroundVisible) {
        if (!textBox.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(textBox.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      // draw text area
      Point textPos = textBox.getTextPosition();
      Dimension textDim = textBox.getTextDimension();

      textDim.width = Math.min(textDim.width, w - textPos.x);
      textDim.height = Math.min(textDim.height, h - textPos.y);

      textPos.x += x;
      textPos.y += y;

      Graphics g = image.getGraphics();
      g.setColor(Color.GREEN);
      g.drawRect(textPos.x - 1, textPos.y - 1, textDim.width + 2, textDim.height + 2);

      // font
      WDFFont font = textBox.getFont();
      int leftWidth = 0;

      if (fontVisible) {
        if (!font.getFontFile().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          int color = font.getRGBColor();
          int[] rgb = new int[]{(color >>> 16) & 0xFF, (color >>> 8) & 0xFF,
              (color & 0xFF)};
          float[] hsb = new float[3];
          Color.RGBtoHSB(rgb[0], rgb[1], rgb[2], hsb);

          Image character;
          BufferedImage bimg;

          String text = "Sample textbox text.";

          for (int i = 0; i < text.length(); i++) {
            character = imgCache.getCharacterImage(font.getFontFile(), text.charAt(i));
            bimg = null;

            if (character != null) {
              bimg = new BufferedImage(character.getWidth(null),
                  character.getHeight(null), BufferedImage.TYPE_INT_ARGB);

              GuiTools.drawImage(character, 0, 0, bimg.getWidth(),
                  bimg.getHeight(), bimg.getGraphics());

              if (font.overrideExeDefaults()) {
                int pixel;
                float[] ohsb = new float[3];

                for (int xi = 0; xi < bimg.getWidth(); xi++) {
                  for (int yi = 0; yi < bimg.getHeight(); yi++) {
                    pixel = bimg.getRGB(xi, yi);

                    if ((pixel & 0xFFFFFF) != 0) {
                      Color.RGBtoHSB((pixel >>> 16) & 0xFF,
                          (pixel >>> 8) & 0xFF, pixel & 0xFF, ohsb);

                      pixel = Color.HSBtoRGB(hsb[0], hsb[1], ohsb[2]);

                      bimg.setRGB(xi, yi, pixel);
                    }
                  }
                }
              }

              GuiTools.drawImage(bimg, textPos.x + leftWidth, textPos.y,
                  textDim.width - leftWidth, textDim.height, gimg);
              leftWidth += bimg.getWidth();
            }
          }
        }
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(textBox.getPosition(), textBox.getDimension());
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (cellEditor == null) {
      cellEditor = new JPanel(new BorderLayout());
      cellEditor.setOpaque(false);

      display = new JCheckBox();
      display.setSelected(true);
      display.setOpaque(false);
      display.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          visible = display.isSelected();
          fireChangeNotification();
        }
      });

      cellEditor.add(display, BorderLayout.WEST);

      lbl = new JLabel(this.getUserObject().toString());
      cellEditor.add(lbl, BorderLayout.CENTER);
    }

    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }

}
