package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.widgets.WDFButton;
import ue.edit.res.stbof.files.wdf.widgets.WDFListBox;
import ue.edit.res.stbof.files.wdf.widgets.WDFMenu;
import ue.edit.res.stbof.files.wdf.widgets.WDFPicture;
import ue.edit.res.stbof.files.wdf.widgets.WDFScrollBar;
import ue.edit.res.stbof.files.wdf.widgets.WDFSlider;
import ue.edit.res.stbof.files.wdf.widgets.WDFTextBox;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget.WDFWidgetType;
import ue.edit.res.stbof.files.wdf.widgets.WDFWindow;
import ue.gui.util.GuiTools;
import ue.util.img.ImageCache;

public abstract class WDFWindowNode extends WDFWidgetNode {

  @Getter @Accessors(fluent = true) private WDFWindow window;

  public WDFWindowNode(WDFWindow window, Stbof res,
      ImageCache imgCache, String name) throws IOException {
    super(window, res, imgCache, name);
    this.window = window;

    // windows
    List<WDFWidget> wnd_widgets = window.getWidgets(WDFWidgetType.WINDOW);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFSubWindowNode((WDFWindow) wid, res, imgCache));
      }
    }

    // buttons
    wnd_widgets = window.getWidgets(WDFWidgetType.BUTTON);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFButtonNode((WDFButton) wid, res, imgCache));
      }
    }

    // sliders
    wnd_widgets = window.getWidgets(WDFWidgetType.SLIDER);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFSliderNode((WDFSlider) wid, res, imgCache));
      }
    }

    // scrollbars
    wnd_widgets = window.getWidgets(WDFWidgetType.SCROLLBAR);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFScrollBarNode((WDFScrollBar) wid, res, imgCache));
      }
    }

    // listboxes
    wnd_widgets = window.getWidgets(WDFWidgetType.LISTBOX);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFListBoxNode((WDFListBox) wid, res, imgCache));
      }
    }

    // pictureboxes
    wnd_widgets = window.getWidgets(WDFWidgetType.PICTUREBOX);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFPictureNode((WDFPicture) wid, res, imgCache));
      }
    }

    // textboxes
    wnd_widgets = window.getWidgets(WDFWidgetType.TEXTBOX);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFTextBoxNode((WDFTextBox) wid, res, imgCache));
      }
    }

    // menus
    wnd_widgets = window.getWidgets(WDFWidgetType.MENU);
    if (wnd_widgets != null) {
      for (WDFWidget wid : wnd_widgets) {
        add(new WDFMenuNode((WDFMenu) wid, res, imgCache));
      }
    }
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (visible) {
      x += window.getPosition().x;
      y += window.getPosition().y;
      w = Math.min(window.getDimension().width, w - window.getPosition().x);
      h = Math.min(window.getDimension().height, h - window.getPosition().y);

      if (this.backgroundVisible) {
        Graphics2D gimg = image.createGraphics();

        if (!window.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          Image img = imgCache.getImage(window.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      Enumeration<?> childs = children();
      while (childs.hasMoreElements()) {
        Object child = childs.nextElement();
        if (child instanceof WDFWidgetNode)
          ((WDFWidgetNode)child).paintOnImage(x, y, w, h, image);
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(window.getPosition(), window.getDimension());
  }

}
