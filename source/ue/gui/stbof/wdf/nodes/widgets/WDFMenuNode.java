package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.data.WDFFont;
import ue.edit.res.stbof.files.wdf.widgets.WDFMenu;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.gui.stbof.wdf.nodes.data.WDFDimensionNode;
import ue.gui.stbof.wdf.nodes.data.WDFFontNode;
import ue.gui.stbof.wdf.nodes.data.WDFOptionNode;
import ue.gui.stbof.wdf.nodes.data.WDFPointNode;
import ue.gui.util.GuiTools;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;

public class WDFMenuNode extends WDFWidgetNode {

  private static final long serialVersionUID = 2492055006452601875L;

  private JPanel cellEditor;
  private JCheckBox display;
  private JLabel lbl;

  private WDFMenu menu;

  private WDFFontNode normalOption;
  private WDFFontNode selectedOption;

  private boolean leftImageVisible = true;
  private boolean topImageVisible = true;
  private boolean rightImageVisible = true;
  private boolean bottomImageVisible = true;
  private boolean fontVisible = true;
  private boolean selectedFontVisible = false;

  public WDFMenuNode(final WDFMenu menu,
      Stbof res, ImageCache imgCache) {
    super(menu, res, imgCache, null);
    this.menu = menu;

    add(new WDFPointNode("Unknown position (?)", menu.getMenuPosition()) {
      private static final long serialVersionUID = 853534063073139306L;

      @Override
      public void setPoint(Point point) {
        menu.setMenuPosition(point);
      }
    });

    add(new WDFDimensionNode("Unknown dimension (?)", menu.getMenuDimension()) {
      private static final long serialVersionUID = -2303499375343434086L;

      @Override
      public void setDimension(Dimension dim) {
        menu.setMenuDimension(dim);
      }
    });

    ArrayList<String> files = res.getFileNames(FileFilters.Fnt);

    normalOption = new WDFFontNode("Font", menu.getFont1(), true, true, files) {
      private static final long serialVersionUID = 2766138050114545361L;

      @Override
      public void setFont(boolean isEnabled, WDFFont font) {
        menu.setFont1(font);
        fontVisible = isEnabled;
        if (fontVisible)
          selectedOption.setOptionEnabled(false);
      }
    };
    add(normalOption);

    selectedOption = new WDFFontNode("Selected text font", menu.getFont2(), true,
        false, files) {
      private static final long serialVersionUID = -1821328782165643976L;

      @Override
      public void setFont(boolean isEnabled, WDFFont font) {
        menu.setFont2(font);
        selectedFontVisible = isEnabled;
        if (selectedFontVisible)
          normalOption.setOptionEnabled(false);
      }
    };
    add(selectedOption);

    files = res.getFileNames(FileFilters.Mnu);
    files.add(0, WDFWidget.NO_IMAGE);

    add(new WDFOptionNode("Menu file",
        menu.getMenuFile(),
        files, false, true
    ) {
      private static final long serialVersionUID = 1558696029749549430L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        menu.setMenuFile(item);
      }
    });

    files = res.getFileNames(FileFilters.Tga);
    files.add(0, WDFWidget.NO_IMAGE);

    add(new WDFOptionNode("Left image",
        menu.getLeftImage(),
        files, true, leftImageVisible
    ) {
      private static final long serialVersionUID = 5718213943567356488L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        menu.setLeftImage(item);
        leftImageVisible = isEnabled;
      }
    });

    add(new WDFOptionNode("Top image",
        menu.getTopImage(),
        files, true, topImageVisible
    ) {
      private static final long serialVersionUID = -7067703812901275440L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        menu.setTopImage(item);
        topImageVisible = isEnabled;
      }
    });

    add(new WDFOptionNode("Right image",
        menu.getRightImage(),
        files, true, rightImageVisible
    ) {
      private static final long serialVersionUID = -7696197056532519464L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        menu.setRightImage(item);
        rightImageVisible = isEnabled;
      }
    });

    add(new WDFOptionNode("Bottom image",
        menu.getBottomImage(),
        files, true, bottomImageVisible
    ) {
      private static final long serialVersionUID = -9080638173251753666L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        menu.setBottomImage(item);
        bottomImageVisible = isEnabled;
      }
    });
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (this.visible) {
      Graphics gimg = image.getGraphics();
      Image img;

      x += menu.getPosition().x;
      y += menu.getPosition().y;
      w = Math.min(menu.getDimension().width, w - menu.getPosition().x);
      h = Math.min(menu.getDimension().height, h - menu.getPosition().y);

      if (this.backgroundVisible) {
        if (!menu.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(menu.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      if (!menu.getTopImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
        img = imgCache.getImage(menu.getTopImage());

        if (topImageVisible) {
          GuiTools.drawImage(img, x, y, 800, 600, gimg);
        }

        y += img.getHeight(null);
        w = img.getWidth(null);
      }

      int leftWidth = 0;

      if (!menu.getLeftImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
        img = imgCache.getImage(menu.getLeftImage());
        if (leftImageVisible) {
          GuiTools.drawImage(img, x, y, 800, 600, gimg);
        }

        leftWidth = img.getWidth(null);
      }

      if (rightImageVisible) {
        if (!menu.getRightImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(menu.getRightImage());
          GuiTools.drawImage(img, x + w - img.getWidth(null), y,
              w, h, gimg);
        }
      }

      // font
      WDFFont font = menu.getFont1();
      WDFFont selfont = menu.getFont2();
      int fheight = 0;

      if (!font.getFontFile().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
        Image character = imgCache.getCharacterImage(font.getFontFile(), 'S');
        fheight = character.getHeight(null);

        if (fontVisible) {
          int color = font.getRGBColor();
          int[] rgb = new int[]{(color >>> 16) & 0xFF, (color >>> 8) & 0xFF,
              (color & 0xFF)};
          float[] hsb = new float[3];
          Color.RGBtoHSB(rgb[0], rgb[1], rgb[2], hsb);

          BufferedImage bimg;

          String text = "Sample menu text.";

          for (int i = 0; i < text.length(); i++) {
            character = imgCache.getCharacterImage(font.getFontFile(), text.charAt(i));
            bimg = null;

            if (character != null) {
              bimg = new BufferedImage(character.getWidth(null),
                  character.getHeight(null), BufferedImage.TYPE_INT_ARGB);

              GuiTools.drawImage(character, 0, 0, bimg.getWidth(),
                  bimg.getHeight(), bimg.getGraphics());

              if (font.overrideExeDefaults()) {
                int pixel;
                float[] ohsb = new float[3];

                for (int xi = 0; xi < bimg.getWidth(); xi++) {
                  for (int yi = 0; yi < bimg.getHeight(); yi++) {
                    pixel = bimg.getRGB(xi, yi);

                    if ((pixel & 0xFFFFFF) != 0) {
                      Color.RGBtoHSB((pixel >>> 16) & 0xFF,
                          (pixel >>> 8) & 0xFF, pixel & 0xFF, ohsb);

                      pixel = Color.HSBtoRGB(hsb[0], hsb[1], ohsb[2]);

                      bimg.setRGB(xi, yi, pixel);
                    }
                  }
                }
              }

              GuiTools.drawImage(bimg, x + leftWidth, y,
                  800, 600, gimg);
              leftWidth += bimg.getWidth();
            }
          }
        }
      }

      // sel font
      if (!selfont.getFontFile().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
        Image character = imgCache.getCharacterImage(selfont.getFontFile(), 'S');
        fheight = character.getHeight(null);

        if (selectedFontVisible) {
          int color = selfont.getRGBColor();
          int[] rgb = new int[]{(color >>> 16) & 0xFF, (color >>> 8) & 0xFF,
              (color & 0xFF)};
          float[] hsb = new float[3];
          Color.RGBtoHSB(rgb[0], rgb[1], rgb[2], hsb);

          BufferedImage bimg;

          String text = "Sample menu text.";

          for (int i = 0; i < text.length(); i++) {
            character = imgCache.getCharacterImage(selfont.getFontFile(), text.charAt(i));
            bimg = null;

            if (character != null) {
              bimg = new BufferedImage(character.getWidth(null),
                  character.getHeight(null), BufferedImage.TYPE_INT_ARGB);

              GuiTools.drawImage(character, 0, 0, bimg.getWidth(),
                  bimg.getHeight(), bimg.getGraphics());

              if (font.overrideExeDefaults()) {
                int pixel;
                float[] ohsb = new float[3];

                for (int xi = 0; xi < bimg.getWidth(); xi++) {
                  for (int yi = 0; yi < bimg.getHeight(); yi++) {
                    pixel = bimg.getRGB(xi, yi);

                    if ((pixel & 0xFFFFFF) != 0) {
                      Color.RGBtoHSB((pixel >>> 16) & 0xFF,
                          (pixel >>> 8) & 0xFF, pixel & 0xFF, ohsb);

                      pixel = Color.HSBtoRGB(hsb[0], hsb[1], ohsb[2]);

                      bimg.setRGB(xi, yi, pixel);
                    }
                  }
                }
              }

              GuiTools.drawImage(bimg, x + leftWidth, y,
                  800, 600, gimg);
              leftWidth += bimg.getWidth();
            }
          }
        }
      }

      if (bottomImageVisible) {
        if (!menu.getBottomImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(menu.getBottomImage());
          GuiTools.drawImage(img, x, y + fheight,
              800, 600, gimg);
        }
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(menu.getPosition(), menu.getDimension());
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (cellEditor == null) {
      cellEditor = new JPanel(new BorderLayout());
      cellEditor.setOpaque(false);

      display = new JCheckBox();
      display.setSelected(true);
      display.setOpaque(false);
      display.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          visible = display.isSelected();
          fireChangeNotification();
        }
      });

      cellEditor.add(display, BorderLayout.WEST);

      lbl = new JLabel(this.getUserObject().toString());
      cellEditor.add(lbl, BorderLayout.CENTER);
    }

    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }

}
