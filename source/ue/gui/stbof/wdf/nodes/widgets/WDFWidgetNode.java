package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Enumeration;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget.WDFWidgetType;
import ue.gui.stbof.wdf.nodes.data.WDFDataNode;
import ue.gui.stbof.wdf.nodes.data.WDFDimensionNode;
import ue.gui.stbof.wdf.nodes.data.WDFIntegerNode;
import ue.gui.stbof.wdf.nodes.data.WDFOptionNode;
import ue.gui.stbof.wdf.nodes.data.WDFPointNode;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;

public abstract class WDFWidgetNode extends WDFDataNode {

  private static final long serialVersionUID = 4597009512162972118L;

  protected boolean visible = true;
  protected boolean backgroundVisible = true;
  @Getter @Accessors(fluent = true)
  protected WDFWidget widget;
  protected ImageCache imgCache;

  public WDFWidgetNode(final WDFWidget widget, Stbof res, ImageCache imgCache, String name) {
      super(name != null ? name : defaultWidgetName(widget));
    this.imgCache = imgCache;
    this.widget = widget;

    add(new WDFIntegerNode("Widget ID", widget.id(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = -7146198557346126823L;

      @Override
      public void setInteger(int value) {
        if (widget.setId(value)) {
          String typeName = WDFWidgetType.toString(widget.type());
          WDFDataNode parent = ((WDFDataNode) this.getParent());
          parent.setUserObject(typeName + "(" + widget.id() + ")");
        }
      }
    });

    add(new WDFPointNode("Position", widget.getPosition()) {
      private static final long serialVersionUID = 3842198313915565797L;

      @Override
      public void setPoint(Point point) {
        widget.setPosition(point);
      }
    });
    add(new WDFDimensionNode("Dimension", widget.getDimension()) {
      private static final long serialVersionUID = 3485729106432199934L;

      @Override
      public void setDimension(Dimension dim) {
        widget.setDimension(dim);
      }
    });

    ArrayList<String> images = res.getFileNames(FileFilters.Tga);
    images.add(0, WDFWidget.NO_IMAGE);

    if (!images.contains(widget.getBackgroundImage())) {
      images.add(widget.getBackgroundImage());
    }

    add(new WDFOptionNode("Background",
        widget.getBackgroundImage(),
        images, true, true
    ) {
      private static final long serialVersionUID = -2541614799376455103L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        widget.setBackgroundImage(item);
        backgroundVisible = isEnabled;
      }
    });
  }

  public WindowDefinitionFile wdf() {
    return widget.wdf();
  }

  public int type() {
    return widget.type();
  }

  public int numDataNodes() {
    // instance id, position, dimension and background
    return 4;
  }

  public abstract void paintOnImage(int x, int y, int w, int h, BufferedImage image);

  public abstract Rectangle getRectangle();

  @Override
  public void commitChanges() {
    Enumeration<?> en = children();

    while (en.hasMoreElements()) {
      Object child = en.nextElement();
      if (child instanceof WDFDataNode)
        ((WDFDataNode) child).commitChanges();
    }
  }

  private static String defaultWidgetName(WDFWidget widget) {
    return WDFWidgetType.toString(widget.type())
      + "(" + widget.id() + ")";
  }

}
