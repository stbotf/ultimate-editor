package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.widgets.WDFPicture;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.gui.stbof.wdf.nodes.data.WDFIntegerNode;
import ue.gui.stbof.wdf.nodes.data.WDFOptionNode;
import ue.gui.util.GuiTools;
import ue.util.img.ImageCache;

public class WDFPictureNode extends WDFWidgetNode {

  private static final long serialVersionUID = 1564810368953177293L;

  private JPanel cellEditor;
  private JCheckBox display;
  private JLabel lbl;

  private WDFPicture pictureBox;

  private boolean placeHolderVisible = true;

  public WDFPictureNode(final WDFPicture pictureBox,
      Stbof res, ImageCache imgCache) {
    super(pictureBox, res, imgCache, null);
    this.pictureBox = pictureBox;

    add(new WDFOptionNode("Image placeholder", null, null,
        true, placeHolderVisible) {
      private static final long serialVersionUID = 9135852638572963043L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        placeHolderVisible = isEnabled;
      }
    });

    add(new WDFIntegerNode("Unknown", pictureBox.getUnknown1(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = -8916670766262564400L;

      @Override
      public void setInteger(int value) {
        pictureBox.setUnknown1(value);
      }
    });
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (this.visible) {
      Graphics gimg = image.getGraphics();
      Image img;

      x += pictureBox.getPosition().x;
      y += pictureBox.getPosition().y;
      w = Math.min(pictureBox.getDimension().width, w - pictureBox.getPosition().x);
      h = Math.min(pictureBox.getDimension().height, h - pictureBox.getPosition().y);

      if (this.backgroundVisible) {
        if (!pictureBox.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(pictureBox.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      if (placeHolderVisible) {
        gimg.setColor(Color.BLUE);
        gimg.drawRect(x, y, w, h);
        gimg.drawLine(x, y, x + w, y + h);
        gimg.drawLine(x, y + h, x + w, y);
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(pictureBox.getPosition(), pictureBox.getDimension());
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (cellEditor == null) {
      cellEditor = new JPanel(new BorderLayout());
      cellEditor.setOpaque(false);

      display = new JCheckBox();
      display.setSelected(true);
      display.setOpaque(false);
      display.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          visible = display.isSelected();
          fireChangeNotification();
        }
      });

      cellEditor.add(display, BorderLayout.WEST);

      lbl = new JLabel(this.getUserObject().toString());
      cellEditor.add(lbl, BorderLayout.CENTER);
    }

    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }

}
