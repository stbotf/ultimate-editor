package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.widgets.WDFSlider;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.gui.stbof.wdf.nodes.data.WDFIntegerNode;
import ue.gui.stbof.wdf.nodes.data.WDFOptionNode;
import ue.gui.util.GuiTools;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;

public class WDFSliderNode extends WDFWidgetNode {

  private JPanel cellEditor;
  private JCheckBox display;
  private JLabel lbl;

  private WDFSlider slider;

  private WDFOptionNode cellOffOption;
  private WDFOptionNode cellOnOption;
  private WDFOptionNode markerOffOption;
  private WDFOptionNode markerOnOption;

  private boolean cellOnImageVisible = false;
  private boolean cellOffImageVisible = true;
  private boolean markerOnImageVisible = true;
  private boolean markerOffImageVisible = false;

  public WDFSliderNode(final WDFSlider slider,
      Stbof res, ImageCache imgCache) {
    super(slider, res, imgCache, null);
    this.slider = slider;

    add(new WDFIntegerNode("Unknown 1", slider.getUnknown1(), 3,
        Short.MIN_VALUE, Short.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setUnknown1((short) value);
      }
    });

    add(new WDFIntegerNode("Max Value", slider.getMaxValue(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setMaxValue(value);
      }
    });

    add(new WDFIntegerNode("Units", slider.getUnits(), 5,
      Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setUnits(value);
      }
    });

    add(new WDFIntegerNode("Width", slider.getWidth(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setWidth(value);
      }
    });

    add(new WDFIntegerNode("Unknown 2", slider.getUnknown2(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setUnknown2(value);
      }
    });

    ArrayList<String> files = res.getFileNames(FileFilters.Tga);
    files.add(0, WDFWidget.NO_IMAGE);

    cellOffOption = new WDFOptionNode("Cell off image",
        slider.getCellOffImage(),
        files, true, cellOffImageVisible
    ) {
      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        slider.setCellOffImage(item);
        cellOffImageVisible = isEnabled;

        if (isEnabled) {
          cellOnImageVisible = false;
          cellOnOption.setOptionEnabled(false);
        }
      }
    };
    add(cellOffOption);

    cellOnOption = new WDFOptionNode("Cell on image",
        slider.getCellOnImage(),
        files, true, cellOnImageVisible
    ) {
      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        slider.setCellOnImage(item);
        cellOnImageVisible = isEnabled;

        if (isEnabled) {
          cellOffImageVisible = false;
          cellOffOption.setOptionEnabled(false);
        }
      }
    };
    add(cellOnOption);

    markerOffOption = new WDFOptionNode("Marker off image",
        slider.getMarkerOffImage(),
        files, true, markerOffImageVisible
    ) {
      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        slider.setMarkerOffImage(item);
        markerOffImageVisible = isEnabled;

        if (isEnabled) {
          markerOnImageVisible = false;
          markerOffOption.setOptionEnabled(false);
        }
      }
    };
    add(markerOffOption);

    markerOnOption = new WDFOptionNode("Marker on image",
        slider.getMarkerOnImage(),
        files, true, markerOnImageVisible
    ) {
      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        slider.setMarkerOnImage(item);
        markerOffImageVisible = isEnabled;

        if (isEnabled) {
          markerOffImageVisible = false;
          markerOffOption.setOptionEnabled(false);
        }
      }
    };
    add(markerOnOption);

    add(new WDFIntegerNode("Max Value 2", slider.getMaxValue2(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setMaxValue2(value);
      }
    });

    add(new WDFIntegerNode("Units 2", slider.getUnits2(), 5,
      Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setUnits2(value);
      }
    });

    add(new WDFIntegerNode("Width 2", slider.getWidth2(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      @Override
      public void setInteger(int value) {
        slider.setWidth2(value);
      }
    });
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (this.visible) {
      Graphics gimg = image.getGraphics();
      Image img;

      x += slider.getPosition().x;
      y += slider.getPosition().y;
      w = Math.min(slider.getDimension().width, w - slider.getPosition().x);
      h = Math.min(slider.getDimension().height, h - slider.getPosition().y);

      if (this.backgroundVisible) {
        if (!slider.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(slider.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      if (cellOffImageVisible) {
        if (!slider.getCellOffImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(slider.getCellOffImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }

      if (cellOnImageVisible) {
        if (!slider.getCellOnImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(slider.getCellOnImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }

      if (cellOffImageVisible) {
        if (!slider.getMarkerOffImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(slider.getMarkerOffImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }

      if (cellOffImageVisible) {
        if (!slider.getMarkerOnImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(slider.getMarkerOnImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(slider.getPosition(), slider.getDimension());
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (cellEditor == null) {
      cellEditor = new JPanel(new BorderLayout());
      cellEditor.setOpaque(false);

      display = new JCheckBox();
      display.setSelected(true);
      display.setOpaque(false);
      display.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          visible = display.isSelected();
          fireChangeNotification();
        }
      });

      cellEditor.add(display, BorderLayout.WEST);

      lbl = new JLabel(this.getUserObject().toString());
      cellEditor.add(lbl, BorderLayout.CENTER);
    }

    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }

}
