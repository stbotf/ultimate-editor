package ue.gui.stbof.wdf.nodes.widgets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.wdf.widgets.WDFScrollBar;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.gui.stbof.wdf.nodes.data.WDFIntegerNode;
import ue.gui.stbof.wdf.nodes.data.WDFOptionNode;
import ue.gui.util.GuiTools;
import ue.util.file.FileFilters;
import ue.util.img.ImageCache;

public class WDFScrollBarNode extends WDFWidgetNode {

  private static final long serialVersionUID = 3083950311832282578L;

  private JPanel cellEditor;
  private JCheckBox display;
  private JLabel lbl;

  private WDFScrollBar scrollBar;

  private WDFOptionNode handleOffOption;
  private WDFOptionNode handleOnOption;

  private boolean handleOnImageVisible = false;
  private boolean handleOffImageVisible = true;

  public WDFScrollBarNode(final WDFScrollBar scrollBar,
      Stbof res, ImageCache imgCache) {
    super(scrollBar, res, imgCache, null);
    this.scrollBar = scrollBar;

    add(new WDFIntegerNode("Unknown", scrollBar.getUnknown(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = 2108213016421922888L;

      @Override
      public void setInteger(int value) {
        scrollBar.setUnknown(value);
      }
    });

    ArrayList<String> files = res.getFileNames(FileFilters.Tga);
    files.add(0, WDFWidget.NO_IMAGE);

    handleOnOption = new WDFOptionNode("Handle on image",
        scrollBar.getHandleOnImage(),
        files, true, handleOnImageVisible
    ) {
      private static final long serialVersionUID = 893679856262331987L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        scrollBar.setHandleOnImage(item);
        handleOnImageVisible = isEnabled;
        if (isEnabled)
          handleOffOption.setOptionEnabled(false);
      }
    };
    add(handleOnOption);

    handleOffOption = new WDFOptionNode("Handle off image",
        scrollBar.getHandleOffImage(),
        files, true, handleOffImageVisible
    ) {
      private static final long serialVersionUID = -8169220567182389468L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        scrollBar.setHandleOffImage(item);
        handleOffImageVisible = isEnabled;
        if (isEnabled)
          handleOnOption.setOptionEnabled(false);
      }
    };
    add(handleOffOption);

    add(new WDFIntegerNode("Min value", scrollBar.getMinValue(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = 7130167412135459434L;

      @Override
      public void setInteger(int value) {
        scrollBar.setMinValue(value);
      }
    });

    add(new WDFIntegerNode("Max value", scrollBar.getMaxValue(), 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = 159934663180718818L;

      @Override
      public void setInteger(int value) {
        scrollBar.setMaxValue(value);
      }
    });
  }

  @Override
  public void paintOnImage(int x, int y, int w, int h, BufferedImage image) {
    if (this.visible) {
      Graphics gimg = image.getGraphics();
      Image img;

      x += scrollBar.getPosition().x;
      y += scrollBar.getPosition().y;
      w = Math.min(scrollBar.getDimension().width, w - scrollBar.getPosition().x);
      h = Math.min(scrollBar.getDimension().height, h - scrollBar.getPosition().y);

      if (this.backgroundVisible) {
        if (!scrollBar.getBackgroundImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(scrollBar.getBackgroundImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        } else {
          gimg.setColor(Color.WHITE);
          gimg.drawRect(x - 1, y - 1, w + 2, h + 2);
        }
      }

      if (handleOffImageVisible) {
        if (!scrollBar.getHandleOffImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(scrollBar.getHandleOffImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }

      if (handleOnImageVisible) {
        if (!scrollBar.getHandleOnImage().equalsIgnoreCase(WDFWidget.NO_IMAGE)) {
          img = imgCache.getImage(scrollBar.getHandleOnImage());
          GuiTools.drawImage(img, x, y, w, h, gimg);
        }
      }
    }
  }

  @Override
  public Rectangle getRectangle() {
    return new Rectangle(scrollBar.getPosition(), scrollBar.getDimension());
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (cellEditor == null) {
      cellEditor = new JPanel(new BorderLayout());
      cellEditor.setOpaque(false);

      display = new JCheckBox();
      display.setSelected(true);
      display.setOpaque(false);
      display.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          visible = display.isSelected();
          fireChangeNotification();
        }
      });

      cellEditor.add(display, BorderLayout.WEST);

      lbl = new JLabel(this.getUserObject().toString());
      cellEditor.add(lbl, BorderLayout.CENTER);
    }

    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }

}
