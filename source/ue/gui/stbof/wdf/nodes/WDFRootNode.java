package ue.gui.stbof.wdf.nodes;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 */
public class WDFRootNode extends DefaultMutableTreeNode {

  private static final long serialVersionUID = 313403071977776272L;

  public WDFRootNode(String value, ArrayList<String> files) {
    super(value);

    for (int i = 0; i < files.size(); i++) {
      add(new DefaultMutableTreeNode(files.get(i)));
    }
  }
}
