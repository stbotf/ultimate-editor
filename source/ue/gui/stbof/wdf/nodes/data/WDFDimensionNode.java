package ue.gui.stbof.wdf.nodes.data;

import java.awt.Dimension;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 */
public abstract class WDFDimensionNode extends DefaultMutableTreeNode {

  private static final long serialVersionUID = 2063006288227788472L;

  public WDFDimensionNode(String name, final Dimension dim) {
    super(name);

    add(new WDFIntegerNode("Width", dim.width, 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = -1047995142372182944L;

      @Override
      public void setInteger(int value) {
        if (dim.width != value) {
          dim.width = value;
          setDimension(dim);
        }
      }
    });
    add(new WDFIntegerNode("Height", dim.height, 5,
        Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = 8825376695497568588L;

      @Override
      public void setInteger(int value) {
        if (dim.height != value) {
          dim.height = value;
          setDimension(dim);
        }
      }
    });
  }

  public abstract void setDimension(Dimension dim);
}
