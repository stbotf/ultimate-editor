package ue.gui.stbof.wdf.nodes.data;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.util.data.ID;

/**
 *
 */
public abstract class WDFBooleanNode extends WDFDataNode {

  private static final long serialVersionUID = 8554856402387826053L;

  private JPanel cellEditor;
  private JLabel lblValue;
  private JComboBox<ID> cmbValue;

  public WDFBooleanNode(String desc, boolean value) {
    super(desc);

    cmbValue = new JComboBox<ID>();

    cmbValue.addItem(new ID("Yes", 1));
    cmbValue.addItem(new ID("No", 0));

    cmbValue.setSelectedIndex((value) ? 0 : 1);

    cmbValue.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
          setBoolean(cmbValue.getSelectedIndex() == 0);
          fireChangeNotification();
        }
      }
    });

    JLabel lblDesc = new JLabel(desc + ": ");

    lblValue = new JLabel(cmbValue.getSelectedItem().toString());

    cellEditor = new JPanel();

    cellEditor.setLayout(new BoxLayout(cellEditor, BoxLayout.X_AXIS));
    cellEditor.setOpaque(false);
    cellEditor.add(lblDesc);
//        cellEditor.add(Box.createHorizontalGlue());
    cellEditor.add(lblValue);
    cellEditor.add(cmbValue);
    cellEditor.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
    cellEditor.setBackground(UIManager.getColor("Tree.selectionBackground"));
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    cellEditor.setOpaque(selected);
    cmbValue.setVisible(selected);
    lblValue.setVisible(!selected);
    lblValue.setText(cmbValue.getSelectedItem().toString());

    return cellEditor;
  }

  public abstract void setBoolean(boolean value);

  @Override
  public void commitChanges() {
    setBoolean(cmbValue.getSelectedIndex() == 0);
  }
}
