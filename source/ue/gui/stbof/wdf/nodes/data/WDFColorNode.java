package ue.gui.stbof.wdf.nodes.data;

import java.awt.Component;
import java.util.Enumeration;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 *
 */
public abstract class WDFColorNode extends WDFDataNode {

  private static final long serialVersionUID = 5234999164821454864L;

  private int color;
  private JPanel cellEditor;
  private JLabel lbl;

  public WDFColorNode(String name, int value) {
    super(name);

    this.color = value;

    cellEditor = new JPanel();
    cellEditor.setLayout(new BoxLayout(cellEditor, BoxLayout.X_AXIS));

    cellEditor.setOpaque(false);

    lbl = new JLabel(name);
    lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    cellEditor.add(lbl);

    add(new WDFIntegerNode("Red", ((value >>> 16) & 0xFF), 3,
        0, 255) {
      private static final long serialVersionUID = 614234311518822749L;

      @Override
      public void setInteger(int value) {
        int tmp = (color & 0x00FFFF) | ((value & 0xFF) << 16);
        if (color != tmp) {
          color = tmp;
          setColor(color);
        }
      }
    });
    add(new WDFIntegerNode("Blue", (value & 0xFF), 3,
        0, 255) {
      private static final long serialVersionUID = 9007489345191809272L;

      @Override
      public void setInteger(int value) {
        int tmp = (color & 0xFFFF00) | (value & 0xFF);
        if (color != tmp) {
          color = tmp;
          setColor(color);
        }
      }
    });
    add(new WDFIntegerNode("Green", ((value >>> 8) & 0xFF), 3,
        0, 255) {
      private static final long serialVersionUID = 3531963528828416361L;

      @Override
      public void setInteger(int value) {
        int tmp = (color & 0xFF00FF) | ((value & 0xFF) << 8);
        if (color != tmp) {
          color = tmp;
          setColor(color);
        }
      }
    });
  }

  public abstract void setColor(int value);

  @Override
  public void commitChanges() {
    Enumeration<?> en = this.children();
    Object child;

    while (en.hasMoreElements()) {
      child = en.nextElement();

      if (child instanceof WDFDataNode) {
        ((WDFDataNode) child).commitChanges();
      }
    }
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    lbl.setOpaque(selected);

    return cellEditor;

  }
}
