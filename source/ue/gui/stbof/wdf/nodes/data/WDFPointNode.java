package ue.gui.stbof.wdf.nodes.data;

import java.awt.Point;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 */
public abstract class WDFPointNode extends DefaultMutableTreeNode {

  private static final long serialVersionUID = -5989001383668220314L;

  public WDFPointNode(String name, final Point pos) {
    super(name);

    add(new WDFIntegerNode("X", pos.x, 5, Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = 5297311871375706561L;

      @Override
      public void setInteger(int value) {
        if (pos.x != value) {
          pos.x = value;
          setPoint(pos);
        }
      }
    });
    add(new WDFIntegerNode("Y", pos.y, 5, Integer.MIN_VALUE, Integer.MAX_VALUE) {
      private static final long serialVersionUID = -2013321237339131861L;

      @Override
      public void setInteger(int value) {
        if (pos.y != value) {
          pos.y = value;
          setPoint(pos);
        }
      }
    });
  }

  public abstract void setPoint(Point point);
}
