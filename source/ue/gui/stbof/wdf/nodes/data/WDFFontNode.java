package ue.gui.stbof.wdf.nodes.data;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.edit.res.stbof.files.wdf.data.WDFFont;

/**
 *
 */
public abstract class WDFFontNode extends WDFDataNode {

  private static final long serialVersionUID = -5280521444183441110L;

  private JPanel cellEditor;
  private JCheckBox chkEnable;
  private JLabel lbl;
  private WDFFont font;

  public WDFFontNode(String desc, WDFFont value, boolean canBeDisabled,
      boolean isEnabled, ArrayList<String> fonts) {
    super(desc);

    this.font = value;

    cellEditor = new JPanel();
    cellEditor.setLayout(new BoxLayout(cellEditor, BoxLayout.X_AXIS));

    cellEditor.setOpaque(false);

    if (canBeDisabled) {
      chkEnable = new JCheckBox();
      chkEnable.setOpaque(false);
      chkEnable.setSelected(isEnabled);
      chkEnable.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          setFont(chkEnable.isSelected(), font);
          fireChangeNotification();
        }
      });
      cellEditor.add(chkEnable);
    }

    lbl = new JLabel(desc);
    cellEditor.add(lbl);

    // children
    add(new WDFColorNode("Color", font.getRGBColor()) {
      private static final long serialVersionUID = 7860398561934174909L;

      @Override
      public void setColor(int value) {
        font.setRGBColor(value);
        setFont(chkEnable.isSelected(), font);
      }
    });

    for (int i = 1; i < 8; i++) {
      add(new WDFIntegerNode("Unknown " + i, font.getUnknown(i - 1), 5,
          Integer.MIN_VALUE, Integer.MAX_VALUE) {
        private static final long serialVersionUID = -8234790411211320036L;

        @Override
        public void setInteger(int value) {
          font.setUnknown(Integer.parseInt(this.getUserObject().toString().substring(8)) - 1,
              value);
          setFont(chkEnable.isSelected(), font);
        }
      });
    }

    add(new WDFBooleanNode("Use color settings", font.overrideExeDefaults()) {
      private static final long serialVersionUID = -3772370058864802978L;

      @Override
      public void setBoolean(boolean value) {
        if (font.setOverrideExeDefaults(value)) {
          setFont(chkEnable.isSelected(), font);
        }
      }
    });

    if (!fonts.contains(font.getFontFile())) {
      fonts.add(font.getFontFile());
    }

    add(new WDFOptionNode("Font file",
        font.getFontFile(),
        fonts, false, true
    ) {
      private static final long serialVersionUID = 62382389504802899L;

      @Override
      public void setSelectedItem(boolean isEnabled, int index, String item) {
        if (!font.getFontFile().equalsIgnoreCase(item)) {
          font.setFontFile(item);
          setFont(chkEnable.isSelected(), font);
        }
      }
    });
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (selected) {
      lbl.setOpaque(true);
      lbl.setBackground(UIManager.getColor("Tree.selectionBackground"));
    } else {
      lbl.setOpaque(false);
    }

    return cellEditor;
  }

  public void setOptionEnabled(boolean value) {
    chkEnable.setSelected(value);
    setFont(chkEnable.isSelected(), font);
    fireChangeNotification();
  }

  public abstract void setFont(boolean isEnabled, WDFFont font);

  @Override
  public void commitChanges() {
    Enumeration<?> en = this.children();
    Object child;

    while (en.hasMoreElements()) {
      child = en.nextElement();

      if (child instanceof WDFDataNode) {
        ((WDFDataNode) child).commitChanges();
      }
    }
  }
}
