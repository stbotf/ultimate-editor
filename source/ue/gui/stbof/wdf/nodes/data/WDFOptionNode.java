package ue.gui.stbof.wdf.nodes.data;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import ue.util.data.StringTools;

/**
 *
 */
public abstract class WDFOptionNode extends WDFDataNode {

  private static final long serialVersionUID = -6774335826433596791L;

  private JPanel cellEditor;
  private JLabel lblValue;
  private JComboBox<String> cmbOptions;
  private JCheckBox chkEnable;

  public WDFOptionNode(String desc, String value, ArrayList<String> options,
      boolean canBeDisabled, boolean isEnabled) {
    super(desc);

    cellEditor = new JPanel();
    cellEditor.setLayout(new BoxLayout(cellEditor, BoxLayout.X_AXIS));

    cellEditor.setOpaque(false);

    if (canBeDisabled) {
      chkEnable = new JCheckBox();
      chkEnable.setOpaque(false);
      chkEnable.setSelected(isEnabled);
      chkEnable.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          updateSelectedItem();
          fireChangeNotification();
        }
      });
      cellEditor.add(chkEnable);
    }

    if (options != null) {
      desc = desc + ": ";

      if (!options.contains(value)) {
        options.add(value);
      }

      cmbOptions = new JComboBox<String>();
      for (int i = 0; i < options.size(); i++) {
        cmbOptions.addItem(options.get(i));
      }

      cmbOptions.setSelectedItem(value);

      cmbOptions.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (e.getStateChange() == ItemEvent.SELECTED) {
            updateSelectedItem();
            fireChangeNotification();
          }
        }
      });
    }
    cellEditor.add(new JLabel(desc));

    if (options != null) {
      lblValue = new JLabel(getSelectedOption());
      cellEditor.add(lblValue);
      cellEditor.add(cmbOptions);
    }

    cellEditor.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
    cellEditor.setBackground(UIManager.getColor("Tree.selectionBackground"));
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    cellEditor.setOpaque(selected);

    if (cmbOptions != null) {
      cmbOptions.setVisible(selected);
      lblValue.setText(getSelectedOption());
      lblValue.setVisible(!selected);
    }

    return cellEditor;
  }

  public void setOptionEnabled(boolean value) {
    if (chkEnable.isSelected() != value) {
      chkEnable.setSelected(value);
      updateSelectedItem();
      fireChangeNotification();
    }
  }

  public abstract void setSelectedItem(boolean isEnabled, int index, String item);

  @Override
  public void commitChanges() {
    updateSelectedItem();
  }

  private String getSelectedOption() {
    return cmbOptions != null ? StringTools.valueOf(cmbOptions.getSelectedItem()) : null;
  }

  private void updateSelectedItem() {
    boolean enabled = chkEnable != null ? chkEnable.isSelected() : true;

    if (cmbOptions != null) {
      setSelectedItem(enabled, cmbOptions.getSelectedIndex(), getSelectedOption());
    } else {
      setSelectedItem(enabled, -1, null);
    }
  }
}
