package ue.gui.stbof.wdf.nodes.data;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

/**
 *
 */
public abstract class WDFDataNode extends DefaultMutableTreeNode {

  private static final long serialVersionUID = -3602101081962768241L;

  private Vector<ActionListener> listeners;

  public WDFDataNode(Object userObject) {
    super(userObject);
    listeners = new Vector<ActionListener>();
  }

  public abstract Component getTreeCellEditorComponent(boolean selected);

  public void addActionListener(ActionListener listener) {
    listeners.add(listener);
    recursiveAddListener(this, listener);
  }

  protected void recursiveAddListener(TreeNode node, ActionListener listener) {
    Enumeration<?> en = node.children();
    TreeNode child;

    while (en.hasMoreElements()) {
      child = (TreeNode) en.nextElement();

      if (child instanceof WDFDataNode) {
        ((WDFDataNode) child).addActionListener(listener);
      } else {
        recursiveAddListener(child, listener);
      }
    }
  }

  protected void fireChangeNotification() {
    for (int i = 0; i < listeners.size(); i++) {
      listeners.get(i).actionPerformed(null);
    }
  }

  public abstract void commitChanges();
}
