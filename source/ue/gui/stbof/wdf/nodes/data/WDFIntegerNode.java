package ue.gui.stbof.wdf.nodes.data;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public abstract class WDFIntegerNode extends WDFDataNode {

  private JPanel cellEditor = new JPanel();
  private JLabel lblValue;
  private JFormattedTextField txtValue;

  private int minVal, maxVal;

  public WDFIntegerNode(String desc, int value, int fieldSize,
      final int minVal, final int maxVal) {
    super(desc);

    this.minVal = minVal;
    this.maxVal = maxVal;

    txtValue = new JFormattedTextField(new DecimalFormat("#####"));
    txtValue.setColumns(fieldSize);
    txtValue.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
    txtValue.setValue(Integer.valueOf(value));
    txtValue.setHorizontalAlignment(JTextField.RIGHT);

    txtValue.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        int value = Integer.parseInt(txtValue.getValue().toString());

        if (value < minVal) {
          value = minVal;
          txtValue.setValue(value);
        } else if (value > maxVal) {
          value = maxVal;
          txtValue.setValue(value);
        }

        setInteger(value);
        fireChangeNotification();
      }
    });
    txtValue.addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_ACCEPT
            || e.getKeyCode() == KeyEvent.VK_ESCAPE) {
          try {
            txtValue.commitEdit();
            fireChangeNotification();
          } catch (ParseException ex) {
          }
        }
      }
    });

    /*
     * Disabled because one can see the delay and the the thing seems unresponsive
     * Would be nice to select all on start edit
    txtValue.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent evt) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              txtValue.selectAll();
            }
        });
      }
    });
    */

    JLabel lblDesc = new JLabel(desc + ": ");

    lblValue = new JLabel(txtValue.getValue().toString());
    lblValue.setHorizontalAlignment(JLabel.LEFT);

    cellEditor.setLayout(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    cellEditor.add(lblDesc, c);
    c.insets.left = 2;
    c.gridx++;
    cellEditor.add(lblValue, c);
    cellEditor.add(txtValue, c);
    cellEditor.add(Box.createHorizontalStrut(50), c);
    cellEditor.setPreferredSize(cellEditor.getPreferredSize());

    cellEditor.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
    cellEditor.setBackground(UIManager.getColor("Tree.selectionBackground"));
  }

  @Override
  public Component getTreeCellEditorComponent(boolean selected) {
    if (selected != txtValue.isVisible()) {
      // update opaque blue selection bar rendering
      cellEditor.setOpaque(selected);

      txtValue.setVisible(selected);
      lblValue.setVisible(!selected);
      lblValue.setText(txtValue.getValue().toString());
    }

    return cellEditor;
  }

  public abstract void setInteger(int value);

  @Override
  public void commitChanges() {
    int value = Integer.parseInt(txtValue.getValue().toString());

    if (value < minVal) {
      value = minVal;
      txtValue.setValue(value);
    } else if (value > maxVal) {
      value = maxVal;
      txtValue.setValue(value);
    }

    setInteger(value);
  }
}
