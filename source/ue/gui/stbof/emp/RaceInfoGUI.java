package ue.gui.stbof.emp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.emp.RaceList;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Meplanet;
import ue.edit.res.stbof.files.pst.Planet;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.res.stbof.files.wtf.Aiminor;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.DescTools;
import ue.gui.util.component.IconButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.file.FileFilters;

/**
 * The race info gui
 */
public class RaceInfoGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private final String strRACE = Language.getString("RaceInfoGUI.11"); //$NON-NLS-1$
  private final String strNEUTRAL = Language.getString("RaceInfoGUI.12"); //$NON-NLS-1$
  private final String strUNREST = Language.getString("RaceInfoGUI.13"); //$NON-NLS-1$

  // read
  private Stbof STBOF;
  private Trek trek;
  private Planet PLANET;

  // edited
  private RaceRst RACES;
  private Meplanet MEPlanet;
  private Aiminor AIMinor;

  // ui components
  private JButton btnADD = new JButton(Language.getString("RaceInfoGUI.0")); //$NON-NLS-1$
  private IconButton btnRename = IconButton.CreateEditButton();
  private JButton btnREMOVE = new JButton(Language.getString("RaceInfoGUI.2")); //$NON-NLS-1$
  private JComboBox<ID> cmbRACES = new JComboBox<ID>(); //combo box with all races
  private JLabel lblDesc = new JLabel(Language.getString("RaceInfoGUI.14"));
  private IconButton btnDescTools = IconButton.CreateDescriptionButton();
  private JTextArea txtINTRO = new JTextArea(8, 30); //text area with race description
  private JScrollPane jspINTRO; //scrollpane for txtINTRO
  private JLabel lblRACE_IMAGE = new JLabel(); //label with race image
  private JLabel lblRace_Pic = new JLabel(Language.getString("RaceInfoGUI.3")); //$NON-NLS-1$
  private JComboBox<String> cmbTGA = new JComboBox<String>(); //combo box with all tgas
  private JLabel[] lblBMorale = new JLabel[7]; //for naming the first five [major] races
  private MoraleSlider[] jsBASE_MORALE = new MoraleSlider[7];
  private JLabel lblRace_Combat_M = new JLabel(Language.getString("RaceInfoGUI.5")); //$NON-NLS-1$
  private JLabel lblStart_Pop = new JLabel(Language.getString("RaceInfoGUI.6")); //$NON-NLS-1$
  private JLabel lblDilithium = new JLabel(Language.getString("RaceInfoGUI.7")); //$NON-NLS-1$
  private JLabel lblHome_Planet = new JLabel(Language.getString("RaceInfoGUI.8")); //$NON-NLS-1$
  private JTextField txtCOMBAT_M = new JTextField(10);
  private JSpinner jsSTART_POP = new JSpinner(new SpinnerNumberModel(0, 0, Short.MAX_VALUE, 1));
  private JComboBox<String> cmbDILITHIUM = new JComboBox<String>(
      new String[]{Language.getString("RaceInfoGUI.9"),
          Language.getString("RaceInfoGUI.10")}); //$NON-NLS-1$ //$NON-NLS-2$
  private JComboBox<String> cmbHOME_PLANET;

  // data
  private short SELECTED = -1;

  public RaceInfoGUI(Stbof stbof, Trek trek) throws IOException {
    this.STBOF = stbof;
    this.trek = trek;
    PLANET = (Planet) stbof.getInternalFile(CStbofFiles.PlanetPst, true);
    RACES = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    MEPlanet = (Meplanet) stbof.getInternalFile(CStbofFiles.MEPlanetBin, true);
    AIMinor = (Aiminor) stbof.getInternalFile(CStbofFiles.AIMinorWtf, true);

    // setup
    this.setLayout(new BorderLayout());
    //default font
    Font def = UE.SETTINGS.getDefaultFont();
    //set font
    lblRace_Pic.setFont(def);
    cmbRACES.setFont(def);
    cmbTGA.setFont(def);
    cmbDILITHIUM.setFont(def);
    lblDesc.setFont(def);
    txtINTRO.setFont(def);
    lblRace_Combat_M.setFont(def);
    lblStart_Pop.setFont(def);
    lblDilithium.setFont(def);
    txtCOMBAT_M.setFont(def);
    jsSTART_POP.setFont(def);
    btnRename.setFont(def);
    btnADD.setFont(def);
    btnREMOVE.setFont(def);
    lblHome_Planet.setFont(def);
    btnADD.setEnabled(false);
    btnREMOVE.setEnabled(false);

    //races combo
    cmbRACES.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          applySelected();
          updateSelectedRace();
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }
      }
    });

    //JTF_START_POP
    jsSTART_POP.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        if (SELECTED >= 0) {
          try {
            short pop = ((Integer) jsSTART_POP.getValue()).shortValue();

            RACES.setStartPop(SELECTED, pop);
          } catch (Exception x) {
            Dialogs.displayError(x);
          }
        }
      }
    });

    //init labels and sliders
    for (int i = 0; i < 6; i++) {
      lblBMorale[i] = new JLabel();
      // TODO: get impossible dif penalty from trek.exe
      jsBASE_MORALE[i] = new MoraleSlider(21, 127, i);
      lblBMorale[i].setFont(def);
      jsBASE_MORALE[i].setFont(def);
    }

    lblBMorale[6] = new JLabel();
    jsBASE_MORALE[6] = new MoraleSlider(0, 195, 6);
    lblBMorale[6].setFont(def);
    jsBASE_MORALE[6].setFont(def);

    cmbTGA.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        short sel = (short) cmbTGA.getSelectedIndex();

        if (sel >= 0 && SELECTED >= 0) {
          try {
            String n = (String) cmbTGA.getSelectedItem();
            RACES.setPic(SELECTED, n);

            TargaImage img = STBOF.getTargaImage(n, LoadFlags.UNCACHED);

            if (img == null) {
              lblRACE_IMAGE.setIcon(null);
            } else {
              Dimension dim = img.getImageSize();

              if (dim.width > 160) {
                lblRACE_IMAGE.setIcon(new ImageIcon(img.getThumbnail(160, false, AlphaMode.Opaque)));
                return;
              } else if (dim.height > 170) {
                lblRACE_IMAGE.setIcon(new ImageIcon(img.getThumbnail(170, false, AlphaMode.Opaque)));
                return;
              } else {
                lblRACE_IMAGE.setIcon(new ImageIcon(img.getImage(AlphaMode.Opaque)));
                lblRACE_IMAGE.setText(null);
              }
            }
          } catch (Exception e) {
            Dialogs.displayError(e);
            lblRACE_IMAGE.setIcon(null);
            lblRACE_IMAGE.setText("N/A");
          }
        }
      }
    });

    //dilithium
    cmbDILITHIUM.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        int sel = cmbDILITHIUM.getSelectedIndex();

        if (sel >= 0 && SELECTED >= 0) {
          try {
            RACES.setDilithium(SELECTED, sel == 1);
          } catch (Exception x) {
            Dialogs.displayError(x);
          }
        }
      }
    });

    //planet index
    cmbHOME_PLANET = new JComboBox<String>(PLANET.getNames());
    cmbHOME_PLANET.setFont(def);
    cmbHOME_PLANET.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // TODO_ sort planet list
        short sel = (short) cmbHOME_PLANET.getSelectedIndex();

        if (sel >= 0 && SELECTED >= 0) {
          try {
            RACES.setPlanet(SELECTED, PLANET.getPlanet(sel).getIndex());
          } catch (Exception iae) {
            Dialogs.displayError(iae);
          }
        }
      }
    });

    //description label
    lblDesc.setVerticalAlignment(SwingConstants.BOTTOM);

    btnDescTools.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (DescTools.showToolsDialog(RACES)) {
          String desc = SELECTED >= 0 ? RACES.getDesc(SELECTED) : null;
          txtINTRO.setText(desc);
        }
      }
    });

    //text area
    txtINTRO.setLineWrap(true);
    txtINTRO.setWrapStyleWord(true);
    //scroll
    jspINTRO = new JScrollPane(txtINTRO);

    //JBtn_Name button
    btnRename.addActionListener(new CBActionListener(x -> onNameEdit(x)));
    btnADD.addActionListener(new ButtonPressed());
    btnREMOVE.addActionListener(new ButtonPressed());

    //fill JCombo_Tgas
    Collection<String> fileNames = STBOF.getFileNames(FileFilters.Tga);

    for (String fileName : fileNames)
      cmbTGA.addItem(fileName);

    //fill races
    this.fillRaces();

    //ads panels
    this.add(new Intro(), BorderLayout.CENTER);
    this.add(new Edit(), BorderLayout.SOUTH);
  }

  private void onNameEdit(ActionEvent evt) throws InvalidArgumentsException {
    String msg = Language.getString("RaceInfoGUI.19"); //$NON-NLS-1$
    String race = RACES.getName(SELECTED);
    String name = JOptionPane.showInputDialog(msg, race);

    if (name != null) {
      renameRace(SELECTED, name);
      fillRaces();
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.RacesInfo;
  }

  private void fillRaces() {
    //fill JCombo_Races
    try {
      int prev = SELECTED;
      SELECTED = -1;

      cmbRACES.removeAllItems();

      for (short x = 0; x < RACES.getNumberOfEntries(); x++) {
        String name = x + ": " + RACES.getName(x);
        cmbRACES.addItem(new ID(name, x));
      }

      if (prev >= 0 && prev < cmbRACES.getItemCount()) {
        cmbRACES.setSelectedIndex(prev);
      }
    } catch (Exception e) {
      Dialogs.displayError(e);
    }
  }

  protected void updateSelectedRace() {
    SELECTED = (short) cmbRACES.getSelectedIndex();

    if (SELECTED >= 0) {
      txtINTRO.setText(RACES.getDesc(SELECTED));
      cmbTGA.setSelectedItem(RACES.getPic(SELECTED));

      for (int i = 0; i < 6; i++) {
        jsBASE_MORALE[i].setValue(RACES.getBaseMorale(SELECTED, i));
      }
      jsBASE_MORALE[6].setValue(RACES.getMoraleThreshold(SELECTED));
      txtCOMBAT_M.setText(Float.toString(RACES.getCombatMulti(SELECTED)));
      jsSTART_POP.setValue((int) RACES.getStartPop(SELECTED));
      if (RACES.getDilithium(SELECTED)) {
        cmbDILITHIUM.setSelectedIndex(1);
      } else {
        cmbDILITHIUM.setSelectedIndex(0);
      }
      cmbHOME_PLANET.setSelectedIndex(PLANET.getIndex(RACES.getPlanet(SELECTED)));
    }
  }

  @Override
  public boolean hasChanges() {
    return RACES.madeChanges() || MEPlanet.madeChanges() || AIMinor.madeChanges()
      || STBOF.isChanged(CStbofFiles.RaceDescRst)
      || trek != null && trek.isChanged(CTrekSegments.RaceList);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(RACES, changes);
    STBOF.checkChanged(MEPlanet, changes);
    STBOF.checkChanged(AIMinor, changes);
    STBOF.checkChanged(CStbofFiles.RaceDescRst, changes);
    if (trek != null)
      trek.checkChanged(CTrekSegments.RaceList, changes);
  }

  @Override
  public void reload() throws IOException {
    RACES = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);
    MEPlanet = (Meplanet) STBOF.getInternalFile(CStbofFiles.MEPlanetBin, true);
    AIMinor = (Aiminor) STBOF.getInternalFile(CStbofFiles.AIMinorWtf, true);

    fillRaces();
  }

  @Override
  public void finalWarning() {
    applySelected();
    RACES.flushDescriptions();
  }

  private void applySelected() {
    if (SELECTED >= 0) {
      RACES.setDesc(SELECTED, txtINTRO.getText());
      Float flt = Float.parseFloat(txtCOMBAT_M.getText());
      RACES.setCombatMulti(SELECTED, flt);
    }
  }

  private class Intro extends JPanel {

    public Intro() {
      this.setLayout(new GridBagLayout());

      JPanel pnlRace = new JPanel(new GridBagLayout());
      {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;

        lblRACE_IMAGE.setHorizontalAlignment(SwingConstants.RIGHT);
        lblRACE_IMAGE.setVerticalAlignment(SwingConstants.TOP);

        pnlRace.add(cmbRACES, c);
        c.insets.left = 5;
        c.gridx = 1;
        c.weightx = 0;
        pnlRace.add(btnRename, c);
        c.insets.left = 0;
        c.insets.top = 5;
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 2;
        c.weighty = 1;
        pnlRace.add(lblRACE_IMAGE, c);
      }

      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;

      //combo
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 2;
      this.add(pnlRace, c);
      //description label
      c.insets = new Insets(5, 0, 5, 5);
      c.gridx = 2;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      c.weightx = 1.0;
      this.add(lblDesc, c);
      //description label
      c.gridx = 3;
      c.gridwidth = 1;
      c.gridheight = 1;
      c.weightx = 0;
      c.weighty = 0;
      c.fill = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.SOUTHEAST;
      this.add(btnDescTools, c);
      //text area intro
      c.insets = new Insets(0, 0, 5, 5);
      c.gridx = 2;
      c.gridy = 1;
      c.gridwidth = 2;
      c.gridheight = 2;
      c.weightx = 1.0;
      c.weighty = 1.0;
      c.fill = GridBagConstraints.BOTH;
      this.add(jspINTRO, c);
    }
  }

  private class EditOwnerMorale extends JPanel {

    public EditOwnerMorale() {
      this.setLayout(new GridBagLayout());
      this.setBorder(BorderFactory.createTitledBorder(Language.getString("RaceInfoGUI.4")));

      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      //base morale main label
      c.gridwidth = 2;
      c.gridheight = 1;

      //base morale sliders
      c.gridwidth = 1;
      c.gridheight = 1;
      c.insets.left = 0;
      for (int i = 0; i < 6; i++) {
        jsBASE_MORALE[i]
          .setPreferredSize(new Dimension(120, jsBASE_MORALE[i].getPreferredSize().height));
        c.gridx = 1;
        c.gridy = i + 1;
        this.add(jsBASE_MORALE[i], c);
      }

      //base morale sliders
      c.insets.left = 5;
      c.insets.right = 0;
      for (int i = 1; i < 7; i++) {
        c.gridx = 0;
        c.gridy = i;
        this.add(lblBMorale[i - 1], c);
      }

      // set minimum width to not adjust on cardassian morale change
      add(Box.createHorizontalStrut(100), c);
    }
  }

  private class Edit extends JPanel {

    public Edit() {
      this.setLayout(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;
      //pic label
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(lblRace_Pic, c);
      //list of images
      c.gridx = 1;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(cmbTGA, c);
      //base combat label
      c.gridx = 0;
      c.gridy = 1;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(lblRace_Combat_M, c);
      //base combat
      c.gridx = 1;
      c.gridy = 1;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(txtCOMBAT_M, c);
      //starting population label
      c.gridx = 0;
      c.gridy = 2;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(lblStart_Pop, c);
      //starting population
      c.gridx = 1;
      c.gridy = 2;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(jsSTART_POP, c);
      //dilithium label
      c.gridx = 0;
      c.gridy = 3;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(lblDilithium, c);
      //dilithium
      c.gridx = 1;
      c.gridy = 3;
      c.gridwidth = 1;
      c.gridheight = 1;
      this.add(cmbDILITHIUM, c);
      //planet label
      c.gridx = 0;
      c.gridy = 4;
      this.add(lblHome_Planet, c);
      //planet
      c.gridx = 1;
      this.add(cmbHOME_PLANET, c);
      //threshold label
      c.gridwidth = 2;
      c.gridx = 0;
      c.gridy = 5;
      this.add(lblBMorale[6], c);
      //threshold
      c.gridy = 6;
      this.add(jsBASE_MORALE[6], c);
      //base morale
      c.gridx = 2;
      c.gridy = 0;
      c.gridheight = 7;
      this.add(new EditOwnerMorale(), c);
    }
  }

  private void renameRace(int raceId, String name) throws InvalidArgumentsException {
    if (RACES.containsRace(name)) {
      // fix old one first
      renameRace(RACES.getIndex(name), name + "?");
    }

    if (raceId < NUM_EMPIRES) {
      // fix meplanet
      if (Arrays.binarySearch(MEPlanet.getOwners(), name) < 0) {
        MEPlanet.setOwner(raceId, name);
      }
    }

    // fix exe
    try {
      if (trek != null) {
        RaceList rlst = (RaceList) trek.getInternalFile(CTrekSegments.RaceList, true);
        String old = rlst.getRaceString(raceId);

        if (raceId > 4) {
          // fix aiminor
          int m = Arrays.binarySearch(AIMinor.getNames(), old);
          if (m < 0) {
            m = AIMinor.add(0);
          }

          AIMinor.setName(m, name);
        }

        rlst.setRaceString(raceId, name);
      }
    } catch (Exception e) {
    }

    RACES.setName(raceId, name);

    // and now fix errors :/
    RACES.check();
    AIMinor.check();
  }

  private class ButtonPressed implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent ae) {
      try {
        if (SELECTED < 0)
          return;

        // remove
        if ((ae.getActionCommand()).equals(Language.getString("RaceInfoGUI.2"))) { //$NON-NLS-1$
          RACES.remove(SELECTED);
          fillRaces();
        }

        // add
        if ((ae.getActionCommand()).equals(Language.getString("RaceInfoGUI.0"))) { //$NON-NLS-1$
          ID[] possibleValues = new ID[RACES.getNumberOfEntries()];

          for (short x = 0; x < RACES.getNumberOfEntries(); x++) {
            possibleValues[x] = new ID(RACES.getName(x), x);
          }

          Object selectedValue = JOptionPane.showInputDialog(null,
              Language.getString("RaceInfoGUI.20"), UE.APP_NAME, //$NON-NLS-1$
              JOptionPane.INFORMATION_MESSAGE, null,
              possibleValues, possibleValues[0]);

          if (selectedValue != null) {
            int ix = selectedValue.hashCode();
            RACES.add(ix);
            fillRaces();
          }
        }

      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }
  }

  private class MoraleSlider extends JSlider implements ChangeListener {

    private int index;

    public MoraleSlider(int h, int w, int i) {
      super(h, w);
      index = i;
      this.addChangeListener(this);
    }

    @Override
    public void stateChanged(ChangeEvent ce) {
      try {
        if (index < 6) {
          if (index < 5) {
            String str = strRACE.replace("%1", Integer.toString(this.getValue())); //$NON-NLS-1$
            str = str.replace("%2", RACES.getName(index)); //$NON-NLS-1$
            lblBMorale[index].setText(str);
          } else {
            lblBMorale[index]
                .setText(strNEUTRAL.replace("%1", Integer.toString(this.getValue()))); //$NON-NLS-1$
          }
          RACES.setBaseMorale(SELECTED, index, (byte) this.getValue());
        } else {
          lblBMorale[index]
              .setText(strUNREST.replace("%1", Integer.toString(this.getValue()))); //$NON-NLS-1$
          RACES.setMoraleThreshold(SELECTED, (short) this.getValue());
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }
  }
}
