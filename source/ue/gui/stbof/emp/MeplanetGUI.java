package ue.gui.stbof.emp;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Meplanet;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

public class MeplanetGUI extends MainPanel {

  // read
  private Stbof stbof;

  // edited
  private Meplanet PLANET;

  // ui components
  private JButton btnADD = new JButton(Language.getString("MeplanetGUI.0")); //$NON-NLS-1$
  private JButton btnEDIT = new JButton(Language.getString("MeplanetGUI.1")); //$NON-NLS-1$
  private JButton btnREMOVE = new JButton(Language.getString("MeplanetGUI.2")); //$NON-NLS-1$
  private JButton btnADD2 = new JButton(Language.getString("MeplanetGUI.0")); //$NON-NLS-1$
  private JButton btnEDIT2 = new JButton(Language.getString("MeplanetGUI.1")); //$NON-NLS-1$
  private JButton btnREMOVE2 = new JButton(Language.getString("MeplanetGUI.2")); //$NON-NLS-1$
  private JLabel jlCOLON = new JLabel(Language.getString("MeplanetGUI.3")); //$NON-NLS-1$
  private JComboBox<ID> jcbOWNER = new JComboBox<ID>();
  private JList<String> jlPLANET = new JList<String>();
  private JComboBox<ID> jcbLEVEL = new JComboBox<ID>();

  /**
   * constructor
   *
   * @param stbof the Meplanet to work with
   * @throws IOException
   */
  public MeplanetGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;
    PLANET = (Meplanet) stbof.getInternalFile(CStbofFiles.MEPlanetBin, true);

    //font
    Font def = UE.SETTINGS.getDefaultFont();
    btnADD.setFont(def);
    btnEDIT.setFont(def);
    btnREMOVE.setFont(def);
    btnADD2.setFont(def);
    btnEDIT2.setFont(def);
    btnREMOVE2.setFont(def);
    jcbOWNER.setFont(def);
    jlPLANET.setFont(def);
    jlCOLON.setFont(def);
    jcbLEVEL.setFont(def);
    jlPLANET.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    //listeners
    ActionListener first = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // add
        if (ae.getActionCommand().equals(Language.getString("MeplanetGUI.0"))) { //$NON-NLS-1$
          String n = JOptionPane.showInputDialog(Language.getString("MeplanetGUI.4"),
              Language.getString("MeplanetGUI.5")); //$NON-NLS-1$ //$NON-NLS-2$
          if (n != null) {
            try {
              PLANET.addOwner(n);
              loadOwners();
            } catch (Exception g) {
              Dialogs.displayError(g);
            }
          }
          return;
        }

        if (jcbOWNER.getSelectedIndex() < 0)
          return;

        // edit
        if (ae.getActionCommand().equals(Language.getString("MeplanetGUI.1"))) { //$NON-NLS-1$
          String n = JOptionPane.showInputDialog(Language.getString("MeplanetGUI.6"),
              jcbOWNER.getSelectedItem()); //$NON-NLS-1$
          if (n != null) {
            try {
              int SELECTED = jcbOWNER.getSelectedIndex();
              PLANET.setOwner(((ID)jcbOWNER.getSelectedItem()).ID, n);
              loadOwners();
              jcbOWNER.setSelectedIndex(SELECTED);
            } catch (Exception g) {
              Dialogs.displayError(g);
            }
          }
          return;
        }

        // remove
        if (ae.getActionCommand().equals(Language.getString("MeplanetGUI.2"))) { //$NON-NLS-1$
          PLANET.removeOwner(((ID)jcbOWNER.getSelectedItem()).ID);
          loadOwners();
          return;
        }
      }
    };
    btnADD.addActionListener(first);
    btnEDIT.addActionListener(first);
    btnREMOVE.addActionListener(first);
    ActionListener second = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (jcbOWNER.getSelectedIndex() < 0)
          return;

        // add
        if (ae.getActionCommand().equals(Language.getString("MeplanetGUI.0"))) { //$NON-NLS-1$
          String n = JOptionPane.showInputDialog(Language.getString("MeplanetGUI.7"),
              Language.getString("MeplanetGUI.8")); //$NON-NLS-1$ //$NON-NLS-2$
          if (n != null) {
            try {
              ID own = (ID) jcbOWNER.getSelectedItem();
              PLANET.addPlanet(own.ID, n);
              updateOwnerSelection();
            } catch (Exception g) {
              Dialogs.displayError(g);
            }
          }
          return;
        }

        if (jlPLANET.getSelectedIndex() < 0)
          return;

        // edit
        if (ae.getActionCommand().equals(Language.getString("MeplanetGUI.1"))) { //$NON-NLS-1$
          String n = JOptionPane.showInputDialog(Language.getString("MeplanetGUI.9"),
              jlPLANET.getSelectedValue()); //$NON-NLS-1$
          if (n != null) {
            try {
              ID own = (ID) jcbOWNER.getSelectedItem();
              PLANET.setPlanet(own.ID, jlPLANET.getSelectedIndex(), n);
              updateOwnerSelection();
            } catch (Exception g) {
              Dialogs.displayError(g);
            }
          }
          return;
        }

        // remove
        if (ae.getActionCommand().equals(Language.getString("MeplanetGUI.2"))) { //$NON-NLS-1$
          try {
            ID own = (ID) jcbOWNER.getSelectedItem();
            PLANET.removePlanet(own.ID, jlPLANET.getSelectedIndex());
            updateOwnerSelection();
          } catch (Exception g) {
            Dialogs.displayError(g);
          }
          return;
        }
      }
    };
    btnADD2.addActionListener(second);
    btnEDIT2.addActionListener(second);
    btnREMOVE2.addActionListener(second);
    jcbOWNER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        updateOwnerSelection();
      }
    });
    jlPLANET.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (jcbOWNER.getSelectedIndex() < 0) {
          btnEDIT2.setEnabled(false);
          btnREMOVE2.setEnabled(false);
          jcbLEVEL.setEnabled(false);
        } else if (jlPLANET.getSelectedIndex() < 0) {
          btnEDIT2.setEnabled(false);
          btnREMOVE2.setEnabled(false);
          jcbLEVEL.setEnabled(false);
        } else {
          btnEDIT2.setEnabled(true);
          btnREMOVE2.setEnabled(true);
          jcbLEVEL.setEnabled(true);
          ID own = (ID) jcbOWNER.getSelectedItem();
          try {
            int col = PLANET.getColonized(own.ID, jlPLANET.getSelectedIndex());
            jcbLEVEL.setSelectedItem(new ID("", col));
          } catch (Exception g) {
            Dialogs.displayError(g);
          }
        }
      }
    });
    jcbLEVEL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (jlPLANET.getSelectedIndex() < 0 || jcbOWNER.getSelectedIndex() < 0) {
          return;
        }
        ID own = (ID) jcbOWNER.getSelectedItem();
        try {
          ID sel = (ID) jcbLEVEL.getSelectedItem();
          if (sel != null)
            PLANET.setColonized(own.ID, jlPLANET.getSelectedIndex(), sel.ID);
        } catch (Exception g) {
          Dialogs.displayError(g);
        }
      }
    });

    //ok init
    jcbLEVEL.addItem(new ID("0: " + LexHelper.mapEvoLvl(0), 0)); //$NON-NLS-1$
    jcbLEVEL.addItem(new ID("1: " + LexHelper.mapEvoLvl(1), 1)); //$NON-NLS-1$
    jcbLEVEL.addItem(new ID("2: " + LexHelper.mapEvoLvl(2), 2)); //$NON-NLS-1$
    jcbLEVEL.addItem(new ID("3: " + LexHelper.mapEvoLvl(3), 3)); //$NON-NLS-1$
    jcbLEVEL.addItem(new ID("4: " + LexHelper.mapEvoLvl(4), 4)); //$NON-NLS-1$
    jcbLEVEL.addItem(new ID("10: " + Language.getString("MeplanetGUI.10"), 10)); //$NON-NLS-1$
    loadOwners();

    //place it
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    add(jcbOWNER, c);
    c.gridx = 0;
    c.gridy = 1;
    c.gridheight = 3;
    JScrollPane scrolly = new JScrollPane(jlPLANET);
    scrolly.setPreferredSize(new Dimension(200, 200));
    add(scrolly, c);
    c.gridx = 1;
    c.gridy = 0;
    c.gridheight = 1;
    add(btnADD, c);
    c.gridx = 2;
    c.gridy = 0;
    add(btnEDIT, c);
    c.gridx = 3;
    c.gridy = 0;
    add(btnREMOVE, c);
    c.gridx = 1;
    c.gridy = 1;
    add(btnADD2, c);
    c.gridx = 2;
    c.gridy = 1;
    add(btnEDIT2, c);
    c.gridx = 3;
    c.gridy = 1;
    add(btnREMOVE2, c);
    c.gridx = 1;
    c.gridy = 2;
    c.gridwidth = 2;
    add(jlCOLON, c);
    c.gridx = 3;
    c.gridy = 2;
    c.gridwidth = 1;
    add(jcbLEVEL, c);
    btnEDIT2.setEnabled(false);
    btnREMOVE2.setEnabled(false);
    jcbLEVEL.setEnabled(false);
  }

  private void loadOwners() {
    int prev = jcbOWNER.getSelectedIndex();
    jcbOWNER.removeAllItems();

    String[] ret = PLANET.getOwners();
    for (int i = 0; i < ret.length; i++) {
      jcbOWNER.addItem(new ID(ret[i], i));
    }

    if (ret.length == 0) {
      btnEDIT.setEnabled(false);
      btnREMOVE.setEnabled(false);
    } else {
      btnEDIT.setEnabled(true);
      btnREMOVE.setEnabled(true);

      if (prev >= 0 && prev < ret.length)
        jcbOWNER.setSelectedIndex(prev);
    }
  }

  protected void updateOwnerSelection() {
    ID own = (ID) jcbOWNER.getSelectedItem();
    if (own != null)
      jlPLANET.setListData(PLANET.getPlanets(own.ID));
  }

  @Override
  public String menuCommand() {
    return MenuCommand.MeSystems;
  }

  @Override
  public boolean hasChanges() {
    return PLANET.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(PLANET, changes);
  }

  @Override
  public void reload() throws IOException {
    PLANET = (Meplanet) stbof.getInternalFile(CStbofFiles.MEPlanetBin, true);
    int prev = jlPLANET.getSelectedIndex();

    // unset selected planet data
    jlPLANET.removeAll();
    jcbLEVEL.setSelectedIndex(-1);
    btnEDIT2.setEnabled(false);
    btnREMOVE2.setEnabled(false);
    jcbLEVEL.setEnabled(false);
    // reload owner
    loadOwners();

    // restore planet selection
    if (prev >= 0 && prev < jlPLANET.getModel().getSize())
      jlPLANET.setSelectedIndex(prev);
  }

  @Override
  public void finalWarning() {
  }
}
