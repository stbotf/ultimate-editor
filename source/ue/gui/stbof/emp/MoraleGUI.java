package ue.gui.stbof.emp;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Morale;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.service.Language;

/**
 * This class is used to edit morale penalties for major races.
 */
public class MoraleGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // read
  Stbof stbof;

  // edited
  private Morale MORALE = null;

  // ui elements
  JLabel[] lblDESC = new JLabel[28];
  JTextField[] txtMORAL = new JTextField[115];

  /**
   * Creates an instance of this class.
   * @throws IOException
   */
  public MoraleGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;
    this.MORALE = (Morale) stbof.getInternalFile(CStbofFiles.MoraleBin, true);
    RaceRst race = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);

    //build and set up
    Font def = UE.SETTINGS.getDefaultFont();
    for (short i = 0; i < NUM_EMPIRES; i++) {
      lblDESC[i] = new JLabel(race.getName(i));
      lblDESC[i].setFont(def);
      lblDESC[i].setHorizontalAlignment(SwingConstants.CENTER);
    }

    String str = null;
    for (short i = 0; i < 23; i++) {
      switch (i) {
        case 0: {
          str = Language.getString("MoraleGUI.0"); //$NON-NLS-1$
          break;
        }
        case 1: {
          str = Language.getString("MoraleGUI.1"); //$NON-NLS-1$
          break;
        }
        case 2: {
          str = Language.getString("MoraleGUI.2"); //$NON-NLS-1$
          break;
        }
        case 3: {
          str = Language.getString("MoraleGUI.3"); //$NON-NLS-1$
          break;
        }
        case 4: {
          str = Language.getString("MoraleGUI.4"); //$NON-NLS-1$
          break;
        }
        case 5: {
          str = Language.getString("MoraleGUI.5"); //$NON-NLS-1$
          break;
        }
        case 6: {
          str = Language.getString("MoraleGUI.6"); //$NON-NLS-1$
          break;
        }
        case 7: {
          str = Language.getString("MoraleGUI.7"); //$NON-NLS-1$
          break;
        }
        case 8: {
          str = Language.getString("MoraleGUI.8"); //$NON-NLS-1$
          break;
        }
        case 9: {
          str = Language.getString("MoraleGUI.9"); //$NON-NLS-1$
          break;
        }
        case 10: {
          str = Language.getString("MoraleGUI.10"); //$NON-NLS-1$
          break;
        }
        case 11: {
          str = Language.getString("MoraleGUI.11"); //$NON-NLS-1$
          break;
        }
        case 12: {
          str = Language.getString("MoraleGUI.12"); //$NON-NLS-1$
          break;
        }
        case 13: {
          str = Language.getString("MoraleGUI.13"); //$NON-NLS-1$
          break;
        }
        case 14: {
          str = Language.getString("MoraleGUI.14"); //$NON-NLS-1$
          break;
        }
        case 15: {
          str = Language.getString("MoraleGUI.15"); //$NON-NLS-1$
          break;
        }
        case 16: {
          str = Language.getString("MoraleGUI.16"); //$NON-NLS-1$
          break;
        }
        case 17: {
          str = Language.getString("MoraleGUI.17"); //$NON-NLS-1$
          break;
        }
        case 18: {
          str = Language.getString("MoraleGUI.18"); //$NON-NLS-1$
          break;
        }
        case 19: {
          str = Language.getString("MoraleGUI.19"); //$NON-NLS-1$
          break;
        }
        case 20: {
          str = Language.getString("MoraleGUI.20"); //$NON-NLS-1$
          break;
        }
        case 21: {
          str = Language.getString("MoraleGUI.21"); //$NON-NLS-1$
          break;
        }
        case 22: {
          str = Language.getString("MoraleGUI.22"); //$NON-NLS-1$
          break;
        }
      }

      int a = i + NUM_EMPIRES;
      lblDESC[a] = new JLabel(str);
      lblDESC[a].setFont(def);
      lblDESC[a].setHorizontalAlignment(SwingConstants.CENTER);
      for (int j = 0; j < NUM_EMPIRES; j++) {
        a = i * NUM_EMPIRES + j;
        txtMORAL[a] = new JTextField(NUM_EMPIRES);
        txtMORAL[a].setFont(def);
        txtMORAL[a].setHorizontalAlignment(SwingConstants.CENTER);
      }
    }

    //lay out
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(0, 5, 0, 5);
    c.fill = GridBagConstraints.BOTH;

    //top
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    for (int i = 0; i < NUM_EMPIRES; i++) {
      c.gridx = i + 1;
      add(lblDESC[i], c);
    }

    //rest
    for (int i = 0; i < 23; i++) {
      c.insets = new Insets(0, 5, 0, 5);
      c.gridy = i + 1;
      c.gridx = 0;
      add(lblDESC[i + NUM_EMPIRES], c);
      for (int j = 0; j < NUM_EMPIRES; j++) {
        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = j + 1;
        add(txtMORAL[i * NUM_EMPIRES + j], c);
      }
    }

    loadMorale();
  }

  private void loadMorale() {
    for (int i = 0; i < 23; i++) {
      for (int j = 0; j < NUM_EMPIRES; j++) {
        int morale = MORALE.getValue(j, i);
        txtMORAL[i * NUM_EMPIRES + j].setText(Integer.toString(morale));
      }
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.MoralePenalties;
  }

  @Override
  public boolean hasChanges() {
    return MORALE.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(MORALE, changes);
  }

  @Override
  public void reload() throws IOException {
    MORALE = (Morale) stbof.getInternalFile(CStbofFiles.MoraleBin, true);
    loadMorale();
  }

  @Override
  public void finalWarning() throws InvalidArgumentsException {
    for (int i = 0; i < 23; i++) {
      for (int j = 0; j < NUM_EMPIRES; j++) {
        String ans = txtMORAL[i * NUM_EMPIRES + j].getText();
        short shrt = Short.parseShort(ans);
        MORALE.changeValue(j, i, shrt);
      }
    }
  }
}
