package ue.gui.stbof.emp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import lombok.val;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.SystemReq;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.Edifice.FilterMask;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.pst.Planet;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.tec.RaceTech;
import ue.edit.res.stbof.files.tec.TecField;
import ue.edit.res.stbof.files.tec.data.TechEntry;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

public class StartConGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_TECH_FIELDS = CStbof.NUM_TECH_FIELDS;

  // read
  private Stbof stbof;
  private RaceRst RACE = null;
  private Planet PLANET = null;
  private TecField TECH = null;
  private Edifice BUILDING = null;

  // edited
  private RaceTech STARTCON = null;

  // ui components
  private JList<ID> lstRACE = new JList<ID>();
  private JTextField[] txtLVL = new JTextField[NUM_TECH_FIELDS];
  private JTextField[] txtPNT = new JTextField[NUM_TECH_FIELDS];
  private JPanel pnlBUILD = new JPanel(new GridBagLayout());
  private IndexedCheckBox[] chkBUILD;
  private JComboBox<ID> cmbMASK = new JComboBox<ID>();
  private JComboBox<ID> cmbLVLMASK = new JComboBox<ID>();
  private JButton btnMARKALL = new JButton(Language.getString("StartConGUI.0")); //$NON-NLS-1$
  private JButton btnAUTOMARK = new JButton(Language.getString("StartConGUI.1")); //$NON-NLS-1$
  private JButton btnAUTOMARKALL = new JButton(Language.getString("StartConGUI.2")); //$NON-NLS-1$

  // data
  private int SELECTED = -1;

  public StartConGUI(Stbof st) throws IOException {
    stbof = st;
    BUILDING = (Edifice) st.getInternalFile(CStbofFiles.EdificeBst, true);
    STARTCON = (RaceTech) st.getInternalFile(CStbofFiles.RaceTechTec, true);
    RACE = (RaceRst) st.getInternalFile(CStbofFiles.RaceRst, true);
    PLANET = (Planet) st.getInternalFile(CStbofFiles.PlanetPst, true);
    TECH = (TecField) st.getInternalFile(CStbofFiles.TecFieldTec, true);

    // build
    for (int i = 0; i < NUM_TECH_FIELDS; i++) {
      txtLVL[i] = new JTextField(6);
      txtPNT[i] = new JTextField(6);
    }

    btnMARKALL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SELECTED < 0 || chkBUILD == null || chkBUILD.length <= 0)
          return;

        boolean val = !chkBUILD[0].isSelected();
        for (int j = 0; j < chkBUILD.length; j++) {
          chkBUILD[j].setSelected(val);
        }
      }
    });

    btnAUTOMARK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SELECTED < 0)
          return;

        try {
          finalWarning();
          TechEntry techEntry = STARTCON.getTechEntry(SELECTED);

          // limit to supported startcon building count
          int num = BUILDING.getNumberOfEntries();
          num = Integer.min(num, STARTCON.getMaxBuidings());

          for (int bld = 0; bld < num; bld++) {
            techEntry.setAvailable(bld, isBuildable(SELECTED, bld, true));
          }

          // refresh
          fillList();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    btnAUTOMARKALL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          finalWarning();

          // limit to supported startcon building count
          int num = BUILDING.getNumberOfEntries();
          num = Integer.min(num, STARTCON.getMaxBuidings());

          for (int SEL = 0; SEL < STARTCON.getNumberOfEntries(); SEL++) {
            TechEntry techEntry = STARTCON.getTechEntry(SEL);
            for (int bld = 0; bld < num; bld++) {
              techEntry.setAvailable(bld, isBuildable(SEL, bld, true));
            }
          }

          // refresh
          fillList();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    // setup
    Font def = UE.SETTINGS.getDefaultFont();
    btnMARKALL.setFont(def);
    btnAUTOMARK.setFont(def);
    btnAUTOMARKALL.setFont(def);
    lstRACE.setFont(def);

    for (int i = 0; i < NUM_TECH_FIELDS; i++) {
      txtLVL[i].setFont(def);
      txtPNT[i].setFont(def);
    }

    lstRACE.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        // save
        try {
          finalWarning();

          // refresh
          fillRace();
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }
      }
    });

    // combo
    cmbMASK.setFont(def);
    cmbMASK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SELECTED < 0 || cmbMASK.getSelectedIndex() < 0 || cmbLVLMASK.getSelectedIndex() < 0)
          return;

        try {
          finalWarning();

          // refresh
          updateLvls();
          fillList();
        } catch (Exception ex) {
          Dialogs.displayError(ex);
          return;
        }
      }
    });

    cmbLVLMASK.setFont(def);
    cmbLVLMASK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SELECTED < 0 || cmbMASK.getSelectedIndex() < 0 || cmbLVLMASK.getSelectedIndex() < 0)
          return;

        try {
          finalWarning();

          // refresh
          fillList();
        } catch (Exception ex) {
          // really bad stuff happening - bail
          Dialogs.displayError(ex);
        }
      }
    });

    // place
    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    add(Box.createHorizontalGlue());

    // panels
    {
      // left panel
      JScrollPane scrRace = new JScrollPane(lstRACE);
      scrRace.setPreferredSize(new Dimension(200, 320));

      JPanel pnl = new JPanel(new BorderLayout());
      pnl.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      pnl.add(scrRace, BorderLayout.CENTER);
      add(pnl);

      // center panel
      pnl = new JPanel(new GridBagLayout());
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;

      String str = Language.getString("StartConGUI.3"); //$NON-NLS-1$
      String[] strt = TECH.getFields();
      for (int i = 0; i < NUM_TECH_FIELDS; i++) {
        JLabel lbl = new JLabel(str.replace("%1", strt[i])); //$NON-NLS-1$
        lbl.setFont(def);
        c.gridx = 0;
        c.gridy = i + 1;
        pnl.add(lbl, c);
        c.gridx = 1;
        pnl.add(txtLVL[i], c);
        c.gridx = 2;
        lbl = new JLabel(Language.getString("StartConGUI.4")); //$NON-NLS-1$
        lbl.setFont(def);
        pnl.add(lbl, c);
        c.gridx = 3;
        pnl.add(txtPNT[i], c);
      }

      c.gridwidth = 4;
      c.gridx = 0;
      c.gridy = 8;
      pnl.add(btnAUTOMARK, c);
      c.gridy = 9;
      pnl.add(btnAUTOMARKALL, c);

      JPanel tmpPnl = new JPanel(new BorderLayout());
      tmpPnl.add(pnl, BorderLayout.CENTER);
      pnl = new JPanel(new BorderLayout());
      pnl.add(tmpPnl, BorderLayout.NORTH);
      add(pnl);

      // right list panel
      JPanel listPnl = new JPanel(new BorderLayout());
      pnl = new JPanel(new BorderLayout());
      JPanel miniPnl = new JPanel(new BorderLayout());
      miniPnl.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      miniPnl.add(cmbMASK, BorderLayout.CENTER);
      pnl.add(miniPnl, BorderLayout.CENTER);
      miniPnl = new JPanel(new BorderLayout());
      miniPnl.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      miniPnl.add(cmbLVLMASK, BorderLayout.CENTER);
      pnl.add(miniPnl, BorderLayout.EAST);
      listPnl.add(pnl, BorderLayout.NORTH);

      JScrollPane JSP = new JScrollPane(pnlBUILD);
      JSP.setPreferredSize(new Dimension(200, 320));
      JScrollBar Jscr = JSP.getVerticalScrollBar();
      Jscr.setBlockIncrement(100);
      Jscr.setUnitIncrement(20);
      JSP.setVerticalScrollBar(Jscr);
      miniPnl = new JPanel(new BorderLayout());
      miniPnl.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
      miniPnl.add(JSP, BorderLayout.CENTER);
      listPnl.add(miniPnl, BorderLayout.CENTER);
      miniPnl = new JPanel(new BorderLayout());
      miniPnl.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      miniPnl.add(btnMARKALL, BorderLayout.CENTER);
      listPnl.add(miniPnl, BorderLayout.SOUTH);
      add(listPnl);
    }

    add(Box.createHorizontalGlue());

    // fill
    String str = Language.getString("StartConGUI.5"); //$NON-NLS-1$
    cmbMASK.addItem(new ID(Language.getString("StartConGUI.6"), FilterMask.All)); //$NON-NLS-1$
    for (int n = 0; n < NUM_EMPIRES; n++) {
      cmbMASK.addItem(new ID(RACE.getName(n), n + 1));
    }
    cmbMASK.addItem(new ID(Language.getString("StartConGUI.7"), FilterMask.AllEmps)); //$NON-NLS-1$
    for (int n = 0; n < NUM_EMPIRES; n++) {
      cmbMASK.addItem(new ID(str.replace("%1", RACE.getName(n)), n + 7)); //$NON-NLS-1$
    }
    cmbMASK.addItem(new ID(Language.getString("StartConGUI.8"), FilterMask.SingleResident)); //$NON-NLS-1$

    updateLvls();

    lstRACE.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    loadStartCon();
    lstRACE.setSelectedIndex(0);
  }

  protected void fillRace() {
    SELECTED = lstRACE.getSelectedIndex();
    if (SELECTED < 0)
      return;

    try {
      for (int i = 0; i < NUM_TECH_FIELDS; i++) {
        TechEntry techEntry = STARTCON.getTechEntry(SELECTED);
        txtLVL[i].setText(Short.toString(techEntry.getTechLvl(i)));
        txtPNT[i].setText(Integer.toString(techEntry.getTechPnt(i)));
      }

      fillList();
    } catch (Exception z) {
      Dialogs.displayError(z);
    }
  }

  private void loadStartCon() {
    short[] butt = STARTCON.getAll();
    ID[] ids = new ID[butt.length];
    int raceCtr = 1;

    String raceName = null;
    for (int i = 0; i < butt.length; i++) {
      if (raceName != null && raceName.equals(RACE.getName(butt[i]))) {
        raceCtr++;
      } else {
        raceName = RACE.getName(butt[i]);
        raceCtr = 1;
      }

      ids[i] = new ID(raceName + " " + raceCtr, i); //$NON-NLS-1$
    }

    // restore selection
    int prev = lstRACE.getSelectedIndex();
    lstRACE.setListData(ids);
    if (prev >= 0 && prev < ids.length)
      lstRACE.setSelectedIndex(prev);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.StartTech;
  }

  protected void updateLvls() {
    ID selMask = (ID) cmbMASK.getSelectedItem();
    int mask = selMask != null ? selMask.ID : FilterMask.All;

    ID sel = (ID) cmbLVLMASK.getSelectedItem();
    cmbLVLMASK.removeAllItems();

    ID all = new ID(Language.getString("StartConGUI.9"), 0); //$NON-NLS-1$
    cmbLVLMASK.addItem(all);
    String str = Language.getString("StartConGUI.10"); //$NON-NLS-1$

    int[] techLvls = BUILDING.usedTechLevels(mask);
    for (int tlvl : techLvls) {
      String lvlName = str.replace("%1", Integer.toString(tlvl)); //$NON-NLS-1$
      ID id = new ID(lvlName, tlvl + 1);
      cmbLVLMASK.addItem(id);
    }

    // restore selection
    cmbLVLMASK.setSelectedItem(sel != null ? sel : all);
  }

  private void fillList() {
    if (SELECTED < 0)
      return;

    try {
      byte msk = (byte) cmbMASK.getSelectedIndex();
      ID lvlSel = (ID) cmbLVLMASK.getSelectedItem();
      byte lvlmsk = lvlSel != null ? (byte) lvlSel.ID : 0;
      pnlBUILD.removeAll();
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 5, 5);
      c.fill = GridBagConstraints.BOTH;
      c.gridwidth = 1;
      c.gridheight = 1;
      c.gridy = 0;
      c.gridx = 0;
      Font def = UE.SETTINGS.getDefaultFont();
      int y = 0;
      ArrayList<ID> accept = new ArrayList<ID>();
      HashSet<Integer> errors = new HashSet<Integer>();

      ID[] ed = BUILDING.getShortIDs(msk, 20);
      for (int i = 0; i < ed.length; i++) {
        int bid = ed[i].ID;

        try {
          Building bld = BUILDING.building(bid);
          if (lvlmsk == 0) {
            accept.add(ed[i]);
          } else if (bld.getTechLevel() == (byte) (lvlmsk - 1)) {
            accept.add(ed[i]);
          }
        } catch (Exception ex) {
          ex.printStackTrace();
          errors.add(i);
        }
      }

      chkBUILD = new IndexedCheckBox[accept.size()];

      for (int i = 0; i < accept.size(); i++) {
        int bid = accept.get(i).ID;

        try {
          TechEntry techEntry = STARTCON.getTechEntry(SELECTED);
          Building bld = BUILDING.building(bid);
          c.gridy = y++;

          String name = bid + ": " + bld.getName(); //$NON-NLS-1$
          chkBUILD[i] = new IndexedCheckBox(name, bid, techEntry.isAvailable(bid));
          chkBUILD[i].setFont(def);
          pnlBUILD.add(chkBUILD[i], c);
        } catch (Exception ex) {
          ex.printStackTrace();
          errors.add(i);
        }
      }

      if (!errors.isEmpty()) {
        String msg = Language.getString("StartConGUI.11"); //$NON-NLS-1$
        msg += "\n" + errors.stream().sorted().map(Object::toString)
          .collect(Collectors.joining(", "));
        Dialogs.displayError(msg);
      }

      // update view
      validate();
      pnlBUILD.setVisible(false);
      pnlBUILD.setVisible(true);
    } catch (Exception z) {
      Dialogs.displayError(z);
    }
  }

  private class IndexedCheckBox extends JCheckBox {

    public int chk_INDEX;

    public IndexedCheckBox(String str, int index, boolean value) {
      super(str, value);
      chk_INDEX = index;
    }
  }

  @Override
  public boolean hasChanges() {
    return STARTCON.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(STARTCON, changes);
  }

  @Override
  public void reload() throws IOException {
    STARTCON = (RaceTech) stbof.getInternalFile(CStbofFiles.RaceTechTec, true);
    SELECTED = -1;
    loadStartCon();
  }

  @Override
  public void finalWarning() {
    if (SELECTED < 0)
      return;

    TechEntry techEntry = STARTCON.getTechEntry(SELECTED);

    for (int i = 0; i < NUM_TECH_FIELDS; i++) {
      short q = Short.parseShort(txtLVL[i].getText());
      int w = Integer.parseInt(txtPNT[i].getText());

      if (q >= 0)
        techEntry.setTechLvl(i, q);
      if (w >= 0)
        techEntry.setTechPnt(i, w);
    }

    // limit to supported startcon building count
    int num = STARTCON.getMaxBuidings();

    // apply current check box selections
    for (int j = 0; j < chkBUILD.length; j++) {
      val bld = chkBUILD[j];
      if (bld.chk_INDEX < num)
        techEntry.setAvailable(bld.chk_INDEX, bld.isSelected());
    }
  }

  private boolean isBuildable(int techEraIndex, int buildingID, boolean testUpgrades) {
    TechEntry techEntry = STARTCON.getTechEntry(techEraIndex);
    Building bld = BUILDING.building(buildingID);
    int race = techEntry.raceId();

    int startLvls[] = new int[6];
    for (int i = 0; i < 6; i++)
      startLvls[i] = techEntry.getTechLvl(i);

    long mask = 1L << race;
    if (race >= NUM_EMPIRES) {
      if ((mask & bld.getResidentRaceReq()) != mask)
        return false;

      int sysReq = bld.getSystemReq();
      if (sysReq == SystemReq.Dilithium && !RACE.getDilithium(race))
        return false;

      if (sysReq <= SystemReq.VolcanicPlanet) {
        int planetIdx = PLANET.getIndex(RACE.getPlanet(race));
        if (sysReq != PLANET.getPlanet(planetIdx).getPlanetType())
          return false;
      }

      // if the building is not buildable by all empires
      // and is not a main building
      if ((bld.getExcludeEmpire() | 0xE0) != 0xE0 && !bld.isMainBuilding())
        return false;

      // check tech levels
      int typ = bld.getTechField();
      int lvl = bld.getTechLevel();

      if (typ == 6) {
        // all techs
        for (int slvl : startLvls) {
          if (slvl < lvl)
            return false;
        }
      } else if (startLvls[typ] < lvl) {
        return false;
      }
    } else {
      if ((mask & bld.getExcludeEmpire()) == mask)
        return false;

      // check tech levels
      int typ = bld.getTechField();
      int lvl = bld.getTechLevel();

      if (typ == 6) {
        // all techs
        for (int slvl : startLvls) {
          if (slvl < lvl)
            return false;
        }
      } else if (startLvls[typ] < lvl) {
        return false;
      }
    }

    if (testUpgrades) {
      // check if buildings that this one upgrades to are buildable
      // and if so make this one not buildable
      for (int id = 0; id < BUILDING.getNumberOfEntries(); id++) {
        if (buildingID == id)
          continue;

        Building bx = BUILDING.building(id);
        if (bx.getUpgradeFor() == buildingID) {
          if (this.isBuildable(techEraIndex, id, false))
            return false;
        }
      }
    }

    return true;
  }
}
