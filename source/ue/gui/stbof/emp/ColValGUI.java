package ue.gui.stbof.emp;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.wtf.Colval;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

public class ColValGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // read
  private Stbof stbof;

  // edited
  private Colval[] COLON = new Colval[NUM_EMPIRES];

  // ui components
  private JComboBox<ID> jcbEMP;
  private JTextField[] txtSET;
  private int SELECTED = -1;

  /**
   * constructor
   * @throws IOException
   */
  public ColValGUI(Stbof st) throws IOException {
    this.stbof = st;

    COLON[0] = (Colval) st.getInternalFile(CStbofFiles.CColValWtf, true);
    COLON[1] = (Colval) st.getInternalFile(CStbofFiles.HColValWtf, true);
    COLON[2] = (Colval) st.getInternalFile(CStbofFiles.FColValWtf, true);
    COLON[3] = (Colval) st.getInternalFile(CStbofFiles.KColValWtf, true);
    COLON[4] = (Colval) st.getInternalFile(CStbofFiles.RColValWtf, true);
    RaceRst RACE = (RaceRst) st.getInternalFile(CStbofFiles.RaceRst, true);

    //create
    jcbEMP = new JComboBox<ID>();
    txtSET = new JTextField[13];

    //setup
    Font def = UE.SETTINGS.getDefaultFont();
    jcbEMP.setFont(def);

    for (int i = 0; i < 13; i++) {
      txtSET[i] = new JTextField(10);
      txtSET[i].setFont(def);
    }

    jcbEMP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (jcbEMP.getSelectedIndex() < 0) {
          return;
        }
        try {
          finalWarning();
          SELECTED = jcbEMP.getSelectedIndex();
          fillEmpire();
        } catch (Exception t) {
          Dialogs.displayError(t);
        }
      }
    });

    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      jcbEMP.addItem(new ID(RACE.getName(emp), emp));
    }

    //place
    JLabel lbl;
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = 1;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.fill = GridBagConstraints.BOTH;
    c.insets = new Insets(5, 5, 5, 5);
    add(jcbEMP, c);
    c.gridx = 0;
    c.gridy = 1;
    lbl = new JLabel(Language.getString("ColValGUI.0")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 2;
    lbl = new JLabel(Language.getString("ColValGUI.1")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 3;
    lbl = new JLabel(Language.getString("ColValGUI.2")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 4;
    lbl = new JLabel(Language.getString("ColValGUI.3")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 5;
    lbl = new JLabel(Language.getString("ColValGUI.4")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 6;
    lbl = new JLabel(Language.getString("ColValGUI.5")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 7;
    lbl = new JLabel(Language.getString("ColValGUI.6")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 8;
    lbl = new JLabel(Language.getString("ColValGUI.7")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 9;
    lbl = new JLabel(Language.getString("ColValGUI.8")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 10;
    lbl = new JLabel(Language.getString("ColValGUI.9")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 11;
    lbl = new JLabel(Language.getString("ColValGUI.10")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 12;
    lbl = new JLabel(Language.getString("ColValGUI.11")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 13;
    lbl = new JLabel(Language.getString("ColValGUI.12")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridx = 1;

    for (int i = 0; i < 13; i++) {
      c.gridy = 1 + i;
      add(txtSET[i], c);
    }
  }

  protected void fillEmpire() {
    for (int i = 0; i < 13; i++) {
      String text = SELECTED < 0 ? "" : Double.toString(COLON[SELECTED].getValue(0, i));
      txtSET[i].setText(text);
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.ColonyVal;
  }

  @Override
  public boolean hasChanges() {
    for (Colval val : COLON) {
      if (val.madeChanges())
        return true;
    }
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    for (Colval val : COLON)
      stbof.checkChanged(val, changes);
  }

  @Override
  public void reload() throws IOException {
    COLON[0] = (Colval) stbof.getInternalFile(CStbofFiles.CColValWtf, true);
    COLON[1] = (Colval) stbof.getInternalFile(CStbofFiles.HColValWtf, true);
    COLON[2] = (Colval) stbof.getInternalFile(CStbofFiles.FColValWtf, true);
    COLON[3] = (Colval) stbof.getInternalFile(CStbofFiles.KColValWtf, true);
    COLON[4] = (Colval) stbof.getInternalFile(CStbofFiles.RColValWtf, true);
    fillEmpire();
  }

  @Override
  public void finalWarning() {
    if (SELECTED < 0) {
      return;
    }
    for (int i = 0; i < 13; i++) {
      String str = txtSET[i].getText();
      double t = Double.parseDouble(str);
      COLON[SELECTED].setValue(0, i, t);
    }
  }
}
