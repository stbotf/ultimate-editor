package ue.gui.stbof.emp;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.StructureBonus;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.AIBldReq;
import ue.edit.res.stbof.files.bin.data.AIBldReqEntry;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.Edifice.FilterMask;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.exception.KeyNotFoundException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.component.IconButton;
import ue.gui.util.component.ImageButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.util.data.ID;

/**
 * Allows editing aibldreq.bin
 */
public class AIBldReqGUI extends MainPanel {

  // read
  private Stbof stbof;
  private Edifice EDIFICE = null;

  // edited
  private AIBldReq AIBLDREQ;

  // ui components
  private IconButton btnMoveUp = IconButton.CreateUpButton();
  private IconButton btnMoveDown = IconButton.CreateDownButton();
  private ImageButton btnAdd = ImageButton.CreatePlusButton();
  private ImageButton btnRemove = ImageButton.CreateMinusButton();
  private JButton btnSync = new JButton(Language.getString("AIBldReqGUI.12"));
  private JLabel[] lblDesc = new JLabel[5];
  private JLabel lblName = new JLabel();
  private JLabel lblID = new JLabel();
  private JTextField[] txtReq = new JTextField[3];
  private JList<ID> lstBld = new JList<ID>();

  // data
  private ID SELECTED = null;

  public AIBldReqGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;
    AIBLDREQ = (AIBldReq) stbof.getInternalFile(CStbofFiles.AIBldReqBin, true);
    EDIFICE = (Edifice) stbof.tryGetInternalFile(CStbofFiles.EdificeBst, true);

    setupComponents();
    placeComponents();
    addListeners();

    fillList();

    // select first entry
    if (lstBld.getModel().getSize() > 0)
      lstBld.setSelectedIndex(0);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.AiBldReq;
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();

    btnMoveUp.setEnabled(false);
    btnMoveDown.setEnabled(false);
    btnAdd.setEnabled(EDIFICE != null);
    btnRemove.setEnabled(false);

    // labels
    lblDesc[0] = new JLabel(Language.getString("AIBldReqGUI.6")); //$NON-NLS-1$
    lblDesc[1] = new JLabel(Language.getString("AIBldReqGUI.7")); //$NON-NLS-1$
    lblDesc[2] = new JLabel(Language.getString("AIBldReqGUI.8")); //$NON-NLS-1$
    lblDesc[3] = new JLabel(Language.getString("AIBldReqGUI.9")); //$NON-NLS-1$
    lblDesc[4] = new JLabel(Language.getString("AIBldReqGUI.10")); //$NON-NLS-1$
    for (JLabel lbl : lblDesc) {
      lbl.setFont(def);
    }

    // text fields
    lblName.setFont(def);
    lblID.setFont(def);
    txtReq[0] = new JTextField(6);
    txtReq[1] = new JTextField(6);
    txtReq[2] = new JTextField(6);

    // makes sure entered values are numbers equal or higher than 0
    InputVerifier iv = new InputVerifier() {
      @Override
      public boolean verify(JComponent input) {
        if (input instanceof JTextField) {
          String str = ((JTextField) input).getText();

          try {
            int val = Integer.parseInt(str);

            return val >= 0;
          } catch (Exception e) {
            // do nothing
          }
        }
        return false;
      }
    };

    for (JTextField txt : txtReq) {
      txt.setFont(def);
      txt.setInputVerifier(iv);
    }

    // list
    lstBld.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lstBld.setFont(def);

    btnSync.setFont(def);
    btnSync.setToolTipText(Language.getString("AIBldReqGUI.13"));
  }

  private void placeComponents() {
    JPanel pnlEdit = new JPanel(new GridBagLayout());
    pnlEdit.setAlignmentY(JComponent.TOP_ALIGNMENT);

    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.NORTH;
    c.gridx = 0;
    c.gridy = 0;
    pnlEdit.add(lblDesc[0], c);
    c.gridy++;
    pnlEdit.add(lblDesc[1], c);
    c.gridy++;
    pnlEdit.add(lblDesc[2], c);
    c.gridy++;
    pnlEdit.add(lblDesc[3], c);
    c.gridy++;
    pnlEdit.add(lblDesc[4], c);
    c.gridx++;
    c.gridy = 0;
    pnlEdit.add(Box.createHorizontalStrut(150), c);
    pnlEdit.add(lblName, c);
    c.gridy++;
    pnlEdit.add(lblID, c);
    c.gridy++;
    pnlEdit.add(txtReq[0], c);
    c.gridy++;
    pnlEdit.add(txtReq[1], c);
    c.gridy++;
    pnlEdit.add(txtReq[2], c);
    c.gridx = 0;
    c.gridy++;
    c.gridwidth = 2;
    pnlEdit.add(btnSync, c);
    c.gridwidth = 1;

    // Buttons panel
    JPanel pnlButtons = new JPanel();
    pnlButtons.setLayout(new BoxLayout(pnlButtons, BoxLayout.X_AXIS));
    {
      pnlButtons.add(btnMoveUp);
      pnlButtons.add(btnMoveDown);
      pnlButtons.add(Box.createHorizontalGlue());
      pnlButtons.add(btnAdd);
      pnlButtons.add(btnRemove);
    }

    JPanel pnlList = new JPanel();
    {
      pnlList.setLayout(new BoxLayout(pnlList, BoxLayout.Y_AXIS));
      JScrollPane scrollPane = new JScrollPane(lstBld);
      scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      pnlList.add(scrollPane);
      pnlList.add(pnlButtons);
    }

    setLayout(new GridBagLayout());
    setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
    c.weightx = 1;
    c.weighty = 1;
    c.gridx = 0;
    c.gridy = 0;
    add(pnlList, c);
    c.weightx = 0;
    c.gridx = 1;
    add(pnlEdit, c);
  }

  private void addListeners() {
    btnMoveUp.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != null) {
        try {
          AIBLDREQ.moveUp(SELECTED.ID);
        }
        finally {
          fillList();
        }
      }
    }));

    btnMoveDown.addActionListener(new CBActionListener(evt -> {
      if (SELECTED != null) {
        finalWarning();
        try {
          AIBLDREQ.moveDown(SELECTED.ID);
        }
        finally {
          fillList();
        }
      }
    }));

    btnAdd.addActionListener(new CBActionListener(evt -> {
      finalWarning();
      if (EDIFICE == null)
        return;

      ID[] ids = EDIFICE.getShortIDs(FilterMask.All, 20);
      ArrayList<ID> l = new ArrayList<ID>();

      for (int i = 0; i < ids.length; i++) {
        Building bld = EDIFICE.building(ids[i].ID);
        if (!bld.isMainBuilding() && !AIBLDREQ.contains(ids[i].ID)
            && bld.getBonusType() != StructureBonus.MartialLaw) {
          l.add(ids[i]);
        }
      }

      ID ret = (ID) JOptionPane.showInputDialog(UE.WINDOW,
        Language.getString("AIBldReqGUI.3"), //$NON-NLS-1$
        Language.getString("AIBldReqGUI.4"), //$NON-NLS-1$
        JOptionPane.PLAIN_MESSAGE,
        null,
        l.toArray(),
        null);

      if (ret != null) {
        AIBLDREQ.add(ret.ID);
        fillList();
        lstBld.setSelectedValue(ret, true);
      }
    }));

    btnRemove.addActionListener(new CBActionListener(evt -> {
      if (SELECTED == null)
        return;

      int sel = lstBld.getSelectedIndex();
      AIBLDREQ.remove(SELECTED.ID);
      SELECTED = null;

      fillList();

      // select next entry
      if (sel >= 0 && sel < lstBld.getModel().getSize())
        lstBld.setSelectedIndex(sel);
    }));

    lstBld.addListSelectionListener(new CBListSelectionListener(evt -> {
      if (SELECTED != null)
        finalWarning();

      SELECTED = lstBld.getSelectedValue();
      updateSelected(SELECTED != null ? SELECTED.ID : -1);
    }));

    btnSync.addActionListener(new CBActionListener(evt -> {
      String msg = Language.getString("AIBldReqGUI.14");
      if (Dialogs.showConfirm(msg)) {
        for (AIBldReqEntry req : AIBLDREQ.listReqs()) {
          Building bld = EDIFICE.getEntry(req.id());
          req.setEnergyReq(bld.getEnergyCost());
        }

        updateSelected(SELECTED != null ? SELECTED.ID : null);
      }
    }));
  }

  private void fillList() {
    ID id = SELECTED;
    SELECTED = null;

    ID[] ids;
    if (EDIFICE != null) {
      String[] names = EDIFICE.getNames();
      int[] hm = AIBLDREQ.getIDList();
      ids = Arrays.stream(hm)
        .filter(x -> x >= 0 && x < names.length)
        .mapToObj(x -> new ID(names[x], x))
        .toArray(ID[]::new);
    } else {
      int[] hm = AIBLDREQ.getIDList();
      ids = Arrays.stream(hm)
        .mapToObj(x -> new ID(Integer.toString(x), x))
        .toArray(ID[]::new);
    }

    lstBld.setListData(ids);
    if (id != null)
      lstBld.setSelectedValue(id, true);
  }

  private void updateSelected(int bldId) {
    if (bldId < 0) {
      // disable everything
      btnMoveUp.setEnabled(false);
      btnMoveDown.setEnabled(false);
      btnRemove.setEnabled(false);
      lblName.setText(null);
      lblID.setText(null);
      for (JTextField txt : txtReq) {
        txt.setEnabled(false);
        txt.setText(null);
      }
    } else {
      // enable and fill everything
      btnMoveUp.setEnabled(true);
      btnMoveDown.setEnabled(true);
      btnAdd.setEnabled(EDIFICE != null);
      btnRemove.setEnabled(true);

      if (EDIFICE != null) {
        try {
          Building bld = EDIFICE.building(bldId);
          lblName.setText(bld.getName());
        } catch (Exception e1) {
          Dialogs.displayError(e1);
          lblName.setText(Language.getString("AIBldReqGUI.11")); // N/A //$NON-NLS-1$
        }
      } else {
        lblName.setText(Language.getString("AIBldReqGUI.11")); // N/A //$NON-NLS-1$
      }

      lblID.setText(Integer.toString(bldId));

      for (JTextField txt : txtReq) {
        txt.setEnabled(true);
      }

      AIBldReqEntry bldReq = AIBLDREQ.getReq(bldId).get();
      txtReq[0].setText(Integer.toString(bldReq.getEnergyReq()));
      txtReq[1].setText(Integer.toString(bldReq.getPopReq()));
      txtReq[2].setText(Integer.toString(bldReq.getMaxPopReq()));
    }
  }

  @Override
  public boolean hasChanges() {
    return AIBLDREQ.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(AIBLDREQ, changes);
  }

  @Override
  public void reload() throws IOException {
    AIBLDREQ = (AIBldReq) stbof.getInternalFile(CStbofFiles.AIBldReqBin, true);
    fillList();
  }

  @Override
  public void finalWarning() throws KeyNotFoundException {
    if (SELECTED == null)
      return;

    AIBldReqEntry bldReq = AIBLDREQ.getReq(SELECTED.ID).get();
    int engy = Integer.parseInt(txtReq[0].getText());
    bldReq.setEnergyReq(engy);
    int pop = Integer.parseInt(txtReq[1].getText());
    bldReq.setPopReq(pop);
    int maxPop = Integer.parseInt(txtReq[2].getText());
    bldReq.setMaxPopReq(maxPop);
  }

}
