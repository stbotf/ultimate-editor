package ue.gui.stbof.emp;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.emp.RaceList;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.wtf.Aiminor;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

public class AiminorGUI extends MainPanel {

  // read
  private Stbof stbof;
  private Trek trek;

  // edited
  private RaceList RL = null;
  private RaceRst RACES;
  private Aiminor AIMINOR;

  // ui components
  private JComboBox<ID> MINORS;
  private MyTextBox[] SETTINGS;
  private JLabel[] lbl_Set;
  private JButton btn_Edit;
  private JButton btn_Add;
  private JButton btn_Remove;

  /**
   * constructor
   * @throws IOException
   */
  public AiminorGUI(Stbof stbof, Trek trek) throws IOException {
    this.stbof = stbof;
    this.trek = trek;
    this.RACES = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    this.AIMINOR = (Aiminor) stbof.getInternalFile(CStbofFiles.AIMinorWtf, true);
    Lexicon dic = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);

    RL = trek != null ? (RaceList) trek.getInternalFile(CTrekSegments.RaceList, true) : null;
    MINORS = new JComboBox<ID>();
    setLayout(new GridBagLayout());

    //settings
    SETTINGS = new MyTextBox[7];
    for (int i = 0; i < 7; i++) {
      SETTINGS[i] = new MyTextBox(i);
    }

    SETTINGS[0].addItem(new ID(Language.getString("AiminorGUI.0"), 0)); //$NON-NLS-1$
    SETTINGS[0].addItem(new ID(Language.getString("AiminorGUI.1"), 1)); //$NON-NLS-1$
    SETTINGS[0].addItem(new ID(Language.getString("AiminorGUI.2"), 2)); //$NON-NLS-1$
    SETTINGS[0].addItem(new ID(Language.getString("AiminorGUI.3"), 3)); //$NON-NLS-1$
    SETTINGS[0].addItem(new ID(Language.getString("AiminorGUI.4"), 4)); //$NON-NLS-1$
    SETTINGS[1].addItem(new ID(Language.getString("AiminorGUI.5"), 5)); //$NON-NLS-1$
    SETTINGS[1].addItem(new ID(Language.getString("AiminorGUI.6"), 6)); //$NON-NLS-1$

    for (int j = 2; j < 7; j++) {
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Enraged), 0));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Hostile), 1));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Icy), 2));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Uncooperative), 3));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Neutral), 4));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Receptive), 5));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Cordial), 6));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Enthusiastic), 7));
      SETTINGS[j].addItem(new ID(dic.getEntry(LexDataIdx.Diplomacy.Reputation.Worshipful), 8));
    }

    //labels
    lbl_Set = new JLabel[7];
    lbl_Set[0] = new JLabel(Language.getString("AiminorGUI.7"),
        SwingConstants.RIGHT); //$NON-NLS-1$
    lbl_Set[1] = new JLabel(Language.getString("AiminorGUI.8"),
        SwingConstants.RIGHT); //$NON-NLS-1$
    for (int j = 2; j < 7; j++) {
      String msg = Language.getString("AiminorGUI.9"); //$NON-NLS-1$
      msg = msg.replace("%1", RACES.getName(j - 2)); //$NON-NLS-1$
      lbl_Set[j] = new JLabel(msg, SwingConstants.RIGHT);
    }

    //buttons
    btn_Edit = new JButton(Language.getString("AiminorGUI.10")); //$NON-NLS-1$
    btn_Edit.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String nName = JOptionPane.showInputDialog(Language.getString("AiminorGUI.11"),
              ((ID) MINORS.getSelectedItem()).toString()); //$NON-NLS-1$

          if (nName != null && nName.length() > 0 && !AIMINOR.containsRace(nName)) {
            int numi = MINORS.getSelectedIndex();

            if (RL != null && RL.getRaceIndex(nName) < 0) {
              int k;
              if ((k = RL.getRaceIndex(AIMINOR.getName(numi))) >= 0) {
                RL.setRaceString(k, nName);
              }
            }

            AIMINOR.setName(numi, nName);
            fillRaces();
            MINORS.setSelectedIndex(numi);
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    btn_Add = new JButton(Language.getString("AiminorGUI.12")); //$NON-NLS-1$
    btn_Add.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String[] pos = AIMINOR.getNames();
          ID[] possibleValues = new ID[pos.length];
          for (int i = 0; i < pos.length; i++) {
            possibleValues[i] = new ID(pos[i], i);
          }

          ID selectedValue = (ID) JOptionPane.showInputDialog(UE.WINDOW,
              Language.getString("AiminorGUI.13"), UE.APP_NAME, //$NON-NLS-1$
              JOptionPane.INFORMATION_MESSAGE, null,
              possibleValues, possibleValues[0]);

          if (selectedValue != null) {
            AIMINOR.add(selectedValue.hashCode());
            fillRaces();
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    btn_Remove = new JButton(Language.getString("AiminorGUI.14")); //$NON-NLS-1$
    btn_Remove.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          if (MINORS.getSelectedIndex() != -1) {
            AIMINOR.remove(MINORS.getSelectedIndex());
            fillRaces();
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //minors
    MINORS.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          if (MINORS.getSelectedIndex() != -1) {
            for (int g = 0; g < 7; g++) {
              SETTINGS[g].setSelectedIndex((int) AIMINOR.getValue(MINORS.getSelectedIndex(), g));
            }
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    fillRaces();

    //set font
    Font def = UE.SETTINGS.getDefaultFont();
    MINORS.setFont(def);
    btn_Edit.setFont(def);
    btn_Add.setFont(def);
    btn_Remove.setFont(def);
    for (int h = 0; h < 7; h++) {
      lbl_Set[h].setFont(def);
      SETTINGS[h].setFont(def);
    }

    //add all
    GridBagConstraints c = new GridBagConstraints();

    //minors
    c.gridx = 1;
    c.gridy = 1;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.EAST;
    c.insets = new Insets(5, 5, 5, 5);
    add(MINORS, c);

    //buttons
    c.fill = GridBagConstraints.NONE;
    c.gridy = 1;
    c.gridx = 0;
    add(btn_Edit, c);

    //settings
    c.fill = GridBagConstraints.BOTH;
    for (int h = 0; h < 7; h++) {
      c.gridx = 0;
      c.gridy++;
      add(lbl_Set[h], c);
      c.gridx = 1;
      add(SETTINGS[h], c);
    }

    setVisible(true);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.MinorAttitudes;
  }

  private void fillRaces() {
    ID sel = (ID) MINORS.getSelectedItem();
    MINORS.removeAllItems();
    for (int i = 0; i < AIMINOR.getNumber(); i++) {
      MINORS.addItem(new ID(AIMINOR.getName(i), i));
    }

    if (sel != null)
      MINORS.setSelectedItem(sel);
  }

  @Override
  public void finalWarning() {
  }

  private class MyTextBox extends JComboBox<ID> {

    int INDEX = 0;

    public MyTextBox(int index) {
      INDEX = index;
      addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
          try {
            if (MINORS.getSelectedIndex() != -1) {
              AIMINOR.setValue(MINORS.getSelectedIndex(), INDEX, getSelectedIndex());
            }
          } catch (Exception e) {
            Dialogs.displayError(e);
          }
        }
      });
    }
  }

  @Override
  public boolean hasChanges() {
    return AIMINOR.madeChanges() || RL != null && RL.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(AIMINOR, changes);
    stbof.checkChanged(RL, changes);
  }

  @Override
  public void reload() throws IOException {
    AIMINOR = (Aiminor) stbof.getInternalFile(CStbofFiles.AIMinorWtf, true);
    RL = trek != null ? (RaceList) trek.getInternalFile(CTrekSegments.RaceList, true) : null;
    fillRaces();
  }
}
