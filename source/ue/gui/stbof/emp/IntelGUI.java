package ue.gui.stbof.emp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.commons.lang3.StringUtils;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Intel;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.common.CSavGame;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.util.data.ID;

/**
 *
 */
public class IntelGUI extends MainPanel {

  private static final int NUM_DIFFICULTIES = CSavGame.NUM_DIFFICULTIES;
  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // read
  private RaceRst races;
  private Lexicon lexicon;

  // edited
  private Stbof stbof;
  private Intel intel;

  // ui components
  private JList<ID> lstEmpires;
  private JLabel lblMult;
  private JLabel lblThreshold;
  private JRadioButton[] btnBaseMult;
  private JSpinner[] spiBaseMult;
  private JRadioButton[] btnTargetMult;
  private JSpinner[] spiTargetMult;
  private JRadioButton[] btnMoraleMult;
  private JSpinner[] spiMoraleMult;
  private JRadioButton[] btnDiffMult;
  private JSpinner[] spiDiffMult;
  private JRadioButton[] btnTreatyMult;
  private JSpinner[] spiTreatyMult;

  // data
  private int SELECTED_EMPIRE = -1;

  public IntelGUI(Stbof stbof) throws IOException {
    this.stbof = stbof;

    // load files
    intel = (Intel) stbof.getInternalFile(CStbofFiles.IntelBin, true);
    races = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    lexicon = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);

    setupComponents();
    placeComponents();

    lstEmpires.setSelectedIndex(0);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.IntelMult;
  }

  private void setupComponents() {
    // label
    lblMult = new JLabel("Product of selected multipliers: N/A");
    lblThreshold = new JLabel("Intel attack delay threshold: N/A");
    lblThreshold.setToolTipText("Calculation: [base multiplier]% * [treaty multiplier]%");

    // list
    lstEmpires = new JList<ID>();

    ID[] ids = new ID[NUM_EMPIRES];
    for (int emp = 0; emp < ids.length; emp++) {
      ids[emp] = new ID(races.getName(emp), emp);
    }

    lstEmpires.setListData(ids);
    lstEmpires.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lstEmpires.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

    lstEmpires.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        try {
          // save changes
          finalWarning();
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }
        updateSelected();
      }
    });

    // radio buttons
    ActionListener listener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateCalculation();
      }
    };

    ButtonGroup group = new ButtonGroup();
    btnBaseMult = new JRadioButton[2];
    btnBaseMult[0] = new JRadioButton("Espionage:");
    btnBaseMult[0].addActionListener(listener);
    group.add(btnBaseMult[0]);
    btnBaseMult[1] = new JRadioButton("Sabotage:");
    btnBaseMult[1].addActionListener(listener);
    group.add(btnBaseMult[1]);
    btnBaseMult[0].setSelected(true);

    btnTargetMult = new JRadioButton[4];
    group = new ButtonGroup();
    for (int i = 0; i < 4; i++) {
      btnTargetMult[i] = new JRadioButton();
      btnTargetMult[i].addActionListener(listener);
      group.add(btnTargetMult[i]);
    }
    btnTargetMult[0].setSelected(true);

    btnMoraleMult = new JRadioButton[8];
    group = new ButtonGroup();
    btnMoraleMult[0] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Rebellious).toLowerCase()));
    btnMoraleMult[1] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Defiant).toLowerCase()));
    btnMoraleMult[2] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Disgruntled).toLowerCase()));
    btnMoraleMult[3] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Apathetic).toLowerCase()));
    btnMoraleMult[4] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Content).toLowerCase()));
    btnMoraleMult[5] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Pleased).toLowerCase()));
    btnMoraleMult[6] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Loyal).toLowerCase()));
    btnMoraleMult[7] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.System.Morale.Fanatic).toLowerCase()));

    for (int i = 0; i < 8; i++) {
      btnMoraleMult[i].addActionListener(listener);
      group.add(btnMoraleMult[i]);
    }
    btnMoraleMult[0].setSelected(true);

    btnDiffMult = new JRadioButton[NUM_DIFFICULTIES];
    group = new ButtonGroup();
    btnDiffMult[0] = new JRadioButton(StringUtils.capitalize(lexicon.getEntry(LexDataIdx.GameSettings.Difficulty.SIMPLE).toLowerCase()));
    btnDiffMult[1] = new JRadioButton(StringUtils.capitalize(lexicon.getEntry(LexDataIdx.GameSettings.Difficulty.EASY).toLowerCase()));
    btnDiffMult[2] = new JRadioButton(StringUtils.capitalize(lexicon.getEntry(LexDataIdx.GameSettings.Difficulty.NORMAL).toLowerCase()));
    btnDiffMult[3] = new JRadioButton(StringUtils.capitalize(lexicon.getEntry(LexDataIdx.GameSettings.Difficulty.HARD).toLowerCase()));
    btnDiffMult[4] = new JRadioButton(StringUtils.capitalize(lexicon.getEntry(LexDataIdx.GameSettings.Difficulty.IMPOSSIBLE).toLowerCase()));

    for (int i = 0; i < NUM_DIFFICULTIES; i++) {
      btnDiffMult[i].addActionListener(listener);
      group.add(btnDiffMult[i]);
    }
    btnDiffMult[0].setSelected(true);

    btnTreatyMult = new JRadioButton[4];
    group = new ButtonGroup();
    btnTreatyMult[0] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.Diplomacy.Relationship.Neutral).toLowerCase()));
    btnTreatyMult[1] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.Diplomacy.Relationship.WAR).toLowerCase()));
    btnTreatyMult[2] = new JRadioButton(StringUtils.capitalize(lexicon.getEntry(LexDataIdx.Diplomacy.Relationship.Alliance).toLowerCase()));
    btnTreatyMult[3] = new JRadioButton(
        StringUtils.capitalize(lexicon.getEntry(LexDataIdx.Diplomacy.Relationship.PEACE).toLowerCase()));

    for (int i = 0; i < 4; i++) {
      btnTreatyMult[i].addActionListener(listener);
      group.add(btnTreatyMult[i]);
    }
    btnTreatyMult[0].setSelected(true);

    // spinners
    ChangeListener chList = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        updateCalculation();
      }
    };

    int maxVal = 9999999;
    int valStep = 1;

    spiBaseMult = new JSpinner[2];
    spiBaseMult[0] = new JSpinner(new SpinnerNumberModel(0, 0, maxVal, valStep));
    spiBaseMult[0].addChangeListener(chList);
    spiBaseMult[1] = new JSpinner(new SpinnerNumberModel(0, 0, maxVal, valStep));
    spiBaseMult[1].addChangeListener(chList);

    spiTargetMult = new JSpinner[4];
    for (int i = 0; i < 4; i++) {
      spiTargetMult[i] = new JSpinner(new SpinnerNumberModel(0, 0, maxVal, valStep));
      spiTargetMult[i].addChangeListener(chList);
    }

    spiMoraleMult = new JSpinner[8];
    for (int i = 0; i < 8; i++) {
      spiMoraleMult[i] = new JSpinner(new SpinnerNumberModel(0, 0, maxVal, valStep));
      spiMoraleMult[i].addChangeListener(chList);
    }

    spiDiffMult = new JSpinner[NUM_DIFFICULTIES];
    for (int i = 0; i < NUM_DIFFICULTIES; i++) {
      spiDiffMult[i] = new JSpinner(new SpinnerNumberModel(0, 0, maxVal, valStep));
      spiDiffMult[i].addChangeListener(chList);
    }

    spiTreatyMult = new JSpinner[8];
    for (int i = 0; i < 4; i++) {
      spiTreatyMult[i] = new JSpinner(new SpinnerNumberModel(0, 0, maxVal, valStep));
      spiTreatyMult[i].addChangeListener(chList);
    }
  }

  protected void updateSelected() {
    SELECTED_EMPIRE = lstEmpires.getSelectedIndex();
    if (SELECTED_EMPIRE < 0)
      return;

    // show data
    try {
      spiBaseMult[0].setValue(intel.getBaseEspAtkMul(SELECTED_EMPIRE));
      spiBaseMult[0].setBackground(Color.WHITE);
    } catch (Exception x) {
      spiBaseMult[0].setValue(0);
      spiBaseMult[0].setBackground(Color.PINK);
    }
    try {
      spiBaseMult[1].setValue(intel.getBaseSabAtkMul(SELECTED_EMPIRE));
      spiBaseMult[1].setBackground(Color.WHITE);
    } catch (Exception x) {
      spiBaseMult[1].setValue(0);
      spiBaseMult[1].setBackground(Color.PINK);
    }

    int index = 0;
    for (int r = 0; r < NUM_EMPIRES; r++) {
      if (r != SELECTED_EMPIRE) {
        try {
          btnTargetMult[index].setText(races.getName(r) + ":");
          spiTargetMult[index].setValue(
              intel.getTargetEmpAtkMul(SELECTED_EMPIRE, r));
          spiTargetMult[index].setBackground(Color.WHITE);
        } catch (Exception x) {
          spiTargetMult[index].setValue(0);
          spiTargetMult[index].setBackground(Color.PINK);
        }
        index++;
      }
    }

    for (int i = 0; i < 8; i++) {
      try {
        spiMoraleMult[i].setValue(
            intel.getMoraleLvlAtkMul(SELECTED_EMPIRE, i));
        spiMoraleMult[i].setBackground(Color.WHITE);
      } catch (Exception x) {
        spiMoraleMult[i].setValue(0);
        spiMoraleMult[i].setBackground(Color.PINK);
      }
    }

    for (int i = 0; i < NUM_DIFFICULTIES; i++) {
      try {
        spiDiffMult[i].setValue(
            intel.getDiffLvlAtkMul(SELECTED_EMPIRE, i));
        spiDiffMult[i].setBackground(Color.WHITE);
      } catch (Exception x) {
        spiDiffMult[i].setValue(0);
        spiDiffMult[i].setBackground(Color.PINK);
      }
    }

    for (int i = 0; i < 4; i++) {
      try {
        spiTreatyMult[i].setValue(
            intel.getTargetTreatyAtkMul(SELECTED_EMPIRE, i));
        spiTreatyMult[i].setBackground(Color.WHITE);
      } catch (Exception x) {
        spiTreatyMult[i].setValue(0);
        spiTreatyMult[i].setBackground(Color.PINK);
      }
    }

    // fix calc
    updateCalculation();
  }

  private void placeComponents() {
    this.setLayout(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.NORTH;

    // left panel
    JLabel label = new JLabel("Empire:");

    JPanel pnlLeft = new JPanel(new BorderLayout());
    pnlLeft.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.BLACK));

    JPanel panel = new JPanel(new GridBagLayout());
    panel.add(label, c);
    c.gridy = 1;
    panel.add(lstEmpires, c);

    pnlLeft.add(panel, BorderLayout.NORTH);

    c.gridy = 0;
    c.gridheight = 7;
    add(pnlLeft, c);

    // base mult
    JPanel pnlBaseMult = new JPanel(new GridLayout(2, 2, 10, 10));
    pnlBaseMult.setBorder(BorderFactory.createTitledBorder("Base multipliers"));

    for (int i = 0; i < 2; i++) {
      pnlBaseMult.add(btnBaseMult[i]);
      pnlBaseMult.add(spiBaseMult[i]);
    }

    c.gridx = 1;
    c.gridheight = 1;
    add(pnlBaseMult, c);

    // diff mult
    JPanel pnlDiffMult = new JPanel(new GridLayout(5, 2, 10, 10));
    pnlDiffMult.setBorder(BorderFactory.createTitledBorder("Difficulty multipliers"));

    for (int i = 0; i < NUM_DIFFICULTIES; i++) {
      pnlDiffMult.add(btnDiffMult[i]);
      pnlDiffMult.add(spiDiffMult[i]);
    }

    c.gridy = 1;
    add(pnlDiffMult, c);

    // target mult
    JPanel pnlTargetMult = new JPanel(new GridLayout(4, 2, 10, 10));
    pnlTargetMult.setBorder(BorderFactory.createTitledBorder("Target empire multipliers"));

    for (int i = 0; i < 4; i++) {
      pnlTargetMult.add(btnTargetMult[i]);
      pnlTargetMult.add(spiTargetMult[i]);
    }

    c.gridy = 3;
    add(pnlTargetMult, c);

    // morale mult
    JPanel pnlMoraleMult = new JPanel(new GridLayout(8, 2, 10, 10));
    pnlMoraleMult.setBorder(BorderFactory.createTitledBorder("Morale multipliers"));

    for (int i = 0; i < 8; i++) {
      pnlMoraleMult.add(btnMoraleMult[i]);
      pnlMoraleMult.add(spiMoraleMult[i]);
    }

    c.gridx = 2;
    c.gridy = 0;
    c.gridheight = 3;
    add(pnlMoraleMult, c);

    // treaty mult
    JPanel pnlTreatyMult = new JPanel(new GridLayout(4, 2, 10, 10));
    pnlTreatyMult.setBorder(BorderFactory.createTitledBorder("Target treaty multipliers"));

    for (int i = 0; i < 4; i++) {
      pnlTreatyMult.add(btnTreatyMult[i]);
      pnlTreatyMult.add(spiTreatyMult[i]);
    }

    c.gridy = 3;
    c.gridheight = 1;
    add(pnlTreatyMult, c);

    // bottom
    c.gridy = 5;
    c.gridx = 1;
    c.gridheight = 1;
    c.gridwidth = 2;
    add(lblThreshold, c);
    c.gridy = 6;
    add(lblMult, c);
  }

  private void updateCalculation() {
    int base = -1, target = -1, morale = -1, diff = -1, treaty = -1;

    for (int i = 0; i < btnBaseMult.length; i++) {
      if (btnBaseMult[i].isSelected()) {
        base = i;
        break;
      }
    }

    for (int i = 0; i < btnTargetMult.length; i++) {
      if (btnTargetMult[i].isSelected()) {
        target = i;
        break;
      }
    }

    for (int i = 0; i < btnMoraleMult.length; i++) {
      if (btnMoraleMult[i].isSelected()) {
        morale = i;
        break;
      }
    }

    for (int i = 0; i < btnDiffMult.length; i++) {
      if (btnDiffMult[i].isSelected()) {
        diff = i;
        break;
      }
    }

    for (int i = 0; i < btnTreatyMult.length; i++) {
      if (btnTreatyMult[i].isSelected()) {
        treaty = i;
        break;
      }
    }

    if (base >= 0 && target >= 0 && morale >= 0 && diff >= 0 && treaty >= 0) {
      double baseFac = 0.01 * (Integer) spiBaseMult[base].getValue();
      double difficultyFac = 0.01 * (Integer) spiDiffMult[diff].getValue();
      double targetFac = 0.01 * (Integer) spiTargetMult[target].getValue();
      double moraleFac = 0.01 * (Integer) spiMoraleMult[morale].getValue();
      double treatyFac = 0.01 * (Integer) spiTreatyMult[treaty].getValue();
      double product = baseFac * difficultyFac * targetFac * moraleFac * treatyFac;

      lblMult.setText("Product of selected multipliers: " + String.format("%.2f", product));
    }

    if (treaty >= 0 && base >= 0) {
      double val = 0.0001 * (Integer) spiBaseMult[base].getValue()
          * (Integer) spiTreatyMult[treaty].getValue();

      lblThreshold.setText("Intel attack delay threshold: 1 + [last area attack turn number] * "
          + String.format("%.2f", val));
    }
  }

  @Override
  public boolean hasChanges() {
    return intel.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(intel, changes);
  }

  @Override
  public void reload() throws IOException {
    intel = (Intel) stbof.getInternalFile(CStbofFiles.IntelBin, true);
    updateSelected();
  }

  @Override
  public void finalWarning() throws ParseException {
    if (SELECTED_EMPIRE < 0)
      return;

    // commit changes
    spiBaseMult[0].commitEdit();
    spiBaseMult[1].commitEdit();

    // save
    intel.setBaseEspAtkMul(SELECTED_EMPIRE, (Integer) spiBaseMult[0].getValue());
    intel.setBaseSabAtkMul(SELECTED_EMPIRE, (Integer) spiBaseMult[1].getValue());

    int index = 0;
    for (int r = 0; r < NUM_EMPIRES; r++) {
      if (r != SELECTED_EMPIRE) {
        intel.setTargetEmpAtkMul(SELECTED_EMPIRE, r, (Integer) spiTargetMult[index].getValue());
        index++;
      }
    }

    for (int i = 0; i < 8; i++) {
      intel.setMoraleLvlAtkMul(SELECTED_EMPIRE, i, (Integer) spiMoraleMult[i].getValue());
    }

    for (int i = 0; i < NUM_DIFFICULTIES; i++) {
      intel.setDiffLvlAtkMul(SELECTED_EMPIRE, i, (Integer) spiDiffMult[i].getValue());
    }

    for (int i = 0; i < 4; i++) {
      intel.setTargetTreatyAtkMul(SELECTED_EMPIRE, i, (Integer) spiTreatyMult[i].getValue());
    }
  }
}
