package ue.gui.stbof.bld;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.BldSet;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.Edifice.FilterMask;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

/**
 * This class is used to display the about window.
 */
public class BldSetGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // read
  private Stbof STBOF;
  private Edifice BUILDING;
  private Lexicon LEX;
  private RaceRst RACE;

  // edited
  private BldSet SET;

  // ui components
  private JComboBox<ID> cmbERA;
  private JComboBox<ID> cmbSYS;
  private JList<ID> lstBLD;
  private JButton btnADD; // add
  private JButton btnREP; // replace
  private JButton btnREM; // remove
  private JTextField txtNUM; // added buildins
  private JList<ID> lstALL; // all buildings
  private JComboBox<ID> cmbBUILD; // what type of building to show
  private JComboBox<ID> cmbLVL; // what level of building to show

  // data
  private int SELECTED_ERA = -1; // selected era
  private int SELECTED_SYS = -1; // selected system
  private int SELECTED_BLD = -1; // selected building - on the left list
  private int SELECTED_ADD = -1; // selected building - on the right list

  private ActionListener actSYS = new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent ae) {
      if (SELECTED_ERA < 0) {
        return;
      }

      try {
        // save previous
        finalWarning();

        // load list o buildings
        SELECTED_BLD = -1;
        SELECTED_SYS = cmbSYS.getSelectedIndex();
        if (SELECTED_SYS < 0)
          return;

        fillList();
        fillBuildList();
      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }
  };

  public BldSetGUI(Stbof st) throws IOException {
    STBOF = st;
    BUILDING = (Edifice) STBOF.getInternalFile(CStbofFiles.EdificeBst, true);
    LEX = (Lexicon) STBOF.getInternalFile(CStbofFiles.LexiconDic, true);
    RACE = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);

    // build
    cmbERA = new JComboBox<ID>();
    cmbSYS = new JComboBox<ID>();
    cmbBUILD = new JComboBox<ID>();
    cmbLVL = new JComboBox<ID>();
    lstBLD = new JList<ID>();
    lstALL = new JList<ID>();
    btnADD = new JButton(Language.getString("BldSetGUI.0")); //$NON-NLS-1$
    btnREP = new JButton(Language.getString("BldSetGUI.1")); //$NON-NLS-1$
    btnREM = new JButton(Language.getString("BldSetGUI.2")); //$NON-NLS-1$
    txtNUM = new JTextField(4);
    txtNUM.setHorizontalAlignment(JTextField.CENTER);

    // setup
    Font def = UE.SETTINGS.getDefaultFont();

    // list
    lstBLD.setFont(def);
    lstBLD.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lstBLD.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (SELECTED_ERA < 0 || SELECTED_SYS < 0) {
          return;
        }

        try {
          // save old
          finalWarning();

          // go
          if (lstBLD.getSelectedIndex() < 0) {
            btnREM.setEnabled(false);
            btnREP.setEnabled(false);
            txtNUM.setEnabled(false);
            return;
          } else {
            btnREM.setEnabled(true);
            if (lstALL.getSelectedIndex() >= 0) {
              btnREP.setEnabled(true);
            }

            txtNUM.setEnabled(true);
          }

          ID id = lstBLD.getSelectedValue();
          SELECTED_BLD = id.ID;
          if (SELECTED_BLD < 0) {
            return;
          }

          int f = SET.getNum((short) SELECTED_SYS, (short) SELECTED_BLD);
          txtNUM.setText(Integer.toString(f));
        } catch (Exception h) {
          Dialogs.displayError(h);
        }
      }
    });

    // era
    cmbERA.setFont(def);
    cmbERA.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          // save
          finalWarning();
          // load file
          loadEra();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    // sys
    cmbSYS.setFont(def);
    cmbSYS.addActionListener(actSYS);

    // add
    btnADD.setFont(def);
    btnADD.setHorizontalAlignment(SwingConstants.LEFT);
    btnADD.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SELECTED_ERA < 0 || SELECTED_SYS < 0 || SELECTED_ADD < 0)
          return;

        try {
          // save previous
          finalWarning();

          // add
          SET.setNum((short) SELECTED_SYS, (short) SELECTED_ADD, 1);
          txtNUM.setText(Integer.toString(1));

          // update
          SELECTED_BLD = SELECTED_ADD;
          SELECTED_ADD = -1;
          fillList();
          fillBuildList();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    // remove
    btnREM.setFont(def);
    btnREM.setHorizontalAlignment(SwingConstants.RIGHT);
    btnREM.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SELECTED_ERA < 0 || SELECTED_SYS < 0 || SELECTED_BLD < 0)
          return;

        try {
          // remove
          SET.setNum((short) SELECTED_SYS, (short) SELECTED_BLD, 0);
          txtNUM.setText(Integer.toString(0));

          // update
          SELECTED_ADD = SELECTED_BLD;
          SELECTED_BLD = -1;
          fillList();
          fillBuildList();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    // other
    txtNUM.setFont(def);

    // replace
    btnREP.setFont(def);
    btnREP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (SELECTED_ERA < 0 || SELECTED_SYS < 0 || SELECTED_BLD < 0 || SELECTED_ADD < 0)
          return;

        try {
          // replace
          int num = SET.getNum((short) SELECTED_SYS, (short) SELECTED_BLD);
          SET.setNum((short) SELECTED_SYS, (short) SELECTED_BLD, 0);
          SET.setNum((short) SELECTED_SYS, (short) SELECTED_ADD, num);

          // update
          int gor = SELECTED_BLD;
          SELECTED_BLD = SELECTED_ADD;
          SELECTED_ADD = gor;
          fillList();
          fillBuildList();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    // all
    lstALL.setFont(def);
    lstALL.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (SELECTED_ERA < 0 || SELECTED_SYS < 0)
          return;

        try {
          // go
          if (lstALL.getSelectedIndex() < 0) {
            btnADD.setEnabled(false);
            btnREP.setEnabled(false);
            return;
          } else {
            btnADD.setEnabled(true);
            if (lstBLD.getSelectedIndex() >= 0) {
              btnREP.setEnabled(true);
            }
          }

          ID id = lstALL.getSelectedValue();
          SELECTED_ADD = id.ID;
        } catch (Exception h) {
          Dialogs.displayError(h);
        }
      }
    });

    // combo
    cmbBUILD.setFont(def);
    cmbBUILD.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (cmbBUILD.getSelectedIndex() < 0 || cmbLVL.getSelectedIndex() < 0) {
          return;
        }

        updateLvls();
        fillBuildList();
      }
    });

    cmbLVL.setFont(def);
    cmbLVL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (cmbBUILD.getSelectedIndex() < 0 || cmbLVL.getSelectedIndex() < 0) {
          return;
        }

        fillBuildList();
      }
    });

    // place
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    JLabel lbl = new JLabel(Language.getString("BldSetGUI.3")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridx = 1;
    lbl = new JLabel(Language.getString("BldSetGUI.4")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 1;
    c.gridx = 0;
    add(cmbERA, c);
    c.gridx = 1;
    add(cmbSYS, c);
    c.gridy = 2;
    c.gridx = 0;
    c.gridheight = 6;
    JScrollPane jspBLD = new JScrollPane(lstBLD);
    jspBLD.setPreferredSize(new Dimension(160, 320));
    jspBLD.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    add(jspBLD, c);
    c.gridheight = 1;
    c.gridx = 1;
    add(btnADD, c);
    c.gridy = 3;
    add(btnREP, c);
    c.gridy = 4;
    add(btnREM, c);
    c.gridy = 5;
    lbl = new JLabel(Language.getString("BldSetGUI.5")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridy = 6;
    add(txtNUM, c);
    c.gridwidth = 2;
    c.gridx = 2;
    c.gridy = 0;
    lbl = new JLabel(Language.getString("BldSetGUI.6")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridwidth = 1;
    c.gridy = 1;
    add(cmbBUILD, c);
    c.gridx = 3;
    add(cmbLVL, c);
    c.gridwidth = 2;
    c.gridx = 2;
    c.gridy = 2;
    c.gridheight = 6;
    JScrollPane jspALL = new JScrollPane(lstALL);
    jspALL.setPreferredSize(new Dimension(160, 320));
    jspALL.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    add(jspALL, c);

    // fill
    // sys
    cmbSYS.addItem(new ID(LEX.getEntry(LexDataIdx.System.HomeWorld.Cardassia), 0));
    cmbSYS.addItem(new ID(LEX.getEntry(LexDataIdx.System.HomeWorld.Sol), 1));
    cmbSYS.addItem(new ID(LEX.getEntry(LexDataIdx.System.HomeWorld.Ferenginar), 2));
    cmbSYS.addItem(new ID(LEX.getEntry(LexDataIdx.System.HomeWorld.QoNos), 3));
    cmbSYS.addItem(new ID(LEX.getEntry(LexDataIdx.System.HomeWorld.Romulus), 4));

    // era
    cmbERA.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.StartCondition.BEGINNING), 0));
    cmbERA.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.StartCondition.EARLY), 1));
    cmbERA.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.StartCondition.DEVELOPED), 2));
    cmbERA.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.StartCondition.EXPANDED), 3));
    cmbERA.addItem(new ID(LEX.getEntry(LexDataIdx.GameSettings.StartCondition.ADVANCED), 4));

    // level
    updateLvls();

    // mask combo
    cmbBUILD.addItem(new ID(Language.getString("BldSetGUI.9"), FilterMask.All)); //$NON-NLS-1$
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      cmbBUILD.addItem(new ID(RACE.getName(emp), emp + 1));
    }
    cmbBUILD.addItem(new ID(Language.getString("BldSetGUI.10"), FilterMask.AllEmps)); //$NON-NLS-1$

    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      String msg = Language.getString("BldSetGUI.11"); //$NON-NLS-1$
      msg = msg.replace("%1", RACE.getName(emp)); //$NON-NLS-1$
      cmbBUILD.addItem(new ID(msg, emp + 7));
    }
    cmbBUILD.addItem(new ID(Language.getString("BldSetGUI.12"), FilterMask.SingleResident)); //$NON-NLS-1$
  }

  protected void loadEra() throws IOException {
    SELECTED_ERA = cmbERA.getSelectedIndex();
    int g = SELECTED_ERA + 1;
    SET = (BldSet) STBOF.getInternalFile("bldset" + g + ".bin", true); //$NON-NLS-1$ //$NON-NLS-2$

    // unset building to skip save
    int prev = SELECTED_BLD;
    SELECTED_BLD = -1;

    lstBLD.clearSelection();
    actSYS.actionPerformed(null);

    // restore selection
    lstBLD.setSelectedValue(new ID("", prev), true); //$NON-NLS-1$
  }

  @Override
  public String menuCommand() {
    return MenuCommand.StartBuildings;
  }

  protected void updateLvls() {
    ID selMask = (ID) cmbBUILD.getSelectedItem();
    int mask = selMask != null ? selMask.ID : FilterMask.All;

    ID sel = (ID) cmbLVL.getSelectedItem();
    cmbLVL.removeAllItems();

    ID all = new ID(Language.getString("BldSetGUI.7"), 0); //$NON-NLS-1$
    cmbLVL.addItem(all);
    String str = Language.getString("BldSetGUI.8"); //$NON-NLS-1$

    int[] techLvls = BUILDING.usedTechLevels(mask);
    for (int tlvl : techLvls) {
      String lvlName = str.replace("%1", Integer.toString(tlvl)); //$NON-NLS-1$
      ID id = new ID(lvlName, tlvl + 1);
      cmbLVL.addItem(id);
    }

    // restore selection
    cmbLVL.setSelectedItem(sel != null ? sel : all);
  }

  private void fillList() {
    try {
      btnREM.setEnabled(false);
      btnREP.setEnabled(false);
      int sel = SELECTED_BLD;
      SELECTED_BLD = -1;
      short[] id = SET.getBuildings((short) SELECTED_SYS);

      ID[] hm = new ID[id.length];
      for (int i = 0; i < id.length; i++) {
        Building bld = BUILDING.building(id[i]);
        hm[i] = new ID(bld.getName(), id[i]);
      }

      lstBLD.setListData(hm);
      lstBLD.setSelectedValue(new ID("", sel), true); //$NON-NLS-1$
    } catch (Exception e) {
      Dialogs.displayError(e);
    }
  }

  private void fillBuildList() {
    try {
      btnADD.setEnabled(false);
      btnREP.setEnabled(false);
      int i = SELECTED_ADD;
      SELECTED_ADD = -1;

      if (cmbBUILD.getSelectedIndex() >= 0) {
        byte msk = (byte) cmbBUILD.getSelectedIndex();
        ID[] hm = BUILDING.getShortIDs(msk, 20);
        ArrayList<ID> accept = new ArrayList<ID>();
        short[] id = SET.getBuildings((short) SELECTED_SYS);

        for (int j = 0; j < hm.length; j++) {
          ID selLvl = (ID) cmbLVL.getSelectedItem();
          if (selLvl != null && selLvl.ID != 0) {
            Building bld = BUILDING.building(hm[j].ID);
            if (bld.getTechLevel() != selLvl.ID - 1)
              continue;
          }

          boolean go = true;
          for (int k = 0; k < id.length && go; k++) {
            if (id[k] == hm[j].ID) {
              go = false;
            }
          }

          if (go)
            accept.add(hm[j]);
        }

        hm = new ID[accept.size()];
        for (int j = 0; j < accept.size(); j++) {
          hm[j] = accept.get(j);
        }

        lstALL.setListData(hm);
        lstALL.setSelectedValue(new ID("", i), true); //$NON-NLS-1$
      }
    } catch (Exception e) {
      Dialogs.displayError(e);
    }
  }

  @Override
  public boolean hasChanges() {
    for (String file : CStbofFiles.BldSets) {
      if (STBOF.isChanged(file))
        return true;
    }
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    for (String file : CStbofFiles.BldSets) {
      STBOF.checkChanged(file, changes);
    }
  }

  @Override
  public void reload() throws IOException {
    SET = null;
    SELECTED_ERA = cmbERA.getSelectedIndex();
    loadEra();
  }

  @Override
  public void finalWarning() throws InvalidArgumentsException {
    if (SELECTED_ERA < 0 || SELECTED_SYS < 0 || SELECTED_BLD < 0)
      return;

    int num = Integer.parseInt(txtNUM.getText());
    if (!(num < 0))
      SET.setNum((short) SELECTED_SYS, (short) SELECTED_BLD, num);
  }

}
