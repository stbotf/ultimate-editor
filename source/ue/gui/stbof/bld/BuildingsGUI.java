package ue.gui.stbof.bld;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SD_SpShips;
import ue.edit.exe.trek.seg.shp.SpecialShips;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.StructureGroup;
import ue.edit.res.stbof.common.CStbof.StructureType;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.AIBldReq;
import ue.edit.res.stbof.files.bin.data.AIBldReqEntry;
import ue.edit.res.stbof.files.bst.Edifbnft;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.tec.RaceTech;
import ue.edit.res.stbof.files.tec.TecField;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.edit.res.stbof.files.tec.data.TechTreeField;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.edit.res.stbof.tools.BuildingTools;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.DescTools;
import ue.gui.util.component.IconButton;
import ue.gui.util.component.ImageButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.dialog.OptionsDialog;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.data.ValueCast;

/**
 * This class is used to display the about window.
 */
public class BuildingsGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // read
  private Stbof STBOF = null;
  private TecField TECH = null;
  private TechTree TECH_TREE = null;
  private RaceRst RACE = null;
  private Lexicon LEX = null;
  private Trek TREK = null;
  private StbofFileInterface stif = null;

  // edited
  private Edifice BUILDING = null;
  private Edifbnft BONUS = null;
  private BuildingTools buildingTools;
  private AIBldReq aiBldReq = null;

  // ui components
  // LEFT PANEL
  private JList<ID> lstBUILD = new JList<ID>();
  private JComboBox<ID> cmbMASK = new JComboBox<ID>();
  private IconButton btnMoveUp = IconButton.CreateUpButton();
  private IconButton btnMoveDown = IconButton.CreateDownButton();
  private ImageButton btnADD = ImageButton.CreatePlusButton();
  private ImageButton btnREMOVE = ImageButton.CreateMinusButton();
  // MAIN PANEL
  private JPanel pnlMAIN = new JPanel();
  private JPanel pnlSWITCH = new JPanel(new GridBagLayout());
  private CardLayout layCARD = new CardLayout();
  private JPanel pnlCARD = new JPanel(layCARD);
  // HEADER
  private JLabel lblPIC = new JLabel();
  private JComboBox<String> cmbTGA = new JComboBox<String>();
  private JLabel lblNAME = new JLabel(Language.getString("BuildingsGUI.9")); //$NON-NLS-1$
  private JLabel lblPLURAL = new JLabel(Language.getString("BuildingsGUI.10")); //$NON-NLS-1$
  private JTextField txtNAME = new JTextField(20);
  private JTextField txtPLURAL = new JTextField(20);
  private JTextArea txtDESC = new JTextArea();
  private JCheckBox chkSyncAIBldReq = new JCheckBox(Language.getString("BuildingsGUI.5"));
  // FOOTER
  private JLabel lblINDEX = new JLabel();
  private JButton btnPrev = new JButton("<<<"); //$NON-NLS-1$
  private JButton btnNext = new JButton(">>>"); //$NON-NLS-1$
  private IconButton btnSettings = IconButton.CreateSettingsButton();
  private IconButton btnDescTools = IconButton.CreateDescriptionButton();
  // PAGE 1
  private JPanel pnlONE = new JPanel();
  private JLabel lblCategory = new JLabel(Language.getString("BuildingsGUI.11")); //$NON-NLS-1$
  private JLabel lblTechField = new JLabel(Language.getString("BuildingsGUI.12")); //$NON-NLS-1$
  private JLabel lblTechLevel = new JLabel(Language.getString("BuildingsGUI.13")); //$NON-NLS-1$
  private JLabel lblBuildCost = new JLabel(Language.getString("BuildingsGUI.14")); //$NON-NLS-1$
  private JLabel lblEnergyUsage = new JLabel(Language.getString("BuildingsGUI.15")); //$NON-NLS-1$
  private JLabel lblSystemReq = new JLabel(Language.getString("BuildingsGUI.16")); //$NON-NLS-1$
  private JLabel lblPoliticalReq = new JLabel(Language.getString("BuildingsGUI.17")); //$NON-NLS-1$
  private JLabel lblBuildLimit = new JLabel(Language.getString("BuildingsGUI.18")); //$NON-NLS-1$
  private JLabel lblGroup = new JLabel(Language.getString("BuildingsGUI.39")); //$NON-NLS-1$
  private JLabel lblPopMaintenance = new JLabel(Language.getString("BuildingsGUI.20")); //$NON-NLS-1$
  private JComboBox<String> cmbCATEGORY = new JComboBox<String>();
  private JComboBox<ID> cmbTECH = new JComboBox<ID>();
  private JComboBox<ID> cmbTECH_LVL = new JComboBox<ID>();
  private JTextField txtIND_COST = new JTextField(7);
  private JTextField txtEN_COST = new JTextField(7);
  private JComboBox<ID> cmbSYS_REQ = new JComboBox<ID>();
  private JComboBox<ID> cmbPOLITIC = new JComboBox<ID>();
  private JComboBox<ID> cmbFREQ = new JComboBox<ID>();
  private JComboBox<String> cmbGROUP = new JComboBox<String>();
  private JComboBox<String> cmbPOP_COST = new JComboBox<String>();
  // PAGE 2
  private JPanel pnlTWO = new JPanel();
  private JLabel lblBonusType = new JLabel(Language.getString("BuildingsGUI.21")); //$NON-NLS-1$
  private JLabel lblRaces[] = new JLabel[6];
  private JLabel lblMoraleBonus = new JLabel(Language.getString("BuildingsGUI.19")); //$NON-NLS-1$
  private JComboBox<ID> cmbBONI_TYPE = new JComboBox<ID>();
  private JButton btnEDIT_BONI = new JButton(Language.getString("BuildingsGUI.3")); //$NON-NLS-1$
  private JTextField[] txtBONI = new JTextField[6];
  private JTextField txtMORALE_BONI = new JTextField(8);
  // PAGE 3
  private JPanel pnlTHR = new JPanel();
  private JLabel lblCanBuild = new JLabel(Language.getString("BuildingsGUI.24")); //$NON-NLS-1$
  private JLabel lblResidentRace = new JLabel(Language.getString("BuildingsGUI.25")); //$NON-NLS-1$
  private JLabel lblUpgrade = new JLabel(Language.getString("BuildingsGUI.23")); //$NON-NLS-1$
  private JList<ID> lstCAN_BUILD = new JList<ID>();
  private JList<ID> lstNEEDED_RACE = new JList<ID>();
  private JComboBox<ID> cmbUPGRADE = new JComboBox<ID>();
  // PAGE 4
  private JPanel pnlFOR = new JPanel();
  private JLabel lblUnrestBonus = new JLabel(Language.getString("BuildingsGUI.26")); //$NON-NLS-1$
  private JTextField[] txtBONI_2 = new JTextField[6];
  private JCheckBox chkBUILDS_SPECIAL;
  // PAGE 5
  private JPanel pnlFIV = new JPanel();
  private JCheckBox[] chkTREK_ID_BASIC = new JCheckBox[7];
  private JCheckBox[] chkTREK_ID_MARTIAL_LAW = new JCheckBox[NUM_EMPIRES];
  private JCheckBox[] chkTREK_ID_SHIPYARD = new JCheckBox[NUM_EMPIRES];
  // PAGE 6
  private JPanel pnlSIX = new JPanel();
  private JCheckBox[] chkTREK_ID_MINOR_SP;

  // data
  private int SELECTED = -1;
  private boolean init = false;
  private boolean selChange = false;

  // keep user confirm static so it is not reset when the panel is changed
  // user confirmed build mask size auto-adjustment
  private static boolean increaseBldMask = false;
  private static boolean increaseRTBldMask = false;
  private static boolean ignoreBldMaskChanges = false;
  // user confirmed tech map size auto-adjustment
  private static boolean increaseTechLvls = false;
  private static boolean ignoreTechLvlChanges = false;

  /**
   * Costructor
   * @throws IOException
   */
  public BuildingsGUI(Stbof st, Trek trek) throws IOException {
    STBOF = st;
    TREK = trek;
    stif = st.files();

    buildingTools = stif.buildingTools();
    BUILDING = stif.edifice();
    BONUS = stif.edifbnft();
    TECH = stif.tecfield();
    LEX = stif.lexicon();
    RACE = stif.raceRst();
    TECH_TREE = stif.techTree();
    aiBldReq = stif.aiBldReq();

    /* CREATE */
    layCARD.setHgap(0);
    layCARD.setVgap(0);

    for (int i = 0; i < 6; i++) {
      txtBONI[i] = new JTextField(8);
      txtBONI_2[i] = new JTextField(8);
    }

    for (int i = 0; i < chkTREK_ID_BASIC.length; i++) {
      chkTREK_ID_BASIC[i] = new JCheckBox(BUILDING.getTrekExeIDDescription(i));
    }

    for (int i = 0; i < chkTREK_ID_MARTIAL_LAW.length; i++) {
      try {
        chkTREK_ID_MARTIAL_LAW[i] = new JCheckBox(RACE.getName(i));
        chkTREK_ID_SHIPYARD[i] = new JCheckBox(RACE.getName(i));
      } catch (Exception ex) {
        chkTREK_ID_MARTIAL_LAW[i] = new JCheckBox("Race " + i);
        chkTREK_ID_SHIPYARD[i] = new JCheckBox("Race " + i);
      }
    }

    chkTREK_ID_MINOR_SP = new JCheckBox[RACE.getNumberOfEntries() - NUM_EMPIRES];
    for (int i = 0; i < chkTREK_ID_MINOR_SP.length; i++) {
      try {
        chkTREK_ID_MINOR_SP[i] = new JCheckBox(RACE.getName(i + NUM_EMPIRES));
      } catch (Exception ex) {
        chkTREK_ID_MINOR_SP[i] = new JCheckBox("Race " + (i - NUM_EMPIRES));
      }
    }

    chkBUILDS_SPECIAL = new JCheckBox(Language.getString("BuildingsGUI.38")); //$NON-NLS-1$

    /* SETUP */
    setupComponents();

    // move buttons
    btnMoveUp.addActionListener(new CBActionListener(evt -> btnMoveUpClick()));
    btnMoveDown.addActionListener(new CBActionListener(evt -> btnMoveDownClick()));

    // technology combos
    // required tech combo
    cmbTECH.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || cmbTECH.getSelectedIndex() < 0)
        return;

      // fill
      TechTreeField field = TECH_TREE.field(cmbTECH.getSelectedIndex());
      int max = field.getMaxTechLevel();

      // get selected technology level
      ID sel = (ID) cmbTECH_LVL.getSelectedItem();

      // update tech level selection defaults
      init = false;
      cmbTECH_LVL.removeAllItems();

      for (int i = 0; i <= max; i++) {
        cmbTECH_LVL.addItem(new ID(Integer.toString(i), i));
      }

      // restore tech level selection
      cmbTECH_LVL.setSelectedItem(sel);
      init = true;

      Building bld = BUILDING.building(SELECTED);
      bld.setTechReq((byte) cmbTECH.getSelectedIndex());
    }));

    // tech level combo
    cmbTECH_LVL.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {
      // accept digits only
      @Override
      public void keyTyped(KeyEvent e) {
        char c = e.getKeyChar();
        if (c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_DELETE && !Character.isDigit(c)) {
          e.consume();
        }
      }
    });

    cmbTECH_LVL.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      // skip edited events cause they fire a 2nd comboBoxChanged event
      if ("comboBoxEdited".equals(evt.getActionCommand()))
        return;

      ID sel = (ID) cmbTECH_LVL.getSelectedItem();
      if (sel == null)
        return;

      // check whether to increase the tech map size for the selected tech level
      if (!increaseTechLvls && !ignoreTechLvlChanges) {
        int reqTechLvls = 1 + sel.ID;
        int techLvls = BUILDING.getTechLvls();

        if (reqTechLvls > techLvls)
          if (!showInsufficientTechMapDialog(techLvls, reqTechLvls))
            return;
      }

      Building bld = BUILDING.building(SELECTED);
      bld.setTechLvl((byte) sel.ID);
    }));

    // population cost combo
    cmbPOP_COST.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || cmbPOP_COST.getSelectedIndex() < 0)
        return;

      Building bld = BUILDING.building(SELECTED);
      if (cmbPOP_COST.getSelectedIndex() == 0) {
        bld.setPopCost((short) 0);
        // with no assignable population, it must be some special building
        if (cmbGROUP.getSelectedIndex() <= StructureGroup.MainResearch) {
          cmbGROUP.setSelectedIndex(StructureGroup.Other);
        }
      } else {
        bld.setPopCost((short) 10);
      }
    }));

    // upgrade req combo
    cmbUPGRADE.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbUPGRADE.getSelectedIndex() >= 0) {
        Building bld = BUILDING.building(SELECTED);
        bld.setUpgradeFor((short) ((ID)cmbUPGRADE.getSelectedItem()).ID);
      }
    }));

    // group combo
    cmbGROUP.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbGROUP.getSelectedIndex() >= 0) {
        Building bld = BUILDING.building(SELECTED);
        bld.setGroup(cmbGROUP.getSelectedIndex());
        // for main group structures, there must be some assignable population cost
        if (cmbGROUP.getSelectedIndex() <= StructureGroup.MainResearch) {
          cmbPOP_COST.setSelectedIndex(1);
        }
      }
    }));

    // system req combo
    cmbSYS_REQ.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbSYS_REQ.getSelectedIndex() >= 0) {
        Building bld = BUILDING.building(SELECTED);
        bld.setSysReq((byte) cmbSYS_REQ.getSelectedIndex());
      }
    }));

    // category combo
    cmbCATEGORY.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbCATEGORY.getSelectedIndex() >= 0) {
        Building bld = BUILDING.building(SELECTED);
        bld.setCategory((byte) cmbCATEGORY.getSelectedIndex());
      }
    }));

    // politic combo
    cmbPOLITIC.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbPOLITIC.getSelectedIndex() >= 0) {
        Building bld = BUILDING.building(SELECTED);
        bld.setPoliticReq((byte) ((ID) cmbPOLITIC.getSelectedItem()).hashCode());
      }
    }));

    // frequency combo
    cmbFREQ.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbFREQ.getSelectedIndex() >= 0) {
        Building bld = BUILDING.building(SELECTED);
        bld.setFreq((byte) cmbFREQ.getSelectedIndex());
      }
    }));

    // boni type combo
    cmbBONI_TYPE.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbBONI_TYPE.getSelectedIndex() >= 0) {
        Building bld = BUILDING.building(SELECTED);
        bld.setBonusType((byte) cmbBONI_TYPE.getSelectedIndex());
      }
    }));

    // mask combo
    cmbMASK.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0 && cmbMASK.getSelectedIndex() >= 0) {
        BuildingsGUI.this.fillList();
        lstBUILD.setSelectedIndex(0);
        lstBUILD.ensureIndexIsVisible(0);
        onSelectionChange(null);
      }
    }));

    // list of buildings
    lstBUILD.addListSelectionListener(new CBListSelectionListener(x -> onSelectionChange(x)));

    // add button
    btnADD.addActionListener(new CBActionListener(evt -> {
      final int sel_id = lstBUILD.getSelectedIndex();

      try {
        ID[] possibleValues = new ID[BUILDING.getNumberOfEntries()];

        for (int x = 0; x < BUILDING.getNumberOfEntries(); x++) {
          Building bld = BUILDING.building(x);
          possibleValues[x] = new ID(bld.getName(), x);
        }

        ID selectedValue = (ID) JOptionPane
            .showInputDialog(UE.WINDOW, Language.getString("BuildingsGUI.4"), //$NON-NLS-1$
                UE.APP_NAME, JOptionPane.INFORMATION_MESSAGE, null, possibleValues,
                possibleValues[0]);

        // abort when no building to copy is selected
        if (selectedValue == null)
          return;

        // check for empire ignore mask size auto-adjustment
        if (!increaseBldMask && !ignoreBldMaskChanges) {
          RaceTech rtec = stif.raceTech();
          int numBuildings = 1 + BUILDING.getNumberOfEntries();
          int maxBuildings = Integer.min(BUILDING.getMaxBuildings(), rtec.getMaxBuidings());

          if (numBuildings > maxBuildings)
            if (!showInsufficientBldMaskDialog(maxBuildings))
              return;
        }

        // add new building if not cancelled
        final int copyId = selectedValue.ID;
        buildingTools.addBuilding(sel_id, copyId, increaseBldMask, increaseRTBldMask);
      } finally {
        // enforce refresh
        SELECTED = -1;
        lstBUILD.clearSelection();

        fillList();
        lstBUILD.setSelectedIndex(sel_id);
        lstBUILD.ensureIndexIsVisible(sel_id);
      }
    }));

    // remove button
    btnREMOVE.addActionListener(new CBActionListener(evt -> {
      if (SELECTED < 0 && BUILDING.getNumberOfEntries() > 1)
        return;

      int hm = SELECTED;

      try {
        buildingTools.removeBuilding(SELECTED);

        // update selection
        if (BUILDING.getNumberOfEntries() <= hm)
          hm = BUILDING.getNumberOfEntries() - 1;
      } finally {
        // enforce refresh
        SELECTED = -1;
        lstBUILD.clearSelection();

        fillList();

        lstBUILD.setSelectedIndex(hm);
        lstBUILD.ensureIndexIsVisible(hm);
      }
    }));

    btnEDIT_BONI.addActionListener(new CBActionListener(evt -> {
      if (cmbBONI_TYPE.getSelectedIndex() >= 0) {
        String n = JOptionPane.showInputDialog(Language.getString("BuildingsGUI.8"),
          BONUS.getBoniDesc(cmbBONI_TYPE.getSelectedIndex())); // $NON-NLS-1$

        if (n != null) {
          int selectedValue = SELECTED;
          BONUS.setBoniDesc(cmbBONI_TYPE.getSelectedIndex(), n);
          BuildingsGUI.this.fillList();
          SELECTED = selectedValue;
        }
      }
    }));

    Collection<String> fileNames = STBOF.getFileNames(new java.io.FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        String str = name.toLowerCase();
        return str.endsWith("_b.tga"); //$NON-NLS-1$
      }
    });

    for (String fileName : fileNames) {
      cmbTGA.addItem(fileName);
    }

    cmbTGA.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      String str = (String) cmbTGA.getSelectedItem();
      Building bld = BUILDING.building(SELECTED);
      bld.setPicture(str.substring(0, str.length() - 6));

      try {
        TargaImage tar = STBOF.getTargaImage(bld.getImagePrefix() + ".tga", LoadFlags.UNCACHED); //$NON-NLS-1$

        Dimension dim = tar.getImageSize();
        if (dim.height > 100) {
          lblPIC.setIcon(new ImageIcon(tar.getThumbnail(100, false, AlphaMode.Opaque)));
          return;
        }
        if (dim.width > 120) {
          lblPIC.setIcon(new ImageIcon(tar.getThumbnail(120, false, AlphaMode.Opaque)));
          return;
        }
        lblPIC.setIcon(new ImageIcon(tar.getImage(AlphaMode.Opaque)));
      } catch (Exception fnf) {
        fnf.printStackTrace();
        lblPIC.setIcon(null);
      }
    }));

    // can build list
    lstCAN_BUILD.addListSelectionListener(new CBListSelectionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      int[] tin = lstCAN_BUILD.getSelectedIndices();
      Building bld = BUILDING.building(SELECTED);

      byte mask = 0; // mask is reverted
      for (int i = 0; (i < tin.length) && (i < NUM_EMPIRES); i++) {
        mask += (byte) (1 << tin[i]);
      }

      byte fin = (byte) (mask ^ ((byte) 0x1F));
      bld.setExcludeEmpire(fin);
    }));

    // needed resident races list
    lstNEEDED_RACE.addListSelectionListener(new CBListSelectionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      int[] tin = lstNEEDED_RACE.getSelectedIndices();
      Building bld = BUILDING.building(SELECTED);

      long mask = 0; // normal mask
      for (int element : tin) {
        mask += 1L << element;
      }

      bld.setReqRace(mask);
    }));

    chkBUILDS_SPECIAL.addActionListener(new CBActionListener(evt -> {
      // builds special ships?
      if (!init || TREK == null || SELECTED < 0 || SELECTED > 255)
        return;

      ByteValueSegment bjmp = (ByteValueSegment) TREK.getSegment(SD_SpShips.SpShpStrcJmp, true);
      SpecialShips bub = (SpecialShips) TREK.getInternalFile(CTrekSegments.SpecialShips, true);

      switch (bjmp.byteValue()) {
        case JmpShort.JLE: { // from 0 - sel
          if (SELECTED == bub.getSPBuilding()) {
            if (SELECTED == 1) {
              // only one left
              bjmp.setValue(JmpShort.JZ);
            }
            bub.setSPBuilding(SELECTED - 1);
          } else {
            bub.setSPBuilding(SELECTED);
          }
          break;
        }
        case JmpShort.JZ: { // single
          if ((bub.getSPBuilding() == 0) && (SELECTED == 1)) {
            bjmp.setValue(JmpShort.JLE);
          }

          bub.setSPBuilding(SELECTED);
          chkBUILDS_SPECIAL.setEnabled(false);
          break;
        }
        default:
          throw new IOException("unexpected jump operation");
      }
    }));

    for (int i = 0; i < chkTREK_ID_BASIC.length; i++) {
      final int idx = i;
      chkTREK_ID_BASIC[idx].addActionListener(new CBActionListener(evt -> {
        if (init && SELECTED >= 0) {
          int id = (chkTREK_ID_BASIC[idx].isSelected()) ? SELECTED : -1;
          BUILDING.setTrekExeID(idx, id);
        }
      }));
    }

    for (int i = 0; i < chkTREK_ID_MARTIAL_LAW.length; i++) {
      final int idx = i;
      chkTREK_ID_MARTIAL_LAW[idx].addActionListener(new CBActionListener(evt -> {
        if (init && SELECTED >= 0) {
          int id = (chkTREK_ID_MARTIAL_LAW[idx].isSelected()) ? SELECTED : -1;
          BUILDING.setTrekExeID(StructureType.MartialLaw, idx, id);
        }
      }));
    }

    for (int i = 0; i < chkTREK_ID_SHIPYARD.length; i++) {
      final int idx = i;
      chkTREK_ID_SHIPYARD[idx].addActionListener(new CBActionListener(evt -> {
        if (init && SELECTED >= 0) {
          int id = (chkTREK_ID_SHIPYARD[idx].isSelected()) ? SELECTED : -1;
          BUILDING.setTrekExeID(StructureType.Shipyard, idx, id);
        }
      }));
    }

    for (int i = 0; i < chkTREK_ID_MINOR_SP.length; i++) {
      final int idx = i;
      chkTREK_ID_MINOR_SP[idx].addActionListener(new CBActionListener(evt -> {
        if (init && SELECTED >= 0) {
          int id = (chkTREK_ID_MINOR_SP[idx].isSelected()) ? SELECTED : -1;
          BUILDING.setTrekExeID(StructureType.MinorShipBuild, idx + NUM_EMPIRES, id);
        }
      }));
    }

    /* PLACE */
    setLayout(new GridBagLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    // Buttons panel
    JPanel pnlButtons = new JPanel();
    pnlButtons.setLayout(new BoxLayout(pnlButtons, BoxLayout.X_AXIS));
    {
      pnlButtons.add(btnMoveUp);
      pnlButtons.add(btnMoveDown);
      pnlButtons.add(Box.createHorizontalGlue());
      pnlButtons.add(btnADD);
      pnlButtons.add(btnREMOVE);
    }

    // LEFT PANEL
    JPanel pnlLEFT = new JPanel(new GridBagLayout());
    JScrollPane JSP = new JScrollPane(lstBUILD);
    JSP.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    {
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1;

      pnlLEFT.add(cmbMASK, c);
      c.insets.top = 5;
      c.weighty = 1;
      c.gridy++;
      pnlLEFT.add(JSP, c);
      c.insets.top = 0;
      c.weighty = 0;
      c.gridy++;
      pnlLEFT.add(pnlButtons, c);
    }

    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;

    // MAIN PANEL
    {
      pnlMAIN.setLayout(new GridBagLayout());
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 3;
      c.insets.bottom = 0;

      pnlMAIN.add(lblPIC, c);
      c.insets.top = 2;
      c.gridy += 3;
      c.gridheight = 1;
      pnlMAIN.add(cmbTGA, c);
      c.insets.bottom = 5;
      c.gridy++;
      pnlMAIN.add(chkSyncAIBldReq, c);
      c.insets.top = 5;
      c.gridx++;
      c.gridy = 0;
      pnlMAIN.add(lblNAME, c);
      c.gridx++;
      pnlMAIN.add(txtNAME, c);
      c.gridx = 1;
      c.gridy++;
      pnlMAIN.add(lblPLURAL, c);
      c.gridx++;
      pnlMAIN.add(txtPLURAL, c);
      c.gridx = 1;
      c.gridy++;
      c.gridwidth = 2;
      c.gridheight = 4;
      c.weightx = 1;
      c.weighty = 1;
      JScrollPane scrDESC = new JScrollPane(txtDESC);
      scrDESC.setPreferredSize(new Dimension(200, 80));
      pnlMAIN.add(scrDESC, c);
    }

    c.weightx = 0;
    c.weighty = 0;

    // ONE PANEL
    {
      pnlONE.setLayout(new GridBagLayout());
      c.gridx = 0;
      c.gridy = 0;
      c.gridwidth = 1;
      c.gridheight = 1;
      pnlONE.add(lblCategory, c);
      c.gridy++;
      pnlONE.add(lblTechField, c);
      c.gridy++;
      pnlONE.add(lblTechLevel, c);
      c.gridy++;
      pnlONE.add(lblBuildCost, c);
      c.gridy++;
      pnlONE.add(lblEnergyUsage, c);
      c.gridy = 0;
      c.gridx = 1;
      pnlONE.add(cmbCATEGORY, c);
      c.gridy = 2;
      pnlONE.add(cmbTECH_LVL, c);
      c.gridy = 1;
      pnlONE.add(cmbTECH, c);
      c.gridy = 3;
      pnlONE.add(txtIND_COST, c);
      c.gridy = 4;
      pnlONE.add(txtEN_COST, c);
      c.gridx = 2;
      c.gridy = 0;
      pnlONE.add(lblSystemReq, c);
      c.gridy = 1;
      pnlONE.add(lblPoliticalReq, c);
      c.gridy = 2;
      pnlONE.add(lblBuildLimit, c);
      c.gridy = 3;
      pnlONE.add(lblGroup, c);
      c.gridy = 4;
      pnlONE.add(lblPopMaintenance, c);
      c.gridx = 3;
      c.gridy = 0;
      pnlONE.add(cmbSYS_REQ, c);
      c.gridy = 1;
      pnlONE.add(cmbPOLITIC, c);
      c.gridy = 2;
      pnlONE.add(cmbFREQ, c);
      c.gridy = 3;
      pnlONE.add(cmbGROUP, c);
      c.gridy = 4;
      pnlONE.add(cmbPOP_COST, c);
    }

    // TWO PANEL
    {
      pnlTWO.setLayout(new GridBagLayout());
      c.fill = GridBagConstraints.NONE;
      c.gridwidth = 1;
      c.gridheight = 1;
      c.gridx = 0;
      c.gridy = 0;
      pnlTWO.add(lblBonusType, c);
      c.gridx = 1;
      c.gridwidth = 2;
      pnlTWO.add(cmbBONI_TYPE, c);
      c.gridwidth = 1;

      for (int i = 0; i < lblRaces.length; i++) {
        if (i < 3) {
          c.gridx = 0;
          c.gridy = i + 1;
        } else {
          c.gridx = 2;
          c.gridy = i - 2;
        }

        pnlTWO.add(lblRaces[i], c);
        c.gridx++;
        pnlTWO.add(txtBONI[i], c);
      }

      c.gridx = 3;
      c.gridy = 0;
      c.fill = GridBagConstraints.BOTH;
      pnlTWO.add(btnEDIT_BONI, c);
      c.gridx = 0;
      c.gridy = 4;
      c.gridwidth = 1;
      pnlTWO.add(lblMoraleBonus, c);
      c.gridx = 1;
      c.anchor = GridBagConstraints.WEST;
      pnlTWO.add(txtMORALE_BONI, c);
    }

    // THREE PANEL
    {
      pnlTHR.setLayout(new GridBagLayout());
      c.fill = GridBagConstraints.BOTH;
      c.anchor = GridBagConstraints.CENTER;
      c.gridwidth = 1;
      c.gridheight = 1;
      c.gridx = 0;
      c.gridy = 0;
      pnlTHR.add(lblCanBuild, c);
      c.gridy = 1;
      JScrollPane scrCAN_BUILD = new JScrollPane(lstCAN_BUILD);
      scrCAN_BUILD.setPreferredSize(new Dimension(200, 100));
      scrCAN_BUILD.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      pnlTHR.add(scrCAN_BUILD, c);
      c.gridx = 1;
      c.gridy = 0;
      pnlTHR.add(lblResidentRace, c);
      c.gridy = 1;
      JScrollPane scrNEEDED = new JScrollPane(lstNEEDED_RACE);
      scrNEEDED.setPreferredSize(new Dimension(200, 100));
      scrNEEDED.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
      pnlTHR.add(scrNEEDED, c);
      c.gridx = 0;
      c.gridy = 2;
      pnlTHR.add(lblUpgrade, c);
      c.gridx = 1;
      pnlTHR.add(cmbUPGRADE, c);
    }

    // FOUR PANEL
    {
      pnlFOR.setLayout(new GridBagLayout());
      c.fill = GridBagConstraints.NONE;
      c.gridwidth = 4;
      c.gridheight = 1;
      c.gridx = 0;
      c.gridy = 0;
      pnlFOR.add(lblUnrestBonus, c);
      c.gridwidth = 1;

      for (int i = 0; i < lblRaces.length; i++) {
        if (i < 3) {
          c.gridx = 0;
          c.gridy = i + 1;
        } else {
          c.gridx = 2;
          c.gridy = i - 2;
        }

        pnlFOR.add(lblRaces[i], c);
        c.gridx++;
        pnlFOR.add(txtBONI_2[i], c);
      }

      c.gridy++;
      c.gridx = 0;
      c.gridwidth = 4;
      pnlFOR.add(chkBUILDS_SPECIAL, c);
    }

    // PANEL FIVE
    {
      pnlFIV.setLayout(new GridBagLayout());
      c.fill = GridBagConstraints.BOTH;
      c.anchor = GridBagConstraints.CENTER;
      c.insets = new Insets(0, 0, 0, 0);
      c.gridwidth = 1;
      c.gridheight = 1;
      c.gridx = 0;

      JPanel pnl = new JPanel(new GridBagLayout());

      for (int i = 0; i < chkTREK_ID_BASIC.length; i++) {
        c.gridy = i;
        pnl.add(chkTREK_ID_BASIC[i], c);
      }
      c.gridy = 0;
      pnlFIV.add(pnl, c);

      c.gridx++;

      pnl = new JPanel(new GridBagLayout());
      pnl.setBorder(BorderFactory.createTitledBorder("Martial Law"));

      for (int i = 0; i < chkTREK_ID_MARTIAL_LAW.length; i++) {
        c.gridy = i;
        pnl.add(chkTREK_ID_MARTIAL_LAW[i], c);
      }
      c.gridy = 0;
      pnlFIV.add(pnl, c);

      pnl = new JPanel(new GridBagLayout());
      pnl.setBorder(BorderFactory.createTitledBorder("Shipyard"));

      c.gridx++;
      for (int i = 0; i < chkTREK_ID_SHIPYARD.length; i++) {
        c.gridy = i;
        pnl.add(chkTREK_ID_SHIPYARD[i], c);
      }
      c.gridy = 0;
      pnlFIV.add(pnl, c);
    }

    // PANEL SIX
    {
      pnlSIX.setLayout(new GridBagLayout());
      c.fill = GridBagConstraints.BOTH;
      c.anchor = GridBagConstraints.CENTER;
      c.insets = new Insets(0, 0, 0, 0);
      c.gridwidth = 1;
      c.gridheight = 1;
      c.gridx = 0;

      JPanel pnl = new JPanel(new GridBagLayout());
      pnl.setBorder(BorderFactory.createTitledBorder("Minor race special structure"));

      for (int i = 0; i < chkTREK_ID_MINOR_SP.length; i++) {
        c.gridy = i % 6;
        c.gridx = i / 6;
        pnl.add(chkTREK_ID_MINOR_SP[i], c);
      }

      pnlSIX.add(pnl, c);
    }

    // CARD PANEL
    {
      pnlCARD.add(pnlONE, "1"); //$NON-NLS-1$
      pnlCARD.add(pnlTWO, "2"); //$NON-NLS-1$
      pnlCARD.add(pnlTHR, "3"); //$NON-NLS-1$
      pnlCARD.add(pnlFOR, "4"); //$NON-NLS-1$
      pnlCARD.add(pnlFIV, "5"); //$NON-NLS-1$
      pnlCARD.add(pnlSIX, "6"); //$NON-NLS-1$
    }

    // SWITCH PANEL
    {
      // left spacing
      c.fill = GridBagConstraints.NONE;
      c.insets = new Insets(0, 5, 0, 5);
      c.anchor = GridBagConstraints.WEST;
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1.0;
      // add strut place holder to equal weightx distribution
      pnlSWITCH.add(btnSettings, c);
      pnlSWITCH.add(Box.createHorizontalStrut(40), c);

      // navigation
      c.gridx++;
      c.weightx = 0;
      pnlSWITCH.add(btnPrev, c);
      c.anchor = GridBagConstraints.CENTER;
      c.gridx++;
      pnlSWITCH.add(lblINDEX, c);
      pnlSWITCH.add(Box.createHorizontalStrut(80), c);
      c.anchor = GridBagConstraints.EAST;
      c.gridx++;
      pnlSWITCH.add(btnNext, c);

      // right spacing & tools
      c.gridx++;
      c.weightx = 1.0;
      // add strut place holder to equal weightx distribution
      pnlSWITCH.add(Box.createHorizontalStrut(40), c);
      pnlSWITCH.add(btnDescTools, c);

      // event listeners
      btnPrev.addActionListener(new CBActionListener(evt -> layCARD.previous(pnlCARD)));
      btnNext.addActionListener(new CBActionListener(evt -> layCARD.next(pnlCARD)));
      btnSettings.addActionListener(new CBActionListener(evt -> showSettingsDialog()));

      btnDescTools.addActionListener(new CBActionListener(evt -> {
        if (DescTools.showToolsDialog(BUILDING)) {
          String desc = SELECTED >= 0 ? BUILDING.getDesc(SELECTED) : null;
          txtDESC.setText(desc);
        }
      }));
    }

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 3;
    c.weightx = 0;
    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.CENTER;
    c.insets = new Insets(0, 0, 0, 0);
    // match left panel width with the ship gui
    // note that the minimum width is also determined by the filter combo box
    add(Box.createHorizontalStrut(165), c);
    add(pnlLEFT, c);
    c.gridx = 1;
    c.gridheight = 1;
    c.weightx = 1.0;
    c.weighty = 1.0;
    c.fill = GridBagConstraints.BOTH;
    c.insets.left = 5;
    add(pnlMAIN, c);
    c.gridy = 1;
    c.insets.top = 5;
    add(pnlCARD, c);
    c.gridy = 2;
    c.weighty = 0;
    add(pnlSWITCH, c);

    /* INIT */
    // mask
    cmbMASK.addItem(new ID(Language.getString("BuildingsGUI.27"), 0)); //$NON-NLS-1$
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      cmbMASK.addItem(new ID(RACE.getName(emp), emp + 1));
    }

    cmbMASK.addItem(new ID(Language.getString("BuildingsGUI.28"), 6)); //$NON-NLS-1$
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      String msg = Language.getString("BuildingsGUI.29"); //$NON-NLS-1$
      msg = msg.replace("%1", RACE.getName(emp)); //$NON-NLS-1$
      cmbMASK.addItem(new ID(msg, emp + 7));
    }
    cmbMASK.addItem(new ID(Language.getString("BuildingsGUI.30"), 12)); //$NON-NLS-1$

    // tech
    String[] str = TECH.getFields();
    int ix = 0;
    for (ix = 0; ix < str.length - 1; ix++) {
      cmbTECH.addItem(new ID(str[ix], ix));
    }

    cmbTECH.addItem(new ID(Language.getString("BuildingsGUI.27"), ix)); //$NON-NLS-1$

    // get max technology tree tech level to add default selections
    TechTreeField field = TECH_TREE.field(0);
    int max = field.getMaxTechLevel();
    for (int i = 0; i <= max; i++) {
      cmbTECH_LVL.addItem(new ID(Integer.toString(i), i));
    }

    // system req
    int i = 0;
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.ArcticPlanet), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.BarrenPlanet), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.DesertPlanet), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.PlanetTypes.GasGiant), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.JunglePlanet), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.OceanicPlanet), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.TerranPlanet), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.VolcanicPlanet), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.Shared.None), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.PlanetTypes.Asteroid), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.AsteroidBeltDilithium), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.Dilithium), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.Galaxy.StellarObjects.Wormhole), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.Galaxy.StellarObjects.RadioPulsar), i++));
    cmbSYS_REQ.addItem(new ID(LEX.getEntry(LexDataIdx.Galaxy.StellarObjects.XRayPulsar), i++));

    // category
    cmbCATEGORY.addItem(Language.getString("BuildingsGUI.32")); //$NON-NLS-1$
    cmbCATEGORY.addItem(Language.getString("BuildingsGUI.33")); //$NON-NLS-1$
    cmbCATEGORY.addItem(Language.getString("BuildingsGUI.34")); //$NON-NLS-1$
    cmbCATEGORY.addItem(Language.getString("BuildingsGUI.35")); //$NON-NLS-1$
    cmbCATEGORY.addItem(Language.getString("BuildingsGUI.36")); //$NON-NLS-1$

    // politic
    i = 0;
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.HomeSystem), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.NativeMemberSystem), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.NonNativeMemberSystem), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.SubjugatedSystem), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.AffiliatedSystem), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.IndependentMinorSystem), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.ConqueredHomeSystem), i++));
    i++;
    i++;
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.Shared.None), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.RebelSystem), i++));
    cmbPOLITIC.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.EmptySystem), i++));

    // frequency
    i = 0;
    cmbFREQ.addItem(new ID(LEX.getEntry(LexDataIdx.Shared.None), i++));
    cmbFREQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.OnePerSystem), i++));
    cmbFREQ.addItem(new ID(LEX.getEntry(LexDataIdx.System.Restrictions.OnePerEmpire), i++));

    // races
    ID[] istr = new ID[NUM_EMPIRES];
    ID[] rstr = new ID[RACE.getNumberOfEntries()];
    for (int n = 0; n < RACE.getNumberOfEntries(); n++) {
      String raceName = RACE.getName(n);
      rstr[n] = new ID(raceName, n);
      if (n < NUM_EMPIRES)
        istr[n] = new ID(raceName, n);
    }

    lstCAN_BUILD.setListData(istr);
    lstNEEDED_RACE.setListData(rstr);
    // pop cost
    cmbPOP_COST.addItem("0"); //$NON-NLS-1$
    cmbPOP_COST.addItem("10"); //$NON-NLS-1$
    // group
    cmbGROUP.addItem(Language.getString("BuildingsGUI.40")); //$NON-NLS-1$
    cmbGROUP.addItem(Language.getString("BuildingsGUI.41")); //$NON-NLS-1$
    cmbGROUP.addItem(Language.getString("BuildingsGUI.42")); //$NON-NLS-1$
    cmbGROUP.addItem(Language.getString("BuildingsGUI.43")); //$NON-NLS-1$
    cmbGROUP.addItem(Language.getString("BuildingsGUI.44")); //$NON-NLS-1$
    cmbGROUP.addItem(Language.getString("BuildingsGUI.45")); //$NON-NLS-1$
    cmbGROUP.addItem(Language.getString("BuildingsGUI.46")); //$NON-NLS-1$

    fillList();

    layCARD.first(pnlCARD);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.BuildingStats;
  }

  @Override
  public boolean hasChanges() {
    if (BUILDING.madeChanges() || BONUS.madeChanges() || aiBldReq.madeChanges()
      || STBOF.isChanged(CStbofFiles.EdifDescBst)
      || STBOF.isChanged(CStbofFiles.RaceTechTec))
      return true;

    for (String file : CStbofFiles.BldSets) {
      if (STBOF.isChanged(file))
        return true;
    }

    if (TREK != null) {
      if (TREK.isChanged(SD_SpShips.SpStrcJmpName)
        || TREK.isChanged(CTrekSegments.SpecialShips))
        return true;

      for (String file : Edifice.SegmentsEdited)
        if (TREK.isChanged(file))
          return true;
    }
    return false;
  }

  @Override
  public void reload() throws IOException {
    // reset settings on reload
    increaseTechLvls = false;
    ignoreTechLvlChanges = false;
    increaseBldMask = false;
    increaseRTBldMask = false;
    ignoreBldMaskChanges = false;

    // refresh all data
    BONUS = stif.edifbnft();
    BUILDING = stif.edifice();
    fillList();
  }

  @Override
  public void finalWarning() throws IOException, InvalidArgumentsException {
    if (increaseBldMask)
      BUILDING.adjustBldMaskLongs();
    if (increaseRTBldMask)
      stif.raceTech().adjustBldMaskLongs();
    if (increaseTechLvls)
      BUILDING.adjustTechLevels();

    try {
      // apply changes
      applySelected();
    }
    catch (KeyNotFoundException e) {
      Dialogs.displayError(e);
    }
    BUILDING.flushDescriptions();

    // update all pending secondary segments
    // don't wait for edifice to be saved to allow inspect changes in segments view
    BUILDING.updateExe();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    // list building stat & bonus changes
    STBOF.checkChanged(BUILDING, changes);
    STBOF.checkChanged(BONUS, changes);

    // list description changes
    STBOF.checkChanged(CStbofFiles.EdifDescBst, changes);

    // list ai building requirement changes
    STBOF.checkChanged(aiBldReq, changes);

    // list additional changes of the building tools
    // that apply on index changes like remove of a building
    STBOF.checkChanged(CStbofFiles.RaceTechTec, changes);
    for (String file : CStbofFiles.BldSets) {
      STBOF.checkChanged(file, changes);
    }

    // list additional trek changes
    if (TREK != null) {
      TREK.checkChanged(SD_SpShips.SpStrcJmpName, changes);
      TREK.checkChanged(CTrekSegments.SpecialShips, changes);
      for (String file : Edifice.SegmentsEdited)
        TREK.checkChanged(file, changes);
    }
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();

    // LEFT PANEL
    cmbMASK.setFont(def);
    lstBUILD.setFont(def);
    lstBUILD.setFixedCellWidth(50);
    lstBUILD.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    btnREMOVE.setEnabled(false);

    // MAIN PANEL
    pnlMAIN.setBorder(new LineBorder(Color.BLACK));
    pnlCARD.setBorder(new LineBorder(Color.BLACK));

    // HEADER
    lblNAME.setFont(def);
    lblPLURAL.setFont(def);
    txtNAME.setFont(def);
    txtPLURAL.setFont(def);
    txtDESC.setLineWrap(true);
    txtDESC.setWrapStyleWord(true);
    txtDESC.setFont(def);
    cmbTGA.setFont(def);
    lblPIC.setPreferredSize(new Dimension(120, 100));
    chkSyncAIBldReq.setFont(def);
    chkSyncAIBldReq.setToolTipText(Language.getString("BuildingsGUI.6"));

    // FOOTER
    lblINDEX.setFont(def);
    btnPrev.setFont(def);
    btnNext.setFont(def);

    // PAGE 1
    lblCategory.setFont(def);
    lblTechField.setFont(def);
    lblTechLevel.setFont(def);
    lblBuildCost.setFont(def);
    lblEnergyUsage.setFont(def);
    cmbCATEGORY.setFont(def);
    cmbTECH.setFont(def);
    cmbTECH_LVL.setFont(def);
    txtIND_COST.setFont(def);
    txtEN_COST.setFont(def);

    lblSystemReq.setFont(def);
    lblPoliticalReq.setFont(def);
    lblBuildLimit.setFont(def);
    lblGroup.setFont(def);
    lblPopMaintenance.setFont(def);
    cmbSYS_REQ.setFont(def);
    cmbPOLITIC.setFont(def);
    cmbFREQ.setFont(def);
    cmbGROUP.setFont(def);
    cmbPOP_COST.setFont(def);

    // PAGE 2
    lblBonusType.setFont(def);
    cmbBONI_TYPE.setFont(def);
    btnEDIT_BONI.setFont(def);

    // race labels
    for (int i = 0; i < lblRaces.length; ++i) {
      if (i < NUM_EMPIRES) {
        lblRaces[i] = new JLabel(RACE.getName(i) + ":"); //$NON-NLS-1$
      } else {
        lblRaces[i] = new JLabel(Language.getString("BuildingsGUI.22")); //$NON-NLS-1$
      }
      lblRaces[i].setFont(def);
    }

    lblMoraleBonus.setFont(def);
    txtMORALE_BONI.setFont(def);
    Arrays.stream(txtBONI).forEach(x -> x.setFont(def));

    // PAGE 3
    lstCAN_BUILD.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    lstNEEDED_RACE.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    lblCanBuild.setFont(def);
    lblResidentRace.setFont(def);
    lblUpgrade.setFont(def);
    lstCAN_BUILD.setFont(def);
    lstNEEDED_RACE.setFont(def);
    cmbUPGRADE.setFont(def);

    // PAGE 4
    lblUnrestBonus.setFont(def);
    Arrays.stream(txtBONI_2).forEach(x -> x.setFont(def));
    chkBUILDS_SPECIAL.setFont(def);
  }

  private void applySelected() throws KeyNotFoundException {
    if (SELECTED < 0)
      return;

    Building bld = BUILDING.building(SELECTED);
    bld.setName(txtNAME.getText());
    bld.setPluralName(txtPLURAL.getText());
    bld.setDesc(txtDESC.getText());

    // cost
    int vhm = Integer.parseInt(txtIND_COST.getText());
    if (vhm > ValueCast.MAX_USHORT)
      vhm = ValueCast.MAX_USHORT;
    if (!(vhm < 0))
      bld.setBuildCost(vhm);

    // energy
    short engyUsage = Short.parseShort(txtEN_COST.getText());
    if (engyUsage >= 0) {
      bld.setEnergyCost(engyUsage);
      if (chkSyncAIBldReq.isSelected()) {
        val req = aiBldReq.getReq(SELECTED);
        if (req.isPresent())
          req.get().setEnergyReq(engyUsage);
      }
    }

    // morale
    short moraleBonus = Short.parseShort(txtMORALE_BONI.getText());
    bld.setMoraleBonus(moraleBonus);

    // bonuses
    for (int i = 0; i < 6; i++) {
      short bonus = Short.parseShort(txtBONI[i].getText());
      bld.setBonusValue(i, bonus);
      bonus = Short.parseShort(txtBONI_2[i].getText());
      bld.setUnrestBonus(i, bonus);
    }
  }

  private void onSelectionChange(ListSelectionEvent e) {
    if (!init || selChange || e != null && e.getValueIsAdjusting())
      return;

    ID sel = lstBUILD.getSelectedValue();
    if (sel == null)
      return;

    try {
      if (SELECTED >= 0) {
        // apply changes
        applySelected();
        // update selection
        SELECTED = sel.ID;
        // skip duplicate selection events
        selChange = true;
        // update building list for name changes
        fillList();
      }

      // update building data
      updateSelectedBuilding();
    } catch (Exception ex) {
      Dialogs.displayError(ex);
    } finally {
      selChange = false;
    }
  }

  private void updateSelectedBuilding() {
    ID sel = lstBUILD.getSelectedValue();
    if (sel == null)
      return;

    // unset selected to skip change events
    SELECTED = -1;

    try {
      Building bld = BUILDING.building(sel.ID);
      txtNAME.setText(bld.getName());
      txtPLURAL.setText(bld.getPluralName());
      txtDESC.setText(BUILDING.getDesc(sel.ID));
      cmbTGA.setSelectedItem(bld.getImagePrefix() + "_b.tga"); //$NON-NLS-1$

      AIBldReqEntry bldReq = aiBldReq.getReq(sel.ID).orElse(null);
      chkSyncAIBldReq.setEnabled(bldReq != null);
      if (bldReq != null)
        chkSyncAIBldReq.setSelected(bldReq.getEnergyReq() == bld.getEnergyCost());

      cmbTECH.setSelectedIndex(bld.getTechField());
      int buildingTechLvl = bld.getTechLevel();
      cmbTECH_LVL.setSelectedItem(new ID(null, buildingTechLvl));
      txtIND_COST.setText(Integer.toString(bld.getBuildCost()));
      txtEN_COST.setText(Short.toString(bld.getEnergyCost()));
      cmbSYS_REQ.setSelectedIndex(bld.getSystemReq());
      cmbCATEGORY.setSelectedIndex(bld.getCategory());
      int t = bld.getPoliticReq();
      cmbPOLITIC.setSelectedIndex(t < 7 ? t : t - 2);
      cmbFREQ.setSelectedIndex(bld.getBuildLimit());
      txtMORALE_BONI.setText(Short.toString(bld.getMoraleBonus()));
      cmbBONI_TYPE.setSelectedIndex(bld.getBonusType());

      // when the building has no upgrade, select appended 'nothing' entry
      int upgradeIndex = bld.isUpgrade() ? bld.getUpgradeFor() : cmbUPGRADE.getItemCount() - 1;
      cmbUPGRADE.setSelectedIndex(upgradeIndex);

      String msg = Language.getString("BuildingsGUI.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(sel.ID)); //$NON-NLS-1$
      lblINDEX.setText(msg); //$NON-NLS-1$ //$NON-NLS-2$

      // pop cost
      long popCost = bld.getPopulationCost();
      cmbPOP_COST.setSelectedIndex(popCost == 0 ? 0 : 1);

      for (int i = 0; i < 6; i++) {
        txtBONI[i].setText(Short.toString(bld.getBonusValue(i)));
        txtBONI_2[i].setText(Short.toString(bld.getUnrestBonus(i)));
      }

      try {
        TargaImage tar = STBOF.getTargaImage(bld.getImagePrefix() + ".tga", LoadFlags.UNCACHED); //$NON-NLS-1$

        Dimension dim = tar.getImageSize();
        if ((dim.height > 120) || (dim.width > 100)) {
          lblPIC.setIcon(new ImageIcon(tar.getThumbnail(120, false, AlphaMode.Opaque)));
        } else {
          lblPIC.setIcon(new ImageIcon(tar.getImage(AlphaMode.Opaque)));
        }
        lblPIC.setText(null);
      } catch (Exception ex) {
        ex.printStackTrace();
        lblPIC.setIcon(null);
        lblPIC.setText(Language.getString("BuildingsGUI.49")); //$NON-NLS-1$
      }

      // who can build
      byte masked = bld.getExcludeEmpire();
      lstCAN_BUILD.clearSelection();
      ArrayList<Integer> tmpa = new ArrayList<Integer>();

      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        byte z = (byte) (1 << emp);
        if ((masked & z) != z)
          tmpa.add(emp);
      }

      if (!tmpa.isEmpty()) {
        int[] quest = new int[tmpa.size()];
        for (int n = 0; n < tmpa.size(); n++) {
          quest[n] = tmpa.get(n);
        }

        lstCAN_BUILD.setSelectedIndices(quest);
      }

      Building lstBld = BUILDING.building(sel.ID);

      // needed race
      lstNEEDED_RACE.clearSelection();
      long msk = lstBld.getResidentRaceReq();
      tmpa = new ArrayList<Integer>();

      for (int n = 0; n < RACE.getNumberOfEntries(); n++) {
        long z = 1L << n;
        if ((msk & z) == z)
          tmpa.add(n);
      }

      if (!tmpa.isEmpty()) {
        int[] quest = new int[tmpa.size()];
        for (int n = 0; n < tmpa.size(); n++) {
          quest[n] = tmpa.get(n);
        }

        lstNEEDED_RACE.setSelectedIndices(quest);
      }

      btnMoveUp.setEnabled(true);
      btnMoveDown.setEnabled(true);
      btnREMOVE.setEnabled(true);

      // builds special ships?
      if ((TREK != null) && (sel.ID <= 256)) {
        try {
          ByteValueSegment bjmp = (ByteValueSegment) TREK.getSegment(SD_SpShips.SpShpStrcJmp, true);
          SpecialShips bub = (SpecialShips) TREK.getInternalFile(CTrekSegments.SpecialShips, true);
          boolean enabled = false;

          switch (bjmp.byteValue()) {
            case JmpShort.JMP: {
              // all
              bjmp.setValue(JmpShort.JLE);
              bub.setSPBuilding(255);
            }
            case JmpShort.JLE: {
              // lower or equal than
              enabled = (sel.ID != 0) && ((sel.ID == bub.getSPBuilding())
                  || ((sel.ID == bub.getSPBuilding() + 1) && (sel.ID < 256)));
              chkBUILDS_SPECIAL.setEnabled(enabled);
              chkBUILDS_SPECIAL.setSelected(sel.ID <= bub.getSPBuilding());

              btnMoveUp.setEnabled(true);
              btnMoveDown.setEnabled(sel.ID != bub.getSPBuilding());

              break;
            }
            case JmpShort.JZ: {
              // one
              enabled = (sel.ID != bub.getSPBuilding()) && (sel.ID < 256);
              chkBUILDS_SPECIAL.setSelected(sel.ID == bub.getSPBuilding());
              chkBUILDS_SPECIAL.setEnabled(enabled);

              btnMoveUp.setEnabled((sel.ID < 256) || (bub.getSPBuilding() != 255));
              btnMoveDown.setEnabled((sel.ID != bub.getSPBuilding()) || (sel.ID != 255));

              break;
            }
            default:
              throw new IOException("unexpected jump operation");
          }
        } catch (Exception k) {
          Dialogs.displayError(k);
        }
      } else {
        chkBUILDS_SPECIAL.setEnabled(false);
        chkBUILDS_SPECIAL.setSelected(false);
      }

      // group
      cmbGROUP.setSelectedIndex(lstBld.getGroup());

      // trek.exe ids
      for (int i = 0; i < chkTREK_ID_BASIC.length; i++) {
        if (TREK == null) {
          chkTREK_ID_BASIC[i].setEnabled(false);
          chkTREK_ID_BASIC[i].setSelected(false);
        } else {
          chkTREK_ID_BASIC[i].setSelected(BUILDING.getTrekExeID(i) == sel.ID);
        }
      }

      // trek.exe id me groups
      for (int i = 0; i < chkTREK_ID_MARTIAL_LAW.length; i++) {
        if (TREK == null) {
          chkTREK_ID_MARTIAL_LAW[i].setEnabled(false);
          chkTREK_ID_MARTIAL_LAW[i].setSelected(false);
          chkTREK_ID_SHIPYARD[i].setEnabled(false);
          chkTREK_ID_SHIPYARD[i].setSelected(false);
        } else {
          chkTREK_ID_MARTIAL_LAW[i]
            .setSelected(BUILDING.getTrekExeID(StructureType.MartialLaw, i) == sel.ID);
          chkTREK_ID_SHIPYARD[i]
            .setSelected(BUILDING.getTrekExeID(StructureType.Shipyard, i) == sel.ID);
        }
      }

      for (int i = 0; i < chkTREK_ID_MINOR_SP.length; i++) {
        if (TREK == null) {
          chkTREK_ID_MINOR_SP[i].setEnabled(false);
          chkTREK_ID_MINOR_SP[i].setSelected(false);
        } else {
          chkTREK_ID_MINOR_SP[i]
            .setSelected(BUILDING.getTrekExeID(StructureType.MinorShipBuild, i + NUM_EMPIRES) == sel.ID);
        }
      }

      // restore selected
      SELECTED = sel.ID;
    } catch (Exception er) {
      Dialogs.displayError(er);
    }
  }

  private void fillList() {
    init = false;
    int prev = SELECTED;
    SELECTED = -1;

    // boni type
    try {
      int dun = cmbBONI_TYPE.getSelectedIndex();
      cmbBONI_TYPE.removeAllItems();
      for (int i = 0; i < BONUS.getNumberOfEntries(); i++) {
        cmbBONI_TYPE.addItem(new ID(i + ": " + BONUS.getBoniDesc(i), i)); //$NON-NLS-1$
      }

      cmbBONI_TYPE.setSelectedIndex(dun);
    } catch (Exception z) {
      Dialogs.displayError(z);
    }

    // upgrade list
    try {
      int dun = cmbUPGRADE.getSelectedIndex();
      cmbUPGRADE.removeAllItems();

      for (int i = 0; i < BUILDING.getNumberOfEntries(); i++) {
        Building bld = BUILDING.building(i);
        String msg = "%1: %2".replace("%1", Integer.toString(i)).replace("%2", bld.getName());
        cmbUPGRADE.addItem(new ID(msg, i));
      }

      cmbUPGRADE.addItem(new ID(Language.getString("BuildingsGUI.37"), Building.NO_UPGRADE)); //$NON-NLS-1$

      if (dun < 0 || dun >= cmbUPGRADE.getItemCount()) {
        dun = cmbUPGRADE.getItemCount() - 1;
      }

      cmbUPGRADE.setSelectedIndex(dun);
    } catch (Exception z) {
      Dialogs.displayError(z);
    }

    // main [buildings] list
    byte msk = (byte) cmbMASK.getSelectedIndex();
    ID[] ed = BUILDING.getShortIDs(msk, 30);
    lstBUILD.setListData(ed);

    // restore selection
    if (prev > -1)
      lstBUILD.setSelectedValue(new ID("", prev), true);

    // default to first entry
    if (lstBUILD.getSelectedIndex() < 0)
      lstBUILD.setSelectedIndex(0);

    // selection update is skipped by init
    // to not apply changes on reload
    updateSelectedBuilding();

    init = true;
  }

  private void btnMoveUpClick() {
    if (!init || SELECTED <= 0)
      return;

    try {
      // apply changes
      applySelected();
      buildingTools.switchIndex(SELECTED, SELECTED - 1);
      SELECTED--;
    } catch (Exception e) {
      Dialogs.displayError(e);
    }

    // update ship list for name changes
    fillList();
  }

  private void btnMoveDownClick() {
    if (!init || SELECTED < 0 || SELECTED >= BUILDING.getNumberOfEntries() - 1)
      return;

    try {
      // apply changes
      applySelected();
      buildingTools.switchIndex(SELECTED, SELECTED + 1);
      SELECTED++;
    } catch (Exception e) {
      Dialogs.displayError(e);
    }

    // update ship list for name changes
    fillList();
  }

  private boolean showInsufficientBldMaskDialog(int maxBuildings) throws IOException {
    String title = "Insufficient build mask size";
    String desc = "The add exceeds the current empire ignore mask limit of %1 buildings."
      + "\nPlease select how to deal with excessive buildings:";
    desc = desc.replace("%1", Integer.toString(maxBuildings));
    String footer = "Be aware that for size changes, trek.exe must be patched manually.";

    String[] options = new String[] {
      // tech tree levels
      "auto-adjust edifice and racetech empire building ignore mask",
      // max used
      "auto-adjust edifice empire building ignore mask",
      // ignore tech map size changes
      "truncate from the ignore masks"
    };

    val dialog = new OptionsDialog(title, desc, options, footer);
    int res = dialog.show();
    if (res < 0) {
      // cancelled or no option selected
      return false;
    }

    // update stored values on finalWarning when the panel got closed
    // this way when buildings are removed again the original size is restored
    switch (res) {
      case 0:
        increaseBldMask = true;
        increaseRTBldMask = true;
        break;
      case 1:
        increaseBldMask = true;
        break;
      default:
        ignoreBldMaskChanges = true;
    }

    return true;
  }

  private boolean showInsufficientTechMapDialog(int techLvls, int reqTechLvls) {
    // require as many tech levels actually used
    reqTechLvls = Integer.max(reqTechLvls, BUILDING.getMaxTechLevel());
    int techTreeLvls = 1 + TECH_TREE.getMaxTechLevel();

    String title = "Insufficient tech map size";
    String desc = "Your tech level change exceeds the current maximum of %1 tech levels."
      + "\nPlease select how to deal with excessive tech levels:";
    desc = desc.replace("%1", Integer.toString(techLvls));
    String footer = "Be aware that for size changes, trek.exe must be patched manually.";

    String[] options = new String[] {
      // tech tree levels
      "patch to %1 tech tree levels"
        .replace("%1", Integer.toString(techTreeLvls)),
      // max used
      "auto-adjust tech levels",
      // ignore tech map size changes
      "truncate from the tech map"
    };

    // default to tech tree tech level count
    // the available selections are loaded from the tech tree anyhow
    val dialog = new OptionsDialog(title, desc, options, footer);

    // disable tech level option in case it is exceeded (manually patched)
    if (techTreeLvls < reqTechLvls) {
      val rbns = dialog.getRbnOptions();
      rbns[0].setEnabled(false);
      rbns[1].setSelected(true);
    }

    int res = dialog.show();
    if (res < 0) {
      // reset tech level if cancelled but ignore recursive selection events
      Building bld = BUILDING.building(SELECTED);
      cmbTECH_LVL.setEnabled(false);
      cmbTECH_LVL.setSelectedItem(new ID(null, bld.getTechLevel()));
      cmbTECH_LVL.setEnabled(true);
      return false;
    }

    // update stored values on finalWarning when the panel got closed
    // this way when buildings are removed again the original size is restored
    switch (res) {
      case 0:
        BUILDING.setTechLevels(techTreeLvls);
        break;
      case 1:
        increaseTechLvls = true;
        break;
      default:
        ignoreTechLvlChanges = true;
    }

    return true;
  }

  private void showSettingsDialog() {
    int techTreeLvls = 1 + TECH_TREE.getMaxTechLevel();
    int loadedTechLvls = BUILDING.getLoadedTechLvls();
    int loadedBldMaskLimit = BUILDING.getLoadedMaxBuildings();

    String title = "Edifice Settings";
    String techLvlDesc = "Select how to deal with excessive tech levels:";
    String bldMaskDesc = "Select how to deal with excessive buildings:";
    String footer = "Be aware that for size changes, trek.exe must be patched manually.";

    String[] techOptions = new String[] {
      // tech tree levels
      "patch to %1 tech tree levels"
        .replace("%1", Integer.toString(techTreeLvls)),
      // max used
      "auto-adjust tech levels",
      // ignore tech map size changes
      "truncate from the tech map",
      // unset to query again next excessive selection
      "reset to %1 loaded tech levels"
        .replace("%1", Integer.toString(loadedTechLvls))
    };

    String[] bldMaskOptions = new String[] {
      // tech tree levels
      "auto-adjust edifice and racetech empire building ignore mask",
      // max used
      "auto-adjust edifice empire building ignore mask",
      // ignore tech map size changes
      "truncate from the ignore masks",
      // unset to query again next excessive add
      "reset to %1 loaded max build mask limit"
        .replace("%1", Integer.toString(loadedBldMaskLimit))
    };

    JLabel lblTechLvlDesc = new JLabel(techLvlDesc);
    JLabel lblBldMaskDesc = new JLabel(bldMaskDesc);

    JRadioButton[] rbnTechLvl = Arrays.stream(techOptions)
      .map(x -> new JRadioButton(x)).toArray(JRadioButton[]::new);
    JRadioButton[] rbnBldMask = Arrays.stream(bldMaskOptions)
      .map(x -> new JRadioButton(x)).toArray(JRadioButton[]::new);

    ButtonGroup rbgTechLvl_Group = new ButtonGroup();
    for (JRadioButton rbn : rbnTechLvl)
      rbgTechLvl_Group.add(rbn);
    ButtonGroup rbgBldMask_Group = new ButtonGroup();
    for (JRadioButton rbn : rbnBldMask)
      rbgBldMask_Group.add(rbn);

    JLabel lblFooter = new JLabel(footer);

    JPanel pnlOptions = new JPanel(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;

    c.insets.set(5, 5, 5, 5);
    pnlOptions.add(lblTechLvlDesc, c);
    c.insets.set(0, 0, 0, 0);
    for (JRadioButton rbn : rbnTechLvl) {
      c.gridy++;
      pnlOptions.add(rbn, c);
    }
    c.insets.set(5, 5, 5, 5);
    c.gridy++;
    pnlOptions.add(lblBldMaskDesc, c);
    c.insets.set(0, 0, 0, 0);
    for (JRadioButton rbn : rbnBldMask) {
      c.gridy++;
      pnlOptions.add(rbn, c);
    }
    c.insets.set(5, 5, 0, 5);
    c.gridy++;
    pnlOptions.add(lblFooter, c);

    int usedTechLvls = BUILDING.getMaxTechLevel();
    int techLvls = BUILDING.getTechLvls();

    // require as many tech levels actually used
    if (techTreeLvls < usedTechLvls || (techLvls == techTreeLvls && techLvls == loadedTechLvls))
      rbnTechLvl[0].setEnabled(false);

    if (increaseTechLvls)
      rbnTechLvl[1].setSelected(true);
    else if (ignoreTechLvlChanges)
      rbnTechLvl[2].setSelected(true);
    else if (techLvls == techTreeLvls && techLvls != loadedTechLvls)
      rbnTechLvl[0].setSelected(true);
    else
      rbnTechLvl[3].setSelected(true);

    if (increaseRTBldMask)
      rbnBldMask[0].setSelected(true);
    else if (increaseBldMask)
      rbnBldMask[1].setSelected(true);
    else if (ignoreBldMaskChanges)
      rbnBldMask[2].setSelected(true);
    else
      rbnBldMask[3].setSelected(true);

    int response = JOptionPane.showConfirmDialog(UE.WINDOW, pnlOptions, title, JOptionPane.OK_CANCEL_OPTION);
    if (response != JOptionPane.OK_OPTION) {
      // abort, menu got closed or cancelled
      return;
    }

    if (rbnTechLvl[0].isSelected()) {
      increaseTechLvls = false;
      ignoreTechLvlChanges = false;
      BUILDING.setTechLevels(techTreeLvls);
    } else if (rbnTechLvl[1].isSelected()) {
      increaseTechLvls = true;
      ignoreTechLvlChanges = false;
      BUILDING.setTechLevels(loadedTechLvls);
    } else if (rbnTechLvl[2].isSelected()) {
      increaseTechLvls = false;
      ignoreTechLvlChanges = true;
      BUILDING.setTechLevels(loadedTechLvls);
    } else {
      increaseTechLvls = false;
      ignoreTechLvlChanges = false;
      BUILDING.setTechLevels(loadedTechLvls);
    }

    // update stored values on finalWarning when the panel got closed
    // this way when buildings are removed again the original size is restored
    if (rbnBldMask[0].isSelected()) {
      increaseBldMask = true;
      increaseRTBldMask = true;
      ignoreBldMaskChanges = false;
    } else if (rbnBldMask[1].isSelected()) {
      increaseBldMask = true;
      increaseRTBldMask = false;
      ignoreBldMaskChanges = false;
    } else if (rbnBldMask[2].isSelected()) {
      increaseBldMask = false;
      increaseRTBldMask = false;
      ignoreBldMaskChanges = true;
      BUILDING.setBldMaskLongs(BUILDING.getLoadedBldMaskLongs());
    } else {
      increaseBldMask = false;
      increaseRTBldMask = false;
      ignoreBldMaskChanges = false;
      BUILDING.setBldMaskLongs(BUILDING.getLoadedBldMaskLongs());
    }
  }

}
