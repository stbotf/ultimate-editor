package ue.gui.stbof.bld;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bst.Edifbnft;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;

/**
 * This class is used to display the about window.
 */
public class BuildingsMultiSetGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_BLD_CAT = CStbof.NUM_BLD_CAT;

  // read
  private Stbof STBOF = null;
  private Edifbnft BONUS = null;
  private RaceRst RACE = null;

  // edited
  private Edifice BUILDING = null;

  // ui components
  private JPanel pnlFILTER;
  private JPanel pnlCONF;
  private JTextField txtVALUE[];
  private JComboBox<ID> cmbBONI;
  private JComboBox<String> cmbSET;
  private JComboBox<ID> cmbEMPIRE;
  private JButton btnEXECUTE;
  private JLabel lblALL;

  // data
  private Timer tCOOLER = new Timer(5000, new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent arg0) {
      btnEXECUTE.setEnabled(true);
    }
  });

  /**
   * Costructor
   * @throws IOException
   */
  public BuildingsMultiSetGUI(Stbof st) throws IOException {
    STBOF = st;
    BUILDING = (Edifice) STBOF.getInternalFile(CStbofFiles.EdificeBst, true);
    BONUS = (Edifbnft) STBOF.getInternalFile(CStbofFiles.EdifBnftBst, true);
    RACE = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);

    /*CREATE*/
    pnlFILTER = new JPanel(new GridBagLayout());
    pnlCONF = new JPanel(new GridBagLayout());

    txtVALUE = new JTextField[6];
    btnEXECUTE = new JButton(Language.getString("BuildingsMultiSetGUI.0")); //$NON-NLS-1$
    cmbBONI = new JComboBox<ID>();
    cmbSET = new JComboBox<String>();
    cmbEMPIRE = new JComboBox<ID>();

    /*SETUP*/
    //font
    Font def = UE.SETTINGS.getDefaultFont();

    // timer
    tCOOLER.setRepeats(false);

    //filter panel
    pnlFILTER.setBorder(new LineBorder(Color.BLACK));

    // set panel
    pnlCONF.setBorder(new LineBorder(Color.BLACK));

    // text fields
    for (int i = 0; i < 6; i++) {
      txtVALUE[i] = new JTextField("5*L^(1-2/1+1)+(L-L)"); //$NON-NLS-1$
      txtVALUE[i].setFont(def);
    }

    //bonus type combo
    cmbBONI.setFont(def);
    for (int i = 0; i < BONUS.getNumberOfEntries(); i++) {
      cmbBONI.addItem(new ID(i + ": " + BONUS.getBoniDesc(i), i));
    }

    // set combo
    cmbSET.setFont(def);
    cmbSET.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (cmbSET.getSelectedIndex() < 0) {
          return;
        }
        if (cmbSET.getSelectedIndex() < 3) {
          // industry cost, energy, morale
          for (int i = 0; i < NUM_BLD_CAT; i++) {
            txtVALUE[i].setEnabled(false);
          }
          lblALL.setText(Language.getString("BuildingsMultiSetGUI.1")); //$NON-NLS-1$
        } else {
          // bonus, unrest
          for (int i = 0; i < NUM_BLD_CAT; i++) {
            txtVALUE[i].setEnabled(true);
          }
          lblALL.setText(Language.getString("BuildingsMultiSetGUI.2")); //$NON-NLS-1$
        }
      }
    });

    // empire combo
    cmbEMPIRE.setFont(def);
    for (int i = 0; i < NUM_EMPIRES; i++) {
      cmbEMPIRE.addItem(new ID(RACE.getName(i), i));
    }
    cmbEMPIRE.addItem(new ID(Language.getString("BuildingsMultiSetGUI.12"), NUM_EMPIRES)); //$NON-NLS-1$
    cmbEMPIRE.setSelectedIndex(NUM_EMPIRES);

    //add button
    btnEXECUTE.setFont(def);
    btnEXECUTE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        btnEXECUTE.setEnabled(false);
        tCOOLER.start();

        try {
          int em = cmbEMPIRE.getSelectedIndex();
          byte empire = em < NUM_EMPIRES ? (byte) (1 << em) : (byte) 0xE0;

          String val[] = new String[6];
          for (int i = 0; i < 6; i++) {
            val[i] = txtVALUE[i].getText();
          }

          switch (cmbSET.getSelectedIndex()) {
            case 0: {
              // industry
              BUILDING.multiSetIndustryCost((byte) cmbBONI.getSelectedIndex(), val[NUM_BLD_CAT], empire);
              break;
            }
            case 1: {
              // energy
              BUILDING.multiSetEnergy((byte) cmbBONI.getSelectedIndex(), val[NUM_BLD_CAT], empire);
              break;
            }
            case 2: {
              // morale
              BUILDING.multiSetMorale((byte) cmbBONI.getSelectedIndex(), val[NUM_BLD_CAT], empire);
              break;
            }
            case 3: {
              // bonus
              BUILDING.multiSetBonus((byte) cmbBONI.getSelectedIndex(), val, empire);
              break;
            }
            case 4: {
              // unrest
              BUILDING.multiSetUnrestBonus((byte) cmbBONI.getSelectedIndex(), val, empire);
              break;
            }
          }
        } catch (Exception tu) {
          Dialogs.displayError(tu);
        }
      }
    });

    /*PLACE*/
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;

    // FILTER PANEL
    JLabel lbl = new JLabel(Language.getString("BuildingsMultiSetGUI.3")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlFILTER.add(lbl, c);
    c.gridx = 1;
    pnlFILTER.add(cmbBONI, c);
    c.gridy = 1;
    c.gridx = 0;
    lbl = new JLabel(Language.getString("BuildingsMultiSetGUI.13")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlFILTER.add(lbl, c);
    c.gridx = 1;
    pnlFILTER.add(cmbEMPIRE, c);

    //SET PANEL
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    lbl = new JLabel(Language.getString("BuildingsMultiSetGUI.4")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlCONF.add(lbl, c);
    c.gridx = 1;
    pnlCONF.add(cmbSET, c);
    c.gridy = 1;
    c.gridx = 0;
    c.gridwidth = 2;
    lbl = new JLabel(Language.getString("BuildingsMultiSetGUI.5")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlCONF.add(lbl, c);

    c.gridwidth = 1;
    for (int i = 0; i < NUM_EMPIRES; i++) {
      c.gridy++;
      c.gridx = 0;
      lbl = new JLabel(RACE.getName(i) + ":"); //$NON-NLS-1$
      lbl.setFont(def);
      pnlCONF.add(lbl, c);
      c.gridx = 1;
      pnlCONF.add(txtVALUE[i], c);
    }

    c.gridy = 7;
    c.gridx = 0;
    lblALL = new JLabel(Language.getString("BuildingsMultiSetGUI.1")); //$NON-NLS-1$
    lblALL.setFont(def);
    pnlCONF.add(lblALL, c);
    c.gridx = 1;
    pnlCONF.add(txtVALUE[NUM_BLD_CAT], c);

    //
    c.gridx = 0;
    c.gridy = 0;
    add(pnlFILTER, c);
    c.gridy = 1;
    add(pnlCONF, c);
    c.gridy = 2;
    c.fill = GridBagConstraints.NONE;
    Dimension dim = btnEXECUTE.getPreferredSize();
    if (dim.width < 60) {
      dim.width = 60;
    }
    btnEXECUTE.setPreferredSize(dim);
    add(btnEXECUTE, c);

    // init
    cmbSET.addItem(Language.getString("BuildingsMultiSetGUI.7")); //$NON-NLS-1$
    cmbSET.addItem(Language.getString("BuildingsMultiSetGUI.8")); //$NON-NLS-1$
    cmbSET.addItem(Language.getString("BuildingsMultiSetGUI.9")); //$NON-NLS-1$
    cmbSET.addItem(Language.getString("BuildingsMultiSetGUI.10")); //$NON-NLS-1$
    cmbSET.addItem(Language.getString("BuildingsMultiSetGUI.11")); //$NON-NLS-1$

  }

  @Override
  public String menuCommand() {
    return MenuCommand.BuildingGroupEdit;
  }

  @Override
  public String getHelpFileName() {
    return "BuildingsMultiSet.html"; //$NON-NLS-1$
  }

  @Override
  public boolean hasChanges() {
    return BUILDING.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(BUILDING, changes);
  }

  @Override
  public void reload() throws IOException {
    BUILDING = (Edifice) STBOF.getInternalFile(CStbofFiles.EdificeBst, true);
  }

  @Override
  public void finalWarning() throws IOException, InvalidArgumentsException {
    // update all pending secondary segments
    // don't wait for edifice to be saved to allow inspect changes in segments view
    BUILDING.updateExe();
  }
}
