package ue.gui.stbof.bld;

import java.io.IOException;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.files.bst.Edifbnft;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.tec.TecField;
import ue.exception.InvalidArgumentsException;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.HtmlReportGUI;
import ue.service.Language;

/**
 * Creates html reports based on input files.
 * <p>
 * Code for generating the html report for buildings is based on Gowron's BuildView2 tool.
 */
public class BuildingsReportGUI extends HtmlReportGUI {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_BLD_CAT = CStbof.NUM_BLD_CAT;

  // html strings
  private static final String strHeaderStart = "<H1 align=center>"; //$NON-NLS-1$
  private static final String strHeaderEnd = "</H1>\n<br>\n<br>\n\n"; //$NON-NLS-1$
  private static final String strTableHeadline =
      "<table border cellspacing=1 cellpadding=1 align=center> \n \n" + //$NON-NLS-1$
          "<TR>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>CATEGORY</TH>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>NAME</TH>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>REQUIRED TECH</TH>\n" + //$NON-NLS-1$
          "<TH colspan=2 align=center bgcolor=silver>COST / EN</TH>\n" + //$NON-NLS-1$
          "<TH colspan=2 align=center bgcolor=silver>OUTPUT / MORALE</TH>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>LIMIT</TH> \n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>SYSTEM REQ.</TH> \n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>POLITICAL REQ.</TH> \n" + //$NON-NLS-1$
          "</TR>\n\n"; //$NON-NLS-1$

  private static final String strTableHeadlineMinors =
      "<table border cellspacing=1 cellpadding=1 align=center> \n \n" + //$NON-NLS-1$
          "<TR>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>RACE</TH>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>CATEGORY</TH>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>NAME</TH>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>REQUIRED TECH</TH>\n" + //$NON-NLS-1$
          "<TH colspan=2 align=center bgcolor=silver>COST / EN</TH>\n" + //$NON-NLS-1$
          "<TH colspan=2 align=center bgcolor=silver>OUTPUT / MORALE</TH>\n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>LIMIT</TH> \n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>SYSTEM REQ.</TH> \n" + //$NON-NLS-1$
          "<TH align=center bgcolor=silver>POLITICAL REQ.</TH> \n" + //$NON-NLS-1$
          "</TR>\n\n"; //$NON-NLS-1$

  private final String[] category = new String[]{
    Language.getString("BuildingsReportGUI.3" /*Research*/),
    Language.getString("BuildingsReportGUI.4" /*Development*/),
    Language.getString("BuildingsReportGUI.5" /*Food*/),
    Language.getString("BuildingsReportGUI.6" /*Energy*/),
    Language.getString("BuildingsReportGUI.7" /*Defense*/)
  }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

  private Edifice edifice;
  private Edifbnft edifbnft;
  private RaceRst race;
  private Lexicon lexicon;
  private TecField tecfield;

  /**
   * Displays report on buildings.
   * @throws IOException
   */
  public BuildingsReportGUI(Stbof stbof) throws IOException {
    val stif = stbof.files();
    edifice = stif.edifice();
    edifbnft = stif.edifbnft();
    race = stif.raceRst();
    lexicon = stif.lexicon();
    tecfield = stif.tecfield();

    updateReport();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.EdificeReport;
  }

  @Override
  protected InternalFile[] filesToCheck() {
    return new InternalFile[] {
      edifice,
      edifbnft,
      race,
      lexicon,
      tecfield
    };
  }

  @Override
  protected void genReport(StringBuffer report) throws InvalidArgumentsException {
    // stuff
    long emp_mask;
    String[] limit = new String[]{
      lexicon.getEntry(LexDataIdx.Shared.None),
      lexicon.getEntry(LexDataIdx.System.Restrictions.OnePerSystem),
      lexicon.getEntry(LexDataIdx.System.Restrictions.OnePerEmpire)
    };
    String[] sys_req = new String[]{
        lexicon.getEntry(LexDataIdx.System.Restrictions.ArcticPlanet),
        lexicon.getEntry(LexDataIdx.System.Restrictions.BarrenPlanet),
        lexicon.getEntry(LexDataIdx.System.Restrictions.DesertPlanet),
        lexicon.getEntry(LexDataIdx.System.PlanetTypes.GasGiant),
        lexicon.getEntry(LexDataIdx.System.Restrictions.JunglePlanet),
        lexicon.getEntry(LexDataIdx.System.Restrictions.OceanicPlanet),
        lexicon.getEntry(LexDataIdx.System.Restrictions.TerranPlanet),
        lexicon.getEntry(LexDataIdx.System.Restrictions.VolcanicPlanet),
        lexicon.getEntry(LexDataIdx.Shared.None),
        lexicon.getEntry(LexDataIdx.System.PlanetTypes.Asteroid),
        lexicon.getEntry(LexDataIdx.System.Restrictions.AsteroidBeltDilithium),
        lexicon.getEntry(LexDataIdx.System.Restrictions.Dilithium),
        lexicon.getEntry(LexDataIdx.Galaxy.StellarObjects.Wormhole),
        lexicon.getEntry(LexDataIdx.Galaxy.StellarObjects.RadioPulsar),
        lexicon.getEntry(LexDataIdx.Galaxy.StellarObjects.XRayPulsar)
    };
    String[] pol_req = new String[]{
        lexicon.getEntry(LexDataIdx.System.Restrictions.HomeSystem),
        lexicon.getEntry(LexDataIdx.System.Restrictions.NativeMemberSystem),
        lexicon.getEntry(LexDataIdx.System.Restrictions.NonNativeMemberSystem),
        lexicon.getEntry(LexDataIdx.System.Restrictions.SubjugatedSystem),
        lexicon.getEntry(LexDataIdx.System.Restrictions.AffiliatedSystem),
        lexicon.getEntry(LexDataIdx.System.Restrictions.IndependentMinorSystem),
        lexicon.getEntry(LexDataIdx.System.Restrictions.ConqueredHomeSystem),
        null,
        null,
        lexicon.getEntry(LexDataIdx.Shared.None),
        lexicon.getEntry(LexDataIdx.System.Restrictions.RebelSystem),
        lexicon.getEntry(LexDataIdx.System.Restrictions.EmptySystem)
    };

    for (int empire = 0; empire < NUM_EMPIRES; empire++) {
      emp_mask = 1L << empire;

      report.append(strHeaderStart);

      // empire
      report.append(race.getName(empire));
      report.append(strHeaderEnd);

      // headline
      report.append(strTableHeadline);

      // sort by category
      for (int cat = 0; cat < NUM_BLD_CAT; cat++) {
        for (int i = 0; i < edifice.getNumberOfEntries(); i++) {
          Building bld = edifice.building(i);

          // ignore basic shipyard
          // this is likely set in trek.exe
          if (i == 15) {
            continue;
          }

          // ignore wrong category
          if (bld.getCategory() != cat) {
            continue;
          }

          // ignore not buildable
          if (((int) emp_mask & bld.getExcludeEmpire()) != 0) {
            continue;
          }

          // ignore main buildings
          // and buildings that don't list the empire as needed race on system
          if (bld.isMainBuilding() || (bld.getResidentRaceReq() & emp_mask) != emp_mask) {
            continue;
          }

          // ignore trade goods and emergency morale programs
          if (bld.getBonusType() == 4 || bld.getBonusType() == 36) {
            continue;
          }

          // accepted building
          // add to report

          // category
          report.append("<TR>\n"); //$NON-NLS-1$
          report.append("<TD align=center>"); //$NON-NLS-1$
          report.append(category[cat]);
          report.append("</TD>\n"); //$NON-NLS-1$

          // name
          report.append("<TH align=center bgcolor=#00E0FF>"); //$NON-NLS-1$
          report.append(bld.getName());
          report.append("</TH>\n"); //$NON-NLS-1$

          // tech requirement
          report.append("<TD align=center>"); //$NON-NLS-1$

          if (bld.getTechField() >= 0
              && bld.getTechField() < tecfield.getFields().length - 1) {
            report.append(
                tecfield.getField(bld.getTechField()).substring(0, 1).toUpperCase()
                    + tecfield.getField(bld.getTechField()).substring(1).toLowerCase());
          } else {
            report.append(Language.getString("BuildingsReportGUI.0")); //$NON-NLS-1$
          }

          report.append(" "); //$NON-NLS-1$
          report.append(bld.getTechLevel());
          report.append("</TD>\n"); //$NON-NLS-1$

          // cost
          report.append("<TD align=right bgcolor=silver>"); //$NON-NLS-1$
          report.append(bld.getBuildCost());
          report.append("</TD>\n"); //$NON-NLS-1$

          // energy
          report.append("<TD align=right bgcolor=silver>"); //$NON-NLS-1$
          report.append(bld.getEnergyCost());
          report.append("</TD>\n"); //$NON-NLS-1$

          // output
          report.append("<TD align=center>"); //$NON-NLS-1$

          if (bld.getBonusType() != 18)   // no output value for shipyards
          {
            if (bld.getBonusValue(empire) > 0) {
              report.append("+");        //$NON-NLS-1$
            }

            report.append(bld.getBonusValue(empire));
          }

          report.append(edifbnft.getBoniDesc(bld.getBonusType()));
          report.append("</TD>\n"); //$NON-NLS-1$

          // morale
          report.append("<TH align=center"); //$NON-NLS-1$

          if (bld.getMoraleBonus() < 0) {
            report.append(" bgcolor=#FF4020>"); //$NON-NLS-1$
            report.append(bld.getMoraleBonus());
          } else if (bld.getMoraleBonus() == 0) {
            report.append(">-----");            //$NON-NLS-1$
          } else {
            report.append(" bgcolor=#00FF00>+"); //$NON-NLS-1$
            report.append(bld.getMoraleBonus());
          }

          report.append("</TH>\n"); //$NON-NLS-1$

          // limit
          report.append("<TD align=center bgcolor=silver>"); //$NON-NLS-1$
          report.append(limit[bld.getBuildLimit()]);
          report.append("</TD>\n"); //$NON-NLS-1$

          // requirement
          report.append("<TD align=center>"); //$NON-NLS-1$
          if (bld.getSystemReq() != 8) {
            report.append(sys_req[bld.getSystemReq()]);
          } else {
            report.append("-----"); //$NON-NLS-1$
          }
          report.append("</TD>\n"); //$NON-NLS-1$

          report.append("<TD align=center>"); //$NON-NLS-1$
          if (bld.getPoliticReq() != 9) {
            report.append(pol_req[bld.getPoliticReq()]);
          } else {
            report.append("-----"); //$NON-NLS-1$
          }
          report.append("</TD>\n"); //$NON-NLS-1$

          report.append("</TR>\n\n"); //$NON-NLS-1$
        }

        if (cat < 4) {
          report.append("<TR>\n"); //$NON-NLS-1$
          report.append("<TD align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append("<TD align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append("<TD align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append(
              "<TD colspan=2 align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append(
              "<TD colspan=2 align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append("<TD align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append("<TD align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append("<TD align=center bgcolor=silver>-----</TD>\n"); //$NON-NLS-1$
          report.append("</TR>\n\n"); //$NON-NLS-1$
        }
      }

      report.append("\n\n</table>\n\n"); //$NON-NLS-1$
      report.append("\n<br>\n<br>\n<br>\n\n"); //$NON-NLS-1$
    }

    // minor races
    report.append(strHeaderStart);

    report.append(Language.getString("BuildingsReportGUI.1")); //$NON-NLS-1$
    report.append(strHeaderEnd);

    // headline
    report.append(strTableHeadlineMinors);

    long r_mask = 0;

    // sort by race
    for (int r = NUM_EMPIRES; r < race.getNumberOfEntries(); r++) {
      r_mask = 1L << r;

      for (int i = 0; i < edifice.getNumberOfEntries(); i++) {
        Building bld = edifice.building(i);

        // ignore basic shipyard
        // this is likely set in trek.exe
        if (i == 15) {
          continue;
        }

        // ignore if not buildable by this race
        // and i not minor race structure
        if ((bld.getResidentRaceReq() & r_mask) != r_mask || (bld.getResidentRaceReq() & 31) != 0) {
          continue;
        }

        // ignore trade goods and emergency morale programs
        if (bld.getBonusType() == 4 || bld.getBonusType() == 36) {
          continue;
        }

        // accepted building
        // add to report

        // race
        report.append("<TR>\n"); //$NON-NLS-1$
        report.append("<TD align=center>"); //$NON-NLS-1$
        report.append(race.getName(r));
        report.append("</TD>\n"); //$NON-NLS-1$

        // category
        report.append("<TD align=center>"); //$NON-NLS-1$
        report.append(category[bld.getCategory()]);
        report.append("</TD>\n"); //$NON-NLS-1$

        // name
        report.append("<TH align=center bgcolor=#00E0FF>"); //$NON-NLS-1$
        report.append(bld.getName());
        report.append("</TH>\n"); //$NON-NLS-1$

        // tech requirement
        report.append("<TD align=center>"); //$NON-NLS-1$

        if (bld.getTechField() >= 0 && bld.getTechField() < tecfield.getFields().length - 1) {
          report.append(tecfield.getField(bld.getTechField()).substring(0, 1).toUpperCase()
              + tecfield.getField(bld.getTechField()).substring(1).toLowerCase());
        } else {
          report.append(Language.getString("BuildingsReportGUI.0")); //$NON-NLS-1$
        }

        report.append(" "); //$NON-NLS-1$
        report.append(bld.getTechLevel());
        report.append("</TD>\n"); //$NON-NLS-1$

        // cost
        report.append("<TD align=right bgcolor=silver>"); //$NON-NLS-1$
        report.append(bld.getBuildCost());
        report.append("</TD>\n"); //$NON-NLS-1$

        // energy
        report.append("<TD align=right bgcolor=silver>"); //$NON-NLS-1$
        report.append(bld.getEnergyCost());
        report.append("</TD>\n"); //$NON-NLS-1$

        // output
        report.append("<TD align=center>"); //$NON-NLS-1$

        if (bld.getBonusType() != 18)   // no output value for shipyards
        {
          if (bld.getBonusValue(NUM_EMPIRES) > 0) {
            report.append("+");        //$NON-NLS-1$
          }

          report.append(bld.getBonusValue(NUM_EMPIRES));
        }

        report.append(edifbnft.getBoniDesc(bld.getBonusType()));
        report.append("</TD>\n"); //$NON-NLS-1$

        // morale
        report.append("<TH align=center"); //$NON-NLS-1$

        if (bld.getMoraleBonus() < 0) {
          report.append(" bgcolor=#FF4020>"); //$NON-NLS-1$
          report.append(bld.getMoraleBonus());
        } else if (bld.getMoraleBonus() == 0) {
          report.append(">-----");            //$NON-NLS-1$
        } else {
          report.append(" bgcolor=#00FF00>+"); //$NON-NLS-1$
          report.append(bld.getMoraleBonus());
        }

        report.append("</TH>\n"); //$NON-NLS-1$

        // limit
        report.append("<TD align=center bgcolor=silver>"); //$NON-NLS-1$
        report.append(limit[bld.getBuildLimit()]);
        report.append("</TD>\n"); //$NON-NLS-1$

        // requirement
        report.append("<TD align=center>"); //$NON-NLS-1$
        if (bld.getSystemReq() != 8) {
          report.append(sys_req[bld.getSystemReq()]);
        } else {
          report.append("-----"); //$NON-NLS-1$
        }
        report.append("</TD>\n"); //$NON-NLS-1$

        report.append("<TD align=center>"); //$NON-NLS-1$
        if (bld.getPoliticReq() != 9) {
          report.append(pol_req[bld.getPoliticReq()]);
        } else {
          report.append("-----"); //$NON-NLS-1$
        }
        report.append("</TD>\n"); //$NON-NLS-1$

        report.append("</TR>\n\n"); //$NON-NLS-1$
      }
    }

    report.append("\n\n</table>\n\n"); //$NON-NLS-1$
    report.append("\n<br>\n<br>\n<br>\n\n"); //$NON-NLS-1$

    // main buildings
    report.append(strHeaderStart);

    report.append(Language.getString("BuildingsReportGUI.2")); //$NON-NLS-1$
    report.append(strHeaderEnd);

    // headline
    report.append(strTableHeadlineMinors);

    r_mask = 0;

    // sort by race
    for (int r = 0; r < NUM_EMPIRES; r++) {
      r_mask = 1L << r;

      for (int i = 0; i < edifice.getNumberOfEntries(); i++) {
        Building bld = edifice.building(i);

        // ignore basic shipyard
        // this is likely set in trek.exe
        if (i == 15) {
          continue;
        }

        // ignore not buildable
        if (((int) r_mask & bld.getExcludeEmpire()) != 0) {
          continue;
        }

        // ignore main buildings
        // and buildings that don't list the empire as needed race on system
        if (!bld.isMainBuilding() || (bld.getResidentRaceReq() & r_mask) != r_mask) {
          continue;
        }

        // ignore trade goods and emergency morale programs
        if (bld.getBonusType() == 4 || bld.getBonusType() == 36) {
          continue;
        }

        // accepted building
        // add to report

        // race
        report.append("<TR>\n"); //$NON-NLS-1$
        report.append("<TD align=center>"); //$NON-NLS-1$
        report.append(race.getName(r));
        report.append("</TD>\n"); //$NON-NLS-1$

        // category
        report.append("<TD align=center>"); //$NON-NLS-1$
        report.append(category[bld.getCategory()]);
        report.append("</TD>\n"); //$NON-NLS-1$

        // name
        report.append("<TH align=center bgcolor=#00E0FF>"); //$NON-NLS-1$
        report.append(bld.getName());
        report.append("</TH>\n"); //$NON-NLS-1$

        // tech requirement
        report.append("<TD align=center>"); //$NON-NLS-1$

        if (bld.getTechField() >= 0
            && bld.getTechField() < tecfield.getFields().length - 1) {
          report.append(tecfield.getField(bld.getTechField()).substring(0, 1).toUpperCase()
              + tecfield.getField(bld.getTechField()).substring(1).toLowerCase());
        } else {
          report.append(Language.getString("BuildingsReportGUI.0")); //$NON-NLS-1$
        }

        report.append(" "); //$NON-NLS-1$
        report.append(bld.getTechLevel());
        report.append("</TD>\n"); //$NON-NLS-1$

        // cost
        report.append("<TD align=right bgcolor=silver>"); //$NON-NLS-1$
        report.append(bld.getBuildCost());
        report.append("</TD>\n"); //$NON-NLS-1$

        // energy
        report.append("<TD align=right bgcolor=silver>"); //$NON-NLS-1$
        report.append(bld.getEnergyCost());
        report.append("</TD>\n"); //$NON-NLS-1$

        // output
        report.append("<TD align=center>"); //$NON-NLS-1$

        if (bld.getBonusType() != 18)   // no output value for shipyards
        {
          if (bld.getBonusValue(5) > 0) {
            report.append("+");        //$NON-NLS-1$
          }

          report.append(bld.getBonusValue(5));
        }

        report.append(edifbnft.getBoniDesc(bld.getBonusType()));
        report.append("</TD>\n"); //$NON-NLS-1$

        // morale
        report.append("<TH align=center"); //$NON-NLS-1$

        if (bld.getMoraleBonus() < 0) {
          report.append(" bgcolor=#FF4020>"); //$NON-NLS-1$
          report.append(bld.getMoraleBonus());
        } else if (bld.getMoraleBonus() == 0) {
          report.append(">-----");            //$NON-NLS-1$
        } else {
          report.append(" bgcolor=#00FF00>+"); //$NON-NLS-1$
          report.append(bld.getMoraleBonus());
        }

        report.append("</TH>\n"); //$NON-NLS-1$

        // limit
        report.append("<TD align=center bgcolor=silver>"); //$NON-NLS-1$
        report.append(limit[bld.getBuildLimit()]);
        report.append("</TD>\n"); //$NON-NLS-1$

        // requirement
        report.append("<TD align=center>"); //$NON-NLS-1$
        if (bld.getSystemReq() != 8) {
          report.append(sys_req[bld.getSystemReq()]);
        } else {
          report.append("-----"); //$NON-NLS-1$
        }
        report.append("</TD>\n"); //$NON-NLS-1$

        report.append("<TD align=center>"); //$NON-NLS-1$
        if (bld.getPoliticReq() != 9) {
          report.append(pol_req[bld.getPoliticReq()]);
        } else {
          report.append("-----"); //$NON-NLS-1$
        }
        report.append("</TD>\n"); //$NON-NLS-1$

        report.append("</TR>\n\n"); //$NON-NLS-1$
      }
    }

    report.append("\n\n</table>\n\n"); //$NON-NLS-1$
    report.append("\n<br>\n<br>\n<br>\n\n"); //$NON-NLS-1$
  }
}
