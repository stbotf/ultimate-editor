package ue.gui.stbof.shp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import org.apache.commons.lang3.RandomStringUtils;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.common.GenericFile;
import ue.edit.common.InternalFile;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.shp.ScreensaverModelList;
import ue.edit.exe.trek.seg.shp.ShipModelSegment;
import ue.edit.exe.trek.seg.shp.data.ShipModelEntry;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.WDFImgReq;
import ue.edit.res.stbof.common.WDFImgReq.WDFImgError;
import ue.edit.res.stbof.files.gif.GifImage;
import ue.edit.res.stbof.files.hob.HobFile;
import ue.edit.res.stbof.files.lst.PaletteList;
import ue.edit.res.stbof.files.lst.Texture;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipCollection;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.sst.data.ShipModelPackage;
import ue.edit.res.stbof.files.tga.TargaHeader;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.exception.KeyNotFoundException;
import ue.gui.common.FileChanges;
import ue.gui.common.InfoList;
import ue.gui.common.InfoPanel;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.stbof.gfx.AniLabel.AlertType;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.UEWorker;
import ue.util.data.CollectionTools;
import ue.util.data.Pair;
import ue.util.data.StringTools;
import ue.util.file.FileFilters;
import ue.util.file.FileStore;
import ue.util.file.PathHelper;
import ue.util.func.FunctionTools;
import ue.util.func.FunctionTools.CheckedFunction;
import ue.util.img.GifColorMap;
import ue.util.stream.StreamTools;

/**
 * Used for managing ship models.
 */
public class ShipModelGUI extends MainPanel {

  private static final AlphaMode IconAlpha = AlphaMode.Opaque;
  private static final ImageIcon DefaultIcon = new ImageIcon(new BufferedImage(60, 50, BufferedImage.TYPE_INT_RGB));

  private String strDescInst = Language.getString("ShipModelGUI.0"); //$NON-NLS-1$
  private String strDescInPa = Language.getString("ShipModelGUI.1"); //$NON-NLS-1$
  private String strNotUsed = Language.getString("ShipModelGUI.20"); //$NON-NLS-1$
  private String strError = Language.getString("ShipModelGUI.16"); //$NON-NLS-1$
  private String strNumPhasers = Language.getString("ShipModelGUI.18"); //$NON-NLS-1$
  private String strScale = Language.getString("ShipModelGUI.19"); //$NON-NLS-1$
  private String strTextureDim = Language.getString("ShipModelGUI.43"); //$NON-NLS-1$

  // read
  private Stbof STBOF;
  private Trek TREK;

  // edited
  private ShipList shipList = null;
  private ShipModelSegment DATA = null;
  private ShipModelPackage SHIP_PACKAGE = null;

  // ui components
  private InfoList lstINSTALLED;
  private InfoList lstINPACK;
  private JButton btnIMPORT = new JButton(Language.getString("ShipModelGUI.7")); //$NON-NLS-1$
  private JButton btnEXPORT = new JButton(Language.getString("ShipModelGUI.10")); //$NON-NLS-1$
  private JButton btnRENAME = new JButton(Language.getString("ShipModelGUI.11")); //$NON-NLS-1$
  private JButton btnREMOVE = new JButton(Language.getString("ShipModelGUI.6")); //$NON-NLS-1$
  private JButton btnRESCALE = new JButton(Language.getString("ShipModelGUI.37")); //$NON-NLS-1$
  private JButton btnDELETE_GRAPHICS = new JButton(Language.getString("ShipModelGUI.14")); //$NON-NLS-1$
  private JButton btnOPEN = new JButton(Language.getString("ShipModelGUI.5")); //$NON-NLS-1$
  private JButton btnCREATE = new JButton(Language.getString("ShipModelGUI.3")); //$NON-NLS-1$
  private JButton btnPREVIEW = new JButton(Language.getString("ShipModelGUI.44")); //$NON-NLS-1$
  private JButton btnSET_PREVIEW = new JButton(Language.getString("ShipModelGUI.45")); //$NON-NLS-1$
  private JButton btnSET_AUTHOR = new JButton(Language.getString("ShipModelGUI.46")); //$NON-NLS-1$
  private JLabel lblINSTALLED = new JLabel();
  private JLabel lblINPACK = new JLabel();
  private JScrollPane jspINPACK;

  // data
  private ArrayList<byte[]> texture_hashes = new ArrayList<>();
  private ArrayList<byte[]> texture_pack_hashes = new ArrayList<>();
  private List<WDFImgReq>   basicImgReqs;
  private List<WDFImgReq>   buildImgReqs;
  private List<WDFImgReq>   redeployImgReqs;
  private List<WDFImgReq>   starbaseImgReqs;
  private HashSet<String>   filesChanged = new HashSet<>();
  private HashSet<String>   graphicsChanged = new HashSet<>();
  private boolean           scrollInPack = false;

  public ShipModelGUI(Stbof st, Trek tk) throws IOException {
    this.STBOF = st;
    this.TREK = tk;
    this.shipList = (ShipList) STBOF.getInternalFile(CStbofFiles.ShipListSst, true);

    if (TREK != null)
      DATA = (ShipModelSegment) TREK.getInternalFile(CTrekSegments.ShipModelSegment, true);

    setupComponents();
    placeComponents();
    setupListeners();
    determineReqImgRes();
    fillList(true);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.InstallModels;
  }

  @Override
  public String getHelpFileName() {
    return "ManageShipModels.html"; //$NON-NLS-1$
  }

  @Override
  public boolean hasChanges() {
    return !filesChanged.isEmpty()
      || !graphicsChanged.isEmpty()
      || shipList.madeChanges()
      || STBOF.isChanged(CStbofFiles.TextureLst)
      || STBOF.isChanged(CStbofFiles.PaletteLst)
      || DATA != null && DATA.madeChanges()
      || TREK != null && TREK.isChanged(CTrekSegments.ScreensaverModelList);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) throws IOException {
    // check stbof.res files
    changes.addAll(STBOF, filesChanged);
    // check files changed by stbof model routines
    changes.addAll(STBOF, STBOF.changedModelGraphicFiles(graphicsChanged));

    STBOF.checkChanged(shipList, changes);
    STBOF.checkChanged(CStbofFiles.TextureLst, changes);
    STBOF.checkChanged(CStbofFiles.PaletteLst, changes);

    // check trek.exe files
    if (TREK != null) {
      TREK.checkChanged(DATA, changes);
      TREK.checkChanged(CTrekSegments.ScreensaverModelList, changes);
    }
  }

  @Override
  public void reload() throws IOException {
    filesChanged.clear();
    graphicsChanged.clear();

    shipList = (ShipList) STBOF.getInternalFile(CStbofFiles.ShipListSst, true);

    // reload trek.exe files
    DATA = TREK != null ? (ShipModelSegment) TREK.getInternalFile(CTrekSegments.ShipModelSegment, true) : null;

    // update ui
    fillList(true);
  }

  @Override
  public void finalWarning() {
    // nothing to do
  }

  private void setupComponents() {
    lblINPACK.setText(strDescInPa.replace("%1", Language.getString("ShipModelGUI.2"))); //$NON-NLS-1$ //$NON-NLS-2$
    lblINSTALLED.setText(strDescInst.replace("%1", Language.getString("ShipModelGUI.2"))); //$NON-NLS-1$ //$NON-NLS-2$

    // make left and right lists the same width
    Dimension d = lblINSTALLED.getPreferredSize();
    Dimension d2 = lblINPACK.getPreferredSize();
    if (d.width < d2.width)
      d.width = d2.width;

    lblINSTALLED.setPreferredSize(d);
    lblINPACK.setPreferredSize(d);

    d = btnEXPORT.getPreferredSize();
    if (d.width < 120)
      d.width = 120;
    btnEXPORT.setPreferredSize(d);

    lstINPACK = new InfoList(DefaultIcon, true, 0, 1);

    JPanel pnlINPACK = new JPanel(new BorderLayout());
    pnlINPACK.add(lstINPACK, BorderLayout.NORTH);
    jspINPACK = new JScrollPane(pnlINPACK);
    JScrollBar jsp = jspINPACK.getVerticalScrollBar();
    jsp.setUnitIncrement(50);
    jspINPACK.setPreferredSize(new Dimension(200, 250));

    lstINSTALLED = new InfoList(DefaultIcon, true, 0, 1);

    Font def = UE.SETTINGS.getDefaultFont();
    lblINPACK.setFont(def);
    lblINSTALLED.setFont(def);
    btnCREATE.setFont(def);
    btnOPEN.setFont(def);
    btnREMOVE.setFont(def);
    btnIMPORT.setFont(def);
    btnEXPORT.setFont(def);
    btnPREVIEW.setFont(def);
    btnSET_PREVIEW.setFont(def);
    btnSET_AUTHOR.setFont(def);
    btnRENAME.setFont(def);
    btnRESCALE.setFont(def);
    btnDELETE_GRAPHICS.setFont(def);
    lstINPACK.setFont(def);
    lstINSTALLED.setFont(def);
  }

  private void setupListeners() {
    btnCREATE.addActionListener(new CBActionListener(evt -> {
      JFileChooser jfc = new JFileChooser(new File(
        UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));

      FileFilter filter = new FileFilter() {
        @Override
        public boolean accept(File f) {
          return f.isDirectory() || f.getName().toLowerCase().endsWith(".zip"); //$NON-NLS-1$
        }

        @Override
        public String getDescription() {
          return Language.getString("ShipModelGUI.41"); //$NON-NLS-1$
        }
      };

      jfc.setFileFilter(filter);
      int returnVal = jfc.showSaveDialog(UE.WINDOW);
      if (returnVal != JFileChooser.APPROVE_OPTION)
        return;

      try {
        File file = jfc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(file).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        if (!file.getName().endsWith(".zip")) //$NON-NLS-1$
          file = new File(workPath, file.getName() + ".zip"); //$NON-NLS-1$

        if (file.exists()) {
          String msg = Language.getString("ShipModelGUI.4")
            .replace("%1", file.getName()); //$NON-NLS-1$ //$NON-NLS-2$

          int respon = JOptionPane.showConfirmDialog(UE.WINDOW, msg,
            UE.APP_NAME, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

          if (respon != JOptionPane.YES_OPTION)
            return;

          file.delete();
        }

        SHIP_PACKAGE = new ShipModelPackage(STBOF, file);
      } finally {
        fillList(false);
      }
    }));

    btnOPEN.addActionListener(new CBActionListener(evt -> {
      JFileChooser jfc = new JFileChooser(
        new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));

      FileFilter filter = new FileFilter() {
        @Override
        public boolean accept(File f) {
          return f.isDirectory() || f.getName().toLowerCase().endsWith(".zip"); //$NON-NLS-1$
        }

        @Override
        public String getDescription() {
          return Language.getString("ShipModelGUI.41"); //$NON-NLS-1$
        }
      };

      jfc.setFileFilter(filter);
      int returnVal = jfc.showOpenDialog(UE.WINDOW);
      if (returnVal != JFileChooser.APPROVE_OPTION)
        return;

      try {
        File file = jfc.getSelectedFile();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, file.getPath());

        SHIP_PACKAGE = new ShipModelPackage(STBOF, file);
      } finally {
        fillList(false);
      }
    }));

    btnREMOVE.addActionListener(new CBActionListener(evt -> {
      try {
        if (lstINPACK.getSelectedItemIndex() >= 0) {
          SHIP_PACKAGE.clearGraphicsString(lstINPACK.getSelectedItemLine1());
          SHIP_PACKAGE.save(null);
        }
      } finally {
        // force refresh
        scrollInPack = true;
        fillList(false);
      }
    }));

    btnIMPORT.addActionListener(new CBActionListener(evt -> {
      // first check for valid selection
      String srcSel = lstINPACK.getSelectedItemLine1();
      if (srcSel == null)
        return;
      String DEST = lstINSTALLED.getSelectedItemLine1();
      if (DEST == null)
        return;

      ShipModelEntry src = SHIP_PACKAGE.getShipModelEntry(srcSel);
      ArrayList<String> removal = new ArrayList<String>();
      if (STBOF.isShipGraphicsStringUsed(DEST))
        removal.add(DEST);

      int scrIdx = DEST.indexOf("_"); //$NON-NLS-1$
      String destName = scrIdx < 0 ? DEST : DEST.substring(0, scrIdx); //$NON-NLS-1$
      ShipModelEntry dest = DATA.getEntry(destName);

      if (!src.isCompatible(dest)) {
        if (STBOF.isShipGraphicsStringUsed(dest.getGraphics())) {
          if (!removal.contains(dest.getGraphics()))
            removal.add(dest.getGraphics());
        }

        if (dest.getGraphics().length() < 3) {
          String gName = dest.getGraphics().toLowerCase() + "_"; //$NON-NLS-1$
          if (STBOF.isShipGraphicsStringUsed(gName)) {
            if (!removal.contains(dest.getGraphics() + "_"))
              removal.add(dest.getGraphics() + "_"); //$NON-NLS-1$
          }

          if (dest.getGraphics().length() < 2) {
            gName = dest.getGraphics().toLowerCase() + "__"; //$NON-NLS-1$
            if (STBOF.isShipGraphicsStringUsed(gName)) {
              if (!removal.contains(dest.getGraphics() + "__"))
                removal.add(dest.getGraphics() + "__"); //$NON-NLS-1$
            }
          }
        }
      }

      if (removal.size() == 1) {
        String msg = Language.getString("ShipModelGUI.8") //$NON-NLS-1$
          .replace("%1", removal.get(0)); //$NON-NLS-1$

        int answer = JOptionPane.showConfirmDialog(UE.WINDOW, msg,
          UE.APP_NAME, JOptionPane.OK_CANCEL_OPTION);
        if (answer != JOptionPane.OK_OPTION)
          return;

      }
      else if (removal.size() == 2) {
        String msg = Language.getString("ShipModelGUI.9") //$NON-NLS-1$
          .replace("%1", removal.get(0)) //$NON-NLS-1$
          .replace("%2", removal.get(1)); //$NON-NLS-1$

        int answer = JOptionPane.showConfirmDialog(UE.WINDOW, msg,
          UE.APP_NAME, JOptionPane.OK_CANCEL_OPTION);
        if (answer != JOptionPane.OK_OPTION)
          return;

      }
      else if (removal.size() == 3) {
        String msg = Language.getString("ShipModelGUI.40") //$NON-NLS-1$
          .replace("%1", removal.get(0)) //$NON-NLS-1$
          .replace("%2", removal.get(1)) //$NON-NLS-1$
          .replace("%3", removal.get(2)); //$NON-NLS-1$

        int answer = JOptionPane.showConfirmDialog(UE.WINDOW, msg,
        UE.APP_NAME, JOptionPane.OK_CANCEL_OPTION);
        if (answer != JOptionPane.OK_OPTION)
          return;
      }

      try {
        if (removal.size() == 1) {
          graphicsChanged.add(removal.get(0));
          if (!STBOF.removeModelGraphicFiles(removal.toArray(new String[1])))
            return;
        }
        else if (removal.size() == 2) {
          graphicsChanged.add(removal.get(0));
          graphicsChanged.add(removal.get(1));
          if (!STBOF.removeModelGraphicFiles(removal.toArray(new String[2])))
            return;
        }
        else if (removal.size() == 3) {
          graphicsChanged.add(removal.get(0));
          graphicsChanged.add(removal.get(1));
          graphicsChanged.add(removal.get(2));
          if (!STBOF.removeModelGraphicFiles(removal.toArray(new String[3])))
            return;
        }

        ImportTask tsk = new ImportTask(
          lstINPACK.getSelectedItemLine1(),
          lstINSTALLED.getSelectedItemLine1());
        GuiTools.runUEWorker(tsk);
      }
      finally {
        // force refresh
        fillList(true);
      }
    }));

    btnEXPORT.addActionListener(new CBActionListener(evt -> {
      // first check for valid selection
      String sel = lstINSTALLED.getSelectedItemLine1();
      if (sel == null)
        return;

      if(!(GuiTools.runUEWorker(new ExportTask(sel))))
        return;

      // save to active package
      // discards cache on success
      SHIP_PACKAGE.save(null);
      scrollInPack = true;

      // reload saved package
      fillList(false);
      lstINPACK.setSelectedItemIndex(lstINPACK.getNumberOfItems() - 1);
    }));

    btnPREVIEW.addActionListener(new CBActionListener(evt -> {
      if (SHIP_PACKAGE == null || lstINPACK.getSelectedItemIndex() < 0)
        return;

      String prefix = lstINPACK.getSelectedItemLine1();
      JFrame frmPrev = new JFrame();
      frmPrev.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      frmPrev.setMaximumSize(new Dimension(800, 600));
      frmPrev.setMinimumSize(new Dimension(400, 300));
      frmPrev.setLayout(new BorderLayout());
      frmPrev.setIconImage(UE.ICON);
      frmPrev.setTitle(prefix);

      try {
        String shipClassName = SHIP_PACKAGE.getClassByPrefix(prefix);
        if (shipClassName != null)
          frmPrev.setTitle(shipClassName);

        String tgaName = prefix + ".jpg"; //$NON-NLS-1$
        ImageIcon icon = SHIP_PACKAGE.getImageIcon(tgaName, IconAlpha);
        frmPrev.add(new JLabel(icon), BorderLayout.CENTER);
      } catch (Exception x) {
        JLabel lbl = new JLabel(x.getLocalizedMessage());
        lbl.setHorizontalAlignment(SwingConstants.CENTER);
        lbl.setHorizontalTextPosition(SwingConstants.CENTER);
        frmPrev.add(lbl, BorderLayout.CENTER);
      }

      try {
        String tgaName = "i_" + prefix + "30.tga"; //$NON-NLS-1$ //$NON-NLS-2$
        TargaImage icon = SHIP_PACKAGE.getTargaImage(tgaName);
        frmPrev.setIconImage(icon.getThumbnail(50, false, IconAlpha));
      } catch (Exception ex) {
        ex.printStackTrace();
      }

      frmPrev.pack();
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension size = frmPrev.getPreferredSize();
      frmPrev.setLocation(new Point(
        (int) (screenSize.getWidth() - size.getWidth()) / 2,
        (int) (screenSize.getHeight() - size.getHeight()) / 2
      ));
      frmPrev.setVisible(true);
    }));

    btnSET_PREVIEW.addActionListener(new CBActionListener(evt -> {
      if (SHIP_PACKAGE == null || lstINPACK.getSelectedItemIndex() < 0)
        return;

      JFileChooser jfc = new JFileChooser(
        new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));

      FileFilter filter = new FileFilter() {
        @Override
        public boolean accept(File f) {
          if (f.isDirectory())
            return true;

          String name = f.getName();
          name = name.toLowerCase();
          return name.endsWith(".jpg"); //$NON-NLS-1$
        }

        @Override
        public String getDescription() {
          return "*.jpg"; //$NON-NLS-1$
        }
      };

      jfc.setFileFilter(filter);
      int returnVal = jfc.showOpenDialog(UE.WINDOW);
      if (returnVal != JFileChooser.APPROVE_OPTION)
        return;

      File file = jfc.getSelectedFile();
      UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, file.getPath());

      byte[] b;
      try (FileInputStream fis = new FileInputStream(file)) {
        b = StreamTools.readAllBytes(fis);
      }

      String imgName = lstINPACK.getSelectedItemLine1().toLowerCase() + ".jpg"; //$NON-NLS-1$
      GenericFile gen = new GenericFile(imgName, b);

      SHIP_PACKAGE.addFile(gen);
      btnPREVIEW.setEnabled(true);
      SHIP_PACKAGE.save(null);
    }));

    btnSET_AUTHOR.addActionListener(new CBActionListener(evt -> {
      if (SHIP_PACKAGE == null || lstINPACK.getSelectedItemIndex() < 0)
        return;

      try {
        String prefix = lstINPACK.getSelectedItemLine1();
        String str = SHIP_PACKAGE.getAuthorByPrefix(prefix);
        if (str == null)
          str = ""; //$NON-NLS-1$

        Object obj = JOptionPane.showInputDialog(UE.WINDOW,
          Language.getString("ShipModelGUI.47"), str); //$NON-NLS-1$

        if (obj == null)
          return;

        str = (String) obj;
        if (str.isEmpty())
          return;

        SHIP_PACKAGE.setAuthorForPrefix(prefix, str);
        SHIP_PACKAGE.save(null);
      } finally {
        // force refresh
        fillList(false);
      }
    }));

    btnRENAME.addActionListener(new CBActionListener(evt -> {
      if (lstINSTALLED.getSelectedItemIndex() < 0)
        return;

      try {
        String g = lstINSTALLED.getSelectedItemLine1();

        // get input
        String r = openRenameDialog(g);
        if (r == null)
          return;
        if (!isValidName(r))
          return;

        r = r.toUpperCase();

        if (!g.equals(r)) {
          // check for removal of orphans
          int dif = r.length() - g.length();

          if (dif == 1) {
            String msg = Language.getString("ShipModelGUI.8"); //$NON-NLS-1$
            String h = g + (g.length() < 2 ? "__" : "_"); //$NON-NLS-1$ //$NON-NLS-2$

            msg = msg.replace("%1", h); //$NON-NLS-1$
            int answer = JOptionPane.showConfirmDialog(UE.WINDOW, msg,
              UE.APP_NAME, JOptionPane.OK_CANCEL_OPTION);
            if (answer != JOptionPane.OK_OPTION)
              return;

            graphicsChanged.add(h);
            if (!STBOF.removeModelGraphicFiles(new String[]{h}))
              return;
          } else if (dif == 2) {
            String h1 = g + "_"; //$NON-NLS-1$
            String h2 = g + "__"; //$NON-NLS-1$
            String msg = Language.getString("ShipModelGUI.9") //$NON-NLS-1$
              .replace("%1", h1) //$NON-NLS-1$
              .replace("%2", h2); //$NON-NLS-1$

            int answer = JOptionPane.showConfirmDialog(UE.WINDOW, msg,
              UE.APP_NAME, JOptionPane.OK_CANCEL_OPTION);
            if (answer != JOptionPane.OK_OPTION)
              return;

            graphicsChanged.add(h1);
            graphicsChanged.add(h2);
            if (!STBOF.removeModelGraphicFiles(new String[]{h1, h2}))
              return;
          }

          // rename
          String t = r;
          ScreensaverModelList sml = (ScreensaverModelList) TREK
            .getInternalFile(CTrekSegments.ScreensaverModelList, true);

          while (g.length() < 4 && t.length() < 4) {
            STBOF.renameModelGraphicFiles(g, t);
            graphicsChanged.add(g);
            graphicsChanged.add(t);

            if (sml.remove(g.toLowerCase() + ".hob")) //$NON-NLS-1$
              sml.add(t.toLowerCase() + ".hob"); //$NON-NLS-1$

            for (ShipDefinition shipDef : shipList.listShips()) {
              if (shipDef.getGraphics().equalsIgnoreCase(g))
                shipDef.setGraphics(t.toLowerCase());
            }

            g = g + "_"; //$NON-NLS-1$
            t = t + "_"; //$NON-NLS-1$
          }

          ShipModelEntry entry = DATA.getEntry(lstINSTALLED.getSelectedItemLine1());
          entry.setGraphics(r);
        }
      } finally {
        // force refresh
        fillList(true);
      }
    }));

    btnRESCALE.addActionListener(new CBActionListener(evt -> {
      if (lstINSTALLED.getSelectedItemIndex() < 0)
        return;

      try {
        String g = lstINSTALLED.getSelectedItemLine1();
        if (g.contains("_")) //$NON-NLS-1$
          return;

        ShipModelEntry entry = DATA.getEntry(g);

        // get input
        String msg = Language.getString("ShipModelGUI.39").replace("%1", g); //$NON-NLS-1$ //$NON-NLS-2$
        String scale = Integer.toString(Math.round(entry.getScale() * 100));
        scale = JOptionPane.showInputDialog(UE.WINDOW, msg, scale);

        if (scale == null)
          return;

        float s = Integer.parseInt(scale) / (float) 100;
        entry.setScale(s);
      } finally {
        // force refresh
        fillList(true);
      }
    }));

    btnDELETE_GRAPHICS.addActionListener(new CBActionListener(evt -> {
      if (lstINSTALLED.getSelectedItemIndex() < 0)
        return;

      try {
        String msg = Language.getString("ShipModelGUI.15"); //$NON-NLS-1$
        String g = lstINSTALLED.getSelectedItemLine1();
        msg = msg.replace("%1", g); //$NON-NLS-1$

        int answer = JOptionPane.showConfirmDialog(UE.WINDOW, msg,
          UE.APP_NAME, JOptionPane.YES_NO_OPTION);
        if (answer != JOptionPane.YES_OPTION)
          return;

        graphicsChanged.add(g);

        if (!STBOF.removeModelGraphicFiles(new String[]{g}))
          return;
      } finally {
        // force refresh
        fillList(true);
      }
    }));

    lstINPACK.addListSelectionListener(new CBListSelectionListener(evt -> {
      if ((lstINPACK.getSelectedItemIndex() < 0)
        || (SHIP_PACKAGE == null)) {
        btnSET_PREVIEW.setEnabled(false);
        btnSET_AUTHOR.setEnabled(false);
        return;
      }

      // setup buttons
      btnREMOVE.setEnabled(true);
      btnIMPORT.setEnabled(canImport());
      btnSET_PREVIEW.setEnabled(true);
      btnSET_AUTHOR.setEnabled(true);

      try {
        String imgName = lstINPACK.getSelectedItemLine1() + ".jpg"; //$NON-NLS-1$
        btnPREVIEW.setEnabled(SHIP_PACKAGE.containsFile(imgName));
      } catch (Exception x) {
        Dialogs.displayError(x);
        btnPREVIEW.setEnabled(false);
      }
    }));

    jspINPACK.getVerticalScrollBar().getModel().addChangeListener(new CBChangeListener(evt -> {
      if (scrollInPack) {
        int item = lstINPACK.getPreferredSize().height / lstINPACK.getNumberOfItems();
        JScrollBar jsb = jspINPACK.getVerticalScrollBar();
        int min = jsb.getValue();
        int max = jsb.getSize().height + min - item;

        int val = item * lstINPACK.getSelectedItemIndex();
        if (val < min || val > max)
          jsb.setValue(val);

        scrollInPack = false;
      }
    }));

    lstINSTALLED.addListSelectionListener(new CBListSelectionListener(evt -> {
      if ((lstINSTALLED.getSelectedItemIndex() < 0) || (DATA == null))
        return;

      btnEXPORT.setEnabled(SHIP_PACKAGE != null);
      btnIMPORT.setEnabled(canImport());
      btnDELETE_GRAPHICS.setEnabled(lstINSTALLED.getSelectedItemLine3().length() <= 0);

      String g = lstINSTALLED.getSelectedItemLine1();
      btnRENAME.setEnabled(!g.contains("_") //$NON-NLS-1$
        && (!g.startsWith("T0") || g.equals("T00"))); //$NON-NLS-1$ //$NON-NLS-2$
      btnRESCALE.setEnabled(!g.contains("_") && !btnDELETE_GRAPHICS.isEnabled()); //$NON-NLS-1$
    }));
  }

  private boolean isValidName(String name) {
    for (int i = 0; i < name.length(); ++i) {
      char c = name.charAt(i);
      if (!Character.isLetterOrDigit(c))
        return false;
    }
    return true;
  }

	private String openRenameDialog(String name) {
    String msg = Language.getString("ShipModelGUI.12").replace("%1", name); //$NON-NLS-1$ //$NON-NLS-2$
    String suggestion = name.length() > 1 ? getSuggestedGraphicsString(
      name.substring(0, name.length() - 1)) : name;
    String renamed = null;

    while (true) {
      renamed = JOptionPane.showInputDialog(UE.WINDOW, msg, suggestion);

      // cancel if empty or same
      if (StringTools.isNullOrEmpty(renamed) || StringTools.equals(name, renamed))
        break;

      // check if used
      if (!isNameUsed(renamed))
        break;

      // re-open to query the user for a new name
      Dialogs.displayError(new Exception(Language.getString("ShipModelGUI.13"))); //$NON-NLS-1$
    }

    return renamed;
  }

  private boolean isNameUsed(String renamed) {
    Set<String> used = shipList.getGraphics();
    String lcName = renamed.toUpperCase();
    return DATA.contains(lcName) || used.contains(lcName)
      || (renamed.length() < 3 && used.contains(lcName + "_")) //$NON-NLS-1$
      || (renamed.length() < 2 && used.contains(lcName + "__")); //$NON-NLS-1$
  }

	private void placeComponents() {
    // add to panel
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.SOUTH;

    JPanel pnlINSTALLED = new JPanel(new BorderLayout());
    pnlINSTALLED.add(lstINSTALLED, BorderLayout.NORTH);
    JScrollPane jspInstalled = new JScrollPane(pnlINSTALLED);
    JScrollBar jsp = jspInstalled.getVerticalScrollBar();
    jsp.setUnitIncrement(50);
    jspInstalled.setPreferredSize(new Dimension(200, 250));

    c.gridx = 0;
    c.gridy = 0;
    c.gridheight = 8;
    c.weightx = 1;
    c.weighty = 1;
    add(jspInstalled, c);

    c.insets.left = 0;
    c.insets.right = 10;
    c.gridheight = 1;
    c.gridx++;
    c.gridy = 0;
    c.weightx = 0;
    c.weighty = 0;
    add(btnIMPORT, c);
    c.insets.top = 0;
    c.gridy++;
    add(btnEXPORT, c);
    c.gridy++;
    add(btnRENAME, c);
    c.gridy++;
    add(btnRESCALE, c);
    c.gridy++;
    add(btnDELETE_GRAPHICS, c);
    c.gridy += 2;
    c.weighty = 1;
    add(Box.createVerticalGlue(), c);
    c.gridy++;
    c.weighty = 0;
    add(lblINSTALLED, c);

    c.insets.top = 5;
    c.insets.left = 5;
    c.insets.right = 5;

    c.gridx++;
    c.gridy = 0;
    c.gridheight = 8;
    c.weightx = 1;
    c.weighty = 1;
    add(jspINPACK, c);

    c.insets.left = 0;
    c.gridx++;
    c.gridy = 0;
    c.gridheight = 1;
    c.weightx = 0;
    c.weighty = 0;
    add(btnREMOVE, c);
    c.insets.top = 0;
    c.gridy++;
    add(btnOPEN, c);
    c.gridy++;
    add(btnCREATE, c);
    c.gridy++;
    add(btnPREVIEW, c);
    c.gridy++;
    add(btnSET_PREVIEW, c);
    c.gridy++;
    add(btnSET_AUTHOR, c);
    c.gridy++;
    c.weighty = 1;
    add(Box.createVerticalGlue(), c);
    c.gridy++;
    c.weighty = 0;
    add(lblINPACK, c);
  }

  // see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57619#p57619
  private void determineReqImgRes() throws IOException {
    ArrayList<String> errors = new ArrayList<>();
    basicImgReqs = WDFImgReq.listShipBaseReqs(TREK, STBOF, errors);
    buildImgReqs = WDFImgReq.listShipBuildReqs(TREK, STBOF, errors);
    redeployImgReqs = WDFImgReq.listShipRedeployReqs(TREK, STBOF, errors);
    starbaseImgReqs = WDFImgReq.listStarbaseReqs(TREK, STBOF, errors);

    if (!errors.isEmpty()) {
      String err = "Failed to load the following TGA requirements:\n\n"
        + String.join("\n", errors);
      Dialogs.displayError("TGA requirements error", err);
    }
  }

  private boolean canImport() {
    if (lstINPACK.getSelectedItemIndex() >= 0 && lstINSTALLED.getSelectedItemIndex() >= 0) {
      try {
        Set<String> used = shipList.getGraphics();
        ShipModelEntry src = SHIP_PACKAGE
            .getShipModelEntry(lstINPACK.getSelectedItemLine1());

        String DEST = lstINSTALLED.getSelectedItemLine1();
        ShipModelEntry dest;

        dest = DEST.contains("_") ? DATA.getEntry(DEST.substring(0, DEST.indexOf("_"))) //$NON-NLS-1$ //$NON-NLS-2$
          : DATA.getEntry(DEST);

        if (!src.isCompatible(dest)) {
          if ((!dest.getGraphics().equals(DEST) && used.contains(dest.getGraphics().toLowerCase())))
            return false;

          if (dest.getGraphics().length() < 3) {
            if ((!(dest.getGraphics() + "_").equals(DEST) && used //$NON-NLS-1$
              .contains(dest.getGraphics().toLowerCase() + "_"))) { //$NON-NLS-1$
              return false;
            }

            if (dest.getGraphics().length() < 2) {
              if ((!(dest.getGraphics() + "__").equals(DEST) && used //$NON-NLS-1$
                .contains(dest.getGraphics().toLowerCase() + "__"))) { //$NON-NLS-1$
                return false;
              }
            }
          }
        }
      } catch (Exception e2) {
        Dialogs.displayError(e2);
        return false;
      }

      return true;
    } else {
      return false;
    }
  }

  private String getSuggestedGraphicsString(String desired) {
    String g = desired;
    int i = 0;
    char c = 'A';

    while (DATA.contains(g.toUpperCase())
        || STBOF.isShipGraphicsStringUsed(g)
        || (g.length() < 3 && STBOF.isShipGraphicsStringUsed(g + "_")) //$NON-NLS-1$
        || (g.length() < 2 && STBOF.isShipGraphicsStringUsed(g + "__"))) //$NON-NLS-1$
    {
      if (i < 10) {
        g = g.substring(0, g.length() - 1) + i++;
      } else {
        if (c <= 'Z') {
          g = g.substring(0, g.length() - 1) + c++;
        } else {
          if (g.length() > 1) {
            i = 0;
            c = 'A';
            g = g.substring(0, g.length() - 1);
          } else {
            return STBOF.getUnusedShipGraphicsString();
          }
        }
      }
    }

    return g;
  }

  // fills the model list
  private void fillList(boolean installed) {
    btnEXPORT.setEnabled(false);
    btnIMPORT.setEnabled(false);
    btnREMOVE.setEnabled(false);
    btnPREVIEW.setEnabled(false);
    btnSET_PREVIEW.setEnabled(false);
    btnSET_AUTHOR.setEnabled(false);
    btnDELETE_GRAPHICS.setEnabled(false);
    btnRENAME.setEnabled(false);
    btnRESCALE.setEnabled(false);

    if (installed && DATA == null)
      return;

    // temporarily disable open & create while loading the data
    btnOPEN.setEnabled(false);
    btnCREATE.setEnabled(false);

    try {
      // load the new data before resetting the info list
      LoadTask task = new LoadTask(installed);
      val loadRes = GuiTools.runUEWorker(task);
      if (task.isCancelled())
        return;

      int numShips = loadRes != null ? loadRes.size() : 0;

      // determine info list to update
      InfoList infoList = installed ? lstINSTALLED : lstINPACK;
      InfoList otherList = installed ? lstINPACK : lstINSTALLED;

      // reset and hide current list
      int index = infoList.getSelectedItemIndex();
      infoList.setVisible(false);
      infoList.removeAllItems();

      // update ship model entries
      ArrayList<String> errors = new ArrayList<String>();
      int numModelsInUse = updateShipModels(installed, infoList, loadRes, errors);

      if (!errors.isEmpty()) {
        String msg = Language.getString("ShipModelGUI.53"); //$NON-NLS-1$
        msg += "\n" + String.join(", ", errors);
        Dialogs.displayError(msg);
      }

      if (installed) {
        lblINSTALLED.setText(strDescInst.replace("%1", Integer.toString(numModelsInUse))); //$NON-NLS-1$
      }
      else {
        lblINPACK.setText(strDescInPa.replace("%1", Integer.toString(numShips))); //$NON-NLS-1$
      }

      infoList.setVisible(true);
      index = Integer.min(Integer.max(index, 0), numShips - 1);
      infoList.setSelectedItemIndex(index);

      if (otherList.getSelectedItemIndex() >= 0) {
        int l = otherList.getSelectedItemIndex();
        otherList.clearSelection();
        otherList.setSelectedItemIndex(l);
      }
    }
    catch(Exception ex) {
      Dialogs.displayError(ex);
    }
    finally {
      btnOPEN.setEnabled(true);
      btnCREATE.setEnabled(true);
    }
  }

  private int updateShipModels(boolean installed, InfoList infoList, List<ShipData> loadRes, ArrayList<String> errors) {
    if (loadRes == null)
      return 0;

    int numModelsInUse = 0;
    for (int i = 0; i < loadRes.size(); i++) {
      ShipData data = loadRes.get(i);

      String line1 = data == null ? strError : data.slotName;
      if (StringTools.isNullOrEmpty(line1)) {
        line1 = Language.getString("ShipModelGUI.52") //$NON-NLS-1$
          .replace("%1", Integer.toString(i)); //$NON-NLS-1$
      }

      String line2 = data == null ? strError : !data.slotUsed ? strNotUsed
        : strNumPhasers.replace("%1", Integer.toString(data.numPhasers));
      String line3 = data == null || !data.slotUsed ? ""
        : strScale.replace("%1", String.format("%.2f", data.scale));

      if (installed && data != null && data.slotUsed)
        numModelsInUse++;

      try {
        ShipCollection shipCol = installed ? shipList : SHIP_PACKAGE;
        String tip = ""; //$NON-NLS-1$

        // author
        String strAuthor = shipCol.getAuthorByPrefix(line1);
        if (strAuthor != null)
          tip += "Author(s): %1".replace("%1", strAuthor); //$NON-NLS-1$ //$NON-NLS-2$

        // ship class
        String shipClass = getShipClass(shipCol, line1);
        if (shipClass != null)
          tip = StringTools.addLine(tip, shipClass, "<br>"); //$NON-NLS-1$;

        // texture size
        String strTextureSize = data != null && data.textureDim != null ? strTextureDim
          .replace("%1", data.textureDim.height + "x" + data.textureDim.width) : null; //$NON-NLS-1$ //$NON-NLS-2$
        if (strTextureSize != null)
          tip = StringTools.addLine(tip, strTextureSize, "<br>"); //$NON-NLS-1$

        if (shipCol.containsFile(line1 + "_a.hob")) //$NON-NLS-1$
        {
          double hobSize = shipCol.getFileSize(line1 + "_a.hob"); //$NON-NLS-1$
          if (hobSize > 0)
            tip = StringTools.addLine(tip, getSizeTip(hobSize), "<br>"); //$NON-NLS-1$
        }

        ImageIcon icon = data != null && data.image != null ? data.image : DefaultIcon;
        InfoPanel panel = new InfoPanel(new AniLabel(icon), line1, line2, line3, null);
        infoList.addItem(panel);

        if (data != null && (data.slotUsed || data.hasContent()) && data.hasErrors()) {
          val errs = data.listErrors();
          String alertText = "<html>" + String.join("<br>", errs) + "</html>";
          // wireframe image is missing for aliens
          // star bases have no 270px large scale build selection image
          AlertType type = !data.hasBasicImages || data.hasDataErrors() ? AlertType.Error
            : data.hasStarbaseImages ? AlertType.Starbase : AlertType.Alien;
          panel.showAlert(type, alertText);
        }

        if (!tip.isEmpty()) {
          tip = "<html>" + tip + "</html>"; //$NON-NLS-1$ //$NON-NLS-2$
          infoList.setTooltip(i, tip);
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        errors.add(line1);
        infoList.addItem(null, line1, line2, line3, null);
        infoList.setTooltip(i, ex.toString());
      }
    }

    return numModelsInUse;
  }

  private String getShipClass(ShipCollection collection, String l1) {
    String shipClass  = collection.getClassByPrefix(l1);
    return shipClass != null ? Language.getString("ShipModelGUI.42") //$NON-NLS-1$
      .replace("%1", shipClass) : null; //$NON-NLS-1$
  }

  private static Pair<String, Double> getSizeInfo(double hobsize) {
    String desc;
    if (hobsize > 1024) {
      hobsize = Math.round(hobsize * 10d / 1024d) / 10d;
      if (hobsize > 1024) {
        hobsize = Math.round(hobsize * 10d / 1024d) / 10d;
        desc = Language.getString("ShipModelGUI.50"); //$NON-NLS-1$
      } else {
        desc = Language.getString("ShipModelGUI.49"); //$NON-NLS-1$
      }
    } else {
      desc = Language.getString("ShipModelGUI.48"); //$NON-NLS-1$
    }
    return new Pair<String, Double>(desc, hobsize);
  }

  private static String getSizeTip(double hobsize) {
    val sizeInfo = getSizeInfo(hobsize);
    return Language.getString("ShipModelGUI.51")
      .replace("%1", Double.toString(sizeInfo.RIGHT))
      .replace("%2", sizeInfo.LEFT); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  }

  private class ShipData {
    // display properties
    public String slotName = null;
    public int numPhasers = 0;
    public float scale = 0;
    // display icon
    public ImageIcon image = null;
    // hob texture size
    public Dimension textureDim = null;

    // whether the ship slot is used by trek.exe
    public boolean slotUsed = false;

    // whether any 3D HOB model is available
    // e.g. the Calamarain doesn't have any HOB file
    public boolean hasHob = false;
    // whether any image is available
    public boolean hasImage = false;

    // whether all the basic WDF ship images are available
    public boolean hasBasicImages = false;
    // whether all the redeployment screen images are available
    @SuppressWarnings("unused")
    public boolean hasRedeployImages = false;
    // whether the starbase selection images are available, these are:
    // - the 120px starbase image in bottom map details
    // - the 120px starbase construction wireframe in bottom map details
    public boolean hasStarbaseImages = false;
    // whether the build view images is available, these are:
    // - the current build task wireframe
    // - the large scale build selection image
    @SuppressWarnings("unused")
    public boolean hasBuildImages = false;

    // load errors
    public HashMap<String, String> loadErrors = new HashMap<>();
    public HashMap<String, String> missingFiles = new HashMap<>();
    public ArrayList<String> compatErrors = new ArrayList<>();

    public boolean hasContent() {
      return hasHob || hasImage;
    }

    public boolean hasErrors() {
      return !loadErrors.isEmpty() || !missingFiles.isEmpty() || !compatErrors.isEmpty();
    }

    public boolean hasDataErrors() {
      return !loadErrors.isEmpty() || !compatErrors.isEmpty();
    }

    public List<String> listErrors() {
      val errorStream = loadErrors.values().stream();
      val missingStream = missingFiles.values().stream();
      val compatStream = compatErrors.stream();
      return Stream.concat(Stream.concat(errorStream, missingStream), compatStream)
        .collect(Collectors.toList());
    }
  }

  private class LoadTask extends UEWorker<ArrayList<ShipData>> {

    private Texture textures = null;
    private Set<String> used = null;
    private boolean installed;

    public LoadTask(boolean inst) {
      this.installed = inst;
      RESULT = new ArrayList<ShipData>();
    }

    @Override
    protected ArrayList<ShipData> work() throws Exception {
      try (val stbofAccess = STBOF.access()) {
        // load required stbof.res files
        textures = (Texture) STBOF.getInternalFile(stbofAccess, CStbofFiles.TextureLst, true);

        if (installed) {
          loadStbof(stbofAccess);
        }
        else {
          stbofAccess.close();
          loadShipPack();
        }
      }

      return RESULT;
    }

    private void loadStbof(FileStore stbofAccess) throws Exception {
      used = shipList.getGraphics();
      String msg = Language.getString("ShipModelGUI.17"); //$NON-NLS-1$

      for (int i = 0; i < DATA.getNumberOfEntries(); i++) {
        if (isCancelled())
          return;

        // load ship model entry
        final ShipModelEntry entry = DATA.getEntry(i);
        final String graphics = entry.getGraphics();
        feedMessage(msg.replace("%1", graphics)); //$NON-NLS-1$

        ShipData data = loadStbofShipData(stbofAccess, graphics, entry);
        RESULT.add(data);

        // now test hacks
        if (graphics.length() < 3) {
          data = loadStbofShipData(stbofAccess, graphics + "_", entry);
          RESULT.add(data);

          if (graphics.length() < 2) {
            data = loadStbofShipData(stbofAccess, graphics + "__", entry);
            RESULT.add(data);
          }
        }
      }

      // get texture hashes
      String tex;
      msg = "Calculating texture hash: %1";

      texture_hashes.clear();

      for (int i = 0; i < textures.getTextureNum(); i++) {
        if (isCancelled())
          return;

        if (textures.isTextureValid(i)) {
          tex = textures.get(i);

          feedMessage(msg.replace("%1", tex)); //$NON-NLS-1$

          try {
            texture_hashes.add(STBOF.getInternalFileHash(stbofAccess, tex));
          } catch (IOException ex) {
            texture_hashes.add(new byte[0]);
          }
        } else {
          texture_hashes.add(new byte[0]);
        }
      }
    }

    private void loadShipPack() throws IOException {
      String msg = Language.getString("ShipModelGUI.21"); //$NON-NLS-1$
      sendMessage(msg);

      String[] candidates = SHIP_PACKAGE.getCandidates();

      for (String gfx : candidates) {
        ShipData data = loadPackedShipData(gfx);
        RESULT.add(data);
      }

      // get texture hashes
      msg = "Calculating texture hash: %1";
      texture_pack_hashes.clear();

      for (int i = 0; i < SHIP_PACKAGE.getTextureNum(); i++) {
        if (isCancelled())
          return;

        if (SHIP_PACKAGE.isTextureValid(i)) {
          String tex = SHIP_PACKAGE.getTexture(i);

          feedMessage(msg.replace("%1", tex)); //$NON-NLS-1$

          try {
            byte[] fileHash = SHIP_PACKAGE.getInternalFileHash(tex);
            texture_pack_hashes.add(fileHash);
          }
          catch (Exception e) {
            String err = "Hash calculation failed: " + tex;
            logError(err + "\n" + e);
            SHIP_PACKAGE.removeTexture(i);
            texture_pack_hashes.add(new byte[0]);
          }
        }
        else {
          texture_pack_hashes.add(new byte[0]);
        }
      }
    }

    private ShipData loadStbofShipData(FileStore stbofAccess, String graphics, ShipModelEntry entry) {
      ShipData data = new ShipData();

      // set display properties
      data.slotName = graphics;
      data.numPhasers = entry.getNumPhasers();
      data.scale = entry.getScale() * 100;
      data.slotUsed = used != null && used.contains(graphics.toLowerCase()); //$NON-NLS-1$

      // load image
      String tgaName = "i_" + graphics + "60.tga"; //$NON-NLS-1$ //$NON-NLS-2$

      try {
        TargaImage icon = (TargaImage) STBOF.getInternalFile(stbofAccess, tgaName, false);
        data.image = new ImageIcon(icon.getImage(IconAlpha));
        data.hasImage = true;
      }
      catch (FileNotFoundException e) {
        String err = "File not found: " + tgaName;
        data.missingFiles.put(tgaName, err);
        logError(err);
      }
      catch (Exception e) {
        String err = "Failed to load: " + tgaName;
        data.loadErrors.put(tgaName, err);
        logError(err + "\n" + e);
      }

      // check whether any image is available
      if (!data.hasImage) {
        val filter = FileFilters.decorated("i_" + graphics, ".tga");
        data.hasImage = STBOF.hasFile(filter);
      }

      // load 3D HOB model
      String hobName = graphics + "_a.hob"; //$NON-NLS-1$

      try {
        HobFile hob = (HobFile) STBOF.getInternalFile(stbofAccess, hobName, false);
        data.hasHob = true;

        if (hob.numberOfTextures() > 0) {
          String t = textures.get(hob.getTextureID(0));
          GifImage gif = (GifImage) STBOF.getInternalFile(stbofAccess, t, false);
          data.textureDim = gif.getResolution();
        }
      }
      catch (FileNotFoundException e) {
        // trek.exe ship model slot is unused
        // e.g. the Calamarain doesn't have any HOB file
        String err = "File not found: " + hobName;
        logError(err);
      }
      catch (Exception e) {
        String err = "Failed to load: " + hobName;
        data.loadErrors.put(hobName, err);
        logError(err + "\n" + e);
      }

      // search missing WDF images
      checkImgReqs(data, tga -> STBOF.tryGetTargaHeader(Optional.of(stbofAccess), tga));

      return data;
    }

    private ShipData loadPackedShipData(String graphics) {
      ShipData data = new ShipData();
      data.slotName = graphics;
      data.slotUsed = true;

      ShipModelEntry sml;
      if (graphics.indexOf("_") < 0) { //$NON-NLS-1$
        sml = SHIP_PACKAGE.getShipModelEntry(graphics);
      } else {
        String modelName = graphics.substring(0, graphics.indexOf("_"));
        sml = SHIP_PACKAGE.getShipModelEntry(modelName); //$NON-NLS-1$
      }

      // phasers and scale are transferred to hack - load once and forget
      data.numPhasers = sml.getNumPhasers();
      data.scale = sml.getScale() * 100;

      String tgaName = "i_" + graphics.toLowerCase() + "60.tga"; //$NON-NLS-1$ //$NON-NLS-2$
      try {
        TargaImage icon = SHIP_PACKAGE.getTargaImage(tgaName);
        data.image = new ImageIcon(icon.getImage(IconAlpha));
        data.hasImage = true;
      }
      catch (FileNotFoundException e) {
        String err = "File not found: " + tgaName;
        data.missingFiles.put(tgaName, err);
        logError(err);
      }
      catch (Exception e) {
        String err = "Failed to load: " + tgaName;
        data.loadErrors.put(tgaName, err);
        logError(err + "\n" + e);
      }

      // check whether any image is available
      if (!data.hasImage) {
        val filter = FileFilters.decorated("i_" + graphics, ".tga");
        data.hasImage = SHIP_PACKAGE.hasFile(filter);
      }

      String hobName = graphics + "_a.hob"; //$NON-NLS-1$
      try {
        HobFile hob = SHIP_PACKAGE.getHobFile(hobName);
        data.hasHob = true;

        if (hob.numberOfTextures() > 0) {
          String t = SHIP_PACKAGE.getTexture(hob.getTextureID(0));
          GifImage gif = SHIP_PACKAGE.getGifImage(t);
          data.textureDim = gif.getResolution();
        }
      }
      catch (FileNotFoundException e) {
        // trek.exe ship model slot is unused
        String err = "File not found: " + hobName;
        data.missingFiles.put(tgaName, err);
        logError(err);
      }
      catch (Exception e) {
        String err = "Failed to load: " + hobName;
        data.loadErrors.put(tgaName, err);
        logError(err + "\n" + e);
      }

      // search missing WDF images
      checkImgReqs(data, tga -> SHIP_PACKAGE.tryGetTargaHeader(tga));

      return data;
    }
  }

  private class ExportTask extends UEWorker<Boolean> {

    private final String strTextureMissing  = Language.getString("ShipModelGUI.24"); //$NON-NLS-1$
    private final String strColorMapMissing = Language.getString("ShipModelGUI.25"); //$NON-NLS-1$

    private String GRAPHICS;

    public ExportTask(String g) {
      Objects.requireNonNull(g);
      GRAPHICS = g;
      RESULT = false;
    }

    @Override
    protected Boolean work() throws Exception {
      String graphicsPrefix = GRAPHICS;

      try {
        // load required stbof.res files
        Texture textures = (Texture) STBOF.getInternalFile(CStbofFiles.TextureLst, true);
        PaletteList palettes = (PaletteList) STBOF.getInternalFile(CStbofFiles.PaletteLst, true);

        String graphicsName = GRAPHICS;
        int index = graphicsName.indexOf("_"); //$NON-NLS-1$
        if (index >= 0)
          graphicsName = graphicsName.substring(0, index);

        // get free graphics string
        graphicsPrefix = SHIP_PACKAGE.isGraphicsStringUsed(graphicsName)
          ? SHIP_PACKAGE.getUnusedGraphicsString() : graphicsName;

        // start export ship model data
        String msg = Language.getString("ShipModelGUI.22").replace("%1", graphicsName); //$NON-NLS-1$ //$NON-NLS-2$
        sendMessage(msg);

        // load hob files
        Hashtable<Integer, Integer> tid = new Hashtable<Integer, Integer>();

        HobFile[] hob = new HobFile[3];
        char c = 'a';
        for (int i = 0; i < hob.length; i++) {
          if (isCancelled()) {
            SHIP_PACKAGE.clearGraphicsString(graphicsPrefix);
            return false;
          }

          String hobName = GRAPHICS + "_" + c + ".hob"; //$NON-NLS-1$ //$NON-NLS-2$
          InternalFile inf = STBOF.getInternalFile(hobName, false);

          // rename hob for new graphics prefix
          hobName = graphicsPrefix.toLowerCase() + "_" + c++ + ".hob"; //$NON-NLS-1$ //$NON-NLS-2$
          hob[i] = new HobFile(hobName, inf.toByteArray());

          for (int j = 0; j < hob[i].numberOfTextures(); j++) {
            tid.put(hob[i].getTextureID(j), hob[i].getTextureID(j));
          }
        }

        // export textures from hobs
        msg = Language.getString("ShipModelGUI.23"); //$NON-NLS-1$
        sendMessage(msg);

        Integer[] t = tid.values().toArray(new Integer[0]);
        StringBuilder errs = new StringBuilder();

        for (Integer i : t) {
          if (isCancelled()) {
            SHIP_PACKAGE.clearGraphicsString(graphicsPrefix);
            return false;
          }

          String textureFileName = textures.get(i);
          byte[] hash = STBOF.getInternalFileHash(textureFileName);
          int k = CollectionTools.getIndex(texture_pack_hashes, hash, Arrays::equals);

          if (k < 0) {
            InternalFile inf;

            try {
              inf = STBOF.getInternalFile(textureFileName, false);
            }
            catch (Exception e) {
              String err = strTextureMissing
                .replace("%1", textureFileName)
                .replace("%2", GRAPHICS);
              logError(err);
              // collect load errors
              errs.append(err + "\n");
              continue;
            }

            GifColorMap gcm = FunctionTools.defaultIfThrown(() -> palettes.getColorMap(i), null);
            if (gcm == null) {
              String err = strColorMapMissing
                .replace("%1", Integer.toString(i))
                .replace("%2", textureFileName)
                .replace("%3", GRAPHICS);
              logError(err);
              // collect load errors
              errs.append(err + "\n");
              continue;
            }

            tid.put(i, SHIP_PACKAGE.addTexture(inf, gcm));
          }
          else {
            tid.put(i, k);
          }
        }

        // export hob files
        msg = Language.getString("ShipModelGUI.26"); //$NON-NLS-1$
        sendMessage(msg);

        for (HobFile h : hob) {
          if (h == null)
            continue;

          for (int j = 0; j < h.numberOfTextures(); j++) {
            h.setTextureID(j, tid.get(h.getTextureID(j)));
          }

          try {
            SHIP_PACKAGE.addHobFile(h);
          } catch(Exception ex) {
            String err = "Failed to export \"" + h.getName() + "\":\n" + ex;
            logError(err);
            // collect load errors
            errs.append(err + "\n");
          }
        }

        // export gui graphic files
        msg = Language.getString("ShipModelGUI.27"); //$NON-NLS-1$
        sendMessage(msg);

        String tgaName = "i_" + GRAPHICS + "120.tga"; //$NON-NLS-1$ //$NON-NLS-2$
        TargaImage tga120 = STBOF.getTargaImage(tgaName, LoadFlags.COPY);
        tga120.setName("i_" + graphicsPrefix + "120.tga"); //$NON-NLS-1$ //$NON-NLS-2$
        SHIP_PACKAGE.addFile(tga120);

        boolean addedTga = addTga(graphicsPrefix, "w", errs); //$NON-NLS-1$

        if (!addedTga) {
          // make a copy of i_xxx120.tga
          TargaImage tga = tga120.clone();
          tga.setName("i_" + graphicsPrefix + "w.tga"); //$NON-NLS-1$ //$NON-NLS-2$
          SHIP_PACKAGE.addFile(tga);
        }

        addedTga = addTga(graphicsPrefix, "30", errs); //$NON-NLS-1$
        addedTga = addTga(graphicsPrefix, "60", errs); //$NON-NLS-1$
        addedTga = addTga(graphicsPrefix, "170", errs); //$NON-NLS-1$

        if (!addedTga) {
          // if not found use 120 to make the image
          TargaImage tga = tga120.resizedCopy(170, 142);
          tga.setName("i_" + graphicsPrefix + "170.tga"); //$NON-NLS-1$ //$NON-NLS-2$
          SHIP_PACKAGE.addFile(tga);
        }

        // export new wide screen images if available
        addedTga = addTga(graphicsPrefix, "270", errs); //$NON-NLS-1$

        ShipModelEntry orig = DATA.getEntry(graphicsName.toUpperCase());
        ShipModelEntry copy = new ShipModelEntry(orig, graphicsPrefix.toUpperCase());
        SHIP_PACKAGE.addShipModelDataEntry(copy);

        // export ship class
        try {
          String cls = shipList.getClassByPrefix(GRAPHICS);
          if (cls != null) {
            SHIP_PACKAGE.setClassForPrefix(copy.getGraphics(), cls);
          }
        }
        catch (Exception e) {
          String err = "Failed determine ship class for \"" + copy.getGraphics() + "\":\n" + e;
          logError(err);
          // collect load errors
          errs.append(err + "\n");
        }

        if (errs.length() > 0) {
          String err = errs.toString().trim();
          Dialogs.displayError("Failed ship model export", err);
        }
      } catch (Exception e) {
        try {
          SHIP_PACKAGE.clearGraphicsString(graphicsPrefix);
        } catch (IOException e1) {
          System.err.print(e1);
        }

        throw e;
      }

      return true;
    }

    private boolean addTga(String graphicsPrefix, String type, StringBuilder errs) {
      String srcName = "i_" + GRAPHICS + type + ".tga"; //$NON-NLS-1$ //$NON-NLS-2$
      String dstName = "i_" + graphicsPrefix + type + ".tga"; //$NON-NLS-1$ //$NON-NLS-2$
      return addTgaFile(srcName, dstName, errs);
    }

    private boolean addTgaFile(String tgaName, String changedName, StringBuilder errs) {
      try {
        val tga = STBOF.tryGetInternalFile(tgaName, LoadFlags.COPY);
        if (tga == null) {
          String err = "File not found: " + tgaName;
          logError(err);
          return false;
        }

        tga.setName(changedName); //$NON-NLS-1$ //$NON-NLS-2$
        SHIP_PACKAGE.addFile(tga);
        return true;
      }
      catch (Exception e) {
        String err = "Failed to load \"" + tgaName + "\":\n" + e;
        logError(err);
        // collect load errors
        errs.append(err + "\n");
      }

      return false;
    }
  }

  private class ImportTask extends UEWorker<Boolean> {

    private String GRAPHICS;
    private String DEST;

    public ImportTask(String what, String where) {
      GRAPHICS = what;
      DEST = where;
      RESULT = false;
    }

    @Override
    protected Boolean work() throws Exception {
      ArrayList<InternalFile> undo_files = new ArrayList<InternalFile>();

      try {
        ShipModelEntry src = SHIP_PACKAGE.getShipModelEntry(GRAPHICS);

        // load required stbof.res files
        Texture textures = (Texture) STBOF.getInternalFile(CStbofFiles.TextureLst, true);
        PaletteList palettes = (PaletteList) STBOF.getInternalFile(CStbofFiles.PaletteLst, true);

        // see if the two slots are compatible
        sendMessage(Language.getString("ShipModelGUI.28")); //$NON-NLS-1$

        if (STBOF.isShipGraphicsStringUsed(DEST)) {
          String err = Language.getString("ShipModelGUI.30") //$NON-NLS-1$
            .replace("%1", GRAPHICS) //$NON-NLS-1$
            .replace("%2", DEST); //$NON-NLS-1$
          throw new FileAlreadyExistsException(err);
        }

        int uScrIdx = DEST.indexOf("_"); //$NON-NLS-1$
        ShipModelEntry dest = uScrIdx > 0 ? DATA.getEntry(DEST.substring(0, uScrIdx))
          : DATA.getEntry(DEST);

        if (dest == null) {
          // bummer
          String err = Language.getString("ShipModelGUI.29") //$NON-NLS-1$
            .replace("%1", DEST); //$NON-NLS-1$
          throw new KeyNotFoundException(err);
        }

        if (!src.isCompatible(dest)) {
          if ((!dest.getGraphics().equals(DEST) && STBOF.isShipGraphicsStringUsed(dest.getGraphics()))) {
            String err = Language.getString("ShipModelGUI.30") //$NON-NLS-1$
              .replace("%1", GRAPHICS) //$NON-NLS-1$
              .replace("%2", DEST); //$NON-NLS-1$
            throw new UnsupportedOperationException(err);
          }

          if (dest.getGraphics().length() < 3) {
            if ((!(dest.getGraphics() + "_").equals(DEST) && STBOF
                .isShipGraphicsStringUsed(dest.getGraphics() + "_"))) { //$NON-NLS-1$ //$NON-NLS-2$
              String err = Language.getString("ShipModelGUI.30") //$NON-NLS-1$
                .replace("%1", GRAPHICS) //$NON-NLS-1$
                .replace("%2", DEST); //$NON-NLS-1$
              throw new UnsupportedOperationException(err);
            }

            if (dest.getGraphics().length() < 2) {
              if ((!(dest.getGraphics() + "__").equals(DEST) && STBOF
                  .isShipGraphicsStringUsed(dest.getGraphics() + "__"))) { //$NON-NLS-1$ //$NON-NLS-2$
                String err = Language.getString("ShipModelGUI.30") //$NON-NLS-1$
                  .replace("%1", GRAPHICS) //$NON-NLS-1$
                  .replace("%2", DEST); //$NON-NLS-1$
                throw new UnsupportedOperationException(err);
              }
            }
          }
        }

        // load hob files
        String msg = Language.getString("ShipModelGUI.31"); //$NON-NLS-1$
        Hashtable<Integer, Integer> textureid = new Hashtable<Integer, Integer>();
        HobFile[] hob = new HobFile[3];
        char c = 'a';

        for (int i = 0; i < hob.length; i++) {
          if (isCancelled()) {
            undo(undo_files);
            return false;
          }

          String hobGfx = GRAPHICS.toLowerCase() + "_" + c + ".hob"; //$NON-NLS-1$ //$NON-NLS-2$
          feedMessage(msg.replace("%1", hobGfx)); //$NON-NLS-1$

          String hobName = DEST.toLowerCase() + "_" + c + ".hob"; //$NON-NLS-1$ //$NON-NLS-2$
          val file = STBOF.tryGetInternalFile(hobName, false);
          if (file != null)
            undo_files.add(file);

          hob[i] = new HobFile(SHIP_PACKAGE.getHobFile(hobGfx));
          hob[i].setName(DEST.toLowerCase() + "_" + c++ + ".hob"); //$NON-NLS-1$ //$NON-NLS-2$

          for (int j = 0; j < hob[i].numberOfTextures(); j++) {
            textureid.put(hob[i].getTextureID(j), hob[i].getTextureID(j));
          }
        }

        // import textures from hobs
        msg = Language.getString("ShipModelGUI.32"); //$NON-NLS-1$
        Integer[] texIds = textureid.values().toArray(new Integer[0]);
        String gif = DEST.toLowerCase();
        c = 'a';

        while (textures.anyTextureFileBeginsWith(gif + "_")) { //$NON-NLS-1$
          gif = RandomStringUtils.randomAlphanumeric(3).toLowerCase();
        }

        StringBuilder errs = new StringBuilder();

        for (Integer texId : texIds) {
          if (isCancelled()) {
            undo(undo_files);
            return false;
          }

          try {
            String file = SHIP_PACKAGE.getTexture(texId);
            feedMessage(msg.replace("%1", file)); //$NON-NLS-1$

            // check if we already have this texture in store
            byte[] hash = SHIP_PACKAGE.getInternalFileHash(file);
            int index = CollectionTools.getIndex(texture_pack_hashes, hash, Arrays::equals);

            if (index < 0) {
              // copy & rename image
              InternalFile inf = SHIP_PACKAGE.getFile(file);
              String gifName = gif + "_" + c++ + ".gif"; //$NON-NLS-1$ //$NON-NLS-2$
              GifImage gi = new GifImage(gifName, inf.toByteArray());

              GifColorMap gcm = SHIP_PACKAGE.getColorMap(texId);

              if (gcm == null) {
                msg = Language.getString("ShipModelGUI.54") //$NON-NLS-1$
                  .replace("%1", Integer.toString(texId)); //$NON-NLS-1$
                logError(msg);
                gcm = gi.getInitialGifColorMap();
              }

              if (gcm == null)
                throw new Exception(Language.getString("ShipModelGUI.33")); //$NON-NLS-1$

              if (textures.getUnusedIndex() >= 0) {
                index = textures.getUnusedIndex();
                textures.set(index, gifName);
              }
              else {
                index = textures.add(gifName);
              }

              textureid.put(texId, index);
              palettes.setColorMap(index, gcm);

              STBOF.addInternalFile(gi);
              filesChanged.add(gifName);
            }
            else {
              textureid.put(texId, index);
            }
          }
          catch (Exception e) {
            String err = "Failed to load model %1 texture id %2:\n"
              .replace("%1", "\"" + GRAPHICS + "\"")
              .replace("%2", Integer.toString(texId));
            err += e;
            logError(err);
            // collect load errors
            errs.append(err + "\n");
          }
        }

        // import hob files
        msg = Language.getString("ShipModelGUI.34"); //$NON-NLS-1$
        sendMessage(msg);

        for (HobFile h : hob) {
          if (h == null)
            continue;

          for (int j = 0; j < h.numberOfTextures(); j++) {
            h.setTextureID(j, textureid.get(h.getTextureID(j)));
          }

          ByteArrayOutputStream baos = new ByteArrayOutputStream();
          h.save(baos);
          STBOF.addInternalFile(new GenericFile(h.getName(), baos.toByteArray()));
          filesChanged.add(h.getName());
        }

        // import gui graphic files
        msg = Language.getString("ShipModelGUI.35"); //$NON-NLS-1$
        sendMessage(msg);

        addTga("120", undo_files); //$NON-NLS-1$
        addTga("w", undo_files); //$NON-NLS-1$
        addTga("30", undo_files); //$NON-NLS-1$
        addTga("60", undo_files); //$NON-NLS-1$
        addTga("170", undo_files); //$NON-NLS-1$

        // import new wide screen images if available
        addTga("270", undo_files); //$NON-NLS-1$

        ScreensaverModelList sml = (ScreensaverModelList) TREK
          .getInternalFile(CTrekSegments.ScreensaverModelList, true);
        if (sml.getNumberofEntries() <= sml.getMax()) {
          sml.remove(GRAPHICS.toLowerCase() + ".hob"); //$NON-NLS-1$

          if (sml.getNumberofEntries() < sml.getMax())
            sml.add(DEST.toLowerCase() + ".hob"); //$NON-NLS-1$
        }

        // set model data
        dest.copyDataFrom(src);
        DATA.updateEntry(dest);

        // clean palette.lst
        palettes.trim(textures);

        if (palettes.getPalettesNum() > 127) {
          msg = Language.getString("ShipModelGUI.36"); //$NON-NLS-1$
          sendMessage(msg);
          return true;
        }

        if (errs.length() > 0) {
          String err = errs.toString().trim();
          Dialogs.displayError("Failed ship model import", err);
        }
      } catch (Exception e) {
        undo(undo_files);
        throw e;
      }

      return true;
    }

    private void addTga(String type, ArrayList<InternalFile> undo_files)
        throws IOException {
      String srcName = "i_" + GRAPHICS + type + ".tga"; //$NON-NLS-1$ //$NON-NLS-2$
      String dstName = "i_" + DEST + type + ".tga"; //$NON-NLS-1$ //$NON-NLS-2$
      addTgaFile(srcName, dstName, undo_files);
    }

    private void addTgaFile(String srcName, String dstName, ArrayList<InternalFile> undo_files)
        throws IOException {
      val tga = SHIP_PACKAGE.tryGetTargaImage(srcName);

      if (tga.isPresent()) {
        val cpy = new TargaImage(dstName, tga.get().toByteArray());
        if (STBOF.hasInternalFile(dstName))
          undo_files.add(STBOF.getInternalFile(dstName, false));

        STBOF.addInternalFile(cpy);
        filesChanged.add(dstName);
      }
    }

    private void undo(ArrayList<InternalFile> undo_files) {
      try {
        for (int i = 0; i < undo_files.size(); i++)
          STBOF.addInternalFile(undo_files.get(i));
      } catch (Exception ex) {
        Dialogs.displayError(ex);
      }
    }
  }

  private void checkImgReqs(ShipData data, CheckedFunction<String, Optional<TargaHeader>, IOException> loadTga) {
    if (data.hasContent()) {
      Consumer<WDFImgError> cbErr = (err) -> {
        switch (err.getErrorType()) {
          case Missing:
            data.missingFiles.put(err.getImageName(), err.getErrorMessage());
            break;
          case Format:
          case Size:
            data.compatErrors.add(err.getErrorMessage());
            break;
          case LoadError:
          default:
            data.loadErrors.put(err.getImageName(), err.getErrorMessage());
            break;
        }
      };

      data.hasBasicImages = WDFImgReq.checkImageReqs(data.slotName, basicImgReqs, loadTga, cbErr);
      data.hasRedeployImages = WDFImgReq.checkImageReqs(data.slotName, redeployImgReqs, loadTga, cbErr);
      data.hasBuildImages = WDFImgReq.checkImageReqs(data.slotName, buildImgReqs, loadTga, cbErr);
      data.hasStarbaseImages = WDFImgReq.checkImageReqs(data.slotName, starbaseImgReqs, loadTga, cbErr);
    }
  }

}
