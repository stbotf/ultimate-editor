package ue.gui.stbof.shp;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.Timer;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.util.data.ID;

public class ShipMultiGUI extends MainPanel {

  // read
  private Stbof stbof;
  private RaceRst RACE;

  // edited
  private ShipList SHIP;

  // ui components
  private JTextField[] txtMUL = new JTextField[33];
  private JButton btnMUL = new JButton(Language.getString("ShipMultiGUI.0")); //$NON-NLS-1$
  private JButton btnADD = new JButton(Language.getString("ShipMultiGUI.1")); //$NON-NLS-1$
  private JButton btnSET = new JButton(Language.getString("ShipMultiGUI.2")); //$NON-NLS-1$
  private JButton btnSEL = new JButton(Language.getString("ShipMultiGUI.3")); //$NON-NLS-1$
  private JComboBox<ID> jcbSHIP = new JComboBox<ID>();
  private JList<ID> lstSHIP = new JList<ID>();
  private JCheckBox chkUP = new JCheckBox(Language.getString("ShipMultiGUI.4"), false); //$NON-NLS-1$
  private JCheckBox chkNEAR = new JCheckBox(Language.getString("ShipMultiGUI.5"), true); //$NON-NLS-1$
  private JCheckBox chkDOWN = new JCheckBox(Language.getString("ShipMultiGUI.6"), false); //$NON-NLS-1$

  // data
  private int SHIP_NUM = 0;
  Timer tCOOLER;

  public ShipMultiGUI(Stbof st) throws IOException {
    this.stbof = st;
    SHIP = (ShipList) st.getInternalFile(CStbofFiles.ShipListSst, true);
    RACE = (RaceRst) st.getInternalFile(CStbofFiles.RaceRst, true);

    tCOOLER = new Timer(5000, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        btnMUL.setEnabled(true);
        btnADD.setEnabled(true);
        btnSET.setEnabled(true);
      }
    });
    tCOOLER.setRepeats(false);

    setupComponents();
    placeComponents();
    setDefaults();
    addListeners();

    jcbSHIP.setSelectedIndex(0);
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();

    for (int i = 0; i < txtMUL.length; i++) {
      if (i != 5 && i != 11 && i != 19) {
        txtMUL[i] = new JTextField(10);
        txtMUL[i].setFont(def);
      }
    }

    //Rounding
    ButtonGroup bgROUND = new ButtonGroup();
    bgROUND.add(chkUP);
    bgROUND.add(chkNEAR);
    bgROUND.add(chkDOWN);

    chkUP.setFont(def);
    chkNEAR.setFont(def);
    chkDOWN.setFont(def);
    btnMUL.setFont(def);
    btnADD.setFont(def);
    btnSET.setFont(def);

    int w = btnMUL.getPreferredSize().width;
    if (w < btnADD.getPreferredSize().width)
      w = btnADD.getPreferredSize().width;
    if (w < btnSET.getPreferredSize().width)
      w = btnSET.getPreferredSize().width;

    int h = btnMUL.getPreferredSize().height;
    if (h < btnADD.getPreferredSize().height)
      h = btnADD.getPreferredSize().height;
    if (h < btnSET.getPreferredSize().height)
      h = btnSET.getPreferredSize().height;

    Dimension dim = new Dimension(w, h);
    btnMUL.setPreferredSize(dim);
    btnADD.setPreferredSize(dim);
    btnSET.setPreferredSize(dim);

    btnSEL.setFont(def);
    jcbSHIP.setFont(def);
    lstSHIP.setFont(def);
    lstSHIP.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    lstSHIP.setFixedCellWidth(50);

    // for easier lookup, list default filters first
    jcbSHIP.addItem(new ID(Language.getString("ShipMultiGUI.8"), 35)); //$NON-NLS-1$

    // next list the ship type filters
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.Scout), 37 + ShipRole.Scout));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.Destroyer), 37 + ShipRole.Destroyer));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.Cruiser), 37 + ShipRole.Cruiser));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.StrikeCruiser), 37 + ShipRole.StrikeCruiser));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.BattleShip), 37 + ShipRole.BattleShip));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.ColonyShip), 37 + ShipRole.ColonyShip));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.Outpost), 37 + ShipRole.Outpost));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.Starbase), 37 + ShipRole.Starbase));
    jcbSHIP.addItem(new ID(LexHelper.mapShipRole(ShipRole.TroopTransport), 37 + ShipRole.TroopTransport - 1));

    // afterwards list the many race filters
    for (int i = 0; i < RACE.getNumberOfEntries(); i++) {
      jcbSHIP.addItem(new ID(RACE.getName(i), i));
    }

    // finally add the monster filter
    jcbSHIP.addItem(new ID(LexHelper.mapRace(Race.Monster), Race.Monster));
  }

  private void placeComponents() {
    Font def = UE.SETTINGS.getDefaultFont();

    // left ship selection bar
    JPanel pnlLeft = new JPanel(new GridBagLayout());
    JScrollPane JSP = new JScrollPane(lstSHIP);
    JSP.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    {
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1;

      pnlLeft.add(jcbSHIP, c);
      c.insets.top = 5;
      c.weighty = 1;
      c.gridy++;
      pnlLeft.add(JSP, c);
      c.weighty = 0;
      c.gridy++;
      pnlLeft.add(btnSEL, c);
    }

    // center property list
    JPanel pnlMain = new JPanel(new GridBagLayout());
    {
      GridBagConstraints c = new GridBagConstraints();
      c.insets = new Insets(5, 5, 10, 5);
      c.fill = GridBagConstraints.BOTH;

      // top
      c.gridx = 0;
      c.gridy = 0;
      JLabel lbl = new JLabel(Language.getString("ShipMultiGUI.10")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridx++;
      pnlMain.add(chkUP);
      c.gridx++;
      pnlMain.add(chkNEAR);
      c.gridx++;
      pnlMain.add(chkDOWN);

      // top left
      c.insets.top = 0;
      c.insets.bottom = 5;
      c.gridx = 0;
      c.gridy = 2;
      lbl = new JLabel(Language.getString("ShipMultiGUI.11")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.12")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.13")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.14")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.15")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.16")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.17")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(" "); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);

      // 1st value column
      c.insets.top = 5;
      c.gridx++;
      c.gridy = 1;
      lbl = new JLabel(Language.getString("ShipMultiGUI.19")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);

      // values
      c.insets.top = 0;
      c.gridy++;
      for (int i = 0; i < 7; i++) {
        if (txtMUL[i] != null)
          pnlMain.add(txtMUL[i], c);
        c.gridy++;
      }

      // 2nd value column
      c.insets.top = 5;
      c.gridx++;
      c.gridy = 1;
      lbl = new JLabel(Language.getString("ShipMultiGUI.20")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);

      // values
      c.insets.top = 0;
      c.gridy++;
      for (int i = 7; i < 14; i++) {
        if (txtMUL[i] != null)
          pnlMain.add(txtMUL[i], c);
        c.gridy++;
      }

      // 3rd value column
      c.insets.top = 5;
      c.gridx++;
      c.gridy = 1;
      lbl = new JLabel(Language.getString("ShipMultiGUI.21")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);

      // values
      c.insets.top = 0;
      c.gridy++;
      for (int i = 14; i < 21; i++) {
        if (txtMUL[i] != null)
          pnlMain.add(txtMUL[i], c);
        c.gridy++;
      }

      // bottom left
      c.gridx = 0;
      c.gridy = 10;
      lbl = new JLabel(Language.getString("ShipMultiGUI.18")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.24")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.25")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.26")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.27")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.22")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);

      // values
      c.gridx++;
      c.gridy = 10;
      for (int i = 21; i < 27; i++) {
        pnlMain.add(txtMUL[i], c);
        c.gridy++;
      }

      // bottom right
      c.gridx = 2;
      c.gridy = 10;
      lbl = new JLabel(Language.getString("ShipMultiGUI.28")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.29")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.30")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.31")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.32")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);
      c.gridy++;
      lbl = new JLabel(Language.getString("ShipMultiGUI.23")); //$NON-NLS-1$
      lbl.setFont(def);
      pnlMain.add(lbl, c);

      // values
      c.gridx++;
      c.gridy = 10;
      for (int i = 27; i < 33; i++) {
        pnlMain.add(txtMUL[i], c);
        c.gridy++;
      }

      // bottom
      JPanel pnlBTN = new JPanel(new GridBagLayout());
      c.insets = new Insets(0, 5, 0, 5);
      c.fill = GridBagConstraints.NONE;
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 0;
      c.weighty = 0;
      pnlBTN.add(btnMUL, c);
      c.gridx++;
      pnlBTN.add(btnADD, c);
      c.gridx++;
      pnlBTN.add(btnSET, c);
      c.insets.top = 10;
      c.gridx = 0;
      c.gridy = 16;
      c.gridwidth = 4;
      pnlMain.add(pnlBTN, c);
    }

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 0;
    c.weighty = 1;

    // MAIN PANEL
    setLayout(new GridBagLayout());
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    // match left panel width with the buildings gui
    // note that the minimum width is also determined by the filter combo box
    add(Box.createHorizontalStrut(165), c);
    add(pnlLeft, c);
    c.insets.left = 5;
    c.gridx++;
    c.weightx = 1;
    add(pnlMain, c);
  }

  private void addListeners() {
    btnMUL.addActionListener(new CBActionListener(x -> onMultiply()));
    btnADD.addActionListener(new CBActionListener(x -> onAdd()));
    btnSET.addActionListener(new CBActionListener(x -> onSet()));
    btnSEL.addActionListener(new CBActionListener(x -> onSelect()));
    jcbSHIP.addActionListener(new CBActionListener(x -> fillList()));
  }

  private void onMultiply() {
    if (lstSHIP.getSelectedIndex() < 0) {
      Dialogs.displayError(new Exception(Language.getString("ShipMultiGUI.7"))); //$NON-NLS-1$
      return;
    }

    cool();
    List<ID> values = lstSHIP.getSelectedValuesList();
    ArrayList<ID> errors = new ArrayList<ID>();

    for (ID val : values) {
      int shipId = val.ID;

      for (int j = 0; j < txtMUL.length; j++) {
        JTextField tf = txtMUL[j];
        if (tf != null) {
          try {
            double mul = Double.parseDouble(tf.getText());
            setVal(j, shipId, getVal(j, shipId) * mul);
          } catch (Exception ex) {
            ex.printStackTrace();
            errors.add(val);
          }
        }
      }
    }

    if (!errors.isEmpty()) {
      String msg = Language.getString("ShipMultiGUI.34"); //$NON-NLS-1$
      msg += "\n" + errors.stream().map(x -> Integer.toString(x.ID) + ": " + x.NAME)
        .collect(Collectors.joining("\n")); //$NON-NLS-1$
      Dialogs.displayError(msg);
    }
  }

  private void onAdd() {
    if (lstSHIP.getSelectedIndex() < 0) {
      Dialogs.displayError(new Exception(Language.getString("ShipMultiGUI.7"))); //$NON-NLS-1$
      return;
    }

    cool();
    List<ID> values = lstSHIP.getSelectedValuesList();
    ArrayList<ID> errors = new ArrayList<ID>();

    for (ID val : values) {
      int shipId = val.ID;

      for (int j = 0; j < txtMUL.length; j++) {
        if (txtMUL[j] != null) {
          try {
            double mul = Double.parseDouble(txtMUL[j].getText());
            setVal(j, shipId, getVal(j, shipId) + mul);
          } catch (Exception ex) {
            ex.printStackTrace();
            errors.add(val);
          }
        }
      }
    }

    if (!errors.isEmpty()) {
      String msg = Language.getString("ShipMultiGUI.34"); //$NON-NLS-1$
      msg += "\n" + errors.stream().map(x -> Integer.toString(x.ID) + ": " + x.NAME)
        .collect(Collectors.joining("\n")); //$NON-NLS-1$
      Dialogs.displayError(msg);
    }
  }

  private void onSet() {
    if (lstSHIP.getSelectedIndex() < 0) {
      Dialogs.displayError(new Exception(Language.getString("ShipMultiGUI.7"))); //$NON-NLS-1$
      return;
    }

    cool();
    List<ID> values = lstSHIP.getSelectedValuesList();
    ArrayList<ID> errors = new ArrayList<ID>();

    for (ID val : values) {
      int shipId = val.ID;

      for (int j = 0; j < txtMUL.length; j++) {
        if (txtMUL[j] != null) {
          try {
            double mul = Double.parseDouble(txtMUL[j].getText());
            setVal(j, shipId, mul);
          } catch (Exception ex) {
            ex.printStackTrace();
            errors.add(val);
          }
        }
      }
    }

    if (!errors.isEmpty()) {
      String msg = Language.getString("ShipMultiGUI.34"); //$NON-NLS-1$
      msg += "\n" + errors.stream().map(x -> Integer.toString(x.ID) + ": " + x.NAME)
        .collect(Collectors.joining("\n")); //$NON-NLS-1$
      Dialogs.displayError(msg);
    }
  }

  private void onSelect() {
    if (lstSHIP.getSelectedIndex() < 0) {
      if (SHIP_NUM > 0) {
        int[] set = new int[SHIP_NUM];
        for (int i = 0; i < SHIP_NUM; i++) {
          set[i] = i;
        }
        lstSHIP.setSelectedIndices(set);
      }
    } else {
      lstSHIP.clearSelection();
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.ShipGroupEdit;
  }

  private void cool() {
    btnMUL.setEnabled(false);
    btnADD.setEnabled(false);
    btnSET.setEnabled(false);
    tCOOLER.start();
  }

  /*i-parameter index, j-ship index*/
  private double getVal(int i, int j) throws Exception {
    ShipDefinition shipDef = SHIP.getShipDefinition(j);
    double ret;

    switch (i) {
      case 0:
        ret = shipDef.getBeamAccuracy();
        break;
      case 1:
        ret = shipDef.getBeamNumber();
        break;
      case 2:
        ret = shipDef.getBeamMultiplier();
        break;
      case 3:
        ret = shipDef.getBeamDamage();
        break;
      case 4:
        ret = shipDef.getBeamPenetration();
        break;
      case 6:
        ret = shipDef.getMaxBeamRange();
        break;
      case 7:
        ret = shipDef.getTorpedoAccuracy();
        break;
      case 8:
        ret = shipDef.getTorpedoNumber();
        break;
      case 9:
        ret = shipDef.getTorpedoMultiplier();
        break;
      case 10:
        ret = shipDef.getTorpedoDamage();
        break;
      case 12:
        ret = shipDef.getMinTorpedoRange();
        break;
      case 13:
        ret = shipDef.getMaxTorpedoRange();
        break;
      case 14:
        ret = shipDef.getSuperRayAccuracy();
        break;
      case 15:
        ret = shipDef.getSuperRayNumber();
        break;
      case 16:
        ret = shipDef.getSuperRayMultiplier();
        break;
      case 17:
        ret = shipDef.getSuperRayDamage();
        break;
      case 18:
        ret = shipDef.getSuperRayPenetration();
        break;
      case 20:
        ret = shipDef.getMaxSuperRayRange();
        break;
      case 21:
        ret = shipDef.getFiringArc();
        break;
      case 22:
        ret = shipDef.getShieldLevel();
        break;
      case 23:
        ret = shipDef.getShieldRecharge();
        break;
      case 24:
        ret = shipDef.getShieldStrength();
        break;
      case 25:
        ret = shipDef.getHullStrength();
        break;
      case 26:
        ret = shipDef.getOffensiveStrength();
        break;
      case 27:
        ret = shipDef.getDefense();
        break;
      case 28:
        ret = shipDef.getAgility();
        break;
      case 29:
        ret = shipDef.getBuildCost();
        break;
      case 30:
        ret = shipDef.getMaintenance();
        break;
      case 31:
        ret = shipDef.getProduction();
        break;
      case 32:
        ret = shipDef.getDefensiveStrength();
        break;
      default:
        throw new Exception(Language.getString("ShipMultiGUI.33") + i); //$NON-NLS-1$
    }

    return ret;
  }

  /*i-parameter index, j-ship index, val- value*/
  private void setVal(int i, int j, double val) throws Exception {
    ShipDefinition shipDef = SHIP.getShipDefinition(j);

    switch (i) {
      case 0:
        shipDef.setBeamAccuracy(round(val));
        break;
      case 1:
        shipDef.setBeamNumber(round(val));
        break;
      case 2:
        shipDef.setBeamMultiplier(round(val));
        break;
      case 3:
        shipDef.setBeamDamage(round(val));
        break;
      case 4:
        shipDef.setBeamPenetration(round(val));
        break;
      case 6:
        shipDef.setMaxBeamRange(val);
        break;
      case 7:
        shipDef.setTorpedoAccuracy(round(val));
        break;
      case 8:
        shipDef.setTorpedoNumber(round(val));
        break;
      case 9:
        shipDef.setTorpedoMultiplier(round(val));
        break;
      case 10:
        shipDef.setTorpedoDamage(round(val));
        break;
      case 12:
        shipDef.setMinTorpedoRange(val);
        break;
      case 13:
        shipDef.setMaxTorpedoRange(val);
        break;
      case 14:
        shipDef.setSuperRayAccuracy(round(val));
        break;
      case 15:
        shipDef.setSuperRayNumber(round(val));
        break;
      case 16:
        shipDef.setSuperRayMultiplier(round(val));
        break;
      case 17:
        shipDef.setSuperRayDamage(round(val));
        break;
      case 18:
        shipDef.setSuperRayPenetration(round(val));
        break;
      case 20:
        shipDef.setMaxSuperRayRange(val);
        break;
      case 21:
        shipDef.setFiringArc(val);
        break;
      case 22:
        shipDef.setShieldLvl(round(val));
        break;
      case 23:
        shipDef.setShieldRecharge(round(val));
        break;
      case 24:
        shipDef.setShieldStrength(round(val));
        break;
      case 25:
        shipDef.setHullStrength(round(val));
        break;
      case 26:
        shipDef.setOffensiveStrength(round(val));
        break;
      case 27:
        shipDef.setDefense(round(val));
        break;
      case 28:
        shipDef.setAgility(val);
        break;
      case 29:
        shipDef.setBuildCost(round(val));
        break;
      case 30:
        shipDef.setMaintenance(round(val));
        break;
      case 31:
        shipDef.setProduction(round(val));
        break;
      case 32:
        shipDef.setDefensiveStrength(round(val));
        break;
      default:
        throw new Exception(Language.getString("ShipMultiGUI.33") + i); //$NON-NLS-1$
    }
  }

  @Override
  public boolean hasChanges() {
    return SHIP.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(SHIP, changes);
  }

  @Override
  public void reload() throws IOException {
    SHIP = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
    fillList();
    setDefaults();
  }

  private void setDefaults() {
    for (int i = 0; i < txtMUL.length; i++) {
      if (txtMUL[i] != null)
        txtMUL[i].setText("1"); //$NON-NLS-1$
    }

    chkUP.setSelected(false);
    chkNEAR.setSelected(true);
    chkDOWN.setSelected(false);
  }

  @Override
  public void finalWarning() {
  }

  //////////////////////////////////////////
  private void fillList() {
    ID filter = (ID) jcbSHIP.getSelectedItem();
    int prev = lstSHIP.getSelectedIndex();

    ID[] shipIds = SHIP.getIDs(filter.ID);
    SHIP_NUM = shipIds.length;
    lstSHIP.setListData(shipIds);

    // restore selection
    if (prev >= 0 && prev < shipIds.length)
      lstSHIP.setSelectedIndex(prev);
    else if (shipIds.length > 0)
      lstSHIP.setSelectedIndex(0);
  }

  private int round(double val) {
    double res;
    if (chkUP.isSelected())
      res = Math.ceil(val);
    if (chkNEAR.isSelected())
      res = Math.round(val);
    else
      res = Math.floor(val);

    return (int) res;
  }
}
