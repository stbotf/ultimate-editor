package ue.gui.stbof.shp;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import lombok.val;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.ShortValueSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SD_Monster;
import ue.edit.exe.trek.sd.SD_ShipNum;
import ue.edit.exe.trek.seg.emp.AlienStartingID;
import ue.edit.exe.trek.seg.emp.EmpireImgPrefixes;
import ue.edit.exe.trek.seg.shp.ScreensaverModelList;
import ue.edit.exe.trek.seg.shp.SelectPhaserHobFile;
import ue.edit.exe.trek.seg.shp.SelectTorpedoHobFile;
import ue.edit.exe.trek.seg.shp.ShipLoadLimit;
import ue.edit.exe.trek.seg.shp.SpecialShips;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.ShipRange;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.common.CStbof.ShipStealth;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Shipname;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.ShipRace;
import ue.edit.res.stbof.files.sst.ShipTech;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.tec.TecField;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.DescTools;
import ue.gui.util.component.IconButton;
import ue.gui.util.component.ImageButton;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBDocumentChangeListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.data.ValueCast;

public class ShipGUI extends MainPanel {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_ERAS = CStbof.NUM_ERAS;
  private static final String STR_UNKNOWN = "???"; //$NON-NLS-1$

  private static final double DefaultBeamModifier = 1;
  private static final double DefaultTorpModifier = 1;
  private static final double DefaultSuperModifier = 1;
  private static final double DefaultShieldModifier = 1;
  private static final double DefaultRechargeModifier = 1;
  private static final double DefaultHullModifier = 1;
  private static final double DefaultDefenseModifier = 1;
  private static final double DefaultAgilityModifier = 0;

  private final String strUPGRADE = Language.getString("ShipGUI.0"); //$NON-NLS-1$
  private final String strSP_TOTAL = Language.getString("ShipGUI.53"); //$NON-NLS-1$

  // read
  private Trek TREK; // make sure this one can be null!
  private Stbof STBOF;
  private RaceRst RACE;
  private TecField TECH;
  private Shipname SHIPNAME;

  // edited
  private ShipList shipList;
  private ShipRace SHIPRACE;
  private ShipTech SHIPTECH;
  private SpecialShips SP;
  private ScreensaverModelList screenSaver;

  // ui components
  private JPanel pnlCARD = new JPanel();
  private CardLayout layCARD = new CardLayout();
  private JComboBox<ID> jcbSHIP = new JComboBox<ID>();
  private JList<ID> lstSHIP = new JList<ID>();
  private ImageButton btnNEW = ImageButton.CreatePlusButton();
  private ImageButton btnRM = ImageButton.CreateMinusButton();
  private IconButton btnMUP = IconButton.CreateUpButton();
  private IconButton btnMDN = IconButton.CreateDownButton();
  private JButton btnPrev = new JButton("<<<"); //$NON-NLS-1$
  private JButton btnNext = new JButton(">>>"); //$NON-NLS-1$
  private IconButton btnSettings = IconButton.CreateSettingsButton();
  private IconButton btnDescTools = IconButton.CreateDescriptionButton();
  private JPanel pnlSWITCH = new JPanel(new GridBagLayout());
  private JLabel lblINDEX = new JLabel();

  // panel one
  private JPanel pnlONE = new JPanel(new GridBagLayout());
  private JTextArea txtDESC = new JTextArea();                  // ship description
  private JTextField txtNAME = new JTextField(40);              // ship name
  private JTextField txtCLASS = new JTextField(40);             // ship class
  private JComboBox<String> jcbPIC = new JComboBox<String>();   // ship's picture
  private JLabel lblPIC = new JLabel();
  private JComboBox<ID> jcbSHIP_ROLE = new JComboBox<ID>();     // ship's type
  private JComboBox<ID> jcbSHIP_NAME = new JComboBox<ID>();     // Ship name group
  private JComboBox<ID> jcbMAP_RANGE = new JComboBox<ID>();     // short,medium,long
  private JTextField txtMAP_SPEED = new JTextField(6);          // speed of ship
  private JTextField txtSCN_RANGE = new JTextField(6);          // scanner range
  private JComboBox<ID> jcbRACE = new JComboBox<ID>();
  private JTextField txtBLD_COST = new JTextField(6);
  private JTextField txtMAIN_COST = new JTextField(6);
  private JComboBox<String> jcbCLOAK = new JComboBox<String>(); // Cloak level
  private JTextField txtPROD = new JTextField(6);

  // panel two
  private JPanel pnlTWO = new JPanel(new GridBagLayout());
  private JTextField txtBEAM_ACCU = new JTextField(8);
  private JTextField txtBEAM_NUM = new JTextField(8);
  private JTextField txtBEAM_MLT = new JTextField(8);
  private JTextField txtBEAM_DAM = new JTextField(8);
  private JTextField txtBEAM_MAX = new JTextField(8);
  private JTextField txtBEAM_PEN = new JTextField(8);
  private JTextField txtTORP_ACCU = new JTextField(8);
  private JTextField txtTORP_NUM = new JTextField(8);
  private JTextField txtTORP_MLT = new JTextField(8);
  private JTextField txtTORP_DAM = new JTextField(8);
  private JTextField txtTORP_MIN = new JTextField(8);
  private JTextField txtTORP_MAX = new JTextField(8);
  private JTextField txtSUPER_ACCU = new JTextField(8);
  private JTextField txtSUPER_NUM = new JTextField(8);
  private JTextField txtSUPER_MLT = new JTextField(8);
  private JTextField txtSUPER_DAM = new JTextField(8);
  private JTextField txtSUPER_MAX = new JTextField(8);
  private JTextField txtSUPER_PEN = new JTextField(8);
  private JTextField txtFIRING_ARC = new JTextField(8);
  private JTextField txtSHLD_LVL = new JTextField(8);
  private JTextField txtSHLD_STR = new JTextField(8);
  private JTextField txtSHLD_REC = new JTextField(8);
  private JTextField txtHULL_STR = new JTextField(8);
  private JTextField txtDEFENSE = new JTextField(8);
  private JTextField txtAGILITY = new JTextField(8);
  private JTextField txtOFFENSIVE = new JTextField(8);
  private JTextField txtDEFENSIVE = new JTextField(8);
  private IconButton btnCalcOff = IconButton.CreateCalcButton();
  private IconButton btnCalcDef = IconButton.CreateCalcButton();

  // panel three
  private JPanel pnlTHR = new JPanel(new GridBagLayout());
  private JLabel lblSPECIAL = new JLabel(Language.getString("ShipGUI.41")); //$NON-NLS-1$
  private JCheckBox chkSPECIAL = new JCheckBox();
  private JCheckBox chkSCREENSAVER = new JCheckBox();
  private JComboBox<ID> cmbPHASER_HOB = new JComboBox<ID>();
  private JComboBox<ID> cmbTORPEDO_HOB = new JComboBox<ID>();
  private JCheckBox chkPHASER_HOB_MOD = new JCheckBox();
  private JCheckBox chkTORPEDO_HOB_MOD = new JCheckBox();
  private JComboBox<ID> jcbPARENT = new JComboBox<ID>();
  private JComboBox<ID> jcbRACE_OVR = new JComboBox<ID>();
  private JLabel lblPARREQ = new JLabel();
  private JLabel[] lblPRNT_TECH = new JLabel[12];
  private JTextField[] txtTEC_REQ = new JTextField[6];

  // data
  private ArrayList<ID> availableShipNameGroups;
  private HashSet<Integer> SPECIAL = new HashSet<Integer>();
  private HashSet<String> SCREENSAVER = new HashSet<String>();
  private int MAX_SCREENSAVER = 0;
  private int SELECTED = -1;
  private boolean init = false;
  private boolean selChange = false;
  private double beamModifier = DefaultBeamModifier;
  private double torpModifier = DefaultTorpModifier;
  private double superModifier = DefaultSuperModifier;
  private double shieldModifier = DefaultShieldModifier;
  private double rechargeModifier = DefaultRechargeModifier;
  private double hullModifier = DefaultHullModifier;
  private double defenseModifier = DefaultDefenseModifier;
  private double agilityModifier = DefaultAgilityModifier;
  private boolean autoCalcOffensiveStrength = false;
  private boolean autoCalcDefensiveStrength = false;

  public ShipGUI(Stbof st, Trek trek) throws IOException {
    STBOF = st;
    TREK = trek;
    RACE = (RaceRst) STBOF.getInternalFile(CStbofFiles.RaceRst, true);
    shipList = (ShipList) STBOF.getInternalFile(CStbofFiles.ShipListSst, true);
    SHIPRACE = (ShipRace) STBOF.getInternalFile(CStbofFiles.ShipRaceSst, true);
    SHIPTECH = (ShipTech) STBOF.getInternalFile(CStbofFiles.ShipTechSst, true);
    SHIPNAME = (Shipname) STBOF.getInternalFile(CStbofFiles.ShipNameBin, true);
    TECH = (TecField) STBOF.getInternalFile(CStbofFiles.TecFieldTec, true);
    if (trek != null) {
      SP = (SpecialShips) TREK.getInternalFile(CTrekSegments.SpecialShips, true);
      screenSaver = (ScreensaverModelList) TREK.getInternalFile(CTrekSegments.ScreensaverModelList, true);
    }
    availableShipNameGroups = SHIPNAME.AvailableNameGroups();

    /* SINCHRONIZE FILES */
    fixShipRace();
    sortShips();
    loadTrekExe();
    loadSettings();

    /* INIT UI */
    initPanels();
    initLayout();
    setupComponents();
    addListeners();

    /* INIT SHIP SELECTION */
    jcbSHIP.setSelectedIndex(0);
    layCARD.first(pnlCARD);
    fillList();
  }

  private void initLayout() {
    setLayout(new GridBagLayout());

    // Buttons panel
    JPanel pnlButtons = new JPanel();
    pnlButtons.setLayout(new BoxLayout(pnlButtons, BoxLayout.X_AXIS));
    {
      pnlButtons.add(btnMUP);
      pnlButtons.add(btnMDN);
      pnlButtons.add(Box.createHorizontalGlue());
      pnlButtons.add(btnNEW);
      pnlButtons.add(btnRM);
    }

    // LEFT PANEL
    JPanel pnlLEFT = new JPanel(new GridBagLayout());
    JScrollPane JSP = new JScrollPane(lstSHIP);
    JSP.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    {
      GridBagConstraints c = new GridBagConstraints();
      c.fill = GridBagConstraints.BOTH;
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1;

      pnlLEFT.add(jcbSHIP, c);
      c.insets.top = 5;
      c.weighty = 1;
      c.gridy++;
      pnlLEFT.add(JSP, c);
      c.insets.top = 0;
      c.weighty = 0;
      c.gridy++;
      pnlLEFT.add(pnlButtons, c);
    }

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridheight = 2;
    c.gridx = 0;
    c.gridy = 0;
    c.weighty = 1;

    // MAIN PANEL
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    // match left panel width with the buildings gui
    // note that the minimum width is also determined by the filter combo box
    add(Box.createHorizontalStrut(165), c);
    add(pnlLEFT, c);
    c.insets.left = 5;
    c.gridheight = 1;
    c.gridx = 1;
    c.weightx = 1;
    add(pnlCARD, c);
    c.insets.top = 5;
    c.gridy = 1;
    c.weighty = 0;
    add(pnlSWITCH, c);
  }

  private void setupComponents() {
    // ship combo
    {
      // for easier lookup, list default filters first
      jcbSHIP.addItem(new ID(Language.getString("ShipGUI.44"), 35)); //$NON-NLS-1$

      // next list all the races
      for (int i = 0; i < RACE.getNumberOfEntries(); i++) {
        ID id = new ID(RACE.getName(i), i);
        jcbSHIP.addItem(id);
        jcbRACE.addItem(id);
        jcbRACE_OVR.addItem(id);
      }

      // list monsters last
      jcbSHIP.addItem(new ID(LexHelper.mapRace(Race.Monster), Race.Monster)); //$NON-NLS-1$
      jcbRACE.addItem(new ID(LexHelper.mapRace(Race.Monster), Race.Monster)); //$NON-NLS-1$
      jcbRACE_OVR.addItem(new ID(LexHelper.mapRace(Race.Monster), Race.Monster)); //$NON-NLS-1$
    }

    Font def = UE.SETTINGS.getDefaultFont();
    jcbSHIP.setFont(def);

    // ship list
    lstSHIP.setFont(def);
    lstSHIP.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lstSHIP.setFixedCellWidth(50);

    // UP DOWN
    btnMUP.setToolTipText(Language.getString("ShipGUI.47")); //$NON-NLS-1$
    btnMDN.setToolTipText(Language.getString("ShipGUI.48")); //$NON-NLS-1$

    // ADD NEW SHIP
    btnNEW.setToolTipText(Language.getString("ShipGUI.49")); //$NON-NLS-1$
    btnNEW.setEnabled(TREK != null); // this function does not work without modifying trek.exe

    // REMOVE SHIP
    btnRM.setToolTipText(Language.getString("ShipGUI.52")); //$NON-NLS-1$
    btnRM.setEnabled(TREK != null); // this function does not work without modifying trek.exe

    btnCalcOff.setToolTipText(Language.getString("ShipGUI.59")); //$NON-NLS-1$
    btnCalcDef.setToolTipText(Language.getString("ShipGUI.60")); //$NON-NLS-1$
  }

  private void loadTrekExe() {
    if (SP != null)
      SPECIAL = SP.getShipIDs();

    if (screenSaver != null) {
      MAX_SCREENSAVER = screenSaver.getMax();
      SCREENSAVER.clear();

      for (int i = 0; i < screenSaver.getNumberofEntries(); i++) {
        String m = screenSaver.get(i);
        m = m.substring(0, m.length() - 4);

        if (shipList.getFirstIDWithPrefix(m) >= 0)
          SCREENSAVER.add(screenSaver.get(i));
      }
    }
  }

  private void loadSettings() {
    beamModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_PHASER, DefaultBeamModifier);
    torpModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_TORP, DefaultTorpModifier);
    superModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_SUPERRAY, DefaultSuperModifier);
    shieldModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_SHIELD, DefaultShieldModifier);
    rechargeModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_RECHARGE, DefaultRechargeModifier);
    hullModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_HULL, DefaultHullModifier);
    defenseModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_DEFENSE, DefaultDefenseModifier);
    agilityModifier = UE.SETTINGS.getDoubleProperty(SettingsManager.SHIP_MOD_AGILITY, DefaultAgilityModifier);
    autoCalcOffensiveStrength = UE.SETTINGS.getBooleanProperty(SettingsManager.SHIP_AUTO_OFFENSIVE);
    autoCalcDefensiveStrength = UE.SETTINGS.getBooleanProperty(SettingsManager.SHIP_AUTO_DEFENSIVE);
  }

  private void fixShipRace() {
    for (ShipDefinition shipDef : shipList.listShips()) {
      int race = SHIPTECH.getRace(shipDef.index());
      shipDef.setRace((short) race, false);
    }
  }

  private void sortShips() {
    try {
      int[] id2 = shipList.allignIDs();
      SHIPTECH.reOrder(id2);
      int[] id = shipList.getStartingIDs();
      SHIPRACE.setShipStarts(id);
    } catch (Exception e) {
      Dialogs.displayError(e);
    }
  }

  private void initPanels() throws IOException {
    // SWITCH PANEL
    {
      Font def = UE.SETTINGS.getDefaultFont();
      lblINDEX.setFont(def);
      GridBagConstraints c = new GridBagConstraints();

      // left spacing
      c.fill = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.WEST;
      c.insets = new Insets(0, 5, 0, 5);
      c.gridx = 0;
      c.gridy = 0;
      c.weightx = 1.0;
      // add strut place holder to equal weightx distribution
      pnlSWITCH.add(btnSettings, c);
      pnlSWITCH.add(Box.createHorizontalStrut(40), c);

      // navigation
      c.gridx++;
      c.weightx = 0;
      pnlSWITCH.add(btnPrev, c);
      c.anchor = GridBagConstraints.CENTER;
      c.gridx++;
      pnlSWITCH.add(lblINDEX, c);
      pnlSWITCH.add(Box.createHorizontalStrut(80), c);
      c.anchor = GridBagConstraints.EAST;
      c.gridx++;
      pnlSWITCH.add(btnNext, c);

      // right spacing & tools
      c.gridx++;
      c.weightx = 1.0;
      // add strut place holder to equal weightx distribution
      pnlSWITCH.add(Box.createHorizontalStrut(40), c);
      pnlSWITCH.add(btnDescTools, c);
    }

    // create panels
    initPanel1();
    initPanel2();
    initPanel3();

    /* SETUP CARD PANEL */
    pnlCARD.setLayout(layCARD);
    pnlCARD.add(pnlONE, "1"); //$NON-NLS-1$
    pnlCARD.add(pnlTWO, "2"); //$NON-NLS-1$
    pnlCARD.add(pnlTHR, "3"); //$NON-NLS-1$
    pnlCARD.setBorder(new LineBorder(Color.BLACK));
  }

  private void initPanel1() throws IOException {
    // setup
    txtDESC.setLineWrap(true);
    txtDESC.setWrapStyleWord(true);

    Font def = UE.SETTINGS.getDefaultFont();
    txtDESC.setFont(def);
    txtNAME.setFont(def);
    txtCLASS.setFont(def);
    jcbPIC.setFont(def);

    Collection<String> fileNames = STBOF.getFileNames(new java.io.FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.toLowerCase().startsWith("i_")      //$NON-NLS-1$
            && name.toLowerCase().endsWith("120.tga");  //$NON-NLS-1$
      }
    });

    for (String fileName : fileNames)
      jcbPIC.addItem(fileName);

    jcbSHIP_ROLE.setFont(def);
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.Scout), ShipRole.Scout));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.Destroyer), ShipRole.Destroyer));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.Cruiser), ShipRole.Cruiser));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.StrikeCruiser), ShipRole.StrikeCruiser));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.BattleShip), ShipRole.BattleShip));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.ColonyShip), ShipRole.ColonyShip));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.Outpost), ShipRole.Outpost));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.Starbase), ShipRole.Starbase));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.Monster), ShipRole.Monster));
    jcbSHIP_ROLE.addItem(new ID(LexHelper.mapShipRole(ShipRole.TroopTransport), ShipRole.TroopTransport));

    jcbSHIP_NAME.setFont(def);
    for (ID group : availableShipNameGroups)
      jcbSHIP_NAME.addItem(group);

    jcbMAP_RANGE.setFont(def);
    jcbMAP_RANGE.addItem(new ID(LexHelper.mapShipRange(ShipRange.None), ShipRange.None)); //$NON-NLS-1$
    jcbMAP_RANGE.addItem(new ID(LexHelper.mapShipRange(ShipRange.Short), ShipRange.Short));
    jcbMAP_RANGE.addItem(new ID(LexHelper.mapShipRange(ShipRange.Medium), ShipRange.Medium));
    jcbMAP_RANGE.addItem(new ID(LexHelper.mapShipRange(ShipRange.Long), ShipRange.Long));
    jcbMAP_RANGE.addItem(new ID(LexHelper.mapShipRange(ShipRange.Towable), ShipRange.Towable)); //$NON-NLS-1$

    txtMAP_SPEED.setFont(def);
    txtSCN_RANGE.setFont(def);
    jcbRACE.setFont(def);

    txtBLD_COST.setFont(def);
    txtMAIN_COST.setFont(def);
    jcbCLOAK.setFont(def);
    txtPROD.setFont(def);

    for (int i = ShipStealth.None; i <= ShipStealth.Cloak_III; ++i)
      jcbCLOAK.addItem(LexHelper.mapShipStealth(i));

    // place
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 0, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.gridx = 0;
    c.gridy = 0;

    JLabel lbl = new JLabel(Language.getString("ShipGUI.50")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE.add(lbl, c);
    c.gridx = 1;
    pnlONE.add(txtNAME, c);

    c.gridx = 0;
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.10")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE.add(lbl, c);
    c.gridx = 1;
    pnlONE.add(txtCLASS, c);

    c.gridx = 0;
    c.gridy++;
    pnlONE.add(lblPIC, c);
    c.gridheight = 2;
    c.gridx = 1;
    JScrollPane jpdesc = new JScrollPane(txtDESC);
    pnlONE.add(jpdesc, c);

    c.insets.bottom = 5;
    c.gridx = 0;
    c.gridy++;
    c.gridheight = 1;
    pnlONE.add(jcbPIC, c);

    ////
    JPanel pnlONE_1 = new JPanel(new GridBagLayout());
    c.gridwidth = 1;
    c.gridheight = 1;
    c.gridx = 0;
    c.gridy = 0;
    lbl = new JLabel(Language.getString("ShipGUI.11")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.12")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.13")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.14")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.15")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);

    c.gridy = 0;
    c.gridx = 1;
    pnlONE_1.add(jcbSHIP_ROLE, c);
    c.gridy++;
    pnlONE_1.add(jcbSHIP_NAME, c);
    c.gridy++;
    pnlONE_1.add(jcbMAP_RANGE, c);
    c.gridy++;
    pnlONE_1.add(txtMAP_SPEED, c);
    c.gridy++;
    pnlONE_1.add(txtSCN_RANGE, c);

    ////////////////////////////////////
    c.gridx = 3;
    c.gridy = 0;
    lbl = new JLabel(Language.getString("ShipGUI.16")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.17")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.18")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.19")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.20")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlONE_1.add(lbl, c);

    c.gridy = 0;
    c.gridx = 4;
    pnlONE_1.add(jcbRACE, c);
    c.gridy = 1;
    pnlONE_1.add(txtBLD_COST, c);
    c.gridy++;
    pnlONE_1.add(txtMAIN_COST, c);
    c.gridy++;
    pnlONE_1.add(jcbCLOAK, c);
    c.gridy++;
    pnlONE_1.add(txtPROD, c);

    ////////////////////////////////////
    c.gridy = 4;
    c.gridx = 0;
    c.gridheight = 1;
    c.gridwidth = 2;
    pnlONE.add(pnlONE_1, c);
  }

  private void initPanel2() throws IOException {
    // setup
    if (TREK == null) {
      chkPHASER_HOB_MOD.setSelected(false);
      chkPHASER_HOB_MOD.setEnabled(false);
      cmbPHASER_HOB.setEnabled(false);
      chkTORPEDO_HOB_MOD.setSelected(false);
      chkTORPEDO_HOB_MOD.setEnabled(false);
      cmbTORPEDO_HOB.setEnabled(false);
    } else {
      try {
        SelectPhaserHobFile sphf = (SelectPhaserHobFile) TREK
          .getInternalFile(CTrekSegments.SelectPhaserHobFile, true);

        chkPHASER_HOB_MOD.setSelected(sphf.isPatched());
        cmbPHASER_HOB.setEnabled(sphf.isPatched());

        ArrayList<String> files = STBOF.getFileNames(new FilenameFilter() {
          @Override
          public boolean accept(File dir, String name) {
            return (name.length() == 11 && name.toLowerCase().endsWith("phaser.hob"));
          }
        });

        for (int i = 0; i < files.size(); i++) {
          cmbPHASER_HOB.addItem(new ID(files.get(i), files.get(i).charAt(0)));
        }
      } catch (FileNotFoundException ex) {
        chkPHASER_HOB_MOD.setSelected(false);
        chkPHASER_HOB_MOD.setEnabled(false);
        cmbPHASER_HOB.setEnabled(false);
        Dialogs.displayError(ex);
      }

      try {
        SelectTorpedoHobFile sthf = (SelectTorpedoHobFile) TREK
          .getInternalFile(CTrekSegments.SelectTorpedoHobFile, true);

        chkTORPEDO_HOB_MOD.setSelected(sthf.isPatched());
        cmbTORPEDO_HOB.setEnabled(sthf.isPatched());

        ArrayList<String> files = STBOF.getFileNames(new FilenameFilter() {
          @Override
          public boolean accept(File dir, String name) {
            return (name.length() == 11 && name.toLowerCase().endsWith("photon.hob"));
          }
        });

        for (int i = 0; i < files.size(); i++) {
          cmbTORPEDO_HOB.addItem(new ID(files.get(i), files.get(i).charAt(0)));
        }
      } catch (FileNotFoundException ex) {
        chkTORPEDO_HOB_MOD.setSelected(false);
        chkTORPEDO_HOB_MOD.setEnabled(false);
        cmbTORPEDO_HOB.setEnabled(false);
        Dialogs.displayError(ex);
      }
    }

    // place
    Font def = UE.SETTINGS.getDefaultFont();
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(0, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridwidth = 1;
    c.gridheight = 1;
    c.gridx = 0;
    c.gridy = 1;
    JLabel lbl = new JLabel(Language.getString("ShipGUI.21")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.22")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.23")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.24")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.25")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.26")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.27")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.28")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(" "); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.29")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.30")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.31")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.32")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);

    ////////////////////////////////////////
    c.insets.top = 5;
    c.gridx = 1;
    c.gridy = 0;
    lbl = new JLabel(Language.getString("ShipGUI.1")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.insets.top = 0;
    c.gridy++;
    pnlTWO.add(txtBEAM_ACCU, c);
    c.gridy++;
    pnlTWO.add(txtBEAM_NUM, c);
    c.gridy++;
    pnlTWO.add(txtBEAM_MLT, c);
    c.gridy++;
    pnlTWO.add(txtBEAM_DAM, c);
    c.gridy++;
    pnlTWO.add(txtBEAM_PEN, c);
    c.gridy += 2;
    pnlTWO.add(txtBEAM_MAX, c);
    c.gridy++;
    c.gridwidth = 4;
    pnlTWO.add(txtFIRING_ARC, c);
    c.gridy += 2;
    c.gridwidth = 1;
    pnlTWO.add(txtSHLD_LVL, c);
    c.gridy++;
    pnlTWO.add(txtSHLD_REC, c);
    c.gridy++;
    pnlTWO.add(txtSHLD_STR, c);
    c.gridy++;
    pnlTWO.add(txtHULL_STR, c);

    //////////////////////////////////////
    c.insets.top = 5;
    c.gridx = 3;
    c.gridy = 0;
    lbl = new JLabel(Language.getString("ShipGUI.2")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.insets.top = 0;
    c.gridy++;
    pnlTWO.add(txtTORP_ACCU, c);
    c.gridy++;
    pnlTWO.add(txtTORP_NUM, c);
    c.gridy++;
    pnlTWO.add(txtTORP_MLT, c);
    c.gridy++;
    pnlTWO.add(txtTORP_DAM, c);
    c.gridy += 2;
    pnlTWO.add(txtTORP_MIN, c);
    c.gridy++;
    pnlTWO.add(txtTORP_MAX, c);

    c.gridy += 3;
    lbl = new JLabel(Language.getString("ShipGUI.38")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.39")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.57")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.58")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);

    /////////////////////////////////////////////
    c.insets.top = 5;
    c.gridx = 4;
    c.gridy = 0;
    lbl = new JLabel(Language.getString("ShipGUI.3")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTWO.add(lbl, c);
    c.insets.top = 0;
    c.gridy++;
    pnlTWO.add(txtSUPER_ACCU, c);
    c.gridy++;
    pnlTWO.add(txtSUPER_NUM, c);
    c.gridy++;
    pnlTWO.add(txtSUPER_MLT, c);
    c.gridy++;
    pnlTWO.add(txtSUPER_DAM, c);
    c.gridy++;
    pnlTWO.add(txtSUPER_PEN, c);
    c.gridy += 2;
    pnlTWO.add(txtSUPER_MAX, c);
    c.gridy += 3;
    pnlTWO.add(txtDEFENSE, c);
    c.gridy++;
    pnlTWO.add(txtAGILITY, c);
    c.gridy++;
    pnlTWO.add(txtOFFENSIVE, c);
    c.gridy++;
    pnlTWO.add(txtDEFENSIVE, c);

    /////////////////////////////////////////////
    c.gridx = 5;
    c.gridy = 12;
    pnlTWO.add(btnCalcOff, c);
    c.gridy++;
    pnlTWO.add(btnCalcDef, c);
  }

  private void initPanel3() {
    // setup
    Font def = UE.SETTINGS.getDefaultFont();
    chkSPECIAL.setFont(def);
    chkSCREENSAVER.setFont(def);
    jcbRACE_OVR.setFont(def);
    lblPARREQ.setFont(def);
    jcbPARENT.setFont(def);

    String[] str = TECH.getFields();
    for (int i = 0; i < 12; i++) {
      lblPRNT_TECH[i] = new JLabel();
      if (i < 6) {
        lblPRNT_TECH[i].setText(str[i] + ":"); //$NON-NLS-1$
        txtTEC_REQ[i] = new JTextField(NUM_ERAS);
        txtTEC_REQ[i].setFont(def);
      }
      lblPRNT_TECH[i].setFont(def);
    }

    // place
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridwidth = 1;
    c.gridheight = 1;

    // 1st value column
    c.gridx = 0;
    c.gridy = 0;
    lblSPECIAL.setFont(def);
    pnlTHR.add(lblSPECIAL, c);
    c.gridy++;
    JLabel lbl = new JLabel(Language.getString("ShipGUI.63")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTHR.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.42")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTHR.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(" "); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTHR.add(lbl, c);
    c.gridy++;
    c.gridwidth = 4;
    lbl = new JLabel(Language.getString("ShipGUI.43")); //$NON-NLS-1$
    c.gridwidth = 1;
    lbl.setFont(def);
    pnlTHR.add(lbl, c);

    // add tech requirement labels
    for (int i = 0; i < 3; i++) {
      c.gridy = 5 + i;
      lbl = new JLabel(str[i] + ":"); //$NON-NLS-1$
      lbl.setFont(def);
      pnlTHR.add(lbl, c);
      c.gridy = 10 + i;
      pnlTHR.add(lblPRNT_TECH[i], c);
    }

    c.gridy = 8;
    lbl = new JLabel(" "); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTHR.add(lbl, c);
    c.gridy++;
    c.gridwidth = 4;
    pnlTHR.add(lblPARREQ, c);

    // value fields
    c.gridwidth = 1;
    c.gridx = 1;
    c.gridy = 0;
    pnlTHR.add(chkSPECIAL, c);
    c.gridy++;
    pnlTHR.add(cmbPHASER_HOB, c);
    c.gridy++;
    pnlTHR.add(jcbPARENT, c);

    // add tech requirement fields
    for (int i = 0; i < 3; i++) {
      c.gridy = 5 + i;
      pnlTHR.add(txtTEC_REQ[i], c);
      c.gridy = 10 + i;
      pnlTHR.add(lblPRNT_TECH[i + 6], c);
    }

    // 2nd value column
    c.gridx = 3;
    c.gridy = 0;
    lbl = new JLabel(Language.getString("ShipGUI.55")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTHR.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.64")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTHR.add(lbl, c);
    c.gridy++;
    lbl = new JLabel(Language.getString("ShipGUI.51")); //$NON-NLS-1$
    lbl.setFont(def);
    pnlTHR.add(lbl, c);

    // add tech requirement labels
    for (int i = 3; i < 6; i++) {
      c.gridy = i + 2;
      lbl = new JLabel(str[i] + ":"); //$NON-NLS-1$
      lbl.setFont(def);
      pnlTHR.add(lbl, c);
      c.gridy = 7 + i;
      pnlTHR.add(lblPRNT_TECH[i], c);
    }

    // value fields
    c.gridx = 4;
    c.gridy = 0;
    pnlTHR.add(chkSCREENSAVER, c);
    c.gridy++;
    pnlTHR.add(cmbTORPEDO_HOB, c);
    c.gridy++;
    pnlTHR.add(jcbRACE_OVR, c);

    // add tech requirement fields
    for (int i = 3; i < 6; i++) {
      c.gridy = i + 2;
      pnlTHR.add(txtTEC_REQ[i], c);
      c.gridy = 7 + i;
      pnlTHR.add(lblPRNT_TECH[i + 6], c);
    }

    //////////////////////////////////////
    c.gridx = 2;
    c.gridy = 1;
    pnlTHR.add(chkPHASER_HOB_MOD, c);
    c.gridx = 5;
    pnlTHR.add(chkTORPEDO_HOB_MOD, c);
  }

  private void addListeners() {
    jcbSHIP.addActionListener(new CBActionListener(evt -> {
      if (!init)
        return;

      // save changes
      applySelected();
      // update ship list
      fillList();
    }));

    lstSHIP.addListSelectionListener(new CBListSelectionListener(evt -> {
      if (!init || selChange || evt.getValueIsAdjusting())
        return;

      ID sel = lstSHIP.getSelectedValue();
      if (sel == null)
        return;

      try {
        if (SELECTED >= 0) {
          // save changes
          applySelected();
          // update selection
          SELECTED = sel.ID;
          // skip duplicate selection events
          selChange = true;
          // update ship list for name changes
          fillList();
        }

        // update ship data
        updateSelectedShip();
      } finally {
        selChange = false;
      }
    }));

    btnMUP.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      int[] id2 = shipList.moveUp(SELECTED);
      int h = -1;
      for (int i = 0; i < id2.length && h == -1; i++) {
        if (id2[i] == SELECTED)
          h = i;
      }

      SELECTED = h;
      SHIPTECH.reOrder(id2);
      int[] id = shipList.getStartingIDs();
      SHIPRACE.setShipStarts(id);

      chkSPECIAL.setSelected(SPECIAL.contains(SELECTED));
      fillList();
    }));

    btnMDN.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      int[] id2 = shipList.moveDown(SELECTED);
      int h = -1;

      for (int i = 0; i < id2.length && h == -1; i++) {
        if (id2[i] == SELECTED)
          h = i;
      }

      SELECTED = h;
      SHIPTECH.reOrder(id2);
      int[] id = shipList.getStartingIDs();
      SHIPRACE.setShipStarts(id);

      chkSPECIAL.setSelected(SPECIAL.contains(SELECTED));
      fillList();
    }));

    btnNEW.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;
      ShipDefinition shipDef = shipList.getShipDefinition(SELECTED);
      if (shipDef.getRace() >= CStbof.NUM_RACES)
        return;

      // apply changes before copying the ship
      applySelected();

      SHIPTECH.insert(SELECTED);
      shipList.createShip(SELECTED);

      int[] id = shipList.getStartingIDs();
      SHIPRACE.setShipStarts(id);

      // now set ship number and aliens in trek.exe
      val sn1 = (ShortValueSegment) TREK.getSegment(SD_ShipNum.ShipNum1, true);
      val sn2 = (ShortValueSegment) TREK.getSegment(SD_ShipNum.ShipNum2, true);
      val sll = (ShipLoadLimit) TREK.getSegment(SD_ShipNum.ShipLoadLimit1, true);
      val sl2 = (ShipLoadLimit) TREK.getSegment(SD_ShipNum.ShipLoadLimit2, true);
      short shpNum = (short) shipList.getNumberOfEntries();
      sn1.setValue(shpNum);
      sn2.setValue(shpNum);
      sll.setValue(shpNum);
      sl2.setValue(shpNum);

      // update alien ship ids in trek.exe
      updateMonsterIDs();

      // reset special ships
      Integer[] sp = new Integer[SPECIAL.size()];
      SPECIAL.toArray(sp);
      for (int i = 0; i < sp.length; i++) {
        if (sp[i] > SELECTED) {
          SPECIAL.remove(sp[i]);
          SPECIAL.add(sp[i] + 1);
        }
      }

      if (SP != null)
        SP.setShipIDs(SPECIAL);

      fillList();
    }));

    btnRM.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;
      ShipDefinition shipDef = shipList.getShipDefinition(SELECTED);
      if (shipDef.getRace() >= CStbof.NUM_RACES)
        return;

      SHIPTECH.remove(SELECTED);
      shipList.removeShip(SELECTED);

      int[] id = shipList.getStartingIDs();
      SHIPRACE.setShipStarts(id);

      // now set ship number and aliens in trek.exe
      val sn1 = (ShortValueSegment) TREK.getSegment(SD_ShipNum.ShipNum1, true);
      val sn2 = (ShortValueSegment) TREK.getSegment(SD_ShipNum.ShipNum2, true);
      val sll = (ShipLoadLimit) TREK.getSegment(SD_ShipNum.ShipLoadLimit1, true);
      val sl2 = (ShipLoadLimit) TREK.getSegment(SD_ShipNum.ShipLoadLimit2, true);
      short shpNum = (short) shipList.getNumberOfEntries();
      sn1.setValue(shpNum);
      sn2.setValue(shpNum);
      sll.setValue(shpNum);
      sl2.setValue(shpNum);

      // update alien ship ids in trek.exe
      updateMonsterIDs();

      // reset special ships
      Integer[] sp = new Integer[SPECIAL.size()];
      if (SPECIAL.contains(SELECTED)) {
        SPECIAL.remove(SELECTED);
      }
      SPECIAL.toArray(sp);

      for (int i = 0; i < sp.length; i++) {
        if (sp[i] > SELECTED) {
          SPECIAL.remove(sp[i]);
          SPECIAL.add(sp[i] - 1);
        }
      }

      if (SP != null) {
        SP.setShipIDs(SPECIAL);
      }

      fillList();
      int t = SELECTED;
      SELECTED = -1;
      lstSHIP.clearSelection();
      lstSHIP.setSelectedIndex(t);
    }));

    btnPrev.addActionListener(new CBActionListener(evt -> layCARD.previous(pnlCARD)));
    btnNext.addActionListener(new CBActionListener(evt -> layCARD.next(pnlCARD)));
    btnSettings.addActionListener(new CBActionListener(evt -> showSettingsDialog()));

    btnDescTools.addActionListener(new CBActionListener(evt -> {
      if (DescTools.showToolsDialog(shipList)) {
        String desc = SELECTED >= 0 ? shipList.getDesc(SELECTED) : null;
        txtDESC.setText(desc);
      }
    }));

    jcbPIC.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      String nm = (String) jcbPIC.getSelectedItem();
      try {
        ShipDefinition shipDef = shipList.getShipDefinition(SELECTED);

        // remove matching model from screen saver
        if (TREK != null) {
          String gfx = shipDef.getGraphics();
          if (!shipList.getGraphics().contains(gfx))
            screenSaver.remove(gfx + ".hob"); //$NON-NLS-1$
        }

        updateShipImage();
        nm = nm.substring(2, nm.length() - 7);
        shipDef.setGraphics(nm);
      } catch (Exception ex) {
        lblPIC.setIcon(null);
        throw ex;
      }
    }));

    jcbSHIP_NAME.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || jcbSHIP_NAME.getSelectedIndex() < 0)
        return;

      ID selectedItem = (ID) jcbSHIP_NAME.getSelectedItem();
      shipList.getShipDefinition(SELECTED).setNameGroup((short)selectedItem.ID);
    }));

    jcbSHIP_ROLE.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || jcbSHIP_ROLE.getSelectedIndex() < 0)
        return;

      short shipRole = (short) jcbSHIP_ROLE.getSelectedIndex();
      shipList.getShipDefinition(SELECTED).setShipRole(shipRole);
    }));

    jcbMAP_RANGE.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || jcbMAP_RANGE.getSelectedIndex() < 0)
        return;

      int range = ((ID) jcbMAP_RANGE.getSelectedItem()).ID;
      shipList.getShipDefinition(SELECTED).setMapRange(range);
    }));

    jcbCLOAK.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || jcbCLOAK.getSelectedIndex() < 0)
        return;

      int stealth = jcbCLOAK.getSelectedIndex();
      shipList.getShipDefinition(SELECTED).setStealth(stealth);
    }));

    jcbRACE_OVR.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || jcbRACE_OVR.getSelectedIndex() < 0)
        return;

      ID race = (ID) jcbRACE_OVR.getSelectedItem();
      shipList.getShipDefinition(SELECTED).setRaceOverride((short)race.ID);
    }));

    jcbRACE.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || jcbRACE.getSelectedIndex() < 0)
        return;

      ID race = (ID) jcbRACE.getSelectedItem();

      if (shipList.setRace(SELECTED, (short)race.ID, true)) {
        SHIPTECH.setRace(SELECTED, race.ID);
        int[] id2 = shipList.allignIDs(); // <- resets ship id's
        int h = -1;
        for (int i = 0; i < id2.length && h == -1; i++) {
          if (id2[i] == SELECTED) {
            h = i;
          }
        }

        SELECTED = h;
        SHIPTECH.reOrder(id2); // reorders tech requirements
        int[] id = shipList.getStartingIDs(); // returns new starting id's
        SHIPRACE.setShipStarts(id); // set starting ship id's

        if (race != null && race.ID == 35) {
          // if all ships are shown, just refill list
          fillList();
        } else {
          // make jcbSHIP show new owners ship list
          jcbSHIP.setSelectedItem(race);
        }
      }
    }));

    btnCalcOff.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0) {
        ShipDefinition sd = shipList.getShipDefinition(SELECTED);
        int strength = calcSelectedOffensiveStrength();
        sd.setOffensiveStrength(strength);
        txtOFFENSIVE.setText(Integer.toString(strength));
      }
    }));

    btnCalcDef.addActionListener(new CBActionListener(evt -> {
      if (init && SELECTED >= 0) {
        ShipDefinition sd = shipList.getShipDefinition(SELECTED);
        int strength = calcSelectedDefensiveStrength();
        sd.setDefensiveStrength(strength);
        txtDEFENSIVE.setText(Integer.toString(strength));
      }
    }));

    // offensive strength listeners
    txtBEAM_ACCU.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtBEAM_NUM.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtBEAM_MLT.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtBEAM_DAM.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtBEAM_PEN.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtTORP_ACCU.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtTORP_NUM.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtTORP_MLT.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtTORP_DAM.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtSUPER_ACCU.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtSUPER_NUM.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtSUPER_MLT.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtSUPER_DAM.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    txtSUPER_PEN.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateOffensiveStrength()));
    // defensive strength listeners
    txtSHLD_STR.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateDefensiveStrength()));
    txtSHLD_REC.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateDefensiveStrength()));
    txtHULL_STR.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateDefensiveStrength()));
    txtDEFENSE.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateDefensiveStrength()));
    txtAGILITY.getDocument().addDocumentListener(new CBDocumentChangeListener(x -> updateDefensiveStrength()));

    jcbPARENT.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0 || jcbPARENT.getSelectedIndex() < 0)
        return;

      ID h = (ID) jcbPARENT.getSelectedItem();
      SHIPTECH.setParent(SELECTED, (short) h.ID);
      updateSelectedParent();
    }));

    chkSPECIAL.addActionListener(new CBActionListener(evt -> {
      if (!init || SELECTED < 0)
        return;

      if (SPECIAL.contains(SELECTED))
        SPECIAL.remove(SELECTED);
      else
        SPECIAL.add(SELECTED);

      if (SP != null) {
        SP.setShipIDs(SPECIAL);
        chkSPECIAL
            .setText(strSP_TOTAL.replace("%1", Integer.toString(SPECIAL.size()))); //$NON-NLS-1$
      }
    }));

    if (TREK != null) {
      chkPHASER_HOB_MOD.addActionListener(new CBActionListener(evt -> {
        if (!init || SELECTED < 0)
          return;

        SelectPhaserHobFile sphf = (SelectPhaserHobFile) TREK
          .getInternalFile(CTrekSegments.SelectPhaserHobFile, true);

        String msg = "%1 custom phaser hob selection for all ships?";
        msg = msg.replace("%1", (sphf.isPatched()) ? "Disable" : "Enable");
        int ans = JOptionPane.showConfirmDialog(null, msg, UE.APP_NAME, JOptionPane.YES_NO_OPTION);

        if (ans == JOptionPane.YES_OPTION) {
          sphf.setPatched(!sphf.isPatched());

          if (sphf.isPatched()) {
            // fix prefixes
            EmpireImgPrefixes prefixes = (EmpireImgPrefixes) TREK
              .getInternalFile(CTrekSegments.EmpireImagePrefixes, false);

            for (ShipDefinition shipDef : shipList.listShips()) {
              int r = shipDef.getRace();
              char prefix = r < NUM_EMPIRES ? prefixes.getEmpirePrefix(r) : 'M';
              shipDef.setPhaserPrefix(prefix);
            }

            cmbPHASER_HOB.setEnabled(true);
            ShipDefinition shipDef = shipList.getShipDefinition(SELECTED);
            char prefix = Character.toLowerCase(shipDef.getPhaserPrefix());
            cmbPHASER_HOB.setSelectedItem(new ID(null, prefix));
          }
        }

        chkPHASER_HOB_MOD.setEnabled(false);
        chkPHASER_HOB_MOD.setSelected(sphf.isPatched());
        cmbPHASER_HOB.setEnabled(sphf.isPatched());
        chkPHASER_HOB_MOD.setEnabled(true);
      }));

      cmbPHASER_HOB.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (!init || SELECTED < 0 || cmbPHASER_HOB.getSelectedIndex() < 0)
            return;

          try {
            char prefix = (char) ((ID)cmbPHASER_HOB.getSelectedItem()).ID;
            shipList.getShipDefinition(SELECTED).setPhaserPrefix(prefix);
          } catch (Exception ex) {
            Dialogs.displayError(ex);
          }
        }
      });

      chkTORPEDO_HOB_MOD.addActionListener(new CBActionListener(evt -> {
        if (!init || SELECTED < 0)
          return;

        SelectTorpedoHobFile sthf = (SelectTorpedoHobFile) TREK
          .getInternalFile(CTrekSegments.SelectTorpedoHobFile, true);

        String msg = "%1 custom torpedo hob selection for all ships?";
        msg = msg.replace("%1", (sthf.isPatched()) ? "Disable" : "Enable");

        int ans = JOptionPane.showConfirmDialog(
          null, msg, UE.APP_NAME, JOptionPane.YES_NO_OPTION);

        if (ans == JOptionPane.YES_OPTION) {
          sthf.setPatched(!sthf.isPatched());

          if (sthf.isPatched()) {
            try {
              // fix prefixes
              EmpireImgPrefixes prefixes = (EmpireImgPrefixes) TREK
                .getInternalFile(CTrekSegments.EmpireImagePrefixes, false);

              for (ShipDefinition shipDef : shipList.listShips()) {
                int r = shipDef.getRace();
                char prefix = r < NUM_EMPIRES ? prefixes.getEmpirePrefix(r) : 'M';
                shipDef.setTorpedoPrefix(prefix);
              }

              ShipDefinition shipDef = shipList.getShipDefinition(SELECTED);
              char selPrefix = Character.toLowerCase(shipDef.getTorpedoPrefix());
              cmbTORPEDO_HOB.setSelectedItem(new ID(null, selPrefix));
              cmbTORPEDO_HOB.setEnabled(true);
            } catch (Exception ex) {
              Dialogs.displayError(ex);
            }
          }
        }

        chkTORPEDO_HOB_MOD.setEnabled(false);
        chkTORPEDO_HOB_MOD.setSelected(sthf.isPatched());
        cmbTORPEDO_HOB.setEnabled(sthf.isPatched());
        chkTORPEDO_HOB_MOD.setEnabled(true);
      }));

      cmbTORPEDO_HOB.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          if (!init || SELECTED < 0 || cmbTORPEDO_HOB.getSelectedIndex() < 0)
            return;

          try {
            char prefix = (char) ((ID)cmbTORPEDO_HOB.getSelectedItem()).ID;
            shipList.getShipDefinition(SELECTED).setTorpedoPrefix(prefix);
          } catch (Exception ex) {
            Dialogs.displayError(ex);
          }
        }
      });

      chkSCREENSAVER.addActionListener(new CBActionListener(evt -> {
        if (!init || SELECTED < 0)
          return;

        ShipDefinition shipDef = shipList.getShipDefinition(SELECTED);
        String m = shipDef.getGraphics().toLowerCase() + ".hob"; //$NON-NLS-1$

        if (SCREENSAVER.contains(m)) {
          SCREENSAVER.remove(m);
        } else {
          if (screenSaver.getMax() <= SCREENSAVER.size())
            return;

          SCREENSAVER.add(m);
        }

        String[] t = SCREENSAVER.toArray(new String[0]);

        for (String s : t) {
          if (shipList.getFirstIDWithPrefix(s.substring(0, s.length() - 4)) < 0)
            SCREENSAVER.remove(s);
        }

        screenSaver.set(SCREENSAVER.toArray(new String[0]));
      }));
    }
  }

  // update alien ship ids in trek.exe
  // they are bound to the ending slots and can't be moved by the UI
  // therefore they are skipped on move updates
  protected void updateMonsterIDs() throws IOException, InvalidArgumentsException {
    // set individual ship ids
    val ids = shipList.getIDs(Race.Monster, null, null);
    for (int i = 0; i < SD_Monster.All.length; ++i) {
      val seg = TREK.getSegment(SD_Monster.All[i], true);
      if (i == 6) {
        // update monster 7
        ((ShortValueSegment)seg).setValue((short)ids[i].ID);
      } else {
        // update monster 1-6 and 8+9
        ((IntValueSegment)seg).setValue(ids[i].ID);
      }
    }

    // set first alien 1-4
    short startId = (short)ids[0].ID;
    AlienStartingID seg = (AlienStartingID) TREK.getInternalFile(CTrekSegments.AlienStartingId, true);
    seg.setID(startId);
    seg = (AlienStartingID) TREK.getInternalFile(CTrekSegments.AlienStartingId2, true);
    seg.setID(startId);
    seg = (AlienStartingID) TREK.getInternalFile(CTrekSegments.AlienStartingId3, true);
    seg.setID(startId);
    seg = (AlienStartingID) TREK.getInternalFile(CTrekSegments.AlienStartingId4, true);
    seg.setID(startId);
  }

  protected void updateShipImage() throws IOException {
    String nm = (String) jcbPIC.getSelectedItem();
    TargaImage img = STBOF.getTargaImage(nm, LoadFlags.UNCACHED);
    lblPIC.setIcon(new ImageIcon(img.getThumbnail(120, false, AlphaMode.Opaque)));
  }

  protected void updateSelectedShip() {
    ID sel = lstSHIP.getSelectedValue();
    if (sel == null)
      return;

    // unset selected to skip addItem events
    int shipId = sel.ID;
    SELECTED = -1;

    // index
    String strIndex = Language.getString("ShipGUI.56"); //$NON-NLS-1$
    strIndex = strIndex.replace("%1", Integer.toString(shipId)); //$NON-NLS-1$
    lblINDEX.setText(strIndex);

    // update selection groups
    jcbPARENT.removeAllItems();
    jcbPARENT.addItem(new ID(Language.getString("ShipGUI.40"), -1)); //$NON-NLS-1$

    // lookup selected ship
    ShipDefinition shipDef = shipId > -1 ? shipList.getShipDefinition(shipId) : null;
    if (shipDef == null) {
     setDefaults();
     // restore selection
     SELECTED = shipId;
     return;
    }

    ID[] idn = shipList.getIDs(shipDef.getRace());
    for (int h = 0; h < idn.length; h++) {
      jcbPARENT.addItem(idn[h]);
    }

    int parentId = SHIPTECH.getParent(shipId);
    if (parentId >= 0) {
      String parentName = shipList.hasIndex(parentId) ?
        shipList.getShipDefinition(parentId).getName() : "???"; //$NON-NLS-1$
      jcbPARENT.setSelectedItem(new ID(parentName, parentId));
    } else {
      jcbPARENT.setSelectedIndex(0);
    }

    updateSelectedParent();

    String msg = Language.getString("ShipGUI.56"); //$NON-NLS-1$
    msg = msg.replace("%1", Integer.toString(shipId)); //$NON-NLS-1$
    lblINDEX.setText(msg); //$NON-NLS-1$ //$NON-NLS-2$

    int shipNum = shipList.getNumberOfEntries();
    short shipRace = shipDef.getRace();
    btnNEW.setEnabled(shipNum < 264 && shipRace != 36 && TREK != null);
    btnRM.setEnabled(shipNum > 15 && shipRace != 36 && TREK != null);
    btnMUP.setEnabled(shipRace != 36);
    btnMDN.setEnabled(shipRace != 36);

    // page 1
    {
      txtNAME.setText(shipDef.getName());
      txtCLASS.setText(shipDef.getShipClass());
      txtDESC.setText(shipDef.getDesc());

      jcbSHIP_ROLE.setSelectedIndex(shipDef.getShipRole());
      // lookup index to unselect if not available
      int shipNameIndex = availableShipNameGroups
        .indexOf(new ID("", shipDef.getNameGroup()));
      jcbSHIP_NAME.setSelectedIndex(shipNameIndex);
      jcbMAP_RANGE.setSelectedItem(new ID("", (int) shipDef.getMapRange())); //$NON-NLS-1$
      txtMAP_SPEED.setText(Short.toString((short) shipDef.getMapSpeed()));
      txtSCN_RANGE.setText(Integer.toString(shipDef.getScanRange() + 1));

      int raceId = shipDef.getRace();
      jcbRACE.setSelectedItem(new ID(null, raceId));
      raceId = shipDef.getRace_override();
      jcbRACE_OVR.setSelectedItem(new ID(null, raceId));

      txtBLD_COST.setText(Integer.toString(shipDef.getBuildCost()));
      txtMAIN_COST.setText(Integer.toString(shipDef.getMaintenance()));
      jcbCLOAK.setSelectedIndex(shipDef.getStealth());
      txtPROD.setText(Integer.toString(shipDef.getProduction()));
    }

    // page 2
    {
      txtBEAM_ACCU.setText(Integer.toString(shipDef.getBeamAccuracy()));
      txtBEAM_NUM.setText(Integer.toString(shipDef.getBeamNumber()));
      txtBEAM_MLT.setText(Integer.toString(shipDef.getBeamMultiplier()));
      txtBEAM_DAM.setText(Integer.toString(shipDef.getBeamDamage()));
      txtBEAM_MAX.setText(Double.toString(shipDef.getMaxBeamRange()));
      txtBEAM_PEN.setText(Integer.toString(shipDef.getBeamPenetration()));
      txtTORP_ACCU.setText(Integer.toString(shipDef.getTorpedoAccuracy()));
      txtTORP_NUM.setText(Integer.toString(shipDef.getTorpedoNumber()));
      txtTORP_MLT.setText(Integer.toString(shipDef.getTorpedoMultiplier()));
      txtTORP_DAM.setText(Integer.toString(shipDef.getTorpedoDamage()));
      txtTORP_MIN.setText(Double.toString(shipDef.getMinTorpedoRange()));
      txtTORP_MAX.setText(Double.toString(shipDef.getMaxTorpedoRange()));
      txtSUPER_ACCU.setText(Integer.toString(shipDef.getSuperRayAccuracy()));
      txtSUPER_NUM.setText(Integer.toString(shipDef.getSuperRayNumber()));
      txtSUPER_MLT.setText(Integer.toString(shipDef.getSuperRayMultiplier()));
      txtSUPER_DAM.setText(Integer.toString(shipDef.getSuperRayDamage()));
      txtSUPER_MAX.setText(Double.toString(shipDef.getMaxSuperRayRange()));
      txtSUPER_PEN.setText(Integer.toString(shipDef.getSuperRayPenetration()));
      txtFIRING_ARC.setText(Double.toString(shipDef.getFiringArc()));
      txtSHLD_LVL.setText(Integer.toString(shipDef.getShieldLevel()));
      txtSHLD_STR.setText(Integer.toString(shipDef.getShieldStrength()));
      txtSHLD_REC.setText(Integer.toString(shipDef.getShieldRecharge()));
      txtHULL_STR.setText(Integer.toString(shipDef.getHullStrength()));
      txtDEFENSE.setText(Integer.toString(shipDef.getDefense()));
      txtAGILITY.setText(Double.toString(shipDef.getAgility()));
      txtOFFENSIVE.setText(Integer.toString(shipDef.getOffensiveStrength()));
      txtDEFENSIVE.setText(Integer.toString(shipDef.getDefensiveStrength()));
    }

    // page 3
    {
      byte[] hah = SHIPTECH.getTechLvls(shipId);
      for (int g = 0; g < 6; g++) {
        txtTEC_REQ[g].setText(Byte.toString(hah[g]));
      }

      if (shipRace != 36) {
        chkSPECIAL.setSelected(SPECIAL.contains(shipId));
        chkSPECIAL.setEnabled(
            (SP != null && SPECIAL.size() < SP.getSPIdSlotsNumber()) || SPECIAL
                .contains(shipId));

        if (SP == null) {
          chkSPECIAL.setText(strSP_TOTAL.replace("%1", "?")); //$NON-NLS-1$ //$NON-NLS-2$
        } else {
          chkSPECIAL.setText(
              strSP_TOTAL.replace("%1", Integer.toString(SPECIAL.size()))); //$NON-NLS-1$
        }// lblSPECIAL.setText(Language.getString("ShipGUI.41")); //$NON-NLS-1$
      } else {
        chkSPECIAL.setEnabled(false);
        chkSPECIAL.setSelected(false);
      }

      char phaserPrefix = Character.toLowerCase(shipDef.getPhaserPrefix());
      cmbPHASER_HOB.setSelectedItem(new ID(null, phaserPrefix));
      char torpedoPrefix = Character.toLowerCase(shipDef.getTorpedoPrefix());
      cmbTORPEDO_HOB.setSelectedItem(new ID(null, torpedoPrefix));
    }

    // update ship image
    {
      // update ship icon
      String pic = "i_" + shipDef.getGraphics() + "120.tga"; //$NON-NLS-1$ //$NON-NLS-2$
      jcbPIC.setSelectedItem(pic);
      if (!jcbPIC.getSelectedItem().equals(pic)) {
        String err = Language.getString("ShipGUI.54").replace("%1", pic); //$NON-NLS-1$ //$NON-NLS-2$
        Dialogs.displayError(new IOException(err));
      }

      try {
        updateShipImage();
      }
      catch (IOException e) {
        e.printStackTrace();
      }

      // update screen saver models
      String hobName = shipDef.getGraphics().toLowerCase() + ".hob"; //$NON-NLS-1$
      chkSCREENSAVER.setSelected(SCREENSAVER.contains(hobName));
      chkSCREENSAVER.setEnabled(TREK != null && (SCREENSAVER.size() < MAX_SCREENSAVER
        || chkSCREENSAVER.isSelected()));
    }

    // restore selection
    SELECTED = shipId;
  }

  private void setDefaults() {
    jcbPARENT.setSelectedIndex(0);
    updateSelectedParent();

    int shipNum = shipList.getNumberOfEntries();
    btnNEW.setEnabled(shipNum < 264 && TREK != null);
    btnRM.setEnabled(shipNum > 15 && TREK != null);
    btnMUP.setEnabled(true);
    btnMDN.setEnabled(true);

    // page 1
    {
      txtDESC.setText(STR_UNKNOWN);
      txtNAME.setText(STR_UNKNOWN);
      txtCLASS.setText(STR_UNKNOWN);
      jcbSHIP_ROLE.setSelectedIndex(ShipRole.None);
      // lookup index to unselect if not available
      int shipNameIndex = availableShipNameGroups.indexOf(new ID("", 0));
      jcbSHIP_NAME.setSelectedIndex(shipNameIndex);
      jcbMAP_RANGE.setSelectedItem(new ID("", 0)); //$NON-NLS-1$
      txtMAP_SPEED.setText(Integer.toString(0));
      txtSCN_RANGE.setText(Integer.toString(1));

      jcbRACE.setSelectedItem(new ID(null, -1));
      jcbRACE_OVR.setSelectedItem(new ID(null, -1));

      txtBLD_COST.setText(Integer.toString(0));
      txtMAIN_COST.setText(Integer.toString(0));
      jcbCLOAK.setSelectedIndex(0);
      txtPROD.setText(Integer.toString(0));
    }

    // page 2
    {
      txtBEAM_ACCU.setText(Integer.toString(0));
      txtBEAM_NUM.setText(Integer.toString(0));
      txtBEAM_MLT.setText(Integer.toString(0));
      txtBEAM_DAM.setText(Integer.toString(0));
      txtBEAM_PEN.setText(Integer.toString(0));
      txtBEAM_MAX.setText(Double.toString(0));
      txtTORP_ACCU.setText(Integer.toString(0));
      txtTORP_NUM.setText(Integer.toString(0));
      txtTORP_MLT.setText(Integer.toString(0));
      txtTORP_DAM.setText(Integer.toString(0));
      txtTORP_MIN.setText(Double.toString(0));
      txtTORP_MAX.setText(Double.toString(0));
      txtSUPER_ACCU.setText(Integer.toString(0));
      txtSUPER_NUM.setText(Integer.toString(0));
      txtSUPER_MLT.setText(Integer.toString(0));
      txtSUPER_DAM.setText(Integer.toString(0));
      txtSUPER_PEN.setText(Integer.toString(0));
      txtSUPER_MAX.setText(Double.toString(0));
      txtFIRING_ARC.setText(Double.toString(0));
      txtSHLD_LVL.setText(Integer.toString(0));
      txtSHLD_STR.setText(Integer.toString(0));
      txtSHLD_REC.setText(Integer.toString(0));
      txtHULL_STR.setText(Integer.toString(0));
      txtDEFENSE.setText(Integer.toString(0));
      txtAGILITY.setText(Double.toString(0));
      txtOFFENSIVE.setText(Integer.toString(0));
      txtDEFENSIVE.setText(Integer.toString(0));
    }

    // page 3
    {
      for (JTextField req : txtTEC_REQ) {
        req.setText(Integer.toString(-1));
      }

      chkSPECIAL.setEnabled(false);
      chkSPECIAL.setSelected(false);

      String spTotal = SP != null ? Integer.toString(SPECIAL.size()) : "?"; //$NON-NLS-1$
      chkSPECIAL.setText(strSP_TOTAL.replace("%1", spTotal)); //$NON-NLS-1$

      cmbPHASER_HOB.setSelectedItem(new ID(null, '?'));
      cmbTORPEDO_HOB.setSelectedItem(new ID(null, '?'));
    }

    // update ship image
    {
      // update ship icon
      jcbPIC.setSelectedIndex(-1);

      try {
        updateShipImage();
      }
      catch (IOException e) {
        e.printStackTrace();
      }

      // update screen saver models
      chkSCREENSAVER.setSelected(false);
      chkSCREENSAVER.setEnabled(TREK != null && SCREENSAVER.size() < MAX_SCREENSAVER);
    }
  }

  private void fillList() {
    init = false;
    int prev = SELECTED;
    SELECTED = -1;

    ID filter = (ID) jcbSHIP.getSelectedItem();
    ID[] ships = filter != null ? shipList.getIDs(filter.ID) : null;
    lstSHIP.setListData(ships);

    if (prev >= 0)
      lstSHIP.setSelectedValue(new ID("", prev), true); //$NON-NLS-1$

    // default to first entry
    if (lstSHIP.getSelectedIndex() < 0)
      lstSHIP.setSelectedIndex(0);

    // selection update is skipped by init
    // to not apply changes on reload
    updateSelectedShip();

    init = true;
  }

  private void updateSelectedParent() {
    if (jcbPARENT.getSelectedIndex() < 0)
      return;

    ID h = (ID) jcbPARENT.getSelectedItem();
    if (h.ID >= 0) {
      if (shipList.hasIndex(h.ID)) {
        ShipDefinition shipDef = shipList.getShipDefinition(h.ID);
        lblPARREQ.setText(strUPGRADE.replace("%1", shipDef.getName())); //$NON-NLS-1$
        byte[] rot = SHIPTECH.getTechLvls(h.ID);
        for (int gut = 0; gut < 6; gut++) {
          lblPRNT_TECH[6 + gut].setText(Byte.toString(rot[gut]));
        }
      } else {
        lblPARREQ.setText(strUPGRADE.replace("%1", STR_UNKNOWN)); //$NON-NLS-1$
        for (int gut = 0; gut < 6; gut++) {
          lblPRNT_TECH[6 + gut].setText(STR_UNKNOWN);
        }
      }
    } else {
      lblPARREQ.setText(strUPGRADE.replace("%1", Language.getString("ShipGUI.40"))); //$NON-NLS-1$ //$NON-NLS-2$
      for (int gut = 0; gut < 6; gut++) {
        lblPRNT_TECH[6 + gut].setText("/"); //$NON-NLS-1$
      }
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.ShipStats;
  }

  @Override
  public boolean hasChanges() {
    if (shipList.madeChanges() || SHIPRACE.madeChanges() || SHIPTECH.madeChanges()
      || STBOF.isChanged(CStbofFiles.ShipDescSst))
      return true;

    if (TREK != null) {
      if (SP != null && SP.madeChanges() || screenSaver != null && screenSaver.madeChanges())
        return true;

      // check trek.exe ship number and index files
      if ( TREK.isChanged(CTrekSegments.AlienStartingId)
        || TREK.isChanged(CTrekSegments.AlienStartingId2)
        || TREK.isChanged(CTrekSegments.AlienStartingId3)
        || TREK.isChanged(CTrekSegments.AlienStartingId4))
        return true;

      for (String file : SD_ShipNum.Names)
        if (TREK.isChanged(file))
          return true;
      for (String file : SD_Monster.Names)
        if (TREK.isChanged(file))
          return true;
    }

    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(shipList, changes);
    STBOF.checkChanged(SHIPRACE, changes);
    STBOF.checkChanged(SHIPTECH, changes);
    STBOF.checkChanged(CStbofFiles.ShipDescSst, changes);
    if (TREK != null) {
      TREK.checkChanged(SP, changes);
      TREK.checkChanged(screenSaver, changes);

      // check trek.exe ship number and index files
      TREK.checkChanged(SD_ShipNum.Names, changes);
      TREK.checkChanged(CTrekSegments.AlienStartingId, changes);
      TREK.checkChanged(CTrekSegments.AlienStartingId2, changes);
      TREK.checkChanged(CTrekSegments.AlienStartingId3, changes);
      TREK.checkChanged(CTrekSegments.AlienStartingId4, changes);
      TREK.checkChanged(SD_Monster.Names, changes);
    }
  }

  @Override
  public void reload() throws IOException {
    SHIPRACE = (ShipRace) STBOF.getInternalFile(CStbofFiles.ShipRaceSst, true);
    SHIPTECH = (ShipTech) STBOF.getInternalFile(CStbofFiles.ShipTechSst, true);
    shipList = (ShipList) STBOF.getInternalFile(CStbofFiles.ShipListSst, true);

    SP = TREK != null ? (SpecialShips) TREK.getInternalFile(CTrekSegments.SpecialShips, true) : null;
    screenSaver = TREK != null ? (ScreensaverModelList) TREK.getInternalFile(CTrekSegments.ScreensaverModelList, true) : null;

    loadTrekExe();
    fixShipRace();
    sortShips();
    fillList();
  }

  @Override
  public void finalWarning() {
    applySelected();
    shipList.flushDescriptions();
  }

  private void applySelected() {
    if (SELECTED < 0)
      return;

    ShipDefinition shipDef = shipList.getShipDefinition(SELECTED);

    // page 1
    {
      // comboboxes set on selection:
      // ship role, name group, map range, built by and stealth
      shipDef.setName(txtNAME.getText());
      shipDef.setShipClass(txtCLASS.getText());
      shipDef.setDesc(txtDESC.getText());

      double mapSpeed = Short.parseShort(txtMAP_SPEED.getText());
      // infinite speed
      if (mapSpeed < 0)
        mapSpeed = -1;
      shipDef.setMapSpeed(mapSpeed);

      int scanRange = Integer.parseInt(txtSCN_RANGE.getText());
      scanRange = ValueCast.cutUShort(--scanRange);
      shipDef.setScanRange(scanRange);

      int buildCost = Integer.parseInt(txtBLD_COST.getText());
      buildCost = ValueCast.cutUShort(buildCost);
      shipDef.setBuildCost(buildCost);

      int maintenance = Integer.parseInt(txtMAIN_COST.getText());
      maintenance = ValueCast.cutUShort(maintenance);
      shipDef.setMaintenance(maintenance);

      int production = Integer.parseInt(txtPROD.getText());
      production = ValueCast.cutUShort(production);
      shipDef.setProduction(production);
    }

    // page 2
    {
      int beamAccuracy = Integer.parseInt(txtBEAM_ACCU.getText());
      shipDef.setBeamAccuracy(beamAccuracy);

      int beamNum = Integer.parseInt(txtBEAM_NUM.getText());
      shipDef.setBeamNumber(beamNum);

      int beamMul = Integer.parseInt(txtBEAM_MLT.getText());
      shipDef.setBeamMultiplier(beamMul);

      int beamDamage = Integer.parseInt(txtBEAM_DAM.getText());
      shipDef.setBeamDamage(beamDamage);

      int beamPenetration = Integer.parseInt(txtBEAM_PEN.getText());
      shipDef.setBeamPenetration(beamPenetration);

      double maxBeamRange = Double.parseDouble(txtBEAM_MAX.getText());
      shipDef.setMaxBeamRange(maxBeamRange);

      int torpAccuracy = Integer.parseInt(txtTORP_ACCU.getText());
      shipDef.setTorpedoAccuracy(torpAccuracy);

      int torpNum = Integer.parseInt(txtTORP_NUM.getText());
      shipDef.setTorpedoNumber(torpNum);

      int torpMul = Integer.parseInt(txtTORP_MLT.getText());
      shipDef.setTorpedoMultiplier(torpMul);

      int torpDamage = Integer.parseInt(txtTORP_DAM.getText());
      shipDef.setTorpedoDamage(torpDamage);

      double minTorpRange = Double.parseDouble(txtTORP_MIN.getText());
      shipDef.setMinTorpedoRange(minTorpRange);

      double maxTorpRange = Double.parseDouble(txtTORP_MAX.getText());
      shipDef.setMaxTorpedoRange(maxTorpRange);

      int superAccuracy = Integer.parseInt(txtSUPER_ACCU.getText());
      shipDef.setSuperRayAccuracy(superAccuracy);

      int superNum = Integer.parseInt(txtSUPER_NUM.getText());
      shipDef.setSuperRayNumber(superNum);

      int superMul = Integer.parseInt(txtSUPER_MLT.getText());
      shipDef.setSuperRayMultiplier(superMul);

      int superDamage = Integer.parseInt(txtSUPER_DAM.getText());
      shipDef.setSuperRayDamage(superDamage);

      int superPenetration = Integer.parseInt(txtSUPER_PEN.getText());
      shipDef.setSuperRayPenetration(superPenetration);

      double maxSuperRange = Double.parseDouble(txtSUPER_MAX.getText());
      shipDef.setMaxSuperRayRange(maxSuperRange);

      double firingArc = Double.parseDouble(txtFIRING_ARC.getText());
      shipDef.setFiringArc(firingArc);

      int shieldLvl = Integer.parseInt(txtSHLD_LVL.getText());
      shipDef.setShieldLvl(shieldLvl);

      int shieldRecharge = Integer.parseInt(txtSHLD_REC.getText());
      shipDef.setShieldRecharge(shieldRecharge);

      int shield = Integer.parseInt(txtSHLD_STR.getText());
      shipDef.setShieldStrength(shield);

      int hull = Integer.parseInt(txtHULL_STR.getText());
      shipDef.setHullStrength(hull);

      int defense = Integer.parseInt(txtDEFENSE.getText());
      shipDef.setDefense(defense);

      double agility = Double.parseDouble(txtAGILITY.getText());
      shipDef.setAgility(agility);

      int offensive = Integer.parseInt(txtOFFENSIVE.getText());
      shipDef.setOffensiveStrength(offensive);

      int defensive = Integer.parseInt(txtDEFENSIVE.getText());
      shipDef.setDefensiveStrength(defensive);
    }

    // page 3
    {
      byte[] tuna = new byte[6];
      for (int j = 0; j < 6; j++) {
        tuna[j] = Byte.parseByte(txtTEC_REQ[j].getText());
        if (tuna[j] < 0) {
          tuna[j] = 0;
        }
      }
      SHIPTECH.setTechLvls(SELECTED, tuna);

      // set upgrade_for, just in case the ship had an upgrade before
      // but got new owner causing the upgrade to become bogus
      ID h = (ID) jcbPARENT.getSelectedItem();
      SHIPTECH.setParent(SELECTED, (short) h.ID);
    }
  }

  @Override
  public String getHelpFileName() {
    return "Ship.html"; //$NON-NLS-1$
  }

  private void showSettingsDialog() {
    String title = "Ship Type Definition Settings";
    String offensiveDesc = "Offensive calculation modifiers:";
    String defensiveDesc = "Defensive calculation modifiers:";
    JLabel lblOffensiveDesc = new JLabel(offensiveDesc);
    JLabel lblDefensiveDesc = new JLabel(defensiveDesc);

    JLabel lblBeamModifier = new JLabel(Language.getString("ShipGUI.1"));
    JLabel lblTorpModifier = new JLabel(Language.getString("ShipGUI.2"));
    JLabel lblSuperModifier = new JLabel(Language.getString("ShipGUI.3"));
    JLabel lblShieldModifier = new JLabel(Language.getString("ShipGUI.30"));
    JLabel lblRechargeModifier = new JLabel(Language.getString("ShipGUI.29"));
    JLabel lblHullModifier = new JLabel(Language.getString("ShipGUI.31"));
    JLabel lblDefenseModifier = new JLabel(Language.getString("ShipGUI.38"));
    JLabel lblAgilityModifier = new JLabel(Language.getString("ShipGUI.39"));
    JLabel lblUpdateOffensive = new JLabel(Language.getString("ShipGUI.61"));
    JLabel lblUpdateDefensive = new JLabel(Language.getString("ShipGUI.62"));

    JTextField txtBeamModifier = new JTextField(4);
    JTextField txtTorpModifier = new JTextField(4);
    JTextField txtSuperModifier = new JTextField(4);
    JTextField txtShieldModifier = new JTextField(4);
    JTextField txtRechargeModifier = new JTextField(4);
    JTextField txtHullModifier = new JTextField(4);
    JTextField txtDefenseModifier = new JTextField(4);
    JTextField txtAgilityModifier = new JTextField(4);
    JCheckBox chkUpdateOffensive = new JCheckBox();
    JCheckBox chkUpdateDefensive = new JCheckBox();

    Font def = UE.SETTINGS.getDefaultFont();
    lblBeamModifier.setFont(def);
    lblTorpModifier.setFont(def);
    lblSuperModifier.setFont(def);
    lblShieldModifier.setFont(def);
    lblRechargeModifier.setFont(def);
    lblHullModifier.setFont(def);
    lblDefenseModifier.setFont(def);
    lblAgilityModifier.setFont(def);
    lblUpdateOffensive.setFont(def);
    lblUpdateDefensive.setFont(def);
    txtBeamModifier.setFont(def);
    txtTorpModifier.setFont(def);
    txtSuperModifier.setFont(def);
    txtShieldModifier.setFont(def);
    txtRechargeModifier.setFont(def);
    txtHullModifier.setFont(def);
    txtDefenseModifier.setFont(def);
    txtAgilityModifier.setFont(def);

    txtBeamModifier.setText(Double.toString(beamModifier));
    txtTorpModifier.setText(Double.toString(torpModifier));
    txtSuperModifier.setText(Double.toString(superModifier));
    txtShieldModifier.setText(Double.toString(shieldModifier));
    txtRechargeModifier.setText(Double.toString(rechargeModifier));
    txtHullModifier.setText(Double.toString(hullModifier));
    txtDefenseModifier.setText(Double.toString(defenseModifier));
    txtAgilityModifier.setText(Double.toString(agilityModifier));
    chkUpdateOffensive.setSelected(autoCalcOffensiveStrength);
    chkUpdateDefensive.setSelected(autoCalcDefensiveStrength);

    JPanel pnlSettings = new JPanel(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.insets.set(5, 5, 5, 5);
    c.gridx = 0;
    c.gridy = 0;

    // labels
    c.gridwidth = 2;
    pnlSettings.add(lblOffensiveDesc, c);
    c.insets.top = 0;
    c.gridy++;
    c.gridwidth = 1;
    pnlSettings.add(lblBeamModifier, c);
    c.gridy++;
    pnlSettings.add(lblTorpModifier, c);
    c.gridy++;
    pnlSettings.add(lblSuperModifier, c);
    c.insets.top = 10;
    c.gridy++;
    c.gridwidth = 2;
    pnlSettings.add(lblDefensiveDesc, c);
    c.insets.top = 0;
    c.gridy++;
    c.gridwidth = 1;
    pnlSettings.add(lblShieldModifier, c);
    c.gridy++;
    pnlSettings.add(lblRechargeModifier, c);
    c.gridy++;
    pnlSettings.add(lblHullModifier, c);
    c.gridy++;
    pnlSettings.add(lblDefenseModifier, c);
    c.gridy++;
    pnlSettings.add(lblAgilityModifier, c);

    // modifiers
    c.insets.top = 0;
    c.gridx++;
    c.gridy = 1;
    c.weightx = 1;
    pnlSettings.add(txtBeamModifier, c);
    c.gridy++;
    pnlSettings.add(txtTorpModifier, c);
    c.gridy++;
    pnlSettings.add(txtSuperModifier, c);
    c.gridy += 2;
    pnlSettings.add(txtShieldModifier, c);
    c.gridy++;
    pnlSettings.add(txtRechargeModifier, c);
    c.gridy++;
    pnlSettings.add(txtHullModifier, c);
    c.gridy++;
    pnlSettings.add(txtDefenseModifier, c);
    c.gridy++;
    pnlSettings.add(txtAgilityModifier, c);

    JPanel pnlOptions = new JPanel(new GridBagLayout());
    GridBagConstraints co = new GridBagConstraints();
    co.gridx = 0;
    co.gridy = 0;
    pnlOptions.add(lblUpdateOffensive, co);
    co.gridy++;
    pnlOptions.add(lblUpdateDefensive, co);
    co.gridx++;
    co.gridy = 0;
    pnlOptions.add(chkUpdateOffensive, co);
    co.gridy++;
    pnlOptions.add(chkUpdateDefensive, co);

    c.gridx = 0;
    c.insets.top = 10;
    c.gridy++;
    c.gridwidth = 2;
    pnlSettings.add(pnlOptions, c);

    int response = JOptionPane.showConfirmDialog(UE.WINDOW, pnlSettings, title, JOptionPane.OK_CANCEL_OPTION);
    if (response == JOptionPane.OK_OPTION) {
      // update modifiers
      beamModifier = Double.parseDouble(txtBeamModifier.getText());
      torpModifier = Double.parseDouble(txtTorpModifier.getText());
      superModifier = Double.parseDouble(txtSuperModifier.getText());
      shieldModifier = Double.parseDouble(txtShieldModifier.getText());
      rechargeModifier = Double.parseDouble(txtRechargeModifier.getText());
      hullModifier = Double.parseDouble(txtHullModifier.getText());
      defenseModifier = Double.parseDouble(txtDefenseModifier.getText());
      agilityModifier = Double.parseDouble(txtAgilityModifier.getText());

      // update settings
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_PHASER, beamModifier);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_TORP, torpModifier);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_SUPERRAY, superModifier);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_SHIELD, shieldModifier);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_RECHARGE, rechargeModifier);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_HULL, hullModifier);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_DEFENSE, defenseModifier);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_MOD_AGILITY, agilityModifier);

      autoCalcOffensiveStrength = chkUpdateOffensive.isSelected();
      autoCalcDefensiveStrength = chkUpdateDefensive.isSelected();
      UE.SETTINGS.setProperty(SettingsManager.SHIP_AUTO_OFFENSIVE, autoCalcOffensiveStrength);
      UE.SETTINGS.setProperty(SettingsManager.SHIP_AUTO_DEFENSIVE, autoCalcDefensiveStrength);

      if (autoCalcOffensiveStrength || autoCalcDefensiveStrength) {
        // apply pending changes
        applySelected();

        for (ShipDefinition shipDef : shipList.listShips()) {
          if (autoCalcOffensiveStrength) {
            int strength = calcOffensiveStrength(shipDef);
            shipDef.setOffensiveStrength(strength);
          }
          if (autoCalcDefensiveStrength) {
            int strength = calcDefensiveStrength(shipDef);
            shipDef.setDefensiveStrength(strength);
          }
        }

        // update selected ship definition fields
        if (SELECTED >= 0) {
          ShipDefinition sd = shipList.getShipDefinition(SELECTED);
          int offensive = sd.getOffensiveStrength();
          txtOFFENSIVE.setText(Integer.toString(offensive));
          int defensive = sd.getDefensiveStrength();
          txtDEFENSIVE.setText(Integer.toString(defensive));
        }
      }
    }
  }

  private void updateOffensiveStrength() {
    if (autoCalcOffensiveStrength) {
      int strength = calcSelectedOffensiveStrength();
      txtOFFENSIVE.setText(Integer.toString(strength));
    }
  }

  private void updateDefensiveStrength() {
    if (autoCalcDefensiveStrength) {
      int strength = calcSelectedDefensiveStrength();
      txtDEFENSIVE.setText(Integer.toString(strength));
    }
  }

  private int calcSelectedOffensiveStrength() {
    if (!init || SELECTED < 0)
      return 0;

    int beamNum = DataTools.tryParseInt(txtBEAM_NUM.getText()).orElse(0);
    int beamDmg = DataTools.tryParseInt(txtBEAM_DAM.getText()).orElse(0);
    int beamMul = DataTools.tryParseInt(txtBEAM_MLT.getText()).orElse(0);
    int beamAcc = DataTools.tryParseInt(txtBEAM_ACCU.getText()).orElse(0);
    int torpNum = DataTools.tryParseInt(txtTORP_NUM.getText()).orElse(0);
    int torpDmg = DataTools.tryParseInt(txtTORP_DAM.getText()).orElse(0);
    int torpMul = DataTools.tryParseInt(txtTORP_MLT.getText()).orElse(0);
    int torpAcc = DataTools.tryParseInt(txtTORP_ACCU.getText()).orElse(0);
    int superNum = DataTools.tryParseInt(txtSUPER_NUM.getText()).orElse(0);
    int superDmg = DataTools.tryParseInt(txtSUPER_DAM.getText()).orElse(0);
    int superMul = DataTools.tryParseInt(txtSUPER_MLT.getText()).orElse(0);
    int superAcc = DataTools.tryParseInt(txtSUPER_ACCU.getText()).orElse(0);
    return calcOffensiveStrength(
      beamNum, beamDmg, beamMul, beamAcc,
      torpNum, torpDmg, torpMul, torpAcc,
      superNum, superDmg, superMul, superAcc);
  }

  private int calcSelectedDefensiveStrength() {
    if (!init || SELECTED < 0)
      return 0;

    int shield = DataTools.tryParseInt(txtSHLD_STR.getText()).orElse(0);
    int recharge = DataTools.tryParseInt(txtSHLD_REC.getText()).orElse(0);
    int hull = DataTools.tryParseInt(txtHULL_STR.getText()).orElse(0);
    int defense = DataTools.tryParseInt(txtDEFENSE.getText()).orElse(0);
    double agility = DataTools.tryParseDouble(txtAGILITY.getText()).orElse(0.0);
    return calcDefensiveStrength(shield, recharge, hull, defense, agility);
  }

  private int calcOffensiveStrength(ShipDefinition shipDef) {
    int beamNum = shipDef.getBeamNumber();
    int beamDmg = shipDef.getBeamDamage();
    int beamMul = shipDef.getBeamMultiplier();
    int beamAcc = shipDef.getBeamAccuracy();
    int torpNum = shipDef.getTorpedoNumber();
    int torpDmg = shipDef.getTorpedoDamage();
    int torpMul = shipDef.getTorpedoMultiplier();
    int torpAcc = shipDef.getTorpedoAccuracy();
    int superNum = shipDef.getSuperRayNumber();
    int superDmg = shipDef.getSuperRayDamage();
    int superMul = shipDef.getSuperRayMultiplier();
    int superAcc = shipDef.getSuperRayAccuracy();
    return calcOffensiveStrength(
      beamNum, beamDmg, beamMul, beamAcc,
      torpNum, torpDmg, torpMul, torpAcc,
      superNum, superDmg, superMul, superAcc);
  }

  private int calcDefensiveStrength(ShipDefinition shipDef) {
    int shield = shipDef.getShieldStrength();
    int recharge = shipDef.getShieldRecharge();
    int hull = shipDef.getHullStrength();
    int defense = shipDef.getDefense();
    double agility = Double.parseDouble(txtAGILITY.getText());
    return calcDefensiveStrength(shield, recharge, hull, defense, agility);
  }

  private int calcOffensiveStrength(
    int beamNum, int beamDmg, int beamMul, int beamAcc,
    int torpNum, int torpDmg, int torpMul, int torpAcc,
    int superNum, int superDmg, int superMul, int superAcc
  ) {
    double beamStrength = beamNum * beamDmg * beamMul * beamAcc * 0.01 * beamModifier;
    double torpStrength = torpNum * torpDmg * torpMul * torpAcc * 0.01 * torpModifier;
    double superStrength = superNum * superDmg * superMul * superAcc * 0.01 * superModifier;

    // weapon num * damage * multiplier * accuracy * 0.01 * modifier X
    // added for each weapon type: Phaser + Torpedo + Third hidden beam stats
    return (int) (beamStrength + torpStrength + superStrength);
  }

  private int calcDefensiveStrength(
    int shield, int recharge, int hull, int defense, double agility
  ) {
    // (shields + hull) * defense * 0.01 * modifier X
    double shieldStrength = shield * shieldModifier;
    double rechargeStrength = recharge * rechargeModifier;
    double hullStrength = hull * hullModifier;
    double baseDefense = shieldStrength + rechargeStrength + hullStrength;
    double defenseStrength = baseDefense * defense * 0.01 * defenseModifier;
    double agilityStrength = baseDefense * agility * 0.01 * agilityModifier;
    return (int) (baseDefense + defenseStrength + agilityStrength);
  }

}
