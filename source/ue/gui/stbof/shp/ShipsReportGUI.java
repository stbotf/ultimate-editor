package ue.gui.stbof.shp;

import java.io.IOException;
import com.google.common.primitives.SignedBytes;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.ShipTech;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.HtmlReportGUI;
import ue.service.Language;
import ue.util.data.ID;

/**
 * Creates html reports based on input files.
 */
public class ShipsReportGUI extends HtmlReportGUI {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // html strings
  private static final String strHeaderStart = "<H1 align=center>"; //$NON-NLS-1$
  private static final String strHeaderEnd = "</H1>\n<br>\n<br>\n\n"; //$NON-NLS-1$
  private static final String strShipListTableHeadline =
    "<table border cellspacing=1 cellpadding=1 width=\"150%\">\n" +
      "<TR>\n" +
        "<TH align=center bgcolor=silver>ID</TH>\n" +
        "<TH align=center bgcolor=silver>NAME</TH>\n" +
        "<TH align=center bgcolor=silver>CLASS</TH>\n" +
        "<TH align=center bgcolor=silver>FUNCTION</TH>\n" +
        "<TH colspan=2 align=center bgcolor=silver>COST/MNT</TH>\n" +
        "<TH align=center bgcolor=silver>OFF. STR.</TH>\n" +
        "<TH align=center bgcolor=silver>DEF. STR.</TH>\n" +
        "<TH align=center bgcolor=silver>SHIELDS</TH>\n" +
        "<TH align=center bgcolor=silver>HULL</TH>\n" +
        "<TH align=center bgcolor=silver>BEAMS</TH>\n" +
        "<TH align=center bgcolor=silver>B. RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>TORPS</TH>\n" +
        "<TH align=center bgcolor=silver>T. RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>S. RAYS</TH>\n" +
        "<TH align=center bgcolor=silver>SR. RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>F. ARC</TH>\n" +
        "<TH align=center bgcolor=silver>S. PEN.</TH>\n" +
        "<TH align=center bgcolor=silver>ACCURACY</TH>\n" +
        "<TH align=center bgcolor=silver>DEF</TH>\n" +
        "<TH align=center bgcolor=silver>AGILITY</TH>\n" +
        "<TH align=center bgcolor=silver>RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>SPEED</TH>\n" +
        "<TH align=center bgcolor=silver>SCAN</TH>\n" +
        "<TH align=center bgcolor=silver>STEALTH</TH>\n" +
        "<TH align=center bgcolor=silver>PROD</TH>\n" +
        "<TH colspan=6 align=center bgcolor=silver>REQUIRED TECH</TH>\n" +
      "</TR>\n\n";

  private static final String strShipListMinorsTableHeadline =
    "<table border cellspacing=1 cellpadding=1 width=\"150%\">\n" +
      "<TR>\n" +
        "<TH align=center bgcolor=silver>RACE</TH>\n" +
        "<TH align=center bgcolor=silver>ID</TH>\n" +
        "<TH align=center bgcolor=silver>NAME</TH>\n" +
        "<TH align=center bgcolor=silver>CLASS</TH>\n" +
        "<TH align=center bgcolor=silver>FUNCTION</TH>\n" +
        "<TH colspan=2 align=center bgcolor=silver>COST/MNT</TH>\n" +
        "<TH align=center bgcolor=silver>OFF. STR.</TH>\n" +
        "<TH align=center bgcolor=silver>DEF. STR.</TH>\n" +
        "<TH align=center bgcolor=silver>SHIELDS</TH>\n" +
        "<TH align=center bgcolor=silver>HULL</TH>\n" +
        "<TH align=center bgcolor=silver>BEAMS</TH>\n" +
        "<TH align=center bgcolor=silver>B. RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>TORPS</TH>\n" +
        "<TH align=center bgcolor=silver>T. RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>S. RAY</TH>\n" +
        "<TH align=center bgcolor=silver>SR. RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>F. ARC</TH>\n" +
        "<TH align=center bgcolor=silver>S. PEN.</TH>\n" +
        "<TH align=center bgcolor=silver>ACCURACY</TH>\n" +
        "<TH align=center bgcolor=silver>DEF</TH>\n" +
        "<TH align=center bgcolor=silver>AGILITY</TH>\n" +
        "<TH align=center bgcolor=silver>RANGE</TH>\n" +
        "<TH align=center bgcolor=silver>SPEED</TH>\n" +
        "<TH align=center bgcolor=silver>SCAN</TH>\n" +
        "<TH align=center bgcolor=silver>STEALTH</TH>\n" +
        "<TH align=center bgcolor=silver>PROD</TH>\n" +
        "<TH colspan=6 align=center bgcolor=silver>REQUIRED TECH</TH>\n" +
      "</TR>\n\n";

  private static final String strShipListTablePadding =
    "<TR>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD align=center bgcolor=silver>---</TD>\n" +
      "<TD colspan=6 align=center bgcolor=silver>---</TD>\n" +
    "</TR>\n\n";

  private static final String[] range_color = new String[]{
    "FFFFFF",
    "00FF00",
    "FFFF00",
    "FF4020",
    "0000FF"
  };

  private RaceRst race;
  private Lexicon lexicon;
  private ShipList shiplist;
  private ShipTech shiptech;

  /**
   * Displays report on ships.
   * @throws IOException
   */
  public ShipsReportGUI(Stbof stbof) throws IOException {
    val stif = stbof.files();
    shiplist = stif.shipList();
    shiptech = stif.shipTech();
    race = stif.raceRst();
    lexicon = stif.lexicon();

    updateReport();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.ShiplistReport;
  }

  @Override
  protected InternalFile[] filesToCheck() {
    return new InternalFile[] {
      shiplist,
      shiptech,
      race,
      lexicon
    };
  }

  @Override
  protected void genReport(StringBuffer report) {
    int shLen = 0;

    // find largest shield recharge value
    for (ShipDefinition shipDef : shiplist.listShips()) {
      int tmp = Integer.toString(shipDef.getShieldRecharge()).length();
      if (tmp > shLen)
        shLen = tmp;
    }

    // get max tech level
    int maxLvl = shiptech.getMaxTechLevel();

    for (int empire = 0; empire < NUM_EMPIRES; empire++) {
      boolean found = false;
      ID[] ids = shiplist.getIDs(empire);
      report.append(strHeaderStart);

      // empire
      report.append(race.getName(empire));
      report.append(strHeaderEnd);

      report.append(strShipListTableHeadline);

      // ascending order tech wise
      for (byte max = 0; max <= maxLvl; max++) {
        if (found)
          report.append(strShipListTablePadding);

        found = false;

        for (byte min = 0; min <= max; min++) {
          for (int i = 0; i < ids.length; i++) {
            byte[] tech_req = shiptech.getTechLvls(ids[i].ID);

            if (max == SignedBytes.max(tech_req) && min == SignedBytes.min(tech_req)) {
              int id = ids[i].ID;
              ShipDefinition shipDef = shiplist.getShipDefinition(id);
              found = true;
              report.append("<TR>\n");

              // ID
              report.append("<TD align=right>" + id + "</TD>\n");
              // name
              report.append("<TD align=center>" + shipDef.getName() + "</TD>\n");
              // class
              report.append("<TD align=center>" + shipDef.getShipClass() + "</TD>\n");

              // function
              short role = shipDef.getShipRole();
              report.append("<TD align=center>" + LexHelper.mapShipRole(role) + "</TD>\n");

              // build cost
              report.append("<TD align=right>" + shipDef.getBuildCost() + "</TD>\n");
              // maintenance
              report.append("<TD align=right>" + shipDef.getMaintenance() + "</TD>\n");

              // offensive strength
              report.append("<TD align=right bgcolor=silver>" + shipDef.getOffensiveStrength() + "</TD>\n");
              // defensive strength
              report.append("<TD align=right bgcolor=silver>" + shipDef.getDefensiveStrength() + "</TD>\n");

              // shields
              String str_shield = Integer.toString(shipDef.getShieldRecharge());
              int slen = str_shield.length();
              while (slen < shLen) {
                str_shield = "&nbsp " + str_shield;
                slen++;
              }

              report.append("<TD align=right><b>" + shipDef.getShieldStrength()
                + "</b> (" + str_shield + ")</TD>\n");
              // hull
              report.append(
                  "<TD align=right><b>" + shipDef.getHullStrength() + "</b></TD>\n");
              // beams
              report.append("<TD align=right>" + shipDef.getBeamMultiplier() + "x"
                + shipDef.getBeamNumber() + "x" + shipDef.getBeamDamage() + "</TD>\n");
              // beam range
              report.append("<TD align=center>" + shipDef.getMaxBeamRange() + "</TD>\n");
              // torps
              report.append("<TD align=right>" + shipDef.getTorpedoMultiplier() + "x"
                + shipDef.getTorpedoNumber() + "x" + shipDef.getTorpedoDamage() + "</TD>\n");
              // torps range
              report.append("<TD align=center>" + shipDef.getMinTorpedoRange() + " - "
                + shipDef.getMaxTorpedoRange() + "</TD>\n");
              // super rays
              report.append("<TD align=right>" + shipDef.getSuperRayMultiplier() + "x"
                + shipDef.getSuperRayNumber() + "x" + shipDef.getSuperRayDamage() + "</TD>\n");
              // super ray range
              report.append("<TD align=center>" + shipDef.getMaxSuperRayRange() + "</TD>\n");
              // firing arc
              report.append("<TD align=center>" + shipDef.getFiringArc() + "</TD>\n");
              // shield penetration
              report.append("<TD align=center>" + shipDef.getBeamPenetration() + " / "
                + shipDef.getSuperRayPenetration() + "</TD>\n");
              // accuracy
              report.append("<TD align=center>" + shipDef.getBeamAccuracy() + " / "
                + shipDef.getTorpedoAccuracy() + " / " + shipDef.getSuperRayAccuracy() + "</TD>\n");
              // defense
              report.append("<TD align=center>" + shipDef.getDefense() + "</TD>\n");
              // agility
              report.append("<TD align=center>" + shipDef.getAgility() + "</TD>\n");

              // range
              int range = (int) shipDef.getMapRange() + 1;
              report.append("<TD align=center bgcolor=#" + range_color[range] + ">"
                + LexHelper.mapShipRange(range) + "</TD>\n");

              // speed
              report.append("<TD align=center bgcolor=#" + range_color[range] + ">"
                + (int) shipDef.getMapSpeed() + "</TD>\n");
              // scan
              report.append("<TD align=center>" + shipDef.getScanRange() + "</TD>\n");

              // stealth
              int stealth = shipDef.getStealth();
              report.append("<TD align=center>" + LexHelper.mapShipStealth(stealth) + "</TD>\n");

              // production
              int prod = shipDef.getProduction();
              report.append("<TD align=center>" + ( prod > 0 ? prod : prod == 0 ? "-" : "?" ) + "</TD>\n");

              // tech req
              for (int t = 0; t < 6; t++) {
                report.append("<TD align=center bgcolor=#00E0FF><b>" + tech_req[t]
                  + "</b></TD>\n");
              }

              report.append("</TR>\n\n");
            }
          }
        }
      }

      report.append("</table><br><br>\n\n");
    }

    // minors
    String minor;
    report.append(strHeaderStart);
    report.append(Language.getString("ShipsReportGUI.0"));
    report.append(strHeaderEnd);
    report.append(strShipListMinorsTableHeadline);

    for (int rid = NUM_EMPIRES; rid < race.getNumberOfEntries(); rid++) {
      minor = race.getName(rid);
      ID[] ids = shiplist.getIDs(rid);

      for (byte max = 0; max <= 11; max++) {
        for (byte min = 0; min <= max; min++) {
          for (int i = 0; i < ids.length; i++) {
            byte[] tech_req = shiptech.getTechLvls(ids[i].ID);

            if (max == SignedBytes.max(tech_req) && min == SignedBytes.min(tech_req)) {
              int id = ids[i].ID;
              ShipDefinition shipDef = shiplist.getShipDefinition(id);

              report.append("<TR>\n");

              // race
              report.append("<TD align=center>" + minor + "</TD>\n");
              // ID
              report.append("<TD align=right>" + id + "</TD>\n");
              // name
              report.append("<TD align=center>" + shipDef.getName() + "</TD>\n");
              // class
              report.append("<TD align=center>" + shipDef.getClass() + "</TD>\n");

              // function
              short role = shipDef.getShipRole();
              report.append("<TD align=center>" + LexHelper.mapShipRole(role) + "</TD>\n");

              // build cost
              report.append("<TD align=right>" + shipDef.getBuildCost()
                + "</TD>\n");
              // maintenance
              report.append("<TD align=right>"
                + shipDef.getMaintenance() + "</TD>\n");

              // offensive strength
              report.append("<TD align=right bgcolor=silver>" + shipDef.getOffensiveStrength() + "</TD>\n");
              // defensive strength
              report.append("<TD align=right bgcolor=silver>" + shipDef.getDefensiveStrength() + "</TD>\n");

              // shields
              String str_shield = Integer.toString(shipDef.getShieldRecharge());
              int slen = str_shield.length();
              while (slen < shLen) {
                str_shield = "&nbsp " + str_shield;
                slen++;
              }

              report.append("<TD align=right><b>" + shipDef.getShieldStrength()
                + "</b> (" + str_shield + ")</TD>\n");
              // hull
              report.append("<TD align=right><b>" + shipDef.getHullStrength() + "</b></TD>\n");
              // beams
              report.append("<TD align=right>" + shipDef.getBeamMultiplier() + "x"
                + shipDef.getBeamNumber() + "x" + shipDef.getBeamDamage() + "</TD>\n");
              // beam range
              report.append("<TD align=center>" + shipDef.getMaxBeamRange() + "</TD>\n");
              // torps
              report.append("<TD align=right>" + shipDef.getTorpedoMultiplier() + "x"
                + shipDef.getTorpedoNumber() + "x" + shipDef.getTorpedoDamage() + "</TD>\n");
              // torps range
              report.append("<TD align=center>" + shipDef.getMinTorpedoRange() + " - "
                + shipDef.getMaxTorpedoRange() + "</TD>\n");
              // super rays
              report.append("<TD align=right>" + shipDef.getSuperRayMultiplier() + "x"
                + shipDef.getSuperRayNumber() + "x" + shipDef.getSuperRayDamage() + "</TD>\n");
              // super ray range
              report.append("<TD align=center>" + shipDef.getMaxSuperRayRange() + "</TD>\n");
              // firing arc
              report.append("<TD align=center>" + shipDef.getFiringArc() + "</TD>\n");
              // shield penetration
              report.append("<TD align=center>" + shipDef.getBeamPenetration() + " / "
                + shipDef.getSuperRayPenetration() + "</TD>\n");
              // accuracy
              report.append("<TD align=center>" + shipDef.getBeamAccuracy() + " / "
                + shipDef.getTorpedoAccuracy() + " / " + shipDef.getSuperRayAccuracy() + "</TD>\n");
              // defense
              report.append("<TD align=center>" + shipDef.getDefense() + "</TD>\n");
              // agility
              report.append("<TD align=center>" + shipDef.getAgility() + "</TD>\n");

              // range
              int range = (int) shipDef.getMapRange() + 1;
              report.append("<TD align=center bgcolor=#" + range_color[range] + ">"
                + LexHelper.mapShipRange(range) + "</TD>\n");

              // speed
              report.append("<TD align=center bgcolor=#" + range_color[range] + ">"
                + (int) shipDef.getMapSpeed() + "</TD>\n");
              // scan
              report.append("<TD align=center>" + shipDef.getScanRange() + "</TD>\n");

              // stealth
              int stealth = shipDef.getStealth();
              report.append("<TD align=center>" + LexHelper.mapShipStealth(stealth) + "</TD>\n");

              // production
              int prod = shipDef.getProduction();
              report.append("<TD align=center>" + ( prod > 0 ? prod : prod == 0 ? "-" : "?" ) + "</TD>\n");

              // tech req
              for (int t = 0; t < 6; t++) {
                report.append("<TD align=center bgcolor=#00E0FF><b>" + tech_req[t] + "</b></TD>\n");
              }

              report.append("</TR>\n\n");
            }
          }
        }
      }
    }
    report.append("</table><br><br>\n\n");

    // aliens
    report.append(strHeaderStart);
    report.append(Language.getString("ShipsReportGUI.1"));
    report.append(strHeaderEnd);

    ID[] ids = shiplist.getIDs(race.getNumberOfEntries() + 1);
    report.append(strShipListTableHeadline);

    // ok
    for (int i = 0; i < ids.length; i++) {
      byte[] tech_req = shiptech.getTechLvls(ids[i].ID);
      int id = ids[i].ID;
      ShipDefinition shipDef = shiplist.getShipDefinition(id);
      report.append("<TR>\n");

      // ID
      report.append("<TD align=right>" + id + "</TD>\n");
      // name
      report.append("<TD align=center>" + shipDef.getName() + "</TD>\n");
      // class
      report.append("<TD align=center>" + shipDef.getShipClass() + "</TD>\n");

      // function
      short role = shipDef.getShipRole();
      report.append("<TD align=center>" + LexHelper.mapShipRole(role) + "</TD>\n");

      // build cost
      report.append("<TD align=right>" + shipDef.getBuildCost() + "</TD>\n");
      // maintenance
      report.append("<TD align=right>" + shipDef.getMaintenance() + "</TD>\n");

      // offensive strength
      report.append("<TD align=right bgcolor=silver>" + shipDef.getOffensiveStrength() + "</TD>\n");
      // defensive strength
      report.append("<TD align=right bgcolor=silver>" + shipDef.getDefensiveStrength() + "</TD>\n");

      // shields
      String str_shield = Integer.toString(shipDef.getShieldRecharge());
      int slen = str_shield.length();
      while (slen < shLen) {
        str_shield = "&nbsp " + str_shield;
        slen++;
      }

      report.append("<TD align=right><b>" + shipDef.getShieldStrength()
        + "</b> (" + str_shield + ")</TD>\n");
      // hull
      report.append("<TD align=right><b>" + shipDef.getHullStrength() + "</b></TD>\n");
      // beams
      report.append("<TD align=right>" + shipDef.getBeamMultiplier() + "x"
        + shipDef.getBeamNumber() + "x" + shipDef.getBeamDamage() + "</TD>\n");
      // beam range
      report.append("<TD align=center>" + shipDef.getMaxBeamRange() + "</TD>\n");
      // torps
      report.append("<TD align=right>" + shipDef.getTorpedoMultiplier() + "x"
        + shipDef.getTorpedoNumber() + "x" + shipDef.getTorpedoDamage() + "</TD>\n");
      // torps range
      report.append("<TD align=center>" + shipDef.getMinTorpedoRange() + " - "
        + shipDef.getMaxTorpedoRange() + "</TD>\n");
      // super rays
      report.append("<TD align=right>" + shipDef.getSuperRayMultiplier() + "x"
        + shipDef.getSuperRayNumber() + "x" + shipDef.getSuperRayDamage() + "</TD>\n");
      // super ray range
      report.append("<TD align=center>" + shipDef.getMaxSuperRayRange() + "</TD>\n");
      // firing arc
      report.append("<TD align=center>" + shipDef.getFiringArc() + "</TD>\n");
      // shield penetration
      report.append("<TD align=center>" + shipDef.getBeamPenetration() + " / "
        + shipDef.getSuperRayPenetration() + "</TD>\n");
      // accuracy
      report.append("<TD align=center>" + shipDef.getBeamAccuracy() + " / "
        + shipDef.getTorpedoAccuracy() + " / " + shipDef.getSuperRayAccuracy() + "</TD>\n");
      // defense
      report.append("<TD align=center>" + shipDef.getDefense() + "</TD>\n");
      // agility
      report.append("<TD align=center>" + shipDef.getAgility() + "</TD>\n");

      // range
      int range = (int) shipDef.getMapRange() + 1;
      report.append("<TD align=center bgcolor=#" + range_color[range] + ">"
        + LexHelper.mapShipRange(range) + "</TD>\n");

      // speed
      report.append("<TD align=center bgcolor=#" + range_color[range] + ">"
        + (int) shipDef.getMapSpeed() + "</TD>\n");
      // scan
      report.append("<TD align=center>" + shipDef.getScanRange() + "</TD>\n");

      // stealth
      int stealth = shipDef.getStealth();
      report.append("<TD align=center>" + LexHelper.mapShipStealth(stealth) + "</TD>\n");

      // production
      int prod = shipDef.getProduction();
      report.append("<TD align=center>" + ( prod > 0 ? prod : prod == 0 ? "-" : "?" ) + "</TD>\n");

      // tech req
      for (int t = 0; t < 6; t++) {
        report.append("<TD align=center bgcolor=#00E0FF><b>" + tech_req[t] + "</b></TD>\n");
      }

      report.append("</TR>\n\n");
    }

    report.append("</table><br><br>\n\n");
  }

}
