package ue.gui.stbof.shp;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.shp.ShipnameCode;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Shipname;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.file.PathHelper;

/**
 * Used to edit shipnames.
 */
public class ShipnameGUI extends MainPanel {

  private final String strGROUP = Language.getString("ShipnameGUI.0"); //$NON-NLS-1$

  // read
  private Stbof stbof;
  private Trek trek;

  // edited
  private Shipname SHIPNAMES;

  // ui components
  private JComboBox<String> GROUP = new JComboBox<String>();
  private JList<String> NAMELIST = new JList<String>();
  private JButton btnADD = new JButton(Language.getString("ShipnameGUI.1")); //$NON-NLS-1$
  private JButton btnEDIT = new JButton(Language.getString("ShipnameGUI.2")); //$NON-NLS-1$
  private JButton btnREMOVE = new JButton(Language.getString("ShipnameGUI.3")); //$NON-NLS-1$
  private JButton btnIMPORT = new JButton("Import");
  private JButton btnEXPORT = new JButton("Export");
  private JButton btnADD_GROUP = new JButton("+"); //$NON-NLS-1$
  private JButton btnREM_GROUP = new JButton("-"); //$NON-NLS-1$

  // data
  private static final int max = 248;

  /**
   * Returns an instance of this object.
   * @throws IOException
   */
  public ShipnameGUI(Stbof stbof, Trek trek) throws IOException {
    this.stbof = stbof;
    this.trek = trek;
    SHIPNAMES = (Shipname) stbof.getInternalFile(CStbofFiles.ShipNameBin, true);

    //set font
    Font def = UE.SETTINGS.getDefaultFont();
    btnADD.setFont(def);
    btnEDIT.setFont(def);
    btnREMOVE.setFont(def);
    btnIMPORT.setFont(def);
    btnEXPORT.setFont(def);
    GROUP.setFont(def);
    NAMELIST.setFont(def);
    btnADD_GROUP.setEnabled(SHIPNAMES.getNumberOfGroups() < max && trek != null);
    btnREM_GROUP.setEnabled(SHIPNAMES.getNumberOfGroups() > 1 && trek != null);

    //add listeners
    btnADD.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (GROUP.getSelectedIndex() != -1) {
          String n = JOptionPane.showInputDialog(Language.getString("ShipnameGUI.4"),
              Language.getString("ShipnameGUI.5")); //$NON-NLS-1$ //$NON-NLS-2$
          if (n != null) {
            try {
              SHIPNAMES.add(GROUP.getSelectedIndex(), n);
              fillNames();
            } catch (Exception g) {
              Dialogs.displayError(g);
            }
          }
        }
      }
    });
    btnEDIT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (GROUP.getSelectedIndex() != -1 && NAMELIST.getSelectedIndex() != -1) {
          String n = JOptionPane.showInputDialog(Language.getString("ShipnameGUI.6"),
              NAMELIST.getSelectedValue()); //$NON-NLS-1$
          if (n != null) {
            try {
              SHIPNAMES.set(GROUP.getSelectedIndex(), NAMELIST.getSelectedValue(), n);
              fillNames();
            } catch (Exception g) {
              Dialogs.displayError(g);
            }
          }
        }
      }
    });
    btnREMOVE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (GROUP.getSelectedIndex() != -1 && NAMELIST.getSelectedIndex() != -1) {
          try {
            SHIPNAMES.remove(GROUP.getSelectedIndex(), NAMELIST.getSelectedValue());
            fillNames();
          } catch (Exception g) {
            Dialogs.displayError(g);
          }
        }
      }
    });
    GROUP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (GROUP.getSelectedIndex() >= 0) {
          fillNames();
        }
      }
    });
    NAMELIST.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (NAMELIST.getSelectedIndex() >= 0) {
          btnEDIT.setEnabled(true);
          btnREMOVE.setEnabled(true);
        } else {
          btnEDIT.setEnabled(false);
          btnREMOVE.setEnabled(false);
        }
      }
    });
    btnIMPORT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser jfc = new JFileChooser(new File(
          UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));

        FileFilter filter = new FileFilter() {
          @Override
          public boolean accept(File f) {
            if (f.isDirectory()) {
              return true;
            }
            return f.getName().toLowerCase().endsWith(".txt");
          }

          @Override
          public String getDescription() {
            return "*.txt"; //$NON-NLS-1$
          }
        };
        jfc.setFileFilter(filter);
        jfc.setApproveButtonText("Import");
        jfc.setDialogTitle("Import list of names...");
        jfc.setMultiSelectionEnabled(false);

        int returnVal = jfc.showOpenDialog(UE.WINDOW);

        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File file = jfc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(file).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
          String str = in.readLine();

          while (str != null) {
            str = str.trim();
            if (str.length() > 0) {
              SHIPNAMES.add(GROUP.getSelectedIndex(), str);
            }
            str = in.readLine();
          }
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }

        fillNames();
      }
    });
    btnEXPORT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser jfc = new JFileChooser(new File(
          UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));

        FileFilter filter = new FileFilter() {
          @Override
          public boolean accept(File f) {
            if (f.isDirectory()) {
              return true;
            }
            return f.getName().toLowerCase().endsWith(".txt");
          }

          @Override
          public String getDescription() {
            return "*.txt"; //$NON-NLS-1$
          }
        };
        jfc.setFileFilter(filter);
        jfc.setApproveButtonText("Export");
        jfc.setDialogTitle("Export list of names...");
        jfc.setMultiSelectionEnabled(false);

        int returnVal = jfc.showSaveDialog(UE.WINDOW);
        if (returnVal != JFileChooser.APPROVE_OPTION)
          return;

        File file = jfc.getSelectedFile();
        String workPath = PathHelper.toFolderPath(file).toString();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

        if (!file.getName().toLowerCase().endsWith(".txt"))
          file = new File(workPath, file.getName() + ".txt");

        if (file.exists() && !Dialogs.confirmOverwrite(file.getName()))
          return;

        try (BufferedWriter out = new BufferedWriter(new FileWriter(file))) {
          String[] names = SHIPNAMES.getEntries(GROUP.getSelectedIndex());

          for (int i = 0; i < names.length; i++) {
            out.write(names[i]);
            out.newLine();
          }
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }
      }
    });
    btnADD_GROUP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (!UE.FILES.checkLoaded(FileManager.P_TREK))
          return;

        // check whether the default trek.exe limit of shipname groups is exceeded
        if (SHIPNAMES.getNumberOfGroups() == 62) {
          if (trek == null) {
            JOptionPane.showMessageDialog(UE.WINDOW,
                "trek.exe not loaded to extend the number of shipname groups. Action cancelled.");
            return;
          }

          try {
            ShipnameCode fix = (ShipnameCode) trek.getInternalFile(CTrekSegments.ShipnameCode, true);

            if (fix.getActiveOption() != ShipnameCode.OPTION_EXTENDED) {
              // the patch is undetected if further modified, so just issue a warning
              int res = JOptionPane.showConfirmDialog(UE.WINDOW,
                  "This action requires to patch trek.exe for extended shipname groups!\n"
                      + "This can be done using QD's BOTF Patcher tool.\n"
                      + "Are you sure to continue?",
                  UE.APP_NAME, JOptionPane.YES_NO_OPTION);
              if (res != JOptionPane.YES_OPTION)
                return;
            }
          } catch (Exception ex) {
            Dialogs.displayError(ex);
            return;
          }
        }

        if (SHIPNAMES.getNumberOfGroups() >= 248) {
          JOptionPane.showMessageDialog(UE.WINDOW,
              "There currently is a maximum of 248 ship name groups supported. Action cancelled.");
          return;
        }

        try {
          SHIPNAMES.addGroup();
          btnADD_GROUP.setEnabled(SHIPNAMES.getNumberOfGroups() < max);
          fillGroups();
          GROUP.setSelectedIndex(SHIPNAMES.getNumberOfGroups() - 1);
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }
      }
    });
    btnREM_GROUP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (!UE.FILES.checkLoaded(FileManager.P_TREK))
          return;

        // check whether to unapply extended shipname group patch
        if (trek != null && SHIPNAMES.getNumberOfGroups() <= 63) {
          try {
            ShipnameCode fix = (ShipnameCode) trek.getInternalFile(CTrekSegments.ShipnameCode, true);
            if (fix.getActiveOption() == ShipnameCode.OPTION_EXTENDED)
              fix.setActiveOption(ShipnameCode.OPTION_APPENDIX_ONLY);
          } catch (Exception ex) {
            Dialogs.displayError(ex);
            return;
          }
        }

        try {
          int prev = GROUP.getSelectedIndex();
          SHIPNAMES.removeGroup(GROUP.getSelectedIndex());
          btnREM_GROUP.setEnabled(SHIPNAMES.getNumberOfGroups() > 1);
          fillGroups();
          int idx = prev >= SHIPNAMES.getNumberOfGroups() ? SHIPNAMES.getNumberOfGroups() - 1 : prev;
          GROUP.setSelectedIndex(idx);
        } catch (Exception ex) {
          Dialogs.displayError(ex);
        }
      }
    });

    //misc
    NAMELIST.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    fillGroups();

    //add items
    JPanel pnlAll = new JPanel();

    GroupLayout layout = new GroupLayout(pnlAll);
    pnlAll.setLayout(layout);

    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);

    JPanel pnlTop = new JPanel(new BorderLayout());
    pnlTop.add(GROUP, BorderLayout.CENTER);

    JPanel pnlCenter = new JPanel(new BorderLayout());
    JScrollPane scrolly = new JScrollPane(NAMELIST);
    pnlCenter.add(scrolly, BorderLayout.CENTER);

    JPanel pnlOptions = new JPanel(new GridLayout(0, 1, 5, 5));
    pnlOptions.add(btnADD);
    pnlOptions.add(btnEDIT);
    pnlOptions.add(btnREMOVE);
    pnlOptions.add(btnIMPORT);
    pnlOptions.add(btnEXPORT);

    JPanel pnlEast = new JPanel(new BorderLayout());
    pnlEast.add(pnlOptions, BorderLayout.NORTH);
    pnlEast.setPreferredSize(pnlEast.getMinimumSize());

    JPanel pnlGRPOPT = new JPanel(new GridLayout(1, 0, 5, 5));
//        pnlGRPOPT.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
    pnlGRPOPT.add(btnADD_GROUP);
    pnlGRPOPT.add(btnREM_GROUP);

    layout.linkSize(SwingConstants.HORIZONTAL, pnlGRPOPT, pnlEast);

    layout.setHorizontalGroup(
        layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(pnlTop, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
                    GroupLayout.DEFAULT_SIZE)
                .addComponent(pnlCenter, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
                    Short.MAX_VALUE))
//              .addComponent(pnlEast, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(pnlGRPOPT, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE)
                .addComponent(pnlEast, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE))
    );

    layout.setVerticalGroup(
        layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(pnlTop, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE)
                .addComponent(pnlGRPOPT, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE))
//              .addComponent(pnlTop, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(pnlCenter, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
                    Short.MAX_VALUE)
                .addComponent(pnlEast, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE))
    );

    setLayout(new BorderLayout());
    add(pnlAll, BorderLayout.CENTER);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.ShipNames;
  }

  @Override
  public boolean hasChanges() {
    return SHIPNAMES.madeChanges() || trek != null && trek.isChanged(CTrekSegments.ShipnameCode);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    stbof.checkChanged(SHIPNAMES, changes);
    if (trek != null)
      trek.checkChanged(CTrekSegments.ShipnameCode, changes);
  }

  @Override
  public void reload() throws IOException {
    SHIPNAMES = (Shipname) stbof.getInternalFile(CStbofFiles.ShipNameBin, true);
    fillGroups();
  }

  /*this is used to apply recent changes before panel is removed
  no need for it in this case tho*/
  @Override
  public void finalWarning() {
  }

  private void fillGroups() {
    int prev = GROUP.getSelectedIndex();

    GROUP.removeAllItems();
    for (int i = 0; i < SHIPNAMES.getNumberOfGroups(); i++) {
      GROUP.addItem(strGROUP.replace("%1", Integer.toString(i))); //$NON-NLS-1$
    }

    // restore selection
    if (prev >= 0 && prev < GROUP.getItemCount())
      GROUP.setSelectedIndex(prev);
  }

  //fills the names list
  private void fillNames() {
    if (GROUP.getSelectedIndex() < 0)
      return;

    try {
      int sel = NAMELIST.getSelectedIndex();
      String[] names = SHIPNAMES.getEntries(GROUP.getSelectedIndex());
      NAMELIST.setListData(names);
      if (sel >= 0 && sel < names.length) {
        NAMELIST.setSelectedIndex(sel);
      } else {
        btnEDIT.setEnabled(false);
        btnREMOVE.setEnabled(false);
      }
    } catch (Exception g) {
      Dialogs.displayError(g);
    }
  }

  @Override
  public String getHelpFileName() {
    return "Shipname.html"; //$NON-NLS-1$
  }
}
