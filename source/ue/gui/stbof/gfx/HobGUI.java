package ue.gui.stbof.gfx;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.hob.HobFile;
import ue.edit.res.stbof.files.lst.Texture;
import ue.exception.InvalidArgumentsException;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.file.FileFilters;

public class HobGUI extends MainPanel {

  private final String strRADIUS = Language.getString("HobGUI.0"); //$NON-NLS-1$

  // read
  private Stbof STBOF;
  private Texture TEXTURE;

  // edited
  private HobFile CURRENT_HOB;

  // ui components
  private JLabel lblMSIZE;
  private JComboBox<String> cmbHOB;
  private Vector<JComboBox<ID>> cmbTEXTURE;
  private JPanel pnlTEXTURE;
  private JPanel pnlPLACEMENT;
  private JTextField txtMSIZE;
  private JTextField[] txtMPLACEMENT;

  // data
  private ID[] textures;

  /**
   * constructor
   * @throws IOException
   */
  public HobGUI(Stbof stbof) throws IOException {
    this.STBOF = stbof;
    this.TEXTURE = (Texture) stbof.getInternalFile(CStbofFiles.TextureLst, true);
    this.CURRENT_HOB = null;

    String[] t = TEXTURE.getEntries();
    Arrays.sort(t);
    textures = new ID[t.length];
    for (int i = 0; i < t.length; i++) {
      textures[i] = new ID(t[i], TEXTURE.getTextureIndex(t[i]));
    }

    //CREATE
    cmbHOB = new JComboBox<String>();
    pnlTEXTURE = new JPanel();
    pnlPLACEMENT = new JPanel();
    txtMSIZE = new JTextField();
    lblMSIZE = new JLabel();
    txtMPLACEMENT = new JTextField[6];
    for (int i = 0; i < 6; i++) {
      txtMPLACEMENT[i] = new JTextField();
    }

    //SETUP
    Font def = UE.SETTINGS.getDefaultFont();
    cmbHOB.setFont(def);
    pnlTEXTURE.setLayout(new GridBagLayout());
    pnlPLACEMENT.setLayout(new GridBagLayout());

    cmbHOB.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          //SAVE OLD
          if (CURRENT_HOB != null) {
            finalWarning();
            if (CURRENT_HOB.madeChanges()) {
              STBOF.addInternalFile(CURRENT_HOB);
              cmbTEXTURE.forEach(cmb -> cmb.setEnabled(false));
            }
          }

          //SETUP NEW
          //load the hob file from stbof.res
          InternalFile tmp = STBOF.getInternalFile((String) cmbHOB.getSelectedItem(), true);
          CURRENT_HOB = tmp instanceof HobFile ? (HobFile) tmp : null;

          updateHobData();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    // model size
    txtMSIZE.setFont(def);
    txtMSIZE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        String t = txtMSIZE.getText();
        try {
          Float size = Float.parseFloat(t);

          if (size > 0) {
            float combat_distance = size * (float) 0.3268 + (float) 949.0173;
            lblMSIZE.setText(strRADIUS.replace("%1", Float.toString(combat_distance))); //$NON-NLS-1$
            CURRENT_HOB.setModelSize(size);
            return;
          }
        } catch (Exception e) {
          lblMSIZE.setText(Language.getString("HobGUI.1")); //$NON-NLS-1$
        }
      }
    });

    lblMSIZE.setFont(def);

    // placement
    for (int i = 0; i < 6; i++) {
      txtMPLACEMENT[i].setFont(def);
    }

    // pnlPLACEMENT
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridwidth = 1;
    c.gridheight = 1;

    JLabel lbl = new JLabel(Language.getString("HobGUI.2")); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridx = 0;
    c.gridy = 0;
    pnlPLACEMENT.add(lbl, c);
    c.gridx = 1;
    pnlPLACEMENT.add(txtMPLACEMENT[0], c);
    lbl = new JLabel("-"); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridx = 2;
    pnlPLACEMENT.add(lbl, c);
    c.gridx = 3;
    pnlPLACEMENT.add(txtMPLACEMENT[1], c);

    lbl = new JLabel(Language.getString("HobGUI.3")); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridx = 0;
    c.gridy = 1;
    pnlPLACEMENT.add(lbl, c);
    c.gridx = 1;
    pnlPLACEMENT.add(txtMPLACEMENT[2], c);
    lbl = new JLabel("-"); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridx = 2;
    pnlPLACEMENT.add(lbl, c);
    c.gridx = 3;
    pnlPLACEMENT.add(txtMPLACEMENT[3], c);

    lbl = new JLabel(Language.getString("HobGUI.4")); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridx = 0;
    c.gridy = 2;
    pnlPLACEMENT.add(lbl, c);
    c.gridx = 1;
    pnlPLACEMENT.add(txtMPLACEMENT[4], c);
    lbl = new JLabel("-"); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridx = 2;
    pnlPLACEMENT.add(lbl, c);
    c.gridx = 3;
    pnlPLACEMENT.add(txtMPLACEMENT[5], c);

    //SHOW
    setLayout(new GridBagLayout());

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    lbl = new JLabel(Language.getString("HobGUI.5")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);

    c.gridx = 1;
    c.gridy = 1;
    add(cmbHOB, c);

    c.gridx = 0;
    c.gridy = 2;
    c.gridwidth = 4;
    add(lblMSIZE, c);

    c.gridx = 1;
    c.gridy = 3;
    c.gridwidth = 1;
    add(txtMSIZE, c);

    lbl = new JLabel(Language.getString("HobGUI.6")); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridx = 0;
    c.gridy = 4;
    add(lbl, c);
    c.gridx = 1;
    c.gridy = 5;
    c.gridwidth = 4;
    c.gridheight = 3;
    add(pnlPLACEMENT, c);

    lbl = new JLabel(Language.getString("HobGUI.7")); //$NON-NLS-1$
    lbl.setFont(def);
    c.gridwidth = 1;
    c.gridheight = 1;
    c.gridx = 0;
    c.gridy = 8;
    add(lbl, c);

    c.gridx = 1;
    c.gridy = 9;
    c.gridwidth = 4;
    add(pnlTEXTURE, c);

    fillHob();

    pnlTEXTURE.setVisible(true);
    setVisible(true);
  }

  protected void fillHob() throws IOException {
    //fill cmbHOB
    int prev = cmbHOB.getSelectedIndex();
    CURRENT_HOB = null;

    cmbHOB.removeAll();
    Collection<String> fileNames = STBOF.getFileNames(FileFilters.Hob);

    for (String fileName : fileNames)
      cmbHOB.addItem(fileName);

    // restore selection
    if (prev >= 0 && prev < fileNames.size())
      cmbHOB.setSelectedIndex(prev);
  }

  protected void updateHobData() {
    loadHobData();
    repaint();
    validate();
  }

  protected void clearHobData() {
    txtMSIZE.setText(""); //$NON-NLS-1$
    lblMSIZE.setText(""); //$NON-NLS-1$

    for (int i = 0; i < 6; i += 2) {
      txtMPLACEMENT[i].setText(""); //$NON-NLS-1$
      txtMPLACEMENT[i + 1].setText(""); //$NON-NLS-1$
    }

    pnlTEXTURE.removeAll();
  }

  protected void loadHobData() {
    if (CURRENT_HOB == null) {
      clearHobData();
      return;
    }

    // size
    float size = CURRENT_HOB.getModelSize();
    float combat_distance = size * (float) 0.3268 + (float) 949.0173;
    txtMSIZE.setText(Float.toString(size));
    lblMSIZE.setText(strRADIUS.replace("%1", Float.toString(combat_distance))); //$NON-NLS-1$

    // placement
    float[] place = CURRENT_HOB.getModelPlacement();
    for (int i = 0; i < 6; i += 2) {
      txtMPLACEMENT[i].setText(Float.toString(place[i]));
      txtMPLACEMENT[i + 1].setText(Float.toString(place[i + 1]));
    }

    //make
    int numTextures = CURRENT_HOB.numberOfTextures();
    cmbTEXTURE = new Vector<JComboBox<ID>>(numTextures);

    //setup
    Font def = UE.SETTINGS.getDefaultFont();
    for (int i = 0; i < numTextures; i++) {
      JComboBox<ID> cmb = new JComboBox<ID>();
      cmb.setFont(def);
      cmbTEXTURE.add(cmb);
    }

    for (int i = 0; i < textures.length; i++) {
      for (JComboBox<ID> cmb : cmbTEXTURE) {
        cmb.addItem(textures[i]);
      }
    }

    //set cmbTextures and show
    pnlTEXTURE.removeAll();
    GridBagConstraints c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;

    for (int i = 0; i < cmbTEXTURE.size(); i++) {
      JComboBox<ID> cmb = cmbTEXTURE.elementAt(i);
      int id = CURRENT_HOB.getTextureID(i);

      if (id > TEXTURE.getMaxIndex() || id < 0) {
        id = 0;
      }

      cmb.setSelectedItem(new ID("", id));
      cmb.addActionListener(new TexAction(i));

      if (i % 3 == 0) {
        pnlTEXTURE.add(cmb, c);
      } else {
        c.gridx++;
        pnlTEXTURE.add(cmb, c);
        if (c.gridx == 2) {
          c.gridy++;
          c.gridx = 0;
        }
      }
    }
  }

  @Override
  public String menuCommand() {
    return MenuCommand.HobFiles;
  }

  @Override
  public boolean hasChanges() {
    return STBOF.hasBeenModified(FileFilters.Hob);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(FileFilters.Hob, changes);
  }

  @Override
  public void reload() throws IOException {
    fillHob();
  }

  @Override
  public void finalWarning() throws InvalidArgumentsException {
    if (CURRENT_HOB == null)
      return;

    float[] place = new float[6];
    String str;

    str = txtMSIZE.getText();
    float size = Float.parseFloat(str);
    if (size > 0) {
      CURRENT_HOB.setModelSize(size);
    }

    for (int i = 0; i < 6; i++) {
      str = txtMPLACEMENT[i].getText();
      place[i] = Float.parseFloat(str);
    }

    CURRENT_HOB.setModelPlacement(place);
  }

  private class TexAction implements ActionListener {

    private int INDEX;

    public TexAction(int i) {
      INDEX = i;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
      try {
        //set texture
        CURRENT_HOB.setTextureID(INDEX, cmbTEXTURE.elementAt(INDEX).getSelectedItem().hashCode());
        if (CURRENT_HOB.madeChanges()) {
          STBOF.addInternalFile(CURRENT_HOB);
        }
      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }
  }
}
