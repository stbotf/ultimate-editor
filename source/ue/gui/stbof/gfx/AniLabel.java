package ue.gui.stbof.gfx;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import lombok.Getter;
import lombok.val;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.exception.UnsupportedImage;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.component.IconButton;

/**
 * Animation.
 */
public class AniLabel extends JPanel {

  public enum AlertType {
    Error,
    Alien,
    Starbase
  }

  @Getter
  private AniOrCur animation;
  private JLabel label;
  private IconButton alertIcon;

  private int msDelay = 1000; //in miliseconds
  private int index = 0;
  private javax.swing.Timer timer;
  private Image[] frames;
  private boolean forceFirstPixelAlpha = false;
  private int alphaColor = 0;

  // for planet bonus & dilithium overlay
  private int overlay_xPos, overlay_yPos;
  private TargaImage overlay = null;
  private TargaImage background = null;
  // limit max frame size to create a thumbnail
  private int maxFrameSize = 0;

  private ActionListener taskPerformer = new CBActionListener(evt -> {
    synchronized (this) {
      if (animation == null || frames == null || msDelay <= 0 || frames.length == 0) {
        timer.stop();
        return;
      }

      if (index >= frames.length)
        index = 0;

      showFrame(index++);
      timer.setDelay(msDelay);

      if (!isDisplayable())
        timer.stop();
    }
  });

  /**
   * Constructs an empty AniLabel
   */
  public AniLabel() {
    label = new JLabel();
    layoutComponents();
  }

  /**
   * Constructs an AniLabel the uses a "still" image
   */
  public AniLabel(Icon img) {
    label = new JLabel(img);
    layoutComponents();
  }

  /**
   * Constructs an AniLabel showing specified animation.
   * @throws IOException
   */
  public AniLabel(AniOrCur ani, boolean showAlpha) throws IOException {
    if (ani == null)
      throw new NullPointerException();

    animation = ani;
    msDelay = ani.getDelay();
    frames = loadFrames(ani, showAlpha);

    label = new JLabel();
    layoutComponents();
    startAnimation();
  }

  /**
   * Sets the animation.
   * @throws IOException
   */
  public void setAnimation(AniOrCur ani, boolean showAlpha) throws IOException {
    stopAnimation();
    animation = null;
    index = 0;

    if (ani == null) {
      setIcon(null);
      return;
    }

    try {
      if (forceFirstPixelAlpha)
        alphaColor = ani.getFirstPixelRawColor();
      frames = loadFrames(ani, showAlpha);
      msDelay = ani.getDelay();
      animation = ani;
      startAnimation();
    } catch (UnsupportedImage ze) {
      Dialogs.displayError(ze);
    }
  }

  public void setForceFirstPixelAlpha(boolean force) {
    forceFirstPixelAlpha = force;
    if (animation != null)
      alphaColor = animation.getFirstPixelRawColor();
  }

  /**
   * Unsets the animation but displays a replacement icon.
   */
  public void setImage(Image image) {
    stopAnimation();
    animation = null;
    setIcon(new ImageIcon(image));
  }

  public void setOverlayImg(int x_offset, int y_offset, TargaImage overlay) {
    this.overlay_xPos = x_offset;
    this.overlay_yPos = y_offset;
    this.overlay = overlay;
  }

  public void setMaxFrameSize(int maxSize) {
    maxFrameSize = maxSize;
  }

  public void setBackgroundImg(TargaImage background) {
    this.background = background;
  }

  private void startAnimation() {
    stopAnimation();
    index = 0;
    timer = new javax.swing.Timer(msDelay, taskPerformer);
    timer.setRepeats(true);

    // Set initial image, even if not yet visible. Otherwise there is a noticable delay,
    // where the animation is left black and waiting for the next timer event.
    showFrame(index++);
    timer.start();
  }

  public void stopAnimation() {
    if (timer != null) {
      timer.stop();
      timer = null;
    }
  }

  private void showFrame(int index) {
    label.setIcon(frames[index] != null ? new ImageIcon(frames[index]) : null);
  }

  private Image[] loadFrames(AniOrCur ani, boolean showAlpha) throws IOException {
    TargaImage[] tgas = ani.getAllFrames();
    Image[] frames = new Image[tgas.length];

    for (int i = 0; i < tgas.length; ++i) {
      if (overlay != null || background != null)
        applyImageLayers(tgas, i);
      frames[i] = getDisplayImage(tgas[i], showAlpha);
    }

    return frames;
  }

  private void applyImageLayers(TargaImage[] images, int index) {
    TargaImage image = images[index];
    TargaImage sourface;

    if (background != null) {
      Dimension bdim = background.getImageSize();
      Dimension rdim = image.getImageSize();
      int x_offset = (bdim.width - rdim.width) / 2;
      int y_offset = (bdim.height - rdim.height) / 2;
      sourface = background.clone();
      sourface.printImage(image, x_offset, y_offset);
    } else {
      sourface = image.clone();
    }

    if (overlay != null)
      sourface.printImage(overlay, overlay_xPos, overlay_yPos);

    images[index] = sourface;
  }

  /**
   * Creates an Image object from a single TargaImage frame.
   * @return the frame as an Image object
   */
  private Image getDisplayImage(TargaImage ti, boolean showAlpha) {
    if (showAlpha && forceFirstPixelAlpha) {
      return maxFrameSize <= 0 ? ti.getImage(alphaColor)
        : ti.getThumbnail(maxFrameSize, false, alphaColor);
    }
    else {
      AlphaMode alphaMode = showAlpha ? AlphaMode.AlphaChannel : AlphaMode.Opaque;
      return maxFrameSize <= 0 ? ti.getImage(alphaMode)
        : ti.getThumbnail(maxFrameSize, false, alphaMode);
    }
  }

  private void setIcon(ImageIcon icon) {
    if (label == null)
      label = new JLabel(icon);
    else
      label.setIcon(icon);
  }

  public void showAlert(AlertType type, String tooltip) {
    if (alertIcon == null) {
      switch (type) {
        case Alien:
          alertIcon = IconButton.CreateAlertAlienButton();
          break;
        case Starbase:
          alertIcon = IconButton.CreateAlertStarbaseButton();
          break;
        default:
          alertIcon = IconButton.CreateAlertErrorButton();
          break;
      }

      alertIcon.setToolTipText(tooltip);

      val c = new GridBagConstraints();
      c.anchor = GridBagConstraints.SOUTHWEST;
      c.insets.left = 3;
      c.insets.bottom = 1;
      c.gridx = 0;
      c.gridy = 0;
      add(alertIcon, c, 0);
    }
  }

  public void hideAlertIcon() {
    if (alertIcon != null) {
      remove(alertIcon);
      alertIcon = null;
    }
  }

  private void layoutComponents() {
    setLayout(new GridBagLayout());
    val c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 0;
    add(label, c);
  }

}
