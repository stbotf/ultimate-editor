package ue.gui.stbof.gfx;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.gif.GifImage;
import ue.edit.res.stbof.files.lst.PaletteList;
import ue.edit.res.stbof.files.lst.Texture;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.img.GifColorMap;

public class TextureGUI extends MainPanel {

  private final String strRGB = Language.getString("TextureGUI.0"); //$NON-NLS-1$
  private final String strPALETTES = Language.getString("TextureGUI.1"); //$NON-NLS-1$

  // read
  private Stbof STBOF;

  // edited
  private PaletteList PALETTE;
  private Texture TEXTURE;

  // ui components
  private JComboBox<ID> cmbTEXTURE = new JComboBox<ID>();
  private JComboBox<String> cmbPALETTE = new JComboBox<String>();
  private JButton btnAddTEX = new JButton(Language.getString("TextureGUI.2")); //$NON-NLS-1$
  private JButton btnUseOWN = new JButton(Language.getString("TextureGUI.3")); //$NON-NLS-1$
  private JLabel lblTextureIMG = new JLabel();
  private JLabel lblPaletteNUM = new JLabel();
  private JPanel pnlCOLOR = new JPanel(new GridLayout(16, 16));
  private JButton[] btnCOLOR = new JButton[256]; //256*4
  private JButton btnTRIM = new JButton(Language.getString("TextureGUI.4")); //$NON-NLS-1$
  private JButton btnSEL_COLOR = new JButton();
  private JButton btnRemTEX = new JButton("Remove");
  private JSlider jslSEL_COLOR_ALPHA = new JSlider(0, 255);
  private JLabel lblSEL_COLOR = new JLabel();

  // data
  private HashSet<String> filesChanged = new HashSet<String>();
  private GifColorMap gcmGIF;
  private GifImage GIF;
  private int SELECTED = -1;
  private int SELECTED_PIXEL = -1;

  /**
   * Constructor
   * @throws IOException
   */
  public TextureGUI(Stbof st) throws IOException {
    STBOF = st;
    TEXTURE = (Texture) st.getInternalFile(CStbofFiles.TextureLst, true);
    PALETTE = (PaletteList) st.getInternalFile(CStbofFiles.PaletteLst, true);
    gcmGIF = null;

    //SETUP
    Font def = UE.SETTINGS.getDefaultFont();
    Border defBORDER = new LineBorder(Color.BLACK);

    //jslSEL_COLOR_ALPHA
    jslSEL_COLOR_ALPHA.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent ce) {
        try {
          byte[] b = gcmGIF.getPixelBytes(SELECTED_PIXEL);
          b[0] = (byte) jslSEL_COLOR_ALPHA.getValue();
          gcmGIF.setPixelBytes(SELECTED_PIXEL, b);

          int pi[] = new int[4];
          for (int i = 0; i < 4; i++) {
            pi[i] = b[i];
            if (pi[i] < 0) {
              pi[i] = 256 + pi[i];
            }
          }

          String str = strRGB.replace("%1", Integer.toString(pi[1])); //$NON-NLS-1$
          str = str.replace("%2", Integer.toString(pi[2])); //$NON-NLS-1$
          str = str.replace("%3", Integer.toString(pi[3])); //$NON-NLS-1$
          str = str.replace("%4", Integer.toString(pi[0])); //$NON-NLS-1$
          lblSEL_COLOR.setText(str);

          //insert colormap and redraw
          GIF.setGifColorMap(gcmGIF);
          lblTextureIMG.setIcon(new ImageIcon(GIF.getImage()));

          //save to palette
          int t = PALETTE.getPalettesNum();
          PALETTE.setColorMap(SELECTED, gcmGIF);
          int z = PALETTE.getPalettesNum();

          if (z > t) {
            cmbPALETTE.addItem(Integer.toString(z - 1));
            lblPaletteNUM.setText(strPALETTES.replace("%1", Integer.toString(z))); //$NON-NLS-1$
            if (z > 127) {
              lblPaletteNUM.setForeground(Color.RED);
            } else {
              lblPaletteNUM.setForeground(Color.BLACK);
            }
          }

          cmbPALETTE.setSelectedIndex(PALETTE.getPaletteIndex(SELECTED));
          repaint();
          validate();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //lblSEL_COLOR
    lblSEL_COLOR.setFont(def);
    //btnSEL_COLOR
    btnSEL_COLOR.setIconTextGap(1);
    btnSEL_COLOR.setFont(def);
    btnSEL_COLOR.setPreferredSize(new Dimension(40, 40));
    btnSEL_COLOR.setBorder(defBORDER);
    btnSEL_COLOR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          String title = Language.getString("TextureGUI.5"); //$NON-NLS-1$
          Color col = JColorChooser.showDialog(pnlCOLOR, title, btnSEL_COLOR.getBackground());

          if (col != null) {
            gcmGIF.setPixel(SELECTED_PIXEL, col.getRGB());
            jslSEL_COLOR_ALPHA.setEnabled(false);
            jslSEL_COLOR_ALPHA.setValue(0);
            jslSEL_COLOR_ALPHA.setEnabled(true);

            //save to palette
            int t = PALETTE.getPalettesNum();
            PALETTE.setColorMap(SELECTED, gcmGIF);

            int z = PALETTE.getPalettesNum();
            if (z > t) {
              cmbPALETTE.addItem(Integer.toString(z - 1));
              lblPaletteNUM.setText(strPALETTES.replace("%1", Integer.toString(z))); //$NON-NLS-1$
              Color fgCol = z > 127 ? Color.RED : Color.BLACK;
              lblPaletteNUM.setForeground(fgCol);
            }
            cmbPALETTE.setSelectedIndex(PALETTE.getPaletteIndex(SELECTED));
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //btnTRIM
    btnTRIM.setFont(def);
    btnTRIM.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          PALETTE.setColorMap(SELECTED, gcmGIF);
          int temp = SELECTED;
          SELECTED = -1;
          PALETTE.trim(TEXTURE);

          int t = PALETTE.getPalettesNum();
          cmbPALETTE.removeAllItems();
          for (int i = 0; i < t; i++) {
            cmbPALETTE.addItem(Integer.toString(i));
          }
          lblPaletteNUM.setText(strPALETTES.replace("%1", Integer.toString(t))); //$NON-NLS-1$
          if (t > 127) {
            lblPaletteNUM.setForeground(Color.RED);
          } else {
            lblPaletteNUM.setForeground(Color.BLACK);
          }
          SELECTED = temp;
          cmbPALETTE.setSelectedIndex(PALETTE.getPaletteIndex(SELECTED));
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //btnRemTEX
    btnRemTEX.setFont(def);
    btnRemTEX.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          if (SELECTED < 0)
            return;

          String textureName = TEXTURE.get(SELECTED);
          STBOF.removeInternalFile(textureName);
          TEXTURE.remove(SELECTED);
          filesChanged.add(textureName);

          PALETTE.setColorMap(SELECTED, PALETTE.getColorMap(0));
          SELECTED = -1;
          cmbTEXTURE.removeAllItems();
          String[] str = TEXTURE.getEntries();
          Arrays.sort(str);
          for (int i = 0; i < str.length; i++) {
            cmbTEXTURE.addItem(new ID(str[i], TEXTURE.getTextureIndex(str[i])));
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //btnUseOWN
    //add own, set cmbPALETTE to select own
    btnUseOWN.setFont(def);
    btnUseOWN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          if (SELECTED < 0) {
            return;
          }
          //load GifColorMap
          int t = PALETTE.getPalettesNum();
          PALETTE.setColorMap(SELECTED, GIF.getInitialGifColorMap());
          int t2 = PALETTE.getPalettesNum();

          if (t2 > t) {
            cmbPALETTE.addItem(Integer.toString(t2 - 1));
            lblPaletteNUM.setText(strPALETTES.replace("%1", Integer.toString(t2))); //$NON-NLS-1$
            if (t2 > 127) {
              lblPaletteNUM.setForeground(Color.RED);
            } else {
              lblPaletteNUM.setForeground(Color.BLACK);
            }
          }
          cmbPALETTE.setSelectedIndex(PALETTE.getPaletteIndex(SELECTED));
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //btnAddTEX
    btnAddTEX.setFont(def);
    btnAddTEX.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          Collection<String> textures = STBOF.getFileNames(new java.io.FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
              name = name.toLowerCase();
              return (name.endsWith(".gif") && !TEXTURE.contains(name)); //$NON-NLS-1$
            }
          });

          if (!textures.isEmpty()) {
            String[] opt = textures.toArray(new String[0]);
            String tex = (String) JOptionPane.showInputDialog(UE.WINDOW,
                Language.getString("TextureGUI.6"), Language.getString("TextureGUI.7"), //$NON-NLS-1$ //$NON-NLS-2$
                JOptionPane.OK_CANCEL_OPTION, null, opt, opt[0]);

            if (tex != null) {
              GifImage gi = (GifImage) STBOF.getInternalFile(tex, true);
              GifColorMap gcm = gi.getInitialGifColorMap();

              int index = TEXTURE.getUnusedIndex();

              if (index < 0) {
                index = TEXTURE.add(tex);
              } else {
                TEXTURE.set(index, tex);
              }

              cmbTEXTURE.addItem(new ID(tex, index));

              int t = PALETTE.getPalettesNum();
              PALETTE.setColorMap(index, gcm);
              int z = PALETTE.getPalettesNum();

              if (z > t) {
                cmbPALETTE.addItem(Integer.toString(z - 1));
                lblPaletteNUM
                    .setText(strPALETTES.replace("%1", Integer.toString(z))); //$NON-NLS-1$

                if (z > 127) {
                  lblPaletteNUM.setForeground(Color.RED);
                } else {
                  lblPaletteNUM.setForeground(Color.BLACK);
                }
              }

              cmbTEXTURE.setSelectedIndex(index);
            }
          } else {
            JOptionPane
                .showMessageDialog(UE.WINDOW, Language.getString("TextureGUI.8"), UE.APP_NAME,
                    JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //btnCOLOR
    for (int i = 0; i < 256; i++) {
      btnCOLOR[i] = new JButton();
      btnCOLOR[i].setIconTextGap(1);
      btnCOLOR[i].setFont(def);
      btnCOLOR[i].setPreferredSize(new Dimension(20, 20));
      btnCOLOR[i].setBorder(defBORDER);
      btnCOLOR[i].addActionListener(new ColAction(i));
    }

    //cmbPALETTE
    //only show
    cmbPALETTE.setFont(def);
    cmbPALETTE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          int sel = cmbPALETTE.getSelectedIndex();
          if (SELECTED < 0 || sel < 0)
            return;

          //load GifColorMap
          gcmGIF = PALETTE.getPalette(sel);
          //show palette
          int[] pixels = gcmGIF.getMapPixels();
          for (int i = 0; i < 256; i++) {
            btnCOLOR[i].setEnabled(false);
            btnCOLOR[i].setBackground(new Color(pixels[i], false));
            btnCOLOR[i].setEnabled(true);
          }
          //insert colormap and redraw
          GIF.setGifColorMap(gcmGIF);
          lblTextureIMG.setIcon(new ImageIcon(GIF.getImage()));
          if (SELECTED_PIXEL < 0) {
            SELECTED_PIXEL = 0;
          }
          btnCOLOR[SELECTED_PIXEL].doClick();
          repaint();
          validate();
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    //lblPaletteNUM
    lblPaletteNUM.setFont(def);
    loadPalette();

    //cmbTEXTURE
    cmbTEXTURE.setFont(def);
    cmbTEXTURE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        try {
          if (cmbTEXTURE.getSelectedIndex() < 0)
            return;

          //SAVE OLD
          if (SELECTED >= 0 && gcmGIF != null) {
            PALETTE.setColorMap(SELECTED, gcmGIF);
            int z = PALETTE.getPalettesNum();
            lblPaletteNUM.setText(strPALETTES.replace("%1", Integer.toString(z))); //$NON-NLS-1$
            if (z > 127) {
              lblPaletteNUM.setForeground(Color.RED);
            } else {
              lblPaletteNUM.setForeground(Color.BLACK);
            }
          }

          //SET
          ID sel = (ID) cmbTEXTURE.getSelectedItem();
          SELECTED = sel.ID;

          //load image
          String str = sel.toString().toLowerCase();
          GIF = (GifImage) STBOF.getInternalFile(str, true);

          //select palette
          try {
            cmbPALETTE.setSelectedIndex(PALETTE.getPaletteIndex(SELECTED));
          } catch (Exception e) {
            cmbPALETTE.setSelectedIndex(0);
          }
        } catch (Exception e) {
          Dialogs.displayError(e);
        }
      }
    });

    loadTexture();

    cmbPALETTE.setPreferredSize(cmbTEXTURE.getPreferredSize());
    Dimension d = new Dimension(536, 536);
    lblTextureIMG.setPreferredSize(d);
    lblTextureIMG.setHorizontalAlignment(JLabel.CENTER);
    lblTextureIMG.setVerticalAlignment(JLabel.CENTER);
    lblTextureIMG.setFont(def);
    lblTextureIMG.setBorder(BorderFactory.createTitledBorder(
      BorderFactory.createLineBorder(Color.GRAY),
      Language.getString("TextureGUI.12"), //$NON-NLS-1$
      TitledBorder.DEFAULT_JUSTIFICATION,
      TitledBorder.DEFAULT_POSITION, def
    ));

    //SHOW
    setLayout(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;

    GridLayout layout = new GridLayout(0, 6);
    layout.setHgap(5);
    layout.setVgap(5);
    JPanel pnlTexOpt = new JPanel(layout);

    JLabel lbl = new JLabel(Language.getString("TextureGUI.9")); //$NON-NLS-1$
    lbl.setFont(def);

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(lbl);
    pnlTexOpt.add(new JLabel());

    lbl = new JLabel(Language.getString("TextureGUI.10")); //$NON-NLS-1$
    lbl.setFont(def);

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(lbl);
    pnlTexOpt.add(new JLabel());

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(cmbTEXTURE);
    pnlTexOpt.add(btnRemTEX, c);

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(cmbPALETTE);
    pnlTexOpt.add(new JLabel());

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(btnAddTEX);
    pnlTexOpt.add(new JLabel());

    lblPaletteNUM.setHorizontalAlignment(JLabel.CENTER);

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(lblPaletteNUM);
    pnlTexOpt.add(new JLabel());

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(btnUseOWN);
    pnlTexOpt.add(new JLabel());

    pnlTexOpt.add(new JLabel());
    pnlTexOpt.add(btnTRIM);
    pnlTexOpt.add(new JLabel());

    c.gridx = 0;
    c.gridy = 0;
    c.gridheight = 4;
    c.gridwidth = 5;
    add(pnlTexOpt, c);
    c.gridy = 4;
    c.gridx = 0;
    c.gridwidth = 1;
    add(lblTextureIMG, c);

    for (int i = 0; i < 256; i++) {
      pnlCOLOR.add(btnCOLOR[i]);
    }

    c.gridx = 1;
    c.gridy = 4;
    c.gridheight = 1;
    c.gridwidth = 4;
    add(pnlCOLOR, c);
    c.gridwidth = 1;
    c.gridy = 5;
    c.gridx = 1;
    lbl = new JLabel(Language.getString("TextureGUI.11")); //$NON-NLS-1$
    lbl.setFont(def);
    add(lbl, c);
    c.gridx = 2;
    add(jslSEL_COLOR_ALPHA, c);
    c.gridx = 3;
    add(btnSEL_COLOR, c);
    c.gridx = 1;
    c.gridwidth = 3;
    c.gridy = 6;
    add(lblSEL_COLOR, c);

    pnlCOLOR.setVisible(true);
    setVisible(true);
  }

  private void loadPalette() {
    int t = PALETTE.getPalettesNum();
    lblPaletteNUM.setText(strPALETTES.replace("%1", Integer.toString(t))); //$NON-NLS-1$
    if (t > 127) {
      lblPaletteNUM.setForeground(Color.RED);
    } else {
      lblPaletteNUM.setForeground(Color.BLACK);
    }

    int prev = cmbPALETTE.getSelectedIndex();
    cmbPALETTE.removeAllItems();
    for (int i = 0; i < t; i++) {
      cmbPALETTE.addItem(Integer.toString(i));
    }

    // restore selection
    if (prev >= 0 && prev < t)
      cmbPALETTE.setSelectedIndex(prev);
  }

  private void loadTexture() {
    String[] str = TEXTURE.getEntries();
    Arrays.sort(str);

    int prev = cmbTEXTURE.getSelectedIndex();
    cmbTEXTURE.removeAllItems();
    for (int i = 0; i < str.length; i++) {
      cmbTEXTURE.addItem(new ID(str[i], TEXTURE.getTextureIndex(str[i])));
    }

    // restore selection
    if (prev >= 0 && prev < str.length)
      cmbTEXTURE.setSelectedIndex(prev);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Textures;
  }

  @Override
  public boolean hasChanges() {
    return TEXTURE.madeChanges() || PALETTE.madeChanges() || !filesChanged.isEmpty();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    STBOF.checkChanged(TEXTURE, changes);
    STBOF.checkChanged(PALETTE, changes);
    changes.addAll(STBOF, filesChanged);
  }

  @Override
  public void reload() throws IOException {
    filesChanged.clear();
    TEXTURE = (Texture) STBOF.getInternalFile(CStbofFiles.TextureLst, true);
    PALETTE = (PaletteList) STBOF.getInternalFile(CStbofFiles.PaletteLst, true);

    // unset selected to not update the texture palette
    SELECTED = -1;
    loadPalette();
    loadTexture();
  }

  @Override
  public void finalWarning() {
    if (SELECTED >= 0 && gcmGIF != null) {
      PALETTE.setColorMap(SELECTED, gcmGIF);
    }
  }

  private class ColAction implements ActionListener {

    private int INDEX;

    public ColAction(int i) {
      INDEX = i;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
      try {
        if (SELECTED < 0)
          return;

        //open ColorChooser
        if (SELECTED_PIXEL >= 0)
          btnCOLOR[SELECTED_PIXEL].setBorder(new LineBorder(Color.BLACK));

        btnCOLOR[INDEX].setBorder(new LineBorder(Color.WHITE));
        SELECTED_PIXEL = INDEX;

        //show
        int pix = gcmGIF.getPixel(SELECTED_PIXEL);
        btnSEL_COLOR.setBackground(new Color(pix, false));
        byte[] b = gcmGIF.getPixelBytes(SELECTED_PIXEL);
        jslSEL_COLOR_ALPHA.setEnabled(false);

        int pi[] = new int[4];
        for (int i = 0; i < 4; i++) {
          pi[i] = b[i];
          if (pi[i] < 0) {
            pi[i] = 256 + pi[i];
          }
        }

        jslSEL_COLOR_ALPHA.setValue(pi[0]);
        jslSEL_COLOR_ALPHA.setEnabled(true);

        String str = strRGB.replace("%1", Integer.toString(pi[1])); //$NON-NLS-1$
        str = str.replace("%2", Integer.toString(pi[2])); //$NON-NLS-1$
        str = str.replace("%3", Integer.toString(pi[3])); //$NON-NLS-1$
        str = str.replace("%4", Integer.toString(pi[0])); //$NON-NLS-1$
        lblSEL_COLOR.setText(str);
      } catch (Exception e) {
        Dialogs.displayError(e);
      }
    }
  }

  @Override
  public String getHelpFileName() {
    return "Textures.html"; //$NON-NLS-1$
  }
}
