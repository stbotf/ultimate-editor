package ue.gui.stbof.gfx;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import ue.UE;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.gui.common.FileChanges;
import ue.gui.common.MainPanel;
import ue.gui.menu.MenuCommand;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.UEWorker;
import ue.util.file.FileFilters;

public class AniGUI extends MainPanel {

  // read
  private Stbof STBOF;

  // edited
  private AniOrCur AOC = null;

  // ui components
  // 0-1 tip, 2-8 stat names, 9-14 stats
  private JLabel[] LABELS = new JLabel[15];
  private JList<String> LIST = new JList<String>();
  private AniLabel ANI = new AniLabel();
  private JSpinner SPEED;
  private JButton btnEXT;
  private JCheckBox chkALPHA;

  public AniGUI(Stbof st) throws IOException {
    STBOF = st;
    setupComponents();
    layoutComponents();

    fillList();
    setupListeners();
    LIST.setSelectedIndex(0);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Animations;
  }

  @Override
  public boolean hasChanges() {
    ListModel<String> aniModel = LIST.getModel();
    for (int i = 0; i < aniModel.getSize(); ++i) {
      String aniName = aniModel.getElementAt(i);
      if (STBOF.isChanged(aniName))
        return true;
    }
    return false;
  }

  @Override
  public void reload() throws IOException {
    AOC = null;
    fillList();
  }

  @Override
  public void finalWarning() throws IOException {
    if (AOC == null)
      return;

    short speed = (short) ((Integer) SPEED.getValue()).intValue();
    if (AOC.setDelay(speed))
      STBOF.addInternalFile(AOC);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    ListModel<String> aniModel = LIST.getModel();
    for (int i = 0; i < aniModel.getSize(); ++i) {
      String aniName = aniModel.getElementAt(i);
      STBOF.checkChanged(aniName, changes);
    }
  }

  private void setupComponents() {
    // create labels
    LABELS[0] = new JLabel(Language.getString("AniGUI.0")); //$NON-NLS-1$
    LABELS[1] = new JLabel(Language.getString("AniGUI.1")); //$NON-NLS-1$
    LABELS[2] = new JLabel(Language.getString("AniGUI.2")); //$NON-NLS-1$
    LABELS[3] = new JLabel(Language.getString("AniGUI.3")); //$NON-NLS-1$
    LABELS[4] = new JLabel(Language.getString("AniGUI.4")); //$NON-NLS-1$
    LABELS[5] = new JLabel(Language.getString("AniGUI.5")); //$NON-NLS-1$
    LABELS[6] = new JLabel(Language.getString("AniGUI.6")); //$NON-NLS-1$
    LABELS[7] = new JLabel(Language.getString("AniGUI.7")); //$NON-NLS-1$
    LABELS[8] = new JLabel(Language.getString("AniGUI.8")); //$NON-NLS-1$

    for (int i = 9; i <= 14; i++) {
      LABELS[i] = new JLabel();
      LABELS[i].setHorizontalAlignment(SwingConstants.RIGHT);
    }
    btnEXT = new JButton(Language.getString("AniGUI.9")); //$NON-NLS-1$

    // create textfield
    SPEED = new JSpinner(new SpinnerNumberModel(0, 0, Short.MAX_VALUE, 1));

    // set font
    Font def = UE.SETTINGS.getDefaultFont();
    for (int i = 0; i <= 14; i++) {
      LABELS[i].setFont(def);
    }
    LIST.setFont(def);
    ANI.setFont(def);
    SPEED.setFont(def);
    btnEXT.setFont(def);

    // set other things
    LIST.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    ANI.setForceFirstPixelAlpha(true);

    // use alpha
    chkALPHA = new JCheckBox("Show transparency", true);
    chkALPHA.setFont(def);
  }

  private void setupListeners() {
    SPEED.addChangeListener(new CBChangeListener(evt -> {
      if (AOC == null)
        return;

      short speed = (short) ((Integer) SPEED.getValue()).intValue();
      if (AOC.setDelay(speed))
        STBOF.addInternalFile(AOC);

      ANI.setAnimation(AOC, chkALPHA.isSelected());
    }));

    chkALPHA.addActionListener(new CBActionListener(evt -> {
      int sel = LIST.getSelectedIndex();
      LIST.clearSelection();
      if (sel >= 0)
        LIST.setSelectedIndex(sel);
    }));

    LIST.addListSelectionListener(new CBListSelectionListener(evt -> {
      if (LIST.getSelectedIndex() >= 0) {
        finalWarning();

        try {
          btnEXT.setEnabled(true);
          String d = LIST.getSelectedValue();
          AOC = STBOF.getAnimation(d, LoadFlags.UNCACHED);

          Dimension dim = AOC.getFrameDim();
          LABELS[9].setText(Double.toString(dim.getWidth()));
          LABELS[10].setText(Double.toString(dim.getHeight()));
          LABELS[11].setText(Short.toString(AOC.getNumFrames()));
          SPEED.setValue((int) (AOC.getDelay()));
          dim = AOC.getTgaDim();
          LABELS[12].setText(Double.toString(dim.getWidth()));
          LABELS[13].setText(Double.toString(dim.getHeight()));
          LABELS[14].setText(Integer.toString(AOC.getTgaSize()));

          if (AOC.isCursor()) {
            SPEED.setEnabled(true);
          } else {
            SPEED.setEnabled(false);
          }
          ANI.setAnimation(AOC, chkALPHA.isSelected());
        } catch (Exception fnfe) {
          btnEXT.setEnabled(false);
          ANI.setAnimation(null, false);
          Dialogs.displayError(fnfe);
        }
      }
    }));

    btnEXT.addActionListener(new CBActionListener(evt -> {
      if (LIST.getSelectedIndex() < 0)
        return;

      final String name = LIST.getSelectedValue();
      btnEXT.setEnabled(false);

      // select path
      JFileChooser fc = new JFileChooser();
      fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
      fc.setDialogTitle(Language.getString("AniGUI.10")); //$NON-NLS-1$
      fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      fc.setMultiSelectionEnabled(false);
      int returnVal = fc.showDialog(UE.WINDOW, Language.getString("AniGUI.11")); //$NON-NLS-1$

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        final File path = fc.getSelectedFile();
        UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, path.getPath());

        UEWorker<Void> worker = new UEWorker<Void>() {
          @Override
          protected Void work() throws Exception {
            setProgressVisible(true);
            setProgress(0);

            int num = AOC.getNumFrames();
            int i = -1;
            String padding = ".000";

            for (int j = 0; j < num; j++) {
              i++;

              // fix padding
              while (padding.length() > 1 && (padding + i).length() > 5) {
                padding = padding.substring(0, padding.length() - 1);
              }

              // construct destination file
              File dest = new File(path, name + padding + i + ".tga"); //$NON-NLS-1$

              // get frame
              TargaImage targ = AOC.getFrame(j);

              // check file existance and fix filename if need be
              while (dest.exists()) {
                i++;
                while (padding.length() > 1 && (padding + i).length() > 5) {
                  padding = padding.substring(0, padding.length() - 1);
                }
                dest = new File(path, name + padding + i + ".tga"); //$NON-NLS-1$
              }

              setProgress((100 * j) / num);
              feedMessage("Extracting %1... %2%"
                .replace("%1", dest.getName())
                .replace("%2", Integer.toString(this.getProgress()))
              );

              // export unmodified tga frames
              try (FileOutputStream fos = new FileOutputStream(dest)) {
                targ.save(fos, 16);
              }
            }

            return null;
          }
        };

        GuiTools.runUEWorker(worker);
      }

      btnEXT.setEnabled(true);
    }));
  }

  private void layoutComponents() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.anchor = GridBagConstraints.NORTH;

    // labels
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    add(LABELS[0], c);
    c.gridwidth = 2;
    c.gridx = 1;
    add(LABELS[1], c);
    c.gridwidth = 1;
    c.gridy = 1;
    add(LABELS[2], c);
    c.gridy = 2;
    add(LABELS[3], c);
    c.gridy = 3;
    add(LABELS[4], c);
    c.gridy = 4;
    add(LABELS[5], c);
    c.gridy = 5;
    add(LABELS[6], c);
    c.gridy = 6;
    add(LABELS[7], c);
    c.gridy = 7;
    add(LABELS[8], c);
    c.gridx = 2;
    c.gridy = 1;
    add(LABELS[9], c);
    c.gridy = 2;
    add(LABELS[10], c);
    c.gridy = 3;
    add(LABELS[11], c);
    c.gridy = 5;
    add(LABELS[12], c);
    c.gridy = 6;
    add(LABELS[13], c);
    c.gridy = 7;
    add(LABELS[14], c);

    // textfield
    c.gridy = 4;
    add(SPEED, c);

    // list
    c.gridheight = 7;
    c.gridx = 0;
    c.gridy = 1;
    JScrollPane scrolly = new JScrollPane(LIST);
    scrolly.setPreferredSize(new Dimension(160, 160));
    add(scrolly, c);

    // animation
    c.gridheight = 1;
    c.gridwidth = 4;
    c.gridx = 0;
    c.gridy = 8;
    add(ANI, c);

    // extract button
    c.gridheight = 1;
    c.gridwidth = 1;
    c.gridx = 3;
    c.gridy = 1;
    add(btnEXT, c);
    c.gridy = 2;
    add(chkALPHA, c);
  }

  private void fillList() throws IOException {
    Collection<String> list = STBOF.getFileNames(FileFilters.AniOrCur);
    LIST.setListData(list.toArray(new String[0]));
  }

}
