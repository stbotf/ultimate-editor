package ue.gui.main;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import lombok.val;
import ue.UE;
import ue.gui.common.DefaultPanel;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.gui.util.component.IconButton;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBListSelectionListener;
import ue.gui.util.gfx.Icons;
import ue.gui.util.style.HoverListRenderer;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.app.AppVersion;
import ue.util.data.StringTools;
import ue.util.file.FileFilters;

/**
 * The user interface start page (MainPanel)
 */
public class StartPagePanel extends DefaultPanel {

  private static final String NONE = Language.getString("StartPagePanel.22");

  // ui components
  private JPanel pnlGUI = new JPanel(new GridBagLayout());
  private JLabel lblWelcome = new JLabel();
  private JLabel lblVersion = new JLabel();
  // update
  private JLabel lblUpdate = new JLabel();
  private JPanel pnlAskUpdate = null;
  private JLabel lblAskUpdate = null;
  private JButton btnAgreeUpdate = null;
  // view options
  private IconButton btnChangelog = new IconButton(Language.getString("StartPagePanel.9"), Icons.CHANGELOG); //$NON-NLS-1$
  private IconButton btnReadme = new IconButton(Language.getString("StartPagePanel.10"), Icons.README); //$NON-NLS-1$
  private IconButton btnFAQ = new IconButton(Language.getString("StartPagePanel.11"), Icons.FAQ); //$NON-NLS-1$
  private IconButton btnHelp = new IconButton(Language.getString("StartPagePanel.12"), Icons.HELP); //$NON-NLS-1$
  private IconButton btnAFC = new IconButton(Language.getString("StartPagePanel.13"), Icons.AFC); //$NON-NLS-1$
  private IconButton btnGitlabs = new IconButton(Language.getString("StartPagePanel.14"), Icons.GIT); //$NON-NLS-1$
  // trek.exe / stbof.res
  private JLabel lblTREK = new JLabel(Language.getString("StartPagePanel.17")); //$NON-NLS-1$
  private JLabel lblSTBOF = new JLabel(Language.getString("StartPagePanel.18")); //$NON-NLS-1$
  private JTextField txtTREK = new JTextField(40);
  private JTextField txtSTBOF = new JTextField(40);
  private JButton btnTREK = new JButton(Language.getString("StartPagePanel.19")); //$NON-NLS-1$
  private JButton btnSTBOF = new JButton(Language.getString("StartPagePanel.19")); //$NON-NLS-1$
  // recent
  private JLabel lblRecentFiles = new JLabel(Language.getString("StartPagePanel.20")); //$NON-NLS-1$
  private JList<String> lstRecentFiles = new JList<>();
  // savegame
  private JLabel lblSavegames = new JLabel(Language.getString("StartPagePanel.21")); //$NON-NLS-1$
  private JList<String> lstSavegames = new JList<>();

  // data
  private boolean firstStart = false;
  private boolean askUpdate = false;
  private boolean loading = false;
  private HoverListRenderer lstRecentFiles_Renderer = new HoverListRenderer();
  private HoverListRenderer lstSavegames_Renderer = new HoverListRenderer();

  /**
   * Constructor.
   */
  public StartPagePanel() {
    // detect missing settings for first application start
    firstStart = !UE.SETTINGS.foundSettings();
    askUpdate = firstStart && !UE.SETTINGS.getBooleanProperty(SettingsManager.CHECK_FOR_UPDATES);

    setupComponents();
    placeComponents();
    loadSettings();
    addListeners();
  }

  private void setupComponents() {
    // default font
    Font font = UE.SETTINGS.getDefaultFont();
    int fontSize = font.getSize();

    Font fontTopic = font.deriveFont(Font.BOLD, fontSize + fontSize / 2);
    lblWelcome.setFont(fontTopic);
    lblVersion.setFont(font);
    // update
    lblUpdate.setFont(font);
    if (lblAskUpdate != null)
      lblAskUpdate.setFont(font);
    if (btnAgreeUpdate != null)
      btnAgreeUpdate.setFont(font);
    // view options
    btnChangelog.setFont(font);
    btnReadme.setFont(font);
    btnFAQ.setFont(font);
    btnHelp.setFont(font);
    btnAFC.setFont(font);
    btnGitlabs.setFont(font);
    // trek.exe / stbof.res
    lblTREK.setFont(font);
    lblSTBOF.setFont(font);
    txtTREK.setFont(font);
    txtSTBOF.setFont(font);
    btnTREK.setFont(font);
    btnSTBOF.setFont(font);
    // recent
    lblRecentFiles.setFont(font);
    lstRecentFiles.setFont(font);
    // savegame
    lblSavegames.setFont(font);
    lstSavegames.setFont(font);

    lblWelcome.setHorizontalAlignment(JLabel.CENTER);
    lblVersion.setHorizontalAlignment(JLabel.CENTER);
    lblUpdate.setHorizontalAlignment(JLabel.CENTER);
    txtTREK.setEditable(false);
    txtTREK.setBackground(getBackground());
    txtTREK.setBorder(BorderFactory.createEmptyBorder());
    txtSTBOF.setEditable(false);
    txtSTBOF.setBackground(getBackground());
    txtSTBOF.setBorder(BorderFactory.createEmptyBorder());
    lstRecentFiles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lstSavegames.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lstRecentFiles.setCellRenderer(lstRecentFiles_Renderer);
    lstSavegames.setCellRenderer(lstSavegames_Renderer);

    if (askUpdate) {
      pnlAskUpdate = new JPanel();
      lblAskUpdate = new JLabel(Language.getString("StartPagePanel.15"));
      btnAgreeUpdate = new JButton(Language.getString("StartPagePanel.16")); //$NON-NLS-1$
    }
  }

  private void addListeners() {
    if (btnAgreeUpdate != null) {
      btnAgreeUpdate.addActionListener(new CBActionListener(x -> {
          // enable automatic updates
          UE.SETTINGS.setProperty(SettingsManager.CHECK_FOR_UPDATES, true);
          // set in progress label
          lblUpdate.setText(Language.getString("StartPagePanel.4"));
          // issue immediate update check
          UE.checkForUpdate(true);
          // remove enable updates question
          pnlGUI.remove(pnlAskUpdate);
          pnlAskUpdate = null;
          lblAskUpdate = null;
          btnAgreeUpdate = null;
      }));
    }

    // view options
    btnChangelog.addActionListener(new CBActionListener(x -> UE.WINDOW.showChangelog()));
    btnReadme.addActionListener(new CBActionListener(x -> UE.WINDOW.showReadme()));
    btnFAQ.addActionListener(new CBActionListener(x -> UE.WINDOW.showFAQ()));
    btnHelp.addActionListener(new CBActionListener(x -> UE.WINDOW.showHelp()));
    btnAFC.addActionListener(new CBActionListener(x -> UE.WINDOW.openAfcWebsite()));
    btnGitlabs.addActionListener(new CBActionListener(x -> UE.WINDOW.openGitWebsite()));

    // trek.exe / stbof.res
    btnSTBOF.addActionListener(new CBActionListener(x -> UE.WINDOW.openFile(txtSTBOF.getText())));
    btnTREK.addActionListener(new CBActionListener(x -> UE.WINDOW.openFile(txtTREK.getText())));

    // recent
    lstRecentFiles.addListSelectionListener(new CBListSelectionListener(x -> {
      if (!loading) {
        String sel = lstRecentFiles.getSelectedValue();
        if (sel != null && !sel.equals(NONE))
          UE.WINDOW.openFile(lstRecentFiles.getSelectedValue());
      }
    }));
    lstRecentFiles.addMouseMotionListener(new MouseAdapter() {
      @Override
      public void mouseMoved(MouseEvent me) {
        Point p = new Point(me.getX(),me.getY());
        int index = lstRecentFiles.locationToIndex(p);
        if (lstRecentFiles_Renderer.updateHoverIndex(index))
          lstRecentFiles.repaint();
      }
    });
    lstRecentFiles.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseExited(MouseEvent e) {
        lstRecentFiles_Renderer.updateHoverIndex(-1);
        lstRecentFiles.repaint();
      }
    });

    // savegames
    lstSavegames.addListSelectionListener(new CBListSelectionListener(x -> {
      if (!loading) {
        String sel = lstSavegames.getSelectedValue();
        if (sel != null && !sel.equals(NONE))
          UE.WINDOW.openFile(lstSavegames.getSelectedValue());
      }
    }));
    lstSavegames.addMouseMotionListener(new MouseAdapter() {
      @Override
      public void mouseMoved(MouseEvent me) {
        Point p = new Point(me.getX(),me.getY());
        int index = lstSavegames.locationToIndex(p);
        if (lstSavegames_Renderer.updateHoverIndex(index))
          lstSavegames.repaint();
      }
    });
    lstSavegames.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseExited(MouseEvent e) {
        lstSavegames_Renderer.updateHoverIndex(-1);
        lstSavegames.repaint();
      }
    });
  }

  private void placeComponents() {
    /*LAYOUT*/
    setLayout(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;

    // view options panel
    JPanel pnlViewOptions = new JPanel(new GridBagLayout());
    {
      c.weightx = 1;
      pnlViewOptions.add(btnChangelog, c);
      c.gridx++;
      pnlViewOptions.add(btnReadme, c);
      c.gridx++;
      pnlViewOptions.add(btnFAQ, c);
      c.gridx++;
      pnlViewOptions.add(btnHelp, c);
      c.gridx++;
      pnlViewOptions.add(btnAFC, c);
      c.gridx++;
      pnlViewOptions.add(btnGitlabs, c);
    }

    c.gridx = 0;
    c.insets = new Insets(5, 5, 5, 5);

    // recent files and savegames
    JPanel pnlFileLists = new JPanel(new GridBagLayout());
    {
      // anchor north to not select items on empty list space
      c.anchor = GridBagConstraints.NORTH;
      c.fill = GridBagConstraints.HORIZONTAL;
      pnlFileLists.add(lblRecentFiles, c);
      c.gridy++;
      pnlFileLists.add(lstRecentFiles, c);
      c.gridx++;
      c.gridy = 0;
      pnlFileLists.add(lblSavegames, c);
      c.gridy++;
      pnlFileLists.add(lstSavegames, c);

      // fill full cell with list background color
      c.fill = GridBagConstraints.BOTH;
      c.weighty = 1;
      JPanel bgRF = new JPanel();
      bgRF.setBackground(lstRecentFiles.getBackground());
      pnlFileLists.add(bgRF, c);
      c.gridx = 0;
      JPanel bgSG = new JPanel();
      bgSG.setBackground(lstSavegames.getBackground());
      pnlFileLists.add(bgSG, c);
    }

    c.weightx = 0;
    c.weighty = 0;

    // middle gui panel
    {
      // update
      if (askUpdate) {
        c.gridx = 0;
        c.gridy++;
        c.gridwidth = 4;
        pnlAskUpdate.setLayout(new BoxLayout(pnlAskUpdate, BoxLayout.X_AXIS));
        pnlAskUpdate.add(Box.createHorizontalGlue());
        pnlAskUpdate.add(lblAskUpdate);
        pnlAskUpdate.add(Box.createHorizontalStrut(10));
        pnlAskUpdate.add(btnAgreeUpdate);
        pnlAskUpdate.add(Box.createHorizontalGlue());
        pnlGUI.add(pnlAskUpdate, c);
      }

      // trek.exe / stbof.res
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 1;
      c.insets.bottom = 0;
      c.weightx = 0;
      pnlGUI.add(lblTREK, c);
      c.gridx++;
      c.gridwidth = 2;
      c.weightx = 1;
      pnlGUI.add(txtTREK, c);
      c.gridx += 2;
      c.gridwidth = 1;
      c.weightx = 0;
      pnlGUI.add(btnTREK, c);
      c.gridx = 0;
      c.gridy++;
      c.insets.top = 0;
      pnlGUI.add(lblSTBOF, c);
      c.gridx++;
      c.gridwidth = 2;
      c.weightx = 1;
      pnlGUI.add(txtSTBOF, c);
      c.gridx += 2;
      c.gridwidth = 1;
      c.weightx = 0;
      pnlGUI.add(btnSTBOF, c);
    }

    // welcome
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.insets.top = 10;
    c.weightx = 1;
    c.weighty = 0;
    add(lblWelcome, c);

    // version
    c.gridy++;
    c.insets.top = 5;
    add(lblVersion, c);
    c.gridy++;
    c.insets.top = 2;
    add(lblUpdate, c);

    // view options
    c.gridy++;
    c.insets.top = 10;
    c.insets.left = 10;
    c.insets.right = 10;
    c.weightx = 1;
    c.weighty = 0;
    add(pnlViewOptions, c);

    // middle gui panel
    c.gridy++;
    c.insets.top = 5;
    c.insets.left = 5;
    c.insets.right = 5;
    add(pnlGUI, c);

    // recent
    c.gridy++;
    c.insets.top = 10;
    c.insets.bottom = 5;
    c.weighty = 1;
    add(pnlFileLists, c);
  }

  private void loadSettings() {
    loading = true;

    // welcome
    lblWelcome.setText(Language.getString("StartPagePanel.1")); //$NON-NLS-1$

    // version info
    AppVersion version = UE.getVersion();
    val versionDate = version.getDateTime().get();
    String release = DateTimeFormatter.RFC_1123_DATE_TIME.format(versionDate);
    String versionInfo = Language.getString("StartPagePanel.2")
      .replace("%1", version.getVersionString())
      .replace("%2", release);
    lblVersion.setText(versionInfo);

    // update info
    if (UE.UPDATE_CHECKER.alreadyChecked()) {
      // already checked
      notifyUpdate(UE.UPDATE_CHECKER.lastResult());
    } else {
      // auto update in progress or disabled
      boolean updatesEnabled = UE.SETTINGS.getBooleanProperty(SettingsManager.CHECK_FOR_UPDATES);
      lblUpdate.setText(Language.getString(updatesEnabled ?
        "StartPagePanel.4" : "StartPagePanel.3"));
    }

    // trek.exe / stbof.res
    String exePath = UE.SETTINGS.getProperty(SettingsManager.EXE);
    boolean foundExe = !StringTools.isNullOrEmpty(exePath);
    String stbofPath = UE.SETTINGS.getProperty(SettingsManager.STBOF);
    boolean foundStbof = !StringTools.isNullOrEmpty(stbofPath);
    txtTREK.setText(exePath);
    btnTREK.setEnabled(foundExe);
    txtSTBOF.setText(stbofPath);
    btnSTBOF.setEnabled(foundStbof);

    // recent
    String[] recentFiles = UE.SETTINGS.getRecentFiles();

    // make sure to set some list data to not raise a null pointer exception
    // when determining the preferred size in MainPanel.displayPanelImpl
    if (recentFiles == null || recentFiles.length == 0)
      recentFiles = new String[] {NONE};
    lstRecentFiles.setListData(recentFiles);

    // savegames
    String[] saves = null;
    if (foundExe || foundStbof) {
      Path dir = Paths.get(foundExe ? exePath : stbofPath).getParent();
      saves = dir.toFile().list(FileFilters.Sav);
      for (int i = 0; i < saves.length; ++i)
        saves[i] = dir.resolve(saves[i]).toString();
    }

    // make sure to set some list data to not raise a null pointer exception
    // when determining the preferred size in MainPanel.displayPanelImpl
    if (saves == null || saves.length == 0)
      saves = new String[] {NONE};
    lstSavegames.setListData(saves);

    loading = false;
  }

  @Override
  public String menuCommand() {
    return MenuCommand.StartPage;
  }

  @Override
  public boolean hasChanges() {
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  public void notifyUpdate(AppVersion updateInfo) {
    // the user is already prompted by UE.checkForUpdate,
    // therefore just update the label
    // on whether an update was found or not
    String txt = updateInfo != null
      ? Language.getString("StartPagePanel.6")
        .replace("%1", updateInfo.getVersionString())
      : Language.getString("StartPagePanel.5");
    lblUpdate.setText(txt);
  }

  @Override
  public void reload() throws IOException {
    UE.SETTINGS.loadSettings();

    // reload language
    String lang = UE.SETTINGS.getProperty(SettingsManager.LANGUAGE);
    if (!StringTools.equals(lang, Language.getLanguage())) {
      Language.loadLanguage(lang);
    }

    loadSettings();
  }

  @Override
  public void finalWarning() {
  }
}
