package ue.gui.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Credits panel.
 */
public class CreditsPanel extends JPanel {

  private ArrayList<String> CREDITS; //credits to show
  private int SPEED; //in miliseconds
  private Font FONT; //the font used
  private boolean BEGIN = true;
  private Color COLOR;
  private Dimension SIZE;
  private GridLayout LAYOUT; //layout manager
  private javax.swing.Timer TIMER; //timer-executes auto-scrolling
  private ActionListener taskPerformer = null; //performs auto-scrolling
  private JPanel MYCRED;
  private JPanel PARENT;

  /**
   * Constructs an empty CreditsPanel
   */
  public CreditsPanel(String[] str, Font fnt, Color col, int speed, Dimension siz) {
    super();
    //super duper
    CREDITS = new ArrayList<String>();
    for (int i = 0; i < str.length; i++) {
      CREDITS.add(str[i]);
    }
    SPEED = speed;
    LAYOUT = new GridLayout();
    FONT = fnt;
    COLOR = col;
    MYCRED = new JPanel();
    MYCRED.setLayout(LAYOUT);
    SIZE = siz;
    setPreferredSize(siz);
    add(MYCRED);
    PARENT = this;
    resetMe();
    //oh oh
    taskPerformer = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent evt) {
        Point pnt = MYCRED.getLocation();
        Dimension siza = PARENT.getSize();
        Dimension siz = MYCRED.getSize();
        pnt.x = 0;
        if (BEGIN) {
          pnt.y = 0 - siz.height;
          BEGIN = false;
        } else {
          if (pnt.y + siz.height > 0) {
            pnt.y = pnt.y - SPEED;
          } else {
            pnt.y = siza.height;
          }
        }
        MYCRED.setLocation(pnt);
      }
    };
    TIMER = new javax.swing.Timer(75, taskPerformer);
    TIMER.start();
  }

  @Override
  public void setPreferredSize(Dimension dim) {
    // make sure this panel is wide enough
    FontMetrics metrics = (new JLabel()).getFontMetrics(FONT);
    int len = 0;
    int tmp;
    for (int i = 0; i < CREDITS.size(); i++) {
      tmp = metrics.stringWidth(CREDITS.get(i));
      if (tmp > len) {
        len = tmp;
      }
    }
    dim.width = len;
    super.setPreferredSize(dim);
    setMaximumSize(dim);
    setSize(dim);
    SIZE = dim;
  }

  public void addString(String str) {
    CREDITS.add(str);
    resetMe();
  }

  public void removeString(String str) {
    CREDITS.remove(str);
    resetMe();
  }

  public void setCreditsFont(Font fnt) {
    FONT = fnt;
    resetMe();
  }

  public void setColor(Color col) {
    COLOR = col;
    resetMe();
  }

  public void setSpeed(int sp) {
    SPEED = sp;
  }

  public void resetMe() {
    MYCRED.removeAll();
    //set cols
    LAYOUT.setRows(CREDITS.size());
    LAYOUT.setVgap(4);
    LAYOUT.setColumns(1);
    //add
    for (int i = 0; i < CREDITS.size(); i++) {
      JLabel lbl = new JLabel(CREDITS.get(i));
      lbl.setFont(FONT);
      lbl.setForeground(COLOR);
      MYCRED.add(lbl, Integer.toString(i));
    }
    setPreferredSize(SIZE);
    BEGIN = true;
  }
}
