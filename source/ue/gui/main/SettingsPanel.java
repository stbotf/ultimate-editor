package ue.gui.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import ue.UE;
import ue.gui.common.DefaultPanel;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.app.AppVersion;
import ue.util.data.ID;
import ue.util.data.StringTools;

/**
 * The user interface (MainPanel) for managing the settings
 */
public class SettingsPanel extends DefaultPanel {

  private static final String[] LANGUAGE_CODE = {"en", "de", "en", "sl"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
  private static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.RFC_1123_DATE_TIME;

  // ui components
  private JButton btnFONT = new JButton();
  private JButton btnSTBR = new JButton();
  private JButton btnTREK = new JButton();
  private JButton btnUPDATE_NOW = new JButton();
  private JCheckBox chkAutoPath = new JCheckBox();
  private JCheckBox chkDEV_UPDATES = new JCheckBox();
  private JCheckBox chkEXPERIMENTAL = new JCheckBox();
  private JCheckBox chkSAVE_SND = new JCheckBox();
  private JCheckBox chkUPDATE_CHECK = new JCheckBox();
  private JComboBox<ID> cmbLANGUAGE = new JComboBox<ID>();
  private JLabel lblAdvanced = new JLabel();
  private JLabel lblBACKUPS = new JLabel();
  private JLabel lblFONT = new JLabel();
  private JLabel lblFONT_DESC = new JLabel();
  private JLabel lblGamePaths = new JLabel();
  private JLabel lblLANGUAGE = new JLabel();
  private JLabel lblLastUpdateCheck = new JLabel();
  private JLabel lblSTBOF = new JLabel();
  private JLabel lblTREK = new JLabel();
  private JLabel lblUpdates = new JLabel();
  private JLabel lblUpdateInterval = new JLabel();
  private JTextField txtSTBOF = new JTextField(40);
  private JTextField txtTREK = new JTextField(40);
  private JSpinner spiBACKUPS = new JSpinner(new SpinnerNumberModel(-1, -1, Integer.MAX_VALUE, 1));
  private JSpinner spiUpdateInterval = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));

  // data
  private boolean init = true;

  /**
   * Constructor.
   */
  public SettingsPanel() {
    setupComponents();
    loadLabels();
    placeComponents();
    loadSettings();
    addListeners();
  }

  private void loadLabels() {
    btnFONT.setText(Language.getString("SettingsPanel.18")); //$NON-NLS-1$
    btnSTBR.setText(Language.getString("SettingsPanel.0")); //$NON-NLS-1$
    btnTREK.setText(Language.getString("SettingsPanel.0")); //$NON-NLS-1$
    btnUPDATE_NOW.setText(Language.getString("SettingsPanel.1")); //$NON-NLS-1$
    chkAutoPath.setText(Language.getString("SettingsPanel.4")); //$NON-NLS-1$
    chkDEV_UPDATES.setText(Language.getString("SettingsPanel.13")); //$NON-NLS-1$
    chkEXPERIMENTAL.setText(Language.getString("SettingsPanel.16")); //$NON-NLS-1$
    chkSAVE_SND.setText(Language.getString("SettingsPanel.2")); //$NON-NLS-1$
    chkUPDATE_CHECK.setText(Language.getString("SettingsPanel.12")); //$NON-NLS-1$
    lblAdvanced.setText(Language.getString("SettingsPanel.22")); //$NON-NLS-1$
    lblBACKUPS.setText(Language.getString("SettingsPanel.15")); //$NON-NLS-1$
    lblFONT.setText(Language.getString("SettingsPanel.7")); //$NON-NLS-1$
    lblGamePaths.setText(Language.getString("SettingsPanel.20")); //$NON-NLS-1$
    lblLANGUAGE.setText(Language.getString("SettingsPanel.8")); //$NON-NLS-1$
    lblSTBOF.setText(Language.getString("SettingsPanel.3")); //$NON-NLS-1$
    lblTREK.setText(Language.getString("SettingsPanel.6")); //$NON-NLS-1$
    lblUpdates.setText(Language.getString("SettingsPanel.21")); //$NON-NLS-1$
    lblUpdateInterval.setText(Language.getString("SettingsPanel.23")); //$NON-NLS-1$
  }

  private void setupComponents() {
    Dimension d = spiBACKUPS.getPreferredSize();
    d.width = 50;
    spiBACKUPS.setPreferredSize(d);

    d = spiUpdateInterval.getPreferredSize();
    d.width = 50;
    spiUpdateInterval.setPreferredSize(d);

    cmbLANGUAGE.addItem(new ID(Language.getString("SettingsPanel.9"), 0)); //$NON-NLS-1$
    for (int i = 1; i < LANGUAGE_CODE.length; i++) {
      Locale locale = new Locale(LANGUAGE_CODE[i]);
      String lang = locale.getDisplayLanguage(locale);
      cmbLANGUAGE.addItem(new ID(StringTools.toUpperFirstChar(lang), i));
    }
  }

  private void addListeners() {
    spiBACKUPS.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        if (init)
          return;

        try {
          spiBACKUPS.commitEdit();
        } catch (ParseException pe) {
          // on error getValue will return last valid value
          // which we are ok with
        }

        UE.SETTINGS.setProperty(SettingsManager.NUMBER_OF_BACKUPS, (Integer) spiBACKUPS.getValue());

        if (UE.SETTINGS.getBooleanProperty(SettingsManager.BACKUP_WARNING)) {
          JOptionPane.showMessageDialog(UE.WINDOW,
              "Be warned, backup files consume a lot of disk space. They're essentialy additional copies of the game.",
              UE.APP_NAME, JOptionPane.INFORMATION_MESSAGE);

          UE.SETTINGS.setProperty(SettingsManager.BACKUP_WARNING, false);
        }
      }
    });

    spiUpdateInterval.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        if (init)
          return;

        try {
          spiUpdateInterval.commitEdit();
        } catch (ParseException pe) {
          // on error getValue will return last valid value
          // which we are ok with
        }

        UE.SETTINGS.setProperty(SettingsManager.UPDATE_INTERVAL, (Integer) spiUpdateInterval.getValue());
      }
    });

    chkUPDATE_CHECK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (init)
          return;

        boolean b = !UE.SETTINGS.getBooleanProperty(SettingsManager.CHECK_FOR_UPDATES);
        UE.SETTINGS.setProperty(SettingsManager.CHECK_FOR_UPDATES, b);
      }
    });

    chkDEV_UPDATES.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (init)
          return;

        boolean b = !UE.SETTINGS.getBooleanProperty(SettingsManager.INCLUDE_DEV_UPDATES);
        UE.SETTINGS.setProperty(SettingsManager.INCLUDE_DEV_UPDATES, b);
      }
    });

    btnUPDATE_NOW.addActionListener(new CBActionListener(x -> UE.checkForUpdate(true)));

    chkEXPERIMENTAL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (init)
          return;

        boolean b = !UE.SETTINGS.getBooleanProperty(SettingsManager.EXPERIMENTAL);
        UE.SETTINGS.setProperty(SettingsManager.EXPERIMENTAL, b);
      }
    });

    btnSTBR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.STBOF)));
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle(UE.APP_NAME + " - " //$NON-NLS-1$
            + Language.getString("SettingsPanel.5")); //$NON-NLS-1$
        fc.setMultiSelectionEnabled(false);

        javax.swing.filechooser.FileFilter filter = new javax.swing.filechooser.FileFilter() {
          @Override
          public boolean accept(File f) {
            if (f.isDirectory()) {
              return true;
            }

            String name = f.getName();
            name = name.toLowerCase();
            return name.endsWith(".res"); //$NON-NLS-1$
          }

          @Override
          public String getDescription() {
            return Language.getString("SettingsPanel.10"); //$NON-NLS-1$
          }
        };

        fc.setFileFilter(filter);

        int retValue = fc
            .showDialog(UE.WINDOW, Language.getString("SettingsPanel.5")); //$NON-NLS-1$
        if (retValue == JFileChooser.APPROVE_OPTION) {
          File fil = fc.getSelectedFile();
          if (fil != null) {
            UE.SETTINGS.setProperty(SettingsManager.STBOF, fil.getPath());
            txtSTBOF.setText(fil.getPath());
          }
        }
      }
    });

    btnTREK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File(UE.SETTINGS.getProperty(SettingsManager.EXE)));
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setDialogTitle(UE.APP_NAME + " - " //$NON-NLS-1$
            + Language.getString("SettingsPanel.5")); //$NON-NLS-1$
        fc.setMultiSelectionEnabled(false);
        javax.swing.filechooser.FileFilter filter = new javax.swing.filechooser.FileFilter() {
          @Override
          public boolean accept(File f) {
            if (f.isDirectory()) {
              return true;
            }

            String name = f.getName();
            name = name.toLowerCase();
            return name.endsWith(".exe"); //$NON-NLS-1$
          }

          @Override
          public String getDescription() {
            return Language.getString("SettingsPanel.11"); //$NON-NLS-1$
          }
        };

        fc.setFileFilter(filter);
        int retValue = fc
            .showDialog(UE.WINDOW, Language.getString("SettingsPanel.5")); //$NON-NLS-1$
        if (retValue == JFileChooser.APPROVE_OPTION) {
          File fil = fc.getSelectedFile();
          if (fil != null) {
            UE.SETTINGS.setProperty(SettingsManager.EXE, fil.getPath());
            txtTREK.setText(fil.getPath());
          }
        }
      }
    });

    cmbLANGUAGE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (init)
          return;

        int i = cmbLANGUAGE.getSelectedIndex();
        if (i >= 0) {
          try {
            if (!init)
              UE.SETTINGS.setProperty(SettingsManager.LANGUAGE, LANGUAGE_CODE[i]);

            Language.loadLanguage(LANGUAGE_CODE[i]);
            updateText();
          } catch (Throwable e) {
            e.printStackTrace();
          }
        }
      }
    });

    btnFONT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        FontDialog dialog = new FontDialog(btnFONT.getFont());
        dialog.setVisible(true);

        if (dialog.getSelectedFont() != null) {
          try {
            Font def = dialog.getSelectedFont();

            UE.SETTINGS.setProperty(SettingsManager.FONT_SIZE, def.getSize());
            UE.SETTINGS.setProperty(SettingsManager.FONT_STYLE, def.getStyle());
            UE.SETTINGS.setProperty(SettingsManager.FONT_NAME, def.getName());
            updateText();
          } catch (Throwable e) {
            e.printStackTrace();
          }
        }
      }
    });
  }

  private void placeComponents() {
    /*LAYOUT*/
    setLayout(new GridBagLayout());

    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 0;

    // gui
    JPanel pnlGUI = new JPanel(new GridBagLayout());
    {
      /*ADD*/
      // language & font
      pnlGUI.add(lblLANGUAGE, c);
      c.gridx++;
      pnlGUI.add(cmbLANGUAGE, c);

      JPanel pnlFont = new JPanel(new FlowLayout(FlowLayout.RIGHT, 9, 5));
      pnlFont.add(lblFONT);
      pnlFont.add(lblFONT_DESC);
      c.gridx++;
      pnlGUI.add(pnlFont, c);
      c.gridx++;
      pnlGUI.add(btnFONT, c);

      // paths
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 2;
      c.insets.top = 15;
      c.insets.bottom = 2;
      pnlGUI.add(lblGamePaths, c);
      c.gridy++;
      c.gridwidth = 1;
      c.insets.top = 5;
      c.insets.bottom = 0;
      pnlGUI.add(lblSTBOF, c);
      c.gridx++;
      c.gridwidth = 2;
      c.weightx = 1;
      pnlGUI.add(txtSTBOF, c);
      c.gridx += 2;
      c.gridwidth = 1;
      c.weightx = 0;
      pnlGUI.add(btnSTBR, c);
      c.gridx = 0;
      c.gridy++;
      pnlGUI.add(lblTREK, c);
      c.gridx++;
      c.gridwidth = 2;
      c.weightx = 1;
      pnlGUI.add(txtTREK, c);
      c.gridx += 2;
      c.gridwidth = 1;
      c.weightx = 0;
      pnlGUI.add(btnTREK, c);

      // updates
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 2;
      c.insets.top = 20;
      pnlGUI.add(lblUpdates, c);
      c.gridy++;
      c.insets.top = 5;
      pnlGUI.add(chkUPDATE_CHECK, c);
      c.gridx = 2;
      c.gridwidth = 1;
      pnlGUI.add(chkDEV_UPDATES, c);
      c.gridx++;
      pnlGUI.add(btnUPDATE_NOW, c);
      // update interval
      c.gridx = 0;
      c.gridy++;
      c.insets.top = 0;
      JPanel pnlUpdateInterval = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
      pnlUpdateInterval.add(lblUpdateInterval);
      pnlUpdateInterval.add(Box.createHorizontalStrut(3));
      pnlUpdateInterval.add(spiUpdateInterval);
      pnlGUI.add(pnlUpdateInterval, c);
      c.gridx = 2;
      c.gridwidth = 1;
      pnlGUI.add(lblLastUpdateCheck, c);

      // advanced
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 2;
      c.insets.top = 20;
      pnlGUI.add(lblAdvanced, c);
      c.gridy++;
      c.insets.top = 5;
      c.insets.bottom = 0;
      pnlGUI.add(chkAutoPath, c);
      c.gridx += 2;
      c.gridwidth = 1;
      pnlGUI.add(chkSAVE_SND, c);
      c.gridx = 0;
      c.gridy++;
      c.gridwidth = 2;
      c.insets.top = 2;
      pnlGUI.add(chkEXPERIMENTAL, c);
      // backups
      c.gridx += 2;
      c.gridwidth = 1;
      JPanel pnlBKP = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
      pnlBKP.add(lblBACKUPS);
      pnlBKP.add(Box.createHorizontalStrut(3));
      pnlBKP.add(spiBACKUPS);
      pnlGUI.add(pnlBKP, c);
    }

    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    add(pnlGUI, c);
  }

  private int findLanguage(String langCode) {
    int k = -1;
    for (int i = 0; i < LANGUAGE_CODE.length; i++) {
      if (LANGUAGE_CODE[i].equals(langCode)) {
        k = i;
        break;
      }
    }
    return k;
  }

  private void loadSettings() {
    // skip change events
    init = true;

    // font
    Font font = UE.SETTINGS.getDefaultFont();
    Font bigFont = font.deriveFont(Font.BOLD);

    String fontDesc = font.getName() + ", " + font.getSize();
    if (font.isPlain())
      fontDesc += ", plain";
    if (font.isItalic())
      fontDesc += ", italic";
    if (font.isBold())
      fontDesc += ", bold";
    lblFONT_DESC.setText(fontDesc);

    updateFont(font, bigFont);

    // current language
    String lang = Language.getLanguage();
    int k = findLanguage(lang);
    if (k >= 0)
      cmbLANGUAGE.setSelectedIndex(k);

    // game paths
    txtSTBOF.setText(UE.SETTINGS.getProperty(SettingsManager.STBOF));
    txtTREK.setText(UE.SETTINGS.getProperty(SettingsManager.EXE));

    // updates
    chkUPDATE_CHECK.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.CHECK_FOR_UPDATES));
    chkDEV_UPDATES.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.INCLUDE_DEV_UPDATES));
    spiUpdateInterval.getModel().setValue(UE.SETTINGS.getIntProperty(SettingsManager.UPDATE_INTERVAL));
    updateLastCheck();

    // advanced
    chkAutoPath.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.AUTO_PATH));
    chkEXPERIMENTAL.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.EXPERIMENTAL));
    chkSAVE_SND.setSelected(UE.SETTINGS.getBooleanProperty(SettingsManager.STORE_SND_DESC));
    spiBACKUPS.getModel().setValue(UE.SETTINGS.getIntProperty(SettingsManager.NUMBER_OF_BACKUPS));

    init = false;
  }

  private void updateFont(Font font, Font fontTopic) {
    btnFONT.setFont(font);
    btnSTBR.setFont(font);
    btnTREK.setFont(font);
    btnUPDATE_NOW.setFont(font);
    chkAutoPath.setFont(font);
    chkDEV_UPDATES.setFont(font);
    chkEXPERIMENTAL.setFont(font);
    chkSAVE_SND.setFont(font);
    chkUPDATE_CHECK.setFont(font);
    cmbLANGUAGE.setFont(font);
    lblAdvanced.setFont(fontTopic);
    lblBACKUPS.setFont(font);
    lblFONT.setFont(font);
    lblFONT_DESC.setFont(font);
    lblGamePaths.setFont(fontTopic);
    lblLANGUAGE.setFont(font);
    lblLastUpdateCheck.setFont(font);
    lblSTBOF.setFont(font);
    lblTREK.setFont(font);
    lblUpdates.setFont(fontTopic);
    lblUpdateInterval.setFont(font);
    spiBACKUPS.setFont(font);
    spiUpdateInterval.setFont(font);
    txtSTBOF.setFont(font);
    txtTREK.setFont(font);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Settings;
  }

  @Override
  public boolean hasChanges() {
    return UE.SETTINGS.madeChanges();
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    if (UE.SETTINGS.madeChanges())
      changes.add(null, UE.SETTINGS.getName());
  }

  public void notifyUpdate(AppVersion updateInfo) {
    updateLastCheck();
  }

  @Override
  public void reload() throws IOException {
    UE.SETTINGS.loadSettings();

    // reload language
    String lang = UE.SETTINGS.getProperty(SettingsManager.LANGUAGE);
    if (!StringTools.equals(lang, Language.getLanguage())) {
      Language.loadLanguage(lang);
      // update label translations
      loadLabels();
    }

    loadSettings();
  }

  @Override
  public void finalWarning() {
    //stbof
    UE.SETTINGS.setProperty(SettingsManager.STBOF, txtSTBOF.getText());
    //trek
    UE.SETTINGS.setProperty(SettingsManager.EXE, txtTREK.getText());
    UE.SETTINGS.setProperty(SettingsManager.AUTO_PATH, chkAutoPath.isSelected());
    // snd descriptions
    UE.SETTINGS.setProperty(SettingsManager.STORE_SND_DESC, chkSAVE_SND.isSelected());
  }

  private void updateText() {
    UE.WINDOW.showPanel(new SettingsPanel());
  }

  private void updateLastCheck() {
    String lastCheck = Language.getString("SettingsPanel.24")
      .replace("%1", UE.UPDATE_CHECKER.lastCheck().format(TIME_FORMAT));
    lblLastUpdateCheck.setText(lastCheck);
  }
}
