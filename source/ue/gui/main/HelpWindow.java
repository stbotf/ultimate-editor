package ue.gui.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.Optional;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;
import org.apache.commons.lang3.StringUtils;
import lombok.val;
import ue.UE;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;

/**
 * Show a window with text.
 */
public class HelpWindow extends JFrame implements HyperlinkListener {

  private static final long serialVersionUID = -3767437591906552937L;

  /**
   * Constructor.
   *
   * @param title The title of the panel whose help will be displayed.
   */
  public HelpWindow(String title, String path, String filename) {
    // window
    this.setTitle(title);
    this.setIconImage(UE.ICON);

    // load html
    val htmlContent = getHtmlContent(path, filename);
    this.setTitle(
        getDocumentTitle(htmlContent).map(subtitle -> title + " - " + subtitle).orElse(title));

    // content
    JTextPane helpPane = new JTextPane();
    helpPane.setEditorKit(new HTMLEditorKit());
    helpPane.setEditable(false);
    helpPane.setOpaque(true);
    helpPane.addHyperlinkListener(this);
    helpPane.setText(htmlContent);

    // add to frame
    this.setLayout(new BorderLayout(0, 0));
    this.add(new JScrollPane(helpPane), BorderLayout.CENTER);

    // show
    val d = Toolkit.getDefaultToolkit().getScreenSize();
    this.setSize(new Dimension(400, 480));

    this.setLocation((d.width - 400) / 2, (d.height - 480) / 2);
    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    this.setVisible(true);
  }

  private String getHtmlContent(String path, String filename) {
    try {
      return UE.FILES.getHelpFile(path, filename);
    } catch (IOException e) {
      Dialogs.displayError(e);
      return "<p><b>Could not load help file.</b></p><br>Error: " + e.getLocalizedMessage();
    }
  }

  private Optional<String> getDocumentTitle(String htmlContent) {
    if (!StringUtils.isBlank(htmlContent)) {
      int start = htmlContent.indexOf("<title>");
      int end = htmlContent.indexOf("</title>");

      if (start >= 0 && end >= 0) {
        return Optional.of(htmlContent.substring(start + 7, end));
      }
    }

    return Optional.empty();
  }

  @Override
  public void hyperlinkUpdate(HyperlinkEvent event) {
    if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
      if (event.getURL() == null) {
        // same page (help)
        ((JTextPane) event.getSource()).scrollToReference(event.getDescription().substring(1));
      } else {
        // external page (call default browser)
        try {
          GuiTools.openInDefaultBrowser(event.getURL().toString());
        } catch (IOException e) {
          Dialogs.displayError(e);
        }
      }
    }
  }
}
