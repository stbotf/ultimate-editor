package ue.gui.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.text.ParseException;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import ue.UE;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.gui.util.event.CBListSelectionListener;

public class FontDialog extends JDialog {

  private JLabel lblFont = new JLabel("Font:");
  private JLabel lblSize = new JLabel("Font size:");
  private JList<String> lstFont = new JList<String>();
  private JSpinner spiSize;
  private JCheckBox chkItalic;
  private JCheckBox chkBold;
  private JScrollPane scrFont;
  private JButton btnOK = new JButton("Ok");
  private JButton btnCancel = new JButton("Cancel");
  private JLabel lblPreview = new JLabel("AaBbCcDdGgHhIiJj");

  private boolean selected = false;

  public FontDialog(Font def) {
    super(UE.WINDOW, UE.APP_NAME + " - Font Chooser", true);

    setupComponents(def);
    placeComponents();
    setupActionListeners();

    pack();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (int) (screenSize.getWidth() / 2 - 100);
    int y = (int) (screenSize.getHeight() / 2 - 100);
    setLocation(new Point(x, y));
  }

  public Font getSelectedFont() {
    return selected ? getPreviewFont() : null;
  }

  private void setupComponents(Font def) {
    scrFont = new JScrollPane(lstFont);
    spiSize = new JSpinner(new SpinnerNumberModel(def.getSize(), 8, 72, 1));
    chkItalic = new JCheckBox("Italic", def.isItalic());
    chkBold = new JCheckBox("Bold", def.isBold());

    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    String fontNames[] = ge.getAvailableFontFamilyNames();
    lstFont.setListData(fontNames);
    lstFont.setSelectedValue(def.getName(), true);
    lstFont.ensureIndexIsVisible(lstFont.getSelectedIndex());

    lblFont.setFont(def);
    lblSize.setFont(def);
    lstFont.setFont(def);
    spiSize.setFont(def);
    btnOK.setFont(def);
    btnCancel.setFont(def);
    lblPreview.setFont(def);

    chkItalic.setFont(new Font(def.getName(), def.getStyle() | Font.ITALIC, def.getSize()));
    chkBold.setFont(new Font(def.getName(), def.getStyle() | Font.BOLD, def.getSize()));
  }

  private void placeComponents() {
    JPanel pnlCenter = new JPanel(new BorderLayout());

    JPanel pnlTop = new JPanel(new BorderLayout());
    lblFont.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnlTop.add(lblFont, BorderLayout.NORTH);

    JPanel pnlFunc = new JPanel(new BorderLayout());
    JPanel pnlList = new JPanel(new BorderLayout());
    pnlList.add(scrFont, BorderLayout.CENTER);
    pnlList.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    pnlFunc.add(pnlList, BorderLayout.CENTER);

    JPanel pnlRight = new JPanel();
    pnlRight.setLayout(new BoxLayout(pnlRight, BoxLayout.Y_AXIS));

    JPanel pnlRightTop = new JPanel(new GridLayout(4, 1, 5, 5));

    JPanel pnlSize = new JPanel(new BorderLayout());
    lblSize.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    pnlSize.add(lblSize, BorderLayout.WEST);

    JPanel pnlSpiSize = new JPanel(new BorderLayout());
    pnlSpiSize.add(spiSize, BorderLayout.CENTER);
    pnlSpiSize.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

    pnlSize.add(pnlSpiSize, BorderLayout.CENTER);

    pnlRightTop.add(pnlSize);

    chkItalic.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    pnlRightTop.add(chkItalic);
    chkBold.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    pnlRightTop.add(chkBold);

    pnlRight.add(pnlRightTop);
    pnlRight.add(Box.createVerticalGlue());

    pnlFunc.add(pnlRight, BorderLayout.EAST);

    JPanel pnlPreview = new JPanel(new BorderLayout());
    pnlPreview.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    lblPreview.setBorder(BorderFactory.createTitledBorder(
        BorderFactory.createLineBorder(Color.GRAY),
        "Preview:", TitledBorder.DEFAULT_JUSTIFICATION,
        TitledBorder.DEFAULT_POSITION, lstFont.getFont()));

    pnlPreview.add(lblPreview, BorderLayout.CENTER);

    pnlFunc.add(pnlPreview, BorderLayout.SOUTH);

    pnlCenter.add(pnlTop, BorderLayout.NORTH);
    pnlCenter.add(pnlFunc, BorderLayout.CENTER);

    JPanel pnlBottom = new JPanel();
    pnlBottom.setLayout(new BoxLayout(pnlBottom, BoxLayout.X_AXIS));
    pnlBottom.add(Box.createHorizontalGlue());

    JPanel pnlButtons = new JPanel(new GridLayout(1, 2, 5, 5));
    pnlButtons.add(btnOK);
    pnlButtons.add(btnCancel);
    pnlButtons.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

    pnlBottom.add(pnlButtons);

    setLayout(new BorderLayout());
    add(pnlCenter, BorderLayout.CENTER);
    add(pnlBottom, BorderLayout.SOUTH);
  }

  private void setupActionListeners() {
    lstFont.addListSelectionListener(new CBListSelectionListener(x -> {
      if (!x.getValueIsAdjusting() && lblPreview != null)
        lblPreview.setFont(getPreviewFont());
    }));

    spiSize.addChangeListener(new CBChangeListener(x -> {
      if (lblPreview != null)
        lblPreview.setFont(getPreviewFont());
    }));

    chkItalic.addChangeListener(new CBChangeListener(x -> {
      if (lblPreview != null)
        lblPreview.setFont(getPreviewFont());
    }));

    chkBold.addChangeListener(new CBChangeListener(x -> {
      if (lblPreview != null)
        lblPreview.setFont(getPreviewFont());
    }));

    btnOK.addActionListener(new CBActionListener(x -> {
      selected = true;
      setVisible(false);
    }));

    btnCancel.addActionListener(new CBActionListener(x -> setVisible(false)));
  }

  private Font getPreviewFont() {
    if (chkItalic == null || chkBold == null || spiSize == null || lstFont == null)
      return getFont();

    int style = Font.PLAIN;
    if (chkItalic.isSelected())
      style = style | Font.ITALIC;
    if (chkBold.isSelected())
      style = style | Font.BOLD;

    try {
      spiSize.commitEdit();
    } catch (ParseException ex) {
      // ignore parse errors for preview
    }

    int size = (Integer) spiSize.getValue();
    return new Font(lstFont.getSelectedValue(), style, size);
  }
}
