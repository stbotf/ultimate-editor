package ue.gui.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import lombok.val;
import ue.UE;
import ue.service.Language;
import ue.util.app.AppVersion;
import ue.util.data.StringTools;

/**
 * This class is used to display the about window.
 */
public class AboutGUI extends JFrame {

  /**
   * Costructor
   */
  public AboutGUI() {
    AppVersion version = UE.getVersion();
    val versionDate = version.getDateTime().get();

    /*CREATE ITEMS*/
    JLabel lblTITLE = new JLabel(Language.getString("AboutGUI.0")); //$NON-NLS-1$
    JLabel txtTITLE = new JLabel(UE.APP_NAME);
    JLabel lblVERSION = new JLabel(Language.getString("AboutGUI.1")); //$NON-NLS-1$
    JLabel txtVERSION = new JLabel(version.getVersionString());
    JLabel lblReleased = new JLabel(Language.getString("AboutGUI.2")); //$NON-NLS-1$
    JLabel txtReleased = new JLabel(versionDate.format(
      DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));
    JLabel lblCreators = new JLabel(Language.getString("AboutGUI.3")); //$NON-NLS-1$
    JTextArea txtCreators = new JTextArea(""); //$NON-NLS-1$
    /*SETUP ITEMS*/
    txtTITLE.setFont(UE.SETTINGS.getDefaultBigFont()); //the title is bold and bigger
    txtTITLE.setHorizontalAlignment(SwingConstants.CENTER);
    Font def = UE.SETTINGS.getDefaultFont();
    lblTITLE.setFont(def);
    lblVERSION.setFont(def);
    lblReleased.setFont(def);
    lblCreators.setFont(def);
    txtCreators.setFont(def);
    txtVERSION.setFont(def);
    txtReleased.setFont(def);
    txtCreators.setEnabled(false);
    txtCreators.setBackground(getBackground());
    txtCreators.setBorder(BorderFactory.createEmptyBorder());
    txtCreators.setForeground(Color.GRAY);
    txtCreators.setDisabledTextColor(Color.GRAY);
    txtVERSION.setForeground(Color.GRAY);
    txtReleased.setForeground(Color.GRAY);
    lblTITLE.setForeground(Color.DARK_GRAY);
    lblVERSION.setForeground(Color.DARK_GRAY);
    lblReleased.setForeground(Color.DARK_GRAY);
    lblCreators.setForeground(Color.DARK_GRAY);
    txtTITLE.setForeground(Color.DARK_GRAY);
    //credits & authors
    CreditsPanel pnlCRED = new CreditsPanel(UE.APP_HELPERS, def, Color.GRAY, 1,
        new Dimension(100, 150));
    String authors = StringTools.joinNotEmpty(", ", UE.APP_AUTHORS);
    txtCreators.setText(authors);
    /*ADD ITEMS TO FRAME*/
    //general layout settings
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(3, 5, 0, 5);
    c.anchor = GridBagConstraints.NORTH;
    c.fill = GridBagConstraints.BOTH;
    //adding items
    c.gridheight = 1;
    c.gridwidth = 2;
    c.gridx = 0;
    c.gridy = 0;
    add(txtTITLE, c);
    c.gridwidth = 1;
    c.gridy = 1;
    add(lblTITLE, c);
    c.gridx = 1;
    c.gridheight = 2;
    add(txtCreators, c);
    c.gridx = 0;
    c.gridy = 3;
    c.gridheight = 1;
    add(lblVERSION, c);
    c.gridx = 1;
    add(txtVERSION, c);
    c.gridx = 0;
    c.gridy = 4;
    add(lblReleased, c);
    c.gridx = 1;
    add(txtReleased, c);
    c.gridx = 0;
    c.gridy = 5;
    add(lblCreators, c);
    c.gridheight = 2;
    c.gridx = 1;
    c.insets = new Insets(5, 5, 5, 5);
    add(pnlCRED, c);
    /*FRAME SETUP*/
    //icon is the same as main
    setIconImage(UE.ICON);
    setTitle(Language.getString("AboutGUI.4")); //$NON-NLS-1$
    pack();
    setResizable(false);    //this frame is not resisable
    setAlwaysOnTop(true);
    //place at the center of screen
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setLocation(new Point((int) (screenSize.getWidth() / 2 - 125),
        (int) (screenSize.getHeight() / 2 - 125)));
    setVisible(true);
  }
}
