package ue.gui.special;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import ue.UE;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.gui.common.DefaultPanel;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.gui.stbof.gfx.AniLabel;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.gui.util.event.CBChangeListener;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.file.PathHelper;
import ue.util.img.Tga2Ani;
import ue.util.stream.StreamTools;

/**
 * This class is an interface used for creating botf animations.
 */
public class Tga2AniGUI extends DefaultPanel {

  private final String strADD = Language.getString("Tga2AniGUI.0"); //$NON-NLS-1$
  private final String strREMOVE = Language.getString("Tga2AniGUI.1"); //$NON-NLS-1$
  private final String strCLEAR = Language.getString("Tga2AniGUI.2"); //$NON-NLS-1$
  private final String strUP = Language.getString("Tga2AniGUI.3"); //$NON-NLS-1$
  private final String strDOWN = Language.getString("Tga2AniGUI.4"); //$NON-NLS-1$
  private final String strGENERATE = Language.getString("Tga2AniGUI.5"); //$NON-NLS-1$
  private final String strSAVE_AS = Language.getString("Tga2AniGUI.6"); //$NON-NLS-1$

  // edited
  private AniOrCur AOC;
  private Tga2Ani T2A = new Tga2Ani();

  // ui components
  private JButton[] BTN = new JButton[7];
  private AniLabel ANI = new AniLabel();
  private JList<String> LIST = new JList<>();
  private JLabel[] LABELS = new JLabel[15];
  private JSpinner SPEED = new JSpinner();

  public Tga2AniGUI() {
    setupComponents();
    layoutComponents();
    setupListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.CreateAniCur;
  }

  @Override
  public boolean hasChanges() {
    return AOC != null && AOC.madeChanges();
  }

  @Override
  public void reload() throws IOException {
    AOC = null;
    T2A.clear();

    // reset labels
    for (int i = 9; i <= 14; i++) {
      LABELS[i] = new JLabel("0"); //$NON-NLS-1$
    }

    // reset animation
    ANI.setAnimation(null, false);
    LIST.setModel(new DefaultListModel<String>());

    // reset details
    SPEED.setEnabled(false);
    BTN[1].setEnabled(false);
    BTN[2].setEnabled(false);
    BTN[3].setEnabled(false);
    BTN[4].setEnabled(false);
    BTN[5].setEnabled(false);
    BTN[6].setEnabled(false);
  }

  @Override
  public void finalWarning() {
    if (AOC == null)
      return;

    int a = (Integer) SPEED.getValue();
    short shr = a < 1 ? 1 : (short) Integer.max(a, Short.MAX_VALUE);
    AOC.setDelay(shr);
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    if (AOC != null && AOC.madeChanges())
      changes.add(null, AOC.getName());
  }

  private void setupComponents() {
    BTN[0] = new JButton(strADD);
    BTN[1] = new JButton(strREMOVE);
    BTN[2] = new JButton(strCLEAR);
    BTN[3] = new JButton(strUP);
    BTN[4] = new JButton(strDOWN);
    BTN[5] = new JButton(strGENERATE);
    BTN[6] = new JButton(strSAVE_AS);
    LABELS[0] = new JLabel(Language.getString("Tga2AniGUI.7")); //$NON-NLS-1$
    LABELS[1] = new JLabel(Language.getString("Tga2AniGUI.8")); //$NON-NLS-1$
    LABELS[2] = new JLabel(Language.getString("Tga2AniGUI.9")); //$NON-NLS-1$
    LABELS[3] = new JLabel(Language.getString("Tga2AniGUI.10")); //$NON-NLS-1$
    LABELS[4] = new JLabel(Language.getString("Tga2AniGUI.7")); //$NON-NLS-1$
    LABELS[5] = new JLabel(Language.getString("Tga2AniGUI.11")); //$NON-NLS-1$
    LABELS[6] = new JLabel(Language.getString("Tga2AniGUI.12")); //$NON-NLS-1$
    LABELS[7] = new JLabel(Language.getString("Tga2AniGUI.13")); //$NON-NLS-1$
    LABELS[8] = new JLabel(Language.getString("Tga2AniGUI.14")); //$NON-NLS-1$

    for (int i = 9; i <= 14; i++) {
      LABELS[i] = new JLabel("0"); //$NON-NLS-1$
    }

    Font def = UE.SETTINGS.getDefaultFont();
    for (int i = 0; i <= 14; i++) {
      LABELS[i].setFont(def);
      if (i > 8) {
        LABELS[i].setHorizontalAlignment(SwingConstants.RIGHT);
      }
    }

    for (int i = 0; i <= 6; i++) {
      BTN[i].setFont(def);
    }

    LIST.setFont(def);
    ANI.setFont(def);
    SPEED.setFont(def);
    LIST.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    SPEED.setEnabled(false);
    BTN[1].setEnabled(false);
    BTN[2].setEnabled(false);
    BTN[3].setEnabled(false);
    BTN[4].setEnabled(false);
    BTN[5].setEnabled(false);
    BTN[6].setEnabled(false);
  }

  private void layoutComponents() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;

    // files
    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 1;
    c.gridheight = 1;
    add(LABELS[0], c);
    c.gridx = 2;
    c.gridwidth = 1;

    // info
    c.gridy = 0;
    for (int i = 1; i < 9; i++) {
      add(LABELS[i], c);
      c.gridy++;
    }

    // info
    c.gridx = 3;
    c.gridy = 1;
    for (int i = 9; i < 15; i++) {
      if (c.gridy == 4) {
        c.gridy++;
      }
      add(LABELS[i], c);
      c.gridy++;
    }

    // jspinner
    c.gridy = 4;
    add(SPEED, c);

    // list
    c.gridheight = 7;
    c.gridx = 0;
    c.gridy = 1;
    JScrollPane scrolly = new JScrollPane(LIST);
    scrolly.setPreferredSize(new Dimension(160, 160));
    add(scrolly, c);

    // animation
    c.gridheight = 1;
    c.gridwidth = 4;
    c.gridx = 0;
    c.gridy = 8;
    add(ANI, c);

    // buttons
    c.gridheight = 1;
    c.gridwidth = 1;
    c.gridx = 1;
    c.gridy = 1;

    for (int i = 0; i <= 6; i++) {
      add(BTN[i], c);
      c.gridy += 1;
    }
  }

  private void setupListeners() {
    SPEED.addChangeListener(new CBChangeListener(evt -> {
      if (AOC != null) {
        short shr;
        int a = ((Integer) SPEED.getValue()).intValue();
        if (a > Short.MAX_VALUE) {
          shr = Short.MAX_VALUE;
        } else if (a < 1) {
          shr = 1;
        } else {
          shr = (short) a;
        }
        AOC.setDelay(shr);
        ANI.setAnimation(AOC, false);
      }
    }));

    BTN[0].addActionListener(new CBActionListener(Tga2AniGUI.this::onAdd));
    BTN[1].addActionListener(new CBActionListener(Tga2AniGUI.this::onRemove));
    BTN[2].addActionListener(new CBActionListener(Tga2AniGUI.this::onClear));
    BTN[3].addActionListener(new CBActionListener(Tga2AniGUI.this::onUp));
    BTN[4].addActionListener(new CBActionListener(Tga2AniGUI.this::onDown));
    BTN[5].addActionListener(new CBActionListener(Tga2AniGUI.this::onGenerate));
    BTN[6].addActionListener(new CBActionListener(Tga2AniGUI.this::onSaveAs));
  }

  private void onAdd(ActionEvent ae) throws IOException {
    String workPath = UE.SETTINGS.getProperty(SettingsManager.WORK_PATH);
    JFileChooser jfc = new JFileChooser(new File(workPath));
    FileFilter filter = new FileFilter() {
      @Override
      public boolean accept(File f) {
        if (f.isDirectory())
          return true;

        String name = f.getName();
        name = name.toLowerCase();
        return name.endsWith(".tga"); //$NON-NLS-1$
      }

      @Override
      public String getDescription() {
        return Language.getString("Tga2AniGUI.15"); //$NON-NLS-1$
      }
    };

    jfc.setFileFilter(filter);
    jfc.setMultiSelectionEnabled(true);
    jfc.setDialogTitle(Language.getString("Tga2AniGUI.16")); //$NON-NLS-1$
    int returnVal = jfc.showDialog(this, strADD);

    if (returnVal != JFileChooser.APPROVE_OPTION)
      return;

    ANI.setAnimation(null, false);

    File[] fil = jfc.getSelectedFiles();
    workPath = PathHelper.toFolderPath(fil[0]).toString();
    UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);
    ArrayList<String> errors = new ArrayList<String>();

    // open and load animation frames
    for (File file : fil) {
      try (FileInputStream fis = new FileInputStream(file)) {
        byte[] b = StreamTools.readAllBytes(fis);
        T2A.addFrame(file.getName(), b);
      } catch (Exception ex) {
        ex.printStackTrace();
        errors.add(file.toString());
      }
    }

    if (!errors.isEmpty()) {
      String msg = Language.getString("Tga2AniGUI.18");
      for (String err : errors)
        msg += "\n" + err;
      Dialogs.displayError(msg);
    }

    String[] a = T2A.getFrameList();
    if (a == null) {
      BTN[1].setEnabled(false);
      BTN[2].setEnabled(false);
      BTN[3].setEnabled(false);
      BTN[4].setEnabled(false);
      BTN[5].setEnabled(false);
      LIST.setModel(new DefaultListModel<String>());
    } else {
      BTN[1].setEnabled(true);
      BTN[2].setEnabled(true);
      BTN[3].setEnabled(true);
      BTN[4].setEnabled(true);
      BTN[5].setEnabled(true);
      LIST.setListData(a);
    }

    BTN[6].setEnabled(false);
    SPEED.setEnabled(false);
  }

  private void onGenerate(ActionEvent ae) throws IOException {
    try {
      String[] options = new String[]{"Cursor", "Animation"};
      String response = JOptionPane.showInputDialog(this,
        "Select type to generate:",
        UE.APP_NAME,
        JOptionPane.PLAIN_MESSAGE,
        null,
        options, options[0]).toString();

      if (response == null)
        return;

      AOC = T2A.getAniOrCur(response.equals(options[0]));
      AOC.markChanged();
    } catch (Exception ef) {
      ANI.setAnimation(null, false);
      Dialogs.displayError(ef);
      return;
    }

    Dimension dim = AOC.getFrameDim();
    LABELS[9].setText(Double.toString(dim.getWidth()));
    LABELS[10].setText(Double.toString(dim.getHeight()));
    LABELS[11].setText(Short.toString(AOC.getNumFrames()));
    SPEED.setValue((int) AOC.getDelay());
    dim = AOC.getTgaDim();
    LABELS[12].setText(Double.toString(dim.getWidth()));
    LABELS[13].setText(Double.toString(dim.getHeight()));
    LABELS[14].setText(Integer.toString(AOC.getTgaSize()));

    //if cursor or not
    SPEED.setEnabled(true);
    BTN[6].setEnabled(true);
    ANI.setAnimation(AOC, false);
  }

  private void onRemove(ActionEvent ae) throws IOException {
    if (LIST.getSelectedIndex() < 0)
      return;

    ANI.setAnimation(null, false);
    T2A.remove(LIST.getSelectedIndex());
    String[] a = T2A.getFrameList();

    if (a == null) {
      BTN[1].setEnabled(false);
      BTN[2].setEnabled(false);
      BTN[3].setEnabled(false);
      BTN[4].setEnabled(false);
      BTN[5].setEnabled(false);
      LIST.setModel(new DefaultListModel<String>());
    } else {
      BTN[1].setEnabled(true);
      BTN[2].setEnabled(true);
      BTN[3].setEnabled(true);
      BTN[4].setEnabled(true);
      BTN[5].setEnabled(true);
      LIST.setListData(a);
    }

    BTN[6].setEnabled(false);
    SPEED.setEnabled(false);
  }

  private void onClear(ActionEvent ae) throws IOException {
    ANI.setAnimation(null, false);
    T2A.clear();
    BTN[1].setEnabled(false);
    BTN[2].setEnabled(false);
    BTN[3].setEnabled(false);
    BTN[4].setEnabled(false);
    BTN[5].setEnabled(false);
    BTN[6].setEnabled(false);
    LIST.setModel(new DefaultListModel<String>());
    SPEED.setEnabled(false);
  }

  private void onUp(ActionEvent ae) throws IOException {
    if (LIST.getSelectedIndex() < 0)
      return;

    ANI.setAnimation(null, false);
    T2A.moveUp(LIST.getSelectedValue());
    String[] a = T2A.getFrameList();

    if (a == null) {
      BTN[1].setEnabled(false);
      BTN[2].setEnabled(false);
      BTN[3].setEnabled(false);
      BTN[4].setEnabled(false);
      BTN[5].setEnabled(false);
      LIST.setModel(new DefaultListModel<String>());
    } else {
      BTN[1].setEnabled(true);
      BTN[2].setEnabled(true);
      BTN[3].setEnabled(true);
      BTN[4].setEnabled(true);
      BTN[5].setEnabled(true);
      LIST.setListData(a);
    }

    BTN[6].setEnabled(false);
    SPEED.setEnabled(false);
  }

  private void onDown(ActionEvent ae) throws IOException {
    if (LIST.getSelectedIndex() < 0)
      return;

    ANI.setAnimation(null, false);
    T2A.moveDown(LIST.getSelectedValue());
    String[] a = T2A.getFrameList();

    if (a == null) {
      BTN[1].setEnabled(false);
      BTN[2].setEnabled(false);
      BTN[3].setEnabled(false);
      BTN[4].setEnabled(false);
      BTN[5].setEnabled(false);
      LIST.setModel(new DefaultListModel<String>());
    } else {
      BTN[1].setEnabled(true);
      BTN[2].setEnabled(true);
      BTN[3].setEnabled(true);
      BTN[4].setEnabled(true);
      BTN[5].setEnabled(true);
      LIST.setListData(a);
    }

    BTN[6].setEnabled(false);
    SPEED.setEnabled(false);
  }

  private void onSaveAs(ActionEvent ae) {
    if (AOC == null)
      return;

    JFileChooser jfc = new JFileChooser(new File(
      UE.SETTINGS.getProperty(SettingsManager.WORK_PATH)));
    int returnVal = jfc.showSaveDialog(this);
    if (returnVal != JFileChooser.APPROVE_OPTION)
      return;

    File fil = jfc.getSelectedFile();
    String workPath = PathHelper.toFolderPath(fil).toString();
    UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, workPath);

    if (fil.exists()) {
      String msg = Language.getString("Tga2AniGUI.17"); //$NON-NLS-1$
      msg = msg.replace("%1", fil.getName()); //$NON-NLS-1$
      int respon = JOptionPane.showConfirmDialog(UE.WINDOW,
          msg, UE.APP_NAME, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

      if (respon != JOptionPane.YES_OPTION)
        return;
    }

    try {
      FileOutputStream fos = new FileOutputStream(fil);
      AOC.save(fos);
      AOC.markSaved();
      fos.close();
    } catch (Exception ef) {
      Dialogs.displayError(ef);
    }
  }

}
