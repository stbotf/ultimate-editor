package ue.gui.special;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.naming.LimitExceededException;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import ue.UE;
import ue.gui.common.DefaultPanel;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.util.data.HexTools;

/**
 * Interface for converting numbers.
 */
public class CalcGUI extends DefaultPanel {

  private final String strCONVERT = Language.getString("CalcGUI.0"); //$NON-NLS-1$
  private final String errINVALID_NUMBER_STRING = Language.getString("CalcGUI.11"); //$NON-NLS-1$
  private final String errHEX_STRING_TOO_BIG = Language.getString("CalcGUI.12"); //$NON-NLS-1$

  // ui components
  private JTextField txtDecimal = new JTextField(24);
  private JTextField txtHex = new JTextField(24);
  private ButtonGroup btgTYPE = new ButtonGroup();
  private ButtonGroup btgEND = new ButtonGroup();
  private JCheckBox chkShort = new JCheckBox(Language.getString("CalcGUI.5"), true); //$NON-NLS-1$
  private JCheckBox chkInt = new JCheckBox(Language.getString("CalcGUI.3")); //$NON-NLS-1$
  private JCheckBox chkLong = new JCheckBox(Language.getString("CalcGUI.4")); //$NON-NLS-1$
  private JCheckBox chkFloat = new JCheckBox(Language.getString("CalcGUI.2")); //$NON-NLS-1$
  private JCheckBox chkDouble = new JCheckBox(Language.getString("CalcGUI.1")); //$NON-NLS-1$
  private JCheckBox chkLittleEndian = new JCheckBox(Language.getString("CalcGUI.6")); //$NON-NLS-1$
  private JCheckBox chkBIG = new JCheckBox(Language.getString("CalcGUI.7")); //$NON-NLS-1$
  private JButton btnDecimal = new JButton(strCONVERT);
  private JButton btnHex = new JButton(strCONVERT);
  private JLabel lblDecimal = new JLabel(Language.getString("CalcGUI.8")); //$NON-NLS-1$
  private JLabel lblType = new JLabel(Language.getString("CalcGUI.10")); //$NON-NLS-1$
  private JLabel lblHex = new JLabel(Language.getString("CalcGUI.9")); //$NON-NLS-1$
  private JLabel lblEndian = new JLabel(Language.getString("CalcGUI.14")); //$NON-NLS-1$

  public CalcGUI() {
    setupComponents();
    setupLayout();
    setupActionListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.Calculator;
  }

  @Override
  public boolean hasChanges() {
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  @Override
  public void reload() {
  }

  @Override
  public void finalWarning() {
  }

  private void setupComponents() {
    Font def = UE.SETTINGS.getDefaultFont();
    txtDecimal.setFont(def);
    txtHex.setFont(def);
    btnDecimal.setFont(def);
    btnHex.setFont(def);
    chkInt.setFont(def);
    chkLong.setFont(def);
    chkFloat.setFont(def);
    chkDouble.setFont(def);
    chkLittleEndian.setFont(def);
    chkBIG.setFont(def);
    chkShort.setFont(def);
    lblDecimal.setFont(def);
    lblType.setFont(def);
    lblHex.setFont(def);
    lblEndian.setFont(def);

    btgTYPE.add(chkShort);
    btgTYPE.add(chkInt);
    btgTYPE.add(chkLong);
    btgTYPE.add(chkFloat);
    btgTYPE.add(chkDouble);
    btgEND.add(chkLittleEndian);
    btgEND.add(chkBIG);
  }

  private void setupLayout() {
    /*PLACE*/
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    c.gridheight = 1;
    c.gridwidth = 1;
    c.gridx = 0;
    c.gridy = 0;

    add(lblDecimal, c);
    c.gridy = 1;
    add(lblType, c);
    c.gridy = 6;
    add(lblHex, c);
    c.gridx = 1;
    c.gridy = 0;
    add(txtDecimal, c);
    c.gridy = 1;
    add(chkShort, c);
    c.gridy = 2;
    add(chkInt, c);
    c.gridy = 3;
    add(chkLong, c);
    c.gridy = 4;
    add(chkFloat, c);
    c.gridy = 5;
    add(chkDouble, c);
    c.gridy = 6;
    add(txtHex, c);
    c.gridx = 2;
    c.gridy = 0;
    add(btnDecimal, c);
    c.gridy = 1;
    add(lblEndian, c);
    c.gridy = 2;
    add(chkLittleEndian, c);
    c.gridy = 3;
    add(chkBIG, c);
    c.gridy = 6;
    add(btnHex, c);
  }

  private void setupActionListeners() {
    btnDecimal.addActionListener(new CBActionListener(x -> {
      String hex;
      int len;

      try {
        if (chkShort.isSelected()) {
          len = 2;
          int num = Integer.parseInt(txtDecimal.getText());

          if (num < Short.MIN_VALUE || num > 65535) {
            Dialogs.displayError(new Exception(errINVALID_NUMBER_STRING));
            return;
          } else {
            if (chkLittleEndian.isSelected()) {
              num = Integer.reverseBytes(num);
            }

            hex = Integer.toHexString(num);
          }
        } else if (chkInt.isSelected()) {
          len = 4;
          int num = Integer.parseInt(txtDecimal.getText());

          if (chkLittleEndian.isSelected()) {
            num = Integer.reverseBytes(num);
          }

          hex = Integer.toHexString(num);
        } else if (chkLong.isSelected()) {
          len = 8;
          long num = Long.parseLong(txtDecimal.getText());

          if (chkLittleEndian.isSelected()) {
            num = Long.reverseBytes(num);
          }

          hex = Long.toHexString(num);
        } else if (chkFloat.isSelected()) {
          len = 4;
          float flt = Float.parseFloat(txtDecimal.getText());

          int num = Float.floatToRawIntBits(flt);

          if (chkLittleEndian.isSelected()) {
            num = Integer.reverseBytes(num);
          }

          hex = Integer.toHexString(num);
        } else {
          len = 8;
          double dbl = Double.parseDouble(txtDecimal.getText());

          long num = Double.doubleToRawLongBits(dbl);

          if (chkLittleEndian.isSelected()) {
            num = Long.reverseBytes(num);
          }

          hex = Long.toHexString(num);
        }

        // fix formatting
        if (hex.length() % 2 != 0) {
          hex = "0" + hex;
        }

        while (hex.length() < Math.max(len, 4) * 2) {
          hex = "00" + hex;
        }

        while (hex.length() > len * 2) {
          if (chkLittleEndian.isSelected()) // big numbers are on the right
          {
            hex = hex.substring(0, hex.length() - 2);
          } else {
            hex = hex.substring(2, hex.length());
          }
        }

        for (int i = 0; i < len - 1; i++) {
          hex = hex.substring(0, (i + 1) * 2 + i) + " " + hex.substring((i + 1) * 2 + i);
        }

        txtHex.setText(hex.toUpperCase());
      } catch (Exception e) {
        throw new NumberFormatException(errINVALID_NUMBER_STRING);
      }
    }));

    btnHex.addActionListener(new CBActionListener(x -> {
      try {
        String txt = txtHex.getText().replace(" ", "");
        String result = HexTools.undecorate(txt);
        boolean isDecorated = txt.length() != result.length();

        // when undecorated pure hex values
        // reverse little endian '56 34 12' to '12 34 56'
        if (!isDecorated && chkLittleEndian.isSelected()) {
          for (int i = 0; i < result.length() - 2; i += 2) {
            result = result.substring(0, i)
              + result.substring(result.length() - 2, result.length())
              + result.substring(i, result.length() - 2);
          }
        }

        // always parse max length
        long num = HexTools.toLong(result);

        if (chkShort.isSelected()) {
          // 16-bit value
          if (num < Short.MIN_VALUE || num > 0xFFFF)
            throw new LimitExceededException(errHEX_STRING_TOO_BIG);
          result = Short.toString((short)num);

        } else if (chkInt.isSelected() || chkFloat.isSelected()) {
          // 32-bit value
          if (num < Integer.MIN_VALUE || num > 0xFFFFFFFFL)
            throw new LimitExceededException(errHEX_STRING_TOO_BIG);

          if (chkFloat.isSelected()) {
            float fl = Float.intBitsToFloat((int)num);
            result = Float.toString(fl);
          } else {
            result = Integer.toString((int)num);
          }

        } else {
          // 64-bit value
          if (chkDouble.isSelected()) {
            double dbl = Double.longBitsToDouble(num);
            result = Double.toString(dbl);
          } else {
            result = Long.toString(num);
          }
        }

        txtDecimal.setText(result);
      } catch (Exception e) {
        throw new NumberFormatException(errINVALID_NUMBER_STRING);
      }
    }));
  }
}
