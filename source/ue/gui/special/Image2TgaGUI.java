package ue.gui.special;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileFilter;
import lombok.val;
import ue.UE;
import ue.edit.task.ConvertTGATask;
import ue.gui.common.DefaultPanel;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.gui.util.GuiTools;
import ue.gui.util.event.CBActionListener;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.util.file.PathHelper;

/**
 * Interface for converting images to tga.
 */
public class Image2TgaGUI extends DefaultPanel {

  private final String strADD = Language.getString("Image2TgaGUI.1"); //$NON-NLS-1$
  private final String strREMOVE = Language.getString("Image2TgaGUI.2"); //$NON-NLS-1$
  private final String strCLEAR = Language.getString("Image2TgaGUI.3"); //$NON-NLS-1$
  private final String strCONVERT = Language.getString("Image2TgaGUI.4"); //$NON-NLS-1$
  private final String strBROWSE = Language.getString("Image2TgaGUI.5"); //$NON-NLS-1$

  // ui components
  private JButton[] BTN = new JButton[5]; // add, remove, clear, convert, browse
  private JList<String> LIST = new JList<String>();
  private JTextField PATH = new JTextField(10);
  private JLabel LBL = new JLabel(Language.getString("Image2TgaGUI.0")); //$NON-NLS-1$

  // data
  private Vector<String> NAMES = new Vector<String>();
  private ArrayList<File> FULLPATHS = new ArrayList<File>();
  private boolean dest_locked = false;

  public Image2TgaGUI() {
    setupComponents();
    setupLayout();
    setupListeners();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.ConvertImage;
  }

  @Override
  public boolean hasChanges() {
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  @Override
  public void reload() throws IOException {
    NAMES.clear();
    FULLPATHS.clear();
    BTN[1].setEnabled(false);
    BTN[2].setEnabled(false);
    BTN[3].setEnabled(false);
    LIST.setModel(new DefaultListModel<String>());
    dest_locked = false;
    PATH.setText(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH));
  }

  @Override
  public void finalWarning() {
  }

  private void setupComponents() {
    BTN[0] = new JButton(strADD);
    BTN[1] = new JButton(strREMOVE);
    BTN[2] = new JButton(strCLEAR);
    BTN[3] = new JButton(strCONVERT);
    BTN[4] = new JButton(strBROWSE);

    //font
    Font def = UE.SETTINGS.getDefaultFont();
    BTN[0].setFont(def);
    BTN[1].setFont(def);
    BTN[2].setFont(def);
    BTN[3].setFont(def);
    BTN[4].setFont(def);
    LIST.setFont(def);
    PATH.setFont(def);
    LBL.setFont(def);

    //set other things
    LIST.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    BTN[1].setEnabled(false);
    BTN[2].setEnabled(false);
    BTN[3].setEnabled(false);
    PATH.setText(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH));
  }

  private void setupLayout() {
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;

    //list
    c.gridheight = 5;
    c.gridwidth = 1;
    c.gridx = 0;
    c.gridy = 0;
    c.weightx = 1;
    c.weighty = 1;
    JScrollPane scrolly = new JScrollPane(LIST);
    scrolly.setPreferredSize(new Dimension(160, 160));
    add(scrolly, c);

    //buttons
    c.gridheight = 1;
    c.gridx = 1;
    c.weightx = 0;
    c.weighty = 0;
    add(BTN[0], c);
    c.gridy = 1;
    add(BTN[1], c);
    c.gridy = 2;
    add(BTN[2], c);
    c.gridy = 3;
    add(BTN[3], c);
    c.gridy = 6;
    add(BTN[4], c);

    //label
    c.gridy = 5;
    c.gridx = 0;
    add(LBL, c);

    //textfield
    c.gridy = 6;
    add(PATH, c);
  }

  private void setupListeners() {
    BTN[0].addActionListener(new CBActionListener(Image2TgaGUI.this::onAdd));
    BTN[1].addActionListener(new CBActionListener(Image2TgaGUI.this::onRemove));
    BTN[2].addActionListener(new CBActionListener(Image2TgaGUI.this::onClear));
    BTN[3].addActionListener(new CBActionListener(Image2TgaGUI.this::onConvert));
    BTN[4].addActionListener(new CBActionListener(Image2TgaGUI.this::onBrowse));
  }

  private void onAdd(ActionEvent e) throws IOException {
    JFileChooser jfc = new JFileChooser(UE.SETTINGS.getProperty(SettingsManager.WORK_PATH));
    FileFilter filter = new FileFilter() {
      @Override
      public boolean accept(File f) {
        if (f.isDirectory())
          return true;

        String name = f.getName();
        name = name.toLowerCase();

        if (name.endsWith(".tga") || name.endsWith(".jpg") //$NON-NLS-1$ //$NON-NLS-2$
            || name.endsWith(".jpeg") || name.endsWith(".png") //$NON-NLS-1$ //$NON-NLS-2$
            || name.endsWith(".gif") || name.endsWith(".bmp")) //$NON-NLS-1$ //$NON-NLS-2$
          return true;

        return false;
      }

      @Override
      public String getDescription() {
        return Language.getString("Image2TgaGUI.6")
            + " (*.tga, *.jpg/*.jpeg, *.gif, *.png, *.bmp)"; //$NON-NLS-1$ //$NON-NLS-2$
      }
    };

    jfc.setFileFilter(filter);
    jfc.setDialogTitle(Language.getString("Image2TgaGUI.7")); //$NON-NLS-1$
    jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    jfc.setMultiSelectionEnabled(true);
    int returnVal = jfc.showDialog(this, strADD);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      //open and get options
      File[] fil = jfc.getSelectedFiles();
      String dirPath = PathHelper.toFolderPath(fil[0]).toString();

      UE.SETTINGS.setProperty(SettingsManager.WORK_PATH, dirPath);
      if (!dest_locked)
        PATH.setText(dirPath);

      for (int i = 0; i < fil.length; i++) {
        NAMES.add(fil[i].getName());
        FULLPATHS.add(fil[i]);
      }

      LIST.setListData(NAMES);
      BTN[1].setEnabled(true);
      BTN[2].setEnabled(true);
      BTN[3].setEnabled(true);
    }
  }

  private void onRemove(ActionEvent e) {
    if (LIST.getSelectedIndex() < 0)
      return;

    List<String> ut = LIST.getSelectedValuesList();

    for (String value : ut) {
      int hin = NAMES.indexOf(value);
      NAMES.remove(hin);
      FULLPATHS.remove(hin);
    }

    if (NAMES.isEmpty()) {
      BTN[1].setEnabled(false);
      BTN[2].setEnabled(false);
      BTN[3].setEnabled(false);
      LIST.setModel(new DefaultListModel<String>());
    } else {
      BTN[1].setEnabled(true);
      BTN[2].setEnabled(true);
      BTN[3].setEnabled(true);
      LIST.setListData(NAMES);
    }
  }

  private void onClear(ActionEvent e) {
    NAMES = new Vector<String>();
    FULLPATHS = new ArrayList<File>();
    BTN[1].setEnabled(false);
    BTN[2].setEnabled(false);
    BTN[3].setEnabled(false);
    LIST.setModel(new DefaultListModel<String>());
    dest_locked = false;
  }

  private void onBrowse(ActionEvent e) {
    JFileChooser fc = new JFileChooser();
    fc.setCurrentDirectory(new File(PATH.getText()));
    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fc.setMultiSelectionEnabled(false);
    fc.setDialogTitle(Language.getString("Image2TgaGUI.8")); //$NON-NLS-1$
    int returnVal = fc.showDialog(UE.WINDOW, Language.getString("Image2TgaGUI.9")); //$NON-NLS-1$

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File fil = fc.getSelectedFile();

      if (fil != null) {
        dest_locked = true;
        PATH.setText(fil.getPath());
      }
    }
  }

  private void onConvert(ActionEvent e) {
    if (FULLPATHS.isEmpty())
      return;

    val task = new ConvertTGATask(FULLPATHS, PATH.getText());
    boolean success = GuiTools.runUEWorker(task);

    if (success) {
      // clear file list
      NAMES.clear();
      FULLPATHS.clear();
      BTN[1].setEnabled(false);
      BTN[2].setEnabled(false);
      BTN[3].setEnabled(false);
      LIST.setModel(new DefaultListModel<String>());
      dest_locked = false;
    }
  }

}
