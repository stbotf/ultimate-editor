package ue.gui.special;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import ue.UE;
import ue.gui.common.DefaultPanel;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.service.GameMonitor;
import ue.service.Language;

/**
 * An interface to monitor multiplayer games.
 */
public class MonitorGameGUI extends DefaultPanel {

  // edited
  private GameMonitor MONITOR;

  // ui components
  private JList<String> lstIP = new JList<String>();
  private JCheckBox chkKILL = new JCheckBox(Language.getString("MonitorGameGUI.0"),
      true); //$NON-NLS-1$
  private JCheckBox chkALERT = new JCheckBox(Language.getString("MonitorGameGUI.1"),
      false); //$NON-NLS-1$
  private JTextField txtPORT = new JTextField(5);
  private JTextArea txtINFO = new JTextArea();
  private JButton btnADD = new JButton(Language.getString("MonitorGameGUI.2")); //$NON-NLS-1$
  private JButton btnEDIT = new JButton(Language.getString("MonitorGameGUI.3")); //$NON-NLS-1$
  private JButton btnREMOVE = new JButton(Language.getString("MonitorGameGUI.4")); //$NON-NLS-1$
  private JButton btnSHAKE = new JButton(Language.getString("MonitorGameGUI.5")); //$NON-NLS-1$
  private JButton btnDISC = new JButton(Language.getString("MonitorGameGUI.6")); //$NON-NLS-1$
  private JButton btnRUN = new JButton(Language.getString("MonitorGameGUI.7")); //$NON-NLS-1$

  public MonitorGameGUI() {
    MONITOR = new GameMonitor(txtINFO);
    MONITOR.setAction(1);
    /*BUILD*/
    JLabel lblPORT = new JLabel(Language.getString("MonitorGameGUI.8")); //$NON-NLS-1$
    String msg = Language.getString("MonitorGameGUI.9"); //$NON-NLS-1$
    msg = msg.replace("%1", MONITOR.getLocalAddress()); //$NON-NLS-1$
    JLabel lblIP = new JLabel(msg);
    JLabel lblCRASH = new JLabel(Language.getString("MonitorGameGUI.10")); //$NON-NLS-1$
    JButton btnOPEN = new JButton(Language.getString("MonitorGameGUI.11")); //$NON-NLS-1$
    /*SET UP*/
    Font def = UE.SETTINGS.getDefaultFont();
    lstIP.setFont(def);
    chkKILL.setFont(def);
    chkALERT.setFont(def);
    lblPORT.setFont(def);
    lblIP.setFont(def);
    lblCRASH.setFont(def);
    txtPORT.setFont(def);
    btnOPEN.setFont(def);
    btnADD.setFont(def);
    btnEDIT.setFont(def);
    btnREMOVE.setFont(def);
    btnSHAKE.setFont(def);
    btnDISC.setFont(def);
    btnRUN.setFont(def);
    //disable
    btnDISC.setEnabled(false);
    btnSHAKE.setEnabled(false);
    btnRUN.setEnabled(false);
    /*ADD ACTIONLISTENERS*/
    btnOPEN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        String lol = txtPORT.getText();
        int lolu = 0;
        try {
          lolu = Integer.parseInt(lol);
          MONITOR.openPort(lolu);
        } catch (Exception t) {
        }
      }
    });
    btnADD.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        String ip = JOptionPane
            .showInputDialog(Language.getString("MonitorGameGUI.12"), UE.APP_NAME); //$NON-NLS-1$
        if (ip != null) {
          String msg = Language.getString("MonitorGameGUI.13"); //$NON-NLS-1$
          msg = msg.replace("%1", ip); //$NON-NLS-1$
          String rs = JOptionPane.showInputDialog(msg, UE.APP_NAME);
          try {
            int port = Integer.parseInt(rs);
            MONITOR.addPeer(ip, port);
          } catch (Exception er) {
          }
        }
        updateList();
      }
    });
    btnREMOVE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (lstIP.getSelectedIndex() < 0) {
          return;
        }
        MONITOR.removePeer(lstIP.getSelectedIndex());
        updateList();
      }
    });
    btnEDIT.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (lstIP.getSelectedIndex() < 0) {
          return;
        }
        String ip = JOptionPane
            .showInputDialog(Language.getString("MonitorGameGUI.12"), UE.APP_NAME); //$NON-NLS-1$
        if (ip != null) {
          String msg = Language.getString("MonitorGameGUI.13"); //$NON-NLS-1$
          msg = msg.replace("%1", ip); //$NON-NLS-1$
          String rs = JOptionPane.showInputDialog(msg, UE.APP_NAME);
          try {
            int port = Integer.parseInt(rs);
            MONITOR.setPeer(ip, port, lstIP.getSelectedIndex());
          } catch (Exception er) {
          }
        }
        updateList();
      }
    });
    btnSHAKE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        MONITOR.connect();
        btnADD.setEnabled(false);
        btnEDIT.setEnabled(false);
        btnREMOVE.setEnabled(false);
        btnDISC.setEnabled(true);
        btnRUN.setEnabled(true);
      }
    });
    btnDISC.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        MONITOR.disconnectAll();
        btnADD.setEnabled(true);
        btnEDIT.setEnabled(true);
        btnREMOVE.setEnabled(true);
        btnDISC.setEnabled(false);
        btnRUN.setEnabled(false);
      }
    });
    btnRUN.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        MONITOR.runAndMonitor();
      }
    });
    chkKILL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        if (chkKILL.isSelected()) {
          if (chkALERT.isSelected()) {
            MONITOR.setAction(3);
          } else {
            MONITOR.setAction(1);
          }
        } else {
          if (chkALERT.isSelected()) {
            MONITOR.setAction(0);
          } else {
            MONITOR.setAction(2);
          }
        }
      }
    });
    //ADD TO PANEL
    setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);
    c.fill = GridBagConstraints.BOTH;
    //list
    c.gridheight = 4;
    c.gridwidth = 3;
    c.gridx = 0;
    c.gridy = 0;
    JScrollPane scrolly = new JScrollPane(lstIP);
    scrolly.setPreferredSize(new Dimension(160, 140));
    add(scrolly, c);
    c.gridheight = 1;
    c.gridwidth = 1;
    c.gridx = 3;
    add(btnADD, c);
    c.gridy = 1;
    add(btnEDIT, c);
    c.gridy = 2;
    add(btnREMOVE, c);
    c.gridy = 4;
    add(btnSHAKE, c);
    c.gridy = 5;
    add(btnDISC, c);
    c.gridy = 6;
    add(btnRUN, c);
    c.gridx = 0;
    c.gridy = 4;
    c.gridwidth = 3;
    add(lblIP, c);
    c.gridy = 5;
    c.gridwidth = 1;
    add(lblPORT, c);
    c.gridx = 1;
    add(txtPORT, c);
    c.gridx = 2;
    add(btnOPEN, c);
    c.gridx = 0;
    c.gridy = 7;
    c.gridwidth = 3;
    add(lblCRASH, c);
    c.gridy = 8;
    add(chkKILL, c);
    c.gridy = 9;
    add(chkALERT, c);
    JScrollPane scr = new JScrollPane(txtINFO);
    scr.setPreferredSize(new Dimension(260, 360));
    c.gridy = 0;
    c.gridx = 4;
    c.gridheight = 11;
    add(scr, c);
  }

  @Override
  public String menuCommand() {
    return MenuCommand.SuperviseMulti;
  }

  private void updateList() {
    String[] str = MONITOR.getPeerList();
    lstIP.setListData(str);
    if (str.length > 0) {
      btnSHAKE.setEnabled(true);
    } else {
      btnSHAKE.setEnabled(false);
    }
  }

  @Override
  public boolean hasChanges() {
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
  }

  @Override
  public void reload() {
    txtINFO.setText(null);
    updateList();
  }

  @Override
  public void finalWarning() {
    MONITOR.disconnectAll();
  }
}
