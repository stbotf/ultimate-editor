package ue.gui.special;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ue.UE;
import ue.edit.exe.trek.tools.TrekTools;
import ue.gui.common.DefaultPanel;
import ue.gui.common.FileChanges;
import ue.gui.menu.MenuCommand;
import ue.service.Language;

/**
 * @author Alan Podlesek
 */
public class ConvertAddressGUI extends DefaultPanel {

  private static final String strHexPrefix = "0x"; //$NON-NLS-1$

  private final String strConvert = Language.getString("ConvertAddressGUI.0"); //$NON-NLS-1$
  private final String strDecimalAddress = Language.getString("ConvertAddressGUI.1"); //$NON-NLS-1$
  private final String strHexAddress = Language.getString("ConvertAddressGUI.2"); //$NON-NLS-1$
  private final String strAsmAddress = Language.getString("ConvertAddressGUI.3"); //$NON-NLS-1$
  private final String strError = Language.getString("ConvertAddressGUI.4"); //$NON-NLS-1$
  private final String strOutOfRange = Language.getString("ConvertAddressGUI.5"); //$NON-NLS-1$

  private JTextField txtDec;
  private JTextField txtHex;
  private JTextField txtAsm;
  private JLabel lblDec;
  private JLabel lblHex;
  private JLabel lblAsm;
  private JButton btnDec = new JButton(strConvert);
  private JButton btnHex = new JButton(strConvert);
  private JButton btnAsm = new JButton(strConvert);

  public ConvertAddressGUI() {
    setupComponents();
    placeComponents();
  }

  @Override
  public String menuCommand() {
    return MenuCommand.ConvertAddresses;
  }

  private void setupComponents() {
    Font fnt = UE.SETTINGS.getDefaultFont();

    txtDec = new JTextField(14);
    txtHex = new JTextField(14);
    txtAsm = new JTextField(14);

    lblDec = new JLabel(strDecimalAddress);
    lblHex = new JLabel(strHexAddress);
    lblAsm = new JLabel(strAsmAddress);

    txtDec.setFont(fnt);
    txtDec.setMaximumSize(new Dimension(txtDec.getPreferredSize().width, 100));
    txtHex.setFont(fnt);
    txtHex.setMaximumSize(new Dimension(txtHex.getPreferredSize().width, 100));
    txtAsm.setFont(fnt);
    txtAsm.setMaximumSize(new Dimension(txtAsm.getPreferredSize().width, 100));

    btnDec.setFont(fnt);
    btnDec.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          int fileOffset = Integer.parseInt(txtDec.getText());
          txtHex.setText(strHexPrefix + Integer.toHexString(fileOffset).toUpperCase());

          int asmAddr = TrekTools.toAsmAddress(fileOffset);
          String txt = asmAddr < 0 ? strOutOfRange : Integer.toHexString(asmAddr).toUpperCase();
          txtAsm.setText(txt);
        } catch (Exception x) {
          txtHex.setText(strError);
          txtAsm.setText(strError);
        }
      }
    });

    btnHex.setFont(fnt);
    btnHex.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          String hex = txtHex.getText();
          if (!hex.startsWith(strHexPrefix))
            hex = strHexPrefix + hex;

          int fileOffset = Integer.decode(hex);
          txtDec.setText(Integer.toString(fileOffset));

          int asmAddr = TrekTools.toAsmAddress(fileOffset);
          String txt = asmAddr < 0 ? strOutOfRange : Integer.toHexString(asmAddr).toUpperCase();
          txtAsm.setText(txt);
        } catch (Exception x) {
          txtDec.setText(strError);
          txtAsm.setText(strError);
        }
      }
    });

    btnAsm.setFont(fnt);
    btnAsm.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          String hex = txtAsm.getText();
          if (!hex.startsWith(strHexPrefix))
            hex = strHexPrefix + hex;

          int asmAddr = Integer.decode(hex);
          int fileOffset = TrekTools.toFileOffset(asmAddr);

          if (fileOffset < 0){
            txtDec.setText(strOutOfRange);
            txtHex.setText(strOutOfRange);
          } else {
            txtDec.setText(Integer.toString(fileOffset));
            txtHex.setText(strHexPrefix + Integer.toHexString(fileOffset).toUpperCase());
          }
        } catch (Exception x) {
          txtDec.setText(strError);
          txtHex.setText(strError);
        }
      }
    });

    lblDec.setFont(fnt);
    lblHex.setFont(fnt);
    lblAsm.setFont(fnt);
  }

  private void placeComponents() {
    setLayout(new BorderLayout());

    JPanel pnl = new JPanel();
    pnl.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets(5, 5, 5, 5);

    c.gridx = 0;
    c.gridy = 0;
    pnl.add(lblDec, c);
    c.insets.left = 0;
    c.gridx++;
    pnl.add(txtDec, c);
    c.gridx++;
    pnl.add(btnDec, c);

    c.insets.top = 0;
    c.insets.left = 5;
    c.gridx = 0;
    c.gridy++;
    pnl.add(lblHex, c);
    c.insets.left = 0;
    c.gridx++;
    pnl.add(txtHex, c);
    c.gridx++;
    pnl.add(btnHex, c);

    c.insets.left = 5;
    c.gridx = 0;
    c.gridy++;
    pnl.add(lblAsm, c);
    c.insets.left = 0;
    c.gridx++;
    pnl.add(txtAsm, c);
    c.gridx++;
    pnl.add(btnAsm, c);

    add(pnl, BorderLayout.CENTER);
  }

  @Override
  public boolean hasChanges() {
    return false;
  }

  @Override
  protected void listChangedFiles(FileChanges changes) {
    // no files touched
  }

  @Override
  public void reload() {
    lblDec.setText(null);
    lblHex.setText(null);
    lblAsm.setText(null);
  }

  @Override
  public void finalWarning() {
    // nothing to do
  }

}
