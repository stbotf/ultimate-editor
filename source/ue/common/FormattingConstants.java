package ue.common;

import java.time.format.DateTimeFormatter;
import lombok.experimental.UtilityClass;

/**
 * Contains various formatters.
 */
@UtilityClass
public class FormattingConstants {

  public static final DateTimeFormatter EU_DATE_FORMAT = DateTimeFormatter
      .ofPattern("dd.MM.yyyy");
  public static final DateTimeFormatter EU_DATE_TIME_FORMAT = DateTimeFormatter
      .ofPattern("dd.MM.yyyy (HH:mm:ss)");

}
