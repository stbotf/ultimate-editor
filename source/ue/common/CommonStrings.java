package ue.common;

import ue.service.Language;

/**
 * @author Alan Podlesek
 */
public abstract class CommonStrings {

  /**
   * Invalid index: %1
   */
  public static final String InvalidIndex_1 = "CommonStrings.0"; //$NON-NLS-1$
  /**
   * File not found: %1
   */
  public static final String FileNotFound_1 = "CommonStrings.2"; //$NON-NLS-1$
  /**
   * File can not be read: %1
   */
  public static final String File_Read_Error_1 = "CommonStrings.3"; //$NON-NLS-1$
  /**
   * File is corrupted: %1
   */
  public static final String File_Is_Corrupted_1 = "CommonStrings.5"; //$NON-NLS-1$
  /**
   * Edit
   */
  public static final String Edit = "CommonStrings.6"; //$NON-NLS-1$

  /**
   * @param string The string with %1 and the like
   * @param args   Strings to replace %1...
   * @return
   */
  public static String populateString(String string, String[] args) {
    if (string != null && args != null) {
      int i = 1;

      for (String arg : args) {
        if (arg != null) {
          string = string.replaceAll("%" + i, arg); //$NON-NLS-1$
          i++;
        }
      }
    }

    return string;
  }

  /**
   * @param string oe of the fields feom this class
   * @param args   arguments to replace %1 and the like
   * @return
   */
  public static String getLocalizedString(String string, String... args) {
    return populateString(Language.getString(string), args);
  }

}
