package ue.edit.task;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import javax.imageio.ImageIO;
import lombok.val;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.exception.InvalidArgumentsException;
import ue.exception.UnsupportedImage;
import ue.gui.util.dialog.Dialogs;
import ue.gui.util.dialog.FileDialogs;
import ue.service.Language;
import ue.service.UEWorker;

public class ConvertTGATask extends UEWorker<Boolean> {

  private final String ConvertFileMsg = Language.getString("ConvertTGATask.0"); //$NON-NLS-1$

  private Collection<File> srcFiles;
  private String dstPath;

  public ConvertTGATask(Collection<File> files, String dstPath) {
    this.srcFiles = files;
    this.dstPath = dstPath;
    RESULT = false;
  }

  @Override
  public Boolean work() {
    ArrayList<String> errors = new ArrayList<String>();
    boolean overwriteAll = false;

    for (File path : srcFiles) {
      if (isCancelled())
        return false;

      final String srcName = path.getName();
      int ind = srcName.indexOf("."); //$NON-NLS-1$
      String dstName = ind <= 0 ? srcName : srcName.substring(0, ind);
      dstName += ".tga"; //$NON-NLS-1$
      File dest = new File(dstPath, dstName);

      // ask whether to overwrite
      if (dest.exists()) {
        if (!overwriteAll) {
          int res = FileDialogs.confirmOverwrite(dest, srcFiles.size() > 1);
          switch (res) {
            case FileDialogs.ALL:
              // update overwrite all check and proceed
              overwriteAll = true;
              break;
            case FileDialogs.YES:
              // proceed
              break;
            case FileDialogs.NO:
              // skip if rejected to overwrite
              continue;
            default:
              cancel(false);
              return false;
          }
        }
      }

      // update progress
      String msg = ConvertFileMsg.replace("%1", srcName); //$NON-NLS-1$
      feedMessage(msg);

      try {
        TargaImage targa = loadTarga(path, srcName);
        if (targa == null)
          continue;

        targa.convertFirstPixelAlpha(false);

        // use buffered stream to reduce IOPS and speed the file write
        try (val fos = new BufferedOutputStream(new FileOutputStream(dest))) {
          targa.save(fos, 16);
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        errors.add(path.toString());
      }
    }

    if (!errors.isEmpty()) {
      String msg = Language.getString("ConvertTGATask.2");
      for (String err : errors)
        msg += "\n" + err;
      Dialogs.displayError(msg);
      return false;
    }

    return RESULT = true;
  }

  private TargaImage loadTarga(File path, String name) {
    String lowerCase = name.toLowerCase();

    try {
      if (lowerCase.endsWith(".tga")) //$NON-NLS-1$
        return new TargaImage(path);

      BufferedImage bi = ImageIO.read(path);
      if (bi == null)
        throw new IOException("Failed to read BufferedImage");

      // Get all the pixels
      int w = bi.getWidth(null);
      int h = bi.getHeight(null);
      int[] rgbs = new int[w * h];
      bi.getRGB(0, 0, w, h, rgbs, 0, w);

      return new TargaImage(rgbs, w, h, true);
    } catch (UnsupportedImage e) {
      String msg = Language.getString("ConvertTGATask.1"); //$NON-NLS-1$
      msg = msg.replace("%1", name); //$NON-NLS-1$
      Dialogs.displayError(new InvalidArgumentsException(msg));
      return null;
    } catch (IOException e) {
      String msg = Language.getString("ConvertTGATask.1"); //$NON-NLS-1$
      msg = msg.replace("%1", name); //$NON-NLS-1$
      Dialogs.displayError(new InvalidArgumentsException(msg));
      return null;
    }
  }

}
