package ue.edit.task;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.common.DataFile;
import ue.edit.common.FileArchive;
import ue.edit.common.FileInfo;
import ue.edit.common.IDataFile.PatchStatus;
import ue.edit.common.InternalFile;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.ArchiveEntry;
import ue.util.file.FileFilters;
import ue.util.file.FileStore;

public class ListFileInfosTask extends UEWorker<Map<String, FileInfo>> {

  private final String LoadCachedMsg = Language.getString("ListFileInfosTask.1"); //$NON-NLS-1$
  private final String LoadArchivedMsg = Language.getString("ListFileInfosTask.2"); //$NON-NLS-1$
  private final String CheckDataMsg = Language.getString("ListFileInfosTask.3"); //$NON-NLS-1$
  private final String InvalidFileMsg = Language.getString("ListFileInfosTask.4"); //$NON-NLS-1$

  private FileArchive archive;
  private Collection<String> fileNames;
  private boolean checkData;

  public ListFileInfosTask(FileArchive archive, Collection<String> fileNames, boolean checkData) {
    this.archive = archive;
    this.fileNames = fileNames;
    this.checkData = checkData;
    // use TreeMap to sort files by file name and skip duplicates
    // for the file check, they need to be processed in strict order
    RESULT = new TreeMap<>();
  }

  @Override
  public Map<String, FileInfo> work() throws IOException {
    // check cached files first to determine the status
    // they might also include additional files not yet stored to the archive
    for (String fileName : fileNames) {
      val file = archive.getCachedFile(fileName);
      if (file == null)
        continue;

      if (isCancelled())
        return RESULT;
      processCached(file);
    }

    // process available archive entries
    val entries = fileNames.stream().map(x -> archive.getArchiveEntry(x))
      .filter(x -> x != null).collect(Collectors.toList());
    if (entries.isEmpty())
      return RESULT;

    for (val entry : entries) {
      if (isCancelled())
        return RESULT;
      processArchiveEntry(entry);
    }

    // if enabled, compare file data
    if (checkData) {
      // prcoess all file sneaks at once, so the archive
      // doesn't have to be reloaded all the time
      try (FileStore fs = archive.access()) {
        for (val entry : entries) {
          if (isCancelled())
            return RESULT;
          compareFileData(fs, entry);
        }
      }
    }

    return RESULT;
  }

  private void processCached(DataFile x) {
    String fileName = x.getName();

    // update progress
    String msg = LoadCachedMsg.replace("%1", fileName); //$NON-NLS-1$
    feedMessage(msg);

    RESULT.put(fileName, FileInfo.builder()
      .name(fileName)
      .fileType(FileFilters.getFileType(fileName))
      .classType(x.getClass())
      .size(x.getSize())
      .status(archive.getFileStatus((InternalFile)x, false))
      .build()
    );
  }

  private void processArchiveEntry(ArchiveEntry x) {
    String fileName = x.getName();

    // skip cached & removed entries
    if (!RESULT.containsKey(fileName) && !archive.isRemoved(fileName)) {
      // update progress
      String msg = LoadArchivedMsg.replace("%1", fileName); //$NON-NLS-1$
      feedMessage(msg);

      RESULT.put(fileName, archive.getArchiveFileInfo(x));
    }
  }

  private void compareFileData(FileStore fs, ArchiveEntry entry) {
    String fileName = entry.getName();

    // skip uncached and removed entries
    if (!archive.isCached(fileName) || archive.isRemoved(fileName))
      return;

    FileInfo cachedInfo = RESULT.get(fileName);
    if (cachedInfo == null) {
      // update progress
      String msg = LoadArchivedMsg.replace("%1", fileName); //$NON-NLS-1$
      feedMessage(msg);

      // if uncached, return archive file info
      // this should not happen unless the archive got modified externally
      RESULT.put(fileName, archive.getArchiveFileInfo(entry));
      return;
    }

    // skip if not loaded or already found to be changed
    if (!cachedInfo.getStatus().isUnchanged())
      return;

    // lookup the cached file and ignore removed ones
    val cachedFile = archive.getCachedFile(fileName);
    if (cachedFile == null)
      return;

    // update progress
    String msg = CheckDataMsg.replace("%1", fileName); //$NON-NLS-1$
    feedMessage(msg);

    // detect sneaked in changes of auto-applied patches
    try {
      byte[] orig = fs.getRawData(entry, archive.isChunked(fileName));
      byte[] cached = cachedFile.getRawData();

      if (!Arrays.equals(orig, cached)) {
        // remember it has sneaked changes
        cachedFile.markSneaked();
        archive.markSneaked();
        cachedInfo.setStatus(PatchStatus.Sneaked);
      }
    }
    catch (IOException e) {
      e.printStackTrace();
      // the file is already loaded, therefore mark to reset the file
      cachedInfo.setStatus(PatchStatus.Reset);
      // still list the error
      String err = InvalidFileMsg.replace("%1", fileName)
        .replace("%2", e.getMessage());
      cachedInfo.setError(err);
    }
  }
}
