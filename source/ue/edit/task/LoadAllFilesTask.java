package ue.edit.task;

import java.io.IOException;
import ue.edit.common.FileArchive;
import ue.service.Language;
import ue.service.UEWorker;

public class LoadAllFilesTask extends UEWorker<Void> {

  private final String LoadFileMsg = Language.getString("LoadAllFilesTask.1"); //$NON-NLS-1$

  private FileArchive archive;

  public LoadAllFilesTask(FileArchive archive) {
    this.archive = archive;
  }

  @Override
  public Void work() throws IOException {
    // load yet uncached files
    archive.loadReadableFiles(x -> {
      if (isCancelled())
        return false;

      // update progress
      String msg = LoadFileMsg.replace("%1", x); //$NON-NLS-1$
      feedMessage(msg);

      return true;
    });

    return RESULT;
  }

}
