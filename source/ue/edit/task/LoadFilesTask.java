package ue.edit.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import ue.edit.common.FileArchive;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.common.InternalFile;
import ue.service.Language;
import ue.service.UEWorker;

public class LoadFilesTask extends UEWorker<ArrayList<InternalFile>> {

  private final String LoadFileMsg = Language.getString("LoadFilesTask.1"); //$NON-NLS-1$

  private FileArchive archive;
  private Collection<String> fileNames;

  public LoadFilesTask(FileArchive archive, Collection<String> fileNames) {
    this.archive = archive;
    this.fileNames = fileNames;
  }

  @Override
  public ArrayList<InternalFile> work() throws IOException {
    // load yet uncached files
    return archive.loadFiles(fileNames, LoadFlags.CACHE, Optional.of(x -> {
      if (isCancelled())
        return false;

      // update progress
      String msg = LoadFileMsg.replace("%1", x); //$NON-NLS-1$
      feedMessage(msg);

      return true;
    }));
  }
}
