package ue.edit.task;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import lombok.val;
import ue.edit.common.DataFile;
import ue.edit.common.FileArchive;
import ue.edit.common.FileInfo;
import ue.edit.common.IDataFile.PatchStatus;
import ue.edit.common.InternalFile;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.ArchiveEntry;
import ue.util.file.FileFilters;
import ue.util.file.FileStore;

public class ListAllFileInfosTask extends UEWorker<Map<String, FileInfo>> {

  private final String LoadCachedMsg = Language.getString("ListAllFileInfosTask.1"); //$NON-NLS-1$
  private final String LoadArchivedMsg = Language.getString("ListAllFileInfosTask.2"); //$NON-NLS-1$
  private final String CheckDataMsg = Language.getString("ListAllFileInfosTask.3"); //$NON-NLS-1$
  private final String InvalidFileMsg = Language.getString("ListAllFileInfosTask.4"); //$NON-NLS-1$

  private FileArchive archive;
  private boolean checkData;

  public ListAllFileInfosTask(FileArchive archive, boolean checkData) {
    this.archive = archive;
    this.checkData = checkData;
    // use TreeMap to sort files by file name and skip duplicates
    // for the file check, they need to be processed in strict order
    RESULT = new TreeMap<>();
  }

  @Override
  public Map<String, FileInfo> work() throws IOException {
    // check cached files first to determine the status
    // they might also include additional files not yet stored to the archive
    archive.processCachedFiles(this::processCached);

    if (isCancelled())
      return RESULT;

    // process cached archive entries
    val entries = archive.getArchiveEntries();
    for (val entry : entries.values()) {
      processArchiveEntry(entry);
    }

    if (isCancelled())
      return RESULT;

    // if enabled, compare file data
    if (checkData) {
      try (FileStore file = archive.access()) {
        // prcoess all file sneaks at once, so the archive
        // doesn't have to be reloaded all the time
        file.processFiles(this::compareFileData);
      }
    }

    return RESULT;
  }

  private boolean processCached(DataFile x) {
    if (isCancelled())
      return false;

    String fileName = x.getName();

    // update progress
    String msg = LoadCachedMsg.replace("%1", fileName); //$NON-NLS-1$
    feedMessage(msg);

    RESULT.put(fileName, FileInfo.builder()
      .name(fileName)
      .fileType(FileFilters.getFileType(fileName))
      .classType(x.getClass())
      .size(x.getSize())
      .status(archive.getFileStatus((InternalFile)x, false))
      .build()
    );

    return true;
  }

  private boolean processArchiveEntry(ArchiveEntry x) {
    if (isCancelled())
      return false;

    String fileName = x.getName();

    // skip cached & removed entries
    if (!RESULT.containsKey(fileName) && !archive.isRemoved(fileName)) {
      // update progress
      String msg = LoadArchivedMsg.replace("%1", fileName); //$NON-NLS-1$
      feedMessage(msg);

      RESULT.put(fileName, archive.getArchiveFileInfo(x));
    }

    return true;
  }

  private boolean compareFileData(FileStore fs, ArchiveEntry entry) {
    if (isCancelled())
      return false;

    String fileName = entry.getName();

    // skip uncached and removed entries
    if (!archive.isCached(fileName) || archive.isRemoved(fileName))
      return true;

    FileInfo cachedInfo = RESULT.get(fileName);
    if (cachedInfo == null) {
      // update progress
      String msg = LoadArchivedMsg.replace("%1", fileName); //$NON-NLS-1$
      feedMessage(msg);

      // if uncached, return archive file info
      // this should not happen unless the archive got modified externally
      RESULT.put(fileName, archive.getArchiveFileInfo(entry));
      return true;
    }

    // skip if not loaded or already found to be changed
    if (!cachedInfo.getStatus().isUnchanged())
      return true;

    // lookup the cached file and ignore removed ones
    val cachedFile = archive.getCachedFile(fileName);
    if (cachedFile == null)
      return true;

    // update progress
    String msg = CheckDataMsg.replace("%1", fileName); //$NON-NLS-1$
    feedMessage(msg);

    // detect sneaked in changes of auto-applied patches
    try {
      byte[] orig = fs.getRawData(entry, archive.isChunked(fileName));
      byte[] cached = cachedFile.getRawData();

      if (!Arrays.equals(orig, cached)) {
        // remember it has sneaked changes
        cachedFile.markSneaked();
        archive.markSneaked();
        cachedInfo.setStatus(PatchStatus.Sneaked);
      }
    }
    catch (IOException e) {
      e.printStackTrace();
      // the file is already loaded, therefore mark to reset the file
      cachedInfo.setStatus(PatchStatus.Reset);
      // still list the error
      String err = InvalidFileMsg.replace("%1", fileName)
        .replace("%2", e.getMessage());
      cachedInfo.setError(err);
    }

    return true;
  }
}
