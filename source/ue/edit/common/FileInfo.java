package ue.edit.common;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import ue.edit.common.IDataFile.PatchStatus;

/**
 * Used to list file details for the file list view.
 */
@Data
@SuperBuilder
public class FileInfo {
  private String      name;
  private String      fileType;
  private Class<?>    classType;
  private int         size;
  private PatchStatus status;
  private String      error;
}
