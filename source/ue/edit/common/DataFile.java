package ue.edit.common;

import ue.util.data.StringTools;

/**
 * Every editable data file must extend this class.
 */
public abstract class DataFile implements IDataFile, Comparable<DataFile> {

  protected String NAME = null;
  private boolean CHANGED = false;

  public DataFile() {
  }

  public DataFile(String name) {
    NAME = name;
  }

  /**
   * Returns the name of the file.
   */
  @Override
  public String getName() {
    return NAME;
  }

  /**
   * Sets the name of the file.
   */
  @Override
  public void setName(String name) {
    NAME = name;
  }

  /**
   * Tests if the files are equal by comparing their filenames.
   *
   * @param o the generic file to be compared with this one
   * @return true if files are equal
   */
  @Override
  public boolean equals(Object o) {
    if (o instanceof DataFile) {
      DataFile gu = (DataFile) o;
      return NAME.equals(gu.getName());
    }
    return false;
  }

  @Override
  public int compareTo(DataFile o) {
    return StringTools.compare(NAME, o.NAME);
  }

  /**
   * Returns the file's hash code.
   */
  @Override
  public int hashCode() {
    return NAME.hashCode();
  }

  /**
   * @return true if changes were made, else false.
   */
  @Override
  public boolean madeChanges() {
    return CHANGED;
  }

  /**
   * Marks this file as changed.
   */
  @Override
  public void markChanged() {
    CHANGED = true;
  }

  /**
   * Marks this file as saved - resets the changed flag to false
   */
  @Override
  public void markSaved() {
    CHANGED = false;
  }
}
