package ue.edit.common;

import java.util.Vector;
import ue.service.Language;

/**
 * Every editable internal file must extend this class.
 */
public interface Checkable {

  static final int INTEGRITY_CHECK_WARNING = 0;
  static final int INTEGRITY_CHECK_ERROR = 1;
  static final int INTEGRITY_CHECK_INFO = 2;

  String getName();

  /**
   * @param type one of the fields from this class
   * @param msg  the message
   * @return
   */
  default String getCheckIntegrityString(int type, String msg) {
    return getCheckIntegrityString(getName(), type, msg);
  }

  static String getCheckIntegrityString(String name, int type, String msg) {
    String ret;

    switch (type) {
      case INTEGRITY_CHECK_WARNING: {
        ret = Language.getString("Checkable.0"); //$NON-NLS-1$
        break;
      }
      case INTEGRITY_CHECK_ERROR: {
        ret = Language.getString("Checkable.1"); //$NON-NLS-1$
        break;
      }
      default: {
        ret = Language.getString("Checkable.2"); //$NON-NLS-1$
      }
    }

    return ret.replace("%1", name).replace("%s", msg); //$NON-NLS-1$
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * return A Vector object containing results of the check as Strings.
   */
  default Vector<String> check() {
    Vector<String> response = new Vector<String>();
    check(response);
    return response;
  }

  void check(Vector<String> response);
}
