package ue.edit.common;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;
import ue.edit.mod.ModList;

/**
 * This is the basic interface every editable file must implement.
 */
public interface CanBeEdited {

  public default String getName() {
    return getSourceFile().getName();
  }

  // FileManager.P_ALT, P_ENGLISH, P_MUSIC, P_SAVE_GAME, P_SFX, P_STBOF, P_TREK
  public int type();

  /**
   * Saves changes.
   *
   * @param dest File where everything should be saved to. If null save to the same file.
   * @return true if save has been successful.
   * @throws IOException
   */
  public boolean save(File dest, boolean makeBackup, String extension) throws IOException;

  /**
   * Opens the provided file.
   *
   * @param file the File to be opened.
   * @throws IOException
   */
  public void open(File file) throws IOException;

  /**
   * @return true if changes were made, else false.
   */
  public boolean isChanged();

  /**
   * @return true if auto-applied silent changes were made, else false.
   */
  public boolean isSneaked();

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @return A Vector object containing results of the check as Strings.
   */
  public Vector<String> check();

  /**
   * Mark this file as changed.
   */
  public void markChanged();

  /**
   * Mark this file as sneaked.
   */
  public void markSneaked();

  /**
   * Discards all not saved changes.
   * @throws IOException
   */
  public void discard() throws IOException;

  /**
   * Returns a mod list containing all known changes to the file.
   */
  public ModList modList();

  public void updateModList();

  /**
   * @return true if this file has been modified.
   * @throws IOException if file couldn't be read for change checking
   * @throws NoSuchAlgorithmException if the hash algorithm for data comparison is missing
   */
  public boolean isModded() throws IOException, NoSuchAlgorithmException;

  /**
   * Returns open file.
   */
  public File getSourceFile();

  /**
   * Returns target file for file save.
   * Set by backup load to overwrite original files.
   */
  public File getTargetFile();

  /**
   * Sets the target file for file save.
   * Used by backup load to overwrite original files.
   */
  public void setTargetFile(File file);

}
