package ue.edit.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import lombok.Getter;
import ue.util.stream.chunk.ChunkWriterDummy;

/**
 * Every editable internal file must extend this class.
 */
public abstract class InternalFile extends DataFile implements Checkable {

  // flag for detected silent binary save changes that apply when no change has been made
  @Getter protected boolean sneaking = false;
  // flag whether the file is written in chunked format
  // this happens to be the case for result.lst only
  @Getter protected boolean isChunked = false;

  // reported file load issues
  private ArrayList<String> errors = new ArrayList<>();
  private ArrayList<String> warnings = new ArrayList<>();
  private ArrayList<String> notes = new ArrayList<>();

  public InternalFile() {
    this.isChunked = false;
  }

  public InternalFile(String name) {
    super(name);
  }

  public void markSneaked() {
    sneaking = true;
  }

  @Override
  public PatchStatus status() {
    return sneaking ? PatchStatus.Sneaked : super.status();
  }

  @Override
  public void markSaved() {
    // unset sneaking if saved, changed is unset by the super call
    sneaking = false;
    super.markSaved();
  }

  public byte[] getRawData() throws IOException {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    // if chunked, use a dummy stream to not write any chunk headers
    // this is needed for binary comparison of the result.lst file
    save(isChunked() ? new ChunkWriterDummy(out) : out);
    return out.toByteArray();
  }

  @Override
  public void check(Vector<String> response) {
    // report notified file load issues
    response.addAll(errors);
    response.addAll(warnings);
    response.addAll(notes);
  }

  @Override
  public void clear() {
    errors.clear();
    warnings.clear();
    notes.clear();
  }

  public List<String> listErrors() {
    return Collections.unmodifiableList(errors);
  }

  public List<String> listWarnings() {
    return Collections.unmodifiableList(warnings);
  }

  public List<String> listNotes() {
    return Collections.unmodifiableList(notes);
  }

  protected void addError(String error) {
    errors.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, error));
  }

  protected void addWarning(String warning) {
    warnings.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, warning));
  }

  protected void addNote(String note) {
    notes.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, note));
  }

}
