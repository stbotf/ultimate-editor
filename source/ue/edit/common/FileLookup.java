package ue.edit.common;

import java.io.FilenameFilter;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.mutable.MutableBoolean;
import com.google.common.collect.Streams;
import lombok.val;
import ue.gui.common.FileChanges;

public abstract class FileLookup extends MainFile {

  private final TreeMap<String, String> loadErrors = new TreeMap<>();

  public abstract boolean hasInternalFile(String fileName);

  /**
   * Returns a list of available file names.
   *
   * @return list of file names
   * @throws IOException
   */
  public abstract Collection<String> getFileNames() throws IOException;

  /**
   * Returns a list of available file names.
   *
   * @param filter to filter file names
   * @return list of file names
   * @throws IOException
   */
  public abstract Collection<String> getFileNames(FilenameFilter filter) throws IOException;

  public abstract int getLoadedFileCount();

  public SortedMap<String, String> listLoadErrors() {
    return Collections.unmodifiableSortedMap(loadErrors);
  }

  public boolean hasLoadError(String fileName) {
    return loadErrors.containsKey(fileName.toLowerCase());
  }

  public String getLoadError(String fileName) {
    return loadErrors.get(fileName.toLowerCase());
  }

  protected void addLoadError(String fileName, String error) {
    loadErrors.put(fileName.toLowerCase(), error);
  }

  protected void removeLoadError(String fileName) {
    loadErrors.remove(fileName.toLowerCase());
  }

  public void clearLoadErrors() {
    loadErrors.clear();
  }

  /**
   * Returns a list of files loaded (in memory)
   *
   * @return list of cached files
   */
  public abstract Set<String> cachedFiles();

  /**
   * List cached files.
   * @return list of file names
   */
  public Set<String> cachedFiles(FilenameFilter filter) {
    HashSet<String> lst = new HashSet<String>();
    processCachedFiles(filter, x -> {
      if (x.madeChanges())
        lst.add(x.getName());
      return true;
    });
    return lst;
  }

  public abstract Collection<? extends InternalFile> listCachedFiles();
  public List<? extends InternalFile> listChangedFiles() {
    return listCachedFiles().stream().filter(x -> x.madeChanges() || x.isSneaking())
      .collect(Collectors.toList());
  }

  public abstract Stream<? extends InternalFile> streamCachedFiles();
  public abstract void processCachedFiles(Predicate<DataFile> callback);
  public abstract void processCachedFiles(FilenameFilter filter, Predicate<DataFile> callback);

  public DataFile findCachedFile(Predicate<DataFile> callback) {
    // feature array to trick callback for effectively final access
    final DataFile found[] = new DataFile[1];

    processCachedFiles(x -> {
      if (callback.test(x)) {
        found[0] = x;
        return false;
      }
      return true;
    });

    return found[0];
  }

  /**
   * @return true if changes were made to the opened file.
   */
  @Override
  public boolean isChanged() {
    return changed || findCachedFile(x -> x.madeChanges()) != null;
  }

  /**
   * @return true if changes were made to the opened file.
   */
  @Override
  public boolean isSneaked() {
    return sneaked || findCachedFile(x -> x.isSneaking()) != null;
  }

  public abstract boolean isCached(String fileName);

  /**
   * @return true if the named internal file has been modified.
   */
  public abstract boolean isChanged(String fileName);

  public boolean hasBeenModified(Iterable<String> fileNames) {
    return Streams.stream(fileNames).anyMatch(x -> isChanged(x));
  }
  public boolean hasBeenModified(String[] fileNames) {
    return Arrays.stream(fileNames).anyMatch(x -> isChanged(x));
  }

  /**
   * @return true if the named internal file has been modified.
   */
  public boolean hasBeenModified(int address) {
    return isChanged(Integer.toString(address));
  }
  public boolean hasBeenModified(int[] addresses) {
    return Arrays.stream(addresses).anyMatch(x -> hasBeenModified(x));
  }

  /**
   * @return true if any of the filtered internal files has been modified.
   */
  public boolean hasBeenModified(FilenameFilter filter) {
    final val result = new MutableBoolean();
    processCachedFiles(filter, x -> {
      if (x.madeChanges()) {
        result.setTrue();
        return false;
      }
      return true;
    });

    return result.isTrue();
  }

  /**
   * List modified files.
   * @return list of file names
   */
  public Set<String> modifiedFiles(FilenameFilter filter) {
    HashSet<String> lst = new HashSet<String>();
    processCachedFiles(filter, x -> {
      if (x.madeChanges())
        lst.add(x.getName());
      return true;
    });
    return lst;
  }

  public abstract int getInternalFileSize(String filename) throws IOException;
  public abstract InternalFile getCachedFile(String fileName);

  /**
   * Can be used to retrieve internal files from the mother ship.
   *
   * @param name the internal file to get
   * @return the requested internal file
   * @throws IOException
   */
  public abstract InternalFile getInternalFile(String name, boolean cache) throws IOException;
  public abstract List<InternalFile> getInternalFiles(Collection<String> fileNames, boolean cache,
    Optional<Predicate<String>> cbProgress) throws IOException;

  /**
   * Returns the internal file object or null if missing.
   * @param fileName the name of the file
   */
  public abstract InternalFile tryGetInternalFile(String fileName, boolean cache);
  public abstract List<InternalFile> tryGetInternalFiles(Collection<String> fileNames, boolean cache,
    Optional<Predicate<String>> cbProgress);

  /**
   * Can be used to retrieve raw file data from the mother ship.
   *
   * @param name the file to load
   * @return the requested raw file data
   * @throws IOException
   * @throws NoSuchElementException
   */
  public abstract Optional<byte[]> getRawData(String name) throws IOException;

  public byte[] getRawData(int address) throws IOException {
    return getRawData(Integer.toString(address)).get();
  }

  /**
   * Enforces to reload the internal file.
   * @return the loaded or reloaded internal file
   * @throws IOException
   */
  public abstract InternalFile reloadFile(String fileName, boolean cache) throws IOException;
  public abstract List<InternalFile> reloadFiles(Collection<String> fileNames, boolean cache) throws IOException;

  public void require(String ... fileNames) throws IOException {
    getInternalFiles(Arrays.asList(fileNames), true, Optional.empty());
  }

  @Override
  public void discard() {
    super.discard();
    loadErrors.clear();
  }

  /**
   * Call this function to discard single file changes.
   */
  public void discard(String fileName) {
    loadErrors.remove(fileName.toLowerCase());
  }

  /**
   * Discard changes of multiple files.
   */
  public void discard(Iterable<String> fileNames) {
    for (String x : fileNames)
      discard(x);
  }

  /**
   * Discard changes of multiple files.
   */
  public void discard(String[] fileNames) {
    for (String x : fileNames)
      discard(x);
  }

  public abstract void discard(FilenameFilter filter);

  /**
   * Call this function to discard single file changes.
   */
  public void discard(int address) {
    discard(Integer.toString(address));
  }
  public void discard(int[] addresses) {
    for (int x : addresses)
      discard(x);
  }

  /**
   * Helper routine to collect modified internal files.
   */
  public void checkChanged(String fileName, FileChanges changes) {
    if (isChanged(fileName))
      changes.add(this, fileName);
  }
  public void checkChanged(Iterable<String> fileNames, FileChanges changes) {
    for (String fileName : fileNames) {
      if (isChanged(fileName))
        changes.add(this, fileName);
    }
  }
  public void checkChanged(String[] fileNames, FileChanges changes) {
    for (String fileName : fileNames) {
      if (isChanged(fileName))
        changes.add(this, fileName);
    }
  }

  /**
   * Helper routine to collect modified internal files.
   */
  public void checkChanged(int address, FileChanges changes) {
    String fileName = Integer.toString(address);
    if (isChanged(fileName))
      changes.add(this, fileName);
  }
  public void checkChanged(int[] addresses, FileChanges changes) {
    for (int x : addresses)
      checkChanged(x, changes);
  }

  /**
   * Helper routine to collect modified internal files.
   */
  public void checkChanged(DataFile file, FileChanges changes) {
    if (file != null && file.madeChanges())
      changes.add(this, file.getName());
  }

  /**
   * Helper routine to collect modified internal files.
   */
  public void checkChanged(Collection<? extends DataFile> files, FileChanges changes) {
    for (DataFile dataFile : files) {
      checkChanged(dataFile, changes);
    }
  }

  /**
   * Helper routine to collect modified internal files.
   */
  public void checkChanged(FilenameFilter filter, FileChanges changes) {
    changes.addAll(this, modifiedFiles(filter));
  }

  public byte[] getInternalFileHash(String filename) throws Exception {
    InternalFile file = this.getInternalFile(filename, false);
    return computeHash(file.toByteArray());
  }

  protected byte[] computeHash(byte[] data) throws Exception {
    MessageDigest m = MessageDigest.getInstance("MD5"); //$NON-NLS-1$
    m.update(data, 0, data.length);
    return m.digest();
  }

}
