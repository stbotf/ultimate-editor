package ue.edit.common;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import lombok.val;
import ue.edit.common.IDataFile.PatchStatus;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.util.data.StringTools;
import ue.util.file.ArchiveEntry;
import ue.util.file.FileStore;
import ue.util.func.AutoLock;

public abstract class FileArchive extends FileLookup {

  public interface LoadFlags {
    public static final int UNCACHED = 0;
    // cache file for reuse and to track file changes
    public static final int CACHE = 1;
    // copy file to obtain a unique instance
    public static final int COPY = 1 << 1;
    // discard existing file if cached
    public static final int RELOAD = 1 << 2;
    // throw IOEXception if missing
    public static final int THROW = 1 << 3;
  }

  // locking table for async file load
  private ConcurrentHashMap<String, AutoLock> lockingTable = new ConcurrentHashMap<>(CStbofFiles.CoreFiles.length);

  @Override
  public boolean hasInternalFile(String fileName) {
    return getArchiveEntry(fileName) != null;
  }

  /**
   * Chunked files consist of a sequence of sub-entries, where each sub-entry
   * is stored with index and size, but is compressed separately.
   * The result.lst SavGame file is the only known file using this special format.
   * @return whether the file is stored in special chunk format (@see ResultList)
   */
  public abstract boolean isChunked(String fileName);
  public abstract Class<?> getFileClass(String fileName);
  public abstract String getFileDescription(String fileName);

  @Override
  public boolean hasBeenModified(FilenameFilter filter) {
    File file = getSourceFile();
    if (removedFiles().stream().anyMatch(x -> filter.accept(file, x)))
      return true;

    return super.hasBeenModified(filter);
  }

  @Override
  public Set<String> modifiedFiles(FilenameFilter filter) {
    Set<String> lst = super.modifiedFiles(filter);
    collectRemovedFiles(lst, filter);
    return lst;
  }

  public abstract int getRemovedFileCount();

  public abstract boolean isRemoved(String fileName);

  /**
   * Returns a list of files to be removed when save() is called.
   * @return list of removed files
   */
  public abstract Set<String> removedFiles();

  /**
   * List removed files.
   * @return list of file names
   */
  public Set<String> removedFiles(FilenameFilter filter) {
    HashSet<String> lst = new HashSet<String>();
    collectRemovedFiles(lst, filter);
    return lst;
  }

  // get unloaded archive file info
  public FileInfo getArchiveFileInfo(ArchiveEntry file) {
    String fileName = file.getName();
    String err = getLoadError(fileName);
    return FileInfo.builder()
      .name(fileName)
      .fileType(file.getFileType())
      .classType(getFileClass(fileName))
      .size(file.getUncompressedSize())
      .status(err != null ? PatchStatus.Invalid : PatchStatus.Unloaded)
      .error(err)
      .build();
  }

  // list file infos for stored archive files
  // including cached and removed archive files, except newly added ones
  public Set<FileInfo> listArchiveFiles() throws IOException {
    TreeSet<FileInfo> files = new TreeSet<>(Comparator.comparing(FileInfo::getName));

    // process all archive files
    val entries = getArchiveEntries();
    for (val entry : entries.values()) {
      files.add(getArchiveFileInfo(entry));
    }

    return files;
  }

  public boolean hasFile(FilenameFilter filter) {
    // first check the archive entries
    val entries = getArchiveEntries();
    for (String lcName : entries.keySet()) {
      if (filter == null || filter.accept(getSourceFile(), lcName)) {
        if (!isCached(lcName) && !isRemoved(lcName))
          return true;
      }
    }

    // for added files also check the file cache
    MutableBoolean found = new MutableBoolean();
    processCachedFiles(filter, x -> { found.setTrue(); return false; });
    return found.isTrue();
  }

  public int getFileCount(FilenameFilter filter) {
    MutableInt cnt = new MutableInt();

    // first check the archive entries
    val entries = getArchiveEntries();
    for (String lcName : entries.keySet()) {
      if (filter == null || filter.accept(getSourceFile(), lcName)) {
        if (!isCached(lcName) && !isRemoved(lcName))
          cnt.increment();
      }
    }

    // for added files also check the file cache
    processCachedFiles(filter, x -> { cnt.increment(); return true; });
    return cnt.intValue();
  }

  /**
   * List all archive file names.
   * @return List of file names
   */
  @Override
  public ArrayList<String> getFileNames() {
    return getFileNames(null);
  }

  /**
   * List filtered archive file names.
   * @return list of file names
   */
  @Override
  public ArrayList<String> getFileNames(FilenameFilter filter) {
    ArrayList<String> lst = new ArrayList<String>();

    // to process existing files in strict order,
    // first check the archive entries
    val entries = getArchiveEntries();
    for (val entry : entries.values()) {
      String lcName = entry.getLcName();
      if (filter == null || filter.accept(getSourceFile(), lcName)) {
        if (!isRemoved(lcName) && !isCached(lcName))
          lst.add(entry.getName());
      }
    }

    // for added files also check the file cache
    processCachedFiles(filter, x -> lst.add(x.getName()));
    lst.sort(null);
    return lst;
  }

  // used to update the file status of all the files in the files gui
  public abstract void loadReadableFiles(Predicate<String> cbCancel) throws IOException;

  // Get all cached archive file entries.
  public abstract Map<String, ArchiveEntry> getArchiveEntries();
  // Get cached archive file entry.
  public abstract ArchiveEntry getArchiveEntry(String fileName);

  public PatchStatus getFileStatus(InternalFile file, boolean checkData) {
    PatchStatus status = file.status();

    // detect sneaked in changes of auto-applied patches
    if (checkData && status != PatchStatus.Sneaked && !file.madeChanges()) {
      try {
        byte[] orig = getRawData(file.getName()).orElse(null);
        byte[] cached = file.getRawData();

        if (!Arrays.equals(orig, cached)) {
          // remember it has sneaked changes
          file.markSneaked();
          markSneaked();
          status = PatchStatus.Sneaked;
        }
      }
      catch (IOException e) {
        e.printStackTrace();
        status = PatchStatus.Invalid;
      }
    }

    return status;
  }

  /**
   * Returns the internal file object or throws.
   * @param fileName the name of the file
   * @throws IOException
   */
  @Override
  public InternalFile getInternalFile(String fileName, boolean cache) throws IOException {
    int loadFlags = cache ? LoadFlags.CACHE | LoadFlags.THROW : LoadFlags.THROW;
    return lockedFileLoad(Optional.empty(), fileName, loadFlags).get();
  }

  public InternalFile getInternalFile(String fileName, int loadFlags) throws IOException {
    return lockedFileLoad(Optional.empty(), fileName, loadFlags | LoadFlags.THROW).get();
  }

  public InternalFile getInternalFile(FileStore arch, String fileName, boolean cache) throws IOException {
    int loadFlags = cache ? LoadFlags.CACHE | LoadFlags.THROW : LoadFlags.THROW;
    return lockedFileLoad(Optional.of(arch), fileName, loadFlags).get();
  }

  public List<InternalFile> getInternalFiles(Collection<String> fileNames, boolean cache,
    Optional<Predicate<String>> cbProgress) throws IOException {
    int loadFlags = cache ? LoadFlags.CACHE | LoadFlags.THROW : LoadFlags.THROW;
    return loadFiles(fileNames, loadFlags, cbProgress);
  }

  public List<InternalFile> getInternalFiles(Collection<String> fileNames, int loadFlags,
    Optional<Predicate<String>> cbProgress) throws IOException {
    return loadFiles(fileNames, loadFlags | LoadFlags.THROW, cbProgress);
  }

  /**
   * Returns the internal file object or null if missing.
   * @param fileName the name of the file
   */
  @Override
  public InternalFile tryGetInternalFile(String fileName, boolean cache) {
    int loadFlags = cache ? LoadFlags.CACHE : LoadFlags.UNCACHED;
    return tryGetInternalFile(fileName, loadFlags);
  }

  public InternalFile tryGetInternalFile(String fileName, int loadFlags) {
    try {
      return lockedFileLoad(Optional.empty(), fileName, loadFlags).orElse(null);
    }
    catch (IOException e) {
      // ignore read errors but print stack trace
      e.printStackTrace();
      return null;
    }
  }

  public InternalFile tryGetInternalFile(FileStore arch, String fileName, boolean cache) throws IOException {
    try {
      int loadFlags = cache ? LoadFlags.CACHE : LoadFlags.UNCACHED;
      return lockedFileLoad(Optional.of(arch), fileName, loadFlags).orElse(null);
    }
    catch (IOException e) {
      // ignore read errors but print stack trace
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public List<InternalFile> tryGetInternalFiles(Collection<String> fileNames, boolean cache,
    Optional<Predicate<String>> cbProgress) {

    int loadFlags = cache ? LoadFlags.CACHE : LoadFlags.UNCACHED;
    return tryGetInternalFiles(fileNames, loadFlags, cbProgress);
  }

  public List<InternalFile> tryGetInternalFiles(Collection<String> fileNames, int loadFlags,
    Optional<Predicate<String>> cbProgress) {
    try {
      return loadFiles(fileNames, loadFlags, cbProgress);
    } catch (IOException e) {
      // ignore read errors but print stack trace
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Enforces to reload the internal file.
   * @return the loaded or reloaded internal file
   * @throws IOException
   */
  @Override
  public InternalFile reloadFile(String fileName, boolean cache) throws IOException {
    int loadFlags = LoadFlags.RELOAD | LoadFlags.THROW | (cache ? LoadFlags.CACHE : LoadFlags.UNCACHED);
    return lockedFileLoad(Optional.empty(), fileName, loadFlags).get();
  }

  @Override
  public List<InternalFile> reloadFiles(Collection<String> fileNames, boolean cache) throws IOException {
    int loadFlags = LoadFlags.RELOAD | LoadFlags.THROW | (cache ? LoadFlags.CACHE : LoadFlags.UNCACHED);
    return loadFiles(fileNames, loadFlags, Optional.empty());
  }

  @Override
  public Optional<byte[]> getRawData(String fileName) throws IOException {
    // return cached data if generic and unmodified
    InternalFile file = getCachedFile(fileName);
    if (file instanceof GenericFile && !file.madeChanges())
      return Optional.of(((GenericFile)file).getData());

    boolean chunked = isChunked(fileName);

    // reload full file
    try (FileStore fs = access()) {
      return fs.getRawData(fileName, chunked);
    }
  }

  @Override
  public int getInternalFileSize(String fileName) throws IOException {
    // for name lookup change to lower case
    if (isRemoved(fileName))
      return 0;

    InternalFile file = getCachedFile(fileName);
    if (file != null)
      return file.getSize();

    try (FileStore zf = access()) {
      return zf.getFileSize(fileName);
    }
  }

  /**
   * Adds an internal file to the main file.
   * @param file the file to add.
   * @throws IOException
   */
  public abstract void addInternalFile(InternalFile file) throws IOException;

  /**
   * Removes requested internal file.
   * @param name the name of the file to remove
   */
  public abstract void removeInternalFile(String name);

  public void renameInternalFile(String current, String renamed) throws IOException {
    if (StringTools.equals(current, renamed))
      return;

    InternalFile file = tryGetInternalFile(current, false);
    removeInternalFile(current);
    if (file != null) {
      file.setName(renamed);
      addInternalFile(file);
    }
  }

  public abstract FileStore access() throws IOException;

  protected InternalFile getInternalFile(Optional<FileStore> arch, String fileName, boolean cache) throws IOException {
    int loadFlags = cache ? LoadFlags.CACHE | LoadFlags.THROW : LoadFlags.THROW;
    return lockedFileLoad(arch, fileName, loadFlags).get();
  }

  protected InternalFile tryGetInternalFile(Optional<FileStore> arch, String fileName, boolean cache) throws IOException {
    try {
      int loadFlags = cache ? LoadFlags.CACHE : LoadFlags.UNCACHED;
      return lockedFileLoad(arch, fileName, loadFlags).orElse(null);
    }
    catch (IOException e) {
      // ignore read errors but print stack trace
      e.printStackTrace();
      return null;
    }
  }

  protected abstract Optional<InternalFile> loadFile(Optional<FileStore> arch, String fileName, int loadFlags) throws IOException;

  protected Optional<InternalFile> lockedFileLoad(Optional<FileStore> arch, String fileName, int loadFlags) throws IOException {
    try (val lock = getFileLock(fileName)) {
      return loadFile(arch, fileName, loadFlags);
    }
  }

  public ArrayList<InternalFile> loadFiles(Collection<String> fileNames, int loadFlags, Optional<Predicate<String>> cbProgress) throws IOException {
    ArrayList<InternalFile> files = new ArrayList<>(fileNames.size());

    try (val file = access()) {
      for (String fileName : fileNames) {
        try (val lock = getFileLock(fileName)) {
          // skip notify cached files
          InternalFile cached = getCachedFile(fileName);
          if (cached != null) {
            files.add(cached);
            continue;
          }

          // update progress
          if (cbProgress.isPresent())
            if (!cbProgress.get().test(fileName))
              break;

          // load file
          val loaded = lockedFileLoad(Optional.of(file), fileName, loadFlags);
          if (loaded.isPresent())
            files.add(loaded.get());
        }
      }
    }

    return files;
  }

  protected AutoLock getFileLock(String fileName) {
    // merge to return the new valid lock object, not null
    val lock = lockingTable.merge(fileName.toLowerCase(), new AutoLock(new ReentrantLock()), (x,y) -> x != null ? x : y);
    lock.lock();
    return lock;
  }

  public byte[] getInternalFileHash(FileStore fileStore, String filename) throws Exception {
    InternalFile file = this.getInternalFile(Optional.of(fileStore), filename, false);
    return computeHash(file.toByteArray());
  }

  private void collectRemovedFiles(Set<String> lst, FilenameFilter filter) {
    File file = getSourceFile();
    removedFiles().stream().filter(x -> filter.accept(file, x)).forEachOrdered(lst::add);
  }

}
