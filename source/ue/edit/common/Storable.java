package ue.edit.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Storable {

  /**
   * Load file.
   * @throws IOException
   */
  void load(InputStream in) throws IOException;

  default void load(byte[] data) throws IOException {
    ByteArrayInputStream bais = new ByteArrayInputStream(data);
    load(bais);
  }

  default void load(byte[] data, int offset) throws IOException {
    ByteArrayInputStream bais = new ByteArrayInputStream(data, offset, getSize());
    load(bais);
  }

  /**
   * Saves changes.
   * @throws IOException
   */
  void save(OutputStream out) throws IOException;

  default byte[] toByteArray() throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    save(baos);
    return baos.toByteArray();
  }

  /**
   * Clear collections and set values to a default state.
   */
  void clear();

  /**
   * Used to get the uncompressed size of the file.
   */
  int getSize();
}
