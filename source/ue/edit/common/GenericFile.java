package ue.edit.common;
//standard

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.service.Language;

/**
 * This class is used to encapsulate files. It is used by stbof to add modified animations to it and
 * elsewhere too.
 */
public class GenericFile extends InternalFile {

  private final String errEMPTY = Language.getString("GenericFile.0"); //$NON-NLS-1$
  private final String errNAME = Language.getString("GenericFile.1"); //$NON-NLS-1$

  protected byte[] data;

  /**
   * Creates an empty generic file.
   */
  public GenericFile() {
  }

  /**
   * Creates an empty generic file with specified name.
   * @param n the name of the file
   */
  public GenericFile(String name) {
    setName(name);
  }

  public GenericFile(String name, byte[] data) {
    setName(name);
    load(data);
  }

  /**
   * Returns the file data.
   */
  public byte[] getData() {
    return data;
  }

  @Override
  public byte[] toByteArray() {
    return data;
  }

  /**
   * Returns the size of the file's data.
   */
  @Override
  public int getSize() {
    return data != null ? data.length : 0;
  }

  /**
   * Sets the data.
   *
   * @param b the new data for this generic file
   */
  public void setData(byte[] b) {
    if (!Arrays.equals(data, b)) {
      data = b;
      this.markChanged();
    }
  }

  /**
   * Tests if the files are equal by comparing their filenames.
   *
   * @param o the generic file to be compared with this one
   * @return true if files are equal
   */
  @Override
  public boolean equals(Object o) {
    if (o instanceof InternalFile) {
      InternalFile gu = (InternalFile) o;
      return getName().equals(gu.getName());
    }

    return false;
  }

  /**
   * Returns the file's hashcode.
   */
  @Override
  public int hashCode() {
    return getName().hashCode();
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
    if (data == null || data.length < 1)
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, errEMPTY)); //$NON-NLS-1$
    if (getName().length() < 1)
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, errNAME)); //$NON-NLS-1$
  }

  @Override
  public void load(byte[] data) {
    this.data = data;
    markSaved();
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] data = new byte[in.available()];
    int len = 0;
    int k = 0;
    while ((len = in.read(data, k, in.available())) > 0) {
      k += len;
    }
    load(data);
  }

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws Exception the function may throw an exception if an error occures.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    if (data != null)
      out.write(data);
  }

  @Override
  public void clear() {
    data = null;
    markChanged();
  }
}
