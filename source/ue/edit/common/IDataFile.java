package ue.edit.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.stream.Collectors;
import ue.exception.DataException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.HexTools;
import ue.util.stream.StreamTools;

/**
 * Base interface for DataFiles.
 */
public interface IDataFile extends Storable {

  public enum PatchStatus {
    Unknown,
    Unloaded,
    Loaded,
    // loaded states
    Orig,
    Patched,
    Changed,
    Sneaked,
    Invalid,
    // unsaved changes
    Patch,
    Unpatch,
    Change,
    Modify,
    Reset;

    public boolean isUnchanged() {
      return this == Orig || this == Loaded;
    }
  }

  /**
   * Returns the name of the file.
   */
  String getName();

  /**
   * Sets the name of the file.
   */
  void setName(String name);

  /**
   * @return true if user aware changes were made.
   */
  boolean madeChanges();

  /**
   * @return true if sneaking changes without user interaction were made.
   */
  boolean isSneaking();

  /**
   * Marks this file as changed.
   */
  void markChanged();

  /**
   * Marks this file as saved - resets the changed flag to false
   */
  void markSaved();

  default PatchStatus status() {
    return madeChanges() ? PatchStatus.Patch : PatchStatus.Loaded;
  }

  default boolean check(byte[] read, byte[] expected) {
    return DataTools.equals(read, expected);
  }

  default boolean check(byte[] read, int offset, byte[] expected) {
    return DataTools.equals(read, offset, expected);
  }

  default boolean check(byte[] read, int offset, int length, byte[] expected) {
    return DataTools.equals(read, offset, length, expected);
  }

  default boolean check(byte[] read, int offset, int length, byte[] expected, int exp_offset) {
    return DataTools.equals(read, offset, length, expected, exp_offset);
  }

  default boolean check(byte[] read, int expected) {
    return DataTools.equals(read, expected);
  }

  default boolean check(byte[] read, int offset, int length, byte expected) {
    return DataTools.equals(read, offset, length, expected);
  }

  default void failed(byte[] read, byte[] expected) throws DataException {
    String err = dataError(read, expected);
    System.err.println(err);
    throw new DataException(err);
  }

  default void failed(byte read, byte expected) throws DataException {
    String err = dataError(read, expected);
    System.err.println(err);
    throw new DataException(err);
  }

  default <T> void failed(T read, T expected) throws DataException {
    String err = valueError(read, expected);
    System.err.println(err);
    throw new DataException(err);
  }

  default <T> void failed(String property, T read, T expected) throws DataException {
    String err = valueError(property, read, expected);
    System.err.println(err);
    throw new DataException(err);
  }

  default void validate(byte[] read, byte[] expected) throws DataException {
    if (!check(read, expected))
      failed(read, expected);
  }

  default void validate(InputStream in, byte[] expected) throws IOException {
    byte[] read = StreamTools.readBytes(in, expected.length);
    if (!check(read, expected))
      failed(read, expected);
  }

  // validate byte
  default void validate(InputStream in, int expected) throws IOException {
    int read = in.read();
    if (read != (expected & 0xFF))
      failed(read, expected);
  }

  default void validate(InputStream in, int len, int expected) throws IOException {
    for (int i = 0; i < len; ++i)
      validate(in, expected);
  }

  // validate signed byte
  default void validateSByte(InputStream in, int expected) throws IOException {
    int read = (byte)in.read();
    if (read != expected)
      failed(read, expected);
  }

  default void validateShort(InputStream in, short expected) throws IOException {
    short read = StreamTools.readShort(in, true);
    if (read != expected)
      failed(read, expected);
  }

  default void validateInt(InputStream in, int expected) throws IOException {
    int read = StreamTools.readInt(in, true);
    if (read != expected)
      failed(read, expected);
  }

  default void validateLong(InputStream in, long expected) throws IOException {
    long read = StreamTools.readLong(in, true);
    if (read != expected)
      failed(read, expected);
  }

  default void validateFloat(InputStream in, float expected) throws IOException {
    float read = StreamTools.readFloat(in, true);
    if (read != expected)
      failed(read, expected);
  }

  default void validateDouble(InputStream in, double expected) throws IOException {
    double read = StreamTools.readDouble(in, true);
    if (read != expected)
      failed(read, expected);
  }

  default void validate(byte[] read, int offset, byte[] expected) throws DataException {
    if (!check(read, offset, expected)) {
      byte[] bytesRead = DataTools.chunkOfRange(read, offset, expected.length);
      failed(bytesRead, expected);
    }
  }

  default void validate(byte[] read, int offset, int length, byte[] expected) throws DataException {
    validate(read, offset, length, expected, 0);
  }

  default void validate(byte[] read, int offset, int length, byte[] expected, int exp_offset) throws DataException {
    if (!check(read, offset, length, expected, exp_offset)) {
      byte[] bytesRead = DataTools.chunkOfRange(read, offset, length);
      byte[] bytesExp = DataTools.chunkOfRange(expected, exp_offset, length);
      failed(bytesRead, bytesExp);
    }
  }

  default void validate(byte[] read, int expected) throws DataException {
    if (!check(read, expected)) {
      byte[] bytesExpected = new byte[read.length];
      Arrays.fill(bytesExpected, (byte)expected);
      failed(read, bytesExpected);
    }
  }

  default void validate(byte[] read, int offset, byte expected) throws DataException {
    validate(read, offset, 1, expected);
  }

  default void validate(byte[] read, int offset, int length, byte expected) throws DataException {
    if (!check(read, offset, length, expected)) {
      byte[] bytesRead = DataTools.chunkOfRange(read, offset, length);
      byte[] bytesExpected = new byte[length];
      Arrays.fill(bytesExpected, expected);
      failed(bytesRead, bytesExpected);
    }
  }

  default <T> void validate(T value, T expected) throws DataException {
    validate(null, value, expected);
  }

  default <T> void validate(String property, T value, T expected) throws DataException {
    if (!value.equals(expected))
      failed(property, value, expected);
  }

  default String dataError(byte[] read, byte[]... expected) {
    return dataError(null, read, expected);
  }

  default String dataError(byte read, byte expected) {
    return dataError(null, HexTools.toHex(read), HexTools.toHex(expected));
  }

  default String dataError(String property, byte[] read, byte[]... expected) {
    String exp = expected.length > 1
      ? "either of " + (Arrays.stream(expected).map(x -> HexTools.toHex(x))
        .collect(Collectors.joining(", ", "[", "]")))
      : HexTools.toHex(expected[0]);

    return dataError(property, HexTools.toHex(read), exp);
  }

  default String dataError(String property, String read, String expected) {
    return Language.getString("IDataFile.0") //$NON-NLS-1$
      .replace("%1", this.getName()) //$NON-NLS-1$
      .replace("%2", property != null ? " <" + property + ">": "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      .replace("%3", read) //$NON-NLS-1$
      .replace("%4", expected); //$NON-NLS-1$
  }

  default <T> String valueError(T value, T expected) {
    return valueError(null, value, expected);
  }

  default <T> String valueError(String property, T value, T expected) {
    return Language.getString("IDataFile.1") //$NON-NLS-1$
      .replace("%1", this.getName()) //$NON-NLS-1$
      .replace("%2", property != null ? " <" + property + ">": "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      .replace("%3", String.valueOf(value)) //$NON-NLS-1$
      .replace("%4", String.valueOf(expected)); //$NON-NLS-1$
  }
}
