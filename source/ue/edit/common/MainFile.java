package ue.edit.common;

import java.io.File;
import lombok.Getter;
import lombok.Setter;

@Getter
public abstract class MainFile implements CanBeEdited {

  // true if changes were made to the opened file.
  protected boolean changed = false;

  // true if changes were made to the opened file.
  protected boolean sneaked = false;

  protected File sourceFile = null;

  @Setter
  protected File targetFile = null;

  public File getTargetFile() {
    return targetFile != null ? targetFile : sourceFile;
  }

  @Override
  public void markChanged() {
    changed = true;
  }

  public void markSneaked() {
    sneaked = true;
  }

  @Override
  public void discard() {
    changed = false;
    sneaked = false;
  }

}
