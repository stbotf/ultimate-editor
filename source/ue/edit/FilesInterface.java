package ue.edit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.swing.JOptionPane;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.FileArchive;
import ue.edit.common.FileInfo;
import ue.edit.common.GenericFile;
import ue.edit.common.InternalFile;
import ue.edit.task.ListAllFileInfosTask;
import ue.edit.task.ListFileInfosTask;
import ue.edit.task.LoadAllFilesTask;
import ue.edit.task.LoadFilesTask;
import ue.gui.util.GuiTools;
import ue.service.Language;
import ue.service.UEWorker;

/**
 * This class is used to add/remove internal files to/from CanBeEdited classes
 */
public class FilesInterface {

  @Getter @Accessors(fluent = true)
  private FileArchive archive = null;

  /**
   * Constructor
   *
   * @param file the file to work with
   */
  public FilesInterface(FileArchive file) {
    this.archive = file;
  }

  public Collection<InternalFile> loadFiles(Collection<String> fileNames) {
    // load yet uncached selected files
    val task = new LoadFilesTask(archive, fileNames);
    return GuiTools.runUEWorker(task);
  }

  public void loadAllFiles() {
    // load yet uncached files
    val task = new LoadAllFilesTask(archive);
    GuiTools.runUEWorker(task);
  }

  /**
   * List contained archive file details, sorted by file name.
   * @param fileNames optional list of file names to check
   * @param checkData whether to double-check the data for sneaking changes
   * @return list of determined internal file details.
   * @throws IOException
   */
  public Map<String, FileInfo> listFileInfos(Optional<Collection<String>> fileNames, boolean checkData) throws IOException {
    val task = fileNames.isPresent() ? new ListFileInfosTask(archive, fileNames.get(), checkData)
      : new ListAllFileInfosTask(archive, checkData);
    return GuiTools.runUEWorker(task);
  }

  /**
   * Discards changes.
   * @throws IOException
   */
  public synchronized void discard() throws IOException {
    archive.discard();
  }

  public void discard(String fileName) {
    archive.discard(fileName);
  }

  /**
   * Removes item.
   *
   * @param a the name of the internal file to be removed.
   */
  public synchronized void remove(String a) {
    archive.removeInternalFile(a);
  }

  public synchronized InternalFile getInternalFile(String file) throws IOException {
    return archive.getInternalFile(file, false);
  }

  public synchronized InternalFile getCachedFile(String file) {
    return archive.getCachedFile(file);
  }

  public synchronized Optional<byte[]> getRawData(String file) throws IOException {
    return archive.getRawData(file);
  }

  /**
   * Adds internal file.
   *
   * @param a the files to be added
   */
  public synchronized void add(File[] a) {
    GuiTools.runUEWorker(new ImportTask(a));
  }

  /**
   * Extracts file.
   *
   * @param inFile the names of the files to extract.
   * @param dest   the destination where to extract the file to.
   * @param raw    whether to bypass loaded files but extract the data unparsed.
   * @throws Exception if an exception occurs
   */
  public synchronized void extract(String[] inFile, File dest, boolean raw) throws Exception {
    File file;
    ArrayList<String> files = new ArrayList<String>();

    for (String name : inFile) {
      file = new File(dest, name);

      if (file.isFile() && file.exists()) {
        String msg = Language.getString("FilesInterface.3"); //$NON-NLS-1$

        int respon = JOptionPane.showConfirmDialog(UE.WINDOW, msg.replace("%1", name), //$NON-NLS-1$
            UE.APP_NAME, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (respon == JOptionPane.YES_OPTION) {
          file.delete();
          files.add(name);
        } else {
          continue;
        }
      } else {
        files.add(name);
      }
    }

    GuiTools.runUEWorker(new ExportTask(files.toArray(new String[0]), dest, raw));
  }

  /**
   * Returns the count of loaded files - those currently in buffer.
   */
  public synchronized int getLoadedFileCount() {
    return archive.getLoadedFileCount();
  }

  /**
   * Returns the count of files to be removed/ignored in the next save.
   */
  public synchronized int getRemovedFileCount() {
    return archive.getRemovedFileCount();
  }

  /**
   * Returns an enumeration of loaded files - those currently in buffer.
   */
  public synchronized Set<String> cachedFiles() {
    return archive.cachedFiles();
  }

  /**
   * Returns an iterator of files to be removed/ignored in the next save.
   */
  public synchronized Set<String> removedFiles() {
    return archive.removedFiles();
  }

  // use this for adding files
  private class ImportTask extends UEWorker<Void> {

    private File[] FILES = null;

    public ImportTask(File[] files) {
      super();
      FILES = files;
    }

    @Override
    public Void work() throws IOException {
      String msg = Language.getString("FilesInterface.2"); //$NON-NLS-1$

      for (File file : FILES) {
        if (isCancelled())
          return null;

        // do not lower case the name, let MOTHERSHIP decide if this is needed
        String name = file.getName();
        feedMessage(msg.replace("%1", name));  //$NON-NLS-1$

        byte[] data;
        try (FileInputStream fis = new FileInputStream(file)) {
          data = new byte[fis.available()];

          int start = 0;
          while (fis.available() > 0) {
            start += fis.read(data, start, fis.available());
          }
        }

        // add file
        GenericFile guf = new GenericFile(name, data);
        archive.addInternalFile(guf);
      }

      return null;
    }
  }

  // use this for extracting files
  private class ExportTask extends UEWorker<Void> {

    private String[] NAMES = null;
    private File DEST = null;
    private boolean raw = false;

    public ExportTask(String[] files, File dest, boolean raw) {
      super();
      this.NAMES = files;
      this.DEST = dest;
      this.raw = raw;
    }

    @Override
    public Void work() throws IOException {
      for (String name : NAMES) {
        if (isCancelled())
          return null;

        String msg = Language.getString("FilesInterface.4"); //$NON-NLS-1$
        feedMessage(msg.replace("%1", name)); //$NON-NLS-1$

        File file = new File(DEST.getAbsolutePath(), name);
        file.createNewFile();

        // extract stuff
        if (raw) {
          val data = archive.getRawData(name);
          if (!data.isPresent())
            continue;

          byte[] bytes = data.get();
          if (bytes.length > 0) {
            try (FileOutputStream out = new FileOutputStream(file)) {
              out.write(bytes);
            }
          }
        } else {
          InternalFile ifile = archive.getInternalFile(name, false);
          if (ifile != null) {
            try (FileOutputStream out = new FileOutputStream(file)) {
              ifile.save(out);
            }
          }
        }
      }

      return null;
    }
  }

}
