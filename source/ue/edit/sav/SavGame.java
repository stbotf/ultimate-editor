package ue.edit.sav;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Stream;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.DataFile;
import ue.edit.common.FileArchive;
import ue.edit.common.GenericFile;
import ue.edit.common.InternalFile;
import ue.edit.mod.ModList;
import ue.edit.res.stbof.Stbof;
import ue.edit.sav.common.CSavFileDesc;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavFilesLC;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.SeedInfo;
import ue.edit.sav.files.Version;
import ue.edit.sav.files.ai.AIAgent;
import ue.edit.sav.files.ai.AIAgtIdCtr;
import ue.edit.sav.files.ai.AINumAgents;
import ue.edit.sav.files.ai.AINumTasks;
import ue.edit.sav.files.ai.AIShpUnt;
import ue.edit.sav.files.ai.AIShpUntCnt;
import ue.edit.sav.files.ai.AISysUnt;
import ue.edit.sav.files.ai.AISysUntCnt;
import ue.edit.sav.files.ai.AITask;
import ue.edit.sav.files.ai.AITaskIdCtr;
import ue.edit.sav.files.ai.AgtDp;
import ue.edit.sav.files.ai.AgtDpCnt;
import ue.edit.sav.files.ai.AgtSh;
import ue.edit.sav.files.ai.AgtShCnt;
import ue.edit.sav.files.ai.AgtSy;
import ue.edit.sav.files.ai.AgtSyCnt;
import ue.edit.sav.files.ai.AgtTk;
import ue.edit.sav.files.ai.AgtTkCnt;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.AlienTFInfo;
import ue.edit.sav.files.emp.EmpsInfo;
import ue.edit.sav.files.emp.IntelInfo;
import ue.edit.sav.files.emp.ProvInfo;
import ue.edit.sav.files.emp.RToSInfo;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.emp.StrcInfo;
import ue.edit.sav.files.emp.TechInfo;
import ue.edit.sav.files.emp.Treaty;
import ue.edit.sav.files.map.GListHead;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.GalInfo;
import ue.edit.sav.files.map.MonsterList;
import ue.edit.sav.files.map.OrderInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.ShipCntr;
import ue.edit.sav.files.map.ShipNameDat;
import ue.edit.sav.files.map.SphereOfInfluence;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.TForceCntr;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.TradeInfo;
import ue.edit.sav.files.map.TradeInfoCnt;
import ue.edit.sav.files.map.TskSh;
import ue.edit.sav.files.map.TskShCnt;
import ue.edit.sav.files.map.TskSy;
import ue.edit.sav.files.map.TskSyCnt;
import ue.edit.sav.task.CheckFilesTask;
import ue.edit.sav.task.SaveTask;
import ue.edit.sav.tools.SavDescHelper;
import ue.edit.sav.tools.SavTools;
import ue.exception.DataException;
import ue.gui.util.GuiTools;
import ue.service.FileManager;
import ue.service.Language;
import ue.util.stream.StreamTools;
import ue.util.data.IsCloneable;
import ue.util.file.ArchiveEntry;
import ue.util.file.FileStore;
import ue.util.file.LzssFile;

/**
 * This class is a middle-man between the GUI and sav files. Opens, edits, saves saved games.
 */
public class SavGame extends FileArchive {

  public static final int TYPE = FileManager.P_SAVE_GAME;

  // keep a cached list of the file entries, sorted by name
  // for the file list and the integrity checks
  private TreeMap<String, ArchiveEntry> SAV_ENTRIES = new TreeMap<>();

  // management & helper classes
  @Getter @Accessors(fluent = true) private SavGameInterface files = new SavGameInterface(this);
  @Getter @Accessors(fluent = true) private SavDescHelper descHelper;

  //other
  private Stbof stbof = null;

  //a hash table of all objects
  private ConcurrentHashMap<String, InternalFile> INTERNAL_FILES = new ConcurrentHashMap<>(CSavFiles.CoreFiles.length);
  private HashSet<String> REMOVED = new HashSet<String>();

  // cached galaxy map view mode
  // @return -2 = show none, -1 = show all (cheat), else the race id
  @Getter @Setter private int empireViewMode = -2;

  public static int getEmpireViewMask(int viewMode) {
    return viewMode < -1 ? 0 : viewMode == -1 ? -1 : (1 << viewMode);
  }

  public int getEmpireViewMask() {
    return getEmpireViewMask(empireViewMode);
  }

  @Override
  public int type() {
    return TYPE;
  }

  @Override
  public String getFileDescription(String fileName) {
    return CSavFileDesc.getDescription(fileName);
  }

  public void updateModList() {
    // ignore
  }

  /**
   * Saves changes to the specified file.
   *
   * @param dest The file to which changes are to be saved.
   * @return true if successful else false.
   * @throws IOException
   */
  @Override
  public boolean save(File dest, boolean backup, String extension) throws IOException {
    // if not specified, save to the open file
    if (dest == null)
      dest = getTargetFile();

    val saveTask = new SaveTask(this, dest, backup, extension);
    boolean success = GuiTools.runUEWorker(saveTask);

    if (success) {
      // keep cached files
      super.discard();
      REMOVED.clear();
      // update file entries for removed and added files
      updateSavFileEntries(dest);
    }

    return success;
  }

  /**
   * @param whereFrom The file to open. Must be a valid saved game.
   * @return a String array of valid command that can be used with the edit function.
   * @throws IOException
   */
  @Override
  public void open(File whereFrom) throws IOException {
    SAV_ENTRIES.clear();
    sourceFile = null;
    discard();

    stbof = UE.FILES.stbof();
    descHelper = new SavDescHelper(this, stbof);
    updateSavFileEntries(whereFrom);
  }

  @Override
  public LzssFile access() throws IOException {
    return new LzssFile(sourceFile, null);
  }

  @Override
  public Optional<byte[]> getRawData(String fileName) throws IOException {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();

    // return cached data if generic and unmodified
    InternalFile file = INTERNAL_FILES.get(lcName);
    if (file instanceof GenericFile && !file.madeChanges())
      return Optional.of(((GenericFile)file).getData());

    // LZSS encrypted binary file flag
    boolean binary = SavTools.isBinary(lcName);

    // reload full file
    try (LzssFile zf = new LzssFile(sourceFile, null)) {
      val entry = zf.getEntry(fileName, false);
      if (!entry.isPresent())
        return Optional.empty();

      try (InputStream in = zf.get_LZSS_InputStream(zf.getEntry(fileName),
        binary, SavTools.isChunked(fileName))) {

        // for result.lst we need to decompress each chunk individually
        // to access the raw file data
        return Optional.of(StreamTools.readAllChunks(in));
      }
    }
  }

  @Override
  public InternalFile getCachedFile(String fileName) {
    // for name lookup change to lower case
    return INTERNAL_FILES.get(fileName.toLowerCase());
  }

  @Override
  public void loadReadableFiles(Predicate<String> cbCancel) throws IOException {
    try (LzssFile zf = new LzssFile(sourceFile, null)) {
      zf.processFiles((arc, entry) -> {
        String fileName = entry.getName();
        String lcName = fileName.toLowerCase();

        // skip if already loaded or removed files
        if (INTERNAL_FILES.containsKey(lcName) || REMOVED.contains(lcName))
          return true;

        // stop if cancelled
        if (!cbCancel.test(fileName))
          return false;

        try (val lock = getFileLock(fileName)) {
          // LZSS encrypted binary file flag
          boolean binary = SavTools.isBinary(lcName);
          // create file and pass in the archive, to accelerate loading file dependencies
          InternalFile file = createFile(Optional.of(arc), fileName, true);

          try (InputStream in = zf.get_LZSS_InputStream(entry, binary, SavTools.isChunked(fileName))) {
            file.load(in);
            INTERNAL_FILES.put(lcName, file);
            removeLoadError(fileName);
          } catch (IOException e) {
            addLoadError(fileName, e instanceof DataException ? e.getMessage() : e.toString());
          }
        }

        return true;
      });
    }
  }

  @Override
  protected Optional<InternalFile> loadFile(Optional<FileStore> arch, String fileName, int loadFlags) throws IOException {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();
    boolean reload = (loadFlags & LoadFlags.RELOAD) != 0;
    boolean copy = (loadFlags & LoadFlags.COPY) != 0;

    if (reload) {
      REMOVED.remove(lcName);
    }
    else if (REMOVED.contains(lcName)) {
      if ((loadFlags & LoadFlags.THROW) != 0)
        throwFileNotFound(fileName);
      return Optional.empty();
    }

    // if already loaded, return cached and possibly modified internal file
    InternalFile file = INTERNAL_FILES.get(lcName);
    if (file != null && !reload)
      return Optional.of(copy ? copyFile(arch, fileName, file) : file);

    boolean cache = (loadFlags & LoadFlags.CACHE) != 0;

    try {
      // reloads save game header to load the missing internal file
      try (LzssFile lzss = arch.isPresent() ? null : access()) {
        val zf = lzss != null ? lzss : (LzssFile) arch.get();

        if (file != null) {
          // reuse same old file so any cached references remain valid
          // e.g. the task force and map helper tools otherwise would have to be reset
          file.clear();
          cache = false;
        } else {
          // create new file and pass in the archive, to accelerate loading file dependencies
          file = createFile(Optional.of(zf), fileName, true);
        }

        val ze = zf.getEntry(fileName, (loadFlags & LoadFlags.THROW) != 0);
        if (!ze.isPresent())
          return Optional.empty();

        // LZSS encrypted binary file flag
        boolean binary = SavTools.isBinary(lcName);

        try (InputStream in = zf.get_LZSS_InputStream(ze.get(), binary, SavTools.isChunked(fileName))) {
          file.load(in);
        }
      }

      if (cache)
        INTERNAL_FILES.put(lcName, file);
      if (copy)
        file = copyFile(arch, fileName, file);

      removeLoadError(fileName);
      return Optional.of(file);

    } catch (IOException e) {
      addLoadError(fileName, e instanceof DataException ? e.getMessage() : e.toString());
      throw e;
    }
  }

  private InternalFile copyFile(Optional<FileStore> arch, String fileName, InternalFile file) throws IOException {
    if (file instanceof IsCloneable)
      return (InternalFile) ((IsCloneable) file).clone();

    // fallback to copy the data
    InternalFile copy = createFile(arch, fileName, true);
    copy.load(file.toByteArray());
    return copy;
  }

  private InternalFile createFile(Optional<FileStore> arc, String fileName, boolean defaultToGeneric) throws IOException {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();
    InternalFile a;

    switch (lcName) {
      case CSavFilesLC.SystInfo:
        a = new SystInfo(this, Optional.ofNullable(stbof));
        break;
      case CSavFilesLC.StellInfo:
        a = new StellInfo(this, stbof);
        break;
      case CSavFilesLC.StarBaseInfo:
        a = new StarBaseInfo(this, stbof);
        break;
      case CSavFilesLC.SectorLst:
        a = new Sector(this);
        break;
      case CSavFilesLC.GameInfo:
        a = new GameInfo(this);
        break;
      case CSavFilesLC.GalInfo:
        a = new GalInfo(this);
        break;
      case CSavFilesLC.EmpsInfo:
        a = new EmpsInfo(this);
        break;
      case CSavFilesLC.ResultLst:
        a = new ResultList(this, stbof, true);
        break;
      case CSavFilesLC.GShipHead:
      case CSavFilesLC.GTForceHd:
      case CSavFilesLC.GWTForceHd:
      case CSavFilesLC.GTFStasisHd:
        a = new GListHead(this);
        break;
      case CSavFilesLC.TForceCntr:
        a = new TForceCntr(this);
        break;
      case CSavFilesLC.GTForceList: {
        GListHead header = (GListHead) getInternalFile(arc, CSavFiles.GTForceHd, true);
        TForceCntr cntr = (TForceCntr) getInternalFile(arc, CSavFiles.TForceCntr, true);
        a = new TaskForceList(header, cntr, this, stbof, false);
        break;
      }
      case CSavFilesLC.GWTForce: {
        GListHead header = (GListHead) getInternalFile(arc, CSavFiles.GWTForceHd, true);
        TForceCntr cntr = (TForceCntr) getInternalFile(arc, CSavFiles.TForceCntr, true);
        a = new TaskForceList(header, cntr, this, stbof, true);
        break;
      }
      case CSavFilesLC.MonsterLst:
        a = new MonsterList(this);
        break;
      case CSavFilesLC.OrdInfo:
        a = new OrderInfo(this, stbof);
        break;
      case CSavFilesLC.ShipCntr:
        a = new ShipCntr(this);
        break;
      case CSavFilesLC.GShipList: {
        GListHead header = (GListHead) getInternalFile(arc, CSavFiles.GShipHead, true);
        ShipCntr cntr = (ShipCntr) getInternalFile(arc, CSavFiles.ShipCntr, true);
        a = new GShipList(header, cntr, this, stbof);
        break;
      }
      case CSavFilesLC.ShipNameDat:
        a = new ShipNameDat(stbof);
        break;
      case CSavFilesLC.RToSInfo:
        a = new RToSInfo(this);
        break;
      case CSavFilesLC.StrcInfo:
        a = new StrcInfo(this, stbof);
        break;
      case CSavFilesLC.AlienInfo:
        a = new AlienInfo(this, stbof);
        break;
      case CSavFilesLC.Treaty:
        a = new Treaty(this);
        break;
      case CSavFilesLC.IntelInfo:
        a = new IntelInfo(this);
        break;
      case CSavFilesLC.TechInfo:
        a = new TechInfo(this);
        break;
      case CSavFilesLC.AIAgtIdCtr:
        a = new AIAgtIdCtr(this);
        break;
      case CSavFilesLC.AINumAgents:
        a = new AINumAgents(this);
        break;
      case CSavFilesLC.AINumTasks:
        a = new AINumTasks(this);
        break;
      case CSavFilesLC.AIShpUnt: {
        AIShpUntCnt header = (AIShpUntCnt) getInternalFile(arc, CSavFiles.AIShpUntCnt, true);
        a = new AIShpUnt(header, this);
        break;
      }
      case CSavFilesLC.AIShpUntCnt:
        a = new AIShpUntCnt(this);
        break;
      case CSavFilesLC.AISysUnt: {
        AISysUntCnt header = (AISysUntCnt) getInternalFile(arc, CSavFiles.AISysUntCnt, true);
        a = new AISysUnt(header, this);
        break;
      }
      case CSavFilesLC.AISysUntCnt:
        a = new AISysUntCnt(this);
        break;
      case CSavFilesLC.AITaskIdCtr:
        a = new AITaskIdCtr(this);
        break;
      case CSavFilesLC.Version:
        a = new Version(this);
        break;
      case CSavFilesLC.SeedInfo:
        a = new SeedInfo(this);
        break;
      default: {
        if (lcName.startsWith(CSavFilesLC.AIAgent)) {
          a = new AIAgent(this);
        } else if (lcName.startsWith(CSavFilesLC.AgtDp)) {
          if (lcName.endsWith(CSavFilesLC.AgtDp_Suffix)) {
            a = new AgtDpCnt(this);
          } else {
            AgtDpCnt header = (AgtDpCnt) getInternalFile(arc, fileName + CSavFiles.AgtDp_Suffix, true);
            a = new AgtDp(header, this);
          }
        } else if (lcName.startsWith(CSavFilesLC.AgtSh)) {
          if (lcName.endsWith(CSavFilesLC.AgtSh_Suffix)) {
            a = new AgtShCnt(this);
          } else {
            AgtShCnt header = (AgtShCnt) getInternalFile(arc, fileName + CSavFiles.AgtSh_Suffix, true);
            a = new AgtSh(header, this, stbof);
          }
        } else if (lcName.startsWith(CSavFilesLC.AgtSy)) {
          if (lcName.endsWith(CSavFilesLC.AgtSy_Suffix)) {
            a = new AgtSyCnt(this);
          } else {
            AgtSyCnt header = (AgtSyCnt) getInternalFile(arc, fileName + CSavFiles.AgtSy_Suffix, true);
            a = new AgtSy(header, this);
          }
        } else if (lcName.startsWith(CSavFilesLC.AgtTk)) {
          if (lcName.endsWith(CSavFilesLC.AgtTk_Suffix)) {
            a = new AgtTkCnt(this);
          } else {
            AgtTkCnt header = (AgtTkCnt) getInternalFile(arc, fileName + CSavFiles.AgtTk_Suffix, true);
            a = new AgtTk(header, this);
          }
        } else if (lcName.startsWith(CSavFilesLC.AITask)) {
          a = new AITask(this);
        } else if (lcName.startsWith(CSavFilesLC.AlienTFInfo)) {
          String raceNbr = lcName.substring(CSavFilesLC.AlienTFInfo.length());
          int raceId = Integer.parseInt(raceNbr);
          a = new AlienTFInfo(raceId, this, stbof);
        } else if (lcName.startsWith(CSavFilesLC.ProvInfo)) {
          a = new ProvInfo();
        } else if (lcName.startsWith(CSavFilesLC.TrdeInfo)) {
          if (lcName.endsWith(CSavFilesLC.TrdeInfo_Suffix)) {
            a = new TradeInfoCnt(this);
          } else {
            TradeInfoCnt header = (TradeInfoCnt) getInternalFile(arc, fileName + CSavFiles.TrdeInfo_Suffix, true);
            a = new TradeInfo(header);
          }
        } else if (lcName.startsWith(CSavFilesLC.TskSh)) {
          if (lcName.endsWith(CSavFilesLC.TskSh_Suffix)) {
            a = new TskShCnt(this);
          } else {
            String tskNbr = lcName.substring(CSavFilesLC.TskSh.length());
            int tskId = Integer.parseInt(tskNbr);
            TskShCnt header = (TskShCnt) getInternalFile(arc, fileName + CSavFiles.TskSh_Suffix, true);
            a = new TskSh(tskId, header, this);
          }
        } else if (lcName.startsWith(CSavFilesLC.TskSy)) {
          if (lcName.endsWith(CSavFilesLC.TskSy_Suffix)) {
            a = new TskSyCnt(this);
          } else {
            String tskNbr = lcName.substring(CSavFilesLC.TskSy.length());
            int tskId = Integer.parseInt(tskNbr);
            TskSyCnt header = (TskSyCnt) getInternalFile(arc, fileName + CSavFiles.TskSy_Suffix, true);
            a = new TskSy(tskId, header, this);
          }
        } else if (lcName.startsWith(CSavFilesLC.SphOfIn)) {
          a = new SphereOfInfluence(this);
          break;
        } else if (defaultToGeneric) {
          a = new GenericFile(""); //$NON-NLS-1$
        } else {
          throw new FileNotFoundException(fileName);
        }
      }
    }

    a.setName(fileName);
    return a;
  }

  /**
   * Returns true if the internal file is cached.
   */
  @Override
  public boolean isCached(String fileName) {
    // for name lookup change to lower case
    return INTERNAL_FILES.containsKey(fileName.toLowerCase());
  }

  public boolean isRemoved(String fileName) {
    // for name lookup change to lower case
    return REMOVED.contains(fileName.toLowerCase());
  }

  @Override
  public boolean isChunked(String fileName) {
    return SavTools.isChunked(fileName);
  }

  @Override
  public Class<?> getFileClass(String fileName) {
    return CSavFiles.getFileClass(fileName);
  }

  public static boolean isFileSupported(String fileName) {
    fileName = fileName.toLowerCase();

    switch (fileName) {
      case CSavFilesLC.SystInfo:
      case CSavFilesLC.StellInfo:
      case CSavFilesLC.StarBaseInfo:
      case CSavFilesLC.SectorLst:
      case CSavFilesLC.GameInfo:
      case CSavFilesLC.GalInfo:
      case CSavFilesLC.EmpsInfo:
      case CSavFilesLC.ResultLst:
      case CSavFilesLC.GShipHead:
      case CSavFilesLC.GTForceHd:
      case CSavFilesLC.GWTForceHd:
      case CSavFilesLC.GTFStasisHd:
      case CSavFilesLC.TForceCntr:
      case CSavFilesLC.GTForceList:
      case CSavFilesLC.GWTForce:
      case CSavFilesLC.MonsterLst:
      case CSavFilesLC.OrdInfo:
      case CSavFilesLC.ShipCntr:
      case CSavFilesLC.GShipList:
      case CSavFilesLC.ShipNameDat:
      case CSavFilesLC.RToSInfo:
      case CSavFilesLC.StrcInfo:
      case CSavFilesLC.AlienInfo:
      case CSavFilesLC.Treaty:
      case CSavFilesLC.IntelInfo:
      case CSavFilesLC.TechInfo:
      case CSavFilesLC.AIAgtIdCtr:
      case CSavFilesLC.AINumAgents:
      case CSavFilesLC.AINumTasks:
      case CSavFilesLC.AIShpUnt:
      case CSavFilesLC.AIShpUntCnt:
      case CSavFilesLC.AISysUnt:
      case CSavFilesLC.AISysUntCnt:
      case CSavFilesLC.AITaskIdCtr:
      case CSavFilesLC.Version:
      case CSavFilesLC.SeedInfo:
        return true;
    }

    return fileName.startsWith(CSavFilesLC.AIAgent)
        || fileName.startsWith(CSavFilesLC.AgtDp)
        || fileName.startsWith(CSavFilesLC.AgtSh)
        || fileName.startsWith(CSavFilesLC.AgtSy)
        || fileName.startsWith(CSavFilesLC.AgtTk)
        || fileName.startsWith(CSavFilesLC.AITask)
        || fileName.startsWith(CSavFilesLC.AlienTFInfo)
        || fileName.startsWith(CSavFilesLC.ProvInfo)
        || fileName.startsWith(CSavFilesLC.TrdeInfo)
        || fileName.startsWith(CSavFilesLC.TskSh)
        || fileName.startsWith(CSavFilesLC.TskSy)
        || fileName.startsWith(CSavFilesLC.SphOfIn);
  }

  /**
   * Returns a list of file names for all non-generic sav files supported.
   */
  public ArrayList<String> getFilesSupported() {
    ArrayList<String> savFiles = getFileNames();
    savFiles.removeIf(x -> !isFileSupported(x));
    return savFiles;
  }

  /**
   * Returns a vector containing result of the check.
   */
  @Override
  public Vector<String> check() {
    Vector<String> response = new Vector<String>();

    // collect files to check
    ArrayList<String> filesSupported = getFilesSupported();
    ArrayList<String> filesToCheck = new ArrayList<String>(filesSupported.size());
    HashSet<String> added = new HashSet<String>();

    // check main files first,
    // to fix & resolve dependencies
    for (String file : CSavFiles.CoreFiles) {
      if (added.add(file.toLowerCase()))
        filesToCheck.add(file);
    }

    // collect supported files to check
    for (String file : filesSupported) {
      if (added.add(file.toLowerCase()))
        filesToCheck.add(file);
    }

    // collect cached files last,
    // as they might change every time
    Enumeration<String> ink = INTERNAL_FILES.keys();
    while (ink.hasMoreElements()) {
      String file = ink.nextElement();
      if (added.add(file))
        filesToCheck.add(file);
    }

    // perform file checks
    val task = new CheckFilesTask(this, filesToCheck);
    response = GuiTools.runUEWorker(task);

    if (task.isCancelled())
      response.add(Language.getString("SavGame.0")); //$NON-NLS-1$
    else if (response.isEmpty())
      response.add(Language.getString("SavGame.1")); //$NON-NLS-1$

    return response;
  }

  @Override
  public void addInternalFile(InternalFile file) throws IOException {
    // added files are considered changed, to make sure the save dialog pops up
    file.markChanged();

    String fileName = file.getName();
    if (fileName.length() > 15) {
      String msg = Language.getString("SavGame.2"); //$NON-NLS-1$
      msg = msg.replace("%1", fileName); //$NON-NLS-1$
      throw new IOException(msg);
    }

    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();

    // clean from removed files
    REMOVED.remove(lcName);

    // only instances derived from GenericFile are handled below
    if (!(file instanceof GenericFile)) {
      INTERNAL_FILES.put(lcName, file);
      markChanged();
      return;
    }

    try {
      InternalFile a = file;

      switch (lcName) {
        case CSavFilesLC.SystInfo:
          a = new SystInfo(this, Optional.ofNullable(stbof));
          break;
        case CSavFilesLC.StellInfo:
          a = new StellInfo(this, stbof);
          break;
        case CSavFilesLC.StarBaseInfo:
          a = new StarBaseInfo(this, stbof);
          break;
        case CSavFilesLC.SectorLst:
          a = new Sector(this);
          break;
        case CSavFilesLC.GameInfo:
          a = new GameInfo(this);
          break;
        case CSavFilesLC.GalInfo:
          a = new GalInfo(this);
          break;
        case CSavFilesLC.EmpsInfo:
          a = new EmpsInfo(this);
          break;
        case CSavFilesLC.ResultLst:
          a = new ResultList(this, stbof, true);
          break;
        case CSavFilesLC.GShipHead:
        case CSavFilesLC.GTForceHd:
        case CSavFilesLC.GWTForceHd:
        case CSavFilesLC.GTFStasisHd:
          a = new GListHead(this);
          break;
        case CSavFilesLC.TForceCntr:
          a = new TForceCntr(this);
          break;
        case CSavFilesLC.GTForceList: {
          GListHead header = (GListHead) getInternalFile(CSavFiles.GTForceHd, true);
          TForceCntr cntr = (TForceCntr) getInternalFile(CSavFiles.TForceCntr, true);
          a = new TaskForceList(header, cntr, this, stbof, false);
          break;
        }
        case CSavFilesLC.GWTForce: {
          GListHead header = (GListHead) getInternalFile(CSavFiles.GWTForceHd, true);
          TForceCntr cntr = (TForceCntr) getInternalFile(CSavFiles.TForceCntr, true);
          a = new TaskForceList(header, cntr, this, stbof, true);
          break;
        }
        case CSavFilesLC.MonsterLst:
          a = new MonsterList(this);
          break;
        case CSavFilesLC.OrdInfo:
          a = new OrderInfo(this, stbof);
          break;
        case CSavFilesLC.ShipCntr:
          a = new ShipCntr(this);
          break;
        case CSavFilesLC.GShipList: {
          GListHead header = (GListHead) getInternalFile(CSavFiles.GShipHead, true);
          ShipCntr cntr = (ShipCntr) getInternalFile(CSavFiles.ShipCntr, true);
          a = new GShipList(header, cntr, this, stbof);
          break;
        }
        case CSavFilesLC.ShipNameDat:
          a = new ShipNameDat(stbof);
          break;
        case CSavFilesLC.RToSInfo:
          a = new RToSInfo(this);
          break;
        case CSavFilesLC.StrcInfo:
          a = new StrcInfo(this, stbof);
          break;
        case CSavFilesLC.AlienInfo:
          a = new AlienInfo(this, stbof);
          break;
        case CSavFilesLC.Treaty:
          a = new Treaty(this);
          break;
        case CSavFilesLC.IntelInfo:
          a = new IntelInfo(this);
          break;
        case CSavFilesLC.TechInfo:
          a = new TechInfo(this);
          break;
        case CSavFilesLC.AIAgtIdCtr:
          a = new AIAgtIdCtr(this);
          break;
        case CSavFilesLC.AINumAgents:
          a = new AINumAgents(this);
          break;
        case CSavFilesLC.AINumTasks:
          a = new AINumTasks(this);
          break;
        case CSavFilesLC.AIShpUnt: {
          AIShpUntCnt header = (AIShpUntCnt) getInternalFile(CSavFiles.AIShpUntCnt, true);
          a = new AIShpUnt(header, this);
          break;
        }
        case CSavFilesLC.AIShpUntCnt:
          a = new AIShpUntCnt(this);
          break;
        case CSavFilesLC.AISysUnt: {
          AISysUntCnt header = (AISysUntCnt) getInternalFile(CSavFiles.AISysUntCnt, true);
          a = new AISysUnt(header, this);
          break;
        }
        case CSavFilesLC.AISysUntCnt:
          a = new AISysUntCnt(this);
          break;
        case CSavFilesLC.AITaskIdCtr:
          a = new AITaskIdCtr(this);
          break;
        case CSavFilesLC.Version:
          a = new Version(this);
          break;
        case CSavFilesLC.SeedInfo:
          a = new SeedInfo(this);
          break;
        default: {
          if (lcName.startsWith(CSavFilesLC.AIAgent)) {
            a = new AIAgent(this);
          } else if (lcName.startsWith(CSavFilesLC.AgtDp)) {
            if (lcName.endsWith(CSavFilesLC.AgtDp_Suffix)) {
              a = new AgtDpCnt(this);
            } else {
              AgtDpCnt header = (AgtDpCnt) getInternalFile(fileName + CSavFiles.AgtDp_Suffix, true);
              a = new AgtDp(header, this);
            }
          } else if (lcName.startsWith(CSavFilesLC.AgtSh)) {
            if (lcName.endsWith(CSavFilesLC.AgtSh_Suffix)) {
              a = new AgtShCnt(this);
            } else {
              AgtShCnt header = (AgtShCnt) getInternalFile(fileName + CSavFiles.AgtSh_Suffix, true);
              a = new AgtSh(header, this, stbof);
            }
          } else if (lcName.startsWith(CSavFilesLC.AgtSy)) {
            if (lcName.endsWith(CSavFilesLC.AgtSy_Suffix)) {
              a = new AgtSyCnt(this);
            } else {
              AgtSyCnt header = (AgtSyCnt) getInternalFile(fileName + CSavFiles.AgtSy_Suffix, true);
              a = new AgtSy(header, this);
            }
          } else if (lcName.startsWith(CSavFilesLC.AgtTk)) {
            if (lcName.endsWith(CSavFilesLC.AgtTk_Suffix)) {
              a = new AgtTkCnt(this);
            } else {
              AgtTkCnt header = (AgtTkCnt) getInternalFile(fileName + CSavFiles.AgtTk_Suffix, true);
              a = new AgtTk(header, this);
            }
          } else if (lcName.startsWith(CSavFilesLC.AITask)) {
            a = new AITask(this);
          } else if (lcName.startsWith(CSavFilesLC.AlienTFInfo)) {
            String raceNbr = lcName.substring(CSavFilesLC.AlienTFInfo.length());
            int raceId = Integer.parseInt(raceNbr);
            a = new AlienTFInfo(raceId, this, stbof);
          } else if (lcName.startsWith(CSavFilesLC.ProvInfo)) {
            a = new ProvInfo();
          } else if (lcName.startsWith(CSavFilesLC.TrdeInfo)) {
            if (lcName.endsWith(CSavFilesLC.TrdeInfo_Suffix)) {
              a = new TradeInfoCnt(this);
            } else {
              TradeInfoCnt header = (TradeInfoCnt) getInternalFile(
                fileName + CSavFiles.TrdeInfo_Suffix, true);
              a = new TradeInfo(header);
            }
          } else if (lcName.startsWith(CSavFilesLC.TskSh)) {
            if (lcName.endsWith(CSavFilesLC.TskSh_Suffix)) {
              a = new TskShCnt(this);
            } else {
              String tskNbr = lcName.substring(CSavFilesLC.TskSh.length());
              int tskId = Integer.parseInt(tskNbr);
              TskShCnt header = (TskShCnt) getInternalFile(fileName + CSavFiles.TskSh_Suffix, true);
              a = new TskSh(tskId, header, this);
            }
          } else if (lcName.startsWith(CSavFilesLC.TskSy)) {
            if (lcName.endsWith(CSavFilesLC.TskSy_Suffix)) {
              a = new TskSyCnt(this);
            } else {
              String tskNbr = lcName.substring(CSavFilesLC.TskSy.length());
              int tskId = Integer.parseInt(tskNbr);
              TskSyCnt header = (TskSyCnt) getInternalFile(fileName + CSavFiles.TskSy_Suffix, true);
              a = new TskSy(tskId, header, this);
            }
          } else if (lcName.startsWith(CSavFilesLC.SphOfIn)) {
            a = new SphereOfInfluence(this);
            break;
          } else {
            a = new GenericFile(""); //$NON-NLS-1$
          }
        }
      }

      a.setName(fileName);
      a.load(file.toByteArray());
      a.markChanged();
      INTERNAL_FILES.put(lcName, a);
      markChanged();
    } catch (Exception u) {
      u.printStackTrace();
    }
  }

  @Override
  public void discard() {
    super.discard();
    INTERNAL_FILES.clear();
    files.clear();
    REMOVED.clear();
  }

  /**
   * Call this function to discard single file changes.
   */
  @Override
  public void discard(String fileName) {
    super.discard(fileName);

    // for name lookup change to lower case
    fileName = fileName.toLowerCase();

    // remove from cache to restore unmodified data
    if (INTERNAL_FILES.remove(fileName) != null)
      files.clear(fileName);

    // clear removed state
    REMOVED.remove(fileName);
  }

  @Override
  public void discard(FilenameFilter filter) {
    if (filter == null) {
      discard();
    } else {
      INTERNAL_FILES.values().removeIf(x -> {
        if (filter.accept(sourceFile, x.getName())) {
          files.clear(x.getName());
          return true;
        }
        return false;
      });
      REMOVED.removeIf(x -> filter.accept(sourceFile, x));
    }
  }

  @Override
  public Collection<InternalFile> listCachedFiles() {
    return Collections.unmodifiableCollection(INTERNAL_FILES.values());
  }

  @Override
  public Stream<InternalFile> streamCachedFiles() {
    return INTERNAL_FILES.values().stream();
  }

  /**
   * Get all known game.sav file entries, sorted by name.
   */
  @Override
  public Map<String, ArchiveEntry> getArchiveEntries() {
    return Collections.unmodifiableMap(SAV_ENTRIES);
  }

  /**
   * Get known game.sav file entry by name.
   */
  @Override
  public ArchiveEntry getArchiveEntry(String fileName) {
    // for name lookup change to lower case
    return SAV_ENTRIES.get(fileName.toLowerCase());
  }

  @Override
  public void processCachedFiles(Predicate<DataFile> callback) {
    //GO THRU LOADED FILES
    for (val file : INTERNAL_FILES.values()) {
      if (!callback.test(file))
        break;
    }
  }

  /**
   * Process all cached save file entries matching the filter and predicate.
   * @param filter file name filter, can be null
   */
  @Override
  public void processCachedFiles(FilenameFilter filter, Predicate<DataFile> callback) {
    //GO THRU LOADED FILES
    for (val file : INTERNAL_FILES.values()) {
      if (filter != null && !filter.accept(sourceFile, file.getName()))
        continue;
      if (!callback.test(file))
        break;
    }
  }

  @Override
  public int getLoadedFileCount() {
    return INTERNAL_FILES.size();
  }

  @Override
  public int getRemovedFileCount() {
    return REMOVED.size();
  }

  @Override
  public Set<String> cachedFiles() {
    return INTERNAL_FILES.keySet();
  }

  @Override
  public Set<String> removedFiles() {
    return Collections.unmodifiableSet(REMOVED);
  }

  @Override
  public void removeInternalFile(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    INTERNAL_FILES.remove(fileName);
    files.clear(fileName);
    REMOVED.add(fileName);
    markChanged();
  }

  private void throwFileNotFound(String fileName) throws FileNotFoundException {
    String msg = Language.getString("SavGame.3") //$NON-NLS-1$
      .replace("%1", fileName); //$NON-NLS-1$
    throw new FileNotFoundException(msg);
  }

  @Override
  public ModList modList() {
    return null;
  }

  @Override
  public boolean isModded() {
    return !REMOVED.isEmpty() || INTERNAL_FILES.values().stream().anyMatch(x -> x.madeChanges());
  }

  @Override
  public boolean isChanged(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    if (REMOVED.contains(fileName))
      return true;

    InternalFile file = INTERNAL_FILES.get(fileName);
    return file != null && file.madeChanges();
  }

  private void updateSavFileEntries(File whereFrom) throws IOException {
    try (LzssFile sf = new LzssFile(whereFrom, null)) {
      // check on missing key files
      for (String file : CSavFiles.CoreFiles) {
        if (!sf.contains(file))
          throwFileNotFound(file);
      }

      loadSavFileEntries(sf);
      sourceFile = whereFrom;
    }
  }

  private void loadSavFileEntries(LzssFile sf) {
    SAV_ENTRIES.clear();
    for (val ze : sf.listFileEntries()) {
      SAV_ENTRIES.put(ze.getLcName(), ze);
    }
  }

}
