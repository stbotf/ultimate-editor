package ue.edit.sav;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import ue.UE;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavFilesLC;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.ai.AIShpUnt;
import ue.edit.sav.files.ai.AISysUnt;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.EmpsInfo;
import ue.edit.sav.files.emp.ProvInfo;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.emp.RToSInfo;
import ue.edit.sav.files.emp.StrcInfo;
import ue.edit.sav.files.emp.TechInfo;
import ue.edit.sav.files.emp.Treaty;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.GalInfo;
import ue.edit.sav.files.map.MonsterList;
import ue.edit.sav.files.map.OrderInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.TradeInfo;
import ue.edit.sav.files.map.TradeInfoCnt;
import ue.edit.sav.tools.AIAgentMgr;
import ue.edit.sav.tools.AITaskMgr;
import ue.edit.sav.tools.AITaskTools;
import ue.edit.sav.tools.MapTools;
import ue.edit.sav.tools.SavFileFilters;
import ue.edit.sav.tools.SectorTools;
import ue.edit.sav.tools.StarbaseTools;
import ue.edit.sav.tools.StellarTools;
import ue.edit.sav.tools.SystemGen;
import ue.edit.sav.tools.SystemTools;
import ue.edit.sav.tools.TaskForceTools;

/**
 * This class is a cached interface to the internal game.sav files.
 */
public class SavGameInterface {

  // common sav files for better lookup
  private AIShpUnt            aiShpUnt = null;
  private AISysUnt            aiSysUnt = null;
  private AlienInfo           alienInfo = null;
  private EmpsInfo            empsInfo = null;
  private GShipList           gShipList = null;
  private GalInfo             galInfo = null;
  private GameInfo            gameInfo = null;
  private TaskForceList       gtfList = null;
  private TaskForceList       gwtForce = null;
  private MonsterList         monsterList = null;
  private OrderInfo           ordInfo = null;
  private List<ProvInfo>      provInfos = null;
  private ResultList          resultList = null;
  private RToSInfo            rToSInfo = null;
  private StarBaseInfo        sbInfo = null;
  private Sector              sectorLst = null;
  private StellInfo           stellInfo = null;
  private StrcInfo            strcInfo = null;
  private SystInfo            systInfo = null;
  private TechInfo            techInfo = null;
  private List<TradeInfo>     tradeInfos = null;
  private List<TradeInfoCnt>  tradeInfoCnts = null;
  private Treaty              treaty = null;

  // management & helper classes
  private AIAgentMgr          aiAgtMgr = null;
  private AITaskMgr           aiTskMgr = null;
  private AITaskTools         aiTaskTools = null;
  private MapTools            mapTools = null;
  private SectorTools         sectorTools = null;
  private StarbaseTools       starbaseTools = null;
  private StellarTools        stellarTools = null;
  private SystemTools         sysTools = null;
  private SystemGen           systemGen = null;
  private TaskForceTools      taskForceTools = null;

  private SavGame sav;

  public SavGameInterface(SavGame sav) {
    this.sav = sav;
  }

  // #region management & helper classes

  public AIAgentMgr aiAgentMgr() {
    if (aiAgtMgr == null)
      aiAgtMgr = new AIAgentMgr(sav);
    return aiAgtMgr;
  }

  public AITaskMgr aiTaskMgr() {
    if (aiTskMgr == null)
      aiTskMgr = new AITaskMgr(sav);
    return aiTskMgr;
  }

  public AITaskTools aiTaskTools() {
    if (aiTaskTools == null)
      aiTaskTools = new AITaskTools(sav);
    return aiTaskTools;
  }

  public MapTools mapTools() {
    if (mapTools == null)
      mapTools = new MapTools(sav);
    return mapTools;
  }

  public SectorTools sectorTools() {
    if (sectorTools == null)
      sectorTools = new SectorTools(sav, UE.FILES.stbof());
    return sectorTools;
  }

  public StarbaseTools starbaseTools() {
    if (starbaseTools == null)
      starbaseTools = new StarbaseTools(sav);
    return starbaseTools;
  }

  public StellarTools stellarTools() {
    if (stellarTools == null)
      stellarTools = new StellarTools(sav);
    return stellarTools;
  }

  public SystemTools systemTools() {
    if (sysTools == null)
      sysTools = new SystemTools(sav);
    return sysTools;
  }

  public SystemGen systemGen() throws IOException {
    if (systemGen == null)
      systemGen = new SystemGen(sav, UE.FILES.stbof());
    return systemGen;
  }

  public TaskForceTools taskForceTools() {
    if (taskForceTools == null)
      taskForceTools = new TaskForceTools(sav, UE.FILES.stbof());
    return taskForceTools;
  }

  // #endregion management & helper classes

  // #region checked InternalFile getters

  public AIShpUnt aiShpUnt() throws IOException {
    if (aiShpUnt == null)
      aiShpUnt = (AIShpUnt) sav.getInternalFile(CSavFiles.AIShpUnt, true);
    return aiShpUnt;
  }

  public AISysUnt aiSysUnt() throws IOException {
    if (aiSysUnt == null)
      aiSysUnt = (AISysUnt) sav.getInternalFile(CSavFiles.AISysUnt, true);
    return aiSysUnt;
  }

  public AlienInfo alienInfo() throws IOException {
    if (alienInfo == null)
      alienInfo = (AlienInfo) sav.getInternalFile(CSavFiles.AlienInfo, true);
    return alienInfo;
  }

  public EmpsInfo empsInfo() throws IOException {
    if (empsInfo == null)
      empsInfo = (EmpsInfo) sav.getInternalFile(CSavFiles.EmpsInfo, true);
    return empsInfo;
  }

  public GShipList gShipList() throws IOException {
    if (gShipList == null)
      gShipList = (GShipList) sav.getInternalFile(CSavFiles.GShipList, true);
    return gShipList;
  }

  public GalInfo galInfo() throws IOException {
    if (galInfo == null)
      galInfo = (GalInfo) sav.getInternalFile(CSavFiles.GalInfo, true);
    return galInfo;
  }

  public GameInfo gameInfo() throws IOException {
    if (gameInfo == null)
      gameInfo = (GameInfo) sav.getInternalFile(CSavFiles.GameInfo, true);
    return gameInfo;
  }

  public TaskForceList gtfList() throws IOException {
    if (gtfList == null)
      gtfList = (TaskForceList) sav.getInternalFile(CSavFiles.GTForceList, true);
    return gtfList;
  }

  public TaskForceList gwtForce() throws IOException {
    if (gwtForce == null)
      gwtForce = (TaskForceList) sav.getInternalFile(CSavFiles.GWTForce, true);
    return gwtForce;
  }

  public MonsterList monsterList() throws IOException {
    if (monsterList == null)
      monsterList = (MonsterList) sav.getInternalFile(CSavFiles.MonsterLst, true);
    return monsterList;
  }

  public OrderInfo ordInfo() throws IOException {
    if (ordInfo == null)
      ordInfo = (OrderInfo) sav.getInternalFile(CSavFiles.OrdInfo, true);
    return ordInfo;
  }

  public List<ProvInfo> provInfos() throws IOException {
    if (provInfos == null)
      provInfos = new ArrayList<>(CStbof.NUM_EMPIRES);

    // load all the five provInfo files
    for (int i = 0; i < CStbof.NUM_EMPIRES; ++i) {
      String fileName = CSavFiles.ProvInfo + Integer.toString(i);
      if (i >= provInfos.size())
        provInfos.add(i, (ProvInfo) sav.getInternalFile(fileName, true));
      else if (provInfos.get(i) == null)
        provInfos.set(i, (ProvInfo) sav.getInternalFile(fileName, true));
    }

    return Collections.unmodifiableList(provInfos);
  }

  public ProvInfo provInfo(int raceId) throws IOException {
    // check for existing provInfo
    ProvInfo info = cachedProvInfo(raceId);
    if (info != null)
      return info;

    // first try to load the missing file
    String fileName = CSavFiles.ProvInfo + Integer.toString(raceId);
    ProvInfo provInfo = (ProvInfo) sav.getInternalFile(fileName, true);

    updateProvInfo(raceId, provInfo);
    return provInfo;
  }

  public ResultList resultList() throws IOException {
    if (resultList == null)
      resultList = (ResultList) sav.getInternalFile(CSavFiles.ResultLst, true);
    return resultList;
  }

  public RToSInfo rToS() throws IOException {
    if (rToSInfo == null)
      rToSInfo = (RToSInfo) sav.getInternalFile(CSavFiles.RToSInfo, true);
    return rToSInfo;
  }

  public StarBaseInfo sbInfo() throws IOException {
    if (sbInfo == null)
      sbInfo = (StarBaseInfo) sav.getInternalFile(CSavFiles.StarBaseInfo, true);
    return sbInfo;
  }

  public Sector sectorLst() throws IOException {
    if (sectorLst == null)
      sectorLst = (Sector) sav.getInternalFile(CSavFiles.SectorLst, true);
    return sectorLst;
  }

  public StellInfo stellInfo() throws IOException {
    if (stellInfo == null)
      stellInfo = (StellInfo) sav.getInternalFile(CSavFiles.StellInfo, true);
    return stellInfo;
  }

  public StrcInfo strcInfo() throws IOException {
    if (strcInfo == null)
      strcInfo = (StrcInfo) sav.getInternalFile(CSavFiles.StrcInfo, true);
    return strcInfo;
  }

  public SystInfo systInfo() throws IOException {
    if (systInfo == null)
      systInfo = (SystInfo) sav.getInternalFile(CSavFiles.SystInfo, true);
    return systInfo;
  }

  public TechInfo techInfo() throws IOException {
    if (techInfo == null)
      techInfo = (TechInfo) sav.getInternalFile(CSavFiles.TechInfo, true);
    return techInfo;
  }

  public List<TradeInfo> tradeInfos() throws IOException {
    if (tradeInfos == null)
      tradeInfos = new ArrayList<>(CStbof.NUM_EMPIRES);

    // load all the five trdeInfo files
    for (int i = 0; i < CStbof.NUM_EMPIRES; ++i) {
      String fileName = CSavFiles.TrdeInfo + Integer.toString(i);
      if (i >= tradeInfos.size())
        tradeInfos.add(i, (TradeInfo) sav.getInternalFile(fileName, true));
      else if (tradeInfos.get(i) == null)
        tradeInfos.set(i, (TradeInfo) sav.getInternalFile(fileName, true));
    }

    return Collections.unmodifiableList(tradeInfos);
  }

  public TradeInfo tradeInfo(int raceId) throws IOException {
    // check for existing trdeInfo
    TradeInfo info = cachedTradeInfo(raceId);
    if (info != null)
      return info;

    // first try to load the missing file
    String fileName = CSavFiles.TrdeInfo + Integer.toString(raceId);
    TradeInfo tradeInfo = (TradeInfo) sav.getInternalFile(fileName, true);

    updateTradeInfo(raceId, tradeInfo);
    return tradeInfo;
  }

  public List<TradeInfoCnt> tradeInfoCnts() throws IOException {
    if (tradeInfoCnts == null)
      tradeInfoCnts = new ArrayList<>(CStbof.NUM_EMPIRES);

    // load all the five trdeInfo*.cnt files
    for (int i = 0; i < CStbof.NUM_EMPIRES; ++i) {
      String fileName = CSavFiles.TrdeInfo + Integer.toString(i) + CSavFiles.TrdeInfo_Suffix;
      if (i >= tradeInfoCnts.size())
        tradeInfoCnts.add(i, (TradeInfoCnt) sav.getInternalFile(fileName, true));
      else if (tradeInfoCnts.get(i) == null)
        tradeInfoCnts.set(i, (TradeInfoCnt) sav.getInternalFile(fileName, true));
    }

    return Collections.unmodifiableList(tradeInfoCnts);
  }

  public TradeInfoCnt tradeInfoCnt(int raceId) throws IOException {
    // check for existing trdeInfo*.cnt
    TradeInfoCnt info = cachedTradeInfoCnt(raceId);
    if (info != null)
      return info;

    // first try to load the missing file
    String fileName = CSavFiles.TrdeInfo + Integer.toString(raceId) + CSavFiles.TrdeInfo_Suffix;
    TradeInfoCnt tradeInfoCnt = (TradeInfoCnt) sav.getInternalFile(fileName, true);

    updateTradeInfoCnt(raceId, tradeInfoCnt);
    return tradeInfoCnt;
  }

  public Treaty treaty() throws IOException {
    if (treaty == null)
      treaty = (Treaty) sav.getInternalFile(CSavFiles.Treaty, true);
    return treaty;
  }

  // #endregion checked InternalFile getters

  // #region unchecked InternalFile lookup

  public Optional<AIShpUnt> findAiShpUnt() {
    if (aiShpUnt == null)
      aiShpUnt = (AIShpUnt) sav.tryGetInternalFile(CSavFiles.AIShpUnt, true);
    return Optional.ofNullable(aiShpUnt);
  }

  public Optional<AISysUnt> findAiSysUnt() {
    if (aiSysUnt == null)
      aiSysUnt = (AISysUnt) sav.tryGetInternalFile(CSavFiles.AISysUnt, true);
    return Optional.ofNullable(aiSysUnt);
  }

  public Optional<AlienInfo> findAlienInfo() {
    if (alienInfo == null)
      alienInfo = (AlienInfo) sav.tryGetInternalFile(CSavFiles.AlienInfo, true);
    return Optional.ofNullable(alienInfo);
  }

  public Optional<EmpsInfo> findEmpsInfo() {
    if (empsInfo == null)
      empsInfo = (EmpsInfo) sav.tryGetInternalFile(CSavFiles.EmpsInfo, true);
    return Optional.ofNullable(empsInfo);
  }

  public Optional<GShipList> findGShipList() {
    if (gShipList == null)
      gShipList = (GShipList) sav.tryGetInternalFile(CSavFiles.GShipList, true);
    return Optional.ofNullable(gShipList);
  }

  public Optional<GalInfo> findGalInfo() {
    if (galInfo == null)
      galInfo = (GalInfo) sav.tryGetInternalFile(CSavFiles.GalInfo, true);
    return Optional.ofNullable(galInfo);
  }

  public Optional<GameInfo> findGameInfo() {
    if (gameInfo == null)
      gameInfo = (GameInfo) sav.tryGetInternalFile(CSavFiles.GameInfo, true);
    return Optional.ofNullable(gameInfo);
  }

  public Optional<TaskForceList> findGtfList() {
    if (gtfList == null)
      gtfList = (TaskForceList) sav.tryGetInternalFile(CSavFiles.GTForceList, true);
    return Optional.ofNullable(gtfList);
  }

  public Optional<TaskForceList> findGwtForce() {
    if (gwtForce == null)
      gwtForce = (TaskForceList) sav.tryGetInternalFile(CSavFiles.GWTForce, true);
    return Optional.ofNullable(gwtForce);
  }

  public Optional<MonsterList> findMonsterList() {
    if (monsterList == null)
      monsterList = (MonsterList) sav.tryGetInternalFile(CSavFiles.MonsterLst, true);
    return Optional.ofNullable(monsterList);
  }

  public Optional<OrderInfo> findOrdInfo() {
    if (ordInfo == null)
      ordInfo = (OrderInfo) sav.tryGetInternalFile(CSavFiles.OrdInfo, true);
    return Optional.ofNullable(ordInfo);
  }

  public List<ProvInfo> findProvInfos() {
    if (provInfos == null)
      provInfos = new ArrayList<>(CStbof.NUM_EMPIRES);

    // load all the five provInfo files
    for (int i = 0; i < CStbof.NUM_EMPIRES; ++i) {
      String fileName = CSavFiles.ProvInfo + Integer.toString(i);
      if (i >= provInfos.size())
        provInfos.add(i, (ProvInfo) sav.tryGetInternalFile(fileName, true));
      else if (provInfos.get(i) == null)
        provInfos.set(i, (ProvInfo) sav.tryGetInternalFile(fileName, true));
    }

    return Collections.unmodifiableList(provInfos);
  }

  public Optional<ProvInfo> findProvInfo(int raceId) {
    // check for existing provInfo
    ProvInfo info = cachedProvInfo(raceId);
    if (info != null)
      return Optional.of(info);

    // first try to load the missing file
    String fileName = CSavFiles.ProvInfo + Integer.toString(raceId);
    ProvInfo provInfo = (ProvInfo) sav.tryGetInternalFile(fileName, true);
    if (provInfo != null)
      updateProvInfo(raceId, provInfo);

    return Optional.ofNullable(provInfo);
  }

  public Optional<ResultList> findResultList() {
    if (resultList == null)
      resultList = (ResultList) sav.tryGetInternalFile(CSavFiles.ResultLst, true);
    return Optional.ofNullable(resultList);
  }

  public Optional<RToSInfo> findRToS() {
    if (rToSInfo == null)
      rToSInfo = (RToSInfo) sav.tryGetInternalFile(CSavFiles.RToSInfo, true);
    return Optional.ofNullable(rToSInfo);
  }

  public Optional<StarBaseInfo> findSbInfo() {
    if (sbInfo == null)
      sbInfo = (StarBaseInfo) sav.tryGetInternalFile(CSavFiles.StarBaseInfo, true);
    return Optional.ofNullable(sbInfo);
  }

  public Optional<Sector> findSectorLst() {
    if (sectorLst == null)
      sectorLst = (Sector) sav.tryGetInternalFile(CSavFiles.SectorLst, true);
    return Optional.ofNullable(sectorLst);
  }

  public Optional<StellInfo> findStellInfo() {
    if (stellInfo == null)
      stellInfo = (StellInfo) sav.tryGetInternalFile(CSavFiles.StellInfo, true);
    return Optional.ofNullable(stellInfo);
  }

  public Optional<SystInfo> findSystInfo() {
    if (systInfo == null)
      systInfo = (SystInfo) sav.tryGetInternalFile(CSavFiles.SystInfo, true);
    return Optional.ofNullable(systInfo);
  }

  public Optional<TechInfo> findTechInfo() {
    if (techInfo == null)
      techInfo = (TechInfo) sav.tryGetInternalFile(CSavFiles.TechInfo, true);
    return Optional.ofNullable(techInfo);
  }

  public List<TradeInfo> findTradeInfos() {
    if (tradeInfos == null)
      tradeInfos = new ArrayList<>(CStbof.NUM_EMPIRES);

    // load all the five trdeInfo files
    for (int i = 0; i < CStbof.NUM_EMPIRES; ++i) {
      String fileName = CSavFiles.TrdeInfo + Integer.toString(i);
      if (i >= tradeInfos.size())
        tradeInfos.add(i, (TradeInfo) sav.tryGetInternalFile(fileName, true));
      else if (tradeInfos.get(i) == null)
        tradeInfos.set(i, (TradeInfo) sav.tryGetInternalFile(fileName, true));
    }

    return Collections.unmodifiableList(tradeInfos);
  }

  public Optional<TradeInfo> findTradeInfo(int raceId) {
    // check for existing trdeInfo
    TradeInfo info = cachedTradeInfo(raceId);
    if (info != null)
      return Optional.of(info);

    // first try to load the missing file
    String fileName = CSavFiles.TrdeInfo + Integer.toString(raceId);
    TradeInfo tradeInfo = (TradeInfo) sav.tryGetInternalFile(fileName, true);
    if (tradeInfo != null)
      updateTradeInfo(raceId, tradeInfo);

    return Optional.ofNullable(tradeInfo);
  }

  public List<TradeInfoCnt> findTradeInfoCnts() {
    if (tradeInfoCnts == null)
      tradeInfoCnts = new ArrayList<>(CStbof.NUM_EMPIRES);

    // load all the five trdeInfo*.cnt files
    for (int i = 0; i < CStbof.NUM_EMPIRES; ++i) {
      String fileName = CSavFiles.TrdeInfo + Integer.toString(i) + CSavFiles.TrdeInfo_Suffix;
      if (i >= tradeInfoCnts.size())
        tradeInfoCnts.add(i, (TradeInfoCnt) sav.tryGetInternalFile(fileName, true));
      else if (tradeInfoCnts.get(i) == null)
        tradeInfoCnts.set(i, (TradeInfoCnt) sav.tryGetInternalFile(fileName, true));
    }

    return Collections.unmodifiableList(tradeInfoCnts);
  }

  public Optional<TradeInfoCnt> findTradeInfoCnt(int raceId) {
    // check for existing trdeInfo*.cnt
    TradeInfoCnt info = cachedTradeInfoCnt(raceId);
    if (info != null)
      return Optional.of(info);

    // first try to load the missing file
    String fileName = CSavFiles.TrdeInfo + Integer.toString(raceId) + CSavFiles.TrdeInfo_Suffix;
    TradeInfoCnt tradeInfoCnt = (TradeInfoCnt) sav.tryGetInternalFile(fileName, true);
    if (tradeInfoCnt != null)
      updateTradeInfoCnt(raceId, tradeInfoCnt);

    return Optional.ofNullable(tradeInfoCnt);
  }

  public Optional<Treaty> findTreaty() {
    if (treaty == null)
      treaty = (Treaty) sav.tryGetInternalFile(CSavFiles.Treaty, true);
    return Optional.ofNullable(treaty);
  }

  // #endregion unchecked InternalFile lookup

  // #region cleanup

  /**
   * Call this function to clear all cached files.
   */
  public void clear() {
    // tools
    aiAgtMgr = null;
    aiTskMgr = null;
    // files
    aiShpUnt = null;
    aiSysUnt = null;
    alienInfo = null;
    empsInfo = null;
    gShipList = null;
    galInfo = null;
    gameInfo = null;
    gtfList = null;
    gwtForce = null;
    monsterList = null;
    ordInfo = null;
    provInfos = null;
    resultList = null;
    rToSInfo = null;
    sbInfo = null;
    sectorLst = null;
    stellInfo = null;
    strcInfo = null;
    systInfo = null;
    techInfo = null;
    tradeInfos = null;
    tradeInfoCnts = null;
    treaty = null;
  }

  /**
   * Call this function to clear single files.
   */
  public void clear(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();

    switch (fileName) {
      case CSavFilesLC.AIShpUnt:
        aiShpUnt = null;
        break;
      case CSavFilesLC.AISysUnt:
        aiSysUnt = null;
        break;
      case CSavFilesLC.AlienInfo:
        alienInfo = null;
        break;
      case CSavFilesLC.EmpsInfo:
        empsInfo = null;
        break;
      case CSavFilesLC.GShipList:
        gShipList = null;
        break;
      case CSavFilesLC.GalInfo:
        galInfo = null;
        break;
      case CSavFilesLC.GameInfo:
        gameInfo = null;
        break;
      case CSavFilesLC.GTForceList:
        gtfList = null;
        break;
      case CSavFilesLC.GWTForce:
        gwtForce = null;
        break;
      case CSavFilesLC.MonsterLst:
        monsterList = null;
        break;
      case CSavFilesLC.OrdInfo:
        ordInfo = null;
        break;
      case CSavFilesLC.ResultLst:
        resultList = null;
        break;
      case CSavFilesLC.RToSInfo:
        rToSInfo = null;
        break;
      case CSavFilesLC.StarBaseInfo:
        sbInfo = null;
        break;
      case CSavFilesLC.SectorLst:
        sectorLst = null;
        break;
      case CSavFilesLC.StellInfo:
        stellInfo = null;
        break;
      case CSavFilesLC.StrcInfo:
        strcInfo = null;
        break;
      case CSavFilesLC.SystInfo:
        systInfo = null;
        break;
      case CSavFilesLC.TechInfo:
        techInfo = null;
        break;
      case CSavFilesLC.Treaty:
        treaty = null;
        break;
      default: {
        // reset AI agent manager
        if (SavFileFilters.AIAgent.accept(null, fileName))
          aiAgtMgr = null;
        // reset AI task manager
        else if (SavFileFilters.AITask.accept(null, fileName))
          aiTskMgr = null;
        else if (fileName.startsWith(CSavFilesLC.ProvInfo)) {
          String strNbr = fileName.substring(CSavFilesLC.ProvInfo.length());
          int raceId = Integer.parseInt(strNbr);
          if (provInfos != null && provInfos.size() > raceId)
            provInfos.set(raceId, null);
        }
        else if (fileName.startsWith(CSavFilesLC.TrdeInfo)) {
          if (fileName.endsWith(CSavFilesLC.TrdeInfo_Suffix)) {
            String strNbr = fileName.substring(CSavFilesLC.TrdeInfo.length(),
              fileName.length() - CSavFilesLC.TrdeInfo_Suffix.length());
            int raceId = Integer.parseInt(strNbr);
            if (tradeInfoCnts != null && tradeInfoCnts.size() > raceId)
              tradeInfoCnts.set(raceId, null);
          } else {
            String strNbr = fileName.substring(CSavFilesLC.TrdeInfo.length());
            int raceId = Integer.parseInt(strNbr);
            if (tradeInfos != null && tradeInfos.size() > raceId)
              tradeInfos.set(raceId, null);
          }
        }
      }
    }
  }

  // #endregion cleanup

  // #region private helpers

  private ProvInfo cachedProvInfo(int raceId) {
    return provInfos != null && provInfos.size() > raceId
      ? provInfos.get(raceId) : null;
  }

  private void updateProvInfo(int raceId, ProvInfo provInfo) {
    // init missing provInfo list
    if (provInfos == null)
      provInfos = new ArrayList<>(CStbof.NUM_EMPIRES);

    // fill preceding provInfo slots
    for (int i = provInfos.size(); i < raceId; ++i) {
      provInfos.add(i, null);
    }

    // update provInfo
    if (raceId >= provInfos.size())
      provInfos.add(raceId, provInfo);
    else
      provInfos.set(raceId, provInfo);
  }

  private TradeInfo cachedTradeInfo(int raceId) {
    return tradeInfos != null && tradeInfos.size() > raceId
      ? tradeInfos.get(raceId) : null;
  }

  private void updateTradeInfo(int raceId, TradeInfo tradeInfo) {
    // init missing trdeInfo list
    if (tradeInfos == null)
      tradeInfos = new ArrayList<>(CStbof.NUM_EMPIRES);

    // fill preceding trdeInfo slots
    for (int i = tradeInfos.size(); i < raceId; ++i) {
      tradeInfos.add(i, null);
    }

    // update trdeInfo
    if (raceId >= tradeInfos.size())
      tradeInfos.add(raceId, tradeInfo);
    else
      tradeInfos.set(raceId, tradeInfo);
  }

  private TradeInfoCnt cachedTradeInfoCnt(int raceId) {
    return tradeInfoCnts != null && tradeInfoCnts.size() > raceId
      ? tradeInfoCnts.get(raceId) : null;
  }

  private void updateTradeInfoCnt(int raceId, TradeInfoCnt tradeInfoCnt) {
    // init missing trdeInfo*.cnt list
    if (tradeInfoCnts == null)
      tradeInfoCnts = new ArrayList<>(CStbof.NUM_EMPIRES);

    // fill preceding trdeInfo*.cnt slots
    for (int i = tradeInfoCnts.size(); i < raceId; ++i) {
      tradeInfoCnts.add(i, null);
    }

    // update trdeInfo*.cnt
    if (raceId >= tradeInfoCnts.size())
      tradeInfoCnts.add(raceId, tradeInfoCnt);
    else
      tradeInfoCnts.set(raceId, tradeInfoCnt);
  }

  // #endregion private helpers

}
