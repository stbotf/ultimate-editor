package ue.edit.sav.common;

/**
 * This interface holds common stbof file name constants.
 */
public interface CSavFiles {
  // list of known files
  public static final String GameInfo = "gameInfo"; //$NON-NLS-1$
  public static final String ResultLst = "result.lst"; //$NON-NLS-1$
  public static final String SectorLst = "sector.lst"; //$NON-NLS-1$
  public static final String StarBaseInfo = "starBaseInfo"; //$NON-NLS-1$
  public static final String StellInfo = "stellInfo"; //$NON-NLS-1$
  public static final String SystInfo = "systInfo"; //$NON-NLS-1$
  public static final String GalInfo = "galInfo"; //$NON-NLS-1$
  public static final String EmpsInfo = "empsInfo"; //$NON-NLS-1$
  public static final String ProvInfo = "provInfo"; //$NON-NLS-1$ *missing
  public static final String IntelInfo = "intelInfo"; //$NON-NLS-1$ *missing
  public static final String IncidentDat = "incident.dat"; //$NON-NLS-1$ *missing
  public static final String GShipHead = "GShipHead"; //$NON-NLS-1$
  public static final String GTForceHd = "GTForceHd"; //$NON-NLS-1$
  public static final String GWTForceHd = "GWTForceHd"; //$NON-NLS-1$
  public static final String GTFStasis = "GTFStasis"; //$NON-NLS-1$ *missing
  public static final String GTFStasisHd = "GTFStasisHd"; //$NON-NLS-1$
  public static final String TForceCntr = "TForceCntr"; //$NON-NLS-1$
  // For the latest status always refer GWTForce, given it includes player task forces re-groupings!
  public static final String GTForceList = "GTForceList"; //$NON-NLS-1$
  public static final String GWTForce = "GWTForce"; //$NON-NLS-1$
  public static final String MonsterLst = "monster.lst"; //$NON-NLS-1$
  public static final String OrdInfo = "ordInfo"; //$NON-NLS-1$
  public static final String ShipCntr = "ShipCntr"; //$NON-NLS-1$
  public static final String GShipList = "GShipList"; //$NON-NLS-1$
  public static final String RToSInfo = "RToSInfo"; //$NON-NLS-1$ *missing
  public static final String ShipNameDat = "shipname.dat"; //$NON-NLS-1$
  public static final String StrcInfo = "strcInfo"; //$NON-NLS-1$
  public static final String AlienInfo = "alienInfo"; //$NON-NLS-1$
  public static final String AlienTFInfo = "alienTFInfo"; //$NON-NLS-1$
  public static final String TrdeInfo = "trdeInfo"; //$NON-NLS-1$
  public static final String TrdeInfo_Suffix = ".cnt"; //$NON-NLS-1$
  public static final String TechInfo = "techinfo"; //$NON-NLS-1$
  public static final String Treaty = "treaty"; //$NON-NLS-1$ *missing
  public static final String SeedInfo = "SeedInfo"; //$NON-NLS-1$ *missing
  public static final String SphOfIn = "SphOfIn"; //$NON-NLS-1$
  public static final String AIAgent = "AIAgent"; //$NON-NLS-1$
  public static final String AIAgtIdCtr = "AIAgtIdCtr"; //$NON-NLS-1$
  public static final String AINumAgents = "AINumAgents"; //$NON-NLS-1$ *missing
  public static final String AINumTasks = "AINumTasks"; //$NON-NLS-1$ *missing
  public static final String AgtTk = "AgtTk"; //$NON-NLS-1$ *missing
  public static final String AgtTk_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String AgtSy = "AgtSy"; //$NON-NLS-1$ *missing
  public static final String AgtSy_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String AgtSh = "AgtSh"; //$NON-NLS-1$
  public static final String AgtSh_Suffix = ".cnt"; //$NON-NLS-1$
  public static final String AgtDp = "AgtDp"; //$NON-NLS-1$ *missing
  public static final String AgtDp_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String AISysUnt = "AISysUnt"; //$NON-NLS-1$ *missing
  public static final String AISysUntCnt = "AISysUnt.cnt"; //$NON-NLS-1$ *missing
  public static final String AIShpUnt = "AIShpUnt"; //$NON-NLS-1$
  public static final String AIShpUntCnt = "AIShpUnt.cnt"; //$NON-NLS-1$
  public static final String AITask = "AITask"; //$NON-NLS-1$
  public static final String AITaskIdCtr = "AITaskIdCtr"; //$NON-NLS-1$
  public static final String TskSh = "TskSh"; //$NON-NLS-1$
  public static final String TskSh_Suffix = ".cnt"; //$NON-NLS-1$
  public static final String TskSy = "TskSy"; //$NON-NLS-1$ *missing
  public static final String TskSy_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String Version = "version"; //$NON-NLS-1$ *missing

  public static Class<?> getFileClass(String fileName) {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();
    return CSavFilesLC.getFileClass(lcName);
  }

  /**
   * List of recognized internal core files that always must be present.
   *
   * Attention, the list is sorted to resolve resulting inconsistencies by the integrity checks.
   * First the details need to be checked, followed by the headers and more general statistics.
   */
  public static final String[] CoreFiles = new String[] {
    CSavFiles.Version, CSavFiles.SeedInfo, CSavFiles.TechInfo,
    CSavFiles.Treaty, CSavFiles.StellInfo, CSavFiles.SystInfo,
    CSavFiles.StrcInfo, CSavFiles.StarBaseInfo, CSavFiles.GTFStasisHd,
    CSavFiles.ShipNameDat, CSavFiles.GShipList, CSavFiles.GShipHead,
    CSavFiles.ShipCntr, CSavFiles.GTForceList, CSavFiles.GWTForce,
    CSavFiles.GTForceHd, CSavFiles.GWTForceHd, CSavFiles.TForceCntr,
    CSavFiles.AlienInfo, CSavFiles.ResultLst, CSavFiles.OrdInfo,
    CSavFiles.SectorLst, CSavFiles.GalInfo, CSavFiles.EmpsInfo,
    CSavFiles.GameInfo
  };

  public static final String[] TrdeInfos = new String[] {
    TrdeInfo + '0', TrdeInfo + '1', TrdeInfo + '2', //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    TrdeInfo + '3', TrdeInfo + '4', //$NON-NLS-1$ //$NON-NLS-2$
  };

  public static final String[] TrdeInfoCnts = new String[] {
    TrdeInfo + '0' + TrdeInfo_Suffix, TrdeInfo + '1' + TrdeInfo_Suffix, //$NON-NLS-1$ //$NON-NLS-2$
    TrdeInfo + '2' + TrdeInfo_Suffix, TrdeInfo + '3' + TrdeInfo_Suffix, //$NON-NLS-1$ //$NON-NLS-2$
    TrdeInfo + '4' + TrdeInfo_Suffix, //$NON-NLS-1$
  };

  public static final String[] ProvInfos = new String[] {
    ProvInfo + '0', ProvInfo + '1', ProvInfo + '2', //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    ProvInfo + '3', ProvInfo + '4', //$NON-NLS-1$ //$NON-NLS-2$
  };

}
