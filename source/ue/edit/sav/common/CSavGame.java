package ue.edit.sav.common;

/**
 * This interface holds common save game constants.
 */
public interface CSavGame {

  // #region Game Characteristics
  public static final int NUM_DIFFICULTIES = 5;
  // #endregion Game Characteristics

  // #region GameSettings
  public interface Difficulty {
    public static final int Simple = 1;
    public static final int Easy = 2;
    public static final int Normal = 3;
    public static final int Hard = 4;
    public static final int Impossible = 5;
  }

  public interface GalaxyShape {
    public static final int Irregular = 0;
    public static final int Elliptical = 1;
    public static final int Ring = 2;
    public static final int Spiral = 3;
  }

  public interface GalaxySize {
    public static final int Small = 0;
    public static final int Medium = 1;
    public static final int Large = 2;
  }

  public interface MinorAmount {
    public static final int None = 0;
    public static final int Few = 1;
    public static final int Some = 2;
    public static final int Many = 3;
  }

  public interface TacticalCombat {
    public static final int Manual = 0;
    public static final int Auto = 2;
  }

  public interface VictoryConditions {
    public static final int Domination = 0;
    public static final int TeamPlay = 1;
    public static final int Vendetta = 2;
  }
  // #endregion GameSettings

  // #region TaskForce
  public interface AbilityMask {

    public static final short CanColonize = 0x1;
    public static final short CanRaidSystem = 0x2;
    public static final short CanInvadeSystem = 0x4;
    public static final short Unknown5 = 0x8;
    public static final short CanIntercept = 0x10;
    public static final short Unknown6 = 0x20;
    public static final short CanTerraform = 0x40;
    public static final short CanCloak = 0x80;
    public static final short CanBuildOutpost = 0x100;
    public static final short CanBuildStarbase = 0x200;
    public static final short CanBeScrapped = 0x400;
    public static final short CanBombardSystem = 0x800;

    public static final short Exclusive = CanCloak | CanBeScrapped;
    public static final short Inclusive = ~Exclusive;

    // helper selectors
    public static final short CanAttackSystem = CanInvadeSystem | CanBombardSystem;
    public static final short Combatant = CanIntercept | CanRaidSystem | CanBombardSystem;
    public static final short NonCombatant =
        CanColonize | CanInvadeSystem | CanTerraform | CanBuildOutpost | CanBuildStarbase;
  }

  public interface ControlCategory {
    public static final int Empire = 1;
    public static final int Minor = 2;
    public static final int Monster = 3;
  }

  // race control flags
  public interface ControlFlags {
    static final int Player   = 1;
    static final int AIEmpire = 1<<1;
    static final int Minor    = 1<<2;
    static final int Monster  = 1<<3;
    static final int All      = Player|AIEmpire|Minor|Monster;
  }

  public interface CrewExperience {
    public static final int Green = 0;
    public static final int Regular = 1;
    public static final int Veteran = 2;
    public static final int Elite = 3;
    public static final int Legendary = 4;
  }

  public interface Mission {
    public static final int Move = 0;     // see OrderInfo
    public static final int Terraform = 1;
    public static final int Colonize = 2;
    public static final int Raid = 3;
    public static final int Intercept = 4;
    public static final int Evade = 5;
    public static final int Engage = 6;
    public static final int BuildOutpost = 7;
    public static final int BuildStarbase = 8;
    public static final int TrainCrew = 9;
    public static final int Expand = 10;  // non-assignable
    public static final int Invalid = 11; // non-assignable
    public static final int Scrap = 12;
    public static final int EnterWormhole = 13;
    public static final int AttackSystem = 14;

    // returns whether the move order can be assigned
    public static boolean canMove(int mission) {
      switch (mission) {
        case Move:
        case Evade:
        case Engage:
          return true;
        default:
          return false;
      }
    }

    public static boolean isSystemBound(int mission) {
      switch (mission) {
        case Terraform:
        case Colonize:
        case TrainCrew:
        case EnterWormhole:
        case AttackSystem:
          return true;
        default:
          return false;
      }
    }
  }
  // #endregion TaskForce
}
