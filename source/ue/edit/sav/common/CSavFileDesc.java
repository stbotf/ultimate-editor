package ue.edit.sav.common;

import ue.service.Language;

/**
 * This interface holds common save game file description constants.
 */
public interface CSavFileDesc {

  public static String getDescription(String fileName) {
    String lcName = fileName.toLowerCase();

    // LIST OF INTERNAL FILE DESCRIPTIONS
    // these must not be statically loaded for language change
    switch (lcName) {
      case CSavFilesLC.GameInfo:
        return Language.getString("CSavFileDesc.1"); //$NON-NLS-1$
      case CSavFilesLC.GShipList:
        return Language.getString("CSavFileDesc.7"); //$NON-NLS-1$
      case CSavFilesLC.GTForceList:
        return Language.getString("CSavFileDesc.2"); //$NON-NLS-1$
      case CSavFilesLC.GWTForce:
        return Language.getString("CSavFileDesc.3"); //$NON-NLS-1$
      case CSavFilesLC.OrdInfo:
        return Language.getString("CSavFileDesc.4"); //$NON-NLS-1$
      case CSavFilesLC.ResultLst:
        return Language.getString("CSavFileDesc.5"); //$NON-NLS-1$
      case CSavFilesLC.StarBaseInfo:
        return Language.getString("CSavFileDesc.10"); //$NON-NLS-1$
      case CSavFilesLC.SystInfo:
        return Language.getString("CSavFileDesc.11"); //$NON-NLS-1$
      case CSavFilesLC.Treaty:
        return Language.getString("CSavFileDesc.12"); //$NON-NLS-1$
    }

    if (lcName.startsWith(CSavFilesLC.TskSh) && !lcName.endsWith(CSavFilesLC.TskSh_Suffix)) {
      return Language.getString("CSavFileDesc.13"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.TskSy) && !lcName.endsWith(CSavFilesLC.TskSy_Suffix)) {
      return Language.getString("CSavFileDesc.14"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AgtDp) && !lcName.endsWith(CSavFilesLC.AgtDp_Suffix)) {
      return Language.getString("CSavFileDesc.17"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AgtSh) && !lcName.endsWith(CSavFilesLC.AgtSh_Suffix)) {
      return Language.getString("CSavFileDesc.18"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AgtSy) && !lcName.endsWith(CSavFilesLC.AgtSy_Suffix)) {
      return Language.getString("CSavFileDesc.19"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AgtTk) && !lcName.endsWith(CSavFilesLC.AgtTk_Suffix)) {
      return Language.getString("CSavFileDesc.20"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AIShpUnt) && !lcName.endsWith(CSavFilesLC.AIShpUntCnt)) {
      return Language.getString("CSavFileDesc.9"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AISysUnt) && !lcName.endsWith(CSavFilesLC.AISysUntCnt)) {
      return Language.getString("CSavFileDesc.16"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AITask)) {
      return Language.getString("CSavFileDesc.21"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.AlienTFInfo)) {
      return Language.getString("CSavFileDesc.15"); //$NON-NLS-1$
    } else if (lcName.startsWith(CSavFilesLC.TrdeInfo) && !lcName.endsWith(CSavFilesLC.TrdeInfo_Suffix)) {
      return Language.getString("CSavFileDesc.8"); //$NON-NLS-1$
    }

    return null;
  }
}
