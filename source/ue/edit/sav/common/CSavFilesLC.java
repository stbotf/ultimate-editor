package ue.edit.sav.common;

import ue.edit.common.GenericFile;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.SeedInfo;
import ue.edit.sav.files.Version;
import ue.edit.sav.files.ai.AIAgent;
import ue.edit.sav.files.ai.AIAgtIdCtr;
import ue.edit.sav.files.ai.AINumAgents;
import ue.edit.sav.files.ai.AINumTasks;
import ue.edit.sav.files.ai.AIShpUnt;
import ue.edit.sav.files.ai.AIShpUntCnt;
import ue.edit.sav.files.ai.AISysUnt;
import ue.edit.sav.files.ai.AISysUntCnt;
import ue.edit.sav.files.ai.AITask;
import ue.edit.sav.files.ai.AITaskIdCtr;
import ue.edit.sav.files.ai.AgtDp;
import ue.edit.sav.files.ai.AgtDpCnt;
import ue.edit.sav.files.ai.AgtSh;
import ue.edit.sav.files.ai.AgtShCnt;
import ue.edit.sav.files.ai.AgtSy;
import ue.edit.sav.files.ai.AgtSyCnt;
import ue.edit.sav.files.ai.AgtTk;
import ue.edit.sav.files.ai.AgtTkCnt;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.AlienTFInfo;
import ue.edit.sav.files.emp.EmpsInfo;
import ue.edit.sav.files.emp.IntelInfo;
import ue.edit.sav.files.emp.ProvInfo;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.emp.StrcInfo;
import ue.edit.sav.files.emp.TechInfo;
import ue.edit.sav.files.emp.Treaty;
import ue.edit.sav.files.map.GListHead;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.GalInfo;
import ue.edit.sav.files.map.MonsterList;
import ue.edit.sav.files.map.OrderInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.ShipCntr;
import ue.edit.sav.files.map.ShipNameDat;
import ue.edit.sav.files.map.SphereOfInfluence;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.TForceCntr;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.TradeInfo;
import ue.edit.sav.files.map.TradeInfoCnt;
import ue.edit.sav.files.map.TskSh;
import ue.edit.sav.files.map.TskShCnt;
import ue.edit.sav.files.map.TskSy;
import ue.edit.sav.files.map.TskSyCnt;

/**
 * This interface holds common stbof file name constants in lower case.
 * These are re-defined for compile time switch case evaluation.
 */
public interface CSavFilesLC {
  // list of known files
  public static final String GameInfo = "gameinfo"; //$NON-NLS-1$
  public static final String ResultLst = "result.lst"; //$NON-NLS-1$
  public static final String SectorLst = "sector.lst"; //$NON-NLS-1$
  public static final String StarBaseInfo = "starbaseinfo"; //$NON-NLS-1$
  public static final String StellInfo = "stellinfo"; //$NON-NLS-1$
  public static final String SystInfo = "systinfo"; //$NON-NLS-1$
  public static final String GalInfo = "galinfo"; //$NON-NLS-1$
  public static final String EmpsInfo = "empsinfo"; //$NON-NLS-1$
  public static final String ProvInfo = "provinfo"; //$NON-NLS-1$ *missing
  public static final String IntelInfo = "intelinfo"; //$NON-NLS-1$ *missing
  public static final String IncidentDat = "incident.dat"; //$NON-NLS-1$ *missing
  public static final String GShipHead = "gshiphead"; //$NON-NLS-1$
  public static final String GTForceHd = "gtforcehd"; //$NON-NLS-1$
  public static final String GWTForceHd = "gwtforcehd"; //$NON-NLS-1$
  public static final String GTFStasis = "gtfstasis"; //$NON-NLS-1$ *missing
  public static final String GTFStasisHd = "gtfstasishd"; //$NON-NLS-1$
  public static final String TForceCntr = "tforcecntr"; //$NON-NLS-1$
  // For the latest status always refer GWTForce, given it includes player task forces re-groupings!
  public static final String GTForceList = "gtforcelist"; //$NON-NLS-1$
  public static final String GWTForce = "gwtforce"; //$NON-NLS-1$
  public static final String MonsterLst = "monster.lst"; //$NON-NLS-1$
  public static final String OrdInfo = "ordinfo"; //$NON-NLS-1$
  public static final String ShipCntr = "shipcntr"; //$NON-NLS-1$
  public static final String GShipList = "gshiplist"; //$NON-NLS-1$
  public static final String RToSInfo = "rtosinfo"; //$NON-NLS-1$ *missing
  public static final String ShipNameDat = "shipname.dat"; //$NON-NLS-1$
  public static final String StrcInfo = "strcinfo"; //$NON-NLS-1$
  public static final String AlienInfo = "alieninfo"; //$NON-NLS-1$
  public static final String AlienTFInfo = "alientfinfo"; //$NON-NLS-1$
  public static final String TrdeInfo = "trdeinfo"; //$NON-NLS-1$
  public static final String TrdeInfo_Suffix = ".cnt"; //$NON-NLS-1$
  public static final String TechInfo = "techinfo"; //$NON-NLS-1$
  public static final String Treaty = "treaty"; //$NON-NLS-1$ *missing
  public static final String SeedInfo = "seedinfo"; //$NON-NLS-1$ *missing
  public static final String SphOfIn = "sphofin"; //$NON-NLS-1$
  public static final String AIAgent = "aiagent"; //$NON-NLS-1$
  public static final String AIAgtIdCtr = "aiagtidctr"; //$NON-NLS-1$
  public static final String AINumAgents = "ainumagents"; //$NON-NLS-1$ *missing
  public static final String AINumTasks = "ainumtasks"; //$NON-NLS-1$ *missing
  public static final String AgtDp = "agtdp"; //$NON-NLS-1$ *missing
  public static final String AgtDp_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String AgtSh = "agtsh"; //$NON-NLS-1$
  public static final String AgtSh_Suffix = ".cnt"; //$NON-NLS-1$
  public static final String AgtSy = "agtsy"; //$NON-NLS-1$ *missing
  public static final String AgtSy_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String AgtTk = "agttk"; //$NON-NLS-1$ *missing
  public static final String AgtTk_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String AISysUnt = "aisysunt"; //$NON-NLS-1$ *missing
  public static final String AISysUntCnt = "aisysunt.cnt"; //$NON-NLS-1$ *missing
  public static final String AIShpUnt = "aishpunt"; //$NON-NLS-1$
  public static final String AIShpUntCnt = "aishpunt.cnt"; //$NON-NLS-1$
  public static final String AITask = "aitask"; //$NON-NLS-1$
  public static final String AITaskIdCtr = "aitaskidctr"; //$NON-NLS-1$
  public static final String TskSh = "tsksh"; //$NON-NLS-1$
  public static final String TskSh_Suffix = ".cnt"; //$NON-NLS-1$
  public static final String TskSy = "tsksy"; //$NON-NLS-1$ *missing
  public static final String TskSy_Suffix = ".cnt"; //$NON-NLS-1$ *missing
  public static final String Version = "version"; //$NON-NLS-1$ *missing

  // requires lower case
  public static Class<?> getFileClass(String lc_fileName) {

    switch (lc_fileName) {
      case CSavFilesLC.SystInfo:      return SystInfo.class;
      case CSavFilesLC.StellInfo:     return StellInfo.class;
      case CSavFilesLC.StarBaseInfo:  return StarBaseInfo.class;
      case CSavFilesLC.SectorLst:     return Sector.class;
      case CSavFilesLC.GameInfo:      return GameInfo.class;
      case CSavFilesLC.GalInfo:       return GalInfo.class;
      case CSavFilesLC.EmpsInfo:      return EmpsInfo.class;
      case CSavFilesLC.ResultLst:     return ResultList.class;
      case CSavFilesLC.GShipHead:
      case CSavFilesLC.GTForceHd:
      case CSavFilesLC.GWTForceHd:
      case CSavFilesLC.GTFStasisHd:   return GListHead.class;
      case CSavFilesLC.TForceCntr:    return TForceCntr.class;
      case CSavFilesLC.GTForceList:   return TaskForceList.class;
      case CSavFilesLC.GWTForce:      return TaskForceList.class;
      case CSavFilesLC.MonsterLst:    return MonsterList.class;
      case CSavFilesLC.OrdInfo:       return OrderInfo.class;
      case CSavFilesLC.ShipCntr:      return ShipCntr.class;
      case CSavFilesLC.GShipList:     return GShipList.class;
      case CSavFilesLC.ShipNameDat:   return ShipNameDat.class;
      case CSavFilesLC.StrcInfo:      return StrcInfo.class;
      case CSavFilesLC.AlienInfo:     return AlienInfo.class;
      case CSavFilesLC.Treaty:        return Treaty.class;
      case CSavFilesLC.IntelInfo:     return IntelInfo.class;
      case CSavFilesLC.TechInfo:      return TechInfo.class;
      case CSavFilesLC.AIAgtIdCtr:    return AIAgtIdCtr.class;
      case CSavFilesLC.AINumAgents:   return AINumAgents.class;
      case CSavFilesLC.AINumTasks:    return AINumTasks.class;
      case CSavFilesLC.AIShpUnt:      return AIShpUnt.class;
      case CSavFilesLC.AIShpUntCnt:   return AIShpUntCnt.class;
      case CSavFilesLC.AISysUnt:      return AISysUnt.class;
      case CSavFilesLC.AISysUntCnt:   return AISysUntCnt.class;
      case CSavFilesLC.AITaskIdCtr:   return AITaskIdCtr.class;
      case CSavFilesLC.Version:       return Version.class;
      case CSavFilesLC.SeedInfo:      return SeedInfo.class;
      default: {
        if (lc_fileName.startsWith(CSavFilesLC.AIAgent)) {
          return AIAgent.class;
        } else if (lc_fileName.startsWith(CSavFilesLC.AgtDp)) {
          if (lc_fileName.endsWith(CSavFilesLC.AgtDp_Suffix)) {
            return AgtDpCnt.class;
          } else {
            return AgtDp.class;
          }
        } else if (lc_fileName.startsWith(CSavFilesLC.AgtSh)) {
          if (lc_fileName.endsWith(CSavFilesLC.AgtSh_Suffix)) {
            return AgtShCnt.class;
          } else {
            return AgtSh.class;
          }
        } else if (lc_fileName.startsWith(CSavFilesLC.AgtSy)) {
          if (lc_fileName.endsWith(CSavFilesLC.AgtSy_Suffix)) {
            return AgtSyCnt.class;
          } else {
            return AgtSy.class;
          }
        } else if (lc_fileName.startsWith(CSavFilesLC.AgtTk)) {
          if (lc_fileName.endsWith(CSavFilesLC.AgtTk_Suffix)) {
            return AgtTkCnt.class;
          } else {
            return AgtTk.class;
          }
        } else if (lc_fileName.startsWith(CSavFilesLC.AITask)) {
          return AITask.class;
        } else if (lc_fileName.startsWith(CSavFilesLC.AlienTFInfo)) {
          return AlienTFInfo.class;
        } else if (lc_fileName.startsWith(CSavFilesLC.ProvInfo)) {
          return ProvInfo.class;
        } else if (lc_fileName.startsWith(CSavFilesLC.TrdeInfo)) {
          if (lc_fileName.endsWith(CSavFilesLC.TrdeInfo_Suffix)) {
            return TradeInfoCnt.class;
          } else {
            return TradeInfo.class;
          }
        } else if (lc_fileName.startsWith(CSavFilesLC.TskSh)) {
          if (lc_fileName.endsWith(CSavFilesLC.TskSh_Suffix)) {
            return TskShCnt.class;
          } else {
            return TskSh.class;
          }
        } else if (lc_fileName.startsWith(CSavFilesLC.TskSy)) {
          if (lc_fileName.endsWith(CSavFilesLC.TskSy_Suffix)) {
            return TskSyCnt.class;
          } else {
            return TskSy.class;
          }
        } else if (lc_fileName.startsWith(CSavFilesLC.SphOfIn)) {
          return SphereOfInfluence.class;
        } else {
          return GenericFile.class;
        }
      }
    }
  }
}
