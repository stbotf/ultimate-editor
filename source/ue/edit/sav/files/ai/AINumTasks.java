package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.tools.AITaskMgr;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AINumTasks extends InternalFile {

  public static final String NUM_FILE_NAME = CSavFiles.AINumTasks;
  public static final String TASK_FILE_NAME = CSavFiles.AITask;

  private SavGame GAME;
  @Getter private int numTasks = 0;

  public AINumTasks(SavGame game) {
    this.GAME = game;
  }

  public AINumTasks(SavGame game, int num) {
    this.GAME = game;
    numTasks = num;
  }

  public void setNumTasks(int num) {
    if (numTasks != num) {
      numTasks = num;
      markChanged();
    }
  }

  public void incTasks() {
    numTasks++;
    markChanged();
  }

  public void decTasks() {
    numTasks--;
    markChanged();
  }

  @Override
  public void load(InputStream in) throws IOException {
    numTasks = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(numTasks, true));
  }

  @Override
  public void clear() {
    numTasks = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // check whether the task number is valid for the available tasks
    AITaskMgr taskMgr = GAME.files().aiTaskMgr();
    int num = taskMgr.getNumTasks();

    if (numTasks != num) {

      String msg = Language.getString("AINumTasks.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(numTasks)) //$NON-NLS-1$
        .replace("%2", Integer.toString(num)); //$NON-NLS-1$
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      setNumTasks(num);
    }
  }

  @Override
  public int getSize() {
    return 4;
  }

}
