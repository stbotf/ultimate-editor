package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeMap;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.emp.AlienInfo;
import ue.util.data.CollectionTools;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// AgtDp lists all the diplomatic relations of an AIAgent controlled race.
public class AgtDp extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.AgtDp_Suffix;
  public static final String FILE_NAME = CSavFiles.AgtDp;

  private AgtDpCnt header;
  private SavGame game;
  @Getter private int agentId;

  // sort diplomacy entries by race id to simplify hex lookup
  private TreeMap<Short, AgtDpEntry> entries = new TreeMap<Short, AgtDpEntry>(); // num entries * 532

  public AgtDp(AgtDpCnt header, SavGame game) {
    this.header = header;
    this.game = game;
  }

  public String descriptor(RaceRst raceRst) {
    AIAgent agt = (AIAgent) game.tryGetInternalFile(CSavFiles.AIAgent + Integer.toString(agentId), true);
    return agt != null ? agt.descriptor(raceRst) : "Agent " + Integer.toString(agentId);
  }

  private void updateHeader() {
    header.setDiplomaticRelationCount(entries.size());
  }

  public int getDiplomaticRelationCount() {
    return entries.size();
  }

  public short[] getRaceIds() {
    return CollectionTools.toShortArray(entries.keySet());
  }

  @Override
  public void setName(String name) {
    super.setName(name);
    updateId();
  }

  public boolean removeDiplomaticRelation(short raceId) {
    if (entries.remove(raceId) != null) {
      updateHeader();
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllDiplomaticRelations() {
    if (!entries.isEmpty()) {
      entries.clear();
      updateHeader();
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return entries.size() * AgtDpEntry.SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    entries.clear();

    int numDiplRel = header.getDiplomaticRelationCount();
    int size = in.available();
    if (size != numDiplRel * AgtDpEntry.SIZE)
      System.out.println(getName() + ": Diplomatic relation number doesn't match the entry count!");

    numDiplRel = Integer.min(size / AgtDpEntry.SIZE, numDiplRel);
    for (int i = 0; i < numDiplRel; ++i) {
      AgtDpEntry entry = new AgtDpEntry(in);
      entries.put(entry.getRaceId(), entry);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (AgtDpEntry entry : entries.values()) {
      entry.save(out);
    }
  }

  @Override
  public void clear() {
    entries.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    AlienInfo alienInfo = (AlienInfo) game.tryGetInternalFile(CSavFiles.AlienInfo, true);
    if (alienInfo == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, "Missing AlienInfo!"));
      return;
    }

    boolean any = entries.keySet().removeIf(raceId -> {
      if (!alienInfo.isRaceAlive(raceId)) {
        String msg = "Found diplomatic relation entry for race id %1, which is not alive by %2. (removed)";
        msg = msg.replace("%1", Integer.toString(raceId));
        msg = msg.replace("%2", CSavFiles.SystInfo);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
        return true;
      }
      return false;
    });

    if (any) {
      updateHeader();
      markChanged();
    }
  }

  private void updateId() {
    String agtNbr = NAME.substring(CSavFiles.AgtDp.length());
    agentId = Integer.parseInt(agtNbr);
  }

  private class AgtDpEntry {
    public static final int SIZE = 532;

    @Getter private short raceId;
    private byte[] unknown;

    public AgtDpEntry(InputStream in) throws IOException {
      load(in);
    }

    public void load(InputStream in) throws IOException {
      raceId = StreamTools.readShort(in, true);
      unknown = StreamTools.readBytes(in, 530);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(raceId, true));
      out.write(unknown);
    }
  }

}
