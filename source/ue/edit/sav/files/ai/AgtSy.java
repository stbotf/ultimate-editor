package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeSet;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.SystInfo;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;
import ue.util.data.CollectionTools;

// AgtSy lists all the system ids of an AIAgent controlled race.
public class AgtSy extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.AgtSy_Suffix;
  public static final String FILE_NAME = CSavFiles.AgtSy;

  // #region fields

  private AgtSyCnt header;
  private SavGame game;
  @Getter private int agentId;

  // sort system ids to simplify hex lookup
  private TreeSet<Short> m_systemIds = new TreeSet<Short>(); // numSystems * 2

  // #endregion fields

  // #region constructor

  public AgtSy(AgtSyCnt header, SavGame game) {
    this.header = header;
    this.game = game;
  }

  // #endregion constructor

  // #region property gets

  public String descriptor(RaceRst raceRst) {
    AIAgent agt = (AIAgent) game.tryGetInternalFile(CSavFiles.AIAgent + Integer.toString(agentId), true);
    return agt != null ? agt.descriptor(raceRst) : "Agent " + Integer.toString(agentId);
  }

  public int getSystemCount() {
    return m_systemIds.size();
  }

  public short[] getSystemIds() {
    return CollectionTools.toShortArray(m_systemIds);
  }

  // #endregion property gets

  // #region property sets

  @Override
  public void setName(String name) {
    super.setName(name);
    updateId();
  }

  public void setSystemIds(short[] systemIds) {
    // check if changed
    if (!differs(systemIds))
      return;

    // update the system ids
    m_systemIds.clear();
    for (short id : systemIds) {
      m_systemIds.add(id);
    }

    markChanged();
  }

  // #endregion property sets

  // #region public helpers

  public void addSystem(short systemId) {
    m_systemIds.add(systemId);
    updateHeader();
    markChanged();
  }

  public boolean removeSystem(short systemId) {
    if (m_systemIds.remove(systemId)) {
      updateHeader();
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllSystems() {
    if (!m_systemIds.isEmpty()) {
      m_systemIds.clear();
      updateHeader();
      markChanged();
    }
  }

  // #endregion public helpers

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    m_systemIds.clear();

    int numSystems = header.getSystemCount();
    int size = in.available();
    if (size != numSystems << 1)
      System.out.println(getName() + ": System number doesn't match the entry number!");

    numSystems = Integer.min(size >> 1, numSystems);
    for (int i = 0; i < numSystems; ++i) {
      m_systemIds.add(StreamTools.readShort(in, true));
    }

    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    for (short sysId : m_systemIds) {
      out.write(DataTools.toByte(sysId, true));
    }
  }

  // #endregion save

  // #region maintenance

  @Override
  public int getSize() {
    return m_systemIds.size() * 2;
  }

  public void onRemovedSystem(short systemId) {
    if (m_systemIds.isEmpty() || m_systemIds.last() < systemId)
      return;

    // update system ids
    TreeSet<Short> copy = new TreeSet<>();
    for (short key : m_systemIds) {
      if (key != systemId)
        copy.add(key < systemId ? key : --key);
    }

    boolean removed = m_systemIds.size() != copy.size();
    m_systemIds = copy;
    markChanged();
    if (removed)
      updateHeader();
  }

  @Override
  public void clear() {
    m_systemIds.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region check

  @Override
  public void check(Vector<String> response) {
    SystInfo systInfo = (SystInfo) game.tryGetInternalFile(CSavFiles.SystInfo, true);
    if (systInfo == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, "Missing SystInfo!"));
      return;
    }

    boolean any = m_systemIds.removeIf(sysId -> {
      if (!systInfo.hasSystem(sysId)) {
        String msg = "Found system id %1, that is missing from %2. (removed)";
        msg = msg.replace("%1", Integer.toString(sysId));
        msg = msg.replace("%2", CSavFiles.SystInfo);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        return true;
      }
      return false;
    });

    if (any) {
      updateHeader();
      markChanged();
    }
  }

  // #endregion check

  // #region private helpers

  private boolean differs(short[] systemIds) {
    if (systemIds.length != m_systemIds.size())
      return true;

    for (short systemId : systemIds) {
      if (!m_systemIds.contains(systemId))
        return true;
    }

    return false;
  }

  private void updateId() {
    String agtNbr = NAME.substring(CSavFiles.AgtSy.length());
    agentId = Integer.parseInt(agtNbr);
  }

  private void updateHeader() {
    header.setSystemCount(m_systemIds.size());
  }

  // #endregion private helpers

}
