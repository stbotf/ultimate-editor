package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.tools.AIAgentMgr;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AINumAgents extends InternalFile {

  public static final String NUM_FILE_NAME = CSavFiles.AINumAgents;
  public static final String AGENT_FILE_NAME = CSavFiles.AIAgent;

  private SavGame GAME;
  @Getter private int numAgents = 0;

  public AINumAgents(SavGame game) {
    this.GAME = game;
  }

  public AINumAgents(SavGame game, int num) {
    this.GAME = game;
    numAgents = num;
  }

  public void setNumAgents(int num) {
    if (numAgents != num) {
      numAgents = num;
      markChanged();
    }
  }

  public void incNumAgents() {
    numAgents++;
    markChanged();
  }

  public void decNumAgents() {
    numAgents--;
    markChanged();
  }

  @Override
  public void load(InputStream in) throws IOException {
    numAgents = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(numAgents, true));
  }

  @Override
  public void clear() {
    numAgents = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // check whether the agent number is valid for the available agents
    AIAgentMgr agentMgr = GAME.files().aiAgentMgr();
    int num = agentMgr.getNumAgents();

    if (numAgents != num) {

      String msg = Language.getString("AINumAgents.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(numAgents)) //$NON-NLS-1$
        .replace("%2", Integer.toString(num)); //$NON-NLS-1$
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      setNumAgents(num);
    }
  }

  @Override
  public int getSize() {
    return 4;
  }

}
