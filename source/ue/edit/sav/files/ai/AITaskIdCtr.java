package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AITaskIdCtr extends InternalFile {

  public static final String CNTR_FILE_NAME = CSavFiles.AITaskIdCtr;
  public static final String TASK_FILE_NAME = CSavFiles.AITask;

  private SavGame game;
  private int taskCtr = 0;

  public AITaskIdCtr(SavGame game) {
    this.game = game;
  }

  public AITaskIdCtr(SavGame game, int count) {
    this.game = game;
    taskCtr = count;
  }

  public int getTaskCounter() {
    return taskCtr;
  }

  public void setTaskCounter(int cntr) {
    if (taskCtr != cntr) {
      taskCtr = cntr;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    taskCtr = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(taskCtr, true));
  }

  @Override
  public void clear() {
    taskCtr = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // check whether the id counter is valid for the available tasks
    val taskMgr = game.files().aiTaskMgr();
    int maxTaskCtr = taskMgr.getMaxTaskId();

    if (taskCtr <= maxTaskCtr) {
      String msg = Language.getString("AITaskIdCtr.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(taskCtr)) //$NON-NLS-1$
        .replace("%2", Integer.toString(maxTaskCtr)); //$NON-NLS-1$
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));

      setTaskCounter(maxTaskCtr + 1);
    }
  }

  @Override
  public int getSize() {
    return 4;
  }
}
