package ue.edit.sav.files.ai;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AgtDpCnt extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.AgtDp_Suffix;
  public static final String AGTSY_FILE_NAME = CSavFiles.AgtDp;

  private SavGame game;
  @Getter private int diplomaticRelationCount = 0;

  public AgtDpCnt(SavGame game) {
    this.game = game;
  }

  public String getAgtDpName() {
    return NAME.substring(0, NAME.length() - CNT_SUFFIX.length());
  }

  public void setDiplomaticRelationCount(int cnt) {
    if (diplomaticRelationCount != cnt) {
      diplomaticRelationCount = cnt;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    diplomaticRelationCount = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(diplomaticRelationCount, true));
  }

  @Override
  public void clear() {
    diplomaticRelationCount = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    String agtName = getAgtDpName();

    try {
      AgtDp agtDp = (AgtDp) game.getInternalFile(agtName, false);
      int cnt = agtDp.getDiplomaticRelationCount();

      if (diplomaticRelationCount != cnt) {
        String msg = "Diplomatic relation number %1 doesn't match the entry count %2! (fixed)";
        msg = msg.replace("%1", Integer.toString(diplomaticRelationCount));
        msg = msg.replace("%2", Integer.toString(cnt));
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      setDiplomaticRelationCount(cnt);
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      String msg = "Missing diplomacy agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    catch (Exception e) {
      e.printStackTrace();
      String msg = "Failed to check diplomacy agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
  }

  @Override
  public int getSize() {
    return 4;
  }
}
