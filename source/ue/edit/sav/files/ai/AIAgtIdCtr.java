package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.tools.AIAgentMgr;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AIAgtIdCtr extends InternalFile {

  public static final String CNTR_FILE_NAME = CSavFiles.AIAgtIdCtr;
  public static final String AGENT_FILE_NAME = CSavFiles.AIAgent;

  private SavGame GAME;
  private int agentCtr = 0;

  public AIAgtIdCtr(SavGame game) {
    this.GAME = game;
  }

  public AIAgtIdCtr(SavGame game, int count) {
    this.GAME = game;
    agentCtr = count;
  }

  public int getAgentCounter() {
    return agentCtr;
  }

  public void setAgentCounter(int cntr) {
    if (agentCtr != cntr) {
      agentCtr = cntr;
      markChanged();
    }
  }

  public void incAgentCounter() {
    agentCtr++;
    markChanged();
  }

  public void decAgentCounter() {
    agentCtr--;
    markChanged();
  }

  @Override
  public void load(InputStream in) throws IOException {
    agentCtr = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(agentCtr, true));
  }

  @Override
  public void clear() {
    agentCtr = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // check whether the id counter is valid for the available tasks
    AIAgentMgr agentMgr = GAME.files().aiAgentMgr();
    int maxAgentCtr = agentMgr.getMaxAgentId();

    if (agentCtr <= maxAgentCtr) {

      String msg = Language.getString("AIAgtIdCtr.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(agentCtr)) //$NON-NLS-1$
        .replace("%2", Integer.toString(maxAgentCtr)); //$NON-NLS-1$
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));

      setAgentCounter(maxAgentCtr + 1);
    }
  }

  @Override
  public int getSize() {
    return 4;
  }
}
