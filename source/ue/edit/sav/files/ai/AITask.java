package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class AITask extends InternalFile {

  // @see
  // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=34081#p34081
  // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54008#p54008
  public interface TaskType {
    public static final int Explore = 0;        // -> file size = 0x100 -> no system id
    public static final int Raid = 1;           // -> file size = 0x104 -> no system id
    public static final int Colonize = 2;       // -> file size = 0x104 -> has system id
    public static final int Terraform = 3;      // -> file size = 0x108 -> has system id
    public static final int DevelopSystem = 4;  // -> file size = 0xF8  -> has system id
    public static final int BuildShip = 5;      // -> file size = 0xF8  -> no system id
    public static final int BuildBase = 6;      // -> file size = 0x104 -> no system id
    public static final int Invade = 7;         // -> file size = 0x110 -> has system id
    public static final int AcceptProposal = 8;
    public static final int OfferProposal = 9;  // -> file size = 0x168 -> no system id
    public static final int AttackSystem = 10;  // -> file size = 0x104 -> no system id
    public static final int BuildWonder = 11;   // -> file size = 0xF4  -> has system id
    public static final int Patrol = 12;        // -> file size = 0x110 -> no system id
    public static final int TrainCrew = 13;     // -> file size = 0xF4  -> has system id
    public static final int Repair = 14;        // -> file size = 0xF0  -> no system id
    public static final int Harass = 15;        // -> file size = 0x10C -> no system id
  }

  public interface TaskCategory {
    public static final int Ship = 1;
    public static final int System = 2;
    public static final int Diplomacy = 3;
  }

  public static final int MAX_CHILD_TASKS = 8;

  // The file suffix used to load & store all the AITasks.
  // It always starts with 0 and is limited by AINumTasks.
  private int fileIndex;

  // For offset details refer SCT listing at:
  // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54833#p54833

  @Accessors(fluent = true)
  private short id;                               // 0x00 +  2 = 2
  // SCT: sequel TaskID (eg Colonize if Tform 'improve false')
  // -> cf. CHILD TASKS feature @ +68; IDs @ +58 ff (Tform shoud be Colonize child task)
  @Accessors(fluent = true)
  private short parentId;                         // 0x02 +  2 = 4

  // task type
  private int type;                               // 0x04 +  4 = 8
  // the TaskCategory of this task
  private int category;                           // 0x08 +  4 = 12
  private int agentId;                            // 0x0C +  4 = 16
  // SCT: adr special Task data - size variable (eg. sys dev C bytes @ +8 plan)
  private int ptrSpecialTaskData;                 // 0x10 +  4 = 20

  // 14 code pointer sub-routine addresses or 0
  // max 9 used (e.g. from 58BF88 Explore, 58C03C Colonize, 58F9B8 Raid)
  // SCT: 5 unused, pattern most 4...3..2
  private int[] subRoutines = new int[14];        // 0x14 + 56 = 76

  // SCT: Task modifier via agenda
  private int taskModifier;                       // 0x4C +  4 = 80
  // TskSh shipIDs (ShipUnits)
  private int ptr_TskSh;                          // 0x50 +  4 = 84
  // TskSy starIDs (SysUnits)
  private int ptr_TskSy;                          // 0x54 +  4 = 88

  // SCT: base for child task IDs -> loaded via [AITask+58h+2*(value at +68h)]
  @Getter(AccessLevel.NONE)
  private TreeSet<Short> childTasks = new TreeSet<Short>(); // 0x58 + 16 = 104
  // private int numChildTasks;                   // 0x68 +  4 = 108

  // SCT: Task Status (shiptasks) 0 = hold/insufficient ships, 1 = gather Source sector, 2 = GO Target sector
  // SCT: for shiptasks 'grab more ships?' marker (not 2 = check more flight turns away)
  // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56589#p56589
  private int taskStatus;                         // 0x6C +  4 = 112
  private int sourceSectorRow;                    // 0x70 +  4 = 116
  private int sourceSectorColumn;                 // 0x74 +  4 = 120
  private int targetSectorRow;                    // 0x78 +  4 = 124
  private int targetSectorColumn;                 // 0x7C +  4 = 128

  // ship task data
  private AIColonizeData colonizeData;            // 0x80 + 132 = 260 (0x104)
  private AITerraformData terraformData;          // 0x80 + 136 = 264 (0x108)
  private AITrainingData trainingData;            // 0x80 + 116 = 244 (0xF4)
  private AIInvadeData invadeData;                // 0x80 + 144 = 272 (0x110)
  // system task data
  private AIDevelopSystemData developSystemData;  // 0x80 + 120 = 248 (0xF8)
  private AIBuildShipData buildShipData;          // 0x80 + 120 = 248 (0xF8)
  private AIBuildWonderData buildWonderData;      // 0x80 + 116 = 244 (0xF4)
  // diplomatic task data
  private AIOfferData offerData;                  // 0x80 + 232 = 360 (0x168)
  // other task data
  private byte[] unknown1;                        // 0x80 + ?? depends on the TASK_TYPE

  // from SCT @ https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54833#p54833
  // +80 can build something marker
  // +82 build order active marker
  // Allocations (word) 41873A/426449
  // +86 scout -number shiptypes:
  // +8A destroyer
  // +8E cruiser
  // +92 artillery
  // +96 command
  // +9A colony
  // +9E TT
  // -build tasks:
  // +A0 MINCR cost
  // +A2 CRALLOC
  // +A4 MINNETCR maintenance
  // +A6 NCRALLOC (ships NETCRALLOC)
  // +A8 1 dil cost?
  // +AA DILALLOC
  // +B0 desired ship functions bitmask
  // - double floats each:
  // +B4 total build cost of OWN TskSh ShipUnits (patrol with station cost) -> all double floats
  // +BC average build cost of OWN TskSh ShipUnits (patrol with station cost)
  // +C4 total build cost (+5% * experience level) of ENEMY TskSh ships in specified area + station cost
  // +CC average build cost (+5% * experience level) of ENEMY TskSh ships in specified area + station cost
  // +D4 Modifier: total build cost of OWN TskSh ShipUnits (patrol with station cost) * Modifier for average ENEMY expBonus
  // +DC clear lower modifier tasks marker (colonize, bldbase, invade, attack = 1) see sub_426A10
  // +E0-E3 turn number when set

  public AITask(SavGame game) {
  }

  public int getSystemId() {
    // ship task data
    if (colonizeData != null)
      return colonizeData.getSystemId();
    if (terraformData != null)
      return terraformData.getSystemId();
    if (trainingData != null)
      return trainingData.getSystemId();
    if (invadeData != null)
      return invadeData.getSystemId();
    // system task data
    if (developSystemData != null)
      return developSystemData.getSystemId();
    if (buildWonderData != null)
      return buildWonderData.getSystemId();
    return -1;
  }

  public boolean isRaceTask(short raceId) {
    return offerData != null && (offerData.getSenderRace() == raceId
      || offerData.getRecipientRace() == raceId);
  }

  @Override
  public int getSize() {
    int unk2Len = 128;

    // ship task data
    if (colonizeData != null)
      unk2Len += AIColonizeData.SIZE;
    if (terraformData != null)
      unk2Len += AITerraformData.SIZE;
    if (trainingData != null)
      unk2Len += AITrainingData.SIZE;
    if (invadeData != null)
      unk2Len += AIInvadeData.SIZE;

    // system task data
    if (developSystemData != null)
      unk2Len += AIDevelopSystemData.SIZE;
    if (buildShipData != null)
      unk2Len += AIBuildShipData.SIZE;
    if (buildWonderData != null)
      unk2Len += AIBuildWonderData.SIZE;

    // diplomatic task data
    if (offerData != null)
      unk2Len += AIOfferData.SIZE;

    // other task data
    if (unknown1 != null)
      unk2Len += unknown1.length;
    return unk2Len;
  }

  @Override
  public void setName(String name) {
    super.setName(name);
    updateNbr();
  }

  public void setId(short taskId) {
    if (this.id != taskId) {
      this.id = taskId;
      markChanged();
    }
  }

  public void setParentId(short taskId) {
    if (parentId != taskId) {
      parentId = taskId;
      markChanged();
    }
  }

  public void setAgentId(int agentId) {
    if (this.agentId != agentId) {
      this.agentId = agentId;
      markChanged();
    }
  }

  public void setSourceSectorColumn(int x) {
    if (sourceSectorColumn != x) {
      sourceSectorColumn = x;
      markChanged();
    }
  }

  public void setSourceSectorRow(int y) {
    if (sourceSectorRow != y) {
      sourceSectorRow = y;
      markChanged();
    }
  }

  public void setTargetSectorColumn(int x) {
    if (targetSectorColumn != x) {
      targetSectorColumn = x;
      markChanged();
    }
  }

  public void setTargetSectorRow(int y) {
    if (targetSectorRow != y) {
      targetSectorRow = y;
      markChanged();
    }
  }

  public Set<Short> listChildTasks() {
    return Collections.unmodifiableSet(childTasks);
  }

  public boolean removeChildTask(short childTaskId) {
    if (childTasks.remove(childTaskId)) {
      markChanged();
      return true;
    }
    return false;
  }

  public boolean onRemovedSystem(short systemId) {
    if (developSystemData != null)
      return developSystemData.onRemovedSystem(systemId);
    else if (buildWonderData != null)
      return buildWonderData.onRemovedSystem(systemId);
    else if (colonizeData != null)
      return colonizeData.onRemovedSystem(systemId);
    else if (terraformData != null)
      return terraformData.onRemovedSystem(systemId);
    else if (trainingData != null)
      return trainingData.onRemovedSystem(systemId);
    else if (invadeData != null)
      return invadeData.onRemovedSystem(systemId);
    return false;
  }

  @Override
  public void load(InputStream in) throws IOException {
    id = StreamTools.readShort(in, true);
    parentId = StreamTools.readShort(in, true);
    type = StreamTools.readInt(in, true);
    category = StreamTools.readInt(in, true);
    agentId = StreamTools.readInt(in, true);
    ptrSpecialTaskData = StreamTools.readInt(in, true);
    StreamTools.read(in, subRoutines, true);
    taskModifier = StreamTools.readInt(in, true);
    ptr_TskSh = StreamTools.readInt(in, true);
    ptr_TskSy = StreamTools.readInt(in, true);

    short[] childs = StreamTools.readShortArray(in, 8, true);
    int numChildTasks = StreamTools.readInt(in, true);
    for (int i = 0; i < numChildTasks; ++i) {
      childTasks.add(childs[i]);
    }

    taskStatus = StreamTools.readInt(in, true);
    sourceSectorRow = StreamTools.readInt(in, true);
    sourceSectorColumn = StreamTools.readInt(in, true);
    targetSectorRow = StreamTools.readInt(in, true);
    targetSectorColumn = StreamTools.readInt(in, true);

    // Ship build tasks have same size but don't list the system id!!
    switch (type) {

      // ship task data
      case TaskType.Colonize:
        colonizeData = new AIColonizeData(in);
        break;
      case TaskType.Terraform:
        terraformData = new AITerraformData(in);
        break;
      case TaskType.TrainCrew:
        trainingData = new AITrainingData(in);
        break;
      case TaskType.Invade:
        invadeData = new AIInvadeData(in);
        break;

      // system task data
      case TaskType.DevelopSystem:
        developSystemData = new AIDevelopSystemData(in);
        break;
      case TaskType.BuildShip:
        buildShipData = new AIBuildShipData(in);
        break;
      case TaskType.BuildWonder:
        buildWonderData = new AIBuildWonderData(in);
        break;

      // diplomatic task data
      case TaskType.OfferProposal:
        offerData = new AIOfferData(in);
        break;

      // other task data
      default:
        unknown1 = StreamTools.readAllBytes(in);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (childTasks.size() > MAX_CHILD_TASKS)
      throw new IOException("Max child task number exceeded.");

    out.write(DataTools.toByte(id, true));
    out.write(DataTools.toByte(parentId, true));
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(category, true));
    out.write(DataTools.toByte(agentId, true));
    out.write(DataTools.toByte(ptrSpecialTaskData, true));
    out.write(DataTools.toByte(subRoutines, true));
    out.write(DataTools.toByte(taskModifier, true));
    out.write(DataTools.toByte(ptr_TskSh, true));
    out.write(DataTools.toByte(ptr_TskSy, true));

    for (short childId : childTasks)
      out.write(DataTools.toByte(childId, true));
    for (int i = childTasks.size(); i < MAX_CHILD_TASKS; ++i)
      out.write(DataTools.toByte((short) 0, true));
    out.write(DataTools.toByte(childTasks.size(), true));

    out.write(DataTools.toByte(taskStatus, true));
    out.write(DataTools.toByte(sourceSectorRow, true));
    out.write(DataTools.toByte(sourceSectorColumn, true));
    out.write(DataTools.toByte(targetSectorRow, true));
    out.write(DataTools.toByte(targetSectorColumn, true));

    // ship task data
    if (colonizeData != null)
      colonizeData.save(out);
    if (terraformData != null)
      terraformData.save(out);
    if (trainingData != null)
      trainingData.save(out);
    if (invadeData != null)
      invadeData.save(out);

    // system task data
    if (developSystemData != null)
      developSystemData.save(out);
    if (buildShipData != null)
      buildShipData.save(out);
    if (buildWonderData != null)
      buildWonderData.save(out);

    // diplomatic task data
    if (offerData != null)
      offerData.save(out);

    // other task data
    if (unknown1 != null)
      out.write(unknown1);
  }

  @Override
  public void clear() {
    id = 0;
    parentId = 0;
    type = 0;
    category = 0;
    agentId = 0;
    ptrSpecialTaskData = 0;
    Arrays.fill(subRoutines, 0);
    taskModifier = 0;
    ptr_TskSh = 0;
    ptr_TskSy = 0;
    childTasks.clear();
    taskStatus = 0;
    sourceSectorRow = 0;
    sourceSectorColumn = 0;
    targetSectorRow = 0;
    targetSectorColumn = 0;

    // ship task data
    colonizeData = null;
    terraformData = null;
    trainingData = null;
    invadeData = null;
    // system task data
    developSystemData = null;
    buildShipData = null;
    buildWonderData = null;
    // diplomatic task data
    offerData = null;
    // other task data
    unknown1 = null;

    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  private void updateNbr() {
    String taskNbr = NAME.substring(CSavFiles.AITask.length());
    fileIndex = Integer.parseInt(taskNbr);
  }

  @Getter
  public class AISystemTaskData {
    public static final int SIZE = 112;   // 0x70

    private short unk1;                   // 0x80 +  2 = 130
    private short unk2;                   // 0x82 +  2 = 132
    private byte[] empty1 = new byte[32]; // 0x84 + 32 = 164
    private short shipType;               // 0xA4 +  2 = 166
    private short nextShipType;           // 0xA6 +  2 = 168
    private short shipType_enabled;       // 0xA8 +  2 = 170
    private short nextShipType_enabled;   // 0xAA +  2 = 172
    private byte[] empty2 = new byte[44]; // 0xAC + 44 = 216
    private int addr1;                    // 0xD8 +  4 = 220
    private byte[] empty3 = new byte[4];  // 0xDC +  4 = 224
    // turn number when set
    private int turnIssued;               // 0xE0 +  4 = 228
    private byte[] empty4 = new byte[8];  // 0xE4 +  8 = 236
    private int addr2;                    // 0xEC +  4 = 240

    public AISystemTaskData(InputStream in) throws IOException {
      load(in);
    }

    public void load(InputStream in) throws IOException {
      unk1 = StreamTools.readShort(in, true);                   //   0 +  2
      unk2 = StreamTools.readShort(in, true);                   //   2 +  2
      StreamTools.read(in, empty1);                             //   4 + 32
      shipType = StreamTools.readShort(in, true);               //  36 +  2
      nextShipType = StreamTools.readShort(in, true);           //  38 +  2
      shipType_enabled = StreamTools.readShort(in, true);       //  40 +  2
      nextShipType_enabled = StreamTools.readShort(in, true);   //  42 +  2
      StreamTools.read(in, empty2);                             //  44 + 44
      addr1 = StreamTools.readInt(in, true);                    //  88 +  4
      StreamTools.read(in, empty3);                             //  92 +  4
      turnIssued = StreamTools.readInt(in, true);               //  96 +  4
      StreamTools.read(in, empty4);                             // 100 +  8
      addr2 = StreamTools.readInt(in, true);                    // 108 +  4 = 112
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(unk1, true));                  //   0 +  2
      out.write(DataTools.toByte(unk2, true));                  //   2 +  2
      out.write(empty1);                                        //   4 + 32
      out.write(DataTools.toByte(shipType, true));              //  36 +  2
      out.write(DataTools.toByte(nextShipType, true));          //  38 +  2
      out.write(DataTools.toByte(shipType_enabled, true));      //  40 +  2
      out.write(DataTools.toByte(nextShipType_enabled, true));  //  42 +  2
      out.write(empty2);                                        //  44 + 44
      out.write(DataTools.toByte(addr1, true));                 //  88 +  4
      out.write(empty3);                                        //  92 +  4
      out.write(DataTools.toByte(turnIssued, true));            //  96 +  4
      out.write(empty4);                                        // 100 +  8
      out.write(DataTools.toByte(addr2, true));                 // 108 +  4 = 112
    }
  }

  @Getter
  public class AIOfferData {
    public static final int SIZE = 232;   // 0xE8

    private byte[] empty1 = new byte[32]; // 0x80  + 32 = 160
    private short money;                  // 0xA0  +  2 = 162 guessed
    private short repeatedMoney;          // 0xA2  +  2 = 164 guessed
    private byte[] empty2 = new byte[52]; // 0xA4  + 52 = 216
    private int addr1;                    // 0xD8  +  4 = 220
    private byte[] empty3 = new byte[4];  // 0xDC  +  4 = 224
    // turn number when set
    private int turnIssued;               // 0xE0  +  4 = 228
    private byte[] empty4 = new byte[8];  // 0xE4  +  8 = 236
    private int addr2;                    // 0xEC  +  4 = 240
    private byte[] unk1 = new byte[48];   // 0xF0  + 48 = 288
    private int responseType;             // 0x120 +  4 = 292 always 8
    private short senderRace;             // 0x124 +  2 = 294
    private short recipientRace;          // 0x126 +  2 = 296
    private byte[] unk2 = new byte[28];   // 0x128 + 28 = 324
    private short money2;                 // 0x144 +  2 = 326 guessed
    private byte[] empty5 = new byte[26]; // 0x146 + 26 = 352
    private short unk3;                   // 0x160 +  2 = 378
    private short unk4;                   // 0x162 +  2 = 380
    private boolean unk5;                 // 0x164 +  4 = 384

    public AIOfferData(InputStream in) throws IOException {
      load(in);
    }

    public void load(InputStream in) throws IOException {
      StreamTools.read(in, empty1);                             //   0 + 32
      money = StreamTools.readShort(in, true);                  //  32 +  2
      repeatedMoney = StreamTools.readShort(in, true);          //  34 +  2
      StreamTools.read(in, empty2);                             //  36 + 52
      addr1 = StreamTools.readInt(in, true);                    //  88 +  4
      StreamTools.read(in, empty3);                             //  92 +  4
      turnIssued = StreamTools.readInt(in, true);               //  96 +  4
      StreamTools.read(in, empty4);                             // 100 +  8
      addr2 = StreamTools.readInt(in, true);                    // 108 +  4 = 112
      StreamTools.read(in, unk1);                               // 112 + 48
      responseType = StreamTools.readInt(in, true);             // 160 +  4
      senderRace = StreamTools.readShort(in, true);             // 164 +  2
      recipientRace = StreamTools.readShort(in, true);          // 166 +  2
      StreamTools.read(in, unk2);                               // 168 + 28
      money2 = StreamTools.readShort(in, true);                 // 196 +  2
      StreamTools.read(in, empty5);                             // 198 + 26
      unk3 = StreamTools.readShort(in, true);                   // 224 +  2
      unk4 = StreamTools.readShort(in, true);                   // 226 +  2
      unk5 = StreamTools.readInt(in, true) != 0;                // 228 +  4 = 232
    }

    public void save(OutputStream out) throws IOException {
      out.write(empty1);                                        //   0 + 32
      out.write(DataTools.toByte(money, true));                 //  32 +  2
      out.write(DataTools.toByte(repeatedMoney, true));         //  34 +  2
      out.write(empty2);                                        //  36 + 52
      out.write(DataTools.toByte(addr1, true));                 //  88 +  4
      out.write(empty3);                                        //  92 +  4
      out.write(DataTools.toByte(turnIssued, true));            //  96 +  4
      out.write(empty4);                                        // 100 +  8
      out.write(DataTools.toByte(addr2, true));                 // 108 +  4 = 112
      out.write(unk1);                                          // 112 + 48
      out.write(DataTools.toByte(responseType, true));          // 160 +  4
      out.write(DataTools.toByte(senderRace, true));            // 164 +  2
      out.write(DataTools.toByte(recipientRace, true));         // 166 +  2
      out.write(unk2);                                          // 168 + 28
      out.write(DataTools.toByte(money2, true));                // 196 +  2
      out.write(empty5);                                        // 198 + 26
      out.write(DataTools.toByte(unk3, true));                  // 224 +  2
      out.write(DataTools.toByte(unk4, true));                  // 226 +  2
      out.write(DataTools.toByte(unk5 ? 1 : 0, true));          // 228 +  4 = 232
    }
  }

  @Getter
  public class AIDevelopSystemData extends AISystemTaskData {
    public static final int SIZE = AISystemTaskData.SIZE + 8;   // 0x78

    private int systemId;   // 0xF0 +  4 = 244
    private boolean unk3;   // 0xF4 +  4 = 248

    public AIDevelopSystemData(InputStream in) throws IOException {
      super(in);
    }

    public boolean onRemovedSystem(int systemId) {
      if (this.systemId > systemId) {
        this.systemId--;
        markChanged();
      } else if (this.systemId == systemId) {
        this.systemId = -1;
        markChanged();
        return true;
      }
      return false;
    }

    public void load(InputStream in) throws IOException {
      super.load(in);                                   //   0 + 112
      systemId = StreamTools.readInt(in, true);         // 112 +   4
      unk3 = StreamTools.readInt(in, true) != 0;        // 116 +   4 = 120
    }

    public void save(OutputStream out) throws IOException {
      super.save(out);                                  //   0 + 112
      out.write(DataTools.toByte(systemId, true));      // 112 +   4
      out.write(DataTools.toByte(unk3 ? 1 : 0, true));  // 116 +   4 = 120
    }
  }

  @Getter
  public class AIBuildShipData extends AISystemTaskData {
    public static final int SIZE = AISystemTaskData.SIZE + 8;   // 0x78

    private int unkNum;     // 0xF0 +  4 = 244 // not the system ID!
    private boolean unk3;   // 0xF4 +  4 = 248

    public AIBuildShipData(InputStream in) throws IOException {
      super(in);
    }

    public void load(InputStream in) throws IOException {
      super.load(in);                                   //   0 + 112
      unkNum = StreamTools.readInt(in, true);           // 112 +   4
      unk3 = StreamTools.readInt(in, true) != 0;        // 116 +   4 = 120
    }

    public void save(OutputStream out) throws IOException {
      super.save(out);                                  //   0 + 112
      out.write(DataTools.toByte(unkNum, true));        // 112 +   4
      out.write(DataTools.toByte(unk3 ? 1 : 0, true));  // 116 +   4 = 120
    }
  }

  @Getter
  public class AIBuildWonderData extends AISystemTaskData {
    public static final int SIZE = AISystemTaskData.SIZE + 4;   // 0x74

    private short systemId;   // 0xF0  +  2 = 242
    private short unk2;       // 0xF2  +  2 = 244

    public AIBuildWonderData(InputStream in) throws IOException {
      super(in);
    }

    public boolean onRemovedSystem(int systemId) {
      if (this.systemId > systemId) {
        this.systemId--;
        markChanged();
      } else if (this.systemId == systemId) {
        this.systemId = -1;
        markChanged();
        return true;
      }
      return false;
    }

    public void load(InputStream in) throws IOException {
      super.load(in);                               //   0 + 112
      systemId = StreamTools.readShort(in, true);   // 112 +   2
      unk2 = StreamTools.readShort(in, true);       // 114 +   2 = 116
    }

    public void save(OutputStream out) throws IOException {
      super.save(out);                              //   0 + 112
      out.write(DataTools.toByte(systemId, true));  // 112 +   2
      out.write(DataTools.toByte(unk2, true));      // 114 +   2 = 116
    }
  }

  @Getter
  public class AIShipTaskData {
    public static final int SIZE = 112;   // 0x70

    private byte[] empty1 = new byte[48]; // 0x80 + 48 = 176
    // desired ship functions bitmask
    private int shipFunc;                 // 0xB0 +  4 = 180
    private int[] unk1 = new int[10];     // 0xB4 + 40 = 220
    // clear lower modifier tasks marker (colonize, bldbase, invade, attack = 1) see sub_426A10
    private int clearLowerTasks;          // 0xDC  +  4 = 224
    // turn number when set
    private int turnIssued;               // 0xE0  +  4 = 228
    private byte[] empty2 = new byte[8];  // 0xE4  +  8 = 236
    private int addr1;                    // 0xEC  +  4 = 240


    public AIShipTaskData(InputStream in) throws IOException {
      load(in);
    }

    public void load(InputStream in) throws IOException {
      StreamTools.read(in, empty1);                             //   0 + 48
      shipFunc = StreamTools.readInt(in, true);                 //  48 +  4
      StreamTools.read(in, unk1, true);                         //  52 + 40
      clearLowerTasks = StreamTools.readInt(in, true);          //  92 +  4
      turnIssued = StreamTools.readInt(in, true);               //  96 +  4
      StreamTools.read(in, empty2);                             // 100 +  8
      addr1 = StreamTools.readInt(in, true);                    // 108 +  4
    }

    public void save(OutputStream out) throws IOException {
      out.write(empty1);                                        //   0 + 48
      out.write(DataTools.toByte(shipFunc, true));              //  48 +  4
      out.write(DataTools.toByte(unk1, true));                  //  52 + 40
      out.write(DataTools.toByte(clearLowerTasks, true));       //  92 +  4
      out.write(DataTools.toByte(turnIssued, true));            //  96 +  4
      out.write(empty2);                                        // 100 +  8
      out.write(DataTools.toByte(addr1, true));                 // 108 +  4
    }
  }

  @Getter
  public class AIColonizeData extends AIShipTaskData {
    public static final int SIZE = AIShipTaskData.SIZE + 20;  // 0x84

    private short systemId;           // 0xF0  +  2 = 242
    private short unk2;               // 0xF2  +  2 = 244
    private int[] unk3 = new int[4];  // 0xF4  + 16 = 260

    public AIColonizeData(InputStream in) throws IOException {
      super(in);
    }

    public boolean onRemovedSystem(short systemId) {
      if (this.systemId > systemId) {
        this.systemId--;
        markChanged();
      } else if (this.systemId == systemId) {
        this.systemId = -1;
        markChanged();
        return true;
      }
      return false;
    }

    public void load(InputStream in) throws IOException {
      super.load(in);                               //   0 + 112
      systemId = StreamTools.readShort(in, true);   // 112 +   2
      unk2 = StreamTools.readShort(in, true);       // 114 +   2
      StreamTools.read(in, unk3, true);             // 116 +  16
    }

    public void save(OutputStream out) throws IOException {
      super.save(out);                              //   0 + 112
      out.write(DataTools.toByte(systemId, true));  // 112 +   2
      out.write(DataTools.toByte(unk2, true));      // 114 +   2
      out.write(DataTools.toByte(unk3, true));      // 116 +  16 = 132
    }
  }

  @Getter
  public class AITerraformData extends AIShipTaskData {
    public static final int SIZE = AIShipTaskData.SIZE + 24;  // 0x88

    private int unk2;                 // 0xF0  +  4 = 244
    private short systemId;           // 0xF4  +  2 = 246
    private short unk3;               // 0xF6  +  2 = 248
    private int[] unk4 = new int[4];  // 0xF8  + 16 = 264

    public AITerraformData(InputStream in) throws IOException {
      super(in);
    }

    public boolean onRemovedSystem(short systemId) {
      if (this.systemId > systemId) {
        this.systemId--;
        markChanged();
      } else if (this.systemId == systemId) {
        this.systemId = -1;
        markChanged();
        return true;
      }
      return false;
    }

    public void load(InputStream in) throws IOException {
      super.load(in);                                           //   0 + 112
      unk2 = StreamTools.readInt(in, true);                     // 112 +   4
      systemId = StreamTools.readShort(in, true);               // 116 +   2
      unk3 = StreamTools.readShort(in, true);                   // 118 +   2
      StreamTools.read(in, unk4, true);                         // 120 +  16
    }

    public void save(OutputStream out) throws IOException {
      super.save(out);                                          //   0 + 112
      out.write(DataTools.toByte(unk2, true));                  // 112 +   4
      out.write(DataTools.toByte(systemId, true));              // 116 +   2
      out.write(DataTools.toByte(unk3, true));                  // 118 +   2
      out.write(DataTools.toByte(unk4, true));                  // 120 +  16 = 136
    }
  }

  @Getter
  public class AITrainingData extends AIShipTaskData {
    public static final int SIZE = AIShipTaskData.SIZE + 4;  // 0x74

    private short systemId;   // 0xF0  +  2 = 242
    private short unk2;       // 0xF2  +  2 = 244

    public AITrainingData(InputStream in) throws IOException {
      super(in);
    }

    public boolean onRemovedSystem(short systemId) {
      if (this.systemId > systemId) {
        this.systemId--;
        markChanged();
      } else if (this.systemId == systemId) {
        this.systemId = -1;
        markChanged();
        return true;
      }
      return false;
    }

    public void load(InputStream in) throws IOException {
      super.load(in);                                           //   0 + 112
      systemId = StreamTools.readShort(in, true);               // 112 +   2
      unk2 = StreamTools.readShort(in, true);                   // 114 +   2
    }

    public void save(OutputStream out) throws IOException {
      super.save(out);                                          //   0 + 112
      out.write(DataTools.toByte(systemId, true));              // 112 +   2
      out.write(DataTools.toByte(unk2, true));                  // 114 +   2 = 116
    }
  }

  @Getter
  public class AIInvadeData extends AIShipTaskData {
    public static final int SIZE = AIShipTaskData.SIZE + 32;  // 0x90

    private short systemId;               // 0xF0  +  2 = 242
    private short align1;                 // 0xF2  +  2 = 244
    private int[] unk2 = new int[6];      // 0xF4  + 24 = 268
    private boolean unk3;                 // 0x10C +  4 = 272

    public AIInvadeData(InputStream in) throws IOException {
      super(in);
    }

    public boolean onRemovedSystem(int systemId) {
      if (this.systemId > systemId) {
        this.systemId--;
        markChanged();
      } else if (this.systemId == systemId) {
        this.systemId = -1;
        markChanged();
        return true;
      }
      return false;
    }

    public void load(InputStream in) throws IOException {
      super.load(in);                                           //   0 + 112
      systemId = StreamTools.readShort(in, true);               // 112 +   2
      align1 = StreamTools.readShort(in, true);                 // 114 +   2
      StreamTools.read(in, unk2, true);                         // 116 +  24
      unk3 = StreamTools.readInt(in, true) != 0;                // 140 +   4 = 144
    }

    public void save(OutputStream out) throws IOException {
      super.save(out);                                          //   0 + 112
      out.write(DataTools.toByte(systemId, true));              // 112 +   2
      out.write(DataTools.toByte(align1, true));                // 114 +   2
      out.write(DataTools.toByte(unk2, true));                  // 116 +  24
      out.write(DataTools.toByte(unk3 ? 1 : 0, true));          // 140 +   4 = 144
    }
  }

}
