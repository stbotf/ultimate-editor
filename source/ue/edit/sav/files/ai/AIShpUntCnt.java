package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// number of AI controled ships
public class AIShpUntCnt extends InternalFile {

  @Getter
  private int shipCount = 0;

  private SavGame game;

  public AIShpUntCnt(SavGame game) {
    this.game = game;
  }

  @Override
  public int getSize() {
    return 4;
  }

  public void setShipCount(int cnt) {
    if (shipCount != cnt) {
      shipCount = cnt;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    shipCount = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(shipCount, true));
  }

  @Override
  public void clear() {
    shipCount = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    val shipUnt = game.files().findAiShpUnt();

    if (shipUnt.isPresent()) {
      int cnt = shipUnt.get().getShipCount();

      if (shipCount != cnt) {
        String msg = "Ship number %1 doesn't match the entry number %2! (fixed)";
        msg = msg.replace("%1", Integer.toString(shipCount));
        msg = msg.replace("%2", Integer.toString(cnt));
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    }
  }
}
