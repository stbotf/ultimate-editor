package ue.edit.sav.files.ai;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AgtShCnt extends InternalFile {

  public static final String CNTR_SUFFIX = CSavFiles.AgtSh_Suffix;
  public static final String AGTSH_FILE_NAME = CSavFiles.AgtSh;

  private SavGame game;
  private int shCnt = 0;

  public AgtShCnt(SavGame game) {
    this.game = game;
  }

  public String getAgtShName() {
    return this.NAME.substring(0, this.NAME.length() - CNTR_SUFFIX.length());
  }

  public int getShCount() {
    return shCnt;
  }

  public void setShCount(int cntr) {
    if (shCnt != cntr) {
      shCnt = cntr;
      this.markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    shCnt = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(shCnt, true));
  }

  @Override
  public void clear() {
    shCnt = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    String agtName = getAgtShName();

    try {
      AgtSh agtSh = (AgtSh) game.getInternalFile(agtName, false);
      int cnt = agtSh.getShipCount();

      if (shCnt != cnt) {
        String msg = "Ship number %1 doesn't match the entry number %2! (fixed)";
        msg = msg.replace("%1", Integer.toString(shCnt));
        msg = msg.replace("%2", Integer.toString(cnt));
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      setShCount(cnt);
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      String msg = "Missing ship agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    catch (Exception e) {
      e.printStackTrace();
      String msg = "Failed to check ship agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
  }

  @Override
  public int getSize() {
    return 4;
  }
}
