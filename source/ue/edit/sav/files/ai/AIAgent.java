package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// refer https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54833#p54833
public class AIAgent extends InternalFile {

  public interface Type {

    static final int Major = 1;
    static final int Minor = 2;
    static final int Monster = 3;
  }

  // The file suffix used to load & store all the AIAgents.
  // It always starts with 0 and is limited by AINumAgents.
  @Getter private int fileIndex;

  private int agentId;        // 0x00 + 4 = 4
  private int type;           // 0x04 + 4 = 8  1: major race, 2: minor race, 3: aliens
  private int raceId;         // 0x08 + 4 = 12 note, for type 3 this exceeds the usual raceId range
  private int subRoutine1;    // 0x0C + 4 = 16 based on the agent type, SCT: eg. sub_4073D0 prepare AI_major_mapViews
  private int subRoutine2;    // 0x10 + 4 = 20 based on the agent type, SCT: eg. sub_407250 AI_control_major

  // SCT: adr special Agent data - size variable (empires 28h -> agenda/personality, production focus, map data...)
  private int dataAddress1;   // 0x14 + 4 = 24
  // SCT: AgtTk TaskIDs
  private int dataAddress2;   // 0x18 + 4 = 28
  // SCT: some TaskIDs sorted by modifier
  private int dataAddress3;   // 0x1C + 4 = 32
  // SCT: some starIDs sorted by 'can build something' Floats?
  private int dataAddress4;   // 0x20 + 4 = 36
  // SCT: some shipIDs sorted by function values
  private int dataAddress5;   // 0x24 + 4 = 40
  // SCT: starIDs sorted by population
  private int dataAddress6;   // 0x28 + 4 = 44
  // SCT: AgtSh shipIDs (ShipUnits)
  private int dataAddress7;   // 0x2C + 4 = 48
  // SCT: AgtSy starIDs (SysUnits)
  private int dataAddress8;   // 0x30 + 4 = 52
  // SCT: AgtDp diplomatics
  private int dataAddress9;   // 0x34 + 4 = 56

  private byte[] unknown1;    // 0x38 + ?? -> depends on the type

  // SCT: resources:
  // +38 +0 pop support + effective income
  // +3C +4 credits
  // +40 +8 fleet support cost + treaty costs pending? + all system Credits effective preview BUG?
  // +44 +C 0 ???
  // +48 +10 ship support cost
  // +4C +14 station support cost
  // +50 +18 treaty costs pending?
  // +54 +1C all system Credits effective preview
  // +58 +20 dilithium sources
  // +5C +24 sum CRALLOC (add to 5* real support costs)
  // +60 +28 sum NETCRALLOC (sub from max support left)
  // +64 +2C max credits left
  // +68 +30 max support left
  // +6C +34 sum DILALLOC (sub from dilithium sources)
  // +70 +38 dilithium sources free
  // +74 +3C rest to spend for buy
  // +78 +40 allotment bonus for buy Ships
  // +7C +44 ? allotment bonus for buy 2? unused?
  // +80 +48 allotment bonus for buy Gifts/Proposals
  // +84 +4C allotment bonus for buy Buildings
  // +88 ?
  // +8C ?
  // +90 ?
  // +94 ?
  // +98 0 ?

  public AIAgent(SavGame game) {
  }

  public String descriptor(RaceRst raceRst) {
    String race = raceRst.getOwnerName(this.raceId);
    // include file name, since it's number differs from the agent id
    return race + " " + this.getName() + " <" + Integer.toString(agentId) + ">";
  }

  public int getId() {
    return agentId;
  }

  public int getType() {
    return type;
  }

  public int getRace() {
    return raceId;
  }

  @Override
  public void setName(String name) {
    super.setName(name);
    updateNbr();
  }

  public void setId(int id) {
    if (agentId != id) {
      agentId = id;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    agentId = StreamTools.readInt(in, true);
    type = StreamTools.readInt(in, true);
    raceId = StreamTools.readInt(in, true);
    subRoutine1 = StreamTools.readInt(in, true);
    subRoutine2 = StreamTools.readInt(in, true);
    dataAddress1 = StreamTools.readInt(in, true);
    dataAddress2 = StreamTools.readInt(in, true);
    dataAddress3 = StreamTools.readInt(in, true);
    dataAddress4 = StreamTools.readInt(in, true);
    dataAddress5 = StreamTools.readInt(in, true);
    dataAddress6 = StreamTools.readInt(in, true);
    dataAddress7 = StreamTools.readInt(in, true);
    dataAddress8 = StreamTools.readInt(in, true);
    dataAddress9 = StreamTools.readInt(in, true);

    unknown1 = StreamTools.readAllBytes(in);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(agentId, true));
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(raceId, true));
    out.write(DataTools.toByte(subRoutine1, true));
    out.write(DataTools.toByte(subRoutine2, true));
    out.write(DataTools.toByte(dataAddress1, true));
    out.write(DataTools.toByte(dataAddress2, true));
    out.write(DataTools.toByte(dataAddress3, true));
    out.write(DataTools.toByte(dataAddress4, true));
    out.write(DataTools.toByte(dataAddress5, true));
    out.write(DataTools.toByte(dataAddress6, true));
    out.write(DataTools.toByte(dataAddress7, true));
    out.write(DataTools.toByte(dataAddress8, true));
    out.write(DataTools.toByte(dataAddress9, true));
    out.write(unknown1);
  }

  @Override
  public void clear() {
    agentId = 0;
    type = 0;
    raceId = 0;
    subRoutine1 = 0;
    subRoutine2 = 0;
    dataAddress1 = 0;
    dataAddress2 = 0;
    dataAddress3 = 0;
    dataAddress4 = 0;
    dataAddress5 = 0;
    dataAddress6 = 0;
    dataAddress7 = 0;
    dataAddress8 = 0;
    dataAddress9 = 0;
    unknown1 = null;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return 56 + (unknown1 != null ? unknown1.length : 0);
  }

  private void updateNbr() {
    String agtNbr = NAME.substring(CSavFiles.AIAgent.length());
    fileIndex = Integer.parseInt(agtNbr);
  }

}
