package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// AgtTk lists all the task ids of an AIAgent controlled race.
public class AgtTk extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.AgtTk_Suffix;
  public static final String FILE_NAME = CSavFiles.AgtTk;

  // #region fields

  private AgtTkCnt header;
  private SavGame game;
  @Getter private int agentId;

  // sort task ids to simplify hex lookup
  private TreeSet<Short> m_taskIds = new TreeSet<Short>(); // numtasks * 2

  // #endregion fields

  // #region constructor

  public AgtTk(AgtTkCnt header, SavGame game) {
    this.header = header;
    this.game = game;
  }

  // #endregion constructor

  // #region property gets

  public String descriptor(RaceRst raceRst) {
    AIAgent agt = (AIAgent) game.tryGetInternalFile(CSavFiles.AIAgent + Integer.toString(agentId), true);
    return agt != null ? agt.descriptor(raceRst) : "Agent " + Integer.toString(agentId);
  }

  public int getTaskCount() {
    return m_taskIds.size();
  }

  public Set<Short> getTaskIds() {
    return Collections.unmodifiableSet(m_taskIds);
  }

  // #endregion property gets

  // #region property sets

  @Override
  public void setName(String name) {
    super.setName(name);
    updateId();
  }

  public void setTaskIds(short[] taskIds) {
    // check if changed
    if (!differs(taskIds))
      return;

    // update the task ids
    m_taskIds.clear();
    for (short id : taskIds) {
      m_taskIds.add(id);
    }

    markChanged();
  }

  // #endregion property sets

  // #region public helpers

  public void addTask(short taskId) {
    m_taskIds.add(taskId);
    updateHeader();
    markChanged();
  }

  public boolean removeTask(short taskId) {
    if (m_taskIds.remove(taskId)) {
      updateHeader();
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllTasks() {
    if (!m_taskIds.isEmpty()) {
      m_taskIds.clear();
      updateHeader();
      markChanged();
    }
  }

  // #endregion public helpers

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    m_taskIds.clear();

    int numtasks = header.getTaskCount();
    int size = in.available();
    if (size != numtasks << 1)
      System.out.println(getName() + ": task number doesn't match the entry number!");

    numtasks = Integer.min(size >> 1, numtasks);
    for (int i = 0; i < numtasks; ++i) {
      m_taskIds.add(StreamTools.readShort(in, true));
    }

    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    for (short sysId : m_taskIds) {
      out.write(DataTools.toByte(sysId, true));
    }
  }

  // #endregion save

  // #region maintenance

  @Override
  public int getSize() {
    return m_taskIds.size() * 2;
  }

  public void onRemovedTask(short taskId) {
    if (m_taskIds.isEmpty() || m_taskIds.last() < taskId)
      return;

    // update task ids
    TreeSet<Short> copy = new TreeSet<>();
    for (short key : m_taskIds) {
      if (key != taskId)
        copy.add(key < taskId ? key : --key);
    }

    boolean removed = m_taskIds.size() != copy.size();
    m_taskIds = copy;
    markChanged();
    if (removed)
      updateHeader();
  }

  @Override
  public void clear() {
    m_taskIds.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region check

  @Override
  public void check(Vector<String> response) {
    boolean any = m_taskIds.removeIf(tskId -> {
      if (!game.hasInternalFile(CSavFiles.TskSh + tskId) && !game.hasInternalFile(CSavFiles.TskSy + tskId)) {
        String msg = "Found task id %1, that is missing from %2. (removed)";
        msg = msg.replace("%1", Integer.toString(tskId));
        msg = msg.replace("%2", CSavFiles.TskSh + " and " + CSavFiles.TskSy + " files");

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        return true;
      }
      return false;
    });

    if (any) {
      updateHeader();
      markChanged();
    }
  }

  // #endregion check

  // #region private helpers

  private boolean differs(short[] taskIds) {
    if (taskIds.length != m_taskIds.size())
      return true;

    for (short taskId : taskIds) {
      if (!m_taskIds.contains(taskId))
        return true;
    }

    return false;
  }

  private void updateId() {
    String agtNbr = NAME.substring(CSavFiles.AgtTk.length());
    agentId = Integer.parseInt(agtNbr);
  }

  private void updateHeader() {
    header.setTaskCount(m_taskIds.size());
  }

  // #endregion private helpers

}
