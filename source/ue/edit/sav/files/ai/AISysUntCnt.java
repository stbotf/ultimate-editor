package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// number of AI controled systems
public class AISysUntCnt extends InternalFile {

  @Getter
  private int systemCount = 0;

  private SavGame game;

  public AISysUntCnt(SavGame game) {
    this.game = game;
  }

  @Override
  public int getSize() {
    return 4;
  }

  public void setSystemCount(int cnt) {
    if (systemCount != cnt) {
      systemCount = cnt;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    systemCount = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(systemCount, true));
  }

  @Override
  public void clear() {
    systemCount = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    val sysUnt = game.files().findAiSysUnt();

    if (sysUnt.isPresent()) {
      int cnt = sysUnt.get().getSystemCount();

      if (systemCount != cnt) {
        String msg = "System number %1 doesn't match the entry number %2! (fixed)";
        msg = msg.replace("%1", Integer.toString(systemCount));
        msg = msg.replace("%2", Integer.toString(cnt));
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    }
  }
}
