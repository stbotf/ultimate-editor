package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeMap;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.TskSh;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * The AIShpUnt is for planning ship tasks. It's likely an abbreviation for ai ship undertaking.
 */
public class AIShpUnt extends InternalFile {

  public static final String FileName = CSavFiles.AIShpUnt;

  private static final int SHPUNT_ENTRY_SIZE = 24;
  private AIShpUntCnt header;
  // private int numShips; // 4 auto-computed

  private SavGame game;

  // keep ship list sorted for BotF
  private TreeMap<Short, ShpUnt> shipUnts = new TreeMap<Short, ShpUnt>(); // numShips * 4 + 4

  public AIShpUnt(AIShpUntCnt header, SavGame game) {
    this.header = header;
    this.game = game;
  }

  private void updateHeader() {
    header.setShipCount(shipUnts.size());
  }

  public int getShipCount() {
    return shipUnts.size();
  }

  public short[] getShipIds() {
    int size = shipUnts.size();
    short[] ids = new short[size];

    int i = 0;
    for (ShpUnt unt : shipUnts.values()) {
      ids[i++] = unt.shipId;
    }
    return ids;
  }

  public boolean hasShipId(short shipId) {
    return shipUnts.containsKey(shipId);
  }

  public ShpUnt getShipUnt(short shipId) {
    return shipUnts.get(shipId);
  }

  public short getShipTask(short shipId) {
    ShpUnt shp = shipUnts.get(shipId);
    return shp.taskId;
  }

  // this possibly requires to add some AITask
  public void addShipUnt(short shipId, short role) {
    ShpUnt shpUnt = new ShpUnt(shipId, role);
    shipUnts.put(shipId, shpUnt);
    updateHeader();
    markChanged();
  }

  public void clearShipUnt(short shipId) {
    ShpUnt shp = shipUnts.get(shipId);
    if (shp != null)
      shp.clearTask();
  }

  public void clearTask(short taskId) {
    for (ShpUnt shp : shipUnts.values()) {
      if (shp.getTaskId() == taskId)
        shp.clearTask();
    }
  }

  public void clearAllTasks() {
    for (ShpUnt shp : shipUnts.values()) {
      shp.clearTask();
    }
  }

  public boolean removeShipUnt(short shipId) {
    ShpUnt rem = shipUnts.remove(shipId);
    if (rem != null) {
      updateHeader();
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAll() {
    if (!shipUnts.isEmpty()) {
      shipUnts.clear();
      updateHeader();
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    shipUnts.clear();

    int numShips = this.header.getShipCount();
    int size = in.available();
    if (size != numShips * SHPUNT_ENTRY_SIZE)
      System.out.println("AIShpUnt: Ship number doesn't match the entry number!");

    numShips = Integer.min(size / SHPUNT_ENTRY_SIZE, numShips);
    for (int i = 0; i < numShips; ++i) {
      ShpUnt shpUnt = new ShpUnt(in);
      shipUnts.put(shpUnt.shipId, shpUnt);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (ShpUnt shpUnt : shipUnts.values()) {
      shpUnt.save(out);
    }
  }

  @Override
  public void clear() {
    shipUnts.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    GShipList shipList = (GShipList) game.tryGetInternalFile(CSavFiles.GShipList, true);
    if (shipList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, "Missing GShipList!"));
      return;
    }

    boolean any = shipUnts.keySet().removeIf(shpId -> {
      if (!shipList.hasShip(shpId)) {
        String msg = "Found ship id %1, that is missing from %2. (common, but removed)";
        msg = msg.replace("%1", Integer.toString(shpId));
        msg = msg.replace("%2", CSavFiles.GShipList);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));

        // remove current AI ship task
        short aiTaskId = getShipTask(shpId);
        if (aiTaskId != -1) {
          try {
            TskSh tskSh = (TskSh) game.getInternalFile(CSavFiles.TskSh + aiTaskId, true);
            tskSh.removeShip(shpId);
          }
          catch (Exception e) {
            msg = "Failed to update ship %1 task %2. (error)";
            msg = msg.replace("%1", Integer.toString(shpId));
            msg = msg.replace("%2", Short.toString(aiTaskId));
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        }

        return true;
      }
      return false;
    });

    if (any) {
      updateHeader();
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return shipUnts.size() * SHPUNT_ENTRY_SIZE;
  }

  // refer https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54833#p54833
  @Getter
  public class ShpUnt {

    private short shipId;         // 2
    // see ue.edit.res.stbof.sst.ShipRole
    private short shipRole;       // 4
    private short nextTaskId;     // 6
    // current task id, which in addition equals the TskSh file number
    private short taskId;         // 8
    // usually all set to 0xFF, sometimes zero
    private int unknown1;         // 12
    // all set to 0xFF, SCT: monster code implies unused coordinate 1
    private int unknown2;         // 16
    // all set to 0xFF, SCT: monster code implies unused coordinate 2
    private int unknown3;         // 20
    // SCT: ignore marker (eg. after scrap order) -> cf. AITask+10h task data
    // -> similar data structure for ship tasks -> @ +14 same ignore marker after range loss
    private int ignore;           // 24

    public ShpUnt(short shipId, short shipRole) {
      this.shipId = shipId;
      this.shipRole = shipRole;
      nextTaskId = -1;
      taskId = -1;
      unknown1 = -1;
      unknown2 = -1;
      unknown3 = -1;
      ignore = 0;
    }

    ShpUnt(InputStream in) throws IOException {
      shipId = StreamTools.readShort(in, true);
      shipRole = StreamTools.readShort(in, true);
      nextTaskId = StreamTools.readShort(in, true);
      taskId = StreamTools.readShort(in, true);
      unknown1 = StreamTools.readInt(in, true);
      unknown2 = StreamTools.readInt(in, true);
      unknown3 = StreamTools.readInt(in, true);
      ignore = StreamTools.readInt(in, true);
    }

    public void clearTask() {
      if (taskId != -1 || nextTaskId != -1) {
        taskId = -1;
        nextTaskId = -1;
        markChanged();
      }
    }

    void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(shipId, true));
      out.write(DataTools.toByte(shipRole, true));
      out.write(DataTools.toByte(nextTaskId, true));
      out.write(DataTools.toByte(taskId, true));
      out.write(DataTools.toByte(unknown1, true));
      out.write(DataTools.toByte(unknown2, true));
      out.write(DataTools.toByte(unknown3, true));
      out.write(DataTools.toByte(ignore, true));
    }

  }

}
