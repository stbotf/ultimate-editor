package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeSet;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.GShipList;
import ue.util.data.CollectionTools;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// AgtSh lists all the ship ids of an AIAgent controlled race.
public class AgtSh extends InternalFile {

  public static final String CNTR_SUFFIX = CSavFiles.AgtSh_Suffix;
  public static final String FILE_NAME = CSavFiles.AgtSh;

  private AgtShCnt header;
  private SavGame game;
  private Stbof stbof;
  @Getter private int agentId = -1;

  // sort ship ids to simplify hex lookup
  private TreeSet<Short> m_shipIds = new TreeSet<Short>(); // numShips * 2

  public AgtSh(AgtShCnt header, SavGame game, Stbof stbof) {
    this.header = header;
    this.game = game;
    this.stbof = stbof;
  }

  public String descriptor(RaceRst raceRst) {
    AIAgent agt = (AIAgent) game.tryGetInternalFile(CSavFiles.AIAgent + Integer.toString(agentId), true);
    return agt != null ? agt.descriptor(raceRst) : "Agent " + Integer.toString(agentId);
  }

  private void updateHeader() {
    header.setShCount(m_shipIds.size());
  }

  public int getShipCount() {
    return m_shipIds.size();
  }

  public short[] getShipIds() {
    return CollectionTools.toShortArray(m_shipIds);
  }

  private boolean differs(short[] shipIds) {
    if (shipIds.length != m_shipIds.size())
      return true;

    for (short shipId : shipIds) {
      if (!m_shipIds.contains(shipId))
        return true;
    }

    return false;
  }

  @Override
  public void setName(String name) {
    super.setName(name);
    updateId();
  }

  public void setShipIds(short[] shipIds) {
    // check if changed
    if (!differs(shipIds)) {
      return;
    }

    // update the ship ids
    m_shipIds.clear();
    for (short id : shipIds) {
      m_shipIds.add(id);
    }

    markChanged();
  }

  public void addShip(short shipId) {
    m_shipIds.add(shipId);
    updateHeader();
    markChanged();
  }

  public boolean removeShip(short shipId) {
    if (m_shipIds.remove(shipId)) {
      updateHeader();
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllShips() {
    if (!m_shipIds.isEmpty()) {
      m_shipIds.clear();
      updateHeader();
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    m_shipIds.clear();

    int numShips = header.getShCount();
    int size = in.available();
    if (size != numShips << 1)
      System.out.println(getName() + ": Ship number doesn't match the entry number!");

    numShips = Integer.min(size >> 1, numShips);
    for (int i = 0; i < numShips; ++i) {
      m_shipIds.add(StreamTools.readShort(in, true));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (short shpId : m_shipIds) {
      out.write(DataTools.toByte(shpId, true));
    }
  }

  @Override
  public void clear() {
    m_shipIds.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    GShipList shipList = (GShipList) game.tryGetInternalFile(CSavFiles.GShipList, true);
    if (shipList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ": Missing GShipList!"));
      return;
    }

    RaceRst raceRst = (RaceRst) stbof.tryGetInternalFile(CStbofFiles.RaceRst, true);

    boolean any = m_shipIds.removeIf(shpId ->
    {
      if (!shipList.hasShip(shpId)) {
        String msg = descriptor(raceRst) + " lists ship id %1, that is missing from %2. (common, but removed)";
        msg = msg.replace("%1", Integer.toString(shpId));
        msg = msg.replace("%2", CSavFiles.GShipList);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));
        return true;
      }
      return false;
    });

    if (any) {
      updateHeader();
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return m_shipIds.size() * 2;
  }

  private void updateId() {
    String agtNbr = NAME.substring(CSavFiles.AgtSh.length());
    agentId = Integer.parseInt(agtNbr);
  }

}
