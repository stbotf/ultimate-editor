package ue.edit.sav.files.ai;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AgtTkCnt extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.AgtTk_Suffix;
  public static final String AGTTk_FILE_NAME = CSavFiles.AgtTk;

  private SavGame game;
  @Getter private int taskCount = 0;

  public AgtTkCnt(SavGame game) {
    this.game = game;
  }

  public String getAgtTkName() {
    return NAME.substring(0, NAME.length() - CNT_SUFFIX.length());
  }

  public void setTaskCount(int cnt) {
    if (taskCount != cnt) {
      taskCount = cnt;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    taskCount = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(taskCount, true));
  }

  @Override
  public void clear() {
    taskCount = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    String agtName = getAgtTkName();

    try {
      AgtTk agtTk = (AgtTk) game.getInternalFile(agtName, false);
      int cnt = agtTk.getTaskCount();

      if (taskCount != cnt) {
        String msg = "Task number %1 doesn't match the entry number %2! (fixed)";
        msg = msg.replace("%1", Integer.toString(taskCount));
        msg = msg.replace("%2", Integer.toString(cnt));
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      setTaskCount(cnt);
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      String msg = "Missing task agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    catch (Exception e) {
      e.printStackTrace();
      String msg = "Failed to check task agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
  }

  @Override
  public int getSize() {
    return 4;
  }

}
