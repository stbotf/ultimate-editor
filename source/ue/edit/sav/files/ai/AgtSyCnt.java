package ue.edit.sav.files.ai;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AgtSyCnt extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.AgtSy_Suffix;
  public static final String AGTSY_FILE_NAME = CSavFiles.AgtSy;

  private SavGame game;
  @Getter private int systemCount = 0;

  public AgtSyCnt(SavGame game) {
    this.game = game;
  }

  public String getAgtSyName() {
    return NAME.substring(0, NAME.length() - CNT_SUFFIX.length());
  }

  public void setSystemCount(int cnt) {
    if (systemCount != cnt) {
      systemCount = cnt;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    systemCount = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(systemCount, true));
  }

  @Override
  public void clear() {
    systemCount = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    String agtName = getAgtSyName();

    try {
      AgtSy agtSy = (AgtSy) game.getInternalFile(agtName, false);
      int cnt = agtSy.getSystemCount();

      if (systemCount != cnt) {
        String msg = "System number %1 doesn't match the entry number %2! (fixed)";
        msg = msg.replace("%1", Integer.toString(systemCount));
        msg = msg.replace("%2", Integer.toString(cnt));
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      setSystemCount(cnt);
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      String msg = "Missing system agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    catch (Exception e) {
      e.printStackTrace();
      String msg = "Failed to check system agent %1! (error)"
        .replace("%1", agtName);
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
  }

  @Override
  public int getSize() {
    return 4;
  }

}
