package ue.edit.sav.files.ai;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeMap;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.TskSy;
import ue.util.data.CollectionTools;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * The AISysUnt file is for planning system tasks. It's likely an abbreviation for ai system undertaking.
 */
public class AISysUnt extends InternalFile {

  public static final String FileName = CSavFiles.AISysUnt;

  private static final int SYSUNT_ENTRY_SIZE = 12;
  private AISysUntCnt header;
  // private int numSystems; // 4 auto-computed

  private SavGame game;

  // keep system list sorted for BotF
  private TreeMap<Short, SysUnt> sysUnts = new TreeMap<>(); // numSystems * 4 + 4

  public AISysUnt(AISysUntCnt header, SavGame game) {
    this.header = header;
    this.game = game;
  }

  public int getSystemCount() {
    return sysUnts.size();
  }

  public short[] getSystemIds() {
    return CollectionTools.toShortArray(sysUnts.keySet());
  }

  public boolean hasSystemId(short systemId) {
    return sysUnts.containsKey(systemId);
  }

  public SysUnt getSystemUnt(short systemId) {
    return sysUnts.get(systemId);
  }

  public short getSystemTask(short systemId) {
    SysUnt sys = sysUnts.get(systemId);
    return sys.taskId;
  }

  // this possibly requires to add some AITask
  public void addSystemUnt(short systemId) {
    SysUnt sysUnt = new SysUnt(systemId);
    sysUnts.put(systemId, sysUnt);
    updateHeader();
    markChanged();
  }

  public void clearSystemUnt(short systemId) {
    SysUnt sys = sysUnts.get(systemId);
    if (sys != null)
      sys.clearTask();
  }

  public void clearTask(short taskId) {
    for (SysUnt sys : sysUnts.values()) {
      if (sys.getTaskId() == taskId)
        sys.clearTask();
    }
  }

  public void clearAllTasks() {
    for (SysUnt sys : sysUnts.values()) {
      sys.clearTask();
    }
  }

  public SysUnt removeSystemUnt(short systemId) {
    SysUnt rem = sysUnts.remove(systemId);
    if (rem != null) {
      updateHeader();
      markChanged();
    }
    return rem;
  }

  public void removeAll() {
    if (!sysUnts.isEmpty()) {
      sysUnts.clear();
      updateHeader();
      markChanged();
    }
  }

  public void onRemovedSystem(short systemId) {
    if (sysUnts.isEmpty() || sysUnts.lastKey() < systemId)
      return;

    // update system ids
    TreeMap<Short, SysUnt> copy = new TreeMap<>();
    for (SysUnt sysUnt : sysUnts.values()) {
      if (sysUnt.systemId != systemId) {
        short sysId = sysUnt.systemId < systemId ? sysUnt.systemId : --sysUnt.systemId;
        copy.put(sysId, sysUnt);
      }
    }

    boolean removed = sysUnts.size() != copy.size();
    sysUnts = copy;
    markChanged();
    if (removed)
      updateHeader();
  }

  @Override
  public int getSize() {
    return sysUnts.size() * SYSUNT_ENTRY_SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    sysUnts.clear();

    int numSystems = this.header.getSystemCount();

    int size = in.available();
    if (size != numSystems * SYSUNT_ENTRY_SIZE) {
      System.out.println("AISysUnt: System number doesn't match the entry number!");
    }

    numSystems = Integer.min(size / SYSUNT_ENTRY_SIZE, numSystems);
    for (int i = 0; i < numSystems; ++i) {
      SysUnt sysUnt = new SysUnt(in);
      sysUnts.put(sysUnt.systemId, sysUnt);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (SysUnt sysUnt : sysUnts.values()) {
      sysUnt.save(out);
    }
  }

  @Override
  public void clear() {
    sysUnts.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    SystInfo systemList = (SystInfo) game.tryGetInternalFile(CSavFiles.SystInfo, true);
    if (systemList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, "Missing SystInfo!"));
      return;
    }

    boolean any = sysUnts.keySet().removeIf(sysId -> {
      if (!systemList.hasSystem(sysId)) {
        String msg = "Found system id %1, that is missing from %2. (common, but removed)";
        msg = msg.replace("%1", Integer.toString(sysId));
        msg = msg.replace("%2", CSavFiles.SystInfo);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));

        // remove current AI system task
        short aiTaskId = getSystemTask(sysId);
        if (aiTaskId != -1) {
          try {
            TskSy tskSy = (TskSy) game.getInternalFile(CSavFiles.TskSy + aiTaskId, true);
            tskSy.removeSystem(sysId);
          }
          catch (Exception e) {
            msg = "Failed to update system %1 task %2. (error)";
            msg = msg.replace("%1", Integer.toString(sysId));
            msg = msg.replace("%2", Short.toString(aiTaskId));
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        }

        return true;
      }
      return false;
    });

    if (any) {
      updateHeader();
      markChanged();
    }
  }

  private void updateHeader() {
    header.setSystemCount(sysUnts.size());
  }

  // refer https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54833#p54833
  @Getter
  public class SysUnt {

    private short systemId /*(starId)*/;    // 2
    private short nextTaskId;               // 4
    // current task id, which in addition equals the TskSy file number
    private short taskId;                   // 6
    // SCT: area not initialized (unusual for BotF) might be unused & just data garbage
    private short unknown;                  // 8
    // SCT: unlock build systemyard marker -> cf. AITask+10h task data
    // similar data structure for sys dev tasks -> e.g. @ +8 sys dev plan
    private int ptrSpecialTaskData;

    public SysUnt(short systemId) {
      this.systemId = systemId;
      nextTaskId = -1;
      taskId = -1;
      unknown = 0;
      ptrSpecialTaskData = 0;
    }

    SysUnt(InputStream in) throws IOException {
      load(in);
    }

    public void load (InputStream in) throws IOException {
      systemId = StreamTools.readShort(in, true);
      nextTaskId = StreamTools.readShort(in, true);
      taskId = StreamTools.readShort(in, true);
      unknown = StreamTools.readShort(in, true);
      ptrSpecialTaskData = StreamTools.readInt(in, true);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(systemId, true));
      out.write(DataTools.toByte(nextTaskId, true));
      out.write(DataTools.toByte(taskId, true));
      out.write(DataTools.toByte(unknown, true));
      out.write(DataTools.toByte(ptrSpecialTaskData, true));
    }

    public void clearTask() {
      if (taskId != -1 || nextTaskId != -1) {
        taskId = -1;
        nextTaskId = -1;
        markChanged();
      }
    }

  }

}
