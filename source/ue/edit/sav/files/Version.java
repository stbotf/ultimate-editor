package ue.edit.sav.files;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;

@Accessors(prefix = {"m"})
public class Version extends InternalFile {

  @Getter private int mVersion = 0;

  public Version(SavGame game) {
  }

  public void setVersion(int version) {
    if (mVersion != version) {
      mVersion = version;
      this.markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    mVersion = Byte.toUnsignedInt((byte)in.read());
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write((byte)mVersion);
  }

  @Override
  public void clear() {
    mVersion = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return 1;
  }
}
