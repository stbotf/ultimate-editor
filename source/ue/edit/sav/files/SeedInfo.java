package ue.edit.sav.files;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Accessors(prefix = {"m"})
public class SeedInfo extends InternalFile {

  private byte[] mUnknown_1 = new byte[20];               // 0x000 (000+020)
  @Getter private int mStartingSeed = 0;                  // 0x014 (020+004)
  private byte[] mUnknown_2 = new byte[264];              // 0x018 (024+264)

  public SeedInfo(SavGame game) {
  }

  public void setStartingSeed(int seed) {
    if (mStartingSeed != seed) {
      mStartingSeed = seed;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    StreamTools.read(in, mUnknown_1);                     // 0x000 (000+020)
    mStartingSeed = StreamTools.readInt(in, true);        // 0x014 (020+004)
    StreamTools.read(in, mUnknown_2);                     // 0x018 (024+264)
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(mUnknown_1);                                // 0x000 (000+020)
    out.write(DataTools.toByte(mStartingSeed, true));     // 0x014 (020+004)
    out.write(mUnknown_2);                                // 0x018 (024+264)
  }

  @Override
  public void clear() {
    Arrays.fill(mUnknown_1, (byte)0);
    mStartingSeed = 0;
    Arrays.fill(mUnknown_2, (byte)0);
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return mUnknown_1.length + 4 + mUnknown_2.length;
  }
}
