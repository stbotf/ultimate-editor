package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class RToSInfo extends InternalFile {

  public static final String FILE_NAME = CSavFiles.RToSInfo;
  public static final short NONE = (short) 0xFF;

  // Lists the home systems of all the races.
  private ArrayList<Short> homeSystemIds = new ArrayList<Short>();

  public RToSInfo(SavGame game) {
  }

  public short getHomeSystem(short raceId) {
    return homeSystemIds.get(raceId);
  }

  public short setHomeSystem(short raceId, short systemId) {
    short prev = homeSystemIds.set(raceId, systemId);
    if (prev != systemId)
      markChanged();
    return systemId;
  }

  public boolean clearHomeSystem(short raceId) {
    // unset the race's home system
    // it is always 35 entries and instead of -1
    // non-participating and dead races are set to 'FF 00'
    short prev = homeSystemIds.set(raceId, NONE);
    if (prev != NONE) {
      markChanged();
      return true;
    }
    return false;
  }

  public int getHomeSystemRace(short systemId) {
    for (int i = 0; i < homeSystemIds.size(); ++i) {
      if (homeSystemIds.get(i) == systemId)
        return i;
    }
    return -1;
  }

  public void onRemovedSystem(short systemId) {
    for (int raceId = 0; raceId < homeSystemIds.size(); ++raceId) {
      short sysId = homeSystemIds.get(raceId);
      // skip 'FF 00', which is set for missing home systems
      if (sysId == 0xFF)
        continue;

      if (sysId > systemId) {
        // update system id
        homeSystemIds.set(raceId, --sysId);
        markChanged();
      } else if (sysId == systemId) {
        // unset the race's home system
        // it is always 35 entries and instead of -1
        // non-participating and dead races are set to 'FF 00'
        homeSystemIds.set(raceId, NONE);
        markChanged();
      }
    }
  }

  @Override
  public int getSize() {
    return homeSystemIds.size() * 2;
  }

  @Override
  public void load(InputStream in) throws IOException {
    homeSystemIds.clear();

    int size = in.available();
    for (; size > 0; size -= 2) {
      homeSystemIds.add(StreamTools.readShort(in, true));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (short sysId : homeSystemIds) {
      out.write(DataTools.toByte(sysId, true));
    }
  }

  @Override
  public void clear() {
    homeSystemIds.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

}
