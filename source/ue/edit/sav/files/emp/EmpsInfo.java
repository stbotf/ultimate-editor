package ue.edit.sav.files.emp;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;
import lombok.val;
import ue.common.CommonStrings;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.sav.SavGame;
import ue.exception.InvalidArgumentsException;
import ue.util.calc.Calc;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * @author Alan P.
 */
public class EmpsInfo extends InternalFile {

  public interface Relationship {
    public static final int Neutral = 0;
    public static final int War = 1;
    public static final int Alliance = 2;
    public static final int Peace = 3;
  }

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public class EmpireStats {
    public String empireName;
    public String emperor;
    public int credits = 0;
    public int income = 0;
    public int dilithium = 0;
    public int supportCost = 0;
    public int populationSupport = 0;
    public int empireMorale = 0;
    public short combatBonus = 0;
    public int shipsInBuild = 0;
    // mapped by major empire relationships
    public HashMap<Integer, Integer> relationships = new HashMap<Integer, Integer>();
    public int knownRaces = 0;

    public EmpireStats() {
      // pre-fill known entries so they aren't missed
      relationships.put(Relationship.Neutral, 0);
      relationships.put(Relationship.War, 0);
      relationships.put(Relationship.Alliance, 0);
      relationships.put(Relationship.Peace, 0);
    }
  }

  public static final int SIZE = 2220; // 444 * 5 empires

  private EmpsInfoEntry[] empire = new EmpsInfoEntry[NUM_EMPIRES];

  /**
   * @param savGame
   * @param name name of the loaded file
   */
  public EmpsInfo(SavGame savGame) {
  }

  /**
   * @param in   input stream to read from
   * @throws IOException
   */
  @Override
  public void load(InputStream in) throws IOException {
    int i = 0;
    for (; i < empire.length && in.available() > 0; ++i) {
      empire[i] = new EmpsInfoEntry(in);
    }

    if (i != empire.length) {
      throw new IOException(CommonStrings.getLocalizedString(
        CommonStrings.File_Is_Corrupted_1, NAME));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    DataOutputStream dos = new DataOutputStream(out);

    for (EmpsInfoEntry entry : empire) {
      entry.save(dos);
    }
    dos.flush();
  }

  @Override
  public void clear() {
    Arrays.fill(empire, null);
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  /**
   * Called to set the uncompressed file size.
   * <p>
   * If incorrect, the trek.exe load is broken and e.g. fails some later turn with: File:
   * ..\..\source\game\solarapi.c, Line: 2792, System is not dest or source of trade route
   * <p>
   * If set too low, the next (auto-)save misses data for the last entry, which turns out to be the
   * romulan star empire this case.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  // GET
  public EmpireStats getEmpireStats(int empireId) {
    EmpireStats stats = new EmpireStats();

    EmpsInfoEntry emp = this.empire[empireId];
    stats.empireName = emp.empire;
    stats.emperor = emp.emperor;
    stats.credits = emp.startCredits;
    stats.income = emp.startIncome;
    stats.dilithium = emp.currentDilithiumIncome;
    stats.supportCost = emp.startSupportCost;
    stats.populationSupport = emp.currentShipPopSupport;
    stats.empireMorale = emp.startMoraleLevel;
    stats.combatBonus = emp.startGroundCombatBonus;
    stats.shipsInBuild = emp.currentShipsInBuild;

    // count the different treaty labels
    for (int treaty : emp.treatyStatus)
      stats.relationships.merge(treaty, 1, Calc::sum);
    // count known races
    stats.knownRaces = Long.bitCount(emp.knownRacesMask);

    return stats;
  }

  public EmpireStats[] getStats() {
    EmpireStats[] stats = new EmpireStats[NUM_EMPIRES];
    for (int i = 0; i < NUM_EMPIRES; ++i)
      stats[i] = getEmpireStats(i);

    return stats;
  }

  public String getEmpireTitle(int index) {
    testIndex(index);
    return this.empire[index].empire;
  }

  public String getEmperor(int index) {
    testIndex(index);
    return this.empire[index].emperor;
  }

  public int getIncome(int index) {
    testIndex(index);
    return this.empire[index].startIncome;
  }

  public int getDilithiumIncome(int index) {
    testIndex(index);
    return this.empire[index].currentDilithiumIncome;
  }

  public int getNumberOfShipsUnderConstruction(int index) {
    testIndex(index);
    return this.empire[index].currentShipsInBuild;
  }

  public int getCreditsAvailable(int index) {
    testIndex(index);
    return this.empire[index].startCredits;
  }

  public int getShipPopSupport(int index) {
    testIndex(index);
    return this.empire[index].currentShipPopSupport;
  }

  /**
   * @param index
   * @return a value from range [-4, 3]
   * @throws InvalidArgumentsException
   */
  public int getEmpireMorale(int index) {
    testIndex(index);
    return this.empire[index].startMoraleLevel;
  }

  public int getTreatyLabel(int index, int empire) {
    testIndex(index);
    return this.empire[index].treatyStatus[empire];
  }

  public int getTerritoryAccess(int index, int empire) {
    testIndex(index);
    return this.empire[index].territoryAccess[empire];
  }

  public int getTradeAccess(int index, int empire) {
    testIndex(index);
    return this.empire[index].tradeAllowed[empire];
  }

  public short getGroundCombatBonus(int index) {
    testIndex(index);
    return this.empire[index].startGroundCombatBonus;
  }

  // SET
  public void setEmpireTitle(int index, String title) {
    testIndex(index);
    if (!this.empire[index].empire.equals(title)) {
      this.empire[index].empire = title;
      markChanged();
    }
  }

  public void setEmperor(int index, String emperor) {
    testIndex(index);
    if (!this.empire[index].emperor.equals(emperor)) {
      this.empire[index].emperor = emperor;
      markChanged();
    }
  }

  public void setCreditsAvailable(int index, int credits) {
    testIndex(index);
    if (this.empire[index].startCredits != credits) {
      this.empire[index].startCredits = credits;
      markChanged();
    }
  }

  public boolean isRaceKnown(int index, int raceId) {
    testIndex(index);
    // known empires (bitwise XOR: 1=card, 2=fed, 4=ferg, 8=klng, 10=rom)
    long mask = 1L << raceId;
    return ((this.empire[index].knownRacesMask & mask) == mask);
  }

  public void setRaceKnown(int index, int raceId, boolean known) {
    testIndex(index);

    // known empires (bitwise XOR: 1=card, 2=fed, 4=ferg, 8=klng, 10=rom)
    long mask = 1L << raceId;

    boolean isKnown = ((this.empire[index].knownRacesMask & mask) == mask);
    if (isKnown != known) {
      if (known) {
        this.empire[index].knownRacesMask |= mask;
      } else {
        this.empire[index].knownRacesMask &= ~mask;
      }

      markChanged();
    }
  }

  public void removeEmpireTradeRoutes(int index, int removedTradeRoutes) {
    if (removedTradeRoutes > 0) {
      testIndex(index);
      val entry = empire[index];
      entry.numTradeRoutes = Integer.max(0, entry.numTradeRoutes - removedTradeRoutes);
      markChanged();
    }
  }

  private void testIndex(int index) {
    if (index < 0 || index >= this.empire.length) {
      throw new IndexOutOfBoundsException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(index)));
    }
  }

  private class EmpsInfoEntry {

    String empire;                                                      // 0x0000 + 40 bytes
    String emperor;                                                     // 0x0028 + 40 bytes
    // address of provInfo (8 bytes per system), at [+4]: number of held systems
    final int addrProvInfo;                                             // 0x0050 + 4 bytes

    /** GUI / preview (0x54 to 0x97)
     * The displayed current turn progress, which is pre-calculated
     * but not effective for next turn.
     */
    // auto-calculated credits with all expense subtracted
    final int currentCredits;                                           // 0x0054 + 4 bytes
    // auto-calculated credits income
    final int currentIncome;                                            // 0x0058 + 4 bytes
    // real task force support cost (i.e. -pop support) + building support costs
    final int currentSupportCost;                                       // 0x005C + 4 bytes
    final int currentDilithiumIncome;                                   // 0x0060 + 4 bytes
    final int currentShipsInBuild;                                      // 0x0064 + 4 bytes
    final int currentResearchPoints;                                    // 0x0068 + 4 bytes
    final int currentTotalPopulation;                                   // 0x006C + 4 bytes
    final short unused1;                                                // 0x0070 + 2 bytes
    final short unknown1;                                               // 0x0072 + 2 bytes
    // current empire morale level displayed in empire state view
    final short currentMoraleLevel;                                     // 0x0074 + 2 bytes
    final short unknown2;                                               // 0x0076 + 2 bytes
    final int currentIntel;                                             // 0x0078 + 4 bytes
    // cumulated research bonus in percent (6 tech areas * 4bytes)
    final int[] currentResearchAreaBonus = new int[6];                  // 0x007C + 24 bytes
    // ship population support is set to 0 if fleet cost is larger i.e. not always the display value
    final int currentShipPopSupport;                                    // 0x0094 + 4 bytes

    /** Effectively used values. (0x98 to 0xDB)
     * Turn start values calculated on last turn progress. Unlike the GUI preview data,
     * these are effectively used for next turn calculation.
     */
    int startCredits;                                                   // 0x0098 + 4 bytes
    final int startIncome;                                              // 0x009C + 4 bytes
    final int startSupportCost;                                         // 0x00A0 + 4 bytes
    final int startDilithiumIncome;                                     // 0x00A4 + 4 bytes
    final int startShipsInBuild;                                        // 0x00A8 + 4 bytes
    final int startResearchPoints;                                      // 0x00AC + 4 bytes
    final int startTotalPopulation;                                     // 0x00B0 + 4 bytes
    // empire wide morale effect of current turn events
    final short eventMoraleEffect;                                      // 0x00B4 + 2 bytes
    final short unknown3;                                               // 0x00B6 + 2 bytes
    final short startMoraleLevel;                                       // 0x00B8 + 2 bytes
    final short unknown4;                                               // 0x00BA + 2 bytes
    final int startIntel;                                               // 0x00BC + 4 bytes
    // cumulated research bonus in percent (6 tech areas * 4 bytes)
    final int[] startResearchAreaBonus = new int[6];                    // 0x00C0 + 24 bytes
    // ship population support is set to 0 if fleet cost is larger i.e. not always the display value
    final int startShipPopSupport;                                      // 0x00D8 + 4 bytes

    /** GUI / preview (0xDC to 0xF5)
     */
    final short currentCreditBonus;                                     // 0x00DC + 2 bytes
    final short currentMoraleBonus;                                     // 0x00DE + 2 bytes
    final short currentRaidBonus;                   // not working      // 0x00E0 + 2 bytes
    final short currentIndustryBonus;               // missing feature  // 0x00E2 + 2 bytes
    final short currentResearchBonus;               // all areas        // 0x00E4 + 2 bytes
    final short currentSecurityBonus;                                   // 0x00E6 + 2 bytes
    final short currentEspionageBonus;                                  // 0x00E8 + 2 bytes
    final short currentSabotageBonus;                                   // 0x00EA + 2 bytes
    final short currentEconomicIntelBonus;          // not working      // 0x00EC + 2 bytes
    final short currentScienceIntelBonus;           // not working      // 0x00EE + 2 bytes
    final short currentMilitaryIntelBonus;          // not working      // 0x00F0 + 2 bytes
    final short currentTradeIncomeBonus;                                // 0x00F2 + 2 bytes
    final short currentGroundCombatBonus;                               // 0x00F4 + 2 bytes


    /** Effectively used values. (0xF6 to 0x10F)
     */
    final short startCreditBonus;                                       // 0x00F6 + 2 bytes
    final short startMoraleBonus;                                       // 0x00F8 + 2 bytes
    final short startRaidBonus;                     // not working      // 0x00FA + 2 bytes
    final short startIndustryBonus;                 // missing feature  // 0x00FC + 2 bytes
    final short startResearchBonus;                 // all areas        // 0x00FE + 2 bytes
    final short startSecurityBonus;                                     // 0x0100 + 2 bytes
    final short startEspionageBonus;                                    // 0x0102 + 2 bytes
    final short startSabotageBonus;                                     // 0x0104 + 2 bytes
    final short startEconomicIntelBonus;            // not working      // 0x0106 + 2 bytes
    final short startScienceIntelBonus;             // not working      // 0x0108 + 2 bytes
    final short startMilitaryIntelBonus;            // not working      // 0x010A + 2 bytes
    final short startTradeIncomeBonus;                                  // 0x010C + 2 bytes
    final short startGroundCombatBonus;                                 // 0x010E + 2 bytes

    // treaty label: 0=Neutral, 1=War, 2=Alliance, 3=Peace
    int[] treatyStatus = new int[NUM_EMPIRES];                          // 0x0110 + 20 bytes
    // territory accessible: 0=No, 1=Yes
    int[] territoryAccess = new int[NUM_EMPIRES];                       // 0x0124 + 20 bytes
    // trade allowed: 0=No, 1=Yes
    int[] tradeAllowed = new int[NUM_EMPIRES];                          // 0x0138 + 20 bytes
    // total number of trade routes
    int numTradeRoutes;                                                 // 0x014C + 4 bytes
    final int unknown5;                                                 // 0x0150 + 4 bytes
    final int trdeInfoAddr;                                             // 0x0154 + 4 bytes
    // known races (bitwise XOR with 0x01 = card, 0x02 = fed, 0x04 = ferg, 0x08 = kling, 0x10 = rom, 0x20 = acam ...)
    long knownRacesMask;                                                // 0x0158 + 8 bytes
    // present (or in production queue) "one per empire" 2 byte building IDs, 40 entries max.
    final short[][] prodQueue = new short[NUM_EMPIRES][8];              // 0x0160 + 80 bytes
    // random events bit mask
    final int randomEventsMask;                                         // 0x01B0 + 4 bytes
    // cumulated build cost of war fleet (ship functions 0-4)
    final int totalFleetBuildCost;                                      // 0x01B4 + 4 bytes
    // credits lost due to raids, subtracted from [0x98]
    final int creditsRaided;                                            // 0x01B8 + 4 bytes
    // SUM: 0x1BC = 444 bytes

    EmpsInfoEntry(InputStream in) throws IOException {
      // empire 40
      empire = StreamTools.readNullTerminatedBotfString(in, 40);
      // emperor 80
      emperor = StreamTools.readNullTerminatedBotfString(in, 40);
      // address of prov info 84
      addrProvInfo = StreamTools.readInt(in, true);
      // displayed_credits 88
      currentCredits = StreamTools.readInt(in, true);
      // income 92
      currentIncome = StreamTools.readInt(in, true);
      // task force support cost 96
      currentSupportCost = StreamTools.readInt(in, true);
      // dilithium income 100
      currentDilithiumIncome = StreamTools.readInt(in, true);
      // ships being built 104
      currentShipsInBuild = StreamTools.readInt(in, true);
      currentResearchPoints = StreamTools.readInt(in, true);
      currentTotalPopulation = StreamTools.readInt(in, true);
      unused1 = StreamTools.readShort(in, true);
      unknown1 = StreamTools.readShort(in, true);
      currentMoraleLevel = StreamTools.readShort(in, true);
      unknown2 = StreamTools.readShort(in, true);
      currentIntel = StreamTools.readInt(in, true);

      for (int i = 0; i < currentResearchAreaBonus.length; i++) {
        currentResearchAreaBonus[i] = StreamTools.readInt(in, true);
      }

      // ship population support 152
      currentShipPopSupport = StreamTools.readInt(in, true);
      // available turn start credits 156
      startCredits = StreamTools.readInt(in, true);
      startIncome = StreamTools.readInt(in, true);
      startSupportCost = StreamTools.readInt(in, true);
      startDilithiumIncome = StreamTools.readInt(in, true);
      startShipsInBuild = StreamTools.readInt(in, true);
      startResearchPoints = StreamTools.readInt(in, true);
      startTotalPopulation = StreamTools.readInt(in, true);
      eventMoraleEffect = StreamTools.readShort(in, true);

      // unknown 184
      unknown3 = StreamTools.readShort(in, true);
      // empire morale 186
      startMoraleLevel = StreamTools.readShort(in, true);
      // unknown 272
      unknown4 = StreamTools.readShort(in, true);

      startIntel = StreamTools.readInt(in, true);
      for (int i = 0; i < startResearchAreaBonus.length; i++) {
        startResearchAreaBonus[i] = StreamTools.readInt(in, true);
      }
      startShipPopSupport = StreamTools.readInt(in, true);

      currentCreditBonus = StreamTools.readShort(in, true);
      currentMoraleBonus = StreamTools.readShort(in, true);
      currentRaidBonus = StreamTools.readShort(in, true);
      currentIndustryBonus = StreamTools.readShort(in, true);
      currentResearchBonus = StreamTools.readShort(in, true);
      currentSecurityBonus = StreamTools.readShort(in, true);
      currentEspionageBonus = StreamTools.readShort(in, true);
      currentSabotageBonus = StreamTools.readShort(in, true);
      currentEconomicIntelBonus = StreamTools.readShort(in, true);
      currentScienceIntelBonus = StreamTools.readShort(in, true);
      currentMilitaryIntelBonus = StreamTools.readShort(in, true);
      currentTradeIncomeBonus = StreamTools.readShort(in, true);
      currentGroundCombatBonus = StreamTools.readShort(in, true);

      startCreditBonus = StreamTools.readShort(in, true);
      startMoraleBonus = StreamTools.readShort(in, true);
      startRaidBonus = StreamTools.readShort(in, true);
      startIndustryBonus = StreamTools.readShort(in, true);
      startResearchBonus = StreamTools.readShort(in, true);
      startSecurityBonus = StreamTools.readShort(in, true);
      startEspionageBonus = StreamTools.readShort(in, true);
      startSabotageBonus = StreamTools.readShort(in, true);
      startEconomicIntelBonus = StreamTools.readShort(in, true);
      startScienceIntelBonus = StreamTools.readShort(in, true);
      startMilitaryIntelBonus = StreamTools.readShort(in, true);
      startTradeIncomeBonus = StreamTools.readShort(in, true);
      startGroundCombatBonus = StreamTools.readShort(in, true);

      // treaty labels 292
      for (int i = 0; i < treatyStatus.length; i++) {
        treatyStatus[i] = StreamTools.readInt(in, true);
      }
      // territory access 312
      for (int i = 0; i < territoryAccess.length; i++) {
        territoryAccess[i] = StreamTools.readInt(in, true);
      }
      // trade access 332
      for (int i = 0; i < tradeAllowed.length; i++) {
        tradeAllowed[i] = StreamTools.readInt(in, true);
      }

      numTradeRoutes = StreamTools.readInt(in, true);
      unknown5 = StreamTools.readInt(in, true);
      trdeInfoAddr = StreamTools.readInt(in, true);

      // known race 352
      knownRacesMask = StreamTools.readLong(in, true);

      for (int i = 0; i < 8; i++) {
        for (int emp = 0; emp < NUM_EMPIRES; emp++) {
          prodQueue[emp][i] = StreamTools.readShort(in, true);
        }
      }

      randomEventsMask = StreamTools.readInt(in, true);
      totalFleetBuildCost = StreamTools.readInt(in, true);
      creditsRaided = StreamTools.readInt(in, true);
    }

    void save(DataOutputStream dos) throws IOException {
      // empire 40
      dos.write(DataTools.toByte(empire, 40, 39));
      // emperor 80
      dos.write(DataTools.toByte(emperor, 40, 39));
      // address of prov info 84
      dos.writeInt(Integer.reverseBytes(addrProvInfo));
      // displayed_credits 88
      dos.writeInt(Integer.reverseBytes(currentCredits));
      // income 92
      dos.writeInt(Integer.reverseBytes(currentIncome));
      // task force support cost 96
      dos.writeInt(Integer.reverseBytes(currentSupportCost));
      // dilithium income 100
      dos.writeInt(Integer.reverseBytes(currentDilithiumIncome));
      // ships being built 104
      dos.writeInt(Integer.reverseBytes(currentShipsInBuild));
      dos.writeInt(Integer.reverseBytes(currentResearchPoints));
      dos.writeInt(Integer.reverseBytes(currentTotalPopulation));
      dos.writeShort(Short.reverseBytes(unused1));
      dos.writeShort(Short.reverseBytes(unknown1));
      dos.writeShort(Short.reverseBytes(currentMoraleLevel));
      dos.writeShort(Short.reverseBytes(unknown2));
      dos.writeInt(Integer.reverseBytes(currentIntel));

      for (int i = 0; i < currentResearchAreaBonus.length; i++) {
        dos.writeInt(Integer.reverseBytes(currentResearchAreaBonus[i]));
      }

      // ship population support 152
      dos.writeInt(Integer.reverseBytes(currentShipPopSupport));
      // available turn start credits 156
      dos.writeInt(Integer.reverseBytes(startCredits));
      dos.writeInt(Integer.reverseBytes(startIncome));
      dos.writeInt(Integer.reverseBytes(startSupportCost));
      dos.writeInt(Integer.reverseBytes(startDilithiumIncome));
      dos.writeInt(Integer.reverseBytes(startShipsInBuild));
      dos.writeInt(Integer.reverseBytes(startResearchPoints));
      dos.writeInt(Integer.reverseBytes(startTotalPopulation));
      dos.writeShort(Short.reverseBytes(eventMoraleEffect));

      // unknown 184
      dos.writeShort(Short.reverseBytes(unknown3));
      // empire morale 186
      dos.writeShort(Short.reverseBytes(startMoraleLevel));
      // unknown 272
      dos.writeShort(Short.reverseBytes(unknown4));

      dos.writeInt(Integer.reverseBytes(startIntel));
      for (int i = 0; i < startResearchAreaBonus.length; i++) {
        dos.writeInt(Integer.reverseBytes(startResearchAreaBonus[i]));
      }
      dos.writeInt(Integer.reverseBytes(startShipPopSupport));

      dos.writeShort(Short.reverseBytes(currentCreditBonus));
      dos.writeShort(Short.reverseBytes(currentMoraleBonus));
      dos.writeShort(Short.reverseBytes(currentRaidBonus));
      dos.writeShort(Short.reverseBytes(currentIndustryBonus));
      dos.writeShort(Short.reverseBytes(currentResearchBonus));
      dos.writeShort(Short.reverseBytes(currentSecurityBonus));
      dos.writeShort(Short.reverseBytes(currentEspionageBonus));
      dos.writeShort(Short.reverseBytes(currentSabotageBonus));
      dos.writeShort(Short.reverseBytes(currentEconomicIntelBonus));
      dos.writeShort(Short.reverseBytes(currentScienceIntelBonus));
      dos.writeShort(Short.reverseBytes(currentMilitaryIntelBonus));
      dos.writeShort(Short.reverseBytes(currentTradeIncomeBonus));
      dos.writeShort(Short.reverseBytes(currentGroundCombatBonus));

      dos.writeShort(Short.reverseBytes(startCreditBonus));
      dos.writeShort(Short.reverseBytes(startMoraleBonus));
      dos.writeShort(Short.reverseBytes(startRaidBonus));
      dos.writeShort(Short.reverseBytes(startIndustryBonus));
      dos.writeShort(Short.reverseBytes(startResearchBonus));
      dos.writeShort(Short.reverseBytes(startSecurityBonus));
      dos.writeShort(Short.reverseBytes(startEspionageBonus));
      dos.writeShort(Short.reverseBytes(startSabotageBonus));
      dos.writeShort(Short.reverseBytes(startEconomicIntelBonus));
      dos.writeShort(Short.reverseBytes(startScienceIntelBonus));
      dos.writeShort(Short.reverseBytes(startMilitaryIntelBonus));
      dos.writeShort(Short.reverseBytes(startTradeIncomeBonus));

      dos.writeShort(Short.reverseBytes(startGroundCombatBonus));
      // treaty labels 292
      for (int v : treatyStatus) {
        dos.writeInt(Integer.reverseBytes(v));
      }
      // territory access 312
      for (int v : territoryAccess) {
        dos.writeInt(Integer.reverseBytes(v));
      }
      // trade access 332
      for (int v : tradeAllowed) {
        dos.writeInt(Integer.reverseBytes(v));
      }

      dos.writeInt(Integer.reverseBytes(numTradeRoutes));
      dos.writeInt(Integer.reverseBytes(unknown5));
      dos.writeInt(Integer.reverseBytes(trdeInfoAddr));

      // known race 352
      dos.writeLong(Long.reverseBytes(knownRacesMask));

      for (int i = 0; i < 8; i++) {
        for (int emp = 0; emp < NUM_EMPIRES; emp++) {
          dos.writeShort(Short.reverseBytes(prodQueue[emp][i]));
        }
      }

      dos.writeInt(Integer.reverseBytes(randomEventsMask));
      dos.writeInt(Integer.reverseBytes(totalFleetBuildCost));
      dos.writeInt(Integer.reverseBytes(creditsRaided));
    }
  }

}
