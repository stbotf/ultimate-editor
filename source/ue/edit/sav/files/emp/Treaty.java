package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.files.emp.data.TreatyEntry;
import ue.exception.InvalidArgumentsException;
import ue.util.calc.Calc;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// the treaty file lists all the established diplomatic agreements
public class Treaty extends InternalFile {

  // #region constants

  public interface TreatyType {
    public static final int Peace = 1;          // for major empires
    public static final int Alliance = 2;
    public static final int WarPact = 3;
    public static final int AcceptedDemand = 4;
    public static final int Friendship = 5;     // for minor races
    public static final int Affiliation = 6;    // for minor races
    public static final int Membership = 7;     // for minor races
    public static final int Gift = 8;
  }

  public interface PeaceType {
    public static final int NonAgression = 0;
    public static final int Friendship = 1;
    public static final int Affiliation = 2;
  }

  // #endregion constants

  // #region member classes

  public class TreatyStats {
    public HashMap<Integer, Integer> treatyTypes = new HashMap<Integer, Integer>(8);
    public HashMap<Integer, Integer> peaceTypes = new HashMap<Integer, Integer>(3);

    public TreatyStats() {
      // pre-fill known entries so they aren't missed
      treatyTypes.put(TreatyType.Peace, 0);
      treatyTypes.put(TreatyType.Alliance, 0);
      treatyTypes.put(TreatyType.WarPact, 0);
      treatyTypes.put(TreatyType.AcceptedDemand, 0);
      treatyTypes.put(TreatyType.Friendship, 0);
      treatyTypes.put(TreatyType.Affiliation, 0);
      treatyTypes.put(TreatyType.Membership, 0);
      treatyTypes.put(TreatyType.Gift, 0);
      peaceTypes.put(PeaceType.NonAgression, 0);
      peaceTypes.put(PeaceType.Friendship, 0);
      peaceTypes.put(PeaceType.Affiliation, 0);
    }
  }

  // #endregion member classes

  // #region data values

  @Getter private int numTreaties = 0;
  @Getter private int nextId = 0;
  private ArrayList<TreatyEntry> entries = new ArrayList<TreatyEntry>();

  // #endregion data values

  // #region constructor

  public Treaty(SavGame game) {
  }

  // #endregion constructor

  // #region getters

  public TreatyEntry getEntry(int treatyId) throws InvalidArgumentsException {
    if (treatyId < 0 || treatyId >= entries.size()) {
      throw new InvalidArgumentsException(
          "Invalid treaty index: %1 (no such entry in this file)"
              .replace("%1", Integer.toString(treatyId)));
    }

    return entries.get(treatyId);
  }

  // #endregion getters

  // #region public helpers

  public void removeRaceTreaties(short raceId) {
    boolean removed = entries.removeIf(x -> {
      return x.getInitiatorRaceId() == raceId
       || x.getRecipientRaceId() == raceId
       || x.getTargetRaceId() == raceId;
    });

    if (removed) {
      numTreaties = entries.size();
      markChanged();
    }
  }

  public TreatyStats getStats() {
    TreatyStats stats = new TreatyStats();

    for (TreatyEntry entry : entries) {
      int treatyType = entry.getTreatyType();
      stats.treatyTypes.merge(treatyType, 1, Calc::sum);
      if (treatyType == TreatyType.Peace) {
        int peaceType = entry.getPeaceType();
        stats.peaceTypes.merge(peaceType, 1, Calc::sum);
      }
    }

    return stats;
  }

  // #endregion public helpers

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    entries.clear();

    numTreaties = StreamTools.readInt(in, true);
    nextId = StreamTools.readInt(in, true);

    while (in.available() >= 64) {
      TreatyEntry entry = new TreatyEntry(this);
      entry.load(in);
      entries.add(entry);
    }

    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(numTreaties, true));
    out.write(DataTools.toByte(nextId, true));

    for (int i = 0; i < entries.size(); i++) {
      entries.get(i).save(out);
    }
  }

  // #endregion save

  // #region maintenance

  @Override
  public int getSize() {
    return 8 + entries.size() * 0x74;
  }

  @Override
  public void clear() {
    numTreaties = 0;
    nextId = 0;
    entries.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region check

  @Override
  public void check(Vector<String> response) {
    HashMap<Short, Integer> minorRaceTreaties = new HashMap<Short, Integer>();
    for (int treatyId = 0; treatyId < entries.size(); treatyId++) {
      TreatyEntry entry = entries.get(treatyId);
      entry.check(response);

      int treatyType = entry.getTreatyType();

      if (!isCreditsTreaty(treatyType)) {
        short initiatorRaceId = entry.getInitiatorRaceId();
        short recipientRaceId = entry.getRecipientRaceId();

        if (initiatorRaceId > 5 || recipientRaceId > 5) {
          short minorRaceId = initiatorRaceId > 5 ? initiatorRaceId : recipientRaceId;
          Integer prevTreatyType = minorRaceTreaties.putIfAbsent(minorRaceId, treatyType);

          if (prevTreatyType != null && isIncompatibleTreaty(prevTreatyType, treatyType)) {
            String msg = "The minor race %1 has two incompatible treaties %2 and %3.";
            msg = msg.replace("%1", Integer.toString(minorRaceId));
            msg = msg.replace("%2", Integer.toString(prevTreatyType));
            msg = msg.replace("%3", Integer.toString(treatyType));

            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        }
      }
    }
  }

  // #endregion check

  // #region private helpers

  private boolean isCreditsTreaty(int treatyType) {
    return treatyType == TreatyType.AcceptedDemand || treatyType == TreatyType.Gift;
  }

  private boolean isIncompatibleTreaty(int treatyType1, int treatyType2) {
    // exclusive treaties aren't compatible with any other treaties
    return isExclusiveTreaty(treatyType1) || isExclusiveTreaty(treatyType2);
  }

  private boolean isExclusiveTreaty(int treatyType) {
    return treatyType == TreatyType.Affiliation || treatyType == TreatyType.Membership;
  }

  // #endregion private helpers

}
