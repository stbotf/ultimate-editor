package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.exception.InvalidArgumentsException;
import ue.util.calc.Calc;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AlienInfo extends InternalFile {

  // #region constants

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_RACES = CStbof.NUM_RACES;

  public interface AlienRelationship {
    public static final int Neutral = 0;
    public static final int Friendship = 1;
    public static final int Affiliation = 2;
    public static final int Alliance = 3;
    public static final int Membership = Alliance;
    public static final int War = 4;
  }

  public interface RaceStatus {
    public static final int Independent = 0;
    public static final int Subjugated = 1;
    public static final int Member = 2;
    // observed '8' for the alive Cardassian AI empire in a corrupted UDML MP game
  }

  // #endregion constants

  // #region fields

  // one entry per each of the 35 BotF races
  private ArrayList<AlienInfoEntry> entries = new ArrayList<AlienInfoEntry>(NUM_RACES);

  private SavGame GAME;
  private Stbof stbof;

  // #endregion fields

  // #region constructor

  public AlienInfo(SavGame game, Stbof stbof) {
    this.GAME = game;
    this.stbof = stbof;
  }

  // #endregion constructor

  // #region property gets

  public boolean isRaceAlive(int raceID) {
    AlienInfoEntry entry = getEntry(raceID);
    return entry.alive != 0;
  }

  public int getRaceStatus(int raceID) {
    AlienInfoEntry entry = getEntry(raceID);
    return entry.status;
  }

  public short getHomeSystem(int raceID) {
    AlienInfoEntry entry = getEntry(raceID);
    return entry.starID;
  }

  public int getRace(short homeStarID) {
    for (int i = 0; i < entries.size(); i++) {
      AlienInfoEntry entry = entries.get(i);
      if (entry.starID == homeStarID)
        return i;
    }

    return -1;
  }

  /**
   * @param raceID    race entry to check
   * @param raceMetID race that was or wasn't met
   * @return true if the first race met the other one
   */
  public boolean raceMet(int raceID, int raceMetID) {
    AlienInfoEntry entry = getEntry(raceID);
    long mask = 1L << raceMetID;
    return (entry.known_races & mask) == mask;
  }

  public int getTreaty(int raceID, int empireID) throws InvalidArgumentsException {
    AlienInfoEntry entry = getEntry(raceID);

    if (empireID >= NUM_EMPIRES || empireID < 0) {
      String msg = "Invalid empire ID: %1".replace("%1", Integer.toString(empireID));
      throw new InvalidArgumentsException(msg);
    }

    return entry.treaty[empireID];
  }

  // #endregion property gets

  // #region property sets

  public void setRaceAlive(int raceID, boolean alive) {
    AlienInfoEntry entry = getEntry(raceID);
    boolean isAlive = entry.alive != 0;

    if (isAlive != alive) {
      entry.alive = (alive) ? (short) 1 : (short) 0;
      markChanged();
    }
  }

  public void setRaceStatus(int raceID, int status) throws InvalidArgumentsException {
    AlienInfoEntry entry = getEntry(raceID);

    if (entry.status == status)
      return;

    if (status != RaceStatus.Independent && status != RaceStatus.Subjugated && status != RaceStatus.Member) {
      String msg = "Invalid diplomacy value: %1".replace("%1", Integer.toString(status));
      throw new InvalidArgumentsException(msg);
    }

    entry.status = status;
    markChanged();
  }

  public void setHomeSystem(int raceID, short starID) {
    AlienInfoEntry entry = getEntry(raceID);
    if (entry.starID != starID) {
      entry.starID = starID;
      markChanged();
    }
  }

  /**
   * @param raceID    race entry to check
   * @param raceMetID race that was or wasn't met
   * @param met
   */
  public void setRaceMet(int raceID, int raceMetID, boolean met) {
    AlienInfoEntry entry = getEntry(raceID);
    long mask = 1L << raceMetID;
    if ((entry.known_races & mask) == mask)
      return;

    if (met)
      entry.known_races |= mask;
    else
      entry.known_races &= ~mask;

    markChanged();
  }

  public void setTreaty(int raceID, int empireID, int treaty) throws InvalidArgumentsException {
    AlienInfoEntry entry = getEntry(raceID);

    if (empireID >= NUM_EMPIRES || empireID < 0) {
      String msg = "Invalid empire ID: %1".replace("%1", Integer.toString(empireID));
      throw new InvalidArgumentsException(msg);
    }

    if (treaty < 0 || treaty >= 5) {
      String msg = "Invalid treaty type: %1".replace("%1", Integer.toString(treaty));
      throw new InvalidArgumentsException(msg);
    }

    if (entry.treaty[empireID] != treaty) {
      entry.treaty[empireID] = treaty;

      // for allied minors reset all other race relations
      if (raceID >= NUM_EMPIRES && (treaty == AlienRelationship.Affiliation
          || treaty == AlienRelationship.Membership)) {
        for (int emp = 0; emp < NUM_EMPIRES; emp++) {
          if (emp != empireID)
            entry.treaty[emp] = AlienRelationship.Neutral;
        }
      }

      markChanged();
    }
  }

  // #endregion property sets

  // #region helper routines

  public void eliminateRace(int raceId) {
    setRaceAlive(raceId, false);
    resetRaceTreaties(raceId);
  }

  public void resetRaceTreaties(int raceId) {
    AlienInfoEntry entry = getEntry(raceId);
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      if (entry.treaty[emp] != AlienRelationship.Neutral) {
        entry.treaty[emp] = AlienRelationship.Neutral;
        markChanged();
      }
    }
  }

  public void onRemovedSystem(short systemId) {
    entries.forEach(x -> {
      // update sytem ids
      if (x.starID > systemId) {
        --x.starID;
        markChanged();
      }
      else if (x.starID == systemId) {
        // mark dead / non-participating
        x.alive = 0;
        // zero non-participating race system
        x.starID = 0;
        markChanged();
      }
    });
  }

  // #endregion helper routines

  // #region stats

  public class AlienStats {
    public int racesTotal = 0;                // lists all available game races
    public int racesAlive = 0;                // races still participating
    public int[] relationships = new int[NUM_EMPIRES];
    public HashMap<Integer, Integer> status = new HashMap<Integer, Integer>();

    public AlienStats() {
      // pre-fill known entries so they aren't missed
      status.put(RaceStatus.Independent, 0);
      status.put(RaceStatus.Subjugated, 0);
      status.put(RaceStatus.Member, 0);
    }
  }

  public class EmpireAlienStats {
    public int[] relationships = new int[NUM_EMPIRES];
    public int membered = 0;
    public int subjugated = 0;
    public int met = 0;
  }

  public AlienStats getStats() {
    AlienStats stats = new AlienStats();
    stats.racesTotal = entries.size();

    for (AlienInfoEntry entry : entries) {
      if (entry.alive != 0) {
        stats.racesAlive++;
        for (int treaty : entry.treaty)
          stats.relationships[treaty]++;
        stats.status.merge(entry.status, 1, Calc::sum);
      }
    }

    return stats;
  }

  public EmpireAlienStats getEmpireStats(int empire) {
    EmpireAlienStats stats = new EmpireAlienStats();

    for (AlienInfoEntry entry : entries) {
      if (entry.alive != 0) {
        int treaty = entry.treaty[empire];
        stats.relationships[treaty]++;
        if (treaty == AlienRelationship.Membership) {
          if (entry.status == RaceStatus.Subjugated)
            stats.subjugated++;
          else
            stats.membered++;
        }

        if (0 != (entry.known_races & (1 << empire)))
          stats.met++;
      }
    }

    return stats;
  }

  public EmpireAlienStats[] getEmpireStats() {
    EmpireAlienStats[] stats = new EmpireAlienStats[NUM_EMPIRES];

    for (int empire = 0; empire < stats.length; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  // #endregion stats

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    entries.clear();
    while (in.available() >= AlienInfoEntry.SIZE) {
      entries.add(new AlienInfoEntry(in));
    }
    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < entries.size(); i++) {
      entries.get(i).save(out);
    }
  }

  // #endregion save

  // #region maintenance

  @Override
  public int getSize() {
    return entries.size() * AlienInfoEntry.SIZE;
  }

  @Override
  public void clear() {
    entries.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region check

  @Override
  public void check(Vector<String> response) {
    Optional<RaceRst> raceRst = stbof != null ? stbof.files().findRaceRst() : Optional.empty();
    val systInfo = GAME.files().findSystInfo();

    for (int raceId = 0; raceId < entries.size(); raceId++) {
      AlienInfoEntry entry = entries.get(raceId);
      val system = systInfo.isPresent() ? systInfo.get().getSystem(entry.starID) : null;

      if (system != null && entry.alive == 1
          && (entry.status == RaceStatus.Subjugated || entry.status == RaceStatus.Member)) {
        try {
          // in vanilla uninhabited systems have 0x23 set for the controlling race
          // therefore additionally check population
          int emp = system.getControllingRace();
          int pop = system.getPopulation();

          if (pop > 0 && (emp < 0 || emp > NUM_EMPIRES)) {
            String msg;
            if (raceRst.isPresent()) {
              msg = "The %1 are marked as being part of an empire, but aren't controlled by any. (fixed: marked as independant)"
                .replace("%1", raceRst.get().getName(raceId));
            } else {
              msg = "Race %1 is marked as being part of an empire, but isn't controlled by any. (fixed: marked as independant)"
                .replace("%1", Integer.toString(raceId));
            }

            response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
            entry.status = RaceStatus.Independent;
            markChanged();
          }
        } catch (Throwable ex) {
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ex.getLocalizedMessage()));
        }
      }

      for (int emp = 0; emp < entry.treaty.length; emp++) {
        long mask = 1L << emp;

        if (entry.treaty[emp] != AlienRelationship.Neutral) {
          if ((entry.known_races & mask) == 0) {
            String msg = "Not met race: %1 must have met %2 for their relationship. (fixed)";
            if (raceRst.isPresent()) {
              msg = msg.replace("%1", raceRst.get().getName(raceId));
              msg = msg.replace("%2", raceRst.get().getName(emp));
            } else {
              msg = msg.replace("%1", Integer.toString(raceId));
              msg = msg.replace("%2", Integer.toString(emp));
            }

            response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
            setRaceMet(raceId, emp, true);
          }

          if (raceId >= NUM_EMPIRES && entry.alive == 1 && entry.treaty[emp] == AlienRelationship.Membership) {
            // subjugation status precedences treaties
            if (entry.status != RaceStatus.Member) {
              try {
                String msg;
                if (raceRst.isPresent()) {
                  msg = "The %1 aren't marked as members of an empire, but have a membership treaty with the %2. (fixed: set treaty to affiliation)"
                    .replace("%1", raceRst.get().getName(raceId))
                    .replace("%2", raceRst.get().getName(emp));
                } else {
                  msg = "Race %1 isn't marked as a member of an empire, but has a membership treaty with race %2. (fixed: set treaty to affiliation)"
                    .replace("%1", Integer.toString(raceId))
                    .replace("%2", Integer.toString(emp));
                }

                response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
                setTreaty(raceId, emp, AlienInfo.AlienRelationship.Affiliation);
              } catch (InvalidArgumentsException ex) {
                response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ex.getLocalizedMessage()));
              }
            }

            // the actual system control precedences treaties
            if (system != null) {
              try {
                int ctrlRaceId = system.getControllingRace();
                if (ctrlRaceId != emp) {
                  try {
                    String msg;
                    if (raceRst.isPresent()) {
                      msg = "The %1 are marked as being members of the %2 empire yet their home system isn't under the empire's control. (fixed: set treaty to affiliation)"
                        .replace("%1", raceRst.get().getName(raceId))
                        .replace("%2", raceRst.get().getName(emp));
                    } else {
                      msg = "Race %1 is marked as being a member of empire %2 yet it's home system isn't under the empire's control. (fixed: set treaty to affiliation)"
                        .replace("%1", Integer.toString(raceId))
                        .replace("%2", Integer.toString(emp));
                    }

                    response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
                    setTreaty(raceId, emp, AlienInfo.AlienRelationship.Affiliation);
                  } catch (InvalidArgumentsException ex) {
                    response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR,
                        ex.getLocalizedMessage()));
                  }
                }
              } catch (Throwable ex) {
                response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ex.getLocalizedMessage()));
              }
            }
          }
        }
      }

      if (raceId >= NUM_EMPIRES) {
        int restricted = 0;

        for (int emp = 0; emp < entry.treaty.length; emp++) {
          if (entry.treaty[emp] == AlienRelationship.Affiliation
              || entry.treaty[emp] == AlienRelationship.Membership) {
            restricted++;
          }
        }

        if (restricted > 1) {
          try {
            String msg;
            if (raceRst.isPresent()) {
              msg = "The %1 have more than one exclusive relationship (affiliation or membership)"
                .replace("%1", raceRst.get().getName(raceId));
            } else {
              msg = "Race %1 has more than one exclusive relationship (affiliation or membership)"
                .replace("%1", Integer.toString(raceId));
            }

            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          } catch (Exception ex) {
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ex.getLocalizedMessage()));
          }
        }
      }

      // Check race alive status.
      // Check all systems since the home world pointed to by AlienInfo
      // e.g. can be extinct by the Borg but still point to the old system.
      if (systInfo.isPresent() && entry.alive != 0 && !systInfo.get().isRaceAlive(raceId)) {
        String msg;
        if (raceRst.isPresent()) {
          msg = "The %1 are marked to be alive, yet they populate no system. (ignored)"
            .replace("%1", raceRst.get().getName(raceId));
        } else {
          msg = "Race %1 is marked to be alive, yet it populates no system. (ignored)"
            .replace("%1", Integer.toString(raceId));
        }

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));

        // Note, if actually still alive, unsetting the alive status leads to following crash:
        // File: gdllist.c, Line: 534, gdlList != NULL
        setRaceAlive(raceId, false);
      }

      if (entry.status != RaceStatus.Member && entry.status != RaceStatus.Independent
          && entry.status != RaceStatus.Subjugated) {
        try {
          String msg;
          if (raceRst.isPresent()) {
            msg = "The %1 have an unknown diplomatic status: %2 (ignored)"
              .replace("%1", raceRst.get().getName(raceId))
              .replace("%2", Integer.toString(entry.status));
          } else {
            msg = "Race %1 has an unknown diplomatic status: %2 (ignored)"
              .replace("%1", Integer.toString(raceId))
              .replace("%2", Integer.toString(entry.status));
          }

          response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
        } catch (Exception ex) {
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ex.getLocalizedMessage()));
        }
      }
    }
  }

  // #endregion check

  // #region private helpers

  private AlienInfoEntry getEntry(int raceID) {
    if (raceID < 0 || raceID >= entries.size()) {
      throw new IndexOutOfBoundsException(
        "Invalid race index: %1 (no such entry in this file)"
          .replace("%1", Integer.toString(raceID)));
    }

    return entries.get(raceID);
  }

  // #endregion private helpers

  // #region private sub-classes

  private class AlienInfoEntry {

    public static final int SIZE = 64;

    short alive;                    // 0x00 - 0x01
    byte[] unknown1 = new byte[22]; // 0x02 - 0x17
    short starID;                   // 0x18 - 0x19
    byte[] unknown2 = new byte[6];  // 0x1A - 0x1F
    long known_races;               // 0x20 - 0x27
    int[] treaty = new int[5];      // 0x28 - 0x3B
    int status;                     // 0x3C - 0x40

    public AlienInfoEntry(InputStream in) throws IOException {
      alive = StreamTools.readShort(in, true);
      StreamTools.read(in, unknown1);
      starID = StreamTools.readShort(in, true);
      StreamTools.read(in, unknown2);
      known_races = StreamTools.readLong(in, true);

      for (int i = 0; i < treaty.length; i++) {
        treaty[i] = StreamTools.readInt(in, true);
      }

      status = StreamTools.readInt(in, true);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(alive, true));
      out.write(unknown1);
      out.write(DataTools.toByte(starID, true));
      out.write(unknown2);
      out.write(DataTools.toByte(known_races, true));

      for (int i = 0; i < treaty.length; i++) {
        out.write(DataTools.toByte(treaty[i], true));
      }

      out.write(DataTools.toByte(status, true));
    }
  }

  // #endregion private sub-classes

}
