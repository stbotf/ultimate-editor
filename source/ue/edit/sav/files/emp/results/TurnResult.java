package ue.edit.sav.files.emp.results;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.orders.Order;
import ue.util.stream.chunk.ChunkInputStream;
import ue.util.stream.chunk.ChunkOutputStream;

public abstract class TurnResult {

  static final String ClassName = TurnResult.class.getSimpleName();

  @Getter @Accessors(fluent = true)
  protected TurnResultHeader header;

  // sub entries
  @Getter protected Order order;
  @Getter protected GenericResult genericSubEntry;

  protected ResultList resultLst;

  public TurnResult(ResultList resultLst, TurnResultHeader header) {
    this.resultLst = resultLst;
    this.header = header;
  }

  public static TurnResult load(ResultList resultLst, ChunkInputStream in) throws IOException {
    TurnResultHeader header = new TurnResultHeader(resultLst, in);

    int remaining = in.available();
    if (remaining > 0) {
      throw new IOException(
          ClassName + "<" + in.currentChunk() + ">: Full entry header read, but still " + remaining
              + " bytes left!");
    }

    if (in.nextChunk() < 0) {
      throw new IOException(
          ClassName + "<" + in.currentChunk() + ">: Failed to read content data chunk!");
    }

    int dataSize = in.available();
    if (dataSize != header.getContentSize()) {
      System.out.println(ClassName + "<" + in.currentChunk() + ">: content size " + header.getContentSize()
          + " doesn't match the chunk data size " + dataSize + "!");
    }

    TurnResult result = null;
    switch (header.getContentSize()) {
      case EconomicResult.SIZE: {
        result = new EconomicResult(resultLst, header, in);
        break;
      }
      case IntelResult.SIZE: {
        result = new IntelResult(resultLst, header, in);
        break;
      }
      case MilitaryResult.SIZE: {
        result = new MilitaryResult(resultLst, header, in);
        break;
      }
      default: {
        result = new GenericTurnResult(resultLst, header, in);
        break;
      }
    }

    return result;
  }

  protected abstract void loadContent(InputStream out) throws IOException;

  public void loadSubEntry(ResultList resultLst, ChunkInputStream in) throws IOException {
    // load sub entry if any
    if (this.header.getSubAddress() != 0) {
      int remaining = in.available();
      if (remaining > 0) {
        throw new IOException(
            ClassName + "<" + in.currentChunk() + ">: Full entry content read, but still "
                + remaining + " bytes left!");
      }

      if (in.nextChunk() < 0) {
        throw new IOException(ClassName + "<" + in.currentChunk() + ">: Failed to read sub entry!");
      }

      remaining = in.available();
      switch (remaining) {
        case 0x20: {
          order = new Order(resultLst, in);
          break;
        }
        default: {
          genericSubEntry = new GenericResult(in);
        }
      }
    }
  }

  protected String descriptor(RaceRst raceRst, TaskForceList gtfList) {
    String hdr = this.header.descriptor(raceRst);
    String ord = this.order != null ? " with " + this.order.descriptor(gtfList) : "";
    return hdr + ord;
  }

  public abstract int getContentSize();

  public int getSize() {
    int size = TurnResultHeader.SIZE + getContentSize();
    if (order != null) {
      size += order.getSize();
    } else if (genericSubEntry != null) {
      size += genericSubEntry.getSize();
    }
    return size;
  }

  // ship id or -1 for military results
  public short getHighValue() {
    return (short) (this.header.getEventValue() >>> 16);
  }

  // -1 for military results
  public short getLowValue() {
    return (short) (this.header.getEventValue() & 0xFFFF);
  }

  protected void saveHeader(ChunkOutputStream out) throws IOException {
    this.header.setContentSize(getContentSize());
    this.header.save(out);
    out.nextChunk();
  }

  protected void saveSubEntry(ChunkOutputStream out) throws IOException {
    if (order != null) {
      out.nextChunk();
      order.save(out);
    } else if (genericSubEntry != null) {
      out.nextChunk();
      genericSubEntry.save(out);
    }
  }

  public void save(ChunkOutputStream out) throws IOException {
    saveHeader(out);
    writeContent(out);
    saveSubEntry(out);
  }

  protected abstract void writeContent(OutputStream out) throws IOException;

  public boolean check(InternalFile file, TaskForceList gtfList, GShipList shipList, RaceRst raceRst, Set<Short> removedTF, Vector<String> response) {
    if (this.genericSubEntry != null) {
      String msg = descriptor(raceRst, gtfList) + " has invalid generic sub entry. (ignored)";
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
    }
    return false;
  }
}
