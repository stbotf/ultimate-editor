package ue.edit.sav.files.emp.results;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.sav.files.emp.ResultList;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;
import ue.util.stream.chunk.ChunkInputStream;

// refer https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54174#p54174
@Getter
public class IntelResult extends TurnResult {

  public static final int SIZE = 0x74; // 116

  private int     eventId;      // 0x00 +   4 = 4   -> identifier for the specific intel event
  private byte    detected;     // 0x04 +   1 = 5   -> 0 = source detected, -1 = source unknown
  private byte    kind;         // 0x05 +   1 = 6   -> 1 = espionage, 2 = sabotage
  private short   flags;        // 0x06 +   2 = 8   -> unknown flags
  private short   targetRace;   // 0x08 +   2 = 10  -> the race id at which the intel attack has been directed
  private short   sourceRace;   // 0x0A +   2 = 12  -> wild guess: the race id by which the intel attack has been issued
  private int     eventType;    // 0x0C +   4 = 16  -> wild guess: 0=self/outgoing, 4=enemy/incoming
  private String  description;  // 0x10 + 100 = 116

  IntelResult(ResultList resultLst, TurnResultHeader header, ChunkInputStream in) throws IOException {
    super(resultLst, header);
    loadContent(in);
    loadSubEntry(resultLst, in);
  }

  @Override
  protected void loadContent(InputStream in) throws IOException {
    eventId = StreamTools.readInt(in, true);
    detected = (byte)in.read();
    kind = (byte)in.read();
    flags = StreamTools.readShort(in, true);
    targetRace = StreamTools.readShort(in, true);
    sourceRace = StreamTools.readShort(in, true);
    eventType = StreamTools.readInt(in, true);
    description = StreamTools.readNullTerminatedBotfString(in, 100);
  }

  @Override
  public int getContentSize() {
    return IntelResult.SIZE;
  }

  @Override
  public void writeContent(OutputStream out) throws IOException {
    out.write(DataTools.toByte(eventId, true));
    out.write(detected);
    out.write(kind);
    out.write(DataTools.toByte(flags, true));
    out.write(DataTools.toByte(targetRace, true));
    out.write(DataTools.toByte(sourceRace, true));
    out.write(DataTools.toByte(eventType, true));
    out.write(DataTools.toByte(description, 100, 99));
  }

}
