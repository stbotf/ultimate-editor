package ue.edit.sav.files.emp.results;

import java.io.IOException;
import java.io.InputStream;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;
import ue.util.stream.chunk.ChunkInputStream;
import ue.util.stream.chunk.ChunkOutputStream;

public class GenericResult {

  @Getter @Accessors(fluent = true) int resultId; // 4
  byte[] header;
  byte[] content;

  public GenericResult(ChunkInputStream in) throws IOException {
    readHeader(in);
    int remaining = in.available();
    if (remaining > 0) {
      String err = "GenericResult<" + in.currentChunk() + ">: Full entry header read,"
        + " but still " + remaining + " bytes left!";
      throw new IOException(err);
    }

    if (in.nextChunk() < 0) {
      String err = "GenericResult<" + in.currentChunk() + ">: Failed to read content data chunk!";
      throw new IOException(err);
    }

    int contentSize = in.available();
    readContent(in, contentSize);
  }

  private void readHeader(InputStream in) throws IOException {
    resultId = StreamTools.readInt(in, true);
    header = StreamTools.readAllBytes(in);
  }

  private void readContent(InputStream in, int size) throws IOException {
    content = StreamTools.readBytes(in, size);
  }

  public int getHeaderSize() {
    return header.length;
  }

  public int getSize() {
    return 4 + header.length + content.length;
  }

  public void save(ChunkOutputStream out) throws IOException {
    out.write(DataTools.toByte(resultId, true));
    out.write(header);
    out.nextChunk();
    out.write(content);
  }
}
