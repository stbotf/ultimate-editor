package ue.edit.sav.files.emp.results;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.emp.results.TurnResultHeader.ContentType;
import ue.edit.sav.files.emp.results.TurnResultHeader.EventFlags;
import ue.edit.sav.files.emp.results.TurnResultHeader.EventType;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.tools.MapTools;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;
import ue.util.stream.chunk.ChunkInputStream;

// For new ships, there are always two ResultContent9C entries:
// One for the completed ship, and one for the created task force.
// In addition there is a ResultContent10 entry.
@Getter
public class MilitaryResult extends TurnResult {

  public static final int SIZE = 0x9C; // 156

  private byte raceId;                    // 1 one byte race identifier (0:card, 1:fed, ...)
  private byte nbr;                       // 2 some counted number it seems, not necessarily unique
  private short unknown1;                 // 4
  private int unknown2;                   // 8
  private short taskForceId;              // 10 the task force id or -1
  private short shipId;                   // 12 the ship id or -1
  // sector index = row*columns + column-1, same like the outpost ids
  private short sectorIndex;              // 14 the computed sector index or -1
  private short systemId;                 // 16 the system id or -1
  private byte[] empty1 = new byte[28];   // 44
  private short targetRaceId;             // 46 e.g. the race id for the discovered race or -1
  private short unknown3;                 // 48
  private int sectorRow;                  // 52 the sector row this event is taking place or -1
  private int sectorColumn;               // 56 the sector column this event is taking place or -1
  private byte[] empty2 = new byte[8];    // 64
  private int unknown4;                   // 68
  private int unknown5;                   // 72
  private short unknown6;                 // 74
  private short empty3;                   // 76
  private String description;             // 116 00-terminated string, mostly unused but filled with memory garbage
  private String name;                    // 156 00-terminated string, remaining space filled with memory garbage

  MilitaryResult(ResultList resultLst, TurnResultHeader header, ChunkInputStream in) throws IOException {
    super(resultLst, header);
    loadContent(in);
    loadSubEntry(resultLst, in);
  }

  @Override
  public int getContentSize() {
    return MilitaryResult.SIZE;
  }

  public short getRaceId() {
    return (short) Byte.toUnsignedInt(raceId);
  }

  public boolean isTaskForceResult() {
    return taskForceId != -1;
  }

  public boolean isShipResult() {
    return shipId != -1;
  }

  public void setTaskForceId(short taskForceId) {
    if (this.taskForceId != taskForceId) {
      this.taskForceId = taskForceId;
      resultLst.markChanged();
    }
  }

  public void setShipId(short shipId) {
    if (this.shipId != shipId) {
      this.shipId = shipId;
      resultLst.markChanged();
    }
  }

  public void setSectorIndex(short sectorIndex) {
    if (this.sectorIndex != sectorIndex) {
      this.sectorIndex = sectorIndex;
      resultLst.markChanged();
    }
  }

  public void setSystemId(short systemId) {
    if (this.systemId != systemId) {
      this.systemId = systemId;
      resultLst.markChanged();
    }
  }

  public void setTargetRaceId(short targetRaceId) {
    if (this.targetRaceId != targetRaceId) {
      this.targetRaceId = targetRaceId;
      resultLst.markChanged();
    }
  }

  public void setSectorRow(int sectorRow) {
    if (this.sectorRow != sectorRow) {
      this.sectorRow = sectorRow;
      resultLst.markChanged();
    }
  }

  public void setSectorColumn(int sectorColumn) {
    if (this.sectorColumn != sectorColumn) {
      this.sectorColumn = sectorColumn;
      resultLst.markChanged();
    }
  }

  public void setDescription(String description) {
    if (!StringTools.equals(this.description, description)) {
      this.description = description;
      resultLst.markChanged();
    }
  }

  public void setName(String name) {
    if (!StringTools.equals(this.name, name)) {
      this.name = name;
      resultLst.markChanged();
    }
  }

  public boolean onRemovedSystem(short systemId) {
    // unset matching system ids
    if (this.systemId == systemId) {
      this.systemId = -1;
      resultLst.markChanged();
      return true;
    }
    // update successive system ids
    else if (this.systemId > systemId) {
      this.systemId--;
      resultLst.markChanged();
    }

    return false;
  }

  @Override
  protected void loadContent(InputStream in) throws IOException {
    raceId = (byte) in.read();
    nbr = (byte) in.read();
    unknown1 = StreamTools.readShort(in, true);
    unknown2 = StreamTools.readInt(in, true);
    taskForceId = StreamTools.readShort(in, true);
    shipId = StreamTools.readShort(in, true);
    sectorIndex = StreamTools.readShort(in, true);
    systemId = StreamTools.readShort(in, true);
    StreamTools.read(in, empty1);
    targetRaceId = StreamTools.readShort(in, true);
    unknown3 = StreamTools.readShort(in, true);
    sectorRow = StreamTools.readInt(in, true);
    sectorColumn = StreamTools.readInt(in, true);
    StreamTools.read(in, empty2);
    unknown4 = StreamTools.readInt(in, true);
    unknown5 = StreamTools.readInt(in, true);
    unknown6 = StreamTools.readShort(in, true);
    empty3 = StreamTools.readShort(in, true);
    description = StreamTools.readNullTerminatedBotfString(in, 40);
    name = StreamTools.readNullTerminatedBotfString(in, 40);
  }

  @Override
  protected void writeContent(OutputStream out) throws IOException {
    out.write(raceId);
    out.write(nbr);
    out.write(DataTools.toByte(unknown1, true));
    out.write(DataTools.toByte(unknown2, true));
    out.write(DataTools.toByte(taskForceId, true));
    out.write(DataTools.toByte(shipId, true));
    out.write(DataTools.toByte(sectorIndex, true));
    out.write(DataTools.toByte(systemId, true));
    out.write(empty1);
    out.write(DataTools.toByte(targetRaceId, true));
    out.write(DataTools.toByte(unknown3, true));
    out.write(DataTools.toByte(sectorRow, true));
    out.write(DataTools.toByte(sectorColumn, true));
    out.write(empty2);
    out.write(DataTools.toByte(unknown4, true));
    out.write(DataTools.toByte(unknown5, true));
    out.write(DataTools.toByte(unknown6, true));
    out.write(DataTools.toByte(empty3, true));
    out.write(DataTools.toByte(description, 40, 39));
    out.write(DataTools.toByte(name, 40, 39));
  }

  @Override
  public boolean check(InternalFile file, TaskForceList gtfList, GShipList shipList, RaceRst raceRst, Set<Short> removedTF, Vector<String> response) {
    boolean remove = super.check(file, gtfList, shipList, raceRst, removedTF, response);

    switch (header.getContentType()) {
      case ContentType.ShipEvent:
        // Check for broken shipId references.
        remove |= checkShipResult(file, gtfList, shipList, raceRst, response);
        break;
      case ContentType.TaskForceEvent:
      case ContentType.Terraformed:
        // Check for broken taskForceId references.
        remove |= checkTaskForceResult(file, gtfList, raceRst, removedTF, response);
        break;
    }
    return remove;
  }

  private String descriptor(RaceRst raceRst, TaskForceList gtfList, boolean withEntity) {
    String hdr = descriptor(raceRst, gtfList);
    String tf = "";
    String shp = "";
    if (withEntity) {
      if (taskForceId != -1)
        tf = " for task force " + Integer.toString(taskForceId);
      if (shipId != -1)
        shp = " for ship " + Integer.toString(shipId);
    }
    String sector = sectorRow > 0 ? " at " + MapTools.sectorPos(sectorColumn, sectorRow) : "";
    return hdr + tf + shp + sector;
  }

  private boolean checkShipResult(InternalFile file, TaskForceList gtfList, GShipList shipList, RaceRst raceRst, Vector<String> response) {
    if (shipId == -1) {
      return false;
    }

    boolean removed = header.getContentType() == ContentType.ShipEvent
      && (header.getEventType() == EventType.ShipDestroyed
      || (header.getFlags1() & EventFlags.UnitDissolved) != 0);

    boolean hasShip = shipList.hasShip(shipId);
    if (!removed && !hasShip) {
      String msg = descriptor(raceRst, gtfList, false) + " references ship %1, that is missing from %2. (ignored)";
      msg = msg.replace("%1", Integer.toString(shipId));
      msg = msg.replace("%2", CSavFiles.GShipList);
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
    }
    else if (removed && hasShip) {
      String msg = descriptor(raceRst, gtfList, false) + " removed ship %1, that is still listed by %2. (ignored)";
      msg = msg.replace("%1", Integer.toString(shipId));
      msg = msg.replace("%2", CSavFiles.GShipList);
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
    }

    return false;
  }

  // turnResult.type == TurnResult.Type.MilitaryResult
  private boolean checkTaskForceResult(InternalFile file, TaskForceList gtfList, RaceRst raceRst, Set<Short> removedTF, Vector<String> response) {
    boolean removed = removedTF.contains(taskForceId);
    boolean hasGtf = gtfList.hasTaskForce(taskForceId);
    if (!removed && !hasGtf) {
      String msg = descriptor(raceRst, gtfList, false) + " references task force %1, that is missing from %2. (removed)";
      msg = msg.replace("%1", Integer.toString(taskForceId));
      msg = msg.replace("%2", gtfList.getName());
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      return true;
    }
    else if (removed && hasGtf) {
      String msg = descriptor(raceRst, gtfList, false) + " removed task force %1, that is still listed by %2. (ignored)";
      msg = msg.replace("%1", Integer.toString(taskForceId));
      msg = msg.replace("%2", gtfList.getName());
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
    }

    return false;
  }

}
