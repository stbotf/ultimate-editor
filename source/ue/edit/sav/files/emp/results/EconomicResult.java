package ue.edit.sav.files.emp.results;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.sav.files.emp.ResultList;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;
import ue.util.stream.chunk.ChunkInputStream;

@Getter
public class EconomicResult extends TurnResult {

  public static final int SIZE = 0x10; // 16

  private int treatyId;       // 4 the entry id of the treaty file
  private short resultValue;  // 6 the event type or built ship id
  private short unknown1;     // 8
  // the systemId is only set for economic results with no ordInfo
  // for other results it may be 0 or -1
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54174#p54174
  private short systemId;     // 10
  private short unknown2;     // 12
  private int unknown3_type;  // 16

  EconomicResult(ResultList resultLst, TurnResultHeader header, ChunkInputStream in) throws IOException {
    super(resultLst, header);
    loadContent(in);
    loadSubEntry(resultLst, in);
  }

  public boolean onRemovedSystem(short systemId) {
    // from observation, the header system id is only set for economic results that have an ordInfo sub-entry
    // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54173#p54173
    if (order != null) {
      int headerSystem = header.getEventValue();

      // for economic results, the header event value is set to either the system id or -1
      if (headerSystem != -1) {
        // unset matching system ids
        if (headerSystem == systemId) {
          header.setEventValue(-1);
          return true;
        }
        // update successive system ids
        else if (headerSystem > systemId) {
          header.setEventValue(--headerSystem);
        }
      }

      // update attached order system ids
      order.onRemovedSystem(systemId);
    }
    // the content system id on the other hand is only set for economic results that have no sub-entry
    // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54174#p54174
    else if (genericSubEntry == null) {
      // unset matching system ids
      if (this.systemId == systemId) {
        this.systemId = -1;
        resultLst.markChanged();
        return true;
      }
      // update successive system ids
      else if (this.systemId > systemId) {
        this.systemId--;
        resultLst.markChanged();
      }
    }

    return false;
  }

  @Override
  protected void loadContent(InputStream in) throws IOException {
    treatyId = StreamTools.readInt(in, true);
    resultValue = StreamTools.readShort(in, true);
    unknown1 = StreamTools.readShort(in, true);
    systemId = StreamTools.readShort(in, true);
    unknown2 = StreamTools.readShort(in, true);
    unknown3_type = StreamTools.readInt(in, true);
  }

  @Override
  public int getContentSize() {
    return EconomicResult.SIZE;
  }

  @Override
  public void writeContent(OutputStream out) throws IOException {
    out.write(DataTools.toByte(treatyId, true));
    out.write(DataTools.toByte(resultValue, true));
    out.write(DataTools.toByte(unknown1, true));
    out.write(DataTools.toByte(systemId, true));
    out.write(DataTools.toByte(unknown2, true));
    out.write(DataTools.toByte(unknown3_type, true));
  }

}
