package ue.edit.sav.files.emp.results;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import com.google.common.io.BaseEncoding;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.UE.DebugFlags;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.files.emp.ResultList;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class TurnResultHeader {

  public static final int SIZE = 0x28; // 40

  public interface Type {

    public static final int EconomicResult = 0x100;     // size = 0x10 + [opt: 0x20 sub header + 0x34]
    public static final int Result2 = 0x200;            // size = 0x74
    public static final int MilitaryResult = 0x300;     // size = 0x9C + [opt: 0x20 sub header + 0x34]
    public static final int IntelResult = 0x400;        // size = 0x74
    public static final int DiplomaticResult = 0x500;   // size = 0x10 + 0x20 sub header + 0x48C

    public static String typeName(int type) {
      switch (type) {
        case EconomicResult:
          return "EconomicResult";
        case Result2:
          return "Result2";
        case MilitaryResult:
          return "MilitaryResult";
        case IntelResult:
          return "IntelResult";
        case DiplomaticResult:
          return "DiplomaticResult";
        default:
          return "Result " + Integer.toString(type);
      }
    }
  }

  public interface EventType {

    // economic results
    public static final byte Unknown01 = (byte) 0x01;
    public static final byte Unknown86 = (byte) 0x86;
    public static final byte Unknown87 = (byte) 0x87;
    public static final byte Unknown88 = (byte) 0x88;
    public static final byte Unknown89 = (byte) 0x89;
    public static final byte Unknown8A = (byte) 0x8A;
    public static final byte Unknown8B = (byte) 0x8B;
    public static final byte Unknown95 = (byte) 0x95;

    // military results
    public static final byte ShipDestroyed = (byte) 0x10;
    public static final byte ShipSysAttLoss = (byte) 0xD1;  // ship attack loss event, or what ever it is

    // intel results
    public static final byte Unknown62 = (byte) 0x62;

    // shared results
    public static final byte UnitRegroup = (byte) 0x00;     // military results: for dissolved task forces
    public static final byte UnitGained = (byte) 0x9D;      // military results
    public static final byte UnitLost = (byte) 0xB5;        // military results
    public static final byte UnitSpawned = (byte) 0xFB;     // e.g. for new ships, but seen for
    // military/result2/intel/diplomatic results
  }

  public interface EventFlags {

    // military results
    public static final short UnitDissolved = (short)0x8000;
  }

  public interface ContentType {

    public static final short None = 0x00;              // shared for economic, intel & result2 results

    // economic results
    public static final short EnergyShortage = 0x06;
    public static final short EnthusedTrading = 0x1F;   // happy trading happening
    public static final short Unk25 = 0x25;
    public static final short PlanetaryShift = 0x40;    // barren climate shift

    // military results
    public static final short TaskForceEvent = 0x04;    // movement, with task force id
    public static final short ShipEvent = 0x05;         // ships built or lost, with shipId
    public static final short Unk9 = 0x09;              // reached?, with sector index
    public static final short RaceDiscovery = 0x0A;
    public static final short SystemManeuver = 0x10;    // attack or protect, with systemId & task force id
    public static final short Terraformed = 0x13;       // with systemId & task force id
    public static final short Encounter = 0x24;         // multiple races involved

    // diplomatic results
    public static final short Encouragement = 0x14;     // encouraged sabotage
    public static final short Rejection = 0x15;         // rejected alliance or war pact
    public static final short Demand = 0x17;            // demand credits
    public static final short Gift = 0x19;              // sent credits

    public static String typeName(int type) {
      switch (type) {
        case EnergyShortage:
          return "EnergyShortage";
        case EnthusedTrading:
          return "EnthusedTrading";
        case Unk25:
          return "Unk25";
        case PlanetaryShift:
          return "PlanetaryShift";
        case TaskForceEvent:
          return "TaskForceEvent";
        case ShipEvent:
          return "ShipEvent";
        case Unk9:
          return "Unk9";
        case RaceDiscovery:
          return "RaceDiscovery";
        case SystemManeuver:
          return "SystemManeuver";
        case Terraformed:
          return "Terraformed";
        case Encounter:
          return "Encounter";
        case Encouragement:
          return "Encouragement";
        case Rejection:
          return "Rejection";
        case Demand:
          return "Demand";
        case Gift:
          return "Gift";
        default:
          return "Subject " + Integer.toString(type);
      }
    }
  }

  // resultId: an incremented unique identifier, starting at
  // different offsets for each of the major races:
  // card: 01 00 00 00 (little endian) = 1
  // fed: AA AA AA 2A (little endian) = max_uint * 1/6
  // ferg: 55 55 55 55 (little endian) = max_uint * 2/6
  // klng: FF FF FF 7F (little endian) = max_uint * 3/6
  // rom: AA AA AA AA (little endian) = max_uint * 4/6
  // max_uint (unsigned) = 0xFFFFFFFF = 4294967295 decimal
  @Accessors(fluent = true)
  private int resultId;         // 4
  private short raceId;         // 6 the race id (Card:0 Fed:1 Ferg:2 Kling:3 Rom:4 like usual)
  private short unknown1;       // 8 unknown, sometimes FF FF = -1
  private short type;           // 10 result type (system build, ship build,...)
  private short empty1;         // 12 always empty 00 from what I've seen
  private byte raceMask;        // 13 involved races (Card:01, Fed:02, Ferg:04, Kling:08, Rom:10)
  private short unknown2;       // 15
  private byte eventType;       // 16 likely some event type identifier, e.g. 10 -> lost ship
  private int eventValue;       // 20 system id or -1 for economic results, FF FF [shpId] for lost military ship results, -1 for other results
  private short flags1;         // 22 some event dependent flags it seems
  private short contentType;    // 24 event type telling on the content data
  private int flags2;           // 28 some event dependent flags it seems
  private int subAddress;       // 32 most likely the loaded memory address of the ordInfo sub-entry data
  private int contentAddress;   // 36 most likely the loaded memory address of the content data
  private int contentSize;      // 40 auto-calculated size of the subsequent content chunk

  protected ResultList resultLst;

  TurnResultHeader(ResultList resultLst, InputStream in) throws IOException {
    this.resultLst = resultLst;

    resultId = StreamTools.readInt(in, true);
    raceId = StreamTools.readShort(in, true);
    unknown1 = StreamTools.readShort(in, true);
    type = StreamTools.readShort(in, true);
    empty1 = StreamTools.readShort(in, true);
    raceMask = (byte) in.read();
    unknown2 = StreamTools.readShort(in, true);
    eventType = (byte) in.read();
    eventValue = StreamTools.readInt(in, true);
    flags1 = StreamTools.readShort(in, true);
    contentType = StreamTools.readShort(in, true);
    flags2 = StreamTools.readInt(in, true);
    subAddress = StreamTools.readInt(in, true);
    contentAddress = StreamTools.readInt(in, true);
    contentSize = StreamTools.readInt(in, true);

    if (UE.DEBUG_MODE && UE.DEBUG_FLAGS.contains(DebugFlags.TurnResult)) {
      val base16 = BaseEncoding.base16();
      System.out.println("== TurnResult ==");
      System.out.println("ResultID: " + base16.encode(DataTools.toByte(resultId, true)));
      System.out.println("RaceID: " + raceId);
      System.out.println("Unknown1: " + base16.encode(DataTools.toByte(unknown1, true)));
      System.out.println("Type: " + base16.encode(DataTools.toByte(type, true)));
      System.out.println("Empty1: " + base16.encode(DataTools.toByte(empty1, true)));
      System.out.println("RaceFlags: " + base16.encode(new byte[]{raceMask}));
      System.out.println("Unknown2: " + base16.encode(DataTools.toByte(unknown2, true)));
      System.out.println("EventType: " + base16.encode(new byte[]{eventType}));
      System.out.println("EventValue: " + base16.encode(DataTools.toByte(eventValue, true)));
      System.out.println("Flags1: " + base16.encode(DataTools.toByte(flags1, true)));
      System.out.println("ContentType: " + contentType);
      System.out.println("Flags2: " + base16.encode(DataTools.toByte(flags2, true)));
      System.out.println("SubAddress: " + base16.encode(DataTools.toByte(subAddress, true)));
      System.out.println("ContentAddress: " + base16.encode(DataTools.toByte(contentAddress, true)));
      System.out.println("ContentSize: " + contentSize);
    }
  }

  public String descriptor(RaceRst raceRst) {
    String race = raceRst.getOwnerName(this.raceId);
    String type = Type.typeName(this.type);
    String id = Integer.toString(this.resultId);
    String subject = " <" + ContentType.typeName(this.contentType) + ">";
    return race + " " + type + " " + id + subject;
  }

  public boolean isTaskForce() {
    return this.contentType == ContentType.TaskForceEvent;
  }

  public boolean isRemoved() {
    return this.eventType == EventType.UnitLost
      || (this.eventType == EventType.UnitRegroup && (this.flags1 & EventFlags.UnitDissolved) != 0);
  }

  public void setContentSize(int contentSize) {
    if (this.contentSize != contentSize) {
      this.contentSize = contentSize;
      resultLst.markChanged();
    }
  }

  public void setEventValue(int eventValue) {
    if (this.eventValue != eventValue) {
      this.eventValue = eventValue;
      resultLst.markChanged();
    }
  }

  void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(resultId, true));
    out.write(DataTools.toByte(raceId, true));
    out.write(DataTools.toByte(unknown1, true));
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(empty1, true));
    out.write(raceMask);
    out.write(DataTools.toByte(unknown2, true));
    out.write(eventType);
    out.write(DataTools.toByte(eventValue, true));
    out.write(DataTools.toByte(flags1, true));
    out.write(DataTools.toByte(contentType, true));
    out.write(DataTools.toByte(flags2, true));
    out.write(DataTools.toByte(subAddress, true));
    out.write(DataTools.toByte(contentAddress, true));
    out.write(DataTools.toByte(contentSize, true));
  }

}
