package ue.edit.sav.files.emp.results;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.sav.files.emp.ResultList;
import ue.util.stream.StreamTools;
import ue.util.stream.chunk.ChunkInputStream;

public class GenericTurnResult extends TurnResult {

  private byte[] unknownResult = null;

  GenericTurnResult(ResultList resultLst, TurnResultHeader header, ChunkInputStream in) throws IOException {
    super(resultLst, header);
    loadContent(in);
    loadSubEntry(resultLst, in);
  }

  @Override
  protected void loadContent(InputStream in) throws IOException {
    unknownResult = StreamTools.readBytes(in, header.getContentSize());
  }

  @Override
  public int getContentSize() {
    return unknownResult.length;
  }

  @Override
  protected void writeContent(OutputStream out) throws IOException {
    out.write(unknownResult);
  }
}
