package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeMap;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.CStbof.StructureGroup;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.sav.SavGame;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.CollectionTools;
import ue.util.stream.StreamTools;

public class StrcInfo extends InternalFile {

  public class StatRace {
    public static final int Cardassians = 0;
    public static final int Federation = 1;
    public static final int Ferengi = 2;
    public static final int Klingons = 3;
    public static final int Romulans = 4;
    public static final int Minors = 5;
    public static final int Shared = 6;
    public static final int None = 7;

    public static final int MajorMask = 0x1F;
  }

  public class StrcStatEntry {
    public String name;
    public int race;        // StatRace 0-4: majors, 5: minors, 6: shared
    public int count = 0;
    public int powered = 0;

    public StrcStatEntry(short count, short powered) {
      this.count = count;
      this.powered = powered;
    }

    public StrcStatEntry combineWith(StrcStatEntry entry) {
      this.count += entry.count;
      this.powered += entry.powered;
      return this;
    }
  }

  public class StrcStats {
    public int strcTotal = 0;
    public int strcMainFood = 0;
    public int strcMainIndustry = 0;
    public int strcMainEnergy = 0;
    public int strcMainIntel = 0;
    public int strcMainResearch = 0;
    public int strcScanners = 0;
    public int strcOther = 0;
    public int powered = 0;
    public HashMap<Short,StrcStatEntry> strcTypes = new HashMap<Short,StrcStatEntry>();
  }

  private class StarStrc {
    // List of all the structures of the system. Given that the entry count varies,
    // the hex values look messed anyhow, therefore skip sorting.
    private HashMap<Short, StructureEntry> structures = new HashMap<>();

    // Remember the load order to attempt to restore it on save.
    // This helps to reduce and analyse sneaking hex changes.
    ArrayList<Short> loadOrder = new ArrayList<>();
  }

  // sort structure listings by star id like they are stored by the game
  private TreeMap<Short, StarStrc> entries = new TreeMap<>();

  private SavGame sav = null;
  private Stbof STBOF = null;
  private Edifice EDIFICE = null;

  public StrcInfo(SavGame savGame, Stbof st) {
    sav = savGame;
    STBOF = st;
  }

  public StrcStats getStats() throws IOException {
    loadStatFiles();
    StrcStats stats = new StrcStats();

    for (val entry : entries.values()) {
      incrementStrcStats(stats, entry);
    }

    return stats;
  }

  public StrcStats getEmpireStats(int[] systemIds) throws IOException {
    loadStatFiles();
    StrcStats stats = new StrcStats();

    for (int systemId : systemIds) {
      val entry = entries.get((short)systemId);
      if (entry != null)
        incrementStrcStats(stats, entry);
    }

    return stats;
  }

  private void loadStatFiles() throws IOException {
    if (EDIFICE == null)
      EDIFICE = (Edifice) STBOF.getInternalFile(CStbofFiles.EdificeBst, true);
  }

  private void incrementStrcStats(StrcStats stats, StarStrc entry) {
    for (StructureEntry strc : entry.structures.values()) {
      stats.strcTotal += strc.count;
      stats.powered += strc.powered_count;

      Building bld = EDIFICE.building(strc.buildingID);
      switch (bld.getGroup()) {
        case StructureGroup.MainFood:
          stats.strcMainFood += strc.count;
          break;
        case StructureGroup.MainIndustry:
          stats.strcMainIndustry += strc.count;
          break;
        case StructureGroup.MainEnergy:
          stats.strcMainEnergy += strc.count;
          break;
        case StructureGroup.MainIntel:
          stats.strcMainIntel += strc.count;
          break;
        case StructureGroup.MainResearch:
          stats.strcMainResearch += strc.count;
          break;
        case StructureGroup.Scanners:
          stats.strcScanners += strc.count;
          break;
        case StructureGroup.Other:
        default:
          stats.strcOther += strc.count;
      }

      StrcStatEntry val = new StrcStatEntry(strc.count, strc.powered_count);
      StrcStatEntry existing = stats.strcTypes.merge(strc.buildingID, val, StrcStatEntry::combineWith);

      // init new entries
      if (existing == val) {
        val.name = bld.getPluralName();
        int majors = StatRace.MajorMask & (StatRace.MajorMask ^ bld.getExcludeEmpire());
        val.race = Integer.bitCount(majors) > 1
          ? StatRace.Shared
          : bld.getResidentRaceReq() == 0 ? StatRace.None
          : majors == 0x01 ? StatRace.Cardassians
          : majors == 0x02 ? StatRace.Federation
          : majors == 0x04 ? StatRace.Ferengi
          : majors == 0x08 ? StatRace.Klingons
          : majors == 0x10 ? StatRace.Romulans
          : StatRace.Minors;
      }
    }
  }

  public void addSystem(short systemId) {
    entries.putIfAbsent(systemId, new StarStrc());
    markChanged();
  }

  public int getCount(short systemId, short buildingID) {
    val starStrc = entries.get(systemId);

    if (starStrc != null) {
      StructureEntry entry = starStrc.structures.get(buildingID);

      if (entry != null)
        return entry.count;
    }

    return 0;
  }

  public int getPoweredOnCount(short systemId, short buildingID) {
    val starStrc = entries.get(systemId);

    if (starStrc != null) {
      StructureEntry entry = starStrc.structures.get(buildingID);

      if (entry != null)
        return entry.powered_count;
    }

    return 0;
  }

  public void setCount(short systemId, short buildingID, short count) {
    val starStrc = entries.get(systemId);

    if (starStrc != null) {
      StructureEntry entry = starStrc.structures.get(buildingID);

      if (entry != null) {
        if (entry.count != count) {
          if (count <= 0) {
            starStrc.structures.remove(buildingID);
          } else {
            entry.count = count;

            if (entry.powered_count > count) {
              entry.powered_count = count;
            }
          }

          markChanged();
        }
      } else {
        entry = new StructureEntry(count, buildingID, (short)0);
        starStrc.structures.put(buildingID, entry);
        markChanged();
      }
    } else {
      entries.put(systemId, new StarStrc());
      this.setCount(systemId, buildingID, count);
    }
  }

  public void setPoweredOnCount(short systemId, short buildingID, short count) {
    val starStrc = entries.get(systemId);

    if (starStrc != null) {
      StructureEntry entry = starStrc.structures.get(buildingID);

      if (entry != null) {
        count = (short) Math.min(count, entry.count);

        if (count != entry.powered_count) {
          entry.powered_count = count;
          markChanged();
        }
      }
    }
  }

  public void changeBuildingID(short systemId, short buildingID, short newID) {
    val starStrc = entries.get(systemId);

    if (starStrc != null) {
      StructureEntry entry = starStrc.structures.get(buildingID);

      if (entry != null) {
        StructureEntry entry2 = starStrc.structures.get(newID);

        starStrc.structures.remove(buildingID);

        if (entry2 != null) {
          entry2.combineWith(entry);
        } else {
          entry.buildingID = newID;
          starStrc.structures.put(newID, entry);
        }

        markChanged();
      }
    }
  }

  public void removeAllMainStructures() {
    // process structures of each sector
    entries.values().removeIf(starStrc -> {
      if (starStrc.structures.values().removeIf(strc -> { return strc.isMainBuilding();}))
        markChanged();
      return starStrc.structures.isEmpty();
    });
  }

  public void removeAllEnergyStructures() {
    // process structures of each sector
    entries.values().removeIf(starStrc -> {
      if (starStrc.structures.values().removeIf(strc -> { return strc.isEnergyBuilding();}))
        markChanged();
      return starStrc.structures.isEmpty();
    });
  }

  public void removeSystemEntries(short systemId) {
    StarStrc strc = entries.get(systemId);
    if (strc != null && !strc.structures.isEmpty()) {
      strc.structures.clear();
      markChanged();
    }
  }

  public void onRemovedSystem(short systemId) {
    if (entries.isEmpty() || entries.lastKey() < systemId)
      return;

    // update system ids
    TreeMap<Short, StarStrc> copy = new TreeMap<>();
    for (val entry : entries.entrySet()) {
      short key = entry.getKey();
      if (key == systemId)
        continue;

      copy.put(key < systemId ? key : --key, entry.getValue());
    }

    entries = copy;
    markChanged();
  }

  public ArrayList<Short> getStarsWithBuilding(short buildingID) {
    ArrayList<Short> ret = new ArrayList<Short>();
    for (val entry : entries.entrySet()) {
      if (entry.getValue().structures.containsKey(buildingID))
        ret.add(entry.getKey());
    }
    return ret;
  }

  @Override
  public void load(InputStream in) throws IOException {
    // systemId, buildingID
    entries.clear();

    while (in.available() > 0) {
      short systemId = StreamTools.readShort(in, true);
      short count = StreamTools.readShort(in, true);
      StarStrc starStrc = entries.get(systemId);
      if (starStrc == null) {
        starStrc = new StarStrc();
        entries.put(systemId, starStrc);
      }

      while (count > 0 && in.available() > 0) {
        StructureEntry entry = new StructureEntry(in);
        StructureEntry existing = starStrc.structures.get(entry.buildingID);

        if (existing != null) {
          // automatically resolve duplicate structure type entries
          existing.combineWith(entry);
          // log warning
          val dscHlp = sav.descHelper();
          val strcName = dscHlp.strcName(entry.buildingID, true);
          val sysName = dscHlp.systemName(systemId, true);
          val raceName = dscHlp.systemRace(systemId, true);
          String msg = Language.getString("StrcInfo.0")
            .replace("%1", strcName)
            .replace("%2", raceName)
            .replace("%3", sysName);
          System.out.println(msg);
          // remember load issues
          addWarning(msg);
        } else {
          starStrc.structures.put(entry.buildingID, entry);
          starStrc.loadOrder.add(entry.buildingID);
        }

        count--;
      }
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (val entry : entries.entrySet()) {
      val starStrc = entry.getValue();
      out.write(DataTools.toByte(entry.getKey(), true));
      out.write(DataTools.toByte((short) starStrc.structures.size(), true));

      for (short bldId : starStrc.loadOrder) {
        StructureEntry strc = starStrc.structures.get(bldId);
        if (strc != null)
          strc.save(out);
      }

      // create hash map to exclude already saved entries
      HashSet<Short> saved = new HashSet<>(starStrc.loadOrder);

      // save remaining orders
      for (val strc : starStrc.structures.entrySet()) {
        if (!saved.contains(strc.getKey()))
          strc.getValue().save(out);
      }
    }
  }

  @Override
  public void clear() {
    super.clear();
    entries.clear();
    markChanged();
  }

  @Override
  public int getSize() {
    Iterator<Short> systemIds = entries.keySet().iterator();
    int size = 0;

    while (systemIds.hasNext()) {
      size += 4;
      short systemId = systemIds.next();
      size += entries.get(systemId).structures.size() * 8;
    }

    return size;
  }

  public short[] getBuildings(short systemId) {
    val starStrc = entries.get(systemId);
    return starStrc == null ? new short[0]
      : CollectionTools.toShortArray(starStrc.structures.keySet());
  }

  private class StructureEntry {

    private short count;
    private short buildingID;
    private short powered_count;
    private short empty;

    public StructureEntry(short count, short buildingID, short powered) {
      this.count = count;
      this.buildingID = buildingID;
      this.powered_count = powered;
    }

    public boolean isMainBuilding() {
      Building bld = EDIFICE.building(buildingID);
      switch (bld.getGroup()) {
        case StructureGroup.MainFood:
        case StructureGroup.MainIndustry:
        case StructureGroup.MainEnergy:
        case StructureGroup.MainIntel:
        case StructureGroup.MainResearch:
          return true;
      }
      return false;
    }

    public boolean isEnergyBuilding() {
      Building bld = EDIFICE.building(buildingID);
      switch (bld.getGroup()) {
        case StructureGroup.Scanners:
        case StructureGroup.Other:
          return true;
      }
      return false;
    }

    StructureEntry(InputStream in) throws IOException {
      count = StreamTools.readShort(in, true);
      buildingID = StreamTools.readShort(in, true);
      powered_count = StreamTools.readShort(in, true);
      empty = StreamTools.readShort(in, true);
    }

    void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(count, true));
      out.write(DataTools.toByte(buildingID, true));
      out.write(DataTools.toByte(powered_count, true));
      out.write(DataTools.toByte(empty, true));
    }

    StructureEntry combineWith(StructureEntry entry) {
      count = (short) (count + entry.count);
      powered_count = (short) (powered_count + entry.powered_count);
      return this;
    }
  }

}
