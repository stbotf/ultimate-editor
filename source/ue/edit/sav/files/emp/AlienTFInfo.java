package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeSet;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AlienTFInfo extends InternalFile {

  public static final String FILE_NAME = CSavFiles.AlienTFInfo;

  SavGame game;
  Stbof stbof;
  private int raceId;

  // private int numTaskForces; // 4 auto-computed

  // An ordered set of the task force ids.
  // Sorting is not relevant here, but so does BotF,
  // and it reads a lot better when comparing the hex code.
  private TreeSet<Short> taskForceIds = new TreeSet<Short>(); // numTaskForces * 2 + 4

  public AlienTFInfo(int raceId, SavGame game, Stbof stbof) {
    this.raceId = raceId;
    this.game = game;
    this.stbof = stbof;
  }

  public String descriptor(RaceRst raceRst) {
    String race = raceRst.getOwnerName(this.raceId);
    return race + " TF info";
  }

  public short[] getFeetIds() {
    int size = taskForceIds.size();
    short[] ids = new short[size];

    int i = 0;
    for (short id : taskForceIds) {
      ids[i++] = id;
    }
    return ids;
  }

  public void setFeetIds(short[] ids) {
    // check for changes
    if (ids.length == taskForceIds.size()) {
      int i = 0;
      for (; i < ids.length; ++i) {
        if (!taskForceIds.contains(ids[i])) {
          break;
        }
      }

      if (i == ids.length) {
        return;
      }
    }

    taskForceIds.clear();
    for (int i = 0; i < ids.length; ++i) {
      taskForceIds.add(ids[i]);
    }
    markChanged();
  }

  public void addTaskForce(short taskForceId) {
    taskForceIds.add(taskForceId);
    markChanged();
  }

  public void removeTaskForce(short taskForceId) {
    taskForceIds.remove(taskForceId);
    markChanged();
  }

  public void removeAllTaskForces() {
    if (!taskForceIds.isEmpty()) {
      taskForceIds.clear();
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    taskForceIds.clear();
    int numTaskForces = StreamTools.readInt(in, true);

    int size = in.available();
    if (size != numTaskForces * 2)
      System.out.println(getName() + ": Task force number doesn't match the entry number!");

    size = Integer.min(size, numTaskForces * 2);
    for (; size > 0; size -= 2) {
      taskForceIds.add(StreamTools.readShort(in, true));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(taskForceIds.size(), true));
    for (short tfId : taskForceIds) {
      out.write(DataTools.toByte(tfId, true));
    }
  }

  @Override
  public void clear() {
    taskForceIds.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // alienTFInfo lists initial turn task forces, therefore refer GTForceList and ignore player re-groupings.
    val gtfList = game.files().findGtfList();
    if (!gtfList.isPresent()) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR,
          "Missing " + CSavFiles.GTForceList + "!"));
      return;
    }

    RaceRst raceRst = (RaceRst) stbof.tryGetInternalFile(CStbofFiles.RaceRst, true);

    boolean any = taskForceIds.removeIf(tfId -> {
      boolean hasGtf = gtfList.get().hasTaskForce(tfId);
      if (!hasGtf) {
        String msg = descriptor(raceRst) + " lists task force id %1, that is missing from %2.";
        msg = msg.replace("%1", Integer.toString(tfId));
        msg = msg.replace("%2", CSavFiles.GTForceList);

        if (!hasGtf) {
          msg += " (removed)";
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
          return true;
        } else {
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      }
      return false;
    });

    if (any) {
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return taskForceIds.size() * 2 + 4;
  }
}
