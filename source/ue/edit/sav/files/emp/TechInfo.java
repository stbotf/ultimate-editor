package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.Vector;
import lombok.val;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.files.tec.data.TechEntry;
import ue.edit.sav.SavGame;
import ue.exception.KeyNotFoundException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class TechInfo extends InternalFile {

  public class TechLevelStats {
    public short min = 0;
    public short max = 0;
    public short average = 0;
  }

  public class TechStats {
    //biotech, computer, construction, energy, propulsion, weapons, sociology[not used]
    public TechLevelStats[] majorTechLevels = new TechLevelStats[7];
    public TechLevelStats[] minorTechLevels = new TechLevelStats[7];
  }

  public class EmpireTechStats {
    //biotech, computer, construction, energy, propulsion, weapons, sociology[not used]
    public short[] techLevels = new short[CStbof.NUM_TECH_FIELDS];
  }

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_MINORS = 30;
  private static final int NUM_RACES = NUM_EMPIRES + NUM_MINORS;

  private EmpireAssignEntry[] empire_research_assignation = new EmpireAssignEntry[NUM_EMPIRES];
  private TreeMap<Short, MinorEntry> minor_entry = new TreeMap<Short, MinorEntry>();
  private TreeMap<Short, TechEntry> entries = new TreeMap<Short, TechEntry>();
  private int bldMaskLongs = TechEntry.DEFAULT_BldMaskLongs;

  public TechInfo(SavGame game) {
  }

  @Override
  public void load(InputStream in) throws IOException {
    entries.clear();
    minor_entry.clear();

    short racesInGame = StreamTools.readShort(in, true);

    // skip entry index
    StreamTools.skip(in, NUM_RACES * Short.BYTES);

    // read empire research assignments / points distribution
    for (int i = 0; i < empire_research_assignation.length; i++) {
      empire_research_assignation[i] = new EmpireAssignEntry(in);
    }

    // read minor race entries
    LinkedList<MinorEntry> minors = new LinkedList<MinorEntry>();
    for (int i = 0; i < racesInGame - NUM_EMPIRES; i++) {
      MinorEntry minor = new MinorEntry(in);
      minors.add(minor);
    }

    // calculate entry size by dividing the remaining data
    int entrySize = in.available() / racesInGame;
    // calculate the building ignore mask size
    bldMaskLongs = TechEntry.calcBuildMaskLongs(entrySize);

    // read tech entries
    while (in.available() > 0) {
      TechEntry entry = new TechEntry(this, in, bldMaskLongs);
      entries.put(entry.raceId(), entry);

      // determine MinorEntry race ids
      if (entry.raceId() > 4) {
        MinorEntry min = minors.pollFirst();
        if (min != null)
          minor_entry.put(entry.raceId(), min);
      }
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    short num = (short) Math.min(NUM_RACES, entries.size());
    num = (short) Math.min(num, minor_entry.size() + NUM_EMPIRES);

    out.write(DataTools.toByte(num, true));

    // write ids
    short entry_idx = 0;
    for (short i = 0; i < NUM_RACES; i++) {
      short idx = entries.containsKey(i) ? entry_idx++ : 0;
      out.write(DataTools.toByte(idx, true));
    }

    // write empire research assignments / points distribution
    for (int i = 0; i < empire_research_assignation.length; i++) {
      empire_research_assignation[i].save(out);
    }

    // write sorted minor race entries
    for (MinorEntry minor : minor_entry.values()) {
      minor.save(out);
    }

    // write sorted tech entries
    for (TechEntry entry : entries.values()) {
      // force to write the configured size and zero or cut the rest
      entry.save(out, bldMaskLongs);
    }
  }

  @Override
  public void clear() {
    entries.clear();
    minor_entry.clear();
    Arrays.fill(empire_research_assignation, null);
    markChanged();
  }

  public TechStats getStats() {
    TechStats stats = new TechStats();

    for (TechEntry entry : entries.values()) {
      if (entry.raceId() < NUM_EMPIRES) {
        for (int i = 0; i < CStbof.NUM_TECH_FIELDS; ++i) {
          TechLevelStats lvl = stats.majorTechLevels[i];
          short techLvl = entry.getTechLvl(i);
          if (lvl == null) {
            lvl = new TechLevelStats();
            lvl.min = techLvl;
            lvl.max = techLvl;
            lvl.average = techLvl;
            stats.majorTechLevels[i] = lvl;
          }
          else {
            lvl.min = (short) Math.min(lvl.min, techLvl);
            lvl.max = (short) Math.max(lvl.max, techLvl);
            lvl.average += techLvl;
          }
        }
      }
      else {
        for (int i = 0; i < CStbof.NUM_TECH_FIELDS; ++i) {
          TechLevelStats lvl = stats.minorTechLevels[i];
          short techLvl = entry.getTechLvl(i);
          if (lvl == null) {
            lvl = new TechLevelStats();
            lvl.min = techLvl;
            lvl.max = techLvl;
            lvl.average = techLvl;
            stats.minorTechLevels[i] = lvl;
          }
          else {
            lvl.min = (short) Math.min(lvl.min, techLvl);
            lvl.max = (short) Math.max(lvl.max, techLvl);
            lvl.average += techLvl;
          }
        }
      }
    }

    for (TechLevelStats lvl : stats.majorTechLevels) {
      if (lvl != null)
        lvl.average = (short) (lvl.average / NUM_EMPIRES);
    }
    for (TechLevelStats lvl : stats.minorTechLevels) {
      if (lvl != null)
        lvl.average = (short) (lvl.average / (entries.size() - NUM_EMPIRES));
    }

    return stats;
  }

  public EmpireTechStats getEmpireStats(short empire) {
    EmpireTechStats stats = new EmpireTechStats();
    TechEntry entry = entries.get(empire);

    for (int i = 0; i < CStbof.NUM_TECH_FIELDS; ++i)
      stats.techLevels[i] = entry.getTechLvl(i);

    return stats;
  }

  public EmpireTechStats[] getEmpireStats() {
    EmpireTechStats[] stats = new EmpireTechStats[NUM_EMPIRES];

    for (short empire = 0; empire < stats.length; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  public short getTechLevel(short raceID, int techField) throws KeyNotFoundException {
    if (techField < 0 || techField >= 7) {
      String msg = Language.getString("TechInfo.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(techField)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }

    TechEntry entry = getTechEntry(raceID);
    if (entry == null) {
      String msg = Language.getString("TechInfo.1") //$NON-NLS-1$
        .replace("%1", Integer.toString(raceID)); //$NON-NLS-1$
      throw new KeyNotFoundException(msg);
    }

    return entry.getTechLvl(techField);
  }

  public TechEntry getTechEntry(short raceID) {
    return entries.get(raceID);
  }

  public void removeMinorRace(short raceId) {
    if (raceId >= NUM_EMPIRES) {
      boolean removed = entries.remove(raceId) != null;
      removed |= minor_entry.remove(raceId) != null;
      if (removed)
        markChanged();
    }
  }

  @Override
  public void check(Vector<String> response) {
    Stbof stbof = UE.FILES.stbof();
    if (stbof != null) {
      val edifice = stbof.files().findEdifice().orElse(null);
      if (edifice != null) {
        int reqBldMaskLongs = edifice.getReqBldMaskLongs();
        if (reqBldMaskLongs > bldMaskLongs) {
          int numBuildings = edifice.getNumberOfEntries();
          int maxBuildings = reqBldMaskLongs * Long.BYTES * 8;
          String msg = Language.getString("TechInfo.0");
          msg = msg.replace("%1", Integer.toString(numBuildings));
          msg = msg.replace("%2", Integer.toString(maxBuildings));
          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        }
      }
    }
  }

  @Override
  public int getSize() {
    int size = 2; // num
    size += NUM_RACES * 2;
    size += empire_research_assignation.length * 20;
    size += minor_entry.size() * 8;
    size += entries.size() * 108;

    return size;
  }

  private class EmpireAssignEntry {

    byte[] unknown1 = new byte[6];
    short[] assigned = new short[6];
    byte[] unknown2 = new byte[2];

    public EmpireAssignEntry(InputStream in) throws IOException {
      StreamTools.read(in, unknown1);
      for (int i = 0; i < assigned.length; i++) {
        assigned[i] = StreamTools.readShort(in, true);
      }
      StreamTools.read(in, unknown2);
    }

    public void save(OutputStream out) throws IOException {
      out.write(unknown1);
      for (int i = 0; i < assigned.length; i++) {
        out.write(DataTools.toByte(assigned[i], true));
      }
      out.write(unknown2);
    }
  }

  private class MinorEntry {

    short currentEvolutionLevel;
    short advancementChance;      // next turn in %
    short defaultChance;          // multiplier
    short bonus;                  // bonus/turn

    public MinorEntry(InputStream in) throws IOException {
      currentEvolutionLevel = StreamTools.readShort(in, true);
      advancementChance = StreamTools.readShort(in, true);
      defaultChance = StreamTools.readShort(in, true);
      bonus = StreamTools.readShort(in, true);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(currentEvolutionLevel, true));
      out.write(DataTools.toByte(advancementChance, true));
      out.write(DataTools.toByte(defaultChance, true));
      out.write(DataTools.toByte(bonus, true));
    }
  }
}
