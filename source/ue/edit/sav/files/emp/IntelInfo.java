package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class IntelInfo extends InternalFile {

  public class IntelStats {
    public int security = 0;          // last turn security output
    public int espionage = 0;         // last turn espionage output
    public int sabotage = 0;          // last turn sabotage output
    public int averageSecurity = 0;   // last 20 turn average security output
    public int averageEspionage = 0;  // last 20 turn average espionage output
    public int averageSabotage = 0;   // last 20 turn average sabotage output
    public int reportCount = 0;
  }

  public class EmpireIntelStats {
    public int security = 0;                    // last turn security output
    public int espionageImpact = 0;             // last turn sabotage impact by other empires
    public int sabotageImpact = 0;              // last turn sabotage impact by other empires
    public int[] espionage = new int[5];        // last turn espionage output
    public int[] sabotage = new int[5];         // last turn sabotage output
    public int averageSecurity = 0;             // last 20 turn average security output
    public int averageEspionageImpact = 0;      // last 20 turn average espionage impact by other empires
    public int averageSabotageImpact = 0;       // last 20 turn average sabotage impact by other empires
    public int[] averageEspionage = new int[5]; // last 20 turn average espionage output
    public int[] averageSabotage = new int[5];  // last 20 turn average sabotage output
    public int reportCount = 0;
  }

  // total size = 0xDF64
  private byte[] unknown1 = new byte[88];                               // 0x0000 + 0x0058
  // 0x1C might be own empire security assignment
  private EmpireStatsReport[] statsReports = new EmpireStatsReport[5];  // 0x0058 + 0x0280 = 5 * 0x080
  private EmpireIntelEntry[] empireEntries = new EmpireIntelEntry[5];   // 0x02D8 + 0xDC8C = 5 * 0x2C1C

  public IntelInfo(SavGame game) {
  }

  @Override
  public void load(InputStream in) throws IOException {
    StreamTools.read(in, unknown1);
    for (int i = 0; i < statsReports.length; i++)
      statsReports[i] = new EmpireStatsReport(in);
    for (int i = 0; i < empireEntries.length; i++)
      empireEntries[i] = new EmpireIntelEntry(in);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(unknown1);
    for (EmpireStatsReport statsReport : statsReports)
      statsReport.save(out);
    for (EmpireIntelEntry entry : empireEntries)
      entry.save(out);
  }

  @Override
  public void clear() {
    Arrays.fill(unknown1, (byte)0);
    Arrays.fill(statsReports, null);
    Arrays.fill(empireEntries, null);
    markChanged();
  }

  public IntelStats getStats() {
    IntelStats stats = new IntelStats();
    int maxTurnCount = determineLastTurn();

    for (EmpireIntelEntry entry : empireEntries) {
      // sum effective outputs
      stats.security += entry.getEffectiveLastTurnSecurityOutput();
      stats.espionage += entry.getEffectiveLastTurnEspionageOutput();
      stats.sabotage += entry.getEffectiveLastTurnSabotageOutput();
      stats.averageSecurity += entry.getEffectiveAverageSecurityOutput(maxTurnCount);
      stats.averageEspionage += entry.getEffectiveAverageEspionageOutput(maxTurnCount);
      stats.averageSabotage += entry.getEffectiveAverageSabotageOutput(maxTurnCount);
      stats.reportCount += entry.reportCount;
    }

    return stats;
  }

  public EmpireIntelStats getEmpireStats(int empire) {
    EmpireIntelStats stats = new EmpireIntelStats();
    int maxTurns = determineLastTurn();

    for (int i = 0; i < empireEntries.length; ++i) {
      EmpireIntelEntry empEntry = empireEntries[i];

      if (i == empire) {
        stats.security = empEntry.securityProgress.getLastTurnOutput();
        stats.averageSecurity = empEntry.securityProgress.getAverageOutput(maxTurns);
        for (int r = 0; r < 4; ++r) {
          // skip own empire, which is not listed for espionage and sabotage
          int idx = r < empire ? r : r+1;
          stats.espionage[idx] = empEntry.espionageProgress[r].getLastTurnOutput();
          stats.sabotage[idx] = empEntry.sabotageProgress[r].getLastTurnOutput();
          stats.averageEspionage[idx] = empEntry.espionageProgress[r].getAverageOutput(maxTurns);
          stats.averageSabotage[idx] = empEntry.sabotageProgress[r].getAverageOutput(maxTurns);
        }
        stats.reportCount = empEntry.reportCount;
      }
      else {
        // determine intel impacts by other empires
        // skip own empire, which is not listed for espionage and sabotage
        int empireIdx = empire < i ? empire : empire-1;
        // sum effective outputs
        stats.espionageImpact += Math.max(0, empEntry.espionageProgress[empireIdx].getLastTurnOutput());
        stats.sabotageImpact += Math.max(0, empEntry.sabotageProgress[empireIdx].getLastTurnOutput());
        stats.averageEspionageImpact += Math.max(0, empEntry.espionageProgress[empireIdx].getAverageOutput(maxTurns));
        stats.averageSabotageImpact += Math.max(0, empEntry.sabotageProgress[empireIdx].getAverageOutput(maxTurns));
      }
    }

    return stats;
  }

  public EmpireIntelStats[] getEmpireStats() {
    EmpireIntelStats[] stats = new EmpireIntelStats[5];

    for (int empire = 0; empire < stats.length; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return 0x58 + statsReports.length * 0x80 + empireEntries.length * 0x2C1C;
  }

  private int determineLastTurn() {
    int lastTurn = 0;
    for (EmpireStatsReport report : statsReports)
      lastTurn = Math.max(lastTurn, report.determineLastTurn());
    return lastTurn;
  }

  public int getEffectiveAverageSecurityOutput(int empireId) {
    int maxTurnCount = determineLastTurn();
    return empireEntries[empireId].getEffectiveAverageSecurityOutput(maxTurnCount);
  }

  public int getEffectiveAverageEspionageOutput(int empireId) {
    int maxTurnCount = determineLastTurn();
    return empireEntries[empireId].getEffectiveAverageEspionageOutput(maxTurnCount);
  }

  public int getEffectiveAverageSabotageOutput(int empireId) {
    int maxTurnCount = determineLastTurn();
    return empireEntries[empireId].getEffectiveAverageSabotageOutput(maxTurnCount);
  }

  private class StatsReportEntry {

    // total size = 0x20
    int[] empTurn = new int[5];       // 0x00 + 0x14: turn number of last update (card, fed, ferg, klng, rom)
    short[] empStats = new short[5];  // 0x14 + 0x0A: last known empire stats (card, fed, ferg, klng, rom)
    short empty;                      // 0x1E + 0x02

    public StatsReportEntry(InputStream in) throws IOException {
      for (int i = 0; i < empTurn.length; i++)
        empTurn[i] = StreamTools.readInt(in, true);
      for (int i = 0; i < empStats.length; i++)
        empStats[i] = StreamTools.readShort(in, true);
      empty = StreamTools.readShort(in, true);
    }

    public void save(OutputStream out) throws IOException {
      for (int i = 0; i < empTurn.length; i++)
        out.write(DataTools.toByte(empTurn[i], true));
      for (int i = 0; i < empStats.length; i++)
        out.write(DataTools.toByte(empStats[i], true));
      out.write(DataTools.toByte(empty, true));
    }

    public int determineLastTurn() {
      int lastTurn = 0;
      for (int turn : empTurn)
        lastTurn = Math.max(lastTurn,  turn);
      return lastTurn;
    }
  }

  private class EmpireStatsReport {

    // total size = 0x80
    StatsReportEntry economyReport;   // 0x00 + 0x20 verified by economic spionage
    StatsReportEntry reserachReport;  // 0x20 + 0x20
    StatsReportEntry militaryReport;  // 0x40 + 0x20
    StatsReportEntry moraleReport;    // 0x60 + 0x20 0 = normal, verified by alliance

    public EmpireStatsReport(InputStream in) throws IOException {
      economyReport = new StatsReportEntry(in);
      reserachReport = new StatsReportEntry(in);
      militaryReport = new StatsReportEntry(in);
      moraleReport = new StatsReportEntry(in);
    }

    public void save(OutputStream out) throws IOException {
      economyReport.save(out);
      reserachReport.save(out);
      militaryReport.save(out);
      moraleReport.save(out);
    }

    private int determineLastTurn() {
      int lastTurn = 0;
      lastTurn = Math.max(lastTurn,  economyReport.determineLastTurn());
      lastTurn = Math.max(lastTurn,  reserachReport.determineLastTurn());
      lastTurn = Math.max(lastTurn,  militaryReport.determineLastTurn());
      lastTurn = Math.max(lastTurn,  moraleReport.determineLastTurn());
      return lastTurn;
    }
  }

  public interface IntelDepartment {
    public static final int General = 0;
    public static final int Economy = 1;
    public static final int Science = 2;
    public static final int Military = 3;
  }

  private class IntelProgress {

    // total size = 0x58
    int lastAttackTurn;   // 0x00 + 0x04: turn number of last intel attack (unless security)
    int progress;         // 0x04 + 0x04: stockpile points for attack threshold, re-set to 0 if attack (unless security)
    int[] outputHistory = new int[20];  // 0x08 + 0x50: area output of the last 20 turns, can be negative

    public IntelProgress(InputStream in) throws IOException {
      lastAttackTurn = StreamTools.readInt(in, true);
      progress = StreamTools.readInt(in, true);

      for (int i = 0; i < outputHistory.length; i++)
        outputHistory[i] = StreamTools.readInt(in, true);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(lastAttackTurn, true));
      out.write(DataTools.toByte(progress, true));

      for (int i = 0; i < outputHistory.length; i++)
        out.write(DataTools.toByte(outputHistory[i], true));
    }

    public int getLastTurnOutput() {
      return outputHistory[0];
    }

    public int getAverageOutput(int maxTurns) {
      maxTurns = Math.min(maxTurns, outputHistory.length);

      int output = 0;
      for (int i = 0; i < maxTurns; i++)
        output += outputHistory[i];
      return output / outputHistory.length;
    }
  }

  private class IntelReport {

    // total size = 0x100
    int index;                            // 0x00 + 0x04: index value of report
    int turn;                             // 0x04 + 0x04: turn number of event
    byte sourceRaceId;                    // 0x08 + 0x01: blamed ? race ID or -1
    byte targetRaceId;                    // 0x09 + 0x01: opponent race ID ?
    byte[] empty1 = new byte[2];          // 0x0A + 0x02: empty data alignment
    int reportListIndex;                  // 0x0C + 0x04: index for report list?
    byte[] reportDetails = new byte[240]; // 0x10 + 0xF0: report text & unknown details

    public IntelReport(InputStream in) throws IOException {
      index = StreamTools.readInt(in, true);
      turn = StreamTools.readInt(in, true);
      sourceRaceId = (byte)in.read();
      targetRaceId = (byte)in.read();
      StreamTools.read(in, empty1);
      reportListIndex = StreamTools.readInt(in, true);
      StreamTools.read(in, reportDetails);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(index, true));
      out.write(DataTools.toByte(turn, true));
      out.write(sourceRaceId);
      out.write(targetRaceId);
      out.write(empty1);
      out.write(DataTools.toByte(reportListIndex, true));
      out.write(reportDetails);
    }
  }

  private class EmpireIntelEntry {

    // total size = 0x2C1C
    int allAreaOutputIndex;                             // 0x0000 + 0x0004: index value for all areas output (loops +1/turn 0-13h)
    int[] espionageAttackThreshold = new int[5];        // 0x0004 + 0x0014: base for espionage attack threshold
    int[] sabotageAttackThreshold = new int[5];         // 0x0018 + 0x0014: base for sabotage attack threshold
    int[] moraleDefenseModifier = new int[8];           // 0x002C + 0x0020: security defense modifier%
    int[] treatyAttackThresholdModifier = new int[4];   // 0x004C + 0x0010: attack threshold modifier%
    int empty1;                                         // 0x005C + 0x0004
    int[] empireDepartmentChoices = new int[4];         // 0x0060 + 0x0010: IntelDepartment selections
    int securityAssignment;                             // 0x0070 + 0x0004: security percentage assignation
    int[] espionageAssignments = new int[4];            // 0x0074 + 0x0010: espionage percentage assignations
    int[] sabotageAssignments = new int[4];             // 0x0084 + 0x0010: sabotage percentage assignations
    byte[] unknown1 = new byte[16];                     // 0x0094 + 0x0010
    IntelProgress securityProgress;                     // 0x00A4 + 0x0058
    IntelProgress[] espionageProgress = new IntelProgress[4]; // 0x00FC + 0x0160 = 4 enemies * 0x58
    IntelProgress[] sabotageProgress = new IntelProgress[4];  // 0x025C + 0x0160 = 4 enemies * 0x58
    int[] trainingStatus = new int[16];                 // 0x03BC + 0x0040: 4 enemies * 4 departments agents training status 1-1Eh (+/- 2 turn)
    int reportEndIndex;                                 // 0x03FC + 0x0004: index for reports 41+ (loops 0-27h) -> overwrite/clear old pointer ?
    int reportNextIndex;                                // 0x0400 + 0x0004: index for next intel report (loops 0-27h)
    int reportCount;                                    // 0x0404 + 0x0004: number of intel reports (28h max.)
    IntelReport[] reports = new IntelReport[40];        // 0x0408 + 0x2800 = 40 * 0x100
    int[] lastAttackTurn = new int[5];                  // 0x2C08 + 0x0014: turn number of last intel attack?

    public EmpireIntelEntry(InputStream in) throws IOException {
      allAreaOutputIndex = StreamTools.readInt(in, true);

      for (int i = 0; i < espionageAttackThreshold.length; i++)
        espionageAttackThreshold[i] = StreamTools.readInt(in, true);
      for (int i = 0; i < sabotageAttackThreshold.length; i++)
        sabotageAttackThreshold[i] = StreamTools.readInt(in, true);
      for (int i = 0; i < moraleDefenseModifier.length; i++)
        moraleDefenseModifier[i] = StreamTools.readInt(in, true);
      for (int i = 0; i < treatyAttackThresholdModifier.length; i++)
        treatyAttackThresholdModifier[i] = StreamTools.readInt(in, true);

      empty1 = StreamTools.readInt(in, true);

      for (int i = 0; i < empireDepartmentChoices.length; i++)
        empireDepartmentChoices[i] = StreamTools.readInt(in, true);

      securityAssignment = StreamTools.readInt(in, true);
      for (int i = 0; i < espionageAssignments.length; i++)
        espionageAssignments[i] = StreamTools.readInt(in, true);
      for (int i = 0; i < sabotageAssignments.length; i++)
        sabotageAssignments[i] = StreamTools.readInt(in, true);

      StreamTools.read(in, unknown1);

      securityProgress = new IntelProgress(in);
      for (int i = 0; i < espionageProgress.length; i++)
        espionageProgress[i] = new IntelProgress(in);
      for (int i = 0; i < sabotageProgress.length; i++)
        sabotageProgress[i] = new IntelProgress(in);
      for (int i = 0; i < trainingStatus.length; i++)
        trainingStatus[i] = StreamTools.readInt(in, true);

      reportEndIndex = StreamTools.readInt(in, true);
      reportNextIndex = StreamTools.readInt(in, true);
      reportCount = StreamTools.readInt(in, true);
      for (int i = 0; i < reports.length; i++)
        reports[i] = new IntelReport(in);

      for (int i = 0; i < lastAttackTurn.length; i++)
        lastAttackTurn[i] = StreamTools.readInt(in, true);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(allAreaOutputIndex, true));

      for (int i = 0; i < espionageAttackThreshold.length; i++)
        out.write(DataTools.toByte(espionageAttackThreshold[i], true));
      for (int i = 0; i < sabotageAttackThreshold.length; i++)
        out.write(DataTools.toByte(sabotageAttackThreshold[i], true));
      for (int i = 0; i < moraleDefenseModifier.length; i++)
        out.write(DataTools.toByte(moraleDefenseModifier[i], true));
      for (int i = 0; i < treatyAttackThresholdModifier.length; i++)
        out.write(DataTools.toByte(treatyAttackThresholdModifier[i], true));

      out.write(DataTools.toByte(empty1, true));

      for (int i = 0; i < empireDepartmentChoices.length; i++)
        out.write(DataTools.toByte(empireDepartmentChoices[i], true));

      out.write(DataTools.toByte(securityAssignment, true));
      for (int i = 0; i < espionageAssignments.length; i++)
        out.write(DataTools.toByte(espionageAssignments[i], true));
      for (int i = 0; i < sabotageAssignments.length; i++)
        out.write(DataTools.toByte(sabotageAssignments[i], true));

      out.write(unknown1);

      securityProgress.save(out);
      for (IntelProgress p : espionageProgress)
        p.save(out);
      for (IntelProgress p : sabotageProgress)
        p.save(out);
      for (int i = 0; i < trainingStatus.length; i++)
        out.write(DataTools.toByte(trainingStatus[i], true));

      out.write(DataTools.toByte(reportEndIndex, true));
      out.write(DataTools.toByte(reportNextIndex, true));
      out.write(DataTools.toByte(reportCount, true));
      for (IntelReport r : reports)
        r.save(out);

      for (int i = 0; i < lastAttackTurn.length; i++)
        out.write(DataTools.toByte(lastAttackTurn[i], true));
    }

    public int getEffectiveLastTurnSecurityOutput() {
      return Math.max(0, securityProgress.outputHistory[0]);
    }

    public int getEffectiveLastTurnEspionageOutput() {
      int total = 0;
      for (int i = 0; i < espionageProgress.length; i++) {
        // empire wise output can be negative
        total += Math.max(0, espionageProgress[i].outputHistory[0]);
      }
      return total;
    }

    public int getEffectiveLastTurnSabotageOutput() {
      int total = 0;
      for (int i = 0; i < sabotageProgress.length; i++) {
        // empire wise output can be negative
        total += Math.max(0, sabotageProgress[i].outputHistory[0]);
      }
      return total;
    }

    public int getEffectiveAverageSecurityOutput(int maxTurns) {
      return Math.max(0, securityProgress.getAverageOutput(maxTurns));
    }

    public int getEffectiveAverageEspionageOutput(int maxTurns) {
      int total = 0;
      for (int i = 0; i < espionageProgress.length; i++) {
        // empire wise output can be negative
        total += Math.max(0, espionageProgress[i].getAverageOutput(maxTurns));
      }
      return total;
    }

    public int getEffectiveAverageSabotageOutput(int maxTurns) {
      int total = 0;
      for (int i = 0; i < sabotageProgress.length; i++) {
        // empire wise output can be negative
        total += Math.max(0, sabotageProgress[i].getAverageOutput(maxTurns));
      }
      return total;
    }
  }
}
