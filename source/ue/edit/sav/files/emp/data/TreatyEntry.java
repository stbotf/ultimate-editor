package ue.edit.sav.files.emp.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.Checkable;
import ue.edit.common.InternalFile;
import ue.edit.sav.files.emp.Treaty;
import ue.edit.sav.files.emp.Treaty.PeaceType;
import ue.edit.sav.files.emp.Treaty.TreatyType;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

public class TreatyEntry {

  private Treaty treaty;

  // contact race name if player initiated
  @Getter private String name;                  // 0x00 + 0x28

  // delay: 1 if waiting on player response, two majors involved, or for gifts to take effect
  @Getter private int delay;                    // 0x28 + 0x04 (guessed)
  @Getter private int treatyId;                 // 0x2C + 0x04
  // @see Treaty.TreatyType (1:peace 2:alliance 3:war pact 4:accepted demand 5:friendship 6:affiliation 7:membership 8:gift)
  @Getter private int treatyType;               // 0x30 + 0x04
  @Getter private short initiatorRaceId;        // 0x34 + 0x02
  @Getter private short recipientRaceId;        // 0x36 + 0x02

  // turns: -1 if unlimited
  @Getter private byte turns;                   // 0x38 + 0x01
  @Getter private byte[] empty1 = new byte[3];  // 0x39 + 0x03 (data alignment)
  // first active turn
  @Getter private int startTurn;                // 0x3C + 0x04
  // last active turn
  @Getter private int endTurn;                  // 0x40 + 0x04

  // hasGift: 0 or 1
  @Getter private int hasGift;                  // 0x44 + 0x04
  // hasDemand: 0 or 1
  @Getter private int hasDemand;                // 0x48 + 0x04
  // hasRepeatedGift: 0 or 1
  @Getter private int hasRepeatedGift;          // 0x4C + 0x04
  // hasRepeatedDemand: 0 or 1
  @Getter private int hasRepeatedDemand;        // 0x50 + 0x04

  // one time credits gift
  @Getter private int creditsGift;              // 0x54 + 0x04
  // one time credits demand
  @Getter private int creditsDemand;            // 0x58 + 0x04
  @Getter private int cedeDisputedTerritories;  // 0x5C + 0x04
  @Getter private int unknown;                  // 0x60 + 0x04

  // peaceType: for major empires -> 0:non-aggression 1:friendship 2:affiliation
  @Getter private int peaceType;                // 0x64 + 0x04
  // declareWar: 0 or 1
  @Getter private int declareWar;               // 0x68 + 0x04 (guessed)
  // declareWarAsWell: 0 or 1
  @Getter private int declareWarAsWell;         // 0x6C + 0x04 (guessed)
  @Getter private short targetRaceId;           // 0x70 + 0x02
  @Getter private short empty2;                 // 0x72 + 0x02 (data alignment)

  public TreatyEntry(Treaty treaty) {
    this.treaty = treaty;
  }

  public void load(InputStream in) throws IOException {
    name = StreamTools.readNullTerminatedBotfString(in, 40);

    delay = StreamTools.readInt(in, true);
    treatyId = StreamTools.readInt(in, true);
    treatyType = StreamTools.readInt(in, true);
    initiatorRaceId = StreamTools.readShort(in, true);
    recipientRaceId = StreamTools.readShort(in, true);
    turns = (byte)in.read();
    StreamTools.read(in, empty1);
    startTurn = StreamTools.readInt(in, true);
    endTurn = StreamTools.readInt(in, true);
    hasGift = StreamTools.readInt(in, true);
    hasDemand = StreamTools.readInt(in, true);
    hasRepeatedGift = StreamTools.readInt(in, true);
    hasRepeatedDemand = StreamTools.readInt(in, true);
    creditsGift = StreamTools.readInt(in, true);
    creditsDemand = StreamTools.readInt(in, true);
    cedeDisputedTerritories = StreamTools.readInt(in, true);
    unknown = StreamTools.readInt(in, true);
    peaceType = StreamTools.readInt(in, true);
    declareWar = StreamTools.readInt(in, true);
    declareWarAsWell = StreamTools.readInt(in, true);
    targetRaceId = StreamTools.readShort(in, true);
    empty2 = StreamTools.readShort(in, true);
  }

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(name, 40, 39));
    out.write(DataTools.toByte(delay, true));
    out.write(DataTools.toByte(treatyId, true));
    out.write(DataTools.toByte(treatyType, true));
    out.write(DataTools.toByte(initiatorRaceId, true));
    out.write(DataTools.toByte(recipientRaceId, true));
    out.write(turns);
    out.write(empty1);
    out.write(DataTools.toByte(startTurn, true));
    out.write(DataTools.toByte(endTurn, true));
    out.write(DataTools.toByte(hasGift, true));
    out.write(DataTools.toByte(hasDemand, true));
    out.write(DataTools.toByte(hasRepeatedGift, true));
    out.write(DataTools.toByte(hasRepeatedDemand, true));
    out.write(DataTools.toByte(creditsGift, true));
    out.write(DataTools.toByte(creditsDemand, true));
    out.write(DataTools.toByte(cedeDisputedTerritories, true));
    out.write(DataTools.toByte(unknown, true));
    out.write(DataTools.toByte(peaceType, true));
    out.write(DataTools.toByte(declareWar, true));
    out.write(DataTools.toByte(declareWarAsWell, true));
    out.write(DataTools.toByte(targetRaceId, true));
    out.write(DataTools.toByte(empty2, true));
  }

  public void setName(String name) {
    if (!StringTools.equals(this.name, name)) {
      this.name = name;
      treaty.markChanged();
    }
  }

  public void setDelay(int delay) {
    if (this.delay != delay) {
      this.delay = delay;
      treaty.markChanged();
    }
  }

  public void setTreatyType(int treatyType) {
    if (this.treatyType != treatyType) {
      this.treatyType = treatyType;
      treaty.markChanged();
    }
  }

  public void setInitiatorRaceId(short raceId) {
    if (this.initiatorRaceId != raceId) {
      this.initiatorRaceId = raceId;
      treaty.markChanged();
    }
  }

  public void setRecipientRaceId(short raceId) {
    if (this.recipientRaceId != raceId) {
      this.recipientRaceId = raceId;
      treaty.markChanged();
    }
  }

  public void setTurns(byte turns) {
    if (this.turns != turns) {
      this.turns = turns;
      treaty.markChanged();
    }
  }

  public void setStartTurn(int turn) {
    if (this.startTurn != turn) {
      this.startTurn = turn;
      treaty.markChanged();
    }
  }

  public void setEndTurn(int turn) {
    if (this.endTurn != turn) {
      this.endTurn = turn;
      treaty.markChanged();
    }
  }

  public void setGift(int gift) {
    int isGift = gift != 0 ? 1 : 0;
    if (this.creditsGift != gift || this.hasGift != isGift) {
      this.creditsGift = gift;
      this.hasGift = isGift;
      this.hasRepeatedGift = 0;
      treaty.markChanged();
    }
  }

  public void setDemand(int demand) {
    int isDemand = demand != 0 ? 1 : 0;
    if (this.creditsDemand != demand || this.hasDemand != isDemand) {
      this.creditsDemand = demand;
      this.hasDemand = isDemand;
      this.hasRepeatedDemand = 0;
      treaty.markChanged();
    }
  }

  public void setRepeatedGift(int gift) {
    int isGift = gift != 0 ? 1 : 0;
    if (this.creditsGift != gift || this.hasRepeatedGift != isGift) {
      this.creditsGift = gift;
      this.hasRepeatedGift = isGift;
      this.hasGift = 0;
      treaty.markChanged();
    }
  }

  public void setRepeatedDemand(int demand) {
    int isDemand = demand != 0 ? 1 : 0;
    if (this.creditsDemand != demand || this.hasRepeatedDemand != isDemand) {
      this.creditsDemand = demand;
      this.hasRepeatedDemand = isDemand;
      this.hasDemand = 0;
      treaty.markChanged();
    }
  }

  public void setCedeDisputedTerritories(boolean cede) {
    int cedeVal = cede ? 1 : 0;
    if (this.cedeDisputedTerritories != cedeVal) {
      this.cedeDisputedTerritories = cedeVal;
      treaty.markChanged();
    }
  }

  public void setPeaceType(int peaceType) {
    if (this.peaceType != peaceType) {
      this.peaceType = peaceType;
      treaty.markChanged();
    }
  }

  public void setDeclareWar(int declareWar) {
    if (this.declareWar != declareWar) {
      this.declareWar = declareWar;
      treaty.markChanged();
    }
  }

  public void setDeclareWarAsWell(int declareWarAsWell) {
    if (this.declareWarAsWell != declareWarAsWell) {
      this.declareWarAsWell = declareWarAsWell;
      treaty.markChanged();
    }
  }

  public void setTargetRaceId(short raceId) {
    if (this.targetRaceId != raceId) {
      this.targetRaceId = raceId;
      treaty.markChanged();
    }
  }

  public void check(Vector<String> response) {
    if (treatyType < TreatyType.Peace || treatyType > TreatyType.Gift) {
      String msg = "The races %1 and %2 have an unknown treaty type: %3";
      msg = msg.replace("%1", Integer.toString(initiatorRaceId));
      msg = msg.replace("%2", Integer.toString(recipientRaceId));
      msg = msg.replace("%3", Integer.toString(treatyType));

      response.add(Checkable.getCheckIntegrityString(treaty.getName(), InternalFile.INTEGRITY_CHECK_WARNING, msg));
    }

    if (peaceType < PeaceType.NonAgression || peaceType > PeaceType.Affiliation) {
      String msg = "The races %1 and %2 have an unknown peace type: %3";
      msg = msg.replace("%1", Integer.toString(initiatorRaceId));
      msg = msg.replace("%2", Integer.toString(recipientRaceId));
      msg = msg.replace("%3", Integer.toString(peaceType));

      response.add(Checkable.getCheckIntegrityString(treaty.getName(), InternalFile.INTEGRITY_CHECK_WARNING, msg));
    }

    if (peaceType != 0 && treatyType != TreatyType.Peace) {
      String msg = "The races %1 and %2 have no peace treaty, but still the peace type is set.";
      msg = msg.replace("%1", Integer.toString(initiatorRaceId));
      msg = msg.replace("%2", Integer.toString(recipientRaceId));

      response.add(Checkable.getCheckIntegrityString(treaty.getName(), InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    if (turns > 0 && startTurn > endTurn) {
      String msg = "The races %1 and %2 have a peace treaty that ends before it even starts.";
      msg = msg.replace("%1", Integer.toString(initiatorRaceId));
      msg = msg.replace("%2", Integer.toString(recipientRaceId));

      response.add(Checkable.getCheckIntegrityString(treaty.getName(), InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    if (treatyType == TreatyType.WarPact && targetRaceId == (short)-1) {
      String msg = "The races %1 and %2 have a war pact but no target race is set.";
      msg = msg.replace("%1", Integer.toString(initiatorRaceId));
      msg = msg.replace("%2", Integer.toString(recipientRaceId));

      response.add(Checkable.getCheckIntegrityString(treaty.getName(), InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    if (initiatorRaceId > 5 && recipientRaceId > 5) {
      String msg = "The two minor races %1 and %2 have a treaty. Yet the game doesn't support this.";
      msg = msg.replace("%1", Integer.toString(initiatorRaceId));
      msg = msg.replace("%2", Integer.toString(recipientRaceId));

      response.add(Checkable.getCheckIntegrityString(treaty.getName(), InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }
}
