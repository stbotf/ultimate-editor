package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;
import com.google.common.collect.HashMultimap;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.emp.results.EconomicResult;
import ue.edit.sav.files.emp.results.GenericResult;
import ue.edit.sav.files.emp.results.IntelResult;
import ue.edit.sav.files.emp.results.MilitaryResult;
import ue.edit.sav.files.emp.results.TurnResult;
import ue.edit.sav.files.emp.results.TurnResultHeader;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.orders.Order;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;
import ue.util.stream.chunk.ChunkInputStream;
import ue.util.stream.chunk.ChunkOutputStream;
import ue.util.stream.chunk.ChunkReader;
import ue.util.stream.chunk.ChunkWriter;

public class ResultList extends InternalFile {

  public class ResultStats {
    public int resultsTotal = 0;
  }

  // No need to sort the entries by id. By the differing entry sizes
  // they looked pretty messed in hex code anyhow.
  private HashMap<Integer, ResultEntry> results = new HashMap<Integer, ResultEntry>();

  // Keep a second index on the task force and ship results.
  // Attention, other than with the MilitaryOrders, there might more
  // than one MilitaryResult per ship or task force!
  HashMultimap<Short, ResultEntry> tfResults = HashMultimap.create();
  HashMultimap<Short, ResultEntry> shpResults = HashMultimap.create();

  // Like with OrderInfo, to restore original sorting, cache the loaded index order.
  // This helps to reduce and analyse sneaking hex changes.
  ArrayList<Integer> loadOrder = new ArrayList<Integer>();

  private SavGame game;
  private Stbof stbof;

  /**
   * result.lst is stored in a special format, containing a series of data entries.
   * While other files contain lists as well, this one uses the LZSS compression algorithm to create the list.
   */
  public ResultList(SavGame game, Stbof stbof, boolean isChunked) {
    this.game = game;
    this.stbof = stbof;
    this.isChunked = isChunked;
  }

  @Override
  public void load(InputStream in) throws IOException {
    if (in instanceof ChunkInputStream) {
      loadChunkStream((ChunkInputStream) in);
    } else {
      ChunkReader cr = new ChunkReader(in);
      cr.header().setHeaderInfo(false, false);
      loadChunkStream(cr);
    }
  }

  private void loadChunkStream(ChunkInputStream in) throws IOException {
    clear();
    markSaved();

    if (in.nextChunk() < 0)
      return;

    int entryNum = StreamTools.readInt(in, true);

    // read entries, each of them introduced by a header chunk
    while (in.nextChunk() > 0) {
      // parse entry data
      ResultEntry entry = new ResultEntry(this, in);
      int resultId = entry.resultId();
      if (results.putIfAbsent(resultId, entry) != null) {
        String title = "Result duplicate detected!";
        String msg = "There is more than one result with same id %1 detected.\nPlease report!"
          .replace("%1", Integer.toString(resultId));
        JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
      } else {
        loadOrder.add(resultId);
      }

      MilitaryResult mil = entry.militaryResult();
      if (mil != null) {
        if (mil.isTaskForceResult())
          tfResults.put(mil.getTaskForceId(), entry);
        if (mil.isShipResult())
          shpResults.put(mil.getShipId(), entry);
      }

      int remaining = in.available();
      if (remaining > 0) {
        throw new IOException("ResultList<" + in.currentChunk()
            + ">: Full entry read, but still " + remaining + " bytes left!");
      }
    }

    if (entryNum != results.size()) {
      System.out.println(
          "ResultList: " + results.size() + " entries found, but " + entryNum + " expected!");
    }
  }

  @Override
  public int getSize() {
    // the chunk headers for trek.exe is part of the save algorithm
    // and therefore not counted for the file data size

    // include first entry count chunk
    int size = 4;
    // then sum all the entry sizes
    for (ResultEntry entry : results.values()) {
      size += entry.getSize();
    }
    return size;
  }

  public ResultStats getStats() {
    ResultStats stats = new ResultStats();
    stats.resultsTotal = results.size();
    return stats;
  }

  public ResultStats getEmpireStats(int empire) {
    ResultStats stats = new ResultStats();

    for (ResultEntry entry : results.values()) {
      if (entry.turnResult == null)
        continue;

      int raceIdx = Integer.min(entry.turnResult.header().getRaceId(), 5);
      if (raceIdx == empire)
        stats.resultsTotal++;
    }

    return stats;
  }

  public ResultStats[] getEmpireStats() {
    ResultStats[] stats = new ResultStats[6];

    for (int empire = 0; empire < stats.length; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  public List<ResultEntry> listSystemResults(short systemId) {
    return results.values().stream()
      .filter(x -> x.isSystemResult(systemId))
      .collect(Collectors.toList());
  }

  public List<ResultEntry> listSectorResults(int sectorIndex) throws IOException {
    Sector sectorLst = game.files().sectorLst();
    return results.values().stream()
      .filter(x -> x.isSectorResult(sectorLst, sectorIndex))
      .collect(Collectors.toList());
  }

  public List<ResultEntry> listRaceResults(short raceId) {
    return results.values().stream()
      .filter(x -> x.isRaceResult(raceId))
      .collect(Collectors.toList());
  }

  public Set<Short> getRemovedTaskForces() {
    HashSet<Short> res = new HashSet<Short>();
    for (ResultEntry re : results.values()) {
      if (re.turnResult == null)
        continue;

      TurnResultHeader hdr = re.turnResult.header();
      if (!hdr.isTaskForce() || !hdr.isRemoved())
        continue;

      if (!(re.turnResult instanceof MilitaryResult))
        continue;

      MilitaryResult mil = (MilitaryResult) re.turnResult;
      res.add(mil.getTaskForceId());
    }

    return res;
  }

  public void changeTaskForceId(short tfId, short tfIdNew) {
    Set<ResultEntry> removed = tfResults.removeAll(tfId);
    for (ResultEntry result : removed) {
      // taskforce results must have a military result
      val mil = result.militaryResult();
      mil.setTaskForceId(tfIdNew);
    }
    // update taskforce results mapping
    tfResults.putAll(tfIdNew, removed);
  }

  public boolean removeResult(int resultId) {
    ResultEntry result = results.remove(resultId);
    if (result != null) {
      MilitaryResult mil = result.militaryResult();

      if (mil != null) {
        if (mil.isTaskForceResult())
          tfResults.remove(mil.getTaskForceId(), result);
        if (mil.isShipResult())
          shpResults.remove(mil.getShipId(), result);
      }

      markChanged();
      return true;
    }
    return false;
  }

  public boolean removeTaskForceResults(short tfId) {
    Set<ResultEntry> removed = tfResults.removeAll(tfId);
    if (!removed.isEmpty()) {
      for (ResultEntry result : removed) {
        results.remove(result.resultId());

        MilitaryResult mil = result.militaryResult();
        if (mil != null && mil.isShipResult())
          shpResults.remove(mil.getShipId(), result);
      }

      markChanged();
      return true;
    }
    return false;
  }

  public boolean removeShipResults(short shpId) {
    Set<ResultEntry> removed = shpResults.removeAll(shpId);
    if (!removed.isEmpty()) {
      for (ResultEntry result : removed) {
        results.remove(result.resultId());

        MilitaryResult mil = result.militaryResult();
        if (mil != null && mil.isTaskForceResult())
          tfResults.remove(mil.getTaskForceId(), result);
      }

      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllTaskForceResults() {
    if (!tfResults.isEmpty()) {
      for (ResultEntry result : tfResults.values()) {
        results.remove(result.resultId());

        MilitaryResult mil = result.militaryResult();
        if (mil != null && mil.isShipResult())
          shpResults.remove(mil.getShipId(), result);
      }

      tfResults.clear();
      markChanged();
    }
  }

  public void removeAllShipResults() {
    if (!shpResults.isEmpty()) {
      for (ResultEntry result : shpResults.values()) {
        results.remove(result.resultId());

        MilitaryResult mil = result.militaryResult();
        if (mil != null && mil.isTaskForceResult())
          tfResults.remove(mil.getTaskForceId(), result);
      }

      shpResults.clear();
      markChanged();
    }
  }

  public void removeAllMilitaryResults() {
    if (!tfResults.isEmpty()) {
      for (ResultEntry result : tfResults.values())
        results.remove(result.resultId());
      tfResults.clear();
      markChanged();
    }

    if (!shpResults.isEmpty()) {
      for (ResultEntry result : shpResults.values())
        results.remove(result.resultId());
      shpResults.clear();
      markChanged();
    }
  }

  public void removeSystemResults(short systemId) {
    boolean removed = results.values().removeIf(x -> {
      // skip not system related results
      if (!x.isSystemResult(systemId))
        return false;

      // for military results like a ship being built,
      // just unset the system Id
      MilitaryResult mil = x.militaryResult();
      if (mil != null) {
        mil.setSystemId((short) -1);
        return false;
      }

      return true;
    });

    if (removed)
      markChanged();
  }

  public void removeSectorResults(int sectorIndex) throws IOException {
    Sector sectorLst = game.files().sectorLst();
    if (results.values().removeIf(x -> x.isSectorResult(sectorLst, sectorIndex)))
      markChanged();
  }

  public void removeRaceResults(short raceId) {
    if (results.values().removeIf(x -> x.isRaceResult(raceId)))
      markChanged();
  }

  public void onRemovedSystem(short systemId) {
    boolean removed = results.values().removeIf(x -> {
      // for military results like a ship being built,
      // just unset matching system Ids
      MilitaryResult mil = x.militaryResult();
      if (mil != null)
        return mil.onRemovedSystem(systemId);

      // update and remove matching economic results
      EconomicResult eco = x.economicResult();
      if (eco != null)
        return eco.onRemovedSystem(systemId);

      return false;
    });

    if (removed)
      markChanged();
  }

  /**
   * Get lost or hijacked ship results.
   */
  public List<MilitaryResult> getLostShipResults() {
    return results.values().stream()
      .filter(x -> x.turnResult != null
        && x.turnResult instanceof MilitaryResult
        && x.turnResult.header().getType() == TurnResultHeader.Type.MilitaryResult
        && x.turnResult.header().getContentType() == TurnResultHeader.ContentType.ShipEvent
        && x.turnResult.header().getEventType() == TurnResultHeader.EventType.UnitLost)
      .map(x -> (MilitaryResult) x.turnResult).collect(Collectors.toList());
  }

  /**
   * Get ship losses of each races, including hijacked ships.
   */
  public Map<Short, Set<Short>> listShipLosses() {
    return results.values().stream()
      .filter(x -> x.turnResult != null && x.turnResult instanceof MilitaryResult)
      .filter(x -> x.turnResult.header().getType() == TurnResultHeader.Type.MilitaryResult
        && x.turnResult.header().getContentType() == TurnResultHeader.ContentType.ShipEvent
        && x.turnResult.header().getEventType() == TurnResultHeader.EventType.UnitLost)
      .collect(Collectors.groupingBy(x -> x.turnResult.header().getRaceId(), Collectors.mapping(
        x -> ((MilitaryResult)x.turnResult).getShipId(), Collectors.toSet())));
  }

  /**
   * Get races with lost or hijacked ship results.
   */
  public Set<Short> getLostShipRaces() {
    return results.values().stream()
      .filter(x -> x.turnResult != null
        && x.turnResult instanceof MilitaryResult
        && x.turnResult.header().getType() == TurnResultHeader.Type.MilitaryResult
        && x.turnResult.header().getContentType() == TurnResultHeader.ContentType.ShipEvent
        && x.turnResult.header().getEventType() == TurnResultHeader.EventType.UnitLost)
      .map(x -> x.turnResult.header().getRaceId()).collect(Collectors.toSet());
  }

  /**
   * Search ship results for the race of a lost or hijacked ship.
   */
  public Optional<Short> getLostShipRace(short shipId) {
    return results.values().stream()
      .filter(x -> x.turnResult != null
        && x.turnResult instanceof MilitaryResult
        && x.turnResult.header().getType() == TurnResultHeader.Type.MilitaryResult
        && x.turnResult.header().getContentType() == TurnResultHeader.ContentType.ShipEvent
        && x.turnResult.header().getEventType() == TurnResultHeader.EventType.UnitLost)
      .map(x -> ((MilitaryResult)x.turnResult).getShipId())
      .filter(x -> x == shipId)
      .findFirst();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (out instanceof ChunkOutputStream) {
      save((ChunkOutputStream) out);
    } else {
      ChunkWriter cw = new ChunkWriter(out);
      cw.header().setHeaderInfo(false, false);
      save(cw);
    }
  }

  public void save(ChunkOutputStream out) throws IOException {
    out.write(DataTools.toByte(results.size(), true));
    out.nextChunk();

    for (int resultId : loadOrder) {
      ResultEntry result = results.get(resultId);
      if (result != null) {
        result.save(out);
        out.nextChunk();
      }
    }

    // create hash map to exclude already saved entries
    HashSet<Integer> saved = new HashSet<>(loadOrder);

    // save remaining result entries
    for (val entry : results.entrySet()) {
      if (!saved.contains(entry.getKey())) {
        entry.getValue().save(out);
        out.nextChunk();
      }
    }
  }

  @Override
  public void clear() {
    if (!results.isEmpty() || !loadOrder.isEmpty()) {
      tfResults.clear();
      shpResults.clear();
      results.clear();
      loadOrder.clear();
      markChanged();
    }
  }

  @Override
  public void check(Vector<String> response) {
    GShipList shipList = (GShipList) game.tryGetInternalFile(CSavFiles.GShipList, true);
    if (shipList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR,
          ": Missing " + CSavFiles.GShipList + "!"));
      return;
    }

    // Since result.lst refers previous turn changes, check GTForceList, not GWTForce.
    TaskForceList gtfList = (TaskForceList) game.tryGetInternalFile(CSavFiles.GTForceList, true);
    if (gtfList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR,
          "Missing " + CSavFiles.GTForceList + "!"));
      return;
    }

    RaceRst raceRst = (RaceRst) stbof.tryGetInternalFile(CStbofFiles.RaceRst, true);

    Set<Short> removedTF = getRemovedTaskForces();
    boolean any = results.values().removeIf(re ->
    {
      if (re.turnResult != null && re.turnResult.check(this, gtfList, shipList, raceRst, removedTF, response)) {
        MilitaryResult mil = re.militaryResult();
        if (mil != null) {
          if (mil.isTaskForceResult())
            tfResults.remove(mil.getTaskForceId(), re);
          if (mil.isShipResult())
            shpResults.remove(mil.getShipId(), re);
        }
        return true;
      }
      return false;
    });

    if (any) {
      markChanged();
    }
  }

  private class ResultEntry {

    TurnResult turnResult;
    GenericResult resGen;

    ResultEntry(ResultList resultList, ChunkInputStream in) throws IOException {
      int headerSize = in.available();
      if (headerSize < 4) {
        System.err.println("ResultList<" + in.currentChunk() + ">: "
          + headerSize + " entry header size read must be at least 4 bytes for the result id!");
      }

      switch (headerSize) {
        case 0x28: {
          turnResult = TurnResult.load(resultList, in);
          break;
        }
        default: {
          resGen = new GenericResult(in);
          break;
        }
      }
    }

    public int resultId() {
      return turnResult != null ? turnResult.header().resultId() : resGen.resultId();
    }

    public EconomicResult economicResult() {
      if (turnResult instanceof EconomicResult)
        return (EconomicResult) turnResult;
      return null;
    }

    public MilitaryResult militaryResult() {
      if (turnResult instanceof MilitaryResult)
        return (MilitaryResult) turnResult;
      return null;
    }

    public IntelResult intelResult() {
      if (turnResult instanceof IntelResult)
        return (IntelResult) turnResult;
      return null;
    }

    public boolean isSystemResult(short systemId) {
      // check system orders
      EconomicResult eco = economicResult();
      if (eco != null)
        return eco.getSystemId() == systemId;

      // check task force orders
      MilitaryResult mil = militaryResult();
      if (mil != null)
        return mil.getSystemId() == systemId;

      return false;
    }

    public boolean isSectorResult(Sector sectorLst, int sectorIndex) {
      short systemId = sectorLst.getSystem(sectorIndex);

      // check military results
      MilitaryResult mil = militaryResult();
      if (mil != null) {
        // check target sector
        if (mil.getSectorIndex() == sectorIndex)
          return true;
        // double-check the sector position
        int secPos = sectorLst.getSectorIndex(mil.getSectorColumn(), mil.getSectorRow());
        if (secPos == sectorIndex)
          return true;
        // check target system
        if (systemId != -1)
          return mil.getSystemId() == systemId;
      }

      // check system results
      if (systemId != -1) {
        EconomicResult eco = economicResult();
        if (eco != null)
          return eco.getSystemId() == systemId;
      }

      return false;
    }

    public boolean isRaceResult(short raceId) {
      if (turnResult == null)
        return false;

      // check turn result race
      if (turnResult.header().getRaceId() == raceId)
        return true;

      // check involved major empires
      if (raceId < CStbof.NUM_EMPIRES) {
        int raceMask = 1 << raceId;
        if ((turnResult.header().getRaceMask() & raceMask) != 0)
          return true;
      }

      // check economic result
      EconomicResult ecoRes = economicResult();
      if (ecoRes != null) {
        Order order = ecoRes.getOrder();
        return order != null ? order.isRaceOrder(raceId) : false;
      }

      // check military result
      MilitaryResult milRes = militaryResult();
      if (milRes != null)
        return milRes.getRaceId() == raceId || milRes.getTargetRaceId() == raceId;

      // check intel result
      IntelResult itlRes = intelResult();
      if (itlRes != null)
        return itlRes.getSourceRace() == raceId || itlRes.getTargetRace() == raceId;

      return false;
    }

    int getSize() {
      if (turnResult != null) {
        return turnResult.getSize();
      }
      if (resGen != null) {
        return resGen.getSize();
      }
      return 0;
    }

    void save(ChunkOutputStream out) throws IOException {
      if (turnResult != null) {
        turnResult.save(out);
      } else {
        resGen.save(out);
      }
    }
  }

}
