package ue.edit.sav.files.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class ProvInfo extends InternalFile {

  // sort province infos to simplify hex lookup
  private TreeSet<Short> provs = new TreeSet<Short>();

  public ProvInfo() {
  }

  // #region properties

  // #region property gets

  public int numProvinces() {
    return provs.size();
  }

  public Set<Short> listProvinces() {
    return Collections.unmodifiableSet(provs);
  }

  public short firstProvince() {
    return provs.first();
  }

  public short lastProvince() {
    return provs.last();
  }

  // #endregion property gets

  // #region property sets

  public void addProvince(short systemId) {
    if (provs.add(systemId))
      markChanged();
  }

  public void removeProvince(short systemId) {
    if (provs.remove(systemId))
      markChanged();
  }

  // #endregion property sets

  // #endregion properties

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    provs.clear();

    int num = StreamTools.readInt(in, true);
    int size = in.available();
    if (size != num * Short.BYTES)
      System.out.println("ProvInfo: Province count doesn't match the entry number!");

    num = Integer.min(size / Short.BYTES, num);
    for (int i = 0; i < num; ++i) {
      short systemId = StreamTools.readShort(in, true);
      provs.add(systemId);
    }

    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(provs.size(), true));
    for (short systemId : provs) {
      out.write(DataTools.toByte(systemId, true));
    }
  }

  // #endregion save

  // #region maintenance

  @Override
  public int getSize() {
    return (provs.size() * Short.BYTES) + Integer.BYTES;
  }

  public void onRemovedSystem(short systemId) {
    if (provs.isEmpty() || provs.last() < systemId)
      return;

    // update system ids
    TreeSet<Short> copy = new TreeSet<>();
    for (short key : provs) {
      if (key != systemId)
        copy.add(key < systemId ? key : --key);
    }

    provs = copy;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public void clear() {
    provs.clear();
    markChanged();
  }

  // #endregion maintenance

}
