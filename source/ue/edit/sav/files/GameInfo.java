package ue.edit.sav.files;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.sav.SavGame;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of the gameInfo file. It holds info on the game.
 */
@Accessors(prefix = {"m"})
public class GameInfo extends InternalFile {

  public class GameStats {
    public int majorRaces = 0;
    public int majorSystemsMax = 0;
    public int majorsBeaten = 0;
    public int minorRaces = 0;
    public int minorsBeaten = 0;
    public int minorsMembered = 0;
    public int minorsSpaceFaring = 0;

    public int majorsAlive() { return majorRaces - majorsBeaten; }
    public int minorsAlive() { return minorRaces - minorsBeaten; }
    public int minorsIndependent() { return minorsAlive() - minorsMembered; }
  }

  public class EmpireGameStats {
    public int score = 0;
    public int intel = 0;
    public int military = 0;
    public int economy = 0;
    public int research = 0;
    public int minors = 0;
    public int systems = 0;
    public int dilithium = 0;
  }

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private static final long MajorBitMask = 0x1f;
  private static final long MinorBitMask = ~MajorBitMask;

  // data
  /** file name (unused) */
  @Getter private String mFileName;                                                   // 0x0000 (0000+0013)

  /** the name of the game */
  @Getter private String mSaveName;                                                   // 0x000D (0013+0051)

  /** 0-based turn number */
  @Getter private int mLastTurn;                                                      // 0x0040 (0064+0004)

  private byte[] mUnknown1 = new byte[8];                                             // 0x0044 (0068+0008)

  /** true if the game is multiplayer */
  @Getter private boolean mMultiplayer;                                               // 0x004C (0076+0004)

  private byte[] mUnknown2 = new byte[104];                                           // 0x0050 (0080+0104)

  // SCT: dword 1 = multiplayer GameIsHost ?
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56913#p56913
  @Getter private boolean mIsHost;                                                    // 0x00B8 (0184+0004)

  private byte[] mUnknown3 = new byte[396];                                           // 0x00BC (0188+0396)

  // SCT: multiplayer teamdomination buddy empire(s) bitmasks (1 byte per empire, +24D-24F unused)
  @Getter private long mTeamBuddies;                                                  // 0x0248 (0584+0008)

  // participating multiplayer empire bit-mask
  @Getter private byte mActivePlayerEmpiresMask;                                      // 0x0250 (0592+0001)

  // beaten multiplayer empire bit-mask
  @Getter private byte mBeatenPlayerEmpiresMask;                                      // 0x0251 (0593+0001)

  /** the race index of the players empire (00=Card, 01=Fed, 02=Ferg, 03=Kling, 04=Rom) */
  @Getter private byte mPlayerEmpire;                                                 // 0x0252 (0594+0001)

  /** predictor for starting EL of minor races */
  @Getter private byte mMinorRaceStartingEL;                                          // 0x0253 (0595+0001)

  /** starting evolution levels for each major (Card, Fed, Ferg, Kling, Rom; 0=Lv1, ..., 4=Lv5) */
  @Getter private byte[] mStartEvoLvls = new byte[NUM_EMPIRES];                       // 0x0254 (0596+0005)

  /** likely empty data alignment */
  private byte[] mEmpty_1 = new byte[3];                                              // 0x0259 (0601+0003)

  // SCT: new game via random system time or from stbof.ini when launching trek.exe -n
  @Getter private int mStartingSeed;                                                  // 0x025C (0604+0004)

  /** galaxy shape (0=irregular, 1=elliptic, 2=ring, 3=spiral */
  @Getter private int mGalaxyShape;                                                   // 0x0260 (0608+0004)

  /** galaxy size (0=small, 1=medium, 2=large) */
  @Getter private int mGalaxySize;                                                    // 0x0264 (0612+0004)

  /** minor race amount (0=none, 1=few, 2=some, 3=many) */
  @Getter private int mMinorRaceAmount;                                               // 0x0268 (0616+0004)

  /** difficulty level (1-5) */
  @Getter private short mDifficulty;                                                  // 0x026C (0620+0002)

  /** random events (0=OFF, 1=ON) */
  @Getter private boolean mRandomEvents;                                              // 0x026E (0622+0002)

  /** races in game (bitwise XOR bytes, matching the order of the race infos / alienInfo)
   * e.g. first byte 0x5F 01011111 in vanilla is read from the right: 0h=Card, ..., 10h=Rom and 40h= the Andorians */
  @Getter private long mRaceMask;                                                     // 0x0270 (0624+0008)

  /** beaten races (bitwise XOR, see mRaceMask)
   * disables ai for when an empire is defeated or a minor race completely erased */
  @Getter private long mBeatenRaceMask;                                               // 0x0278 (0632+0008)

  /** races building ships (bitwise XOR, see mRaceMask) */
  @Getter private long mShipRaceMask;                                                 // 0x0280 (0640+0008)

  private byte[] mUnknown4 = new byte[8];                                             // 0x0288 (0648+0008)

  /** tactical mode (0=detailed, 2=abstract) */
  @Getter private int mTacticalMode;                                                  // 0x0290 (0656+0004)

  /** active cheat bitmask */
  @Getter private int mCheatMask;                                                     // 0x0294 (0660+0004)

  /** active random event (bitwise XOR, 1=trade guild strike, 2=subspace anomaly, 8=end trade strike, 10h=end anomaly) */
  @Getter private int mRandomEventMask;                                               // 0x0298 (0664+0004)

  private int mUnknown5;                                                              // 0x029C (0668+0004)

  /** generated monsters bitmask
   * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?t=611&start=22 */
  @Getter private int mMonsterMask;                                                   // 0x02A0 (0672+0004)

  /** strategical timer */
  @Getter private int mStrategicalTimer;                                              // 0x02A4 (0676+0004)

  /** tactical timer */
  @Getter private int mTacticalTimer;                                                 // 0x02A8 (0680+0004)

  /** current turn empire stats index, shrinked by half every 250 turns
   * @see mEmpireStatsShrink */
  @Getter private short mEmpireStatsIdx;                                              // 0x02AC (0684+0002)

  // last turn points (total score - starting score, used for statistic)
  @Getter private short mLastTurnScore;                                               // 0x02AE (0686+0002)

  private byte[] mUnknown6 = new byte[8];                                             // 0x02B0 (0688+0008)

  /** empire stats */
  @Getter private short[][] mEmpireStatistics = new short[NUM_EMPIRES][250];          // 0x02B8 (0696+2500)

  /** increased by one every 250 turns to shrink the empire stats
   * each shrink every second statistic value is removed to avoid data overflows */
  @Getter private int mEmpireStatsShrink;                                             // 0x0C7C (3196+0004)

  /** current turn -2 */
  @Getter private int mTwoTurnsBefore;                                                // 0x0C80 (3200+0004)

  /** turn 1 starting score bias to only count improvements */
  @Getter private short[] mEmpireStartScore = new short[NUM_EMPIRES];                 // 0x0C84 (3204+0010)

  // SCT: vanilla bugged due to different overflow issues and starting score deviation bias
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56913#p56913
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56648#p56648
  @Getter private short[] mEmpireTotalScoreRank = new short[NUM_EMPIRES];             // 0x0C8E (3214+0010)

  /** military strength values of the empires */
  @Getter private short[] mEmpireMilitaryStats = new short[NUM_EMPIRES];              // 0x0C98 (3224+0010)

  /** military strength values of the empires for the last ten turns */
  @Getter private short[][] mEmpireMilitaryStatHistory = new short[NUM_EMPIRES][10];  // 0x0CA2 (3234+0100)

  /** military score rank listing the empire ids - SCT: vanilla overflow issue with score */
  @Getter private short[] mEmpireMiliaryRank = new short[NUM_EMPIRES];                // 0x0D06 (3334+0010)

  /** economy strength values of the empires */
  @Getter private short[] mEmpireEconomyStats = new short[NUM_EMPIRES];               // 0x0D10 (3344+0010)

  /** economy score rank listing the empire ids - SCT: vanilla overflow issue with score */
  @Getter private short[] mEmpireEconomicRank = new short[NUM_EMPIRES];               // 0x0D1A (3354+0010)

  /** science strength values of the empires for the last ten turns */
  @Getter private short[][] mEmpireScienceStatHistory = new short[NUM_EMPIRES][10];   // 0x0D24 (3364+0100)

  /** science strength values of the empires */
  @Getter private short[] mEmpireScienceStats = new short[NUM_EMPIRES];               // 0x0D88 (3464+0010)

  /** science score rank listing the empire ids */
  @Getter private short[] mEmpireScienceRank = new short[NUM_EMPIRES];                // 0x0D92 (3474+0010)

  /** minor races membered by the empires */
  @Getter private short[] mEmpireMemberStats = new short[NUM_EMPIRES];                // 0x0D9C (3484+0010)

  /** systems occupied by the empires */
  @Getter private short[] mEmpireSystemStats = new short[NUM_EMPIRES];                // 0x0DA6 (3494+0010)

  /** systems with dilithium occupied by the empires */
  @Getter private short[] mEmpireDilithiumStats = new short[NUM_EMPIRES];             // 0x0DB0 (3404+0010)

  /** likely empty data alignment */
  private short mEmpty_2;                                                             // 0x0DBA (3514+0002)

  /** victory conditions (00=dominance, 01=multiplayer team dominance, 02=vendetta) */
  @Getter private int mVictoryConditions;                                             // 0x0DBC (3514+0004)

  public GameInfo(SavGame game) {
  }

  // specified by SavEdit
  @Override
  public void check(Vector<String> response) {
    if (mFileName.length() > 12) {
      String msg = Language.getString("GameInfo.0"); //$NON-NLS-1$
      msg = msg.replace("%1", mFileName); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    if (mSaveName.length() > 49) {
      String msg = Language.getString("GameInfo.1"); //$NON-NLS-1$
      msg = msg.replace("%1", mSaveName); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    if (mLastTurn < 0) {
      String msg = Language.getString("GameInfo.2"); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    if (mLastTurn >= Short.MAX_VALUE) {
      String msg = Language.getString("GameInfo.3") //$NON-NLS-1$
        .replace("%1", Integer.toString(mLastTurn));
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      // always indexes the next stats value
      mEmpireStatsIdx = (short)1;
      mLastTurn = 0;
      markChanged();
    }

    // first  shrink at turn  250 -> 250/2 = 125, increased only every 2nd turn
    // second shrink at turn  500 -> 250/2 = 125, increased only every 4th turn
    // third  shrink at turn 1000 -> 250/2 = 125, increased only every 8th turn
    int empStatIdx = (mLastTurn + 1) >> mEmpireStatsShrink;

    if (empStatIdx != mEmpireStatsIdx) {
      String msg = Language.getString("GameInfo.4") //$NON-NLS-1$
        .replace("%1", Short.toString(mEmpireStatsIdx))
        .replace("%2", Integer.toString(mLastTurn));
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));

      mEmpireStatsIdx = (short)empStatIdx;
      markChanged();
    }

    if (mPlayerEmpire < 0 || mPlayerEmpire > 4) {
      String msg = Language.getString("GameInfo.5"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(mPlayerEmpire)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    short t = (short) (1 << mPlayerEmpire);
    if ((mActivePlayerEmpiresMask & t) == 0) {
      String msg = Language.getString("GameInfo.6"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(mActivePlayerEmpiresMask) + " (fixed)"); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
      mActivePlayerEmpiresMask |= t;
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return 3520;
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    mFileName = StreamTools.readNullTerminatedBotfString(in, 13);   // 0x0000 (0000+0013)
    mSaveName = StreamTools.readNullTerminatedBotfString(in, 51);   // 0x000D (0013+0051)
    mLastTurn = StreamTools.readInt(in, true);                      // 0x0040 (0064+0004)
    StreamTools.read(in, mUnknown1);                                // 0x0044 (0068+0008)
    int x = StreamTools.readInt(in, true);                          // 0x004C (0076+0004)
    mMultiplayer = x != 0;
    StreamTools.read(in, mUnknown2);                                // 0x0050 (0080+0104)
    x = StreamTools.readInt(in, true);                              // 0x00B8 (0184+0004)
    mIsHost = x != 0;
    StreamTools.read(in, mUnknown3);                                // 0x00BC (0188+0396)
    mTeamBuddies = StreamTools.readLong(in, true);                  // 0x0248 (0584+0008)
    mActivePlayerEmpiresMask = (byte)in.read();                     // 0x0250 (0592+0001)
    mBeatenPlayerEmpiresMask = (byte)in.read();                     // 0x0251 (0593+0001)
    mPlayerEmpire = (byte)in.read();                                // 0x0252 (0594+0001)
    mMinorRaceStartingEL = (byte)in.read();                         // 0x0253 (0595+0001)
    StreamTools.read(in, mStartEvoLvls);                            // 0x0254 (0596+0005)
    StreamTools.read(in, mEmpty_1);                                 // 0x0259 (0601+0003)
    mStartingSeed = StreamTools.readInt(in, true);                  // 0x025C (0604+0004)
    mGalaxyShape = StreamTools.readInt(in, true);                   // 0x0260 (0608+0004)
    mGalaxySize = StreamTools.readInt(in, true);                    // 0x0264 (0612+0004)
    mMinorRaceAmount = StreamTools.readInt(in, true);               // 0x0268 (0616+0004)
    mDifficulty = StreamTools.readShort(in, true);                  // 0x026C (0620+0002)
    short randoms = StreamTools.readShort(in, true);                // 0x026E (0622+0002)
    mRandomEvents = randoms != 0;
    mRaceMask = StreamTools.readLong(in, true);                     // 0x0270 (0624+0008)
    mBeatenRaceMask = StreamTools.readLong(in, true);               // 0x0278 (0632+0008)
    mShipRaceMask = StreamTools.readLong(in, true);                 // 0x0280 (0640+0008)
    StreamTools.read(in, mUnknown4);                                // 0x0288 (0648+0008)
    mTacticalMode = StreamTools.readInt(in, true);                  // 0x0290 (0656+0004)
    mCheatMask = StreamTools.readInt(in, true);                     // 0x0294 (0660+0004)
    mRandomEventMask = StreamTools.readInt(in, true);               // 0x0298 (0664+0004)
    mUnknown5 = StreamTools.readInt(in, true);                      // 0x029C (0668+0004)
    mMonsterMask = StreamTools.readInt(in, true);                   // 0x02A0 (0672+0004)
    mStrategicalTimer = StreamTools.readInt(in, true);              // 0x02A4 (0676+0004)
    mTacticalTimer = StreamTools.readInt(in, true);                 // 0x02A8 (0680+0004)
    mEmpireStatsIdx = StreamTools.readShort(in, true);              // 0x02AC (0684+0002)
    mLastTurnScore = StreamTools.readShort(in, true);               // 0x02AE (0686+0002)
    StreamTools.read(in, mUnknown6);                                // 0x02B0 (0688+0008)

    for (int i = 0; i < mEmpireStatistics.length; ++i)
      StreamTools.read(in, mEmpireStatistics[i], true);             // 0x02B8 (0696+2500)

    mEmpireStatsShrink = StreamTools.readInt(in, true);             // 0x0C7C (3196+0004)
    mTwoTurnsBefore = StreamTools.readInt(in, true);                // 0x0C80 (3200+0004)

    for (int i = 0; i < mEmpireStartScore.length; ++i)
      mEmpireStartScore[i] = StreamTools.readShort(in, true);       // 0x0C84 (3204+0010)
    for (int i = 0; i < mEmpireTotalScoreRank.length; ++i)
      mEmpireTotalScoreRank[i] = StreamTools.readShort(in, true);   // 0x0C8E (3214+0010)
    for (int i = 0; i < mEmpireMilitaryStats.length; ++i)
      mEmpireMilitaryStats[i] = StreamTools.readShort(in, true);    // 0x0C98 (3224+0010)
    for (short[] militaryStatHistory : mEmpireMilitaryStatHistory)
      for (int i = 0; i < militaryStatHistory.length; ++i)
        militaryStatHistory[i] = StreamTools.readShort(in, true);   // 0x0CA2 (3234+0100)
    for (int i = 0; i < mEmpireMiliaryRank.length; ++i)
      mEmpireMiliaryRank[i] = StreamTools.readShort(in, true);      // 0x0D06 (3334+0010)
    for (int i = 0; i < mEmpireEconomyStats.length; ++i)
      mEmpireEconomyStats[i] = StreamTools.readShort(in, true);     // 0x0D10 (3344+0010)
    for (int i = 0; i < mEmpireEconomicRank.length; ++i)
      mEmpireEconomicRank[i] = StreamTools.readShort(in, true);     // 0x0D1A (3354+0010)
    for (short[] scienceStatHistory : mEmpireScienceStatHistory)
      for (int i = 0; i < scienceStatHistory.length; ++i)
        scienceStatHistory[i] = StreamTools.readShort(in, true);    // 0x0D24 (3364+0100)
    for (int i = 0; i < mEmpireScienceStats.length; ++i)
      mEmpireScienceStats[i] = StreamTools.readShort(in, true);     // 0x0D88 (3464+0010)
    for (int i = 0; i < mEmpireScienceRank.length; ++i)
      mEmpireScienceRank[i] = StreamTools.readShort(in, true);      // 0x0D92 (3474+0010)
    for (int i = 0; i < mEmpireMemberStats.length; ++i)
      mEmpireMemberStats[i] = StreamTools.readShort(in, true);      // 0x0D9C (3484+0010)
    for (int i = 0; i < mEmpireSystemStats.length; ++i)
      mEmpireSystemStats[i] = StreamTools.readShort(in, true);      // 0x0DA6 (3494+0010)
    for (int i = 0; i < mEmpireDilithiumStats.length; ++i)
      mEmpireDilithiumStats[i] = StreamTools.readShort(in, true);   // 0x0DB0 (3404+0010)

    mEmpty_2 = StreamTools.readShort(in, true);                     // 0x0DBA (3514+0002)
    mVictoryConditions = StreamTools.readInt(in, true);             // 0x0DBC (3514+0004)
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(mFileName, 13, 12));                     // 0x0000 (0000+0013)
    out.write(DataTools.toByte(mSaveName, 51, 49));                     // 0x000D (0013+0051)
    out.write(DataTools.toByte(mLastTurn, true));                       // 0x0040 (0064+0004)
    out.write(mUnknown1);                                               // 0x0044 (0068+0008)
    out.write(DataTools.toByte(mMultiplayer ? 1 : 0, true));            // 0x004C (0076+0004)
    out.write(mUnknown2);                                               // 0x0050 (0080+0104)
    out.write(DataTools.toByte(mIsHost ? 1 : 0, true));                 // 0x00B8 (0184+0004)
    out.write(mUnknown3);                                               // 0x00BC (0188+0396)
    out.write(DataTools.toByte(mTeamBuddies, true));                    // 0x0248 (0584+0008)
    out.write(mActivePlayerEmpiresMask);                                // 0x0250 (0592+0001)
    out.write(mBeatenPlayerEmpiresMask);                                // 0x0251 (0593+0001)
    out.write(mPlayerEmpire);                                           // 0x0252 (0594+0001)
    out.write(mMinorRaceStartingEL);                                    // 0x0253 (0595+0001)
    out.write(mStartEvoLvls);                                           // 0x0254 (0596+0005)
    out.write(mEmpty_1);                                                // 0x0259 (0601+0003)
    out.write(DataTools.toByte(mStartingSeed, true));                   // 0x025C (0604+0004)
    out.write(DataTools.toByte(mGalaxyShape, true));                    // 0x0260 (0608+0004)
    out.write(DataTools.toByte(mGalaxySize, true));                     // 0x0264 (0612+0004)
    out.write(DataTools.toByte(mMinorRaceAmount, true));                // 0x0268 (0616+0004)
    out.write(DataTools.toByte(mDifficulty, true));                     // 0x026C (0620+0002)
    out.write(DataTools.toByte((short)(mRandomEvents ? 1 : 0), true));  // 0x026E (0622+0002)
    out.write(DataTools.toByte(mRaceMask, true));                       // 0x0270 (0624+0008)
    out.write(DataTools.toByte(mBeatenRaceMask, true));                 // 0x0278 (0632+0008)
    out.write(DataTools.toByte(mShipRaceMask, true));                   // 0x0280 (0640+0008)
    out.write(mUnknown4);                                               // 0x0288 (0648+0008)
    out.write(DataTools.toByte(mTacticalMode, true));                   // 0x0290 (0656+0004)
    out.write(DataTools.toByte(mCheatMask, true));                      // 0x0294 (0660+0004)
    out.write(DataTools.toByte(mRandomEventMask, true));                // 0x0298 (0664+0004)
    out.write(DataTools.toByte(mUnknown5, true));                       // 0x029C (0668+0004)
    out.write(DataTools.toByte(mMonsterMask, true));                    // 0x02A0 (0672+0004)
    out.write(DataTools.toByte(mStrategicalTimer, true));               // 0x02A4 (0676+0004)
    out.write(DataTools.toByte(mTacticalTimer, true));                  // 0x02A8 (0680+0004)
    out.write(DataTools.toByte(mEmpireStatsIdx, true));                 // 0x02AC (0684+0002)
    out.write(DataTools.toByte(mLastTurnScore, true));                  // 0x02AE (0686+0002)
    out.write(mUnknown6);                                               // 0x02B0 (0688+0008)
    for (short[] empireStats : mEmpireStatistics)
      out.write(DataTools.toByte(empireStats, true));                   // 0x02B8 (0696+2500)
    out.write(DataTools.toByte(mEmpireStatsShrink, true));              // 0x0C7C (3196+0004)
    out.write(DataTools.toByte(mTwoTurnsBefore, true));                 // 0x0C80 (3200+0004)
    for (short startingScore : mEmpireStartScore)
      out.write(DataTools.toByte(startingScore, true));                 // 0x0C84 (3204+0010)
    for (short totalRank : mEmpireTotalScoreRank)
      out.write(DataTools.toByte(totalRank, true));                     // 0x0C8E (3214+0010)
    for (short militaryStat : mEmpireMilitaryStats)
      out.write(DataTools.toByte(militaryStat, true));                  // 0x0C98 (3224+0010)
    for (short[] militaryStatHistory : mEmpireMilitaryStatHistory)
      for (short militaryStat : militaryStatHistory)
        out.write(DataTools.toByte(militaryStat, true));                // 0x0CA2 (3234+0100)
    for (short militaryRank : mEmpireMiliaryRank)
      out.write(DataTools.toByte(militaryRank, true));                  // 0x0D06 (3334+0010)
    for (short economyStat : mEmpireEconomyStats)
      out.write(DataTools.toByte(economyStat, true));                   // 0x0D10 (3344+0010)
    for (short economyRank : mEmpireEconomicRank)
      out.write(DataTools.toByte(economyRank, true));                   // 0x0D1A (3354+0010)
    for (short[] scienceStatHistory : mEmpireScienceStatHistory)
      for (short scienceStat : scienceStatHistory)
        out.write(DataTools.toByte(scienceStat, true));                 // 0x0D24 (3364+0100)
    for (short scienceStat : mEmpireScienceStats)
      out.write(DataTools.toByte(scienceStat, true));                   // 0x0D88 (3464+0010)
    for (short scienceRank : mEmpireScienceRank)
      out.write(DataTools.toByte(scienceRank, true));                    // 0x0D92 (3474+0010)
    for (short memberStat : mEmpireMemberStats)
      out.write(DataTools.toByte(memberStat, true));                    // 0x0D9C (3484+0010)
    for (short systemStat : mEmpireSystemStats)
      out.write(DataTools.toByte(systemStat, true));                    // 0x0DA6 (3494+0010)
    for (short dilithiumStat : mEmpireDilithiumStats)
      out.write(DataTools.toByte(dilithiumStat, true));                 // 0x0DB0 (3404+0010)
    out.write(DataTools.toByte(mEmpty_2, true));                        // 0x0DBA (3514+0002)
    out.write(DataTools.toByte(mVictoryConditions, true));              // 0x0DBC (3514+0004)
  }

  @Override
  public void clear() {
    mFileName = "";
    mSaveName = "";
    mLastTurn = 0;
    Arrays.fill(mUnknown1, (byte)0);
    mMultiplayer = false;
    Arrays.fill(mUnknown2, (byte)0);
    mTeamBuddies = 0;
    mActivePlayerEmpiresMask = 0;
    mBeatenPlayerEmpiresMask = 0;
    mPlayerEmpire = 0;
    mMinorRaceStartingEL = 0;
    Arrays.fill(mStartEvoLvls, (byte)0);
    Arrays.fill(mEmpty_1, (byte)0);
    mStartingSeed = 0;
    mGalaxyShape = 0;
    mGalaxySize = 0;
    mMinorRaceAmount = 0;
    mDifficulty = 0;
    mRandomEvents = false;
    mRaceMask = 0;
    mBeatenRaceMask = 0;
    mShipRaceMask = 0;
    Arrays.fill(mUnknown4, (byte)0);
    mTacticalMode = 0;
    mCheatMask = 0;
    mRandomEventMask = 0;
    mUnknown5 = 0;
    mMonsterMask = 0;
    mStrategicalTimer = 0;
    mTacticalTimer = 0;
    mEmpireStatsIdx = 0;
    mLastTurnScore = 0;
    Arrays.fill(mUnknown6, (byte)0);
    for (short[] empireStats : mEmpireStatistics)
      Arrays.fill(empireStats, (short)0);
    mEmpireStatsShrink = 0;
    mTwoTurnsBefore = 0;
    Arrays.fill(mEmpireStartScore, (short)0);
    Arrays.fill(mEmpireTotalScoreRank, (short)0);
    Arrays.fill(mEmpireMilitaryStats, (short)0);
    for (short[] militaryStatHistory : mEmpireMilitaryStatHistory)
      Arrays.fill(militaryStatHistory, (short)0);
    Arrays.fill(mEmpireMiliaryRank, (short)0);
    Arrays.fill(mEmpireEconomyStats, (short)0);
    Arrays.fill(mEmpireEconomicRank, (short)0);
    for (short[] scienceStatHistory : mEmpireScienceStatHistory)
      Arrays.fill(scienceStatHistory, (short)0);
    Arrays.fill(mEmpireScienceStats, (short)0);
    Arrays.fill(mEmpireScienceRank, (short)0);
    Arrays.fill(mEmpireMemberStats, (short)0);
    Arrays.fill(mEmpireSystemStats, (short)0);
    Arrays.fill(mEmpireDilithiumStats, (short)0);
    mEmpty_2 = 0;
    mVictoryConditions = 0;
    markChanged();
  }

  // gets
  public int getCurrentTurn() {
    return mLastTurn + 1;
  }

  public byte getStartEvoLvl(int empire) {
    return mStartEvoLvls[empire];
  }

  public int getEmpireScore(int empire, int turn) {
    short[] empireStats = mEmpireStatistics[empire];
    // loop in case the end of the list is reached
    turn = turn % empireStats.length;
    return Short.toUnsignedInt(empireStats[turn]);
  }

  public int getEmpireStartScore(int empire) {
    return Short.toUnsignedInt(mEmpireStartScore[empire]);
  }

  public int getEmpireMilitary(int empire) {
    return Short.toUnsignedInt(mEmpireMilitaryStats[empire]);
  }

  public int getEmpireEconomy(int empire) {
    return Short.toUnsignedInt(mEmpireEconomyStats[empire]);
  }

  public int getEmpireScience(int empire) {
    return Short.toUnsignedInt(mEmpireScienceStats[empire]);
  }

  public int getEmpireMembers(int empire) {
    return Short.toUnsignedInt(mEmpireMemberStats[empire]);
  }

  public int getEmpireSystems(int empire) {
    return Short.toUnsignedInt(mEmpireSystemStats[empire]);
  }

  public int getEmpireDilithium(int empire) {
    return Short.toUnsignedInt(mEmpireDilithiumStats[empire]);
  }

  public GameStats getStats() {
    GameStats stats = new GameStats();
    stats.majorRaces = Long.bitCount(MajorBitMask & mRaceMask);
    for (short cnt : mEmpireSystemStats)
      stats.majorSystemsMax = Math.max(stats.majorSystemsMax, cnt);
    stats.majorsBeaten = Long.bitCount(MajorBitMask & mBeatenRaceMask);
    stats.minorRaces = Long.bitCount(MinorBitMask & mRaceMask);
    stats.minorsBeaten = Long.bitCount(MinorBitMask & mBeatenRaceMask);
    for (short cnt : mEmpireMemberStats)
      stats.minorsMembered += cnt;
    stats.minorsSpaceFaring = Long.bitCount(MinorBitMask & mShipRaceMask);

    return stats;
  }

  public EmpireGameStats getEmpireStats(int empire) {
    EmpireGameStats stats = new EmpireGameStats();

    int lastTurn = getLastTurn();
    stats.score = getEmpireScore(empire, lastTurn);
    stats.intel = getEmpireStartScore(empire);
    stats.military = getEmpireMilitary(empire);
    stats.economy = getEmpireEconomy(empire);
    stats.research = getEmpireScience(empire);
    stats.minors = getEmpireMembers(empire);
    stats.systems = getEmpireSystems(empire);
    stats.dilithium = getEmpireDilithium(empire);

    return stats;
  }

  public EmpireGameStats[] getEmpireStats() {
    EmpireGameStats[] stats = new EmpireGameStats[NUM_EMPIRES];

    for (int empire = 0; empire < NUM_EMPIRES; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  // sets

  /**
   * Sets the name of the game.
   */
  public void setGameName(String name) {
    if (name.length() > 49) {
      name = name.substring(0, 48);
    }
    if (!mSaveName.equals(name)) {
      mSaveName = name;
      this.markChanged();
    }
  }

  /**
   * Sets the actual turn number of the game.
   */
  public void setCurrentTurn(short turn) {
    setLastTurn(turn-1);
  }

  /**
   * Sets the 0-based passed turn number of the game.
   */
  public void setLastTurn(int turn) {
    if (mLastTurn == turn)
      return;

    mLastTurn = turn;
    mTwoTurnsBefore = turn > 0 ? turn-1 : 0;

    // always indexes the next stats value
    ++turn;

    mEmpireStatsShrink = 0;
    while (turn >= 250) {
      turn >>= 1;
      ++mEmpireStatsShrink;
    }

    mEmpireStatsIdx = (short)turn;
    markChanged();
  }

  /**
   * Sets the player empire.
   */
  public void setPlayerEmpire(byte emp) throws InvalidArgumentsException {
    if (emp < 0 || emp > 4) {
      String msg = Language.getString("GameInfo.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Byte.toString(emp)); //$NON-NLS-1$
      throw new InvalidArgumentsException(getName() + ": " + msg);
    }
    if (mPlayerEmpire != emp) {
      mPlayerEmpire = emp;
      mActivePlayerEmpiresMask = (byte) (1 << emp);
      markChanged();
    }
  }

  public void defeatRace(int raceId) {
    long raceMask = 1L << raceId;

    // flag race beaten
    if ((mBeatenRaceMask & raceMask) == 0) {
      mBeatenRaceMask |= raceMask;
      markChanged();
    }

    // remove from space faring races
    if ((mShipRaceMask & raceMask) != 0) {
      mShipRaceMask &= ~raceMask;
      markChanged();
    }

    if (raceId < NUM_EMPIRES) {
      // update participating multiplayer empires
      if ((mActivePlayerEmpiresMask & raceMask) != 0) {
        mActivePlayerEmpiresMask &= ~raceMask;
        mBeatenPlayerEmpiresMask |= raceMask;
        markChanged();
      }
    }
  }

  public void removeRace(int raceId) {
    defeatRace(raceId);

    long raceMask = 1L << raceId;
    if ((mRaceMask & raceMask) != 0) {
      mRaceMask &= ~raceMask;
      markChanged();
    }
  }

  public void eliminateRace(int raceId) {
    if (raceId < NUM_EMPIRES) {
      // Removal of major empires from participating race mask, results in
      // load crash: "File: ..\..\source\\universe\readrace.c, Line: 257, 0"
      defeatRace(raceId);
    } else {
      // Removing the techInfo entry but keeping space faring minors listed, results in
      // load crash: "File: gdllist.c, Line: 764, gdlList != NULL"
      removeRace(raceId);
    }
  }

  /**
   * Sets difficulty.
   */
  public void setDifficulty(short diff) throws InvalidArgumentsException {
    if (diff < 1 || diff > 5) {
      String msg = Language.getString("GameInfo.8"); //$NON-NLS-1$
      msg = msg.replace("%1", Short.toString(diff)); //$NON-NLS-1$
      throw new InvalidArgumentsException(getName() + ": " + msg);
    }
    if (mDifficulty != diff) {
      mDifficulty = diff;
      this.markChanged();
    }
  }

  /**
   * Sets if the game is multiplayer.
   */
  public void setMultiplayer(boolean mp) {
    if (mMultiplayer != mp) {
      mMultiplayer = mp;
      this.markChanged();
    }
  }

  public void setTacticalMode(int mode) throws InvalidArgumentsException {
    if (mode < 0 || mode > 2) {
      String msg = Language.getString("GameInfo.9"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(mode)); //$NON-NLS-1$
      throw new InvalidArgumentsException(getName() + ": " + msg);
    }
    if (mTacticalMode != mode) {
      mTacticalMode = mode;
      this.markChanged();
    }
  }

  public void setVictoryConditions(int victoryConditions) throws InvalidArgumentsException {
    if (victoryConditions < 0 || victoryConditions > 2) {
      String msg = Language.getString("GameInfo.10"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(victoryConditions)); //$NON-NLS-1$
      throw new InvalidArgumentsException(getName() + ": " + msg);
    }
    if (mVictoryConditions != victoryConditions) {
      mVictoryConditions = victoryConditions;
      this.markChanged();
    }
  }

  public void setGalaxyShape(int shape) throws InvalidArgumentsException {
    if (shape < 0 || shape > 3) {
      String msg = Language.getString("GameInfo.11"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(shape)); //$NON-NLS-1$
      throw new InvalidArgumentsException(getName() + ": " + msg);
    }
    if (mGalaxyShape != shape) {
      mGalaxyShape = shape;
      this.markChanged();
    }
  }

  public void setGalaxySize(int size) throws InvalidArgumentsException {
    if (size < 0 || size > 2) {
      String msg = Language.getString("GameInfo.12"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(size)); //$NON-NLS-1$
      throw new InvalidArgumentsException(getName() + ": " + msg);
    }
    if (mGalaxySize != size) {
      mGalaxySize = size;
      this.markChanged();
    }
  }

  public void setRandomEvents(boolean active) {
    if (mRandomEvents != active) {
      mRandomEvents = active;
      this.markChanged();
    }
  }

  public void setStrategicalTimer(int timer) {
    if (mStrategicalTimer != timer) {
      mStrategicalTimer = timer;
      this.markChanged();
    }
  }

  public void setTacticalTimer(int timer) {
    if (mTacticalTimer != timer) {
      mTacticalTimer = timer;
      this.markChanged();
    }
  }
}