package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class TForceCntr extends InternalFile {

  public static final String CNTR_FILE_NAME = "TForceCntr";
  private SavGame GAME;

  private int TASKFORCE_CNTR = 0; // for next task force id

  public TForceCntr(SavGame game) {
    this.GAME = game;
  }

  public int getTFCounter() {
    return TASKFORCE_CNTR;
  }

  public void setTFCounter(int cntr) {
    if (TASKFORCE_CNTR != cntr) {
      TASKFORCE_CNTR = cntr;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    TASKFORCE_CNTR = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(TASKFORCE_CNTR, true));
  }

  @Override
  public void clear() {
    TASKFORCE_CNTR = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    try {
      // check whether the id counter is not exceeded
      TaskForceList gWTFORCE = (TaskForceList) GAME.getInternalFile(CSavFiles.GWTForce, true);
      int gwtfMax = gWTFORCE.getMaxTaskForceId();
      TaskForceList gTFORCELIST = (TaskForceList) GAME.getInternalFile(CSavFiles.GTForceList, true);
      int gtfMax = gTFORCELIST.getMaxTaskForceId();
      int max = Integer.max(gwtfMax, gtfMax);

      if (max >= TASKFORCE_CNTR) {
        String msg = Language.getString("TForceCntr.1") //$NON-NLS-1$#
          .replace("%1", Integer.toString(TASKFORCE_CNTR))
          .replace("%2", gwtfMax > gtfMax ? CSavFiles.GWTForce : CSavFiles.GTForceList) //$NON-NLS-1$#
          .replace("%3", Integer.toString(max)); //$NON-NLS-1$#
        msg += " (fixed)";

        // task force counter lists always the next used id
        setTFCounter(max + 1);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
      }
    } catch (Exception e) {
      response = new Vector<String>();

      String msg = Language.getString("TForceCntr.0"); //$NON-NLS-1$
      msg += "\n" + e.getMessage(); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
  }

  @Override
  public int getSize() {
    return 4;
  }
}
