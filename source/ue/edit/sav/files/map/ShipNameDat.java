package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Shipname;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class ShipNameDat extends InternalFile {

  private static final int DEFAULT_NAMEGROUP_CNT = 62;
  private static final int NAMEGROUP_SIZE = 12;
  public static final String SHIPNAMEDAT_FILE_NAME = "shipname.dat";

  private Stbof stbof;
  private Shipname shipName;
  private ArrayList<NameGroup> nameGroups = new ArrayList<NameGroup>();

  public ShipNameDat(Stbof res) throws IOException {
    this.stbof = res;
    this.shipName = (Shipname) res.getInternalFile(CStbofFiles.ShipNameBin, true);
  }

  private static int nextIndex(int current, int len) {
    return current >= len ? 0 : ++current;
  }

  public String nextName(int groupIndex) {
    NameGroup group = nameGroups.get(groupIndex);
    String[] names = shipName.getEntries(groupIndex);

    if (group.nextNameIndex == -1) {
      // generate a random index and initialize fields
      group.firstNameIndex = (int) (Math.random() * names.length);
      group.nextNameIndex = nextIndex(group.firstNameIndex, names.length);
      group.iteration = 0;

      return names[group.firstNameIndex];
    }

    // double check for safety
    if (group.nextNameIndex >= names.length) {
      group.nextNameIndex = 0;
    }

    // update iteration if passed through
    if (group.nextNameIndex == group.firstNameIndex) {
      group.iteration++;
    }

    String name = names[group.nextNameIndex];
    group.nextNameIndex = nextIndex(group.nextNameIndex, names.length);

    if (group.iteration > 0) {
      name += " " + ('A' + group.iteration - 1);
    }

    this.markChanged();
    return name;
  }

  public String nextNameByType(short shipType) throws IOException {
    ShipList shipList = stbof.files().shipList();
    return nextName(shipList.getShipDefinition(shipType).getNameGroup());
  }

  @Override
  public void load(InputStream in) throws IOException {
    nameGroups.clear();

    int size = in.available();
    int groupCnt = size / NAMEGROUP_SIZE;

    if (groupCnt != DEFAULT_NAMEGROUP_CNT) {
      System.out.println("Name group count is modified to: " + groupCnt);
    }

    for (int i = 0; i < groupCnt; ++i) {
      NameGroup nameGroup = new NameGroup(in);
      nameGroups.add(nameGroup);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (NameGroup nameGroup : nameGroups) {
      nameGroup.save(out);
    }
  }

  @Override
  public void clear() {
    nameGroups.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return nameGroups.size() * NAMEGROUP_SIZE;
  }

  private class NameGroup {

    int firstNameIndex; // 04 is initialized to 0xFFFFFFFF
    int nextNameIndex; // 08 is initialized to 0xFFFFFFFF
    int iteration; // 12 is initialized to 0xFFFF0000

    NameGroup(InputStream in) throws IOException {
      firstNameIndex = StreamTools.readInt(in, true);
      nextNameIndex = StreamTools.readInt(in, true);
      iteration = StreamTools.readInt(in, true);
    }

    void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(firstNameIndex, true));
      out.write(DataTools.toByte(nextNameIndex, true));
      out.write(DataTools.toByte(iteration, true));
    }
  }
}
