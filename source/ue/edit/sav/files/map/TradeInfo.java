package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.sav.files.map.data.TradeId;
import ue.edit.sav.files.map.data.TradeRoute;

public class TradeInfo extends InternalFile {

  @Data @AllArgsConstructor
  public class TradeUpdate {
    @Accessors(fluent = true)
    private TradeId id;
    private TradeRoute route;
  }

  private TradeInfoCnt header;

  // sort trade infos to simplify hex lookup
  private TreeMap<TradeId, TradeRoute> m_tradeRoutes = new TreeMap<>();

  public TradeInfo(TradeInfoCnt header) {
    this.header = header;
  }

  public void updateHeader() {
    header.setTradeRouteCount(m_tradeRoutes.size());
  }

  @Override
  public int getSize() {
    return m_tradeRoutes.size() * TradeRoute.SIZE;
  }

  public int numTradeRoutes() {
    return m_tradeRoutes.size();
  }

  public Collection<TradeRoute> listTradeRoutes() {
    return Collections.unmodifiableCollection(m_tradeRoutes.values());
  }

  public TradeRoute getTradeRoute(TradeId tradeRouteId) {
    return m_tradeRoutes.get(tradeRouteId);
  }

  public List<TradeRoute> getSystemRoutes(int systemId) {
    return m_tradeRoutes.values().stream()
      .filter(x -> x.getSourceSystemId() == systemId || x.getTargetSystemId() == systemId)
      .collect(Collectors.toList());
  }

  public List<TradeRoute> getIncomingRoutes(int systemId) {
    return m_tradeRoutes.values().stream()
      .filter(x -> x.getTargetSystemId() == systemId)
      .collect(Collectors.toList());
  }

  public List<TradeRoute> getOutgoingRoutes(int systemId) {
    return m_tradeRoutes.values().stream()
      .filter(x -> x.getSourceSystemId() == systemId)
      .collect(Collectors.toList());
  }

  // @returns the target system ID
  public short cancelTradeRoute(TradeId routeId) {
    TradeRoute route = m_tradeRoutes.get(routeId);
    short systemId = -1;

    if (route != null) {
      systemId = route.getTargetSystemId();
      route.cancel();
    }

    return systemId;
  }

  public void cancelAllTradeRoutes() {
    m_tradeRoutes.values().forEach(TradeRoute::cancel);
  }

  public TradeRoute removeTradeRoute(TradeId tradeRouteId) {
    TradeRoute route = m_tradeRoutes.remove(tradeRouteId);
    if (route != null) {
      updateHeader();
      markChanged();
    }
    return route;
  }

  public void removeTradeRoutes(Collection<TradeId> routes) {
    boolean changed = false;
    for (TradeId trId : routes) {
      changed |= m_tradeRoutes.remove(trId) != null;
    }

    if (changed) {
      updateHeader();
      markChanged();
    }
  }

  public void onRemovedSystem(short systemId) {
    boolean removed = m_tradeRoutes.values().removeIf(x -> {
      short sourceId = x.getSourceSystemId();
      short targetId = x.getTargetSystemId();

      // remove routes of specified system
      if (sourceId == systemId)
        return true;

      // cancel routes to specified system
      if (targetId == systemId)
        x.cancel();

      // update system ids
      if (sourceId > systemId)
        x.setSourceSystemId(--sourceId);
      if (targetId > systemId)
        x.setTargetSystemId(--targetId);
      return false;
    });

    if (removed) {
      updateHeader();
      markChanged();
    }
  }

	public List<TradeUpdate> updateTradeIds() {
    ArrayList<TradeUpdate> updates = new ArrayList<>();
    TreeMap<TradeId, TradeRoute> copy = new TreeMap<>();
    short nbr = 0;

    for (TradeRoute route : m_tradeRoutes.values()) {
      TradeId prevId = route.id();
      if (prevId.number() != nbr) {
        TradeId newId = new TradeId(prevId.race(), nbr);
        route.setId(newId);
        // To not risk any recursive id updates, the returned ids
        // must be sorted. This is ensured by iterating the TreeMap.
        updates.add(new TradeUpdate(prevId, route));
      }

      copy.put(route.id(), route);
      nbr++;
    }

    if (!updates.isEmpty()) {
      m_tradeRoutes = copy;
      markChanged();
    }

		return updates;
	}

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public void load(InputStream in) throws IOException {
    m_tradeRoutes.clear();

    int num = header.getTradeRouteCount();
    int size = in.available();
    if (size != num * TradeRoute.SIZE)
      System.out.println("TrdeInfo: Trade route count doesn't match the entry number!");

    num = Integer.min(size / TradeRoute.SIZE, num);
    for (int i = 0; i < num; ++i) {
      TradeRoute route = new TradeRoute(this);
      route.load(in);
      m_tradeRoutes.put(route.id(), route);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (TradeRoute entry : m_tradeRoutes.values()) {
      entry.save(out);
    }
  }

  @Override
  public void clear() {
    m_tradeRoutes.clear();
    markChanged();
  }

}
