package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.sav.SavGame;
import ue.edit.sav.files.map.data.StellEntry;
import ue.service.Language;
import ue.util.calc.Calc;

/**
 * This class is a representation of the stellInfo file. It holds a list of stellar objects in the
 * game.
 */
public class StellInfo extends InternalFile {

  public class StellarStats {
    public int stellarObjectsTotal = 0;
    // mapped by StellarType
    public HashMap<Integer, Integer> stellarTypes = new HashMap<Integer, Integer>();
    public int wormholesStable = 0;
    public int wormholesInstable = 0;

    public StellarStats() {
      // pre-fill known entries so they aren't missed
      stellarTypes.put(StellarType.BlackHole, 0);
      stellarTypes.put(StellarType.XRayPulsar, 0);
      stellarTypes.put(StellarType.Nebula, 0);
      stellarTypes.put(StellarType.NeutronStar, 0);
      stellarTypes.put(StellarType.WormHole, 0);
      stellarTypes.put(StellarType.RadioPulsar, 0);
    }
  }

  private ArrayList<StellEntry> ENTRY = new ArrayList<StellEntry>();

  @Getter @Accessors(fluent = true)
  private SavGame savGame;
  @Getter @Accessors(fluent = true)
  private Stbof stbof;

  public StellInfo(SavGame sav, Stbof stbof) {
    this.savGame = sav;
    this.stbof = stbof;
  }

  @Override
  public void check(Vector<String> response) {
    Objstruc stell = (Objstruc) stbof.tryGetInternalFile(CStbofFiles.ObjStrucSmt, true);
    if (stell == null) {
      String msg = "Missing " + CStbofFiles.ObjStrucSmt; //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
    }

    for (int i = 0; i < ENTRY.size(); i++) {
      StellEntry ste = ENTRY.get(i);
      if (ste.getName().length() > 39) {
        String msg = Language.getString("StellInfo.0"); //$NON-NLS-1$
        msg = msg.replace("%1", ste.getName()); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }

      checkStellType(stell, ste, response);
    }

    String[] pt = getGraphicFiles();
    for (int i = 0; i < pt.length; i++) {
      if (pt[i].endsWith(".ani")) //$NON-NLS-1$
      {
        AniOrCur AOC = null;
        try {
          AOC = stbof.getAnimation(pt[i], LoadFlags.UNCACHED);
        } catch (Exception fnf) {
          response.add(getGenericErrorString(fnf.getMessage()));
        }

        if (AOC == null) {
          response.add(getLoadAniErrorString(pt[i]));
        }
      } else {
        TargaImage tar = null;
        try {
          tar = stbof.getTargaImage(pt[i], LoadFlags.UNCACHED);
        } catch (IOException fnf) {
          response.add(getGenericErrorString(fnf.getMessage()));
        }

        if (tar == null) {
          String msg = Language.getString("StellInfo.2"); //$NON-NLS-1$
          msg = msg.replace("%1", pt[i]); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      }
    }
  }

  private void checkStellType(Objstruc stell, StellEntry ste, Vector<String> response) {
    int type = ste.getType();
    if (type >= 0 && type < 12)
      return;

    // support modded stellar types (GALM)
    if (stell != null && stell.hasIndex(type))
      return;

    String msg = Language.getString("StellInfo.1"); //$NON-NLS-1$
    msg = msg.replace("%1", ste.getName()); //$NON-NLS-1$
    msg = msg.replace("%2", Integer.toString(type)); //$NON-NLS-1$
    response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
  }

  @Override
  public int getSize() {
    return ENTRY.size() * StellEntry.SIZE;
  }

  /**
   * Creates and adds a new stellar object to the list.
   *
   * @return object index
   */
  public int add(int h_index, int v_index) {
    int h_pos = h_index * 5 + (1 + (int) (Math.random() * 3));
    int v_pos = v_index * 5 + (1 + (int) (Math.random() * 3));

    ENTRY.add(new StellEntry(this, h_pos, v_pos));
    return ENTRY.size() - 1;
  }

  public StellEntry remove(int index) {
    checkIndex(index);
    StellEntry rem = ENTRY.remove(index);

    if (rem != null) {
      markChanged();

      // update all wormhole link indices for the removed array element
      for (int i = 0; i < ENTRY.size(); i++) {
        ENTRY.get(i).onRemovedStellObj(index);
      }
    }

    return rem;
  }

  public void removeAll() {
    if (!ENTRY.isEmpty()) {
      ENTRY.clear();
      markChanged();
    }
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    while (in.available() > 0) {
      StellEntry se = new StellEntry(this);
      se.load(in);
      ENTRY.add(se);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    int size = ENTRY.size();
    for (int i = 0; i < size; i++) {
      StellEntry se = ENTRY.get(i);
      se.save(out);
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  //gets

  /**
   * Gets the stellar object entry of specified index.
   * @param index the index of the stellar object
   * @return the stellar object entry
   */
  public StellEntry getStellarObject(int index) {
    checkIndex(index);
    return ENTRY.get(index);
  }

  /**
   * Returns a list of names
   */
  public String[] getNames() {
    return ENTRY.stream().map(x -> x.getName()).toArray(String[]::new);
  }

  /**
   * Returns a list of graphic files.
   */
  public String[] getGraphicFiles() {
    ArrayList<String> resi = new ArrayList<String>();

    for (int i = 0; i < ENTRY.size(); i++) {
      StellEntry se = ENTRY.get(i);
      boolean isNeb = se.getAnimation().equals("neb-"); //$NON-NLS-1$
      String str = se.getAnimation() + (isNeb ? ".tga" : ".ani"); //$NON-NLS-1$ //$NON-NLS-2$

      if (!resi.contains(str))
        resi.add(str);
    }

    return resi.toArray(new String[0]);
  }

  public StellarStats getStats() {
    StellarStats stats = new StellarStats();
    stats.stellarObjectsTotal = ENTRY.size();
    for (StellEntry entry : ENTRY) {
      stats.stellarTypes.merge(entry.getType(), 1, Calc::sum);
      if (entry.getType() == StellarType.WormHole) {
        if (entry.getLink() != -1)
          stats.wormholesStable++;
        else
          stats.wormholesInstable++;
      }
    }

    return stats;
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("StellInfo.3"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private String getGenericErrorString(String error) {
    String msg = Language.getString("StellInfo.4") //$NON-NLS-1$
      .replace("%1", getName()) //$NON-NLS-1$
      .replace("%2", error); //$NON-NLS-1$
    return msg;
  }

  private String getLoadAniErrorString(String ani) {
    String msg = Language.getString("StellInfo.5") //$NON-NLS-1$
      .replace("%1", getName()) //$NON-NLS-1$
      .replace("%2", ani); //$NON-NLS-1$
    return msg;
  }

  public short getNumberOfEntries() {
    return (short) ENTRY.size();
  }

}
