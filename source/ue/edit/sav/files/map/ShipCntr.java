package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class ShipCntr extends InternalFile {

  public static final String CNTR_FILE_NAME = "ShipCntr";

  private SavGame game;
  private int shipCtr = 0; // for next ship id

  public ShipCntr(SavGame game) {
    this.game = game;
  }

  public int getShipCounter() {
    return shipCtr;
  }

  public void setShipCounter(int cntr) {
    if (shipCtr != cntr) {
      shipCtr = cntr;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    shipCtr = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(shipCtr, true));
  }

  @Override
  public void clear() {
    shipCtr = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    GShipList shipList = game.files().findGShipList().orElse(null);
    if (shipList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ": Missing GShipList!"));
      return;
    }

    // check whether the id counter is not exceeded
    int maxId = shipList.getMaxShipId();

    if (shipCtr <= maxId) {
      String msg = Language.getString("ShipCntr.0") //$NON-NLS-1$#
        .replace("%1", Integer.toString(shipCtr))
        .replace("%2", Integer.toString(maxId));
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));

      setShipCounter(maxId + 1);
    }
  }

  @Override
  public int getSize() {
    return 4;
  }
}
