package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import java.util.TreeMap;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.tools.MapTools;
import ue.exception.KeyNotFoundException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of the starBaseInfo file. It holds a list of outposts/starbases in
 * the game.
 */
public class StarBaseInfo extends InternalFile {

  public class StarbaseStats {
    public int starbasesTotal = 0;
    public int starbasesInBuild = 0;
    public int starbasesDamaged = 0;
    public int outpostsTotal = 0;
    public int outpostsInBuild = 0;
    public int outpostsDamaged = 0;
  }

  private Optional<ShipList> shipList = Optional.empty();
  private SavGame savGame;
  private Stbof stbof;

  // sort outposts by id to simplify hex lookup
  private TreeMap<Short, PostEntry> ENTRY = new TreeMap<Short, PostEntry>();

  @Override
  public void check(Vector<String> response) {
    boolean removed = ENTRY.values().removeIf(x -> x.check(response));
    if (removed) {
      val galInfo = savGame.files().findGalInfo();
      if (galInfo.isPresent())
        galInfo.get().setBaseCount((short) ENTRY.size());
    }
  }

  @Override
  public int getSize() {
    return ENTRY.size() * 92;
  }

  /**
   * @param savGame
   * @throws IOException
   */
  public StarBaseInfo(SavGame savGame, Stbof stbof) {
    this.savGame = savGame;
    this.stbof = stbof;

    if (stbof != null)
      shipList = stbof.files().findShipList();
  }

  /**
   * @param in the InputStream to read from
   * @throws IOException
   */
  @Override
  public void load(InputStream in) throws IOException {
    while (in.available() > 0) {
      PostEntry se = new PostEntry(this, in);
      ENTRY.put(se.ID, se);
    }
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (PostEntry se : ENTRY.values()) {
      se.save(out);
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  public StarbaseStats getStats() {
    StarbaseStats stats = new StarbaseStats();

    for (PostEntry se : ENTRY.values())
      incrementStarbaseStats(stats, se);

    return stats;
  }

  public StarbaseStats getEmpireStats(int empire) {
    StarbaseStats stats = new StarbaseStats();

    for (PostEntry se : ENTRY.values()) {
      int raceIdx = Integer.min(se.OWNER, 5);
      if (raceIdx == empire)
        incrementStarbaseStats(stats, se);
    }

    return stats;
  }

  public StarbaseStats[] getEmpireStats() {
    StarbaseStats[] stats = new StarbaseStats[5];

    for (int empire = 0; empire < stats.length; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  private void incrementStarbaseStats(StarbaseStats stats, PostEntry se) {

    if (se.PRODUCTION_INVESTED < se.PRODUCTION_COST) {
      if (se.ROLE == ShipRole.Starbase)
        stats.starbasesInBuild++;
      else
        stats.outpostsInBuild++;
    } else {
      if (se.ROLE == ShipRole.Starbase)
        stats.starbasesTotal++;
      else
        stats.outpostsTotal++;
    }

    if (shipList.isPresent()) {
      ShipDefinition shipDef = shipList.get().getShipDefinition(se.SHIP_ID);
      int max_hull = shipDef.getHullStrength();
      if (se.HULL < max_hull) {
        if (se.ROLE == ShipRole.Starbase)
          stats.starbasesDamaged++;
        else
          stats.outpostsDamaged++;
      }
    }
  }

  /**
   * Returns a list of names
   */
  public boolean hasKey(short index) {
    return ENTRY.containsKey(index);
  }

  /**
   * Returns a list of names
   */
  public ID[] listEntries() {
    ID[] res = new ID[ENTRY.size()];

    int i = 0;
    for (PostEntry se : ENTRY.values()) {
      res[i++] = new ID(se.NAME, se.ID);
    }

    return res;
  }

  /**
   * Returns the name of the outpost.
   *
   * @param index the index of the outpost
   * @throws KeyNotFoundException
   */
  public String getName(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.NAME;
  }

  /**
   * Returns owner of the outpost.
   *
   * @param index the index of the outpost
   * @throws KeyNotFoundException
   */
  public short getOwner(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.OWNER;
  }

  /**
   * Returns the ShipRole of the outpost(ship).
   *
   * @param index the index of the outpost
   * @throws KeyNotFoundException
   */
  public short getRole(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.ROLE;
  }

  /**
   * Returns ship id (for shiplist.sst) of the outpost.
   *
   * @param index the index of the outpost
   * @throws KeyNotFoundException
   */
  public short getShipID(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.SHIP_ID;
  }

  /**
   * @param index outpost index
   * @return outpost hull
   * @throws KeyNotFoundException
   */
  public int getHull(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.HULL;
  }

  /**
   * @param index outpost index
   * @return production cost
   * @throws KeyNotFoundException
   */
  public double getProductionCost(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.PRODUCTION_COST;
  }

  /**
   * @param index outpost index
   * @return production invested
   * @throws KeyNotFoundException
   */
  public double getProductionInvested(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.PRODUCTION_INVESTED;
  }

  /**
   * @param index outpost index
   * @return scan range
   * @throws KeyNotFoundException
   */
  public double getScanRange(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return pe.SCAN_RANGE;
  }

  // sets

  /**
   * Sets the name of the outpost.
   *
   * @param index the index of the outpost
   * @throws KeyNotFoundException
   */
  public void setName(short index, String name) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);

    if (!pe.NAME.equals(name)) {
      pe.NAME = name;
      markChanged();
    }
  }

  /**
   * Sets the owner of the outpost.
   *
   * @param index the index of the outpost
   * @param owner the owner
   * @throws KeyNotFoundException
   */
  public void setOwner(short index, short owner) throws KeyNotFoundException {
    checkOwner(owner);
    PostEntry pe = getStarBase(index);

    if (pe.OWNER != owner) {
      pe.OWNER = owner;
      markChanged();
    }
  }

  /**
   * Sets the ShipRole of the outpost(ship).
   *
   * @param index the index of the outpost
   * @param role  ship role
   * @throws KeyNotFoundException
   */
  public void setRole(short index, short role) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);

    if (pe.ROLE != role) {
      pe.ROLE = role;
      markChanged();
    }
  }

  /**
   * @param index outpost index
   * @param id    ship id
   * @throws KeyNotFoundException
   */
  public void setShipID(short index, short id) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    if (pe.SHIP_ID != id) {
      pe.SHIP_ID = id;
      markChanged();
    }
  }

  public void setHull(short index, int hull) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    if ((pe.HULL != hull) && (hull >= 1)) {
      pe.HULL = hull;
      markChanged();
    }
  }

  /**
   * Sets outpost position and id. Outpost ids are based on sector coordinates cause there might
   * never be more than one outpost in same sector.
   *
   * @param index       the index of the outpost
   * @param targetIndex the new outpost id, should equal the sector index
   * @param vert        vertical position
   * @param hori        horizontal position
   * @return new outpost id
   * @throws KeyNotFoundException
   */
  public short move(short index, short targetIndex, int vert, int hori) throws KeyNotFoundException {
    if ((vert < 0) || (hori < 0)) {
      String msg = Language.getString("StarBaseInfo.5"); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }

    PostEntry pe = getStarBase(index);
    if (targetIndex != index) {
      // check whether at the target location there already exists an outpost
      PostEntry targetPe = ENTRY.get(targetIndex);
      if (targetPe != null) {
        String msg = Language.getString("StarBaseInfo.6"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", pe.NAME); //$NON-NLS-1$
        msg = msg.replace("%3", targetPe.NAME); //$NON-NLS-1$
        msg = msg.replace("%4", Short.toString(pe.ID)); //$NON-NLS-1$
        throw new KeyNotFoundException(msg);
      }

      pe.ID = targetIndex;
    }

    // move outpost
    pe.H_SEC = hori;
    pe.V_SEC = vert;
    ENTRY.remove(index);
    ENTRY.put(targetIndex, pe);

    markChanged();
    return targetIndex;
  }

  /**
   * Switches outpost position and id. The id is or should be computed by the sector coordinates,
   * therefore it is switched. If however they don't, better don't mess with other possible id
   * conflicts.
   *
   * @param index1 the first outpost to exchange
   * @param index2 the second outpost to exchange
   * @throws KeyNotFoundException
   */
  public void switchPositionAndId(short index1, short index2) throws KeyNotFoundException {
    PostEntry pe1 = getStarBase(index1);
    PostEntry pe2 = getStarBase(index2);

    // switch outpost position and id
    int vert = pe1.V_SEC;
    int hori = pe1.H_SEC;
    pe1.ID = index2;
    pe1.V_SEC = pe2.V_SEC;
    pe1.H_SEC = pe2.H_SEC;
    pe2.ID = index1;
    pe2.V_SEC = vert;
    pe2.H_SEC = hori;

    // update outpost entries
    ENTRY.remove(index1);
    ENTRY.remove(index2);
    ENTRY.put(index1, pe2);
    ENTRY.put(index2, pe1);

    markChanged();
  }

  public int[] getPosition(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);
    return new int[]{pe.H_SEC, pe.V_SEC};
  }

  /**
   * @param index outpost index
   * @param cost  production cost
   * @throws KeyNotFoundException
   */
  public void setProductionCost(short index, double cost) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);

    if (pe.PRODUCTION_COST != cost) {
      pe.PRODUCTION_COST = cost;
      markChanged();
    }
  }

  /**
   * @param index    outpost index
   * @param invested production invested
   * @throws KeyNotFoundException
   */
  public void setProductionInvested(short index, double invested) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);

    if (pe.PRODUCTION_INVESTED != invested) {
      pe.PRODUCTION_INVESTED = invested;
      markChanged();
    }
  }

  /**
   * @param index outpost index
   * @param range scan range
   * @throws KeyNotFoundException
   */
  public void setScanRange(short index, int range) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);

    if (pe.SCAN_RANGE != range) {
      pe.SCAN_RANGE = range;
      markChanged();
    }
  }

  /**
   * Returns the descriptive name of the sector where the starbase is
   *
   * @param index the index of the outpost
   * @throws KeyNotFoundException
   */
  public String getDescription(short index) throws KeyNotFoundException {
    PostEntry pe = getStarBase(index);

    try {
      byte[] but = new byte[1];
      but[0] = (byte) (97 + pe.H_SEC);

      String ret = Language.getString("StarBaseInfo.7"); //$NON-NLS-1$
      ret = ret.replace("%1",
          DataTools.fromNullTerminatedBotfString(but).toUpperCase() + (pe.V_SEC + 1)); //$NON-NLS-1$
      return ret;
    } catch (Exception ex) {
      ex.printStackTrace();
      return ""; //$NON-NLS-1$
    }
  }

  private PostEntry getStarBase(short index) throws KeyNotFoundException {
    PostEntry pe = ENTRY.get(index);
    if (pe == null) {
      String msg = Language.getString("StarBaseInfo.8"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new KeyNotFoundException(msg);
    }
    return pe;
  }

  private void checkOwner(int owner) {
    if ((owner < 0) || (owner > 4)) {
      String msg = Language.getString("StarBaseInfo.9"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(owner)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  /**
   * @param index the new outpost id, should equal the sector index
   * @param role  outpost(6) or starbase(7)
   * @param vert  vertical pos
   * @param hori  horizontal pos
   * @throws KeyNotFoundException
   */
  public void add(short index, short role, int vert, int hori) throws KeyNotFoundException {
    PostEntry pe = ENTRY.get(index);
    if (pe != null) {
      String msg = Language.getString("StarBaseInfo.10"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", "new"); //$NON-NLS-1$
      msg = msg.replace("%3", pe.NAME); //$NON-NLS-1$
      msg = msg.replace("%4", Short.toString(pe.ID)); //$NON-NLS-1$
      throw new KeyNotFoundException(msg);
    }

    pe = new PostEntry(this, index, role, vert, hori);
    ENTRY.put(index, pe);

    markChanged();
  }

  /**
   * Removes the outpost and resets ids of others
   *
   * @param index outpost index
   */
  public void remove(short index) {
    if (ENTRY.remove(index) != null)
      markChanged();
  }

  public void removeAll() {
    if (!ENTRY.isEmpty()) {
      ENTRY.clear();
      markChanged();
    }
  }

  /**
   * @return Number of outposts in this file
   */
  public int getNumberOfEntries() {
    return ENTRY.size();
  }

  private class PostEntry {

    public String NAME;
    public short OWNER;
    public short ID; // ensures only one outpost per sector
    public short ROLE;
    public short SHIP_ID;
    // position in sectors
    public int V_SEC;
    public int H_SEC;
    public double PRODUCTION_COST;
    public double PRODUCTION_INVESTED;
    public int SCAN_RANGE;
    public int HULL;
    private byte[] UNKNOWN_2 = new byte[12];

    private StarBaseInfo sbInfo;

    // constructors
    public PostEntry(StarBaseInfo starBaseInfo, InputStream in) throws IOException {
      sbInfo = starBaseInfo;

      // name 0-40
      NAME = StreamTools.readNullTerminatedBotfString(in, 40);
      // owner 40-2
      OWNER = StreamTools.readShort(in, true);
      // ID 42-2
      ID = StreamTools.readShort(in, true);
      // role 44-2
      ROLE = StreamTools.readShort(in, true);
      // ship id 46-2
      SHIP_ID = StreamTools.readShort(in, true);
      // vertical 48-4
      V_SEC = StreamTools.readInt(in, true);
      // horizontal 52-4
      H_SEC = StreamTools.readInt(in, true);
      // production cost 56-8
      PRODUCTION_COST = StreamTools.readDouble(in, true);
      // production invested 64-8
      PRODUCTION_INVESTED = StreamTools.readDouble(in, true);
      // unknown 72-4
      SCAN_RANGE = StreamTools.readInt(in, true);
      // hull 76-4
      HULL = StreamTools.readInt(in, true);
      // unknown 80-12
      StreamTools.read(in, UNKNOWN_2);
    }

    public boolean check(Vector<String> response) {
      if (shipList.isPresent() && !shipList.get().hasIndex(SHIP_ID)) {
        String msg = "%1 has invalid ship type %2";
        msg = msg.replace("%1", descriptor());
        msg = msg.replace("%2", Short.toString(SHIP_ID));

        val sector = savGame.files().findSectorLst().orElse(null);
        if (sector != null) {
          msg += " (removed)";
          int sectorIdx = sector.getSectorIndex(V_SEC, H_SEC);
          sector.setStarbase(sectorIdx, (short) -1);
        }

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        return true;
      }

      if (NAME.length() > 39) {
        String msg = Language.getString("StarBaseInfo.0"); //$NON-NLS-1$
        msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      if ((OWNER < 0) || (OWNER > 4)) {
        String msg = Language.getString("StarBaseInfo.1"); //$NON-NLS-1$
        msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
        msg = msg.replace("%2", Short.toString(OWNER)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      if ((ROLE < 6) || (ROLE > 7)) {
        String msg = Language.getString("StarBaseInfo.2"); //$NON-NLS-1$
        msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
        msg = msg.replace("%2", Short.toString(ROLE)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      if (HULL <= 0) {
        String msg = Language.getString("StarBaseInfo.4"); //$NON-NLS-1$
        msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
        msg += " (fixed)";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

        HULL = 1;
        markChanged();
      }

      if (shipList.isPresent()) {
        ShipDefinition shipDef = shipList.get().getShipDefinition(SHIP_ID);
        int hull = shipDef.getHullStrength();

        if (hull < HULL) {
          String msg = Language.getString("StarBaseInfo.3"); //$NON-NLS-1$
          msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
          msg += " (fixed)";
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

          HULL = hull;
          markChanged();
        }
      }

      return false;
    }

    public PostEntry(StarBaseInfo starBaseInfo, short id, short role, int v, int h) {
      sbInfo = starBaseInfo;
      OWNER = 0;
      NAME = "";
      ID = id;
      ROLE = role;
      SHIP_ID = 0;
      V_SEC = v;
      H_SEC = h;
      HULL = 1;
      PRODUCTION_COST = 1;
      PRODUCTION_INVESTED = 1;
      SCAN_RANGE = (role == 6) ? 1 : 2;
    }

    public String descriptor() {
      val raceRst = sbInfo.stbof.files().findRaceRst();
      String race = raceRst.isPresent() ? raceRst.get().getOwnerName(OWNER) : "race '" + Integer.toString(OWNER) + "'";
      return race + " " + inlineDescriptor();
    }

    public String inlineDescriptor() {
      short sbRole = ShipRole.isStarBase(ROLE) ? ROLE : ShipRole.Starbase;
      String sbType = LexHelper.mapShipRole(sbRole);
      String sector = MapTools.sectorPos(H_SEC, V_SEC);
      return sbType + " \"" + NAME + "\" " + Integer.toString(ID) + " at " + sector;
    }

    // save
    public void save(OutputStream out) throws IOException {
      // name 0-40
      byte[] b = DataTools.toByte(NAME, 40, 39);
      out.write(b);
      // owner
      b = DataTools.toByte(OWNER, true);
      out.write(b);
      // ID 42-2
      b = DataTools.toByte(ID, true);
      out.write(b);
      // role
      b = DataTools.toByte(ROLE, true);
      out.write(b);
      // ship id
      b = DataTools.toByte(SHIP_ID, true);
      out.write(b);
      // vertical
      b = DataTools.toByte(V_SEC, true);
      out.write(b);
      // horizontal
      b = DataTools.toByte(H_SEC, true);
      out.write(b);
      // production cost
      b = DataTools.toByte(PRODUCTION_COST, true);
      out.write(b);
      // production invested
      b = DataTools.toByte(PRODUCTION_INVESTED, true);
      out.write(b);
      // unknown
      out.write(DataTools.toByte(SCAN_RANGE, true));
      // hull
      out.write(DataTools.toByte(HULL, true));
      // unknown
      out.write(UNKNOWN_2);
    }
  }
}
