package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.common.CommonStrings;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.exception.InvalidArgumentsException;

/**
 * Used for loading SphOfIn0-SphOfIn4 files.
 */
public class SphereOfInfluence extends InternalFile {

  private byte[] map;

  public SphereOfInfluence(SavGame game) {
  }

  public int getInfluence(int sectorIndex) throws InvalidArgumentsException {
    testIndex(sectorIndex);

    return map[sectorIndex] & 0xFF;
  }

  public void setInfluence(int sectorIndex, int influence) throws InvalidArgumentsException {
    testIndex(sectorIndex);

    if ((map[sectorIndex] & 0xFF) == influence) {
      return;
    }

    map[sectorIndex] = (byte) influence;
    this.markChanged();
  }

  private void testIndex(int sectorIndex) throws InvalidArgumentsException {
    if (sectorIndex < 0 || sectorIndex >= map.length) {
      throw new InvalidArgumentsException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(sectorIndex)));
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    map = new byte[in.available()];

    int read = 0;
    while (in.available() > 0) {
      read += in.read(map, read, map.length - read);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(map);
  }

  @Override
  public void clear() {
    map = null;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return map != null ? map.length : 0;
  }
}
