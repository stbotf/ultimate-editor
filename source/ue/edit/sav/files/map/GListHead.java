package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class GListHead extends InternalFile {

  private SavGame game;

  private int load_routine;               // 4  0x00-0x03: GShipHead: offset sub_449C00, others: offset loc_448D00
  private int sub_492F90;                 // 8  0x04-0x07: call_get_code_address?
  private int sub_492E90;                 // 12 0x08-0x0B: call__EnterCriticalSection_
  private int sub_492E60;                 // 16 0x0C-0x0F: system_memory__clear_file?
  private int SLOT_SIZE;                  // 20 0x10-0x13:
  private int TOTAL_SLOTS;                // 24 0x14-0x17:
  private int OCCUPIED_SLOTS;             // 28 0x18-0x1B:
  private int flags;                      // 32 0x1C-0x1F: GTFStasisHd/GWTForceHd = 0x1, GShipHead/GTForceHd = 0x81
  private int random_seed;                // 36 0x20-0x23: overridden each app launch
  private int contentAddress;             // 40 0x24-0x27: only valid for the current running instance

  @Getter private int initialSlotsTotal = 0;

  public GListHead(SavGame game) {
    this.game = game;
  }

  @Override
  public void load(InputStream in) throws IOException {
    load_routine = StreamTools.readInt(in, true);
    sub_492F90 = StreamTools.readInt(in, true);
    sub_492E90 = StreamTools.readInt(in, true);
    sub_492E60 = StreamTools.readInt(in, true);
    SLOT_SIZE = StreamTools.readInt(in, true);
    TOTAL_SLOTS = StreamTools.readInt(in, true);
    OCCUPIED_SLOTS = StreamTools.readInt(in, true);
    flags = StreamTools.readInt(in, true);
    random_seed = StreamTools.readInt(in, true);
    contentAddress = StreamTools.readInt(in, true);
    initialSlotsTotal = TOTAL_SLOTS;

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(load_routine, true));
    out.write(DataTools.toByte(sub_492F90, true));
    out.write(DataTools.toByte(sub_492E90, true));
    out.write(DataTools.toByte(sub_492E60, true));
    out.write(DataTools.toByte(SLOT_SIZE, true));
    out.write(DataTools.toByte(TOTAL_SLOTS, true));
    out.write(DataTools.toByte(OCCUPIED_SLOTS, true));
    out.write(DataTools.toByte(flags, true));
    out.write(DataTools.toByte(random_seed, true));
    out.write(DataTools.toByte(contentAddress, true));
  }

  @Override
  public void clear() {
    load_routine = 0;
    sub_492F90 = 0;
    sub_492E90 = 0;
    sub_492E60 = 0;
    SLOT_SIZE = 0;
    TOTAL_SLOTS = 0;
    OCCUPIED_SLOTS = 0;
    flags = 0;
    random_seed = 0;
    contentAddress = 0;
    initialSlotsTotal = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    switch (NAME) {
      case CSavFiles.GShipHead:
        checkHeaderIntegrity(game.files().findGShipList().orElse(null), response);
        break;
      case CSavFiles.GTForceHd:
        checkHeaderIntegrity(game.files().findGtfList().orElse(null), response);
        break;
      case CSavFiles.GWTForceHd:
        checkHeaderIntegrity(game.files().findGwtForce().orElse(null), response);
        break;
      case CSavFiles.GTFStasisHd:
        // not yet supported
        break;
    }
  }

  private void checkHeaderIntegrity(GList list, Vector<String> response) {
    if (list == null)
      return;

    int entryCount = list.getEntryCount();
    if (OCCUPIED_SLOTS != entryCount) {
      String msg = "Used slot count %1 doesn't match the entry count %2 of %3! (fixed)";
      msg = msg.replace("%1", Integer.toString(OCCUPIED_SLOTS));
      msg = msg.replace("%2", Integer.toString(entryCount));
      msg = msg.replace("%3", list.getName());
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      setOccupiedSlotNum(entryCount);
    }

    if (TOTAL_SLOTS < OCCUPIED_SLOTS) {
      String msg = "Total slot count %1 is lower than the used count %2! (fixed)";
      msg = msg.replace("%1", Integer.toString(TOTAL_SLOTS));
      msg = msg.replace("%2", Integer.toString(OCCUPIED_SLOTS));
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      TOTAL_SLOTS = OCCUPIED_SLOTS;
      markChanged();
    }

    int entrySize = list.getEntrySize();
    if (SLOT_SIZE != entrySize) {
      String msg = "Slot size %1 doesn't match the entry size %2 of %3! (fixed)";
      msg = msg.replace("%1", Integer.toString(SLOT_SIZE));
      msg = msg.replace("%2", Integer.toString(entrySize));
      msg = msg.replace("%3", list.getName());
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      SLOT_SIZE = entrySize;
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return 40;
  }

  public int getOccupiedSlotNum() {
    return OCCUPIED_SLOTS;
  }

  public void setOccupiedSlotNum(int num) {
    if (OCCUPIED_SLOTS != num) {
      // increase slots in steps of 16
      if (num > TOTAL_SLOTS)
        TOTAL_SLOTS = (num / 16 + 1) * 16;

      OCCUPIED_SLOTS = num;
      markChanged();
    }
  }

  public int getSlotSize() {
    return SLOT_SIZE;
  }

  public void setSlotSize(int size) {
    if (SLOT_SIZE != size) {
      SLOT_SIZE = size;
      markChanged();
    }
  }

  public int getTotalSlotsNum() {
    return TOTAL_SLOTS;
  }

  public void setTotalSlotNum(int num) {
    if (TOTAL_SLOTS != num) {
      TOTAL_SLOTS = Integer.max(OCCUPIED_SLOTS, num);
      markChanged();
    }
  }
}
