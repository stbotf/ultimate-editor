package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.sav.SavGame;

/**
 * Base class for GWTForce, GTForceList and GShipList.
 */
public abstract class GList extends InternalFile {

  @Getter @Accessors(fluent = true) protected SavGame sav;
  @Getter @Accessors(fluent = true) protected Stbof stbof;
  @Getter protected GListHead header;

  // for integrity check detection
  @Getter protected int entrySize = 0;
  @Getter protected int readableSlots = 0;
  @Getter protected int usedSlots = 0;

  public GList(GListHead header, SavGame game, Stbof stbof, int entrySize) {
    this.header = header;
    this.sav = game;
    this.stbof = stbof;
    this.entrySize = entrySize;
  }

  @Override
  public void load(InputStream in) throws IOException {
    readableSlots = in.available() / entrySize;

    int num = header.getOccupiedSlotNum();
    usedSlots = Math.min(num, readableSlots);
    markSaved();
  }

  @Override
  public void clear() {
    readableSlots = 0;
    usedSlots = 0;
    markChanged();
  }

  public abstract int getEntryCount();

  @Override
  public void check(Vector<String> response) {
    int initialSlotsTotal = header.getInitialSlotsTotal();
    if (initialSlotsTotal != this.readableSlots) {
      String msg = "%1 listed a total slot count of %2, but actually there were %3 readable slots! (fixed)";
      msg = msg.replace("%1", header.getName());
      msg = msg.replace("%2", Integer.toString(initialSlotsTotal));
      msg = msg.replace("%3", Integer.toString(this.readableSlots));
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      // just disable the error and mark changed
      // missing slots are automatically added on save
      this.readableSlots = initialSlotsTotal;
      this.markChanged();
    }
  }
}
