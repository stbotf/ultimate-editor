package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class TskSyCnt extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.TskSy_Suffix;
  public static final String TSKSY_FILE_NAME = CSavFiles.TskSy;

  private SavGame game;
  private int systemCnt = 0;

  public TskSyCnt(SavGame game) {
    this.game = game;
  }

  public String getTskSyName() {
    return NAME.substring(0, NAME.length() - CNT_SUFFIX.length());
  }

  public int getSystemCount() {
    return systemCnt;
  }

  public void setSystemCount(int cnt) {
    if (systemCnt != cnt) {
      systemCnt = cnt;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    systemCnt = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(systemCnt, true));
  }

  @Override
  public void clear() {
    systemCnt = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    TskSy tskSy = (TskSy) game.tryGetInternalFile(getTskSyName(), false);
    int cnt = tskSy.getSystemCount();

    if (systemCnt != cnt) {
      String msg = "System number %1 doesn't match the entry number %2! (fixed)";
      msg = msg.replace("%1", Integer.toString(systemCnt));
      msg = msg.replace("%2", Integer.toString(cnt));
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    setSystemCount(cnt);
  }

  @Override
  public int getSize() {
    return 4;
  }
}
