package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeSet;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.CollectionTools;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class TskSy extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.TskSy_Suffix;
  public static final String FILE_NAME = CSavFiles.TskSy;

  private TskSyCnt header;
  private SavGame game;

  // sort system ids to simplify hex lookup
  private TreeSet<Short> m_systemIds = new TreeSet<>(); // numSystems * 2

  public TskSy(int taskId, TskSyCnt header, SavGame game) {
    this.header = header;
    this.game = game;
  }

  private void updateHeader() {
    header.setSystemCount(m_systemIds.size());
  }

  public int getSystemCount() {
    return m_systemIds.size();
  }

  public short[] getSystemIds() {
    return CollectionTools.toShortArray(m_systemIds);
  }

  private boolean differs(short[] systemIds) {
    if (systemIds.length != m_systemIds.size())
      return true;

    for (short systemId : systemIds) {
      if (!m_systemIds.contains(systemId))
        return true;
    }

    return false;
  }

  public void setSystemIds(short[] systemIds) {
    // check if changed
    if (!differs(systemIds))
      return;

    // update the system ids
    m_systemIds.clear();
    for (short id : systemIds) {
      m_systemIds.add(id);
    }

    markChanged();
  }

  public void addSystem(short systemId) {
    m_systemIds.add(systemId);
    updateHeader();
    markChanged();
  }

  public boolean removeSystem(short systemId) {
    if (m_systemIds.remove(systemId)) {
      updateHeader();
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllSystems() {
    if (!m_systemIds.isEmpty()) {
      m_systemIds.clear();
      updateHeader();
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    m_systemIds.clear();

    int numSystems = header.getSystemCount();
    int size = in.available();
    if (size != numSystems << 1)
      System.out.println(getName() + ": System number doesn't match the entry number!");

    numSystems = Integer.min(size >> 1, numSystems);
    for (int i = 0; i < numSystems; ++i) {
      m_systemIds.add(StreamTools.readShort(in, true));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (short sysId : m_systemIds) {
      out.write(DataTools.toByte(sysId, true));
    }
  }

  @Override
  public void check(Vector<String> response) {
    SystInfo systInfo = (SystInfo) game.tryGetInternalFile(CSavFiles.SystInfo, true);
    if (systInfo == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, "Missing SystInfo!"));
      return;
    }

    boolean any = m_systemIds.removeIf(sysId ->
    {
      if (!systInfo.hasSystem(sysId)) {
        String msg = "Found system id %1, that is missing from %2. (removed)";
        msg = msg.replace("%1", Integer.toString(sysId));
        msg = msg.replace("%2", CSavFiles.SystInfo);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        return true;
      }
      return false;
    });

    if (any) {
      updateHeader();
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return m_systemIds.size() * 2;
  }

  public void onRemovedSystem(short systemId) {
    TreeSet<Short> copy = new TreeSet<>();
    boolean changed = false;

    for (short sysId : m_systemIds) {
      if (sysId > systemId) {
        // decremeted
        copy.add(--sysId);
        changed = true;
      }
      else if (sysId == systemId) {
        // removed
        changed = true;
      }
      else {
        // unchanged
        copy.add(sysId);
      }
    }

    if (changed) {
      m_systemIds = copy;
      header.setSystemCount(m_systemIds.size());
      markChanged();
    }
  }

  @Override
  public void clear() {
    m_systemIds.clear();
    updateHeader();
    markChanged();
  }

}
