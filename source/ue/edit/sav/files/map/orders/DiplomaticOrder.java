package ue.edit.sav.files.map.orders;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class DiplomaticOrder {

  public static final int SIZE = 1164; // 0x48C

  public interface Type {

    public static final int Send = 0; // offer treaty incl. alliance, demand money
    public static final int Reject = 2; // reject alliance or war pact
    public static final int DeclareWar = 6;
    public static final int EncourageSabotage = 7;
    public static final int SendCredits = 8;
  }

  public interface Action {

    // None = invalid order for that no decision is saved yet
    // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=58916#p58916
    public static final int None = -1;
    public static final int Announcement = 0; // declare war, encourage sabotage
    public static final int Peace = 1; // offer treaty (non-aggression, firendship, affiliation)
    public static final int Alliance = 2; // offer/reject alliance
    public static final int WarPact = 3; // offer war pact
    public static final int DemandMoney = 4;
    public static final int SendCredits = 8;
  }

  private int type;                       // 0x0   +  4 =  4 Type, diplomatic order type
  private int addr_1;                     // 0x4   +  4 =  8
  private String recipientName;           // 0x8   + 40 = 48
  private int playerEvent;                // 0x30  +  4 = 52 0/1 whether the player gets to see a message in the event log
  private int unknown_2;                  // 0x34  +  4 = 56
  private int action;                     // 0x38  +  4 = 60 Action, again some diplomatic action related type
  private short sourceRace;               // 0x3C  +  2 = 62 the race that issued the diplomatic action
  private short targetRace;               // 0x3E  +  2 = 64 the race to that the diplomatic action is directed
  private byte unknown_3;                 // 0x40  +  1 = 65
  private byte[] empty_2 = new byte[11];  // 0x41  + 11 = 76
  private int creditGift;                 // 0x4C  +  4 = 80 0/1 whether credits are sent
  private int creditDemand;               // 0x50  +  4 = 84 0/1 whether credits are requested
  private byte[] empty_3 = new byte[8];   // 0x54  +  8 = 92
  private int creditsSent;                // 0x5C  +  4 = 96 the credits being sent
  private int creditsRequested;           // 0x60  +  4 = 100 the credits that is asked for
  private byte[] empty_4 = new byte[20];  // 0x64  + 20 = 120
  private short unknown_4;                // 0x78  +  2 = 122 always -1
  private short empty_5;                  // 0x7A  +  2 = 124
  private short responderRace;            // 0x7C  +  2 = 126 set to the race that reacted on some message or event, or -1
  private short empty_6;                  // 0x7E  +  2 = 128
  private int turn;                       // 0x80  +  4 = 132 the turn the message got sent, likely set when a response is expected
  private int hasText;                    // 0x84  +  4 = 136
  private byte[] text = new byte[1024];   // 0x88 +1024 = 1160
  private int empty_7;                    // 0x488 +  4 = 1164

  private Order order;

  DiplomaticOrder(Order order, InputStream in, int size) throws IOException {
    if (size != SIZE)
      throw new IOException("Encountered invalid DiplomaticOrder size of: '" + size + "'!");

    this.order = order;
    type = StreamTools.readInt(in, true);                             // 0-4
    addr_1 = StreamTools.readInt(in, true);                           // 4-4
    recipientName = StreamTools.readNullTerminatedBotfString(in, 40); // 8-40
    playerEvent = StreamTools.readInt(in, true);                      // 48-4
    unknown_2 = StreamTools.readInt(in, true);                        // 52-4
    action = StreamTools.readInt(in, true);                           // 56-4
    sourceRace = StreamTools.readShort(in, true);                     // 60-2
    targetRace = StreamTools.readShort(in, true);                     // 62-2
    unknown_3 = (byte) in.read();                                     // 64-1
    StreamTools.read(in, empty_2);                                    // 65-11
    creditGift = StreamTools.readInt(in, true);                       // 76-4
    creditDemand = StreamTools.readInt(in, true);                     // 80-4
    StreamTools.read(in, empty_3);                                    // 84-8
    creditsSent = StreamTools.readInt(in, true);                      // 92-4
    creditsRequested = StreamTools.readInt(in, true);                 // 96-4
    StreamTools.read(in, empty_4);                                    // 100-20
    unknown_4 = StreamTools.readShort(in, true);                      // 120-2
    empty_5 = StreamTools.readShort(in, true);                        // 122-2
    responderRace = StreamTools.readShort(in, true);                  // 124-2
    empty_6 = StreamTools.readShort(in, true);                        // 126-2
    turn = StreamTools.readInt(in, true);                             // 128-4
    hasText = StreamTools.readInt(in, true);                          // 132-4
    StreamTools.read(in, text);                                       // 136-1024
    empty_7 = StreamTools.readInt(in, true);                          // 1160-4
  }

  public int getSize() {
    return SIZE;
  }

  public void save(OutputStream out) throws IOException {
    // 0-4
    out.write(DataTools.toByte(type, true));
    // 4-4
    out.write(DataTools.toByte(addr_1, true));
    // 8-40
    out.write(DataTools.toByte(recipientName, 40, 39));
    // 48-4
    out.write(DataTools.toByte(playerEvent, true));
    // 52-4
    out.write(DataTools.toByte(unknown_2, true));
    // 56-4
    out.write(DataTools.toByte(action, true));
    // 60-2
    out.write(DataTools.toByte(sourceRace, true));
    // 62-2
    out.write(DataTools.toByte(targetRace, true));
    // 64-1
    out.write(unknown_3);
    // 65-11
    out.write(empty_2);
    // 76-4
    out.write(DataTools.toByte(creditGift, true));
    // 80-4
    out.write(DataTools.toByte(creditDemand, true));
    // 84-8
    out.write(empty_3);
    // 92-4
    out.write(DataTools.toByte(creditsSent, true));
    // 96-4
    out.write(DataTools.toByte(creditsRequested, true));
    // 100-20
    out.write(empty_4);
    // 120-2
    out.write(DataTools.toByte(unknown_4, true));
    // 122-2
    out.write(DataTools.toByte(empty_5, true));
    // 124-2
    out.write(DataTools.toByte(responderRace, true));
    // 126-2
    out.write(DataTools.toByte(empty_6, true));
    // 128-4
    out.write(DataTools.toByte(turn, true));
    // 132-4
    out.write(DataTools.toByte(hasText, true));
    // 136-1024
    out.write(text);
    // 1160-4
    out.write(DataTools.toByte(empty_7, true));
  }

  // @returns whether the order is valid = true or should be removed = false
  public boolean check(Vector<String> response) {

    // check for invalid actions
    if (action == Action.None) {
      String msg = order.descriptor(null);

      if (type == Type.DeclareWar) {
        // fix invalid war declaration action
        // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=58916#p58916
        action = Action.Announcement;
        order.file().markChanged();

        msg += " war declaration missed the action. (fixed)";
        response.add(order.file().getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        return true;
      } else {
        msg += " of type %1 missed the action. (removed)";
        msg = msg.replace("%1", Integer.toString(type));
        response.add(order.file().getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        return false;
      }
    }

    return true;
  }

}
