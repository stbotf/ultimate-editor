package ue.edit.sav.files.map.orders;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import com.google.common.io.BaseEncoding;
import lombok.Getter;
import lombok.val;
import ue.UE;
import ue.UE.DebugFlags;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// only the immediate build order is listed
@Getter
public class EconomicOrder {

  private static final String ClassName = EconomicOrder.class.getSimpleName();
  private static final int SIZE = 52;

  public interface Type {

    // refer https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54283#p54283
    public static final int BUILD = 0;              // build structure or ship
    public static final int BUY = 1;
    public static final int UPGRADE = 2;            // upgrade structure
    public static final int SCRAP = 3;              // scrap structure
    public static final int TERRAFORMED = 5;
    public static final int TRADE_ROUTE = 7;
    public static final int SEND_CREDITS = 8;       // (transfer credits) sometimes coupled with diplomatic orders
    public static final int TRANFER_DILITHIUM = 9;  // unfinished
    public static final int TRANFER_FOOD = 10;      // unfinished
    public static final int TRANFER_ENERGY = 11;    // unfinished
    public static final int COLONIZED = 12;
  }

  // sub type
  // refer https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54283#p54283
  public interface Action {

    public static final int SEND_CREDITS = 0;
    public static final int BUILD_STRUCTURE = 1;
    public static final int UPGRADE_STRUCTURE = 2;
    public static final int BUILD_SHIP = 3;
    public static final int BUILD_TRADE_GOODS = 5;
    public static final int MORALE_UNREST = 7;
  }

  private int type;                       // 04 the economic order type
  private short systemId;                 // 06 system or star id the order is assigned to or -1
  private short unknown_1_flags;          // 08 always 0x0080 little endian
  private short structureIndex;           // 10 structure index to build or 0xFF7F (max signed short)
  private short upgradeIndex;             // 12 to what structure index to upgrade or 0xFF7F (max signed short)
  private int unknown_2;                  // 16 usually 0x22000000, upgrade 0x09000000
  private int action;                     // 20 the economic order action
  private short shipModelId;              // 22 the ship model to build or -1
  private short unknown_3;                // 24 for trade goods 0xFF00
  private byte[] unknown_4 = new byte[8]; // 32 always 0xFF it seems
  private int empty_1;                    // 36 always 0x00 it seems
  private int creditsSpent;               // 40 e.g. what credits are sent
  private int empty_2;                    // 44 always 0x00 it seems
  private short targetRaceId;             // 46 the race an action like a gift is directed at
  private byte[] empty_3 = new byte[6];   // 52 always 0x00 it seems

  private Order order;

  EconomicOrder(Order order, InputStream in, int size) throws IOException {
    if (size != 52)
      throw new IOException(ClassName + ": Encountered invalid size of: '" + size + "'!");

    this.order = order;
    load(in);
  }

  public boolean setSystemId(short systemId) {
    if (this.systemId != systemId) {
      this.systemId = systemId;
      order.file().markChanged();
      return true;
    }
    return false;
  }

  public boolean onRemovedSystem(short systemId) {
    // unset matching system ids
    if (this.systemId == systemId) {
      this.systemId = -1;
      order.file().markChanged();
      return true;
    }
    // update successive system ids
    else if (this.systemId > systemId) {
      this.systemId--;
      order.file().markChanged();
    }
    return false;
  }

  public int getSize() {
    return SIZE;
  }

  public void load(InputStream in) throws IOException {
    type = StreamTools.readInt(in, true);
    systemId = StreamTools.readShort(in, true);
    unknown_1_flags = StreamTools.readShort(in, true);
    structureIndex = StreamTools.readShort(in, true);
    upgradeIndex = StreamTools.readShort(in, true);
    unknown_2 = StreamTools.readInt(in, true);
    action = StreamTools.readInt(in, true);
    shipModelId = StreamTools.readShort(in, true);
    unknown_3 = StreamTools.readShort(in, true);
    StreamTools.read(in, unknown_4);
    empty_1 = StreamTools.readInt(in, true);
    creditsSpent = StreamTools.readInt(in, true);
    empty_2 = StreamTools.readInt(in, true);
    targetRaceId = StreamTools.readShort(in, true);
    StreamTools.read(in, empty_3);

    if (UE.DEBUG_MODE && UE.DEBUG_FLAGS.contains(DebugFlags.EconomicOrder)) {
      val base16 = BaseEncoding.base16();
      System.out.println("Type: " + type);
      System.out.println("SystemId: " + systemId);
      System.out.println("Unknown1_Flags: " + base16.encode(DataTools.toByte(unknown_1_flags, true)));
      System.out.println("StructureIndex: " + structureIndex);
      System.out.println("UpgradeIndex: " + upgradeIndex);
      System.out.println("Unknown2: " + base16.encode(DataTools.toByte(unknown_2, true)));
      System.out.println("Action:" + action);
      System.out.println("ShipModelId: " + shipModelId);
      System.out.println("Unknown3: " + base16.encode(DataTools.toByte(unknown_3, true)));
      System.out.println("Unknown4: " + base16.encode(unknown_4));
      System.out.println("Empty1: " + base16.encode(DataTools.toByte(empty_1, true)));
      System.out.println("CreditsSpent: " + creditsSpent);
      System.out.println("Empty2: " + base16.encode(DataTools.toByte(empty_2, true)));
      System.out.println("TargetRaceId: " + targetRaceId);
      System.out.println("Empty3: " + base16.encode(empty_3));
    }
  }

  void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(systemId, true));
    out.write(DataTools.toByte(unknown_1_flags, true));
    out.write(DataTools.toByte(structureIndex, true));
    out.write(DataTools.toByte(upgradeIndex, true));
    out.write(DataTools.toByte(unknown_2, true));
    out.write(DataTools.toByte(action, true));
    out.write(DataTools.toByte(shipModelId, true));
    out.write(DataTools.toByte(unknown_3, true));
    out.write(unknown_4);
    out.write(DataTools.toByte(empty_1, true));
    out.write(DataTools.toByte(creditsSpent, true));
    out.write(DataTools.toByte(empty_2, true));
    out.write(DataTools.toByte(targetRaceId, true));
    out.write(empty_3);
  }

}
