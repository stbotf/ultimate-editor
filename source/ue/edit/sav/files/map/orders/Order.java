package ue.edit.sav.files.map.orders;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import com.google.common.io.BaseEncoding;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.UE.DebugFlags;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.TaskForceList;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;
import ue.util.stream.chunk.ChunkInputStream;
import ue.util.stream.chunk.ChunkOutputStream;

@Getter
public class Order {

  public static final int HEADER_SIZE = 32;

  enum CoordSelector {
    SOURCE,
    TARGET,
    ALL
  }

  public interface OrderType {

    // refer BotF error labels: https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54283#p54283
    public static final int ECONOMY = 0;            // for build and other economic orders
    public static final int ECONOMIC_RESULT = 1;    // refer result.lst
    public static final int MILITARY = 2;           // for ship and task force orders
    public static final int MILITARY_RESULT = 3;    // refer result.lst
    public static final int INTEL = 4;              // refer result.lst
    public static final int DIPLOMACY = 5;          // for diplomatic orders
    public static final int MP_SHIP_UPDATE = 6;
    public static final int LABOR = 7;
    public static final int BUILD_WONDER = 8;       // a special economic build order
    public static final int UNK9 = 9;               // observed in a UDML MP game, content size = 4 bytes: [CB-D0]5555D5

    public static String typeName(int type) {
      switch (type) {
        case ECONOMY:
          return "ECONOMY";
        case ECONOMIC_RESULT:
          return "ECONOMIC_RESULT";
        case MILITARY:
          return "MILITARY";
        case MILITARY_RESULT:
          return "MILITARY_RESULT";
        case INTEL:
          return "INTEL";
        case DIPLOMACY:
          return "DIPLOMACY";
        case MP_SHIP_UPDATE:
          return "MP_SHIP_UPDATE";
        case LABOR:
          return "LABOR";
        case BUILD_WONDER:
          return "BUILD_WONDER";
        default:
          return "UNK" + Integer.toString(type);
      }
    }
  }

  // same like the resultId from result.lst, this is an incremented unique identifier,
  // offset by [raceId*max_uint/6]
  @Accessors(fluent = true)
  private int orderId;                    // 4
  private int turnOrdered;                // 8  turn of when the order was assigned
  private short raceID;                   // 10 the race id this order is related to
  private short unknown_1;                // 12 looks to be some boolean flag that I once found flagged in a BOP large game
  private byte[] empty_1 = new byte[4];   // 16 always zero
  private int type;                       // 20 the order type
  private int contentAddress;             // 24 for result.lst, in OrderInfo always zero
  private int load_routine;               // 28 for fleet orders: sub_4E1970, sometimes sub_4DFFE0
  // private int contentSize;             // 32 auto-calculated

  private EconomicOrder economicOrder = null;
  private MilitaryOrder militaryOrder = null;
  private DiplomaticOrder diplomaticOrder = null;
  private WonderBuildOrder wonderBuildOrder = null;
  private byte[] unknownOrder = null;

  @Accessors(fluent = true)
  private InternalFile file;
  private StbofFileInterface stif = null;

  public Order(InternalFile file, InputStream in) throws IOException {
    this.file = file;
    Stbof stbof = UE.FILES.stbof();
    stif = stbof != null ? stbof.files() : null;

    int contentSize = readHeader(in);
    readContent(in, contentSize);
  }

  public Order(InternalFile file, ChunkInputStream in) throws IOException {
    this.file = file;
    Stbof stbof = UE.FILES.stbof();
    stif = stbof != null ? stbof.files() : null;

    // For chunked streams, the Order data read and written
    // is split to a header and a content chunk.
    int contentSize = readHeader(in);
    int remaining = in.available();

    if (remaining > 0) {
      throw new IOException("Order<" + in.currentChunk()
          + ">: Full entry header read, but still " + remaining + " bytes left!");
    }

    if (in.nextChunk() < 0) {
      throw new IOException("Order<" + in.currentChunk()
          + ">: Failed to read content data chunk!");
    }

    int dataSize = in.available();
    if (dataSize != contentSize) {
      System.out.println("Order<" + in.currentChunk() + ">: content size "
          + contentSize + " doesn't match the chunk data size " + dataSize + "!");
    }

    readContent(in, contentSize);
  }

  /**
   * @param raceRst Stbof.res race lookup for the race name.
   * @param tfList  Task force list to lookup MilitaryOrder details. For last turn results pass GTForceList, else GWTForce.
   * @return        A descriptive identification string of the order.
   */
  public String descriptor(TaskForceList tfList) {
    RaceRst raceRst = stif != null ? stif.findRaceRst().orElse(null) : null;

    String race = raceRst != null ? raceRst.getOwnerName(raceID) : "Race " + Integer.toString(raceID);
    String typeName =  OrderType.typeName(type);
    String order = Integer.toUnsignedString(orderId);
    String mil = militaryOrder != null ? " for " + militaryOrder.unitDescriptor(tfList) : "";
    String mis = militaryOrder != null ? " " + militaryOrder.getMissionName() : "";
    return race + " " + typeName + mis + " order " + order + mil;
  }

  private int readHeader(InputStream in) throws IOException {
    orderId = StreamTools.readInt(in, true);
    turnOrdered = StreamTools.readInt(in, true);
    raceID = StreamTools.readShort(in, true);
    unknown_1 = StreamTools.readShort(in, true);
    StreamTools.read(in, empty_1);
    type = StreamTools.readInt(in, true);
    contentAddress = StreamTools.readInt(in, true);
    load_routine = StreamTools.readInt(in, true);
    int contentSize = StreamTools.readInt(in, true);

    if (UE.DEBUG_MODE && UE.DEBUG_FLAGS.contains(DebugFlags.Order)) {
      val base16 = BaseEncoding.base16();
      System.out.println("Order:");
      System.out.println("OrderId: " + base16.encode(DataTools.toByte(orderId, true)));
      System.out.println("TurnOrdered: " + turnOrdered);
      System.out.println("RaceID: " + raceID);
      System.out.println("Unknown1: " + unknown_1);
      System.out.println("Empty1: " + base16.encode(empty_1));
      System.out.println("Type: " + type);
      System.out.println("ContentAddress: " + base16.encode(DataTools.toByte(contentAddress, true)));
      System.out.println("LoadRoutine: " + base16.encode(DataTools.toByte(load_routine, true)));
      System.out.println("ContentSize: " + contentSize);
    }

    // return the content data size
    return contentSize;
  }

  private void readContent(InputStream in, int contentSize) throws IOException {
    switch (type) {
      case OrderType.ECONOMY: {
        economicOrder = new EconomicOrder(this, in, contentSize);
        break;
      }
      case OrderType.MILITARY: {
        militaryOrder = new MilitaryOrder(this, in, contentSize);
        break;
      }
      case OrderType.DIPLOMACY: {
        diplomaticOrder = new DiplomaticOrder(this, in, contentSize);
        break;
      }
      case OrderType.BUILD_WONDER: {
        wonderBuildOrder = new WonderBuildOrder(this, in, contentSize);
        break;
      }
      case OrderType.INTEL:
      default: {
        unknownOrder = new byte[contentSize];

        int len = 0;
        while (len < contentSize) {
          len += in.read(unknownOrder, len, contentSize - len);
        }

        if (UE.DEBUG_MODE) {
          System.out.println(orderTypeName(type) + ": [id " + orderId + ", turn " + turnOrdered
            + ", race " + raceID + "] -> " + BaseEncoding.base16().encode(unknownOrder));
        }
      }
    }
  }

  private static String orderTypeName(int type) {
    switch (type) {
      case OrderType.ECONOMY:
        return "Economy Order";
      case OrderType.ECONOMIC_RESULT:
        return "Economy Result";
      case OrderType.MILITARY:
        return "Military Order";
      case OrderType.MILITARY_RESULT:
        return "Military Result";
      case OrderType.INTEL:
        return "Intel Order";
      case OrderType.DIPLOMACY:
        return "Diplomacy Order";
      case OrderType.MP_SHIP_UPDATE:
        return "MP_SHIP_UPDATE Order";
      case OrderType.LABOR:
        return "Labor Order";
      case OrderType.BUILD_WONDER:
        return "Build Wonder Order";
      case OrderType.UNK9:
        return "UNK9 Order";
      default:
        return "Unknown Order [" + type + "]";
    }
  }

  public EconomicOrder economicOrder() {
    return economicOrder;
  }

  public MilitaryOrder militaryOrder() {
    return militaryOrder;
  }

  public DiplomaticOrder diplomaticOrder() {
    return diplomaticOrder;
  }

  public WonderBuildOrder wonderBuildOrder() {
    return wonderBuildOrder;
  }

  public byte[] unknownOrder() {
    return unknownOrder;
  }

  public int getTargetSectorIndex(Sector sectorLst) {
    if (militaryOrder == null)
      return -1;

    return sectorLst.getSectorIndex(
      militaryOrder.getTargetSectorColumn(),
      militaryOrder.getTargetSectorRow());
  }

  public boolean isSystemOrder(short systemId) {
    if (economicOrder != null)
      return economicOrder.getSystemId() == systemId;
    else if (wonderBuildOrder != null)
      return wonderBuildOrder.getSystemId() == systemId;
    else
      return false;
  }

  public boolean isSectorOrder(Sector sectorLst, int sectorIndex) {
    short systemId = sectorLst.getSystem(sectorIndex);

    // check military order
    if (militaryOrder != null && sectorIndex == getTargetSectorIndex(sectorLst)) {
      // accept all task force missions
      // including starbase build, raid, intercept and task force movement
      return true;
    }

    // check system orders
    return systemId != -1 && isSystemOrder(systemId);
  }

  public boolean isRaceOrder(short raceId) {
    if (this.raceID == raceId)
      return true;

    // check incoming gifts
    if (economicOrder != null)
      return economicOrder.getTargetRaceId() == raceId;

    // check incoming diplomatic offers & demands
    if (diplomaticOrder != null) {
      return diplomaticOrder.getSourceRace() == raceId
        || diplomaticOrder.getTargetRace() == raceId
        || diplomaticOrder.getResponderRace() == raceId;
    }

    return false;
  }

  public boolean onRemovedSystem(short systemId) {
    if (economicOrder != null)
      return economicOrder.onRemovedSystem(systemId);
    else if (wonderBuildOrder != null)
      return wonderBuildOrder.onRemovedSystem(systemId);
    else
      return false;
  }

  public int getContentSize() {
    switch (type) {
      case OrderType.ECONOMY:
        return economicOrder.getSize();
      case OrderType.MILITARY:
        return militaryOrder.getSize();
      case OrderType.DIPLOMACY:
        return diplomaticOrder.getSize();
      case OrderType.BUILD_WONDER:
        return wonderBuildOrder.getSize();
      case OrderType.INTEL:
      default:
        if (unknownOrder != null) {
          return unknownOrder.length;
        }
    }
    return 0;
  }

  public int getSize() {
    return HEADER_SIZE + getContentSize();
  }

  public void save(OutputStream out) throws IOException {
    writeHeader(out);
    writeContent(out);
  }

  public void save(ChunkOutputStream out) throws IOException {
    writeHeader(out);
    out.nextChunk();
    writeContent(out);
  }

  // @returns whether the order is valid = true or should be removed = false
  public boolean check(TaskForceList gwtfList, Vector<String> response) {
    if (militaryOrder != null)
      return militaryOrder.check(gwtfList, response);
    else if (diplomaticOrder != null)
      return diplomaticOrder.check(response);
    else
      return true;
  }

  private void writeHeader(OutputStream out) throws IOException {
    out.write(DataTools.toByte(orderId, true));
    out.write(DataTools.toByte(turnOrdered, true));
    out.write(DataTools.toByte(raceID, true));
    out.write(DataTools.toByte(unknown_1, true));
    out.write(empty_1);
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(contentAddress, true)); // for result.lst, 00 in ordInfo
    out.write(DataTools.toByte(load_routine, true));
    out.write(DataTools.toByte(getContentSize(), true));
  }

  private void writeContent(OutputStream out) throws IOException {
    switch (type) {
      case OrderType.ECONOMY:
        economicOrder.save(out);
        break;
      case OrderType.MILITARY:
        militaryOrder.save(out);
        break;
      case OrderType.DIPLOMACY:
        diplomaticOrder.save(out);
        break;
      case OrderType.BUILD_WONDER:
        wonderBuildOrder.save(out);
        break;
      case OrderType.INTEL:
      default:
        if (unknownOrder != null) {
          out.write(unknownOrder);
        }
    }
  }

}
