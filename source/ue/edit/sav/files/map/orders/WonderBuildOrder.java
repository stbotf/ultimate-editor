package ue.edit.sav.files.map.orders;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import com.google.common.io.BaseEncoding;
import lombok.Getter;
import lombok.val;
import ue.UE;
import ue.UE.DebugFlags;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// only the immediate build order is listed
@Getter
public class WonderBuildOrder {

  private static final String ClassName = WonderBuildOrder.class.getSimpleName();
  private static final int SIZE = 8;

  private short systemId;                 // 02 the system id the order is assigned to
  private short structureIndex;           // 04 structure index to build
  private int unknown_1;                  // 08 always 1 it seems

  private Order order;

  WonderBuildOrder(Order order, InputStream in, int size) throws IOException {
    if (size != 8)
      throw new IOException(ClassName + ": Encountered invalid size of: '" + size + "'!");

    this.order = order;
    load(in);
  }

  public boolean setSystemId(short systemId) {
    if (this.systemId != systemId) {
      this.systemId = systemId;
      order.file().markChanged();
      return true;
    }
    return false;
  }

  public boolean onRemovedSystem(short systemId) {
    // unset matching system ids
    if (this.systemId == systemId) {
      this.systemId = -1;
      order.file().markChanged();
      return true;
    }
    // update successive system ids
    else if (this.systemId > systemId) {
      this.systemId--;
      order.file().markChanged();
    }
    return false;
  }

  public int getSize() {
    return SIZE;
  }

  public void load(InputStream in) throws IOException {
    systemId = StreamTools.readShort(in, true);
    structureIndex = StreamTools.readShort(in, true);
    unknown_1 = StreamTools.readInt(in, true);

    if (UE.DEBUG_MODE && UE.DEBUG_FLAGS.contains(DebugFlags.WonderBuildOrder)) {
      val base16 = BaseEncoding.base16();
      System.out.println("SystemId: " + systemId);
      System.out.println("StructureIndex: " + structureIndex);
      System.out.println("Unknown1: " + base16.encode(DataTools.toByte(unknown_1, true)));
    }
  }

  void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(systemId, true));
    out.write(DataTools.toByte(structureIndex, true));
    out.write(DataTools.toByte(unknown_1, true));
  }

}
