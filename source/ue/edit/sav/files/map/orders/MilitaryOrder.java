package ue.edit.sav.files.map.orders;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import com.google.common.io.BaseEncoding;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.UE.DebugFlags;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.data.TaskForce;
import ue.edit.sav.tools.MapTools;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class MilitaryOrder {

  public interface Type {

    public static final int MOVE = 0;
    public static final int TERRAFORM = 1;       // ? action https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54174#p54174
    public static final int COLONIZE = 2;
    public static final int ATTACK_SYSTEM = 0xE;

    public static String typeName(int type) {
      switch (type) {
        case MOVE:
          return "MOVE";
        case TERRAFORM:
          return "TERRAFORM";
        case COLONIZE:
          return "COLONIZE";
        case ATTACK_SYSTEM:
          return "ATTACK_SYSTEM";
        default:
          return "mission " + Integer.toString(type);
      }
    }
  }

  private int type;                           // 04 the MilitaryOrder type
  private int targetSectorRow;                // 08 the destination sector row
  private int targetSectorColumn;             // 12 the destination sector column
  private int sourceSectorRow;                // 16 the start sector row when the order was assigned
  private int sourceSectorColumn;             // 20 the start sector column when the order was assigned
  private int mission;                        // 24 the assigned ue.edit.sav.CSavGame.Mission
  private int isSystemEffective;              // 28 for system attack, terraform and colonize 1, else -1
  private short taskForceId;                  // 30
  private byte[] alignment_1 = new byte[2];   // 32 32bit alignment, always 0xFFFF
  private byte[] empty_1 = new byte[4];       // 36 always zero
  @Accessors(fluent = true)
  private int militaryOrderId;                // 40 referred by GTForceList & GWTForce at 0x54
  private byte[] empty_2 = new byte[4];       // 44 always zero

  // command dependent values
  // for move command:  max range of the task force (0:short, 1:medium, 2:large)
  // for terraform command: the planet id (0 = right most)
  // for system attack: the computed sector index (row*columns + column-1, same like the outpost ids)
  private int commandValue1;          // 48
  // for move command:  the speed (sectors per turn) when the order was given
  private int commandValue2;          // 52

  private Order order;

  MilitaryOrder(Order order, InputStream in, int size) throws IOException {
    if (size != 52)
      throw new IOException("Encountered invalid TaskForceOrder size of: '" + size + "'!");

    this.order = order;
    type = StreamTools.readInt(in, true);
    targetSectorRow = StreamTools.readInt(in, true);
    targetSectorColumn = StreamTools.readInt(in, true);
    sourceSectorRow = StreamTools.readInt(in, true);
    sourceSectorColumn = StreamTools.readInt(in, true);
    mission = StreamTools.readInt(in, true);
    isSystemEffective = StreamTools.readInt(in, true);
    taskForceId = StreamTools.readShort(in, true);
    StreamTools.read(in, alignment_1);
    StreamTools.read(in, empty_1);
    militaryOrderId = StreamTools.readInt(in, true);
    StreamTools.read(in, empty_2);
    commandValue1 = StreamTools.readInt(in, true);
    commandValue2 = StreamTools.readInt(in, true);

    if (UE.DEBUG_MODE && UE.DEBUG_FLAGS.contains(DebugFlags.MilitaryOrder)) {
      val base16 = BaseEncoding.base16();
      System.out.println("Type: " + type);
      System.out.println("Target Sector: " + targetSectorColumn + "." + targetSectorRow);
      System.out.println("Source Sector: " + sourceSectorColumn + "." + sourceSectorRow);
      System.out.println("Mission: " + mission);
      System.out.println("IsSystemEffective: " + isSystemEffective);
      System.out.println("TaskForceId: " + taskForceId);
      System.out.println("Align1: " + base16.encode(alignment_1));
      System.out.println("Empty1: " + base16.encode(empty_1));
      System.out.println("TaskForceOrderId: " + militaryOrderId);
      System.out.println("Empty2: " + base16.encode(empty_2));
      System.out.println("CommandValue1 (FlightRange/SectorIndex): " + commandValue1);
      System.out.println("CommandValue2 (FlightSpeed): " + commandValue2);
    }
  }

  public String descriptor(TaskForceList tfList) {
    String unit = unitDescriptor(tfList);
    String mis = LexHelper.mapTFMission(mission) + " mission";
    return unit + ", with " + mis;
  }

  public String unitDescriptor(TaskForceList tfList) {
    TaskForce tf = tfList.getTaskForce(taskForceId);
    if (tf != null)
      return tf.inlineDescriptor();

    String tfname = Integer.toString(this.taskForceId);
    String sector =  MapTools.sectorPos(sourceSectorColumn, sourceSectorRow);
    return "task force " + tfname + ", ordered at " + sector;
  }

  public String getMissionName() {
    return LexHelper.mapTFMission(mission);
  }

  public boolean isMissionCompatible(int tfMission) {
    return mission == tfMission || (mission == Mission.Move && Mission.canMove(tfMission));
  }

  public int getSize() {
    return 52;
  }

  public short taskForceId() {
    return taskForceId;
  }

  public int targetSectorRow() {
    return targetSectorRow;
  }

  public int targetSectorColumn() {
    return targetSectorColumn;
  }

  public int sourceSectorRow() {
    return sourceSectorRow;
  }

  public int sourceSectorColumn() {
    return sourceSectorColumn;
  }

  public int mission() {
    return mission;
  }

  public void setTaskForceId(short taskForceId) {
    this.taskForceId = taskForceId;
  }

  public void setTargetSectorRow(int y) {
    this.targetSectorRow = y;
  }

  public void setTargetSectorColumn(int x) {
    this.targetSectorColumn = x;
  }

  public void setSourceSectorRow(int y) {
    this.sourceSectorRow = y;
  }

  public void setSourceSectorColumn(int x) {
    this.sourceSectorColumn = x;
  }

  public void setMission(int mission) {
    this.mission = mission;
  }

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(targetSectorRow, true));
    out.write(DataTools.toByte(targetSectorColumn, true));
    out.write(DataTools.toByte(sourceSectorRow, true));
    out.write(DataTools.toByte(sourceSectorColumn, true));
    out.write(DataTools.toByte(mission, true));
    out.write(DataTools.toByte(isSystemEffective, true));
    out.write(DataTools.toByte(taskForceId, true));
    out.write(alignment_1);
    out.write(empty_1);
    out.write(DataTools.toByte(militaryOrderId, true));
    out.write(empty_2);
    out.write(DataTools.toByte(commandValue1, true));
    out.write(DataTools.toByte(commandValue2, true));
  }

  // @returns whether the order is valid = true or should be removed = false
  public boolean check(TaskForceList gwtfList, Vector<String> response) {
    TaskForce tf = gwtfList.getTaskForce(taskForceId);

    if (tf == null) {
      String msg = order.descriptor(gwtfList) + " misses task force in %1. (removed)"
        .replace("%1", CSavFiles.GWTForce);
      response.add(order.file().getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      return false;
    }

    // check for deviating task force missions
    // - moved task forces may have the evade or engage mission set
    // - when terraforming completed, the order might still be set
    // - for the TRAIN_CREW order the AI seems to miss update the ENGAGE task force mission
    int tfMission = tf.getMission();
    if (!isMissionCompatible(tfMission)) {
      if (mission == Mission.Terraform && tfMission == Mission.Evade) {
        String msg = order.descriptor(gwtfList);
        msg += " of planet %1 ended. (removed)";
        msg = msg.replace("%1", Integer.toString(commandValue1));
        response.add(order.file().getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
        return false;
      }

      String msg = order.descriptor(gwtfList);
      String tfMissionName = LexHelper.mapTFMission(tfMission);

      msg += " incorrectly was set to %1 by %2. (fixed)";
      msg = msg.replace("%1", tfMissionName);
      msg = msg.replace("%2", gwtfList.getName());
      response.add(order.file().getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));

      tf.setMission(mission);
    }

    return true;
  }

}
