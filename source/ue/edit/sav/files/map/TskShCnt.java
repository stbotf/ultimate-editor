package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class TskShCnt extends InternalFile {

  public static final String CNT_SUFFIX = CSavFiles.TskSh_Suffix;
  public static final String TSKSH_FILE_NAME = CSavFiles.TskSh;

  private SavGame game;
  private int shipCnt = 0;

  public TskShCnt(SavGame game) {
    this.game = game;
  }

  public String getTskShName() {
    return this.NAME.substring(0, this.NAME.length() - CNT_SUFFIX.length());
  }

  public int getShipCount() {
    return shipCnt;
  }

  public void setShipCount(int cntr) {
    if (shipCnt != cntr) {
      shipCnt = cntr;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    shipCnt = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(shipCnt, true));
  }

  @Override
  public void clear() {
    shipCnt = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    TskSh tskSh = (TskSh) game.tryGetInternalFile(getTskShName(), false);
    int cnt = tskSh.getShipCount();

    if (shipCnt != cnt) {
      String msg = "Ship number %1 doesn't match the entry number %2! (fixed)";
      msg = msg.replace("%1", Integer.toString(shipCnt));
      msg = msg.replace("%2", Integer.toString(cnt));
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    setShipCount(cnt);
  }

  @Override
  public int getSize() {
    return 4;
  }
}
