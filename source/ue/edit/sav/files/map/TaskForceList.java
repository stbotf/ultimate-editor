package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;
import lombok.Getter;
import lombok.val;
import ue.UE;
import ue.common.CommonStrings;
import ue.edit.common.Checkable;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.files.emp.AlienTFInfo;
import ue.edit.sav.files.map.data.Ship;
import ue.edit.sav.files.map.data.TaskForce;
import ue.exception.KeyNotFoundException;
import ue.exception.SavException;
import ue.util.calc.Calc;
import ue.util.data.CollectionTools;

/**
 * Always refer GWTForce!
 * GWTForce includes player task forces re-groupings.
 * GTForceList on the other hand just lists the initial turn task forces.
 */
public class TaskForceList extends GList {

  public static final int ENTRY_SIZE = TaskForce.SIZE;
  public static final short TFID_RECOMPUTE_BARRIER = Short.MAX_VALUE - 5000;

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public class TaskForceStats {
    public int taskForcesTotal = 0;
    public int taskForcesCloaked = 0;
    // mapped by value
    public HashMap<Integer, Integer> range = new HashMap<Integer, Integer>();
    public HashMap<Integer, Integer> speed = new HashMap<Integer, Integer>();
    public HashMap<Integer, Integer> scanRange = new HashMap<Integer, Integer>();
    // mapped by mission
    public HashMap<Integer, Integer> missions = new HashMap<Integer, Integer>();
    public int systemsAttacked = 0;

    public TaskForceStats() {
      // pre-fill known entries so they aren't missed
      missions.put(Mission.Move, 0);
      missions.put(Mission.Move, 0);
      missions.put(Mission.Terraform, 0);
      missions.put(Mission.Colonize, 0);
      missions.put(Mission.Raid, 0);
      missions.put(Mission.Intercept, 0);
      missions.put(Mission.Evade, 0);
      missions.put(Mission.Engage, 0);
      missions.put(Mission.BuildOutpost, 0);
      missions.put(Mission.BuildStarbase, 0);
      missions.put(Mission.TrainCrew, 0);
      missions.put(Mission.Scrap, 0);
      missions.put(Mission.EnterWormhole, 0);
      missions.put(Mission.AttackSystem, 0);
    }
  }

  private static short[] tempIdCtrs = new short[] {
    -2,     // FF FE for Cardassians
    -5000,  // EC 78 for Federation
    -10000, // D8 F0 for Ferengi
    -15000, // C5 68 for Klingons
    -20000, // B1 E0 for Romulans
    -25000  // 9E 58 for Minors I guess
  };

  private SavGameInterface sgif;
  private TForceCntr tfCntr;
  private GShipList shipList;
  private ShipList shipModels;
  private OrderInfo ordInfo;
  @Getter private boolean isGWTForce = false;

  // use sorted TreeMap to store taskforces and process checks in sequence
  private TreeMap<Short, TaskForce> entries = new TreeMap<Short, TaskForce>();
  private ArrayList<TaskForce> duplicates = new ArrayList<TaskForce>();
  @Getter private boolean extendedTFPatch = false;

  public TaskForceList(GListHead header, TForceCntr tfCntr, SavGame game, Stbof stbof,
      boolean isGWTForce) throws IOException {
    super(header, game, stbof, ENTRY_SIZE);
    this.tfCntr = tfCntr;
    this.isGWTForce = isGWTForce;

    sgif = game.files();
    shipList = (GShipList) game.getInternalFile(CSavFiles.GShipList, true);
    shipModels = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
    ordInfo = (OrderInfo) game.getInternalFile(CSavFiles.OrdInfo, true);
  }

  private void updateHeader() {
    header.setOccupiedSlotNum(getEntryCount());
  }

  @Override
  public int getEntryCount() {
    return entries.size() + duplicates.size();
  }

  public short[] getTaskForceIds() {
    return CollectionTools.toShortArray(entries.keySet());
  }

  public int[] getSectorTaskForceNumbersByRace(int sectorRow, int sectorColumn) throws IOException {
    RaceRst raceRst = stbof.files().raceRst();
    int[] ret = new int[raceRst.getNumberOfEntries() + 11];
    Collection<TaskForce> values = entries.values();

    for (TaskForce tf : values) {
      if (!tf.isInSector(sectorColumn, sectorRow)) {
        continue;
      }

      int ownerId = tf.getOwnerId();
      if (ownerId >= 0 && ownerId < ret.length) {
        ret[ownerId]++;
      } else {
        if (UE.DEBUG_MODE) {
          System.out.println(getName() + ": invalid race index in taskforce " + tf.id()
              + ", race " + ownerId);
        }
      }
    }

    return ret;
  }

  public boolean hasSectorTaskForces(int sectorColumn, int sectorRow) {
    for (TaskForce tf : entries.values()) {
      if (tf.isInSector(sectorColumn, sectorRow)) {
        return true;
      }
    }
    return false;
  }

  public short[] getSectorTaskForces(int sectorColumn, int sectorRow) {
    ArrayList<Short> tfIds = new ArrayList<Short>();
    for (TaskForce tf : entries.values()) {
      if (tf.isInSector(sectorColumn, sectorRow)) {
        tfIds.add(tf.id());
      }
    }

    return CollectionTools.toShortArray(tfIds);
  }

  public short getMaxTaskForceId() {
    int max = 0;
    for (TaskForce tf : entries.values()) {
      // ignore reserved negative temporary ids
      max = Integer.max(max, tf.id());
    }

    return (short)max;
  }

  public TaskForce taskForce(short taskForceId) throws KeyNotFoundException {
    TaskForce tf = entries.get(taskForceId);
    if (tf == null) {
      throw new KeyNotFoundException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(taskForceId)));
    }
    return tf;
  }

  public TaskForce getTaskForce(short taskForceId) {
    return entries.get(taskForceId);
  }

  public List<TaskForce> getRaceTaskForces(int raceId) {
    return entries.values().stream().filter(x -> x.getOwnerId() == raceId)
      .collect(Collectors.toList());
  }

  @Override
  public void load(InputStream in) throws IOException {
    super.load(in);
    entries.clear();
    duplicates.clear();

    int tfPatchDetected = 0;

    for (int i = 0; i < usedSlots; i++) {
      TaskForce tf = new TaskForce(this, shipList, shipModels, ordInfo);
      tf.load(in);

      if (entries.putIfAbsent(tf.id(), tf) != null)
        duplicates.add(tf);

      // Note, extended task force patch detection only works reliable
      // since botf does not support an empty task force list.
      if (tf.isDetectedTFPatch()) {
        extendedTFPatch |= true;
        tfPatchDetected++;
      }
    }

    if (extendedTFPatch && tfPatchDetected != usedSlots) {
      String msg = "Warning, inconsistent extended task force patch detection!\n"
        + "Detected " + tfPatchDetected + " of " + usedSlots + " task forces to be patched.\n"
        + "Enforce task force patch for consistency?";
      int response = JOptionPane.showConfirmDialog(UE.WINDOW, msg, UE.APP_NAME, JOptionPane.YES_NO_OPTION);
      if (response == JOptionPane.YES_OPTION) {
        for (TaskForce tf : entries.values())
          tf.enforceTFPatch(true);
      }
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    // check available slot number
    int tfCount = entries.size() + duplicates.size();
    int slots_num = header.getTotalSlotsNum();
    if (tfCount > slots_num) {
      // can't fix the slot number here as it might already have been written
      throw new SavException(getName() + ": Insufficient slot number!");
    }

    for (TaskForce tf : entries.values()) {
      tf.save(out, this.extendedTFPatch);
    }

    for (TaskForce tf : duplicates)
      tf.save(out, this.extendedTFPatch);

    // zero unused task force slots
    if (tfCount < slots_num) {
      byte[] padding = new byte[ENTRY_SIZE];
      for (; tfCount < slots_num; ++tfCount)
        out.write(padding);
    }
  }

  @Override
  public void clear() {
    super.clear();
    entries.clear();
    duplicates.clear();
    extendedTFPatch = false;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    super.check(response);

    // reverse map shipId to taskForceId to detect and remove ship duplicates
    HashMap<Short /*shipId*/, TaskForce /*tfId*/> shipTFs = new HashMap<>();

    // process taskforces prior to GShipList, so we know whether they are
    // referenced by some other taskforce, before removing any that
    // are mapped wrong by the ship taskforce property
    checkRemoveTaskForces(shipTFs, response);

    // restore entries with duplicate task force id that still refer valid ships
    fixDuplicates(response);

    // check ship taskforce id, owner and GWTForce ship orders
    entries.values().forEach(tf -> tf.checkShipValues(sav, response));

    // Since all the task force ships are already collected to detect duplicates,
    // check GWTForce to remove ships with no task force.
    // Without a task force, we don't know the map position anyhow.
    checkRemoveLonelyShips(shipTFs, response);

    // finally, when the regular task force id limit is exceeded,
    // reset the counter but recompute new ids for all the task forces
    try {
      fixTaskForceIdOverflow(response);
    } catch (Exception e) {
      String msg = "Failed to fix taskforce id overflow with:\n  %1"
        .replace("%1", e.toString());
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  private void checkRemoveTaskForces(HashMap<Short, TaskForce> shipTFs, Vector<String> response) {
    // sort entries to process later ids first, as they are likely more up to date
    // reverse, when there is a value overflow and ids become negative
    TaskForce[] sorted = new TaskForce[entries.size()];
    int max = sorted.length;
    int min = 0;

    for (TaskForce tf : entries.values()) {
      if (tf.id() < 0) {
        // reverse on detected value overflow
        sorted[--max] = tf;
      } else {
        sorted[min++] = tf;
      }
    }

    // when taskforces got removed, the header file needs to be updated
    boolean removed = false;

    // check to remove invalid ships, duplicates and empty task forces
    for (TaskForce tf : sorted) {
      if (checkRemoveTFShips(tf, shipTFs, response)) {
        entries.remove(tf.id());
        removed = true;
      }
    }

    // check detected task force duplicates for the same
    // duplicates might e.g. be caused by an incorrect header task force count,
    // since previously used slots keep their data
    removed |= duplicates.removeIf(tf -> {
      return checkRemoveTFShips(tf, shipTFs, response);
    });

    // update task force count
    if (removed) {
      updateHeader();
      markChanged();
    }
  }

  private boolean checkRemoveTFShips(TaskForce tf, HashMap<Short, TaskForce> shipTFs, Vector<String> response) {
    short[] shipIds = tf.getShipIDs();

    for (short shpId : shipIds) {
      Ship shp = shipList.tryGetShip(shpId);

      if (shp == null) {
        String msg = tf.descriptor() + " lists ship id %1, that is missing from %2! (removed)"
          .replace("%1", Integer.toString(shpId))
          .replace("%2", CSavFiles.GShipList);
        response.add(Checkable.getCheckIntegrityString(getName(),
            InternalFile.INTEGRITY_CHECK_ERROR, msg));
        tf.removeShip(shpId);
        continue;
      }

      // check ship taskforce id whether it actually is linked to another valid taskforce
      short shpTFId = shp.getTaskForceId();
      if (shpTFId != tf.id()) {
        TaskForce other_tf = entries.get(shpTFId);
        if (other_tf != null && other_tf.hasShip(shpId)) {
          String msg = tf.descriptor() + " lists ship id %1, which has a valid taskforce reference to %2! (removed)"
            .replace("%1", Integer.toString(shpId))
            .replace("%2", other_tf.descriptor());
          response.add(Checkable.getCheckIntegrityString(getName(),
            InternalFile.INTEGRITY_CHECK_ERROR, msg));
          tf.removeShip(shpId);
          continue;
        }
      }

      TaskForce prev = shipTFs.putIfAbsent(shpId, tf);
      if (prev != null) {
        String msg = tf.descriptor();
        if (prev.id() == tf.id()) {
          msg += " has duplicate ship id %1!"
            .replace("%1", Integer.toString(shpId));
        } else {
          msg += " lists ship id %1, which already was listed by %2!"
            .replace("%1", Integer.toString(shpId))
            .replace("%2", prev.descriptor());
        }
        msg += " (removed)";

        response.add(Checkable.getCheckIntegrityString(getName(),
          InternalFile.INTEGRITY_CHECK_ERROR, msg));
        tf.removeShip(shpId);
      }
    }

    // remove empty task forces
    if (tf.shipCount() == 0) {
      String msg = tf.descriptor() + " has not a single ship! (removed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      removeFromAlien(tf, response);
      removeOrders(tf.id(), response);
      removeResults(tf.id(), response);
      return true;
    }
    return false;
  }

  private void fixDuplicates(Vector<String> response) {
    if (this.isGWTForce) {
      // fix task force duplicates, which sometimes occur for temporary GWTForce ids,
      // when the player redeploys task forces
      // see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56145#p56145
      for (TaskForce tf : duplicates) {

        // use a temporary id since otherwise GTForceList & alienTFInfo needs to be updated too
        // and therefore GTForceList task forces might have to be re-grouped
        short tfId = this.getTempTFId(tf);
        String msg = "%1 has a duplicate id, which now is updated to %2! (fixed)";
        msg = msg.replace("%1", tf.descriptor());
        msg = msg.replace("%2", Integer.toString(tfId));
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

        // since they are duplicates, just update explicit taskforce ships and orders
        // but leave other orders, results and stuff untouched
        boolean ordersChanged = tf.setId(tfId);
        if (ordersChanged) {
          msg = "Orders of %1 have been updated for the changed id! (fixed)";
          msg = msg.replace("%1", tf.descriptor());
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));
        }

        entries.put(tfId, tf);
      }
      duplicates.clear();
    } else {
      for (TaskForce tf : duplicates) {
        // since BotF relies on unique identifiers, it is very unlikely
        // that there are duplicates in GTForceList
        String msg = "Detected duplicate GTForceList id for %1, which should never occure, but is kept! (ignored)";
        msg = msg.replace("%1", tf.descriptor());
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    }
  }

  private void checkRemoveLonelyShips(HashMap<Short, TaskForce> shipTFs, Vector<String> response) {
    if (this.isGWTForce) {
      short[] shipIds = shipList.getShipIds();
      for (short shpId : shipIds) {
        if (shipTFs.containsKey(shpId))
          continue;

        Ship shp = shipList.tryGetShip(shpId);
        String msg = shp.descriptor() + " has task force id %1, but is missing from %2. (%3)";
        msg = msg.replace("%1", Integer.toString(shp.getTaskForceId()));
        msg = msg.replace("%2", getName());

        // remove ship from agents
        try {
          sgif.taskForceTools().removeFromShipAgents(shp);
          msg = msg.replace("%3", "removed");
        } catch (Exception e) {
          msg = msg.replace("%3", "failed to remove");
          msg += ":\n" + e.toString();
        }

        // remove ship from GShipList
        shipList.removeShip(shpId);

        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
      }
    }
  }

  private void fixTaskForceIdOverflow(Vector<String> response) throws KeyNotFoundException, IOException {
    // update GWTForce along with GTForceList, given GWTForce might include temporary negative ids
    // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=55981#p55981
    if (this.isGWTForce || entries.isEmpty())
      return;

    // GTForceList must not include any negative task force ids that are considered temporary
    short first = entries.firstKey();
    if (first < 0) {
      // find latest used taskforce id
      short max = Short.MIN_VALUE;
      for (short tfId : entries.keySet()) {
        if (tfId < 0)
          max = tfId;
        else
          break;
      }

      String msg = "Detected max id overflow by taskforce %1 (unsigned %2). Valid ids are limited"
        + " to 16bit signed short value of %3!";
      msg = msg.replace("%1", Short.toString(max))
        .replace("%2", Integer.toString(Short.toUnsignedInt(max)))
        .replace("%3", Short.toString(Short.MAX_VALUE));
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));

      recomputeTaskForceIds(response);
    }

    // check whether the max signed short task force id is almost reached
    short last = entries.lastKey();
    if (last > TFID_RECOMPUTE_BARRIER) {
      String msg = "Max taskforce id %1 almost reached the signed short overflow limit of %2.";
      msg = msg.replace("%1", Short.toString(last))
        .replace("%3", Integer.toString(Short.MAX_VALUE));
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));

      recomputeTaskForceIds(response);
    }
  }

  private void recomputeTaskForceIds(Vector<String> response) throws KeyNotFoundException, IOException {
    assert(!isGWTForce());

    String msg = "→ Recomputing all the taskforce ids, updating all references and resetting the counter...";
    response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));

    // taskforces are referenced by GTForceList, GWTForce, GShipList, alienTFInfo,
    // monster.lst, ordInfo and result.lst, and all of them need to be updated
    // abort if required files are missing
    sav.require(CSavFiles.GShipList, CSavFiles.MonsterLst, CSavFiles.OrdInfo, CSavFiles.ResultLst);
    val gwtForce = sgif.gwtForce();

    // in order to not mess any references, process entries in order, starting at index zero
    short nextId = 0;
    for (; nextId < entries.size(); ++nextId) {
      val next = entries.higherEntry((short)(nextId - 1));
      if (next == null)
        break;
      recomputeTaskForceId(gwtForce, next.getValue(), nextId);
    }

    // since we can't change ids while processing them, copy the taskforce list
    val copy = new ArrayList<>(entries.values());

    // next proceed with the most negative entries
    for (val next : copy) {
      if (next == null || next.id() >= 0)
        break;
      recomputeTaskForceId(gwtForce, next, nextId);

      if (next.id() != nextId)
        sgif.taskForceTools().changeTaskForceId(next, nextId);
      ++nextId;
    }

    // Reset TForceCntr. Tt should always match GTForceList,
    // since new GWTForce taskforces have a negative temporary id.
    tfCntr.setTFCounter(nextId);

    msg = "→ Completed to re-assign all the taskforce ids. Max used id now is %1."
      .replace("%1", Integer.toString(nextId - 1));
    response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
  }

  private void recomputeTaskForceId(TaskForceList gwtForce, TaskForce tf, short nextId)
    throws KeyNotFoundException, IOException {
    short tfId = tf.id();

    if (tfId != nextId) {
      sgif.taskForceTools().changeTaskForceId(tf, nextId);

      // since GWTForce might miss some taskforces from GTForceList, but add new
      // temporary ones, update both the lists simultaneously to not mess the index!
      TaskForce gwt = gwtForce.getTaskForce(tfId);
      if (gwt != null)
        sgif.taskForceTools().changeTaskForceId(gwt, nextId);
    }
  }

  private short getTempTFId(TaskForce tf) {
    int raceId = tf.getOwnerId();
    int idx = raceId < NUM_EMPIRES ? raceId : NUM_EMPIRES;
    short id = tempIdCtrs[idx];
    while (this.hasTaskForce(id))
      --id;
    return id;
  }

  private void removeFromAlien(TaskForce tf, Vector<String> response) {
    int raceId = tf.getOwnerId();
    String tfInfoFileName = CSavFiles.AlienTFInfo + raceId;
    AlienTFInfo alienTFInfo = (AlienTFInfo) sav.tryGetInternalFile(tfInfoFileName, true);

    // remove from alienTFInfo
    // only the monsters have no alienTFInfo
    if (alienTFInfo != null) {
      alienTFInfo.removeTaskForce(tf.id());
    } else {
      MonsterList monsters = (MonsterList) sav.tryGetInternalFile(CSavFiles.MonsterLst, true);
      if (monsters == null) {
        String msg = "Missing " + CSavFiles.MonsterLst + "!";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      } else if (!monsters.removeMonster(tf.id())) {
        // if not listed by the monsters either, the alienTFInfo likely is missing
        String err = "Missing " + tfInfoFileName + "!";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, err));
      }
    }
  }

  private void removeOrders(short tfId, Vector<String> response) {
    // only remove entries for next turn orders
    if (!isGWTForce)
      return;

    // remove orders
    val orderInfo = sgif.findOrdInfo();
    if (!orderInfo.isPresent()) {
      String err = "Missing " + CSavFiles.OrdInfo + "!";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, err));
    } else {
      orderInfo.get().removeTaskForceOrder(tfId);
    }
  }

  private void removeResults(short tfId, Vector<String> response) {
    // only remove entries for last turn results
    if (isGWTForce)
      return;

    // remove results
    val resultLst = sgif.findResultList();
    if (!resultLst.isPresent()) {
      String err = "Missing " + CSavFiles.ResultLst + "!";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, err));
    } else {
      resultLst.get().removeTaskForceResults(tfId);
    }
  }

  @Override
  public int getSize() {
    return header.getTotalSlotsNum() * ENTRY_SIZE;
  }

  public TaskForceStats getStats() {
    TaskForceStats stats = new TaskForceStats();
    stats.taskForcesTotal = entries.size();

    HashSet<Integer> systemsAttacked = new HashSet<Integer>();
    for (TaskForce tf : entries.values()) {
      incrementTaskForceStats(stats, tf, systemsAttacked);
    }

    return stats;
  }

  public TaskForceStats getEmpireStats(int empire) {
    TaskForceStats stats = new TaskForceStats();

    HashSet<Integer> systemsAttacked = new HashSet<Integer>();
    for (TaskForce tf : entries.values()) {
      int raceIdx = Integer.min(tf.getOwnerId(), NUM_EMPIRES);
      if (raceIdx == empire)
        incrementTaskForceStats(stats, tf, systemsAttacked);
    }

    return stats;
  }

  public TaskForceStats[] getEmpireStats() {
    TaskForceStats[] stats = new TaskForceStats[6];

    for (int empire = 0; empire < stats.length; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  private void incrementTaskForceStats(TaskForceStats stats, TaskForce tf, HashSet<Integer> systemsAttacked) {
    if (tf.isCloaked())
      stats.taskForcesCloaked++;

    stats.range.merge((int)tf.abilities().getMapRange(), 1, Calc::sum);
    stats.speed.merge((int)tf.abilities().getMapSpeed(), 1, Calc::sum);
    stats.scanRange.merge((int)tf.abilities().getScanRange(), 1, Calc::sum);

    int mission = tf.getMission();
    stats.missions.merge(mission, 1, Calc::sum);
    if (mission == Mission.AttackSystem){
      if (systemsAttacked.add(tf.getTargetSectorIndex()))
        stats.systemsAttacked++;
    }
  }

  public boolean hasTaskForce(short taskForceId) {
    return entries.containsKey(taskForceId);
  }

  public boolean hasShip(short taskForceId, short shipID) {
    // don't bother on missing task forces for this plain test
    return Optional.ofNullable(entries.get(taskForceId)).map(tf -> tf.hasShip(shipID))
        .orElse(false);
  }

  /**
   * @param shipId the ship id for which to search the task force
   * @param checkShipList whether to first check the task force listed by GShipList
   * @return null if not found
   */
  public TaskForce findShipTaskForce(short shipId, boolean checkShipList) {
    // first try the task force listed by GShipList if any
    if (checkShipList) {
      short tfId = shipList.getShipTaskForceId(shipId);
      TaskForce tf = this.getTaskForce(tfId);
      if (tf != null && tf.hasShip(shipId))
        return tf;
    }

    // fallback to search all the task forces
    for (TaskForce tf : entries.values()) {
      if (tf.hasShip(shipId)) {
        return tf;
      }
    }

    return null;
  }

  public TaskForce addTaskForce(int raceId, int x, int y) {
    int tfId = tfCntr.getTFCounter();
    return addTaskForce((short)tfId, raceId, x, y);
  }

  public TaskForce addTaskForce(short taskForceId, int raceId, int x, int y) {
    TaskForce tf = new TaskForce(this, shipList, shipModels, ordInfo, taskForceId, raceId, x, y);
    entries.put(taskForceId, tf);
    updateHeader();
    markChanged();
    updateTFIdCtr(taskForceId);

    return tf;
  }

  // Note that this call only updates explicit ship and order references!
  // Other file references not listed by the taskforce itself need to be updated separately!
  public void changeTaskForceId(short formerTFId, short taskForceId) throws KeyNotFoundException {
    if (formerTFId == taskForceId)
      return;

    TaskForce tf = entries.remove(formerTFId);
    if (tf == null)
      throw new KeyNotFoundException("Didn't find task force to change key.");

    entries.put(taskForceId, tf);
    updateTFIdCtr(taskForceId);
    tf.setId(taskForceId);
  }

  // updates the task force id counter
  private void updateTFIdCtr(short taskForceId) {
    int tfId = tfCntr.getTFCounter();
    if (taskForceId >= tfId)
      tfCntr.setTFCounter(taskForceId + 1);
  }

  public void removeTaskForce(short taskForceId, boolean force) {
    if (!force) {
      TaskForce tf = entries.get(taskForceId);
      if (tf == null) {
        return;
      }
      if (tf.shipCount() > 0) {
        throw new RuntimeException(getName() + ": Task force '" + Integer.toString(taskForceId)
            + "' still has ships and cannot be removed!");
      }
    }

    if (entries.remove(taskForceId) != null) {
      updateHeader();
      markChanged();
    }
  }

  public void removeAll() {
    if (!entries.isEmpty()) {
      entries.clear();
      updateHeader();
      markChanged();
    }
  }
}
