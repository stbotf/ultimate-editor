package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class TradeInfoCnt extends InternalFile {

  public static final String CNTR_SUFFIX = ".cnt";//$NON-NLS-1$
  public static final String TRADEINFO_FILE_NAME = "TradeInfo"; //$NON-NLS-1$

  private SavGame game;
  private int tradeRouteCount = 0;

  public TradeInfoCnt(SavGame game) {
    this.game = game;
  }

  public String getTradeInfoName() {
    return NAME.substring(0, NAME.length() - CNTR_SUFFIX.length());
  }

  public int getTradeRouteCount() {
    return tradeRouteCount;
  }

  public void setTradeRouteCount(int cnt) {
    if (tradeRouteCount != cnt) {
      tradeRouteCount = cnt;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    tradeRouteCount = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(tradeRouteCount, true));
  }

  @Override
  public void clear() {
    tradeRouteCount = 0;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    TradeInfo tradeInfo = (TradeInfo) game.tryGetInternalFile(getTradeInfoName(), false);
    int cnt = tradeInfo.numTradeRoutes();

    if (tradeRouteCount != cnt) {
      String msg = "Trade route number %1 doesn't match the entry number %2! (fixed)";
      msg = msg.replace("%1", Integer.toString(tradeRouteCount));
      msg = msg.replace("%2", Integer.toString(cnt));
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
    setTradeRouteCount(cnt);
  }

  @Override
  public int getSize() {
    return 4;
  }
}
