package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;
import lombok.val;
import ue.UE;
import ue.common.CommonStrings;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.map.orders.MilitaryOrder;
import ue.edit.sav.files.map.orders.Order;
import ue.exception.KeyNotFoundException;

public class OrderInfo extends InternalFile {

  public static final String FileName = CSavFiles.OrdInfo;

  private SavGame game;

  public enum CoordSelector {
    SOURCE,
    TARGET,
    ALL
  }

  // Store orders in a hash map. Given that the entries are of different size,
  // it doesn't help hex analysis to sort them.
  private HashMap<Integer, Order> orders = new HashMap<Integer, Order>();

  // Keeo second index on the task force orders.
  // Other than the task force order group index, task forces
  // by themselves always only may have one single order.
  private HashMap<Short, Order> tfOrders = new HashMap<Short, Order>();

  // Like with ResultList, to restore original sorting, cache the loaded index order.
  // This helps to reduce and analyse sneaking hex changes.
  private ArrayList<Integer> loadOrder = new ArrayList<Integer>();

  public OrderInfo(SavGame game, Stbof stbof) {
    this.game = game;
  }

  @Override
  public void load(InputStream in) throws IOException {
    clear();

    while (in.available() >= Order.HEADER_SIZE) {
      Order order = new Order(this, in);
      int orderId = order.orderId();
      if (orders.putIfAbsent(orderId, order) != null) {
        String title = "Order duplicate detected!";
        String msg = "There is more than one order with same id %1 detected.\nPlease report!"
          .replace("%1", Integer.toUnsignedString(orderId));
        JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
      } else {
        loadOrder.add(orderId);
      }

      MilitaryOrder mil = order.militaryOrder();
      if (mil != null && tfOrders.putIfAbsent(mil.taskForceId(), order) != null) {
        String title = "MilitaryOrder duplicate detected!";
        String msg = "There is more than one military order for task force %1 detected.\nPlease report!"
          .replace("%1", Short.toString(mil.getTaskForceId()));
        JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
      }
    }

    if (in.available() > 0) {
      String msg = "OrderInfo: Detected corrupt excessive file data!";
      JOptionPane.showMessageDialog(UE.WINDOW, msg, UE.APP_NAME, JOptionPane.ERROR_MESSAGE);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int ordId : loadOrder) {
      Order order = orders.get(ordId);
      if (order != null)
        order.save(out);
    }

    // create hash map to exclude already saved entries
    HashSet<Integer> saved = new HashSet<>(loadOrder);

    // save remaining orders
    for (val order : orders.entrySet()) {
      if (!saved.contains(order.getKey()))
        order.getValue().save(out);
    }
  }

  @Override
  public void clear() {
    if (!orders.isEmpty() || !loadOrder.isEmpty()) {
      orders.clear();
      tfOrders.clear();
      loadOrder.clear();
      markChanged();
    }
  }

  @Override
  public void check(Vector<String> response) {
    // Check GWTForce for the current task force orders.
    val gwtfList = game.files().findGwtForce().orElse(null);
    if (gwtfList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, "Missing " + CSavFiles.GWTForce + "!"));
      return;
    }

    boolean any = orders.values().removeIf(order -> {
      if (!order.check(gwtfList, response)) {
        MilitaryOrder mil = order.militaryOrder();
        if (mil != null)
          tfOrders.remove(mil.taskForceId());
        return true;
      }
      return false;
    });

    if (any)
      markChanged();
  }

  @Override
  public int getSize() {
    int size = 0;
    for (Order order : orders.values())
      size += order.getSize();

    return size;
  }

  public void setTaskForceId(short tfId, short tfIdNew) throws KeyNotFoundException {
    if (tfId == tfIdNew)
      return;

    Order order = getTaskForceOrder(tfId);
    MilitaryOrder tfOrder = order.militaryOrder();
    tfOrder.setTaskForceId(tfIdNew);
    markChanged();

    tfOrders.remove(tfId);
    tfOrders.put(tfIdNew, order);
  }

  public void setTargetSectorColumn(short tfId, int x) throws KeyNotFoundException {
    Order order = getTaskForceOrder(tfId);
    MilitaryOrder tfOrder = order.militaryOrder();
    if (tfOrder.targetSectorColumn() != x) {
      tfOrder.setTargetSectorColumn(x);
      markChanged();
    }
  }

  public void setTargetSectorRow(short tfId, int y) throws KeyNotFoundException {
    Order order = getTaskForceOrder(tfId);
    MilitaryOrder tfOrder = order.militaryOrder();
    if (tfOrder.targetSectorRow() != y) {
      tfOrder.setTargetSectorRow(y);
      markChanged();
    }
  }

  public void setSourceSectorColumn(short tfId, int x) throws KeyNotFoundException {
    Order order = getTaskForceOrder(tfId);
    MilitaryOrder tfOrder = order.militaryOrder();
    if (tfOrder.sourceSectorColumn() != x) {
      tfOrder.setSourceSectorColumn(x);
      markChanged();
    }
  }

  public void setSourceSectorRow(short tfId, int y) throws KeyNotFoundException {
    Order order = getTaskForceOrder(tfId);
    MilitaryOrder tfOrder = order.militaryOrder();
    if (tfOrder.sourceSectorRow() != y) {
      tfOrder.setSourceSectorRow(y);
      markChanged();
    }
  }

  public List<Order> listSystemOrders(short systemId) {
    return orders.values().stream()
      .filter(x -> x.isSystemOrder(systemId))
      .collect(Collectors.toList());
  }

  public List<Order> listSectorOrders(int sectorIndex) throws IOException {
    Sector sectorLst = game.files().sectorLst();
    ArrayList<Order> result = new ArrayList<>();

    for (Order order : orders.values()) {
      if (order.isSectorOrder(sectorLst, sectorIndex))
        result.add(order);
    }

    return result;
  }

  public List<Order> listRaceOrders(short raceId) {
    return orders.values().stream()
      .filter(x -> x.isRaceOrder(raceId))
      .collect(Collectors.toList());
  }

  // without knowing more on the task force orders, they should be updated whenever
  // a star system or stellar object is moved, cause some orders are at least
  // known to depend on a star system once the target position is reached
  public int updateTargetPosition(CoordSelector selector, int currentX, int currentY,
    int newX, int newY, boolean bothway) {

    int numChanges = 0;
    for (Order order : tfOrders.values()) {
      MilitaryOrder tfOrder = order.militaryOrder();
      if (tfOrder == null)
        continue;

      if (selector == CoordSelector.SOURCE || selector == CoordSelector.ALL) {
        if (tfOrder.sourceSectorColumn() == currentX
            && tfOrder.sourceSectorRow() == currentY) {
          tfOrder.setSourceSectorColumn(newX);
          tfOrder.setSourceSectorRow(newY);
          ++numChanges;
        } else if (bothway && tfOrder.sourceSectorColumn() == newX
            && tfOrder.sourceSectorRow() == newY) {
          tfOrder.setSourceSectorColumn(currentX);
          tfOrder.setSourceSectorRow(currentY);
          ++numChanges;
        }
      }

      if (selector == CoordSelector.TARGET || selector == CoordSelector.ALL) {
        if (tfOrder.targetSectorColumn() == currentX
            && tfOrder.targetSectorRow() == currentY) {
          tfOrder.setTargetSectorColumn(newX);
          tfOrder.setTargetSectorRow(newY);
          ++numChanges;
        } else if (bothway && tfOrder.targetSectorColumn() == newX
            && tfOrder.targetSectorRow() == newY) {
          tfOrder.setTargetSectorColumn(currentX);
          tfOrder.setTargetSectorRow(currentY);
          ++numChanges;
        }
      }
    }

    if (numChanges > 0)
      markChanged();

    return numChanges;
  }

  public boolean removeOrder(int orderId) {
    Order order = orders.remove(orderId);
    if (order != null) {
      MilitaryOrder mil = order.militaryOrder();
      if (mil != null)
        tfOrders.remove(mil.taskForceId());
      markChanged();
      return true;
    }
    return false;
  }

  public boolean removeTaskForceOrder(short taskForceId) {
    Order order = tfOrders.remove(taskForceId);
    if (order != null) {
      orders.remove(order.orderId());
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllTaskForceOrders() {
    if (!tfOrders.isEmpty()) {
      for (Order order : tfOrders.values())
        orders.remove(order.orderId());
      tfOrders.clear();
      markChanged();
    }
  }

  public void removeSystemOrders(short systemId) {
    if (orders.values().removeIf(x -> x.isSystemOrder(systemId)))
      markChanged();
  }

  public void removeRaceOrders(short raceId) {
    if (orders.values().removeIf(x -> x.isRaceOrder(raceId)))
      markChanged();
  }

  public void onRemovedSystem(short systemId) {
    orders.values().removeIf(x -> x.onRemovedSystem(systemId));
  }

  public Order tryGetOrder(int orderIndex) {
    return orders.get(orderIndex);
  }

  public Order getOrder(int orderIndex) throws KeyNotFoundException {
    Order order = orders.get(orderIndex);
    if (order == null) {
      throw new KeyNotFoundException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toUnsignedString(orderIndex)));
    }
    return order;
  }

  public Order tryGetTaskForceOrder(short taskForceId) {
    return tfOrders.get(taskForceId);
  }

  public Order getTaskForceOrder(short taskForceId) throws KeyNotFoundException {
    Order order = tfOrders.get(taskForceId);
    if (order == null) {
      throw new KeyNotFoundException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(taskForceId)));
    }
    return order;
  }

}
