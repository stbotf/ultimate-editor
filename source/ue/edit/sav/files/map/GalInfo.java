package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * galInfo
 */
public class GalInfo extends InternalFile {

  private byte[] UNKNOWN_1 = new byte[8];     //  8
  private int GAL_SIZE;                       // 12
  private short STAR_COUNT;                   // 14
  private short ANOMALY_COUNT;                // 16
  private short OUTPOST_COUNT;                // 18
  private byte[] UNKNOWN_2 = new byte[2];     // 20
  private short SECTOR_COUNT;                 // 22
  private byte[] UNKNOWN_3 = new byte[26];    // 48

  private SavGame savGame;

  /**
   * @param savGame the save game
   */
  public GalInfo(SavGame savGame) {
    this.savGame = savGame;
  }

  /**
   * @param num number of anomalies
   */
  public void setAnomalyCount(short num) {
    if (ANOMALY_COUNT != num) {
      ANOMALY_COUNT = num;
      markChanged();
    }
  }

  /**
   * @return number of star systems
   */
  public short getSystemCount() {
    return STAR_COUNT;
  }

  /**
   * @return number of anomalies
   */
  public short getAnomalyCount() {
    return ANOMALY_COUNT;
  }

  @Override
  public void check(Vector<String> response) {
    if (GAL_SIZE < 0 || GAL_SIZE > 2) {
      String msg = Language.getString("GalInfo.1") //$NON-NLS-1$
        .replace("%1", Integer.toString(GAL_SIZE)); //$NON-NLS-1$
      msg += " (fixed)";
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

      GAL_SIZE = GAL_SIZE < 0 ? 0 : 2;
      markChanged();
    }

    val sgif = savGame.files();

    val systInfo = sgif.findSystInfo().orElse(null);
    if (systInfo != null) {
      int sysCount = systInfo.getNumberOfEntries();
      if (STAR_COUNT != sysCount) {
        String msg = Language.getString("GalInfo.2") //$NON-NLS-1$
          .replace("%1", systInfo.getName()) //$NON-NLS-1$
          .replace("%2", Integer.toString(sysCount)) //$NON-NLS-1$
          .replace("%3", Integer.toString(STAR_COUNT)); //$NON-NLS-1$
        msg += " (fixed)";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

        STAR_COUNT = (short)sysCount;
        markChanged();
      }
    }

    val sbInfo = sgif.findSbInfo().orElse(null);
    if (sbInfo != null) {
      int sbCount = sbInfo.getNumberOfEntries();
      if (OUTPOST_COUNT != sbCount) {
        String msg = Language.getString("GalInfo.4") //$NON-NLS-1$
          .replace("%1", sbInfo.getName()) //$NON-NLS-1$
          .replace("%2", Integer.toString(sbCount)) //$NON-NLS-1$
          .replace("%3", Integer.toString(OUTPOST_COUNT)); //$NON-NLS-1$
        msg += " (fixed)";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

        OUTPOST_COUNT = (short)sbCount;
        markChanged();
      }
    }

    val sectorLst = sgif.findSectorLst().orElse(null);
    if (sectorLst != null) {
      int sectorCount = sectorLst.getNumberOfSectors();
      if (SECTOR_COUNT != sectorCount) {
        String msg = Language.getString("GalInfo.5") //$NON-NLS-1$
          .replace("%1", sectorLst.getName()) //$NON-NLS-1$
          .replace("%2", Integer.toString(sectorCount)) //$NON-NLS-1$
          .replace("%3", Integer.toString(SECTOR_COUNT)); //$NON-NLS-1$
        msg += " (fixed)";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

        SECTOR_COUNT = (short)sectorCount;
        markChanged();
      }
    }

    val stellInfo = sgif.findStellInfo().orElse(null);
    if (stellInfo != null) {
      int anomalyCount = stellInfo.getNumberOfEntries();
      if (ANOMALY_COUNT != anomalyCount) {
        String msg = Language.getString("GalInfo.3") //$NON-NLS-1$
          .replace("%1", stellInfo.getName()) //$NON-NLS-1$
          .replace("%2", Integer.toString(anomalyCount)) //$NON-NLS-1$
          .replace("%3", Integer.toString(ANOMALY_COUNT)); //$NON-NLS-1$
        msg += " (fixed)";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

        ANOMALY_COUNT = (short)anomalyCount;
        markChanged();
      }
    }
  }

  @Override
  public int getSize() {
    return 48;
  }

  /**
   * @param in the InputStream to read from
   * @throws IOException
   */
  @Override
  public void load(InputStream in) throws IOException {
    StreamTools.read(in, UNKNOWN_1);
    GAL_SIZE = StreamTools.readInt(in, true);
    STAR_COUNT = StreamTools.readShort(in, true);
    ANOMALY_COUNT = StreamTools.readShort(in, true);
    OUTPOST_COUNT = StreamTools.readShort(in, true);
    StreamTools.read(in, UNKNOWN_2);
    SECTOR_COUNT = StreamTools.readShort(in, true);
    StreamTools.read(in, UNKNOWN_3);

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(UNKNOWN_1);
    out.write(DataTools.toByte(GAL_SIZE, true));
    out.write(DataTools.toByte(STAR_COUNT, true));
    out.write(DataTools.toByte(ANOMALY_COUNT, true));
    out.write(DataTools.toByte(OUTPOST_COUNT, true));
    out.write(UNKNOWN_2);
    out.write(DataTools.toByte(SECTOR_COUNT, true));
    out.write(UNKNOWN_3);
  }

  @Override
  public void clear() {
    Arrays.fill(UNKNOWN_1, (byte)0);
    GAL_SIZE = 0;
    STAR_COUNT = 0;
    ANOMALY_COUNT = 0;
    OUTPOST_COUNT = 0;
    Arrays.fill(UNKNOWN_2, (byte)0);
    SECTOR_COUNT = 0;
    Arrays.fill(UNKNOWN_3, (byte)0);
    markChanged();
  }

  /**
   * @param num number of outposts
   */
  public void setBaseCount(short num) {
    if (OUTPOST_COUNT != num) {
      OUTPOST_COUNT = num;
      markChanged();
    }
  }

  public void setSystemCount(short numSystems) {
    if (STAR_COUNT != numSystems) {
      STAR_COUNT = numSystems;
      markChanged();
    }
  }

}
