package ue.edit.sav.files.map;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import org.apache.commons.io.IOUtils;
import com.google.common.base.CharMatcher;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.ControlType;
import ue.edit.res.stbof.common.CStbof.PlanetType;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.common.CStbof.SystemStatus;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.pst.data.PlanetEntry;
import ue.edit.sav.SavGame;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.stream.StreamTools;

/**
 * Represents systInfo
 **/
public class SystInfo extends InternalFile {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // #region fields

  @Getter @Accessors(fluent = true)
  private final SavGame savGame;
  @Getter @Accessors(fluent = true)
  private final Optional<Stbof> stbof;

  // systems must be sorted by index
  private ArrayList<SystemEntry> SYSTEMS = new ArrayList<SystemEntry>();
  private byte[] precedingSystemData = null;
  @Getter private boolean extendedBuildQueuePatch = false;

  // #endregion fields

  // #region constructor

  public SystInfo(SavGame savGame, Optional<Stbof> st) {
    this.savGame = savGame;
    this.stbof = st;
  }

  // #endregion constructor

  // #region properties

  /**
   * Returns number of entries
   **/
  public int getNumberOfEntries() {
    return SYSTEMS.size();
  }

  public List<SystemEntry> listSystems() {
    return Collections.unmodifiableList(SYSTEMS);
  }

  public boolean hasSystem(int systemId) {
    return systemId >= 0 && systemId < SYSTEMS.size();
  }

  /**
   * Returns a system entry
   * @param index system index
   */
  public SystemEntry getSystem(int index) {
    checkSystemIndex(index);
    return SYSTEMS.get(index);
  }

  // #endregion properties

  // #region generic info

  /**
   * Returns file size
   **/
  @Override
  public int getSize() {
    int retval = precedingSystemData != null ? precedingSystemData.length : 0;

    for (SystemEntry entry : SYSTEMS) {
      retval += entry.getSize();
    }

    return retval;
  }

  // #endregion generic info

  // #region helpers

  public boolean isRaceAlive(int raceId) {
    for (SystemEntry system : SYSTEMS) {
      // specially the minors can be under control by another empire but still be alive
      // therefore both the resident and the controlling race must be checked
      if (system.getPopulation() > 0 && (system.getResidentRace() == raceId || system.getControllingRace() == raceId))
        return true;
    }
    return false;
  }

  public SystemEntry addEmptySystem(int sectorPosX, int sectorPosY) {
    // Always append to the end. No need to sort the indices.
    // There however ofc must be no duplicates!
    int systemId = SYSTEMS.size();

    SystemEntry system = new SystemEntry(this, systemId);
    system.setSectorPosition(sectorPosX, sectorPosY);
    SYSTEMS.add(system);

    markChanged();
    return system;
  }

  /**
   * CAUTION! After adding the system, you must set image position or you'll have two systems drawn
   * in the same sector
   */
  public int duplicateSystem(int index) {
    checkSystemIndex(index);
    SystemEntry re = new SystemEntry(SYSTEMS.get(index), SYSTEMS.size());
    SYSTEMS.add(re);
    markChanged();
    return SYSTEMS.size();
  }

  public SystemEntry removeSystem(short systemId) {
    val entry = SYSTEMS.remove(systemId);
    if (entry != null) {
      updateSystemIndices();
      markChanged();
    }
    return entry;
  }

  @Override
  public void clear() {
    SYSTEMS.clear();
    precedingSystemData = null;
    extendedBuildQueuePatch = false;
    markChanged();
  }

  // #endregion helpers

  // #region info

  /**
   * Returns an array of system names.
   */
  public String[] getNames() {
    return SYSTEMS.stream().map(SystemEntry::getName).toArray(String[]::new);
  }

  public interface SystemFilter {
    public static final int Any = -1;
    public static final int Cardassians = 0;
    public static final int Federation = 1;
    public static final int Ferengi = 2;
    public static final int Klingons = 3;
    public static final int Romulans = 4;
    // not membered neutral systems (minors & rebels)
    public static final int Neutral = 5;
    public static final int Membered = 6;
    public static final int Subjugated = 7;
  }

  /**
   * @param filter SystemFilter
   * @return array of system ids
   */
  public ID[] getSystemIDs(int filter, boolean populated_only) {
    ArrayList<ID> ids = new ArrayList<ID>();

    for (int x = 0; x < SYSTEMS.size(); x++) {
      SystemEntry z = SYSTEMS.get(x);

      if (populated_only && z.getResidentRace() < 0)
        continue;

      switch (filter) {
        case SystemFilter.Any:
          ids.add(new ID(z.getName(), x));
          break;
        case SystemFilter.Cardassians:
        case SystemFilter.Federation:
        case SystemFilter.Ferengi:
        case SystemFilter.Klingons:
        case SystemFilter.Romulans:
          if (z.getControllingRace() == filter)
            ids.add(new ID(z.getName(), x));
          break;
        case SystemFilter.Neutral:
          if (z.getControllingRace() >= NUM_EMPIRES)
            ids.add(new ID(z.getName(), x));
          break;
        case SystemFilter.Membered:
          if (z.getSystemStatus() == SystemStatus.Membered)
            ids.add(new ID(z.getName(), x));
          break;
        case SystemFilter.Subjugated:
          if (z.isSubjugated())
            ids.add(new ID(z.getName(), x));
          break;
      }
    }

    return ids.toArray(new ID[0]);
  }

  /**
   * returns a list of animation files used by system stars and planets
   */
  public Set<String> getGraphicFiles() {
    HashSet<String> ret = new HashSet<>();
    int siz = SYSTEMS.size();

    for (int index = 0; index < siz; index++) {
      SystemEntry system = SYSTEMS.get(index);
      String ani = system.getStarAni();

      String ext = (ani.equals("neb-") ? ".tga" : ".ani"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      ret.add(ani + ext);
      ret.add(ani + "map.tga"); //$NON-NLS-1$
      ret.add(ani + "maps.tga"); //$NON-NLS-1$

      for (int i = 0; i < system.getNumPlanets(); ++i) {
        PlanetEntry pe = system.getPlanet(i);
        ret.add(pe.getAnimation());
      }
    }
    return ret;
  }

  // #endregion info

  // #region load

  private class SearchResult {

    byte[] extraData;
    boolean found;
  }

  private SearchResult findSystemEntry(InputStream in, int idx) throws IOException {
    SearchResult result = new SearchResult();
    ByteArrayOutputStream extra = new ByteArrayOutputStream();

    while (in.available() >= SystemEntry.HEADER_SIZE) {
      try {
        in.mark(Integer.MAX_VALUE);
        String error = testSystemEntry(in, idx);
        if (error == null) {
          // if succeeded, reset test load
          // and proceed to load next system
          result.found = true;
          in.reset();
          break;
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      // reset input read position
      in.reset();

      // read 4 bytes to the extra space buffer
      extra.write(StreamTools.readBytes(in, 4));
    }

    if (!result.found) {
      while (in.available() > 0) {
        extra.write(StreamTools.readBytes(in, 4));
      }
    }

    result.extraData = extra.size() > 0 ? extra.toByteArray() : null;
    return result;
  }

  public static String testSystemEntry(InputStream in, int expected_index) throws IOException {
    if (in.available() < SystemEntry.HEADER_SIZE)
      return "Insufficient size: " + in.available();

    // system index 0-4
    int index = StreamTools.readInt(in, true);
    if (index != expected_index)
      return "Wrong index: " + index + "(Expected: " + expected_index + ")";

    // system control 4-4
    int systemControl = StreamTools.readInt(in, true);
    if (systemControl < 0 || systemControl > 2)
      return "Wrong system control: " + systemControl;

    // name: 8-40
    String name = StreamTools.readNullTerminatedBotfString(in, 40);
    if (name.isEmpty() || CharMatcher.javaIsoControl().matchesAnyOf(name)) {
      // not sure this is needed, it's only here to make sure we are parsing correctly
      // if the file interpretation is correct then this should never happen
      return "Invalid name: " + name;
    }

    // system status 48-1
    int systemStatus = in.read();
    // support "Extra" system status (BOP)
    if (systemStatus < 0 || systemStatus > SystemStatus.Extra)
      return "Invalid system status: " + systemStatus;

    // star type 49-1
    int starType = in.read();
    if (starType < 0)
      return "Invalid star type: " + starType;

    // star ani 50-5
    String starAni = StreamTools.readNullTerminatedBotfString(in, 5);
    if (starAni.isEmpty() || CharMatcher.javaIsoControl().matchesAnyOf(starAni))
      return "Invalid star ani: " + starAni;

    StreamTools.skip(in, 9);

    // population 64-4
    int population = StreamTools.readInt(in, true);
    if (population < 0)
      return "Invalid population: " + population;

    StreamTools.skip(in, 38);

    // number of planets 106-4
    int numPlanets = StreamTools.readInt(in, true);
    if (numPlanets < 0 || numPlanets > 10)
      return "Invalid planets: " + numPlanets;

    return null;
  }

  @Override
  public void load(InputStream in) throws IOException {
    SYSTEMS.clear();
    val bis = new ByteArrayInputStream(IOUtils.toByteArray(in));

    // load system entries
    SystemEntry sys = null;
    int idx = 0;

    while (bis.available() > 0) {
      // detect excessive system data for extended build queue patch (UDM3, GALM,...)
      SearchResult res = findSystemEntry(bis, idx);

      // add extra space to last system entry
      if (res.extraData != null) {
        if (sys != null) {
          extendedBuildQueuePatch |= sys.setExtra(res.extraData);
        } else {
          precedingSystemData = res.extraData;
        }
      }

      if (!res.found)
        break;

      // read next system entry
      sys = new SystemEntry(this);
      sys.load(bis);
      SYSTEMS.add(sys);
      idx = sys.index() + 1;
    }

    // sort systems by index
    Collections.sort(SYSTEMS);
    markSaved();
  }

  // #endregion load

  // #region save

  /**
   * used to save changes.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    if (precedingSystemData != null && precedingSystemData.length > 0)
      out.write(precedingSystemData);

    for (SystemEntry z : SYSTEMS) {
      z.save(out);
    }
  }

  // #endregion save

  // #region check

  @Override
  public void check(Vector<String> response) {
    Hashtable<Integer, String> idset = new Hashtable<Integer, String>();
    int ent = SYSTEMS.size();

    for (int x = 0; x < ent; x++) {
      SystemEntry sys = SYSTEMS.get(x);
      int sysIndex = sys.index();

      if (idset.containsKey(sysIndex)) {
        String msg = Language.getString("SystInfo.0"); //$NON-NLS-1$
        msg = msg.replace("%1", sys.getName()); //$NON-NLS-1$
        msg = msg.replace("%2", idset.get(sysIndex)); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(sysIndex)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      } else {
        idset.put(sysIndex, sys.getName());
      }

      sys.check(response);
    }

    checkGraphicFiles(response);
  }

  private void checkGraphicFiles(Vector<String> response) {
    if (!stbof.isPresent())
      return;

    val qpt = getGraphicFiles();
    for (String pt : qpt) {
      if (pt.endsWith(".ani")) //$NON-NLS-1$
      {
        AniOrCur AOC = null;
        try {
          AOC = stbof.get().getAnimation(pt, LoadFlags.UNCACHED);
        } catch (Exception fnf) {
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, fnf.getMessage()));
        }

        if (AOC == null) {
          String msg = Language.getString("SystInfo.1"); //$NON-NLS-1$
          msg = msg.replace("%1", pt); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      } else {
        TargaImage tar = null;
        try {
          tar = stbof.get().getTargaImage(pt, LoadFlags.UNCACHED);
        } catch (IOException fnf) {
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, fnf.getMessage()));
        }

        if (tar == null) {
          String msg = Language.getString("SystInfo.2"); //$NON-NLS-1$
          msg = msg.replace("%1", pt); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      }
    }
  }

  // #endregion check

  // #region stats

  public class PlanetStats {
    public int planetType = 0;
    public String planetTypeName;
    // [size]: small = 0, medium, large
    public int[] size = new int[3];
  }

  public class SystemBaseStats {
    // count & status
    public int systemsOwned = 0;
    public int systemsInhabitable = 0;
    public int systemsColonized = 0;
    public int systemsSubjugated = 0;
    public int systemsRebelled = 0;
    public int systemsStarving = 0;
    public int systemsIdle = 0;

    // star type -> 4: red giant, 5: orange, 6: yellow, 7: white, 8: green, 9: blue
    public HashMap<Integer, Integer> starTypes = new HashMap<Integer, Integer>();

    // planets
    public int planetsOwned = 0;
    public int planetsInhabitable = 0;
    public int planetsTerraformed = 0;
    public int planetsColonized = 0;
    public int planetsLarge = 0;
    public int planetsMedium = 0;
    public int planetsSmall = 0;
    // mapped by planet type -> 0: arctic, 1: barren, 2: desert, 3: gas giant,
    // 4: jungle, 5: oceanic, 6: terran, 7: volcanic,...
    public HashMap<Integer, PlanetStats> planets = new HashMap<Integer, PlanetStats>();

    // resources & bonuses
    public int dilithiumOwned = 0;
    public int dilithiumProduction = 0;
    public int dilithiumShortage = 0;
    // food bonus: 0, 0+, 0.1+, 0.2+, 0.3+, 0.4+, 0.5+
    public int foodBonus[] = new int[7];    // for jungle, oceanic & terran
    // energy bonus: 0, 0+, 0.25+, 0.5+, 0.75+, 1.0+
    public int energyBonus[] = new int[6];  // for arctic, barren, desert, gas giants, volcanic & others

    // population
    public int populationMax = 0;
    public int populationTerraformed = 0;
    public int populationTotal = 0;

    // production
    // raw = cumulated edifice.bst output, total = with bonuses
    public int foodProductionTotal = 0;
    public int foodProductionMaxRaw = 0;
    public int foodProductionAssigned = 0;
    public int foodProductionExtra = 0;
    public int foodProductionBonus = 0;
    public int foodConsumption = 0;
    public int foodExcess = 0;
    public int industryProductionTotal = 0;
    public int industryProductionMaxRaw = 0;
    public int industryProductionAssigned = 0;
    public int industryProductionExtra = 0;
    public int industryProductionBonus = 0;
    public int industryInvested = 0;
    public int industryUsage = 0;
    public int energyProductionTotal = 0;
    public int energyProductionMaxRaw = 0;
    public int energyProductionAssigned = 0;
    public int energyProductionExtra = 0;
    public int energyProductionBonus = 0;
    public int energyConsumption = 0;
    public int energyExcess = 0;
    public int intelTotal = 0;
    public int intelMaxRaw = 0;
    public int intelAssigned = 0;
    public int intelExtra = 0;
    public int intelBonus = 0;
    public int researchTotal = 0;
    public int researchMaxRaw = 0;
    public int researchAssigned = 0;
    public int researchExtra = 0;
    public int researchBonus = 0;
    public int incomeTotal = 0;
    public int incomeRaw = 0;
    public int incomeExtra = 0;
    public int incomeBonus = 0;
    public int incomeRemnant = 0;
    public int shipBuildExtra = 0;
    public int shipsInBuild = 0;

    // trade routes
    public int tradeRoutesIncoming = 0;
    public int tradeRoutesLocal = 0;
    public int tradeRoutesExtra = 0;

    // morale
    public int moraleAmount = 0;
    // 0:rebellious 1:defiant 2:disgruntled 3:apathetic 4:content 5:pleased 6:loyal 7:fanatic
    public int[] moraleLvls = new int[8];

    public void init() {
      // pre-fill known entries so they aren't missed
      starTypes.put(StellarType.RedGiant, 0);
      starTypes.put(StellarType.OrangeStar, 0);
      starTypes.put(StellarType.YellowStar, 0);
      starTypes.put(StellarType.WhiteStar, 0);
      starTypes.put(StellarType.GreenStar, 0);
      starTypes.put(StellarType.BlueStar, 0);
      addStatsPlanetType(PlanetType.Arctic);
      addStatsPlanetType(PlanetType.Barren);
      addStatsPlanetType(PlanetType.Desert);
      addStatsPlanetType(PlanetType.GasGiant);
      addStatsPlanetType(PlanetType.Jungle);
      addStatsPlanetType(PlanetType.Oceanic);
      addStatsPlanetType(PlanetType.Terran);
      addStatsPlanetType(PlanetType.Volcanic);
    }

    public int industryExcess() { return industryProductionTotal - industryUsage; }
    public int averageMorale() { return systemsColonized < 2 ? moraleAmount : moraleAmount / systemsColonized; }

    public PlanetStats addStatsPlanetType(int planetType) {
      PlanetStats tmp = new PlanetStats();
      PlanetStats planetStats = planets.putIfAbsent(planetType, tmp);
      if (planetStats == null) {
        planetStats = tmp;
        planetStats.planetType = planetType;
        planetStats.planetTypeName = LexHelper.mapPlanetType(planetType);
      }
      return planetStats;
    }
  }

  public class SystemStats extends SystemBaseStats {
    public int systemsTotal = 0;
    public int planetsTotal = 0;
    public int dilithiumTotal = 0;

    public int systemsUninhabited() { return systemsTotal - systemsColonized; }
    public int systemsUninhabitable() { return systemsTotal - systemsInhabitable; }
  }

  public class EmpireSystemStats extends SystemBaseStats {
    public int systemsLost = 0;
    public int[] systemIds;     // include empire system ids for system related empire stats

    public int systemsUninhabited() { return systemsOwned - systemsColonized; }
    public int systemsUninhabitable() { return systemsOwned - systemsInhabitable; }
  }

  public SystemStats getSystemStats() throws IOException {
    SystemStats stats = new SystemStats();
    stats.init();
    stats.systemsTotal = SYSTEMS.size();

    for (SystemEntry sys : SYSTEMS) {
      sys.incrementBaseStats(stats, stbof);
      stats.planetsTotal += sys.getNumPlanets();
      if (sys.getNaturalDilithium() != 0) {
        stats.dilithiumTotal++;
      }
    }

    return stats;
  }

  public EmpireSystemStats getEmpireStats(int empire) throws IOException {
    EmpireSystemStats stats = new EmpireSystemStats();
    ArrayList<Integer> sysIds = new ArrayList<Integer>();

    for (SystemEntry sys : SYSTEMS) {
      boolean hasOwner = sys.getControlType() == ControlType.AI || sys.getControlType() == ControlType.Player;
      // 0: cardassian, 1: federation, 2: ferengi, 3: klingon, 4: romulan
      // 5: independent minors & rebels (=23h)
      int ctrRaceIdx = Integer.min(sys.getControllingRace(), NUM_EMPIRES);
      int resRaceIdx = Integer.min(sys.getResidentRace(), NUM_EMPIRES);

      // uninhabited systems have resRaceIdx = -1
      if (resRaceIdx == empire && (!hasOwner || ctrRaceIdx != empire)) {
        stats.systemsLost++;
        continue;
      }

      if (hasOwner && empire == ctrRaceIdx) {
        sysIds.add(sys.index());
        sys.incrementBaseStats(stats, stbof);
      }
    }
    stats.systemIds = sysIds.stream().mapToInt(Integer::intValue).toArray();

    return stats;
  }

  public EmpireSystemStats[] getEmpireStats() throws IOException {
    EmpireSystemStats[] stats = new EmpireSystemStats[6];

    // 0: cardassian, 1: federation, 2: ferengi, 3: klingon, 4: romulan
    // 5: independent minors & rebels (=23h)
    for (int empire = 0; empire < 6; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  // #endregion stats

  // #region private helpers

  private void checkSystemIndex(int index) {
    if (index < 0 || index >= SYSTEMS.size()) {
      String msg = Language.getString("SystInfo.3"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(getName() + ": " + msg);
    }
  }

  private void updateSystemIndices() {
    for (int i = 0; i < SYSTEMS.size(); ++i) {
      SystemEntry system = SYSTEMS.get(i);
      system.setIndex(i);
    }
  }

  // #endregion private helpers

}
