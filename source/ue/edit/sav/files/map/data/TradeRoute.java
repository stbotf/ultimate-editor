package ue.edit.sav.files.map.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.sav.files.map.TradeInfo;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class TradeRoute {

  public static final int SIZE = 20; // 0x14

  @Accessors(fluent = true)
  private TradeId id;             // 4
  private int empty;              // 8
  private short sourceSystemId;   // 12
  private short targetSystemId;   // 16
  // credits income of outgoing trade routes
  private int income;             // 20
  // credits income of incoming trade routes (0.05 * pop source system)
  private int targetIncome;       // 24

  private TradeInfo tradeInfo;

  public TradeRoute(TradeInfo tradeInfo) {
    this.tradeInfo = tradeInfo;
  }

  public void setId(TradeId id) {
    if (!this.id.equals(id)) {
      this.id = id;
      tradeInfo.markChanged();
    }
  }

  public void cancel() {
    if (targetSystemId != -1) {
      targetSystemId = -1;
      income = 0;
      targetIncome = 0;
      tradeInfo.markChanged();
    }
  }

  public void setSourceSystemId(short systemId) {
    if (sourceSystemId != systemId) {
      sourceSystemId = systemId;
      tradeInfo.markChanged();
    }
  }

  public void setTargetSystemId(short systemId) {
    if (targetSystemId != systemId) {
      targetSystemId = systemId;
      tradeInfo.markChanged();
    }
  }

  public void load(InputStream in) throws IOException {
    id = new TradeId(in);
    empty = StreamTools.readInt(in, true);
    sourceSystemId = StreamTools.readShort(in, true);
    targetSystemId = StreamTools.readShort(in, true);
    income = StreamTools.readInt(in, true);
    targetIncome = StreamTools.readInt(in, true);
  }

  public void save(OutputStream out) throws IOException {
    id.save(out);
    out.write(DataTools.toByte(empty, true));
    out.write(DataTools.toByte(sourceSystemId, true));
    out.write(DataTools.toByte(targetSystemId, true));
    out.write(DataTools.toByte(income, true));
    out.write(DataTools.toByte(targetIncome, true));
  }

}
