package ue.edit.sav.files.map.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.ShipTech;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.GShipList;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

public class Ship {

  public static final int SIZE = 116;

  private GShipList gShipList;

// #region data fields

  @Getter private String  name;                   // 0x00 + 40 = 40
  @Getter private short   raceID;                 // 0x28 +  2 = 42
  @Accessors(fluent = true)
  @Getter private short   shipID;                 // 0x2A +  2 = 44
  @Getter private int     beamAccuBonus;          // 0x2C +  4 = 48
  @Getter private int     torpAccuBonus;          // 0x30 +  4 = 52
  @Getter private int     defenseBonus;           // 0x34 +  4 = 56
  @Getter private int     shieldStr;              // 0x38 +  4 = 60
  @Getter private int     hullStr;                // 0x3C +  4 = 64
  @Getter private int     crewExp;                // 0x40 +  4 = 68
  @Getter private int     crewExpLevel;           // 0x44 +  4 = 72
  @Getter private byte[]  unknown1 = new byte[8]; // 0x48 +  8 = 80
  @Getter private short   taskForceId;            // 0x50 +  2 = 82
  @Getter private int     shipType;               // 0x52 +  4 = 86  -> id in shiplist.sst
  @Getter private short   unknown2;               // 0x56 +  2 = 88
  @Getter private int     cloak;                  // 0x58 +  4 = 92  -> indicator for battles, 1=yes
  @Getter private int     agility;                // 0x5C +  4 = 96
  @Getter private int     interceptAbility;       // 0x60 +  4 = 100 -> intercept or something similar
  @Getter private int     bombingAbility;         // 0x64 +  4 = 104
  @Getter private byte[]  unknown3 = new byte[4]; // 0x68 +  4 = 108
  @Getter private int     constructionAbility;    // 0x6C +  4 = 112 -> guessed
  @Getter private int     groundCombatStrength;   // 0x70 +  4 = 116

// #endregion data fields

// #region constructor

  public Ship(GShipList gShipList) {
    this.gShipList = gShipList;
  }

  public Ship(GShipList gShipList, short shipId, short shipType, short taskForceId, short raceId, String name) {
    this.gShipList = gShipList;
    this.shipID = shipId;
    this.shipType = shipType;
    this.taskForceId = taskForceId;
    this.raceID = raceId;
    this.name = name;
    this.hullStr = 0;
    this.shieldStr = 0;
    this.agility = 0;
    this.interceptAbility = 0;
    this.bombingAbility = 0;
    this.constructionAbility = 0;
    this.groundCombatStrength = 0;
  }

  public Ship(GShipList gShipList, ShipList shipModels, ShipTech shipTech,
    short shipId, short shipType, short taskForceId, short raceId, String name) {

    this(gShipList, shipId, shipType, taskForceId, raceId, name);
    ShipDefinition shipDef = shipModels.getShipDefinition(shipType);
    hullStr = shipDef.getHullStrength();
    shieldStr = shipDef.getShieldStrength();

    // 0:biotech, 1:computer, 2:building, 3_energy, 4:propulsion, 5:weapons
    byte[] techLvls = shipTech.getTechLvls(shipType);

    // compute ship abilities
    // agility:
    // = shipfunction/=value:[0/8, 1/10, 2/6, 3/2, 4/4, 5-8/0, 9/1]
    // + shiptech.sst computer + propulsion tech requirements
    // interceptAbility (for warships only /function 0-4):
    // = value at 5C + map speed
    // bombingAbility (for warships only /function 0-4):
    // = shiptech.sst computer + weapon tech requirements, if>0 bombing capable
    agility = techLvls[1] + techLvls[4];

    int shipRole = shipDef.getShipRole();
    switch (shipRole) {
      case ShipRole.Scout: {
        agility += 8;
        int mapSpeed = (int) shipDef.getMapSpeed();
        interceptAbility = agility + mapSpeed;
        bombingAbility = techLvls[1] + techLvls[5];
        break;
      }
      case ShipRole.Destroyer: {
        agility += 10;
        int mapSpeed = (int) shipDef.getMapSpeed();
        interceptAbility = agility + mapSpeed;
        bombingAbility = techLvls[1] + techLvls[5];
        break;
      }
      case ShipRole.Cruiser: {
        agility += 6;
        int mapSpeed = (int) shipDef.getMapSpeed();
        interceptAbility = agility + mapSpeed;
        bombingAbility = techLvls[1] + techLvls[5];
        break;
      }
      case ShipRole.StrikeCruiser: {
        agility += 2;
        int mapSpeed = (int) shipDef.getMapSpeed();
        interceptAbility = agility + mapSpeed;
        bombingAbility = techLvls[1] + techLvls[5];
        break;
      }
      case ShipRole.BattleShip: {
        agility += 4;
        break;
      }
      case ShipRole.TroopTransport: {
        // (for TTs only) 50 times shiptech.sst construction tech requirement
        constructionAbility = 50 * techLvls[2];
        // base ground combat strength (i.e. TT production value /10)
        int production = shipDef.getProduction();
        groundCombatStrength = production / 10;
        agility += 1;
        break;
      }
      case ShipRole.ColonyShip:
      case ShipRole.Monster:
      default:
        break;
    }
  }

// #endregion constructor

// #region properties

// #region property gets

  public String descriptor() {
    Stbof stbof = gShipList.stbof();
    Optional<RaceRst> raceRst = stbof != null ? stbof.files().findRaceRst() : Optional.empty();
    Optional<ShipList> shipList = stbof != null ? stbof.files().findShipList() : Optional.empty();
    return descriptor(raceRst, shipList);
  }

  private String descriptor(Optional<RaceRst> raceRst, Optional<ShipList> shipModels) {
    String race = raceRst.isPresent() ? raceRst.get().getOwnerName(raceID) : "Race " + Integer.toString(raceID);
    ShipDefinition shipDef = shipModels.isPresent() ? shipModels.get().getShipDefinition(shipType) : null;
    short role = shipDef != null ? shipDef.getShipRole() : ShipRole.None;
    String catName = role != ShipRole.None ? LexHelper.mapShipRole(role) : "ship";
    String shpId = Integer.toString(this.shipID);
    String preCat = role == ShipRole.None ? "Unknown " : "";
    return preCat + race + " " + catName + " " + shpId + " \"" + name + "\"";
  }

// #endregion property gets

// #region property sets

  public void setName(String name) {
    if (!StringTools.equals(this.name, name)) {
      this.name = name;
      gShipList.markChanged();
    }
  }

  public void setTaskForceId(short taskForceId) {
    if (this.taskForceId != taskForceId) {
      this.taskForceId = taskForceId;
      gShipList.markChanged();
    }
  }

  public void setRaceId(short raceId) {
    if (this.raceID != raceId) {
      this.raceID = raceId;
      gShipList.markChanged();
    }
  }

  public void setShipType(int shipType) {
    if (this.shipType != shipType) {
      this.shipType = shipType;
      gShipList.markChanged();
    }
  }

// #endregion property sets

// #endregion properties

// #region load

  public void load(InputStream in) throws IOException {
    name = StreamTools.readNullTerminatedBotfString(in, 40);

    raceID = StreamTools.readShort(in, true);
    shipID = StreamTools.readShort(in, true);
    beamAccuBonus = StreamTools.readInt(in, true);
    torpAccuBonus = StreamTools.readInt(in, true);
    defenseBonus = StreamTools.readInt(in, true);
    shieldStr = StreamTools.readInt(in, true);
    hullStr = StreamTools.readInt(in, true);
    crewExp = StreamTools.readInt(in, true);
    crewExpLevel = StreamTools.readInt(in, true);
    StreamTools.read(in, unknown1);
    taskForceId = StreamTools.readShort(in, true);
    shipType = StreamTools.readInt(in, true);
    unknown2 = StreamTools.readShort(in, true);
    cloak = StreamTools.readInt(in, true);
    agility = StreamTools.readInt(in, true);
    interceptAbility = StreamTools.readInt(in, true);
    bombingAbility = StreamTools.readInt(in, true);
    StreamTools.read(in, unknown3);
    constructionAbility = StreamTools.readInt(in, true);
    groundCombatStrength = StreamTools.readInt(in, true);
  }

// #endregion load

// #region save

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(name, 40, 39));
    out.write(DataTools.toByte(raceID, true));
    out.write(DataTools.toByte(shipID, true));
    out.write(DataTools.toByte(beamAccuBonus, true));
    out.write(DataTools.toByte(torpAccuBonus, true));
    out.write(DataTools.toByte(defenseBonus, true));
    out.write(DataTools.toByte(shieldStr, true));
    out.write(DataTools.toByte(hullStr, true));
    out.write(DataTools.toByte(crewExp, true));
    out.write(DataTools.toByte(crewExpLevel, true));
    out.write(unknown1);
    out.write(DataTools.toByte(taskForceId, true));
    out.write(DataTools.toByte(shipType, true));
    out.write(DataTools.toByte(unknown2, true));
    out.write(DataTools.toByte(cloak, true));
    out.write(DataTools.toByte(agility, true));
    out.write(DataTools.toByte(interceptAbility, true));
    out.write(DataTools.toByte(bombingAbility, true));
    out.write(unknown3);
    out.write(DataTools.toByte(constructionAbility, true));
    out.write(DataTools.toByte(groundCombatStrength, true));
  }

// #endregion save

// #region check

  public boolean check(SavGame game, Vector<String> response) {
    Stbof stbof = gShipList.stbof();
    ShipList shipList = stbof != null ? stbof.files().findShipList().orElse(null) : null;

    // check for valid ship type
    if (shipList != null)
      return checkInvalidShipType(game.files(), shipList, response);

    return false;
  }

// #endregion check

// #region private helpers

  private boolean checkInvalidShipType(SavGameInterface sgif, ShipList shipModels, Vector<String> response) {
    if (shipModels.hasIndex(shipType))
      return false;

    String msg = "Ship %1 of task force %2 has invalid ship type %3 (%4)";
    msg = msg.replace("%1", Integer.toString(shipID));
    msg = msg.replace("%2", Integer.toString(taskForceId));
    msg = msg.replace("%3", Integer.toString(shipType));

    // remove from task force
    Boolean failed = false;
    try {
      sgif.taskForceTools().removeFromTaskForce(taskForceId, shipID);
    } catch (Exception e) {
      String err = msg.replace("%4", "failed TF remove");
      response.add(gShipList.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      response.add("Exception: " + err);
      failed = true;
    }

    // remove from agents
    try {
      sgif.taskForceTools().removeFromShipAgents(raceID, shipID);
    } catch (Exception e) {
      String err = msg.replace("%4", "failed TF remove");
      response.add(gShipList.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      response.add("Exception: " + err);
      failed = true;
    }

    if (!failed) {
      msg = msg.replace("%4", "removed");
      response.add(gShipList.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    return true;
  }

// #endregion private helpers

}
