package ue.edit.sav.files.map.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.common.Checkable;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.BuildOrder;
import ue.edit.res.stbof.common.CStbof.ControlType;
import ue.edit.res.stbof.common.CStbof.MoraleLevel;
import ue.edit.res.stbof.common.CStbof.PlanetSize;
import ue.edit.res.stbof.common.CStbof.PlanetType;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.StellarGfx;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.common.CStbof.SystemStatus;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.pst.data.PlanetEntry;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.SystInfo.PlanetStats;
import ue.edit.sav.files.map.SystInfo.SystemBaseStats;
import ue.service.Language;
import ue.util.calc.Calc;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

/**
 * Represents a systInfo system entry
 **/
@Getter
public class SystemEntry implements Comparable<SystemEntry> {

  public static final int HEADER_SIZE = 808; // 0x328
  public static final int SPECIAL_SIZE = 64; // 0x40
  private static final String ClassName = SystemEntry.class.getSimpleName();
  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  @Getter(AccessLevel.NONE)
  private SystInfo systInfo = null;

  // #region data fields

  // identified and editable
  @Accessors(fluent = true)
  private int     index;                          // 0x00 +  4 = 4   -> system index
  private int     controlType;                    // 0x04 +  4 = 8   -> uninhabited/AI/player @see ue.edit.res.stbof.CStbof.ControlType
  private String  name;                           // 0x08 + 40 = 48  -> system name
  private byte    systemStatus;                   // 0x30 +  1 = 49  -> native/membered/etc @see ue.edit.res.stbof.CStbof.SystemStatus
  private int     starType;                       // 0x31 +  1 = 50  -> unsigned byte type of stellar object, @see ue.edit.res.stbof.CStbof.StellType
  private String  starAni;                        // 0x32 +  5 = 55  -> graphics files
  private byte[]  empty_1 = new byte[8];          // 0x37 +  8 = 63
  private byte    exploredBy;                     // 0x3F +  1 = 64  -> XOR empire index
  private int     population;                     // 0x40 +  4 = 68  -> system population
  private short   residentRace;                   // 0x44 +  2 = 70  -> the resident local population race id or -1
  private byte[]  empty_2 = new byte[6];          // 0x46 +  6 = 76
  private int     controllingRace;                // 0x4C +  4 = 80  -> race id in control of the system, noone, minor & rebel = 23h
  private int     index_2;                        // 0x50 +  4 = 84  -> repeated index
  private int     pop2Industry;                   // 0x54 +  4 = 88  -> population allocated to industry (population NOT units!)
  private int     pop2Energy;                     // 0x58 +  4 = 92  -> population allocated to energy
  private int     pop2Food;                       // 0x5C +  4 = 96  -> population allocated to food
  private int     pop2Research;                   // 0x60 +  4 = 100 -> population allocated to research
  private int     pop2Intel;                      // 0x64 +  4 = 104 -> population allocated to intel
  private short   artifactsNbr;                   // 0x68 +  2 = 106 -> SCT: random value for unfinished 'Artifacts' feature
  private short   numPlanets;                     // 0x6A +  2 = 108 -> number of planets
  // private short  numSpecials;                  // 0x6C +  2 = 110 -> auto-calculated, always 0 by default, @see specials
  private short   naturalDilithium;               // 0x6E +  2 = 112 -> natural dilithium amount (0-5)
  private int     systemTraits;                   // 0x70 +  4 = 116 -> panet types & dilithium(8)
  private int     minorHomePlanetMask;            // 0x74 +  4 = 120 -> minor home planets mask (cf. 0x70)

  // The system position is used for reverse sector lookup. If invalid, task force actions crash with:
  // File: ..\..\source\game\military\military.c, Line: 2150, There HAS to be a valid sector here!
  private int     systemPosX;                     // 0x78 +  4 = 124 -> horizontal map picture position (of ????map.tga)
  private int     systemPosY;                     // 0x7C +  4 = 128 -> vertical map picture position (of ????map.tga)

  // current turn assignment
  // BuildOrder: ORDER ECONOMY (typeID)
  // 1=build building, 2=upgrade building(s), 3=build ship, 5=tradegoods, 7=unrest order
  private int     buildOrder;                     // 0x80 +  4 = 132 -> @see ue.edit.res.stbof.CStbof.BuildOrder
  private short   investedIndustry;               // 0x84 +  2 = 134
  private short   unrestTurn;                     // 0x86 +  2 = 136 -> turn count for unrest order delay
  private short   currentBuildType;               // 0x88 +  2 = 138 -> current build structure type (max signed int = 7FFFh)
  private short   upgradeBuildType;               // 0x8A +  2 = 140 -> new upgrade build structure (max signed int = 7FFFh)
  private int     buildCost;                      // 0x8C +  4 = 144
  private short   shipType;                       // 0x90 +  2 = 146 -> or -1 or 0
  private short   upgrageType;                    // 0x92 +  2 = 148 -> @see ue.edit.res.stbof.CStbof.UpgradeType
  // from last turn completion
  private int     lastBuildOrder;                 // 0x94 +  4 = 152 -> @see ue.edit.res.stbof.CStbof.BuildOrder
  private short   lastInvestedIndustry;           // 0x98 +  2 = 154
  private short   lastUnrestTurn;                 // 0x9A +  2 = 156 -> last turn count for unrest order delay
  private short   lastCurrentBuildType;           // 0x9C +  2 = 158 -> last current build structure type
  private short   lastUpgradeBuildType;           // 0x9E +  2 = 160 -> last new upgrade build structure type
  private int     lastBuildCost;                  // 0xA0 +  4 = 164
  private short   lastShipType;                   // 0xA4 +  2 = 166
  private short   lastUpgradeType;                // 0xA6 +  2 = 168 -> @see ue.edit.res.stbof.CStbof.UpgradeType

  private int     addrPlayerProductionQueue;      // 0xA8 +  4 = 172 -> length 0x6C
  private int     addrStrcInfo;                   // 0xAC +  4 = 176 -> strcInfo
  private int     addrPlanetList;                 // 0xB0 +  4 = 180 -> 0x4C / planet, @see planets
  private int     addrPlanetSpecials;             // 0xB4 +  4 = 184 -> 0x04 / planet, unused, @see planetSpecials
  private int     addrSpecials;                   // 0xB8 +  4 = 188 -> 0x40 / special, unused, @see specials
  private int     maxPopulation;                  // 0xBC +  4 = 192 -> max population * 10 (for a feature of edifice.bst+7A)

  // gui preview
  private byte[]  floating_guiPlaceholder1 = new byte[124]; // 0xC0 + 124 = 316
  // dilithium production, including special structures
  private int     floating_dilithiumRawProduction;          // 0x13C +  4 = 320
  private byte[]  floating_guiPlaceholder2 = new byte[64];  // 0x140 + 64 = 384

  private int     foodMaxRawProduction;           // 0x180 + 4 = 388 -> cumulated edifice.bst output of main Food
  private int     foodBonus;                      // 0x184 + 4 = 392 -> +% food (blg + planet bonuses)
  private int     energyMaxRawProduction;         // 0x188 + 4 = 396 -> cumulated edifice.bst output of main Energy
  private int     energyBonus;                    // 0x18C + 4 = 400 -> +% energy (blg + planet bonuses)
  private int     industryMaxRawProduction;       // 0x190 + 4 = 404 -> cumulated edifice.bst output of main Industry
  private int     industryBonus;                  // 0x194 + 4 = 408 -> +% industry
  private int     researchMaxRawProduction;       // 0x198 + 4 = 412 -> cumulated edifice.bst output of main Research
  private int     researchBonus;                  // 0x19C + 4 = 416 -> +% research (not working)
  private int     researchEmpireBonus;            // 0x1A0 + 4 = 420 -> +% research empire-wide (all areas)
  // unused building output, set to 1 when ai builds trade goods
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=55808#p55808
  // @see 3. Trade Goods @ https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=404#p404
  // @see bottom side effects @ https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=420#p420
  private int     incomeExtra;                    // 0x1A4 + 4 = 424 -> + credits (unused)
  private int     incomeBonus;                    // 0x1A8 + 4 = 428 -> +% credits
  private int     incomeEmpireBonus;              // 0x1AC + 4 = 432 -> +% total credits (i.e. empire-wide)
  private int     shield;                         // 0x1B0 + 4 = 436 -> shield per energy tech level
  private int     moraleProduction;               // 0x1B4 + 4 = 440 -> + morale building output
  private int     intelMaxRawProduction;          // 0x1B8 + 4 = 444 -> cumulated edifice.bst output of main Intel
  private int     intelBonus_General;             // 0x1BC + 4 = 448 -> +% general intelligence (& +% intel local -> bug)
  private int     intelBonus;                     // 0x1C0 + 4 = 452 -> +% intel total
  private int     intelBonus_Security;            // 0x1C4 + 4 = 456 -> +% security
  private int     intelBonus_Espionage;           // 0x1C8 + 4 = 460 -> +% espionage
  private int     intelBonus_Sabotage;            // 0x1CC + 4 = 464 -> +% sabotage
  private int     intelBonus_Economy;             // 0x1D0 + 4 = 468 -> +% economic intel (not working)
  private int     orbitalDefense;                 // 0x1D4 + 4 = 472 -> anti-ship defenses (base output of running OBs)
  private int     shipExperience;                 // 0x1D8 + 4 = 476
  private int     shipYardProduction;             // 0x1DC + 4 = 480 -> unused
  private int     researchBonus_Biology;          // 0x1E0 + 4 = 484 -> +% biotech research
  private int     researchBonus_Computer;         // 0x1E4 + 4 = 488 -> +% computer research
  private int     researchBonus_Construction;     // 0x1E8 + 4 = 492 -> +% construction research
  private int     researchBonus_Propulsion;       // 0x1EC + 4 = 496 -> +% propulsion research
  private int     researchBonus_Weapons;          // 0x1F0 + 4 = 500 -> +% weapon research
  private int     researchBonus_Energy;           // 0x1F4 + 4 = 504 -> +% energy research
  private int     scanRange;                      // 0x1F8 + 4 = 508
  // dilithium production, including special structures
  private int     dilithiumRawProduction;         // 0x1FC + 4 = 512 -> dilithium production, including special structures
  private int     tradeRoutesExtra;               // 0x200 + 4 = 516 -> + additional system trade routes
  private int     empty_3;                        // 0x204 + 4 = 520 -> unused
  private int     moraleEmpireBonus;              // 0x208 + 4 = 524 -> + morale empire-wide
  private int     moraleBonus;                    // 0x20C + 4 = 528 -> +/- morale ([edifice.bst+82h] & unrest order)
  private int     tradeEmpireIncome;              // 0x210 + 4 = 532 -> +% income on trade route (empire-wide)
  private int     groundDefenseBonus;             // 0x214 + 4 = 536 -> +% ground defense
  private int     raidDefenseBonus;               // 0x218 + 4 = 540 -> +% raid defense bonus (not working)
  private int     empty_4;                        // 0x21C + 4 = 544 -> unused
  private int     foodExtra;                      // 0x220 + 4 = 548 -> + local static extra food
  private int     energyExtra;                    // 0x224 + 4 = 552 -> + local static extra energy
  private int     industryExtra;                  // 0x228 + 4 = 556 -> + local static extra industry
  private int     intelExtra;                     // 0x22C + 4 = 560 -> + local static extra intelligence
  private int     researchExtra;                  // 0x230 + 4 = 564 -> + local static extra research
  private int     shipBuildExtra;                 // 0x234 + 4 = 568 -> + local static extra ship building (e.g. utopia planetia)
  private int     bribeResistanceExtra;           // 0x238 + 4 = 572 -> + local static extra bribe resistance (for membered minor race systems)
  private int     groundCombatEmpireBonus;        // 0x23C + 4 = 576 -> +% ground combat empire-wide

  // gui preview
  private byte[]  floating_guiPlaceholder3 = new byte[52];  // 0x240 + 52 = 628
  // production, including special structures (duplicate) -> loaded from 0x13C=316
  private int     floating_dilithiumProduction;             // 0x274 +  4 = 632
  // bugged AI (& dilithium shortage) Marker, flags system supply sorted by name
  // 1 = read Industry Output for Ship Construction (AI military power check)
  // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56634#p56634
  private int     floating_dilithiumSupply;                 // 0x278 +  4 = 636
  // build ship marker 0=false, 1=true (cf. buildOrder @ 0x80 =3)
  private int     floating_buildsShip;                      // 0x27C +  4 = 640
  private byte[]  floating_guiPlaceholder4 = new byte[24];  // 0x280 + 24 = 664

  private int     unknown_3;                      // 0x298 + 4 = 668 -> 0 or -1
  private short   popEventGrowth;                 // 0x29C + 2 = 670 -> pop increase/loss due to growth/starvation
  private short   pop2IndustryAmount;             // 0x29E + 2 = 672 -> % of manned main industry units
  private short   pop2FoodAmount;                 // 0x2A0 + 2 = 674 -> % of manned main food units
  private short   pop2EnergyAmount;               // 0x2A2 + 2 = 676 -> % of manned main energy units
  private short   pop2IntelAmount;                // 0x2A4 + 2 = 678 -> % of manned main intel units
  private short   pop2ResearchAmount;             // 0x2A6 + 2 = 680 -> % of manned main research units
  private int     popUnemployed;                  // 0x2A8 + 4 = 684 -> unemployed population
  private int     foodTotal;                      // 0x2AC + 4 = 688 -> last turn food output total after bonuses
  private short   foodRemaining;                  // 0x2B0 + 2 = 690 -> remaining food total (= last turn food output total on save)
  private short   foodConsumption;                // 0x2B2 + 2 = 692 -> food consumption
  private short   foodMaxGrowth;                  // 0x2B4 + 2 = 694 -> current population*2 (max growth check)
  private short   energyTotal;                    // 0x2B6 + 2 = 696 -> energy output total after bonuses
  private short   energyConsumption;              // 0x2B8 + 2 = 698 -> energy consumption
  private short   energyRemaining;                // 0x2BA + 2 = 700 -> energy surplus or shortfall
  private int     incomeTotal;                    // 0x2BC + 4 = 704 -> credits output total after bonuses
  private short   incomeRemaining;                // 0x2C0 + 2 = 706 -> system Credits effective (i.e. -support costs)
  private short   buildCostTotal;                 // 0x2C2 + 2 = 708 -> total building support cost (from edifice.bst+7E) + buy costs
  private int     industryTotal;                  // 0x2C4 + 4 = 712 -> industry output total after bonuses
  private int     researchTotal;                  // 0x2C8 + 4 = 716 -> research output total after bonuses

  // dilithium production, including special structures (duplicate) -> loaded from 0x1FC=508
  private int     dilithiumProduction;            // 0x2CC + 4 = 720
  // bugged AI (& dilithium shortage) Marker, flags system supply sorted by name
  // 1 = read Industry Output for Ship Construction (AI military power check)
  // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56634#p56634
  private int     dilithiumSupply;                // 0x2D0 + 4 = 724
  private int     buildsShip;                     // 0x2D4 + 4 = 728 -> build ship marker 0=false, 1=true (cf. buildOrder @ 0x80 =3)

  private int     intelToal;                      // 0x2D8 + 4 = 732 -> intel output total after bonuses
  private short   morale;                         // 0x2DC + 2 = 734 -> morale value
  private short   moraleLevel;                    // 0x2DE + 2 = 736 -> MoraleLevel (-4 to +3) @see ue.edit.res.stbof.CStbof.MoraleLevel
  private int     noIndustry;                     // 0x2E0 + 4 = 740 -> sets Industry output to zero if 1 (e.g. general strike)
  private int     noCredits;                      // 0x2E4 + 4 = 744 -> sets Credit output to zero if 1 (e.g. general strike)
  private int     noResearch;                     // 0x2E8 + 4 = 748 -> sets Research output to zero if 1 (e.g. general strike)
  private int     noIntel;                        // 0x2EC + 4 = 752 -> sets Intel output to zero if 1 (e.g. general strike)
  private short   baseMorale;                     // 0x2F0 + 2 = 754 -> base morale
  private short   empty_5;                        // 0x2F2 + 2 = 756 -> unused
  private int     moraleEventNbr;                 // 0x2F4 + 4 = 760 -> random +/- morale event number, events if 0 (default=32h)
  private short   foodStructureType;              // 0x2F8 + 2 = 762 -> edifice.bst ID of main Food (or max signend 7FFFh)
  private short   energyStructureType;            // 0x2FA + 2 = 764 -> edifice.bst ID of main Energy (or max signend 7FFFh)
  private short   industryStructureType;          // 0x2FC + 2 = 766 -> edifice.bst ID of main Industry (or max signend 7FFFh)
  private short   intelStructureType;             // 0x2FE + 2 = 768 -> edifice.bst ID of main Intel (or max signend 7FFFh)
  private short   researchStructureType;          // 0x300 + 2 = 770 -> edifice.bst ID of main Research (or max signend 7FFFh)
  private short   empty_6;                        // 0x302 + 2 = 772
  private int     events;                         // 0x304 + 4 = 776 -> random events bit mask
  private byte    tradeRoutesIncoming;            // 0x308 + 1 = 777 -> number of incoming trade routes
  private byte    tradeRoutesLocal;               // 0x309 + 1 = 778 -> number of system trade routes
  private short   empty_7;                        // 0x30A + 2 = 780

  // trek.exe internal trade route structure addresses, ignored on load
  // overridden by UE to store hidden system positions
  private int     addrIncomingTradeRoutes;        // 0x30C + 4 = 784 -> address of incoming trade routes list
  private int     addrLocalTradeRoutes;           // 0x310 + 4 = 788 -> address of system trade routes list

  private int     updateWorker;                   // 0x314 + 4 = 792 -> update worker assignments marker (f.e. after pop loss) -> bugged!
  private int     updatePower;                    // 0x318 + 4 = 796 -> update powered buildings marker (f.e. after energy output reduction) -> bugged!
  private int     noTrade;                        // 0x31C + 4 = 800 -> skips trade route & trade goods output if not 0 (siege/blockade marker? unused?)
  private int     noGrowth;                       // 0x320 + 4 = 804 -> bombing marker -> skips pop growth if not 0
  private int     autoProduction;                 // 0x324 + 4 = 808 -> auto production queue setting (0=manual, 1=automatic)

  // the planet entry array, with 0x4C=76 bytes per planet
  private @NonNull ArrayList<PlanetEntry> planets = new ArrayList<PlanetEntry>();

  // unknown, unused planet specials array, with 0x4 bytes per planet
  // - for new games there never seem to be any specials and numSpecials is set to zero
  // - if set, the special values are zero most of the time, else 1, 2, 3
  // @see systInfo data @ 004379CC load game / @ 0043777A save game
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54342#p54342
  private @NonNull ArrayList<Integer> planetSpecials = new ArrayList<>();

  // unknown, unused system specials array, with 0x40 bytes per special
  // - refer systInfo data @ 004379CC load game / @ 0043777A save game
  // - has no data written to the save games
  // - SCT: Unfinished GUI code implies it was meant for a none-planet system
  //   features array, like: asteroid, -belt... (cf. edifice.bst). But only the
  //   workaround of the small dilithium icon above sun animation made it into game.
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54365#p54365
  private byte[][] specials;

  // trade route id list, 4 bytes each
  // - id consists of 2 bytes for the race and 2 bytes for the trade route number
  // - count is determined by tradeRoutesIncoming + tradeRoutesLocal
  // - sorting must be kept to not crash
  private @NonNull ArrayList<TradeId> tradeRoutes = new ArrayList<>();

  // player production queue data
  private PlayerData playerData;

  // extra error data space
  // - for in case the next system read doesn't pass the system check,
  // @see SystInfo.findSystemEntry
  // - empty if not modded
  private byte[] extra;

  // #endregion data fields

  // #region constructor

  public SystemEntry(SystInfo systInfo) {
    this.systInfo = systInfo;
  }

  public SystemEntry(SystInfo systInfo, int systemId) {
    this.systInfo = systInfo;
    this.index = systemId;
    this.index_2 = systemId;
    name = "System " + systemId;
    starType = StellarType.DefaultStar;
    starAni = StellarGfx.Default;

    // noone, minor & rebel = 23h
    controllingRace = Race.None;
    residentRace = -1;
    // always set as default build order
    buildOrder = CStbof.BuildOrder.TradeGoods;
    unknown_3 = -1;
    moraleEventNbr = 50;
    foodStructureType = Short.MAX_VALUE;
    energyStructureType = Short.MAX_VALUE;
    industryStructureType = Short.MAX_VALUE;
    intelStructureType = Short.MAX_VALUE;
    researchStructureType = Short.MAX_VALUE;
  }

  // duplicate system with new index
  public SystemEntry(SystemEntry source, int index) {
    systInfo = source.systInfo;

    this.index = index;
    controlType = source.controlType;
    name = source.name;
    systemStatus = source.systemStatus;
    starType = source.starType;
    starAni = source.starAni;
    empty_1 = source.empty_1.clone();
    exploredBy = source.exploredBy;
    population = source.population;
    residentRace = source.residentRace;
    empty_2 = source.empty_2.clone();
    controllingRace = source.controllingRace;
    index_2 = source.index_2;
    pop2Industry = source.pop2Industry;
    pop2Energy = source.pop2Energy;
    pop2Food = source.pop2Food;
    pop2Research = source.pop2Research;
    pop2Intel = source.pop2Intel;
    artifactsNbr = source.artifactsNbr;
    numPlanets = source.numPlanets;
    // numSpecials = source.numSpecials;
    naturalDilithium = source.naturalDilithium;
    systemTraits = source.systemTraits;
    minorHomePlanetMask = source.minorHomePlanetMask;
    systemPosX = source.systemPosX;
    systemPosY = source.systemPosY;
    buildOrder = source.buildOrder;
    investedIndustry = source.investedIndustry;
    unrestTurn = source.unrestTurn;
    currentBuildType = source.currentBuildType;
    upgradeBuildType = source.upgradeBuildType;
    buildCost = source.buildCost;
    shipType = source.shipType;
    upgrageType = source.upgrageType;
    lastBuildOrder = source.lastBuildOrder;
    lastInvestedIndustry = source.lastInvestedIndustry;
    lastUnrestTurn = source.lastUnrestTurn;
    lastCurrentBuildType = source.lastCurrentBuildType;
    lastUpgradeBuildType = source.lastUpgradeBuildType;
    lastBuildCost = source.lastBuildCost;
    lastShipType = source.lastShipType;
    lastUpgradeType = source.lastUpgradeType;
    addrPlayerProductionQueue = source.addrPlayerProductionQueue;
    addrStrcInfo = source.addrStrcInfo;
    addrPlanetList = source.addrPlanetList;
    addrPlanetSpecials = source.addrPlanetSpecials;
    addrSpecials = source.addrSpecials;
    maxPopulation = source.maxPopulation;
    floating_guiPlaceholder1 = source.floating_guiPlaceholder1.clone();
    floating_dilithiumRawProduction = source.floating_dilithiumProduction;
    floating_guiPlaceholder2 = source.floating_guiPlaceholder2.clone();
    foodMaxRawProduction = source.foodMaxRawProduction;
    foodBonus = source.foodBonus;
    energyMaxRawProduction = source.energyMaxRawProduction;
    energyBonus = source.energyBonus;
    industryMaxRawProduction = source.industryMaxRawProduction;
    industryBonus = source.industryBonus;
    researchMaxRawProduction = source.researchMaxRawProduction;
    researchBonus = source.researchBonus;
    researchEmpireBonus = source.researchEmpireBonus;
    incomeExtra = source.incomeExtra;
    incomeBonus = source.incomeBonus;
    incomeEmpireBonus = source.incomeEmpireBonus;
    shield = source.shield;
    moraleProduction = source.moraleProduction;
    intelMaxRawProduction = source.intelMaxRawProduction;
    intelBonus_General = source.intelBonus_General;
    intelBonus = source.intelBonus;
    intelBonus_Security = source.intelBonus_Security;
    intelBonus_Espionage = source.intelBonus_Espionage;
    intelBonus_Sabotage = source.intelBonus_Sabotage;
    intelBonus_Economy = source.intelBonus_Economy;
    orbitalDefense = source.orbitalDefense;
    shipExperience = source.shipExperience;
    shipYardProduction = source.shipYardProduction;
    researchBonus_Biology = source.researchBonus_Biology;
    researchBonus_Computer = source.researchBonus_Computer;
    researchBonus_Construction = source.researchBonus_Construction;
    researchBonus_Propulsion = source.researchBonus_Propulsion;
    researchBonus_Weapons = source.researchBonus_Weapons;
    researchBonus_Energy = source.researchBonus_Energy;
    scanRange = source.scanRange;
    dilithiumRawProduction = source.dilithiumRawProduction;
    tradeRoutesExtra = source.tradeRoutesExtra;
    empty_3 = source.empty_3;
    moraleEmpireBonus = source.moraleEmpireBonus;
    moraleBonus = source.moraleBonus;
    tradeEmpireIncome = source.tradeEmpireIncome;
    groundDefenseBonus = source.groundDefenseBonus;
    raidDefenseBonus = source.raidDefenseBonus;
    empty_4 = source.empty_4;
    foodExtra = source.foodExtra;
    energyExtra = source.energyExtra;
    industryExtra = source.industryExtra;
    intelExtra = source.intelExtra;
    researchExtra = source.researchExtra;
    shipBuildExtra = source.shipBuildExtra;
    bribeResistanceExtra = source.bribeResistanceExtra;
    groundCombatEmpireBonus = source.groundCombatEmpireBonus;
    floating_guiPlaceholder3 = source.floating_guiPlaceholder3.clone();
    floating_dilithiumProduction = source.floating_dilithiumProduction;
    floating_dilithiumSupply = source.floating_dilithiumSupply;
    floating_buildsShip = source.floating_buildsShip;
    floating_guiPlaceholder4 = source.floating_guiPlaceholder4.clone();
    unknown_3 = source.unknown_3;
    popEventGrowth = source.popEventGrowth;
    pop2IndustryAmount = source.pop2IndustryAmount;
    pop2FoodAmount = source.pop2FoodAmount;
    pop2EnergyAmount = source.pop2EnergyAmount;
    pop2IntelAmount = source.pop2IntelAmount;
    pop2ResearchAmount = source.pop2ResearchAmount;
    popUnemployed = source.popUnemployed;
    foodTotal = source.foodTotal;
    foodRemaining = source.foodRemaining;
    foodConsumption = source.foodConsumption;
    foodMaxGrowth = source.foodMaxGrowth;
    energyTotal = source.energyTotal;
    energyConsumption = source.energyConsumption;
    energyRemaining = source.energyRemaining;
    incomeTotal = source.incomeTotal;
    incomeRemaining = source.incomeRemaining;
    buildCostTotal = source.buildCostTotal;
    industryTotal = source.industryTotal;
    researchTotal = source.researchTotal;
    dilithiumProduction = source.dilithiumProduction;
    dilithiumSupply = source.dilithiumSupply;
    buildsShip = source.buildsShip;
    intelToal = source.intelToal;
    morale = source.morale;
    moraleLevel = source.moraleLevel;
    noIndustry = source.noIndustry;
    noCredits = source.noCredits;
    noResearch = source.noResearch;
    noIntel = source.noIntel;
    baseMorale = source.baseMorale;
    empty_5 = source.empty_5;
    moraleEventNbr = source.moraleEventNbr;
    foodStructureType = source.foodStructureType;
    energyStructureType = source.energyStructureType;
    industryStructureType = source.industryStructureType;
    intelStructureType = source.intelStructureType;
    researchStructureType = source.researchStructureType;
    empty_6 = source.empty_6;
    events = source.events;
    tradeRoutesIncoming = source.tradeRoutesIncoming;
    tradeRoutesLocal = source.tradeRoutesLocal;
    empty_7 = source.empty_7;
    addrIncomingTradeRoutes = source.addrIncomingTradeRoutes;
    addrLocalTradeRoutes = source.addrLocalTradeRoutes;
    updateWorker = source.updateWorker;
    updatePower = source.updatePower;
    noTrade = source.noTrade;
    noGrowth = source.noGrowth;
    autoProduction = source.autoProduction;

    PlanetEntry pe;
    for (int i = 0; i < source.planets.size(); i++) {
      pe = source.planets.get(i);
      planets.add(new PlanetEntry(systInfo, pe, Optional.of(this)));
    }

    planetSpecials.addAll(source.planetSpecials);
    specials = source.specials != null ? source.specials.clone() : null;
    tradeRoutes.addAll(source.tradeRoutes);
    playerData = source.playerData != null ? new PlayerData(source.playerData) : null;
    extra = source.extra != null ? source.extra.clone() : null;
  }

  // #endregion constructor

  // #region properties

  // #region property gets

  public String descriptor() {
    String race = getOwnerName();
    int[] pos = getSectorCoordinates();
    return race + " " + name + " system at " + (pos[0]+1) + "." + (pos[1]+1);
  }

  public int getControllingRace() {
    // for minors fallback to the resident race
    if (controllingRace < 0)
      return residentRace >= NUM_EMPIRES ? residentRace : -1;

    val stbof = systInfo.stbof();
    Optional<RaceRst> raceRst = stbof.isPresent() ? stbof.get().files().findRaceRst() : Optional.empty();

    boolean valid = raceRst.isPresent() ? controllingRace < raceRst.get().getNumberOfEntries()
      : controllingRace != Race.None;
    return valid ? controllingRace : residentRace >= NUM_EMPIRES ? residentRace : -1;
  }

  public String getOwnerName() {
    if (population <= 0) {
      String owner = LexHelper.mapEntry(LexDataIdx.System.Population.Uninhabited, "Uninhabited");
      return StringTools.toUpperFirstChar(owner);
    }

    int raceId = getControllingRace();
    if (raceId < 0)
      return "Independent";

    val stbof = systInfo.stbof();
    if (stbof.isPresent()) {
      val raceRst = stbof.get().files().findRaceRst();
      if (raceRst.isPresent())
        return raceRst.get().getOwnerName(raceId);
    }

    return "Race " + raceId;
  }

  /**
   * Returns the systems animation file
   */
  public String getAni() {
    // TODO: what if it's a tga instead?
    return starAni + ".ani"; //$NON-NLS-1$
  }

  public int[] getSystemPosition() {
    int[] pos = new int[2];
    pos[0] = systemPosX;
    pos[1] = systemPosY;
    return pos;
  }

  public int[] getSectorCoordinates() {
    int[] pos = new int[2];
    pos[0] = systemPosX / 5;
    pos[1] = systemPosY / 5;
    return pos;
  }

  public int getSectorIndex() throws IOException {
    Sector sectorLst = systInfo.savGame().files().sectorLst();
    int x = systemPosX / 5;
    int y = systemPosY / 5;
    return y * sectorLst.getHSectors() + x;
  }

	public boolean isInhabited() {
		return population > 0;
	}

	public boolean isUninhabited() {
		return population <= 0;
	}

  public boolean isSubjugated() {
    return systemStatus == SystemStatus.Subjugated
      || systemStatus == SystemStatus.ConqueredHome;
  }

  public boolean isExploredBy(byte empire) {
    return (exploredBy & empire) == 0;
  }

  public boolean hasDilithium() {
    return (systemTraits & 0x0800) != 0;
  }

  public String[] getPlanetNames() {
    return planets.stream().map(PlanetEntry::getName).toArray(String[]::new);
  }

  public int getNumTradeRoutes() {
    return tradeRoutesIncoming + tradeRoutesLocal;
  }

  public int getFoodSurplus(int bonusFactor) {
    // food total, remaining, bonus and consumption are from last turn and therefore not accurate
    // food extra & raw production might be from last turn as well
    return foodExtra + (foodMaxRawProduction * pop2FoodAmount * bonusFactor / 1000) - population;
  }

  public PlanetEntry getPlanet(int index) {
    checkPlanetIndex(index);
    return planets.get(index);
  }

  // #endregion property gets

  // #region property sets

  public void setIndex(int index) {
    if (this.index != index) {
      this.index = index;
      this.index_2 = index;
      systInfo.markChanged();
    }
  }

  public void setName(String name) {
    if (name.length() > 39)
      name = name.substring(0, 39);

    if (!this.name.equals(name)) {
      this.name = name;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the systems animation file
   * @param ani   animation file (example: 'vtos.ani')
   */
  public void setAni(String ani) throws IllegalArgumentException {
    ani = ani.toLowerCase();

    if (!ani.endsWith(".ani")) { //$NON-NLS-1$
      String msg = Language.getString("SystemEntry.29"); //$NON-NLS-1$
      msg = msg.replace("%1", ani); //$NON-NLS-1$
      throw new IllegalArgumentException(getName() + ": " + msg);
    }

    if (ani.length() > 8)
      return;

    ani = ani.substring(0, ani.length() - 4);
    if (!this.starAni.equals(ani)) {
      this.starAni = ani;
      systInfo.markChanged();
    }
  }

  /**
   * @param star  the star(object) type
   */
  public void setStarType(int starType) {
    if (this.starType != starType) {
      this.starType = starType;
      systInfo.markChanged();
    }
  }

  /**
   * Set the system position within the galaxy map grid.
   * The position of 5 counts per sector is used for reverse lookup,
   * and therefore must be aligned with the sector coordinates!
   *
   * @param posX the horizontal system position (0+)
   * @param posY the vertical system position (0+)
   */
  public void setSystemPosition(int posX, int posY) {
    if (systemPosX != posX || systemPosY != posY) {
      systemPosX = posX;
      systemPosY = posY;
      systInfo.markChanged();
    }
  }

  public static int genSystemPos(int sectorPos) {
    return sectorPos * 5 + (int) Math.random() % 3 + 1;
  }

  /**
   * Set the system position by sector coordinates.
   * The secotr relative position is randomized.
   *
   * @param h_row the index of the horizontal row (0+)
   * @param v_row the index of the vertical row (0+)
   */
  public void setSectorPosition(int h_row, int v_row) {
    int posX = genSystemPos(h_row);
    int posY = genSystemPos(v_row);
    setSystemPosition(posX, posY);
  }

  /**
   * Changes the system sector, but retains the sector relative system position.
   * Therefore, a system aligned to top left sector edge remains in top left sector edge.
   *
   * @param h_row the index of the horizontal row (0+)
   * @param v_row the index of the vertical row (0+)
   */
  public void changeSectorCoordinates(int h_row, int v_row) {
    int posX = h_row * 5 + this.systemPosX % 5;
    int posY = v_row * 5 + this.systemPosY % 5;
    setSystemPosition(posX, posY);
  }

  /**
   * @param controlType the system manager (0-none, 1-ai, 2-player)
   */
  public void setControlType(int controlType) {
    if (this.controlType != controlType) {
      this.controlType = controlType;
      systInfo.markChanged();
    }
  }

  /**
   * @param morale the morale value (1-194)
   */
  public void setMorale(short morale) {
    if (this.morale == morale)
      return;

    this.morale = morale;
    if (morale == 0) {
      // default to content morale
      this.moraleLevel = MoraleLevel.Content;
    } else if (morale < 25) {
      this.moraleLevel = MoraleLevel.Rebellious;
    } else if (morale < 51) {
      this.moraleLevel = MoraleLevel.Defiant;
    } else if (morale < 76) {
      this.moraleLevel = MoraleLevel.Disgruntled;
    } else if (morale < 100) {
      this.moraleLevel = MoraleLevel.Apathetic;
    } else if (morale < 126) {
      this.moraleLevel = MoraleLevel.Content;
    } else if (morale < 151) {
      this.moraleLevel = MoraleLevel.Pleased;
    } else if (morale < 176) {
      this.moraleLevel = MoraleLevel.Loyal;
    } else {
      this.moraleLevel = MoraleLevel.Fanatic;
    }

    systInfo.markChanged();
  }

  public void setPopulation(int pop) {
    if (this.population != pop) {
      this.population = pop;
      systInfo.markChanged();
    }
  }

  public void setResidentRace(short race) {
    if (this.residentRace != race) {
      this.residentRace = race;
      systInfo.markChanged();
    }
  }

  public void setControllingRace(short race) {
    if (this.controllingRace != race) {
      this.controllingRace = race;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the population allocated to industry
   * @param pop   the population allocated
   */
  public void setPop2Industry(int pop) {
    // adjust to available population
    int other = pop2Energy + pop2Food + pop2Research + pop2Intel;
    int available = population - other;
    if (available < pop)
      pop = available < 0 ? 0 : available;

    if (this.pop2Industry != pop) {
      this.pop2Industry = pop;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the population allocated to energy
   * @param pop   the population allocated
   */
  public void setPop2Energy(int pop) {
    // adjust to available population
    int other = pop2Industry + pop2Food + pop2Research + pop2Intel;
    int available = population - other;
    if (available < pop)
      pop = available < 0 ? 0 : available;

    if (this.pop2Energy != pop) {
      this.pop2Energy = pop;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the population allocated to food
   * @param pop   the population allocated
   */
  public void setPop2Food(int pop) {
    // adjust to available population
    int other = pop2Industry + pop2Energy + pop2Research + pop2Intel;
    int available = population - other;
    if (available < pop)
      pop = available < 0 ? 0 : available;

    if (this.pop2Food != pop) {
      this.pop2Food = pop;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the population allocated to research
   * @param pop   the population allocated
   */
  public void setPop2Research(int pop) {
    // adjust to available population
    int other = pop2Industry + pop2Energy + pop2Food + pop2Intel;
    int available = population - other;
    if (available < pop)
      pop = available < 0 ? 0 : available;

    if (this.pop2Research != pop) {
      this.pop2Research = pop;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the population allocated to intel
   * @param pop   the population allocated
   */
  public void setPop2Intel(int pop) {
    // adjust to available population
    int other = pop2Industry + pop2Energy + pop2Food + pop2Research;
    int available = population - other;
    if (available < pop)
      pop = available < 0 ? 0 : available;

    if (this.pop2Intel != pop) {
      this.pop2Intel = pop;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the minor race home planet mask.
   */
  public void setMinorHomePlanetMask(int minorHomePlanetMask) {
    if (this.minorHomePlanetMask != minorHomePlanetMask) {
      this.minorHomePlanetMask = minorHomePlanetMask;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the system status (politics)
   * @param status use one of the SystemStatus fields.
   */
  public void setSystemStatus(byte status) {
    if (this.systemStatus != status) {
      this.systemStatus = status;
      systInfo.markChanged();
    }
  }

  /**
   * Sets the systems build queue order type
   * @param type  use one of the QUEUE_ORDER_TYPE fields.
   */
  public void setBuildQueueOrderType(int type) {
    if (this.buildOrder != type) {
      this.buildOrder = type;
      systInfo.markChanged();
    }
  }

  /**
   * Sets if dilithium is present
   * @param dil   true if dilithium should be present
   */
  public void setDilithium(boolean dil) {
    if (hasDilithium() == dil)
      return;

    if (dil) {
      systemTraits |= 0x0800;
      naturalDilithium = 1;
    } else {
      systemTraits &= ~0x0800;
      naturalDilithium = 0;
    }

    systInfo.markChanged();
  }

  /**
   * Sets if the system was explored by given empire
   * @param empire   empire index
   * @param explored set to true if the mepire has explored this system
   */
  public void setExploredBy(byte empire, boolean explored) {
    if (((this.exploredBy & empire) == 0) != explored) {
      if (explored)
        this.exploredBy = (byte) (this.exploredBy & (~empire));
      else
        this.exploredBy = (byte) (this.exploredBy | empire);

      systInfo.markChanged();
    }
  }

  public void setExploredByMask(byte mask) {
    if (this.exploredBy != mask) {
      this.exploredBy = mask;
      systInfo.markChanged();
    }
  }

  // @returns whether the extended build queue patch is detected
  public boolean setExtra(byte[] extraData) throws IOException {
    if (playerData != null && extraData.length == 96) {
      playerData.setExtendedBuildQueueData(extraData);
      systInfo.markChanged();
      return true;
    } else {
      if (!Arrays.equals(this.extra, extraData)) {
        extra = extraData;
        systInfo.markChanged();
      }
      return false;
    }
  }

  // #endregion property sets

  // #endregion properties

  // #region helper routines

  public void removeIncomingTradeRoute(TradeId tradeRouteId) {
    if (tradeRoutes != null && tradeRoutesIncoming > 0 && tradeRoutes.remove(tradeRouteId)) {
      tradeRoutesIncoming--;
      systInfo.markChanged();
    }
  }

  /**
   * Appends a default planet to the end of the planet list
   * @throws IOException
   */
  public PlanetEntry addPlanet() throws IOException {
    PlanetEntry planet = new PlanetEntry(systInfo, numPlanets, Optional.of(this));
    addPlanet(planet);
    return planet;
  }

  /**
   * Appends a planet with specified max population effective attributes
   * to the end of the planet list.
   * @throws IOException
   */
  public PlanetEntry addPlanet(int planetType, short planetSize, int atmosphere, short engyBonus, short growthBonus) throws IOException {
    PlanetEntry planet = new PlanetEntry(systInfo, numPlanets, Optional.of(this));
    planet.setPlanetType(planetType);
    planet.setPlanetSize(planetSize);
    planet.setAtmosphere(atmosphere);
    planet.setEnergyBonus(engyBonus);
    planet.setGrowthBonus(growthBonus);
    addPlanet(planet);
    return planet;
  }

  public void addPlanet(PlanetEntry planet) throws IOException {
    val stbof = systInfo.stbof().get();
    Planspac planSpace = stbof.files().planSpac();

    planets.add(planet);
    numPlanets = (short) planets.size();

    // add planet specials entry
    planetSpecials.add(0);
    // update max population
    if (planet.isTerraformed()) {
      maxPopulation += planSpace.get(planet.getPlanetType(), planet.getPlanetSize(),
        planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
    }

    systInfo.markChanged();
  }

  /**
   * Copies a planet entry to the end of the planet list
   * @throws IOException
   */
  public void copyPlanet(PlanetEntry planet) throws IOException {
    // copy existing planet
    PlanetEntry pe = new PlanetEntry(systInfo, planet, Optional.of(this));

    planets.add(pe);
    numPlanets = (short) planets.size();
    addPlanetTraits(pe);
    systInfo.markChanged();
  }

  /**
   * Removes a planet from the planet list
   * @param index the planet index
   * @returns the removed planet entry
   * @throws IOException
   */
  public PlanetEntry removePlanet(int index) throws IOException {
    PlanetEntry removed = planets.remove(index);
    numPlanets = (short) planets.size();

    // set indexes
    for (int i = index; i < numPlanets; i++) {
      PlanetEntry pe = getPlanet(i);
      pe.setIndex((short) i);
    }

    remPlanetTraits(removed);
    systInfo.markChanged();
    return removed;
  }

  /**
   * Moves a planet one position up in the planet list
   * @param index the planet index
   */
  public void movePlanetUp(short index) {
    if (index == 0)
      return;

    PlanetEntry temp = getPlanet(index);
    PlanetEntry temp2 = getPlanet(index - 1);
    temp2.setIndex(index);
    temp.setIndex((short) (index - 1));
    planets.set(index, temp2);
    planets.set(index - 1, temp);
    systInfo.markChanged();
  }

  /**
   * Moves a planet one position down in the planet list
   * @param index the planet index
   */
  public void movePlanetDown(short index) {
    if (index == numPlanets - 1)
      return;

    PlanetEntry temp = getPlanet(index);
    PlanetEntry temp2 = getPlanet(index + 1);
    temp2.setIndex(index);
    temp.setIndex((short) (index + 1));
    planets.set(index, temp2);
    planets.set(index + 1, temp);
    systInfo.markChanged();
  }

  public void updateSystemTraits() {
    systemTraits &= 0xFFFFFF00;
    maxPopulation = 0;

    try {
      for (PlanetEntry pe : planets) {
        addPlanetTraits(pe);
      }

      if (population > maxPopulation)
        population = maxPopulation;
    }
    catch(IOException e) {
      e.printStackTrace();
    }
  }

  public List<TradeId> listIncomingTradeRoutes() {
    return tradeRoutes.stream().filter(x -> x.race() != controllingRace).collect(Collectors.toList());
  }

  public List<TradeId> listOutgoingTradeRoutes() {
    return tradeRoutes.stream().filter(x -> x.race() == controllingRace).collect(Collectors.toList());
  }

  public List<TradeId> removeIncomingTradeRoutes() {
    List<TradeId> rem = new ArrayList<>();

    boolean removed = tradeRoutes.removeIf(tr -> {
      if (tr.race() != controllingRace) {
        rem.add(tr);
        return true;
      }
      return false;
    });

    // always reset the incoming trade routes count
    tradeRoutesIncoming = 0;

    if (removed)
      systInfo.markChanged();

    return rem;
  }

  public List<TradeId> removeLocalTradeRoutes() {
    List<TradeId> rem = new ArrayList<>();

    boolean removed = tradeRoutes.removeIf(tr -> {
      if (tr.race() == controllingRace) {
        rem.add(tr);
        return true;
      }
      return false;
    });

    // always reset the local system trade routes count
    tradeRoutesLocal = 0;

    if (removed)
      systInfo.markChanged();

    return rem;
  }

  public void updateTradeRouteId(TradeId prevId, TradeId newId) {
    // process ids in sequence and retain the sorting
    for (int i = 0; i < tradeRoutes.size(); ++i) {
      if (tradeRoutes.get(i).equals(prevId)) {
        tradeRoutes.set(i, newId);
        systInfo.markChanged();
        break;
      }
    }
  }

  public void eliminate() {
    if (!isInhabited())
      return;

    resetSystemControl();
    resetEvents();
    resetAssignments();
    resetBuildOrders();
    resetMorale();
    resetTradeRoutes();
    resetIncome();
    resetProduction();
    resetBonuses();
    unknown_3 = -1;

    systInfo.markChanged();
  }

  public void resetSystemControl() {
    controlType = ControlType.None;
    systemStatus = 0;
    population = 0;
    controllingRace = Race.None;
    residentRace = -1;
    minorHomePlanetMask = 0;
    playerData = null;
    foodStructureType = Short.MAX_VALUE;
    energyStructureType = Short.MAX_VALUE;
    industryStructureType = Short.MAX_VALUE;
    intelStructureType = Short.MAX_VALUE;
    researchStructureType = Short.MAX_VALUE;

    systInfo.markChanged();
  }

  public void resetEvents() {
    popEventGrowth = 0;
    noIndustry = 0;
    noCredits = 0;
    noResearch = 0;
    noIntel = 0;
    moraleEventNbr = 50;
    events = 0;
    updateWorker = 0;
    updatePower = 0;
    noTrade = 0;
    noGrowth = 0;

    systInfo.markChanged();
  }

  public void resetAssignments() {
    pop2Industry = 0;
    pop2Food = 0;
    pop2Energy = 0;
    pop2Intel = 0;
    pop2Research = 0;
    pop2IndustryAmount = 0;
    pop2FoodAmount = 0;
    pop2EnergyAmount = 0;
    pop2IntelAmount = 0;
    pop2ResearchAmount = 0;
    popUnemployed = population;

    systInfo.markChanged();
  }

  public void resetBuildOrders() {
    buildOrder = CStbof.BuildOrder.TradeGoods;
    investedIndustry = 0;
    unrestTurn = 0;
    currentBuildType = 0;
    upgradeBuildType = 0;
    buildCost = 0;
    shipType = 0;
    upgrageType = 0;
    lastBuildOrder = 0;
    lastInvestedIndustry = 0;
    lastUnrestTurn = 0;
    lastCurrentBuildType = 0;
    lastUpgradeBuildType = 0;
    lastBuildCost = 0;
    lastShipType = 0;
    lastUpgradeType = 0;
    buildCostTotal = 0;
    buildsShip = 0;

    systInfo.markChanged();
  }

  public void resetMorale() {
    morale = 0;
    moraleLevel = 0;
    baseMorale = 0;
    moraleProduction = 0;
    moraleEmpireBonus = 0;
    moraleBonus = 0;

    systInfo.markChanged();
  }

  public void resetTradeRoutes() {
    tradeRoutesIncoming = 0;
    tradeRoutesLocal = 0;
    tradeRoutes.clear();

    systInfo.markChanged();
  }

  public void resetIncome() {
    incomeTotal = 0;
    incomeRemaining = 0;
    tradeEmpireIncome = 0;
    incomeExtra = 0;
    incomeBonus = 0;
    incomeEmpireBonus = 0;

    systInfo.markChanged();
  }

  public void resetProduction() {
    foodMaxRawProduction = 0;
    energyMaxRawProduction = 0;
    industryMaxRawProduction = 0;
    researchMaxRawProduction = 0;
    shield = 0;
    intelMaxRawProduction = 0;
    orbitalDefense = 0;
    shipExperience = 0;
    shipYardProduction = 0;
    scanRange = 0;
    dilithiumRawProduction = 0;
    floating_dilithiumRawProduction = 0;
    floating_dilithiumProduction = 0;
    floating_dilithiumSupply = 0;
    floating_buildsShip = 0;
    foodTotal = 0;
    foodRemaining = 0;
    foodConsumption = 0;
    foodMaxGrowth = 0;
    energyTotal = 0;
    energyConsumption = 0;
    energyRemaining = 0;
    industryTotal = 0;
    researchTotal = 0;
    dilithiumProduction = 0;
    dilithiumSupply = 0;
    intelToal = 0;
    autoProduction = 0;

    systInfo.markChanged();
  }

  public void resetBonuses() {
    foodBonus = 0;
    energyBonus = 0;
    industryBonus = 0;
    researchBonus = 0;
    researchEmpireBonus = 0;
    intelBonus_General = 0;
    intelBonus = 0;
    intelBonus_Security = 0;
    intelBonus_Espionage = 0;
    intelBonus_Sabotage = 0;
    intelBonus_Economy = 0;
    researchBonus_Biology = 0;
    researchBonus_Computer = 0;
    researchBonus_Construction = 0;
    researchBonus_Propulsion = 0;
    researchBonus_Weapons = 0;
    researchBonus_Energy = 0;
    tradeRoutesExtra = 0;
    groundDefenseBonus = 0;
    raidDefenseBonus = 0;
    foodExtra = 0;
    energyExtra = 0;
    industryExtra = 0;
    intelExtra = 0;
    researchExtra = 0;
    shipBuildExtra = 0;
    bribeResistanceExtra = 0;
    groundCombatEmpireBonus = 0;

    systInfo.markChanged();
  }

  // #endregion helper routines

  // #region generic info

  /**
   * Called to set the uncompressed file size.
   * <p>
   * If incorrect, the trek.exe load is broken and e.g. fails with: File:
   * ..\..\source\game\solarapi.c, Line: 2792, System is not dest or source of trade route
   */
  public int getSize() {
    int size = HEADER_SIZE + planets.size() * 76;
    size += planetSpecials.size() * 4;
    if (specials != null)
      size += specials.length * SPECIAL_SIZE;

    size += tradeRoutes.size() * 4;
    if (playerData != null)
      size += playerData.getSize();
    if (extra != null)
      size += extra.length;

    return size;
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public int compareTo(SystemEntry se) throws ClassCastException {
    return (index - se.index);
  }

  // #endregion generic info

  // #region load

  public void load(InputStream in) throws IOException {
    planets.clear();

    // index 0-4
    index = StreamTools.readInt(in, true);
    // man 4-4
    controlType = StreamTools.readInt(in, true);
    // name: 8-40 bytes
    name = StreamTools.readNullTerminatedBotfString(in, 40);
    // system status 48-1
    systemStatus = (byte) in.read();
    // star type 49-1
    starType = in.read();
    // star ani 50-5
    starAni = StreamTools.readNullTerminatedBotfString(in, 5);
    // unknown_2 55-8
    StreamTools.read(in, empty_1);
    // explored by
    exploredBy = (byte) in.read();
    // population 64-4
    population = StreamTools.readInt(in, true);
    // resident 68-2
    residentRace = StreamTools.readShort(in, true);
    // unknown_3 70-6
    StreamTools.read(in, empty_2);
    // controlling race 76-4
    controllingRace = StreamTools.readInt(in, true);
    // repeated index 80-4
    index_2 = StreamTools.readInt(in, true);
    // population allocated to industry 84-4
    pop2Industry = StreamTools.readInt(in, true);
    // population allocated to energy 88-4
    pop2Energy = StreamTools.readInt(in, true);
    pop2Food = StreamTools.readInt(in, true);
    // population allocated to research 96-4
    pop2Research = StreamTools.readInt(in, true);
    // population allocated to intel 100-4
    pop2Intel = StreamTools.readInt(in, true);
    // artifactsNbr 104-2
    artifactsNbr = StreamTools.readShort(in, true);
    // planets 106-2
    numPlanets = StreamTools.readShort(in, true);
    // number of specials 108-2
    short numSpecials = StreamTools.readShort(in, true);
    // dilithium amount 110-2
    naturalDilithium = StreamTools.readShort(in, true);
    // system resources 112-4
    systemTraits = StreamTools.readInt(in, true);
    // planet atmospheres bit mask 116-4
    minorHomePlanetMask = StreamTools.readInt(in, true);
    // horizontal position 120-4
    systemPosX = StreamTools.readInt(in, true);
    // vertical position 124-4
    systemPosY = StreamTools.readInt(in, true);
    // vertical position 128-4
    buildOrder = StreamTools.readInt(in, true);
    // industry invested 132-2
    investedIndustry = StreamTools.readShort(in, true);
    // 134-2
    unrestTurn = StreamTools.readShort(in, true);
    // 136-2
    currentBuildType = StreamTools.readShort(in, true);
    // 138-2
    upgradeBuildType = StreamTools.readShort(in, true);
    // 140-4
    buildCost = StreamTools.readInt(in, true);
    // 144-2
    shipType = StreamTools.readShort(in, true);
    // 146-2
    upgrageType = StreamTools.readShort(in, true);
    // 148-4
    lastBuildOrder = StreamTools.readInt(in, true);
    // 152-2
    lastInvestedIndustry = StreamTools.readShort(in, true);
    // 154-2
    lastUnrestTurn = StreamTools.readShort(in, true);
    // 156-2
    lastCurrentBuildType = StreamTools.readShort(in, true);
    // 158-2
    lastUpgradeBuildType = StreamTools.readShort(in, true);
    // 160-4
    lastBuildCost = StreamTools.readInt(in, true);
    // 164-2
    lastShipType = StreamTools.readShort(in, true);
    // 166-2
    lastUpgradeType = StreamTools.readShort(in, true);
    // 168-4
    addrPlayerProductionQueue = StreamTools.readInt(in, true);
    // 172-4
    addrStrcInfo = StreamTools.readInt(in, true);
    // 176-4
    addrPlanetList = StreamTools.readInt(in, true);
    // 180-4
    addrPlanetSpecials = StreamTools.readInt(in, true);
    // 184-4
    addrSpecials = StreamTools.readInt(in, true);
    // 188-4
    maxPopulation = StreamTools.readInt(in, true);
    // 192-124
    StreamTools.read(in, floating_guiPlaceholder1);
    // 316-4
    floating_dilithiumRawProduction = StreamTools.readInt(in, true);
    // 320-64
    StreamTools.read(in, floating_guiPlaceholder2);
    // 384-4
    foodMaxRawProduction = StreamTools.readInt(in, true);
    // 388-4
    foodBonus = StreamTools.readInt(in, true);
    // 392-4
    energyMaxRawProduction = StreamTools.readInt(in, true);
    // 396-4
    energyBonus = StreamTools.readInt(in, true);
    // 400-4
    industryMaxRawProduction = StreamTools.readInt(in, true);
    // 404-4
    industryBonus = StreamTools.readInt(in, true);
    // 408-4
    researchMaxRawProduction = StreamTools.readInt(in, true);
    // 412-4
    researchBonus = StreamTools.readInt(in, true);
    // 416-4
    researchEmpireBonus = StreamTools.readInt(in, true);
    // 420-4
    incomeExtra = StreamTools.readInt(in, true);
    // 424-4
    incomeBonus = StreamTools.readInt(in, true);
    // 428-4
    incomeEmpireBonus = StreamTools.readInt(in, true);
    // 432-4
    shield = StreamTools.readInt(in, true);
    // 436-4
    moraleProduction = StreamTools.readInt(in, true);
    // 440-4
    intelMaxRawProduction = StreamTools.readInt(in, true);
    // 444-4
    intelBonus_General = StreamTools.readInt(in, true);
    // 448-4
    intelBonus = StreamTools.readInt(in, true);
    // 452-4
    intelBonus_Security = StreamTools.readInt(in, true);
    // 456-4
    intelBonus_Espionage = StreamTools.readInt(in, true);
    // 460-4
    intelBonus_Sabotage = StreamTools.readInt(in, true);
    // 464-4
    intelBonus_Economy = StreamTools.readInt(in, true);
    // 468-4
    orbitalDefense = StreamTools.readInt(in, true);
    // 472-4
    shipExperience = StreamTools.readInt(in, true);
    // 476-4
    shipYardProduction = StreamTools.readInt(in, true);
    // 480-4
    researchBonus_Biology = StreamTools.readInt(in, true);
    // 484-4
    researchBonus_Computer = StreamTools.readInt(in, true);
    // 488-4
    researchBonus_Construction = StreamTools.readInt(in, true);
    // 492-4
    researchBonus_Propulsion = StreamTools.readInt(in, true);
    // 496-4
    researchBonus_Weapons = StreamTools.readInt(in, true);
    // 500-4
    researchBonus_Energy = StreamTools.readInt(in, true);
    // 504-4
    scanRange = StreamTools.readInt(in, true);
    // 508-4
    dilithiumRawProduction = StreamTools.readInt(in, true);
    // 512-4
    tradeRoutesExtra = StreamTools.readInt(in, true);
    // 516-4
    empty_3 = StreamTools.readInt(in, true);
    // 520-4
    moraleEmpireBonus = StreamTools.readInt(in, true);
    // 524-4
    moraleBonus = StreamTools.readInt(in, true);
    // 528-4
    tradeEmpireIncome = StreamTools.readInt(in, true);
    // 532-4
    groundDefenseBonus = StreamTools.readInt(in, true);
    // 536-4
    raidDefenseBonus = StreamTools.readInt(in, true);
    // 540-4
    empty_4 = StreamTools.readInt(in, true);
    // 544-4
    foodExtra = StreamTools.readInt(in, true);
    // 548-4
    energyExtra = StreamTools.readInt(in, true);
    // 552-4
    industryExtra = StreamTools.readInt(in, true);
    // 556-4
    intelExtra = StreamTools.readInt(in, true);
    // 560-4
    researchExtra = StreamTools.readInt(in, true);
    // 564-4
    shipBuildExtra = StreamTools.readInt(in, true);
    // 568-4
    bribeResistanceExtra = StreamTools.readInt(in, true);
    // 572-4
    groundCombatEmpireBonus = StreamTools.readInt(in, true);
    // 576-52
    StreamTools.read(in, floating_guiPlaceholder3);
    // 628-4
    floating_dilithiumProduction = StreamTools.readInt(in, true);
    // 632-4
    floating_dilithiumSupply = StreamTools.readInt(in, true);
    // 636-4
    floating_buildsShip = StreamTools.readInt(in, true);
    // 640-24
    StreamTools.read(in, floating_guiPlaceholder4);
    // 664-4
    unknown_3 = StreamTools.readInt(in, true);
    // 668-2
    popEventGrowth = StreamTools.readShort(in, true);
    // 670-2
    pop2IndustryAmount = StreamTools.readShort(in, true);
    // 672-2
    pop2FoodAmount = StreamTools.readShort(in, true);
    // 674-2
    pop2EnergyAmount = StreamTools.readShort(in, true);
    // 676-2
    pop2IntelAmount = StreamTools.readShort(in, true);
    // 678-2
    pop2ResearchAmount = StreamTools.readShort(in, true);
    // 680-4
    popUnemployed = StreamTools.readInt(in, true);
    // 684-4
    foodTotal = StreamTools.readInt(in, true);
    // 688-2
    foodRemaining = StreamTools.readShort(in, true);
    // 690-2
    foodConsumption = StreamTools.readShort(in, true);
    // 692-2
    foodMaxGrowth = StreamTools.readShort(in, true);
    // 694-2
    energyTotal = StreamTools.readShort(in, true);
    // 696-2

    energyConsumption = StreamTools.readShort(in, true);
    // 698-2
    energyRemaining = StreamTools.readShort(in, true);
    // 700-4
    incomeTotal = StreamTools.readInt(in, true);
    // 704-2
    incomeRemaining = StreamTools.readShort(in, true);
    // 706-2
    buildCostTotal = StreamTools.readShort(in, true);
    // 708-4
    industryTotal = StreamTools.readInt(in, true);
    // 712-4
    researchTotal = StreamTools.readInt(in, true);
    // 716-4
    dilithiumProduction = StreamTools.readInt(in, true);
    // 720-4
    dilithiumSupply = StreamTools.readInt(in, true);
    // 724-4
    buildsShip = StreamTools.readInt(in, true);
    // 728-4
    intelToal = StreamTools.readInt(in, true);
    // morale 732-2
    morale = StreamTools.readShort(in, true);
    // morale level for description text 734-2
    moraleLevel = StreamTools.readShort(in, true);
    // 736-4
    noIndustry = StreamTools.readInt(in, true);
    // 740-4
    noCredits = StreamTools.readInt(in, true);
    // 744-4
    noResearch = StreamTools.readInt(in, true);
    // 748-4
    noIntel = StreamTools.readInt(in, true);
    // 752-2
    baseMorale = StreamTools.readShort(in, true);
    // 754-2
    empty_5 = StreamTools.readShort(in, true);
    // 756-4
    moraleEventNbr = StreamTools.readInt(in, true);
    // 760-2
    foodStructureType = StreamTools.readShort(in, true);
    // 762-2
    energyStructureType = StreamTools.readShort(in, true);
    // 764-2
    industryStructureType = StreamTools.readShort(in, true);
    // 766-2
    intelStructureType = StreamTools.readShort(in, true);
    // 768-2
    researchStructureType = StreamTools.readShort(in, true);
    // 770-2
    empty_6 = StreamTools.readShort(in, true);
    // 772-4
    events = StreamTools.readInt(in, true);
    // 776-1
    tradeRoutesIncoming = (byte) in.read();
    // 777-1
    tradeRoutesLocal = (byte) in.read();
    // 778-2
    empty_7 = StreamTools.readShort(in, true);
    // 780-4
    addrIncomingTradeRoutes = StreamTools.readInt(in, true);
    // 784-4
    addrLocalTradeRoutes = StreamTools.readInt(in, true);
    // 788-4
    updateWorker = StreamTools.readInt(in, true);
    // 792-4
    updatePower = StreamTools.readInt(in, true);
    // 796-4
    noTrade = StreamTools.readInt(in, true);
    // 800-4
    noGrowth = StreamTools.readInt(in, true);
    // 804-4
    autoProduction = StreamTools.readInt(in, true);

    // planet data
    planets.ensureCapacity(numPlanets);
    for (short i = 0; i < numPlanets; ++i) {
      planets.add(new PlanetEntry(systInfo, in, Optional.of(this)));
    }

    // unused planet specials
    planetSpecials.ensureCapacity(numPlanets);
    for (int i = 0; i < numPlanets; ++i) {
      planetSpecials.add(StreamTools.readInt(in, true));
    }

    // unused system specials
    if (numSpecials > 0) {
      specials = new byte[SPECIAL_SIZE][numSpecials];
      for (int i = 0; i < numSpecials; ++i) {
        StreamTools.read(in, specials[i]);
      }
    }

    // trade route identifiers
    int numTradeRoutes = getNumTradeRoutes();
    tradeRoutes.clear();
    for (int i = 0; i < numTradeRoutes; ++i) {
      tradeRoutes.add(new TradeId(in));
    }

    // player production queue data
    if (controlType == ControlType.Player)
      playerData = new PlayerData(in);

    // read error extra data space
    extra = new byte[0];
  }

  // #endregion load

  // #region save

  public boolean save(OutputStream out) throws IOException {
    // index 0-4
    out.write(DataTools.toByte(index, true));
    // control type 4-4
    out.write(DataTools.toByte(controlType, true));
    // name: 8-40 bytes
    out.write(DataTools.toByte(name, 40, 39));
    // system status 48-1
    out.write(systemStatus);
    // star type 49-1
    out.write((byte)starType);
    // star ani 50-5
    out.write(DataTools.toByte(starAni, 5, 4));
    // empty_1 55-8
    out.write(empty_1);
    // explored by
    out.write(exploredBy);
    // population 64-4
    out.write(DataTools.toByte(population, true));
    // resident 68-2
    out.write(DataTools.toByte(residentRace, true));
    // empty_2 70-6
    out.write(empty_2);
    // controlling race 76-4
    out.write(DataTools.toByte(controllingRace, true));
    // repeated index 80-4
    out.write(DataTools.toByte(index_2, true));
    // population allocated to industry 84-4
    out.write(DataTools.toByte(pop2Industry, true));
    // population allocated to energy 88-4
    out.write(DataTools.toByte(pop2Energy, true));
    // population allocated to food 92-4
    out.write(DataTools.toByte(pop2Food, true));
    // population allocated to research 96-4
    out.write(DataTools.toByte(pop2Research, true));
    // population allocated to intel 100-4
    out.write(DataTools.toByte(pop2Intel, true));
    // artifactsNbr 104-2
    out.write(DataTools.toByte(artifactsNbr, true));
    // planets 106-2
    out.write(DataTools.toByte((short)planets.size(), true));
    // number of specials 108-2
    short numSpecials = specials != null ? (short) specials.length : 0;
    out.write(DataTools.toByte(numSpecials, true));
    // dilithium amount 110-2
    out.write(DataTools.toByte(naturalDilithium, true));
    // system resources 112-4
    out.write(DataTools.toByte(systemTraits, true));
    // minorHomePlanetFlag 116-4
    out.write(DataTools.toByte(minorHomePlanetMask, true));
    // horizontal position 120-4
    out.write(DataTools.toByte(systemPosX, true));
    // vertical position 124-4
    out.write(DataTools.toByte(systemPosY, true));
    // build queue order type 128-4
    out.write(DataTools.toByte(buildOrder, true));
    // industry invested 132-2
    out.write(DataTools.toByte(investedIndustry, true));
    // 134-2
    out.write(DataTools.toByte(unrestTurn, true));
    // 136-2
    out.write(DataTools.toByte(currentBuildType, true));
    // 138-2
    out.write(DataTools.toByte(upgradeBuildType, true));
    // 140-4
    out.write(DataTools.toByte(buildCost, true));
    // 144-2
    out.write(DataTools.toByte(shipType, true));
    // 146-2
    out.write(DataTools.toByte(upgrageType, true));
    // 148-4
    out.write(DataTools.toByte(lastBuildOrder, true));
    // 152-2
    out.write(DataTools.toByte(lastInvestedIndustry, true));
    // 154-2
    out.write(DataTools.toByte(lastUnrestTurn, true));
    // 156-2
    out.write(DataTools.toByte(lastCurrentBuildType, true));
    // 158-2
    out.write(DataTools.toByte(lastUpgradeBuildType, true));
    // 160-4
    out.write(DataTools.toByte(lastBuildCost, true));
    // 164-2
    out.write(DataTools.toByte(lastShipType, true));
    // 166-2
    out.write(DataTools.toByte(lastUpgradeType, true));
    // 168-4
    out.write(DataTools.toByte(addrPlayerProductionQueue, true));
    // 172-4
    out.write(DataTools.toByte(addrStrcInfo, true));
    // 176-4
    out.write(DataTools.toByte(addrPlanetList, true));
    // 180-4
    out.write(DataTools.toByte(addrPlanetSpecials, true));
    // 184-4
    out.write(DataTools.toByte(addrSpecials, true));
    // 188-4
    out.write(DataTools.toByte(maxPopulation, true));
    // 192-124
    out.write(floating_guiPlaceholder1);
    // 316-4
    out.write(DataTools.toByte(floating_dilithiumRawProduction, true));
    // 320-64
    out.write(floating_guiPlaceholder2);
    // 384-4
    out.write(DataTools.toByte(foodMaxRawProduction, true));
    // 388-4
    out.write(DataTools.toByte(foodBonus, true));
    // 392-4
    out.write(DataTools.toByte(energyMaxRawProduction, true));
    // 396-4
    out.write(DataTools.toByte(energyBonus, true));
    // 400-4
    out.write(DataTools.toByte(industryMaxRawProduction, true));
    // 404-4
    out.write(DataTools.toByte(industryBonus, true));
    // 408-4
    out.write(DataTools.toByte(researchMaxRawProduction, true));
    // 412-4
    out.write(DataTools.toByte(researchBonus, true));
    // 416-4
    out.write(DataTools.toByte(researchEmpireBonus, true));
    // 420-4
    out.write(DataTools.toByte(incomeExtra, true));
    // 424-4
    out.write(DataTools.toByte(incomeBonus, true));
    // 428-4
    out.write(DataTools.toByte(incomeEmpireBonus, true));
    // 432-4
    out.write(DataTools.toByte(shield, true));
    // 436-4
    out.write(DataTools.toByte(moraleProduction, true));
    // 440-4
    out.write(DataTools.toByte(intelMaxRawProduction, true));
    // 444-4
    out.write(DataTools.toByte(intelBonus_General, true));
    // 448-4
    out.write(DataTools.toByte(intelBonus, true));
    // 452-4
    out.write(DataTools.toByte(intelBonus_Security, true));
    // 456-4
    out.write(DataTools.toByte(intelBonus_Espionage, true));
    // 460-4
    out.write(DataTools.toByte(intelBonus_Sabotage, true));
    // 464-4
    out.write(DataTools.toByte(intelBonus_Economy, true));
    // 468-4
    out.write(DataTools.toByte(orbitalDefense, true));
    // 472-4
    out.write(DataTools.toByte(shipExperience, true));
    // 476-4
    out.write(DataTools.toByte(shipYardProduction, true));
    // 480-4
    out.write(DataTools.toByte(researchBonus_Biology, true));
    // 484-4
    out.write(DataTools.toByte(researchBonus_Computer, true));
    // 488-4
    out.write(DataTools.toByte(researchBonus_Construction, true));
    // 492-4
    out.write(DataTools.toByte(researchBonus_Propulsion, true));
    // 496-4
    out.write(DataTools.toByte(researchBonus_Weapons, true));
    // 500-4
    out.write(DataTools.toByte(researchBonus_Energy, true));
    // 504-4
    out.write(DataTools.toByte(scanRange, true));
    // 508-4
    out.write(DataTools.toByte(dilithiumRawProduction, true));
    // 512-4
    out.write(DataTools.toByte(tradeRoutesExtra, true));
    // 516-4
    out.write(DataTools.toByte(empty_3, true));
    // 520-4
    out.write(DataTools.toByte(moraleEmpireBonus, true));
    // 524-4
    out.write(DataTools.toByte(moraleBonus, true));
    // 528-4
    out.write(DataTools.toByte(tradeEmpireIncome, true));
    // 532-4
    out.write(DataTools.toByte(groundDefenseBonus, true));
    // 536-4
    out.write(DataTools.toByte(raidDefenseBonus, true));
    // 540-4
    out.write(DataTools.toByte(empty_4, true));
    // 544-4
    out.write(DataTools.toByte(foodExtra, true));
    // 548-4
    out.write(DataTools.toByte(energyExtra, true));
    // 552-4
    out.write(DataTools.toByte(industryExtra, true));
    // 556-4
    out.write(DataTools.toByte(intelExtra, true));
    // 560-4
    out.write(DataTools.toByte(researchExtra, true));
    // 564-4
    out.write(DataTools.toByte(shipBuildExtra, true));
    // 568-4
    out.write(DataTools.toByte(bribeResistanceExtra, true));
    // 572-4
    out.write(DataTools.toByte(groundCombatEmpireBonus, true));
    // 576-52
    out.write(floating_guiPlaceholder3);
    // 628-4
    out.write(DataTools.toByte(floating_dilithiumProduction, true));
    // 632-4
    out.write(DataTools.toByte(floating_dilithiumSupply, true));
    // 636-4
    out.write(DataTools.toByte(floating_buildsShip, true));
    // 640-24
    out.write(floating_guiPlaceholder4);
    // 664-4
    out.write(DataTools.toByte(unknown_3, true));
    // 668-2
    out.write(DataTools.toByte(popEventGrowth, true));
    // 670-2
    out.write(DataTools.toByte(pop2IndustryAmount, true));
    // 672-2
    out.write(DataTools.toByte(pop2FoodAmount, true));
    // 674-2
    out.write(DataTools.toByte(pop2EnergyAmount, true));
    // 676-2
    out.write(DataTools.toByte(pop2IntelAmount, true));
    // 678-2
    out.write(DataTools.toByte(pop2ResearchAmount, true));
    // 680-4
    out.write(DataTools.toByte(popUnemployed, true));
    // 684-4
    out.write(DataTools.toByte(foodTotal, true));
    // 688-2
    out.write(DataTools.toByte(foodRemaining, true));
    // 690-2
    out.write(DataTools.toByte(foodConsumption, true));
    // 692-2
    out.write(DataTools.toByte(foodMaxGrowth, true));
    // 694-2
    out.write(DataTools.toByte(energyTotal, true));
    // 696-2
    out.write(DataTools.toByte(energyConsumption, true));
    // 698-2
    out.write(DataTools.toByte(energyRemaining, true));
    // 700-4
    out.write(DataTools.toByte(incomeTotal, true));
    // 704-2
    out.write(DataTools.toByte(incomeRemaining, true));
    // 706-2
    out.write(DataTools.toByte(buildCostTotal, true));
    // 708-4
    out.write(DataTools.toByte(industryTotal, true));
    // 712-4
    out.write(DataTools.toByte(researchTotal, true));
    // 716-4
    out.write(DataTools.toByte(dilithiumProduction, true));
    // 720-4
    out.write(DataTools.toByte(dilithiumSupply, true));
    // 724-4
    out.write(DataTools.toByte(buildsShip, true));
    // 728-4
    out.write(DataTools.toByte(intelToal, true));
    // morale 732-2
    out.write(DataTools.toByte(morale, true));
    // morale level for description text 734-2
    out.write(DataTools.toByte(moraleLevel, true));
    // 736-4
    out.write(DataTools.toByte(noIndustry, true));
    // 740-4
    out.write(DataTools.toByte(noCredits, true));
    // 744-4
    out.write(DataTools.toByte(noResearch, true));
    // 748-4
    out.write(DataTools.toByte(noIntel, true));
    // 752-2
    out.write(DataTools.toByte(baseMorale, true));
    // 754-2
    out.write(DataTools.toByte(empty_5, true));
    // 756-4
    out.write(DataTools.toByte(moraleEventNbr, true));
    // 760-2
    out.write(DataTools.toByte(foodStructureType, true));
    // 762-2
    out.write(DataTools.toByte(energyStructureType, true));
    // 764-2
    out.write(DataTools.toByte(industryStructureType, true));
    // 766-2
    out.write(DataTools.toByte(intelStructureType, true));
    // 768-2
    out.write(DataTools.toByte(researchStructureType, true));
    // 770-2
    out.write(DataTools.toByte(empty_6, true));
    // 772-4
    out.write(DataTools.toByte(events, true));
    // 776-1
    out.write(tradeRoutesIncoming);
    // 777-1
    out.write(tradeRoutesLocal);
    // 778-2
    out.write(DataTools.toByte(empty_7, true));
    // 780-4
    out.write(DataTools.toByte(addrIncomingTradeRoutes, true));
    // 784-4
    out.write(DataTools.toByte(addrLocalTradeRoutes, true));
    // 788-4
    out.write(DataTools.toByte(updateWorker, true));
    // 792-4
    out.write(DataTools.toByte(updatePower, true));
    // 796-4
    out.write(DataTools.toByte(noTrade, true));
    // 800-4
    out.write(DataTools.toByte(noGrowth, true));
    // 804-4
    out.write(DataTools.toByte(autoProduction, true));

    // planet data
    PlanetEntry pe;
    for (int i = 0; i < planets.size(); i++) {
      pe = planets.get(i);
      pe.save(out);
    }

    // unused planet specials
    for (int special : planetSpecials) {
      out.write(DataTools.toByte(special, true));
    }

    // unused system specials
    if (specials != null) {
      for (byte[] special : specials) {
        out.write(special);
      }
    }

    // trade route identifiers
    if (tradeRoutes != null) {
      for (TradeId tradeRoute : tradeRoutes) {
        tradeRoute.save(out);
      }
    }

    // player production queue data
    if (playerData != null)
      playerData.save(out);

    // read error extra data space
    if (extra != null)
      out.write(extra);

    return true;
  }

  // #endregion save

  // #region check

  public boolean check(Vector<String> response) {
    boolean changed = false;

    if (controlType < 0 || controlType > 2) {
      String msg = Language.getString("SystemEntry.0"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(controlType)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (name.length() > 39) {
      String msg = Language.getString("SystemEntry.1"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    // support "Extra" system status (BOP)
    if (systemStatus < 0 || systemStatus > SystemStatus.Extra) {
      String msg = Language.getString("SystemEntry.27"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(systemStatus)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    else if (systemStatus == SystemStatus.Extra) {
      String msg = Language.getString("SystemEntry.28"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
    }

    checkStellType(starType, response);

    if (starAni.length() > 4) {
      String msg = Language.getString("SystemEntry.3"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", starAni); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (population < 0) {
      String msg = Language.getString("SystemEntry.4"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(population)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (pop2Industry < 0) {
      String msg = Language.getString("SystemEntry.5"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(pop2Industry)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (pop2Energy < 0) {
      String msg = Language.getString("SystemEntry.6"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(pop2Energy)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (pop2Food < 0) {
      String msg = Language.getString("SystemEntry.7"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(pop2Food)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (pop2Research < 0) {
      String msg = Language.getString("SystemEntry.8"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(pop2Research)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (pop2Intel < 0) {
      String msg = Language.getString("SystemEntry.9"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(pop2Intel)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    // usually the morale never exceeds 195, but by modding, it can go up to 200
    if (morale < 0 || morale > 200) {
      String msg = Language.getString("SystemEntry.10"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(morale)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (moraleLevel < MoraleLevel.Rebellious || moraleLevel > MoraleLevel.Fanatic) {
      String msg = Language.getString("SystemEntry.11"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(moraleLevel)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    // The update worker flag looks to be a broken feature, sometimes corrupting save games,
    // see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56104#p56104
    //
    // In other cases, like after conquering a system, or on subspace scanner upgrade,
    // it however seems to work without crashing the game.
    if (updateWorker != 0) {
      String msg = Language.getString("SystemEntry.26") //$NON-NLS-1$
        .replace("%1", descriptor()) + " (unset)"; //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
      updateWorker = 0;
    }

    // if set, there should be one specials entry per planet
    int numPlanSp = planetSpecials != null ? planetSpecials.size() : 0;
    if (numPlanSp != numPlanets) {
      String msg = Language.getString("SystemEntry.12"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(numPlanSp)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
    }

    int routes = tradeRoutes != null ? tradeRoutes.size() : 0;
    int numTradeRoutes = getNumTradeRoutes();
    if (numTradeRoutes != routes) {
      String msg = Language.getString("SystemEntry.13"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(routes)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    if (extra != null) {
      if (extra.length >= HEADER_SIZE) {
        String msg = Language.getString("SystemEntry.14"); //$NON-NLS-1$
        msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
    }

    // check planet entries
    Hashtable<Short, String> idset = new Hashtable<Short, String>();

    for (short i = 0; i < planets.size(); i++) {
      PlanetEntry planetEntry = planets.get(i);

      // check for id duplicates
      if (idset.containsKey(i)) {
        String msg = Language.getString("SystemEntry.15"); //$NON-NLS-1$
        msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
        msg = msg.replace("%2", planetEntry.getName()); //$NON-NLS-1$
        msg = msg.replace("%3", idset.get(planetEntry.getIndex())); //$NON-NLS-1$
        msg = msg.replace("%4", Integer.toString(i)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      } else {
        idset.put(planetEntry.getIndex(), planetEntry.getName());
      }

      // check planet properties
      planetEntry.check(response, Optional.of(this));

      // check for missing system traits
      int planetType = planetEntry.getPlanetType();
      if (planetType >= 0 && planetType <= 10 && (systemTraits & (1 << planetType)) == 0) {
        String msg = "Planet shift in %1: Missing system trait for %2 planet type " + planetType + ". (common, but fixed)";
        msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        systemTraits = systemTraits | (1 << planetType);
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
        changed = true;
        systInfo.markChanged();
      }
    }

    // check for invalid system traits caused by planet shift
    for (int planetType = 0; planetType <= 10; ++planetType) {
      if ((systemTraits & (1 << planetType)) != 0) {
        boolean found = false;

        for (int i = 0; i < planets.size(); i++) {
          PlanetEntry pe = planets.get(i);
          if (pe.getPlanetType() == planetType) {
            found = true;
            break;
          }
        }

        if (!found) {
          String msg = "Planet shift in %1: Invalid system trait for planet type " + planetType + ". (common, but fixed)";
          msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
          systemTraits = systemTraits & ~(1 << planetType);
          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
          changed = true;
          systInfo.markChanged();
        }
      }
    }

    return changed;
  }

  private void checkStellType(int starType, Vector<String> response) {
    if (starType >= 0 && starType < 12)
      return;

    val stbof = systInfo.stbof();
    if (!stbof.isPresent())
      return;

    Optional<Objstruc> stell = stbof.get().files().findObjStruc();
    if (!stell.isPresent())
      return;

    if (!stell.get().hasIndex(starType)) {
      String msg = Language.getString("SystemEntry.2"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(starType)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  private void checkPlanetIndex(int plIndex) {
    if (plIndex < 0 || plIndex >= planets.size()) {
      String msg = Language.getString("SystemEntry.25"); //$NON-NLS-1$
      msg = msg.replace("%1", descriptor()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(plIndex)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(systInfo.getName() + ": " + msg);
    }
  }

  private String getCheckIntegrityString(int type, String msg) {
    return Checkable.getCheckIntegrityString(ClassName, type, msg);
  }

  // #endregion check

  // #region PlayerData

  public class PlayerData {

    BuildQueueEntry[] buildQueueEntries;    // 96
    public int slotsUsed;                   // 100 the number of slots in use
    public int unknown;                     // 104 always 1, possibly indicates whether it's active
    public int buy1st;                      // 108 buy first slot item, see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54344#p54344

    public PlayerData(InputStream in) throws IOException {
      // default build queue size to 4 entries
      // extended build queue patch is detected and updated by next system search
      // @see setExtendedBuildQueueData
      buildQueueEntries = new BuildQueueEntry[4];
      for (int i = 0; i < 4; i++) {
        buildQueueEntries[i] = new BuildQueueEntry(in);
      }

      slotsUsed = StreamTools.readInt(in, true);
      unknown = StreamTools.readInt(in, true);
      buy1st = StreamTools.readInt(in, true);
    }

    public PlayerData(PlayerData playerData) {
      buildQueueEntries = new BuildQueueEntry[playerData.buildQueueEntries.length];
      for (int i = 0; i < buildQueueEntries.length; i++) {
        buildQueueEntries[i] = new BuildQueueEntry(playerData.buildQueueEntries[i]);
      }

      slotsUsed = playerData.slotsUsed;
      unknown = playerData.unknown;
      buy1st = playerData.buy1st;
    }

    public void setExtendedBuildQueueData(byte[] extraData) throws IOException {
      BuildQueueEntry[] entries = new BuildQueueEntry[8];

      // add already loaded entries
      for (int i = 0; i < 4; i++)
        entries[i] = buildQueueEntries[i];

      // parse back faulty queue end data
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      out.write(DataTools.toByte(slotsUsed, true));
      out.write(DataTools.toByte(unknown, true));
      out.write(DataTools.toByte(buy1st, true));
      out.write(extraData);

      // re-parse entries
      ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
      for (int i = 4; i < 8; i++)
        entries[i] = new BuildQueueEntry(in);

      // update entries
      buildQueueEntries = entries;

      // re-parse faulty queue end data
      slotsUsed = StreamTools.readInt(in, true);
      unknown = StreamTools.readInt(in, true);
      buy1st = StreamTools.readInt(in, true);
    }

    public int getSize() {
      return 0xC + buildQueueEntries.length * BuildQueueEntry.SIZE;
    }

    public void save(OutputStream out) throws IOException {
      for (int i = 0; i < buildQueueEntries.length; i++) {
        buildQueueEntries[i].save(out);
      }

      out.write(DataTools.toByte(slotsUsed, true));
      out.write(DataTools.toByte(unknown, true));
      out.write(DataTools.toByte(buy1st, true));
    }

    public class BuildQueueEntry {

      public static final int SIZE = 24; // 0x18

      public int    buildOrder;       // 0x00 + 4 = 4  -> @see ue.edit.res.stbof.CStbof.BuildOrder
      public short  investedIndustry; // 0x04 + 2 = 6
      public short  unrestTurn;       // 0x06 + 2 = 8  -> turn count for unrest order delay
      public short  currentBuildType; // 0x08 + 2 = 10 -> current build structure type (or max signed int 7FFFh)
      public short  upgradeBuildType; // 0x0A + 2 = 12 -> new upgrade build structure type (or max signed int 7FFFh)
      public int    buildCost;        // 0x0C + 4 = 16 -> total build cost
      public short  shipType;         // 0x10 + 2 = 18 -> or -1 or 0
      public short  upgrageType;      // 0x12 + 2 = 20 -> @see ue.edit.res.stbof.CStbof.UpgradeType
      public int    buildCount;       // 0x14 + 4 = 24

      public BuildQueueEntry(InputStream in) throws IOException {
        // 0-4
        buildOrder = StreamTools.readInt(in, true);
        // 4-2
        investedIndustry = StreamTools.readShort(in, true);
        // 6-2
        unrestTurn = StreamTools.readShort(in, true);
        // 8-2
        currentBuildType = StreamTools.readShort(in, true);
        // 10-2
        upgradeBuildType = StreamTools.readShort(in, true);
        // 12-4
        buildCost = StreamTools.readInt(in, true);
        // 16-2
        shipType = StreamTools.readShort(in, true);
        // 18-2
        upgrageType = StreamTools.readShort(in, true);
        // 20-4
        buildCount = StreamTools.readInt(in, true);
      }

      public BuildQueueEntry(BuildQueueEntry buildQueueEntry) {
        buildOrder = buildQueueEntry.buildOrder;
        investedIndustry = buildQueueEntry.investedIndustry;
        unrestTurn = buildQueueEntry.unrestTurn;
        currentBuildType = buildQueueEntry.currentBuildType;
        upgradeBuildType = buildQueueEntry.upgradeBuildType;
        buildCost = buildQueueEntry.buildCost;
        shipType = buildQueueEntry.shipType;
        upgrageType = buildQueueEntry.upgrageType;
        buildCount = buildQueueEntry.buildCount;
      }

      public void save(OutputStream out) throws IOException {
        // 0-4
        out.write(DataTools.toByte(buildOrder, true));
        // 4-2
        out.write(DataTools.toByte(investedIndustry, true));
        // 6-2
        out.write(DataTools.toByte(unrestTurn, true));
        // 8-2
        out.write(DataTools.toByte(currentBuildType, true));
        // 10-2
        out.write(DataTools.toByte(upgradeBuildType, true));
        // 12-4
        out.write(DataTools.toByte(buildCost, true));
        // 16-2
        out.write(DataTools.toByte(shipType, true));
        // 18-2
        out.write(DataTools.toByte(upgrageType, true));
        // 20-4
        out.write(DataTools.toByte(buildCount, true));
      }
    }
  }

  // #endregion PlayerData

  // #region stats

  public void incrementBaseStats(SystemBaseStats stats, Optional<Stbof> stbof) {
    Optional<Planboni> optPlanBoni = stbof.isPresent() ? stbof.get().files().findPlanBoni() : Optional.empty();
    Optional<Planspac> optPlanSpace = stbof.isPresent() ? stbof.get().files().findPlanSpac() : Optional.empty();

    switch (systemStatus) {
      case SystemStatus.Subjugated:
      case SystemStatus.ConqueredHome:
        stats.systemsSubjugated++;
        break;
      case SystemStatus.Rebel:
        stats.systemsRebelled++;
    }

    if (population > 0) {
      stats.systemsColonized++;
      stats.populationTotal += population;

      // morale
      stats.moraleAmount += morale;
      int moraleIdx = moraleLevel + 4;
      // 0:rebellious 1:defiant 2:disgruntled 3:apathetic 4:content 5:pleased 6:loyal 7:fanatic
      if (moraleIdx >= 0 && moraleIdx < stats.moraleLvls.length)
        stats.moraleLvls[moraleIdx]++;

      if (buildOrder == BuildOrder.TradeGoods)
        stats.systemsIdle++;

      if (foodTotal < foodConsumption)
        stats.systemsStarving++;
    }

    stats.dilithiumProduction += dilithiumRawProduction;
    stats.dilithiumShortage += dilithiumProduction;

    // production raw ( cumulated edifice.bst output ) & total ( + bonuses )
    stats.foodProductionTotal += foodTotal;
    stats.foodProductionMaxRaw += foodMaxRawProduction;
    stats.foodProductionAssigned += foodMaxRawProduction * pop2FoodAmount / 100;
    stats.foodProductionExtra += foodExtra;
    stats.foodProductionBonus += foodMaxRawProduction * pop2FoodAmount * foodBonus / 10000;
    stats.foodConsumption += foodConsumption;
    stats.foodExcess += foodRemaining;
    stats.industryProductionTotal += industryTotal;
    stats.industryProductionMaxRaw += industryMaxRawProduction;
    stats.industryProductionAssigned += industryMaxRawProduction * pop2IndustryAmount / 100;
    stats.industryProductionExtra += industryExtra;
    stats.industryProductionBonus += industryMaxRawProduction * pop2IndustryAmount * industryBonus / 10000;
    stats.industryInvested += investedIndustry;
    stats.industryUsage += Integer.min(buildCost, industryTotal);
    stats.energyProductionTotal += energyTotal;
    stats.energyProductionMaxRaw += energyMaxRawProduction;
    stats.energyProductionAssigned += energyMaxRawProduction * pop2EnergyAmount / 100;
    stats.energyProductionExtra += energyExtra;
    stats.energyProductionBonus += energyMaxRawProduction * pop2EnergyAmount * energyBonus / 10000;
    stats.energyConsumption += energyConsumption;
    stats.energyExcess += energyRemaining;
    stats.intelTotal += intelToal;
    stats.intelMaxRaw += intelMaxRawProduction;
    stats.intelAssigned += intelMaxRawProduction * pop2IntelAmount / 100;
    stats.intelExtra += intelExtra;
    stats.intelBonus += intelMaxRawProduction * pop2IntelAmount * intelBonus / 10000;
    stats.researchTotal += researchTotal;
    stats.researchMaxRaw += researchMaxRawProduction;
    stats.researchAssigned += researchMaxRawProduction * pop2ResearchAmount / 100;
    stats.researchExtra += researchExtra;
    stats.researchBonus += researchMaxRawProduction * pop2ResearchAmount * (researchBonus + researchEmpireBonus) / 10000;
    stats.incomeTotal += incomeTotal;
    stats.incomeExtra += incomeExtra;
    // there is no raw income, therefore calculate by total income
    int incomeRaw = incomeTotal * 100 / (100 + incomeBonus + incomeEmpireBonus);
    stats.incomeRaw += incomeRaw;
    stats.incomeBonus += incomeTotal - incomeRaw;
    stats.incomeRemnant += incomeRemaining;
    stats.shipBuildExtra += shipBuildExtra;

    if (buildsShip == 1)
      stats.shipsInBuild++;

    stats.tradeRoutesIncoming += tradeRoutesIncoming;
    stats.tradeRoutesLocal += tradeRoutesLocal;
    stats.tradeRoutesExtra += tradeRoutesExtra;

    stats.starTypes.merge(starType, 1, Calc::sum);

    boolean hasOwner = controlType == ControlType.AI || controlType == ControlType.Player;
    if (hasOwner) {
      stats.planetsOwned += numPlanets;
      stats.systemsOwned++;
      if (naturalDilithium != 0)
        stats.dilithiumOwned++;
    }

    int inhabitable = 0;
    for (PlanetEntry planet : planets) {
      int planetType = planet.getPlanetType();
      int planetSize = planet.getPlanetSize();

      // planet size
      if (planetSize >= PlanetSize.Large)
        stats.planetsLarge++;
      else if (planetSize >= PlanetSize.Medium)
        stats.planetsMedium++;
      else
        stats.planetsSmall++;

      // terraforming & max population
      if (optPlanSpace.isPresent()) {
        int maxpop = optPlanSpace.get().get(planetType, planetSize,
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());

        if (maxpop > 0) {
          inhabitable++;
          stats.planetsInhabitable++;
          stats.populationMax += maxpop;

          // both are 0 when terraformed
          if (planet.getTerraformed() >= planet.getTerraformPoints()) {
            stats.planetsTerraformed++;
            stats.populationTerraformed += maxpop;
            if (population > 0)
              stats.planetsColonized++;
          }
        }
      }

      // planet type mapped stats
      PlanetStats planetStats = stats.addStatsPlanetType(planetType);
      planetStats.size[planetSize]++;

      // planet type & bonus stats
      if (optPlanBoni.isPresent()) {
        int bonus = optPlanBoni.get().get(planetType, planetSize, planet.getFoodBonusLvl(), planet.getEnergyBonusLvl());
        if (bonus >= 0) {
          switch (planetType) {
            case PlanetType.Jungle:
            case PlanetType.Oceanic:
            case PlanetType.Terran: {
              // food bonus: 0, 0+, 0.1+, 0.2+, 0.3+, 0.4+, 0.5+
              int bonusIdx = bonus == 0 ? 0 : 1 + bonus / 2;
              bonusIdx = Integer.min(bonusIdx, 6);
              stats.foodBonus[bonusIdx]++;
              break;
            }
            // arctic and barren planets, as well as gas giants, have energy bonus type
            // in vanilla BotF however by the planboni.bin mappings it is always mapped to zero
            // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=162&t=204
            case PlanetType.Desert:
            case PlanetType.Volcanic:
            case PlanetType.Arctic:
            case PlanetType.Barren:
            case PlanetType.GasGiant:
            default: {
              // energy bonus: 0, 0+, 0.25+, 0.5+, 0.75+, 1.0+
              int bonusIdx = bonus <= 0 ? 0 : 1 + bonus / 5;
              bonusIdx = Integer.min(bonusIdx, 5);
              stats.energyBonus[bonusIdx]++;
            }
          }
        }
      }
    }

    if (inhabitable > 0)
      stats.systemsInhabitable++;
  }

  // #endregion stats

  // #region private helpers

  private void addPlanetTraits(PlanetEntry pe) throws IOException {
    // update system features
    systemTraits |= 1 << pe.getPlanetType();

    // update max system population
    if (pe.isTerraformed()) {
      int maxPlanPop = maxPlanetPop(pe);
      maxPopulation += maxPlanPop;
    }
  }

  private void remPlanetTraits(PlanetEntry pe) throws IOException {
    // reset system features
    resetSysReq();

    // update max system population
    if (pe.isTerraformed()) {
      int maxPlanPop = maxPlanetPop(pe);

      maxPopulation = Integer.max(maxPopulation - maxPlanPop, 0);
      if (maxPopulation > population)
        population = maxPopulation;
    }
  }

  private void resetSysReq() {
    systemTraits &= 0xFFFFFF00;
    for (PlanetEntry planet : planets) {
      systemTraits |= 1 << planet.getPlanetType();
    }
  }

  private int maxPlanetPop(PlanetEntry pe) throws IOException {
    val stbof = systInfo.stbof().get();
    Planspac planSpace = stbof.files().planSpac();
    return planSpace.get(pe.getPlanetType(), pe.getPlanetSize(),
      pe.getEnergyBonusLvl(), pe.getGrowthBonusLvl());
  }

  // #endregion private helpers

}
