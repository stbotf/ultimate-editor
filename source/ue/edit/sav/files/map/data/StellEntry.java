package ue.edit.sav.files.map.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.StellInfo;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

/**
 * The StellEntry class lists properties of a single stellar object instance.
 * It can be accessed by the stellInfo file, which lists all the stellar object instances.
 */
@Getter
public class StellEntry {

  public static final int SIZE = 88;

  private StellInfo stellInfo;

  // #region data fields

  private int     type;
  private String  name = "";
  private String  animation = "blkh";
  private short   link = -1;
  private byte[]  unknown_1 = new byte[2];
  private int     posX;
  private int     posY;
  private int     visitorsMask;

  // #endregion data fields

  // #region constructor

  public StellEntry(StellInfo stellInfo) {
    this.stellInfo = stellInfo;
  }

  public StellEntry(StellInfo stellInfo, int h_pos, int v_pos) {
    this.stellInfo = stellInfo;
    this.posX = h_pos;
    this.posY = v_pos;
  }

  // #endregion constructor

  // #region properties

  // #region property gets

  public int[] getPosition() {
    int[] pos = new int[2];
    pos[0] = posX / 5;
    pos[1] = posY / 5;
    return pos;
  }

  public int getSectorIndex() throws IOException {
    Sector sectorLst = stellInfo.savGame().files().sectorLst();
    int x = posX / 5;
    int y = posY / 5;
    return y * sectorLst.getHSectors() + x;
  }

  // #endregion property gets

  // #region property sets

  /**
   * Sets the name of the stellar object.
   * @param name  the new name
   */
  public void setName(String name) {
    if (!StringTools.equals(this.name, name)) {
      this.name = name;
      stellInfo.markChanged();
    }
  }

  /**
   * Sets the animation of the stellar object.
   * @param ani   the animation file
   */
  public void setAnimation(String ani) {
    if (!StringTools.equals(this.animation, ani)) {
      this.animation = ani;
      stellInfo.markChanged();
    }
  }

  /**
   * Sets the position of the stellar object.
   * @param h_row the index of the horizontal row (0+)
   * @param v_row the index of the vertical row (0+)
   */
  public void setPosition(int h_row, int v_row) {
    int h_pos = h_row * 5 + (int) (Math.random() * 3) + 1;
    int v_pos = v_row * 5 + (int) (Math.random() * 3) + 1;
    if (posX != h_pos || posY != v_pos) {
      posX = h_pos;
      posY = v_pos;
      stellInfo.markChanged();
    }
  }

  /**
   * Sets the type of the stellar object.
   * @param type  the type
   */
  public void setType(int type) {
    if (this.type != type) {
      this.type = type;
      stellInfo.markChanged();
    }
  }

  /**
   * Sets the stellar object index this one links to.
   * NOTE: links work only on wormholes.
   * @param link  the index to which this stellar object should link
   */
  public void setLink(short link) {
    if (this.link != link) {
      this.link = link;
      stellInfo.markChanged();
    }
  }

  /**
   * Sets which empires already moved a ship to explore the stellar object.
   * @param mask  the empire visitors mask
   */
  public void setVisitorsMask(int mask) {
    if (this.visitorsMask != mask) {
      this.visitorsMask = mask;
      stellInfo.markChanged();
    }
  }

  // #endregion property sets

  // #endregion properties

  // #region helper routines

  public void onRemovedStellObj(int index) {
    if (link > index)
      link--;
    else if (link == index)
      link = -1;
  }

  // #endregion helper routines

  // #region load

  public void load(InputStream in) throws IOException {
    type = StreamTools.readInt(in, true);                         //  0 + 4
    name = StreamTools.readNullTerminatedBotfString(in, 40);      //  4 + 40
    animation = StreamTools.readNullTerminatedBotfString(in, 20); // 44 + 20
    link = StreamTools.readShort(in, true);                       // 64 + 2
    StreamTools.read(in, unknown_1);                              // 66 + 2
    posX = StreamTools.readInt(in, true);                         // 68 + 4
    posY = StreamTools.readInt(in, true);                         // 72 + 4
    // again horizontal and vertical
    StreamTools.skip(in, 8);                                      // 76 + 8
    visitorsMask = StreamTools.readInt(in, true);                 // 84 + 4
  }

  // #endregion load

  // #region save

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(name, 40, 39));
    out.write(DataTools.toByte(animation, 20, 19));
    out.write(DataTools.toByte(link, true));
    out.write(unknown_1);
    out.write(DataTools.toByte(posX, true));
    out.write(DataTools.toByte(posY, true));
    out.write(DataTools.toByte(posX, true));
    out.write(DataTools.toByte(posY, true));
    out.write(DataTools.toByte(visitorsMask, true));
  }

  // #endregion save

}
