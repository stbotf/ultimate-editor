package ue.edit.sav.files.map.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@AllArgsConstructor
@Data @Accessors(fluent = true)
public class TradeId implements Comparable<TradeId> {

  private final short race;
  private final short number;

  public TradeId(InputStream in) throws IOException {
    race = StreamTools.readShort(in, true);
    number = StreamTools.readShort(in, true);
  }

  public int index() {
    return (race << 16) | number;
  }

  @Override
  public int compareTo(TradeId trId) {
    return index() - trId.index();
  }

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(race, true));
    out.write(DataTools.toByte(number, true));
  }

  public TradeId next() {
    return new TradeId(race, (short)(number + 1));
  }

  public TradeId previous() {
    return new TradeId(race, (short)(number - 1));
  }

}
