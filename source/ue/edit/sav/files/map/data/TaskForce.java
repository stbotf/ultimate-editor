package ue.edit.sav.files.map.data;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.common.Checkable;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavGame.AbilityMask;
import ue.edit.sav.common.CSavGame.ControlCategory;
import ue.edit.sav.common.CSavGame.ControlFlags;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.OrderInfo;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.orders.MilitaryOrder;
import ue.edit.sav.files.map.orders.Order;
import ue.edit.sav.tools.MapTools;
import ue.edit.sav.tools.TaskForceTools;
import ue.exception.KeyNotFoundException;
import ue.util.data.DataTools;
import ue.util.data.CollectionTools;
import ue.util.stream.StreamTools;

/**
 * @author Alan
 */
public class TaskForce {

  public static final int SIZE = 108;

  @Getter private TaskForceList tfList;
  private GShipList shipList;
  private ShipList shipModels;
  private OrderInfo ordInfo;
  // maintain ship order by a plain ArrayList collection
  private ArrayList<Integer> shipIds = new ArrayList<Integer>();
  @Getter private boolean detectedTFPatch = false;

  // By GShipList & ordInfo / result.lst the task force ids are limited to 16-bit word.
  // Further all negative short type values are reserved temporary ids to be replaced next turn.
  // Except FF FF = -1, which is reserved for 'no task force'.
  //
  // Temporary ids are assigned when re-grouping the task forces.
  // They are only stored to GWTForce, while GTForceList is left untouched.
  // They are offset for the different empires and counted down by -1 each task force.
  // They must not be considered for the TForceCntr, as it soon crashes the game on turn progress.
  //
  // Empire starting id offsets are:
  // Cardassians: FF FE (-2)
  // Federation:  EC 78 (-5000)
  // Ferengi:     D8 F0 (-10000)
  // Klingons:    C5 68 (-15000)
  // Romulans:    B1 E0 (-20000)
  // Minors:      9E 58 (-25000) (guessed)
  @Getter @Accessors(fluent = true)
  private short id;                                 // 0x00 +  2 = 2

  // Following the id are another two empty data alignment bytes,
  // that must not be read for the task force id, given that would break the negative temporary ids.
  // E.g. first temporary cardassian id is -2 = FF FE while it is followed by 00 00.
  // Further other files only support 2 bytes for the task force id anyhow.
  private short empty_1;                            // 0x02 +  2 = 4 unused empty space

  private int shipCount;                            // 0x04 +  4 = 8 auto-determined on save
  private byte[] shipIdData = new byte[36];         // 0x08 + 36 = 44
  @Getter private int mission;                      // 0x2C +  4 = 48 order/command, see ue.edit.sav.CSavGame.Mission
  // Mission data, e.g. for TT system attack and move destination.
  // The move destination is the sector.lst target sector index, disabled by -1
  @Getter private int targetSectorIndex;            // 0x30 +  4 = 52 (MissionData) row*columns + column-1, 0xFFFFFFFF if none
  @Getter private int ownerId;                      // 0x34 +  4 = 56
  @Getter private int sectorRow;                    // 0x38 +  4 = 60
  @Getter private int sectorColumn;                 // 0x3C +  4 = 64

  // task force abilities
  // short  abilityMask;                            // 0x40 +  2 = 66 see ue.edit.sav.CSavGame.AbilityMask, SCT: only word gets read
  private short empty_2;                            // 0x42 +  2 = 68 unused empty space
  // int    firstColonyShipID;                      // 0x44 +  4 = 72
  private byte[] unknown_1 = new byte[4];           // 0x48 +  4 = 76
  @Getter private int controlCategory;              // 0x4C +  4 = 80 See CONTROL fields at the top
  // short  production;                             // 0x50 +  2 = 82: 1 for early tech colony ships, 3 if type II
  // short  scanRange;                              // 0x52 +  2 = 84
  @Getter private int militaryOrderId;              // 0x54 +  4 = 88 A task force order group index, or 0 if none. Can occur multiple times.
  // double mapRange;                               // 0x58 +  8 = 96
  // double mapSpeed;                               // 0x60 +  8 = 104
  @Getter private boolean cloaked;                  // 0x68 +  4 = 108

  @Getter @Accessors(fluent = true)
  private TaskforceAbilities abilities = new TaskforceAbilities();

  public TaskForce(TaskForceList tfList, GShipList shipList, ShipList shipModels, OrderInfo ordInfo) {
    this.tfList = tfList;
    this.shipList = shipList;
    this.shipModels = shipModels;
    this.ordInfo = ordInfo;
  }

  public TaskForce(TaskForceList tfList, GShipList shipList, ShipList shipModels, OrderInfo ordInfo, short taskForceId, int raceId, int x, int y) {
    this(tfList, shipList, shipModels, ordInfo);
    id = taskForceId;
    mission = Mission.Evade;
    targetSectorIndex = -1;
    ownerId = raceId;
    sectorRow = y;
    sectorColumn = x;
    Arrays.fill(unknown_1, (byte) -1);
    controlCategory = raceControlCategory(raceId);
  }

  public boolean isGWTForce() {
    return tfList.isGWTForce();
  }

  public String descriptor() {
    val raceRst = raceRst();
    String race = raceRst.isPresent() ? raceRst.get().getOwnerName(ownerId) : "<owner " + ownerId + ">";
    return race + " " + inlineDescriptor();
  }

  public String inlineDescriptor() {
    String sector = " at " + MapTools.sectorPos(sectorColumn, sectorRow);
    return "task force " + Integer.toString(this.id) + sector;
  }

  private int readShipId(int idx, boolean extended) {
    return extended
      ? ByteBuffer.wrap(shipIdData, 2*idx, 2).order(ByteOrder.LITTLE_ENDIAN).getShort()
      : ByteBuffer.wrap(shipIdData, 4*idx, 4).order(ByteOrder.LITTLE_ENDIAN).getInt();
  }

  // attempt to detect extended 16 ship task force patch
  // which is not fully reliable, since botf sometimes keeps previous ship ids listed
  private boolean detectTFPatch(int shipCount) {
    if (shipCount < 1)
      return false;

    int lastShipIdx = shipCount > 16 ? 15 : shipCount - 1;
    return lastShipIdx > 8 || readShipId(lastShipIdx + 1, true) == -1;
  }

  private void determineShipIds(int shipCount) throws IOException {
    if (shipCount == 0)
      return;

    DeterminedShipIds res = determineShipIdsImpl(shipCount);
    this.shipIds = res.shipIds;
    this.detectedTFPatch = res.extended;
  }

  private class DeterminedShipIds {
    // maintain ship order by a plain ArrayList collection
    ArrayList<Integer> shipIds;
    boolean extended;

    public DeterminedShipIds(ArrayList<Integer> shipIds, boolean extended) {
      this.shipIds = shipIds;
      this.extended = extended;
    }
  }

  private DeterminedShipIds determineShipIdsImpl(int shipCount) throws IOException {
    boolean extended = detectTFPatch(shipCount);

    LoadTFShipsResult res = loadShipIds(extended, shipCount);
    if (res.valid == shipCount)
      return new DeterminedShipIds(res.shipIds, extended);

    // when there was a mismatch, try the other data format
    LoadTFShipsResult alt = loadShipIds(!extended, shipCount);
    if (alt.valid == shipCount)
      return new DeterminedShipIds(alt.shipIds, !extended);

    // when there are no valid ships at all, fallback to the other list
    if (!alt.hasValid())
      return new DeterminedShipIds(res.shipIds, extended);
    if (!res.hasValid())
      return new DeterminedShipIds(alt.shipIds, !extended);

    // for 4byte ids, when the task force id is -1, the ship count clearly exceeds the written ids
    // therefore fallback to 2byte extended 16 ship task force patch
    if (extended && alt.hasInvalid())
      return new DeterminedShipIds(res.shipIds, extended);
    if (!extended && res.hasInvalid())
      return new DeterminedShipIds(alt.shipIds, !extended);

    // when the task force id doesn't match, there clearly must be something wrong
    if (alt.hasWrongTF())
      return new DeterminedShipIds(res.shipIds, extended);
    if (res.hasWrongTF())
      return new DeterminedShipIds(alt.shipIds, !extended);

    // finally, choose whatever the determined parser found
    return new DeterminedShipIds(res.shipIds, extended);
  }

  public void enforceTFPatch(boolean extended) throws IOException {
    LoadTFShipsResult res = loadShipIds(extended, this.shipCount);
    this.shipIds = res.shipIds;
  }

  private class LoadTFShipsResult {
    public int valid = 0;
    public int invalid = 0;
    public int duplicates = 0;
    public int missing = 0;
    public int wrongTF = 0;

    // use a plain ArrayList to maintain the ship sequence
    ArrayList<Integer> shipIds = new ArrayList<Integer>();

    public boolean hasValid() { return valid > 0; }
    public boolean hasInvalid() { return invalid > 0; }
    @SuppressWarnings("unused")
    public boolean hasDuplicates() { return duplicates > 0; }
    @SuppressWarnings("unused")
    public boolean hasMissing() { return missing > 0; }
    public boolean hasWrongTF() { return wrongTF > 0; }
  }

  private LoadTFShipsResult loadShipIds(boolean extended, int shipCount) throws IOException {
    LoadTFShipsResult res = new LoadTFShipsResult();
    if (shipCount == 0)
      return res;

    ByteArrayInputStream in = new ByteArrayInputStream(shipIdData);

    // limit to read specified ship count, BotF doesn't always clean unused slots
    int maxShips = Math.min(shipCount, extended ? 18 : 9);

    for (int i = 0; i < maxShips; ++i) {
      int shpId = extended ? StreamTools.readShort(in, true) : StreamTools.readInt(in, true);

      // return all ships read
      res.shipIds.add(shpId);

      if (shpId == -1) {
        res.invalid++;
        continue;
      }

      // check for already added ships
      if (res.shipIds.contains(shpId)) {
        res.duplicates++;
        continue;
      }

      // check whether the ship exists
      short shpTFId = this.shipList.getShipTaskForceId((short)shpId);
      if (shpTFId == -1) {
        res.missing++;
        continue;
      }

      // GTForceList might list outdated task forces, given other than GWTForce
      // it is not updated on current turn player task force re-groupings.
      // The ship task force id however is always the latest.
      if (shpTFId != id) {
        String msg = "Task force %1 ship %2 has wrong task force id %3."
          .replace("%1", Integer.toString(id))
          .replace("%2", Integer.toString(shpId))
          .replace("%3", Integer.toString(shpTFId));
        System.out.println(msg);
        res.wrongTF++;

        // GWTForce always lists the latest task forces
        // and is kept in sync with GShipList
        if (this.tfList.isGWTForce())
          continue;
      }

      // passed full ship validation
      res.valid++;
    }

    return res;
  }

  // Update the id with caution!! It will both invalidate internal mappings of UE,
  // plus it will invalidate all the BotF taskforce references!!
  // The only updated references are explictly referenced ships and military orders.
  public boolean setId(short taskForceId) {
    if (this.id == taskForceId)
      return false;

    // Always update ships and orders along with GWTForce, not GTForceList!!
    boolean ordersChanged = false;
    if (tfList.isGWTForce()) {

      // update ships for the changed task force id
      for (int shipId : shipIds) {
        Ship shp = shipList.tryGetShip((short)shipId);
        if (shp == null) {
          System.out.println("Ship " + shipId + " is missing from " + shipList.getName());
          continue;
        }

        shp.setTaskForceId(taskForceId);
      }

      // update GWTForce order if referenced
      if (this.militaryOrderId != 0 && tfList.isGWTForce()) {
        Order order = ordInfo.tryGetTaskForceOrder(this.id);
        if (order != null) {
          MilitaryOrder mil = order.militaryOrder();

          // verify military order to resolve TF id duplicates
          if (mil != null && mil.militaryOrderId() == this.militaryOrderId
            && order.getRaceID() == this.ownerId && mil.isMissionCompatible(mission)) {
            try {
              ordInfo.setTaskForceId(this.id, taskForceId);
              ordersChanged = true;
            }
            catch (KeyNotFoundException e) {
              e.printStackTrace();
            }
          }
        }
      }
    }

    this.id = taskForceId;
    tfList.markChanged();
    return ordersChanged;
  }

  public void setOwner(int raceId) {
    if (this.ownerId != raceId) {
      this.ownerId = raceId;
      tfList.markChanged();
    }
  }

  public boolean isInSector(int sectorColumn, int sectorRow) {
    return this.sectorColumn == sectorColumn && this.sectorRow == sectorRow;
  }

  /**
   * @return [column, row]
   */
  public int[] getLocation() {
    return new int[]{sectorColumn, sectorRow};
  }

  public void setLocation(int sectorRow, int sectorColumn) {
    if (this.sectorRow != sectorRow || this.sectorColumn != sectorColumn) {
      this.sectorRow = sectorRow;
      this.sectorColumn = sectorColumn;
      tfList.markChanged();
    }
  }

  /**
   * @return "column.row", 1 based
   */
  public String getSectorPos() {
    return MapTools.sectorPos(sectorColumn, sectorRow);
  }

  public int distanceFrom(int sectorRow, int sectorColumn) {
    int x = Math.abs(this.sectorColumn - sectorColumn);
    int y = Math.abs(this.sectorRow - sectorRow);
    return Math.max(x, y);
  }

  public int targetDistance() {
    if (militaryOrderId == 0)
      return -1;

    Order order = ordInfo.tryGetTaskForceOrder(id);
    if (order == null)
      return -1;

    MilitaryOrder mil = order.militaryOrder();
    if (mil == null)
      return -1;

    return distanceFrom(mil.getTargetSectorRow(), mil.getTargetSectorColumn());
  }

  public void setTargetSector(int sectorIndex) {
    if (this.targetSectorIndex != sectorIndex) {
      this.targetSectorIndex = sectorIndex;
      tfList.markChanged();
    }
  }

  public void setMission(int mission) {
    if (this.mission != mission) {
      this.mission = mission;
      tfList.markChanged();
    }
  }

  /**
   * Ship count & role dependent default task force mission.
   * <p>
   * - default task forces with non-combatants to evade, cause even when they are escorted by combat
   * ships, they usually aren't meant to participate in battle - default single ship missions to
   * evade cause they usually are either on exploration, return to base or are sent for support -
   * default all other task forces to engage
   */
  public int defaultMission() {
    return abilities.hasNonCombatant() || shipIds.size() < 2 ? Mission.Evade : Mission.Engage;
  }

  public void resetMission() {
    // The AI usually disregards the assigned mission but redeploys ships on next turn.
    // Nonetheless for player task forces lets determine some proper mission default.
    int defMission = defaultMission();

    if (mission != defMission) {
      mission = defMission;
      tfList.markChanged();
    }

    resetTargetSector();
    resetOrder();
  }

  public void resetTargetSector() {
    if (targetSectorIndex != -1) {
      targetSectorIndex = -1;
      tfList.markChanged();
    }
  }

  // OrdInfo is updated with GWTForce,
  // e.g. for moving task forces
  public void resetOrder() {
    // clear task force orders
    // not all the time the militaryOrderId is set
    ordInfo.removeTaskForceOrder(this.id);

    if (militaryOrderId != 0) {
      militaryOrderId = 0;
      tfList.markChanged();
    }
  }

  /**
   * reset local sector missions, but retain order & target sector
   */
  public void resetLocalMission() {
    switch (mission) {
      case Mission.Move:
      case Mission.Evade:
      case Mission.Engage:
        break;
      case Mission.AttackSystem:
      case Mission.BuildOutpost:
      case Mission.BuildStarbase:
      case Mission.Colonize:
      case Mission.EnterWormhole:
      case Mission.Intercept:
      case Mission.Raid:
      case Mission.Scrap:
      case Mission.Terraform:
      case Mission.TrainCrew:
      default:
        mission = defaultMission();
        tfList.markChanged();
    }
  }

  /**
   * reset missions that lack the necessary task force abilities
   */
  public void ensureMissionAbility() {
    switch (mission) {
      case Mission.Move:
      case Mission.Evade:
      case Mission.EnterWormhole:
      case Mission.TrainCrew:
        return;
      case Mission.Terraform:
        if (abilities.canTerraform())
          return;
      case Mission.Colonize:
        if (abilities.canColonize())
          return;
      case Mission.Raid:
        if (abilities.canRaid())
          return;
      case Mission.Intercept:
        if (abilities.canIntercept())
          return;
      case Mission.Engage:
        if (abilities.hasCombatant())
          return;
      case Mission.AttackSystem:
        if (abilities.canAttackSystem())
          return;
      case Mission.BuildOutpost:
        if (abilities.canBuildOutpost())
          return;
      case Mission.BuildStarbase:
        if (abilities.canBuildStarbase())
          return;
      case Mission.Scrap:
        if (abilities.canBeScrapped())
          return;
    }

    // reset mission & task force order
    resetMission();
  }

  public int shipCount() {
    return shipIds.size();
  }

  public boolean hasShip(short shipId) {
    return shipIds.contains((int)shipId);
  }

  public short[] getShipIDs() {
    // GShipList is limited to short type index values
    // which also limits to determine task force ship abilities
    // therefore cast down to unify the index values
    return CollectionTools.intToShortArray(shipIds);
  }

  public Set<Short> getShipIdSet() {
    return shipIds.stream()
      .map(x -> x.shortValue())
      .collect(Collectors.toSet());
  }

  public void addShip(short shipId) throws KeyNotFoundException {
    int maxShips = this.tfList.isExtendedTFPatch() ? 18 : 9;
    if (shipIds.size() >= maxShips)
      throw new RuntimeException("No free ships slot available!");

    if (hasShip(shipId)) {
      System.out.println(tfList.getName() + ": Task force " + this.id + " ship " + shipId + " already added!");
      return;
    }

    // add ship
    shipIds.add((int) shipId);
    tfList.markChanged();

    // make sure to update the ship task force id
    shipList.getShip(shipId).setTaskForceId(this.id);

    // update task force abilities
    abilities.addShipAbilities(shipId);
  }

  public void addShips(Collection<Short> shipIds) throws KeyNotFoundException {
    for (short shipId : shipIds)
      addShip(shipId);
  }

  public boolean removeShip(short shipId) {
    if (shipIds.remove(Integer.valueOf(shipId))) {
      // update abilities for the remaining ships
      updateAbilities();
      tfList.markChanged();
      return true;
    }
    return false;
  }

  public boolean removeShips(Collection<Short> shipIds) {
    boolean removed = false;

    for (short shipId : shipIds) {
      if (this.shipIds.remove(Integer.valueOf(shipId)))
        removed = true;
    }

    if (removed) {
      // update abilities for the remaining ships
      updateAbilities();
      tfList.markChanged();
    }

    return removed;
  }

  public boolean checkShipValues(SavGame game, Vector<String> response) {
    boolean changed = false;

    if (tfList.isGWTForce()) {
      // For GWTForce, check the ships. Other than expected,
      // they are all updated by current turn changes.
      for (Integer sid : shipIds) {
        short shpId = sid.shortValue();

        Ship shp = shipList.tryGetShip(shpId);
        checkShipTaskForce(game, shp, response);
        checkShipOwner(game, shp, response);
      }

      // check task force order
      // for GWTForce it must always be in sync with OrdInfo
      changed |= checkOrder(game, response);
    }

    return changed;
  }

  private void checkShipTaskForce(SavGame game, Ship shp, Vector<String> response) {
    int shpTfId = shp.getTaskForceId();
    if (shpTfId != this.id) {
      String msg = "%1 has task force id %2, but actually is listed by task force %3. (fixed)";
      msg = msg.replace("%1", shp.descriptor());
      msg = msg.replace("%2", Integer.toString(shpTfId));
      msg = msg.replace("%3", Integer.toString(this.id));
      response.add(tfList.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));

      // fix task force id
      shp.setTaskForceId(this.id);
    }
  }

  private void checkShipOwner(SavGame game, Ship shp, Vector<String> response) {
    int shpRaceId = shp.getRaceID();
    if (shpRaceId != ownerId) {
      String msg = "%1 has race id %2, but actually is owned by %3. (fixed)";
      msg = msg.replace("%1", shp.descriptor());
      msg = msg.replace("%2", Integer.toString(shp.getTaskForceId()));
      msg = msg.replace("%3", Integer.toString(this.id));
      response.add(tfList.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));

      // fix ship owner
      shp.setRaceId((short)ownerId);

      // update ship agents
      try {
        TaskForceTools tfTools = game.files().taskForceTools();
        tfTools.removeFromShipAgents(shpRaceId, shp.shipID());
        tfTools.addShipToAgents(game, shp);
      }
      catch (IOException e) {
        msg = "Failed to fix %1 owner agents for TF %2:\n";
        msg = msg.replace("%1", shp.descriptor());
        msg = msg.replace("%2", Integer.toString(this.id));
        msg += e.getMessage();
        response.add(tfList.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
    }
  }

  private boolean checkOrder(SavGame game, Vector<String> response) {
    Order order = ordInfo.tryGetTaskForceOrder(this.id);
    if (order == null) {
      // it is not unusual that the referred order is missing
      // this also happens with working vanilla save games
      if (militaryOrderId != 0) {
        String msg = descriptor() + " misses the assigned military order %1 in %2. (common, but unset)"
          .replace("%1", Integer.toString(this.militaryOrderId))
          .replace("%2", CSavFiles.OrdInfo);
        response.add(Checkable.getCheckIntegrityString(tfList.getName(),
            InternalFile.INTEGRITY_CHECK_INFO, msg));

        militaryOrderId = 0;
        tfList.markChanged();
        return true;
      }
      return false;
    }

    MilitaryOrder tfOrder = order.militaryOrder();
    int ordMilId = tfOrder.militaryOrderId();
    if (ordMilId != militaryOrderId) {
      String msg = descriptor() + " has wrong military order %1 set instead of %2. (fixed)"
        .replace("%1", Integer.toString(this.militaryOrderId))
        .replace("%2", Integer.toString(ordMilId));
      response.add(Checkable.getCheckIntegrityString(tfList.getName(),
        InternalFile.INTEGRITY_CHECK_WARNING, msg));

      militaryOrderId = ordMilId;
      tfList.markChanged();
      return true;
    }

    return false;
  }

  private short getShipAbilities(Ship ship) {
    int shipType = ship.getShipType();
    ShipDefinition shipDef = shipModels.getShipDefinition(shipType);
    int shipRole = shipDef.getShipRole();

    int stealth = shipDef.getStealth();
    short abilityMask = stealth > 3 ? AbilityMask.CanCloak : 0;

    switch (shipRole) {
      // note that cloaked ships are unflagged for bombardment
      case ShipRole.Scout:
      case ShipRole.Destroyer:
      case ShipRole.Cruiser:
        abilityMask |= AbilityMask.CanRaidSystem | AbilityMask.CanIntercept
            | AbilityMask.CanBombardSystem;
        break;
      case ShipRole.StrikeCruiser:
      case ShipRole.BattleShip:
        abilityMask |= AbilityMask.CanRaidSystem | AbilityMask.CanBombardSystem;
        break;

      case ShipRole.ColonyShip:
        abilityMask |= AbilityMask.CanTerraform | AbilityMask.CanColonize;
        break;
      case ShipRole.Monster:
        break;
      case ShipRole.TroopTransport:
        abilityMask |= AbilityMask.CanBuildOutpost | AbilityMask.CanBuildStarbase
            | AbilityMask.CanInvadeSystem;
        break;
      case ShipRole.None:
      default:
        String err = "%1 of %2 has invalid ship type: %3" //$NON-NLS-1$
          .replace("%1", ship.descriptor()) //$NON-NLS-1$
          .replace("%2", descriptor()) //$NON-NLS-1$
          .replace("%3", Integer.toString(shipType)); //$NON-NLS-1$
        System.err.println(err);
    }

    return abilityMask;
  }

  public void updateAbilities() {
    TaskforceAbilities tfAbilities = new TaskforceAbilities();
    for (int id : shipIds) {
      if (id != -1)
        tfAbilities.addShipAbilities((short) id);
    }

    if (!tfAbilities.equals(abilities)) {
      abilities = tfAbilities;
      cloaked &= tfAbilities.canCloak();
      tfList.markChanged();

      // check whether to reset the task force mission
      ensureMissionAbility();
    }
  }

  public void load(InputStream in) throws IOException {
    id = StreamTools.readShort(in, true);
    empty_1 = StreamTools.readShort(in, true);

    // read ship count to limit valid ship ids
    // and to help detect 18 ship task force patch
    shipCount = StreamTools.readInt(in, true);

    // support extended task force patch (UDM3, GALM,...)
    // by reading and analyzing the full ship id data block
    StreamTools.read(in, shipIdData);
    determineShipIds(shipCount);

    mission = StreamTools.readInt(in, true);
    targetSectorIndex = StreamTools.readInt(in, true);
    ownerId = StreamTools.readInt(in, true);
    sectorRow = StreamTools.readInt(in, true);
    sectorColumn = StreamTools.readInt(in, true);
    abilities.abilityMask = StreamTools.readShort(in, true);
    empty_2 = StreamTools.readShort(in, true);
    abilities.firstColonyShipID = StreamTools.readInt(in, true);
    StreamTools.read(in, unknown_1);
    controlCategory = StreamTools.readInt(in, true);
    abilities.colonyLvl = StreamTools.readShort(in, true);
    abilities.scanRange = StreamTools.readShort(in, true);
    militaryOrderId = StreamTools.readInt(in, true);
    abilities.mapRange = StreamTools.readDouble(in, true);
    abilities.mapSpeed = StreamTools.readDouble(in, true);
    cloaked = StreamTools.readInt(in, true) != 0;
  }

  public void save(OutputStream out, boolean extended) throws IOException {
    out.write(DataTools.toByte(id, true));
    out.write(DataTools.toByte(empty_1, true));
    out.write(DataTools.toByte(shipIds.size(), true));

    for (int shpId : shipIds) {
      if (extended)
        out.write(DataTools.toByte((short)shpId, true));
      else
        out.write(DataTools.toByte(shpId, true));
    }

    // fill unused ship slots
    int maxShips = extended ? 18 : 9;
    int idSize = extended ? 2 : 4;
    int unused = shipIds.size() < maxShips ? maxShips - shipIds.size() : 0;
    for (int i = 0; i < unused * idSize; i++) {
      out.write(0xFF);
    }

    out.write(DataTools.toByte(mission, true));
    out.write(DataTools.toByte(targetSectorIndex, true));
    out.write(DataTools.toByte(ownerId, true));
    out.write(DataTools.toByte(sectorRow, true));
    out.write(DataTools.toByte(sectorColumn, true));
    out.write(DataTools.toByte(abilities.abilityMask, true));
    out.write(DataTools.toByte(empty_2, true));
    out.write(DataTools.toByte(abilities.firstColonyShipID, true));
    out.write(unknown_1);
    out.write(DataTools.toByte(controlCategory, true));
    out.write(DataTools.toByte(abilities.colonyLvl, true));
    out.write(DataTools.toByte(abilities.scanRange, true));
    out.write(DataTools.toByte(militaryOrderId, true));
    out.write(DataTools.toByte(abilities.mapRange, true));
    out.write(DataTools.toByte(abilities.mapSpeed, true));
    out.write(DataTools.toByte(cloaked ? 1 : 0, true));
  }

  public boolean isPlayerControlled() throws IOException {
    GameInfo gameInfo = tfList.sav().files().gameInfo();
    short playerEmpires = gameInfo.getActivePlayerEmpiresMask();

    int raceFlag = 1 << this.ownerId;
    return (playerEmpires & raceFlag) != 0;
  }

  public boolean isControlledBy(int controlFlags) throws IOException {
    // check control category
    switch (controlCategory) {
      case ControlCategory.Monster:
        // TODO: check -> newly deployed monsters don't have an AI agent yet
        return (controlFlags & ControlFlags.Monster) != 0;
      case ControlCategory.Minor:
        return (controlFlags & ControlFlags.Minor) != 0;
      case ControlCategory.Empire:
        if ((controlFlags & (ControlFlags.AIEmpire|ControlFlags.Player)) == 0)
          return false;
    }

    // check player empire
    boolean playerControlled = isPlayerControlled();
    int reqFlags = playerControlled ? ControlFlags.Player : ControlFlags.AIEmpire;
    return (controlFlags & reqFlags) != 0;
  }

  private Optional<RaceRst> raceRst() {
    Stbof stbof = tfList.stbof();
    return stbof != null ? stbof.files().findRaceRst() : Optional.empty();
  }

  private static int raceControlCategory(int raceId) {
    return raceId == Race.Monster ? ControlCategory.Monster
        : raceId > 4 ? ControlCategory.Minor : ControlCategory.Empire;
  }

  @Getter @EqualsAndHashCode
  public class TaskforceAbilities {
    short   abilityMask = AbilityMask.Exclusive;  // 66 see ue.edit.sav.CSavGame.AbilityMask, SCT: only word gets read
    int     firstColonyShipID = -1;               // 72
    short   colonyLvl = 0;                        // 82 1 for early tech colony ships, 3 if type II
    short   scanRange = 0;                        // 84
    double  mapRange = Double.MAX_VALUE;          // 96
    double  mapSpeed = Double.MAX_VALUE;          // 104

    public boolean hasCombatant() {
      return (abilityMask & AbilityMask.Combatant) != 0;
    }

    public boolean hasNonCombatant() {
      return (abilityMask & AbilityMask.NonCombatant) != 0;
    }

    public boolean hasColonyShip() {
      return firstColonyShipID != -1;
    }

    public boolean canAttackSystem() {
      return (abilityMask & AbilityMask.CanAttackSystem) != 0;
    }

    public boolean canRaid() {
      return (abilityMask & AbilityMask.CanRaidSystem) != 0;
    }

    public boolean canIntercept() {
      return (abilityMask & AbilityMask.CanIntercept) != 0;
    }

    public boolean canBuildOutpost() {
      return (abilityMask & AbilityMask.CanBuildOutpost) != 0;
    }

    public boolean canBuildStarbase() {
      return (abilityMask & AbilityMask.CanBuildStarbase) != 0;
    }

    public boolean canTerraform() {
      return (abilityMask & AbilityMask.CanTerraform) != 0;
    }

    public boolean canColonize() {
      return (abilityMask & AbilityMask.CanColonize) != 0;
    }

    public boolean canCloak() {
      return (abilityMask & AbilityMask.CanCloak) != 0;
    }

    public boolean canBeScrapped() {
      return (abilityMask & AbilityMask.CanBeScrapped) != 0;
    }

    public void addShipAbilities(short shipId) {
      // ship can be null by removeAllTaskForces
      // when second task force list becomes out of sync
      Ship ship = shipList.tryGetShip(shipId);
      if (ship == null)
        return;

      short abilities = getShipAbilities(ship);
      abilityMask |= abilities & AbilityMask.Inclusive;
      abilityMask &= AbilityMask.Inclusive | (abilities & AbilityMask.Exclusive);

      // update first colony ship
      if (firstColonyShipID == -1 && (abilities & AbilityMask.CanColonize) != 0)
        firstColonyShipID = shipId;

      ShipDefinition shipDef = shipModels.getShipDefinition(ship.getShipType());
      scanRange = (short) Integer.max(scanRange, shipDef.getScanRange());
      mapRange = Double.min(mapRange, shipDef.getMapRange());
      mapSpeed = Double.min(mapSpeed, shipDef.getMapSpeed());

      if ((abilities & AbilityMask.CanColonize) != 0) {
        // three colony ships still have the same production like it was only one
        colonyLvl = (short) Integer.max(colonyLvl, shipDef.getProduction());
      }
    }
  }

}
