package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import com.google.common.primitives.Bytes;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of the sector.lst file. It holds a list of sectors in the game.
 */
public class Sector extends InternalFile {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public class SectorStats {
    public int sectorsHorizontal = 0;
    public int sectorsVertical = 0;
    public int sectorsOwned = 0;
    public int[] sectorOwns = new int[NUM_EMPIRES + 1];
    public int sectorsClaimed = 0;
    public int[] sectorClaims = new int[NUM_EMPIRES];
    public int sectorsDisputed = 0;
    public int systems = 0;
    public int stellarobjects = 0;
    public int starbases = 0;

    public int sectorsTotal() { return sectorsHorizontal * sectorsVertical; }
    public int sectorsUnclaimed() { return sectorsHorizontal * sectorsVertical - sectorsClaimed; }
  }

  public class EmpireSectorStats {
    public int sectorsOwned = 0;
    public int sectorsClaimed = 0;
    public int sectorsDisputed = 0;
    public int systems = 0;
    public int stellarobjects = 0;
    public int starbases = 0;
    public int explored = 0;
    public int revealed = 0;
  }

  private ArrayList<SectorEntry> ENTRY = new ArrayList<SectorEntry>();
  private int height;
  private int width;
  private SavGame GAME;
  private TaskForceList gwtForce;

  public Sector(SavGame game) throws IOException {
    GAME = game;
    gwtForce = (TaskForceList) GAME.getInternalFile(CSavFiles.GWTForce, true);
  }

  /**
   * Compute sector index by position
   *
   * @param vert vertical position
   * @param hori horizontal position
   */
  public int getSectorIndex(int vert, int hori) {
    int hsize = width + 1;
    return vert * hsize + hori;
  }

  public int getNumberOfSectors() {
    return ENTRY.size();
  }

  public int getStarbaseCount() {
    return (int) ENTRY.stream().filter(x -> x.hasStarBase()).count();
  }

  public int getStellarObjectCount() {
    return (int) ENTRY.stream().filter(x -> x.hasStellar()).count();
  }

  public int getSystemCount() {
    return (int) ENTRY.stream().filter(x -> x.hasSystem()).count();
  }

  public SectorStats getSectorStats() {
    SectorStats stats = new SectorStats();
    stats.sectorsHorizontal = getHSectors();
    stats.sectorsVertical = getVSectors();
    for (SectorEntry entry : ENTRY) {
      if (entry.isClaimed()) {
        stats.sectorsClaimed++;
        int claims = 0;
        for (int empire = 0; empire < NUM_EMPIRES; ++empire) {
          if (entry.isClaimed(empire)) {
            stats.sectorClaims[empire]++;
            claims++;
          }
        }
        // check ownership
        if (claims > 1) {
          stats.sectorsDisputed++;
        }
        else if (claims == 1) {
          int owner = entry.getOwner();
          if (owner != -1) {
            stats.sectorsOwned++;
            if (owner < NUM_EMPIRES)
              stats.sectorOwns[owner]++;
            else
              stats.sectorOwns[NUM_EMPIRES]++;
          }
        }
      }
      if (entry.SYSTEM != -1)
        stats.systems++;
      if (entry.STELLAR != -1)
        stats.stellarobjects++;
      if (entry.STARBASE != -1)
        stats.starbases++;
    }
    return stats;
  }

  public EmpireSectorStats getEmpireStats(int empire) {
    EmpireSectorStats stats = new EmpireSectorStats();

    for (SectorEntry entry : ENTRY) {
      if (0 != (entry.EXPLOREDBY & (1<<empire)))
        stats.explored++;
      if (0 != (entry.REVEALEDBY & (1<<empire)))
        stats.revealed++;

      if (!entry.isClaimed(empire))
        continue;

      stats.sectorsClaimed++;
      if (Integer.bitCount(entry.OWNER_CLAIM) > 1) {
        stats.sectorsDisputed++;
        continue;
      }

      int owner = entry.getOwner();
      if (owner != empire && !(empire == NUM_EMPIRES && owner >= NUM_EMPIRES))
        continue;

      stats.sectorsOwned++;
      if (entry.SYSTEM != -1)
        stats.systems++;
      if (entry.STELLAR != -1)
        stats.stellarobjects++;
      if (entry.STARBASE != -1)
        stats.starbases++;
    }

    return stats;
  }

  public EmpireSectorStats[] getEmpireStats() {
    EmpireSectorStats[] stats = new EmpireSectorStats[NUM_EMPIRES];

    for (int empire = 0; empire < NUM_EMPIRES; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  // specified by SavEdit
  @Override
  public void check(Vector<String> response) {
    String msg;

    if (ENTRY.size() == 0) {
      msg = Language.getString("Sector.0"); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }

    try {
      StarBaseInfo base = (StarBaseInfo) GAME.getInternalFile(CSavFiles.StarBaseInfo, true);
      StellInfo stell = (StellInfo) GAME.getInternalFile(CSavFiles.StellInfo, true);
      SystInfo syst = (SystInfo) GAME.getInternalFile(CSavFiles.SystInfo, true);

      for (int i = 0; i < ENTRY.size(); i++) {
        SectorEntry sec = ENTRY.get(i);

        if (sec.STARBASE > -1 && !base.hasKey(sec.STARBASE)) {
          msg = Language.getString("Sector.1"); //$NON-NLS-1$
          msg = msg.replace("%1", getDescription(i, true)); //$NON-NLS-1$
          msg = msg.replace("%2", Integer.toString(sec.STARBASE)); //$NON-NLS-1$
          msg += " (removed)";
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

          sec.STARBASE = -1;
          this.markChanged();
        }

        if (sec.STELLAR != -1 && sec.SYSTEM != -1) {
          msg = Language.getString("Sector.2"); //$NON-NLS-1$
          msg = msg.replace("%1", getDescription(i, true)); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }

        if (sec.STELLAR != -1) {
          try {
            stell.getStellarObject(sec.STELLAR).getType();
          } catch (IndexOutOfBoundsException ze) {
            msg = Language.getString("Sector.3"); //$NON-NLS-1$
            msg = msg.replace("%1", getDescription(i, true)); //$NON-NLS-1$
            msg = msg.replace("%2", Integer.toString(sec.SYSTEM)); //$NON-NLS-1$
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        }

        if (sec.SYSTEM != -1) {
          try {
            syst.getSystem(sec.SYSTEM).getControlType();
          } catch (IndexOutOfBoundsException ze) {
            msg = Language.getString("Sector.4"); //$NON-NLS-1$
            msg = msg.replace("%1", getDescription(i, true)); //$NON-NLS-1$
            msg = msg.replace("%2", Integer.toString(sec.SYSTEM)); //$NON-NLS-1$
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        }
      }
    } catch (Exception e) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, e.getMessage()));
    }
  }

  @Override
  public int getSize() {
    return ENTRY.size() * 104;
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    while (in.available() > 0) {
      SectorEntry se = new SectorEntry(in);
      ENTRY.add(se);
    }

    // determine map size
    int size = ENTRY.size();
    SectorEntry se = ENTRY.get(size - 1);
    width = se.H_INDEX;
    height = se.V_INDEX;

    // sort sectors by index (y * width + x)
    Collections.sort(ENTRY);

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    int size = ENTRY.size();
    SectorEntry se;
    for (int i = 0; i < size; i++) {
      se = ENTRY.get(i);
      se.save(out);
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  // gets

  /**
   * Returns the number of sectors in a horizontal line
   */
  public int getHSectors() {
    return (width + 1);
  }

  /**
   * Returns the number of sectors in a vertical line
   */
  public int getVSectors() {
    return (height + 1);
  }

  /**
   * Returns the descriptive name of the sector
   *
   * @param index the index of the sector
   */
  public String getDescription(int index, boolean numeric) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.getDescription(numeric);
  }

  public String getNumericPosition(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.getNumericPosition();
  }

  public String getPositionCode(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.getPositionCode();
  }

  public String getHexPosition(int index, String delimeter) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.getHexPosition(delimeter);
  }

  /**
   * Returns the vertical index of a sector.
   *
   * @param index the index of the sector
   */
  public int getVertical(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.V_INDEX;
  }

  /**
   * Returns the horizontal index of a sector.
   *
   * @param index the index of the sector
   */
  public int getHorizontal(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.H_INDEX;
  }

  /**
   * Returns the position of a sector.
   *
   * @param index the index of the sector
   */
  public int[] getPosition(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return new int[]{se.H_INDEX, se.V_INDEX};
  }

  /**
   * Returns the revealed mask of a sector.
   *
   * @param index the index of the sector
   */
  public long getRevealed(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.REVEALEDBY;
  }

  /**
   * Returns the explorer mask of a sector.
   *
   * @param index the index of the sector
   */
  public long getExplored(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.EXPLOREDBY;
  }

  public boolean getExploredBy(int index, int race) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    long mask = 1 << race;
    return (se.EXPLOREDBY & mask) != 0;
  }

  public boolean getScannedBy(int index, int race) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    long mask = 1 << race;
    return (se.REVEALEDBY & mask) != 0;
  }

  /**
   * Returns the system index of a sector.
   *
   * @param index the index of the sector
   * @return the index of the system in this sector or -1 if none
   */
  public short getSystem(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.SYSTEM;
  }

  /**
   * Returns the index of the stellar object in sector.
   *
   * @param index the index of the sector
   * @return the index of the stellar object in this sector or -1 if none
   */
  public short getStellarObject(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.STELLAR;
  }

  /**
   * Sets the index of the stellar object in sector.
   *
   * @param index the index of the sector
   * @param obj   the index of the stellar object in this sector or -1 if none
   */
  public void setStellarObject(int index, short obj) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);

    if (se.STELLAR != obj) {
      se.STELLAR = obj;
      markChanged();
    }
  }

  public void removeSystem(int sectorIndex) {
    checkIndex(sectorIndex);
    SectorEntry se = ENTRY.get(sectorIndex);
    if (se.hasSystem()) {
      se.removeSystem();
      markChanged();
    }
  }

  public void onRemovedSystem(int systemId) {
    for (SectorEntry se : ENTRY) {
      if (se.SYSTEM > systemId) {
        se.SYSTEM -= 1;
        markChanged();
      } else if (se.SYSTEM == systemId) {
        se.SYSTEM = -1;
        markChanged();
      }
    }
  }

  public void removeAllStellarObjects() {
    for (SectorEntry se : ENTRY) {
      if (se.STELLAR != -1) {
        se.STELLAR = -1;
        markChanged();
      }
    }
  }

  /**
   * Returns the id of the star base in sector.
   *
   * @param index the index of the sector
   * @return the index of the star base in this sector or -1 if none
   */
  public short getStarbase(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.STARBASE;
  }

  /**
   * Returns the owner mask of the sector.
   *
   * @param index the index of the sector
   */
  public short getOwnerMask(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.OWNER_CLAIM;
  }

  /**
   * Returns the race of the owner of the sector.
   *
   * @param index the index of the sector
   * @return the owner race or -1 if contested or not owned
   */
  public int getOwnerRace(int index) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    return se.getOwner();
  }

  /**
   * Returns the scan strength of the sector.
   *
   * @param index the index of the sector
   * @param view  the race
   */
  public int getScanStrength(int index, int view) throws InvalidArgumentsException {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    checkView(view);
    return se.SCAN_STRENGTH[view];
  }

  // sets

  /**
   * Sets the scan strength of the sector.
   *
   * @param index the index of the sector
   * @param view  the race
   * @param value the new scan strength
   */
  public void setScanStrength(int index, int view, int value) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    if (view < 0 || view > NUM_EMPIRES) {
      return;
    }
    if (se.SCAN_STRENGTH[view] == value) {
      return;
    }
    se.SCAN_STRENGTH[view] = value;
    this.markChanged();
  }

  /**
   * Sets the revealed mask of a sector.
   *
   * @param index the index of the sector
   * @param rev   new reveal mask
   */
  public void setRevealed(int index, long rev) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    if (se.REVEALEDBY == rev) {
      return;
    }
    se.REVEALEDBY = rev;
    this.markChanged();
  }

  /**
   * Sets the explored mask of a sector.
   *
   * @param index the index of the sector
   * @param rev   new explore mask
   */
  public void setExplored(int index, long rev) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    if (se.EXPLOREDBY == rev) {
      return;
    }
    se.EXPLOREDBY = rev;
    this.markChanged();
  }

  public void setExploredBy(int index, int race, boolean explored) {
    checkIndex(index);

    SectorEntry se = ENTRY.get(index);
    long mask = 1L << race;

    if (((se.EXPLOREDBY & mask) != 0) != explored) {
      if (explored) {
        se.EXPLOREDBY = se.EXPLOREDBY | mask;
      } else {
        mask = ~mask;
        se.EXPLOREDBY = se.EXPLOREDBY & mask;
      }

      this.markChanged();
    }
  }

  public void setScannedBy(int index, int race, boolean revealed) {
    checkIndex(index);

    SectorEntry se = ENTRY.get(index);
    long mask = 1L << race;

    if (((se.REVEALEDBY & mask) != 0) != revealed) {
      if (revealed) {
        se.REVEALEDBY = se.REVEALEDBY | mask;
      } else {
        mask = ~mask;
        se.REVEALEDBY = se.REVEALEDBY & mask;
      }

      this.markChanged();
    }
  }

  /**
   * Sets the owner claim of a sector.
   *
   * @param index     the index of the sector
   * @param raceMask  new owner claim mask
   */
  public void setOwnerClaim(int index, short raceMask) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);

    if (se.OWNER_CLAIM != raceMask) {
      se.OWNER_CLAIM = raceMask;
      this.markChanged();
    }
  }

  /**
   * Sets the starbase of a sector.
   *
   * @param index the index of the sector
   * @param rev   new starbase
   */
  public void setStarbase(int index, short rev) {
    checkIndex(index);

    SectorEntry se = ENTRY.get(index);
    if (se.STARBASE == rev) {
      return;
    }

    se.STARBASE = rev;
    this.markChanged();
  }

  public void removeAllStarbases() {
    for (SectorEntry se : ENTRY) {
      if (se.STARBASE != -1) {
        se.STARBASE = -1;
        this.markChanged();
      }
    }
  }

  // other

  /**
   * Switches two sectors.
   *
   * @param index the index of the first sector
   * @param rev   index of the second sector
   */
  public void switchSectors(int index, int rev) {
    checkIndex(index);
    checkIndex(rev);
    SectorEntry sea = ENTRY.get(index);
    SectorEntry se2 = ENTRY.get(rev);
    int a = sea.V_INDEX;
    int b = sea.H_INDEX;
    sea.V_INDEX = se2.V_INDEX;
    sea.H_INDEX = se2.H_INDEX;
    se2.V_INDEX = a;
    se2.H_INDEX = b;
    Collections.sort(ENTRY);
    this.markChanged();
  }

  /**
   * Adds a horizontal line.
   */
  public void addHorizontalLine() {
    ENTRY.ensureCapacity(ENTRY.size() + width);
    height++;
    // add new line to the end of the list
    for (int i = 0; i <= width; i++) {
      ENTRY.add(new SectorEntry(i, height));
    }
    // no sorting needed
    this.markChanged();
  }

  /**
   * Adds a vertical line.
   */
  public void addVerticalLine() {
    ENTRY.ensureCapacity(ENTRY.size() + height);
    width++;
    // add new line to the end of the list
    for (int i = 0; i <= height; i++) {
      ENTRY.add(new SectorEntry(width, i));
    }
    // afterwards sort new vertical row entries
    Collections.sort(ENTRY);
    this.markChanged();
  }

  public void setSystem(int index, short system) {
    checkIndex(index);
    SectorEntry se = ENTRY.get(index);
    if (se.SYSTEM != system) {
      se.SYSTEM = system;
      markChanged();
    }
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("Sector.6"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private void checkView(int view) throws InvalidArgumentsException {
    if (view < 0 || view > NUM_EMPIRES) {
      String msg = Language.getString("Sector.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(view)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }
  }

  // class #1
  private class SectorEntry implements Comparable<SectorEntry> {
    // data
    public int V_INDEX;                                 // 0x0  + 4  = 4
    public int H_INDEX;                                 // 0x4  + 4  = 8
    public short STARBASE = -1;                         // 0x8  + 2  = 10
    public short STELLAR = -1;                          // 0xA  + 2  = 12
    public short SYSTEM = -1;                           // 0xC  + 2  = 14
    // mask of the empire claims
    public short OWNER_CLAIM = 0;                       // 0xE  + 2  = 16
    // agreed sector ownership per empire + minor races
    private int[] agreedOwnership = new int[6];         // 0x10 + 24 = 40
    public long EXPLOREDBY = 0;                         // 0x28 + 8  = 48
    public long REVEALEDBY = 0;                         // 0x30 + 8  = 56

    // GWTForce / GTForceList
    // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56589#p56589
    // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=58110#p58110
    // Note, it's not sure whether both the task force lists might be swapped.
    private int GWTForceAddr = 0;                       // 0x38 + 4  = 60
    private int GTForceListAddr = 0;                    // 0x3C + 4  = 64

    public int[] SCAN_STRENGTH = new int[NUM_EMPIRES];  // 0x40 + 20 = 84
    private byte[] UNKNOWN_2 = new byte[20];            // 0x54 + 20 = 104

    // constructors
    public SectorEntry(InputStream in) throws IOException {
      // vertical index 0-4
      V_INDEX = StreamTools.readInt(in, true);
      // horizontal index 4-4
      H_INDEX = StreamTools.readInt(in, true);
      // starbase 8-2
      STARBASE = StreamTools.readShort(in, true);
      // stellar object 10-2
      STELLAR = StreamTools.readShort(in, true);
      // system 12-2
      SYSTEM = StreamTools.readShort(in, true);
      // claim 14-2
      OWNER_CLAIM = StreamTools.readShort(in, true);
      // agreed ownership per empire + minor races 16-24
      for (int i = 0; i < 6; i++) {
        agreedOwnership[i] = StreamTools.readInt(in, true);
      }
      // explored by 40-8
      EXPLOREDBY = StreamTools.readLong(in, true);
      // revealed by 48-8
      REVEALEDBY = StreamTools.readLong(in, true);
      // GWTForceAddr 56-4
      GWTForceAddr = StreamTools.readInt(in, true);
      // GTForceListAddr 60-4
      GTForceListAddr = StreamTools.readInt(in, true);
      // scan strength 64-20
      for (int i = 0; i < NUM_EMPIRES; i++) {
        SCAN_STRENGTH[i] = StreamTools.readInt(in, true);
      }
      // unknown 84-20
      StreamTools.read(in, UNKNOWN_2);
    }

    public SectorEntry(int h_index, int v_index) {
      V_INDEX = v_index;
      H_INDEX = h_index;

      for (int i = 0; i < 6; i++) {
        if (i < NUM_EMPIRES) {
          SCAN_STRENGTH[i] = 0;
        }
        agreedOwnership[i] = -1;
      }

      UNKNOWN_2 = new byte[20];
    }

    public String getDescription(boolean numeric) {
      String ret = Language.getString("Sector.5"); //$NON-NLS-1$

      numeric = numeric || getHSectors() > ('Z' - 65);
      String positionCode = numeric ? getNumericPosition() : getPositionCode();
      ret = ret.replace("%1", positionCode);
      return ret;
    }

    public String getNumericPosition() {
      return Integer.toString(H_INDEX + 1) + "." + Integer.toString(V_INDEX + 1);
    }

    public String getPositionCode() {
      try {
        val binaryString = new ArrayList<Byte>();
        val asciiLetterA = 65;
        int numberToConvert = H_INDEX;

        do {
          val remainder = numberToConvert % 26;
          binaryString.add(0, (byte) (remainder + asciiLetterA));

          numberToConvert = (numberToConvert - remainder) / 26;
        } while (numberToConvert > 0);

        return DataTools.fromNullTerminatedBotfString(Bytes.toArray(binaryString)) + (V_INDEX + 1);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      return "";
    }

    public String getHexPosition(String delimeter) {
      String ret = Integer.toHexString(V_INDEX).toUpperCase() + delimeter
          + Integer.toHexString(H_INDEX).toUpperCase();
      return ret;
    }

    public boolean hasStarBase() {
      return STARBASE != -1;
    }

    public boolean hasStellar() {
      return STELLAR != -1;
    }

    public boolean hasSystem() {
      return SYSTEM != -1;
    }

    public boolean isClaimed() {
      return OWNER_CLAIM != 0;
    }

    public boolean isClaimed(int empire) {
      // 01 = card, 02 = fed, 04 = ferg, 08 = kling, 10 = rom
      return 0 != (OWNER_CLAIM & (1 << empire));
    }

    public void removeSystem() {
      if (SYSTEM != -1) {
        SYSTEM = -1;
        markChanged();
      }
    }

    // determine race for uncontested sectors
    // @return -1 is contested
    public int getOwner() {
      switch (OWNER_CLAIM) {
        case 0:
          // return possible minor race claim
          return agreedOwnership[NUM_EMPIRES];
        case 1:
          // Cardassians
          return 0;
        case 2:
          // Federation
          return 1;
        case 4:
          // Ferengi
          return 2;
        case 8:
          // Klingons
          return 3;
        case 16:
          // Romulans
          return 4;
        default:
          // contested
          return -1;
      }
    }

    // save
    public void save(OutputStream out) throws IOException {
      // vertical index
      out.write(DataTools.toByte(V_INDEX, true));
      // horizontal index
      out.write(DataTools.toByte(H_INDEX, true));
      // starbase
      out.write(DataTools.toByte(STARBASE, true));
      // stellar
      out.write(DataTools.toByte(STELLAR, true));
      // system
      out.write(DataTools.toByte(SYSTEM, true));
      // owner
      out.write(DataTools.toByte(OWNER_CLAIM, true));
      // unknown
      for (int i = 0; i < 6; i++) {
        out.write(DataTools.toByte(agreedOwnership[i], true));
      }
      // explered
      out.write(DataTools.toByte(EXPLOREDBY, true));
      // revealed
      out.write(DataTools.toByte(REVEALEDBY, true));
      // GWTForceAddr
      out.write(DataTools.toByte(GWTForceAddr, true));
      // GTForceListAddr
      out.write(DataTools.toByte(GTForceListAddr, true));
      // scan strength
      for (int i = 0; i < NUM_EMPIRES; i++) {
        out.write(DataTools.toByte(SCAN_STRENGTH[i], true));
      }
      // unknown
      out.write(UNKNOWN_2);
    }

    // for comparing
    @Override
    public boolean equals(Object o) {
      if (!(o instanceof SectorEntry)) {
        return false;
      }

      SectorEntry se = (SectorEntry) o;

      return (se.V_INDEX == V_INDEX && se.H_INDEX == H_INDEX);
    }

    @Override
    public int hashCode() {
      return (V_INDEX * width + H_INDEX);
    }

    @Override
    public int compareTo(SectorEntry se) throws ClassCastException {
      return (hashCode() - se.hashCode());
    }
  }

  public boolean hasTaskForces(int index) {
    checkIndex(index);

    SectorEntry se = ENTRY.get(index);
    return gwtForce.hasSectorTaskForces(se.H_INDEX, se.V_INDEX);
  }

  public short[] getTaskForces(int index) {
    checkIndex(index);

    SectorEntry se = ENTRY.get(index);
    return gwtForce.getSectorTaskForces(se.H_INDEX, se.V_INDEX);
  }

}
