package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Optional;
import java.util.Vector;
import lombok.val;
import ue.common.CommonStrings;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.ShipTech;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavGame.CrewExperience;
import ue.edit.sav.files.map.data.Ship;
import ue.exception.KeyNotFoundException;
import ue.exception.SavException;
import ue.util.calc.Calc;
import ue.util.data.CollectionTools;

public class GShipList extends GList {

  public static final String FileName = CSavFiles.GShipList;
  public static final int ENTRY_SIZE = Ship.SIZE;

  public class ShipStats extends EmpireShipStats{
    // mapped by monster ship type
    public HashMap<Integer, Integer> monsterTypes = new HashMap<Integer, Integer>();
  }

  public class EmpireShipStats {
    public int shipsTotal = 0;
    public int shipsCloaked = 0;

    // mapped by ship category / ship role
    public HashMap<Short, Integer> shipCategories = new HashMap<Short, Integer>();
    // mapped by value
    public HashMap<Integer, Integer> range = new HashMap<Integer, Integer>();
    public HashMap<Integer, Integer> speed = new HashMap<Integer, Integer>();
    public HashMap<Integer, Integer> scanRange = new HashMap<Integer, Integer>();
    public HashMap<Integer, Integer> experienceLvl = new HashMap<Integer, Integer>();

    public EmpireShipStats() {
      // pre-fill known entries so they aren't missed
      shipCategories.put(ShipRole.Scout, 0);
      shipCategories.put(ShipRole.Destroyer, 0);
      shipCategories.put(ShipRole.Cruiser, 0);
      shipCategories.put(ShipRole.StrikeCruiser, 0);
      shipCategories.put(ShipRole.BattleShip, 0);
      shipCategories.put(ShipRole.ColonyShip, 0);
      shipCategories.put(ShipRole.TroopTransport, 0);
      experienceLvl.put(CrewExperience.Green, 0);
      experienceLvl.put(CrewExperience.Regular, 0);
      experienceLvl.put(CrewExperience.Veteran, 0);
      experienceLvl.put(CrewExperience.Elite, 0);
      experienceLvl.put(CrewExperience.Legendary, 0);
    }
  }

  private HashMap<Short, Ship> entries = new HashMap<Short, Ship>();
  private ShipCntr shipCntr;
  private ShipNameDat shipNameDat;
  private Optional<ShipList> shipModels = Optional.empty();
  private Optional<ShipTech> shipTech = Optional.empty();

  public GShipList(GListHead header, ShipCntr shipCntr, SavGame game, Stbof stbof)
      throws IOException {
    super(header, game, stbof, ENTRY_SIZE);

    this.shipCntr = shipCntr;
    this.shipNameDat = (ShipNameDat) game.getInternalFile(CSavFiles.ShipNameDat, true);

    if (stbof != null) {
      val stif = stbof.files();
      this.shipModels = stif.findShipList();
      this.shipTech = stif.findShipTech();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    super.load(in);
    entries.clear();

    for (int i = 0; i < usedSlots; i++) {
      Ship sh = new Ship(this);
      sh.load(in);
      entries.put(sh.shipID(), sh);
    }

    markSaved();
  }

  private void updateHeader() {
    header.setOccupiedSlotNum(entries.size());
  }

  @Override
  public void save(OutputStream out) throws IOException {
    // check available slot number
    int tfCount = entries.size();
    int slots_num = header.getTotalSlotsNum();
    if (tfCount > slots_num) {
      // can't fix the slot number here as it might already have been written
      throw new SavException(getName() + ": Insufficient slot number!");
    }

    Short[] IDs = entries.keySet().toArray(new Short[0]);
    Arrays.sort(IDs);

    for (short id : IDs) {
      Ship sh = entries.get(id);
      sh.save(out);
    }

    // zero unused ship slots
    if (tfCount < slots_num) {
      byte[] padding = new byte[116];
      for (; tfCount < slots_num; ++tfCount)
        out.write(padding);
    }
  }

  @Override
  public void clear() {
    super.clear();
    entries.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    super.check(response);
    checkInvalidShips(response);
  }

  private boolean checkInvalidShips(Vector<String> response) {
    // remove invalid ships
    boolean any = entries.values().removeIf(shp -> {
      return shp.check(sav, response);
    });

    if (any) {
      updateHeader();
      markChanged();
    }
    return any;
  }

  @Override
  public int getSize() {
    return header.getTotalSlotsNum() * header.getSlotSize();
  }

  public ShipStats getStats() {
    ShipStats stats = new ShipStats();
    stats.shipsTotal = entries.size();

    for (Ship ship : entries.values())
      incrementShipStats(stats, ship);

    return stats;
  }

  public EmpireShipStats getEmpireStats(int empire) {
    EmpireShipStats stats = new EmpireShipStats();

    for (Ship ship : entries.values()) {
      int raceIdx = Integer.min(ship.getRaceID(), 5);
      if (raceIdx == empire)
        incrementShipStats(stats, ship);
    }

    return stats;
  }

  public EmpireShipStats[] getEmpireStats() {
    EmpireShipStats[] stats = new EmpireShipStats[6];

    for (int empire = 0; empire < stats.length; ++empire)
      stats[empire] = getEmpireStats(empire);

    return stats;
  }

  private void incrementShipStats(EmpireShipStats stats, Ship ship) {
    int shipType = ship.getShipType();

    stats.experienceLvl.merge(ship.getCrewExpLevel(), 1, Calc::sum);
    if (ship.getCloak() != 0)
      stats.shipsCloaked++;

    if (shipModels.isPresent()) {
      ShipList shpLst = shipModels.get();
      ShipDefinition shipDef = shpLst.getShipDefinition(shipType);

      short role =  shipDef.getShipRole();
      stats.shipCategories.merge(role, 1, Calc::sum);

      if (role == ShipRole.Monster && stats instanceof ShipStats)
        ((ShipStats) stats).monsterTypes.merge(shipType, 1, Calc::sum);

      int range =  (int) shipDef.getMapRange();
      stats.range.merge(range, 1, Calc::sum);
      int speed =  (int) shipDef.getMapSpeed();
      stats.speed.merge(speed, 1, Calc::sum);
      int scanRange =  shipDef.getScanRange();
      stats.scanRange.merge(scanRange, 1, Calc::sum);
    }
  }

  @Override
  public int getEntryCount() {
    return entries.size();
  }

  public short[] getShipIds() {
    return CollectionTools.toShortArray(entries.keySet());
  }

  public boolean hasShip(short shipId) {
    return entries.containsKey(shipId);
  }

  public short getShipTaskForceId(short shipId) {
    return Optional.ofNullable(entries.get(shipId)).map(Ship::getTaskForceId).orElse((short) -1);
  }

  public short getShipRole(short index) throws KeyNotFoundException {
    Ship sh = getShip(index);
    ShipDefinition shipDef = shipModels.isPresent() ? shipModels.get().getShipDefinition(sh.getShipType()) : null;
    return shipDef != null ? shipDef.getShipRole() : ShipRole.None;
  }

  public Ship tryGetShip(short index) {
    return entries.get(index);
  }

  public Ship getShip(short index) throws KeyNotFoundException {
    Ship sh = entries.get(index);
    if (sh == null) {
      throw new KeyNotFoundException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(index)));
    }
    return sh;
  }

  public int getMaxShipId() {
    int max = 0;
    for (Ship shp : entries.values()) {
      max = Integer.max(max, shp.shipID());
    }

    return max;
  }

  public short addShip(short taskForceId, short shipType, short raceId) throws IOException {
    short shipId = (short) shipCntr.getShipCounter();
    addShip(shipId, taskForceId, shipType, raceId);

    // update ship id counter
    shipCntr.setShipCounter(shipId + 1);
    return shipId;
  }

  // For adding ships, stbof must be loaded!
  private void addShip(short shipId, short taskForceId, short shipType, short raceId) throws IOException {
    String name = shipNameDat.nextNameByType(shipType);
    Ship ship = new Ship(this, shipModels.get(), shipTech.get(), shipId, shipType, taskForceId, raceId, name);

    entries.put(shipId, ship);
    updateHeader();
    markChanged();
  }

  public void removeShip(short shipId) {
    if (entries.remove(shipId) != null) {
      updateHeader();
      markChanged();
    }
  }

  public void removeAllShips() {
    if (!entries.isEmpty()) {
      entries.clear();
      updateHeader();
      markChanged();
    }
  }
}
