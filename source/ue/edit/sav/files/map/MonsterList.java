package ue.edit.sav.files.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeSet;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class MonsterList extends InternalFile {

  public static final String FileName = CSavFiles.MonsterLst;

  private SavGame game;
  // private int numTaskForces; // 4 auto-computed

  // sort task force ids to simplify hex lookup
  private TreeSet<Short> m_taskForceIds = new TreeSet<Short>(); // numTaskForces * 2 + 4

  public MonsterList(SavGame game) {
    this.game = game;
  }

  public short[] getTaskForceIds() {
    int size = m_taskForceIds.size();
    short[] ids = new short[size];

    int i = 0;
    for (short id : m_taskForceIds) {
      ids[i++] = id;
    }
    return ids;
  }

  private boolean differs(short[] taskForceIds) {
    if (taskForceIds.length != m_taskForceIds.size()) {
      return true;
    }

    for (short shipId : taskForceIds) {
      if (!m_taskForceIds.contains(shipId)) {
        return true;
      }
    }

    return false;
  }

  public void setTaskForceIds(short[] taskForceIds) {
    // check if changed
    if (!differs(taskForceIds)) {
      return;
    }

    // update the task force ids
    m_taskForceIds.clear();
    for (short id : taskForceIds) {
      m_taskForceIds.add(id);
    }

    markChanged();
  }

  public void addMonster(short taskForceId) {
    m_taskForceIds.add(taskForceId);
    markChanged();
  }

  public boolean removeMonster(short taskForceId) {
    if (m_taskForceIds.remove(taskForceId)) {
      markChanged();
      return true;
    }
    return false;
  }

  public void removeAllMonsters() {
    if (!m_taskForceIds.isEmpty()) {
      m_taskForceIds.clear();
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    m_taskForceIds.clear();
    int numTaskForces = StreamTools.readInt(in, true);

    int size = in.available();
    if (size != numTaskForces << 1)
      System.out.println("MonsterList: Task force number doesn't match the entry count!");

    numTaskForces = Integer.min(size >> 1, numTaskForces);
    for (int i = 0; i < numTaskForces; ++i) {
      m_taskForceIds.add(StreamTools.readShort(in, true));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(m_taskForceIds.size(), true));
    for (short shpId : m_taskForceIds) {
      out.write(DataTools.toByte(shpId, true));
    }
  }

  @Override
  public void clear() {
    m_taskForceIds.clear();
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // monsters always must be listed by both the lists, given they can't be player re-grouped
    TaskForceList gtfList = (TaskForceList) game.tryGetInternalFile(CSavFiles.GTForceList, true);
    if (gtfList == null) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR,
          "Missing " + CSavFiles.GTForceList + "!"));
      return;
    }
    TaskForceList gwtfList = (TaskForceList) game.tryGetInternalFile(CSavFiles.GWTForce, true);
    if (gwtfList == null) {
      response.add(
          getCheckIntegrityString(INTEGRITY_CHECK_ERROR, "Missing " + CSavFiles.GWTForce + "!"));
      return;
    }

    boolean any = m_taskForceIds.removeIf(tfId -> {
      boolean hasGtf = gtfList.hasTaskForce(tfId);
      boolean hasGwtf = gwtfList.hasTaskForce(tfId);
      if (!hasGtf || !hasGwtf) {
        String msg = "Found task force id %1, that is missing from %2.";
        String missing = !hasGtf ? !hasGwtf ?
            CSavFiles.GTForceList + " and " + CSavFiles.GWTForce
            : CSavFiles.GTForceList + " but not " + CSavFiles.GWTForce
            : CSavFiles.GWTForce + " but not " + CSavFiles.GTForceList;
        msg = msg.replace("%1", Integer.toString(tfId));
        msg = msg.replace("%2", missing);

        if (!hasGtf && !hasGwtf) {
          msg += " (removed)";
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
          return true;
        } else {
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      }
      return false;
    });

    if (any) {
      markChanged();
    }
  }

  @Override
  public int getSize() {
    return m_taskForceIds.size() * 2 + 4;
  }
}
