package ue.edit.sav.task;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.edit.sav.tools.SavTools;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.LzssEntry;
import ue.util.file.LzssFile;

public class SaveTask extends UEWorker<Boolean> {

  private static final String TmpFileName = "sav.tmp"; //$NON-NLS-1$

  private final File sourceFile;
  private final File destFile;
  private final boolean backup;
  private final String ext;

  private Map<String, ? extends InternalFile> filesChanged;
  private Set<String> removed;
  private File outFile;

  public SaveTask(SavGame sav, File destination, boolean backup, String extension) {
    this.backup = backup;
    this.ext = extension;
    sourceFile = sav.getSourceFile();
    outFile = destFile = destination;

    // collect work for async processing
    // this way the iteration doesn't become undefined when additional files are loaded during save
    filesChanged = sav.listChangedFiles().stream().collect(Collectors.toMap(
      x -> x.getName().toLowerCase(), Function.identity(), (x,y) -> y, HashMap::new));
    removed = sav.removedFiles();
    RESULT = false;
  }

  @Override
  protected Boolean work() throws IOException {
    // create temporary file and backup if destination already exists
    checkDestination();

    if (isCancelled()) {
      // delete temp file created by getWritableLocalFile
      outFile.delete();
      return false;
    }

    try (LzssFile source = new LzssFile(sourceFile, null)) {
      // remove current destination file
      outFile.delete();

      // create new SavFile
      try (LzssFile dest = new LzssFile(outFile, source.getName())) {
        // write all the sav entry files
        writeSavFiles(source, dest);
      }
    } catch (Exception e) {
      outFile.delete();
      throw e;
    }

    // last chance to cancel action
    if (isCancelled()) {
      outFile.delete();
      return false;
    }

    // replace destination by temporary file
    applyTempFile();

    // on success mark all files saved
    for (InternalFile file : filesChanged.values())
      file.markSaved();

    return true;
  }

  private void checkDestination() throws IOException {
    if (!destFile.exists())
      return;

    // create backup
    if (backup) {
      sendMessage(Language.getString("SaveTask.0")); //$NON-NLS-1$

      File file = new File(destFile.getPath() + ext);
      int i = 1;

      while (file.exists()) {
        file = new File(destFile.getPath() + i + ext);
        i++;
      }

      Files.copy(destFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    String msg = Language.getString("SaveTask.4"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", TmpFileName)); //$NON-NLS-1$

    outFile = FileManager.getWritableLocalFile(TmpFileName); //$NON-NLS-1$
  }

  private boolean writeSavFiles(LzssFile source, LzssFile dest) throws IOException {
    // the raw output stream must not be closed here
    // it later is closed by SavFile.close
    OutputStream out = dest.getRawOutputStream();

    // copy full entry list from previous save game
    ArrayList<LzssEntry> entryList = new ArrayList<>(source.listFileEntries());

    // detect loaded files to skip
    // for name lookup change to lower case
    HashSet<String> fileUpdates = entryList.stream().map(x -> x.getLcName())
      .filter(filesChanged::containsKey).collect(Collectors.toCollection(HashSet::new));

    // add new files
    int fileCount = entryList.size() + filesChanged.size() - fileUpdates.size();
    entryList.ensureCapacity(fileCount);
    for (InternalFile stfil : filesChanged.values()) {
      String fileName = stfil.getName();

      // for name lookup change to lower case
      if (fileUpdates.contains(fileName.toLowerCase()))
        continue;

      LzssEntry ze = new LzssEntry(fileName);
      entryList.add(ze);
    }

    // match trek.exe file order
    Collections.sort(entryList, (x, y) -> SavTools.compareSavName(x.getName(), y.getName()));

    // process all file entries
    for (LzssEntry ze : entryList) {
      if (isCancelled())
        break;

      // skip file if removed
      // for name lookup change to lower case
      String fileName = ze.getLcName();
      if (removed.contains(fileName))
        continue;

      InternalFile stfil = filesChanged.get(fileName);
      if (stfil != null) {
        // write internal files
        String msg = Language.getString("SaveTask.2"); //$NON-NLS-1$
        msg = msg.replace("%1", fileName); //$NON-NLS-1$
        feedMessage(msg);

        // create new SavEntry
        ze.setCompressedSize(-1);
        ze.setUncompressedSize(stfil.getSize());

        // copy unknown hash value
        val old = source.findEntry(stfil.getName());
        ze.setUnknown(old.isPresent() ? old.get().getUnknown() : 0);

        // add next entry
        dest.putNextEntry(ze);

        // write new file
        try (OutputStream zos = dest.getOutputStream(ze, SavTools.isChunked(fileName), SavTools.isBinary(fileName))) {
          stfil.save(zos);
        }

        // complete the entry
        dest.closeEntry();
      } else {
        // add next entry
        dest.putNextEntry(ze);

        // copy unloaded file
        copyFile(out, source, ze);

        // complete the entry
        dest.closeEntry();
      }
    }

    return true;
  }

  private void copyFile(OutputStream out, LzssFile source, LzssEntry ze) throws IOException {
    String msg = Language.getString("SaveTask.1"); //$NON-NLS-1$
    msg = msg.replace("%1", ze.getName()); //$NON-NLS-1$
    feedMessage(msg);

    try (InputStream is = source.getRawInputStream(ze)) {
      byte[] buf = new byte[1024];
      while (is.available() > 0) {
        int len = is.read(buf);
        out.write(buf, 0, len);
      }
    }
  }

  private void applyTempFile() throws IOException {
    if (outFile == destFile)
      return;

    String msg = Language.getString("SaveTask.1"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

    // replace with TMP file
    // on error keep temp file for manual rename
    Files.copy(outFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

    msg = Language.getString("SaveTask.5") //$NON-NLS-1$
      .replace("%1", outFile.getName()); //$NON-NLS-1$
    sendMessage(msg);

    // attempt to delete the temporary file
    boolean deleted = outFile.delete();
    if (!deleted) {
      // the save operation succeeded, just the temp file couldn't be removed
      msg = Language.getString("SaveTask.3"); //$NON-NLS-1$
      logError(msg.replace("%1", outFile.getName())); //$NON-NLS-1$
    }
  }

}
