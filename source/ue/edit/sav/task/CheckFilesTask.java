package ue.edit.sav.task;

import java.util.Collection;
import java.util.Optional;
import java.util.TreeSet;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.sav.SavGame;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.LzssFile;

public class CheckFilesTask extends UEWorker<Vector<String>> {

  private SavGame savGame;
  private TreeSet<String> files;

  public CheckFilesTask(SavGame savGame, Collection<String> files) {
    this.savGame = savGame;
    this.files = new TreeSet<>(files);
    RESULT = new Vector<String>();
  }

  @Override
  public Vector<String> work() {
    // check all of the file as whole
    try (LzssFile su = new LzssFile(savGame.getSourceFile(), null)) {
      Vector<String> temp = su.check();
      if (!temp.isEmpty())
        RESULT.addAll(temp);
    } catch (Exception ex) {
      // append error and proceed with internal file checks
      RESULT.add(getGenericErrorString(ex.getMessage()));
    }

    // pre-load files
    savGame.tryGetInternalFiles(files, true, Optional.of(x -> {
      feedMessage("Loading " + x + "...");
      return !isCancelled();
    }));

    // report load errors
    for (val err : savGame.listLoadErrors().entrySet()) {
      String msg = Language.getString("CheckFilesTask.1"); //$NON-NLS-1$
      msg = msg.replace("%1", err.getKey()); //$NON-NLS-1$
      msg = msg.replace("%2", err.getValue()); //$NON-NLS-1$
      RESULT.add(msg);

      // skip to check load errors twice
      files.remove(err.getKey());
    }

    // check internal files
    for (String file : files) {
      if (isCancelled())
        break;

      try {
        InternalFile test = savGame.getInternalFile(file, true);
        feedMessage("Performing self-check on " + test.getName() + "...");

        Vector<String> temp = test.check();
        if (!temp.isEmpty())
          RESULT.addAll(temp);
      } catch (Exception ex) {
        // append error and proceed to check next file
        RESULT.add(getGenericErrorString(ex.getMessage()));
      }
    }

    return RESULT;
  }

  private String getGenericErrorString(String error) {
    String msg = Language.getString("CheckFilesTask.0"); //$NON-NLS-1$
    msg = msg.replace("%1", savGame.getName()); //$NON-NLS-1$
    msg = msg.replace("%2", error); //$NON-NLS-1$
    return msg;
  }
}
