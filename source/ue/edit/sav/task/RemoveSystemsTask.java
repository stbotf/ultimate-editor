package ue.edit.sav.task;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.edit.sav.tools.SavFileFilters;
import ue.edit.sav.tools.SystemTools;
import ue.exception.KeyNotFoundException;
import ue.service.UEWorker;

public class RemoveSystemsTask extends UEWorker<Void> {

  private SavGame sav;
  private SavGameInterface sgif;
  private List<Short> systemIds;

  public RemoveSystemsTask(SavGame savGame, Collection<Short> systemIds) {
    this.sav = savGame;
    sgif = sav.files();

    // sort system ids in reverse order, to not override any id changes
    this.systemIds = systemIds.stream()
      .sorted(Collections.reverseOrder())
      .collect(Collectors.toList());
  }

  @Override
  public Void work() throws KeyNotFoundException, IOException {
    updateStatus(UEWorker.STATUS_WORKING);
    SystemTools systemTools = sgif.systemTools();
    SystInfo systInfo = sgif.systInfo();

    // pre-load files
    preloadFiles();

    // remove systems
    boolean first = true;
    for (short systemId : systemIds) {
      if (isCancelled())
        break;

      SystemEntry system = systInfo.getSystem(systemId);
      String msg = "Removing " + system.descriptor() + "...";
      if (first) {
        sendMessage(msg);
        first = false;
      }
      else {
        feedMessage(msg);
      }

      systemTools.removeSystem(systemId);
    }

    updateStatus(isCancelled() ? UEWorker.STATUS_CANCEL : UEWorker.STATUS_DONE);
    return RESULT;
  }

  private void preloadFiles() throws IOException {
    val aiAgents = sav.getFileNames(SavFileFilters.AIAgent);
    sav.getInternalFiles(aiAgents, true, Optional.of(x -> {
      feedMessage("Loading " + x + "...");
      return !isCancelled();
    }));
    feedMessage("Loading AI agent manager...");
    sgif.aiAgentMgr();

    val aiTasks = sav.getFileNames(SavFileFilters.AITask);
    sav.getInternalFiles(aiTasks, true, Optional.of(x -> {
      feedMessage("Loading " + x + "...");
      return !isCancelled();
    }));
    feedMessage("Loading AI task manager...");
    sgif.aiTaskMgr();
    if (isCancelled())
      return;

    val agtFiles = sav.getFileNames(SavFileFilters.AgtDp);
    agtFiles.addAll(sav.getFileNames(SavFileFilters.AgtSh));
    agtFiles.addAll(sav.getFileNames(SavFileFilters.AgtSy));
    agtFiles.addAll(sav.getFileNames(SavFileFilters.AgtTk));
    agtFiles.addAll(sav.getFileNames(SavFileFilters.TskSh));
    agtFiles.addAll(sav.getFileNames(SavFileFilters.TskSy));
    sav.getInternalFiles(agtFiles, true, Optional.of(x -> {
      feedMessage("Loading " + x + "...");
      return !isCancelled();
    }));
    if (isCancelled())
      return;

    feedMessage("Loading race files...");
    sgif.alienInfo();
    sgif.gameInfo();
    sgif.rToS();
    sgif.techInfo();
    sgif.tradeInfos();
    sgif.treaty();
    val alienFiles = sav.getFileNames(SavFileFilters.AlienTF);
    sav.getInternalFiles(alienFiles, true, Optional.of(x -> {
      feedMessage("Loading " + x + "...");
      return !isCancelled();
    }));
    if (isCancelled())
      return;

    feedMessage("Loading system files...");
    sgif.aiSysUnt();
    sgif.strcInfo();
    sgif.systInfo();
    sgif.provInfos();
    if (isCancelled())
      return;

    feedMessage("Loading order & event files...");
    sgif.gtfList();
    sgif.gwtForce();
    sgif.monsterList();
    sgif.ordInfo();
    sgif.resultList();
    if (isCancelled())
      return;
  }

}
