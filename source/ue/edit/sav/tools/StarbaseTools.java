package ue.edit.sav.tools;

import java.io.IOException;
import javax.swing.JOptionPane;
import ue.UE;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.StarBaseInfo;
import ue.exception.KeyNotFoundException;

/**
 * Contains methods for starbase & outpost edit operations in a saved game.
 */
public class StarbaseTools {

  SavGame game;
  SavGameInterface sgif;

  public StarbaseTools(SavGame game) {
    this.game = game;
    sgif = game.files();
  }

  public void addStarbase(ShipList shipList, int sectorIndex, short shipID, boolean noWarning) throws KeyNotFoundException, IOException {
    Sector sector = sgif.sectorLst();
    short starbaseId = sector.getStarbase(sectorIndex);
    if (starbaseId >= 0 && !noWarning) {
      int response = JOptionPane.showConfirmDialog(UE.WINDOW,
        "Sector %1 already contains a star base. Replace it?", UE.APP_NAME,
        JOptionPane.OK_CANCEL_OPTION);

      if (response == JOptionPane.CANCEL_OPTION)
        return;

      // remove
      removeStarbase(shipList, sectorIndex);
    }

    // add
    StarBaseInfo sbInfo = sgif.sbInfo();
    starbaseId = (short) sectorIndex;
    ShipDefinition shipDef = shipList.getShipDefinition(shipID);

    int vert = sector.getVertical(sectorIndex);
    int horz = sector.getHorizontal(sectorIndex);
    sbInfo.add(starbaseId, shipDef.getShipRole(), vert, horz);
    sbInfo.setHull(starbaseId, shipDef.getHullStrength());
    sbInfo.setScanRange(starbaseId, shipDef.getScanRange());
    sbInfo.setOwner(starbaseId, shipDef.getRace());
    sbInfo.setName(starbaseId, shipDef.getName());
    sbInfo.setShipID(starbaseId, shipID);

    sector.setStarbase(sectorIndex, starbaseId);
    updateStarbaseCount();
  }

  public void removeStarbase(ShipList ships, int sectorIndex) throws IOException {
    Sector sector = sgif.sectorLst();
    short sbId = sector.getStarbase(sectorIndex);
    if (sbId >= 0) {
      StarBaseInfo sbInfo = sgif.sbInfo();
      sbInfo.remove(sbId);
      updateStarbaseCount();
      sector.setStarbase(sectorIndex, (short) -1);
    }
  }

  public void removeAllStarbases() throws IOException {
    sgif.sbInfo().removeAll();
    sgif.galInfo().setBaseCount((short) 0);
    sgif.sectorLst().removeAllStarbases();
  }

  /**
   * Moves the star base to given location
   *
   * @param starbaseId  id of the star base
   * @param x           sector column
   * @param y           sector row
   * @param swap        whether sectors are swapped
   * @throws KeyNotFoundException
   * @throws IOException
   */
  public void moveStarbase(short starbaseId, int x, int y, boolean swap) throws KeyNotFoundException, IOException {
    Sector sector = sgif.sectorLst();

    // compute destination sector index,
    // which should but doesn't necessarily match the destination starbaseId
    int destIndex = sector.getSectorIndex(y, x);

    // check on already existent star base
    short destId = sector.getStarbase(destIndex);
    if (!swap) {
      if (destId >= 0) {
        String msg = "%1 already contains a star base. Switch locations?";
        msg = msg.replace("%1", sector.getDescription(x, false));

        int response = JOptionPane
            .showConfirmDialog(UE.WINDOW, msg, UE.APP_NAME, JOptionPane.OK_CANCEL_OPTION);
        if (response == JOptionPane.CANCEL_OPTION) {
          return;
        }
      }
    }

    // get current star base position
    StarBaseInfo sbInfo = sgif.sbInfo();
    int[] pos = sbInfo.getPosition(starbaseId);
    int sourceIndex = pos[1] * sector.getHSectors() + pos[0];

    swapStarbasePositions(sourceIndex, destIndex, false);
  }

  public void swapStarbasePositions(int sourceIndex, int destIndex, boolean swappedSectors)
    throws KeyNotFoundException, IOException {

    Sector sectors = sgif.sectorLst();
    StarBaseInfo sbInfo = sgif.sbInfo();
    short sourceId = sectors.getStarbase(swappedSectors ? destIndex : sourceIndex);
    short destId = sectors.getStarbase(swappedSectors ? sourceIndex : destIndex);

    if (sourceId >= 0 && destId >= 0) {
      // simply exchange sector location and id
      // no matter whether the previous ids match with the sector coordinates
      // enforcing a computed id might raise further conflicts
      // this way there is also no need to update sector.lst
      sbInfo.switchPositionAndId(sourceId, destId);

      // update swapped sectors for the swapped ids
      // if sectors aren't swapped, the ids are already correct
      if (swappedSectors) {
        sectors.setStarbase(sourceIndex, destId);
        sectors.setStarbase(destIndex, sourceId);
      }
    } else if (sourceId >= 0) {
      // update source star base
      int[] pos = sectors.getPosition(destIndex);
      sourceId = sbInfo.move(sourceId, (short) destIndex, pos[1], pos[0]);

      // update the sector references
      sectors.setStarbase(destIndex, sourceId);
      sectors.setStarbase(sourceIndex, (short) -1);
    } else if (destId >= 0) {
      // update target star base
      int[] pos = sectors.getPosition(sourceIndex);
      destId = sbInfo.move(destId, (short) sourceIndex, pos[1], pos[0]);

      // update the sector references
      sectors.setStarbase(sourceIndex, destId);
      sectors.setStarbase(destIndex, (short) -1);
    }
  }

  private void updateStarbaseCount() throws IOException {
    sgif.galInfo().setBaseCount((short) sgif.sbInfo().getNumberOfEntries());
  }

}
