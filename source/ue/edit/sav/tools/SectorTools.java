package ue.edit.sav.tools;

import java.io.IOException;
import lombok.val;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.OrderInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.data.TaskForce;
import ue.edit.sav.files.map.orders.Order;
import ue.exception.KeyNotFoundException;

/**
 * Contains methods for general sector edit operations in a saved game.
 */
public class SectorTools {

  SavGame game;
  SavGameInterface sgif;
  StbofFileInterface stif;

  public SectorTools(SavGame game, Stbof stbof) {
    this.game = game;
    sgif = game.files();
    stif = stbof.files();
  }

  public void setSectorExploration(int sectorIndex, long explored) throws IOException {
    Sector sector = sgif.sectorLst();
    sector.setExplored(sectorIndex, explored);

    short stellId = sector.getStellarObject(sectorIndex);
    if (stellId >= 0)
      sgif.stellInfo().getStellarObject(stellId).setVisitorsMask((int) (explored & 31));

    short sysId = sector.getSystem(sectorIndex);
    if (sysId >= 0)
      sgif.systInfo().getSystem(sysId).setExploredByMask((byte) (explored & 31));
  }

  public void clearAllSectorOrders(int sectorIndex) throws IOException {
    OrderInfo ordInfo = sgif.ordInfo();

    // remove all target sector orders,
    // including economic and military orders
    val orders = ordInfo.listSectorOrders(sectorIndex);
    for (Order order : orders) {
      ordInfo.removeOrder(order.orderId());

      val mil = order.getMilitaryOrder();
      if (mil != null) {
        // reset assigned task force mission
        short taskForceId = mil.getTaskForceId();
        if (taskForceId != -1) {
          TaskForce tf = game.files().gwtForce().getTaskForce(taskForceId);
          tf.resetMission();
        }
      }
    }
  }

  public void clearAllSectorResults(int sectorIndex) throws IOException {
    // remove all sector related event results,
    // including economic and military results
    sgif.resultList().removeSectorResults(sectorIndex);
  }

  public void clearFullSectorAsync(int sectorIndex) throws KeyNotFoundException, IOException {
    if (!sgif.systemTools().removeFromSectorAsync(sectorIndex)) {
      // stop if cancelled
      return;
    }

    sgif.stellarTools().removeFromSector(sectorIndex);
    sgif.starbaseTools().removeStarbase(stif.shipList(), sectorIndex);
    sgif.taskForceTools().removeAllSectorTaskForces(sectorIndex);
  }

}
