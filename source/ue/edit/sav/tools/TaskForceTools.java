package ue.edit.sav.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavGame.ControlFlags;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.files.GameInfo;
import ue.edit.sav.files.ai.AIAgent;
import ue.edit.sav.files.ai.AIShpUnt;
import ue.edit.sav.files.ai.AgtSh;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.AlienTFInfo;
import ue.edit.sav.files.emp.ResultList;
import ue.edit.sav.files.map.GShipList;
import ue.edit.sav.files.map.OrderInfo;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.TaskForceList;
import ue.edit.sav.files.map.TskSh;
import ue.edit.sav.files.map.data.Ship;
import ue.edit.sav.files.map.data.TaskForce;
import ue.exception.KeyNotFoundException;
import ue.util.data.CollectionTools;

/**
 * Contains methods for task force edit operations in a saved game.
 */
public class TaskForceTools {

  SavGame game;
  SavGameInterface sgif;
  StbofFileInterface stif;

  public TaskForceTools(SavGame game, Stbof stbof) {
    this.game = game;
    sgif = game.files();
    stif = stbof.files();
  }

  /**
   * Moves GWTForce task force and the corresponding GTForceList ships.
   * @see collectGTForceShips
   *
   * @param taskForceId the GWTForce task force id
   * @param x the target sector column
   * @param y the target sector row
   * @param keepMission whether not to reset local sector missions, which must be false unless the whole sector is swapped
   * @throws KeyNotFoundException thrown when a ship id is missing from GShipList and the ship abilities can't be determined
   * @throws IOException
   */
  public void moveTaskForce(short taskForceId, int x, int y, boolean keepMission) throws KeyNotFoundException, IOException {
    TaskForce gwtf = sgif.gwtForce().taskForce(taskForceId);

    // Fix temporary task force id for AI task forces,
    // to not become empty next turn and crash the game.
    fixTempGWTForceId(gwtf);

    // If re-grouped, changes are only stored to GWTForce.
    // The ship positions however must be kept in sync, therefore
    // search & collect the corresponding GTForceList ships.
    TaskForce gtf = collectGTForceShips(gwtf);

    // set task force location
    gwtf.setLocation(y, x);
    gtf.setLocation(y, x);

    // reset local sector mission unless the whole sector is swapped
    if (!keepMission) {
      gwtf.resetLocalMission();
      gtf.resetLocalMission();
    }
  }

  /**
   * Fixes temporary task force ids to keep GWTForce & GTForceList in sync.
   *
   * This is specially relevant when collecting task forces,
   * since temporary ids should never be stored to GTForceList!
   *
   * When storing a different id, however some additional order for the removed
   * old task force needs to be added or there is some other hint I missed.
   * Without they remain empty and later crash the game.
   *
   * @param gwtf the GWTForce task force to fix the temporary id
   * @return whether id had a negative temporary id and got fixed
   * @throws KeyNotFoundException thrown when the task force was missing from GWTForce
   * @throws IOException
   */
  private boolean fixTempGWTForceId(TaskForce gwtf) throws KeyNotFoundException, IOException {
    short taskForceId = gwtf.id();
    if (taskForceId < 0) {
      TaskForce gtf = sgif.gtfList().getTaskForce(taskForceId);
      taskForceId = nextTFId();

      changeTaskForceId(gwtf, taskForceId);
      if (gtf != null)
        changeTaskForceId(gtf, taskForceId);

      return true;
    }
    return false;
  }

  public void changeTaskForceId(TaskForce tf, short taskForceId) throws KeyNotFoundException, IOException {
    // TaskForce always is constructed for either GWTForce or GTForceList
    // and keeps an internal pointer. Therefore never mess the lists.
    TaskForceList tfList = tf.getTfList();
    short formerTFId = tf.id();

    // update taskforce list, ships and explicit order refferings
    tfList.changeTaskForceId(tf.id(), taskForceId);

    if (tfList.isGWTForce()) {
      // reset remaining former task force orders
      // that havn't been updated by GWTForce changeTaskForceId
      sgif.ordInfo().removeTaskForceOrder(formerTFId);
    } else {
      // update alienTFInfo
      int raceId = tf.getOwnerId();
      removeGTAlien(formerTFId, raceId);
      addGTAlien(taskForceId, raceId);

      // update result.lst
      sgif.resultList().changeTaskForceId(formerTFId, taskForceId);
    }
  }

  /**
   * Collect GTForceList sector task force ships.
   *
   * Ideally it returns the same task force. If re-grouped, it however
   * collects all related ships and creates a new task force if needed.
   *
   * This is used to keep both lists in sync.
   * Which is required, because task forces that are re-grouped for next turn,
   * are only stored to GWTForce. The game however searches matching
   * former GTForceList ship task forces by sector, and crashes with
   * "File: gdllist.c, Line: 534, gdlList != NULL", when there is
   * not a single task force of same race & sector.
   *
   * @param gwtf the GWTForce task force
   * @throws KeyNotFoundException Thrown when a ship id is missing from GShipList,
   *                              and the ship abilities can't be determined.
   * @throws IOException
   */
  private TaskForce collectGTForceShips(TaskForce gwtf) throws KeyNotFoundException, IOException {
    TaskForceList gTForceList = sgif.gtfList();
    short taskForceId = gwtf.id();
    short[] gwtShipIds = gwtf.getShipIDs();
    int[] loc = gwtf.getLocation();

    Set<Short> shipIds = CollectionTools.toSet(gwtShipIds);

    // lookup GTForceList for when the task force already existed
    TaskForce gtf = gTForceList.getTaskForce(taskForceId);

    if (gtf != null) {
      // split deviating GTForceList task force ships
      splitDeviatingTFShips(gtf, shipIds);
      if (shipIds.isEmpty())
        return gtf;
    } else {
      // create empty new task force for collecting the ships
      gtf = gTForceList.addTaskForce(taskForceId, gwtf.getOwnerId(), gwtf.getSectorColumn(), gwtf.getSectorRow());
      addGTAlien(taskForceId, gwtf.getOwnerId());
    }

    // when there are remaining ships to collect, search all sector task forces
    short[] gtfIds = gTForceList.getSectorTaskForces(loc[0], loc[1]);
    ArrayList<Short> missingSectorTFs = new ArrayList<Short>();
    ArrayList<Short> removed = new ArrayList<Short>();

    for (short gtfId : gtfIds) {
      // skip already checked GWTForce task force
      if (gtfId == taskForceId)
        continue;

      TaskForce tf = gTForceList.getTaskForce(gtfId);
      if (tf == null) {
        System.out.println("Error: Task force " + Short.toString(gtfId) + " not found.");
        continue;
      }

      // remove collected sector task force ships
      val shpIt = shipIds.iterator();
      while (shpIt.hasNext()) {
        short shipId = shpIt.next();
        if (removeFromTaskForce(tf, shipId)) {
          removed.add(shipId);
          shpIt.remove();
        }
      }

      if (shipIds.isEmpty())
        break;
    }

    // when there are still ships remaining, search other sector task forces
    val shpIt = shipIds.iterator();
    while (shpIt.hasNext()) {
      short shipId = shpIt.next();
      TaskForce shpTF = gTForceList.findShipTaskForce(shipId, true);
      if (shpTF != null && removeFromTaskForce(shpTF, shipId)) {
        missingSectorTFs.add(shipId);
        removed.add(shipId);
        shpIt.remove();
      }
    }

    // transfer ships
    gtf.addShips(removed);

    if (!shipIds.isEmpty()) {
      String err = "Warning: The following ships are completely missing from GTForceList:\n";
      System.out.println(err + shipIds);
    }

    if (!missingSectorTFs.isEmpty()) {
      System.out.println("Warning: The following ships are missing from GTForceList sector "
        + gwtf.getSectorPos() + ":\n" + missingSectorTFs
      );
    }

    return gtf;
  }

  private TaskForce splitDeviatingTFShips(TaskForce tf, Set<Short> shipIds) throws KeyNotFoundException, IOException {
    ArrayList<Short> deviating = new ArrayList<Short>();

    // find deviating task force ships
    short[] gtShipIds = tf.getShipIDs();
    for (short shipId : gtShipIds) {
      if (!shipIds.remove(shipId))
        deviating.add(shipId);
    }

    // split deviating task force ships
    TaskForce tfCopy = null;
    if (deviating.size() > 0) {
      tf.removeShips(deviating);

      tfCopy = sgif.gtfList().addTaskForce(tf.getOwnerId(), tf.getSectorColumn(), tf.getSectorRow());
      addGTAlien(tf.id(), tf.getOwnerId());
      tfCopy.addShips(deviating);
      // reset mission for added ship types
      tfCopy.resetMission();
    }

    // return task force copy
    return tfCopy;
  }

  private void moveTaskForces(short[] taskForceIds, int x, int y, boolean keepMission) throws KeyNotFoundException, IOException {
    for (short tfId : taskForceIds)
      moveTaskForce(tfId, x, y, keepMission);
  }

  /**
   * swaps task force positions but retains orders & mission
   * @throws KeyNotFoundException
   */
  public void swapSectorTaskForces(int sourceIndex, int destIndex) throws KeyNotFoundException, IOException {
    Sector sector = sgif.sectorLst();
    int[] sourcePos = sector.getPosition(sourceIndex);
    int[] destPos = sector.getPosition(destIndex);
    short[] sourceTfIds = sector.getTaskForces(sourceIndex);
    short[] destTfIds = sector.getTaskForces(destIndex);
    moveTaskForces(sourceTfIds, destPos[0], destPos[1], true);
    moveTaskForces(destTfIds, sourcePos[0], sourcePos[1], true);
  }

  public void moveAllSectorTaskForces(int sectorIndex, int x, int y) throws KeyNotFoundException, IOException {
    short[] taskForceIds = sgif.sectorLst().getTaskForces(sectorIndex);
    moveTaskForces(taskForceIds, x, y, false);
  }

  // #region TF missions

  public void changeMissionTargets(int sourceIdx, int destIdx, boolean updateSource,
    boolean updateTarget, boolean keepMission) throws KeyNotFoundException, IOException {

    AITaskMgr taskMgr = sgif.aiTaskMgr();
    Sector sector = sgif.sectorLst();
    int sx = sector.getHorizontal(sourceIdx);
    int sy = sector.getVertical(sourceIdx);
    int dx = sector.getHorizontal(destIdx);
    int dy = sector.getVertical(destIdx);

    // update matching orders and AI tasks
    if (updateSource) {
      sgif.ordInfo().updateTargetPosition(OrderInfo.CoordSelector.TARGET, sx, sy, dx, dy, updateTarget);

      // for AI tasks update both the source and target locations
      // many tasks like repair and training have same source and target location
      // even the explore task might have to be routed in a star system to build
      // and order ships
      taskMgr.updateTargetPosition(AITaskMgr.CoordSelector.ALL, sx, sy, dx, dy, updateTarget);
    } else if (updateTarget) {
      sgif.ordInfo().updateTargetPosition(OrderInfo.CoordSelector.TARGET, dx, dy, sx, sy, false);
      taskMgr.updateTargetPosition(AITaskMgr.CoordSelector.ALL, dx, dy, sx, sy, false);
    }

    TaskForceList gTForceList = sgif.gtfList();
    TaskForceList gWTForce = sgif.gwtForce();

    // update matching task force sector targets
    // also update GTForceList initial turn targets
    short[] tfIds = gWTForce.getTaskForceIds();
    for (short tfId : tfIds) {
      TaskForce gwtf = gWTForce.taskForce(tfId);

      int wtarget = gwtf.getTargetSectorIndex();
      if (updateSource && wtarget == sourceIdx) {
        // update target sector
        gwtf.setTargetSector(destIdx);
        // reset local sector missions
        if (!keepMission)
          gwtf.resetLocalMission();
      }
      if (updateTarget && wtarget == destIdx) {
        // update target sector
        gwtf.setTargetSector(sourceIdx);
        // reset local sector missions
        if (!keepMission)
          gwtf.resetLocalMission();
      }

      // also update GTForceList
      // although this should be redundant
      TaskForce gtf = gTForceList.getTaskForce(tfId);

      if (gtf != null) {
        int target = gtf.getTargetSectorIndex();
        if (updateSource && target == sourceIdx) {
          // update target sector
          gtf.setTargetSector(destIdx);
          // reset local sector missions
          if (!keepMission)
            gtf.resetLocalMission();
        }
        if (updateTarget && target == destIdx) {
          // update target sector
          gtf.setTargetSector(sourceIdx);
          // reset local sector missions
          if (!keepMission)
            gtf.resetLocalMission();
        }
      }
    }
  }

  /**
   * cancel system bound target sector task force missions
   * used when a system is hidden or removed
   * @throws KeyNotFoundException
   * @throws IOException
   */
  public void cancelSectorSystemMissions(int sectorIndex) throws KeyNotFoundException, IOException {
    TaskForceList gWTForce = sgif.gwtForce();
    TaskForceList gTForceList = sgif.gtfList();
    short[] gwtfs = gWTForce.getTaskForceIds();
    short[] gtfs = gTForceList.getTaskForceIds();

    // iterate all the task forces, to also find those that are on the move
    for (short tfId : gwtfs) {
      TaskForce gwtf = gWTForce.taskForce(tfId);
      cancelSectorSystemMission(sectorIndex, gwtf);
    }

    // reset GTForceList missions, which however should be redundant
    for (short tfId : gtfs) {
      TaskForce gtf = gTForceList.taskForce(tfId);
      cancelSectorSystemMission(sectorIndex, gtf);
    }
  }

  private void cancelSectorSystemMission(int sectorIndex, TaskForce tf)
    throws KeyNotFoundException, IOException {

    // skip task forces that don't have a system bound mission
    int gwtMission = tf.getMission();
    if (!Mission.isSystemBound(gwtMission))
      return;

    // skip missions that have a different target system
    int wtarget = tf.getTargetSectorIndex();
    if (wtarget == sectorIndex)
      return;

    // reset mission & task force order
    tf.resetMission();
  }

  /**
   * reset specific target sector task force missions
   * used to reset the ENTER_WORMHOLE mission if moved
   * @throws KeyNotFoundException
   * @throws IOException
   */
  public void cancelTargetSectorMission(int sectorIndex, int mission)
    throws KeyNotFoundException, IOException {

    TaskForceList gTForceList = sgif.gtfList();
    TaskForceList gWTForce = sgif.gwtForce();
    short[] gwtfs = gWTForce.getTaskForceIds();

    // even though ENTER_WORMHOLE is a local sector mission,
    // iterate all the task forces just in case
    for (short tfId : gwtfs) {
      TaskForce gwtf = gWTForce.taskForce(tfId);
      cancelTargetSectorMission(sectorIndex, mission, gwtf);
    }

    // reset GTForceList missions, which however should be redundant
    short[] gtfs = gTForceList.getTaskForceIds();
    for (short tfId : gtfs) {
      TaskForce gtf = gTForceList.taskForce(tfId);
      cancelTargetSectorMission(sectorIndex, mission, gtf);
    }
  }

  private void cancelTargetSectorMission(int sectorIndex, int mission, TaskForce tf)
    throws KeyNotFoundException, IOException {

    // skip task forces that have a different mission
    int tfMission = tf.getMission();
    if (tfMission != mission)
      return;

    // skip missions that have a different target system
    int wtarget = tf.getTargetSectorIndex();
    if (wtarget == sectorIndex)
      return;

    // reset mission & task force order
    tf.resetMission();
  }

  public void cancelMission(int mission) throws IOException {
    TaskForceList gTForceList = sgif.gtfList();
    TaskForceList gWTForce = sgif.gwtForce();
    short[] gwtfs = gWTForce.getTaskForceIds();

    for (short tfId : gwtfs) {
      TaskForce gwtf = gWTForce.getTaskForce(tfId);
      cancelMission(mission, gwtf);
    }

    // reset GTForceList missions, which however should be redundant
    short[] gtfs = gTForceList.getTaskForceIds();
    for (short tfId : gtfs) {
      TaskForce gtf = gTForceList.getTaskForce(tfId);
      cancelMission(mission, gtf);
    }
  }

  private void cancelMission(int mission, TaskForce tf) {
    int tfMission = tf.getMission();

    // skip task forces that have a different mission
    if (tfMission == mission) {
      // reset mission & task force order
      tf.resetMission();
    }
  }

  public void clearTaskForceOrders(short taskForceId)
      throws KeyNotFoundException, IOException {
    AIAgentMgr agentMgr = sgif.aiAgentMgr();
    TaskForce gwtf = sgif.gwtForce().taskForce(taskForceId);
    int raceId = gwtf.getOwnerId();

    // reset mission & task force order
    gwtf.resetMission();

    // also reset GTForceList mission if set
    // although this should be redundant
    TaskForce gtf = sgif.gtfList().getTaskForce(taskForceId);
    if (gtf != null)
      gtf.resetMission();

    // clear AI ship tasks (optional)
    // depends on GTForceList, but for the AI, GWTForce and GTForceList
    // task forces should always be same
    if (agentMgr.hasRaceAgent(raceId)) {
      AIShpUnt aiShpUnt = sgif.aiShpUnt();
      short[] shipIds = gwtf.getShipIDs();
      for (short shipId : shipIds) {
        // skip ships missing from AIShpUnt
        // e.g. monsters have no AI task planning
        if (!aiShpUnt.hasShipId(shipId))
          continue;

        short aiTaskId = aiShpUnt.getShipTask(shipId);
        if (aiTaskId != -1) {
          TskSh tskSh = (TskSh) game.getInternalFile(CSavFiles.TskSh + aiTaskId, true);
          tskSh.removeShip(shipId);
          aiShpUnt.clearShipUnt(shipId);
        }
      }
    }
  }

  private void clearTaskForceOrders(short[] taskForceIds)
    throws KeyNotFoundException, IOException {
    for (short tfId : taskForceIds)
      clearTaskForceOrders(tfId);
  }

  public void clearAllSectorTaskForceOrders(int sectorIndex)
      throws KeyNotFoundException, IOException {
    short[] taskForceIds = sgif.sectorLst().getTaskForces(sectorIndex);
    clearTaskForceOrders(taskForceIds);
  }

  public void clearTaskForceResults(short taskForceId)
      throws KeyNotFoundException, IOException {
    // remove task force results
    ResultList resultList = sgif.resultList();
    resultList.removeTaskForceResults(taskForceId);

    // remove ship results
    // therefore lookup the current turn selection task force ships
    TaskForce tf = sgif.gwtForce().getTaskForce(taskForceId);
    if (tf != null) {
      short[] shipIds = tf.getShipIDs();
      for (short shipId : shipIds)
        resultList.removeShipResults(shipId);
    }
  }

  private void clearTaskForceResults(short[] taskForceIds)
    throws KeyNotFoundException, IOException {
    for (short tfId : taskForceIds)
      clearTaskForceResults(tfId);
  }

  public void clearAllSectorTaskForceResults(int sectorIndex)
      throws KeyNotFoundException, IOException {
    // lookup GTForceList for the turn start sector task force ids
    int[] pos = sgif.sectorLst().getPosition(sectorIndex);
    short[] taskForceIds = sgif.gtfList().getSectorTaskForces(pos[0], pos[1]);
    clearTaskForceResults(taskForceIds);
  }

  // #endregion TF missions

  public short[] addTaskForce(ShipList shipModels, int sectorIndex, short raceId, short shipType, int shipCount)
    throws KeyNotFoundException, IOException {
    TaskForceList gTForceList = sgif.gtfList();
    TaskForceList gWTForce = sgif.gwtForce();
    GShipList shipList = sgif.gShipList();

    ArrayList<Short> added = new ArrayList<Short>(shipCount);
    int[] pos = sgif.sectorLst().getPosition(sectorIndex);

    for (int i = 0; i < shipCount; ) {
      // add task force to gTForceList and gWTForce
      short taskForceId = nextTFId();
      TaskForce gwtf = gWTForce.addTaskForce(taskForceId, raceId, pos[0], pos[1]);
      TaskForce gtf = gTForceList.addTaskForce(taskForceId, raceId, pos[0], pos[1]);
      addGTAlien(taskForceId, raceId);

      int max = i + Integer.min(shipCount - i, 9);

      // add ships
      for (; i < max; i++) {
        // add ship to GShipList
        short shipId = shipList.addShip(taskForceId, shipType, raceId);

        // assign ship to the task force
        // and for the player race we are done
        gwtf.addShip(shipId);
        gtf.addShip(shipId);

        // remember ships for agent assignment
        added.add(shipId);
      }

      // reset mission for added ship types
      gwtf.resetMission();
      gtf.resetMission();
    }

    // for non-player races, add to AI agents
    short[] shipIds = CollectionTools.toShortArray(added);
    addShipsToRaceAgent(shipIds, raceId);

    return shipIds;
  }

  private short nextTFId() throws IOException {
    return (short) (1 + Integer.max(sgif.gwtForce().getMaxTaskForceId(), sgif.gtfList().getMaxTaskForceId()));
  }

  private void addGTAlien(short taskForceId, int raceId) throws IOException {
    // update alienTFInfo that maps the GTForceList ownership for when the turn started
    AlienTFInfo alienTFInfo = (AlienTFInfo) game.tryGetInternalFile(CSavFiles.AlienTFInfo + raceId, true);
    if (alienTFInfo != null) {
      alienTFInfo.addTaskForce(taskForceId);
    } else {
      // only the monsters have no alienTFInfo
      sgif.monsterList().addMonster(taskForceId);
    }
  }

  /**
   * Removes a ship from the task force, but not from GShipList.
   * @throws IOException
   * @throws KeyNotFoundException
   */
  public void removeFromTaskForce(short taskForceId, short shipId) throws IOException, KeyNotFoundException {
    TaskForce gwtf = sgif.gwtForce().taskForce(taskForceId);
    removeFromTaskForce(gwtf, shipId);

    // also remove ship from GTForceList
    TaskForce gtf = findShipTaskForce(sgif.gtfList(), taskForceId, shipId);
    removeFromTaskForce(gtf, shipId);
  }

  private boolean removeFromTaskForce(TaskForce tf, short shipId) throws IOException {
    if (tf.removeShip(shipId)) {
      if (!tf.isGWTForce())
        sgif.resultList().removeShipResults(shipId);
      if (tf.shipCount() == 0)
        removeFromTFList(tf);
      return true;
    }
    return false;
  }

  private void removeTaskForceShips(TaskForce tf) throws IOException {
    short[] shipIds = tf.getShipIDs();
    if (shipIds.length == 0) {
      removeFromTFList(tf);
      return;
    }

    // remove ships from AI agents
    // remove by gwtForce, since the agents are for active task force management,
    // and therefore always refer to the newest task force state
    if (tf.isGWTForce()) {
      // don't remove from GShipList yet!
      // for ship race lookup, they must still be listed
      int raceId = tf.getOwnerId();
      removeFromShipAgents(raceId, shipIds);
    }

    // either case, remove all task force ships
    GShipList shipList = sgif.gShipList();
    for (short shipId : shipIds) {
      // remove from task force, ordInfo, resultList, alienTFInfo & monsters
      removeFromTaskForce(tf, shipId);
      // remove from global GShipList
      shipList.removeShip(shipId);
    }
  }

  private void removeTaskForceSiblings(TaskForce tf, short[] shipIds) throws IOException {
    TaskForceList otherTFList = tf.isGWTForce() ? sgif.gtfList() : sgif.gwtForce();
    for (short shipId : shipIds) {
      // plus search and remove from other task force list
      TaskForce otherTF = findShipTaskForce(otherTFList, tf.id(), shipId);
      removeFromTaskForce(otherTF, shipId);
    }
  }

  private void removeFromTFList(TaskForce tf) throws IOException {
    TaskForceList tfList = tf.getTfList();
    short taskForceId = tf.id();

    // remove task force
    tfList.removeTaskForce(taskForceId, true);

    if (tfList.isGWTForce()) {
      sgif.ordInfo().removeTaskForceOrder(taskForceId);
    } else {
      removeGTAlien(taskForceId, tf.getOwnerId());
      sgif.resultList().removeTaskForceResults(taskForceId);
    }
  }

  private void removeGTAlien(short taskForceId, int raceId) throws IOException {
    AlienTFInfo alienTFInfo = (AlienTFInfo) game.tryGetInternalFile(CSavFiles.AlienTFInfo + raceId, true);

    // update alienTFInfo that maps the GTForceList ownership for when the turn started
    if (alienTFInfo != null) {
      alienTFInfo.removeTaskForce(taskForceId);
    } else {
      // only the monsters have no alienTFInfo
      sgif.monsterList().removeMonster(taskForceId);
    }
  }

  private TaskForce findShipTaskForce(TaskForceList lst, short expectedTFId, short shipId) throws IOException {
    TaskForce tf = expectedTFId == -1 ? null : lst.getTaskForce(expectedTFId);
    if (tf == null || !tf.hasShip(shipId))
      tf = sgif.gtfList().findShipTaskForce(shipId, true);
    return tf;
  }

  public void removeTaskForce(short taskForceId) throws IOException {
    TaskForce gwtf = sgif.gwtForce().getTaskForce(taskForceId);
    if (gwtf == null)
      return;
    removeTaskForce(gwtf);
  }

  public void removeTaskForce(TaskForce tf) throws IOException {
    removeTaskForceSiblings(tf, tf.getShipIDs());
    removeTaskForceShips(tf);
  }

  public void removeTaskForce(TaskForceList tfList, short taskForceId) throws IOException {
    TaskForce tf = tfList.getTaskForce(taskForceId);
    if (tf != null)
      removeTaskForceShips(tf);
  }

  /**
   * Completely remove a ship and also remove empty taskforces.
   * @throws IOException
   * @throws KeyNotFoundException
   */
  public void removeShip(short taskForceId, short shipId) throws IOException, KeyNotFoundException {
    int raceId = sgif.gwtForce().taskForce(taskForceId).getOwnerId();
    removeFromTaskForce(taskForceId, shipId);
    removeFromShipAgents(raceId, shipId);
  }

  /**
   * Add ship to AI agents.
   * @throws IOException
   */
  public void addShipToAgents(SavGame game, Ship shp) throws IOException {
    int raceId = shp.getRaceID();
    if (raceId == -1)
      return;

    // ship list is required to add any ships
    ShipList shipModels = stif.shipList();

    short shipId = shp.shipID();
    ShipDefinition shipDef = shipModels.getShipDefinition(shp.getShipType());
    short shipRole = shipDef.getShipRole();

    // for non-player races add to AI agent
    AIAgent agent = sgif.aiAgentMgr().getRaceAgent(raceId);
    if (agent == null)
      return;

    // get AgtSh
    String agtFileName = CSavFiles.AgtSh + agent.getId();
    AgtSh agtSh = (AgtSh) game.getInternalFile(agtFileName, true);

    // add to AgtSh
    agtSh.addShip(shipId);

    // add to AIShpUnt & TskSh
    // but skip monsters that have no task planning
    if (agent.getType() != AIAgent.Type.Monster) {
      // get AIShpUnt
      AIShpUnt aiShpUnt = (AIShpUnt) game.getInternalFile(CSavFiles.AIShpUnt, true);

      // add to ShpUnt
      aiShpUnt.addShipUnt(shipId, shipRole);
    }
  }

  /**
   * Remove ships from AI agents.
   * @throws IOException
   */
  public void removeFromShipAgents(Ship shp) throws IOException {
    removeFromShipAgents(shp.getRaceID(), shp.shipID());
  }

  public void removeFromShipAgents(int tfRaceId, short shipId) throws IOException {
    // remove from AI agents
    removeAgentShips(tfRaceId, new short[] { shipId });

    // remove from AIShpUnt & TskSh files
    removeShpUntShip(shipId);
  }

  public void removeFromShipAgents(int tfRaceId, short[] shipIds) throws IOException {
    // remove from AI agents
    removeAgentShips(tfRaceId, shipIds);

    // remove from AIShpUnt & TskSh files
    for (short shipId : shipIds)
      removeShpUntShip(shipId);
  }

  /**
   * For hijacked ships, falls back to lookup the race from result.lst
   * @throws IOException
   */
  private void removeAgentShips(int tfRaceId, short[] shipIds) throws IOException {
    Set<Short> toRemove = CollectionTools.toSet(shipIds);
    Set<Short> removed = new HashSet<Short>(toRemove.size());

    // remove from task force race agent
    // which usually should be the case
    removeRaceAgentShips(toRemove, tfRaceId, removed);
    if (removed.size() == toRemove.size())
    return;

    // if not removed, check the event log for hijacked ship agents
    removeHijackedAgentShips(toRemove, removed);
    if (removed.size() == toRemove.size())
      return;

    // if still not found, check for inconsistent ship races
    // these always should match the task force race though
    removeFromShipRaceAgents(toRemove, tfRaceId, removed, true);
    if (removed.size() == toRemove.size())
      return;

    // if not hijacked, ignore player ships
    // only the AI has agent files
    GameInfo gameInfo = sgif.gameInfo();
    if (gameInfo != null && gameInfo.getPlayerEmpire() == tfRaceId)
      return;

    // fallback to search all ship agents
    removeAllAgentShips(toRemove, removed);

    if (removed.size() != toRemove.size()) {
      toRemove.removeAll(removed);

      String err = "Removed non-player ships were not listed by AI agents: " + toRemove;
      System.out.println(err);
    }
  }

  private int removeHijackedAgentShips(Collection<Short> shipIds, Collection<Short> removed) throws IOException {
    // To not fail on multi-ship removal during ship iteration,
    // and not skip processed ship ids from other agent removal,
    // first determine all races with lost or hijacked ship results.
    Set<Short> lostShipRaces = sgif.resultList().getLostShipRaces();

    // remove ships from race agents
    int cnt = 0;
    for (short raceId : lostShipRaces) {
      cnt += removeRaceAgentShips(shipIds, raceId, removed);
    }

    return cnt;
  }

  private int removeAllAgentShips(Collection<Short> shipIds, Collection<Short> removed) {
    val agts = game.getFileNames(SavFileFilters.AgtSh);
    int cnt = 0;

    for (String agt : agts) {
      int rem = removeAgentShips(shipIds, agt, removed);
      if (rem != 0) {
        System.out.println("Removed ships from unexpected agent: " + agt);
        cnt += rem;

        if (shipIds.size() == cnt)
          break;
      }
    }

    return cnt;
  }

  public int removeFromShipRaceAgents(Collection<Short> shipIds, int tfRaceId, Collection<Short> removed, boolean skipTfRace) throws IOException {
    GShipList shipList = sgif.gShipList();
    int cnt = 0;

    // map ships by race
    Map<Short, Set<Short>> raceShips = shipIds.stream()
      .map(shipId -> shipList.tryGetShip(shipId))
      .filter(ship -> ship != null)
      .collect(Collectors.groupingBy(Ship::getRaceID, Collectors.mapping(Ship::shipID, Collectors.toSet())));

    for (val entry : raceShips.entrySet()) {
      short shipRaceId = entry.getKey();
      val raceShipIds = entry.getValue();

      if (shipRaceId != tfRaceId) {
        for (short shipId : raceShipIds) {
          System.out.println("Warning: Removed ship [" + shipId + "] race [" + shipRaceId
            + "] differs from task force race [" + tfRaceId + "].");
        }
      } else if (skipTfRace) {
        continue;
      }

      cnt += removeRaceAgentShips(raceShipIds, shipRaceId, removed);
    }

    return cnt;
  }

  private int removeRaceAgentShips(Collection<Short> shipIds, int raceId, Collection<Short> removed) {
    AIAgentMgr agentMgr = sgif.aiAgentMgr();

    // only the non-player races have an AI agent
    AIAgent agent = agentMgr.getRaceAgent(raceId);
    if (agent == null)
      return 0;

    // lookup the AgtSh
    String agtFileName = CSavFiles.AgtSh + agent.getId();
    return removeAgentShips(shipIds, agtFileName, removed);
  }

  private int removeAgentShips(Collection<Short> shipIds, String agtShName, Collection<Short> removed) {
    // lookup the AgtSh
    AgtSh agtSh = (AgtSh) game.tryGetInternalFile(agtShName, true);
    if (agtSh == null)
      return 0;

    // remove from AgtSh
    int cnt = 0;
    for (short shipId : shipIds) {
      if (agtSh.removeShip(shipId) && removed != null) {
        removed.add(shipId);
        cnt++;
      }
    };

    return cnt;
  }

  private void removeShpUntShip(short shipId) throws IOException {
    AIShpUnt aiShpUnt = sgif.aiShpUnt();

    // skip ships missing from AIShpUnt
    // e.g. monsters have no AI task planning
    if (!aiShpUnt.hasShipId(shipId))
      return;

    short aiTaskId = aiShpUnt.getShipTask(shipId);
    aiShpUnt.removeShipUnt(shipId);

    // remove current AI ship task
    if (aiTaskId != -1) {
      TskSh tskSh = (TskSh) game.tryGetInternalFile(CSavFiles.TskSh + aiTaskId, true);
      if (tskSh != null)
        tskSh.removeShip(shipId);
    }
  }

  private void removeTaskForces(short[] taskForceIds) throws IOException {
    for (short tfId : taskForceIds)
      removeTaskForce(tfId);
  }

  public void removeRaceTaskForces(int raceId) throws IOException {
    val taskForces = sgif.gwtForce().getRaceTaskForces(raceId);
    for (TaskForce taskForce : taskForces)
      removeTaskForce(taskForce);
  }

  public void removeAllTaskForces() throws IOException {
    sgif.gwtForce().removeAll();
    sgif.gtfList().removeAll();
    sgif.ordInfo().removeAllTaskForceOrders();
    sgif.resultList().removeAllMilitaryResults();
    sgif.monsterList().removeAllMonsters();

    // remove from alienTFInfo, that maps the GTForceList ownership
    // only the monsters have no alienTFInfo
    val alienFiles = game.getFileNames(SavFileFilters.AlienTF);
    for (String alienFile : alienFiles) {
      AlienTFInfo alienTFInfo = (AlienTFInfo) game.tryGetInternalFile(alienFile, true);
      alienTFInfo.removeAllTaskForces();
    }

    removeAllShips();
  }

  public void addPlayerTaskForce(int shipRole, int shipCount) throws KeyNotFoundException, IOException {
    short raceId = sgif.gameInfo().getPlayerEmpire();
    addRaceTaskForce(raceId, shipRole, shipCount);
  }

  public void addRaceTaskForce(short raceId, int shipRole, int shipCount) throws KeyNotFoundException, IOException {
    ShipList shipModels = stif.shipList();
    int shipType = shipModels.getShipType(raceId, shipRole);

    AlienInfo alienInfo = sgif.alienInfo();
    int systemIdx = alienInfo.getHomeSystem(raceId);
    systemIdx = systemIdx != -1 ? systemIdx : 0;

    SystInfo systInfo = sgif.systInfo();
    int sectorIdx = systInfo.getSystem(systemIdx).getSectorIndex();

    addTaskForce(shipModels, sectorIdx, raceId, (short)shipType, shipCount);
  }

  // remove from agents both for AI races and for monster
  private void removeAllShips() throws IOException {
    // remove from AI agents
    sgif.aiShpUnt().removeAll();

    // remove from AI ship agents
    val agtShFiles = game.getFileNames(SavFileFilters.AgtSh);
    for (String agtShFile : agtShFiles) {
      AgtSh agtSh = (AgtSh) game.tryGetInternalFile(agtShFile, true);
      agtSh.removeAllShips();
    }

    // remove current AI ship tasks
    val tskShFiles = game.getFileNames(SavFileFilters.TskSh);
    for (String tskShFile : tskShFiles) {
      TskSh tskSh = (TskSh) game.tryGetInternalFile(tskShFile, true);
      tskSh.removeAllShips();
    }

    // remove from global ship list
    sgif.gShipList().removeAllShips();
  }

  public void removeAllTaskForces(int controlFlags) throws IOException {
    if ((controlFlags & ControlFlags.All) == ControlFlags.All) {
      removeAllTaskForces();
    } else {
      removeAllTaskForces(sgif.gwtForce(), controlFlags);
      removeAllTaskForces(sgif.gtfList(), controlFlags);
    }
  }

  private void removeAllTaskForces(TaskForceList tfList, int controlFlags) throws IOException {
    short[] tfs = tfList.getTaskForceIds();
    for (short tfId : tfs) {
      TaskForce tf = tfList.getTaskForce(tfId);
      if (tf.isControlledBy(controlFlags))
        removeTaskForce(tfList, tfId);
    }
  }

  public void removeAllSectorTaskForces(int sectorIndex) throws IOException {
    short[] taskForceIds = sgif.sectorLst().getTaskForces(sectorIndex);
    removeTaskForces(taskForceIds);
  }

  public void setTaskForceOwner(short taskForceId, short raceId) throws KeyNotFoundException, IOException {
    TaskForce gwtf = sgif.gwtForce().taskForce(taskForceId);

    // Fix temporary task force id for AI task forces,
    // to not become empty next turn and crash the game.
    fixTempGWTForceId(gwtf);

    // If re-grouped, changes are only stored to GWTForce.
    // The ship ownership however must be kept in sync, therefore
    // search & collect the corresponding GTForceList ships.
    TaskForce gtf = collectGTForceShips(gwtf);
    int formerRaceId = gtf.getOwnerId();

    // update task force owner
    gwtf.setOwner(raceId);
    gtf.setOwner(raceId);

    // reset mission & task force order
    gwtf.resetMission();
    gtf.resetMission();

    // update ship owner
    GShipList shipList = sgif.gShipList();
    short[] shipIds = gwtf.getShipIDs();
    for (short shipId : shipIds)
      shipList.getShip(shipId).setRaceId(raceId);

    // update alienTFInfo
    removeGTAlien(gtf.id(), formerRaceId);
    addGTAlien(gtf.id(), raceId);

    // update AI agents if any / not a player races
    removeFromShipAgents(formerRaceId, shipIds);
    addShipsToRaceAgent(shipIds, raceId);
  }

  private void addShipsToRaceAgent(short[] shipIds, short raceId)
    throws KeyNotFoundException, IOException {
    AIAgentMgr agentMgr = sgif.aiAgentMgr();

    // for AI and monsters, check that we actually have an agent to assign
    // when the monster wins a battle, the game otherwise crashes
    AIAgent agent = agentMgr.getRaceAgent(raceId);
    if (agent == null)
      return;

    // for non-player races, add to AI agents
    // but skip new monsters and other races that have no agent
    AgtSh agtSh = (AgtSh) game.tryGetInternalFile(CSavFiles.AgtSh + agent.getId(), true);
    if (agtSh == null)
      return;

    // add to AgtSh
    for (short shipId : shipIds)
      agtSh.addShip(shipId);

    // for AI further add to AIShpUnt
    AIShpUnt aiShpUnt = sgif.aiShpUnt();
    GShipList shipList = sgif.gShipList();
    ShipList shipModels = stif.shipList();
    for (short shipId : shipIds) {
      int shipType = shipList.getShip(shipId).getShipType();
      ShipDefinition shipDef = shipModels.getShipDefinition(shipType);
      short shipRole = shipDef.getShipRole();
      aiShpUnt.addShipUnt(shipId, shipRole);
    }
  }

  private void setTaskForceOwners(short[] taskForceIds, short raceId) throws KeyNotFoundException, IOException {
    for (short taskForceId : taskForceIds)
      setTaskForceOwner(taskForceId, raceId);
  }

  public void setAllSectorTaskForceOwners(int sectorIndex, short raceId) throws KeyNotFoundException, IOException {
    short[] taskForceIds = sgif.sectorLst().getTaskForces(sectorIndex);
    setTaskForceOwners(taskForceIds, raceId);
  }

}
