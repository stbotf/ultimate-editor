package ue.edit.sav.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.ai.AINumTasks;
import ue.edit.sav.files.ai.AITask;
import ue.edit.sav.files.ai.AITaskIdCtr;
import ue.edit.sav.files.ai.AgtTk;
import ue.edit.sav.files.ai.AITask.TaskCategory;
import ue.gui.util.dialog.Dialogs;

public class AITaskMgr {

  // #region constants

  static final String ClassName = AITaskMgr.class.getSimpleName();
  static final String CntSuffix = ".cnt";

  public enum CoordSelector {
    SOURCE,
    TARGET,
    ALL
  }

  // #endregion constants

  // #region fields

  // map AITasks by task id, sorted by id
  TreeMap<Short, AITask> taskMap = new TreeMap<>();
  // map AITasks by agent id
  HashMap<Integer, List<AITask>> agentTasks = new HashMap<>();

  SavGame game;
  AITaskIdCtr taskCtr;
  AINumTasks taskNum;

  // #endregion fields

  // #region constructor

  public AITaskMgr(SavGame game) {
    this.game = game;
    this.taskCtr = (AITaskIdCtr) game.tryGetInternalFile(CSavFiles.AITaskIdCtr, true);
    this.taskNum = (AINumTasks) game.tryGetInternalFile(CSavFiles.AINumTasks, true);
    reload();
  }

  // #endregion constructor

  // #region property gets

  public int getNumTasks() {
    return taskMap.size();
  }

  // called for AITaskIdCtr correction
  public short getMaxTaskId() {
    return taskMap.isEmpty() ? -1 : taskMap.lastKey();
  }

  public AITask getTask(short taskId) {
    return taskMap.get(taskId);
  }

  public List<AITask> getAgentTasks(int agentId) {
    return agentTasks.get(agentId);
  }

  // #endregion property gets

  // #region property sets

  public AITask removeTask(short taskId) throws IOException {
    AITask task = removeTaskImpl(taskId);

    if (task != null) {
      // update successive AITask file names
      updateFileNumbers();
      // update the total task number
      updateTaskNum();
    }

    return task;
  }

  public void removeTasks(Collection<Short> taskIds) throws IOException {
    boolean removed = false;
    for (short taskId : taskIds) {
      if (removeTaskImpl(taskId) != null)
        removed = true;
    }

    if (removed) {
      // update successive AITask file names
      updateFileNumbers();
      // update the total task number
      updateTaskNum();
    }
  }

  public void removeTaskList(Collection<AITask> tasks) throws IOException {
    boolean removed = false;
    for (AITask task : tasks) {
      if (removeTaskImpl(task.id()) != null)
        removed = true;
    }

    if (removed) {
      // update successive AITask file names
      updateFileNumbers();
      // update the total task number
      updateTaskNum();
    }
  }

  public void removeAllTasks() throws IOException {
    // clear all tasks from AgtTk mappings
    val tkFiles = game.getFileNames(SavFileFilters.AgtTk);
    for (String file : tkFiles) {
      AgtTk agentTasks = (AgtTk) game.tryGetInternalFile(file, true);
      if (agentTasks != null)
        agentTasks.removeAllTasks();
    }

    // remove all TskSh files
    val shFiles = game.getFileNames(SavFileFilters.TskSh);
    for (String file : shFiles) {
      remSubFile(file);
    }

    // remove all TskSy files
    val syFiles = game.getFileNames(SavFileFilters.TskSy);
    for (String file : syFiles) {
      remSubFile(file);
    }

    // remove AITask files
    for (AITask task : taskMap.values()) {
      game.removeInternalFile(task.getName());
    }

    // clear task maps
    taskMap.clear();
    agentTasks.clear();
    // update the total task number
    updateTaskNum();
  }

  // #endregion property sets

  // #region helper routines

  public List<AITask> listTasksOfCategory(int category) throws IOException {
    return taskMap.values().stream()
      .filter(task -> task.getCategory() == category)
      .collect(Collectors.toList());
  }

  public List<AITask> findSystemTasks(short systemId) throws IOException {
    return taskMap.values().stream()
      .filter(task -> task.getSystemId() == systemId)
      .collect(Collectors.toList());
  }

  public void onRemovedSystem(short systemId) throws IOException {
    ArrayList<AITask> rem = new ArrayList<>();

    // remove and update system IDs
    boolean removed = taskMap.values().removeIf(task -> {
      boolean match = task.onRemovedSystem(systemId);
      if (match)
        rem.add(task);
      return match;
    });

    if (removed) {
      removeTaskFileRefs(rem);
      // update successive AITask file names
      updateFileNumbers();
      // update the total task number
      updateTaskNum();
    }
  }

  public List<AITask> removeTasksOfCategory(int category) throws IOException {
    ArrayList<AITask> rem = new ArrayList<>();

    boolean removed = taskMap.values().removeIf(task -> {
      boolean match = task.getCategory() == category;
      if (match)
        rem.add(task);
      return match;
    });

    if (removed) {
      removeTaskFileRefs(rem);
      // update successive AITask file names
      updateFileNumbers();
      // update the total task number
      updateTaskNum();
    }

    return rem;
  }

  /**
   * remove AI agent tasks
   */
  public List<AITask> removeAgentTasks(int agentId) throws IOException {
    // ignore the AgtTk, but use loaded agent task mappings
    List<AITask> rem = agentTasks.remove(agentId);
    if (rem != null)
      removeTaskList(rem);
    return rem;
  }

  public List<AITask> removeRaceTasks(short raceId) throws IOException {
    ArrayList<AITask> rem = new ArrayList<>();

    boolean removed = taskMap.values().removeIf(task -> {
      boolean match = task.isRaceTask(raceId);
      if (match)
        rem.add(task);
      return match;
    });

    if (removed) {
      removeTaskFileRefs(rem);
      // update successive AITask file names
      updateFileNumbers();
      // update the total task number
      updateTaskNum();
    }

    return rem;
  }

  public List<AITask> removeTargetSectorTasks(int x, int y) throws IOException {
    ArrayList<AITask> rem = new ArrayList<>();

    boolean removed = taskMap.values().removeIf(task -> {
      boolean match = task.getTargetSectorColumn() == x && task.getTargetSectorRow() == y;
      if (match)
        rem.add(task);
      return match;
    });

    if (removed) {
      removeTaskFileRefs(rem);
      // update successive AITask file names
      updateFileNumbers();
      // update the total task number
      updateTaskNum();
    }

    return rem;
  }

  // remove all the agent tasks and update onfollowing agent ids
  public void onRemovedAgent(int agentId) throws IOException {
    removeAgentTasks(agentId);

    HashMap<Integer, List<AITask>> copy = new HashMap<>();
    for (val entry : agentTasks.entrySet()) {
      int agtId = entry.getKey();
      List<AITask> tasks = entry.getValue();

      // decrement all onfollowing agent ids
      if (agtId > agentId) {
        agtId--;
        for (AITask task : tasks)
          task.setAgentId(agtId);
      }

      copy.put(agtId, tasks);
    }

    agentTasks = copy;
  }

  // without knowing more on ai tasks, they should be updated whenever
  // a star system or stellar object is moved, cause some tasks are at least
  // known to depend on a star system once the target position is reached
  public int updateTargetPosition(CoordSelector selector, int currentX, int currentY, int newX,
      int newY, boolean bothway) {
    int numChanges = 0;
    for (AITask task : taskMap.values()) {
      if (selector == CoordSelector.SOURCE || selector == CoordSelector.ALL) {
        if (task.getSourceSectorColumn() == currentX
            && task.getSourceSectorRow() == currentY) {
          task.setSourceSectorColumn(newX);
          task.setSourceSectorRow(newY);
          ++numChanges;
        } else if (bothway && task.getSourceSectorColumn() == newX
            && task.getSourceSectorRow() == newY) {
          task.setSourceSectorColumn(currentX);
          task.setSourceSectorRow(currentY);
          ++numChanges;
        }
      }

      if (selector == CoordSelector.TARGET || selector == CoordSelector.ALL) {
        if (task.getTargetSectorColumn() == currentX
            && task.getTargetSectorRow() == currentY) {
          task.setTargetSectorColumn(newX);
          task.setTargetSectorRow(newY);
          ++numChanges;
        } else if (bothway && task.getTargetSectorColumn() == newX
            && task.getTargetSectorRow() == newY) {
          task.setTargetSectorColumn(currentX);
          task.setTargetSectorRow(currentY);
          ++numChanges;
        }
      }
    }

    return numChanges;
  }

  // #endregion helper routines

  // #region load

  public void reload() {
    clear();
    val entries = game.getFileNames(SavFileFilters.AITask);
    ArrayList<String> errors = new ArrayList<String>();

    // load AITasks
    for (String entry : entries) {
      try {
        AITask task = (AITask) game.getInternalFile(entry, true);
        taskMap.put(task.id(), task);
        List<AITask> aiTasks = agentTasks.merge(task.getAgentId(), new ArrayList<>(), (x,y) -> x != null ? x : y);
        aiTasks.add(task);
      } catch (IOException e) {
        errors.add(ClassName + ": Failed to load task " + entry + "!");
      }
    }

    if (!errors.isEmpty()) {
      String err = String.join("\n", errors);
      Dialogs.displayError(ClassName + " load error", err);
    }
  }

  // #endregion load

  // #region maintenance

  public void clear() {
    taskMap.clear();
    agentTasks.clear();
  }

  // #endregion maintenance

  // #region private helpers

  private AITask removeTaskImpl(short taskId) throws IOException {
    AITask task = taskMap.remove(taskId);
    if (task != null)
      removeTaskFileRefs(task);
    return task;
  }

  private void removeTaskFileRefs(AITask task) throws IOException {
    val tasks = agentTasks.get(task.getAgentId());
    if (tasks != null)
      tasks.remove(task);

    // remove from ShpUnt & SysUnt
    int category = task.getCategory();
    if (category == TaskCategory.Ship)
      game.files().aiShpUnt().clearTask(task.id());
    else if (category == TaskCategory.System)
      game.files().aiSysUnt().clearTask(task.id());

    // update AgtTk & remove TskSh & TskSy files
    removeTaskMappings(task.getAgentId(), task.id());

    // recursively remove all child tasks
    for (short childTaskId : task.listChildTasks())
      removeTaskImpl(childTaskId);

    // remove from parent task
    short parentId = task.parentId();
    if (parentId != -1) {
      AITask parent = taskMap.get(parentId);
      if (parent != null)
        parent.removeChildTask(task.id());
    }

    game.removeInternalFile(task.getName());
  }

  private void removeTaskFileRefs(Collection<AITask> tasks) throws IOException {
    for (AITask task : tasks)
      removeTaskFileRefs(task);
  }

  private void removeTaskMappings(int agentId, short taskId) {
    // remove TskSh & TskSy files
    remSubFile(CSavFiles.TskSh + taskId);
    remSubFile(CSavFiles.TskSy + taskId);

    // remove from AgtTk mapping
    AgtTk agtTk = (AgtTk) game.tryGetInternalFile(CSavFiles.AgtTk + agentId, true);
    if (agtTk != null)
      agtTk.removeTask(taskId);
  }

  private void remSubFile(String fileName) {
    game.removeInternalFile(fileName);
    game.removeInternalFile(fileName + CntSuffix);
  }

  private void updateTaskNum() {
    taskNum.setNumTasks(taskMap.size());
  }

  private void updateFileNumbers() throws IOException {
    // sort file renames by previous file suffix, so no changes are overridden
    TreeSet<Integer> sfxUpdates = taskMap.values().stream().map(AITask::getFileIndex)
      .collect(Collectors.toCollection(TreeSet<Integer>::new));

    // update all AITask suffix numbers
    int i = 0;
    for (int fileIdx : sfxUpdates) {
      if (fileIdx != i)
        game.renameInternalFile(CSavFiles.AITask + fileIdx, CSavFiles.AITask + i);

      // number all the files without leaving any gaps
      i++;
    }
  }

  // #endregion private helpers

}
