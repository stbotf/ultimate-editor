package ue.edit.sav.tools;

import java.util.HashMap;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.common.CSavFilesLC;

public abstract class SavTools {

  // the trek.exe sav file sorting
  // .cnt files precede the data files for:
  // fileTRDEINFO, fileAISYSUNT, fileAISHPUNT, fileAGT*, fileTSK*
  private static final String[] SavFileSequence = new String[]{CSavFiles.Version,
      CSavFiles.SeedInfo,
      CSavFiles.GameInfo, CSavFiles.GalInfo, CSavFiles.SystInfo, CSavFiles.StellInfo,
      CSavFiles.SectorLst,
      CSavFiles.MonsterLst, CSavFiles.StarBaseInfo, CSavFiles.RToSInfo,
      CSavFiles.AlienInfo,
      // owned race task forces, 0.. counted
      CSavFiles.AlienTFInfo,
      // general empire info
      CSavFiles.EmpsInfo,
      // prov info, 0..4 counted
      CSavFiles.ProvInfo,
      // trade route info, 0..4 counted
      CSavFiles.TrdeInfo,
      // misc details
      CSavFiles.StrcInfo, CSavFiles.OrdInfo, CSavFiles.ResultLst, CSavFiles.TechInfo,
      CSavFiles.IncidentDat, CSavFiles.Treaty, CSavFiles.GShipList, CSavFiles.GTForceList,
      CSavFiles.GWTForce, CSavFiles.GTFStasis, CSavFiles.GShipHead, CSavFiles.GTForceHd,
      CSavFiles.GWTForceHd, CSavFiles.GTFStasisHd, CSavFiles.ShipCntr, CSavFiles.TForceCntr,
      CSavFiles.ShipNameDat, CSavFiles.IntelInfo, CSavFiles.AINumAgents, CSavFiles.AINumTasks,
      // ai agents & tasks, 0.. counted
      CSavFiles.AIAgent, CSavFiles.AITask,
      // ai agent & task counter
      CSavFiles.AIAgtIdCtr, CSavFiles.AITaskIdCtr,
      // ai controlled system and ship lists
      CSavFiles.AISysUnt, CSavFiles.AIShpUnt,
      // agent details, 0.. counted, grouped by id
      CSavFiles.AgtTk, CSavFiles.AgtSy, CSavFiles.AgtSh, CSavFiles.AgtDp,
      // task details, 0.. counted, grouped by id
      CSavFiles.TskSh, CSavFiles.TskSy,
      // sphere of influence, 0..4 counted
      CSavFiles.SphOfIn
  };

  private static final String AgtSavGroup = "Agt";
  private static final String TskSavGroup = "Tsk";

  private static final HashMap<String, Integer> SavFileIndexMap = initSavFileIndexMap();

  /**
   * Chunked files consist of a sequence of subfiles, where each subfile
   * is stored with index and size but compressed on their own.
   * The result.lst SavGame file is the only known file using this special format.
   * @return whether the file is stored in special chunk format (@see ResultList)
   */
  public static boolean isChunked(String fileName) {
    return CSavFiles.ResultLst.equalsIgnoreCase(fileName);
  }

  /**
   * Tells whether the file encryption is in binary or in text format.
   *
   * This is essential for not messing the BotF save game data files.
   * It defines whether the LZSS lookup dictionary is pre-filled with:
   * - zeros (0x00) for binary data
   * - space characters (0x20) for textual data
   *
   * By current research only the techInfo file must be binary encrypted.
   * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=54211#p54211
   */
  public static boolean isBinary(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    switch (fileName) {
      // testing showed that the 0x20 techInfo entry index
      // is overwritten if not binary encrypted
      case CSavFilesLC.TechInfo:
        return true;
    }

    //Further files possibly binary encrypted:
    //    case CSavFiles.fileGALINFO:
    //    case CSavFiles.fileGSHIPHEAD:
    //    case CSavFiles.fileGSHIPLIST:
    //    case CSavFiles.fileGTFORCEHD:
    //    case CSavFiles.fileGTFSTASIS:
    //    case CSavFiles.fileGTFSTASISHD:
    //    case CSavFiles.fileGWTFORCEHD:
    //    case CSavFiles.fileINTELINFO:
    //    case CSavFiles.fileINCIDENT_DAT:
    //    case CSavFiles.fileMONSTER_LST:
    //    case CSavFiles.fileRTOSINFO:
    //    case CSavFiles.fileSEEDINFO:
    //    case CSavFiles.fileSHIPCNTR:
    //    case CSavFiles.fileSHIPNAME_DAT:
    //    case CSavFiles.fileSTELLINFO:
    //    case CSavFiles.fileTFORCECNTR:
    //    case CSavFiles.fileTREATY:
    //    case CSavFiles.fileVERSION:
    //
    //if (fileName.startsWith("AI"))
    //    return true;
    //if (fileName.startsWith("alienTFInfo"))
    //    return true;
    //if (fileName.startsWith("SphOfIn"))
    //    return true;
    //if (fileName.startsWith("Tsk"))
    //    return true;

    // all the rest crashed during load if 00'ed
    return false;
  }

  public static boolean isSavFileCnt(String fileName) {
    return fileName.endsWith(".cnt");
  }

  public static String remSavFileCnt(String fileName) {
    if (fileName.startsWith(CSavFiles.AlienTFInfo)) {
      return CSavFiles.AlienTFInfo;
    }
    if (fileName.startsWith(CSavFiles.ProvInfo)) {
      return CSavFiles.ProvInfo;
    }
    if (fileName.startsWith(CSavFiles.TrdeInfo)) {
      return CSavFiles.TrdeInfo;
    }
    if (fileName.startsWith(CSavFiles.AIAgent)) {
      return CSavFiles.AIAgent;
    }
    if (fileName.startsWith(CSavFiles.AITask)) {
      return CSavFiles.AITask;
    }
    if (fileName.startsWith(CSavFiles.AgtDp)) {
      return CSavFiles.AgtDp;
    }
    if (fileName.startsWith(CSavFiles.AgtSh)) {
      return CSavFiles.AgtSh;
    }
    if (fileName.startsWith(CSavFiles.AgtSy)) {
      return CSavFiles.AgtSy;
    }
    if (fileName.startsWith(CSavFiles.AgtTk)) {
      return CSavFiles.AgtTk;
    }
    if (fileName.startsWith(CSavFiles.TskSh)) {
      return CSavFiles.TskSh;
    }
    if (fileName.startsWith(CSavFiles.TskSy)) {
      return CSavFiles.TskSy;
    }
    if (fileName.startsWith(CSavFiles.SphOfIn)) {
      return CSavFiles.SphOfIn;
    }

    if (fileName.endsWith(".cnt")) {
      fileName = fileName.substring(0, fileName.length() - 4);
    }

    return fileName;
  }

  public static int getSavNameIndex(String fileName) {
    return SavFileIndexMap.getOrDefault(remSavFileCnt(fileName), SavFileSequence.length);
  }

  // compare sav file names according to the trek.exe save file order
  public static int compareSavName(String sav1Name, String sav2Name) {
    // group agent and task files
    if (sav1Name.startsWith(AgtSavGroup) && sav2Name.startsWith(AgtSavGroup)
        || sav1Name.startsWith(TskSavGroup) && sav2Name.startsWith(TskSavGroup)) {
      int sav1Nbr = extractDigitsAsInteger(sav1Name);
      int sav2Nbr = extractDigitsAsInteger(sav2Name);
      if (sav1Nbr != sav2Nbr) {
        return sav1Nbr - sav2Nbr;
      }
    }

    // compare on the file index
    int idx1 = getSavNameIndex(sav1Name);
    int idx2 = getSavNameIndex(sav2Name);
    if (idx1 != idx2) {
      return idx1 - idx2;
    }

    // .cnt counters come first
    boolean is1Cnt = isSavFileCnt(sav1Name);
    boolean is2Cnt = isSavFileCnt(sav2Name);
    if (is1Cnt != is2Cnt) {
      return is1Cnt ? -1 : 1;
    }

    // for the rest, default to lexicographical order
    return sav1Name.compareTo(sav2Name);
  }

  private static HashMap<String, Integer> initSavFileIndexMap() {
    HashMap<String, Integer> map = new HashMap<String, Integer>();
    for (int i = 0; i < SavFileSequence.length; i++) {
      map.put(SavFileSequence[i], i);
    }

    return map;
  }

  private static int extractDigitsAsInteger(String value) {
    return Optional.ofNullable(value)
        .map(StringUtils::getDigits)
        .map(StringUtils::trimToNull)
        .map(Integer::parseInt)
        .orElse(0);
  }
}
