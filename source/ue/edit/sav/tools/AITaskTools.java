package ue.edit.sav.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import lombok.val;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.ai.AISysUnt.SysUnt;
import ue.edit.sav.files.ai.AITask;
import ue.edit.sav.files.ai.AITask.TaskCategory;
import ue.edit.sav.files.ai.AITask.TaskType;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.TskSh;
import ue.edit.sav.files.map.TskSy;

/**
 * Contains methods for ai task edit operations in a saved sav.
 */
public class AITaskTools {

  private SavGame sav;
  private SavGameInterface sgif;

  public AITaskTools(SavGame sav) {
    this.sav = sav;
    sgif = sav.files();
  }

  public void removeAllTasks() throws IOException {
    // ensure to reset all AIShpUnt tasks
    sgif.aiShpUnt().clearAllTasks();
    // ensure to reset all AISysUnt tasks
    sgif.aiSysUnt().clearAllTasks();
    // remove AITask, TskSh and TskSy files
    // plus update the AgtTk task mappings
    sav.files().aiTaskMgr().removeAllTasks();
  }

  public void removeAllShipTasks() throws IOException {
    // ensure to reset all AIShpUnt tasks
    sgif.aiShpUnt().clearAllTasks();
    // remove AITask, TskSh and TskSy files
    // plus update the AgtTk task mappings
    sav.files().aiTaskMgr().removeTasksOfCategory(TaskCategory.Ship);
  }

  public void removeAllSystemTasks() throws IOException {
    // ensure to reset all AISysUnt tasks
    sgif.aiSysUnt().clearAllTasks();
    // remove AITask, TskSh and TskSy files
    // plus update the AgtTk task mappings
    sav.files().aiTaskMgr().removeTasksOfCategory(TaskCategory.System);
  }

  public void removeAllDiplomaticTasks() throws IOException {
    // remove AITask, TskSh and TskSy files
    // plus update the AgtTk task mappings
    sav.files().aiTaskMgr().removeTasksOfCategory(TaskCategory.Diplomacy);
  }

  public void resetSubTasks() throws IOException {
    // reset AIShpUnt tasks
    sgif.aiShpUnt().clearAllTasks();
    // reset AISysUnt tasks
    sgif.aiSysUnt().clearAllTasks();

    // reset TskSyX + TskSyX.cnt
    val tskSyNames = sav.getFileNames(SavFileFilters.TskSy);
    for (String tskSyName : tskSyNames) {
      TskSy syTsk = (TskSy) sav.getInternalFile(tskSyName, true);
      syTsk.clear();
    }

    // reset TskShX + TskShX.cnt
    val tskShNames = sav.getFileNames(SavFileFilters.TskSh);
    for (String tskShName : tskShNames) {
      TskSh shTsk = (TskSh) sav.getInternalFile(tskShName, true);
      shTsk.clear();
    }
  }

  // recursively lookup the system tasks and eliminate duplicates
  public List<AITask> lookupSystemTasks(short systemId) throws IOException {
    SysUnt sysUnt = sgif.aiSysUnt().getSystemUnt(systemId);
    return sysUnt != null ? collectSystemTasks(sysUnt) : new ArrayList<AITask>();
  }

  public void removeSystemTasks(short systemId) throws IOException {
    SysUnt sysUnt = sgif.aiSysUnt().getSystemUnt(systemId);

    // uninhabited and player controlled systems have no AISysUnt entry
    if (sysUnt != null) {
      // checking the AITask system id isn't sufficient for ship build tasks
      // therefore recursively collect all the system tasks
      List<AITask> sysTasks = collectSystemTasks(sysUnt);

      // remove found AITasks
      // plus update SysUnt, AgtTk and remove TskSh & TskSy files
      sgif.aiTaskMgr().removeTaskList(sysTasks);
    }
  }

  /**
   * remove local & target sector tasks
   */
  public void removeSectorTasks(int sectorIndex) throws IOException {
    AITaskMgr taskMgr = sgif.aiTaskMgr();
    Sector sectorLst = sgif.sectorLst();
    int[] pos = sectorLst.getPosition(sectorIndex);

    // remove system AI tasks
    // plus update ShpUnt, SysUnt, AgtTk and remove TskSh & TskSy files
    short systemId = sectorLst.getSystem(sectorIndex);
    if (systemId != -1)
      removeSystemTasks(systemId);

    // remove all target sector tasks
    // plus update ShpUnt, SysUnt, AgtTk and remove TskSh & TskSy files
    taskMgr.removeTargetSectorTasks(pos[0], pos[1]);
  }

  public void resetSystemSubTasks(short systemId) throws IOException {
    SysUnt sysUnt = sgif.aiSysUnt().getSystemUnt(systemId);

    // uninhabited and player controlled systems have no AISysUnt entry
    if (sysUnt != null) {
      // checking the AITask system id isn't sufficient for ship build tasks
      // therefore recursively collect all the system tasks
      List<AITask> sysTasks = collectSystemTasks(sysUnt);

      // reset TskSy files
      for (AITask task : sysTasks) {
        TskSy syTsk = (TskSy) sav.tryGetInternalFile(CSavFiles.TskSy + task.id(), true);
        if (syTsk != null)
          syTsk.clear();
      }

      sysUnt.clearTask();
    }
  }

  private List<AITask> collectSystemTasks(SysUnt sysUnt) throws IOException {
    List<AITask> tasks = new ArrayList<AITask>();
    TreeSet<Short> checked = new TreeSet<Short>();
    short taskId = sysUnt.getTaskId();
    short nextId = sysUnt.getNextTaskId();

    // next system tasks of different type rather are queued by SysUnt
    // and not by the tasks themselves
    if (taskId != -1)
      collectSystemTasks(sysUnt.getSystemId(), taskId, tasks, checked);
    if (nextId != -1)
      collectSystemTasks(sysUnt.getSystemId(), nextId, tasks, checked);

    return tasks;
  }

  private void collectSystemTasks(short systemId, short taskId, List<AITask> tasks, TreeSet<Short> alreadyChecked) throws IOException {
    AITaskMgr taskMgr = sgif.aiTaskMgr();

    while (taskId != -1) {
      AITask task = taskMgr.getTask(taskId);
      if (!alreadyChecked.add(taskId))
        break;

      checkSystemTask(systemId, task);
      tasks.add(task);
      taskId = task.parentId();
    }
  }

  private boolean checkSystemTask(short systemId, AITask task) throws IOException {
    if (task.getCategory() == TaskCategory.System
      && (task.getType() != TaskType.DevelopSystem || task.getSystemId() == systemId))
        return true;

    String msg = "AITaskTools: Found task %1 that doesn't match specified system id %2."
      .replace("%1", Short.toString(task.id()))
      .replace("%2", Short.toString(systemId));
    System.out.println(msg);
    return false;
  }

}
