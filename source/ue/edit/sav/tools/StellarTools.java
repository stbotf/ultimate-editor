package ue.edit.sav.tools;

import java.io.IOException;
import javax.swing.JOptionPane;
import ue.UE;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.files.map.Sector;
import ue.edit.sav.files.map.StellInfo;
import ue.edit.sav.files.map.data.StellEntry;
import ue.exception.KeyNotFoundException;

/**
 * Contains methods for stellar object edit operations in a saved game.
 */
public class StellarTools {

  SavGame game;
  SavGameInterface sgif;

  public StellarTools(SavGame game) {
    this.game = game;
    sgif = game.files();
  }

  public void addStellarObject(Objstruc objstruc, int sectorIndex, short type, String name,
    boolean noWarning) throws KeyNotFoundException, IOException {

    Sector sector = sgif.sectorLst();
    int prevId = sector.getStellarObject(sectorIndex);

    if (prevId >= 0 && !noWarning) {
      int response = JOptionPane.showConfirmDialog(UE.WINDOW,
          "Sector %1 already contains a stellar object. Replace it?", UE.APP_NAME,
          JOptionPane.OK_CANCEL_OPTION);

      if (response == JOptionPane.CANCEL_OPTION)
        return;

      // remove
      removeFromSector(sectorIndex);
    }

    // add
    StellInfo stellInfo = sgif.stellInfo();
    int objId = stellInfo.add(sector.getHorizontal(sectorIndex), sector.getVertical(sectorIndex));
    StellEntry stellObj = stellInfo.getStellarObject(objId);
    String sStr = sector.getDescription(sectorIndex, false);
    name = name + sStr.substring(sStr.indexOf(" "));

    stellObj.setType(type);
    stellObj.setName(name);
    stellObj.setAnimation(objstruc.getGraph(type));
    stellObj.setPosition(sector.getHorizontal(sectorIndex), sector.getVertical(sectorIndex));
    sgif.galInfo().setAnomalyCount(stellInfo.getNumberOfEntries());
    sector.setStellarObject(sectorIndex, (short) objId);

    // update stellar object exploration
    long explored = sector.getExplored(sectorIndex);
    stellObj.setVisitorsMask((int) (explored & 31));
  }

  public void removeFromSector(int sectorIndex) throws KeyNotFoundException, IOException {
    short stellId = sgif.sectorLst().getStellarObject(sectorIndex);
    if (stellId >= 0)
      removeStellarObject(stellId);
  }

  public void removeStellarObject(int stellId) throws KeyNotFoundException, IOException {
    StellInfo stellInfo = sgif.stellInfo();
    StellEntry rem = stellInfo.remove(stellId);

    if (rem != null) {
      sgif.galInfo().setAnomalyCount(stellInfo.getNumberOfEntries());
      Sector sector = sgif.sectorLst();

      // update all sector anomaly indices for the removed array element
      for (int i = 0; i < sector.getNumberOfSectors(); i++) {
        short sid = sector.getStellarObject(i);
        if (sid == stellId)
          sector.setStellarObject(i, (short) -1);
        else if (sid > stellId)
          sector.setStellarObject(i, --sid);
      }

      // cancel enter wormhole missions
      if (rem.getType() == StellarType.WormHole)
        sgif.taskForceTools().cancelTargetSectorMission(rem.getSectorIndex(), Mission.EnterWormhole);
    }
  }

  public void removeAllStellarObjects() throws IOException {
    sgif.stellInfo().removeAll();
    sgif.galInfo().setAnomalyCount((short)0);
    sgif.sectorLst().removeAllStellarObjects();
    sgif.taskForceTools().cancelMission(Mission.EnterWormhole);
  }

  // move stellar object to target map position
  // switches any existing systems and stellar objects
  public void moveStellarObject(short stellarId, int x, int y) throws KeyNotFoundException, IOException {
    int destIndex = sgif.sectorLst().getSectorIndex(y, x);
    moveStellarObject(stellarId, destIndex);
  }

  // move stellar object to destination sector index
  // switches any existing systems and stellar objects
  public void moveStellarObject(short stellarId, int destIndex) throws KeyNotFoundException, IOException {
    MapTools mapTools = sgif.mapTools();
    Sector sector = sgif.sectorLst();
    short destObj = sector.getStellarObject(destIndex);
    short destSys = destObj < 0 ? sector.getSystem(destIndex) : -1;

    int[] pos = sgif.stellInfo().getStellarObject(stellarId).getPosition();
    int srcIndex = sector.getSectorIndex(pos[1], pos[0]);

    // move stellar object to destination sector
    mapTools.switchSectorStellarObjs(srcIndex, destIndex);
    // move target system
    mapTools.switchSectorSystems(srcIndex, destIndex);

    // update sector exploration
    mapTools.updateSectorExploration(destIndex);
    mapTools.updateSectorExploration(srcIndex);

    // update target sectors whenever a star system or stellar object is moved
    boolean updateTarget = destSys >= 0 || destObj >= 0;
    sgif.taskForceTools().changeMissionTargets(srcIndex, destIndex, true, updateTarget, false);
  }

  // update stellInfo stellar object position at given sector index
  public boolean updateStellarPosition(int sectorIndex) throws IOException {
    Sector sectors = sgif.sectorLst();

    int stellId = sectors.getStellarObject(sectorIndex);
    if (stellId >= 0) {
      int[] pos = sectors.getPosition(sectorIndex);
      sgif.stellInfo().getStellarObject(stellId).setPosition(pos[0], pos[1]);
      return true;
    }
    return false;
  }

}
