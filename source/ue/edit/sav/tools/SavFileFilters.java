package ue.edit.sav.tools;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Locale;
import ue.edit.sav.common.CSavFilesLC;
import ue.util.file.FileFilters;

public abstract class SavFileFilters {

  public static final FilenameFilter AgtDp = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtDp)
        && !name.endsWith(CSavFilesLC.AgtDp_Suffix);
    }
  };

  public static final FilenameFilter AgtDpCnt = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtDp)
        && name.endsWith(CSavFilesLC.AgtDp_Suffix);
    }
  };

  public static final FilenameFilter AgtSh = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtSh)
        && !name.endsWith(CSavFilesLC.AgtSh_Suffix);
    }
  };

  public static final FilenameFilter AgtShCnt = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtSh)
        && name.endsWith(CSavFilesLC.AgtSh_Suffix);
    }
  };

  public static final FilenameFilter AgtSy = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtSy)
        && !name.endsWith(CSavFilesLC.AgtSy_Suffix);
    }
  };

  public static final FilenameFilter AgtSyCnt = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtSy)
        && name.endsWith(CSavFilesLC.AgtSy_Suffix);
    }
  };

  public static final FilenameFilter AgtTk = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtTk)
        && !name.endsWith(CSavFilesLC.AgtTk_Suffix);
    }
  };

  public static final FilenameFilter AgtTkCnt = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AgtTk)
        && name.endsWith(CSavFilesLC.AgtTk_Suffix);
    }
  };

  public static final FilenameFilter AIAgent = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AIAgent)
        && !name.equals(CSavFilesLC.AIAgtIdCtr);
    }
  };

  public static final FilenameFilter AITask = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.AITask)
        && !name.equals(CSavFilesLC.AITaskIdCtr);
    }
  };

  public static final FilenameFilter AlienTF = FileFilters.prefix(CSavFilesLC.AlienTFInfo);

  public static final FilenameFilter TskSh = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.TskSh)
        && !name.endsWith(CSavFilesLC.TskSh_Suffix);
    }
  };

  public static final FilenameFilter TskShCnt = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.TskSh)
        && name.endsWith(CSavFilesLC.TskSh_Suffix);
    }
  };

  public static final FilenameFilter TskSy = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.TskSy)
        && !name.endsWith(CSavFilesLC.TskSy_Suffix);
    }
  };

  public static final FilenameFilter TskSyCnt = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      name = name.toLowerCase(Locale.ROOT);
      return name.startsWith(CSavFilesLC.TskSy)
        && name.endsWith(CSavFilesLC.TskSy_Suffix);
    }
  };

}
