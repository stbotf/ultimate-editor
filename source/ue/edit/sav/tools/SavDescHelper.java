package ue.edit.sav.tools;

import java.util.Optional;
import lombok.val;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.sav.SavGameInterface;
import ue.service.Language;
import ue.edit.sav.SavGame;

/**
 * Description helper to simplify name lookup by id.
 */
public class SavDescHelper {

  static final String strUnknown = "???";

  private SavGameInterface sgif;
  private Optional<StbofFileInterface> stif;

  public SavDescHelper(SavGame sav, Stbof stbof) {
    sgif = sav.files();
    stif = stbof != null ? Optional.of(stbof.files()) : Optional.empty();
  }

  public String strcName(int buildingId, boolean withId) {
    if (stif.isPresent()) {
      val edifice = stif.get().findEdifice();
      if (edifice.isPresent()) {
        try {
          String bldName = edifice.get().getEntry(buildingId).getName();
          return withId ? "[" + Integer.toString(buildingId) + "] " + bldName : bldName;
        } catch (Exception e) {
          return "<" + strMissing() + "> " + strStrc() + " " + Integer.toString(buildingId);
        }
      }
    }

    return strStrc() + " " + Integer.toString(buildingId);
  }

  public String systemName(int starId, boolean withId) {
    val systInfo = sgif.findSystInfo();
    if (systInfo.isPresent()) {
      try {
        String sysName = systInfo.get().getSystem(starId).getName();
        return withId ? "[" + Integer.toString(starId) + "] " + sysName : sysName;
      } catch (Exception e) {
        return "<" + strMissing() + "> " + strSystem() + " " + Integer.toString(starId);
      }
    }

    return strSystem() + " " + Integer.toString(starId);
  }

  public String systemRace(int starId, boolean withId) {
    if (!stif.isPresent())
      return strUnknown;

    val systInfo = sgif.findSystInfo();
    if (!systInfo.isPresent())
      return strUnknown;

    try {
      int raceId = systInfo.get().getSystem(starId).getControllingRace();
      val raceRst = stif.get().findRaceRst();
      if (!raceRst.isPresent())
        return "race '" + Integer.toString(raceId) + "'";

      String raceName = raceRst.get().getName(raceId);
      return withId ? "[" + Integer.toString(raceId) + "] " + raceName : raceName;
    } catch (Exception e) {
      return strUnknown;
    }
  }

  private String strMissing() {
    return Language.getString("SavDescHelper.1");
  }

  private String strSystem() {
    return Language.getString("SavDescHelper.2");
  }

  private String strStrc() {
    return Language.getString("SavDescHelper.3");
  }
}
