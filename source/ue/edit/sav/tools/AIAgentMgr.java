package ue.edit.sav.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.sav.SavGame;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.ai.AIAgent;
import ue.edit.sav.files.ai.AIAgtIdCtr;
import ue.edit.sav.files.ai.AINumAgents;
import ue.edit.sav.files.ai.AgtDp;
import ue.gui.util.dialog.Dialogs;
import ue.util.data.CollectionTools;

public class AIAgentMgr {

  static final String ClassName = AIAgentMgr.class.getSimpleName();
  static final String CntSuffix = ".cnt";

  // map AITasks by id
  HashMap<Integer, AIAgent> agentMap = new HashMap<>();
  HashMap<Integer, AIAgent> raceMap = new HashMap<>();
  SavGame game;
  AIAgtIdCtr agentCtr;
  AINumAgents numAgents;

  public AIAgentMgr(SavGame game) {
    this.game = game;
    agentCtr = (AIAgtIdCtr) game.tryGetInternalFile(CSavFiles.AIAgtIdCtr, true);
    numAgents = (AINumAgents) game.tryGetInternalFile(CSavFiles.AINumAgents, true);
    reload();
  }

  public void reload() {
    agentMap.clear();
    raceMap.clear();

    val entries = game.getFileNames(SavFileFilters.AIAgent);
    ArrayList<String> errors = new ArrayList<String>();

    // load AIAgents
    for (String entry : entries) {
      try {
        AIAgent agent = (AIAgent) game.getInternalFile(entry, true);
        agentMap.put(agent.getId(), agent);
        raceMap.put(agent.getRace(), agent);
      } catch (Exception e) {
        errors.add(ClassName + ": Failed to load agent " + entry + "!");
      }
    }

    if (!errors.isEmpty()) {
      String err = String.join("\n", errors);
      Dialogs.displayError(ClassName + " load error", err);
    }
  }

  public int getNumAgents() {
    return agentMap.size();
  }

  // called for AIAgtIdCtr correction
  public int getMaxAgentId() {
    return agentMap.isEmpty() ? -1 : Collections.max(agentMap.keySet());
  }

  public AIAgent getAgent(int agentId) {
    return agentMap.get(agentId);
  }

  public boolean hasRaceAgent(int raceId) {
    return raceMap.containsKey(raceId);
  }

  public AIAgent getRaceAgent(int raceId) {
    return raceMap.get(raceId);
  }

  public Optional<AIAgent> removeRaceAgent(int raceId) throws IOException {
    AIAgent agent = raceMap.remove(raceId);
    if (agent != null) {
      int remId = agent.getId();

      // remove AIAgent file
      game.removeInternalFile(agent.getName());
      agentMap.remove(remId);

      // remove all the agent tasks and update successive task agent ids
      game.files().aiTaskMgr().onRemovedAgent(remId);

      // remove AgtDp, AgtSh, AgtSy and AgtTk files
      removeSubAgents(remId);

      // update other agent AgtDp relations
      removeRaceRelations((short)raceId);

      // update successive agent files names
      decSuccessiveAgentNames(agent.getFileIndex());
      decSuccessiveSubAgentNames(remId);

      // reduce the agent number
      numAgents.decNumAgents();

      // update the AIAgtIdCtr
      agentCtr.decAgentCounter();
    }

    return Optional.ofNullable(agent);
  }

  public void removeMonsters() throws IOException {
    List<Integer> monsters = raceMap.keySet().stream()
      .filter(x -> x >= Race.Monster)
      .collect(Collectors.toList());
    for (int raceId : monsters)
      removeRaceAgent(raceId);
  }

  private void removeRaceRelations(short raceId) {
    for (int agentId : agentMap.keySet()) {
      AgtDp agtDp = (AgtDp) game.tryGetInternalFile(CSavFiles.AgtDp + agentId, true);
      if (agtDp != null)
        agtDp.removeDiplomaticRelation(raceId);
    }
  }

  private void removeSubAgents(int agentId) {
    // remove sub agent files
    remSubFile(CSavFiles.AgtDp + agentId);
    remSubFile(CSavFiles.AgtSh + agentId);
    remSubFile(CSavFiles.AgtSy + agentId);
    remSubFile(CSavFiles.AgtTk + agentId);
  }

  private void decSuccessiveAgentNames(int remFileIdx) throws IOException {
    // collect agents sorted by entry number, so no changes are overridden
    val entryUpdates = agentMap.values().stream().filter(x -> x.getFileIndex() > remFileIdx)
      .collect(Collectors.toMap(AIAgent::getFileIndex, Function.identity(),
        CollectionTools.throwingMerger(), TreeMap<Integer, AIAgent>::new));

    // proceed to update successive agent file names
    for (AIAgent agt : entryUpdates.values()) {
      game.removeInternalFile(agt.getName());
      agt.setName(CSavFiles.AIAgent + (agt.getFileIndex() - 1));
      game.addInternalFile(agt);
    }
  }

  private void decSuccessiveSubAgentNames(int agentId) throws IOException {
    // collect and sort successive agent ids, so no changes are overridden
    TreeSet<Integer> idUpdates = agentMap.keySet().stream().filter(x -> x > agentId)
      .collect(Collectors.toCollection(TreeSet<Integer>::new));

    // proceed to update successive agent ids
    for (int agtId : idUpdates) {
      AIAgent agt = agentMap.remove(agtId);
      agt.setId(agtId - 1);
      agentMap.put(agtId - 1, agt);

      // update sub agent files
      decSubAgtId(CSavFiles.AgtDp, agtId);
      decSubAgtId(CSavFiles.AgtSh, agtId);
      decSubAgtId(CSavFiles.AgtSy, agtId);
      decSubAgtId(CSavFiles.AgtTk, agtId);
    }
  }

  private void remSubFile(String fileName) {
    game.removeInternalFile(fileName);
    game.removeInternalFile(fileName + CntSuffix);
  }

  private void decSubAgtId(String agtPrefix, int agtId) throws IOException {
    String agtName = agtPrefix + agtId;
    String renamed = agtPrefix + (agtId - 1);
    game.renameInternalFile(agtName, renamed);
    game.renameInternalFile(agtName + CntSuffix, renamed + CntSuffix);
  }

}
