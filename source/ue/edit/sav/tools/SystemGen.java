package ue.edit.sav.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import lombok.AllArgsConstructor;
import lombok.val;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.PlanetAni;
import ue.edit.res.stbof.common.CStbof.PlanetAtmosphere;
import ue.edit.res.stbof.common.CStbof.PlanetType;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.pst.data.PlanetEntry;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.util.calc.Dice;
import ue.util.calc.Numeral;

/**
 * System generator.
 */
public class SystemGen {

  private static int MAX_PLANETS = CStbof.MAX_PLANETS;
  private static float DEFAULT_DIL_FREQ = 0.1f;

  public interface SysGenOption {
    public static final int Empty = 0;
    public static final int Dreadful = 1;
    public static final int Miserable = 2;
    public static final int Inhabitable = 3;
    public static final int Peculiar = 4;
    public static final int Acceptable = 5;
    public static final int Average = 6;
    public static final int Productive = 7;
    public static final int Remarkable = 8;
    public static final int EnergyHub = 9;
    public static final int Superior = 10;
    public static final int Inconceivable = 11;
    public static final int Random = 12;
  }

  // system generation population target scales
  // 1.0 means max possible system population, which is
  // a rather theoretic value that lacks any variance
  private static final float[] SysGenPopScale = new float[] {
    0f,     // Empty
    0.04f,  // Dreadful
    0.08f,  // Miserable
    0.14f,  // Inhabitable
    0.22f,  // Peculiar
    0.32f,  // Acceptable
    0.45f,  // Average
    0.60f,  // Productive
    0.75f,  // Remarkable
    0.75f,  // EnergyHub
    0.82f,  // Superior
    0.90f,  // Inconceivable
    -1f     // Random
  };

  private static final float[] SysGenBonusScale = new float[] {
    0f,     // Empty
    0.04f,  // Dreadful
    0.08f,  // Miserable
    0.15f,  // Inhabitable
    0.50f,  // Peculiar
    0.30f,  // Acceptable
    0.40f,  // Average
    0.50f,  // Productive
    0.60f,  // Remarkable
    0.75f,  // EnergyHub
    0.80f,  // Superior
    0.90f,  // Inconceivable
    -1f     // Random
  };

  @AllArgsConstructor
  private class PlanTypeEntry {
    int planetType;
    short planetSize;
    int population;
    float freq;
  }

  // to not keep outdated file references, lookup all save game files on demand
  private SavGameInterface sgif;
  private StbofFileInterface stif;
  // environ.est lists planet frequencies
  private Environ environ;
  // planspac.bin maps planet type & size to max population values
  private Planspac planSpac;

  // determined stbof.res planet type populations
  private ArrayList<PlanTypeEntry> popSortedPlanetTypes = new ArrayList<>();

  private int numPlanetTypes;
  private int maxPlanetPopulation = 0;
  private float dilFreq = DEFAULT_DIL_FREQ;
  private float engyFreqFac = 0.0f;
  private float grwtFreqFac = 0.0f;

  public SystemGen(SavGame sav, Stbof stbof) throws IOException {
    this.sgif = sav.files();
    this.stif = stbof.files();
    this.environ = stif.environ();
    this.planSpac = stif.planSpac();
    updatePlanetList();
  }

  private void updatePlanetList() {
    numPlanetTypes = environ.numPlanetTypes();
    float freqSum = environ.getFreqSum();
    float freqFac = 1.0f / (freqSum * 3.0f);
    float sumEngyFreq = 0.0f;

    for (int type = 0; type < numPlanetTypes; ++type) {
      float planFreq = environ.getFrequency(type);
      if (PlanetType.isEnergyType(type))
        sumEngyFreq += planFreq;

      float sizeFreq = freqFac * planFreq;
      for (short size = 0; size < 3; ++size) {
        int pop = planSpac.get(type, size, 2, 2);
        popSortedPlanetTypes.add(new PlanTypeEntry(type, size, pop, sizeFreq));
        maxPlanetPopulation = Integer.max(maxPlanetPopulation, pop);
      }
    }

    // compute energy & growth normalization factors
    engyFreqFac = 1.0f / sumEngyFreq;
    grwtFreqFac = 1.0f / (1.0f - sumEngyFreq);

    // sort planet type entries by population
    popSortedPlanetTypes.sort(Comparator.comparing(x -> x.population));
  }

  // check whether stbof got changed - which however
  // should never be the case during save game editing
  private void checkUpdatePlanetList() throws IOException {
    val env = stif.environ();
    val spac = stif.planSpac();
    if (env != this.environ || spac != planSpac) {
      this.environ = env;
      this.planSpac = spac;
      updatePlanetList();
    }
  }

  public SystemEntry genSystem(int sectorIndex, int option) throws IOException {
    // to not keep outdated file references, lookup files on demand
    val sectorLst = sgif.sectorLst();
    val systInfo = sgif.systInfo();

    // add empty system
    int[] pos = sectorLst.getPosition(sectorIndex);
    SystemEntry system = systInfo.addEmptySystem(pos[0], pos[1]);
    system.setDilithium(Math.random() < dilFreq);
    system.setName(genSystemName());

    float popScale = SysGenPopScale[option];

    // skip empty systems, but generate random planets if negative
    if (popScale != 0.0f) {
      int popTarget = (int) (maxPlanetPopulation * 5 * Dice.rand(popScale, 0.02f + 0.5f * popScale));
      int maxPopulation = 0;

      // fill planets till estimation is met or max num planets is reached
      for (int i = 0; i < MAX_PLANETS && maxPopulation < popTarget; ++i) {
        val planet = genPlanet(system, option);
        maxPopulation += planSpac.get(planet.getPlanetType(), planet.getPlanetSize(),
          planet.getEnergyBonusLvl(), planet.getGrowthBonusLvl());
      }
    }

    return system;
  }

  public PlanetEntry genPlanet(SystemEntry system, int option) throws IOException {
    checkUpdatePlanetList();

    // further related files:
    // Planet (planet.pst) lists pre-defined planet definitions
    // Planboni (planboni.bin) maps planet type & size to food & energy bonuses

    float popScale = SysGenPopScale[option];
    float bonScale = SysGenBonusScale[option];

    // dice planet type and size by max population size, selected by planet fequency
    // reduce spread on misarable and inconceivable systems
    float typeVariance = 1.0f - Math.abs(0.5f - popScale);
    double typeScale = Dice.rand(popScale, typeVariance);
    PlanTypeEntry planType = null;

    for (val pte : popSortedPlanetTypes) {
      typeScale -= option == SysGenOption.EnergyHub ? PlanetType.isEnergyType(pte.planetType) ?
        pte.freq * 0.9f * engyFreqFac : pte.freq * 0.1f * grwtFreqFac : pte.freq;

      if (typeScale <= 0) {
        planType = pte;
        break;
      }
    }

    PlanetEntry planet;
    if (planType != null) {
      int atmo = genAtmo(planType.planetType);

      // dice planet energy bonus by bonus scale
      double engyScale = Dice.rand(bonScale, 0.5 - 0.5 * bonScale);
      short engyBonus = (short) (engyScale * 3);

      // dice planet growth bonus by population scale
      double growthScale = Dice.rand(popScale, 0.5f - 0.5 * popScale);
      short growthBonus = (short) (growthScale * 3);

      planet = system.addPlanet(planType.planetType, planType.planetSize, atmo, engyBonus, growthBonus);
      planet.setAnimation(PlanetAni.map(planType.planetType, planType.planetSize, atmo) + ".ani");
    } else {
      planet = system.addPlanet();
    }

    planet.setTerraformPoints(genTerraformPoints(planet.getPlanetType(), planet.getPlanetSize()));
    planet.setName(system.getName() + " " + Numeral.roman(system.getNumPlanets()));
    return planet;
  }

  private String genSystemName() throws IOException {
    // starname.bin lists known generic star names to use
    val starName = stif.starName();

    HashSet<String> usedSystemNames = new HashSet<>(Arrays.asList(sgif.systInfo().getNames()));
    int count = starName.count();
    int nameIdx = (int) (Math.random() * count);
    String systemName = starName.get(nameIdx);
    if (usedSystemNames.contains(systemName)) {
      // when more system names are in the database, keep searching
      if (count > usedSystemNames.size()) {
        boolean found = false;

        // try to randomly find an unused system name
        for (int i = 0; i < 3; ++i) {
          nameIdx = (int) (Math.random() * count);
          systemName = starName.get(nameIdx);
          if (!usedSystemNames.contains(systemName)) {
            found = true;
            break;
          }
        }

        // fallback to sequentially find next unused system name
        if (!found) {
          for (int i = 0; i < count; ++i) {
            systemName = starName.get(nameIdx);
            if (!usedSystemNames.contains(systemName))
              break;
          }
        }
      } else {
        // fallback to number the system
        int nbr = 1;
        String sysNbrName;
        do {
          sysNbrName = systemName + " " + Numeral.roman(++nbr);
        } while (usedSystemNames.contains(sysNbrName));

        systemName = sysNbrName;
      }
    }

    return systemName;
  }

  private int genAtmo(int planetType) {
    switch (planetType) {
      case PlanetType.GasGiant:
        return PlanetAtmosphere.Methane;
      case PlanetType.Jungle:
      case PlanetType.Terran:
        return PlanetAtmosphere.OxygenRich;
      case PlanetType.Arctic:
      case PlanetType.Barren:
      case PlanetType.Desert:
      case PlanetType.Oceanic: {
        double r = Math.random();
        return r >= 0.5 ? PlanetAtmosphere.OxygenRich : PlanetAtmosphere.OxygenThin;
      }
      case PlanetType.Volcanic: {
        double r = Math.random();
        return r >= 0.666666 ? PlanetAtmosphere.Sulfuric : r >= 0.333333 ?
          PlanetAtmosphere.OxygenThin : PlanetAtmosphere.OxygenRich;
      }
      default:
        return PlanetAtmosphere.None;
    }
  }

  private int genTerraformPoints(int planetType, int planetSize) {
    switch (planetType) {
      case PlanetType.GasGiant:
      case PlanetType.Terran:
        return 0;
      case PlanetType.Arctic:
        switch (planetSize) {
          case 0:
            // observed: 175, 195, 210, 227, 245
            return Dice.dice(175, 245);
          case 1:
            // observed: 270, 292, 325, 350, 385
            return Dice.dice(270, 385);
          case 2:
            // observed: 520, 552, 560, 585, 595, 617, 630, 665
            return Dice.dice(520, 665);
        }
      case PlanetType.Barren:
        switch (planetSize) {
          case 0:
            // observed: 135, 180, 220, 225, 275
            return Dice.dice(135, 275);
          case 1:
            // observed: 220, 225, 270, 275, 315, 330, 385
            return Dice.dice(220, 385);
          case 2:
            // observed: 585, 630, 660, 715, 770
            return Dice.dice(585 , 770);
        }
      case PlanetType.Desert:
        switch (planetSize) {
          case 0:
            // observed: 110, 137, 165
            return Dice.dice(110, 165);
          case 1:
            // observed: 220, 240, 247, 300
            return Dice.dice(220, 300);
          case 2:
            // observed: 440, 450, 510
            return Dice.dice(440, 510);
        }
      case PlanetType.Jungle:
        switch (planetSize) {
          case 0:
            return Dice.dice(120, 235);
          case 1:
            // observed: 315
            return Dice.dice(230, 345);
          case 2:
            return Dice.dice(535, 650);
        }
      case PlanetType.Oceanic:
        switch (planetSize) {
          case 0: {
            // observed: 0, 100, 120, 135
            int tp = Dice.dice(80, 135);
            return tp < 100 ? 0 : tp;
          }
          case 1: {
            // observed: 0, 150, 180, 195, 237
            int tp = Dice.dice(100, 237);
            return tp < 150 ? 0 : tp;
          }
          case 2: {
            // observed: 0
            int tp = Dice.dice(250, 450);
            return tp < 320 ? 0 : tp;
          }
        }
      case PlanetType.Volcanic:
        switch (planetSize) {
          case 0:
            // observed: 165, 198, 220, 262, 265, 275
            return Dice.dice(165, 275);
          case 1:
            // observed: 315, 330, 367, 420
            return Dice.dice(315, 420);
          case 2:
            // observed: 735, 787, 861, 927
            return Dice.dice(735, 927);
        }
    }

    return 0;
  }

}
