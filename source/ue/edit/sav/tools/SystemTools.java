package ue.edit.sav.tools;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.val;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.sav.SavGame;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.common.CSavFiles;
import ue.edit.sav.files.ai.AgtDp;
import ue.edit.sav.files.ai.AgtSy;
import ue.edit.sav.files.emp.AlienInfo;
import ue.edit.sav.files.emp.ProvInfo;
import ue.edit.sav.files.map.SystInfo;
import ue.edit.sav.files.map.TskSy;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.edit.sav.files.map.data.TradeId;
import ue.edit.sav.task.RemoveSystemsTask;
import ue.exception.KeyNotFoundException;
import ue.gui.util.GuiTools;

/**
 * Contains methods for system edit operations in a saved game.
 */
public class SystemTools {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private SavGame sav;
  private SavGameInterface sgif;

  public SystemTools(SavGame sav) {
    this.sav = sav;
    sgif = sav.files();
  }

  public SystemEntry addSystem(int sectorIndex, int option) throws IOException {
    SystInfo systInfo = sgif.systInfo();

    // generate system
    val system = sgif.systemGen().genSystem(sectorIndex, option);
    int systemId = system.index();

    // update sector.lst
    sgif.sectorLst().setSystem(sectorIndex, (short)systemId);

    // update number of stars
    int numSystems = systInfo.getNumberOfEntries();
    sgif.galInfo().setSystemCount((short)numSystems);

    // return, strcInfo entry is not required
    return system;
  }

  public void cancelIncomingTradeRoutes(short systemId) throws IOException {
    SystInfo systInfo = sgif.systInfo();
    val system = systInfo.getSystem(systemId);
    if (system != null)
      cancelIncomingTradeRoutes(system);
  }

  public void cancelIncomingTradeRoutes(@NonNull SystemEntry system) throws IOException {
    // remove incoming trade routes from systInfo system entry
    List<TradeId> routes = system.removeIncomingTradeRoutes();
    for (TradeId route : routes) {
      // cancelled trade routes keep being listed for the source system,
      // however the target system needs to be unset
      val trInfo = sgif.tradeInfo(route.race());
      trInfo.cancelTradeRoute(route);
    }
  }

  public void cancelOutgoingTradeRoutes(short systemId) throws IOException {
    SystInfo systInfo = sgif.systInfo();
    val system = systInfo.getSystem(systemId);
    if (system != null)
      cancelOutgoingTradeRoutes(system);
  }

  public void cancelOutgoingTradeRoutes(@NonNull SystemEntry system) throws IOException {
    // only empires establish outgoing trade routes
    int raceId = system.getControllingRace();
    if (raceId < NUM_EMPIRES) {
      SystInfo systInfo = sgif.systInfo();
      List<TradeId> routes = system.listOutgoingTradeRoutes();

      for (TradeId route : routes) {
        // cancelled trade routes keep being listed for the source system,
        // however the target system needs to be unset
        val trInfo = sgif.tradeInfo(route.race());
        int targetId = trInfo.cancelTradeRoute(route);

        // remove incoming route for the target system
        if (targetId != -1) {
          val target = systInfo.getSystem(targetId);
          target.removeIncomingTradeRoute(route);
        }
      }
    }
  }

  public void cancelSystemTradeRoutes(short systemId) throws IOException {
    cancelIncomingTradeRoutes(systemId);
    cancelOutgoingTradeRoutes(systemId);
  }

  public void cancelSystemTradeRoutes(SystemEntry system) throws IOException {
    cancelIncomingTradeRoutes(system);
    cancelOutgoingTradeRoutes(system);
  }

  public void cancelAllTradeRoutes() throws IOException {
    for (val trdeInfo : sgif.tradeInfos())
      trdeInfo.cancelAllTradeRoutes();
  }

  public void removeSystemTradeRoutes(SystemEntry system) throws IOException {
    int raceId = system.getControllingRace();
    if (raceId != Race.None) {
      // update systInfo & trdeInfo
      cancelSystemTradeRoutes(system);

      // remove local system trade routes
      List<TradeId> removed = system.removeLocalTradeRoutes();

      // update empire infos
      if (raceId >= 0 && raceId < NUM_EMPIRES) {
        // remove from trdeInfo
        val trInfo = sgif.tradeInfo(raceId);
        trInfo.removeTradeRoutes(removed);

        // reduce total empire trade route count
        // this doesn't seem to be much relevant though
        sgif.empsInfo().removeEmpireTradeRoutes(raceId, removed.size());

        /**
         * For some reason the trade route ids must not be changed,
         * else the game crashes on load or when processing the turn.
         * Looks like there is some special rule for the numbers, or a hidden reference
         * in systInfo, given whole systems can be reset and removed without issue.
         * Note that even vanilla games sometimes have gaps for the numbers.
         */
        // // update successive trade route numbers of other systems
        // SystInfo systInfo = sgif.systInfo();
        // val tidUpdates = trInfo.updateTradeIds();
        // SystemEntry srcSys = null;

        // for (val tidUpdate : tidUpdates) {
        //   TradeRoute route = tidUpdate.getRoute();
        //   // update source system
        //   short srcId = route.getSourceSystemId();
        //   if (srcSys == null || srcSys.index() != srcId)
        //     srcSys = systInfo.getSystem(srcId);
        //   srcSys.updateTradeRouteId(tidUpdate.id(), route.id());
        //   // update target system
        //   short tgtId = route.getSourceSystemId();
        //   if (tgtId != -1) {
        //     SystemEntry tgtSys = systInfo.getSystem(tgtId);
        //     tgtSys.updateTradeRouteId(tidUpdate.id(), route.id());
        //   }
        // }
      }
    }
  }

  public void eliminateSector(int sectorIndex) throws KeyNotFoundException, IOException {
    short systemId = sgif.sectorLst().getSystem(sectorIndex);
    if (systemId >= 0)
      eliminateSystem(systemId);
  }

  public void eliminateSystem(short systemId) throws KeyNotFoundException, IOException {
    SystInfo systInfo = sgif.systInfo();
    val system = systInfo.getSystem(systemId);

    if (system != null) {
      int sectorIdx = system.getSectorIndex();

      // update systInfo, trdeInfo and empsInfo
      removeSystemTradeRoutes(system);

      // update strcInfo
      sgif.strcInfo().removeSystemEntries(systemId);

      // update AgtSy
      val agtSyFileNames = sav.getFileNames(SavFileFilters.AgtSy);
      val agtSyFiles = sav.getInternalFiles(agtSyFileNames, true, Optional.empty());
      for (val file : agtSyFiles) {
        ((AgtSy)file).removeSystem(systemId);
      }

      // update AITask, AgtTk, TskSh and TskSy files
      // remove all sector tasks, including system bound ones
      // like colonize, invade, crew training and repair tasks
      // but also task force movement is better be reset for new AI planning
      sgif.aiTaskTools().removeSectorTasks(sectorIdx);

      // update AISysUnt
      sgif.aiSysUnt().removeSystemUnt(systemId);

      // update ordInfo system orders
      sgif.ordInfo().removeSystemOrders(systemId);

      // update resultLst system events
      sgif.resultList().removeSystemResults(systemId);

      // cancel target sector system missions
      sgif.taskForceTools().cancelSectorSystemMissions(sectorIdx);

      // remove province and check for home system elimination
      removeEmpireProvince(system);

      // unset system control and reset population & assignement
      system.eliminate();
    }
  }

  public void removeFromSector(int sectorIndex) throws KeyNotFoundException, IOException {
    short systemId = sgif.sectorLst().getSystem(sectorIndex);
    if (systemId >= 0)
      removeSystem(systemId);
  }

  public boolean removeFromSectorAsync(int sectorIndex) throws IOException {
    short systemId = sgif.sectorLst().getSystem(sectorIndex);
    return systemId < 0 || removeSystemAsync(systemId);
  }

  public void removeSystem(short systemId) throws KeyNotFoundException, IOException {
    SystInfo systInfo = sgif.systInfo();
    val system = systInfo.getSystem(systemId);

    if (system != null) {
      // update ShpUnt, SysUnt, AgtTk and remove TskSh & TskSy files
      // - remove all sector tasks, including system bound ones
      //   like colonize, invade, crew training and repair tasks
      // - plus reset AI planning for task force movement
      int sectorIdx = system.getSectorIndex();
      sgif.aiTaskTools().removeSectorTasks(sectorIdx);

      // update AITasks
      sgif.aiTaskMgr().onRemovedSystem(systemId);

      // update TskSy files
      val tskSyFileNames = sav.getFileNames(SavFileFilters.TskSy);
      val tskSyFiles = sav.getInternalFiles(tskSyFileNames, true, Optional.empty());
      for (val file : tskSyFiles) {
        ((TskSy)file).onRemovedSystem(systemId);
      }

      // update sector.lst
      // to not break system lookup, the sector tasks must be removed first
      sgif.sectorLst().onRemovedSystem(systemId);

      // update systInfo, trdeInfo and empsInfo
      // the system ids here must still be valid to allow
      // update incoming trade route ids of other systems
      removeSystemTradeRoutes(system);

      // update trdeInfo system IDs
      // no trade route may be removed before having cancelled the routes,
      // since they are needed for target system lookup
      for (val trdeInfo : sgif.tradeInfos())
        trdeInfo.onRemovedSystem(systemId);

      // update strcInfo
      sgif.strcInfo().onRemovedSystem(systemId);

      // update AgtSy
      val agtSyFileNames = sav.getFileNames(SavFileFilters.AgtSy);
      val agtSyFiles = sav.getInternalFiles(agtSyFileNames, true, Optional.empty());
      for (val file : agtSyFiles) {
        ((AgtSy)file).onRemovedSystem(systemId);
      }

      // update AISysUnt
      sgif.aiSysUnt().onRemovedSystem(systemId);

      // update ordInfo system orders
      sgif.ordInfo().onRemovedSystem(systemId);

      // update resultLst system events
      sgif.resultList().onRemovedSystem(systemId);

      // cancel target sector system missions
      sgif.taskForceTools().cancelSectorSystemMissions(sectorIdx);

      // remove province and check home system elimination ahead of updating
      // the alienInfo & provInfo system IDs, so former ids remain valid
      // for switching the home system
      removeEmpireProvince(system);

      // update empire + minor race home system ids
      sgif.alienInfo().onRemovedSystem(systemId);

      // update provInfo0 to provInfo4
      for (val provInfo : sgif.provInfos())
        provInfo.onRemovedSystem(systemId);

      // get home system race or -1
      sgif.rToS().onRemovedSystem(systemId);

      // for recursive data lookup, keep the system ids valid till all are updated
      // this is relevant for:
      // - the trade routes, which refer other systems to update
      // - sector task removal, which looks up the sector system
      systInfo.removeSystem(systemId);

      // update number of stars
      int numSystems = systInfo.getNumberOfEntries();
      sgif.galInfo().setSystemCount((short)numSystems);
    }
  }

  public boolean removeSystemAsync(short systemId) {
    // remove system
    val task = new RemoveSystemsTask(sav, Collections.singletonList(systemId));
    GuiTools.runUEWorker(task);
    return !task.isCancelled();
  }

  public boolean removeSystemsAsync(Collection<Short> systemIds) {
    // remove systems
    val task = new RemoveSystemsTask(sav, systemIds);
    GuiTools.runUEWorker(task);
    return !task.isCancelled();
  }

  public boolean removeAllUninhabitedSystems() throws IOException {
    // first collect all the system ids
    val systems = sgif.systInfo().listSystems();
    List<Short> toRemove = systems.stream()
      .filter(SystemEntry::isUninhabited)
      .map(x -> (short)x.index())
      .collect(Collectors.toList());

    // remove systems
    val task = new RemoveSystemsTask(sav, toRemove);
    GuiTools.runUEWorker(task);
    return !task.isCancelled();
  }

  public boolean removeAllMinorRaceSystems() throws IOException {
    // first collect all the system ids
    val systems = sgif.systInfo().listSystems();
    List<Short> toRemove = systems.stream()
      .filter(x -> x.isInhabited() && x.getControllingRace() >= NUM_EMPIRES)
      .map(x -> (short)x.index())
      .collect(Collectors.toList());

    // remove systems
    val task = new RemoveSystemsTask(sav, toRemove);
    GuiTools.runUEWorker(task);
    return !task.isCancelled();
  }

  public boolean removeAllEmpireProvinces() throws IOException {
    AlienInfo alienInfo = sgif.alienInfo();

    // first collect all the system ids
    val systems = sgif.systInfo().listSystems();
    List<Short> toRemove = systems.stream()
      .filter(x -> x.isInhabited() && x.getControllingRace() < NUM_EMPIRES
        && alienInfo.getHomeSystem(x.getControllingRace()) != x.index())
      .map(x -> (short)x.index())
      .collect(Collectors.toList());

    // remove systems
    val task = new RemoveSystemsTask(sav, toRemove);
    GuiTools.runUEWorker(task);
    return !task.isCancelled();
  }

  public boolean removeAllEmpireHomeSystems() throws IOException {
    AlienInfo alienInfo = sgif.alienInfo();

    // first collect all the system ids
    val systems = sgif.systInfo().listSystems();
    List<Short> toRemove = systems.stream()
      .filter(x -> x.isInhabited() && x.getControllingRace() < NUM_EMPIRES
        && alienInfo.getHomeSystem(x.getControllingRace()) == x.index())
      .map(x -> (short)x.index())
      .collect(Collectors.toList());

    // remove systems
    val task = new RemoveSystemsTask(sav, toRemove);
    GuiTools.runUEWorker(task);
    return !task.isCancelled();
  }

  public void removePlanet(short systemId, short planetId) throws KeyNotFoundException, IOException {
    SystInfo systInfo = sgif.systInfo();
    SystemEntry system = systInfo.getSystem(systemId);
    boolean wasInhabited = system.isInhabited();
    val planet = system.removePlanet(planetId);

    if (planet != null && wasInhabited && system.isUninhabited())
      eliminateSystem(systemId);
  }

  private void removeEmpireProvince(SystemEntry system) throws IOException {
    if (system.isUninhabited())
      return;

    short systemId = (short) system.index();

    // remove from provInfo0 to provInfo4
    // so it is not considered for home system replacement
    for (val provInfo : sgif.provInfos())
      provInfo.removeProvince(systemId);

    // check to update resident race home systems
    short sysRace = system.getResidentRace();
    if (sysRace >= 0 && sgif.alienInfo().getHomeSystem(sysRace) == systemId)
      eliminateHomeSystem(sysRace, systemId);

    // also check to update contolling race home systems,
    // even though that might be redundant
    int ctrlRace = system.getControllingRace();
    if (ctrlRace >= 0 && ctrlRace != sysRace && sgif.alienInfo().getHomeSystem(ctrlRace) == systemId)
      eliminateHomeSystem((short) ctrlRace, systemId);
  }

  private void eliminateHomeSystem(short raceId, int systemId) throws IOException {
    if (raceId < CStbof.NUM_EMPIRES) {
      ProvInfo provInfo = sgif.provInfo(raceId);

      if (provInfo.numProvinces() > 0) {
        short nextSysId = provInfo.firstProvince();

        String msg = "Warning, updating empire home system of race %1 from id %2 to the new system id %3!!"
          .replace("%1", Integer.toString(raceId))
          .replace("%2", Integer.toString(systemId))
          .replace("%3", Integer.toString(nextSysId));
        System.out.println(msg);

        sgif.alienInfo().setHomeSystem(raceId, nextSysId);
        sgif.rToS().setHomeSystem(raceId, nextSysId);
        return;
      }
    }

    String msg = "Warning, eliminating the last system %1 of race %2!!"
      .replace("%1", Integer.toString(systemId))
      .replace("%2", Integer.toString(raceId));
    System.out.println(msg);

    // for minor races, remove all diplomatic relations
    // from AgtDpX and update AgtDpX.cnt
    if (raceId >= NUM_EMPIRES) {
      val agtDpFileNames = sav.getFileNames(SavFileFilters.AgtDp);
      val agtDpFiles = sav.getInternalFiles(agtDpFileNames, true, Optional.empty());
      for (val file : agtDpFiles) {
        AgtDp agtDp = (AgtDp) file;
        agtDp.removeDiplomaticRelation(raceId);
      }
    }

    // remove all race related treaties
    sgif.treaty().removeRaceTreaties(raceId);

    // remove all race related orders,
    // including military orders, gift offers, diplomatic treaties and demands
    sgif.ordInfo().removeRaceOrders(raceId);

    // remove all race related event results,
    // including military results and intel results
    sgif.resultList().removeRaceResults(raceId);

    // remove all remaining race related AI tasks
    // which specially includes all related diplomatic tasks
    sgif.aiTaskMgr().removeRaceTasks(raceId);

    // remove task forces
    // for minors it otherwise crashes without a log during turn processing
    sgif.taskForceTools().removeRaceTaskForces(raceId);

    // remove alienTFInfo task force mapping
    String tfInfoName = CSavFiles.AlienTFInfo + raceId;
    sav.removeInternalFile(tfInfoName);

    // kill minor race or empire
    sgif.gameInfo().eliminateRace(raceId);
    sgif.aiAgentMgr().removeRaceAgent(raceId);
    sgif.alienInfo().eliminateRace(raceId);
    sgif.rToS().clearHomeSystem(raceId);

    // Participating space faring races listed by gameInfo must not be removed from techInfo!
    // If removed, load crashes with: "File: gdllist.c, Line: 764, gdlList != NULL"
    sgif.techInfo().removeMinorRace(raceId);
  }

}
