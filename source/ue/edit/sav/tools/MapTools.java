package ue.edit.sav.tools;

import java.io.IOException;
import ue.edit.sav.SavGameInterface;
import ue.edit.sav.SavGame;
import ue.edit.sav.files.map.Sector;
import ue.exception.KeyNotFoundException;

/**
 * Contains methods for general map edit operations in a saved game.
 */
public class MapTools {

  SavGame game;
  SavGameInterface sgif;

  public MapTools(SavGame game) {
    this.game = game;
    sgif = game.files();
  }

  public static String sectorPos(int column, int row) {
    return Integer.toString(column+1) + "." + Integer.toString(row+1);
  }

  public void switchSectors(int sectorIndex, int x, int y) throws KeyNotFoundException, IOException {
    Sector sector = sgif.sectorLst();
    int destIndex = y * sector.getHSectors() + x;

    // switch sector
    // note that when the sector is switched first, the sector indexes are exchanged
    sector.switchSectors(sectorIndex, destIndex);

    // with swapped sectors, update all moved object sector references
    sgif.starbaseTools().swapStarbasePositions(sectorIndex, destIndex, true);
    boolean targetMoved = sgif.stellarTools().updateStellarPosition(sectorIndex);
    boolean sourceMoved = sgif.stellarTools().updateStellarPosition(destIndex);
    targetMoved |= updateSystemPosition(sectorIndex);
    sourceMoved |= updateSystemPosition(destIndex);

    // move task forces
    sgif.taskForceTools().swapSectorTaskForces(sectorIndex, destIndex);

    // update target sectors whenever a star system or stellar object is moved
    sgif.taskForceTools().changeMissionTargets(sectorIndex, destIndex, sourceMoved, targetMoved, true);
  }

  // switch sector systems, not updating stellar objects, exploration and stuff
  public void switchSectorSystems(int srcIndex, int destIndex) throws IOException {
    Sector sectors = sgif.sectorLst();

    short srcId = sectors.getSystem(srcIndex);
    short destId = sectors.getSystem(destIndex);

    // update target sector
    sectors.setSystem(destIndex, srcId);
    if (srcId >= 0) {
      int[] pos = sectors.getPosition(destIndex);
      sgif.systInfo().getSystem(srcId).changeSectorCoordinates(pos[0], pos[1]);
    }

    // update source sector
    sectors.setSystem(srcIndex, destId);
    if (destId >= 0) {
      int[] pos = sectors.getPosition(srcIndex);
      sgif.systInfo().getSystem(destId).changeSectorCoordinates(pos[0], pos[1]);
    }
  }

  // switch sector stellar objects, not updating systems, exploration and stuff
  public void switchSectorStellarObjs(int srcIndex, int destIndex) throws IOException {
    Sector sectors = sgif.sectorLst();
    short srcId = sectors.getStellarObject(srcIndex);
    short destId = sectors.getStellarObject(destIndex);

    // update target sector
    sectors.setStellarObject(destIndex, srcId);
    if (srcId >= 0) {
      int[] pos = sectors.getPosition(destIndex);
      sgif.stellInfo().getStellarObject(srcId).setPosition(pos[0], pos[1]);
    }

    // update source sector
    sectors.setStellarObject(srcIndex, destId);
    if (destId >= 0) {
      int[] pos = sectors.getPosition(srcIndex);
      sgif.stellInfo().getStellarObject(destId).setPosition(pos[0], pos[1]);
    }
  }

  // move system to target map position
  // switches any existing systems and stellar objects
  public void moveSystem(short systemId, int x, int y) throws KeyNotFoundException, IOException {
    int destIndex = sgif.sectorLst().getSectorIndex(y, x);
    moveSystem(systemId, destIndex);
  }

  // move system to destination sector index
  // switches any existing systems and stellar objects
  public void moveSystem(short systemId, int destIndex) throws KeyNotFoundException, IOException {
    MapTools mapTools = sgif.mapTools();
    Sector sector = sgif.sectorLst();
    short destSys = sector.getSystem(destIndex);
    short destObj = destSys < 0 ? sector.getStellarObject(destIndex) : -1;

    int[] pos = sgif.systInfo().getSystem(systemId).getSectorCoordinates();
    int srcIndex = sector.getSectorIndex(pos[1], pos[0]);

    // move system to destination sector
    mapTools.switchSectorSystems(srcIndex, destIndex);
    // move target stellar object
    mapTools.switchSectorStellarObjs(srcIndex, destIndex);

    // update sector exploration
    mapTools.updateSectorExploration(destIndex);
    mapTools.updateSectorExploration(srcIndex);

    // update target sectors whenever a star system or stellar object is moved
    boolean updateTarget = destSys >= 0 || destObj >= 0;
    sgif.taskForceTools().changeMissionTargets(srcIndex, destIndex, true, updateTarget, false);
  }

  // update systInfo system position at given sector index
  public boolean updateSystemPosition(int sectorIndex) throws IOException {
    Sector sectors = sgif.sectorLst();

    int id = sectors.getSystem(sectorIndex);
    if (id >= 0) {
      int[] pos = sectors.getPosition(sectorIndex);
      sgif.systInfo().getSystem(id).changeSectorCoordinates(pos[0], pos[1]);
      return true;
    }
    return false;
  }

  public void updateSectorExploration(int sectorIndex) throws IOException {
    long explored = sgif.sectorLst().getExplored(sectorIndex);
    sgif.sectorTools().setSectorExploration(sectorIndex, explored);
  }

}
