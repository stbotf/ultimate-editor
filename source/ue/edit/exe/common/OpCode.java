package ue.edit.exe.common;

/**
 * operation codes
 */
public interface OpCode {

  public enum Register {
    AL, CL, DL, BL,
    AH, CH, DH, BH,
    AX, CX, DX, BX,
    SP, BP, SI, DI,
    EAX, ECX, EDX, EBX,
    ESP, EBP, ESI, EDI
  }

  // AddIntToReg
  static final byte   ADD_EAX       = 0x05;

  // JmpIf: 0xF + flags
  public interface JmpFlags {
    static final byte JO            = (byte)0x80; // jump on overflow
    static final byte JNO           = (byte)0x81; // jump on no overflow
    static final byte JB            = (byte)0x82; // jump if below:            <unsigned>
    static final byte JNB           = (byte)0x83; // jump if above or equal    (JAE) <unsigned>
    static final byte JZ            = (byte)0x84; // jump if zero / equal      (JE)  <signed/unsigned>
    static final byte JNZ           = (byte)0x85; // jump if not zero / equal  (JNE) <signed/unsigned>
    static final byte JBE           = (byte)0x86; // jump if below or equal    <unsigned>
    static final byte JA            = (byte)0x87; // jump if above             <unsigned>
    static final byte JS            = (byte)0x88; // jump if signed            (negative)
    static final byte JNS           = (byte)0x89; // jump if not signed        (positive or zero)
    static final byte JP            = (byte)0x8A; // jump if parity is even    (JPE, bit count of least significant byte)
    static final byte JNP           = (byte)0x8B; // jump if parity is odd     (JPO, bit count of least significant byte)
    static final byte JL            = (byte)0x8C; // jump if less              <signed>
    static final byte JGE           = (byte)0x8D; // jump if greater or equal  <signed>
    static final byte JLE           = (byte)0x8E; // jump if less or equal     <signed>
    static final byte JG            = (byte)0x8F; // jump if greater           <signed>
  }

  static final byte[] JO            = new byte[] {0x0F, JmpFlags.JO};
  static final byte[] JNO           = new byte[] {0x0F, JmpFlags.JNO};
  static final byte[] JB            = new byte[] {0x0F, JmpFlags.JB};
  static final byte[] JNB           = new byte[] {0x0F, JmpFlags.JNB};
  static final byte[] JZ            = new byte[] {0x0F, JmpFlags.JZ};
  static final byte[] JNZ           = new byte[] {0x0F, JmpFlags.JNZ};
  static final byte[] JBE           = new byte[] {0x0F, JmpFlags.JBE};
  static final byte[] JA            = new byte[] {0x0F, JmpFlags.JA};
  static final byte[] JS            = new byte[] {0x0F, JmpFlags.JS};
  static final byte[] JNS           = new byte[] {0x0F, JmpFlags.JNS};
  static final byte[] JP            = new byte[] {0x0F, JmpFlags.JP};
  static final byte[] JNP           = new byte[] {0x0F, JmpFlags.JNP};
  static final byte[] JL            = new byte[] {0x0F, JmpFlags.JL};
  static final byte[] JGE           = new byte[] {0x0F, JmpFlags.JGE};
  static final byte[] JLE           = new byte[] {0x0F, JmpFlags.JLE};
  static final byte[] JG            = new byte[] {0x0F, JmpFlags.JG};

  // push / pop register
  static final byte   PUSH_EAX      = 0x50;
  static final byte   PUSH_ECX      = 0x51;
  static final byte   PUSH_EDX      = 0x52;
  static final byte   PUSH_EBX      = 0x53;
  static final byte   PUSH_ESP      = 0x54;
  static final byte   PUSH_EBP      = 0x55;
  static final byte   PUSH_ESI      = 0x56;
  static final byte   PUSH_EDI      = 0x57;
  static final byte   POP_EAX       = 0x58;
  static final byte   POP_ECX       = 0x59;
  static final byte   POP_EDX       = 0x5A;
  static final byte   POP_EBX       = 0x5B;
  static final byte   POP_ESP       = 0x5C;
  static final byte   POP_EBP       = 0x5D;
  static final byte   POP_ESI       = 0x5E;
  static final byte   POP_EDI       = 0x5F;

  // CompareShortReg
  static final byte[] CMP_AX_SHORT  = new byte[]{0x66, 0x3D};
  static final byte[] CMP_CX_SHORT  = new byte[]{0x66, (byte) 0x81, (byte) 0xF9};
  static final byte[] CMP_DX_SHORT  = new byte[]{0x66, (byte) 0x81, (byte) 0xFA};
  static final byte[] CMP_BX_SHORT  = new byte[]{0x66, (byte) 0x81, (byte) 0xFB};

  // CompareShortToMemOffset
  static final byte[] CMP_MEM_ESP_SHORT = new byte[]{0x66, (byte) 0x81, 0x7C};

  // CompareByteToMemOffset
  public interface CmpMemReg {
    static final byte EAX = (byte) 0x78;
    static final byte ECX = (byte) 0x79;
    static final byte EDX = (byte) 0x7A;
    static final byte EBX = (byte) 0x7B;
    static final byte EBP = (byte) 0x7D;
    static final byte ESI = (byte) 0x7E;
    static final byte EDI = (byte) 0x7F;
  }

  static final byte[] CMP_WORD_PTR_EAX_BYTE  = new byte[]{0x66, (byte) 0x83, CmpMemReg.EAX /*0x78*/};
  static final byte[] CMP_WORD_PTR_ECX_BYTE  = new byte[]{0x66, (byte) 0x83, CmpMemReg.ECX /*0x79*/};
  static final byte[] CMP_WORD_PTR_EDX_BYTE  = new byte[]{0x66, (byte) 0x83, CmpMemReg.EDX /*0x7A*/};
  static final byte[] CMP_WORD_PTR_EBX_BYTE  = new byte[]{0x66, (byte) 0x83, CmpMemReg.EBX /*0x7B*/};
  static final byte[] CMP_WORD_PTR_EBP_BYTE  = new byte[]{0x66, (byte) 0x83, CmpMemReg.EBP /*0x7D*/};
  static final byte[] CMP_WORD_PTR_ESI_BYTE  = new byte[]{0x66, (byte) 0x83, CmpMemReg.ESI /*0x7E*/};
  static final byte[] CMP_WORD_PTR_EDI_BYTE  = new byte[]{0x66, (byte) 0x83, CmpMemReg.EDI /*0x7F*/};

  // CompareByteReg
  static final byte[] CMP_BX        = new byte[]{0x66, (byte) 0x83, (byte) 0xFB};
  static final byte[] CMP_CX        = new byte[]{0x66, (byte) 0x83, (byte) 0xF9};
  static final byte[] CMP_DX        = new byte[]{0x66, (byte) 0x83, (byte) 0xFA};
  static final byte[] CMP_SI        = new byte[]{0x66, (byte) 0x83, (byte) 0xFE};

  // CompareRegReg
  static final byte[] CMP_BX_DX     = new byte[]{0x66, (byte) 0x3B, (byte) 0xDA};

  public final byte[] MOV_AL = new byte[]{0x66, MovByteReg.AL /*0xB0*/};
  public final byte[] MOV_CL = new byte[]{0x66, MovByteReg.CL /*0xB1*/};
  public final byte[] MOV_DL = new byte[]{0x66, MovByteReg.DL /*0xB2*/};
  public final byte[] MOV_BL = new byte[]{0x66, MovByteReg.BL /*0xB3*/};
  public final byte[] MOV_AH = new byte[]{0x66, MovByteReg.AH /*0xB4*/};
  public final byte[] MOV_CH = new byte[]{0x66, MovByteReg.CH /*0xB5*/};
  public final byte[] MOV_DH = new byte[]{0x66, MovByteReg.DH /*0xB6*/};
  public final byte[] MOV_BH = new byte[]{0x66, MovByteReg.BH /*0xB7*/};

  // MovShortToReg
  public interface MovShortReg {
    public final byte AX = (byte) 0xB8;
    public final byte CX = (byte) 0xB9;
    public final byte DX = (byte) 0xBA;
    public final byte BX = (byte) 0xBB;
    public final byte SP = (byte) 0xBC;
    public final byte BP = (byte) 0xBD;
    public final byte SI = (byte) 0xBE;
    public final byte DI = (byte) 0xBF;
  }

  public final byte[] MOV_AX = new byte[]{0x66, MovShortReg.AX /*0xB8*/};
  public final byte[] MOV_CX = new byte[]{0x66, MovShortReg.CX /*0xB9*/};
  public final byte[] MOV_DX = new byte[]{0x66, MovShortReg.DX /*0xBA*/};
  public final byte[] MOV_BX = new byte[]{0x66, MovShortReg.BX /*0xBB*/};
  public final byte[] MOV_SP = new byte[]{0x66, MovShortReg.SP /*0xBC*/};
  public final byte[] MOV_BP = new byte[]{0x66, MovShortReg.BP /*0xBD*/};
  public final byte[] MOV_SI = new byte[]{0x66, MovShortReg.SI /*0xBE*/};
  public final byte[] MOV_DI = new byte[]{0x66, MovShortReg.DI /*0xBF*/};

  // MovShortToMemPtr
  public final byte[] MOV_MEM_SHORT = new byte[]{0x66, (byte) 0xC7};

  // PushInt
  static final byte   PUSH_INT      = 0x68;

  // IMul
  static final byte[] IMUL_EAX      = new byte[]{0x6B, (byte) 0xC0};

  // JmpShort
  static final byte   JO_SHORT      = 0x70; // jump on overflow
  static final byte   JNO_SHORT     = 0x71; // jump on no overflow
  static final byte   JB_SHORT      = 0x72; // jump if below:            <unsigned>
  static final byte   JNB_SHORT     = 0x73; // jump if above or equal    (JAE) <unsigned>
  static final byte   JZ_SHORT      = 0x74; // jump if zero / equal      (JE)  <signed/unsigned>
  static final byte   JNZ_SHORT     = 0x75; // jump if not zero / equal  (JNE) <signed/unsigned>
  static final byte   JBE_SHORT     = 0x76; // jump if below or equal    <unsigned>
  static final byte   JA_SHORT      = 0x77; // jump if above             <unsigned>
  static final byte   JS_SHORT      = 0x78; // jump if signed            (negative)
  static final byte   JNS_SHORT     = 0x79; // jump if not signed        (positive or zero)
  static final byte   JP_SHORT      = 0x7A; // jump if parity is even    (JPE, bit count of least significant byte)
  static final byte   JNP_SHORT     = 0x7B; // jump if parity is odd     (JPO, bit count of least significant byte)
  static final byte   JL_SHORT      = 0x7C; // jump if less              <signed>
  static final byte   JGE_SHORT     = 0x7D; // jump if greater or equal  <signed>
  static final byte   JLE_SHORT     = 0x7E; // jump if less or equal     <signed>
  static final byte   JG_SHORT      = 0x7F; // jump if greater           <signed>

  // CompareByteReg
  static final byte   CMP_AL        = 0x3C;
  static final byte[] CMP_BL        = new byte[]{(byte) 0x80, (byte) 0xFB};
  static final byte[] CMP_DL        = new byte[]{(byte) 0x80, (byte) 0xFA};
  static final byte[] CMP_BH        = new byte[]{(byte) 0x80, (byte) 0xFF};
  static final byte[] CMP_CH        = new byte[]{(byte) 0x80, (byte) 0xFD};

  // AddIntToReg
  static final byte[] ADD_EAX_INT   = new byte[]{(byte) 0x81, (byte) 0xC0};
  static final byte[] ADD_ECX_INT   = new byte[]{(byte) 0x81, (byte) 0xC1};
  static final byte[] ADD_EDX_INT   = new byte[]{(byte) 0x81, (byte) 0xC2};
  static final byte[] ADD_EBX_INT   = new byte[]{(byte) 0x81, (byte) 0xC3};
  static final byte[] ADD_ESP_INT   = new byte[]{(byte) 0x81, (byte) 0xC4};
  static final byte[] ADD_EBP_INT   = new byte[]{(byte) 0x81, (byte) 0xC5};
  static final byte[] ADD_ESI_INT   = new byte[]{(byte) 0x81, (byte) 0xC6};
  static final byte[] ADD_EDI_INT   = new byte[]{(byte) 0x81, (byte) 0xC7};

  // CompareIntReg
  static final byte[] CMP_ECX_INT   = new byte[]{(byte) 0x81, (byte) 0xF9};

  // CompareByteReg
  static final byte[] CMP_DWORD_PTR_EAX_BYTE  = new byte[]{(byte) 0x83, CmpMemReg.EAX /*0x78*/};
  static final byte[] CMP_DWORD_PTR_ECX_BYTE  = new byte[]{(byte) 0x83, CmpMemReg.ECX /*0x79*/};
  static final byte[] CMP_DWORD_PTR_EDX_BYTE  = new byte[]{(byte) 0x83, CmpMemReg.EDX /*0x7A*/};
  static final byte[] CMP_DWORD_PTR_EBX_BYTE  = new byte[]{(byte) 0x83, CmpMemReg.EBX /*0x7B*/};
  static final byte[] CMP_DWORD_PTR_EBP_BYTE  = new byte[]{(byte) 0x83, CmpMemReg.EBP /*0x7D*/};
  static final byte[] CMP_DWORD_PTR_ESI_BYTE  = new byte[]{(byte) 0x83, CmpMemReg.ESI /*0x7E*/};
  static final byte[] CMP_DWORD_PTR_EDI_BYTE  = new byte[]{(byte) 0x83, CmpMemReg.EDI /*0x7F*/};

  // AddByteToReg
  public interface AddReg {
    static final byte OPCODE = (byte) 0x83;

    static final byte EAX = (byte) 0xC0;
    static final byte ECX = (byte) 0xC1;
    static final byte EDX = (byte) 0xC2;
    static final byte EBX = (byte) 0xC3;
    static final byte ESP = (byte) 0xC4;
    static final byte EBP = (byte) 0xC5;
    static final byte ESI = (byte) 0xC6;
    static final byte EDI = (byte) 0xC7;
  }

  static final byte[] ADD_EAX_BYTE  = new byte[]{AddReg.OPCODE, AddReg.EAX /*0xC0*/};
  static final byte[] ADD_ECX_BYTE  = new byte[]{AddReg.OPCODE, AddReg.ECX /*0xC1*/};
  static final byte[] ADD_EDX_BYTE  = new byte[]{AddReg.OPCODE, AddReg.EDX /*0xC2*/};
  static final byte[] ADD_EBX_BYTE  = new byte[]{AddReg.OPCODE, AddReg.EBX /*0xC3*/};
  static final byte[] ADD_ESP_BYTE  = new byte[]{AddReg.OPCODE, AddReg.ESP /*0xC4*/};
  static final byte[] ADD_EBP_BYTE  = new byte[]{AddReg.OPCODE, AddReg.EBP /*0xC5*/};
  static final byte[] ADD_ESI_BYTE  = new byte[]{AddReg.OPCODE, AddReg.ESI /*0xC6*/};
  static final byte[] ADD_EDI_BYTE  = new byte[]{AddReg.OPCODE, AddReg.EDI /*0xC7*/};

  // SubByteReg
  public interface SubReg {
    static final byte EAX = (byte) 0xE8;
    static final byte ECX = (byte) 0xE9;
    static final byte EDX = (byte) 0xEA;
    static final byte EBX = (byte) 0xEB;
    static final byte ESP = (byte) 0xEC;
    static final byte EBP = (byte) 0xED;
    static final byte ESI = (byte) 0xEE;
    static final byte EDI = (byte) 0xEF;
  }

  static final byte[] SUB_EAX       = new byte[]{(byte) 0x83, SubReg.EAX /*0xE8*/};
  static final byte[] SUB_ECX       = new byte[]{(byte) 0x83, SubReg.ECX /*0xE9*/};
  static final byte[] SUB_EDX       = new byte[]{(byte) 0x83, SubReg.EDX /*0xEA*/};
  static final byte[] SUB_EBX       = new byte[]{(byte) 0x83, SubReg.EBX /*0xEB*/};
  static final byte[] SUB_ESP       = new byte[]{(byte) 0x83, SubReg.ESP /*0xEC*/};
  static final byte[] SUB_EBP       = new byte[]{(byte) 0x83, SubReg.EBP /*0xED*/};
  static final byte[] SUB_ESI       = new byte[]{(byte) 0x83, SubReg.ESI /*0xEE*/};
  static final byte[] SUB_EDI       = new byte[]{(byte) 0x83, SubReg.EDI /*0xEF*/};

  // CmpRegByte
  static final byte[] CMP_EAX_BYTE  = new byte[]{(byte) 0x83, (byte) 0xF8};
  static final byte[] CMP_ECX_BYTE  = new byte[]{(byte) 0x83, (byte) 0xF9};
  static final byte[] CMP_EDX_BYTE  = new byte[]{(byte) 0x83, (byte) 0xFA};
  static final byte[] CMP_EBP_BYTE  = new byte[]{(byte) 0x83, (byte) 0xFD};
  static final byte[] CMP_ESI_BYTE  = new byte[]{(byte) 0x83, (byte) 0xFE};

  // LeaMemOffset
  static final byte   LEA           = (byte) 0x8D;

  // MovRegByte
  public interface MovByteReg {
    public final byte AL = (byte) 0xB0;
    public final byte CL = (byte) 0xB1;
    public final byte DL = (byte) 0xB2;
    public final byte BL = (byte) 0xB3;
    public final byte AH = (byte) 0xB4;
    public final byte CH = (byte) 0xB5;
    public final byte DH = (byte) 0xB6;
    public final byte BH = (byte) 0xB7;
  }

  // MovRegInt
  static final byte   MOV_EAX       = (byte) 0xB8;
  static final byte   MOV_ECX       = (byte) 0xB9;
  static final byte   MOV_EDX       = (byte) 0xBA;
  static final byte   MOV_EBX       = (byte) 0xBB;
  static final byte   MOV_EBP       = (byte) 0xBD;
  static final byte   MOV_ESI       = (byte) 0xBE;
  static final byte   MOV_EDI       = (byte) 0xBF;

  // jmp
  static final byte   RETN          = (byte) 0xC3;

  // MovIntToMemPtr
  public final byte   MOV_INT       = (byte) 0xC7;

  // jmp
  static final byte   CALL          = (byte) 0xE8;
  static final byte   JMP           = (byte) 0xE9;
  static final byte   JMP_SHORT     = (byte) 0xEB;
}
