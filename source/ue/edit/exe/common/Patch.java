package ue.edit.exe.common;

import java.io.IOException;

public interface Patch {
  public boolean isPatched();
  public boolean isDefault();

  public void patch() throws IOException;
  public void unpatch() throws IOException;

  public default void setPatched(boolean patched) throws IOException {
    if (patched)
      patch();
    else
      unpatch();
  }
}
