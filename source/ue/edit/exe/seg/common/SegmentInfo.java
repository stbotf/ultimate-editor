package ue.edit.exe.seg.common;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import ue.edit.common.FileInfo;
import ue.edit.exe.trek.sd.SegmentDefinition;

/**
 * Used to list segment details for the segment list view.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class SegmentInfo extends FileInfo {
  private int               address;
  private SegmentDefinition definition;
  private Object            value;
}
