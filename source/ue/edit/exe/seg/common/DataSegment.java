package ue.edit.exe.seg.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.exe.trek.sd.DataDefinition;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.exception.DataException;
import ue.util.stream.StreamTools;

/**
 * Predefined data segment implementation.
 */
public class DataSegment extends InternalSegment {

  public DataSegment(SegmentDefinition def) {
    this(def.address, def);
  }

  public DataSegment(int address, SegmentDefinition def) {
    super(address, def);
    if (!(def instanceof DataDefinition))
      throw new IllegalArgumentException();
  }

  @Override
  public DataDefinition definition() {
    return (DataDefinition) definition;
  }

  @Override
  public boolean isDefault() {
    // if loaded successfully it must be matching the segment definition data
    return true;
  }

  public byte[] defaultValue() {
    return definition().data();
  }

  @Override
  public void reset() {
  }

  @Override
  public int getSize() {
    return definition.size();
  }

  public boolean check(byte[] data) {
    return check(data, defaultValue());
  }

  @Override
  public boolean check(byte[] data, int offset) {
    return check(data, offset, defaultValue());
  }

  public void validate(byte[] data) throws IOException {
    validate(data, defaultValue());
  }

  @Override
  public void validate(byte[] data, int offset) throws DataException{
    validate(data, offset, defaultValue());
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] data = StreamTools.readBytes(in, definition.size());
    validate(data, defaultValue());
  }

  @Override
  public void load(byte[] data) throws IOException {
    validate(data, defaultValue());
  }

  @Override
  public void load(byte[] data, int offset) throws IOException {
    validate(data, offset, defaultValue());
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(defaultValue());
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(defaultValue());
  }

  @Override
  public void clear() {
  }

  @Override
  public void check(Vector<String> response) {
  }
}
