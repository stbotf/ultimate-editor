package ue.edit.exe.seg.common;

import java.util.Vector;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.common.DataFile;
import ue.edit.common.InternalFile;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.service.Language;
import ue.util.data.HexTools;

/**
 * Every editable internal segment must extend this class.
 */
public abstract class InternalSegment extends InternalFile implements ISegment {

  /**
   * trek.exe segment details for the edited segment,
   * including name, position, type, size and description of the segment.
   */
  @Getter @Accessors(fluent = true) protected final SegmentDefinition definition;

  /**
   * The trek.exe file offset, which might deviate from definition,
   * when the definition is reused for multiple segments,
   * or the definition address is relative to some other segment.
   */
  @Getter @Accessors(fluent = true) protected final int address;

  /**
   * whether on load a value change or applied patch got detected
   */
  @Getter @Accessors(fluent = true) protected boolean changeDetected;

  public InternalSegment(SegmentDefinition def) {
    super(def.name());
    this.definition = def;
    this.address = def.address;
  }

  public InternalSegment(int address, SegmentDefinition def) {
    super(def.name());
    this.definition = def;
    this.address = address;
  }

  @Override
  public PatchStatus status() {
    return madeChanges() ? isDefault() ? PatchStatus.Unpatch
      : changeDetected() ? PatchStatus.Modify : PatchStatus.Patch
      : isSneaking() ? PatchStatus.Sneaked
      : changeDetected() ? PatchStatus.Patched : PatchStatus.Orig;
  }

  /**
   * Called on load to detect initial file changes.
   */
  @Override
  public void checkForChanges() {
    changeDetected = !isDefault();
  }

  @Override
  public int compareTo(DataFile o) {
    if (!(o instanceof InternalSegment))
      return super.compareTo(o);

    return this.address - ((InternalSegment)o).address;
  }

  @Override
  public void check(Vector<String> response) {
    val cb = definition.cbIntegrityCheck();
    if (cb != null)
      cb.accept(this, response);
  }

  @Override
  public String getCheckIntegrityString(int type, String msg) {
    String ret;

    switch (type) {
      case INTEGRITY_CHECK_WARNING: {
        ret = Language.getString("InternalSegment.1"); //$NON-NLS-1$
        break;
      }
      case INTEGRITY_CHECK_ERROR: {
        ret = Language.getString("InternalSegment.2"); //$NON-NLS-1$
        break;
      }
      default: {
        ret = Language.getString("InternalSegment.3"); //$NON-NLS-1$
      }
    }

    String hexAddr = "0x" + HexTools.toHex(address); //$NON-NLS-1$
    return ret.replace("%1", hexAddr).replace("%s", msg); //$NON-NLS-1$ //$NON-NLS-2$
  }
}
