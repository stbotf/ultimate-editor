package ue.edit.exe.seg.common;

import java.io.IOException;
import java.io.OutputStream;
import ue.edit.common.IDataFile;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.service.Language;
import ue.util.data.HexTools;

/**
 * Base interface for internal segments.
 */
public interface ISegment extends IDataFile {

  static final byte NOP = (byte) 0x90;

  /**
   * trek.exe segment details for the edited segment,
   * including name, position, type, size and description of the segment.
   */
   SegmentDefinition definition();

  /**
   * The trek.exe file offset, which might deviate from definition,
   * when the definition is reused for multiple segments,
   * or the definition address is relative to some other segment.
   */
  int address();

  /**
   * whether on load a value change or applied patch got detected
   */
  boolean changeDetected();

  default String description() {
    return definition().description();
  }

  default String getLocation() {
    return "0x" + HexTools.toHex(definition().address) + "+0x" + HexTools.toHex(getSize());
  }

  /**
   * whether the current value matches the original value
   */
  abstract boolean isDefault();

  abstract PatchStatus status();

  /**
   * Called on load to detect initial file changes.
   */
  void checkForChanges();

  abstract void saveDefault(OutputStream out) throws IOException;

  abstract void reset();

  @Override
  default String dataError(String property, String read, String expected) {
    return Language.getString("ISegment.0") //$NON-NLS-1$
      .replace("%1", "0x" + HexTools.toHex(this.address())) //$NON-NLS-1$ //$NON-NLS-2$
      .replace("%2", this.getName()) //$NON-NLS-1$
      .replace("%3", property != null ? " <" + property + ">": "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      .replace("%4", read) //$NON-NLS-1$
      .replace("%5", expected); //$NON-NLS-1$
  }

  @Override
  default <T> String valueError(String property, T value, T expected) {
    return Language.getString("ISegment.1") //$NON-NLS-1$
      .replace("%1", "0x" + HexTools.toHex(this.address())) //$NON-NLS-1$ //$NON-NLS-2$
      .replace("%2", this.getName()) //$NON-NLS-1$
      .replace("%3", property != null ? " <" + property + ">": "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
      .replace("%4", String.valueOf(value)) //$NON-NLS-1$
      .replace("%5", String.valueOf(expected)); //$NON-NLS-1$
  }
}
