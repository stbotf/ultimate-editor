package ue.edit.exe.seg.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.val;
import ue.edit.common.Checkable;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.gui.common.FileChanges;

/**
 * Used to group related internal segments.
 */
public abstract class MultiSegment implements Checkable {

  protected Trek trek;
  @Getter protected String name;

  public MultiSegment(String name, Trek trek) {
    this.name = name;
    this.trek = trek;
  }

  public abstract String[] listSegmentNames();
  public abstract List<InternalSegment> listSegments();

  public boolean hasSegment(String name) {
    for (String seg : listSegmentNames()) {
      if (seg.equals(name))
        return true;
    }
    return false;
  }

  public abstract boolean isLoaded() throws IOException;

  public void load() throws IOException {
    if (!isLoaded())
      reload();
  }

  public void load(boolean reload) throws IOException {
    if (reload || !isLoaded())
      reload();
  }

  public abstract void reset() throws IOException;
  public abstract void reload() throws IOException;

  public boolean madeChanges() {
    for (val seg : listSegments())
      if (seg.madeChanges())
        return true;
    return false;
  }

  public void listChangedFiles(FileChanges changes) {
    for (val seg : listSegments())
      trek.checkChanged(seg, changes);
  }

  protected List<InternalSegment> loadSegments(SegmentDefinition[] defs, boolean cache) throws IOException {
    return loadSegments(InternalSegment.class, defs, cache);
  }

  protected <T extends InternalSegment> List<T> loadSegments(Class<T> type, SegmentDefinition[] defs, boolean cache) throws IOException {
    ArrayList<T> segments = new ArrayList<T>(defs.length);
    for (int i = 0; i < defs.length; ++i) {
      InternalSegment seg = trek.getSegment(defs[i], cache);
      segments.add(type.cast(seg));
    }
    return segments;
  }
}
