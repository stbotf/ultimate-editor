package ue.edit.exe.seg.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.exe.common.Patch;
import ue.edit.exe.trek.sd.PatchDefinition;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Generic data patch segment implementation.
 */
public class DataPatchSegment extends GenericSegment implements Patch {

  @Getter @Accessors(fluent = true) private boolean isPatched = false;
  @Getter @Accessors(fluent = true) private int patchOption = -1;

  public DataPatchSegment(SegmentDefinition def) {
    this(def.address, def);
  }

  public DataPatchSegment(int address, SegmentDefinition def) {
    super(address, def);
    if (!(def instanceof PatchDefinition))
      throw new IllegalArgumentException();
  }

  @Override
  public PatchDefinition definition() {
    return (PatchDefinition) definition;
  }

  @Override
  public void patch() {
    patch(0);
  }

  public void patch(int patchOption) {
    if (!isPatched || this.patchOption != patchOption) {
      data = definition().patched()[patchOption];
      this.patchOption = patchOption;
      isPatched = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (isPatched) {
      data = defaultValue();
      isPatched = false;
      markChanged();
    }
  }

  @Override
  public void reset() {
    unpatch();
  }

  @Override
  public void load(InputStream in) throws IOException {
    load(StreamTools.readBytes(in, definition.size()));
  }

  @Override
  public void load(byte[] data) throws IOException {
    if (!detectPatch(data, 0))
      super.load(data);
  }

  @Override
  public void load(byte[] data, int offset) throws IOException {
    if (!detectPatch(data, 0))
      super.load(data, offset);
  }

  @Override
  public void check(Vector<String> response) {
    if (isPatched && patchOption == -1) {
      val patches = definition().patched();
      response.add(valueError(data, patches));
    }
  }

  @Override
  public void clear() {
    super.clear();
    isPatched = false;
    patchOption = -1;
  }

  public void validateDefault() throws IOException {
    if (isPatched)
      failed(data, defaultValue());
  }

  public void validatePatched() throws IOException {
    validatePatched(0);
  }

  public void validatePatched(int patchOption) throws IOException {
    if (!isPatched || this.patchOption != patchOption)
      failed(data, definition().patched()[patchOption]);
  }

  private boolean detectPatch(byte[] data, int offset) throws IOException {
    val def = definition();
    isPatched = !DataTools.equals(data, offset, def.defaultValue());
    patchOption = -1;

    if (!isPatched) {
      // detected original code
      this.data = def.defaultValue();
      markSaved();
      return true;
    }

    // determine patch option
    val patches = def.patched();
    for (int i = 0; i < patches.length; ++i) {
      if (DataTools.equals(data, offset, patches[i])) {
        // detected patched option
        this.data = patches[i];
        patchOption = i;
        markSaved();
        return true;
      }
    }

    return false;
  }
}
