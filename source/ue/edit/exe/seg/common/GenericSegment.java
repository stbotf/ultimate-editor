package ue.edit.exe.seg.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.trek.sd.DataDefinition;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.stream.StreamTools;

/**
 * @author Alan Podlesek
 */
public class GenericSegment extends DataSegment {

  protected byte[] data;

  public GenericSegment(SegmentDefinition def) {
    super(def);
  }

  public GenericSegment(int address, SegmentDefinition def) {
    super(address, def);
  }

  public GenericSegment(String name, int address, byte[] orig) {
    this(new DataDefinition(address, name, orig));
  }

  public GenericSegment(String name, int address, byte[] orig, byte[] data) {
    this(name, address, orig);
    this.data = data;
  }

  public GenericSegment(String name, int address, byte[] orig, byte[] data, boolean mark_as_modified) {
    this(name, address, orig);
    this.data = data;
    if (mark_as_modified)
      markChanged();
  }

  public GenericSegment(String name, int address, byte[] orig, InputStream in, int size) throws IOException {
    this(name, address, orig);
    load(in);
  }

  @Override
  public boolean isDefault() {
    return check(data);
  }

  public byte[] getData() {
    byte[] d = new byte[data.length];
    System.arraycopy(data, 0, d, 0, data.length);
    return d;
  }

  public void setData(byte[] d) {
    int dataSize = d != null ? d.length : 0;
    int segSize = data != null ? data.length : 0;
    if (dataSize != segSize) {
      String msg = "Data size %1 doesn't match the segment size %2!";
      throw new IllegalArgumentException(msg);
    }

    if (!Arrays.equals(data, d)) {
      data = d;
      markChanged();
    }
  }

  @Override
  public void reset() {
    setData(defaultValue());
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#check()
   */
  @Override
  public void check(Vector<String> response) {
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#getSize()
   */
  @Override
  public int getSize() {
    return data != null ? data.length : 0;
  }

  @Override
  public void load(InputStream in) throws IOException {
    data = StreamTools.readBytes(in, definition.size());
    markSaved();
  }

  @Override
  public void load(byte[] data) throws IOException {
    if (data.length != definition.size())
      throw new IndexOutOfBoundsException("Invalid data size.");
    this.data = data;
    markSaved();
  }

  @Override
  public void load(byte[] data, int offset) throws IOException {
    if (offset < 0 || data.length < offset + definition.size())
      throw new IndexOutOfBoundsException("Invalid data size.");
    this.data = data.length == definition.size() ?
      data : Arrays.copyOfRange(data, offset, offset + definition.size());
    markSaved();
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#save(java.io.OutputStream)
   */
  @Override
  public void save(OutputStream out) throws IOException {
    if (data != null)
      out.write(data);
  }

  @Override
  public void clear() {
    data = null;
    markChanged();
  }
}
