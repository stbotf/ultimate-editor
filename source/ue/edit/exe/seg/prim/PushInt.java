package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * push integer or offset
 * used for max building id and numeric sector id patch
 */
public class PushInt extends IntValueSegment {

  public static final int SIZE = Integer.BYTES + 1;
  private static final int OPCODE = OpCode.PUSH_INT;

  public PushInt(SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in.read(), OPCODE);
    value = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(OPCODE);                        // 1
    out.write(DataTools.toByte(value, true)); // 4
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(OPCODE);                                  // 1
    out.write(DataTools.toByte(defaultValue(), true));  // 4
  }

  @Override
  public int getSize() {
    return SIZE;
  }
}
