package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class AddRegInt extends IntValueSegment {

  public static final int SIZE = 5;
  public static final byte EAX = 0x05;

  public AddRegInt(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public byte opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());

    // value
    value = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());                      // 1
    out.write(DataTools.toByte(value, true)); // 4
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());                                // 1
    out.write(DataTools.toByte(defaultValue(), true));  // 4
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  public static byte opCode(Register register) {
    switch (register) {
      case EAX:
        return EAX;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
