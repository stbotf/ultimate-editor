package ue.edit.exe.seg.prim;

import java.util.List;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.sd.SegmentDefinition;

/**
 * Segment area base class for patch segments that consist of multiple sub-segments.
 */
public abstract class SegmentArea extends InternalSegment {

  public SegmentArea(SegmentDefinition def) {
    super(def);
  }

  public abstract List<InternalSegment> listSubSegments();
}
