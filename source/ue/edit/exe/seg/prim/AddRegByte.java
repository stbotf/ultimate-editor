package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;

public class AddRegByte extends SignedByteValueSegment {

  public static final int SIZE = 3;

  public static final byte[] EAX = OpCode.ADD_EAX_BYTE;
  public static final byte[] ECX = OpCode.ADD_ECX_BYTE;
  public static final byte[] EDX = OpCode.ADD_EDX_BYTE;
  public static final byte[] EBX = OpCode.ADD_EBX_BYTE;
  public static final byte[] ESP = OpCode.ADD_ESP_BYTE;
  public static final byte[] EBP = OpCode.ADD_EBP_BYTE;
  public static final byte[] ESI = OpCode.ADD_ESI_BYTE;
  public static final byte[] EDI = OpCode.ADD_EDI_BYTE;

  public AddRegByte(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());

    value = in.read();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());
    out.write(value);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());
    out.write(defaultValue());
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  public static byte[] opCode(Register register) {
    switch (register) {
      case EAX:
        return EAX;
      case ECX:
        return ECX;
      case EDX:
        return EDX;
      case EBX:
        return EBX;
      case ESP:
        return ESP;
      case EBP:
        return EBP;
      case ESI:
        return ESI;
      case EDI:
        return EDI;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
