package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.value.SByteValue;
import ue.exception.InvalidArgumentsException;

/**
 * Signed byte value segment base class.
 */
public class SignedByteValueSegment extends ByteValueSegment implements SByteValue {

  public SignedByteValueSegment(SegmentDefinition def) {
    super(def);
  }

  public SignedByteValueSegment(int address, SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    value = (byte) in.read();
    markSaved();
  }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    if (value < Byte.MIN_VALUE || value > Byte.MAX_VALUE) {
      String msg = "Value %1 exceeds the signed byte boundary!"
        .replace("%1", Integer.toString(value));
      throw new InvalidArgumentsException(msg);
    }

    if (this.value != value) {
      this.value = value;
      markChanged();
      return true;
    }
    return false;
  }

  @Override
  public int defaultValue() {
    return (Byte)definition().defaultValue();
  }

}
