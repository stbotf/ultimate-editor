package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.trek.sd.SegmentDefinition;

/**
 * Jump-Short instruction.
 */
public class JmpShort extends ByteValueSegment {

  public static final int SIZE = 2;

  // for segment definitions specify as flags
  public static final byte JO  = OpCode.JO_SHORT;   // jump on overflow
  public static final byte JNO = OpCode.JNO_SHORT;  // jump on no overflow
  public static final byte JB  = OpCode.JB_SHORT;   // jump if below:            <unsigned>
  public static final byte JNB = OpCode.JNB_SHORT;  // jump if above or equal    (JAE) <unsigned>
  public static final byte JZ  = OpCode.JZ_SHORT;   // jump if zero / equal      (JE)  <signed/unsigned>
  public static final byte JNZ = OpCode.JNZ_SHORT;  // jump if not zero / equal  (JNE) <signed/unsigned>
  public static final byte JBE = OpCode.JBE_SHORT;  // jump if below or equal    <unsigned>
  public static final byte JA  = OpCode.JA_SHORT;   // jump if above             <unsigned>
  public static final byte JS  = OpCode.JS_SHORT;   // jump if signed            (negative)
  public static final byte JNS = OpCode.JNS_SHORT;  // jump if not signed        (positive or zero)
  public static final byte JP  = OpCode.JP_SHORT;   // jump if parity is even    (JPE, bit count of least significant byte)
  public static final byte JNP = OpCode.JNP_SHORT;  // jump if parity is odd     (JPO, bit count of least significant byte)
  public static final byte JL  = OpCode.JL_SHORT;   // jump if less              <signed>
  public static final byte JGE = OpCode.JGE_SHORT;  // jump if greater or equal  <signed>
  public static final byte JLE = OpCode.JLE_SHORT;  // jump if less or equal     <signed>
  public static final byte JG  = OpCode.JG_SHORT;   // jump if greater           <signed>
  public static final byte JMP = OpCode.JMP_SHORT;

  public JmpShort(SegmentDefinition def) {
    super(def);
    validateOp(operation());
  }

  public byte operation() {
    return definition.hasFlags() ? (byte)definition.flags() : JMP;
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in.read(), operation());
    value = in.read();
    if (definition.hasOffset())
      validate(value, definition.offset());

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(operation());
    out.write(value);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(operation());
    out.write(defaultValue());
  }

  private static void validateOp(byte op) {
    switch (op) {
      case JO:
      case JNO:
      case JB:
      case JNB:
      case JZ:
      case JNZ:
      case JBE:
      case JA:
      case JS:
      case JNS:
      case JP:
      case JNP:
      case JL:
      case JGE:
      case JLE:
      case JG:
      case JMP:
        break;
      default:
        throw new UnsupportedOperationException("Invalid operation: " + op);
    }
  }
}
