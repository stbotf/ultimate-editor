package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class CmpWordPtrShort extends ShortValueSegment {

  // add register flags
  private static final byte ADD_NONE = 0x24;

  public CmpWordPtrShort(SegmentDefinition def) {
    super(def);
    validateRegister(register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] op = operation(register());
    byte[] b = StreamTools.readBytes(in, op.length);
    validate(b, op);
    validate(in, ADD_NONE);
    validate(in.read(), registerOffset());

    value = StreamTools.readShort(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(operation(register()));         // 3
    out.write(ADD_NONE);                      // 1
    out.write(registerOffset());              // 1
    out.write(DataTools.toByte(value, true)); // 2
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(operation(register()));                   // 3
    out.write(ADD_NONE);                                // 1
    out.write(registerOffset());                        // 1
    out.write(DataTools.toByte(defaultValue(), true));  // 2
  }

  @Override
  public int getSize() {
    return getSize(register());
  }

  public static int getSize(Register register) {
    return operation(register).length + 4;
  }

  private static byte[] operation(Register register) {
    switch (register) {
      case ESP:
        return OpCode.CMP_MEM_ESP_SHORT;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }

  private static void validateRegister(Register register) {
    operation(register);
  }
}
