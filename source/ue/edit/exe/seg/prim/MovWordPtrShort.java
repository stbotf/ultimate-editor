package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// mov word ptr [reg+x] value
// same like MovDwordPtrInt except the op code and the type value
public class MovWordPtrShort extends ShortValueSegment implements IMovMemPtr {

  private static final byte[] OP_KEY = OpCode.MOV_MEM_SHORT;

  @Getter private final int opFlags;

  // all register values supported
  public MovWordPtrShort(SegmentDefinition def) {
    super(def);
    opFlags = IMovMemPtr.opFlags(def.register(), def.flags(), def.offset());
  }

  @Override
  public byte[] opKey() {
    return OP_KEY;
  }

  @Override
  public int opFlags() {
    return opFlags;
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(StreamTools.readBytes(in, OP_KEY.length), OP_KEY);
    validateParams(in);

    // value
    value = StreamTools.readShort(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    // with dword offset 7 bytes, with byte offset 4, else 3
    // +1 when EXT is set
    out.write(opCode());
    out.write(DataTools.toByte(value, true));           // 2
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    // with dword offset 7 bytes, with byte offset 4, else 3
    // +1 when EXT is set
    out.write(opCode());
    out.write(DataTools.toByte(defaultValue(), true));  // 2
  }

  @Override
  public int getSize() {
    return OP_KEY.length + IMovMemPtr.getParamSize(opFlags) + Short.BYTES;
  }

  public static int getSize(Register register, int flags, int offset) {
    return OP_KEY.length + IMovMemPtr.getParamSize(register, flags, offset) + Short.BYTES;
  }
}
