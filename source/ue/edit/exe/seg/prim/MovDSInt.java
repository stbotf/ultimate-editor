package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// mov ds:addr value
public class MovDSInt extends IntValueSegment {

  public static final int SIZE = 10;

  private static final byte[] test = new byte[]{(byte) 0xC7, (byte) 0x05};

  @Getter private int memoryOffset;

  public MovDSInt(SegmentDefinition def) {
    super(def);
  }

  public MovDSInt(int address, SegmentDefinition def) {
    super(address, def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, test);

    memoryOffset = StreamTools.readInt(in, true);
    if (definition.hasOffset())
      validate(memoryOffset, definition.offset());

    // value
    value = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(test);                                  // 2
    out.write(DataTools.toByte(memoryOffset, true));  // 4
    out.write(DataTools.toByte(value, true));         // 4
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(test);                                    // 2
    out.write(DataTools.toByte(memoryOffset, true));    // 4
    out.write(DataTools.toByte(defaultValue(), true));  // 4
  }

  @Override
  public int getSize() {
    return SIZE;
  }
}
