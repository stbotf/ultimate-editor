package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import ue.edit.exe.common.OpCode.Register;
import ue.util.stream.StreamTools;

// mov <x> ptr [reg+x] value
// base interface for MovIntToMemPtr and MovShortToMemPtr
// both are same except the op code and the type value
public interface IMovMemPtr extends IValueSegment {

  // size
  static final int PARAM_SIZE = 1;              // 1 byte register
  static final int PARAM_BYTE_OFFSET_SIZE = 2;  // 1 byte register + 1 byte offset
  static final int PARAM_DWORD_OFFSET_SIZE = 5; // 1 byte register + 4 byte offset

  // registers
  static final int EAX = 0x00; // ds
  static final int ECX = 0x01; // ds
  static final int EDX = 0x02; // ds
  static final int EBX = 0x03; // ds
  static final int ESP = 0x04; // ss (enforces EXT flag)
  static final int EBP = 0x05; // ss
  static final int ESI = 0x06; // ds
  static final int EDI = 0x07; // ds

  /**
   * Register MODIFIERS to be specified with the segment definition flags.
   * Assembled they are set on the upper bits of the shared register byte.
   * ADD and MUL flags can be combined like "ADD_EAX|MUL2".
   * If any is set, the EXT flag is inserted at 2nd byte.
   */
  // mov <x> ptr [reg1+REG2+x] value
  static final int ADD_EAX = 0x20; // 0x20 to 0x27, actual asm 0x00 to 0x07
  static final int ADD_ECX = 0x08; // 0x08 to 0x0f
  static final int ADD_EDX = 0x10; // 0x10 to 0x17
  static final int ADD_EBX = 0x18; // 0x18 to 0x1f
  static final int ADD_EBP = 0x28; // 0x28 to 0x2f
  static final int ADD_ESI = 0x30; // 0x30 to 0x37
  static final int ADD_EDI = 0x38; // 0x38 to 0x3f
  // mov <x> ptr [reg1+reg2*MUL+x] value
  static final int MUL2    = 0x40; // 0x40 to 0x7f
  static final int MUL4    = 0x80; // 0x80 to 0xbf
  static final int MUL8    = 0xc0; // 0xc0 to 0xff

  // ADD_NONE is mostly used for ESP register to not conflict with the EXT flag
  // it may further be set when some modifier is used
  static final int ADD_NONE = 0x20; // 0x20 to 0x27
  static final int REG_MASK = 0x07;
  static final int ADD_MASK = 0x38;
  static final int MOD_MASK = 0xF8;

  // op-flags
  static final int EXT = 0x04;
  static final int BYTE_OFFSET = 0x40;
  static final int DWORD_OFFSET = 0x80;

  byte[] opKey();

  default byte[] opCode() {
    return opCode(opKey(), asmRegister(), asmFlags(), registerOffset(), opFlags());
  }

  default int asmRegister() {
    return asmRegister(register());
  }

  default int asmFlags() {
    return asmFlags(registerFlags(), opFlags());
  }

  int opFlags();

  /**
   * Determines the op-code flags by register, flags and offset.
   * @param register  the segment definition register
   * @param flags     the segment definition register flags
   * @param offset    the segment definition register offset
   * @return the op-code flags
   */
  static int opFlags(Register register, int flags, int offset) {
    int opFlags = 0;

    // for ESP register and modifiers the EXT flag must be specified
    if (register == Register.ESP || (flags & MOD_MASK) != 0)
      opFlags |= EXT;

    if (offset > 0x7f || offset < -0x80)
      opFlags |= DWORD_OFFSET;
    else if (offset != 0)
      opFlags |= BYTE_OFFSET;

    return opFlags;
  }

  default void validateParams(InputStream in) throws IOException {
    int register = asmRegister();
    int regFlags = asmFlags();
    int opFlags = opFlags();
    int b = in.read();

    if ((b & EXT) != 0) {
      validate(b, opFlags);
      validate(in.read(), register | regFlags);
    } else {
      validate(b, opFlags | register | regFlags);
    }

    int offset = registerOffset();

    if ((opFlags & BYTE_OFFSET) != 0) {
      validate(in.read(), offset);
    } else if ((opFlags & DWORD_OFFSET) != 0) {
      validate(StreamTools.readInt(in, true), offset);
    }
  }

  static int getParamSize(Register register, int flags, int offset) {
    int opFlags = opFlags(register, flags, offset);
    return getParamSize(opFlags);
  }

  static int getParamSize(int opFlags) {
    switch (opFlags) {
      case EXT:
        return PARAM_SIZE + 1;
      case EXT|BYTE_OFFSET:
        return PARAM_BYTE_OFFSET_SIZE + 1;
      case EXT|DWORD_OFFSET:
        return PARAM_DWORD_OFFSET_SIZE + 1;
      case BYTE_OFFSET:
        return PARAM_BYTE_OFFSET_SIZE;
      case DWORD_OFFSET:
        return PARAM_DWORD_OFFSET_SIZE;
      default:
        return PARAM_SIZE;
    }
  }

  static byte[] opCode(byte[] opKey, int asmRegister, int asmFlags, int offset, int opFlags) {
    byte[] op = Arrays.copyOf(opKey, opKey.length + getParamSize(opFlags));
    int idx = opKey.length;

    if ((opFlags & EXT) != 0) {
      op[idx++] = (byte) opFlags;
      op[idx++] = (byte) (asmRegister|asmFlags);
    } else {
      op[idx++] = (byte) (asmRegister|asmFlags|opFlags);
    }

    if ((opFlags & BYTE_OFFSET) != 0) {
      op[idx]   = (byte) offset;
    } else if ((opFlags & DWORD_OFFSET) != 0) {
      op[idx]   = (byte) offset;
      op[idx+1] = (byte) (offset >>> 8);
      op[idx+2] = (byte) (offset >>> 16);
      op[idx+3] = (byte) (offset >>> 24);
    }

    return op;
  }

  static int asmRegister(Register register) {
    switch(register) {
      case EAX:
        return EAX;
      case ECX:
        return ECX;
      case EDX:
        return EDX;
      case EBX:
        return EBX;
      case ESP:
        return ESP;
      case EBP:
        return EBP;
      case ESI:
        return ESI;
      case EDI:
        return EDI;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }

  static int asmFlags(int registerFlags, int opFlags) {
    int flags = registerFlags;
    int add = flags & ADD_MASK;

    // fix ADD_EAX
    if (add == ADD_EAX)
      flags = flags & ~ADD_EAX;
    // fix ADD_NONE
    if ((opFlags & EXT) != 0 && add == 0)
      flags |= ADD_NONE;

    return flags;
  }
}
