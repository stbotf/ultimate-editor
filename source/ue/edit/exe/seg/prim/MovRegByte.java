package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;

/**
 * Move byte value to register instruction.
 */
public class MovRegByte extends SignedByteValueSegment {

  public static final int SIZE = 3;

  public static final byte[] AL = OpCode.MOV_AL;
  public static final byte[] CL = OpCode.MOV_CL;
  public static final byte[] DL = OpCode.MOV_DL;
  public static final byte[] BL = OpCode.MOV_BL;
  public static final byte[] AH = OpCode.MOV_AH;
  public static final byte[] CH = OpCode.MOV_CH;
  public static final byte[] DH = OpCode.MOV_DH;
  public static final byte[] BH = OpCode.MOV_BH;

  public MovRegByte(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public MovRegByte(int address, SegmentDefinition def) {
    super(address, def);
    // validate register
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());     // 2

    // value
    value = in.read();          // 1
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());        // 2
    out.write(value);           // 1
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());        // 2
    out.write(defaultValue());  // 1
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  private static byte[] opCode(Register register) {
    switch (register) {
      case AL:
        return AL;
      case CL:
        return CL;
      case DL:
        return DL;
      case BL:
        return BL;
      case AH:
        return AH;
      case CH:
        return CH;
      case DH:
        return DH;
      case BH:
        return BH;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
