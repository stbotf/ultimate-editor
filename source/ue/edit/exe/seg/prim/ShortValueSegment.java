package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.value.ShortValue;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Short value segment base class.
 */
public class ShortValueSegment extends ValueSegment implements ShortValue {

  protected short value;

  public ShortValueSegment(SegmentDefinition def) {
    this(def.getAddress(), def);
  }

  public ShortValueSegment(int address, SegmentDefinition def) {
    super(address, def);
    if (!(definition().defaultValue() instanceof Short))
      throw new IllegalArgumentException();
  }

  @Override
  public void load(InputStream in) throws IOException {
    value = StreamTools.readShort(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(value, true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(DataTools.toByte(defaultValue(), true));
  }

  @Override public short shortValue() { return value; }
  @Override public int   intValue()   { return value; }
  @Override public long  longValue()  { return value; }

  @Override
  public void setValue(short value) throws InvalidArgumentsException {
    definition().validate(this, value);
    forceValue(value);
  }

  @Override
  public void setShortValue(short value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public void setIntValue(int value) throws InvalidArgumentsException {
    if (value > Short.MAX_VALUE || value < Short.MIN_VALUE)
      throw new InvalidArgumentsException();
    setValue((short)value);
  }

  @Override
  public void setLongValue(long value) throws InvalidArgumentsException {
    if (value > Short.MAX_VALUE || value < Short.MIN_VALUE)
      throw new InvalidArgumentsException();
    setValue((short)value);
  }

  public void forceValue(short value) {
    if (this.value != value) {
      this.value = value;
      markChanged();
    }
  }

  @Override
  public boolean isDefault() {
    return value == defaultValue();
  }

  public short defaultValue() {
    return (Short)definition().defaultValue();
  }

  @Override
  public void reset() {
    forceValue(defaultValue());
  }

  @Override
  public void clear() {
    value = 0;
    markChanged();
  }

  public void validate(short value) throws IOException {
    validate(this.value, value);
  }

  public void validateDefault() throws IOException {
    validate(this.value, defaultValue());
  }

  // overwrite toString for the set segment value menu
  @Override
  public String toString() {
    return Short.toString(value);
  }

}
