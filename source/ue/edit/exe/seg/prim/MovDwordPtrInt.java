package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// mov dword ptr [reg+x] value
// same like MovWordPtrShort except the op code and the type value
public class MovDwordPtrInt extends IntValueSegment implements IMovMemPtr {

  private static final byte OP_KEY = OpCode.MOV_INT;

  // size
  public static final int REG_SIZE = 6;
  public static final int REG_BYTE_OFFSET_SIZE = 7;
  public static final int REG_DWORD_OFFSET_SIZE = 10;
  public static final int EXT_SIZE = 7;
  public static final int EXT_BYTE_OFFSET_SIZE = 8;
  public static final int EXT_DWORD_OFFSET_SIZE = 11;

  @Getter private final int opFlags;

  // all register values supported
  public MovDwordPtrInt(SegmentDefinition def) {
    super(def);
    opFlags = IMovMemPtr.opFlags(def.register(), def.flags(), def.offset());
  }

  @Override
  public byte[] opKey() {
    return new byte[]{OP_KEY};
  }

  @Override
  public int opFlags() {
    return opFlags;
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, OP_KEY);
    validateParams(in);

    // value
    value = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    // with dword offset 6 bytes, with byte offset 3, else 2
    // +1 when EXT is set
    out.write(opCode());
    out.write(DataTools.toByte(value, true));           // 4
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    // with dword offset 6 bytes, with byte offset 3, else 2
    // +1 when EXT is set
    out.write(opCode());
    out.write(DataTools.toByte(defaultValue(), true));  // 4
  }

  @Override
  public int getSize() {
    return 1 + IMovMemPtr.getParamSize(opFlags) + Integer.BYTES;
  }

  public static int getSize(Register register, int flags, int offset) {
    return 1 + IMovMemPtr.getParamSize(register, flags, offset) + Integer.BYTES;
  }
}
