package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.JmpFlags;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.data.HexTools;
import ue.util.stream.StreamTools;

/**
 * Conditional jump instruction.
 */
public class JmpIf extends IntValueSegment {

  public static final int SIZE = 6;

  // 2 byte instructions
  // for segment definitions specify as register
  public static final byte[] JO   = OpCode.JO;  // jump on overflow
  public static final byte[] JNO  = OpCode.JNO; // jump on no overflow
  public static final byte[] JB   = OpCode.JB;  // jump if below:            <unsigned>
  public static final byte[] JNB  = OpCode.JNB; // jump if above or equal    (JAE) <unsigned>
  public static final byte[] JZ   = OpCode.JZ;  // jump if zero / equal      (JE)  <signed/unsigned>
  public static final byte[] JNZ  = OpCode.JNZ; // jump if not zero / equal  (JNE) <signed/unsigned>
  public static final byte[] JBE  = OpCode.JBE; // jump if below or equal    <unsigned>
  public static final byte[] JA   = OpCode.JA;  // jump if above             <unsigned>
  public static final byte[] JS   = OpCode.JS;  // jump if signed            (negative)
  public static final byte[] JNS  = OpCode.JNS; // jump if not signed        (positive or zero)
  public static final byte[] JP   = OpCode.JP;  // jump if parity is even    (JPE, bit count of least significant byte)
  public static final byte[] JNP  = OpCode.JNP; // jump if parity is odd     (JPO, bit count of least significant byte)
  public static final byte[] JL   = OpCode.JL;  // jump if less              <signed>
  public static final byte[] JGE  = OpCode.JGE; // jump if greater or equal  <signed>
  public static final byte[] JLE  = OpCode.JLE; // jump if less or equal     <signed>
  public static final byte[] JG   = OpCode.JG;  // jump if greater           <signed>

  public JmpIf(SegmentDefinition def) {
    this(def.address, def);
  }

  public JmpIf(int address, SegmentDefinition def) {
    super(address, def);
    // validate flags
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.flags());
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());
    value = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());                      // 2
    out.write(DataTools.toByte(value, true)); // 4
  }

  private static byte[] opCode(int flags) {
    switch (flags) {
      case JmpFlags.JO:
        return JO;
      case JmpFlags.JNO:
        return JNO;
      case JmpFlags.JB:
        return JB;
      case JmpFlags.JNB:
        return JNB;
      case JmpFlags.JZ:
        return JZ;
      case JmpFlags.JNZ:
        return JNZ;
      case JmpFlags.JBE:
        return JBE;
      case JmpFlags.JA:
        return JA;
      case JmpFlags.JS:
        return JS;
      case JmpFlags.JNS:
        return JNS;
      case JmpFlags.JP:
        return JP;
      case JmpFlags.JNP:
        return JNP;
      case JmpFlags.JL:
        return JL;
      case JmpFlags.JGE:
        return JGE;
      case JmpFlags.JLE:
        return JLE;
      case JmpFlags.JG:
        return JG;
      default:
        throw new UnsupportedOperationException("Invalid operation: 0x" + HexTools.toHex(flags));
    }
  }
}
