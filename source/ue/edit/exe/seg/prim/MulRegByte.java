package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;

public class MulRegByte extends SignedByteValueSegment {

  public static final int SIZE = 3;

  public static final byte[] EAX = OpCode.IMUL_EAX;

  public MulRegByte(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());

    // value
    value = in.read();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());  // 2
    out.write(value);     // 1
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());        // 2
    out.write(defaultValue());  // 1
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  public static byte[] opCode(Register register) {
    switch (register) {
      case EAX:
        return EAX;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
