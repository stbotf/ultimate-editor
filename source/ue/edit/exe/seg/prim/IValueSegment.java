package ue.edit.exe.seg.prim;

import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.common.ISegment;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;

/**
 * Value segment base class.
 */
public interface IValueSegment extends ISegment {

  @Override
  ValueDefinition<?> definition();

  default Register register() {
    return definition().register();
  }

  default int registerFlags() {
    return definition().flags();
  }

  default int registerOffset() {
    return definition().offset();
  }

  public abstract short shortValue();
  public abstract int   intValue();
  public abstract long  longValue();

  public abstract void  setShortValue(short value) throws InvalidArgumentsException;
  public abstract void  setIntValue(int value) throws InvalidArgumentsException;
  public abstract void  setLongValue(long value) throws InvalidArgumentsException;
}
