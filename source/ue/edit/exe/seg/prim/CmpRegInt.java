package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Max building ids
 */
public class CmpRegInt extends IntValueSegment {

  public static final int SIZE = 6;

  private static final byte[] op_ecx = OpCode.CMP_ECX_INT;

  public CmpRegInt(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public byte[] opCode() {
    return operation(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] op = operation(register());
    byte[] b = StreamTools.readBytes(in, op.length);
    validate(b, op);

    value = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(operation(register()));         // 2
    out.write(DataTools.toByte(value, true)); // 4
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(operation(register()));                   // 2
    out.write(DataTools.toByte(defaultValue(), true));  // 4
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  private static byte[] operation(Register register) {
    switch (register) {
      case ECX:
        return op_ecx;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
