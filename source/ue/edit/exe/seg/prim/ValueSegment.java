package ue.edit.exe.seg.prim;

import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;

/**
 * Value segment base class.
 */
public abstract class ValueSegment extends InternalSegment implements IValueSegment {

  public ValueSegment(SegmentDefinition def) {
    super(def);
  }

  public ValueSegment(int address, SegmentDefinition def) {
    super(address, def);
  }

  @Override
  public ValueDefinition<?> definition() {
    return (ValueDefinition<?>) definition;
  }

  @Override
  public PatchStatus status() {
    return madeChanges() ? isDefault() ? PatchStatus.Reset : PatchStatus.Change
      : sneaking ? PatchStatus.Sneaked
      : changeDetected() ? PatchStatus.Changed : PatchStatus.Orig;
  }

}
