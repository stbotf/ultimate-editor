package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.val;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.value.LongValue;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Long value segment base class.
 */
public class LongValueSegment extends ValueSegment implements LongValue {

  protected long value;
  protected boolean isDouble;

  public LongValueSegment(SegmentDefinition def) {
    this(def.getAddress(), def);
  }

  public LongValueSegment(int address, SegmentDefinition def) {
    super(address, def);
    val orig = definition().defaultValue();
    isDouble = orig instanceof Double;
    if (!isDouble && !(definition().defaultValue() instanceof Long))
      throw new IllegalArgumentException();
  }

  @Override
  public boolean isFloatingPoint() {
    return isDouble;
  }

  @Override
  public void load(InputStream in) throws IOException {
    value = StreamTools.readLong(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(value, true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(DataTools.toByte(defaultValue(), true));
  }

  @Override public short shortValue() { return (short)value; }
  @Override public int   intValue()   { return (int)value; }
  @Override public long  longValue()  { return value; }

  @Override
  public void setValue(long value) throws InvalidArgumentsException {
    definition().validate(this, value);
    forceValue(value);
  }

  @Override
  public void setShortValue(short value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public void setIntValue(int value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public void setLongValue(long value) throws InvalidArgumentsException {
    setValue(value);
  }

  public void forceValue(long value) {
    if (this.value != value) {
      this.value = value;
      markChanged();
    }
  }

  @Override
  public boolean isDefault() {
    return value == defaultValue();
  }

  public long defaultValue() {
    return isDouble ? Double.doubleToLongBits(((Double)definition().defaultValue()))
      : (Integer)definition().defaultValue();
  }

  @Override
  public void reset() {
    forceValue(defaultValue());
  }

  @Override
  public void clear() {
    value = 0;
    markChanged();
  }

  public void validate(long value) throws IOException {
    validate(this.value, value);
  }

  public void validateDouble(double value) throws IOException {
    validate(getDouble(), value);
  }

  public void validateDefault() throws IOException {
    validate(this.value, defaultValue());
  }

  // overwrite toString for the set segment value menu
  @Override
  public String toString() {
    return isDouble ? Double.toString(getDouble()) : Long.toString(value);
  }

}
