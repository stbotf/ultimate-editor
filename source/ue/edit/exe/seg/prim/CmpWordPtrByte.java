package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.data.ValueCast;

/**
 * Used for OrbitalDefenseBld6 and basic shipyard building ids
 */
public class CmpWordPtrByte extends SignedByteValueSegment {

  public static final int SIZE = 5;

  public static final byte[] EAX = OpCode.CMP_WORD_PTR_EAX_BYTE;
  public static final byte[] EDX = OpCode.CMP_WORD_PTR_EDX_BYTE;
  public static final byte[] EBX = OpCode.CMP_WORD_PTR_EBX_BYTE;
  public static final byte[] EBP = OpCode.CMP_WORD_PTR_EBP_BYTE;
  public static final byte[] ESI = OpCode.CMP_WORD_PTR_ESI_BYTE;
  public static final byte[] EDI = OpCode.CMP_WORD_PTR_EDI_BYTE;

  public CmpWordPtrByte(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());
    validate(in, definition.offset());
    value = in.read();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());
    out.write(definition.offset());
    out.write(value);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());
    out.write(definition.offset());
    out.write(defaultValue());
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    // ids must be unsigned byte this case
    ValueCast.checkUByte(value);
    return super.setValue(value);
  }

  private static byte[] opCode(Register register) {
    switch (register) {
      case EAX:
        return EAX;
      case EDX:
        return EDX;
      case EBX:
        return EBX;
      case EBP:
        return EBP;
      case ESI:
        return ESI;
      case EDI:
        return EDI;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
