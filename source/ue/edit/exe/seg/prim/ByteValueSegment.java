package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.value.ByteValue;
import ue.exception.InvalidArgumentsException;

/**
 * Byte value segment base class.
 */
public class ByteValueSegment extends ValueSegment implements ByteValue {

  // unsigned byte value
  protected int value;

  public ByteValueSegment(SegmentDefinition def) {
    this(def.getAddress(), def);
  }

  public ByteValueSegment(int address, SegmentDefinition def) {
    super(address, def);
    if (!(definition().defaultValue() instanceof Byte))
      throw new IllegalArgumentException();
  }

  @Override
  public void load(InputStream in) throws IOException {
    value = in.read();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(value);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(defaultValue());
  }

  @Override
  public int byteValue() {
    return value;
  }

  @Override public short shortValue() { return (short)value; }
  @Override public int   intValue()   { return value; }
  @Override public long  longValue()  { return value; }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    if (value < 0 || value > 0xFF) {
      String msg = "Value %1 exceeds the unsigned byte boundary!"
        .replace("%1", Integer.toString(value));
      throw new InvalidArgumentsException(msg);
    }

    definition().validate(this, value);
    return forceValue(value);
  }

  @Override
  public void setShortValue(short value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public void setIntValue(int value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public void setLongValue(long value) throws InvalidArgumentsException {
    if (value > Integer.MAX_VALUE || value < Integer.MIN_VALUE)
      throw new InvalidArgumentsException();
    setValue((int)value);
  }

  public boolean forceValue(int value) {
    if (this.value != value) {
      this.value = value;
      markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean isDefault() {
    return value == defaultValue();
  }

  public int defaultValue() {
    return Byte.toUnsignedInt((Byte)definition().defaultValue());
  }

  @Override
  public void reset() {
    forceValue(defaultValue());
  }

  @Override
  public void clear() {
    value = 0;
    markChanged();
  }

  public void validate(int value) throws IOException {
    validate(this.value, value);
  }

  public void validateDefault() throws IOException {
    validate(this.value, defaultValue());
  }

  // overwrite toString for the set segment value menu
  @Override
  public String toString() {
    return Integer.toString(value);
  }

}
