package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.common.DataSegment;
import ue.edit.exe.trek.sd.SegmentDefinition;

/**
 * push register
 */
public class PopReg extends DataSegment {

  public static final int SIZE = 1;

  public static final byte EAX = OpCode.POP_EAX;
  public static final byte ECX = OpCode.POP_ECX;
  public static final byte EDX = OpCode.POP_EDX;
  public static final byte EBX = OpCode.POP_EBX;
  public static final byte ESP = OpCode.POP_ESP;
  public static final byte EBP = OpCode.POP_EBP;
  public static final byte ESI = OpCode.POP_ESI;
  public static final byte EDI = OpCode.POP_EDI;

  public PopReg(SegmentDefinition def) {
    super(def);
    // validate register
    opCode(def.register());
  }

  public byte opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  public static byte opCode(Register register) {
    switch (register) {
      case EAX:
        return EAX;
      case ECX:
        return ECX;
      case EDX:
        return EDX;
      case EBX:
        return EBX;
      case ESP:
        return ESP;
      case EBP:
        return EBP;
      case ESI:
        return ESI;
      case EDI:
        return EDI;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
