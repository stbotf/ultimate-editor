package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.stream.StreamTools;

public class CmpRegByte extends ByteValueSegment {

  public static final byte AL     = OpCode.CMP_AL;
  public static final byte[] BL   = OpCode.CMP_BL;
  public static final byte[] DL   = OpCode.CMP_DL;
  public static final byte[] BH   = OpCode.CMP_BH;
  public static final byte[] CH   = OpCode.CMP_CH;
  public static final byte[] BX   = OpCode.CMP_BX;
  public static final byte[] CX   = OpCode.CMP_CX;
  public static final byte[] DX   = OpCode.CMP_DX;
  public static final byte[] SI   = OpCode.CMP_SI;
  public static final byte[] EAX  = OpCode.CMP_EAX_BYTE;
  public static final byte[] ECX  = OpCode.CMP_ECX_BYTE;
  public static final byte[] EDX  = OpCode.CMP_EDX_BYTE;
  public static final byte[] EBP  = OpCode.CMP_EBP_BYTE;
  public static final byte[] ESI  = OpCode.CMP_ESI_BYTE;

  private static final byte[] OpAL  = new byte[] { AL };

  public CmpRegByte(SegmentDefinition def) {
    super(def);
    validateRegister(def.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] op = operation(register());
    byte[] b = StreamTools.readBytes(in, op.length);
    validate(b, op);

    value = in.read();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(operation(register())); // 2 or 3
    out.write(value);                 // 1
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(operation(register())); // 2 or 3
    out.write(defaultValue());        // 1
  }

  @Override
  public int getSize() {
    return getSize(register());
  }

  public static int getSize(Register register) {
    return operation(register).length + 1;
  }

  public static byte[] operation(Register register) {
    switch (register) {
      case AL:
        return OpAL;
      case BL:
        return BL;
      case DL:
        return DL;
      case BH:
        return BH;
      case CH:
        return CH;
      case BX:
        return BX;
      case CX:
        return CX;
      case DX:
        return DX;
      case SI:
        return SI;
      case EAX:
        return EAX;
      case ECX:
        return ECX;
      case EDX:
        return EDX;
      case EBP:
        return EBP;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }

  private static void validateRegister(Register register) {
    operation(register);
  }
}
