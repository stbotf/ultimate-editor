package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;

public class SubRegByte extends ByteValueSegment {

  public static final int SIZE = 3;

  public static final byte[] EAX = OpCode.SUB_EAX;
  public static final byte[] ECX = OpCode.SUB_ECX;
  public static final byte[] EDX = OpCode.SUB_EDX;
  public static final byte[] EBX = OpCode.SUB_EBX;
  public static final byte[] ESP = OpCode.SUB_ESP;
  public static final byte[] EBP = OpCode.SUB_EBP;
  public static final byte[] ESI = OpCode.SUB_ESI;
  public static final byte[] EDI = OpCode.SUB_EDI;

  public SubRegByte(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());
    value = in.read();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());        // 2
    out.write(value);           // 1
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());        // 2
    out.write(defaultValue());  // 1
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  private byte[] opCode(Register register) {
    switch (register) {
      case EAX:
        return EAX;
      case ECX:
        return ECX;
      case EDX:
        return EDX;
      case EBX:
        return EBX;
      case ESP:
        return ESP;
      case EBP:
        return EBP;
      case ESI:
        return ESI;
      case EDI:
        return EDI;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
