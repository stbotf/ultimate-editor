package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Max building ids and count
 */
public class CmpRegShort extends ShortValueSegment {

  public static final byte[] AX = OpCode.CMP_AX_SHORT;
  public static final byte[] CX = OpCode.CMP_CX_SHORT;
  public static final byte[] DX = OpCode.CMP_DX_SHORT;
  public static final byte[] BX = OpCode.CMP_BX_SHORT;

  public CmpRegShort(SegmentDefinition def) {
    super(def);
    validateRegister(register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] op = operation(register());
    byte[] b = StreamTools.readBytes(in, op.length);
    validate(b, op);

    value = StreamTools.readShort(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(operation(register()));         // 2 or 3
    out.write(DataTools.toByte(value, true)); // 2
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(operation(register()));                   // 2 or 3
    out.write(DataTools.toByte(defaultValue(), true));  // 2
  }

  @Override
  public int getSize() {
    return getSize(register());
  }

  public static int getSize(Register register) {
    return operation(register).length + 2;
  }

  private static byte[] operation(Register register) {
    switch (register) {
      case AX:
        return AX;
      case CX:
        return CX;
      case DX:
        return DX;
      case BX:
        return BX;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }

  private static void validateRegister(Register register) {
    operation(register);
  }
}
