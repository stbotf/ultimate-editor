package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Plain jump or call instruction.
 */
public class Jmp extends IntValueSegment {

  public static final int SIZE = 5;

  // for segment definitions specify as flags
  public static final byte CALL  = OpCode.CALL;
  public static final byte JMP   = OpCode.JMP;

  public Jmp(SegmentDefinition def) {
    this(def.address, def);
  }

  public Jmp(int address, SegmentDefinition def) {
    super(address, def);
  }

  public byte operation() {
    return definition.hasFlags() ? (byte)definition.flags() : JMP;
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, operation());
    value = StreamTools.readInt(in, true);
    if (definition.hasOffset())
      validate(value, definition.offset());

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(operation());                   // 1
    out.write(DataTools.toByte(value, true)); // 4
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(operation());                             // 1
    out.write(DataTools.toByte(defaultValue(), true));  // 4
  }
}
