package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.val;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.value.IntValue;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Integer value segment base class.
 */
public class IntValueSegment extends ValueSegment implements IntValue {

  protected int value;
  protected boolean isFloat;

  public IntValueSegment(SegmentDefinition def) {
    this(def.getAddress(), def);
  }

  public IntValueSegment(int address, SegmentDefinition def) {
    super(address, def);
    val orig = definition().defaultValue();
    isFloat = orig instanceof Float;
    if (!isFloat && !(orig instanceof Integer))
      throw new IllegalArgumentException();
  }

  @Override
  public boolean isFloatingPoint() {
    return isFloat;
  }

  @Override
  public void load(InputStream in) throws IOException {
    value = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(value, true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(DataTools.toByte(defaultValue(), true));
  }

  @Override public short shortValue() { return (short)value; }
  @Override public int   intValue()   { return value; }
  @Override public long  longValue()  { return value; }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    definition().validate(this, value);
    return forceValue(value);
  }

  @Override
  public void setShortValue(short value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public void setIntValue(int value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public void setLongValue(long value) throws InvalidArgumentsException {
    if (value > Integer.MAX_VALUE || value < Integer.MIN_VALUE)
      throw new InvalidArgumentsException();
    setValue((int)value);
  }

  public boolean forceValue(int value) {
    if (this.value != value) {
      this.value = value;
      markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean isDefault() {
    return value == defaultValue();
  }

  public int defaultValue() {
    return isFloat ? Float.floatToIntBits((Float)definition().defaultValue())
      : (Integer)definition().defaultValue();
  }

  @Override
  public void reset() {
    forceValue(defaultValue());
  }

  @Override
  public void clear() {
    value = 0;
    markChanged();
  }

  public void validate(int value) throws IOException {
    validate(this.value, value);
  }

  public void validateFloat(float value) throws IOException {
    validate(getFloat(), value);
  }

  public void validateDefault() throws IOException {
    validate(this.value, defaultValue());
  }

  @Override
  public String toString() {
    return isFloat ? Float.toString(getFloat()) : Integer.toString(value);
  }
}
