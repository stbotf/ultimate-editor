package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;

public class LeaMemOffset extends SignedByteValueSegment {

  public static final int SIZE = 3;

  // flags
  public static final int FROM_EBP = 0;
  public static final int FROM_ECX = 1;

  private static final byte[] eax_from_ebp = new byte[]{OpCode.LEA, (byte) 0x45};
  private static final byte[] eax_from_ecx = new byte[]{OpCode.LEA, (byte) 0x41};

  public LeaMemOffset(SegmentDefinition def) {
    super(def);
    // validate register and flags
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.register(), definition.flags());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());
    super.load(in);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());
    out.write(value);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());
    out.write(defaultValue());
  }

  @Override
  public int getSize() {
    return getSize(register());
  }

  public static int getSize(Register register) {
    switch (register) {
      case EAX:
        return SIZE;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }

  public byte[] opCode(Register register, int flags) {
    switch (register) {
      case EAX:
        switch (flags) {
          case FROM_EBP:
            return eax_from_ebp;
          case FROM_ECX:
            return eax_from_ecx;
          default:
            throw new UnsupportedOperationException("Invalid flags: " + flags);
        }
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
