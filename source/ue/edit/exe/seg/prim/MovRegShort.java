package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Move short value to register instruction.
 */
public class MovRegShort extends ShortValueSegment {

  public static final int SIZE = 4;

  public static final byte[] AX = OpCode.MOV_AX;
  public static final byte[] CX = OpCode.MOV_CX;
  public static final byte[] DX = OpCode.MOV_DX;
  public static final byte[] BX = OpCode.MOV_BX;
  public static final byte[] SP = OpCode.MOV_SP;
  public static final byte[] BP = OpCode.MOV_BP;
  public static final byte[] SI = OpCode.MOV_SI;
  public static final byte[] DI = OpCode.MOV_DI;

  public MovRegShort(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public MovRegShort(int address, SegmentDefinition def) {
    super(address, def);
    // validate register
    opCode();
  }

  public byte[] opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, opCode());                   // 2

    // value
    value = StreamTools.readShort(in, true);  // 2
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());                      // 2
    out.write(DataTools.toByte(value, true)); // 2
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());                                // 2
    out.write(DataTools.toByte(defaultValue(), true));  // 2
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  private static byte[] opCode(Register register) {
    switch (register) {
      case AX:
        return AX;
      case CX:
        return CX;
      case DX:
        return DX;
      case BX:
        return BX;
      case SP:
        return SP;
      case BP:
        return BP;
      case SI:
        return SI;
      case DI:
        return DI;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
