package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Move int value to register instruction.
 */
public class MovRegInt extends IntValueSegment {

  public static final int SIZE = 5;

  public static final byte EAX = OpCode.MOV_EAX;
  public static final byte ECX = OpCode.MOV_ECX;
  public static final byte EDX = OpCode.MOV_EDX;
  public static final byte EBX = OpCode.MOV_EBX;
  public static final byte EBP = OpCode.MOV_EBP;
  public static final byte ESI = OpCode.MOV_ESI;
  public static final byte EDI = OpCode.MOV_EDI;

  public MovRegInt(SegmentDefinition def) {
    super(def);
    // validate register
    opCode();
  }

  public MovRegInt(int address, SegmentDefinition def) {
    super(address, def);
    // validate register
    opCode();
  }

  public byte opCode() {
    return opCode(definition.register());
  }

  @Override
  public void load(InputStream in) throws IOException {
    // register
    validate(in, opCode());                   // 1

    // value
    value = StreamTools.readInt(in, true);    // 4
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(opCode());                      // 1
    out.write(DataTools.toByte(value, true)); // 4
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(opCode());                                // 1
    out.write(DataTools.toByte(defaultValue(), true));  // 4
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  private static byte opCode(Register register) {
    switch (register) {
      case EAX:
        return EAX;
      case ECX:
        return ECX;
      case EDX:
        return EDX;
      case EBX:
        return EBX;
      case EBP:
        return EBP;
      case ESI:
        return ESI;
      case EDI:
        return EDI;
      default:
        throw new UnsupportedOperationException("Invalid register: " + register);
    }
  }
}
