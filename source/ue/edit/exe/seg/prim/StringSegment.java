package ue.edit.exe.seg.prim;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.StringDefinition;
import ue.edit.value.StringValue;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

/**
 * Predefined string segment implementation.
 */
public class StringSegment extends InternalSegment implements StringValue {

  @Getter @Accessors(fluent = true)
  protected String value;

  public StringSegment(SegmentDefinition def) {
    this(def.address, def);
  }

  public StringSegment(int address, SegmentDefinition def) {
    super(address, def);
    if (!(def instanceof StringDefinition))
      throw new IllegalArgumentException();
  }

  @Override
  public StringDefinition definition() {
    return (StringDefinition) definition;
  }

  @Override
  public boolean isDefault() {
    return definition().value().equals(value);
  }

  public String defaultValue() {
    return definition().value();
  }

  @Override
  public boolean setValue(String value) {
    if (!StringTools.equals(this.value, value)) {
      this.value = value;
      markChanged();
      return true;
    }
    return false;
  }

  @Override
  public void reset() {
    value = defaultValue();
  }

  @Override
  public int getSize() {
    return definition.size();
  }

  public boolean check(String value) {
    return defaultValue().equals(value);
  }

  public void validate(String value) throws IOException {
    validate(value, defaultValue());
  }

  @Override
  public void load(InputStream in) throws IOException {
    value = StreamTools.readNullTerminatedBotfString(in, definition.size());
  }

  @Override
  public void load(byte[] data) throws IOException {
    value = DataTools.fromNullTerminatedBotfString(data, 0, definition.size());
  }

  @Override
  public void load(byte[] data, int offset) throws IOException {
    value = DataTools.fromNullTerminatedBotfString(data, offset, definition.size());
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(value, definition.size(), definition().length()));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(DataTools.toByte(defaultValue(), definition.size(), definition().length()));
  }

  @Override
  public void clear() {
    value = defaultValue();
  }

  @Override
  public void check(Vector<String> response) {
  }

  // overwrite toString for the set segment value menu
  @Override
  public String toString() {
    return value;
  }

}
