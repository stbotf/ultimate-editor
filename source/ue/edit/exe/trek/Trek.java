package ue.edit.exe.trek;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;
import java.util.function.Predicate;
import java.util.stream.Stream;
import com.google.common.io.BaseEncoding;
import lombok.Getter;
import lombok.val;
import lombok.var;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.DataFile;
import ue.edit.common.FileLookup;
import ue.edit.common.IDataFile.PatchStatus;
import ue.edit.common.InternalFile;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.SegmentInfo;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.patch.bld.BuildingCountPatch;
import ue.edit.exe.trek.patch.tec.TechLevelsPatch;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.seg.bld.MainBldUpgrades;
import ue.edit.exe.trek.seg.bld.MinorRaceMainBldIDLimits;
import ue.edit.exe.trek.seg.bld.PunyFactories;
import ue.edit.exe.trek.seg.bld.PunyFarms;
import ue.edit.exe.trek.seg.emp.AIShpSets;
import ue.edit.exe.trek.seg.emp.AlienStartingID;
import ue.edit.exe.trek.seg.emp.BaseMapRanges;
import ue.edit.exe.trek.seg.emp.EmpireImgPrefixes;
import ue.edit.exe.trek.seg.emp.MEStartingConditions;
import ue.edit.exe.trek.seg.emp.MapRangeTechBonuses;
import ue.edit.exe.trek.seg.emp.MinorRacesNumbers;
import ue.edit.exe.trek.seg.emp.RaceList;
import ue.edit.exe.trek.seg.emp.RaceListAddresses;
import ue.edit.exe.trek.seg.map.PlanetBonusTypes;
import ue.edit.exe.trek.seg.shp.ScreensaverModelList;
import ue.edit.exe.trek.seg.shp.SelectPhaserHobFile;
import ue.edit.exe.trek.seg.shp.SelectPlasmaHobFile;
import ue.edit.exe.trek.seg.shp.SelectTorpedoHobFile;
import ue.edit.exe.trek.seg.shp.ShipModelSegment;
import ue.edit.exe.trek.seg.shp.ShipnameCode;
import ue.edit.exe.trek.seg.shp.SpecialShips;
import ue.edit.exe.trek.task.TrekCheckTask;
import ue.edit.exe.trek.task.TrekSaveTask;
import ue.edit.mod.ModList;
import ue.edit.value.ValueType;
import ue.exception.DataException;
import ue.gui.menu.TrekMenu;
import ue.gui.util.GuiTools;
import ue.service.FileManager;
import ue.service.Language;
import ue.util.data.HexTools;
import ue.util.func.FunctionTools.CheckedFunction;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of the trek.exe file, it communicates with other classes in the
 * edit.exe.trek sub-packages. Also it sends MainPanel instances to use for editing the file. Unlike
 * other files this one deosn't have a mod list. The reason for this is because changing trek.exe's
 * size would break things.
 */
public class Trek extends FileLookup {

  public static final int TYPE = FileManager.P_TREK;

  // known trek.exe constants
  private static final int TREK_SIZE = 0x1B2E00; // 1781248
  private static final int TREK_STBOF_POS = 0x173E38; // 1523256
  private static final String TREK_HASH = "A6F686CBA6D3B7936B90103A41F92173";

  @Getter @Accessors(fluent = true)
  private final TrekMenu menu = new TrekMenu();

  // Sorted maps on all the loaded segments. The default sorting is by address
  // since it is the natural segment order and allows to check on segment overlappings.
  private final TreeMap<Integer, InternalSegment> segments = new TreeMap<>();
  private final TreeMap<String, InternalSegment> segmentsByName = new TreeMap<>();

  private BuildingCountPatch buildingCount = null;
  private TechLevelsPatch techLevels = null;

  @Override
  public int type() {
    return TYPE;
  }

  public BuildingCountPatch buildingCount() throws IOException {
    if (buildingCount == null) {
      buildingCount = new BuildingCountPatch(this);
      buildingCount.load();
    }
    return buildingCount;
  }

  public TechLevelsPatch techLevels() throws IOException {
    if (techLevels == null) {
      techLevels = new TechLevelsPatch(this);
      techLevels.load();
    }
    return techLevels;
  }

  /**
   * Make sure file is actually an InternalSegment object or this function will ignore your call.
   * @throws FileAlreadyExistsException
   */
  public void addSegment(InternalSegment file) throws FileAlreadyExistsException {
    validateUnused(file.address(), file.getSize());
    segments.put(file.address(), file);
    segmentsByName.put(file.getName(), file);
    markChanged();
  }

  /**
   * Discards loaded internal segments.
   */
  @Override
  public void discard() {
    super.discard();
    segments.clear();
    segmentsByName.clear();
    clearShortHands();
  }

  /**
   * Call this function to discard single file changes.
   */
  @Override
  public void discard(int address) {
    val seg = segments.remove(address);
    if (seg != null) {
      String segName = seg.getName();
      super.discard(segName);
      segmentsByName.remove(segName);
      clearShortHand(segName);
    } else {
      var def = CTrekSegments.AllSegments.get(address);
      if (def != null)
        super.discard(def.name());
    }
  }

  @Override
  public void discard(String segmentName) {
    super.discard(segmentName);

    val seg = segmentsByName.remove(segmentName);
    if (seg != null) {
      segments.remove(seg.address());
      clearShortHand(segmentName);
    }
  }

  @Override
  public void discard(FilenameFilter filter) {
    if (filter == null) {
      discard();
      return;
    }

    segments.values().removeIf(x -> {
      String segName = x.getName();
      if (filter.accept(sourceFile, segName)) {
        super.discard(segName);
        segmentsByName.remove(segName);
        clearShortHand(segName);
        return true;
      }
      return false;
    });
  }

  private void clearShortHands() {
    buildingCount = null;
    techLevels = null;
  }

  private void clearShortHand(String segmentName) {
    if (buildingCount != null && buildingCount.hasSegment(segmentName))
      buildingCount = null;
    if (techLevels != null && techLevels.hasSegment(segmentName))
      techLevels = null;
  }

  @Override
  public boolean hasInternalFile(String fileName) {
    return isCached(fileName) || Arrays.stream(CTrekSegments.SEGMENTLIST)
      .anyMatch(x -> x.equalsIgnoreCase(fileName));
  }

  /**
   * List available internal segments, sorted by name.
   * @return list of segment names
   */
  @Override
  public Collection<String> getFileNames() {
    // use TreeSet to sort segments by name and skip duplicates
    TreeSet<String> names = new TreeSet<String>();
    Collections.addAll(names, CTrekSegments.SEGMENTLIST);
    processCachedFiles(x -> names.add(x.getName()));
    return names;
  }

  /**
   * List available internal segments, sorted by name.
   * @return list of segment names
   */
  @Override
  public Collection<String> getFileNames(FilenameFilter filter) {
    // first collect all file names to remove duplicates
    val names = getFileNames();
    // then remove any names that don't match the provided filter
    if (filter != null)
      names.removeIf(x -> !filter.accept(sourceFile, x));
    return names;
  }

  /**
   * List trek.exe segment details, sorted by segment address - the trek.exe file offset.
   * @param checkData whether to double-check the data for sneaked in changes
   * @return list of available internal segment details.
   */
  public Set<SegmentInfo> listSegments(boolean checkData) {
    // use TreeSet to sort segments by segment address and skip duplicates
    TreeSet<SegmentInfo> files = new TreeSet<>(Comparator.comparingInt(SegmentInfo::getAddress));

    // check cached segments first to determine the status
    processCachedSegments(x -> files.add(SegmentInfo.builder()
      .address(x.address())
      .name(x.getName())
      .classType(x.getClass())
      .definition(x.definition())
      .size(x.getSize())
      .status(getSegmentStatus(x, checkData))
      .value(getSegmentValue(x))
      .build()
    ));

    // collect further segments known from segment list
    Arrays.stream(CTrekSegments.SEGMENTLIST).forEach(x -> {
      String err = getLoadError(x);
      files.add(SegmentInfo.builder()
        .name(x)
        .address(getSegmentAddress(x))
        .classType(getSegmentType(x))
        .size(getInternalFileSize(x))
        .status(err != null ? PatchStatus.Invalid : PatchStatus.Unloaded)
        .error(err)
        .build());
      });

    // collect further segments from segment definitions
    CTrekSegments.AllSegments.values().stream().forEach(x -> {
      String err = getLoadError(x.name());
      files.add(SegmentInfo.builder()
        .address(x.address)
        .name(x.name())
        .classType(x.type)
        .definition(x)
        .size(x.size())
        .status(err != null ? PatchStatus.Invalid : PatchStatus.Unloaded)
        .error(err)
        .build());
    });

    return files;
  }

  private PatchStatus getSegmentStatus(InternalSegment seg, boolean checkData) {
    PatchStatus status = seg.status();

    if (checkData && status != PatchStatus.Sneaked && !seg.madeChanges()) {
      try {
        // detect sneaked in changes of auto-applied patches
        byte[] orig = getRawData(seg.address(), seg.getSize());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        seg.save(out);
        byte[] cached = out.toByteArray();

        if (!Arrays.equals(orig, cached)) {
          // remember it has sneaked changes
          seg.markSneaked();
          markSneaked();
          status = PatchStatus.Sneaked;
        }
      }
      catch (IOException e) {
        e.printStackTrace();
        status = PatchStatus.Invalid;
      }
    }

    return status;
  }

  private Object getSegmentValue(InternalSegment seg) {
    if (seg instanceof ValueType)
      return ((ValueType)seg).value();
    return null;
  }

  @Override
  public Collection<InternalSegment> listCachedFiles() {
    return Collections.unmodifiableCollection(segments.values());
  }

  @Override
  public Stream<InternalSegment> streamCachedFiles() {
    return segments.values().stream();
  }

  @Override
  public void processCachedFiles(Predicate<DataFile> callback) {
    processCachedSegments(x -> callback.test(x));
  }

  @Override
  public void processCachedFiles(FilenameFilter filter, Predicate<DataFile> callback) {
    processCachedSegments(filter, x -> callback.test(x));
  }

  public void processCachedSegments(Predicate<InternalSegment> callback) {
    //GO THRU LOADED SEGMENTS
    for (val seg : segments.values()) {
      if (!callback.test(seg))
        break;
    }
  }

  /**
   * Process all cached segments matching the filter and predicate.
   * @param filter segment name filter, can be null
   */
  public void processCachedSegments(FilenameFilter filter, Predicate<InternalSegment> callback) {
    //GO THRU LOADED SEGMENTS
    for (val seg : segments.values()) {
      if (filter != null && !filter.accept(sourceFile, seg.getName()))
        continue;
      if (!callback.test(seg))
        break;
    }
  }

  @Override
  public int getLoadedFileCount() {
    return segments.size();
  }

  /**
   * Returns a list of loaded internal segments.
   */
  @Override
  public Set<String> cachedFiles() {
    return segmentsByName.keySet();
  }

  public void updateModList() {
    // ignore
  }

  /**
   * Saves changes to the specified file. This creates a new thread with low priority (so it won't
   * eat all resources) and shows a dialog box with the option to cancel the operation.
   *
   * @param dest The file to which changes are to be saved. If the file already exists, it is
   *             overwritten without confirmation
   * @return true if succesful else false.
   */
  @Override
  public boolean save(File dest, boolean backup, String extension) {
    // if not specified, save to the open file
    if (dest == null)
      dest = getTargetFile();

    val saveTask = new TrekSaveTask(this, dest, backup, extension);
    boolean success = GuiTools.runUEWorker(saveTask);

    if (success) {
      // keep cached files
      super.discard();
      sourceFile = dest;
    }

    return success;
  }

  /**
   * Opens the specified file and tests it, if it is a valid trek.exe file.
   * Should anything be wrong an error is reported.
   *
   * @param file The file to open. Must be a valid stbof.res file!
   * @throws IOException
   */
  @Override
  public void open(File file) throws IOException {
    sourceFile = null;
    discard();

    // check size
    if (file.length() < TREK_STBOF_POS + 9)
      throw new IOException(Language.getString("Trek.0")); //$NON-NLS-1$

    try (FileInputStream in = new FileInputStream(file)) {
      StreamTools.skip(in, TREK_STBOF_POS);

      byte[] RES_NAME = "STBOF.RES".getBytes();
      byte[] data = StreamTools.readBytes(in, RES_NAME.length);

      if (!Arrays.equals(data, RES_NAME))
        throw new IOException(Language.getString("Trek.0")); //$NON-NLS-1$

      sourceFile = file;
    }
  }

  public int getSegmentAddress(String segName) {
    switch (segName) {
      case CTrekSegments.MEStartCond:
        return MEStartingConditions.SEGMENT_ADDRESS;
      case CTrekSegments.AlienStartingId:
        return AlienStartingID.ID1_SEGMENT_ADDRESS;
      case CTrekSegments.AlienStartingId2:
        return AlienStartingID.ID2_SEGMENT_ADDRESS;
      case CTrekSegments.AlienStartingId3:
        return AlienStartingID.ID3_SEGMENT_ADDRESS;
      case CTrekSegments.AlienStartingId4:
        return AlienStartingID.ID4_SEGMENT_ADDRESS;
      case CTrekSegments.SpecialShips:
        return SpecialShips.SEGMENT_ADDRESS;
      case CTrekSegments.ShipModelSegment:
        return ShipModelSegment.SEGMENT_ADDRESS;
      case CTrekSegments.ScreensaverModelList:
        return ScreensaverModelList.SEGMENT_ADDRESS;
      case CTrekSegments.RaceList:
        return RaceList.SEGMENT_ADDRESS;
      case CTrekSegments.BaseMapRanges:
        return BaseMapRanges.SEGMENT_ADDRESS;
      case CTrekSegments.MapRangeBonuses:
        return MapRangeTechBonuses.SEGMENT_ADDRESS;
      case CTrekSegments.RaceListAddresses:
        return RaceListAddresses.SEGMENT_ADDRESS;
      case CTrekSegments.PunyFarms:
        return PunyFarms.SEGMENT_ADDRESS;
      case CTrekSegments.PunyFactories:
        return PunyFactories.SEGMENT_ADDRESS;
      case CTrekSegments.AIShipSets:
        return AIShpSets.SEGMENT_ADDRESS;
      case CTrekSegments.MinMinorsNumber:
        return MinorRacesNumbers.MIN_MINORS_SEGMENT_ADDRESS;
      case CTrekSegments.MaxMinorsNumber:
        return MinorRacesNumbers.MAX_MINORS_SEGMENT_ADDRESS;
      case CTrekSegments.EmpireImagePrefixes:
        return EmpireImgPrefixes.SEGMENT_ADDRESS;
      case CTrekSegments.MinorMainBldIds:
        return MinorRaceMainBldIDLimits.SEGMENT_ADDRESS;
      case CTrekSegments.MainBldUpgrades:
        return MainBldUpgrades.SEGMENT_ADDRESS;
      case CTrekSegments.PlanetBonusTypes:
        return PlanetBonusTypes.SEGMENT_ADDRESS;
      case CTrekSegments.ShipnameCode:
        return ShipnameCode.SEGMENT_ADDRESS;
      case CTrekSegments.SelectPlasmaHobFile:
        return SelectPlasmaHobFile.SEGMENT_ADDRESS;
      case CTrekSegments.SelectPhaserHobFile:
        return SelectPhaserHobFile.SEGMENT_ADDRESS;
      case CTrekSegments.SelectTorpedoHobFile:
        return SelectTorpedoHobFile.SEGMENT_ADDRESS;
      default:
        throw new UnsupportedOperationException("Not supported yet.");
    }
  }

  @Override
  public int getInternalFileSize(String filename) {
    switch (filename) {
      case CTrekSegments.MEStartCond:
        return MEStartingConditions.SIZE;
      case CTrekSegments.AlienStartingId:
      case CTrekSegments.AlienStartingId2:
      case CTrekSegments.AlienStartingId3:
      case CTrekSegments.AlienStartingId4:
        return AlienStartingID.SIZE;
      case CTrekSegments.SpecialShips:
        return SpecialShips.SIZE;
      case CTrekSegments.ShipModelSegment:
      case CTrekSegments.ScreensaverModelList:
        return readFileSize(filename);
      case CTrekSegments.RaceList:
        return RaceList.SIZE;
      case CTrekSegments.BaseMapRanges:
        return BaseMapRanges.SIZE;
      case CTrekSegments.MapRangeBonuses:
        return MapRangeTechBonuses.SIZE;
      case CTrekSegments.RaceListAddresses:
        return RaceListAddresses.DEFAULT_SIZE;
      case CTrekSegments.PunyFarms:
        return PunyFarms.SIZE;
      case CTrekSegments.PunyFactories:
        return PunyFactories.SIZE;
      case CTrekSegments.AIShipSets:
        return AIShpSets.SIZE;
      case CTrekSegments.MinMinorsNumber:
      case CTrekSegments.MaxMinorsNumber:
        return MinorRacesNumbers.SIZE;
      case CTrekSegments.EmpireImagePrefixes:
        return EmpireImgPrefixes.SIZE;
      case CTrekSegments.MinorMainBldIds:
        return MinorRaceMainBldIDLimits.SIZE;
      case CTrekSegments.MainBldUpgrades:
        return MainBldUpgrades.SIZE;
      case CTrekSegments.PlanetBonusTypes:
        return PlanetBonusTypes.SIZE;
      case CTrekSegments.ShipnameCode:
        return ShipnameCode.SIZE;
      case CTrekSegments.SelectPlasmaHobFile:
        return SelectPlasmaHobFile.SIZE;
      case CTrekSegments.SelectPhaserHobFile:
        return SelectPhaserHobFile.SIZE;
      case CTrekSegments.SelectTorpedoHobFile:
        return SelectTorpedoHobFile.SIZE;
      default:
        throw new UnsupportedOperationException("Not supported yet.");
    }
  }

  private int readFileSize(String fileName) {
    InternalFile seg = getCachedFile(fileName);
    return seg != null ? seg.getSize() : 0;
  }

  /**
   * Returns an internal segment by segment definition.
   *
   * @param def     the segment definition
   * @param cache   whether to cache the segment
   * @return internal segment
   * @throws IOException
   */
  public InternalSegment getSegment(SegmentDefinition def, boolean cache) throws IOException {
    return loadSegment(def, cache, false);
  }

  public InternalSegment tryGetSegment(SegmentDefinition def, boolean cache) {
    try {
      return loadSegment(def, cache, false);
    }
    catch (IOException e) {
      // ignore read errors but print stack trace
      e.printStackTrace();
      return null;
    }
  }

  public InternalSegment[] getSegments(SegmentDefinition[] defs, boolean cache) throws IOException {
    val segs = new InternalSegment[defs.length];
    for (int i = 0; i < defs.length; ++i)
      segs[i] = getSegment(defs[i], cache);
    return segs;
  }

  public InternalSegment reloadSegment(SegmentDefinition def, boolean cache) throws IOException {
    return loadSegment(def, cache, true);
  }

  public void loadAllSegments() throws IOException {
    // load all segments from segment list
    for (String segName : CTrekSegments.SEGMENTLIST) {
      loadSegment(segName, -1, true, false);
    }

    // load all segments from segment definitions
    for (SegmentDefinition sd : CTrekSegments.AllSegments.values()) {
      loadSegment(sd, true, false);
    }
  }

  // used to update the segment status for all the segments in segments gui
  public void loadReadableSegments() {
    // load all segments from segment list
    for (String segName : CTrekSegments.SEGMENTLIST) {
      try {
        loadSegment(segName, -1, true, false);
      } catch(Exception e) {
        // logged by the load call
      }
    }

    // load all segments from segment definitions
    for (SegmentDefinition sd : CTrekSegments.AllSegments.values()) {
      try {
        loadSegment(sd, true, false);
      } catch(Exception e) {
        // logged by the load call
      }
    }
  }

  private InternalSegment loadSegment(SegmentDefinition def, boolean cache, boolean reload) throws IOException {
    try {
      val seg = loadSegmentImpl(def, cache, reload);
      removeLoadError(def.name());
      return seg;
    } catch (Exception e) {
      addLoadError(def.name(), e instanceof DataException ? e.getMessage() : e.toString());
      e.printStackTrace();
      throw e;
    }
  }

  private InternalSegment loadSegmentImpl(SegmentDefinition def, boolean cache, boolean reload) throws IOException {
    if (def.address < 0 || def.size() < -1) {
      String msg = Language.getString("Trek.4"); //$NON-NLS-1$
      msg = msg.replace("%1", "0x" + Integer.toHexString(def.address)); //$NON-NLS-1$ //$NON-NLS-2$
      throw new IllegalArgumentException(msg);
    }

    if (def.address + def.size() > sourceFile.length()) {
      String msg = Language.getString("Trek.3"); //$NON-NLS-1$
      msg = msg.replace("%1", "0x" + Integer.toHexString(def.address)); //$NON-NLS-1$ //$NON-NLS-2$
      throw new FileNotFoundException(msg);
    }

    InternalSegment seg = loadSegmentImpl(def.name(), def.address, cache, reload, x -> {
      try {
        return (InternalSegment) def.type.getConstructor(SegmentDefinition.class).newInstance(def);
      }
      catch (Exception e) {
        String msg = Language.getString("Trek.4"); //$NON-NLS-1$
        msg = msg.replace("%1", "0x" + Integer.toHexString(def.address)); //$NON-NLS-1$ //$NON-NLS-2$
        throw new IOException(msg, e);
      }
    });

    if (!def.type.isInstance(seg)) {
      String err = Language.getString("Trek.16"); //$NON-NLS-1$
      err = err.replace("%1", seg.getName()); //$NON-NLS-1$
      err = err.replace("%2", seg.getClass().getSimpleName()); //$NON-NLS-1$
      err = err.replace("%3", def.type.getSimpleName()); //$NON-NLS-1$
      throw new FileNotFoundException(err);
    } else if (def.size() != -1 && seg.getSize() != def.size()) {
      // this is bad because merging/splitting segments won't work since some segments
      // aren't generic
      // so lets make sure we throw an ugly error if we mess up the size
      // this still doesn't protect against mocking around with non generic segments
      // but if they are changed via a generic segment (very bad idea), saving will
      // fail
      String err = Language.getString("Trek.17"); //$NON-NLS-1$
      err = err.replace("%1", seg.getName()); //$NON-NLS-1$
      err = err.replace("%3", Integer.toString(seg.getSize())); //$NON-NLS-1$
      err = err.replace("%2", Integer.toString(def.size())); //$NON-NLS-1$
      throw new FileNotFoundException(err);
    }

    return seg;
  }

  @Override
  public InternalFile getCachedFile(String fileName) {
    return segmentsByName.get(fileName);
  }

  /**
   * Returns the internal segment.
   * @throws IOException when the segment is not known or trek.exe can't be read
   */
  @Override
  public InternalSegment getInternalFile(String segName, boolean cache) throws IOException {
    return loadSegment(segName, -1, cache, false);
  }

  @Override
  public List<InternalFile> getInternalFiles(Collection<String> fileNames, boolean cache,
    Optional<Predicate<String>> cbProgress) throws IOException {
    return loadFiles(fileNames, cache, false, cbProgress);
  }

  /**
   * Returns the internal segment or null if missing.
   * @param fileName the name of the file
   */
  @Override
  public InternalSegment tryGetInternalFile(String fileName, boolean cache) {
    try {
      return loadSegment(fileName, -1, cache, false);
    }
    catch (IOException e) {
      // ignore read errors but print stack trace
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public List<InternalFile> tryGetInternalFiles(Collection<String> fileNames, boolean cache,
    Optional<Predicate<String>> cbProgress) {
    try {
      return loadFiles(fileNames, cache, false, cbProgress);
    }
    catch (IOException e) {
      // ignore read errors but print stack trace
      e.printStackTrace();
      return new ArrayList<>();
    }
  }

  /**
   * Attention, getRawData skips any cached changes!
   */
  @Override
  public Optional<byte[]> getRawData(String segName) throws IOException {
    int addr = getSegmentAddress(segName);
    int size = getInternalFileSize(segName);
    return Optional.of(getRawData(addr, size));
  }

  /**
   * Attention, getRawData skips any cached changes!
   */
  public byte[] getRawData(int addr, int size) throws IOException {
    try (FileInputStream fis = new FileInputStream(sourceFile)){
      StreamTools.skip(fis, addr);
      return StreamTools.readBytes(fis, size);
    }
  }

  /**
   * Enforces to reload the internal segment.
   * @return the loaded or reloaded internal segment
   * @throws IOException
   */
  @Override
  public InternalFile reloadFile(String segName, boolean cache) throws IOException {
    InternalFile file = loadSegment(segName, -1, cache, true);
    if (file == null) {
      String msg = Language.getString("Trek.3"); //$NON-NLS-1$
      msg = msg.replace("%1", segName); //$NON-NLS-1$
      throw new FileNotFoundException(msg);
    }
    return file;
  }

  @Override
  public List<InternalFile> reloadFiles(Collection<String> fileNames, boolean cache) throws IOException {
    return loadFiles(fileNames, cache, true, Optional.empty());
  }

  private InternalSegment createSegment(String segName) throws IOException {
    InternalSegment a = null;

    switch (segName) {
      case CTrekSegments.MEStartCond:
        a = new MEStartingConditions();
        break;
      case CTrekSegments.AlienStartingId:
        a = new AlienStartingID(AlienStartingID.ID1_SEGMENT_ADDRESS);
        break;
      case CTrekSegments.AlienStartingId2:
        a = new AlienStartingID(AlienStartingID.ID2_SEGMENT_ADDRESS);
        break;
      case CTrekSegments.AlienStartingId3:
        a = new AlienStartingID(AlienStartingID.ID3_SEGMENT_ADDRESS);
        break;
      case CTrekSegments.AlienStartingId4:
        a = new AlienStartingID(AlienStartingID.ID4_SEGMENT_ADDRESS);
        break;
      case CTrekSegments.SpecialShips:
        a = new SpecialShips(this);
        break;
      case CTrekSegments.ShipModelSegment:
        a = new ShipModelSegment(UE.FILES.stbof());
        break;
      case CTrekSegments.ScreensaverModelList:
        a = new ScreensaverModelList();
        break;
      case CTrekSegments.RaceList:
        a = new RaceList(this);
        break;
      case CTrekSegments.BaseMapRanges:
        a = new BaseMapRanges();
        break;
      case CTrekSegments.MapRangeBonuses:
        a = new MapRangeTechBonuses(this);
        break;
      case CTrekSegments.RaceListAddresses:
        a = new RaceListAddresses();
        break;
      case CTrekSegments.PunyFarms:
        a = new PunyFarms();
        break;
      case CTrekSegments.PunyFactories:
        a = new PunyFactories();
        break;
      case CTrekSegments.AIShipSets:
        a = new AIShpSets();
        break;
      case CTrekSegments.MinMinorsNumber:
        a = new MinorRacesNumbers(MinorRacesNumbers.MIN_MINORS_SEGMENT_ADDRESS);
        break;
      case CTrekSegments.MaxMinorsNumber:
        a = new MinorRacesNumbers(MinorRacesNumbers.MAX_MINORS_SEGMENT_ADDRESS);
        break;
      case CTrekSegments.EmpireImagePrefixes:
        a = new EmpireImgPrefixes();
        break;
      case CTrekSegments.MinorMainBldIds:
        a = new MinorRaceMainBldIDLimits(UE.FILES.stbof());
        break;
      case CTrekSegments.MainBldUpgrades:
        a = new MainBldUpgrades();
        break;
      case CTrekSegments.PlanetBonusTypes:
        a = new PlanetBonusTypes();
        break;
      case CTrekSegments.ShipnameCode:
        a = new ShipnameCode(this);
        break;
      case CTrekSegments.SelectPlasmaHobFile:
        a = new SelectPlasmaHobFile();
        break;
      case CTrekSegments.SelectPhaserHobFile:
        a = new SelectPhaserHobFile();
        break;
      case CTrekSegments.SelectTorpedoHobFile:
        a = new SelectTorpedoHobFile();
        break;
      default:
        throw new UnsupportedOperationException("Not supported yet.");
    }

    a.setName(segName);
    return a;
  }

  private Class<?> getSegmentType(String segName) {
    switch (segName) {
      case CTrekSegments.MEStartCond:
        return MEStartingConditions.class;
      case CTrekSegments.AlienStartingId:
      case CTrekSegments.AlienStartingId2:
      case CTrekSegments.AlienStartingId3:
      case CTrekSegments.AlienStartingId4:
        return AlienStartingID.class;
      case CTrekSegments.SpecialShips:
        return SpecialShips.class;
      case CTrekSegments.ShipModelSegment:
        return ShipModelSegment.class;
      case CTrekSegments.ScreensaverModelList:
        return ScreensaverModelList.class;
      case CTrekSegments.RaceList:
        return RaceList.class;
      case CTrekSegments.BaseMapRanges:
        return BaseMapRanges.class;
      case CTrekSegments.MapRangeBonuses:
        return MapRangeTechBonuses.class;
      case CTrekSegments.RaceListAddresses:
        return RaceListAddresses.class;
      case CTrekSegments.PunyFarms:
        return PunyFarms.class;
      case CTrekSegments.PunyFactories:
        return PunyFactories.class;
      case CTrekSegments.AIShipSets:
        return AIShpSets.class;
      case CTrekSegments.MinMinorsNumber:
      case CTrekSegments.MaxMinorsNumber:
        return MinorRacesNumbers.class;
      case CTrekSegments.EmpireImagePrefixes:
        return EmpireImgPrefixes.class;
      case CTrekSegments.MinorMainBldIds:
        return MinorRaceMainBldIDLimits.class;
      case CTrekSegments.MainBldUpgrades:
        return MainBldUpgrades.class;
      case CTrekSegments.PlanetBonusTypes:
        return PlanetBonusTypes.class;
      case CTrekSegments.ShipnameCode:
        return ShipnameCode.class;
      case CTrekSegments.SelectPlasmaHobFile:
        return SelectPlasmaHobFile.class;
      case CTrekSegments.SelectPhaserHobFile:
        return SelectPhaserHobFile.class;
      case CTrekSegments.SelectTorpedoHobFile:
        return SelectTorpedoHobFile.class;
    }

    var def = CTrekSegments.AllSegmentsByName.get(segName);
    if (def != null)
      return def.getType();

    throw new UnsupportedOperationException("Not supported yet.");
  }

  public InternalSegment loadSegment(String segName, int address, boolean cache, boolean reload) throws IOException {
    try {
      val seg = loadSegmentImpl(segName, address, cache, reload, this::createSegment);
      removeLoadError(segName);
      return seg;
    } catch (Exception e) {
      addLoadError(segName, e instanceof DataException ? e.getMessage() : e.toString());
      throw e;
    }
  }

  public ArrayList<InternalFile> loadFiles(Collection<String> segNames, boolean cache, boolean reload,
    Optional<Predicate<String>> cbProgress) throws IOException {
    ArrayList<InternalFile> result = new ArrayList<>();

    // TODO: cleanup loadSegments, they should not reload the archive file all the time
    for (String segName : segNames) {
      // skip cached segments
      if (isCached(segName))
        continue;

      // update progress
      if (cbProgress.isPresent())
        if (!cbProgress.get().test(segName))
          break;

      // load segment
      try {
        val seg = loadSegmentImpl(segName, -1, cache, reload, this::createSegment);
        removeLoadError(segName);
        result.add(seg);
      } catch (Exception e) {
        addLoadError(segName, e instanceof DataException ? e.getMessage() : e.toString());
        throw e;
      }
    }

    return result;
  }

  private InternalSegment loadSegmentImpl(String segName, int address, boolean cache, boolean reload, CheckedFunction<String, InternalSegment, IOException> instFnc) throws IOException {
    InternalSegment seg = address != -1 ? segments.get(address) : segmentsByName.get(segName);

    if (seg != null) {
      if (!reload)
        return seg;

      // reuse same old segment so any cached references remain valid
      // @see Stbof.load
      seg.clear();
      cache = false;
    } else {
      seg = instFnc.apply(segName);
      validateUnused(seg.address(), seg.getSize());
    }

    if (address == -1)
      address = seg.address();

    try (FileInputStream fis = new FileInputStream(sourceFile)){
      StreamTools.skip(fis, address);
      seg.load(fis);

      // detect initial file changes
      seg.checkForChanges();

      // check if the reported size equals actual size
      long size = sourceFile.length() - fis.available() - address;
      if (size != seg.getSize())
        throw new IOException("Segment size mismatch: %1".replace("%1", segName));

      if (cache) {
        segments.put(address, seg);
        segmentsByName.put(segName, seg);
      }
    }

    return seg;
  }

  /**
   * Checks internal files for unusal data and other problems.
   *
   * @return A Vector containing the check results.
   */
  @Override
  public Vector<String> check() {
    val checkTask = new TrekCheckTask(this);
    Vector<String> response = Optional.ofNullable(
      GuiTools.runUEWorker(checkTask)).orElse(new Vector<>());

    // return
    if (checkTask.isCancelled())
      response.add(Language.getString("Trek.7")); //$NON-NLS-1$
    if (response.isEmpty())
      response.add(Language.getString("Trek.5")); //$NON-NLS-1$

    return response;
  }

  private void validateUnused(int address, int size) throws FileAlreadyExistsException {
    val prev = segments.floorEntry(address);
    if (prev != null) {
      val seg = prev.getValue();
      int end = seg.address() + seg.getSize();
      if (end > address)
        throwAlreadyExists(address, seg);
    }

    val next = segments.ceilingEntry(address+1);
    if (next != null) {
      val seg = next.getValue();
      if (seg.address() < address + size)
        throwAlreadyExists(address, seg);
    }
  }

  private void throwAlreadyExists(int address, InternalSegment seg) throws FileAlreadyExistsException {
    String addr = HexTools.toHex(address);
    String msg = "Segment address 0x%1 is overlapped by segment %2"
      .replace("%1", addr)
      .replace("%2", seg.getName() + " [" + seg.getLocation() + "]");
    throw new FileAlreadyExistsException(msg);
  }

  @Override
  public boolean isModded() throws IOException, NoSuchAlgorithmException {
    // check size
    if (sourceFile.length() != TREK_SIZE)
      return true;

    try (FileInputStream fis = new FileInputStream(sourceFile)) {
      byte[] data = StreamTools.readAllBytes(fis);
      MessageDigest m = MessageDigest.getInstance("MD5"); //$NON-NLS-1$
      m.update(data, 0, data.length);

      byte[] hash = m.digest();
      byte[] h = BaseEncoding.base16().decode(TREK_HASH);

      return !Arrays.equals(hash, h);
    }
  }

  @Override
  public boolean isCached(String fileName) {
    return segmentsByName.containsKey(fileName);
  }

  @Override
  public boolean isChanged(String fileName) {
    InternalSegment file = segmentsByName.get(fileName);
    return file != null && file.madeChanges();
  }

  @Override
  public ModList modList() {
    return null;
  }

}
