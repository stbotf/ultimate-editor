package ue.edit.exe.trek.patch.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_TechLvl;
import ue.edit.exe.trek.seg.tec.BldTechReq;
import ue.edit.exe.trek.seg.tec.BldTechReq2;
import ue.edit.exe.trek.seg.tec.TechCostFixture;
import ue.edit.exe.trek.seg.tec.TechMapSize;
import ue.edit.exe.trek.seg.tec.TechTotal;
import ue.edit.value.IntValue;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Sets the trek.exe technology levels.
 */
public class TechLevelsPatch extends MultiSegment implements IntValue {

  public static final String NAME =  SD_TechLvl.Prefix;

  // segments
  private List<CmpRegByte>    seg_numTechLevel;
  private List<IntValueSegment>   seg_maxTechLevel;
  private List<ByteValueSegment>  seg_maxTechLevel_byte;
  private List<ByteValueSegment>  seg_maxTechLevel_neg;
  private List<IntValueSegment>   seg_secLastTechLevel;
  private List<ByteValueSegment>  seg_secLastTechLevel_byte;
  private TechCostFixture         seg_TechCostFixture;  // at 0x000534ac, tech num 11 = 0Bh
  private TechTotal               seg_TechTotal;        // at 0x000530a0, incl. sociology = 11 * 7 = 77 = 4Dh
  private TechMapSize             seg_TechMapSize;      // at 0x0004fa59, defaults to 1F4h (Edifice patcher)
  private BldTechReq              seg_BldTechReq;       // at 0x0004f52b (Edifice patcher)
  private BldTechReq2             seg_BldTechReq2;      // at 0x0004f541 (Edifice patcher)

  private int techLevels = 0;

  public TechLevelsPatch(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public boolean isFloatingPoint() {
    return false;
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_TechLvl.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    val list = new ArrayList<InternalSegment>();
    list.addAll(seg_numTechLevel);
    list.addAll(seg_maxTechLevel);
    list.addAll(seg_maxTechLevel_byte);
    list.addAll(seg_maxTechLevel_neg);
    list.addAll(seg_secLastTechLevel);
    list.addAll(seg_secLastTechLevel_byte);
    list.add(seg_TechCostFixture);
    list.add(seg_TechTotal);
    list.add(seg_TechMapSize);
    list.add(seg_BldTechReq2);
    list.add(seg_BldTechReq2);
    return list;
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_numTechLevel != null;
  }

  @Override
  public void reset() throws IOException {
    seg_numTechLevel = null;
    seg_numTechLevel = null;
    seg_numTechLevel = null;
    seg_TechCostFixture = null;
    seg_TechTotal = null;
    seg_TechMapSize = null;
    seg_BldTechReq = null;
    seg_BldTechReq2 = null;
    techLevels = 0;
  }

  @Override
  public void reload() throws IOException {
    // num tech levels = future tech 11 = 0Bh
    seg_numTechLevel          = loadSegments(CmpRegByte.class, SD_TechLvl.Num, true);

    // max tech level 10 = 0Ah
    seg_maxTechLevel          = loadSegments(IntValueSegment.class, SD_TechLvl.Max, true);
    seg_maxTechLevel_byte     = loadSegments(ByteValueSegment.class, SD_TechLvl.MaxByte, true);
    seg_maxTechLevel_neg      = loadSegments(ByteValueSegment.class, SD_TechLvl.MaxNeg, true);

    // before last tech level 9 = 09h
    seg_secLastTechLevel      = loadSegments(IntValueSegment.class, SD_TechLvl.SndLast, true);
    seg_secLastTechLevel_byte = loadSegments(ByteValueSegment.class, SD_TechLvl.SndLastByte, true);

    seg_TechCostFixture       = (TechCostFixture) trek.getSegment(SD_TechLvl.CostFix, true);
    seg_TechTotal             = (TechTotal)       trek.getSegment(SD_TechLvl.Total, true);
    seg_TechMapSize           = (TechMapSize)     trek.getSegment(SD_TechLvl.MapSize, true);
    seg_BldTechReq            = (BldTechReq)      trek.getSegment(SD_TechLvl.BldReq, true);
    seg_BldTechReq2           = (BldTechReq2)     trek.getSegment(SD_TechLvl.BldReq2, true);

    techLevels = calcTechLevels();
  }

  @Override
  public int intValue() {
    return techLevels;
  }

  @Override
  public boolean setValue(int numTechs) throws InvalidArgumentsException {
    if (this.techLevels != numTechs) {
      this.techLevels = numTechs;

      for (val seg : seg_numTechLevel)
        seg.setValue(numTechs);
      for (val seg : seg_maxTechLevel)
        seg.setValue(numTechs-1);
      for (val seg : seg_maxTechLevel_byte)
        seg.setValue(numTechs-1);
      for (val seg : seg_maxTechLevel_neg)
        seg.setValue(-1 * (numTechs-1));
      for (val seg : seg_secLastTechLevel)
        seg.setValue(numTechs-2);
      for (val seg : seg_secLastTechLevel_byte)
        seg.setValue(numTechs-2);

      seg_TechCostFixture.setValue(numTechs);
      seg_TechTotal.setTechLevels(numTechs);
      seg_TechMapSize.setTechLevels(numTechs);
      seg_BldTechReq.setTechLevels(numTechs);
      seg_BldTechReq2.setTechLevels(numTechs);
      return true;
    }
    return false;
  }

  @Override
  public void load(InputStream in) throws IOException {
    techLevels = StreamTools.readShort(in, true);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(techLevels, true));
  }

  @Override
  public void clear() {
    techLevels = 0;
  }

  @Override
  public void check(Vector<String> response) {
    for (val seg : seg_numTechLevel)
      seg.check(response);
    for (val seg : seg_maxTechLevel)
      seg.check(response);
    for (val seg : seg_maxTechLevel_byte)
      seg.check(response);
    for (val seg : seg_maxTechLevel_neg)
      seg.check(response);
    for (val seg : seg_secLastTechLevel)
      seg.check(response);
    for (val seg : seg_secLastTechLevel_byte)
      seg.check(response);

    seg_TechCostFixture.check(response);
    seg_TechTotal.check(response);
    seg_TechMapSize.check(response);
    seg_BldTechReq.check(response);
    seg_BldTechReq2.check(response);
  }

  private int calcTechLevels() {
    int num = Integer.MAX_VALUE;
    for (val seg : seg_numTechLevel)
      num = Integer.min(num, seg.byteValue());
    for (val seg : seg_maxTechLevel)
      num = Integer.min(num, seg.intValue() + 1);
    for (val seg : seg_maxTechLevel_byte)
      num = Integer.min(num, seg.byteValue() + 1);
    for (val seg : seg_maxTechLevel_neg)
      num = Integer.min(num, (-1 * seg.byteValue()) + 1);
    for (val seg : seg_secLastTechLevel)
      num = Integer.min(num, seg.intValue() + 2);
    for (val seg : seg_secLastTechLevel_byte)
      num = Integer.min(num, seg.byteValue() + 2);

    num = Integer.min(num, seg_TechCostFixture.byteValue());
    num = Integer.min(num, seg_TechTotal.getTechLevels());
    num = Integer.min(num, seg_TechMapSize.getTechLevels());
    num = Integer.min(num, seg_BldTechReq.getTechLevels());
    num = Integer.min(num, seg_BldTechReq2.getTechLevels());

    return num;
  }
}
