package ue.edit.exe.trek.patch.map;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_SectorSize;

/**
 * Sets trek.exe sector size.
 */
public final class SectorSizePatch extends MultiSegment {

  public static final String NAME = SD_SectorSize.Prefix; //$NON-NLS-1$

  public static final int GAL_ZOOM_MODES = 4; // few, some, many, zoomed in

  // segments
  private MovRegInt[][] seg_sectorSize = new MovRegInt[GAL_ZOOM_MODES][];

  public SectorSizePatch(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_SectorSize.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    return Arrays.stream(seg_sectorSize).flatMap(Arrays::stream).collect(Collectors.toList())
    ;
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_sectorSize[0] != null;
  }

  @Override
  public void reset() throws IOException {
    Arrays.fill(seg_sectorSize, null);
  }

  @Override
  public void reload() throws IOException {
    for (int i = 0; i < GAL_ZOOM_MODES; ++i) {
      seg_sectorSize[i] = new MovRegInt[SD_SectorSize.Pixels[i].length];
      for (int j = 0; j < SD_SectorSize.Pixels[i].length; ++j) {
        seg_sectorSize[i][j] = (MovRegInt) trek.getSegment(SD_SectorSize.Pixels[i][j], true);
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
    listSegments().forEach(x -> x.check(response));
  }

  public int getSectorSize(int gsize, boolean test) throws IOException {
    int sectorSize = seg_sectorSize[gsize][0].intValue();

    if (test) {
      for (val seg : seg_sectorSize[gsize]) {
        try {
          seg.validate(sectorSize);
        } catch (Exception e) {
          val addr = Long.toHexString(seg.address());
          throw new IOException(e.getMessage() + " (0x" + addr + ")"); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
    }

    return sectorSize;
  }

  public void setSectorSize(int gsize, short size) throws Exception {
    int sectorSize = getSectorSize(gsize, false);
    if (size != sectorSize) {
      for (val seg : seg_sectorSize[gsize])
        seg.setValue(sectorSize);
    }
  }
}
