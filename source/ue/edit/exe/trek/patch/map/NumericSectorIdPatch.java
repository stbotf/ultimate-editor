package ue.edit.exe.trek.patch.map;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import lombok.Getter;
import lombok.val;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.DataPatchSegment;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.AddRegByte;
import ue.edit.exe.seg.prim.PushInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_NumericSID;
import ue.exception.InvalidArgumentsException;

/**
 * Sets the trek.exe sector ids to numeric format "xx.yy".
 */
public class NumericSectorIdPatch extends MultiSegment implements Patch {

  public static final String NAME = SD_NumericSID.Prefix;

  // segments
  private AddRegByte        seg_literal1_conversion;
  private PushInt           seg_literal1_offset;
  private DataPatchSegment  seg_literal1;
  private AddRegByte        seg_literal2_conversion;
  private DataPatchSegment  seg_literal2;

  @Getter private boolean patched;

  public NumericSectorIdPatch(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_NumericSID.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    val list = new ArrayList<InternalSegment>();
    list.add(seg_literal1_conversion);
    list.add(seg_literal1_offset);
    list.add(seg_literal1);
    list.add(seg_literal2_conversion);
    list.add(seg_literal2);
    return list;
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_literal1 != null;
  }

  @Override
  public void reset() throws IOException {
    seg_literal1_conversion = null;
    seg_literal1_offset = null;
    seg_literal1 = null;
    seg_literal2_conversion = null;
    seg_literal2 = null;
  }

  @Override
  public void reload() throws IOException {
    patched = false;

    // the anomaly name literal, which suffixes the sector position
    seg_literal1_conversion = (AddRegByte)        trek.getSegment(SD_NumericSID.Literal1_Conv, true);
    seg_literal1_offset =     (PushInt)           trek.getSegment(SD_NumericSID.Literal1_Offset, true);
    seg_literal1 =            (DataPatchSegment)  trek.getSegment(SD_NumericSID.Literal1, true);
    // the galaxy map header line and event text sector position
    seg_literal2_conversion = (AddRegByte)        trek.getSegment(SD_NumericSID.Literal2_Conv, true);
    seg_literal2 =            (DataPatchSegment)  trek.getSegment(SD_NumericSID.Literal2, true);

    int charConv = seg_literal1_conversion.byteValue();
    if (charConv == 0x41) {
      seg_literal1_offset.validateDefault();
      seg_literal1.validateDefault();
      seg_literal2_conversion.validateDefault();
      seg_literal2.validateDefault();
    } else {
      seg_literal1_conversion.validate(0x01);
      // bop unnecessarily relocated the patched "%s %d.%d" literal,
      // while the sneaked patch offset just features the unused preceding data alignment byte
      if (seg_literal1_offset.check(SD_NumericSID.bop_literal1_offset)) {
        seg_literal1.validateDefault();
        seg_literal2_conversion.validate(0x01);
        // validate brackets literal
        seg_literal2.validatePatched(1);
      } else {
        seg_literal1_offset.validate(SD_NumericSID.patch_literal1_offset);
        seg_literal1.validatePatched();
        seg_literal2_conversion.validate(0x01);
        // validate numeric literal
        seg_literal2.validatePatched();
      }
      patched = true;
    }
  }

  @Override
  public boolean isDefault() {
    return !patched;
  }

  public boolean isBraced() {
    return patched && seg_literal2.patchOption() == 1;
  }

  @Override
  public void patch() throws IOException {
    patch(0);
  }

  public void patchBraced() throws IOException {
    patch(1);
  }

  private void patch(int literal) throws IOException {
    if (!patched) {
      try {
        seg_literal1_conversion.setValue(0x01);
        seg_literal1_offset.setValue(SD_NumericSID.patch_literal1_offset);
        seg_literal1.patch();
        seg_literal2_conversion.setValue(0x01);
        patched = true;
      }
      catch (InvalidArgumentsException e) {
        throw new IOException(e);
      }
    }

    // always patch literal2 for in case the numeric braces option changed
    seg_literal2.patch(literal);
  }

  @Override
  public void unpatch() throws IOException {
    if (patched) {
      try {
        seg_literal1_conversion.setValue(0x41);
        seg_literal1_offset.setValue(SD_NumericSID.orig_literal1_offset);
        seg_literal1.unpatch();
        seg_literal2_conversion.setValue(0x61);
        seg_literal2.unpatch();
        patched = false;
      }
      catch (InvalidArgumentsException e) {
        throw new IOException(e);
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
    seg_literal1_conversion.check(response);
    seg_literal1_offset.check(response);
    seg_literal1.check(response);
    seg_literal2_conversion.check(response);
    seg_literal2.check(response);
  }
}
