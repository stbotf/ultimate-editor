package ue.edit.exe.trek.patch.map;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_MinEmpDist;
import ue.exception.InvalidArgumentsException;

/**
 * Patches and sets the trek.exe minimum empire distance.
 */
public final class MinEmpireDistancePatch extends MultiSegment implements Patch {

  public static final String NAME = SD_MinEmpDist.Prefix;

  public static final int GAL_SIZE_MODES = 3; // small, medium, large
  public static final int GAL_SHAPES = 4;     // irregular, elliptic, ring, spiral

  // segments
  private MovRegInt[][] seg_minEmpireDistance = new MovRegInt[GAL_SIZE_MODES][GAL_SHAPES];
  private RingMinEmpireDistancePatch patch_ringEmpDistance;

  public MinEmpireDistancePatch(Trek trek) {
    super(NAME, trek);
    patch_ringEmpDistance = new RingMinEmpireDistancePatch(trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_MinEmpDist.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    return Stream.of(
      Arrays.stream(seg_minEmpireDistance).flatMap(Arrays::stream),
      patch_ringEmpDistance.listSegments().stream()
    ).flatMap(x -> x).collect(Collectors.toList())
    ;
  }

  @Override
  public boolean isLoaded() throws IOException {
    return patch_ringEmpDistance.isLoaded();
  }

  @Override
  public void reset() throws IOException {
    patch_ringEmpDistance.reset();
    for (val segs : seg_minEmpireDistance)
      Arrays.fill(segs, null);
  }

  @Override
  public void reload() throws IOException {
    patch_ringEmpDistance.reload();
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      for (int j = 0; j < GAL_SHAPES; ++j) {
        seg_minEmpireDistance[i][j] = (MovRegInt) trek.getSegment(SD_MinEmpDist.GalSectors[i][j], true);
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
    listSegments().forEach(x -> x.check(response));
  }

  public int getMinEmpireDistance(int galSize, int galShape) {
    // for small and large ring galaxy, check whether the fix is in place
    // if not, default to read the shared value
    if (galSize == 0 && galShape == 2 && patch_ringEmpDistance.isPatched())
      return patch_ringEmpDistance.getSmallRingValue();
    else if (galSize == 2 && galShape == 2 && patch_ringEmpDistance.isPatched())
      return patch_ringEmpDistance.getLargeRingValue();
    else
      return seg_minEmpireDistance[galSize][galShape].intValue();
  }

  public void setMinEmpireDistance(int galSize, int galShape, int dist) throws IOException, InvalidArgumentsException {
    // when small or large ring galaxy min empire distance is set,
    // apply patch to modify the values independently from the galaxy width
    if (galSize == 0 && galShape == 2) {
      // only patch if changed
      if (getMinEmpireDistance(galSize, galShape) != dist) {
        patch_ringEmpDistance.patch();
        patch_ringEmpDistance.setSmallRingValue(dist);
      }
    } else if (galSize == 2 && galShape == 2) {
      // only patch if changed
      if (getMinEmpireDistance(galSize, galShape) != dist) {
        patch_ringEmpDistance.patch();
        patch_ringEmpDistance.setLargeRingValue(dist);
      }
    } else {
      seg_minEmpireDistance[galSize][galShape].setValue(dist);
    }
  }

  @Override
  public boolean isDefault() {
    if (!patch_ringEmpDistance.isDefault())
      return false;

    // minimum empire distance
    if (!Arrays.stream(seg_minEmpireDistance)
      .flatMap(Arrays::stream)
      .allMatch(InternalSegment::isDefault))
      return false;

    return true;
  }

  @Override
  public boolean isPatched() {
    return patch_ringEmpDistance.isPatched();
  }

  @Override
  public void patch() throws IOException {
    patch_ringEmpDistance.patch();
  }

  @Override
  public void unpatch() throws IOException {
    patch_ringEmpDistance.unpatch();
  }
}
