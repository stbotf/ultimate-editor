package ue.edit.exe.trek.patch.map;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.MovDwordPtrInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_SysAlign;
import ue.exception.InvalidArgumentsException;

/**
 * Sets trek.exe system alignment.
 */
public final class SystemAlignmentPatch extends MultiSegment {

  public static final String NAME = SD_SysAlign.Prefix; //$NON-NLS-1$

  public static final int GAL_ZOOM_MODES = 4; // few, some, many, zoomed in

  // segments
  private MovDwordPtrInt[][]  seg_systemAlignment = new MovDwordPtrInt[GAL_ZOOM_MODES][];

  public SystemAlignmentPatch(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_SysAlign.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    return Arrays.stream(seg_systemAlignment).flatMap(Arrays::stream).collect(Collectors.toList());
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_systemAlignment[0] != null;
  }

  @Override
  public void reset() throws IOException {
    Arrays.fill(seg_systemAlignment, null);
  }

  @Override
  public void reload() throws IOException {
    for (int i = 0; i < GAL_ZOOM_MODES; ++i) {
      seg_systemAlignment[i] = new MovDwordPtrInt[SD_SysAlign.Scale[i].length];
      for (int j = 0; j < SD_SysAlign.Scale[i].length; ++j) {
        seg_systemAlignment[i][j] = (MovDwordPtrInt) trek.getSegment(SD_SysAlign.Scale[i][j], true);
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
    listSegments().forEach(x -> x.check(response));
  }

  public float getSystemAlignment(int gsize, boolean test) throws IOException {
    float align = seg_systemAlignment[gsize][0].getFloat();

    if (test) {
      for (val seg : seg_systemAlignment[gsize]) {
        try {
          seg.validateFloat(align);
        } catch (Exception e) {
          val addr = Long.toHexString(seg.address());
          throw new IOException(e.getMessage() + " (0x" + addr + ")"); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
    }

    return align;
  }

  public void setSystemAlignment(int gsize, float align) throws IOException, InvalidArgumentsException {
    float sysAlign = getSystemAlignment(gsize, false);
    if (align != sysAlign) {
      for (val seg : seg_systemAlignment[gsize])
        seg.setFloat(align);
    }
  }
}
