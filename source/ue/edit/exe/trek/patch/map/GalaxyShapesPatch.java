package ue.edit.exe.trek.patch.map;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.LongValueSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_GalShape;
import ue.edit.exe.trek.seg.map.LargeSpiralAxisFix;
import ue.exception.InvalidArgumentsException;

/**
 * Patches and sets trek.exe galaxy shape values.
 */
public final class GalaxyShapesPatch extends MultiSegment implements Patch {

  public static final String NAME = SD_GalShape.Prefix;

  public static final int GAL_ARCS = 3;       // inner, middle, outer
  public static final int GAL_AXIS = 2;       // x/y or major/minor
  public static final int GAL_SIZE_MODES = 3; // small, medium, large

  // segments
  private IntValueSegment[][]         seg_ellipseAxis = new IntValueSegment[GAL_SIZE_MODES][GAL_AXIS];
  private IntValueSegment[][]         seg_spiralEllipseAxis = new IntValueSegment[GAL_SIZE_MODES][GAL_AXIS];
  private LongValueSegment[]          seg_spiralArmArcs = new LongValueSegment[GAL_ARCS];
  private MovRegInt[]                 seg_spiralArmWidth = new MovRegInt[GAL_SIZE_MODES];
  private MovRegInt[]                 seg_ringCoreSize = new MovRegInt[GAL_SIZE_MODES];
  private MovRegInt[]                 seg_ringWidth = new MovRegInt[GAL_SIZE_MODES];
  private LargeSpiralAxisFix          seg_largeSpiralAxisFix;
  private RingMinEmpireDistancePatch  patch_ringEmpDistance;

  public GalaxyShapesPatch(Trek trek) {
    super(NAME, trek);
    patch_ringEmpDistance = new RingMinEmpireDistancePatch(trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_GalShape.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    return Stream.of(
      Arrays.stream(seg_ellipseAxis).flatMap(Arrays::stream),
      Arrays.stream(seg_spiralEllipseAxis).flatMap(Arrays::stream),
      Arrays.stream(seg_spiralArmArcs),
      Arrays.stream(seg_spiralArmWidth),
      Arrays.stream(seg_ringCoreSize),
      Arrays.stream(seg_ringWidth),
      Stream.of(seg_largeSpiralAxisFix),
      patch_ringEmpDistance.listSegments().stream()
    ).flatMap(x -> x).collect(Collectors.toList());
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_largeSpiralAxisFix != null;
  }

  @Override
  public void reset() throws IOException {
    for (val segs : seg_ellipseAxis)
      Arrays.fill(segs, null);
    for (val segs : seg_spiralEllipseAxis)
      Arrays.fill(segs, null);
    Arrays.fill(seg_spiralArmArcs, null);
    Arrays.fill(seg_spiralArmWidth, null);
    Arrays.fill(seg_ringCoreSize, null);
    Arrays.fill(seg_ringWidth, null);
    seg_largeSpiralAxisFix = null;
    patch_ringEmpDistance.reset();
  }

  @Override
  public void reload() throws IOException {
    patch_ringEmpDistance.reload();

    // ellipse axes
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      for (int j = 0; j < GAL_AXIS; ++j) {
        seg_ellipseAxis[i][j] = (IntValueSegment) trek.getSegment(SD_GalShape.EllipseAxis[i][j], true);
      }
    }

    // spiral axes
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      for (int j = 0; j < GAL_AXIS; ++j) {
        // note, for large spiral galaxy by default both axis are shared
        seg_spiralEllipseAxis[i][j] = (IntValueSegment) trek.getSegment(SD_GalShape.SpiralEllipseAxis[i][j], true);
      }
    }
    seg_largeSpiralAxisFix = (LargeSpiralAxisFix) trek.getSegment(SD_GalShape.LargeSpiralAxis, true);

    // spiral arm arc
    for (int i = 0; i < GAL_ARCS; ++i) {
      seg_spiralArmArcs[i] = (LongValueSegment) trek.getSegment(SD_GalShape.SpiralArmArcs[i], true);
    }

    // spiral arm width
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      seg_spiralArmWidth[i] = (MovRegInt) trek.getSegment(SD_GalShape.SpiralArmWidth[i], true);
    }

    // ring core size
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      seg_ringCoreSize[i] = (MovRegInt) trek.getSegment(SD_GalShape.RingCoreSize[i], true);
    }

    // ring width
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      seg_ringWidth[i] = (MovRegInt) trek.getSegment(SD_GalShape.RingWidth[i], true);
    }
  }

  @Override
  public void check(Vector<String> response) {
    listSegments().forEach(x -> x.check(response));
  }

  @Override
  public boolean isDefault() {
    if (!patch_ringEmpDistance.isDefault())
      return false;

    // ellipse axes
    if (!Arrays.stream(seg_ellipseAxis)
      .flatMap(Arrays::stream)
      .allMatch(InternalSegment::isDefault))
      return false;

    // spiral axes
    if (!Arrays.stream(seg_spiralEllipseAxis)
      .flatMap(Arrays::stream)
      .allMatch(InternalSegment::isDefault))
      return false;

    if (!seg_largeSpiralAxisFix.isDefault())
      return false;

    // spiral arm arc
    if (!Arrays.stream(seg_spiralArmArcs)
      .allMatch(InternalSegment::isDefault))
      return false;

    // spiral arm width
    if (!Arrays.stream(seg_spiralArmWidth)
      .allMatch(InternalSegment::isDefault))
      return false;

    // ring core size
    if (!Arrays.stream(seg_ringCoreSize)
      .allMatch(InternalSegment::isDefault))
      return false;

    // ring width
    if (!Arrays.stream(seg_ringWidth)
      .allMatch(InternalSegment::isDefault))
      return false;

    return true;
  }

  @Override
  public boolean isPatched() {
    return patch_ringEmpDistance.isPatched();
  }

  @Override
  public void patch() throws IOException {
    patch_ringEmpDistance.patch();
  }

  @Override
  public void unpatch() throws IOException {
    patch_ringEmpDistance.unpatch();
  }

  public int getEllipseGalEllipseAxis(int galSize, int axis) {
    return seg_ellipseAxis[galSize][axis].intValue();
  }

  public void setEllipseGalEllipseAxis(int galSize, int axis, int value) throws InvalidArgumentsException {
    seg_ellipseAxis[galSize][axis].setValue(value);
  }

  public int getRingGalCoreSize(int galSize) {
    return seg_ringCoreSize[galSize].intValue();
  }

  public void setRingGalCoreSize(int i, int coreSize) throws InvalidArgumentsException {
    seg_ringCoreSize[i].setValue(coreSize);
  }

  public int getRingGalWidth(int galSize) {
    return seg_ringWidth[galSize].intValue();
  }

  public void setRingGalWidth(int galSize, int value) throws IOException, InvalidArgumentsException
  {
      val seg = seg_ringWidth[galSize];
      if (seg.intValue() != value)
      {
        if (galSize != 1) {
          // make sure the segments aren't shared by the min empire distance
          patch_ringEmpDistance.patch();
        }

        seg.setValue(value);
      }
  }

  public double getSpiralGalArmArc(int arc) {
    return seg_spiralArmArcs[arc].getDouble() / Math.PI;
  }

  public void setSpiralGalArmArc(int arc, double armArc) throws InvalidArgumentsException {
    seg_spiralArmArcs[arc].setDouble(armArc * Math.PI, 0.01);
  }

  public int getSpiralGalArmWidth(int galSize) {
    return seg_spiralArmWidth[galSize].intValue();
  }
  public void setSpiralGalArmWidth(int galSize, int armWidth) throws InvalidArgumentsException {
    seg_spiralArmWidth[galSize].setValue(armWidth);
  }

  public int getSpiralGalEllipseAxis(int galSize, int axis) {
    // for large galaxy X axis, check whether the fix is in place
    // if not, default to read the shared value
    if (galSize == 2 && axis == 0 && seg_largeSpiralAxisFix.isPatched())
      return seg_largeSpiralAxisFix.intValue();
    else
      return seg_spiralEllipseAxis[galSize][axis].intValue();
  }

  public void setSpiralGalEllipseAxis(int galSize, int axis, int value) throws InvalidArgumentsException {
    if (galSize == 2) {
      // when for the large galaxy either axis is changed,
      // apply patch to modify the values independently
      // the patch overwrites a redundant galaxy width value
      // @see sd_largeSpiralAxisFix and sd_galWidth
      if (!seg_largeSpiralAxisFix.isPatched()) {
        // only patch on changes
        if (seg_spiralEllipseAxis[galSize][axis].intValue() == value)
          return;

        seg_largeSpiralAxisFix.patch();

        // for other axis the applied patch still must be initialized
        // but only do so if not already patched before
        if (axis != 0) {
          // copy shared axis value for the patch
          int shared_size = seg_spiralEllipseAxis[galSize][axis].intValue();
          seg_largeSpiralAxisFix.setValue(shared_size);
        }
      }
    }

    if (galSize == 2 && axis == 0) {
      // set patched unique axis value
      seg_largeSpiralAxisFix.setValue(value);
    } else {
      // set other axis values
      seg_spiralEllipseAxis[galSize][axis].setValue(value);
    }
  }
}
