package ue.edit.exe.trek.patch.map;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.CmpWordPtrShort;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_GalShape;
import ue.edit.exe.trek.sd.SD_MapSize;
import ue.edit.exe.trek.seg.map.LargeSpiralAxisFix;

/**
 * Sets the horizontal and vertical trek.exe sector counts.
 */
public class MapSizePatch extends MultiSegment {

  public static final String NAME = SD_MapSize.Prefix;

  public static final int GAL_SIZE_MODES = 3;  // small, medium, large

  // segments
  private IntValueSegment[][] seg_mapWidth = new IntValueSegment[GAL_SIZE_MODES][];
  private IntValueSegment[][] seg_mapHeight = new IntValueSegment[GAL_SIZE_MODES][];
  private IntValueSegment[]   seg_mapWidth5 = new IntValueSegment[GAL_SIZE_MODES];
  private IntValueSegment[]   seg_mapHeight5 = new IntValueSegment[GAL_SIZE_MODES];
  private LargeSpiralAxisFix  seg_largeSpiralAxisFix;
  private RingMinEmpireDistancePatch patch_ringEmpDistance;

  public MapSizePatch(Trek trek) {
    super(NAME, trek);
    patch_ringEmpDistance = new RingMinEmpireDistancePatch(trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_MapSize.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    return Stream.of(
      Stream.of(seg_mapWidth, seg_mapHeight).flatMap(Arrays::stream).flatMap(Arrays::stream),
      Stream.of(seg_mapWidth5, seg_mapHeight5).flatMap(Arrays::stream),
      Stream.of(seg_largeSpiralAxisFix)
    ).flatMap(x -> x).collect(Collectors.toList());
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_largeSpiralAxisFix != null;
  }

  @Override
  public void reset() throws IOException {
    patch_ringEmpDistance.reset();
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      seg_mapWidth[i] = null;
    }
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      seg_mapHeight[i] = null;
    }
    seg_largeSpiralAxisFix = null;
  }

  @Override
  public void reload() throws IOException {
    patch_ringEmpDistance.reload();

    // map width
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      seg_mapWidth[i] = new IntValueSegment[SD_MapSize.Width[i].length];
      for (int j = 0; j < SD_MapSize.Width[i].length; ++j) {
        seg_mapWidth[i][j] = (IntValueSegment) trek.getSegment(SD_MapSize.Width[i][j], true);
      }
      seg_mapWidth5[i] = (IntValueSegment) trek.getSegment(SD_MapSize.Width5[i], true);
    }

    // map height
    for (int i = 0; i < GAL_SIZE_MODES; ++i) {
      seg_mapHeight[i] = new IntValueSegment[SD_MapSize.Height[i].length];
      for (int j = 0; j < SD_MapSize.Height[i].length; ++j) {
        seg_mapHeight[i][j] = (IntValueSegment) trek.getSegment(SD_MapSize.Height[i][j], true);
      }
      seg_mapHeight5[i] = (IntValueSegment) trek.getSegment(SD_MapSize.Height5[i], true);
    }

    // spiral axes
    seg_largeSpiralAxisFix = (LargeSpiralAxisFix) trek.getSegment(SD_GalShape.LargeSpiralAxis, true);
  }

  @Override
  public void check(Vector<String> response) {
    listSegments().forEach(x -> x.check(response));
  }

  // width_address
  public int getMapWidth(int size, boolean test) throws IOException {
    int width = seg_mapWidth[size][0].intValue();

    if (test) {
      for (val seg : seg_mapWidth[size]) {
        validateValue(seg, width);
      }
      validateValue(seg_mapWidth5[size], width * 5);
    }

    return width;
  }

  public void setMapWidth(int size, int value) throws Exception {
    int mapWidth = getMapWidth(size, false);
    if (value != mapWidth) {
      if (size != 1) {
        // make sure to not overwrite the min empire distance
        patch_ringEmpDistance.patch();
      }

      for (val seg : seg_mapWidth[size])
        seg.setValue(value);

      seg_mapWidth5[size].setValue(value * 5);

      setSpaceObjectsLimit();
      setAIScanRange();

      // for large galaxy maps, further check whether the largeSpiralAxisFix is applied
      // if not, set the map width value
      if (size == 2 && !seg_largeSpiralAxisFix.isPatched())
          seg_largeSpiralAxisFix.setValue(value);
    }
  }

  public int getMapHeight(int size, boolean test) throws IOException {
    int height = seg_mapHeight[size][0].intValue();

    if (test) {
      for (val seg : seg_mapHeight[size]) {
        validateValue(seg, height);
      }
      validateValue(seg_mapHeight5[size], height * 5);
    }

    return height;
  }

  public void setMapHeight(int size, int value) throws Exception {
    int mapHeight = getMapHeight(size, false);
    if (value != mapHeight) {
      for (val seg : seg_mapHeight[size])
        seg.setValue(value);

      seg_mapHeight5[size].setValue(value * 5);

      setSpaceObjectsLimit();
      setAIScanRange();
    }
  }

  private void validateValue(IntValueSegment seg, int value) throws IOException {
    try {
      seg.validate(value);
    } catch (Exception e) {
      String addr = Integer.toHexString(seg.address());
      throw new IOException(e.getMessage() + " (0x" + addr + ")"); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  private void setSpaceObjectsLimit() throws Exception {
    val seg_limit1 = (CmpWordPtrShort) trek.getSegment(SD_MapSize.SpaceObjLimit[0], true);
    val seg_limit2 = (CmpWordPtrShort) trek.getSegment(SD_MapSize.SpaceObjLimit[1], true);
    seg_limit2.validate(seg_limit1.shortValue());

    short limit = 0;
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      int sectors = getMapHeight(i, false) * getMapWidth(i, false);
      limit = (short) Integer.max(limit, sectors);
    }

    int objLimit = seg_limit1.shortValue();
    if (limit != objLimit && limit > 0) {
      seg_limit1.setValue(limit);
      seg_limit2.setValue(limit);
    }
  }

  private void setAIScanRange() throws Exception {
    MovRegInt seg_AIScanRange = (MovRegInt) trek.getSegment(SD_MapSize.AIScanRange, true);

    int max = 0;
    for (int i = 0; i < GAL_SIZE_MODES; i++) {
      max = Integer.max(max, this.getMapHeight(i, false));
      max = Integer.max(max, this.getMapWidth(i, false));
    }

    if (max > seg_AIScanRange.intValue())
      seg_AIScanRange.setValue(max);
  }
}
