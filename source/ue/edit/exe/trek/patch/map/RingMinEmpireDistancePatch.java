package ue.edit.exe.trek.patch.map;

import java.io.IOException;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_GalShape;
import ue.edit.exe.trek.sd.SD_MapSize;
import ue.edit.exe.trek.sd.SD_MinEmpDistFix;
import ue.edit.exe.trek.seg.map.RingMinEmpireDistanceFix;
import ue.exception.InvalidArgumentsException;

/**
 * Patches and sets the trek.exe small and large ring galaxy minimum empire distance.
 */
public final class RingMinEmpireDistancePatch extends MultiSegment implements Patch {

  public static final String NAME = SD_MinEmpDistFix.Prefix;

  // segments
  private RingMinEmpireDistanceFix  seg_smallRing_minEmpireDistanceFix;
  private RingMinEmpireDistanceFix  seg_largeRing_minEmpireDistanceFix;

  public RingMinEmpireDistancePatch(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_MinEmpDistFix.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    return Stream.of(seg_smallRing_minEmpireDistanceFix, seg_largeRing_minEmpireDistanceFix)
      .collect(Collectors.toList());
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_smallRing_minEmpireDistanceFix != null;
  }

  @Override
  public void reset() throws IOException {
    seg_smallRing_minEmpireDistanceFix = null;
    seg_largeRing_minEmpireDistanceFix = null;
  }

  @Override
  public void reload() throws IOException {
    // minimum empire distance
    seg_smallRing_minEmpireDistanceFix = (RingMinEmpireDistanceFix) trek.getSegment(SD_MinEmpDistFix.SmallRing, true);
    seg_largeRing_minEmpireDistanceFix = (RingMinEmpireDistanceFix) trek.getSegment(SD_MinEmpDistFix.LargeRing, true);
  }

  @Override
  public void check(Vector<String> response) {
    listSegments().forEach(x -> x.check(response));
  }

  @Override
  public boolean isDefault() {
    return seg_smallRing_minEmpireDistanceFix.isDefault()
      && seg_largeRing_minEmpireDistanceFix.isDefault();
  }

  @Override
  public boolean isPatched() {
    return seg_smallRing_minEmpireDistanceFix.isPatched()
      && seg_largeRing_minEmpireDistanceFix.isPatched();
  }

  @Override
  public void patch() throws IOException {
    try {
      // separates the small & large ring galaxy min empire distance values from ring width
      // @see sd_mapWidth
      patchSmallRingMinEmpireDistance();
      patchLargeRingMinEmpireDistance();
    }
    catch (InvalidArgumentsException e) {
      throw new IOException(e);
    }
  }

  @Override
  public void unpatch() throws IOException {
    try {
      unpatchSmallRingMinEmpireDistance();
      unpatchLargeRingMinEmpireDistance();
    }
    catch (InvalidArgumentsException e) {
      throw new IOException(e);
    }
  }

  public int getSmallRingValue() {
    return seg_smallRing_minEmpireDistanceFix.intValue();
  }

  public int getLargeRingValue() {
    return seg_largeRing_minEmpireDistanceFix.intValue();
  }

  public void setSmallRingValue(int value) throws InvalidArgumentsException {
    seg_smallRing_minEmpireDistanceFix.setValue(value);
  }

  public void setLargeRingValue(int value) throws InvalidArgumentsException {
    seg_largeRing_minEmpireDistanceFix.setValue(value);
  }

  private void patchSmallRingMinEmpireDistance() throws IOException, InvalidArgumentsException {
    if (!seg_smallRing_minEmpireDistanceFix.isPatched()) {
      val seg_sharedRingWidth_small = (MovRegInt) trek.getSegment(SD_GalShape.Shared_SmallRingWidth, false);
      seg_smallRing_minEmpireDistanceFix.patch();
      // copy old shared ring width value
      seg_smallRing_minEmpireDistanceFix.setValue(seg_sharedRingWidth_small.intValue());
    }
  }

  private void patchLargeRingMinEmpireDistance() throws IOException, InvalidArgumentsException {
    if (!seg_largeRing_minEmpireDistanceFix.isPatched()) {
      val seg_sharedRingWidth_large = (MovRegInt) trek.getSegment(SD_GalShape.Shared_LargeRingWidth, false);
      seg_largeRing_minEmpireDistanceFix.patch();
      // copy old shared ring width value
      seg_largeRing_minEmpireDistanceFix.setValue(seg_sharedRingWidth_large.intValue());
    }
  }

  private void unpatchSmallRingMinEmpireDistance() throws IOException, InvalidArgumentsException {
    if (seg_smallRing_minEmpireDistanceFix.isPatched()) {
      val seg_mapWidth = (RingMinEmpireDistanceFix) trek.getSegment(SD_MapSize.SmallRingWidth, false);
      seg_smallRing_minEmpireDistanceFix.unpatch();
      // copy sd_mapWidth value that is shared when patched
      seg_smallRing_minEmpireDistanceFix.setValue(seg_mapWidth.intValue());
    }
  }

  private void unpatchLargeRingMinEmpireDistance() throws IOException, InvalidArgumentsException {
    if (seg_largeRing_minEmpireDistanceFix.isPatched()) {
      val seg_mapWidth = (RingMinEmpireDistanceFix) trek.getSegment(SD_MapSize.LargeRingWidth, false);
      seg_largeRing_minEmpireDistanceFix.unpatch();
      // copy sd_mapWidth value that is shared when patched
      seg_largeRing_minEmpireDistanceFix.setValue(seg_mapWidth.intValue());
    }
  }
}
