package ue.edit.exe.trek.patch.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.ShortValueSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_BldCnt;
import ue.edit.exe.trek.sd.SD_BldId_MainResearch;
import ue.edit.value.ShortValue;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Sets the trek.exe building count limits.
 *
 * Note that the last romulan research building happens to match the max building id.
 * Since other buildings might be added, it is set by SD_BldId_MainResearch instead.
 * @see SD_BldId_MainResearch
 */
public class BuildingCountPatch extends MultiSegment implements ShortValue {

  public static final String NAME = SD_BldCnt.Prefix;

  // max building id
  private List<IntValueSegment>   seg_maxIntIDs;
  private List<ShortValueSegment> seg_maxShortIDs;

  // num total buildings
  private List<ShortValueSegment> seg_numBlds;

  private short numBuildings = 0;

  public BuildingCountPatch(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(SD_BldCnt.Prefix_BldMax) || name.startsWith(SD_BldCnt.Prefix_BldNum);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_BldCnt.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    val list = new ArrayList<InternalSegment>();
    list.addAll(seg_maxIntIDs);
    list.addAll(seg_maxShortIDs);
    list.addAll(seg_numBlds);
    return list;
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_maxIntIDs != null;
  }

  @Override
  public void reset() throws IOException {
    seg_maxIntIDs = null;
    seg_maxShortIDs = null;
    seg_numBlds = null;
    numBuildings = 0;
  }

  @Override
  public void reload() throws IOException {
    // max building id
    seg_maxIntIDs   = loadSegments(IntValueSegment.class, SD_BldCnt.MaxInt, true);
    seg_maxShortIDs = loadSegments(ShortValueSegment.class, SD_BldCnt.MaxShort, true);

    // num total buildings
    seg_numBlds     = loadSegments(ShortValueSegment.class, SD_BldCnt.Num, true);

    numBuildings = Short.MAX_VALUE;
    for (val seg : seg_maxIntIDs)
      numBuildings = (short)Integer.min(numBuildings, seg.intValue() + 1);
    for (val seg : seg_maxShortIDs)
      numBuildings = (short)Integer.min(numBuildings, seg.shortValue() + 1);
    for (val seg : seg_numBlds)
      numBuildings = (short)Integer.min(numBuildings, seg.shortValue());
  }

  @Override
  public short shortValue() {
    return numBuildings;
  }

  @Override
  public void setValue(short value) throws InvalidArgumentsException {
    if (this.numBuildings != value) {
      this.numBuildings = value;

      for (val seg : seg_maxIntIDs)
        seg.setValue(numBuildings-1);
      for (val seg : seg_maxShortIDs)
        seg.setValue((short)(numBuildings-1));
      for (val seg : seg_numBlds)
        seg.setValue(numBuildings);
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    numBuildings = StreamTools.readShort(in, true);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(numBuildings, true));
  }

  @Override
  public void clear() {
    numBuildings = 0;
    for (val seg : seg_maxIntIDs)
      seg.clear();
    for (val seg : seg_maxShortIDs)
      seg.check();
    for (val seg : seg_numBlds)
      seg.check();
  }

  @Override
  public void check(Vector<String> response) {
    for (val seg : seg_maxIntIDs)
      seg.check(response);
    for (val seg : seg_maxShortIDs)
      seg.check(response);
    for (val seg : seg_numBlds)
      seg.check(response);
  }
}
