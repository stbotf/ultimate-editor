package ue.edit.exe.trek.patch.bld;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.ValueSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_BldId_MainEnergy;
import ue.edit.exe.trek.sd.SD_BldId_MainFarm;
import ue.edit.exe.trek.sd.SD_BldId_MainIndustry;
import ue.edit.exe.trek.sd.SD_BldId_MainIntel;
import ue.edit.exe.trek.sd.SD_BldId_MainResearch;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.StructureGroup;
import ue.exception.InvalidArgumentsException;

/**
 * Sets the trek.exe main building ID ranges.
 *
 * Note that the last romulan research building happens to match the max building id.
 * @see SD_BldId_MainResearch
 */
public class MainStructurePatch extends MultiSegment {

  public static final String NAME = "MainStructurePatch"; //$NON-NLS-1$

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // max building id
  private IntValueSegment[][] seg_minFoodIDs      = new IntValueSegment[NUM_EMPIRES][2];
  private ValueSegment[][]    seg_maxFoodIDs      = new ValueSegment[NUM_EMPIRES][5];
  private IntValueSegment[][] seg_minIndustryIDs  = new IntValueSegment[NUM_EMPIRES][3];
  private IntValueSegment[][] seg_minEnergyIDs    = new IntValueSegment[NUM_EMPIRES][5];
  private ValueSegment[][]    seg_maxEnergyIDs    = new ValueSegment[NUM_EMPIRES][4];
  private IntValueSegment[][] seg_minIntelIDs     = new IntValueSegment[NUM_EMPIRES][8];
  private ValueSegment[][]    seg_maxIntelIDs     = new ValueSegment[NUM_EMPIRES][7];
  private IntValueSegment[][] seg_minResearchIDs  = new IntValueSegment[NUM_EMPIRES][9];
  private IntValueSegment[][] seg_maxResearchIDs  = new IntValueSegment[NUM_EMPIRES][8];
  private boolean isLoaded = false;

  private int[] minFoodID = new int[NUM_EMPIRES];
  private int[] maxFoodID = new int[NUM_EMPIRES];
  private int[] minIndustryID = new int[NUM_EMPIRES];
  private int[] minEnergyID = new int[NUM_EMPIRES];
  private int[] maxEnergyID = new int[NUM_EMPIRES];
  private int[] minIntelID = new int[NUM_EMPIRES];
  private int[] maxIntelID = new int[NUM_EMPIRES];
  private int[] minResearchID = new int[NUM_EMPIRES];
  private int[] maxResearchID = new int[NUM_EMPIRES];

  public MainStructurePatch(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public String[] listSegmentNames() {
    return Stream.of(
      SD_BldId_MainFarm.Names,
      SD_BldId_MainIndustry.Names,
      SD_BldId_MainEnergy.Names,
      SD_BldId_MainIntel.Names,
      SD_BldId_MainResearch.Names
    ).flatMap(Arrays::stream).toArray(String[]::new);
  }

  @Override
  public List<InternalSegment> listSegments() {
    return Stream.of(
      seg_minFoodIDs,
      seg_maxFoodIDs,
      seg_minIndustryIDs,
      seg_minEnergyIDs,
      seg_maxEnergyIDs,
      seg_minIntelIDs,
      seg_maxIntelIDs,
      seg_minResearchIDs,
      seg_maxResearchIDs
    ).flatMap(Arrays::stream).flatMap(Arrays::stream)
      .collect(Collectors.toList());
  }

  @Override
  public boolean isLoaded() throws IOException {
    return isLoaded;
  }

  @Override
  public void reset() throws IOException {
    isLoaded = false;
    Arrays.fill(minFoodID, -1);
    Arrays.fill(maxFoodID, -1);
    Arrays.fill(minIndustryID, -1);
    Arrays.fill(minEnergyID, -1);
    Arrays.fill(maxEnergyID, -1);
    Arrays.fill(minIntelID, -1);
    Arrays.fill(maxIntelID, -1);
    Arrays.fill(minResearchID, -1);
    Arrays.fill(maxResearchID, -1);

    for (val seg : seg_minFoodIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_maxFoodIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_minIndustryIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_minEnergyIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_maxEnergyIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_minIntelIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_maxIntelIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_minResearchIDs)
      Arrays.fill(seg, null);
    for (val seg : seg_maxResearchIDs)
      Arrays.fill(seg, null);
  }

  @Override
  public void reload() throws IOException {
    // farms
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 2; ++i)
        seg_minFoodIDs[emp][i] = (IntValueSegment) trek.getSegment(SD_BldId_MainFarm.Start[emp][i], true);
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 5; ++i)
        seg_maxFoodIDs[emp][i] = (ValueSegment) trek.getSegment(SD_BldId_MainFarm.End[emp][i], true);
    // industry
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 3; ++i)
        seg_minIndustryIDs[emp][i] = (IntValueSegment) trek.getSegment(SD_BldId_MainIndustry.Start[emp][i], true);
    // energy
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 5; ++i)
        seg_minEnergyIDs[emp][i] = (IntValueSegment) trek.getSegment(SD_BldId_MainEnergy.Start[emp][i], true);
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 4; ++i)
        seg_maxEnergyIDs[emp][i] = (ValueSegment) trek.getSegment(SD_BldId_MainEnergy.End[emp][i], true);
    // intel
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 8; ++i)
        seg_minIntelIDs[emp][i] = (IntValueSegment) trek.getSegment(SD_BldId_MainIntel.Start[emp][i], true);
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 7; ++i)
        seg_maxIntelIDs[emp][i] = (ValueSegment) trek.getSegment(SD_BldId_MainIntel.End[emp][i], true);
    // research
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 9; ++i)
        seg_minResearchIDs[emp][i] = (IntValueSegment) trek.getSegment(SD_BldId_MainResearch.Start[emp][i], true);
    for (int emp = 0; emp < NUM_EMPIRES; ++emp)
      for (int i = 0; i < 8; ++i)
        seg_maxResearchIDs[emp][i] = (IntValueSegment) trek.getSegment(SD_BldId_MainResearch.End[emp][i], true);

    loadIdValues();
    isLoaded = true;
  }

  private void loadIdValues() {
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      // farms
      minFoodID[emp] = Integer.MAX_VALUE;
      for (val seg : seg_minFoodIDs[emp])
        minFoodID[emp] = Integer.min(minFoodID[emp], seg.intValue());
      maxFoodID[emp] = -1;
      for (val seg : seg_maxFoodIDs[emp])
        maxFoodID[emp] = Integer.max(maxFoodID[emp], seg.intValue());
      // industry
      minIndustryID[emp] = Integer.MAX_VALUE;
      for (val seg : seg_minIndustryIDs[emp])
        minIndustryID[emp] = Integer.min(minIndustryID[emp], seg.intValue());
      // energy
      minEnergyID[emp] = Integer.MAX_VALUE;
      for (val seg : seg_minEnergyIDs[emp])
        minEnergyID[emp] = Integer.min(minEnergyID[emp], seg.intValue());
      maxEnergyID[emp] = -1;
      for (val seg : seg_maxEnergyIDs[emp])
        maxEnergyID[emp] = Integer.max(maxEnergyID[emp], seg.intValue());
      // intel
      minIntelID[emp] = Integer.MAX_VALUE;
      for (val seg : seg_minIntelIDs[emp])
        minIntelID[emp] = Integer.min(minIntelID[emp], seg.intValue());
      maxIntelID[emp] = -1;
      for (val seg : seg_maxIntelIDs[emp])
        maxIntelID[emp] = Integer.max(maxIntelID[emp], seg.intValue());
      // research
      minResearchID[emp] = Integer.MAX_VALUE;
      for (val seg : seg_minResearchIDs[emp])
        minResearchID[emp] = Integer.min(minResearchID[emp], seg.intValue());
      maxResearchID[emp] = -1;
      for (val seg : seg_maxResearchIDs[emp])
        maxResearchID[emp] = Integer.max(maxResearchID[emp], seg.intValue());
    }
  }

  public int getMinId(int empire, int bldType) {
    switch (bldType) {
      case StructureGroup.MainFood:
        return minFoodID[empire];
      case StructureGroup.MainIndustry:
        return minIndustryID[empire];
      case StructureGroup.MainEnergy:
        return minEnergyID[empire];
      case StructureGroup.MainIntel:
        return minIntelID[empire];
      case StructureGroup.MainResearch:
        return minResearchID[empire];
      default:
        throw new IllegalArgumentException();
    }
  }

  public int getMaxId(int empire, int bldType) {
    switch (bldType) {
      case StructureGroup.MainFood:
        return maxFoodID[empire];
      case StructureGroup.MainEnergy:
        return maxEnergyID[empire];
      case StructureGroup.MainIntel:
        return maxIntelID[empire];
      case StructureGroup.MainResearch:
        return maxResearchID[empire];
      default:
        throw new IllegalArgumentException();
    }
  }

  public void setMinId(int empire, int bldType, int id) throws InvalidArgumentsException {
    if (id < 0)
      throw new InvalidArgumentsException();

    if (id != getMinId(empire, bldType)) {
      switch (bldType) {
        case StructureGroup.MainFood:
          minFoodID[empire] = id;
          break;
        case StructureGroup.MainIndustry:
          minIndustryID[empire] = id;
          break;
        case StructureGroup.MainEnergy:
          minEnergyID[empire] = id;
          break;
        case StructureGroup.MainIntel:
          minIntelID[empire] = id;
          break;
        case StructureGroup.MainResearch:
          minResearchID[empire] = id;
          break;
        default:
          throw new IllegalArgumentException();
      }

      IntValueSegment[] segments = (IntValueSegment[]) getMinSegments(empire, bldType);
      for (val seg : segments)
        seg.setValue(id);
    }
  }

  public void setMaxId(int empire, int bldType, int id) throws InvalidArgumentsException {
    if (id < 0)
      throw new InvalidArgumentsException();

    if (id != getMaxId(empire, bldType)) {
      switch (bldType) {
        case StructureGroup.MainFood:
          maxFoodID[empire] = id;
          break;
        case StructureGroup.MainEnergy:
          maxEnergyID[empire] = id;
          break;
        case StructureGroup.MainIntel:
          maxIntelID[empire] = id;
          break;
        case StructureGroup.MainResearch:
          maxResearchID[empire] = id;
          break;
        default:
          throw new IllegalArgumentException();
      }

      ValueSegment[] segments = getMaxSegments(empire, bldType);
      for (val seg : segments)
        seg.setIntValue(id);
    }
  }

  @Override
  public void check(Vector<String> response) {
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      for (val seg : seg_minFoodIDs[emp])
        seg.check(response);
      for (val seg : seg_maxFoodIDs[emp])
        seg.check(response);
      for (val seg : seg_minIndustryIDs[emp])
        seg.check(response);
      for (val seg : seg_minEnergyIDs[emp])
        seg.check(response);
      for (val seg : seg_maxEnergyIDs[emp])
        seg.check(response);
      for (val seg : seg_minIntelIDs[emp])
        seg.check(response);
      for (val seg : seg_maxIntelIDs[emp])
        seg.check(response);
      for (val seg : seg_minResearchIDs[emp])
        seg.check(response);
      for (val seg : seg_maxResearchIDs[emp])
        seg.check(response);
    }
  }

  private ValueSegment[] getMinSegments(int empire, int bldType) {
    switch (bldType) {
      case StructureGroup.MainFood:
        return seg_minFoodIDs[empire];
      case StructureGroup.MainIndustry:
        return seg_minIndustryIDs[empire];
      case StructureGroup.MainEnergy:
        return seg_minEnergyIDs[empire];
      case StructureGroup.MainIntel:
        return seg_minIntelIDs[empire];
      case StructureGroup.MainResearch:
        return seg_minResearchIDs[empire];
      default:
        throw new IllegalArgumentException();
    }
  }

  private ValueSegment[] getMaxSegments(int empire, int bldType) {
    switch (bldType) {
      case StructureGroup.MainFood:
        return seg_maxFoodIDs[empire];
      case StructureGroup.MainEnergy:
        return seg_maxEnergyIDs[empire];
      case StructureGroup.MainIntel:
        return seg_maxIntelIDs[empire];
      case StructureGroup.MainResearch:
        return seg_maxResearchIDs[empire];
      default:
        throw new IllegalArgumentException();
    }
  }
}
