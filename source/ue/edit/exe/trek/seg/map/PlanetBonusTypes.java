

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.exception.InvalidArgumentsException;

/**
 * Planet bonus types
 */
public class PlanetBonusTypes extends InternalSegment {

  public static final String FILENAME = CTrekSegments.PlanetBonusTypes;
  public static final int SEGMENT_ADDRESS = 0x0003E260;
  public static final int MAPPED_PLANET_TYPES = 6;
  public static final int SIZE = MAPPED_PLANET_TYPES * Integer.BYTES;
  public static final int energy = 0xE9;
  public static final int food = 0xF6;

  public static final int[] Default_bonusTypes = new int[] {
    0xe9, // loc_43EEE9
    0xe9, // loc_43EEE9
    0xf6, // loc_43EEF6
    0xf6, // loc_43EEF6
    0xf6, // loc_43EEF6
    0xe9, // loc_43EEE9
  };

  private static byte[] test = new byte[]{(byte) 0xEE, 0x43, 0x00};

  private int[] type = new int[MAPPED_PLANET_TYPES];

  public PlanetBonusTypes() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, PlanetBonusTypes.class));
  }

  /**
   * @param in  the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    for (int i = 0; i < MAPPED_PLANET_TYPES; i++) {
      type[i] = in.read();
      validate(in, test);
    }

    markSaved();
  }

  /**
   * Used to save changes.
   *
   * @param out the OutputStream to write the file to.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < MAPPED_PLANET_TYPES; i++) {
      out.write(type[i]);
      out.write(test);
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < MAPPED_PLANET_TYPES; i++) {
      out.write(Default_bonusTypes[i]);
      out.write(test);
    }
  }

  @Override
  public void clear() {
    Arrays.fill(type, 0);
    markChanged();
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < MAPPED_PLANET_TYPES; i++) {
      if (type[i] != energy && type[i] != food) {
        String msg = "Invalid planet bonus type (0x%1) for planet type ID %2.";
        msg = msg.replace("%1", Integer.toHexString(type[i]));
        msg = msg.replace("%2", Integer.toString(i + 2));
        response.add(this.getCheckIntegrityString(InternalSegment.INTEGRITY_CHECK_ERROR, msg));
      }
    }
  }

  @Override
  public boolean isDefault() {
    for (int t = 0; t < type.length; ++t)
      if (type[t] != Default_bonusTypes[t])
        return false;
    return true;
  }

  public void setBonusType(int planet_type, int bonus_type) throws InvalidArgumentsException {
    if (bonus_type != energy && bonus_type != food) {
      String msg = "Invalid planet bonus type: 0x%1";
      msg = msg.replace("%1", Integer.toString(bonus_type));
      throw new InvalidArgumentsException(msg);
    }

    planet_type = planet_type - 2;

    if (planet_type < 0 || planet_type >= MAPPED_PLANET_TYPES) {
      String msg = "Invalid or unsupported planet type: 0x%1";
      msg = msg.replace("%1", Integer.toString(planet_type + 2));
      throw new InvalidArgumentsException(msg);
    }

    if (type[planet_type] != bonus_type) {
      type[planet_type] = bonus_type;
      markChanged();
    }
  }

  public int getBonusType(int planet_type) {
    planet_type = planet_type - 2;
    if (planet_type < 0 || planet_type >= MAPPED_PLANET_TYPES)
      return energy;

    return type[planet_type];
  }

  @Override
  public void reset() {
    for (int t = 0; t < type.length; ++t) {
      if (type[t] != Default_bonusTypes[t]) {
        type[t] = Default_bonusTypes[t];
        markChanged();
      }
    }
  }
}
