package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class contains a code fixture for the MUM stellar details panel name mappings.
 * Can be removed once MUM is fixed.
 */
public class StellTypeDetailsNameMumFix extends InternalSegment {

  public static final String NAME = "stellType_detailsNameMumFix";
  public static final int SEGMENT_ADDRESS = 0x000B5824; // asm_4B6424
  public static final int SIZE = 48; // 0x30

  private static final String DESC = "Sneaks in to revert an unused MUM patch to reduce code space"
    + " of the stellar type name mappings at 0xB59EB / asm_4B65EB. (read only support)"
    + "<br>Further overwrites GALM mappings that become unused when the extended details name mappings patch is applied.";

  public static final SegmentDefinition SEGMENT_DEFINITION =
    new SegmentDefinition(SEGMENT_ADDRESS, NAME, StellTypeDetailsNameMumFix.class)
      .desc(DESC).size(SIZE);

  private interface DefaultAddr {
    static final int System       = 0x4B64D8;
    static final int BlackHole    = 0x4B682E;
    static final int XRayPulsar   = 0x4B6844;
    static final int Nebula       = 0x4B685A;
    static final int NeutronStar  = 0x4B6870;
    static final int WormHole     = 0x4B6886;
    static final int RadioPulsar  = 0x4B689C;
  }

  private interface MumAddr {
    static final int System       = 0x4B64D8;
    static final int BlackHole    = 0x4B682E;
    static final int XRayPulsar   = 0x4B6844;
    static final int Nebula       = 0x4B684B;
    static final int NeutronStar  = 0x4B6852;
    static final int WormHole     = 0x4B6859;
    static final int RadioPulsar  = 0x4B6860;
  }

  private interface GalmAddr {
    static final int System           = 0x4B64D8;
    static final int ProtoNebula      = 0x4B682E;
    static final int Pulsar           = 0x4B6844;
    static final int EmissionNebula   = 0x4B684B;
    static final int QuantumFilament  = 0x4B6852;
    static final int Wormhole         = 0x4B6859;
    static final int RedGiant         = 0x4B6860;
    static final int Blackhole        = 0x4B6867;
    static final int Unused           = 0x90909090; // PlanetaryNebula
  }

  // offsets used for lexicon description lookup
  private static final int[] DefaultOffsets = new int[] { // 48
    DefaultAddr.BlackHole,
    DefaultAddr.XRayPulsar,
    DefaultAddr.Nebula,
    DefaultAddr.NeutronStar,
    DefaultAddr.System,  // red gas giant
    DefaultAddr.System,  // orange star
    DefaultAddr.System,  // yellow star
    DefaultAddr.System,  // white star
    DefaultAddr.System,  // green star
    DefaultAddr.System,  // blue star
    DefaultAddr.WormHole,
    DefaultAddr.RadioPulsar,
  };

  private static final int[] MumOffsets = new int[] { // 48
    MumAddr.BlackHole,
    MumAddr.XRayPulsar,
    MumAddr.Nebula,
    MumAddr.NeutronStar,
    MumAddr.System,       // red gas giant
    MumAddr.System,       // orange star
    MumAddr.System,       // yellow star
    MumAddr.System,       // white star
    MumAddr.System,       // green star
    MumAddr.System,       // blue star
    MumAddr.WormHole,
    MumAddr.RadioPulsar,
  };

  private static final int[] GalmOffsets = new int[] { // 56
    GalmAddr.ProtoNebula,
    GalmAddr.Pulsar,
    GalmAddr.EmissionNebula,
    GalmAddr.QuantumFilament,
    GalmAddr.System,       // red gas giant
    GalmAddr.System,       // orange star
    GalmAddr.System,       // yellow star
    GalmAddr.System,       // white star
    GalmAddr.System,       // green star
    GalmAddr.System,       // blue star
    GalmAddr.Wormhole,
    GalmAddr.RedGiant,
    GalmAddr.Blackhole,
    GalmAddr.Unused,
  };

  @Getter
  private boolean isMUM = false;
  private boolean isGALM = false;

  public StellTypeDetailsNameMumFix(SegmentDefinition sd) {
    super(sd);
  }

  @Override
  public void load(InputStream in) throws IOException { // 48
    for (int i = 0; i < 12; ++i) {              // 12*4 = 48
      int addr = StreamTools.readInt(in, true);
      if (addr == DefaultOffsets[i])
        continue;

      if (!isGALM && addr == MumOffsets[i])
        isMUM = true;
      else if (!isMUM && addr == GalmOffsets[i+2])
        isGALM = true;
      else
        failed(addr, DefaultOffsets[i]);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    // only vanilla default supported
    saveDefault(out);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException { // 48
    for (int i = 0; i < 12; ++i) {              // 12*4 = 48
      out.write(DataTools.toByte(DefaultOffsets[i], true));
    }
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    return true;
  }

  @Override
  public void reset() {
  }

  @Override
  public void clear() {
  }
}
