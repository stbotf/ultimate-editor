package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.seg.sub.Sub_43D800;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx.Galaxy.StellarObjects;
import ue.exception.SegmentException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class contains the extended tooltip stellar name mapping fixture.
 */
public class StellTypeTooltipNameMap extends InternalSegment implements Patch {

  public static final String NAME = "stellType_tooltipNameMap";
  public static final int SEGMENT_ADDRESS = 0x0003CBCC; // asm_43D7CC
  public static final int SIZE = 228; // 0xE4

  public static final byte MaxSpaceObjects = 57;

  private static final String DESC = "Extends the stellar object tooltip"
    + " name mappings to max 57 stellar types.";

  public static final SegmentDefinition SEGMENT_DEFINITION =
    new SegmentDefinition(SEGMENT_ADDRESS, NAME, StellTypeTooltipNameMap.class)
      .desc(DESC).size(SIZE);

  private interface SwitchAddr {
    static final int BlackHole    = 0x43D832;
    static final int XRayPulsar   = 0x43D83C;
    static final int Nebula       = 0x43D846;
    static final int NeutronStar  = 0x43D850;
    static final int RedGasGiant  = 0x43D85A;
    static final int OrangeStar   = 0x43D864;
    static final int YellowStar   = 0x43D86E;
    static final int WhiteStar    = 0x43D878;
    static final int GreenStar    = 0x43D882;
    static final int BlueStar     = 0x43D88C;
    static final int WormHole     = 0x43D896;
    static final int RadioPulsar  = 0x43D8A0;
  }

  // offsets used for lexicon dictionary lookup of the names
  private static final int[] DefaultOffsets = new int[] { // 48
    SwitchAddr.BlackHole,
    SwitchAddr.XRayPulsar,
    SwitchAddr.Nebula,
    SwitchAddr.NeutronStar,
    SwitchAddr.RedGasGiant,
    SwitchAddr.OrangeStar,
    SwitchAddr.YellowStar,
    SwitchAddr.WhiteStar,
    SwitchAddr.GreenStar,
    SwitchAddr.BlueStar,
    SwitchAddr.WormHole,
    SwitchAddr.RadioPulsar,
  };

  private static final short NoLexID = -1;

  // ids used for lexicon dictionary lookup of the stellar object names
  private static final short[] DefaultLexIDs = new short[] { // 24
    StellarObjects.BlackHole,
    StellarObjects.XRayPulsar,
    StellarObjects.Nebula,
    StellarObjects.NeutronStar,
    StellarObjects.RedGiant,
    StellarObjects.OrangeStar,
    StellarObjects.YellowStar,
    StellarObjects.WhiteStar,
    StellarObjects.GreenStar,
    StellarObjects.BlueStar,
    StellarObjects.Wormhole,
    StellarObjects.RadioPulsar
  };

  @Getter private boolean isExtended = false;
  @Getter private int     numStellTypes = 0;

  // for vanilla code we have 12 offsets, but just the values 0-3 plus 10 and 11 can bet set
  // when extended, all descriptions can be set independently
  private int[] nameIds = new int[MaxSpaceObjects];

  public StellTypeTooltipNameMap(SegmentDefinition sd) {
    super(sd);
    Arrays.fill(nameIds, NoLexID);
  }

  /**
   * @param in  the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException { // 228
    byte[] data = StreamTools.readBytes(in, 58);      // 58
    boolean isGALM = data[56] == OpCode.CMP_AL;

    if (isGALM) {
      isExtended = true;
      numStellTypes = data[57]+1;

      if (numStellTypes < 0 || numStellTypes > MaxSpaceObjects) {
        String err = "The specified number of %1 space objects exceeds the max supported limit of %2."
          .replace("%1", Integer.toString(numStellTypes))
          .replace("%2", Integer.toString(MaxSpaceObjects));
        throw new SegmentException(err);
      }

      // for GALM the whole subroutine is moved by 4 bytes
      validate(in, Sub_43D800.start);                 // 4
      validate(in, Sub_43D800.loc_43D806_beg);        // 15

      // check for broken unknown StarType log call
      // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57543#p57543
      data = StreamTools.readBytes(in, 5);            // 5
      if (!check(data, Sub_43D800.asm_43D815_call))
        validate(data, Sub_43D800.asm_43D819_galm);

      validate(in, Sub_43D800.loc_43D806_end);        // 12
      validate(in, Sub_43D800.loc_43D826_and);        // 5
      validate(in, Sub_43D800.asm_43D82F_galm);       // 7

      // read GALM lexicon ids - to be removed once patched
      loadGALMIds(in);                                // 101

      validate(in, 21, 0);                            // 21
    } else {
      // align 10h
      validate(data, 0, 4, (byte)0);

      // validate vanilla jump table offsets
      validateOffsets(data);

      validate(data[52], OpCode.CMP_AL);
      numStellTypes = data[53]+1;

      if (numStellTypes < 0 || numStellTypes > MaxSpaceObjects) {
        String err = "The specified number of %1 space objects exceeds the max supported limit of %2."
          .replace("%1", Integer.toString(numStellTypes))
          .replace("%2", Integer.toString(MaxSpaceObjects));
        throw new SegmentException(err);
      }

      validate(data, 54, Sub_43D800.start);
      validate(in, Sub_43D800.loc_43D806_beg);        // 15
      validate(in, Sub_43D800.asm_43D815_call);       // 5
      validate(in, Sub_43D800.loc_43D806_end);        // 12
      validate(in, Sub_43D800.loc_43D826_and);        // 5

      // read jump switch instruction
      data = StreamTools.readBytes(in, 7);            // 7

      if (check(data, 0, 7, Sub_43D800.asm_43D82B_fix)) {
        isExtended = true;

        // validate code fixture
        data = StreamTools.readBytes(in, 11);         // 11
        validate(data, 0, data.length, Sub_43D800.asm_43D82B_fix, 7);
        validate(in, 0); // align 2h                  // 1

        // load patched lexicon ids
        loadExtendedIdTable(in);                      // 114
      } else {
        // validate orig last for error code deviation
        isExtended = false;

        // validate unpatched vanilla jump switch
        validate(data, Sub_43D800.asm_43D82B_jmp);

        loadOrigIds(in);                              // 120

        // align 10h
        validate(in, 6, 0);                           // 6
      }
    }

    markSaved();
  }

  /**
   * Used to save changes.
   * @param out the OutputStream to write the file to.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    save(out, isExtended);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    save(out, false);
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  public boolean setNumStellTypes(int num) {
    if (numStellTypes != num) {
      numStellTypes = num;
      markChanged();
      return true;
    }
    return false;
  }

  public int getLexId(int index) {
    return nameIds[index];
  }

  public boolean setLexId(int index, int lexId) {
    if (nameIds[index] != lexId) {
      nameIds[index] = lexId;
      markChanged();
      return true;
    }
    return false;
  }

  public void insert(int index, int lexId) {
    // shift entries
    for (int i = nameIds.length - 1; i > index; --i)
      nameIds[i] = nameIds[i-1];

    nameIds[index] = lexId;
    markChanged();
  }

  public void remove(int index) {
    // shift entries
    int lastIdx = nameIds.length - 1;
    for (int i = index; i < lastIdx; ++i)
      nameIds[i] = nameIds[i+1];

    nameIds[lastIdx] = NoLexID;
    markChanged();
  }

  @Override
  public boolean isDefault() {
    if (isExtended)
      return false;

    for (int i = 0; i < nameIds.length; ++i) {
      int expected = i < DefaultLexIDs.length ? DefaultLexIDs[i] : NoLexID;
      if (nameIds[i] != expected)
        return false;
    }

    return true;
  }

  @Override
  public boolean isPatched() {
    return isExtended;
  }

  @Override
  public void patch() {
    if (!isPatched()) {
      isExtended = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (!isDefault()) {
      isExtended = false;
      markChanged();
    }
  }

  public void setExtended(boolean enable) {
    if (isExtended != enable) {
      isExtended = enable;
      markChanged();
    }
  }

  @Override
  public void reset() {
    unpatch();
  }

  @Override
  public void clear() {
    isExtended = false;
  }

  @Override
  public void check(Vector<String> response) {
  }

  private void validateOffsets(byte[] data) throws IOException {
    // validate offsets used for lexicon name lookup
    for (int i = 0; i < DefaultOffsets.length; ++i) {
      int offset = DataTools.toInt(data, 4 + i*4, true);
      validate(offset, DefaultOffsets[i]);
    }
  }

  private void loadGALMIds(InputStream in) throws IOException { // 101
    validate(in, MovRegInt.EAX);                    // 1
    nameIds[StellarType.BlackHole] = StreamTools.readInt(in, true); // 4

    // jmp  Lexicon_Term Sub_48B2E0
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, 0x4DAA0);                       // 4

    for (int i = 1; i < 14; ++i) {                  // 13*7 = 91
      // mov  eax, <lexId>
      validate(in, MovRegInt.EAX);                  // 1
      nameIds[i] = StreamTools.readInt(in, true);   // 4

      // jmp to jmp Lexicon_Term Sub_48B2E0
      validate(in, JmpShort.JMP);                   // 1
      int lexJmpOffset = -5 - i * 7;
      validate(in, lexJmpOffset);                   // 1
    }
  }

  private void loadExtendedIdTable(InputStream in) throws IOException { // 114
    // load space object name ids
    for (int i = 0; i < nameIds.length; ++i)        // 57*2 = 114
      nameIds[i] = StreamTools.readShort(in, true);
  }

  private void loadOrigIds(InputStream in) throws IOException { // 120
    for (int i = 0; i < 12; ++i) {                  // 12*10 = 120
      validate(in, MovRegInt.EAX);                  // 1
      nameIds[i] = StreamTools.readInt(in, true);   // 4

      // jmp  Lexicon_Term Sub_48B2E0
      validate(in, Jmp.JMP);                        // 1
      int lexTermOffset = 0x4DAA4 - i * 10;
      validateInt(in, lexTermOffset);               // 4
    }
  }

  private void save(OutputStream out, boolean extended) throws IOException { // 228
    // align 10h
    StreamTools.write(out, 0, 4);

    // write vanilla jump table offsets
    saveOffsets(out);                               // 48

    out.write(OpCode.CMP_AL);
    out.write(numStellTypes-1);
    out.write(Sub_43D800.start);
    out.write(Sub_43D800.loc_43D806_beg);           // 15
    out.write(Sub_43D800.asm_43D815_call);          // 5
    out.write(Sub_43D800.loc_43D806_end);           // 12
    out.write(Sub_43D800.loc_43D826_and);           // 5

    if (isExtended) {
      // write code fixture
      out.write(Sub_43D800.asm_43D82B_fix);         // 18
      out.write(0); // align 2h                     // 1

      // write patched lexicon ids
      saveExtendedIdTable(out);                         // 114
    } else {
      // validate unpatched vanilla jump switch
      out.write(Sub_43D800.asm_43D82B_jmp);         // 7

      // write lexicon ids
      saveOrigIds(out);                             // 120

      // align 10h
      StreamTools.write(out, 0, 6);                 // 6
    }
  }

  private void saveOffsets(OutputStream out) throws IOException {
    // offsets used for lexicon name lookup
    for (int offset : DefaultOffsets)               // 48
      out.write(DataTools.toByte(offset, true));
  }

  private void saveExtendedIdTable(OutputStream out) throws IOException { // 114
    // ids for lexicon name lookup
    for (int id : nameIds)                                      // 57*2 = 114
      out.write(DataTools.toByte((short)id, true));
    for (int i = 0; i < MaxSpaceObjects - nameIds.length; ++i)  // 0
      out.write(DataTools.toByte(NoLexID, true));
  }

  private void saveOrigIds(OutputStream out) throws IOException { // 120
    for (int i = 0; i < 12; ++i) {                      // 12*10 = 120
      // mov   eax, <lexId>
      out.write(MovRegInt.EAX);                         // 1
      out.write(DataTools.toByte(nameIds[i], true));    // 4

      // jmp  Lexicon_Term Sub_48B2E0
      int lexTermOffset = 0x4DAA4 - i * 10;
      out.write(Jmp.JMP);                               // 1
      out.write(DataTools.toByte(lexTermOffset, true)); // 4
    }
  }
}
