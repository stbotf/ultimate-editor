package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.JmpIf;
import ue.edit.exe.seg.prim.MulRegByte;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class contains a code fixture for the GALM stellar type map generation.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57607#p57607
 */
public class StellTypeGenGALMFix extends InternalSegment implements Patch {

  public static final String NAME = "stellType_genGalmFix";
  public static final int SEGMENT_ADDRESS = 0x000AF924; // asm_4B0524
  public static final int SIZE = 9;

  public static final int Default_JGE = 0x311;  // jge     loc_4B083B (+785)
  public static final int GALM_JGE    = 0x31D;  // jge     loc_4B0847 (+797)
  public static final int DefaultStellarEntrySize = 12;
  public static final int FaultyGALMEntrySize = 15;

  private static final String DESC = "Sneaks in to revert GALM JGE and some faulty entry size patch"
    + " of the galaxy map generation at 0xAF924 / asm_4B0524."
    + "<br>See https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57607#p57607";

  public static final SegmentDefinition SEGMENT_DEFINITION =
    new SegmentDefinition(SEGMENT_ADDRESS, NAME, StellTypeGenGALMFix.class)
      .desc(DESC).size(SIZE);

  @Getter private boolean isGALM = false;

  public StellTypeGenGALMFix(SegmentDefinition sd) {
    super(sd);
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, JmpIf.JGE);

    // check for GALM jge patch
    int jmpAddr = StreamTools.readInt(in, true);
    isGALM = jmpAddr == GALM_JGE;
    if (!isGALM)
      validate(jmpAddr, Default_JGE);

    // 0x000AF92A / asm_4B052A
    // this looks to be a stellar type entry size multiplier
    validate(in, MulRegByte.EAX);
    int entrySize = in.read();
    if (!isGALM || entrySize != FaultyGALMEntrySize)
      validate(entrySize, DefaultStellarEntrySize);

    markSaved();
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    return !isGALM;
  }

  @Override
  public boolean isPatched() {
    return isGALM;
  }

  @Override
  public void patch() throws IOException {
    // may not be patched
  }

  @Override
  public void unpatch() throws IOException {
    // unpatched by default
  }

  @Override
  public void save(OutputStream out) throws IOException {
    // sneak in to revert faulty GALM patches
    saveDefault(out);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    // sneak in to revert the GALM jge patch
    out.write(JmpIf.JGE);                           // 2
    out.write(DataTools.toByte(Default_JGE, true)); // 4

    // sneak in to revert faulty GALM stellar type entry size patch
    // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57607#p57607
    out.write(MulRegByte.EAX);                      // 2
    out.write(12);                                  // 1
  }

  @Override
  public void reset() {
  }

  @Override
  public void clear() {
  }
}
