package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * by default large X is shared with large Y 0xae740
 * this fixture separates the values
 * in replacement, large galaxy width from 0xae75d+6 reuses address 0xae6e0+6
 */
public class LargeSpiralAxisFix extends IntValueSegment implements Patch {

  public static final int SEGMENT_ADDRESS = 0xae75d;
  public static final int SIZE = 15;

  public static final String PREFIX = "galaxyShape"; //$NON-NLS-1$
  public static final String DESCRIPTION = "By default large X is shared with large Y 0xae740."
    + "\nWhen patched, the large galaxy width segment 0xae75d is replaced"
    + " by shared large galaxy width 0xae6e0.";

  public static final int DEFAULT_LargeWidth = 25;
  public static final int DEFAULT_LargeSpiralAxis = 15;

  public static final ValueDefinition<Integer> SEGMENT_DEFINITION =
    new ValueDefinition<Integer>(SEGMENT_ADDRESS, PREFIX, LargeSpiralAxisFix.class, DEFAULT_LargeWidth)
      .suffix().desc(DESCRIPTION).size(SIZE);

  // orig
  private static final int mov_mem = 0xC7;
  private static final byte[] test_orig2 = new byte[]{
    (byte) 0x05, (byte) 0x14, (byte) 0xB3, (byte) 0x5C, (byte) 0x00
  };
  private static final byte[] test_orig3 = new byte[]{
    (byte) 0xE9, (byte) 0x49, (byte) 0xFD, (byte) 0xFF, (byte) 0xFF
  };

  // patched
  private static final int mov_esi = 0xBE;
  // fill empty space with nops and jump to code that sets map width before exit
  private static final byte[] test_patched2 = new byte[]{
    (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90,
    (byte) 0xE9, (byte) 0x74, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF
  };

  @Getter
  private boolean patched = false;

  public LargeSpiralAxisFix(SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    patched = false;

    int op = in.read();
    if (op == mov_mem) {
      validate(in, test_orig2);
      value = StreamTools.readInt(in, true);
      validate(in, test_orig3);
    } else {
      validate(op, mov_esi);
      value = StreamTools.readInt(in, true);
      validate(in, test_patched2);
      patched = true;
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (patched) {
      out.write(mov_esi);                       // 1
      out.write(DataTools.toByte(value, true)); // 4
      out.write(test_patched2);                 // 10
    } else {
      out.write(mov_mem);                       // 1
      out.write(test_orig2);                    // 5
      out.write(DataTools.toByte(value, true)); // 4
      out.write(test_orig3);                    // 5
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(mov_mem);                                 // 1
    out.write(test_orig2);                              // 5
    out.write(DataTools.toByte(defaultValue(), true));  // 4
    out.write(test_orig3);                              // 5
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void patch() {
    if (!patched) {
      value = DEFAULT_LargeSpiralAxis;
      patched = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (patched) {
      value = DEFAULT_LargeWidth;
      patched = false;
      markChanged();
    }
  }

  public int getEllipseAxis() {
    if (!patched) {
      throw new UnsupportedOperationException(
        "The SpiralLargeAxisFix patch must be applied to read the ellipse axis.");
    }
    return intValue();
  }

  public int getGalaxyWidth() {
    if (patched) {
      throw new UnsupportedOperationException(
        "The SpiralLargeAxisFix patch must be unapplied to read the galaxy width.");
    }
    return intValue();
  }

  public void setEllipseAxis(int value) throws InvalidArgumentsException {
    if (!patched) {
      throw new UnsupportedOperationException(
        "The SpiralLargeAxisFix patch must be applied to set the ellipse axis.");
    }
    setValue(value);
  }

  public void setGalaxyWidth(int value) throws InvalidArgumentsException {
    if (patched) {
      throw new UnsupportedOperationException(
        "The SpiralLargeAxisFix patch must be unapplied to set the galaxy width.");
    }
    setValue(value);
  }
}
