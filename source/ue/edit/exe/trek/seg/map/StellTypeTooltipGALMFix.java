package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;

/**
 * This class contains a code fixture for the GALM stellar type tooltips.
 */
public class StellTypeTooltipGALMFix extends Jmp implements Patch {

  public static final String NAME = "stellType_tooltipGalmFix";
  public static final int SEGMENT_ADDRESS = 0x000DE74D; // asm_4DF34D

  public static final int Default_CALL = 0xFFF5E4AE;  // call    sub_43D800 (-0xA1B52)
  public static final int GALM_CALL    = 0xFFF5E4B2;  // call    sub_43D804 (-0xA1B4E)

  private static final String DESC = "Reverts GALM CALL patch at 0xDE74D / asm_4DF34D to unmodified vanilla code"
    + " if changed to extended galaxy generation patch.";

  public static final ValueDefinition<Integer> SEGMENT_DEFINITION =
    new ValueDefinition<Integer>(SEGMENT_ADDRESS, NAME, StellTypeTooltipGALMFix.class, Default_CALL)
      .flags(Jmp.CALL).desc(DESC).size(SIZE);

  @Getter private boolean isGALM = false;

  public StellTypeTooltipGALMFix(SegmentDefinition sd) {
    super(sd);
  }

  @Override
  public void load(InputStream in) throws IOException {
    super.load(in);

    // sneak in to revert the GALM call patch
    if (value == GALM_CALL) {
      value = Default_CALL;
      isGALM = true;
    } else {
      validate(value, Default_CALL);
    }

    markSaved();
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isPatched() {
    return isGALM;
  }

  @Override
  public void patch() throws IOException {
    // may not be patched
  }

  @Override
  public void unpatch() throws IOException {
    // unpatched by default
  }
}
