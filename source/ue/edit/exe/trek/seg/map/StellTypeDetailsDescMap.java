package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.seg.sub.Sub_4FB2D0;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.files.dic.idx.LexMenuIdx.Galaxy.SystemPanel.StellarDescriptions;
import ue.exception.SegmentException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class contains the extended stellar details panel description mapping fixture,
 * as well as the hard-coded neb-.tga animation fixture for the nebula object.
 */
public class StellTypeDetailsDescMap extends InternalSegment implements Patch {

  public static final String NAME = "stellType_detailsDescMap";
  public static final int SEGMENT_ADDRESS = 0x000FA694; // asm_4FB294
  public static final int SIZE = 364; // 0x16C

  public static final byte MaxSpaceObjects = 30;

  private static final String DESC = "Allows to both extend the stellar object details panel description"
    + " mappings to max 30 stellar types, as well as to disable the hard-coded nebula graphic."
    + "<br>When the nebula graphic is disabled, it is looked up from objstruc.smt in stbof.res instead.";

  public static final SegmentDefinition SEGMENT_DEFINITION =
    new SegmentDefinition(SEGMENT_ADDRESS, NAME, StellTypeDetailsDescMap.class)
      .desc(DESC).size(SIZE);

  private static final int DefaultCallbackMapAddress = 0x4FB240;

  private interface SwitchAddr {
    static final int Fallback     = 0x4FB34C;
    static final int BlackHole    = 0x4FB336;
    static final int XRayPulsar   = 0x4FB3B5;
    static final int Nebula       = 0x4FB3BF;
    static final int NeutronStar  = 0x4FB3DB;
    static final int WormHole     = 0x4FB3E5;
    static final int RadioPulsar  = 0x4FB3EF;
  }

  // offsets used for lexicon description lookup
  private static final int[] DefaultOffsets = new int[] { // 48
    SwitchAddr.BlackHole,
    SwitchAddr.XRayPulsar,
    SwitchAddr.Nebula,
    SwitchAddr.NeutronStar,
    SwitchAddr.Fallback, // red gas giant
    SwitchAddr.Fallback, // orange star
    SwitchAddr.Fallback, // yellow star
    SwitchAddr.Fallback, // white star
    SwitchAddr.Fallback, // green star
    SwitchAddr.Fallback, // blue star
    SwitchAddr.WormHole,
    SwitchAddr.RadioPulsar,
  };

  private static final short NoLexID = -1;

  // ids used for lexicon description lookup
  private static final short[] DefaultLexIDs = new short[] { // 24
    StellarDescriptions.BlackHole,
    StellarDescriptions.RadiativeEmission,
    StellarDescriptions.Nebula,
    StellarDescriptions.NeutronStarEmission,
    NoLexID, // red gas giant
    NoLexID, // orange star
    NoLexID, // yellow star
    NoLexID, // white star
    NoLexID, // green star
    NoLexID, // blue star
    StellarDescriptions.Wormhole,
    StellarDescriptions.RadioPulsarEmission,
  };

  // CmpByteReg.EAX num switch cases
  static final byte Default_LastSpObjType = 11;

  @Getter private boolean isNebulaHardCoded = false;
  @Getter private boolean isExtended = false;
  @Getter private int     numStellTypes = 0;

  // for vanilla code we have 12 offsets, but just the values 0-3 plus 10 and 11 can bet set
  // when extended, all descriptions can be set independently
  private int[] descIds = new int[MaxSpaceObjects];

  public StellTypeDetailsDescMap(SegmentDefinition sd) {
    super(sd);
    Arrays.fill(descIds, NoLexID);
  }

  /**
   * @param in  the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException { // 364
    byte[] data = StreamTools.readBytes(in, 12 + 12*4); // 60

    // check code alignment for off_4FB2A0 - 1
    // for unmodded vanilla, the first 12 bytes are zeroed
    // for patched code, they list the first 6 lexicon space object description ids
    // and therefore are not to be expected to all be set to zero
    isExtended = !check(data, 0, 12, (byte)0);
    boolean isGALM = false;

    if (isExtended) {
      // load patched lexicon ids
      loadExtendedIdTable(data);

      // validate start block of subroutine 4FB2D0
      validate(in, Sub_4FB2D0.start);               // 31

      data = StreamTools.readBytes(in, 6);          // 6
      isGALM = check(data, Sub_4FB2D0.jnz_4FB2EF);
      if (isGALM) {
        // GALM overrides the hard-coded nebula tga to free some space
        isNebulaHardCoded = false;
        // reset ids and discard the offset table
        // it holds no relevant information and is not fully read
        // since GALM moves additional code to free some space
        for (int i = 0; i < descIds.length; ++i)
          descIds[i] = NoLexID;
      } else {
        // validate jump to moved loc_4FB3A2 if not zero
        validate(data, Sub_4FB2D0.patched_4FB2EF);
      }
    } else {
      // validate vanilla jump table offsets
      validateOffsets(data);

      // validate start block of subroutine 4FB2D0
      validate(in, Sub_4FB2D0.start);               // 31

      // validate jump to loc_4FB3A2 if not zero
      validate(in, Sub_4FB2D0.jnz_4FB2EF);          // 6
    }

    // validate start of first vanilla local routine
    validate(in, Sub_4FB2D0.loc_4FB2F5);            // 53

    // asm_4FB32A: read num space object types (last id + 1)
    validate(in, CmpRegByte.EAX);                   // 2
    numStellTypes = in.read() + 1;                  // 1

    if (numStellTypes < 0 || numStellTypes > MaxSpaceObjects) {
      String err = "The specified number of %1 space objects exceeds the max supported limit of %2."
        .replace("%1", Integer.toString(numStellTypes))
        .replace("%2", Integer.toString(MaxSpaceObjects));
      throw new SegmentException(err);
    }

    // asm_4FB32D: handle default space object description
    validate(in, JmpShort.JA);                      // 1
    validate(in, 0x1D);                             // 1

    if (isGALM) {                                   // 209
      loadGALMJmpSwitch(in);
    } else if (isExtended) {
      loadExtendedJmpSwitch(in);
    } else {
      loadOrigJmpSwitch(in);
    }

    markSaved();
  }

  private void loadGALMJmpSwitch(InputStream in) throws IOException { // 209
    // asm_4FB32F: handle types by jump switch (jmp off_4FB290[eax*4])
    validate(in, Sub_4FB2D0.loc_4FB2F5_galm);     // 7
    // loc_4FB336: mov case 0, StellarDescriptions.BlackHole
    validate(in, MovRegInt.EAX);                  // 1
    descIds[StellarType.BlackHole] = StreamTools.readInt(in, true); // 4
    // loc_4FB33B: lexicon description load
    validate(in, Sub_4FB2D0.loc_4FB33B);          // 17

    validateFinish(in);                           // 86

    // loc_4FB3A2: write jnz ecx handling
    validate(in, Sub_4FB2D0.loc_4FB3A2);          // 19

    // loc_4FB3B5+: jump switch handling
    loadGALMIds(in);                              // 45

    // validate align end
    validate(in, 23, NOP);                        // 23
    validate(in, 7, 0);                           // 7
  }

  private void loadExtendedJmpSwitch(InputStream in) throws IOException { // 209
    // asm_4FB32F: handle nebula type 2
    validate(in, CmpRegByte.EAX);                 // 2
    validate(in, StellarType.Nebula);             // 1
    validate(in, JmpShort.JZ);                    // 1
    validate(in, 0x6E);                           // 1
    // asm_4FB32F: handle types by data table lookup (loc_4FB3CC)
    validate(in, Jmp.JMP);                        // 1
    validateInt(in, 0x80);                        // 4
    // loc_4FB339: write moved jnz ecx handling of loc_4FB3A2
    validate(in, Sub_4FB2D0.moved_4FB3A2);        // 19

    validateFinish(in);                           // 86

    // check for nopped nebula hard-coding of moved loc_4FB3BF
    byte[] data = StreamTools.readBytes(in, 18);  // 18
    isNebulaHardCoded = !check(data, NOP);
    if (isNebulaHardCoded) {
      // validate unpatched last to report original code deviation on error
      validate(data, Sub_4FB2D0.moved_nebula_4FB3BF);
    }

    // asm_4FB3B4: mov case 2
    validate(in, MovRegInt.EAX);                  // 1
    validateInt(in, StellarType.Nebula);          // 4

    // asm_4FB3B9: lexicon ID lookup
    validate(in, Sub_4FB2D0.patched_lex_lookup_4FB3B9); // 8

    // default -1 ids
    validate(in, CmpRegByte.EAX);                 // 2
    validate(in, 0);                              // 1
    validate(in, JmpShort.JB);                    // 1
    validate(in, 0x86);                           // 1

    // asm_4FB3C6: moved lexicon description load
    validate(in, Sub_4FB2D0.moved_4FB33B);        // 17
    // asm_4FB3D7: jump to finish
    validate(in, Jmp.JMP);                        // 1
    validateInt(in, -144);                        // 4

    // validate align end
    data = StreamTools.readBytes(in, 36);         // 36
    validate(data, 0);
  }

  private void loadOrigJmpSwitch(InputStream in) throws IOException { // 209
    // asm_4FB32F: handle types by jump switch (jmp off_4FB2A0[eax*4])
    validate(in, Sub_4FB2D0.loc_4FB2F5_jmp);      // 7
    // loc_4FB336: mov case 0, StellarDescriptions.BlackHole
    validate(in, MovRegInt.EAX);                  // 1
    descIds[StellarType.BlackHole] = StreamTools.readInt(in, true); // 4
    // loc_4FB33B: lexicon description load
    validate(in, Sub_4FB2D0.loc_4FB33B);          // 17

    validateFinish(in);                           // 86

    // loc_4FB3A2: write jnz ecx handling
    validate(in, Sub_4FB2D0.loc_4FB3A2);          // 19
    // loc_4FB3B5+: jump switch handling
    loadOrigIds(in);                              // 68

    // validate align end
    byte[] data = StreamTools.readBytes(in, 7);   // 7
    validate(data, 0);
  }

  /**
   * Used to save changes.
   *
   * @param out the OutputStream to write the file to.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    save(out, isExtended, isNebulaHardCoded);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    save(out, false, false);
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  public boolean setNumStellTypes(int num) {
    if (numStellTypes != num) {
      numStellTypes = num;
      markChanged();
      return true;
    }
    return false;
  }

  public int getLexId(int index) {
    return descIds[index];
  }

  public boolean setLexId(int index, int lexId) {
    if (descIds[index] != lexId) {
      descIds[index] = lexId;
      markChanged();
      return true;
    }
    return false;
  }

  public void insert(int index, int lexId) {
    // shift entries
    for (int i = descIds.length - 1; i > index; --i)
      descIds[i] = descIds[i-1];

    descIds[index] = lexId;
    markChanged();
  }

  public void remove(int index) {
    // shift entries
    int lastIdx = descIds.length - 1;
    for (int i = index; i < lastIdx; ++i)
      descIds[i] = descIds[i+1];

    descIds[lastIdx] = NoLexID;
    markChanged();
  }

  public boolean starDefaultsChanged() {
    for (int i = 4; i < 10; ++i) {
      if (descIds[i] != -1)
        return true;
    }
    return false;
  }

  @Override
  public boolean isDefault() {
    if (isExtended || !isNebulaHardCoded)
      return false;

    for (int i = 0; i < descIds.length; ++i) {
      int expected = i < DefaultLexIDs.length ? DefaultLexIDs[i] : NoLexID;
      if (descIds[i] != expected)
        return false;
    }

    return true;
  }

  @Override
  public boolean isPatched() {
    return isExtended || !isNebulaHardCoded;
  }

  @Override
  public void patch() {
    if (!isPatched()) {
      isNebulaHardCoded = false;
      isExtended = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (!isDefault()) {
      isNebulaHardCoded = true;
      isExtended = false;
      markChanged();
    }
  }

  public void setHardCodedNebula(boolean enable) {
    if (enable != isNebulaHardCoded) {
      isNebulaHardCoded = enable;
      markChanged();
    }
  }

  public void setExtended(boolean enable) {
    if (isExtended != enable) {
      isExtended = enable;
      markChanged();
    }
  }

  @Override
  public void reset() {
    unpatch();
  }

  @Override
  public void clear() {
    isNebulaHardCoded = true;
    isExtended = false;
  }

  @Override
  public void check(Vector<String> response) {
  }

  private void validateOffsets(byte[] data) throws IOException {
    // validate offsets used for lexicon description lookup
    for (int i = 0; i < DefaultOffsets.length; ++i) {
      int offset = DataTools.toInt(data, 12 + i*4, true);
      validate(offset, DefaultOffsets[i]);
    }
  }

  private void validateFinish(InputStream in) throws IOException { // 86
    // loc_4FB34C: the switch default
    validate(in, Sub_4FB2D0.loc_4FB34C_start);      // 46
    validate(in, MovRegInt.EBX);                    // 1
    validate(in, DataTools.toByte(DefaultCallbackMapAddress, true));  // 4
    validate(in, Sub_4FB2D0.loc_4FB34C_end);        // 20
    validate(in, Sub_4FB2D0.loc_4FB393);            // 9
    validate(in, Sub_4FB2D0.retn_4FB39C);           // 6
  }

  private void loadExtendedIdTable(byte[] data) {
    // load space object description ids
    for (int i = 0; i < descIds.length; ++i)
      descIds[i] = DataTools.toShort(data, i*2, true);
  }

  private void loadOrigIds(InputStream in) throws IOException { // 68
    // loc_4FB3B5: mov case 1, StellarDescriptions.RadiativeEmission
    validate(in, MovRegInt.EAX);                    // 1
    descIds[StellarType.XRayPulsar] = StreamTools.readInt(in, true); // 4
    // jmp     loc_4FB33B
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, -132);                          // 4

    // loc_4FB3BF (0xFA7BF): nebula HC fix
    byte[] data = StreamTools.readBytes(in, Sub_4FB2D0.nebula_4FB3BF.length); // 18
    isNebulaHardCoded = !check(data, NOP);
    if (isNebulaHardCoded)
      validate(data, Sub_4FB2D0.nebula_4FB3BF);

    // loc_4FB3BF+18: mov case 2, StellarDescriptions.Nebula
    validate(in, MovRegInt.EAX);                    // 1
    descIds[StellarType.Nebula] = StreamTools.readInt(in, true); // 4
    // jmp     loc_4FB33B
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, -160);                          // 4

    // loc_4FB3DB: mov case 3, StellarDescriptions.NeutronStarEmission
    validate(in, MovRegInt.EAX);                    // 1
    descIds[StellarType.NeutronStar] = StreamTools.readInt(in, true); // 4
    // jmp     loc_4FB33B
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, -170);                          // 4

    // loc_4FB3E5: mov case 10, StellarDescriptions.Wormhole
    validate(in, MovRegInt.EAX);                    // 1
    descIds[StellarType.WormHole] = StreamTools.readInt(in, true); // 4
    // jmp     loc_4FB33B
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, -180);                          // 4

    // loc_4FB3EF: mov case 11, StellarDescriptions.RadioPulsarEmission
    validate(in, MovRegInt.EAX);                    // 1
    descIds[StellarType.RadioPulsar] = StreamTools.readInt(in, true); // 4
    // jmp     loc_4FB33B
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, -190);                          // 4
  }

  // read GALM lexicon ids - to be removed once patched
  private void loadGALMIds(InputStream in) throws IOException { // 45
    // mov     case <idx>
    validate(in, MovRegInt.EAX);                    // 1
    descIds[StellarType.XRayPulsar] = StreamTools.readInt(in, true); // 4
    // jmp     loc_4FB33B
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, -132);                          // 4

    for (int i = 2; i < 7; ++i) {                   // 5*7 = 35
      int idx = i > 3 ? i + 6 : i;

      // mov case <idx>
      validate(in, MovRegInt.EAX);                  // 1
      descIds[idx] = StreamTools.readInt(in, true); // 4

      // jmp short to last jmp loc_4FB33B
      validate(in, JmpShort.JMP);                   // 1
      int jmpOffset = i > 2 ? -9 : -12;
      validate(in, jmpOffset);                      // 1
    }
  }

  private void save(OutputStream out, boolean extended, boolean hardCodedNebula) throws IOException { // 364
    if (extended) {
      // write patched lexicon ids
      saveExtendedIdTable(out);                     // 60

      // write start block of subroutine 4FB2D0
      out.write(Sub_4FB2D0.start);                  // 31
      // jump to moved loc_4FB3A2 if zero
      out.write(Sub_4FB2D0.patched_4FB2EF);         // 6
    } else {
      // code alignment for off_4FB2A0 - 1
      StreamTools.write(out, 0, 12);                // 12
      // write vanilla jump table offsets
      saveOffsets(out);                             // 48

      // write start block of subroutine 4FB2D0
      out.write(Sub_4FB2D0.start);                  // 31
      // jump to loc_4FB3A2 if zero
      out.write(Sub_4FB2D0.jnz_4FB2EF);             // 6
    }

    // loc_4FB2F5: write start of first vanilla local routine
    out.write(Sub_4FB2D0.loc_4FB2F5);               // 53

    // asm_4FB32A: write max switch case type number
    out.write(CmpRegByte.EAX);                      // 2
    int limit = Integer.min(numStellTypes, MaxSpaceObjects);
    out.write(limit-1);                             // 1

    // asm_4FB32D: handle default space object description
    out.write(JmpShort.JA);                         // 1
    out.write(0x1D);                                // 1

    if (extended) {                                 // 211
      // asm_4FB32F: handle nebula type 2
      out.write(CmpRegByte.EAX);                    // 2
      out.write(StellarType.Nebula);                // 1
      out.write(JmpShort.JZ);                       // 1
      out.write(0x6E);                              // 1
      // asm_4FB32F: handle types by data table lookup (loc_4FB3CC)
      out.write(Jmp.JMP);                           // 1
      out.write(DataTools.toByte(0x80, true));      // 4
      // loc_4FB339: write moved jnz ecx handling of loc_4FB3A2
      out.write(Sub_4FB2D0.moved_4FB3A2);           // 19

      saveFinish(out);                              // 86

      // write moved nebula type handling of loc_4FB3BF
      if (hardCodedNebula) {
        out.write(Sub_4FB2D0.moved_nebula_4FB3BF);  // 18
      } else {
        // nop nebula hard-coding
        StreamTools.write(out, NOP, Sub_4FB2D0.moved_nebula_4FB3BF.length); // 18
      }

      // asm_4FB3B4: mov case 2
      out.write(MovRegInt.EAX);                     // 1
      out.write(DataTools.toByte(StellarType.Nebula, true)); // 4

      // asm_4FB3B9: lexicon ID lookup
      out.write(Sub_4FB2D0.patched_lex_lookup_4FB3B9); // 8

      // default -1 ids
      out.write(CmpRegByte.EAX);                    // 2
      out.write(0);                                 // 1
      out.write(JmpShort.JB);                       // 1
      out.write(0x86);                              // 1

      // asm_4FB3C1: write moved lexicon description load
      out.write(Sub_4FB2D0.moved_4FB33B);           // 17
      // asm_4FB3D2: jump to finish
      out.write(Jmp.JMP);                           // 1
      out.write(DataTools.toByte(-144, true));      // 4

      StreamTools.write(out, 0, 36);                // 36
    } else {
      // asm_4FB32F: handle types by jump switch (jmp off_4FB2A0[eax*4])
      out.write(Sub_4FB2D0.loc_4FB2F5_jmp);         // 7
      // loc_4FB336: mov case 0
      out.write(MovRegInt.EAX);                     // 1
      out.write(DataTools.toByte(descIds[StellarType.BlackHole], true)); // 4
      // loc_4FB33B: write lexicon description load
      out.write(Sub_4FB2D0.loc_4FB33B);             // 17

      saveFinish(out);                              // 86

      // loc_4FB3A2: write jnz ecx handling
      out.write(Sub_4FB2D0.loc_4FB3A2);             // 19
      // loc_4FB3B5+: write jump switch handling
      saveOrigIds(out, hardCodedNebula);            // 68

      // write align end
      StreamTools.write(out, 0, 7);                 // 7
    }
  }

  private void saveOffsets(OutputStream out) throws IOException {
    // offsets used for lexicon description lookup
    for (int offset : DefaultOffsets)               // 48
      out.write(DataTools.toByte(offset, true));
  }

  private void saveFinish(OutputStream out) throws IOException { // 86
    // loc_4FB34C: the switch default
    out.write(Sub_4FB2D0.loc_4FB34C_start);         // 46
    out.write(MovRegInt.EBX);                       // 1
    out.write(DataTools.toByte(DefaultCallbackMapAddress, true));   // 4
    out.write(Sub_4FB2D0.loc_4FB34C_end);           // 20
    out.write(Sub_4FB2D0.loc_4FB393);               // 9
    out.write(Sub_4FB2D0.retn_4FB39C);              // 6
  }

  private void saveExtendedIdTable(OutputStream out) throws IOException { // 60
    // ids for lexicon description lookup
    for (int id : descIds)                          // 30 * 2
      out.write(DataTools.toByte((short)id, true));
    for (int i = 0; i < MaxSpaceObjects - descIds.length; ++i) // 0
      out.write(DataTools.toByte(NoLexID, true));
  }

  private void saveOrigIds(OutputStream out, boolean hardCodedNebula) throws IOException { // 68
    // loc_4FB3B5: mov case 1
    out.write(MovRegInt.EAX);                       // 1
    out.write(DataTools.toByte(descIds[StellarType.XRayPulsar], true)); // 4
    // jmp     loc_4FB33B
    out.write(Jmp.JMP);                             // 1
    out.write(DataTools.toByte(-132, true));        // 4

    // loc_4FB3BF (0xFA7BF)
    if (hardCodedNebula) {
      out.write(Sub_4FB2D0.nebula_4FB3BF);          // 18
    } else {
      // nop nebula hard-coding
      StreamTools.write(out, NOP, Sub_4FB2D0.nebula_4FB3BF.length); // 18
    }

    // loc_4FB3BF+18: mov case 2
    out.write(MovRegInt.EAX);                       // 1
    out.write(DataTools.toByte(descIds[StellarType.Nebula], true)); // 4
    // jmp     loc_4FB33B
    out.write(Jmp.JMP);                             // 1
    out.write(DataTools.toByte(-160, true));        // 4

    // loc_4FB3DB: mov case 3
    out.write(MovRegInt.EAX);                       // 1
    out.write(DataTools.toByte(descIds[StellarType.NeutronStar], true)); // 4
    // jmp     loc_4FB33B
    out.write(Jmp.JMP);                             // 1
    out.write(DataTools.toByte(-170, true));        // 4

    // loc_4FB3E5: mov case 10
    out.write(MovRegInt.EAX);                       // 1
    out.write(DataTools.toByte(descIds[StellarType.WormHole], true)); // 4
    // jmp     loc_4FB33B
    out.write(Jmp.JMP);                             // 1
    out.write(DataTools.toByte(-180, true));        // 4

    // loc_4FB3EF: mov case 11
    out.write(MovRegInt.EAX);                       // 1
    out.write(DataTools.toByte(descIds[StellarType.RadioPulsar], true)); // 4
    // jmp     loc_4FB33B
    out.write(Jmp.JMP);                             // 1
    out.write(DataTools.toByte(-190, true));        // 4
  }
}
