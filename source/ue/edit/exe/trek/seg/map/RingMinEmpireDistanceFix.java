package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.DataSegment;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.MovDSInt;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.SegmentArea;
import ue.edit.exe.trek.sd.SD_MinEmpDistFix;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.value.IntValue;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * by default small and large ring galaxy min empire distance is shared
 * with the galaxy sd_ringWidth 0xae58f and 0xae705
 * this fixture separates these values
 *
 * in exchange
 * the map width from 0xae5b1+6 reuses address 0xae54e+6
 * and map width from 0xae727+6 reuses address 0xae6e0+6
 */
public class RingMinEmpireDistanceFix extends SegmentArea implements Patch, IntValue {

  public static final int SIZE = 15;

  public static final int DEFAULT_SmallWidth = 13;
  public static final int DEFAULT_LargeWidth = 25;
  public static final int DEFAULT_SmallMinEmpireDistance = 4;
  public static final int DEFAULT_LargeMinEmpireDistance = 6;

  // patch detection
  private static final byte orig_op = (byte) 0xC7; // mov mem

  // relative offset jumps
  private static final int orig_smallGalaxyJmp    = -267; // 0xFFFFFEF5
  private static final int orig_largeGalaxyJmp    = -641; // 0xFFFFFD7F
  private static final int patched_smallGalaxyJmp = -114; // 0xFFFFFF8E
  private static final int patched_largeGalaxyJmp = -86;  // 0xFFFFFFAA

  private MovDSInt orig_mapWidth;
  private MovRegInt patched_minEmpDist;
  private DataSegment patched_nops  = new DataSegment(address + 5, SD_MinEmpDistFix.PatchedNops);
  private Jmp         jmpSegment    = new Jmp(address + 10, SD_MinEmpDistFix.Jmp);

  @Getter private boolean patched = false;
  @Getter private boolean isLargeGalaxy = false;

  public RingMinEmpireDistanceFix(SegmentDefinition def) {
    super(def);
  }

  @Override
  public boolean isFloatingPoint() {
    return false;
  }

  @Override
  public void load(InputStream in) throws IOException {
    // load data ahead to check on the patch operation
    byte[] data = StreamTools.readBytes(in, SIZE);
    patched = data[0] != orig_op;
    isLargeGalaxy = false;

    // first determine galaxy size
    jmpSegment.load(data, 10);
    if (!jmpSegment.check(patched ? patched_smallGalaxyJmp : orig_smallGalaxyJmp)) {
      jmpSegment.validate(patched ? patched_largeGalaxyJmp : orig_largeGalaxyJmp);
      isLargeGalaxy = true;
    }

    // init sub segments
    patched_minEmpDist = new MovRegInt(address, isLargeGalaxy
      ? SD_MinEmpDistFix.PatchedLargeRingGal
      : SD_MinEmpDistFix.PatchedSmallRingGal);
    orig_mapWidth = new MovDSInt(address, isLargeGalaxy
      ? SD_MinEmpDistFix.OrigLargeMapWidth
      : SD_MinEmpDistFix.OrigSmallMapWidth);

    if (patched) {
      // read ring galaxy minimum empire distance
      patched_minEmpDist.load(data);
      // validate patched
      patched_nops.validate(data, 5);

    } else {
      // read galaxy map width
      orig_mapWidth.load(data);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (patched) {
      // write ring galaxy min empire distance
      patched_minEmpDist.save(out); // 5
      patched_nops.save(out);       // 5
    } else {
      // write map width
      orig_mapWidth.save(out);      // 10
    }

    jmpSegment.save(out);           // 5
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    // write map width
    orig_mapWidth.saveDefault(out);             // 10
    out.write(Jmp.JMP);                         // 1
    int offset = isLargeGalaxy ? orig_largeGalaxyJmp : orig_smallGalaxyJmp;
    out.write(DataTools.toByte(offset,  true)); // 4
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    return !patched && orig_mapWidth.check(isLargeGalaxy ? DEFAULT_LargeWidth : DEFAULT_SmallWidth);
  }

  @Override
  public void patch() {
    if (!patched) {
      jmpSegment.forceValue(isLargeGalaxy ? patched_largeGalaxyJmp : patched_smallGalaxyJmp);
      patched = true;

      // mark changed for the operation change
      patched_minEmpDist.markChanged();
      patched_nops.markChanged();
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (patched) {
      jmpSegment.forceValue(isLargeGalaxy ? orig_largeGalaxyJmp : orig_smallGalaxyJmp);
      patched = false;

      // mark changed for the operation change
      orig_mapWidth.markChanged();
      markChanged();
    }
  }

  public int getMinEmpireDistance() {
    if (!patched) {
      throw new UnsupportedOperationException(
        "The RingMinEmpireDistanceFix patch must be applied to read the min empire distance.");
    }
    return patched_minEmpDist.intValue();
  }

  public int getMapWidth() {
    if (patched) {
      throw new UnsupportedOperationException(
        "The RingMinEmpireDistanceFix patch must be unapplied to read the map width.");
    }
    return orig_mapWidth.intValue();
  }

  public boolean setMinEmpireDistance(int value) throws InvalidArgumentsException {
    if (!patched) {
      throw new UnsupportedOperationException(
        "The RingMinEmpireDistanceFix patch must be applied to set the min empire distance.");
    }

    if (patched_minEmpDist.setValue(value)) {
      markChanged();
      return true;
    }
    return false;
  }

  public boolean setMapWidth(int value) throws InvalidArgumentsException {
    if (patched) {
      throw new UnsupportedOperationException(
        "The RingMinEmpireDistanceFix patch must be unapplied to set the map width.");
    }

    if (orig_mapWidth.setValue(value)) {
      markChanged();
      return true;
    }
    return false;
  }

  @Override
  public void clear() {
    orig_mapWidth.clear();
    patched_minEmpDist.clear();
    jmpSegment.clear();
    patched = false;
    isLargeGalaxy = false;
  }

  @Override
  public void check(Vector<String> response) {
    // is already validated on load
  }

  @Override
  public int intValue() {
    return patched ? patched_minEmpDist.intValue() : orig_mapWidth.intValue();
  }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    if (patched)
      return setMinEmpireDistance(value);
    else
      return setMapWidth(value);
  }

  @Override
  public void reset() {
    unpatch();
    if (!orig_mapWidth.isDefault()) {
      orig_mapWidth.reset();
      markChanged();
    }
  }

  @Override
  public String toString() {
    return Integer.toString(intValue());
  }

  @Override
  public List<InternalSegment> listSubSegments() {
    return patched
      ? Arrays.asList(patched_minEmpDist, patched_nops, jmpSegment)
      : Arrays.asList(orig_mapWidth, jmpSegment);
  }
}
