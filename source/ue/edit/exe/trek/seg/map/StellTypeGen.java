package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.BitSet;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpIf;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.MulRegByte;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.seg.sub.Sub_4B0410;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * The stellar type generation routine sub_4B0410 first checks the star anomaly ratio
 * asm_57F738 / hex_17d538 for whether to generate a new system or an anomaly.
 * Then it keeps generating ids, till a matching type is found.
 *
 * The original code checks exact 6 hard-coded system type ids and 6 anomaly ones.
 * When patched, it is replaced by one merged bitset, to support max 128 stellar types.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57594#p57594
 */
public class StellTypeGen extends InternalSegment implements Patch {

  public static final String NAME = "stellType_gen";
  public static final int SEGMENT_ADDRESS = 0x000AFB34; // asm_4B0734
  public static final int SIZE = 380; // 0x17C

  public static final int MaxStellarTypes = 128;
  public static final int DefaultNumStellarTypes = 12;
  public static final int DefaultStellarEntrySize = 12;
  public static final int FaultyGALMEntrySize = 15;

  private static final String DESC = "This patch extends the stellar type galaxy generation"
    + " to max 128 stellar types. Further it sneaks in to replace the faulty GALM code patch."
    + "<br>See https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57594#p57594"
    + "and https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57607#p57607";

  public static final SegmentDefinition SEGMENT_DEFINITION =
    new SegmentDefinition(SEGMENT_ADDRESS, NAME, StellTypeGen.class)
      .desc(DESC).size(SIZE);

  // system id mask used for dicing stellar objects by the galaxy generation
  private static final long[] DefaultSysWords = new long[] { 0x03F0 };
  private static final BitSet DefaultSysMask = BitSet.valueOf(DefaultSysWords);
  private static final byte[] DefaultAnomalyIds = new byte[] { 0, 2, 1, 0xB, 3, 0xA };

  @Getter private boolean isExtended = false;
  @Getter private boolean isGALM = false;

  // for vanilla code we have 12 offsets, but just the values 0-3 plus 10 and 11 can bet set
  // when extended, all descriptions can be set independently
  private BitSet sysMask = new BitSet(MaxStellarTypes);

  public StellTypeGen(SegmentDefinition sd) {
    super(sd);
  }

  @Override
  public void load(InputStream in) throws IOException { // 380
    validate(in, JmpIf.JNB);                        // 2

    // check for broken GALM JNB
    // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57543#p57543
    int jnb = StreamTools.readInt(in, true);        // 4
    isGALM = jnb == 145;  // jnb 0x004B07CB (only if fixed)
    if (!isGALM)
      validate(jnb, 133); // jnb 0x004B07BF

    // edx = num stellar types read from objstruc.smt
    // eax = generate stellar type id [0 to num stellar types -1]
    validate(in, Sub_4B0410.loc_4B073A_start);      // 13

    // check extended
    byte[] data = StreamTools.readBytes(in, 6);     // 6
    isExtended = check(data, Sub_4B0410.asm_4B0747_patched);
    if (!isExtended)
      validate(data, Sub_4B0410.asm_4B0747_orig);

    validate_4B074D(in);                            // 12

    // 0x000AFB59 / asm_4B0759
    // this looks to be a stellar type entry size multiplier
    // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57607#p57607
    validate(in, MulRegByte.EAX);                   // 2
    int entrySize = in.read();                      // 1
    if (!isGALM || entrySize != FaultyGALMEntrySize)
      validate(entrySize, DefaultStellarEntrySize);

    validate(in, Sub_4B0410.loc_4B0752_end);        // 47

    // loc_4B078B
    loadSysMask(in);                                // 52
    validate_4B07BF(in);                            // 23

    // read subroutine end edited by GALM - to be removed once patched
    validate(in, Sub_4B0410.loc_4B07D6_start);      // 7

    // 0x000AFBDD / asm_4B07DD
    validate(in, MulRegByte.EAX);                   // 2
    entrySize = in.read();                          // 1
    if (!isGALM || entrySize != FaultyGALMEntrySize)
      validate(entrySize, DefaultStellarEntrySize);

    validate_4B07E0(in);                            // 146

    // 0x000AFC72 / asm_4B0872
    validate(in, MulRegByte.EAX);                   // 2
    entrySize = in.read();                          // 1
    if (!isGALM || entrySize != FaultyGALMEntrySize)
      validate(entrySize, DefaultStellarEntrySize);
    validate_4B0875(in);                            // orig: 59, GALM: 47

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    save(out, sysMask, isExtended);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    save(out, DefaultSysMask, false);
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  public boolean isSystem(int index) {
    return sysMask.get(index);
  }

  public boolean setSystem(int index, boolean isSystem) {
    if (sysMask.get(index) != isSystem) {
      sysMask.set(index, isSystem);
      markChanged();
      return true;
    }
    return false;
  }

  public void insert(int index, boolean isSystem) {
    // shift entries
    for (int i = sysMask.length() - 1; i > index; --i)
      sysMask.set(i, sysMask.get(i-1));

    sysMask.set(index, isSystem);
    markChanged();
  }

  public void remove(int index) {
    // shift entries
    int lastIdx = sysMask.length() - 1;
    for (int i = index; i < lastIdx; ++i)
      sysMask.set(i, sysMask.get(i+1));

    sysMask.clear(lastIdx);
    markChanged();
  }

  @Override
  public boolean isDefault() {
    if (isExtended)
      return false;

    for (int i = 0; i < DefaultNumStellarTypes; ++i) {
      if (sysMask.get(i) != DefaultSysMask.get(i))
        return false;
    }

    return true;
  }

  @Override
  public boolean isPatched() {
    return isExtended;
  }

  @Override
  public void patch() {
    if (!isPatched()) {
      isExtended = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (!isDefault()) {
      isExtended = false;
      markChanged();
    }
  }

  public void setExtended(boolean enable) {
    if (isExtended != enable) {
      isExtended = enable;
      markChanged();
    }
  }

  public boolean updateExtended(int numStellTypes) {
    boolean ext = checkExtended(numStellTypes);
    if (isExtended != ext) {
      isExtended = ext;
      markChanged();
      return true;
    }
    return false;
  }

  @Override
  public void reset() {
    unpatch();
  }

  @Override
  public void clear() {
    isExtended = false;
  }

  @Override
  public void check(Vector<String> response) {
  }

  private void validate_4B074D(InputStream in) throws IOException { // 12
    if (isExtended) {                               // extended: 5
      // jmp     short loc_4B078B
      validate(in, JmpShort.JMP);                   // 1
      validate(in, 0x3C);                           // 1
      validate(in, 3, NOP);                         // 3
    } else {                                        // orig: 5
      // cmp     eax, 4
      validate(in, CmpRegByte.EAX);                 // 2
      int firstSysId = in.read();                   // 1
      sysMask.set(firstSysId);

      // jnz     short loc_4B078B
      validate(in, JmpShort.JNZ);                   // 1
      validate(in, 0x39);                           // 1
    }

    validate(in, Sub_4B0410.loc_4B0752_start);      // 7
  }

  private void loadSysIds(InputStream in) throws IOException { // orig: 27, GALM: 37
    for (int i = 0; i < 5; ++i) {                   // 5*5 = 25
      // cmp     eax, <5,6,7,8,9>
      validate(in, CmpRegByte.EAX);                 // 2
      int sysId = in.read();                        // 1
      sysMask.set(sysId);

      // jz short loc_4B0752
      validate(in, JmpShort.JZ);                    // 1
      int offset = -62 - i * 5;
      validate(in, offset);                         // 1
    }

    byte[] data = StreamTools.readBytes(in, 2);     // 2
    isGALM = check(data, CmpRegByte.EAX);

    if (isGALM) {                                   // GALM: 10
      // sneak in extended mode
      isExtended = true;
      loadGALMSysIds(in);                           // 8

      // jmp short loc_4B073A
      validate(in, JmpShort.JMP);                   // 1
      validate(in, -118);                           // 1
    } else {
      // jmp short loc_4B073A
      validate(data[0], JmpShort.JMP);
      validate(data[1], (byte)-108);
    }
  }

  private void loadGALMSysIds(InputStream in) throws IOException { // 8
    // cmp     eax, 0Ch
    int sysId = in.read();                          // 1
    sysMask.set(sysId);

    // jz short loc_4B0752
    validate(in, JmpShort.JZ);                      // 1
    int offset = -62 - 5 * 5;
    validate(in, offset);                           // 1

    // cmp     eax, 0Dh
    validate(in, CmpRegByte.EAX);                   // 2
    sysId = in.read();                              // 1
    sysMask.set(sysId);

    // jz short loc_4B0752
    validate(in, JmpShort.JZ);                      // 1
    offset = -62 - 6 * 5;
    validate(in, offset);                           // 1
  }

  private void loadAnomalyIds(InputStream in) throws IOException { // 25
    // stellar type 0 is always expected to be an anomaly
    // due to the test call at asm_4B07D2
    validate(false, sysMask.get(0));

    for (int i = 0; i < 5; ++i) {                   // 5*5 = 25
      // cmp     eax, <2,1,B,3,A>
      validate(in, CmpRegByte.EAX);                 // 2
      int anomalyId = in.read();                    // 1
      // anomalies have no system flag
      validate(false, sysMask.get(anomalyId));

      // jz short loc_4B07D6, GALM loc_4B07E2
      validate(in, JmpShort.JZ);                    // 1
      int offset = (isGALM ? 45 : 43) - i * 5;
      validate(in, offset);                         // 1
    }
  }

  private void loadSysMask(InputStream in) throws IOException { // 52
    // loc_4B078B
    if (isExtended) {                               // extended: 52
      validate(in, Sub_4B0410.loc_4B078B_patched);  // 36

      // load system mask
      byte[] data = StreamTools.readBytes(in, 16);  // 16
      for (int i = 0; i < 16; ++i) {
        for (int j = 0; j < 8; ++j) {
          int idx = i*8 + j;
          int flag = (data[i] << j) & 0x80;
          sysMask.set(idx, flag != 0);
        }
      }
    } else {                                        // orig: 52, GALM: 64
      // read system ids
      loadSysIds(in);                               // orig: 27, GALM: 37

      // read anomaly ids
      loadAnomalyIds(in);                           // 25

      if (isGALM)
        validate(in, 2, NOP);                       // 2
    }
  }

  private void validate_4B07BF(InputStream in) throws IOException { // 23
    validate(in, Sub_4B0410.loc_4B07BF_start);      // 8

    if (isGALM) {             // asm_4B07D3         // GALM: 15
      // call    sub_4B68D0
      validate(in, Jmp.CALL);                       // 1
      validateInt(in, 0x60F8);                      // 4

      validate(in, Sub_4B0410.asm_4B07CC_orig);     // 8

      // jmp short loc_4B081E
      validate(in, JmpShort.JNZ);                   // 1
      validate(in, 0x3C);                           // 1
    } else if (isExtended) {  // asm_4B07C7         // extended: 15
      // call    sub_4B68D0
      validate(in, Jmp.CALL);                       // 1
      validateInt(in, 0x6104);                      // 4

      validate(in, Sub_4B0410.asm_4B07CC_patched);  // 8

      // jmp short loc_4B078B
      validate(in, JmpShort.JMP);                   // 1
      validate(in, 0xB5);                           // 1
    } else {                  // asm_4B07C7         // orig: 15
      // call    sub_4B68D0
      validate(in, Jmp.CALL);                       // 1
      validateInt(in, 0x6104);                      // 4

      validate(in, Sub_4B0410.asm_4B07CC_orig);     // 8

      // jmp short loc_4B07A6
      validate(in, JmpShort.JNZ);                   // 1
      validate(in, 0xD0);                           // 1
    }
  }

  // GALM: asm_4B07EC
  private void validate_4B07E0(InputStream in) throws IOException { // 146
    validate(in, Sub_4B0410.asm_4B07E0_orig);       // 48

    if (isGALM) { // asm_4B081C                     // GALM: 98

      // jmp short loc_4B083D
      validate(in, JmpShort.JMP);                   // 1
      validate(in, 0x1F);                           // 1

      // cmp     eax, 0Ch
      validate(in, CmpRegByte.EAX);                 // 2
      int anomalyId = in.read();                    // 1

      // anomalies have no system flag
      // broken on GALM for black holes (anomaly id 12 / 0Ch):
      // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57543#p57543
      if (anomalyId == 12)
        sysMask.clear(anomalyId);
      else
        validate(false, sysMask.get(anomalyId));

      // jz short loc_4B07E2
      validate(in, JmpShort.JZ);                    // 1
      validate(in, 0xBF);                           // 1

      // jmp short loc_4B07B0
      validate(in, JmpShort.JMP);                   // 1
      validate(in, 0x8B);                           // 1

      validate(in, NOP);                            // 1
      validate(in, Sub_4B0410.loc_4B081A_orig);     // 18 (loc_4B0826)

      // call    sub_4B0390
      validate(in, Jmp.CALL);                       // 1
      validateInt(in, -1197);                       // 4

      validate(in, Sub_4B0410.loc_4B0831_inc);      // 5

      // call    loc_4B0702
      validate(in, Jmp.JMP);                        // 1
      validateInt(in, -325);                        // 4

      validate(in, Sub_4B0410.loc_4B083B_orig);     // 24
      validate(in, Sub_4B0410.asm_4B085F_galm);     // 23
      validate(in, Sub_4B0410.asm_4B086A_orig);     // 8

    } else { // asm_4B0810                          // orig: 98

      validate(in, Sub_4B0410.asm_4B0810_orig);     // 10
      validate(in, Sub_4B0410.loc_4B081A_orig);     // 18

      // call    sub_4B0390
      validate(in, Jmp.CALL);                       // 1
      validateInt(in, -1185);                       // 4

      validate(in, Sub_4B0410.loc_4B0831_inc);      // 5

      // call    loc_4B0702
      validate(in, Jmp.JMP);                        // 1
      validateInt(in, -313);                        // 4

      validate(in, Sub_4B0410.loc_4B083B_orig);     // 24
      validate(in, Sub_4B0410.asm_4B0853_orig);     // 23
      validate(in, Sub_4B0410.asm_4B086A_orig);     // 8
    }
  }

  private void validate_4B0875(InputStream in) throws IOException { // orig: 59, GALM: 47
    if (isGALM) { // asm_4B07E2                     // 50
      validate(in, Sub_4B0410.asm_4B0875_orig);     // 40

      // call    sub_4B6210
      validate(in, Jmp.CALL);                       // 1
      validateInt(in, 0x5962);                      // 4

      // jmp short loc_4B084D
      validate(in, JmpShort.JMP);                   // 1
      validate(in, 0x9D);                           // 1

    } else { // asm_4B0810                          // 62
      validate(in, Sub_4B0410.asm_4B0875_orig);     // 40

      // call    sub_4B6210
      validate(in, Jmp.CALL);                       // 1
      validateInt(in, 0x596E);                      // 4

      // jmp short loc_4B0841
      validate(in, JmpShort.JMP);                   // 1
      validate(in, 0x9D);                           // 1

      // align 10h
      validate(in, 12, 0);                          // 12
    }
  }

  private boolean checkExtended(int numStellTypes) {
    // vanilla code only allows for max 12 stellar types
    if (numStellTypes > DefaultNumStellarTypes)
      return true;
    // furthermore the first anomaly type must be zero by the test call at asm_4B07D2
    if (sysMask.get(0))
      return true;
    // finally there can be max 6 system and max 6 anomaly types
    int sysNum = sysMask.cardinality();
    return sysNum > 6 || numStellTypes - sysNum > 6;
  }

  private void save(OutputStream out, BitSet sysMask, boolean extended) throws IOException { // 380
    // jnb 0x004B07BF
    out.write(JmpIf.JNB);                           // 2
    out.write(DataTools.toByte(133, true));         // 4

    // edx = num stellar types read from objstruc.smt
    // eax = generate stellar type id [0 to num stellar types -1]
    out.write(Sub_4B0410.loc_4B073A_start);         // 13

    // check extended
    out.write(isExtended ? Sub_4B0410.asm_4B0747_patched : Sub_4B0410.asm_4B0747_orig); // 6

    save_4B074D(out);                               // 12

    // 0x000AFB59 / asm_4B0759
    // sneak in to revert faulty GALM stellar type entry size patch
    // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57607#p57607
    out.write(MulRegByte.EAX);                      // 2
    out.write(DefaultStellarEntrySize);             // 1
    out.write(Sub_4B0410.loc_4B0752_end);           // 47

    // loc_4B078B
    saveSysMask(out);                               // 52
    save_4B07BF(out);                               // 23

    // re-write subroutine end edited by GALM - to be removed once patched
    out.write(Sub_4B0410.loc_4B07D6_start);         // 7

    // 0x000AFBDD / asm_4B07DD
    // sneak in to revert faulty GALM stellar type entry size patch
    out.write(MulRegByte.EAX);                      // 2
    out.write(DefaultStellarEntrySize);             // 1
    save_4B07E0(out);                               // 146

    // 0x000AFC72 / asm_4B0872
    // sneak in to revert faulty GALM stellar type entry size patch
    out.write(MulRegByte.EAX);                      // 2
    out.write(DefaultStellarEntrySize);             // 1
    save_4B0875(out);                               // 59
  }


  private void save_4B074D(OutputStream out) throws IOException { // 12
    if (isExtended) {                               // extended: 5
      // jmp     short loc_4B078B
      out.write(JmpShort.JMP);                      // 1
      out.write(0x3C);                              // 1
      StreamTools.write(out, NOP, 3);               // 3
    } else {                                        // orig: 5
      // cmp     eax, 4
      out.write(CmpRegByte.EAX);                    // 2
      int firstSysId = sysMask.nextSetBit(0);
      out.write(firstSysId);                        // 1

      // jnz     short loc_4B078B
      out.write(JmpShort.JNZ);                      // 1
      out.write(0x39);                              // 1
    }

    out.write(Sub_4B0410.loc_4B0752_start);         // 7
  }

  private void saveSysIds(OutputStream out, BitSet sysMask) throws IOException { // 27
    // skip first system id written above
    int sysId = sysMask.nextSetBit(0);

    for (int i = 0; i < 5; ++i) {                   // 5*5 = 25
      // find next system id
      sysId = sysMask.nextSetBit(++sysId);

      // cmp     eax, <5,6,7,8,9>
      out.write(CmpRegByte.EAX);                    // 2
      out.write(sysId);                             // 1

      // jz short loc_4B0752
      out.write(JmpShort.JZ);                       // 1
      int offset = -62 - i * 5;
      out.write(offset);                            // 1
    }

    // jmp short loc_4B073A
    out.write(JmpShort.JMP);                        // 1
    out.write(-108);                                // 1
  }

  private void saveAnomalyIds(OutputStream out, BitSet sysMask) throws IOException { // 25
    // stellar type 0 is always expected to be an anomaly
    // due to the test call at asm_4B07D2
    validate(false, sysMask.get(0));
    int anomalyId = 0;

    // to not sneak in any anomaly type changes,
    // take care on vanilla comparisons that are out of order
    boolean isVanilla = sysMask.equals(DefaultSysMask);

    for (int i = 0; i < 5; ++i) {                   // 5*5 = 25
      // find next anomaly id
      anomalyId = isVanilla ? DefaultAnomalyIds[i+1] : sysMask.nextClearBit(++anomalyId);

      // cmp     eax, <2,1,B,3,A>
      out.write(CmpRegByte.EAX);                    // 2
      out.write(anomalyId);                         // 1

      // jz short loc_4B07D6, GALM loc_4B07E2
      out.write(JmpShort.JZ);                       // 1
      int offset = (isGALM ? 45 : 43) - i * 5;
      out.write(offset);                            // 1
    }
  }

  private void saveSysMask(OutputStream out) throws IOException { // 52
    // loc_4B078B
    if (isExtended) {                               // extended: 52
      out.write(Sub_4B0410.loc_4B078B_patched);     // 36

      // load system mask
      byte[] data = new byte[16];
      for (int i = 0; i < 16; ++i) {
        for (int j = 0; j < 8; ++j) {
          int idx = i*8+j;
          if (sysMask.get(idx))
            data[i] |= 0x80 >>> j;
        }
      }

      out.write(data);                              // 16
    } else {                                        // orig: 52
      // read system ids
      saveSysIds(out, sysMask);                     // 27

      // read anomaly ids
      saveAnomalyIds(out, sysMask);                 // 25

      if (isGALM)
        StreamTools.write(out, NOP, 2);             // 2
    }
  }

  private void save_4B07BF(OutputStream out) throws IOException { // 23
    out.write(Sub_4B0410.loc_4B07BF_start);         // 8

    if (isExtended) { // asm_4B07C7                 // extended: 15
      // call    sub_4B68D0
      out.write(Jmp.CALL);                          // 1
      out.write(DataTools.toByte(0x6104, true));    // 4

      out.write(Sub_4B0410.asm_4B07CC_patched);     // 8

      // jmp short loc_4B078B
      out.write(JmpShort.JMP);                      // 1
      out.write(0xB5);                              // 1
    } else {          // asm_4B07C7                 // orig: 15
      // call    sub_4B68D0
      out.write(Jmp.CALL);                          // 1
      out.write(DataTools.toByte(0x6104, true));    // 4

      out.write(Sub_4B0410.asm_4B07CC_orig);        // 8

      // jmp short loc_4B07A6
      out.write(JmpShort.JNZ);                      // 1
      out.write(0xD0);                              // 1
    }
  }

  private void save_4B07E0(OutputStream out) throws IOException { // 146
    out.write(Sub_4B0410.asm_4B07E0_orig);          // 48

    out.write(Sub_4B0410.asm_4B0810_orig);          // 10
    out.write(Sub_4B0410.loc_4B081A_orig);          // 18

    // call    sub_4B0390
    out.write(Jmp.CALL);                            // 1
    out.write(DataTools.toByte(-1185, true));       // 4

    out.write(Sub_4B0410.loc_4B0831_inc);           // 5

    // call    loc_4B0702
    out.write(Jmp.JMP);                             // 1
    out.write(DataTools.toByte(-313, true));        // 4

    out.write(Sub_4B0410.loc_4B083B_orig);          // 24
    out.write(Sub_4B0410.asm_4B0853_orig);          // 23
    out.write(Sub_4B0410.asm_4B086A_orig);          // 8
  }

  private void save_4B0875(OutputStream out) throws IOException { // 59
    out.write(Sub_4B0410.asm_4B0875_orig);          // 40

    // call    sub_4B6210
    out.write(Jmp.CALL);                            // 1
    out.write(DataTools.toByte(0x596E, true));      // 4

    // jmp short loc_4B0841
    out.write(JmpShort.JMP);                        // 1
    out.write(0x9D);                                // 1

    // align 10h
    StreamTools.write(out, 0, 12);                  // 12
  }
}
