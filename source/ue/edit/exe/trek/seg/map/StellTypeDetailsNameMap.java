package ue.edit.exe.trek.seg.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.seg.sub.Sub_4B6454;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx.Galaxy.StellarObjects;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class contains the extended stellar details panel name mapping fixture.
 */
public class StellTypeDetailsNameMap extends InternalSegment implements Patch {

  public static final String NAME = "stellType_detailsNameMap";
  public static final int SEGMENT_ADDRESS = 0x000B5C27; // asm_4B6827
  public static final int SIZE = 139; // 0x8B

  public static final byte MaxSpaceObjects = 54;

  private static final String DESC = "Extends the stellar object details panel"
    + " name mappings to max 54 stellar types. In addition it reverts the shrinked MUM"
    + " name mappings that aren't of any further use and not supported for write.";

  public static final SegmentDefinition SEGMENT_DEFINITION =
    new SegmentDefinition(SEGMENT_ADDRESS, NAME, StellTypeDetailsNameMap.class)
      .desc(DESC).size(SIZE);

  private static final short NoLexID = -1;

  // ids used for lexicon dictionary lookup of the stellar object names
  private static final short[] DefaultLexIDs = new short[] { // 24
    StellarObjects.BlackHole,
    StellarObjects.XRayPulsar,
    StellarObjects.Nebula,
    StellarObjects.NeutronStar,
    NoLexID, // red gas giant
    NoLexID, // orange star
    NoLexID, // yellow star
    NoLexID, // white star
    NoLexID, // green star
    NoLexID, // blue star
    StellarObjects.Wormhole,
    StellarObjects.RadioPulsar,
  };

  @Getter
  private boolean isExtended = false;

  // for vanilla code we have 12 offsets, but just the values 0-3 plus 10 and 11 can bet set
  // when extended, all names can be set independently
  private int[] nameIds = new int[MaxSpaceObjects];

  public StellTypeDetailsNameMap(SegmentDefinition sd) {
    super(sd);
    Arrays.fill(nameIds, NoLexID);
  }

  /**
   * @param in  the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException { // 139
    byte[] data = StreamTools.readBytes(in, 7);       // 7

    if (check(data, Sub_4B6454.loc_4B6827_galm)) {
      isExtended = true;

      // read GALM lexicon ids - to be removed once patched
      loadGALMIds(in);                                // 71

      validate(in, 61, NOP);                          // 61
    } else if (check(data, 0, 7, Sub_4B6454.loc_4B6827_fix)) {
      isExtended = true;

      // validate code fixture
      data = StreamTools.readBytes(in, 24);           // 24
      validate(data, 0, data.length, Sub_4B6454.loc_4B6827_fix, 7);

      // load patched lexicon ids
      loadExtendedIdTable(in);                        // 108
    } else {
      // validate orig last for error code deviation
      isExtended = false;

      // validate unpatched vanilla jump switch
      validate(data, Sub_4B6454.loc_4B6827_jmp);

      loadOrigIds(in);
    }

    markSaved();
  }

  /**
   * Used to save changes.
   * @param out the OutputStream to write the file to.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    save(out, isExtended);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    save(out, false);
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  public int getLexId(int index) {
    return nameIds[index];
  }

  public boolean setLexId(int index, int lexId) {
    if (nameIds[index] != lexId) {
      nameIds[index] = lexId;
      markChanged();
      return true;
    }
    return false;
  }

  public void insert(int index, int lexId) {
    // shift entries
    for (int i = nameIds.length - 1; i > index; --i)
      nameIds[i] = nameIds[i-1];

    nameIds[index] = lexId;
    markChanged();
  }

  public void remove(int index) {
    // shift entries
    int lastIdx = nameIds.length - 1;
    for (int i = index; i < lastIdx; ++i)
      nameIds[i] = nameIds[i+1];

    nameIds[lastIdx] = NoLexID;
    markChanged();
  }

  public boolean starDefaultsChanged() {
    for (int i = 4; i < 10; ++i) {
      if (nameIds[i] != -1)
        return true;
    }
    return false;
  }

  @Override
  public boolean isDefault() {
    if (isExtended)
      return false;

    for (int i = 0; i < nameIds.length; ++i) {
      int expected = i < DefaultLexIDs.length ? DefaultLexIDs[i] : NoLexID;
      if (nameIds[i] != expected)
        return false;
    }

    return true;
  }

  @Override
  public boolean isPatched() {
    return isExtended;
  }

  @Override
  public void patch() {
    if (!isPatched()) {
      isExtended = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (!isDefault()) {
      isExtended = false;
      markChanged();
    }
  }

  public void setExtended(boolean enable) {
    if (isExtended != enable) {
      isExtended = enable;
      markChanged();
    }
  }

  @Override
  public void reset() {
    unpatch();
  }

  @Override
  public void clear() {
    isExtended = false;
  }

  @Override
  public void check(Vector<String> response) {
  }

  private void loadGALMIds(InputStream in) throws IOException { // 71
    // mov   eax, <lexId>
    validate(in, MovRegInt.EAX);                    // 1
    nameIds[StellarType.BlackHole] = StreamTools.readInt(in, true); // 4

    // call  Lexicon_Term Sub_48B2E0
    validate(in, Jmp.CALL);                         // 1
    validateInt(in, -0x2B558);                      // 4

    validate(in, Sub_4B6454.loc_4B682E_movEspA0);   // 7

    // jmp  lo_Literal_mod_anomalies loc_4B64D8
    validate(in, Jmp.JMP);                          // 1
    validateInt(in, -0x36C);                        // 4

    for (int i = 1; i < 8; ++i) {                   // 7*7 = 49
      int idx = i > 3 ? i + 6 : i;

      // mov  eax, <lexId>
      validate(in, MovRegInt.EAX);                  // 1
      nameIds[idx] = StreamTools.readInt(in, true); // 4

      // jmp  short loc_4B6833
      int loadLexOffset = -0x11 - i * 7;
      validate(in, JmpShort.JMP);                   // 1
      validate(in, loadLexOffset);                  // 1
    }
  }

  private void loadExtendedIdTable(InputStream in) throws IOException { // 108
    // load space object name ids
    for (int i = 0; i < nameIds.length; ++i)          // 54*2 = 108
      nameIds[i] = StreamTools.readShort(in, true);
  }

  private void loadOrigIds(InputStream in) throws IOException { // 132
    boolean isMUM = false;

    // read vanilla lexicon ids
    for (int i = 0; i < 6; ++i) {                     // 6*22 = 132, MUM: 22 + 5*7 = 57
      int idx = i > 3 ? i + 6 : i;

      // mov   eax, <lexId>
      validate(in, MovRegInt.EAX);                    // 1
      nameIds[idx] = StreamTools.readInt(in, true);   // 4

      // detect MUM modification
      byte jmp = (byte)in.read();                     // 1
      if (i > 0 && jmp == JmpShort.JMP) {
        int lexLoadOffset = -17 - 7*i;
        validate(in, lexLoadOffset);                  // 1
        isMUM = true;
      } else {
        // validate unmodded vanilla call
        validate(jmp, Jmp.CALL);

        // call  Lexicon_Term Sub_48B2E0
        int lexTermOffset = -0x2B558 - i * 0x16;
        validateInt(in, lexTermOffset);               // 4

        validate(in, Sub_4B6454.loc_4B682E_movEspA0); // 7

        // jmp  lo_Literal_mod_anomalies loc_4B64D8
        int finishLoadOffset = -0x36C - i * 0x16;
        validate(in, Jmp.JMP);                        // 1
        validateInt(in, finishLoadOffset);            // 4
      }
    }

    if (isMUM) {
      validate(in, 75, NOP);                          // 75
    }
  }

  private void save(OutputStream out, boolean extended) throws IOException { // 139
    if (extended) {
      // write lexicon table lookup fixture
      out.write(Sub_4B6454.loc_4B6827_fix);         // 31

      // write patched lexicon ids
      saveExtendedIdTable(out);                     // 108
    } else {
      // write jump switch instruction
      out.write(Sub_4B6454.loc_4B6827_jmp);         // 7

      // write lexicon ids
      saveOrigIds(out);                             // 132
    }
  }

  private void saveExtendedIdTable(OutputStream out) throws IOException { // 108
    // ids for lexicon name lookup
    for (int id : nameIds)                                      // 54*2 = 108
      out.write(DataTools.toByte((short)id, true));
    for (int i = 0; i < MaxSpaceObjects - nameIds.length; ++i)  // 0
      out.write(DataTools.toByte(NoLexID, true));
  }

  private void saveOrigIds(OutputStream out) throws IOException { // 132
    for (int i = 0; i < 6; ++i) {                           // 6*22 = 132
      int idx = i > 3 ? i + 6 : i;

      // mov   eax, <lexId>
      out.write(MovRegInt.EAX);                             // 1
      out.write(DataTools.toByte(nameIds[idx], true));      // 4

      // call  Lexicon_Term Sub_48B2E0
      int lexTermOffset = -0x2B558 - i * 0x16;
      out.write(Jmp.CALL);                                  // 1
      out.write(DataTools.toByte(lexTermOffset, true));     // 4

      out.write(Sub_4B6454.loc_4B682E_movEspA0);            // 7

      // jmp  lo_Literal_mod_anomalies loc_4B64D8
      int finishLoadOffset = -0x36C - i * 0x16;
      out.write(Jmp.JMP);                                   // 1
      out.write(DataTools.toByte(finishLoadOffset, true));  // 4
    }
  }
}
