package ue.edit.exe.trek.seg.tec;

import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.AddRegInt;
import ue.edit.exe.trek.sd.SD_TechLvl;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;

public class TechMapSize extends AddRegInt {

  public static final int SEGMENT_ADDRESS = 0x0004fa59;
  public static final int SIZE = AddRegInt.SIZE;
  public static final int DEFAULT_VALUE = 500;

  public static final int TECH_HEADERSIZE = 8;

  public static final String PREFIX = SD_TechLvl.Prefix;
  public static final String DESCRIPTION = "edifice.bst technology map size";

  public static final ValueDefinition<Integer> SEGMENT_DEFINITION =
    new ValueDefinition<Integer>(SEGMENT_ADDRESS, PREFIX, TechMapSize.class, Register.EAX, DEFAULT_VALUE)
      .suffix().desc(DESCRIPTION).size(SIZE);

  public TechMapSize() {
    super(SEGMENT_DEFINITION);
  }

  public TechMapSize(SegmentDefinition def) {
    super(def);
  }

  public int getTechLevels() {
    return calcLvls(value - TECH_HEADERSIZE);
  }

  public void setTechLevels(int lvls) throws InvalidArgumentsException {
    setValue(TECH_HEADERSIZE + calcTechMapSize(lvls));
  }

  private static int calcTechMapSize(int lvls) {
    return 16 + 7 /*eras*/ * (lvls * 6 + 2);
  }

  private static int calcLvls(int techMapSize) {
    return (((techMapSize - 16) / 7 /*eras*/) - 2) / 6;
  }
}
