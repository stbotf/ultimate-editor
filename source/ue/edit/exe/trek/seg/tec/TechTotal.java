package ue.edit.exe.trek.seg.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.trek.sd.SD_TechLvl;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.stream.StreamTools;

public class TechTotal extends ByteValueSegment {

  public static final int SEGMENT_ADDRESS = 0x000530a0;
  public static final int SIZE = 4;
  public static final byte DEFAULT_VALUE = 11 * 7;

  public static final String PREFIX = SD_TechLvl.Prefix;
  public static final String DESCRIPTION = "Unsigned value patch to allow for max 42 technology levels.";

  public static final ValueDefinition<Byte> SEGMENT_DEFINITION =
    new ValueDefinition<Byte>(SEGMENT_ADDRESS, PREFIX, TechTotal.class, DEFAULT_VALUE)
      .suffix().desc(DESCRIPTION).size(SIZE);

  private static final byte[] test_orig = new byte[] {
    (byte) 0x66, (byte) 0x83, (byte) 0xF9
  };
  private static final byte[] test_patch = new byte[] {
    (byte) 0x80, (byte) 0xF9
  };

  byte[] data;
  boolean patched = false;
  boolean invalid = false;

  public TechTotal() {
    super(SEGMENT_DEFINITION);
  }

  public TechTotal(SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    data = StreamTools.readBytes(in, SIZE);
    loadData();
    markSaved();
  }

  private void loadData() {
    byte[] test1 = Arrays.copyOf(data, test_orig.length);
    patched = !check(test1, test_orig);

    if (patched) {
      byte[] test2 = Arrays.copyOf(data, test_patch.length);
      if (!check(test2, test_patch)) {
        invalid = true;
        return;
      }

      value = Byte.toUnsignedInt(data[2]);
    } else {
      value = Byte.toUnsignedInt(data[3]);
    }
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (invalid) {
      out.write(data);
    } else if (patched) {
      // tech lvl <= 42 with unsigned value patch
      out.write(test_patch);
      out.write(value);
      out.write(NOP);
    } else {
      // tech lvl <= 36
      out.write(test_orig);
      out.write(value);
    }
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    if (invalid)
      throw new UnsupportedOperationException("Total tech level patch could not be recognized.");

    if (super.setValue(value)) {
      // values above 0x7F require the unsigned value patch
      patched = value > 0x7F;
      return true;
    }
    return false;
  }

  // returns the max acceptable tech levels with truncated sociology
  // max 42 tech levels, @see setTechLevels
  // if more than 36 tech levels, sociology is truncated
  public int getTechLevels() {
    // always default to the higher tech level limit
    return value == 0xFF ? 42 : (value / 6);
  }

  // the byte value limit is 255 = 0xFF
  // max with truncated sociology is: 42 tech levels ( * 6 science fields = 252 = 0xFC )
  // max with complete sociology is:  36 tech levels ( * 7 science fields = 252 = 0xFC )
  public void setTechLevels(int lvls) throws InvalidArgumentsException {
    if (lvls > 42)
      throw new InvalidArgumentsException("Max 42 tech levels supported.");

    // when tech levels exceeds 36, truncate as few as possible
    setValue(lvls > 36 ? 0xFF : lvls * 7);
  }
}
