package ue.edit.exe.trek.seg.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.trek.sd.SD_TechLvl;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.stream.StreamTools;

public class TechCostFixture extends ByteValueSegment implements Patch {

  public static final int SEGMENT_ADDRESS = 0x000534ac;
  public static final int SIZE = 16;
  public static final byte DEFAULT_VALUE = 11; // future tech 11 = 0Bh

  public static final String PREFIX = SD_TechLvl.Prefix;
  public static final String DESCRIPTION = "The TechCostFixture replaces the fixed"
    + " GetTechCost_Desc address compution by a configurable tech level value.";

  public static final ValueDefinition<Byte> SEGMENT_DEFINITION =
    new ValueDefinition<Byte>(SEGMENT_ADDRESS, PREFIX, TechCostFixture.class, DEFAULT_VALUE)
      .suffix().desc(DESCRIPTION).size(SIZE);

  private static final byte[] test_orig = new byte[] {
    (byte) 0x88, (byte) 0xC2,                                       // mov dl,al                      where al is some tech level entry size
    (byte) 0x8D, (byte) 0x04, (byte) 0x95, 0x00, 0x00, 0x00, 0x00,  // lea eax,dword ptr ds:[edx*4]   == al * 4
    (byte) 0x29, (byte) 0xD0,                                       // sub eax,edx                    == al * 3, one subtracted
    (byte) 0xC1, (byte) 0xE0, (byte) 0x02,                          // shl eax,2                      == al * 12, two times doubled
    (byte) 0x29, (byte) 0xD0                                        // sub eax,edx                    == al * 11 tech levels
  };
  private static final byte[] test_patch1 = new byte[] {
    (byte) 0x0F, (byte) 0xB6, (byte) 0xC0,                          // movzx eax,al
    (byte) 0x6B, (byte) 0xC0                                        // imul eax,eax, <TECH_LEVEL>     replaced by a one byte multiplier that can be modified
  };
  private static final byte[] test_patch2 = new byte[] {
    (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90,             // all "no operation" fillers
    (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90,
    (byte) 0x90, (byte) 0x90
  };

  byte[] data;
  boolean patched = false;
  boolean invalid = false;

  public TechCostFixture() {
    super(SEGMENT_DEFINITION);
  }

  public TechCostFixture(SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    data = StreamTools.readBytes(in, SIZE);
    loadData();
    markSaved();
  }

  private void loadData() {
    patched = !check(data, test_orig);

    if (patched) {
      byte[] test1 = Arrays.copyOf(data, test_patch1.length);
      if (!check(test1, test_patch1)) {
        invalid = true;
        return;
      }

      byte[] test2 = Arrays.copyOfRange(data, test_patch1.length + 1, test_patch1.length + 1 + test_patch2.length);
      if (!check(test2, test_patch2)) {
        invalid = true;
        return;
      }

      value = Byte.toUnsignedInt(data[5]);
    } else {
      value = DEFAULT_VALUE;
    }
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (invalid) {
      out.write(data);
    } else if (patched) {
      out.write(test_patch1);
      out.write(value);
      out.write(test_patch2);
    } else {
      out.write(test_orig);
    }
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    if (invalid)
      throw new UnsupportedOperationException("Tech level cost patch not recognized.");

    if (super.setValue(value)) {
      patched = value != DEFAULT_VALUE;
      return true;
    }
    return false;
  }

  @Override
  public boolean isPatched() {
    return patched;
  }

  @Override
  public void patch() throws IOException {
    if (!patched) {
      patched = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() throws IOException {
    if (patched) {
      patched = false;
      value = DEFAULT_VALUE;
      markChanged();
    }
  }
}
