package ue.edit.exe.trek.seg.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.trek.sd.SD_TechLvl;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class BldTechReq extends IntValueSegment implements Patch {

  public static final int SEGMENT_ADDRESS = 0x0004f52b;
  public static final int SIZE = 16;
  public static final int DEFAULT_VALUE = 68; // 11 * 6 + 2

  public static final String PREFIX = SD_TechLvl.Prefix;
  public static final String DESCRIPTION = "BldTechReq replaces the fixed edifice.bst"
    + " technology field size compution at 0x4f52b to allow set the tech levels.";

  public static final ValueDefinition<Integer> SEGMENT_DEFINITION =
    new ValueDefinition<Integer>(SEGMENT_ADDRESS, PREFIX, BldTechReq.class, DEFAULT_VALUE)
      .suffix().desc(DESCRIPTION).size(SIZE);

  private static final byte[] test_orig1 = new byte[] {
    (byte) 0x89, (byte) 0xD0,               // mov eax,edx
    (byte) 0xC1, (byte) 0xE0, (byte) 0x04,  // shl eax,4      == edx * 16
    (byte) 0x01, (byte) 0xD0                // add eax,edx    == edx * 17
  };
  private static final byte[] test_orig2 = new byte[] {
    (byte) 0x8B, 0x15, 0x44, 0x42, 0x5B, 0  // mov edx, ds:dword_5B4244
  };
  private static final byte[] test_orig3 = new byte[] {
    (byte) 0xC1, (byte) 0xE0, (byte) 0x02   // shl eax,2      == edx * 68 (for 11 tech levels)
  };
  private static final byte[] test_patch1 = new byte[] {
    (byte) 0x69, (byte) 0xC2                // imul eax,edx, <dword TECH_LEVELS * 6 + 2>
  };
  boolean patched = false;

  public BldTechReq() {
    super(SEGMENT_DEFINITION);
  }

  public BldTechReq(SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] data1 = StreamTools.readBytes(in, 7);
    byte[] data2 = StreamTools.readBytes(in, test_orig2.length);
    byte[] data3 = StreamTools.readBytes(in, 3);
    validate(data2, test_orig2);
    patched = false;

    if (check(data1, 0, test_patch1)) {
      validate(data1, 6, 1, NOP);
      validate(data3, 0, 3, NOP);
      value = DataTools.toInt(data1, 2, true);
      patched = true;
    } else {
      validate(data1, test_orig1);
      validate(data3, test_orig3);
      value = DEFAULT_VALUE;
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (patched) {
      out.write(test_patch1);
      out.write(DataTools.toByte(value, true));
      out.write(NOP);
      out.write(test_orig2);
      out.write(NOP);
      out.write(NOP);
      out.write(NOP);
    } else {
      out.write(test_orig1);
      out.write(test_orig2);
      out.write(test_orig3);
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(test_orig1);
    out.write(test_orig2);
    out.write(test_orig3);
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    if (super.setValue(value)) {
      patched = value != DEFAULT_VALUE;
      return true;
    }
    return false;
  }

  public int getTechLevels() {
    return (value - 2) / 6;
  }

  public void setTechLevels(int lvls) throws InvalidArgumentsException {
    setValue(lvls * 6 + 2);
  }

  @Override
  public boolean isPatched() {
    return patched;
  }

  @Override
  public void patch() throws IOException {
    if (!patched) {
      patched = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() throws IOException {
    if (patched) {
      patched = false;
      value = DEFAULT_VALUE;
      markChanged();
    }
  }
}
