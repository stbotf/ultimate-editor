package ue.edit.exe.trek.seg.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.trek.sd.SD_TechLvl;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class BldTechReq2 extends IntValueSegment implements Patch {

  public static final int SEGMENT_ADDRESS = 0x0004f541;
  public static final int SIZE = 11;
  public static final int DEFAULT_VALUE = 68;            // 11 tech levels * 4 + 24 = 68
  public static final int MAX_UNPATCHED_TECH_LEVELS = 28; // 28 tech levels * 4 + 24 = 124 = 0x7C *signed*

  public static final String PREFIX = SD_TechLvl.Prefix;
  public static final String DESCRIPTION = "BldTechReq2 patches the move operation at 0x4f541"
    + " to allow set dword values for the edifice.bst tech field size."
    + "\nIf unpatched, it is limited to 28 tech levels * 4 + 24 = 124 = 0x7C *signed*";

  public static final ValueDefinition<Integer> SEGMENT_DEFINITION =
    new ValueDefinition<Integer>(SEGMENT_ADDRESS, PREFIX, BldTechReq2.class, DEFAULT_VALUE)
      .suffix().desc(DESCRIPTION).size(SIZE);

  // mov to ax from ptr with byte offset
  private static final byte[] test_orig1 = new byte[] {
    // mov ax,word ptr ds:[edx+eax*2+44h],
    // where edx is the computed BldTechReq tech fields * 0x44 DEFAULT_VALUE size
    (byte) 0x66, (byte) 0x8B, (byte) 0x44, (byte) 0x42, /*(byte) 0x44 DEFAULT_VALUE size*/
  };

  // mov to ax from ptr with dword offset
  private static final byte[] test_patch1 = new byte[] {
    // mov ax,word ptr ds:[edx+eax*2 + <BUILDING_TECH_FIELD_OFFSET + 8 byte header
    //                               +  16 bytes tech field counter size>]
    (byte) 0x66, (byte) 0x8B, (byte) 0x84, (byte) 0x42
  };

  private static final byte[] test_ret = new byte[] {
    // pop ecx, pop ebx, ret
    (byte) 0x59, (byte) 0x5B, (byte) 0xC3
  };

  byte[] data;
  boolean patched = false;
  boolean invalid = false;

  public BldTechReq2() {
    super(SEGMENT_DEFINITION);
  }

  public BldTechReq2(SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    data = StreamTools.readBytes(in, 11);
    loadData();
    markSaved();
  }

  private void loadData() {
    patched = false;
    invalid = false;

    if (check(data, 0, test_patch1) && check(data, 8, test_ret)) {
      patched = true;
      value = DataTools.toInt(data, 4, true);
      return;
    } else if (check(data, 0, test_orig1) && check(data, 5, test_ret)) {
      value = data[4];
      return;
    }

    invalid = true;
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (invalid) {
      out.write(data);
    } else if (patched) {
      out.write(test_patch1);
      out.write(DataTools.toByte(value, true));
      out.write(test_ret);
    } else {
      out.write(test_orig1);
      out.write(value);
      out.write(test_ret);
      out.write(0);
      out.write(0);
      out.write(0);
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(test_orig1);
    out.write(defaultValue());
    out.write(test_ret);
    out.write(0);
    out.write(0);
    out.write(0);
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean setValue(int value) throws InvalidArgumentsException {
    if (invalid)
      throw new UnsupportedOperationException("Building tech requirement patch could not be recognized.");

    if (super.setValue(value)) {
      patched = value > 0x7F;
      return true;
    }
    return false;
  }

  public int getTechLevels() {
    return (value - 24) / 4;
  }

  public void setTechLevels(int lvls) throws InvalidArgumentsException {
    setValue(lvls * 4 + 24);
  }

  @Override
  public boolean isDefault() {
    return !patched && value == DEFAULT_VALUE;
  }

  @Override
  public boolean isPatched() {
    return patched;
  }

  @Override
  public void patch() throws IOException {
    if (!patched) {
      patched = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() throws IOException {
    if (patched) {
      patched = false;
      value = DEFAULT_VALUE;
      markChanged();
    }
  }
}
