package ue.edit.exe.trek.seg.sub;

import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpShort;

/**
 * This class contains unmodified vanilla reference code
 * for star_names__anomaly_literal_mod subroutine 4B6454.
 */
public interface Sub_4B6454 {
  static final byte NOP = (byte)0x90;

  // offset switch jmp
  static final byte[] loc_4B6827_jmp = new byte[] {         // 7
    (byte)0xFF, 0x24, (byte)0x85, // jmp
    0x24, 0x64, 0x4B, 0           // offset
  };

  static final byte[] loc_4B6827_galm = new byte[] {         // 7
    (byte)0xFF, 0x24, (byte)0x85, // jmp
    0x1C, 0x64, 0x4B, 0           // offset
  };

  static final byte[] loc_4B682E_movEspA0 = new byte[] {    // 7
    (byte)0x89, (byte)0x84, 0x24, (byte)0xA0, 0, 0, 0,      // mov   [esp+0A0h], eax
  };

  static final byte[] loc_4B6827_fix = new byte[] {         // 31
    0x66, (byte)0x8B, 4, 0x45, 0x46, 0x68, 0x4B, 0,         // mov ax,word ptr ds:[eax*2+4B6846]
    CmpRegByte.EAX[0], CmpRegByte.EAX[1], 0,                // cmp eax,0
    JmpShort.JB, 0xC,                                       // jb trek.4B6840
    Jmp.CALL, (byte)0xA7, 0x4A, (byte)0xFD, (byte)0xFF,     // call trek.48B2E0
    (byte)0x89, (byte)0x84, 0x24, (byte)0xA0, 0, 0, 0,      // mov dword ptr ss:[esp+A0],eax
    Jmp.JMP, (byte)0x93, (byte)0xFC, (byte)0xFF, (byte)0xFF,// jmp trek.4B64D8
    0                                                       // align 2h
  };
}
