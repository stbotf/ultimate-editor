package ue.edit.exe.trek.seg.sub;

import ue.edit.exe.common.OpCode;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.PopReg;
import ue.edit.exe.seg.prim.PushReg;

/**
 * This class contains unmodified vanilla reference code
 * for Lexicon_StarType subroutine 43D800.
 */
public interface Sub_43D800 {
  static final byte NOP = (byte)0x90;

  // preceded by last stellar type id (0x3C, 0x0B / cmp al, 0Bh)
  static final byte[] start = new byte[] {                  // 4
    JmpShort.JBE, 0x22,                                     // jbe     short loc_43D826
    PushReg.EDX,                                            // push    edx
    PushReg.EBX                                             // push    ebx
  };

  static final byte[] loc_43D806_beg = new byte[] {         // 15
    MovRegInt.EBX, (byte)0xF0, (byte)0x86, 0x57, 0,         // mov     ebx, offset aUnknownStartyp
    MovRegInt.EDX, (byte)0xAA, 0, 0, 0,                     // mov     edx, 0AAh
    MovRegInt.EAX, 4, (byte)0x87, 0x57, 0,                  // mov     eax, offset a____SourceG_54
  };

  static final byte[] asm_43D815_call = new byte[] {        // 5
    Jmp.CALL, (byte)0x96, (byte)0xA9, 0x0C, 0,              // call    sub_5081B0
  };

  static final byte[] asm_43D819_galm = new byte[] {        // 5
    Jmp.CALL, (byte)0x92, (byte)0xA9, 0x0C, 0,              // call    sub_5081B0
  };

  static final byte[] loc_43D806_end = new byte[] {         // 12
    (byte)0x85, (byte)0xC0,                                 // test    eax, eax
    JmpShort.JNZ, (byte)0xE8,                               // jnz     short loc_43D806
    MovRegInt.EAX, 0x20, (byte)0x87, 0x57, 0,               // mov     eax, offset aDonTKnowWhatTh
    PopReg.EBX,                                             // pop     ebx
    PopReg.EDX,                                             // pop     edx
    OpCode.RETN                                             // retn
  };

  static final byte[] loc_43D826_and = new byte[] {         // 5
    0x25, (byte)0xFF, 0, 0, 0,                              // and     eax, 0FFh
  };

  // offset switch jmp
  static final byte[] asm_43D82B_jmp = new byte[] {         // 7
    (byte)0xFF, 0x24, (byte)0x85,                           // jmp
    (byte)0xD0, (byte)0xD7, 0x43, 0                         // offset
  };

  static final byte[] asm_43D82B_fix = new byte[] {         // 18
    0x66, (byte)0x8B, 0x04, 0x45,                           // mov
    0x3E, (byte)0xD8, 0x43, 0,                              // offset
    CmpRegByte.EAX[0], CmpRegByte.EAX[1], 0,                // cmp     eax, 0
    JmpShort.JB, (byte)0xCC,                                // jb      trek.43D804
    (byte)0xE9, (byte)0xA3, (byte)0xDA, 4, 0                // jmp     Lexicon_Term trek.48B2E0                         |
  };

  static final byte[] asm_43D82F_galm = new byte[] {        // 7
    (byte)0xFF, 0x24, (byte)0x85,                           // jmp
    (byte)0xCC, (byte)0xD7, 0x43, 0                         // offset
  };
}
