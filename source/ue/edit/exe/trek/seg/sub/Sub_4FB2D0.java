package ue.edit.exe.trek.seg.sub;

import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.AddReg;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpIf;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.PopReg;
import ue.edit.exe.seg.prim.PushReg;
import ue.edit.exe.seg.prim.SubRegByte;

/**
 * This class contains unmodified vanilla reference code for subroutine 4FB2D0.
 */
public interface Sub_4FB2D0 {
  static final byte NOP = (byte)0x90;

  static final byte[] start = new byte[] {                                              // 31
    PushReg.ECX, PushReg.ESI, SubRegByte.ESP[0], SubRegByte.ESP[1], 0x10,               // 5
    (byte)0x89, (byte)0xC6, 0x66, (byte)0x89, 0x50, 0x18, (byte)0x8B, 0x40, 0x16,       // 9
    (byte)0xC1, (byte)0xF8, 0x10, CmpRegByte.EAX[0], CmpRegByte.EAX[1], (byte)0xFF,     // 6
    JmpIf.JZ[0], JmpIf.JZ[1], (byte)0xB2, 0, 0, 0, (byte)0x8B, 0x4E, 0x1C,              // 9
    (byte)0x85, (byte)0xC9                                                              // 2
  };

  static final byte[] jnz_4FB2EF = new byte[] {                                         // 6
    JmpIf.JNZ[0], JmpIf.JNZ[1], (byte)0xAD, 0, 0, 0
  };

  static final byte[] patched_4FB2EF = new byte[] {                                     // 6
    JmpShort.JNZ, 0x48, NOP, NOP, NOP, NOP
  };

  static final byte[] loc_4FB2F5 = new byte[] {                                         // 53
    PushReg.EBX, 0x0F, (byte)0xBF, (byte)0xCA, (byte)0x8D, 4, OpCode.LEA, 0, 0, 0, 0,   // 11
    0x29, (byte)0xC8, (byte)0xC1, (byte)0xE0, 2, 0x29, (byte)0xC8,                      // 7
    (byte)0x8B, 0x0D, (byte)0xCC, 0x36, 0x5A, 0, (byte)0xC1, (byte)0xE0, 3,             // 9
    1, (byte)0xC1, OpCode.LEA, 0x41, 0x2C, PushReg.EAX, 0x68, 0x40, 0x26, 0x58, 0,      // 11
    OpCode.LEA, 0x44, 0x24, 0x0C, PushReg.EAX, Jmp.CALL, 0x74, (byte)0x8A, 1, 0,        // 10
    (byte)0x8B, 1, (byte)0x83, (byte)0xC4, 0x0C                                         // 5
  };

  // offset switch jmp at asm_4FB32F (loc_4FB2F5+0x3A)
  static final byte[] loc_4FB2F5_jmp = new byte[] {                                     // 7
    (byte)0xFF, 0x24, (byte)0x85,   // jmp
    (byte)0xA0, (byte)0xB2, 0x4F, 0 // offset
  };

  static final byte[] loc_4FB2F5_galm = new byte[] {                                    // 7
    (byte)0xFF, 0x24, (byte)0x85,   // jmp
    (byte)0x90, (byte)0xB2, 0x4F, 0 // offset
  };

  // loc_4FB33B
  static final byte[] loc_4FB33B = new byte[] {                                         // 17
    // call sub_48B2E0
    Jmp.CALL, (byte)0xA0, (byte)0xFF, (byte)0xF8, (byte)0xFF,                           // 5
    (byte)0x8B, 0x5E, 0x14, (byte)0x89, (byte)0xC2, (byte)0x89, (byte)0xD8,             // 7
    // call sub_541B60
    Jmp.CALL, 0x14, 0x68, 4, 0                                                          // 5
  };

  static final byte[] moved_4FB33B = new byte[] {                                       // 17
    // call sub_48B2E0
    Jmp.CALL, 0x15, (byte)0xFF, (byte)0xF8, (byte)0xFF,                                 // 5
    (byte)0x8B, 0x5E, 0x14, (byte)0x89, (byte)0xC2, (byte)0x89, (byte)0xD8,             // 7
    // call sub_541B60
    Jmp.CALL, (byte)0x89, 0x67, 4, 0                                                    // 5
  };

  static final byte[] loc_4FB34C_start = new byte[] {                                   // 46
    OpCode.LEA, 0x54, 0x24, 4, (byte)0x8B, 0x46, 8, Jmp.CALL, 0x48, 0x28, 3, 0,         // 12
    (byte)0x8B, 0x46, 8, Jmp.CALL, (byte)0xF0, 0x28, 3, 0, Jmp.CALL, 0x3B, 0x1E, 4, 0,  // 13
    Jmp.CALL, 0x66, 0x51, 4, 0, OpCode.LEA, 0x51, 4, (byte)0x8B, 0x46, 0x0C,            // 11
    Jmp.CALL, (byte)0xEB, 0x67, 4, 0, (byte)0x83, 0x39, 2, 0x74, 0x19,                  // 10
  };

  static final byte[] loc_4FB34C_end = new byte[] {                                     // 20
    MovRegInt.EDX, 1, 0, 0, 0, MovRegInt.EAX, 0xF, 0, 0, 0, (byte)0x89, (byte)0xF1, // 12
    Jmp.CALL, 0x30, (byte)0x99, (byte)0xF9, (byte)0xFF, (byte)0x89, 0x46, 0x1C          // 8
  };

  static final byte[] loc_4FB393 = new byte[] {                                         // 9
    (byte)0x8B, 0x46, 4, Jmp.CALL, (byte)0x95, 0x22, 4, 0, PopReg.EBX
  };

  static final byte[] retn_4FB39C = new byte[] {                                         // 6
    AddReg.OPCODE, AddReg.ESP, 0x10, PopReg.ESI, PopReg.ECX, OpCode.RETN
  };

  static final byte[] loc_4FB3A2 = new byte[] {                                         // 19
    (byte)0x89, (byte)0xC8, Jmp.CALL, (byte)0xC7, (byte)0x98, (byte)0xF9, (byte)0xFF,
    (byte)0xC7, 0x46, 0x1C, 0, 0, 0, 0, Jmp.JMP, 0x40, (byte)0xFF, (byte)0xFF, (byte)0xFF
  };

  static final byte[] moved_4FB3A2 = new byte[] {                                       // 19
    (byte)0x89, (byte)0xC8, Jmp.CALL, 0x30, (byte)0x99, (byte)0xF9, (byte)0xFF,
    (byte)0xC7, 0x46, 0x1C, 0, 0, 0, 0, JmpShort.JMP, (byte)0xAC, NOP, NOP, NOP
  };

  static final byte[] nebula_4FB3BF = new byte[] {                                      // 18
    0x68, (byte)0xC4, 0x27, 0x58, 0, OpCode.LEA, 0x44, 0x24, 0x08,                      // 9
    PushReg.EAX, Jmp.CALL, (byte)0xCB, (byte)0x89, 0x01, 0x00,                          // 6
    AddReg.OPCODE, AddReg.ESP, 0x08                                                     // 3
  };

  static final byte[] moved_nebula_4FB3BF = new byte[] {                                // 18
    0x68, (byte)0xC4, 0x27, 0x58, 0, OpCode.LEA, 0x44, 0x24, 0x08,                      // 9
    PushReg.EAX, Jmp.CALL, (byte)0xE8, (byte)0x89, 0x01, 0x00,                          // 6
    AddReg.OPCODE, AddReg.ESP, 0x08                                                     // 3
  };

  static final byte[] patched_lex_lookup_4FB3B9 = new byte[] {                          // 8
    0x66, (byte)0x8B, 4, 0x45, (byte)0x94, (byte)0xB2, 0x4F, 0,
  };
}
