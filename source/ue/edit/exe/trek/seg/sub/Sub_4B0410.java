package ue.edit.exe.trek.seg.sub;

import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.OpCode.MovByteReg;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpShort;

/**
 * This class contains unmodified vanilla reference code
 * for Star_Anomaly_Generation subroutine 4B0410.
 */
public interface Sub_4B0410 {
  static final byte NOP = (byte)0x90;

  static final byte[] loc_4B073A_start = new byte[] {         // 13
    // edx = num stellar types read from objstruc.smt
    (byte)0x8B, (byte)0x54, 0x24, 0x10,                       // mov     edx,dword ptr ss:[esp+10]
    // address of stellar type chance table
    (byte)0x8B, (byte)0x44, 0x24, 0x0C,                       // mov     eax,dword ptr ss:[esp+C]
    // eax = generate stellar type id [0 to num stellar types -1]
    Jmp.CALL, (byte)0x89, 0x61, 0, 0                          // call trek.4B68D0
  };

  static final byte[] asm_4B0747_orig = new byte[] {          // 6
    // unused mov edx
    (byte)0x89, (byte)0xC2,                                   // mov     edx, eax
    // store generated stellar type id
    (byte)0x89, 0x44, 0x24, 0x14                              // mov     dword ptr ss:[esp+14], eax
  };

  static final byte[] asm_4B0747_patched = new byte[] {       // 6
    // store generated stellar type id
    (byte)0x89, 0x44, 0x24, 0x14,                             // mov     dword ptr ss:[esp+14], eax
    // zero edx to remember system check
    0x33, (byte)0xD2,                                         // xor     edx, edx
  };

  static final byte[] loc_4B0752_start = new byte[] {         // 7
    (byte)0x8B, 0x44, 0x24, 0x56,                             // mov     eax, dword ptr ss:[esp+56]
    (byte)0xC1, (byte)0xF8, 0x10                              // sar     eax, 10h
  };

  static final byte[] loc_4B0752_end = new byte[] {           // 47
    (byte)0x8B, 0x5C, 0x24, 4,                                // mov     ebx, dword ptr ss:[esp+4]
    (byte)0x8B, 0x54, 0x24, 0x3E,                             // mov     edx, dword ptr ss:[esp+3E]
    (byte)0x8B, 0x74, 0x24, 0x58,                             // mov     esi, dword ptr ss:[esp+58]
    1, (byte)0xC3,                                            // add     ebx, eax
    (byte)0x8A, 0x44, 0x24, 0x14,                             // mov     al, byte ptr ss:[esp+14]
    (byte)0xC1, (byte)0xFA, 0x10,                             // sar     edx, 10h
    (byte)0x88, 0x43, 8,                                      // mov     byte ptr ds:[ebx+8],al
    (byte)0x8B, 0x44, 0x24, 0x66,                             // mov     eax, dword ptr ss:[esp+66]
    0x46,                                                     // inc     esi
    (byte)0xC1, (byte)0xF8, 0x10,                             // sar     eax, 10h
    0x66, (byte)0x89, 0x74, 0x24, 0x58,                       // mov     word ptr ss:[esp+58], si
    Jmp.CALL, 0x0A, (byte)0xFC, (byte)0xFF, (byte)0xFF,       // call    sub_4B0390
    Jmp.JMP, 0x77, (byte)0xFF, (byte)0xFF, (byte)0xFF         // jmp     loc_4B0702
  };

  static final byte[] loc_4B078B_patched = new byte[] {       // 36
    MovByteReg.BL, 8,                                         // mov     bl, 8        // load divisor
    (byte)0xF6, (byte)0xF3,                                   // div     bl           // divide by 32
    (byte)0x8A, (byte)0xCC,                                   // mov     cl, ah       // mov remainder
    0x0F, (byte)0xBE, (byte)0xC0,                             // movsx   eax, al      // expand division result
    (byte)0x8A, (byte)0x80, (byte)0xAF, 7, 0x4B, 0,           // mov     al, byte ptr ds:[eax*4+4B07AF] // load system mask
    (byte)0xD2, (byte)0xE0,                                   // shl     al, cl       // shift left by remainder
    0x24, (byte)0x80,                                         // and     al, 80h      // check for system flag
    CmpRegByte.EDX[0], CmpRegByte.EDX[1], 0,                  // cmp     edx, 0       // looking for 0 = systems, 1 = anomalies
    0x75, 6,                                                  // jne     loc_4B07
    CmpRegByte.AL, 0,                                         // cmp     al, 0
    JmpShort.JNZ, (byte)0xAB,                                 // jne     loc_4B0752   // is system
    JmpShort.JMP, (byte)0x91,                                 // jmp     loc_4B073A   // is no system
    CmpRegByte.AL, 0,                                         // cmp     al, 0
    JmpShort.JZ, 0x29,                                        // jne     loc_4B07D6   // is anomaly
    JmpShort.JMP, 0x10                                        // jmp     loc_4B07BF   // is no anomaly
  };

  static final byte[] loc_4B07BF_start = new byte[] {         // 8
    (byte)0x8B, 0x54, 0x24, 0x10,                             // mov     edx, dword ptr ss:[esp+10]
    (byte)0x8B, 0x44, 0x24, 0x0C                              // mov     eax, dword ptr ss:[esp+C]
  };

  static final byte[] asm_4B07CC_orig = new byte[] {          // 8
    (byte)0x89, (byte)0xC2,                                   // mov     edx, eax (unused)
    (byte)0x89, 0x44, 0x24, 0x14,                             // mov     dword ptr ss:[esp+14], eax
    (byte)0x85, (byte)0xC0                                    // test    eax, eax
  };

  static final byte[] asm_4B07CC_patched = new byte[] {       // 8
    (byte)0x89, 0x44, 0x24, 0x14,                             // mov     dword ptr ss:[esp+14], eax
    0x33, (byte)0xD2,                                         // xor     edx, edx
    (byte)0xB2, 1                                             // mov     dl, 1
  };

  static final byte[] loc_4B07D6_start = new byte[] {         // 7
    (byte)0x8B, 0x44, 0x24, 0x5E,                             // mov     eax, dword ptr ss:[esp+5E]
    (byte)0xC1, (byte)0xF8, 0x10                              // sar     eax, 10h
  };

  // GALM: asm_4B07EC
  static final byte[] asm_4B07E0_orig = new byte[] {          // 48
    3, 0x44, 0x24, 8,                                         // add     eax, dword ptr ss:[esp+8]
    (byte)0x8A, 0x54, 0x24, 0x14,                             // mov     dl, byte ptr ss:[esp+14]
    (byte)0x8B, 0x5C, 0x24, 0x14,                             // mov     ebx, dword ptr ss:[esp+14]
    (byte)0x88, 0x50, 8,                                      // mov     byte ptr ds:[eax+8], dl
    (byte)0x85, (byte)0xDB,                                   // test    ebx, ebx
    0x75, 0x27,                                               // jne     loc_4B081A
    (byte)0x8B, 0x54, 0x24, 0x3E,                             // mov     edx, dword ptr ss:[esp+3E]
    (byte)0xC1, (byte)0xFA, 0x10,                             // sar     edx, 10h
    (byte)0x8D, 0x14, (byte)0x92,                             // lea     edx, dword ptr ds:[edx+edx*4]
    1, (byte)0xEA,                                            // add     edx,ebp
    (byte)0x89, 0x10,                                         // mov     dword ptr ds:[eax], edx
    (byte)0x8B, 0x54, 0x24, 0x66,                             // mov     edx, dword ptr ss:[esp+66]
    (byte)0xC1, (byte)0xFA, 0x10,                             // sar     edx, 10h
    (byte)0x8D, 0x14, (byte)0x92,                             // lea     edx, dword ptr ds:[edx+edx*4]
    1, (byte)0xEA,                                            // add     edx, ebp
    (byte)0x89, 0x50, 4,                                      // mov     dword ptr ds:[eax+4], edx
  };

  static final byte[] asm_4B0810_orig = new byte[] {          // 10
    0x66, (byte)0xFF, 0x44, 0x24, 0x60,                       // inc     word ptr ss:[esp+60]
    Jmp.JMP, (byte)0xE8, (byte)0xFE, (byte)0xFF, (byte)0xFF,  // jmp     loc_4B0702
  };

  static final byte[] loc_4B081A_orig = new byte[] {          // 18
    (byte)0x8B, 0x54, 0x24, 0x3E,                             // mov     edx, dword ptr ss:[esp+3E]
    (byte)0x8B, 0x4C, 0x24, 0x66,                             // mov     ecx, dword ptr ss:[esp+66]
    (byte)0x89, (byte)0xC3,                                   // mov     ebx, eax
    (byte)0xC1, (byte)0xF9, 0x10,                             // sar     ecx, 10h
    (byte)0xC1, (byte)0xFA, 0x10,                             // sar     edx, 10h
    (byte)0x89, (byte)0xC8                                    // mov     eax, ecx
  };

  static final byte[] loc_4B0831_inc = new byte[] {           // 5
    0x66, (byte)0xFF, 0x44, 0x24, 0x60                        // inc     word ptr ss:[esp+60]
  };

  static final byte[] loc_4B083B_orig = new byte[] {          // 24
    (byte)0x8B, 0x7C, 0x24, 0x1C,                             // mov     edi, dword ptr ss:[esp+1C]
    0x31, (byte)0xF6,                                         // xor     esi, esi
    (byte)0x8B, 0x54, 0x24, 0x5E,                             // mov     edx, dword ptr ss:[esp+5E]
    0x0F, (byte)0xBF, (byte)0xC6,                             // movsx   eax, si
    (byte)0xC1, (byte)0xFA, 0x10,                             // sar     edx, 10h
    0x39, (byte)0xD0,                                         // cmp     eax, edx
    0x7C, 0x23,                                               // jl      loc_4B0872
    (byte)0x8B, 0x44, 0x24, 0x0C                              // mov     eax, dword ptr ss:[esp+C]
  };

  static final byte[] asm_4B0853_orig = new byte[] {          // 23
    Jmp.CALL, 8, 0x26, (byte)0xFE, (byte)0xFF,                // call    sub_492E60
    (byte)0x8B, 0x44, 0x24, 8,                                // mov     eax, dword ptr ss:[esp+8]
    Jmp.CALL, (byte)0xFF, 0x25, (byte)0xFE, (byte)0xFF,       // call    sub_492E60
    (byte)0x8B, 0x44, 0x24, 4,                                // mov     eax, dword ptr ss:[esp+4]
    Jmp.CALL, (byte)0xF6, 0x25, (byte)0xFE, (byte)0xFF        // call    sub_492E60
  };

  static final byte[] asm_4B085F_galm = new byte[] {          // 23
    Jmp.CALL, (byte)0xFC, 0x25, (byte)0xFE, (byte)0xFF,       // call    sub_492E60
    (byte)0x8B, 0x44, 0x24, 8,                                // mov     eax, dword ptr ss:[esp+8]
    Jmp.CALL, (byte)0xF3, 0x25, (byte)0xFE, (byte)0xFF,       // call    sub_492E60
    (byte)0x8B, 0x44, 0x24, 4,                                // mov     eax, dword ptr ss:[esp+4]
    Jmp.CALL, (byte)0xEA, 0x25, (byte)0xFE, (byte)0xFF        // call    sub_492E60
  };

  static final byte[] asm_4B086A_orig = new byte[] {          // 8
    (byte)0x83, (byte)0xC4, 0x78,                             // add     esp, 78h
    0x5D,                                                     // pop     ebp
    0x5F,                                                     // pop     edi
    0x5E,                                                     // pop     esi
    0x59,                                                     // pop     ecx
    OpCode.RETN                                               // ret
  };

  static final byte[] asm_4B0875_orig = new byte[] {          // 40
    3, 0x44, 0x24, 8,                                         // add     eax, dword ptr ss:[esp+8]
    0x66, (byte)0x8B, 0x18,                                   // mov     bx, word ptr ds:[eax]
    0x66, (byte)0x8B, 0x50, 4,                                // mov     dx, word ptr ds:[eax+4]
    (byte)0x8A, 0x40, 8,                                      // mov     al, byte ptr ds:[eax+8]
    (byte)0x8B, 0x4C, 0x24, 0x10,                             // mov     ecx, dword ptr ss:[esp+10]
    0x25, (byte)0xFF, 0, 0, 0,                                // and     eax, 0FFh
    0x0F, (byte)0xBF, (byte)0xDB,                             // movsx   ebx, bx
    0x51,                                                     // push    ecx
    0x0F, (byte)0xBF, (byte)0xD2,                             // movsx   edx, dx
    0x46,                                                     // inc     esi
    0x57,                                                     // push    edi
    (byte)0x8B, 0x4C, 0x24, 0x14,                             // mov     ecx, dword ptr ss:[esp+14]
    (byte)0x89, 0x44, 0x24, 0x1C,                             // mov     dword ptr ss:[esp+1C], eax
  };
}
