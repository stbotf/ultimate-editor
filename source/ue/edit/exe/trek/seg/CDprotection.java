package ue.edit.exe.trek.seg;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * CD Protection Jump
 * This class is used for turning cd protection off/on.
 */
public class CDprotection extends InternalSegment {

  public static final int SEGMENT_ADDRESS = 0x000005A0;
  public static final int SIZE = 5;
  public static final int DEFAULT_VALUE = 5;

  private byte[] DATA = new byte[SIZE];

  /**
   * Returns whether the cd protection code is completely removed.
   */
  @Getter private boolean isRemoved;

  public CDprotection(SegmentDefinition def) {
    super(def);
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    //read data
    StreamTools.read(in, DATA);
    isRemoved = DataTools.equals(DATA, 0);
    markSaved();
  }

  /**
   * Used to save changes.
   *
   * @param out the OutputStream to write the file to.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DATA);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    // don't care
    out.write(DATA);
  }

  @Override
  public void clear() {
    Arrays.fill(DATA, (byte)0);
    markChanged();
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void check(Vector<String> response) {
    String msg = Language.getString("CDprotection.0"); //$NON-NLS-1$
    msg = msg.replace("%1", Long.toString(address())); //$NON-NLS-1$

    if (DATA[3] != JmpShort.JZ && DATA[3] != JmpShort.JMP && !isRemoved)
      response.add(msg);
  }

  @Override
  public boolean isDefault() {
    // not relevant
    return true;
  }

  /**
   * Enables/disabled cd protection.
   */
  public void setEnabled(boolean enabled) {
    byte op = enabled ? JmpShort.JZ : JmpShort.JMP;
    if (!isRemoved && DATA[3] != op) {
      DATA[3] = op;
      markChanged();
    }
  }

  /**
   * Returns cd protection's state.
   */
  public boolean isEnabled() {
    return DATA[3] == JmpShort.JZ;
  }

  @Override
  public void reset() {
  }
}
