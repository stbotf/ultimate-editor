package ue.edit.exe.trek.seg.shp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import lombok.val;
import ue.UE;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.trek.sd.SD_ShipNum;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.stream.StreamTools;

/**
 * Limit for loading the ship models.
 * Patches for unsigned byte value if 127 is exceeded.
 */
public class ShipLoadLimit extends ByteValueSegment {

  public static final int LIMIT1_SEGMENT_ADDRESS = 0x0006EC08;
  public static final int LIMIT2_SEGMENT_ADDRESS = 0x0006EC66;
  public static final int SIZE = 5;

  public static final byte[] Default_Data1 = new byte[] {
    (byte)0x83, (byte)0xF8, 0x7D, 0x7D /*JGE*/, 0x5E
  };
  public static final byte[] Default_Data2 = new byte[] {
    (byte)0x83, (byte)0xFA, 0x7D, 0x7C /*JL*/, (byte)0xA2
  };

  private static final byte CMP_Orig = (byte)0x83;
  private static final byte CMP_Fix = (byte)0x80;
  private static final byte JNZ_Fix = JmpShort.JNZ;
  private static final byte JZ_Fix = JmpShort.JZ;

  @Getter private boolean isPatched = false;
  @Getter private boolean isDisabled = false;

  public ShipLoadLimit(SegmentDefinition def) {
    super(def);
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean forceValue(int limit) {
    if (super.forceValue(limit)) {
      // patch if needed
      isPatched = value > Integer.MAX_VALUE;
      return true;
    }
    return false;
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] data = StreamTools.readBytes(in, SIZE);
    byte[] def = defaultData();
    boolean isJge = isJge();
    boolean isNopped = data[0] == NOP;
    isPatched = data[0] == CMP_Fix || isJge && isNopped;

    if (isPatched) {
      if (isNopped) {
        validate(data, 1, 4, NOP);
        value = Integer.MAX_VALUE;
        isDisabled = true;
      } else {
        validate(data[1], def[1]);
        validate(data[3], jmpFix());
        validate(data[4], def[4]);
        value = Byte.toUnsignedInt(data[2]);
      }
    } else {
      validate(data[1], def[1]);
      validate(data[0], CMP_Orig);
      validate(data, 3 , 2, def, 3);
      value = Byte.toUnsignedInt(data[2]);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    byte[] def = defaultData();

    if (isPatched) {
      if (isDisabled) {
        // fix ShipLoadLimit1 to ShipLoadLimit2
        val sl2 = (ShipLoadLimit) UE.FILES.trek().getSegment(SD_ShipNum.ShipLoadLimit2, false);
        value = sl2.isDisabled ? Integer.min(value, 255) : sl2.value;
        isDisabled = false;
      }

      // fix to compare the low byte instead of the whole register
      out.write(CMP_Fix);
      out.write(def[1]);
      out.write(value);
      // fix jump instruction to
      // jge -> jz only abort load when there are NO ship models
      // jl -> jnz continue load as long the unsigned limit is reached
      out.write(jmpFix());
      out.write(def[4]);
    } else {
      out.write(def, 0, 2);
      out.write(value);
      out.write(def, 3, 2);
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(defaultData());
  }

  private boolean isJge() {
    return register() == Register.EAX;
  }

  private byte jmpFix() {
    return isJge() ? JZ_Fix : JNZ_Fix;
  }

  private byte[] defaultData() {
    Register reg = register();
    switch (reg) {
      case EAX:
        return Default_Data1;
      case EDX:
        return Default_Data2;
      default:
        throw new IllegalArgumentException("Invlid register: " + reg);
    }
  }
}
