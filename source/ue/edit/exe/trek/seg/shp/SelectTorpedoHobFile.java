package ue.edit.exe.trek.seg.shp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.stream.StreamTools;

/**
 * Torpedo hob loading code
 * This class contains alien id.
 */
public class SelectTorpedoHobFile extends InternalSegment {

  public static final String FILENAME = CTrekSegments.SelectTorpedoHobFile;
  public static final int SEGMENT_ADDRESS = 0x00095406;

  private static byte[] testOriginal = new byte[]{
    0x66, (byte)0x8B, (byte)0x84, 0x02, (byte)0xDC, 0x01, 0x00, 0x00, 0x25, (byte)0xFF,
    (byte)0xFF, 0x00, 0x00, (byte)0x89, (byte)0x84, 0x24, (byte)0xCC, 0x01, 0x00, 0x00,
    (byte)0x83, (byte)0xF8, 0x05, 0x7D, 0x47, 0x31, (byte)0xC0, (byte)0x8A, (byte)0x84,
    0x24, (byte)0xCC, 0x01, 0x00, 0x00, (byte)0xE8, 0x53, 0x4B, (byte)0xFA, (byte)0xFF};
  private static byte[] testExtended = new byte[]{
    0x03, (byte)0xC2, (byte)0x8B, (byte)0xD4, (byte)0xE8, 0x51, 0x28, 0x09, 0x00, (byte)0x8A,
    (byte)0x84, 0x24, 0x1B, 0x01, 0x00, 0x00, (byte)0x90, (byte)0x90, (byte)0x90,
    (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90,
    (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90,
    (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90, (byte)0x90};
  public static final int SIZE = testOriginal.length;

  private boolean extendedShiplistPatch;

  public SelectTorpedoHobFile() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, SelectTorpedoHobFile.class));
  }

  /**
   * @param in  the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    extendedShiplistPatch = false;
    byte[] b = StreamTools.readBytes(in, testOriginal.length);

    if (!check(b, testOriginal)) {
      validate(b, testExtended);
      extendedShiplistPatch = true;
    }

    markSaved();
  }

  /**
   * Used to save changes.
   *
   * @param out the OutputStream to write the file to.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    out.write(extendedShiplistPatch ? testExtended : testOriginal);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(testOriginal);
  }

  @Override
  public void clear() {
    extendedShiplistPatch = false;
    markChanged();
  }

  /**
   * @return uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public boolean isDefault() {
    return !extendedShiplistPatch;
  }

  public void setPatched(boolean patch) {
    if (extendedShiplistPatch != patch) {
      extendedShiplistPatch = patch;
      markChanged();
    }
  }

  public boolean isPatched() {
    return extendedShiplistPatch;
  }

  @Override
  public void reset() {
    setPatched(false);
  }
}
