package ue.edit.exe.trek.seg.shp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.sd.ValueDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.stream.StreamTools;

/**
 * Handling of ship names
 */
public class ShipnameCode extends InternalSegment {

  public static final String FILENAME = CTrekSegments.ShipnameCode;

  public static final int SEGMENT_ADDRESS = 0x00074827;
  public static final int SIZE = 554; // 35 * 15 + 21 + 8
  public static final int OPTION_ORIGINAL = 0;
  public static final int OPTION_EXTENDED = 1;
  public static final int OPTION_APPENDIX_ONLY = 2;

  private static final String[] code = new String[]
      {
          "66C78064555B00FFFF3DE802000075DC5AC300000000000000535156575583EC4066894" +
              "4243C89D5BB2800000031D289E88954242CE8F2830A008D5C24308D54242CB860B75700" +
              "E88DFD09008B44243AC1F8108B5C242C8D14850000000001DA8B0A31FF85C90F8E87000" +
              "00089C2C1E00229D0C1E0028BB06E555B00C1FE108B906C555B0083FEFF0F8498000000" +
              "3B9068555B000F848C0000008D41FFE8972C090089C28B44243AC1F810894424348B742" +
              "434C1E00229F08D3485000000008B866C555B0001C289D0C1FA1FF7F989D131C089966C" +
              "555B0089DE8B54243AC1FA1039D00F8DA80000008B1683C6044001D7EBE7BE70B757008" +
              "9D889EFE808FC0900578A0688073C0074108A460183C60288470183C7023C0075E85F31" +
              "C083C4405D5F5E595BC3BAD9000000B874B75700E8942A090089C2C1FA1FF7F98B44243" +
              "AC1F810894424348B742434C1E00229F08D348500000000899668555B0089966C555B00" +
              "66FF8670555B008B966E555B00B81A000000C1FA108944243489D0C1FA1FF77C2434668" +
              "99670555B00E909FFFFFF01CF89F8C1E70229C7C1E70229C78BB3F8000000C1E7028B4C" +
              "242C01FE89E701CEB92C0000005789C8C1E902F2A58AC880E103F2A45FB9280000008D7" +
              "4240489EF8B5C243C5789C8C1E902F2A58AC880E103F2A45F6685DB74716683FB32746B" +
              "6683FB337465BE9CB757008D7C2438578A0688073C0074108A460183C60288470183C70" +
              "23C0075E85F8D04950000000029D08A048570555B00",
          "899064555B009090903DE802000075DC5AC300000000000000535156575583EC4066894" +
              "4243C89D5BB2800000031D289E88954242CE8F2830A008D5C24308D54242CB860B75700" +
              "E88DFD09008B44243AC1F8108B5C242C8D14850000000001DA8B0A31FF85C90F8E87000" +
              "00089C2C1E00229D08A9069555B000FB6B06A555B008974243090909090909090909090" +
              "3A9068555B000F848C0000008D41FFE8972C090089C28B44243AC1F810894424348B742" +
              "434C1E00229F08BF00FB68669555B009090909001C289D0C1FA1FF7F989D131C0889669" +
              "555B0089DE8B54243AC1FA1039D00F8DA80000008B1683C6044001D7EBE7BE70B757008" +
              "9D889EFE808FC0900578A0688073C0074108A460183C60288470183C7023C0075E85F31" +
              "C083C4405D5F5E595BC3BAD9000000B874B75700E8942A090089C2C1FA1FF7F98B44243" +
              "AC1F810894424348B742434C1E00229F0889068555B00889069555B00FE806A555B0090" +
              "909090909090900FB6906A555B009090909090909080FA1B750233D2895424309090889" +
              "06A555B0090E909FFFFFF01CF89F8C1E70229C7C1E70229C7C1E7028D349383C604F646" +
              "038074F703F78BFCB92C0000005789C8C1E902F2A58AC880E103F2A45FB9280000008D7" +
              "4240489EF8B5C24305789C8C1E902F2A58AC880E103F2A45F6685DB74716683FA32746B" +
              "6683FA337465BE9CB757008D7C2438578A0688073C0074108A460183C60288470183C70" +
              "23C0075E85F8D04950000000029D08A806A555B0090",
          "66C78064555B00FFFF3DE802000075DC5AC300000000000000535156575583EC4066894" +
              "4243C89D5BB2800000031D289E88954242CE8F2830A008D5C24308D54242CB860B75700" +
              "E88DFD09008B44243AC1F8108B5C242C8D14850000000001DA8B0A31FF85C90F8E87000" +
              "00089C2C1E00229D0C1E0028BB06E555B00C1FE108B906C555B0083FEFF0F8498000000" +
              "3B9068555B0074F2897424308D41FFE8972C090089C28B44243AC1F810894424348B742" +
              "434C1E00229F08D3485000000008B866C555B0001C289D0C1FA1FF7F989D131C089966C" +
              "555B0089DE8B54243AC1FA1039D00F8DA80000008B1683C6044001D7EBE7BE70B757008" +
              "9D889EFE808FC0900578A0688073C0074108A460183C60288470183C7023C0075E85F31" +
              "C083C4405D5F5E595BC3BAD9000000B874B75700E8942A090089C2C1FA1FF7F98B44243" +
              "AC1F810894424348B742434C1E00229F08D348500000000899668555B0089966C555B00" +
              "66FF8670555B008B966E555B00B81A000000C1FA1080FA1B750233D2895424309090668" +
              "99670555B00E909FFFFFF01CF89F8C1E70229C7C1E70229C78BB3F8000000C1E7028B4C" +
              "242C01FE89E701CEB92C0000005789C8C1E902F2A58AC880E103F2A45FB9280000008D7" +
              "4240489EF8B5C24305789C8C1E902F2A58AC880E103F2A45F6685DB74716683FA32746B" +
              "6683FA337465BE9CB757008D7C2438578A0688073C0074108A460183C60288470183C70" +
              "23C0075E85F8D04950000000029D08A048570555B00"
      };

  private static final byte[][] options = new byte[][] {
    DatatypeConverter.parseHexBinary(code[0]),
    DatatypeConverter.parseHexBinary(code[1]),
    DatatypeConverter.parseHexBinary(code[2])
  };

  private static final ValueDefinition<Byte> sd_activeOption = new ValueDefinition<Byte>(0x17959D, null, ByteValueSegment.class, (byte)0x41);

  private Trek trek;
  private int option = -1;
  private ByteValueSegment seg_activeOption;

  public ShipnameCode(Trek trek) {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, ShipnameCode.class));
    this.trek = trek;
  }

  @Override
  public boolean isDefault() {
    return option == OPTION_ORIGINAL;
  }

  public int getActiveOption() {
    return option;
  }

  public void setActiveOption(int val) throws IOException, InvalidArgumentsException {
    if (val != option) {
      option = val;

      // if not already loaded, validate the segment value
      if (seg_activeOption == null) {
        seg_activeOption = (ByteValueSegment) trek.getSegment(sd_activeOption, true);
        if (!seg_activeOption.check(0x41))
          seg_activeOption.validate(0x40);
      }

      int flag = option != OPTION_ORIGINAL ? 0x41 : 0x40;
      seg_activeOption.setValue(flag);
    }
  }

  @Override
  public void reset() {
    if (option != OPTION_ORIGINAL) {
      option = OPTION_ORIGINAL;
      markChanged();
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] b = StreamTools.readBytes(in, options[0].length);

    if (check(b, options[0])) {
      option = OPTION_ORIGINAL;
    } else if (check(b, options[1])) {
      option = OPTION_EXTENDED;
    } else {
      validate(b, options[2]);
      option = OPTION_APPENDIX_ONLY;
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    HexBinaryAdapter adapter = new HexBinaryAdapter();
    byte[] b = adapter.unmarshal(code[option]);
    out.write(b);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    HexBinaryAdapter adapter = new HexBinaryAdapter();
    byte[] b = adapter.unmarshal(code[OPTION_ORIGINAL]);
    out.write(b);
  }

  @Override
  public void clear() {
    seg_activeOption = null;
    option = OPTION_ORIGINAL;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }
}
