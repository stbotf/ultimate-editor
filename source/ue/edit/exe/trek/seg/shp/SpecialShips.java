package ue.edit.exe.trek.seg.shp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;
import com.google.common.primitives.SignedBytes;
import ue.UE;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpIf;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SD_SpShips;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.ShipTech;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.stream.StreamTools;

/**
 * Reads/writes all of the trek.exe's function responsible for special ships.
 *
 * @author Alan Podlesek
 */
public class SpecialShips extends InternalSegment {

  public static final String FILENAME = CTrekSegments.SpecialShips;
  public static final int SEGMENT_ADDRESS = 0x000F3170;
  public static final int SIZE = 226;

  public static final int Default_StarbaseClass = 7;
  public static final int Default_Building = 38;
  public static final int[] Default_CallAddr = new int[] {
    0xFFF61762, 0x00027D5D, 0xFFF7BC04, 0xFFF47C99, 0x00026FD1, 0x0002797A
  };
  public static final int Default_SPReg = 0xFB;
  public static final int[] Default_SPRegs = new int[] {
    Default_SPReg, Default_SPReg, Default_SPReg, Default_SPReg, Default_SPReg };
  public static final int[] Default_SPShps = new int[] { 26, 27, 34, 41, 42 };
  public static final byte Default_SPRaceMask = 1;

  private static final int UNK1_SEGMENT_ADDRESS = 0x00054900;
  private static final int UNK2_SEGMENT_ADDRESS = 0x0011AF00;
  private static final int UNK3_SEGMENT_ADDRESS = 0x0006EDC0;
  private static final int UNK4_SEGMENT_ADDRESS = 0x0003AE80;
  private static final int UNK5_SEGMENT_ADDRESS = 0x0011A1F0;
  private static final int UNK6_SEGMENT_ADDRESS = 0x0011ABA0;

  private static final byte[] PUSH_BLOCK = new byte[]{
    OpCode.PUSH_EBX, OpCode.PUSH_ECX, OpCode.PUSH_EDX,
    OpCode.PUSH_ESI, OpCode.PUSH_EDI
  };

  // saveNormal
  private static final byte[] PREPARE_STACK = new byte[]{   // 6
    (byte) 0x81, (byte) 0xEC, 0x30, 1, 0, 0
  };
  private static final byte[] PREPARE_ESI = new byte[]{     // 27 bytes
    (byte) 0x8B, 0x3D, (byte) 0xA8, 0x5D, 0x59,
    0x00, (byte) 0x8B, 0x77, 0x1A, (byte) 0xC1,
    (byte) 0xFE, 0x10, 0x69, (byte) 0xF6, 0x28,
    0x03, 0x00, 0x00, (byte) 0xA1, (byte) 0xC8,
    0x36, 0x5A, 0x00, 0x01, (byte) 0xC6, 0x31,
    (byte) 0xC0
  };
  private static final byte[] READ_SYS_OWNER = new byte[]{  // 3
    (byte) 0x8A, 0x46, 0x4C
  };
  private static final byte[] CLEAR_STACK = new byte[]{     // 6
    (byte) 0x81, (byte) 0xC4, 0x30, 1, 0, 0
  };
  private static final byte[] POP_BLOCK = new byte[]{       // 6
    OpCode.POP_EDI, OpCode.POP_ESI, OpCode.POP_EDX,
    OpCode.POP_ECX, OpCode.POP_EBX, OpCode.RETN
  };
  private static final byte[] READ_SYS_RACE = new byte[]{   // 3
    (byte) 0x8A, 0x46, 0x44
  };

  private static final String DESC = "Sets special ships or updates code for special ship patches."
    + "<br>Further sneaks in a minor fixture for disabled special ships to match the BOP mod,"
    + " which skips some unnecessary NOPs.";

  private Trek trek;

  private int BUILDING;
  private int SP_REG[] = new int[5];    // byte values
  private int SP_SHIPS[] = new int[5];  // byte values
  private int CALL_ADDRESS[] = new int[6];
  private byte SP_RACE_MASK;

  // settings
  private boolean SPECIAL_SHIPS = true;
  private boolean MINOR_SHIPS = false;
  private boolean MINOR_SHIPS_SIMPLE = false;
  private boolean skipRaceCheck = false;

  private int STARBASE_CLASS;
  private int max_slots = 0;

  public SpecialShips(Trek trek) {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, SpecialShips.class).desc(DESC));
    this.trek = trek;
  }

  /**
   * @param in   stream to read from
   * @throws IOException
   */
  @Override
  public void load(InputStream in) throws IOException {
    // read and test input
    validate(in, PUSH_BLOCK);                     // 5     5

    int b = in.read();                            // 1     6
    if (b == OpCode.PUSH_EBP) {
      SPECIAL_SHIPS = false;
      MINOR_SHIPS = true;
      loadMinorShipsMod(in);                      // 220 226
    } else {
      // expected code
      final byte[] PREPARE_STACK = new byte[]{    // 5 bytes
        (byte) 0xEC, 0x30, 0x01, 0x00, 0x00
      };

      MINOR_SHIPS = false;
      validate(b, 0x81);
      validate(in, PREPARE_STACK);                // 5    11
      loadNormal(in);                             // 215 226
    }

    markSaved();
  }

  /**
   * @param in
   * @throws Exception
   */
  private void loadMinorShipsMod(InputStream in) throws IOException
  {
    final byte[] PREPARE_STACK = new byte[]{        // 6 bytes
      (byte) 0x81, (byte) 0xEC, 0x30, 0x01, 0x00,
      0x00
    };
    final byte[] SET_EBP_TO_MINORS = new byte[]{    // 5
      (byte) 0xBD, 0x44, 0x00, 0x00, 0x00
    };
    final byte[] PREPARE_ESI = new byte[]{          // 27
      (byte) 0x8B, 0x3D, (byte) 0xA8, 0x5D, 0x59,
      0x00, (byte) 0x8B, 0x77, 0x1A, (byte) 0xC1,
      (byte) 0xFE, 0x10, 0x69, (byte) 0xF6, 0x28,
      0x03, 0x00, 0x00, (byte) 0xA1, (byte) 0xC8,
      0x36, 0x5A, 0x00, 0x01, (byte) 0xC6, 0x31,
      (byte) 0xC0
    };
    final byte[] READ_SYS_EBP = new byte[]{         // 3
      (byte) 0x8A, 0x04, 0x2E
    };
    final byte[] CHECK_AND_SET_EBP_FOR_RERUN = new byte[]{ // 8
      (byte) 0x83, (byte) 0xFD, 0x44, 0x75, 0x08,
      (byte) 0x83, (byte) 0xC5, 0x08
    };
    final byte[] CLEAR_STACK = new byte[]{          // 6
      (byte) 0x81, (byte) 0xC4, 0x30, 0x01,
      0x00, 0x00
    };
    final byte[] POP_BLOCK = new byte[]{            // 6
      OpCode.POP_EDI, OpCode.POP_ESI, OpCode.POP_EDX,
      OpCode.POP_ECX, OpCode.POP_EBX, OpCode.RETN
    };

    validate(in, PREPARE_STACK);                // 6    6
    validate(in, SET_EBP_TO_MINORS);            // 5    11
    validate(in, PREPARE_ESI);                  // 27   38
    validate(in, READ_SYS_EBP);                 // 3    41
    loadMRMRaceTest(in, 152);                   // 36   77
    loadShipLoadAndBasicChecks(in, 103, 116);   // 51   128
    loadAddShipToList(in, -31);                 // 65   193
    validate(in, CHECK_AND_SET_EBP_FOR_RERUN);  // 8    201
    validate(in, Jmp.JMP);                      // 1    202
    validateInt(in, -195);                      // 4    206
    validate(in, CLEAR_STACK);                  // 6    212
    validate(in, OpCode.POP_EBP);               // 1    213
    validate(in, POP_BLOCK);                    // 6    219
    validate(in, 0);                            // 16   220
  }

  private void loadMRMRaceTest(InputStream in, int exit_address) throws IOException
  {
    final byte[] IS_EMPIRE = new byte[]{            // 2 bytes
      0x3C, 0x04
    };
    final byte[] IS_SYS_OWNER = new byte[]{         // 3
      (byte) 0x83, (byte) 0xFD, 0x4C
    };
    final byte[] GET_SUBJUDGED_STATUS = new byte[]{ // 12
      (byte) 0xBD, 0x10, 0x20, 0x5A, 0x00,
      0x6B, (byte) 0xC0, 0x40, (byte) 0x8B,
      0x44, 0x28, 0x3C
    };
    final byte[] TEST_EAX = new byte[]{             // 3
      (byte) 0x83, (byte) 0xF8, 0x01
    };
    final byte[] READ_SYS_EBP = new byte[]{         // 3
      (byte) 0x8A, 0x04, 0x2E
    };

    validate(in, IS_EMPIRE);            // 2    2
    validate(in, JmpShort.JA);          // 1    3
    validate(in, 0xA);                  // 1    4
    validate(in, IS_SYS_OWNER);         // 3    7
    validate(in, JmpShort.JZ);          // 1    8
    validate(in, 0x18);                 // 1    9
    validate(in, Jmp.JMP);              // 1    10
    validateInt(in, exit_address - 14); // 4    14
    validate(in, OpCode.PUSH_EBP);      // 1    15
    validate(in, GET_SUBJUDGED_STATUS); // 12   27
    validate(in, OpCode.POP_EBP);       // 1    28
    validate(in, TEST_EAX);             // 3    31
    validate(in, JmpShort.JZ);          // 1    32
    validate(in, 0xE8);                 // 1    33
    validate(in, READ_SYS_EBP);         // 3    36
  }

  /**
   * Unpatched minor ships mod normal load.
   * at SEGMENT_ADDRESS + 11
   *
   * @param in
   * @param b leftovers of previous testInput read
   * @throws Exception
   */
  private void loadNormal(InputStream in) throws IOException
  {
    final byte[] PREPARE_ESI = new byte[]{          // 27
      (byte) 0x8B, 0x3D, (byte) 0xA8, 0x5D, 0x59,
      0x00, (byte) 0x8B, 0x77, 0x1A, (byte) 0xC1,
      (byte) 0xFE, 0x10, 0x69, (byte) 0xF6, 0x28,
      0x03, 0x00, 0x00, (byte) 0xA1, (byte) 0xC8,
      0x36, 0x5A, 0x00, 0x01, (byte) 0xC6, 0x31,
      (byte) 0xC0
    };
    final byte[] READ_SYS_OWNER = new byte[]{       // 3
      (byte) 0x8A, 0x46, 0x4C
    };
    final byte[] READ_SYS_RACE = new byte[]{        // 3
      (byte) 0x8A, 0x46, 0x44
    };
    final byte[] CLEAR_STACK = new byte[]{          // 6
      (byte) 0x81, (byte) 0xC4, 0x30, 0x01,
      0x00, 0x00
    };
    final byte[] POP_BLOCK = new byte[]{            // 6
      0x5F, 0x5E, 0x5A, 0x59, 0x5B,
      (byte) 0xC3
    };

    validate(in, PREPARE_ESI);         // 27   27

    // detect simple minor ships build patch
    // when activated, player can't build empire ships in minor race system (fixed on save)
    byte[] b = StreamTools.readBytes(in, READ_SYS_RACE.length); // 3    30
    MINOR_SHIPS_SIMPLE = check(b, READ_SYS_RACE);
    if (!MINOR_SHIPS_SIMPLE)
      validate(b, READ_SYS_OWNER);

    loadShipLoadAndBasicChecks(in, 134, 147);       // 51   81
    loadRaceAndSP1Check(in);                        // 12   93
    loadSpecialBuildingCheck(in, 33);               // 19   112
    loadAddShipToList(in, -62);                     // 65   177
    validate(in, CLEAR_STACK);                      // 6    183
    validate(in, POP_BLOCK);                        // 6    192
    loadSpecialShipChecks(in, -129, -96);                // 26   215
  }

  // SEGMENT_ADDRESS + 41
  private void loadShipLoadAndBasicChecks(InputStream in, int jump_to_next_address, int jump_to_exit_address) throws IOException
  {
    final byte[] TEST_CALL_RESULTS = new byte[]{    // 4
      (byte) 0x89, (byte) 0xC1, (byte) 0x85,
      (byte) 0xC0
    };
    final byte[] UNKNOWN = new byte[]{              // 10
      0x66, (byte) 0x8B, 0x59, 0x04, 0x31,
      (byte) 0xC0, (byte) 0x89, (byte) 0xE2,
      (byte) 0x88, (byte) 0xD8
    };
    final byte[] CHECK_IF_OUTPOST = new byte[]{     // 8
      (byte) 0x8B, 0x54, 0x24, 0x68, 0x66,
      (byte) 0x83, (byte) 0xFA, 0x06
    };
    final byte[] CHECK_IF_STARBASE = new byte[]{    // 3
      0x66, (byte) 0x83, (byte) 0xFA
    };

    validate(in, Jmp.CALL);                           // 1    1
    CALL_ADDRESS[0] = StreamTools.readInt(in, true);  // 4    5
    validate(in, Jmp.CALL);                           // 1    6
    CALL_ADDRESS[1] = StreamTools.readInt(in, true);  // 4    10

    validate(in, TEST_CALL_RESULTS);                  // 4    14
    validate(in, JmpIf.JZ);                           // 2    16
    validateInt(in, jump_to_exit_address - 20);       // 4    20
    validate(in, UNKNOWN);                            // 10   30
    validate(in, Jmp.CALL);                           // 1    31
    CALL_ADDRESS[2] = StreamTools.readInt(in, true);  // 4    35

    validate(in, CHECK_IF_OUTPOST);                   // 8    43
    validate(in, JmpShort.JZ);                        // 1    44
    validate(in, jump_to_next_address - 45);          // 1    45
    validate(in, CHECK_IF_STARBASE);                  // 3    48

    // or some other class
    STARBASE_CLASS = in.read();                       // 1    49

    validate(in, JmpShort.JZ);                        // 1    50
    validate(in, jump_to_next_address - 51);          // 1    51
  }

  // SEGMENT_ADDRESS + 92
  private void loadRaceAndSP1Check(InputStream in) throws IOException
  {
    final byte[] COMPARE_SP = new byte[]{           // 2
      0x66, (byte) 0x83
    };
    final byte[] DISABLED_COMPARE_SP = new byte[]{  // 6
      (byte)0xEB, 0x17, NOP, NOP, NOP, NOP
    };
    final byte[] DISABLED_COMPARE_SP_ALT = new byte[]{  // 6
      NOP, NOP, NOP, NOP, (byte)0xEB, 0x13
    };
    final byte[] RACE_CHECK = new byte[] {          // 3
      (byte)0x80, 0x7E, 0x4C
    };

    // detect skipped race check
    byte[] b = StreamTools.readBytes(in, 6);        // 6    6
    skipRaceCheck = check(b, 0, 6, NOP);
    if (!skipRaceCheck) {
      validate(b, 0, RACE_CHECK);
      // Race mask for races that build special ships. Defaults to 1 == Federation.
      // @see 1c. Empire Check: https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=3441#p3441
      SP_RACE_MASK = b[3];
      validate(b, 4, JmpShort.JNZ);
      validate(b, 5, (byte)0x19);
    }

    b = StreamTools.readBytes(in, 6);               // 6    12
    if (!check(b, 0, COMPARE_SP)) {
      // validate patched
      // 90 90 90 90 EB 13 (UE) == EB 17 90 90 90 90 (BOP)
      if (!check(b, DISABLED_COMPARE_SP))
        validate(b, DISABLED_COMPARE_SP_ALT);

      // both
      SPECIAL_SHIPS = false;
    } else {
      // load normal (vanilla default)
      validate(b, 4, JmpShort.JNZ);
      validate(b, 5, (byte)0x60);

      SP_REG[0] = Byte.toUnsignedInt(b[2]);
      if (SP_REG[0] == 0xFB)
        max_slots++;

      SP_SHIPS[0] = Byte.toUnsignedInt(b[3]);
      SPECIAL_SHIPS = true;
    }
  }

  private void loadSpecialBuildingCheck(InputStream in, int jump_next_address) throws IOException // 19
  {
    final byte[] TEST_SP_BUILDING_1 = new byte[]{   // 3
      0x31, (byte) 0xC0, (byte) 0xBA
    };
    final byte[] TEST_SP_BUILDING_2 = new byte[]{   // 3
      (byte) 0x8A, 0x46, 0x4C
    };
    final byte[] TEST_SP_BUILDING_3 = new byte[]{   // 2
      (byte) 0x85, (byte) 0xC0
    };

    validate(in, TEST_SP_BUILDING_1);                 // 3    3
    BUILDING = StreamTools.readInt(in, true);         // 4    7

    validate(in, TEST_SP_BUILDING_2);                 // 3    10
    validate(in, Jmp.CALL);                           // 1    11
    CALL_ADDRESS[3] = StreamTools.readInt(in, true);  // 4    15

    validate(in, TEST_SP_BUILDING_3);                 // 2    17
    validate(in, JmpShort.JZ);                        // 1    18
    validate(in, jump_next_address + 19);             // 1    19
  }

  private void loadAddShipToList(InputStream in, int jump_next_address) throws IOException // 65
  {
    final byte[] ADD_SHIP_TO_LIST_1 = new byte[]{   // 47
      (byte) 0xBA, 0x03, 0x00, 0x00, 0x00, 0x31,
      (byte) 0xC0, 0x66, (byte) 0x89, (byte) 0x9C,
      0x24, 0x28, 0x01, 0x00, 0x00, 0x66,
      (byte) 0x8B, (byte) 0x84, 0x24, 0x1C, 0x01,
      0x00, 0x00, (byte) 0x89, (byte) 0x94, 0x24,
      0x20, 0x01, 0x00, 0x00, (byte) 0x89,
      (byte) 0x84, 0x24, 0x2C, 0x01, 0x00, 0x00,
      (byte) 0x8D, (byte) 0x94, 0x24, 0x20, 0x01,
      0x00, 0x00, (byte) 0x8B, 0x47, 0x38
    };
    final byte[] ADD_SHIP_TO_LIST_2 = new byte[]{   // 2
      (byte) 0x89, (byte) 0xC8
    };
    final byte[] ADD_SHIP_TO_LIST_3 = new byte[]{   // 4
      (byte) 0x89, (byte) 0xC1, (byte) 0x85,
      (byte) 0xC0
    };

    validate(in, ADD_SHIP_TO_LIST_1);                 // 47   47
    validate(in, Jmp.CALL);                           // 1    48
    CALL_ADDRESS[4] = StreamTools.readInt(in, true);  // 4    52

    validate(in, ADD_SHIP_TO_LIST_2);                 // 2    54
    validate(in, Jmp.CALL);                           // 1    55
    CALL_ADDRESS[5] = StreamTools.readInt(in, true);  // 4    59

    validate(in, ADD_SHIP_TO_LIST_3);                 // 4    63
    validate(in, JmpShort.JNZ);                       // 1    64
    validate(in, jump_next_address - 65);             // 1    65
  }

  private void loadSpecialShipChecks(InputStream in, int jump_next_address, int jump_add_address) throws IOException
  {
    final byte[] COMPARE_SP = new byte[]{         // 2
      0x66, (byte) 0x83
    };

    for (int i = 1; i < 5; i++)
    {
      validate(in, COMPARE_SP);                   // 2
      SP_REG[i] = in.read();                      // 1

      if (SP_REG[i] == 0xFB) {
        max_slots++;
      }

      SP_SHIPS[i] = in.read();                    // 1

      // jump
      validate(in, JmpShort.JZ);                  // 1
      validate(in, jump_add_address - i * 6);     // 1
    }                                             // 24   24

    validate(in, JmpShort.JMP);                   // 1    25
    validate(in, jump_next_address + 26);         // 1    26
  }

  @Override
  public void check(Vector<String> response) {
    if (MINOR_SHIPS_SIMPLE) {
      Stbof stbof = UE.FILES.stbof();
      if (stbof != null) {
        try {
          RaceRst races = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
          ShipList ships = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
          ShipTech tech = (ShipTech) stbof.getInternalFile(CStbofFiles.ShipTechSst, true);
          String msg = "%1 have less than two ships available at tech. level 1.";

          for (int r = 0; r < races.getNumberOfEntries(); r++) {
            ID[] ids = ships.getIDs(r);
            int num = 0;

            for (ID id : ids) {
              if (SignedBytes.max(tech.getTechLvls(id.hashCode())) == 1) {
                num++;

                if (num >= 2)
                  break;
              }
            }

            if (num < 2) {
              String err = msg.replace("%1", races.getName(r));
              response.add(getCheckIntegrityString(InternalSegment.INTEGRITY_CHECK_ERROR, err));
            }
          }
        } catch (Exception ex) {
          ex.printStackTrace();
          response.add(getCheckIntegrityString(InternalSegment.INTEGRITY_CHECK_ERROR, ex.getMessage()));
        }
      }
    }
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    if (MINOR_SHIPS || MINOR_SHIPS_SIMPLE || !SPECIAL_SHIPS || skipRaceCheck)
      return false;
    if (STARBASE_CLASS != Default_StarbaseClass || BUILDING != Default_Building
      || SP_RACE_MASK != Default_SPRaceMask)
      return false;

    for (int i = 0; i < CALL_ADDRESS.length; ++i)
      if (CALL_ADDRESS[i] != Default_CallAddr[i])
        return false;
    for (int i = 0; i < SP_SHIPS.length; ++i)
      if (SP_SHIPS[i] != Default_SPShps[i])
        return false;
    for (int i = 0; i < SP_REG.length; ++i)
      if (SP_REG[i] != Default_SPReg)
        return false;

    return true;
  }

  /**
   * @param b if true special ships are enabled
   */
  public void setSPEnabled(boolean b) {
    if (areSPEnabled() != b) {
      this.markChanged();
    }

    SPECIAL_SHIPS = b;

    if (SPECIAL_SHIPS) {
      MINOR_SHIPS_SIMPLE = MINOR_SHIPS || MINOR_SHIPS_SIMPLE;
      MINOR_SHIPS = false;
    }
  }

  /**
   * @param id building id of the building that allows building special ships
   */
  public void setSPBuilding(int id) {
    if (id != BUILDING) {
      if (id >= 0 && id <= 255) {
        BUILDING = id;
        this.markChanged();
      }
    }
  }

  /**
   * @return building id
   */
  public int getSPBuilding() {
    return BUILDING;
  }

  /**
   * @return true if special ships are enabled
   */
  public boolean areSPEnabled() {
    return SPECIAL_SHIPS;
  }

  public HashSet<Integer> getShipIDs() {
    HashSet<Integer> ret = new HashSet<Integer>();

    if (areSPEnabled()) {
      for (int i = 0; i < 5; i++) {
        if (SP_REG[i] == 0xFB) {
          ret.add(SP_SHIPS[i]);
        }
      }
    }

    return ret;
  }

  /**
   * @param ids ship ids of special ships (max 5)
   */
  public void setShipIDs(HashSet<Integer> ids) {
    setSPEnabled(ids.size() > 0 && ids.size() <= max_slots);

    if (areSPEnabled()) {
      int[] ships = new int[ids.size()];

      int i = 0;

      Iterator<Integer> it = ids.iterator();

      while (it.hasNext()) {
        ships[i++] = it.next();
      }

      i = 0;

      for (int slot = 0; slot < 5; slot++) {
        if (SP_REG[slot] == 0xFB && SP_SHIPS[slot] != ships[i]) {
          this.markChanged();
          SP_SHIPS[slot] = ships[i];

          if (i < ships.length - 1) {
            i++;
          }
        }
      }
    }
  }

  public int getSPIdSlotsNumber() {
    return max_slots;
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#save(java.io.OutputStream)
   */
  @Override
  public void save(OutputStream out) throws IOException
  {
    updateAddresses();

    if (MINOR_SHIPS) {
      saveAsUsingMinorShipMod(out);   // 221 226
    } else {
      saveNormal(out);                // 221 226
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException
  {
    out.write(PUSH_BLOCK);                          // 5     5
    out.write(PREPARE_STACK);                       // 6    11
    out.write(PREPARE_ESI);                         // 27   38
    out.write(READ_SYS_OWNER);                      // 3    41

    saveShipLoadAndBasicChecks(out, 134, 147, Default_CallAddr, Default_StarbaseClass); // 51   92
    saveRaceAndSP1Check(out, 108, 31, false, Default_SPRaceMask, Default_SPShps);       // 12  104
    saveSpecialBuildingCheck(out, 33, Default_CallAddr, Default_Building);              // 19  123
    saveAddShipToList(out, -62, Default_CallAddr);  // 65  188
    out.write(CLEAR_STACK);                         // 6   194
    out.write(POP_BLOCK);                           // 6   200
    saveSpecialShipChecks(out, -129, -96, Default_SPShps, Default_SPRegs);              // 26  226
  }

  /**
   * @param out
   * @throws IOException
   */
  private void saveNormal(OutputStream out) throws IOException {
    out.write(PUSH_BLOCK);                          // 5     5
    out.write(PREPARE_STACK);                       // 6    11
    out.write(PREPARE_ESI);                         // 27   38

    if (MINOR_SHIPS_SIMPLE) {
      out.write(READ_SYS_RACE);                     // 3    41
    } else {
      out.write(READ_SYS_OWNER);                    // 3    41
    }

    saveShipLoadAndBasicChecks(out, 134, 147, CALL_ADDRESS, STARBASE_CLASS);            // 51   92
    saveRaceAndSP1Check(out, 108, 31, skipRaceCheck, SP_RACE_MASK, SPECIAL_SHIPS ? SP_SHIPS : null);  // 12  104
    saveSpecialBuildingCheck(out, 33, CALL_ADDRESS, BUILDING);                          // 19  123
    saveAddShipToList(out, -62, CALL_ADDRESS);      // 65  188
    out.write(CLEAR_STACK);                         // 6   194
    out.write(POP_BLOCK);                           // 6   200
    saveSpecialShipChecks(out, -129, -96, SP_SHIPS, SP_REG);  // 26  226
  }

  private static void saveShipLoadAndBasicChecks(OutputStream out, int jump_to_next_address,
    int jump_to_exit_address, int[] callAddr, int starBaseClass) throws IOException
  {
    final byte[] TEST_CALL_RESULTS = new byte[]{    // 4
      (byte) 0x89, (byte) 0xC1, (byte) 0x85,
      (byte) 0xC0
    };
    final byte[] JZ = new byte[]{                   // 2
      (byte) 0x0F, (byte) 0x84
    };
    final byte[] UNKNOWN = new byte[]{              // 10
      0x66, (byte) 0x8B, 0x59, 0x04, 0x31,
      (byte) 0xC0, (byte) 0x89, (byte) 0xE2,
      (byte) 0x88, (byte) 0xD8
    };
    final byte[] CHECK_IF_OUTPOST = new byte[]{     // 8
      (byte) 0x8B, 0x54, 0x24, 0x68, 0x66,
      (byte) 0x83, (byte) 0xFA, 0x06
    };
    final byte[] SJZ = new byte[]{                  // 1
      0x74
    };
    final byte[] CHECK_IF_STARBASE = new byte[]{    // 3
      0x66, (byte) 0x83, (byte) 0xFA
    };

    out.write(Jmp.CALL);
    out.write(DataTools.toByte(callAddr[0], true));     //     5        5

    out.write(Jmp.CALL);
    out.write(DataTools.toByte(callAddr[1], true));     //      5       10

    out.write(TEST_CALL_RESULTS);                       //      4       14
    out.write(JZ);                                      //      2       16
    int exitAddr = jump_to_exit_address - 20;
    out.write(DataTools.toByte(exitAddr, true));        //      4       20

    out.write(UNKNOWN);                                 //      10      30
    out.write(Jmp.CALL);
    out.write(DataTools.toByte(callAddr[2], true));     //      5       35

    out.write(CHECK_IF_OUTPOST);                        //      8       43
    out.write(SJZ);
    out.write(jump_to_next_address - 45);               //      2       45

    out.write(CHECK_IF_STARBASE);                       //      3       48
    out.write(starBaseClass);                           //      1       49

    out.write(SJZ);
    out.write(jump_to_next_address - 51);               //      2       51
  }

  private static void saveRaceAndSP1Check(OutputStream out, int jump_next_address, int jump_add_address,
    boolean skipRaceCheck, byte raceMask, int[] specialShips)
    throws IOException {
    final byte[] COMPARE_SP = new byte[] {      // 2
      0x66, (byte)0x83
    };
    final byte[] RACE_CHECK = new byte[] {      // 3
      (byte)0x80, 0x7E, 0x4C
    };
    final byte[] RACE_CHECK_JMP = new byte[] {  // 2
      0x75, 0x19
    };

    // write or skip the race check
    if (skipRaceCheck) {                    // 6
      for (int i = 0; i < 6; i++)
        out.write(NOP);
    } else {
      out.write(RACE_CHECK);
      out.write(raceMask);
      out.write(RACE_CHECK_JMP);
    }

    if (specialShips != null) {
      out.write(COMPARE_SP);                // 2 -> 8
      out.write(0xFB);                      // 1 -> 9
      out.write(specialShips[0]);           // 1 -> 10
      out.write(JmpShort.JNZ);              // 11
      out.write(jump_next_address - 12);    // 12
    } else {
      out.write(0xEB);
      out.write(jump_add_address - 8);
      for (int i = 0; i < 4; i++)
        out.write(NOP);                     // 12
    }
  }

  private static void saveSpecialBuildingCheck(OutputStream out, int jump_next_address,
    int[] callAddr, int bldID) throws IOException {
    final byte[] TEST_SP_BUILDING_1 = new byte[]{   // 3
      0x31, (byte) 0xC0, (byte) 0xBA
    };
    final byte[] TEST_SP_BUILDING_2 = new byte[]{   // 3
      (byte) 0x8A, 0x46, 0x4C
    };
    final byte[] TEST_SP_BUILDING_3 = new byte[]{   // 2
      (byte) 0x85, (byte) 0xC0
    };

    out.write(TEST_SP_BUILDING_1);                  // 3     3
    out.write(DataTools.toByte(bldID, true));       // 4     7
    out.write(TEST_SP_BUILDING_2);                  // 3    10
    out.write(Jmp.CALL);                            // 1    11
    out.write(DataTools.toByte(callAddr[3], true)); // 4    15
    out.write(TEST_SP_BUILDING_3);                  // 2    17
    out.write(JmpShort.JZ);                         // 1    18
    out.write(jump_next_address + 19);              // 1    19
  }

  private static void saveAddShipToList(OutputStream out, int jump_next_address, int[] callAddr) throws IOException
  {
    final byte[] ADD_SHIP_TO_LIST_1 = new byte[]{   // 47
      (byte) 0xBA, 0x03, 0x00, 0x00, 0x00, 0x31,
      (byte) 0xC0, 0x66, (byte) 0x89, (byte) 0x9C,
      0x24, 0x28, 0x01, 0x00, 0x00, 0x66,
      (byte) 0x8B, (byte) 0x84, 0x24, 0x1C, 0x01,
      0x00, 0x00, (byte) 0x89, (byte) 0x94, 0x24,
      0x20, 0x01, 0x00, 0x00, (byte) 0x89,
      (byte) 0x84, 0x24, 0x2C, 0x01, 0x00, 0x00,
      (byte) 0x8D, (byte) 0x94, 0x24, 0x20, 0x01,
      0x00, 0x00, (byte) 0x8B, 0x47, 0x38
    };
    final byte[] ADD_SHIP_TO_LIST_2 = new byte[]{   // 2
      (byte) 0x89, (byte) 0xC8
    };
    final byte[] ADD_SHIP_TO_LIST_3 = new byte[]{   // 4
      (byte) 0x89, (byte) 0xC1, (byte) 0x85,
      (byte) 0xC0
    };

    out.write(ADD_SHIP_TO_LIST_1);                  // 47   47
    out.write(Jmp.CALL);                            // 1    48
    out.write(DataTools.toByte(callAddr[4], true)); // 4    52
    out.write(ADD_SHIP_TO_LIST_2);                  // 2    54
    out.write(Jmp.CALL);                            // 1    55
    out.write(DataTools.toByte(callAddr[5], true)); // 4    59
    out.write(ADD_SHIP_TO_LIST_3);                  // 4    63
    out.write(JmpShort.JNZ);                        // 1    64
    out.write(jump_next_address - 65);              // 1    65
  }

  private static void saveSpecialShipChecks(OutputStream out, int jump_next_address, int jump_add_address, int[] spShps, int[] spReg)
    throws IOException {
    final byte[] COMPARE_SP = new byte[]{           // 2
      0x66, (byte) 0x83
    };

    for (int i = 1; i < 5; i++)
    {
      out.write(COMPARE_SP);                // 2
      out.write(spReg[i]);                  // 1
      out.write(spShps[i]);                 // 1
      out.write(JmpShort.JZ);               // 1
      out.write(jump_add_address - i * 6);  // 1
    }                                       // 6    24

    out.write(JmpShort.JMP);                // 1    25
    out.write(jump_next_address + 26);      // 1    26
  }

  /**
   * @param out
   * @throws IOException
   */
  private void saveAsUsingMinorShipMod(OutputStream out) throws IOException
  {
    final byte[] PREPARE_STACK = new byte[]{        // 6
      (byte) 0x81, (byte) 0xEC, 0x30, 0x01, 0x00,
      0x00
    };
    final byte[] SET_EBP_TO_MINORS = new byte[]{    // 5
      (byte) 0xBD, 0x44, 0x00, 0x00, 0x00
    };
    final byte[] PREPARE_ESI = new byte[]{          // 27 bytes
      (byte) 0x8B, 0x3D, (byte) 0xA8, 0x5D, 0x59,
      0x00, (byte) 0x8B, 0x77, 0x1A, (byte) 0xC1,
      (byte) 0xFE, 0x10, 0x69, (byte) 0xF6, 0x28,
      0x03, 0x00, 0x00, (byte) 0xA1, (byte) 0xC8,
      0x36, 0x5A, 0x00, 0x01, (byte) 0xC6, 0x31,
      (byte) 0xC0
    };
    final byte[] READ_SYS_EBP = new byte[]{         // 3
      (byte) 0x8A, 0x04, 0x2E
    };
    final byte[] CHECK_AND_SET_EBP_FOR_RERUN = new byte[]{ // 8
      (byte) 0x83, (byte) 0xFD, 0x44, 0x75, 0x08,
      (byte) 0x83, (byte) 0xC5, 0x08
    };
    final byte[] JMP = new byte[]{                  // 1
      (byte) 0xE9
    };
    final byte[] CLEAR_STACK = new byte[]{          // 6
      (byte) 0x81, (byte) 0xC4, 0x30, 0x01,
      0x00, 0x00
    };
    final byte[] POP_EBP = new byte[]{              // 1
      0x5D
    };
    final byte[] PUSH_EBP = new byte[]{             // 1
      0x55
    };
    final byte[] POP_BLOCK = new byte[]{            // 6
      0x5F, 0x5E, 0x5A, 0x59, 0x5B,
      (byte) 0xC3
    };
    final byte[] BLANK_BLOCK_END = new byte[]{      // 1
      0x00
    };

    out.write(PUSH_BLOCK);                      // 5     5
    out.write(PUSH_EBP);                        // 1     6
    out.write(PREPARE_STACK);                   // 6    12
    out.write(SET_EBP_TO_MINORS);               // 5    17
    out.write(PREPARE_ESI);                     // 27   44
    out.write(READ_SYS_EBP);                    // 3    47
    saveMRMRaceTest(out, 152);                  // 36   83
    saveShipLoadAndBasicChecks(out, 103, 116, CALL_ADDRESS, STARBASE_CLASS);  // 51  134
    saveAddShipToList(out, -31, CALL_ADDRESS);  // 65  199
    out.write(CHECK_AND_SET_EBP_FOR_RERUN);     // 8   207
    out.write(JMP);                             // 1   208
    out.write(DataTools.toByte(-195, true));    // 4   212
    out.write(CLEAR_STACK);                     // 6   218
    out.write(POP_EBP);                         // 1   219
    out.write(POP_BLOCK);                       // 6   225
    out.write(BLANK_BLOCK_END);                 // 1   226
  }

  private static void saveMRMRaceTest(OutputStream out, int exit_address) throws IOException
  {
    final byte[] IS_EMPIRE = new byte[]{            // 2
      0x3C, 0x04
    };
    final byte[] JA = new byte[]{                   // 2
      0x77, 0x0A
    };
    final byte[] IS_SYS_OWNER = new byte[]{         // 3
      (byte) 0x83, (byte) 0xFD, 0x4C
    };
    final byte[] JZ = new byte[]{                   // 2
      0x74, 0x18
    };
    final byte[] JMP = new byte[]{                  // 1
      (byte) 0xE9
    };
    final byte[] PUSH_EBP = new byte[]{             // 1 byte
      0x55
    };
    final byte[] GET_SUBJUDGED_STATUS = new byte[]{ // 12
      (byte) 0xBD, 0x10, 0x20, 0x5A, 0x00,
      0x6B, (byte) 0xC0, 0x40, (byte) 0x8B,
      0x44, 0x28, 0x3C
    };
    final byte[] POP_EBP = new byte[]{              // 1
      0x5D
    };
    final byte[] TEST_EAX = new byte[]{             // 3
      (byte) 0x83, (byte) 0xF8, 0x01
    };
    final byte[] JZ2 = new byte[]{                  // 2
      0x74, (byte) 0xE8
    };
    final byte[] READ_SYS_EBP = new byte[]{         // 3
      (byte) 0x8A, 0x04, 0x2E
    };

    out.write(IS_EMPIRE);                                 // 2     2
    out.write(JA);                                        // 2     4
    out.write(IS_SYS_OWNER);                              // 3     7
    out.write(JZ);                                        // 2     9
    out.write(JMP);                                       // 1    10
    out.write(DataTools.toByte(exit_address - 14, true)); // 4    14
    out.write(PUSH_EBP);                                  // 1    15
    out.write(GET_SUBJUDGED_STATUS);                      // 12   27
    out.write(POP_EBP);                                   // 1    28
    out.write(TEST_EAX);                                  // 3    31
    out.write(JZ2);                                       // 2    33
    out.write(READ_SYS_EBP);                              // 3    36
  }

  @Override
  public void clear() {
    BUILDING = 0;
    Arrays.fill(SP_REG, 0);
    Arrays.fill(SP_SHIPS, 0);
    Arrays.fill(CALL_ADDRESS, 0);
    SPECIAL_SHIPS = true;
    MINOR_SHIPS = false;
    MINOR_SHIPS_SIMPLE = false;
    skipRaceCheck = false;
    STARBASE_CLASS = 0;
    max_slots = 0;
    markChanged();
  }

  public void setMinorShipsMod(boolean b) {
    if (MINOR_SHIPS != b) {
      markChanged();
      MINOR_SHIPS = b;

      if (MINOR_SHIPS) {
        SPECIAL_SHIPS = false;
        MINOR_SHIPS_SIMPLE = false;
      }
    }
  }

  public boolean areMinorShipsEnabled() {
    return MINOR_SHIPS;
  }

  public void setMinorShipsSimpleMod(boolean b) {
    if (MINOR_SHIPS_SIMPLE != b) {
      MINOR_SHIPS_SIMPLE = b;
      markChanged();

      // disable the advanced minor ships building mod
      if (MINOR_SHIPS_SIMPLE)
        MINOR_SHIPS = false;
    }
  }

  public boolean areMinorShipsSimpleEnabled() {
    return MINOR_SHIPS_SIMPLE;
  }

  private void updateAddresses() {
    CALL_ADDRESS[0] = UNK1_SEGMENT_ADDRESS;
    CALL_ADDRESS[1] = UNK2_SEGMENT_ADDRESS;
    CALL_ADDRESS[2] = UNK3_SEGMENT_ADDRESS;
    CALL_ADDRESS[3] = UNK4_SEGMENT_ADDRESS;
    CALL_ADDRESS[4] = UNK5_SEGMENT_ADDRESS;
    CALL_ADDRESS[5] = UNK6_SEGMENT_ADDRESS;

    for (int i = 0; i < CALL_ADDRESS.length; i++) {
      CALL_ADDRESS[i] -= address();
    }

    if (MINOR_SHIPS) {
      CALL_ADDRESS[0] -= 88;
      CALL_ADDRESS[1] -= 93;
      CALL_ADDRESS[2] -= 118;
      // CALL_ADDRESS[3] = unused
      CALL_ADDRESS[4] -= 186;
      CALL_ADDRESS[5] -= 193;
    } else {
      CALL_ADDRESS[0] -= 46;
      CALL_ADDRESS[1] -= 51;
      CALL_ADDRESS[2] -= 76;
      CALL_ADDRESS[3] -= 119;
      CALL_ADDRESS[4] -= 175;
      CALL_ADDRESS[5] -= 182;
    }
  }

  public void shiftBuildingIndices(int addedIndex) throws IOException {
    ByteValueSegment bjmp = (ByteValueSegment) trek.getSegment(SD_SpShips.SpShpStrcJmp, true);
    switch (bjmp.byteValue()) {
      case JmpShort.JZ: {
        if (BUILDING >= addedIndex) {
          setSPBuilding(addedIndex + 1);
        }
        break;
      }
    }
  }

  public void removeBuildingIndex(int index) throws IOException {
    ByteValueSegment bjmp = (ByteValueSegment) trek.getSegment(SD_SpShips.SpShpStrcJmp, true);
    switch (bjmp.byteValue()) {
      case JmpShort.JZ: {
        if (BUILDING > index) {
          setSPBuilding(index - 1);
        } else if (getSPBuilding() == index) {
          setSPBuilding(255);
        }
        break;
      }
    }
  }

  public void switchedBuildingIndex(int idx1, int idx2) throws IOException {
    ByteValueSegment bjmp = (ByteValueSegment) trek.getSegment(SD_SpShips.SpShpStrcJmp, true);
    switch (bjmp.byteValue()) {
      case JmpShort.JZ: {
        if (BUILDING == idx1) {
          setSPBuilding(idx2);
        } else if (BUILDING == idx2) {
          setSPBuilding(idx1);
        }
        break;
      }
    }
  }

  @Override
  public void reset() {
    if (MINOR_SHIPS || MINOR_SHIPS_SIMPLE || !SPECIAL_SHIPS || skipRaceCheck) {
      MINOR_SHIPS = false;
      MINOR_SHIPS_SIMPLE = false;
      SPECIAL_SHIPS = true;
      skipRaceCheck = false;
      markChanged();
    }

    if (STARBASE_CLASS != Default_StarbaseClass) {
      STARBASE_CLASS = Default_StarbaseClass;
      markChanged();
    }

    if (BUILDING != Default_Building) {
      BUILDING = Default_Building;
      markChanged();
    }

    if (SP_RACE_MASK != Default_SPRaceMask) {
      SP_RACE_MASK = Default_SPRaceMask;
      markChanged();
    }

    for (int i = 0; i < CALL_ADDRESS.length; ++i) {
      if (CALL_ADDRESS[i] != Default_CallAddr[i]) {
        CALL_ADDRESS[i] = Default_CallAddr[i];
        markChanged();
      }
    }

    for (int i = 0; i < SP_SHIPS.length; ++i) {
      if (SP_SHIPS[i] != Default_SPShps[i]) {
        SP_SHIPS[i] = Default_SPShps[i];
        markChanged();
      }
    }

    for (int i = 0; i < SP_REG.length; ++i) {
      if (SP_REG[i] != Default_SPReg) {
        SP_REG[i] = Default_SPReg;
        markChanged();
      }
    }
  }
}
