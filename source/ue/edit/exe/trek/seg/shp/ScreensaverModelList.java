package ue.edit.exe.trek.seg.shp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Contains a list of ship models to be displayed in screensaver.
 */
public class ScreensaverModelList extends InternalSegment {

  public static final String FILENAME = CTrekSegments.ScreensaverModelList;
  public static final int SEGMENT_ADDRESS = 0x001809A8;
  public static final int DEFAULT_MAX_MODELS = 71;
  public static final int DEFAULT_SIZE = DEFAULT_MAX_MODELS * 8;

  public static final String[] Default_Models = new String[] {
    "t01", "t03", "t04", "t05", "t06", "t07", "t08", "t09",                                    // monster models
    "hc",  "hu",  "ht",  "hs",  "ho",  "hl1", "hl2", "hm1", "hm2", "hh1", "hh2", "hh3", "hh4", // federation models
    "fl1", "fl2", "fl3", "fm1", "fm2", "fh1", "fu",  "fs",  "fo",  "fc",                       // ferengi models
    "ku",  "kl1", "kl2", "km1", "km2", "km3", "kh1", "ks",  "ko",  "kt",                       // klingon models
    "cl1", "cl2", "cm1", "ch1", "ch2", "ch3", "cs",  "co",  "cu",  "ct",                       // cardassian models
    "ru",  "rl1", "rl2", "rm1", "rm2", "rh2", "rh1", "rs",  "ro",  "rc",                       // romulan models
    "m01", "m05", "m06", "m08", "m09", "m10", "m11", "m12", "m13", "m14"                       // minor models
  };

  public static final String FileSuffix = ".hob"; //$NON-NLS-1$

  private int MAX = DEFAULT_MAX_MODELS;
  private ArrayList<String> MODELS = new ArrayList<String>();

  /**
   * @param in  the InputStream to read from
   * @param max the maximum number of entries
   */
  public ScreensaverModelList() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, ScreensaverModelList.class));
  }

  @Override
  public boolean isDefault() {
    for (int i = 0; i < MODELS.size(); ++i) {
      String hob = Default_Models[i] + FileSuffix;
      if (!MODELS.get(i).equals(hob))
        return false;
    }
    return true;
  }

  public boolean set(String[] models) {
    if (models.length > MAX && MAX > 0)
      return false;

    MODELS.clear();

    for (String m : models) {
      if (!MODELS.contains(m))
        MODELS.add(m);
    }

    markChanged();
    return true;
  }

  public String get(int i) {
    return MODELS.get(i);
  }

  /**
   * @return the maximum allowed entries
   */
  public int getMax() {
    return MAX;
  }

  @Override
  public void reset() {
    for (int i = 0; i < MODELS.size(); ++i) {
      String hob = Default_Models[i] + FileSuffix;
      if (!MODELS.get(i).equals(hob)) {
        MODELS.set(i, hob);
        markChanged();
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    int slots = MAX >= 0 ? MAX : MODELS.size();
    return slots * 8;
  }

  @Override
  public void load(InputStream in) throws IOException {
    MODELS.clear();

    for (int i = 0; i < MAX && in.available() > 0; i++) {
      String m = StreamTools.readNullTerminatedBotfString(in, 8).toLowerCase();
      if (!MODELS.contains(m))
        MODELS.add(m);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (MODELS.size() < 1) {
      // write empty strings
      byte[] b = new byte[8];
      for (int i = 0; i < MAX; i++)
        out.write(b);
    } else {
      int index = 0;
      for (int i = 0; i < MAX; i++) {
        out.write(DataTools.toByte(MODELS.get(index++), 8, 7));
        if (index >= MODELS.size())
          index = 0;
      }
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < Default_Models.length; ++i) {
      // all names are aligned in 8 byte blocks
      String hob = Default_Models[i] + FileSuffix;
      out.write(DataTools.toByte(hob, 8, 7));
    }
  }

  @Override
  public void clear() {
    MODELS.clear();
    markChanged();
  }

  public int getNumberofEntries() {
    return MODELS.size();
  }

  public void add(String m) {
    if (!MODELS.contains(m))
      MODELS.add(m);
    markChanged();
  }

  public boolean remove(String string) {
    if (MODELS.size() < 2)
      return false;

    markChanged();
    return MODELS.remove(string);
  }
}
