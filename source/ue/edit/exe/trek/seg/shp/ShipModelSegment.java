package ue.edit.exe.trek.seg.shp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import lombok.val;
import lombok.var;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.seg.shp.data.ShipModelData;
import ue.edit.exe.trek.seg.shp.data.ShipModelEntry;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;

/**
 * Contains ship model data, such as number of phasers and model scale.
 */
public class ShipModelSegment extends InternalSegment {

  public static final String FILENAME = CTrekSegments.ShipModelSegment;
  public static final int SEGMENT_ADDRESS = 0x0018F7FC;

  public static final int DEFAULT_MAX_SHIPMODELS = 74;
  public static final int UNLIMITED_MAX_SHIPMODELS = -1;
  public static final int DEFAULT_SIZE = DEFAULT_MAX_SHIPMODELS * ShipModelEntry.SIZE;

  public static final ShipModelData[] Default_ShipModelData = new ShipModelData[] {
    // Cardassians
    new ShipModelData("CH1"), new ShipModelData("CH2"), new ShipModelData("CH3"),
    new ShipModelData("CM1"), new ShipModelData("CL1"), new ShipModelData("CL2"),
    new ShipModelData("CS"),  new ShipModelData("CO"),  new ShipModelData("CU"),
    new ShipModelData("CT"),
    // Ferengi
    new ShipModelData("FH1"), new ShipModelData("FM1"), new ShipModelData("FM2"),
    new ShipModelData("FL1"), new ShipModelData("FL2"), new ShipModelData("FL3"),
    new ShipModelData("FS"),  new ShipModelData("FO"),  new ShipModelData("FU"),
    new ShipModelData("FC"),
    // Federation
    // note, No. 6 is "HM1" duplicate
    new ShipModelData("HH1"), new ShipModelData("HH2"), new ShipModelData("HH3"),
    new ShipModelData("HH4"), new ShipModelData("HM1"), new ShipModelData("HM1"),
    new ShipModelData("HM2"), new ShipModelData("HL1"), new ShipModelData("HL2"),
    new ShipModelData("HU"),  new ShipModelData("HC"),  new ShipModelData("HT"),
    new ShipModelData("HS"),  new ShipModelData("HO"),
    // Klingons
    new ShipModelData("KH1"), new ShipModelData("KM1"), new ShipModelData("KM2"),
    new ShipModelData("KM3"), new ShipModelData("KL1"), new ShipModelData("KL2"),
    new ShipModelData("KS"),  new ShipModelData("KO"),  new ShipModelData("KU"),
    new ShipModelData("KT"),
    // Romulans
    new ShipModelData("RL1"), new ShipModelData("RL2"), new ShipModelData("RM1"),
    new ShipModelData("RM2"), new ShipModelData("RH1"), new ShipModelData("RH2"),
    new ShipModelData("RS"),  new ShipModelData("RO"),  new ShipModelData("RU"),
    new ShipModelData("RC"),
    // Minors
    new ShipModelData("M01"), new ShipModelData("M05"), new ShipModelData("M06"),
    new ShipModelData("M08"), new ShipModelData("M09"), new ShipModelData("M10"),
    new ShipModelData("M11"), new ShipModelData("M12"), new ShipModelData("M13"),
    new ShipModelData("M14"),
    // Monsters
    new ShipModelData("T01"), new ShipModelData("T02",
      1.0f, 1, new byte[] { (byte)192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }),
    new ShipModelData("T03"), new ShipModelData("T04"), new ShipModelData("T05"),
    new ShipModelData("T06"), new ShipModelData("T07"), new ShipModelData("T08"),
    new ShipModelData("T09"), new ShipModelData("TMP"),
  };

  private ArrayList<ShipModelEntry> DATA = new ArrayList<ShipModelEntry>();

  /**
   * @param address       location of this segment in file
   */
  public ShipModelSegment(Stbof stbof) {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, ShipModelSegment.class));
  }

  public ShipModelSegment(Stbof stbof, InputStream in, int maxShipModels) throws IOException {
    this(stbof);
    load(in, maxShipModels);
  }

  /**
   * Returns true is the entry is present in this list.
   */
  public boolean contains(String graphics) {
    for (ShipModelEntry entry : this.DATA) {
      if (entry.getGraphics().equals(graphics))
        return true;
    }
    return false;
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#check()
   */
  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < this.DATA.size(); i++)
      DATA.get(i).check(response);
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#getSize()
   */
  @Override
  public int getSize() {
    return 48 * this.DATA.size();
  }

  /**
   * Use this to read ship model data
   *
   * @param in  InputStream to read from
   * @throws IOException
   * @param maxShipModels maximum ship model entries to read
   */
  @Override
  public void load(InputStream in) throws IOException {
    load(in, DEFAULT_MAX_SHIPMODELS);
  }

  public void load(InputStream in, int maxShipModels) throws IOException {
    DATA.clear();

    // on max < 0 this should read until in.available <= 0
    int max = maxShipModels;
    while ((in.available() > 0) && (max != 0)) {
      val entry = new ShipModelEntry(this);
      entry.load(in);
      DATA.add(entry);
      max--;
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < DATA.size(); i++)
      DATA.get(i).save(out);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < Default_ShipModelData.length; i++)
      Default_ShipModelData[i].save(out);
  }

  @Override
  public void clear() {
    DATA.clear();
    markChanged();
  }

  @Override
  public boolean isDefault() {
    if (DATA.size() != Default_ShipModelData.length)
      return false;
    for (int i = 0; i < DATA.size(); ++i)
      if (!DATA.get(i).equals(Default_ShipModelData[i]))
        return false;
    return true;
  }

  /**
   * @return number of ship model data entries
   */
  public int getNumberOfEntries() {
    return DATA.size();
  }

  /**
   * @param i entry index
   * @return ship model data entry
   */
  public ShipModelEntry getEntry(int index) {
    checkIndex(index);
    return DATA.get(index);
  }

  /**
   * @param graphics graphics stirng
   * @return ship model data entry
   */
  public ShipModelEntry getEntry(String graphics) {
    for (int i = 0; i < DATA.size(); ++i) {
      var entry = DATA.get(i);
      if (entry.getGraphics().equalsIgnoreCase(graphics))
        return entry;
    }
    return null;
  }

  /**
   * @return the index
   */
  public int getIndex(String graphics) {
    for (int i = 0; i < DATA.size(); ++i) {
      var entry = DATA.get(i);
      if (entry.getGraphics().equalsIgnoreCase(graphics))
        return i;
    }
    return -1;
  }

  private void checkIndex(int index) {
    if ((index < 0) || (index >= this.DATA.size())) {
      throw new IndexOutOfBoundsException(Language.getString("ShipModelSegment.0")
        .replace("%1", Integer.toString(index))); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  public void addEntry(ShipModelEntry entry) {
    entry.setGraphics(entry.getGraphics().toUpperCase());
    DATA.add(entry);
    markChanged();
  }

  public boolean removeEntry(String entry) {
    entry = entry.toUpperCase();

    for (int i = 0; i < DATA.size(); i++) {
      if (DATA.get(i).getGraphics().equals(entry)) {
        DATA.remove(i);
        markChanged();
        return true;
      }
    }

    return false;
  }

  public void updateEntry(ShipModelEntry dest) {

    for (int i = 0; i < DATA.size(); i++) {
      if (DATA.get(i).getGraphics().equals(dest.getGraphics())) {
        DATA.set(i, dest);
        markChanged();
        return;
      }
    }
  }

  @Override
  public void reset() {
    if (isDefault())
      return;

    DATA.clear();
    DATA.ensureCapacity(Default_ShipModelData.length);

    for (val entry : Default_ShipModelData)
      DATA.add(new ShipModelEntry(this, entry));
    markChanged();
  }
}