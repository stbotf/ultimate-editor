package ue.edit.exe.trek.seg.shp.data;

import ue.edit.exe.trek.seg.shp.ShipModelSegment;

/**
 * A ship model entry used for setting ship phasers etc in trek.exe
 */
public class ShipModelEntry extends ShipModelData {
  private ShipModelSegment sms;

  // used on load
  public ShipModelEntry(ShipModelSegment sms) {
    this.sms = sms;
  }

  // used to copy defaults
  public ShipModelEntry(ShipModelSegment sms, ShipModelData entry) {
    super(entry);
    this.sms = sms;
  }

  public ShipModelEntry(ShipModelEntry entry) {
    super(entry);
    this.sms = entry.sms;
  }

  // used on export
  public ShipModelEntry(ShipModelEntry entry, String graphics) {
    this(entry);
    this.graphics = graphics;
  }

  @Override
  public boolean copyDataFrom(ShipModelData entry) {
    if (super.copyDataFrom(entry)) {
      sms.markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean setGraphics(String graphics) {
    if (super.setGraphics(graphics)) {
      sms.markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean setNumPhasers(int numPhasers) {
    if (super.setNumPhasers(numPhasers)) {
      sms.markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean setScale(float scale) {
    if (super.setScale(scale)) {
      sms.markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean setPhaserSlots(byte[] data) {
    if (super.setPhaserSlots(data)) {
      sms.markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean setUnknown1(byte[] data) {
    if (super.setUnknown1(data)) {
      sms.markChanged();
      return true;
    }
    return false;
  }

  @Override
  public boolean setUnknown2(byte[] data) {
    if (super.setUnknown2(data)) {
      sms.markChanged();
      return true;
    }
    return false;
  }
}
