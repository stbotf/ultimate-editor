package ue.edit.exe.trek.seg.shp.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Data;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * A ship model data entry used for setting ship phasers etc in trek.exe
 */
@Data
public class ShipModelData {
  public static final int     SIZE = 48;
  public static final int     Default_Phasers = 8;
  public static final float   Default_Scale = 1.0f;
  public static final byte[]  Default_Slots = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0 };

  protected String graphics;    //  4    04
  protected byte[] unknown1;    //  8    12
  protected int numPhasers;     //  4    16
  protected byte[] unknown2;    // 12    28
  protected float scale;        //  4    32
  protected byte[] phaserSlots; // 16    48 (total bytes)

  public ShipModelData() {
  }

  public ShipModelData(ShipModelData entry) {
    graphics = entry.graphics;
    unknown1 = Arrays.copyOf(entry.unknown1, 8);
    numPhasers = entry.numPhasers;
    unknown2 = Arrays.copyOf(entry.unknown2, 12);
    scale = entry.scale;
    phaserSlots = Arrays.copyOf(entry.phaserSlots, 16);
  }

  public ShipModelData(ShipModelData entry, String graphics) {
    this(entry);
    this.graphics = graphics;
  }

  public ShipModelData(String graphics) {
    this.graphics = graphics;
    this.unknown1 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
    this.numPhasers = Default_Phasers;
    this.unknown2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    this.scale = Default_Scale;
    this.phaserSlots = Arrays.copyOf(Default_Slots, 16);
  }

  public ShipModelData(String graphics, float scale, int numPhasers, byte[] phaserSlots) {
    this.graphics = graphics;
    this.unknown1 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
    this.numPhasers = numPhasers;
    this.unknown2 = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    this.scale = scale;
    this.phaserSlots = phaserSlots;
  }

  public boolean copyDataFrom(ShipModelData entry) {
    boolean changed = setUnknown1(entry.unknown1);
    changed |= setNumPhasers(entry.numPhasers);
    changed |= setUnknown2(entry.unknown2);
    changed |= setScale(entry.scale);
    changed |= setPhaserSlots(entry.phaserSlots);
    return changed;
  }

  public boolean setGraphics(String graphics) {
    if (this.graphics != graphics) {
      this.graphics = graphics;
      return true;
    }
    return false;
  }

  public boolean setNumPhasers(int numPhasers) {
    if (this.numPhasers != numPhasers) {
      this.numPhasers = numPhasers;
      return true;
    }
    return false;
  }

  public boolean setScale(float scale) {
    if (this.scale != scale) {
      this.scale = scale;
      return true;
    }
    return false;
  }

  public boolean setPhaserSlots(byte[] data) {
    if (!Arrays.equals(phaserSlots, data)) {
      phaserSlots = data != null ? Arrays.copyOf(data, 16) : null;
      return true;
    }
    return false;
  }

  public boolean setUnknown1(byte[] data) {
    if (!Arrays.equals(unknown1, data)) {
      unknown1 = data != null ? Arrays.copyOf(data, 8) : null;
      return true;
    }
    return false;
  }

  public boolean setUnknown2(byte[] data) {
    if (!Arrays.equals(unknown2, data)) {
      unknown2 = data != null ? Arrays.copyOf(data, 12) : null;
      return true;
    }
    return false;
  }

  public void check(Vector<String> response) {
    String str = Language.getString("ShipModelData.0") //$NON-NLS-1$
      .replace("%1", graphics); //$NON-NLS-1$

    if ((numPhasers > 16) || (numPhasers < 0)) {
      String msg = Language.getString("ShipModelData.1") //$NON-NLS-1$
        .replace("%1", Integer.toString(numPhasers)); //$NON-NLS-1$
      response.add(str.replace("%2", msg)); //$NON-NLS-1$
    }
    if (Float.compare(scale, 0.0f) < 0.01f) {
      String msg = Language.getString("ShipModelData.2"); //$NON-NLS-1$
      response.add(str.replace("%2", msg)); //$NON-NLS-1$
    }
  }

  /**
   * @param in in InputStream to read the data from
   */
  public void load(InputStream in) throws IOException {
    graphics = StreamTools.readNullTerminatedBotfString(in, 4);
    unknown1 = StreamTools.readBytes(in, 8);
    numPhasers = StreamTools.readInt(in, true);
    unknown2 = StreamTools.readBytes(in, 12);
    scale = StreamTools.readFloat(in, true);
    phaserSlots = StreamTools.readBytes(in, 16);
  }

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(graphics, 4, 3));
    out.write(unknown1);
    out.write(DataTools.toByte(numPhasers, true));
    out.write(unknown2);
    out.write(DataTools.toByte(scale, true));
    out.write(phaserSlots);
  }

  /**
   * @param d ShipModelData to compare to
   * @return true if the two ShipModelData objects are compatible - meaning they only differ in
   * graphics string
   */
  public boolean isCompatible(ShipModelData d) {
    return (d.numPhasers == numPhasers &&
      (Math.abs(d.scale - scale) < 0.05d) &&
      Arrays.equals(d.phaserSlots, phaserSlots));
  }
}
