package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.UE;
import ue.edit.exe.common.OpCode.JmpFlags;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.HexTools;
import ue.util.data.ID;
import ue.util.stream.StreamTools;

/**
 * Contains the first alien monster ship model ID.
 * All monsters following need to be in sequence with IDs equal to [previous ID]+1.
 */
public class AlienStartingID extends InternalSegment {

  public static final String ID1_FILENAME = CTrekSegments.AlienStartingId;
  public static final String ID2_FILENAME = CTrekSegments.AlienStartingId2;
  public static final String ID3_FILENAME = CTrekSegments.AlienStartingId3;
  public static final String ID4_FILENAME = CTrekSegments.AlienStartingId4;
  public static final int ID1_SEGMENT_ADDRESS = 0x0004F355; // sub   eax, 73h
  public static final int ID2_SEGMENT_ADDRESS = 0x00049FC3; // sub   eax, 73h
  public static final int ID3_SEGMENT_ADDRESS = 0x00049DA3; // sub   eax, 73h
  public static final int ID4_SEGMENT_ADDRESS = 0x0004E876; // sub   eax, 73h
  public static final int SIZE = 13;

  public static final String DESC = "The first alien monster ship model ID."
    + " All monsters following need to be in sequence with IDs equal to [previous ID]+1."
    + "<br>When the signed single byte limit of 127 is exceeded,"
    + " the ship model count check is removed to patch the code for dword values."
    + "<br>When on load the patched ID was split to two sub &lt;byte&gt; instructions,"
    + " it sneakingly is patched to write a single sub &lt;int&gt; instruction.";

  public static final int Default_AlienStartID = 115;

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // code & instructions
  private static final byte[] SUB_SHORT = new byte[]{(byte)0x83, (byte)0xE8};
  private static final byte   SUB_DWORD = 0x2D;
  private static final byte[] CMP = new byte[]{0x66, 0x3D, 9, 0}; // cmp ax, 9
  private static final byte   NOP = (byte) 0x90;
  // JMP values for the different IDs
  private static final byte[] JMP1 = new byte[]{0x76, 0x32, (byte)0x89, (byte)0xEB, (byte)0x89, (byte) 0xFA};
  private static final byte[] JMP2 = new byte[]{0x0F, (byte)0x86, 0x2F, 1, 0, 0};
  private static final byte[] JMP3 = new byte[]{0x0F, (byte)0x86, (byte)0xF5, 0, 0, 0};
  private static final byte[] JMP4 = new byte[]{0x0F, (byte)0x87, (byte)0xD3, 0, 0, 0};

  // first monster ship model id
  private int ID;
  private byte[] JMP = new byte[6];

  public AlienStartingID(int address) {
    super(new SegmentDefinition(address, null, AlienStartingID.class).desc(DESC));
  }

  @Override
  public void check(Vector<String> response) {
    if (ID < NUM_EMPIRES) {
      String msg = Language.getString("AlienStartingID.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Long.toString(address())); //$NON-NLS-1$
      msg = msg.replace("%2", "5"); //$NON-NLS-1$ //$NON-NLS-2$
      response.add(msg);
    }

    // compare value with the one in shiplist.sst
    Stbof stbof = UE.FILES.stbof();
    if (stbof != null) {
      try {
        ShipList sl = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
        ID[] id = sl.getIDs(Race.Monster);

        if (ID != id[0].ID) {
          String msg = Language.getString("AlienStartingID.2"); //$NON-NLS-1$
          msg = msg.replace("%1", Long.toString(address())); //$NON-NLS-1$
          msg = msg.replace("%2", Integer.toString(ID)); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(id[0].ID)); //$NON-NLS-1$
          response.add(msg);
        }
      } catch (Exception e) {
        e.printStackTrace();
        response.add(e.getLocalizedMessage());
      }
    }
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  /**
   * @param in  InputStream to read from
   * @throws IOException
   */
  @Override
  public void load(InputStream in) throws IOException {
    byte[] b = StreamTools.readBytes(in, 7);

    if (b[0] == SUB_DWORD) {
      // read new patch where the ID is changed to a dword value
      ID = DataTools.toInt(b, 1, true);
      validate(b[5], NOP);
      validate(b[6], NOP);
    } else if (check(b, 3, 2, SUB_SHORT)) {
      // read old patch where the ID was split in half
      // see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=5220#p5220
      validate(b, 0, 2, SUB_SHORT);
      ID = b[2] + b[5];
      validate(b[6], NOP);
    } else {
      // read orig
      validate(b, 0, 2, SUB_SHORT); // 2
      ID = b[2];                    // 1
      // validate cmp ax, 9
      validate(b, 3, CMP);          // 4
    }

    StreamTools.read(in, JMP);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (ID > 127) {
      // enable more than <max signed> 127 ships
      // and in exchange disable the id check
      out.write(SUB_DWORD);                   // 1
      out.write(DataTools.toByte(ID, true));  // 4

      // nop remaining 2 bytes of the 4 byte compare instruction
      out.write(NOP);                         // 1
      out.write(NOP);                         // 1

      // since the compare instruction is overwritten,
      // disable the ship model count checks
      if (JMP[0] == JmpShort.JBE) {
        // for ID 1 always jump to not abort load
        JMP[0] = JmpShort.JMP;
      } else if (JMP[0] == 0x0F) {
        // check 2 byte jmp loc_ instruction
        short j = DataTools.toShort(JMP, false);
        if (j == JmpFlags.JBE) {
          // for ID 2 and 3 always jump to not abort load
          JMP[0] = NOP;
          JMP[1] = (byte) 0xE9;
        } else if (j == JmpFlags.JA) {
          // for ID 4 always skip to proceed load
          Arrays.fill(JMP, NOP);
        }
      }

      out.write(JMP);                         // 6
    } else {
      // keep the loaded jmp instruction
      // no matter whether it is patched or not
      out.write(SUB_SHORT);                   // 2
      out.write(ID);                          // 1
      out.write(CMP);                         // 4
      out.write(JMP);                         // 6
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(Default_AlienStartID);
    out.write(CMP);
    out.write(getDefaultJmp());
  }

  @Override
  public void clear() {
    ID = 0;
    Arrays.fill(JMP, (byte)0);
    markChanged();
  }

  @Override
  public boolean isDefault() {
    return ID == Default_AlienStartID;
  }

  /**
   * Sets first ID. 5-255
   */
  public void setID(short id) throws InvalidArgumentsException {
    if ((id < NUM_EMPIRES) || (id > 255)) {
      String err = Language.getString("AlienStartingID.1"); //$NON-NLS-1$
      throw new InvalidArgumentsException(err);
    }

    ID = id;
    markChanged();
  }

  /**
   * Returns the first ID.
   */
  public int getID() {
    return ID;
  }

  @Override
  public void reset() {
    if (ID != Default_AlienStartID) {
      ID = Default_AlienStartID;
      markChanged();
    }
  }

  private byte[] getDefaultJmp() throws IOException {
    switch(address) {
      case ID1_SEGMENT_ADDRESS:
        return JMP1;
      case ID2_SEGMENT_ADDRESS:
        return JMP2;
      case ID3_SEGMENT_ADDRESS:
        return JMP3;
      case ID4_SEGMENT_ADDRESS:
        return JMP4;
      default:
        throw new IOException("Invalid AlienStartID address: 0x" + HexTools.toHex(address));
    }
  }
}
