package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Reads the list of addresses that point to race names in trek.exe
 *
 * @author Alan Podlesek
 */
public class RaceListAddresses extends InternalSegment {

  public static final String FILENAME = CTrekSegments.RaceListAddresses;
  public static final int SEGMENT_ADDRESS = 0x00189440;
  public static final int NUM_RACES = CStbof.NUM_RACES;
  public static final int DEFAULT_SIZE = NUM_RACES * Integer.BYTES;

  public static final String DESC = "Auto-updated address list of the race names listed by the 'raceList' segment.";

  private static final int[] Default_RaceNameAddr = new int[] {
    0x5768E8, // off_58B640 "Cardassians"
    0x5768F4, // off_58B644 "Federation"
    0x576900, // off_58B648 "Ferengi"
    0x576908, // off_58B64C "Klingons"
    0x576914, // off_58B650 "Romulans"
    0x576920, // off_58B654 "Acamarians"
    0x57692C, // off_58B658 "Andorians"
    0x576938, // off_58B65C "Angosians"
    0x576944, // off_58B660 "Antedeans"
    0x576950, // off_58B664 "Anticans"
    0x57695C, // off_58B668 "Bajorans"
    0x576968, // off_58B66C "Bandi"
    0x576970, // off_58B670 "Benzites"
    0x57697C, // off_58B674 "Betazoids"
    0x576988, // off_58B678 "Bolians"
    0x576990, // off_58B67C "Bynars"
    0x576998, // off_58B680 "Caldonians"
    0x5769A4, // off_58B684 "Chalnoth"
    0x5769B0, // off_58B688 "Edos"
    0x5769B8, // off_58B68C "Ktarians"
    0x5769C4, // off_58B690 "Malcorians"
    0x5769D0, // off_58B694 "Mintakans"
    0x5769DC, // off_58B698 "Mizarians"
    0x5769E8, // off_58B69C "Nausicaans"
    0x5769F4, // off_58B6A0 "Pakleds"
    0x5769FC, // off_58B6A4 "Selay"
    0x576A04, // off_58B6A8 "Sheliak"
    0x576A0C, // off_58B6AC "Takarans"
    0x576A18, // off_58B6B0 "Talarians"
    0x576A24, // off_58B6B4 "Tamarians"
    0x576A30, // off_58B6B8 "Trill"
    0x576A38, // off_58B6BC "Ullians"
    0x576A40, // off_58B6C0 "Vulcans"
    0x576A48, // off_58B6C4 "Yridians"
    0x576A54, // off_58B6C8 "Zakdorn"
  };

  private ArrayList<Integer> addresses = new ArrayList<Integer>();

  public RaceListAddresses() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, RaceListAddresses.class).desc(DESC));
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return addresses.size() * Integer.BYTES;
  }

  /**
   * @param in  the InputStream to read from
   * @throws IOException
   */
  @Override
  public void load(InputStream in) throws IOException {
    load(in, NUM_RACES);
  }

  /**
   * @param in            the InputStream to read from
  // @param maxAddresses  number of addresses to read, if < 0 read to the end of stream
   * @throws IOException
   */
  public void load(InputStream in, int maxAddresses) throws IOException {
    addresses.clear();

    while (maxAddresses != 0 && in.available() > 0) {
      addresses.add(StreamTools.readInt(in, true));
      maxAddresses--;
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < addresses.size(); i++)
      out.write(DataTools.toByte(addresses.get(i), true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < Default_RaceNameAddr.length; i++)
      out.write(DataTools.toByte(Default_RaceNameAddr[i], true));
  }

  @Override
  public void clear() {
    addresses.clear();
    markChanged();
  }

  @Override
  public boolean isDefault() {
    for (int r = 0; r < NUM_RACES; ++r)
      if (addresses.get(r) != Default_RaceNameAddr[r])
        return false;
    return true;
  }

  /**
   * @param index   address index
   * @param address
   */
  public void setRaceAddress(int index, int address) {
    int prev = addresses.set(index, address);
    if (prev != address)
      markChanged();
  }

  /**
   * @param index address index
   * @return
   */
  public int getRaceAddress(int index) {
    return addresses.get(index);
  }

  @Override
  public void reset() {
    for (int r = 0; r < NUM_RACES; ++r) {
      if (addresses.get(r) != Default_RaceNameAddr[r]) {
        addresses.set(r, Default_RaceNameAddr[r]);
        markChanged();
      }
    }
  }
}
