package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.EqualsAndHashCode;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * AI ship sets
 * @author Alan Podlesek
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=218&t=707
 */
public class AIShpSets extends InternalSegment {

  public static final String FILENAME = CTrekSegments.AIShipSets;
  public static final int SEGMENT_ADDRESS = 0x18a0c0;
  public static final int SIZE = 12960; // 8*5*9*36 = 12960

  public static final String DESC = "Up to 36 ship build tasks per agenda per empire."
    + " Followed by a last fallback agenda not editable by UE."
    + "<br>Agendas include: normal expansion, unopposed expansion, normal consolidation,"
    + " defensive consolidation, offensive consolidation, cold war,"
    + " defensive war, offensive war and jihad"
    + "<p>Note that in unmodded vanilla the last 4 cardassian entries errornously are zeroed"
    + " to scout ship tasks. As long they persist, the segment is flagged to be patched.";

  private static final int NUM_AIAGENDAS = CTrek.NUM_AIAGENDAS;
  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_SLOTS = 36;

  private ShpBldTask[][][] buildItems = new ShpBldTask[NUM_EMPIRES][NUM_AIAGENDAS][NUM_SLOTS];

  public AIShpSets() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, AIShpSets.class).desc(DESC));
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int emp = 0; emp < NUM_EMPIRES; emp++)
      for (int agenda = 0; agenda < NUM_AIAGENDAS; agenda++)
        for (int item = 0; item < 36; item++)
          buildItems[emp][agenda][item] = new ShpBldTask(in);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int emp = 0; emp < NUM_EMPIRES; emp++)
      for (int agenda = 0; agenda < NUM_AIAGENDAS; agenda++)
        for (int item = 0; item < 36; item++)
          buildItems[emp][agenda][item].save(out);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int agenda = 0; agenda < NUM_AIAGENDAS; agenda++) {
        for (int item = 0; item < 36; item++) {
          val def = CShpBldTasks.getDefault(emp, agenda, item);
          def.save(out);
        }
      }
    }
  }

  @Override
  public void clear() {
    for (int emp = 0; emp < NUM_EMPIRES; emp++)
      for (int agenda = 0; agenda < NUM_AIAGENDAS; agenda++)
        for (int item = 0; item < 36; item++)
          buildItems[emp][agenda][item] = null;
    markChanged();
  }

  /**
   * Sets ship type id
   *
   * @param empire (0-4)
   * @param agenda (0-9)
   * @param index  (0-35)
   * @param type   (0-5 or 9)
   */
  public boolean setShipType(int empire, int agenda, int index, short type) {
    if (buildItems[empire][agenda][index].shipRole != type) {
      buildItems[empire][agenda][index].shipRole = type;
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Returns ship type.
   *
   * @param empire (0-4)
   * @param agenda (0-9)
   * @param index  (0-35)
   * @return
   */
  public int getShipType(int empire, int agenda, int index) {
    return buildItems[empire][agenda][index].shipRole;
  }

  @Override
  public boolean isDefault() {
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int agenda = 0; agenda < NUM_AIAGENDAS; agenda++) {
        for (int item = 0; item < 36; item++) {
          val def = CShpBldTasks.getDefault(emp, agenda, item);
          val task = buildItems[emp][agenda][item];
          if (!task.equals(def))
            return false;
        }
      }
    }
    return true;
  }

  @Override
  public void reset() {
    if (isDefault())
      return;

    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int agenda = 0; agenda < NUM_AIAGENDAS; agenda++) {
        for (int item = 0; item < 36; item++) {
          val task = CShpBldTasks.getDefault(emp, agenda, item);
          buildItems[emp][agenda][item] = new ShpBldTask(task);
        }
      }
    }
    markChanged();
  }

  @EqualsAndHashCode
  public static class ShpBldTask {
    short shipRole;
    short unk1 = 0;
    int repeat = 0;

    public ShpBldTask(short role) {
      this.shipRole = role;
    }

    public ShpBldTask(short role, int repeat) {
      this.shipRole = role;
      this.repeat = repeat;
    }

    public ShpBldTask(ShpBldTask task) {
      this.shipRole = task.shipRole;
      this.unk1 = task.unk1;
      this.repeat = task.repeat;
    }

    public ShpBldTask(InputStream in) throws IOException {
      shipRole = StreamTools.readShort(in, true);
      unk1 = StreamTools.readShort(in, true);
      repeat = StreamTools.readInt(in, true);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(shipRole, true));
      out.write(DataTools.toByte(unk1, true));
      out.write(DataTools.toByte(repeat, true));
    }
  }
}
