package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Contains map range tech bonus values.
 */
public class MapRangeTechBonuses extends InternalSegment {

  public static final String FILENAME = CTrekSegments.MapRangeBonuses;
  public static final int SEGMENT_ADDRESS = 0x0018E880;

  public static final String DESC = "Range bonus values for the first 11 tech levels per empire."
    + "<br>For some reason unmodded vanilla trek.exe has a glitch on the cardassian tech 8 short range bonus."
    + " Since it is an obvious typo, it is flagged to be patched as long the glitch is not fixed.";

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_RANGES = 3;
  private static final int NUM_TECHLVLS = CStbof.DEFAULT_NUM_TECHLVLS;
  private static final int NUM_BONUSES = NUM_EMPIRES * NUM_TECHLVLS * NUM_RANGES;

  public static final int SHORT_RANGE = 0;
  public static final int MEDIUM_RANGE = 1;
  public static final int LONG_RANGE = 2;
  public static final int SIZE = NUM_BONUSES * Integer.BYTES;

  // 5 empires * 3 ranges * 11 tech levels
  // note, tech levels and ranges are switched for better readability
  public static final int[][][] Default_MapRangeBonuses = new int[][][] {
    {    // Cardassians
      // note that tech 8 short range in vanilla is below tech 7,
      // which is a bug and fixed for the defaults
      { 0, 0, 0, 1, 1, 1, 2, 2 /*1*/, 2, 3, 3 },
      { 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3 },
      { 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3 },
    }, { // Federation
      { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3 },
      { 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3 },
      { 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3 },
    }, { // Ferengi
      { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3 },
      { 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3 },
      { 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3 },
    }, { // Klingons
      { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3 },
      { 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3 },
      { 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3 },
    }, { // Romulans
      { 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3 },
      { 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3 },
      { 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3 },
    },
  };

  private Trek trek;

  // 5 empire * 11 tech lvls * 3 ranges
  private int[] bonus = new int[NUM_BONUSES];

  public MapRangeTechBonuses(Trek trek) {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, MapRangeTechBonuses.class).desc(DESC));
    this.trek = trek;
  }

  @Override
  public boolean isDefault() {
    for (int emp = 0; emp < NUM_EMPIRES; emp++)
      for (int lvl = 0; lvl < NUM_TECHLVLS; lvl++)
        for (int r = 0; r < NUM_RANGES; r++)
          if (bonus[emp * NUM_RANGES * NUM_TECHLVLS + lvl * NUM_RANGES + r]
            != Default_MapRangeBonuses[emp][r][lvl])
            return false;
    return true;
  }

  // level starting at 0
  public int getRangeBonus(int level, int emp, int range) {
    return bonus[emp * NUM_RANGES * NUM_TECHLVLS + level * NUM_RANGES + range];
  }

  // level starting at 0
  public void setRangeBonus(int level, int emp, int range, int value) {
    int i = emp * NUM_RANGES * NUM_TECHLVLS + level * NUM_RANGES + range;

    if (bonus[i] != value) {
      bonus[i] = value;
      markChanged();
    }
  }

  @Override
  public void reset() {
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int lvl = 0; lvl < NUM_TECHLVLS; lvl++) {
        for (int r = 0; r < NUM_RANGES; r++) {
          int def = Default_MapRangeBonuses[emp][r][lvl];
          setRangeBonus(lvl, emp, r, def);
        }
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
    try {
      BaseMapRanges base = (BaseMapRanges) trek.getInternalFile(CTrekSegments.BaseMapRanges, true);
      int[] last = new int[NUM_RANGES];

      for (int empire = 0; empire < NUM_EMPIRES; empire++) {
        last[0] = last[1] = last[2] = Integer.MIN_VALUE;

        for (int lvl = 0; lvl < NUM_TECHLVLS; lvl++) {
          for (int r = 0; r < NUM_RANGES; r++) {
            int range = base.getBaseRange(empire, r) + getRangeBonus(lvl, empire, r);

            if (range <= 0) {
              String msg = Language.getString("MapRangeTechBonuses.0"); //$NON-NLS-1$
              msg = msg.replace("%2", LexHelper.mapShipRange(r)); //$NON-NLS-1$
              msg = msg.replace("%3", LexHelper.mapOwner(empire)); //$NON-NLS-1$
              msg = msg.replace("%4", Integer.toString(lvl+1)); //$NON-NLS-1$
              response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
            } else if (range < last[r]) {
              String msg = Language.getString("MapRangeTechBonuses.1"); //$NON-NLS-1$
              msg = msg.replace("%2", LexHelper.mapShipRange(r)); //$NON-NLS-1$
              msg = msg.replace("%3", LexHelper.mapOwner(empire)); //$NON-NLS-1$
              msg = msg.replace("%4", Integer.toString(lvl+1)); //$NON-NLS-1$
              msg = msg.replace("%5", Integer.toString(lvl)); //$NON-NLS-1$
              msg = msg.replace("%6", Integer.toString(range)); //$NON-NLS-1$
              msg = msg.replace("%7", Integer.toString(last[r])); //$NON-NLS-1$
              response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
            }

            last[r] = range;
          }
        }
      }
    } catch (Exception e) {
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, e.getLocalizedMessage()));
    }
  }

  @Override
  public int getSize() {
    return bonus.length * Integer.BYTES;
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int i = 0; i < bonus.length; i++) {
      bonus[i] = StreamTools.readInt(in, true);
    }
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < bonus.length; i++) {
      out.write(DataTools.toByte(bonus[i], true));
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int lvl = 0; lvl < NUM_TECHLVLS; lvl++) {
        for (int r = 0; r < NUM_RANGES; r++) {
          int value = Default_MapRangeBonuses[emp][r][lvl];
          out.write(DataTools.toByte(value, true));
        }
      }
    }
  }

  @Override
  public void clear() {
    Arrays.fill(bonus, 0);
    markChanged();
  }
}
