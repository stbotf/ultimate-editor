package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * List of races stored in trek.exe
 */
public class RaceList extends InternalSegment {

  public static final String FILENAME = CTrekSegments.RaceList;
  public static final int SEGMENT_ADDRESS = 0x001746e8;
  public static final int NUM_RACES = CStbof.NUM_RACES;
  public static final int SIZE = 372;

  public static final String DESC = "This segment lists all the race names,"
    + " each terminated by a null character, and automatically updates the 'racelistAddresses' segment."
    + "<br>Since the names are partially looked up in race.rst from stbof.res instead,"
    + " on name change both lists must be updated."
    + "<br>To not waste segment space, it sneaks in to strip the partially applied"
    + " 12 byte alignment of unmodified 'vanilla' names.";

  // default alignment is 4 byte blocks with null characters
  private static final String[] Default_RaceNames = new String[] {
    "Cardassians", "Federation", "Ferengi",   "Klingons",   "Romulans",
    "Acamarians",  "Andorians",  "Angosians", "Antedeans",  "Anticans",
    "Bajorans",    "Bandi",      "Benzites",  "Betazoids",  "Bolians",
    "Bynars",      "Caldonians", "Chalnoth",  "Edos",       "Ktarians",
    "Malcorians",  "Mintakans",  "Mizarians", "Nausicaans", "Pakleds",
    "Selay",       "Sheliak",    "Takarans",  "Talarians",  "Tamarians",
    "Trill",       "Ullians",    "Vulcans",   "Yridians",   "Zakdorn"
  };

  private static final int my_offset = 0x5768E8;

  private RaceListAddresses addresses;

  private String[] race = new String[35];
  private int data_length = 0;
  private boolean isDwordAligned = false;

  public RaceList(Trek exe) throws IOException{
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, RaceList.class).desc(DESC));
    addresses = (RaceListAddresses) exe.getInternalFile(CTrekSegments.RaceListAddresses, true);
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] b = StreamTools.readBytes(in, SIZE);
    isDwordAligned = true;

    // now get strings
    int offset = 0;
    int begin = 0;
    int expected = 0;
    for (int i = 0; i < NUM_RACES; i++) {
      begin = addresses.getRaceAddress(i) - my_offset;

      if (begin < 0 || begin > 371) {
        race[i] = "";
      } else {
        race[i] = DataTools.fromNullTerminatedBotfString(b, begin, b.length - begin);
        data_length += race[i].length();
      }

      // check for unmodified vanilla binary data gaps
      if (isDwordAligned) {
        if (begin != expected)
          isDwordAligned = false;
        else
          expected += race[i].length() + dwordSpacing(race[i].length()+1);
      }

      // sneak in to remove any data gaps
      addresses.setRaceAddress(i, my_offset + offset);
      offset += race[i].length() + 1;
      data_length++;
    }

    // mark saved to sneak in changes
    addresses.markSaved();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < NUM_RACES; i++) {
      String name = race[i];
      out.write(DataTools.toByte(name, name.length() + 1, name.length()));
    }
    out.write(new byte[SIZE - data_length]);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < NUM_RACES; i++) {
      String name = Default_RaceNames[i];
      out.write(DataTools.toByte(name, name.length() + 1, name.length()));
      // by default the names are aligned in 4 byte blocks with null characters
      int spacing = dwordSpacing(name.length()+1);
      for (; spacing != 0; --spacing)
        out.write(0);
    }
  }

  @Override
  public void clear() {
    Arrays.fill(race, null);
    data_length = 0;
    markChanged();
  }

  @Override
  public boolean isDefault() {
    if (!isDwordAligned)
      return false;
    for (int r = 0; r < NUM_RACES; ++r)
      if (!race[r].equals(Default_RaceNames[r]))
        return false;
    return true;
  }

  /**
   * @param i race id (0 - 35)
   * @param r race string
   * @return true if the race was set
   */
  public boolean setRaceString(int i, String r) {
    if (i < 0 || i > 34)
      return false;
    if (race[i].equals(r))
      return false;

    int dif = r.length() - race[i].length();
    if (data_length + dif > 372) {
      r = r.substring(0, 372 - data_length - dif);
      dif = r.length() - race[i].length();
    }

    data_length += dif;
    race[i] = r;

    for (int j = i + 1; j < 35; j++) {
      addresses.setRaceAddress(j, addresses.getRaceAddress(j) + dif);
    }

    markChanged();
    return true;
  }

  /**
   * If this returns -1 then botf won't identify the race either.
   *
   * @param r race string
   * @return race id or -1 if not found
   */
  public int getRaceIndex(String r) {
    for (int i = 34; i >= 0; i--) {
      if (race[i].equals(r)) {
        return i;
      }
    }
    return -1;
  }

  /**
   * @param i race id
   * @return null or race string
   */
  public String getRaceString(int i) {
    return i >= 0 && i < 35 ? race[i] : null;
  }

  @Override
  public void reset() {
    for (int r = 0; r < NUM_RACES; ++r) {
      if (!race[r].equals(Default_RaceNames[r])) {
        race[r] = Default_RaceNames[r];
        markChanged();
      }
    }
  }

  private int dwordSpacing(int len) {
    return 3 & (4 - len % 4);
  }
}
