package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.common.CommonStrings;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.service.Language;

/**
 * Empire image prefixes
 */
public class EmpireImgPrefixes extends InternalSegment {

  public static final String FILENAME = CTrekSegments.EmpireImagePrefixes;
  public static final int SEGMENT_ADDRESS = 0x00190A4C;
  public static final int SIZE = 5;

  public static final char[] Default_Prefixes = new char[] {'C', 'H', 'F', 'K', 'R'};

  private char[] PREFIX = new char[SIZE];

  public EmpireImgPrefixes() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, EmpireImgPrefixes.class));
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int i = 0; i < PREFIX.length; ++i)
      PREFIX[i] = (char) in.read();
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < PREFIX.length; ++i)
      out.write(PREFIX[i]);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < Default_Prefixes.length; ++i)
      out.write(Default_Prefixes[i]);
  }

  @Override
  public void clear() {
    Arrays.fill(PREFIX, (char)0);
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < PREFIX.length; i++) {
      if (!Character.isLetterOrDigit(PREFIX[i])) {
        String msg = Language.getString("EmpireImgPrefixes.1");
        msg = msg.replace("%1", "0x" + Integer.toHexString(PREFIX[i]));
        response.add(msg);
      }
    }
  }

  @Override
  public boolean isDefault() {
    for (int i = 0; i < PREFIX.length; ++i) {
      if (PREFIX[i] != Default_Prefixes[i])
        return false;
    }
    return true;
  }

  public char getEmpirePrefix(int empire_index) {
    checkIndex(empire_index);
    return PREFIX[empire_index];
  }

  public void setEmpirePrefix(int empire_index, char prefix) {
    checkIndex(empire_index);

    if (PREFIX[empire_index] != prefix) {
      PREFIX[empire_index] = prefix;
      markChanged();
    }
  }

  @Override
  public void reset() {
    for (int i = 0; i < PREFIX.length; ++i) {
      if (PREFIX[i] != Default_Prefixes[i]) {
        PREFIX[i] = Default_Prefixes[i];
        markChanged();
      }
    }
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= PREFIX.length) {
      String err = CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(index));
      throw new IndexOutOfBoundsException(err);
    }
  }

  @Override
  public int getSize() {
    return SIZE;
  }
}
