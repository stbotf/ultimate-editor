package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Contains base map range values.
 */
public class BaseMapRanges extends InternalSegment {

  public static final String FILENAME = CTrekSegments.BaseMapRanges;
  public static final int SEGMENT_ADDRESS = 0x0018E62C;
  public static final int SIZE = 15 * Integer.BYTES;

  public static final int SHORT_RANGE = 0;
  public static final int MEDIUM_RANGE = 1;
  public static final int LONG_RANGE = 2;

  // Card, Fed, Ferg, Klng, Rom / short, medium, long range
  public static final int[] Default_Ranges = new int[] {
    1, 2, 3,
    1, 2, 4,
    1, 2, 4,
    1, 2, 3,
    1, 2, 3
  };

  // 0 <= empire <= 4
  // 0 <= range <= 2
  private int[] base = new int[15];   // [empire * 3 + range]

  public BaseMapRanges() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, BaseMapRanges.class));
  }

  @Override
  public boolean isDefault() {
    for (int i = 0; i < base.length; ++i)
      if (base[i] != Default_Ranges[i])
        return false;
    return true;
  }

  public int getBaseRange(int race, int range) {
    return base[race * 3 + range];
  }

  public void setBaseRange(int race, int range, int value) {
    int i = race * 3 + range;
    if (base[i] != value) {
      base[i] = value;
      markChanged();
    }
  }

  @Override
  public void reset() {
    for (int i = 0; i < base.length; ++i) {
      if (base[i] != Default_Ranges[i]) {
        base[i] = Default_Ranges[i];
        markChanged();
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < base.length; i++) {
      if (base[i] <= 0) {
        String msg = Language.getString("BaseMapRanges.0"); //$NON-NLS-1$
        msg = msg.replace("%1", Long.toString(address())); //$NON-NLS-1$
        int rc = i % 3;
        int rn = i - rc * 3;
        msg = msg.replace("%2", Integer.toString(rn)); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(rc)); //$NON-NLS-1$
        response.add(msg);
      }
    }
  }

  @Override
  public int getSize() {
    return base.length * Integer.BYTES;
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int i = 0; i < base.length; ++i)
      base[i] = StreamTools.readInt(in, true);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < base.length; ++i)
      out.write(DataTools.toByte(base[i], true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < Default_Ranges.length; ++i)
      out.write(DataTools.toByte(Default_Ranges[i], true));
  }

  @Override
  public void clear() {
    base = new int[15];
    markChanged();
  }
}
