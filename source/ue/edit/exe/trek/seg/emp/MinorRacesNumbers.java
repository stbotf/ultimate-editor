package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.tools.TrekTools;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Min and max numbers of minor races
 * @author Alan Podlesek
 */
public class MinorRacesNumbers extends InternalSegment {

  public static final String MIN_MINORS_FILENAME = CTrekSegments.MinMinorsNumber;
  public static final String MAX_MINORS_FILENAME = CTrekSegments.MaxMinorsNumber;
  public static final int MIN_MINORS_SEGMENT_ADDRESS = 0x000B1720; // min number of minor races
  public static final int MAX_MINORS_SEGMENT_ADDRESS = 0x000B1670; // max number of minor races
  public static final int SIZE = 172;

  public static final String DESC = "This segment allows to set the min/max minors "
    + "for the nine minor amount and galaxy size combinations."
    + "<br>To allow configure all settings independently, it sneaks in a minor code patch"
    + " for the medium/few setting to not share code of:"
    + "<br>max: small/many, min: small/some.";

  // 3 galaxy sizes * 3 minor amount settings (few, some, many)
  // note, one is subtracted by GalacticMapGUI
  public static final int[][] Default_MaxMinors = new int[][] {
    // small galaxy
    {  3,  5,  8 },
    // medium galaxy
    {  8, 15, 21 },
    // large galaxy
    { 12, 24, 30 },
  };

  // 3 galaxy sizes * 3 minor amount settings (few, some, many)
  // note, one is subtracted by GalacticMapGUI
  public static final int[][] Default_MinMinors = new int[][] {
    // small galaxy
    {  1,  3,  5 },
    // medium galaxy
    {  3,  9, 15 },
    // large galaxy
    {  6, 12, 24 },
  };

  private static final byte[] code_block1 = new byte[] {
    0x51, (byte)0x89, (byte)0xC1, (byte)0x89, (byte)0xD0, (byte)0xC1, (byte)0xE2, 0x02,
    (byte)0x83, (byte)0xF9, 0x01, 0x73, 0x13, (byte)0x85, (byte)0xC9, 0x75, 0x0B,
    (byte)0x83, (byte)0xF8, 0x03, 0x77, 0x06, (byte)0xFF, (byte)0xA2
  };

  private static final byte[] code_block2 = new byte[] {
    0x31, (byte)0xC0, 0x59, (byte)0xC3, 0x77, 0x0B, (byte)0x83, (byte)0xF8,
    0x03, 0x77, (byte)0xF5, (byte)0xFF, (byte)0xA2
  };

  private static final byte[] code_block3 = new byte[] {
    (byte)0x83, (byte)0xF9, 0x02, 0x75, (byte)0xEA, (byte)0x83, (byte)0xF8,
    0x03, 0x77, (byte)0xE5, (byte)0xFF, (byte)0xA2
  };

  private static final int mov_num = 0xB8;

  private static final byte[] pop_and_retn = new byte[] {
    0x59, (byte)0xC3
  };

  // 'None' is a non-editable xor instruction and therefore skipped
  private static final int NUM_EDITABLE_MINOR_SETTINGS = CStbof.NUM_MINOR_SETTINGS-1;
  private static final int NUM_GAL_SIZES = CStbof.NUM_GAL_SIZES;
  private static final int NUM_MINOR_SETTINGS = CStbof.NUM_MINOR_SETTINGS;
  private static final int NUM_SETTINGS = NUM_GAL_SIZES * NUM_EDITABLE_MINOR_SETTINGS;

  // max offsets / min offsets
  // loc_4B22BC  / loc_4B236C // [small,  none]
  // loc_4B22DD  / loc_4B238D // [small,  few ]
  // loc_4B22E4  / loc_4B2394 // [small,  some]
  // loc_4B22EB  / loc_4B239B // [small,  many]
  // loc_4B22BC  / loc_4B236C // [medium, none]
  // loc_4B22EB  / loc_4B2394 // [medium, few ]: duplicate of max[small,many] / min[small,some]
  // loc_4B22F2  / loc_4B23A2 // [medium, some]
  // loc_4B22F9  / loc_4B23A9 // [medium, many]
  // loc_4B22BC  / loc_4B236C // [large,  none]
  // loc_4B2300  / loc_4B23B0 // [large,  few ]
  // loc_4B2307  / loc_4B23B7 // [large,  some]
  // loc_4B230E  / loc_4B23BE // [large,  many]
  private int[] offset = new int[NUM_GAL_SIZES * NUM_MINOR_SETTINGS];
  private int[] jmp_offset = new int[NUM_GAL_SIZES];
  private int[] minor_num = new int[NUM_SETTINGS];

  public MinorRacesNumbers(int address) {
    super(new SegmentDefinition(address, null, MinorRacesNumbers.class).desc(DESC));
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    int[][] def = defaults();
    for (int galSize = 0; galSize < NUM_GAL_SIZES; ++galSize) {
      for (int setting = 0; setting < NUM_EDITABLE_MINOR_SETTINGS; ++setting) {
        if (minor_num[galSize * NUM_EDITABLE_MINOR_SETTINGS + setting] != def[galSize][setting])
          return false;
      }
    }
    return true;
  }

  public int getMinorsNumber(int galsize, int setting) {
    return minor_num[galsize * NUM_EDITABLE_MINOR_SETTINGS + setting];
  }

  public void setMinorsNumber(int galsize, int setting, int val) {
    if (val > 31) {
      val = 31;
    } else if (val < 0) {
      val = 0;
    }

    if (minor_num[galsize * NUM_EDITABLE_MINOR_SETTINGS + setting] != val) {
      minor_num[galsize * NUM_EDITABLE_MINOR_SETTINGS + setting] = val;
      this.markChanged();
    }
  }

  @Override
  public void reset() {
    int[][] def = defaults();
    for (int galSize = 0; galSize < NUM_GAL_SIZES; ++galSize) {
      for (int setting = 0; setting < NUM_EDITABLE_MINOR_SETTINGS; ++setting) {
        int idx = galSize * NUM_EDITABLE_MINOR_SETTINGS + setting;
        if (minor_num[idx] != def[galSize][setting]) {
          minor_num[idx] = def[galSize][setting];
          markChanged();
        }
      }
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    // offsets
    for (int i = 0; i < offset.length; i++) {
      offset[i] = StreamTools.readInt(in, true);
    }

    // block1
    validate(StreamTools.readBytes(in, code_block1.length), code_block1);

    // jmp_offset 1
    jmp_offset[0] = StreamTools.readInt(in, true);

    // block 2
    validate(StreamTools.readBytes(in, code_block2.length), code_block2);

    // jmp_offset 2
    jmp_offset[1] = StreamTools.readInt(in, true);

    // block 3
    validate(StreamTools.readBytes(in, code_block3.length), code_block3);

    // jmp_offset 3
    jmp_offset[2] = StreamTools.readInt(in, true);

    // min/max minors
    for (int i = 0; i < NUM_SETTINGS; ++i) { // 1+4+2=7
      int op = in.read();

      if (i == (NUM_SETTINGS-1) && op == 0) {
        // by default there are 11 bytes of empty sub routine alignment
        // the first 7 bytes will get overridden to allow configure all settings independently
        validate(StreamTools.readBytes(in, 6), (byte)0);
      } else {
        // each option has 7 bytes
        validate(op, mov_num);                                                  // 1: mov eax
        minor_num[i] = StreamTools.readInt(in, true);                           // 4: .. min/max value
        validate(StreamTools.readBytes(in, pop_and_retn.length), pop_and_retn); // 2: pop ecx, retn
      }
    }

    // now fix: duplicate 3rd (max) / 2nd (min) index by the offset mappings
    int[] tmp = Arrays.copyOf(offset, offset.length);
    // override 'None' setting for offset comparison below
    tmp[0] = tmp[1]; // small galaxy none = few
    tmp[4] = tmp[5]; // medium galaxy none = few
    tmp[8] = tmp[9]; // large galaxy none = few

    int[] new_min = new int[NUM_SETTINGS];
    for (int galSize = 0; galSize < NUM_GAL_SIZES; galSize++) {
      // skip 'None' minor settings
      for (int setting = 1; setting < NUM_MINOR_SETTINGS; setting++) {
        // tmp offsets:
        // loc_4B22DD / loc_4B238D skipped tmp[0]
        // loc_4B22DD / loc_4B238D [small,  few] -> index:  0/7 = 0
        // loc_4B22E4 / loc_4B2394 [small, some] -> index:  7/7 = 1
        // loc_4B22EB / loc_4B239B [small, many] -> index: 14/7 = 2
        // loc_4B22EB / loc_4B2394 skipped
        // loc_4B22EB / loc_4B2394 [medium, few] -> index: 14/7 = 2 / 7/7 = 1
        // loc_4B22F2 / loc_4B23A2 [medium,some] -> index: 21/7 = 3
        // loc_4B22F9 / loc_4B23A9 [medium,many] -> index: 28/7 = 4
        // loc_4B2300 / loc_4B23B0 skipped
        // loc_4B2300 / loc_4B23B0 [large,  few] -> index: 35/7 = 5
        // loc_4B2307 / loc_4B23B7 [large, some] -> index: 42/7 = 6
        // loc_4B230E / loc_4B23BE [large, many] -> index: 49/7 = 7
        int index = (tmp[galSize * NUM_MINOR_SETTINGS + setting] - tmp[0]) / 7;
        new_min[galSize * NUM_EDITABLE_MINOR_SETTINGS + setting - 1] = minor_num[index];
      }
    }

    minor_num = new_min;
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    int asmAddr = TrekTools.toAsmAddress(address());
    int fix = 0;

    // recompute offsets to separate all 9 settings
    // by default medium/few shares code of
    // max: small/many, min: small/some
    for (int i = 0; i < offset.length; i++) {
      // skip every fourth offset for the 'None' minors settings
      if (i % 4 != 0) {
        // add an initial offset + 7 for each code block
        offset[i] = asmAddr + 109 + (i - fix) * 7;
      } else {
        fix++;
      }

      out.write(DataTools.toByte(offset[i], true));
    }

    // block1
    out.write(code_block1);

    // jmp_offset 1
    out.write(DataTools.toByte(jmp_offset[0], true));

    // block 2
    out.write(code_block2);

    // jmp_offset 2
    out.write(DataTools.toByte(jmp_offset[1], true));

    // block 3
    out.write(code_block3);

    // jmp_offset 3
    out.write(DataTools.toByte(jmp_offset[2], true));

    // min/max minors
    // overwrite empty bytes with a 9th code block
    for (int i = 0; i < NUM_SETTINGS; i++) {
      out.write(mov_num);
      out.write(DataTools.toByte(minor_num[i], true));
      out.write(pop_and_retn);
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    int asmAddr = TrekTools.toAsmAddress(address());
    int[][] def = defaults();
    boolean shareSmallMany = def[1][0] == def[0][2];
    int fix = 0;

    // recompute offsets for shared vanilla code
    // medium/few = max: small/many, min: small/some
    for (int i = 0; i < offset.length; i++) {
      // skip every fourth offset for the 'None' minors settings
      if (i % 4 == 0) {
        fix++;
      } else if (i == 5) {
        // copy shared offset
        offset[i] = shareSmallMany ? offset[3] : offset[2];
        fix++;
      } else {
        // add an initial offset + 7 for each code block
        offset[i] = asmAddr + 109 + (i - fix) * 7;
      }

      out.write(DataTools.toByte(offset[i], true));
    }

    // block1
    out.write(code_block1);

    // jmp_offset 1
    out.write(DataTools.toByte(jmp_offset[0], true));

    // block 2
    out.write(code_block2);

    // jmp_offset 2
    out.write(DataTools.toByte(jmp_offset[1], true));

    // block 3
    out.write(code_block3);

    // jmp_offset 3
    out.write(DataTools.toByte(jmp_offset[2], true));

    // min/max minors
    for (int galSize = 0; galSize < NUM_GAL_SIZES; ++galSize) {
      for (int setting = 0; setting < NUM_EDITABLE_MINOR_SETTINGS; ++setting) {
        // skip shared code block
        if (galSize == 1 && setting == 0)
          continue;

        // write 7 bytes min/max minors code block
        out.write(mov_num);                                       // 1
        out.write(DataTools.toByte(def[galSize][setting], true)); // 4
        out.write(pop_and_retn);                                  // 2
      }
    }

    // restore empty bytes for the 9th min/max minors code block
    for (int i = 0; i < 7; i++)
      out.write(NOP);
  }

  @Override
  public void clear() {
    Arrays.fill(offset, 0);
    Arrays.fill(jmp_offset, 0);
    Arrays.fill(minor_num, 0);
    markChanged();
  }

  private int[][] defaults() {
    switch (address) {
      case MAX_MINORS_SEGMENT_ADDRESS:
        return Default_MaxMinors;
      case MIN_MINORS_SEGMENT_ADDRESS:
        return Default_MinMinors;
    }
    throw new IndexOutOfBoundsException();
  }
}
