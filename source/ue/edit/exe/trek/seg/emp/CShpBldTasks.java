package ue.edit.exe.trek.seg.emp;

import lombok.val;
import ue.edit.exe.trek.seg.emp.AIShpSets.ShpBldTask;
import ue.edit.res.stbof.common.CStbof.ShipRole;

/**
 * AIShpSets build tasks
 */
public interface CShpBldTasks {

  public static final short NoShipRole      = ShipRole.None;
  public static final short Scout           = ShipRole.Scout;
  public static final short Destroyer       = ShipRole.Destroyer;
  public static final short Cruiser         = ShipRole.Cruiser;
  public static final short StrikeCruiser   = ShipRole.StrikeCruiser;
  public static final short BattleShip      = ShipRole.BattleShip;
  public static final short ColonyShip      = ShipRole.ColonyShip;
  public static final short TroopTransport  = ShipRole.TroopTransport;

  static ShpBldTask bt(short shipRole) {
    return new ShpBldTask(shipRole);
  }

  static ShpBldTask bt(short shipRole, int repeat) {
    return new ShpBldTask(shipRole, repeat);
  }

  public static final ShpBldTask None = bt(NoShipRole);

  // Cardassians
  public static final ShpBldTask[][] Default_ShpBldTasks_Card = new ShpBldTask[][] {
    // normal expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(TroopTransport), bt(BattleShip, 1),  bt(TroopTransport), bt(Cruiser),        bt(Destroyer),
      bt(StrikeCruiser),  bt(TroopTransport), bt(BattleShip),     bt(Destroyer),      bt(Cruiser),        bt(Scout),
      bt(StrikeCruiser),  bt(BattleShip),     bt(TroopTransport), bt(Cruiser),        bt(Destroyer)
    },
    // unopposed expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(ColonyShip),     bt(TroopTransport), bt(Scout),
      bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),
      bt(Cruiser, 1),     bt(Destroyer),      bt(BattleShip),     bt(Scout),          bt(StrikeCruiser),  bt(BattleShip),
      bt(Destroyer),      bt(Cruiser),        bt(BattleShip),     bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),
      bt(BattleShip),     bt(TroopTransport)
    },
    // normal consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Cruiser),
      bt(Destroyer),      bt(ColonyShip),     bt(BattleShip, 1),  bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),
      bt(Cruiser),        bt(BattleShip),     bt(TroopTransport), bt(StrikeCruiser),  bt(Cruiser),        bt(BattleShip),
      bt(TroopTransport), bt(StrikeCruiser),  bt(Cruiser),        bt(TroopTransport), bt(Destroyer),      bt(BattleShip),
      bt(Cruiser),        bt(TroopTransport), bt(BattleShip),     bt(StrikeCruiser)
    },
    // defensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(BattleShip, 1),  bt(Cruiser),        bt(TroopTransport), bt(BattleShip),     bt(StrikeCruiser),  bt(TroopTransport),
      bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Cruiser),        bt(BattleShip),     bt(StrikeCruiser),
      bt(TroopTransport)
    },
    // offensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(TroopTransport), bt(BattleShip),     bt(Cruiser),        bt(TroopTransport),
      bt(Cruiser),        bt(BattleShip),     bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),  bt(Cruiser),
      bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(Cruiser),        bt(BattleShip),     bt(TroopTransport),
      bt(StrikeCruiser),  bt(Cruiser),        bt(Scout),          bt(Cruiser),        bt(TroopTransport), bt(Cruiser)
    },
    // cold war
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(Cruiser),        bt(Destroyer, 1),   bt(BattleShip),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),
      bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(StrikeCruiser),  bt(Scout),
      bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Destroyer)
    },
    // defensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(TroopTransport, 1),
      bt(BattleShip),     bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(BattleShip),     bt(TroopTransport),
      bt(StrikeCruiser),  bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Scout),          bt(StrikeCruiser),
      bt(Cruiser),        bt(TroopTransport), bt(Destroyer),      bt(BattleShip),     bt(Cruiser)
    },
    // offensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(TroopTransport),
      bt(Cruiser, 1),     bt(Destroyer),      bt(TroopTransport), bt(BattleShip),     bt(Cruiser),        bt(TroopTransport),
      bt(Destroyer),      bt(BattleShip),     bt(Scout),          bt(StrikeCruiser),  bt(TroopTransport), bt(Cruiser),
      bt(Destroyer),      bt(BattleShip),     bt(Cruiser),        bt(TroopTransport)
    },
    // jihad
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(Destroyer),      bt(TroopTransport), bt(Cruiser, 1),     bt(BattleShip),
      bt(TroopTransport), bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),
      bt(TroopTransport), bt(Cruiser),        bt(BattleShip),     bt(TroopTransport), bt(Cruiser),        bt(Destroyer)
    }
  };

  // Federation
  public static final ShpBldTask[][] Default_ShpBldTasks_Fed = new ShpBldTask[][] {
    // normal expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(TroopTransport), bt(BattleShip, 1),  bt(TroopTransport), bt(Cruiser),        bt(Destroyer),
      bt(StrikeCruiser),  bt(BattleShip),     bt(Cruiser),        bt(Scout),          bt(BattleShip),     bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer)
    },
    // unopposed expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(ColonyShip),     bt(TroopTransport), bt(Scout),
      bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(TroopTransport), bt(BattleShip, 1),
      bt(Destroyer),      bt(Cruiser),        bt(Scout),          bt(StrikeCruiser),  bt(BattleShip),     bt(TroopTransport),
      bt(Destroyer),      bt(Cruiser),        bt(Scout),          bt(BattleShip),     bt(Destroyer),      bt(TroopTransport)
    },
    // normal consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Cruiser),
      bt(ColonyShip),     bt(BattleShip, 1),  bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),  bt(Cruiser),
      bt(BattleShip),     bt(Scout),          bt(TroopTransport), bt(Cruiser),        bt(BattleShip),     bt(TroopTransport),
      bt(StrikeCruiser),  bt(Cruiser),        bt(TroopTransport), bt(Destroyer),      bt(BattleShip),     bt(TroopTransport)
    },
    // defensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(TroopTransport), bt(BattleShip, 1),  bt(Cruiser),        bt(TroopTransport), bt(BattleShip),     bt(StrikeCruiser),
      bt(Cruiser),        bt(TroopTransport), bt(Destroyer),      bt(BattleShip),     bt(Scout),          bt(Cruiser),
      bt(TroopTransport), bt(BattleShip),     bt(StrikeCruiser),  bt(TroopTransport)
    },
    // offensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(TroopTransport), bt(BattleShip),     bt(Cruiser),        bt(TroopTransport),
      bt(BattleShip),     bt(Destroyer),      bt(StrikeCruiser),  bt(TroopTransport), bt(Cruiser),        bt(Scout),
      bt(Cruiser),        bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(BattleShip),     bt(TroopTransport),
      bt(StrikeCruiser),  bt(Cruiser),        bt(Scout),          bt(TroopTransport), bt(Cruiser)
    },
    // cold war
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(TroopTransport),
      bt(Destroyer),      bt(Cruiser, 1),     bt(BattleShip),     bt(TroopTransport), bt(Cruiser),        bt(Destroyer),
      bt(StrikeCruiser),  bt(Cruiser),        bt(TroopTransport), bt(Scout),          bt(Cruiser),        bt(BattleShip),
      bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(TroopTransport)
    },
    // defensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(BattleShip),     bt(ColonyShip),     bt(TroopTransport, 1),
      bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser),  bt(BattleShip),     bt(TroopTransport),
      bt(Destroyer),      bt(Cruiser),        bt(BattleShip),     bt(TroopTransport), bt(StrikeCruiser),  bt(Scout),
      bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(BattleShip),     bt(StrikeCruiser),  bt(Cruiser)
    },
    // offensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(TroopTransport),
      bt(Cruiser, 1),     bt(Destroyer),      bt(TroopTransport), bt(BattleShip),     bt(Cruiser),        bt(StrikeCruiser),
      bt(Scout),          bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(BattleShip),     bt(TroopTransport),
      bt(Cruiser),        bt(StrikeCruiser),  bt(Scout),          bt(TroopTransport), bt(Cruiser)
    },
    // jihad
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(Destroyer),      bt(TroopTransport), bt(Cruiser, 1),     bt(BattleShip),
      bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer),      bt(StrikeCruiser),  bt(BattleShip),     bt(TroopTransport), bt(Cruiser),
      bt(Destroyer)
    }
  };

  // Ferengi
  public static final ShpBldTask[][] Default_ShpBldTasks_Ferg = new ShpBldTask[][] {
    // normal expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(TroopTransport),
      bt(Destroyer),      bt(ColonyShip),     bt(Cruiser, 1),     bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(Scout),          bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(Destroyer),
      bt(BattleShip),     bt(Destroyer),      bt(Cruiser),        bt(Destroyer),      bt(Scout),          bt(StrikeCruiser),
      bt(Destroyer),      bt(TroopTransport), bt(Destroyer)
    },
    // unopposed expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(ColonyShip),     bt(TroopTransport), bt(TroopTransport), bt(Destroyer),
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(ColonyShip),
      bt(Destroyer),      bt(TroopTransport, 1),  bt(Cruiser),    bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),
      bt(Scout),          bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser),  bt(Destroyer),
      bt(TroopTransport), bt(Destroyer),      bt(BattleShip),     bt(Destroyer),      bt(Scout),          bt(Destroyer),
      bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Destroyer)
    },
    // normal consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(TroopTransport),
      bt(Destroyer),      bt(Cruiser),        bt(ColonyShip),     bt(StrikeCruiser),  bt(Cruiser, 1),     bt(TroopTransport),
      bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),      bt(Scout),          bt(BattleShip),     bt(Destroyer),
      bt(StrikeCruiser),  bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(BattleShip),     bt(Destroyer),
      bt(StrikeCruiser),  bt(Destroyer)
    },
    // defensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(StrikeCruiser),
      bt(Destroyer),      bt(TroopTransport), bt(ColonyShip),     bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser, 1),
      bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(StrikeCruiser),
      bt(TroopTransport), bt(BattleShip),     bt(Scout),          bt(Cruiser),        bt(StrikeCruiser),  bt(TroopTransport),
      bt(BattleShip),     bt(Cruiser),        bt(Destroyer),      bt(StrikeCruiser),  bt(TroopTransport)
    },
    // offensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(TroopTransport),
      bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(Destroyer),      bt(Cruiser),
      bt(BattleShip),     bt(TroopTransport), bt(StrikeCruiser),  bt(Cruiser),        bt(Destroyer),      bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer),      bt(Cruiser),        bt(Scout),          bt(Cruiser),        bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer)
    },
    // cold war
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(Destroyer, 1),   bt(StrikeCruiser),  bt(TroopTransport), bt(Destroyer),      bt(Destroyer),      bt(Scout),
      bt(Destroyer),      bt(BattleShip),     bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(Destroyer),
      bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(Destroyer)
    },
    // defensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(TroopTransport, 1),
      bt(StrikeCruiser),  bt(Destroyer),      bt(Scout),          bt(TroopTransport), bt(BattleShip),     bt(Destroyer),
      bt(StrikeCruiser),  bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser),
      bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Scout),          bt(Destroyer),      bt(StrikeCruiser),
      bt(Destroyer)
    },
    // offensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(ColonyShip),     bt(Cruiser, 1),     bt(Destroyer),
      bt(TroopTransport), bt(Destroyer),      bt(BattleShip),     bt(Destroyer),      bt(TroopTransport), bt(Cruiser),
      bt(Destroyer),      bt(TroopTransport), bt(Scout),          bt(StrikeCruiser),  bt(Destroyer),      bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer),      bt(Destroyer),      bt(TroopTransport), bt(StrikeCruiser),  bt(Destroyer)
    },
    // jihad
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Cruiser, 1),
      bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(TroopTransport),
      bt(Destroyer),      bt(BattleShip),     bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),      bt(Scout),
      bt(Destroyer),      bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),
      bt(Destroyer),      bt(Scout),          bt(Destroyer)
    }
  };

  // Klingons
  public static final ShpBldTask[][] Default_ShpBldTasks_Klng = new ShpBldTask[][] {
    // normal expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(Scout),          bt(ColonyShip),     bt(Cruiser, 1),     bt(Destroyer),      bt(TroopTransport), bt(Scout),
      bt(BattleShip),     bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(Scout),
      bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(Destroyer),      bt(BattleShip),
      bt(Destroyer),      bt(Scout),          bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(TroopTransport),
      bt(Destroyer)
    },
    // unopposed expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(ColonyShip),     bt(TroopTransport), bt(TroopTransport), bt(Scout),
      bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(ColonyShip),     bt(Destroyer),
      bt(TroopTransport), bt(Cruiser, 1),     bt(Destroyer),      bt(Scout),          bt(Destroyer),      bt(BattleShip),
      bt(Destroyer),      bt(Scout),          bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),  bt(Destroyer),
      bt(Cruiser),        bt(TroopTransport), bt(Destroyer)
    },
    // normal consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(TroopTransport),
      bt(BattleShip),     bt(StrikeCruiser),  bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(TroopTransport),
      bt(BattleShip),     bt(StrikeCruiser),  bt(TroopTransport), bt(Scout),          bt(Cruiser),        bt(Destroyer),
      bt(BattleShip),     bt(Cruiser),        bt(StrikeCruiser),  bt(TroopTransport), bt(Cruiser),        bt(Destroyer)
    },
    // defensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Cruiser),
      bt(StrikeCruiser),  bt(Cruiser),        bt(TroopTransport), bt(Destroyer),      bt(BattleShip),     bt(Scout),
      bt(Cruiser),        bt(StrikeCruiser),  bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(BattleShip),
      bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser)
    },
    // offensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(TroopTransport),
      bt(Cruiser),        bt(BattleShip),     bt(TroopTransport), bt(StrikeCruiser),  bt(Cruiser),        bt(Destroyer),
      bt(Cruiser),        bt(Destroyer),      bt(Cruiser),        bt(Scout),          bt(Cruiser),        bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer)
    },
    // cold war
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(Scout),          bt(TroopTransport),
      bt(Destroyer),      bt(Destroyer, 1),   bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(Scout),          bt(Destroyer),      bt(BattleShip),     bt(Destroyer),      bt(TroopTransport), bt(StrikeCruiser),
      bt(Destroyer),      bt(Scout),          bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(Scout),          bt(Destroyer)
    },
    // defensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(Scout),
      bt(TroopTransport), bt(Cruiser, 1),     bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Destroyer),
      bt(StrikeCruiser),  bt(Scout),          bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(BattleShip),
      bt(TroopTransport), bt(Scout),          bt(StrikeCruiser),  bt(Destroyer),      bt(TroopTransport), bt(Cruiser),
      bt(Destroyer)
    },
    // offensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(TroopTransport),
      bt(Scout),          bt(Cruiser, 1),     bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Destroyer),
      bt(Cruiser),        bt(TroopTransport), bt(Scout),          bt(StrikeCruiser),  bt(Destroyer),      bt(TroopTransport),
      bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Destroyer),      bt(Scout),          bt(Cruiser),
      bt(Destroyer)
    },
    // jihad
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(Destroyer),      bt(TroopTransport), bt(Cruiser, 1),     bt(Destroyer),
      bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(Scout),          bt(TroopTransport), bt(Cruiser),
      bt(Destroyer),      bt(BattleShip),     bt(TroopTransport), bt(Destroyer),      bt(Scout),          bt(Cruiser),
      bt(Destroyer),      bt(StrikeCruiser),  bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(Destroyer),
      bt(TroopTransport), bt(Scout),          bt(Cruiser),        bt(Destroyer)
    },
  };

  // Romulans
  public static final ShpBldTask[][] Default_ShpBldTasks_Rom = new ShpBldTask[][] {
    // normal expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(Scout),          bt(ColonyShip),     bt(Cruiser, 1),     bt(Destroyer),      bt(TroopTransport), bt(Scout),
      bt(StrikeCruiser),  bt(Destroyer),      bt(BattleShip),     bt(Cruiser),        bt(Scout),          bt(TroopTransport),
      bt(StrikeCruiser),  bt(Destroyer),      bt(Cruiser),        bt(BattleShip),     bt(Destroyer),      bt(Cruiser),
      bt(StrikeCruiser),  bt(TroopTransport), bt(Destroyer)
    },
    // unopposed expansion
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(ColonyShip),     bt(TroopTransport), bt(TroopTransport), bt(Scout),
      bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(ColonyShip),     bt(Destroyer),
      bt(TroopTransport), bt(Cruiser, 1),     bt(Scout),          bt(StrikeCruiser),  bt(Destroyer),      bt(BattleShip),
      bt(Scout),          bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),
      bt(TroopTransport), bt(Destroyer)
    },
    // normal consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(TroopTransport), bt(StrikeCruiser),  bt(BattleShip),     bt(Cruiser),
      bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(BattleShip),     bt(TroopTransport), bt(StrikeCruiser),
      bt(Scout),          bt(Cruiser),        bt(Destroyer),      bt(BattleShip),     bt(Cruiser),        bt(StrikeCruiser),
      bt(TroopTransport), bt(Cruiser),        bt(StrikeCruiser)
    },
    // defensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Cruiser),
      bt(Destroyer),      bt(ColonyShip),     bt(StrikeCruiser),  bt(BattleShip, 1),  bt(TroopTransport), bt(Cruiser),
      bt(StrikeCruiser),  bt(BattleShip),     bt(TroopTransport), bt(Cruiser),        bt(Scout),          bt(StrikeCruiser),
      bt(TroopTransport), bt(BattleShip),     bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(BattleShip),
      bt(StrikeCruiser),  bt(BattleShip),     bt(StrikeCruiser)
    },
    // offensive consolidation
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(TroopTransport), bt(Destroyer),
      bt(ColonyShip),     bt(Cruiser, 1),     bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(StrikeCruiser),
      bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(BattleShip),     bt(Destroyer),      bt(Cruiser),
      bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(Cruiser),        bt(StrikeCruiser),  bt(Destroyer),
      bt(TroopTransport), bt(Cruiser),        bt(Scout),          bt(Cruiser),        bt(BattleShip),     bt(Cruiser),
      bt(Destroyer)
    },
    // cold war
    new ShpBldTask[] {
      bt(Scout),          bt(ColonyShip),     bt(TroopTransport), bt(Destroyer),      bt(Scout),          bt(TroopTransport),
      bt(Destroyer),      bt(Cruiser, 1),     bt(TroopTransport), bt(Destroyer),      bt(Scout),          bt(StrikeCruiser),
      bt(BattleShip),     bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(Scout),
      bt(StrikeCruiser),  bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(Destroyer),      bt(Scout)
    },
    // defensive war
    new ShpBldTask[] {
      bt(Scout),          bt(TroopTransport), bt(Destroyer),      bt(ColonyShip),     bt(StrikeCruiser),  bt(TroopTransport),
      bt(Cruiser, 1),     bt(BattleShip),     bt(TroopTransport), bt(Destroyer),      bt(StrikeCruiser),  bt(Cruiser),
      bt(BattleShip),     bt(TroopTransport), bt(StrikeCruiser),  bt(Scout),          bt(StrikeCruiser),  bt(TroopTransport),
      bt(Destroyer),      bt(Cruiser)
    },
    // offensive war
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(TroopTransport), bt(ColonyShip),     bt(TroopTransport), bt(Scout),
      bt(Cruiser, 1),     bt(Destroyer),      bt(Cruiser),        bt(TroopTransport), bt(StrikeCruiser),  bt(Scout),
      bt(Cruiser),        bt(TroopTransport), bt(BattleShip),     bt(Destroyer),      bt(TroopTransport), bt(Cruiser),
      bt(StrikeCruiser),  bt(Destroyer),      bt(TroopTransport), bt(Scout),          bt(Cruiser),        bt(Destroyer)
    },
    // jihad
    new ShpBldTask[] {
      bt(Scout),          bt(Destroyer),      bt(Destroyer),      bt(TroopTransport), bt(Cruiser, 1),     bt(StrikeCruiser),
      bt(Destroyer),      bt(TroopTransport), bt(Cruiser),        bt(Scout),          bt(TroopTransport), bt(BattleShip),
      bt(Cruiser),        bt(Destroyer),      bt(TroopTransport), bt(Scout),          bt(Cruiser),        bt(StrikeCruiser),
      bt(TroopTransport), bt(Destroyer),      bt(Cruiser),        bt(BattleShip),     bt(Scout),          bt(StrikeCruiser),
      bt(Cruiser),        bt(Destroyer)
    }
  };

  // Shared 'emergency' agenda, not editable by UE.
  // Default, used by BotF AI when all systems with shipyards are orbited by non-allied fleets.
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=34142#p34142
  public static final ShpBldTask[][] Default_ShpBldTasks_Fallback = new ShpBldTask[][] {
    // normal expansion
    new ShpBldTask[] {
      bt(ColonyShip),     bt(TroopTransport), bt(Scout),          bt(Destroyer),      bt(StrikeCruiser),  bt(TroopTransport),
      bt(Cruiser, 1),     bt(BattleShip),     bt(Destroyer),      bt(StrikeCruiser),  bt(TroopTransport), bt(Cruiser),
      bt(BattleShip),     bt(StrikeCruiser),  bt(TroopTransport), bt(Scout),          bt(StrikeCruiser),  bt(Destroyer),
      bt(Cruiser)
    }
  };

  public static final ShpBldTask[][][] Default_ShpBldTasks = new ShpBldTask[][][] {
    Default_ShpBldTasks_Card,
    Default_ShpBldTasks_Fed,
    Default_ShpBldTasks_Ferg,
    Default_ShpBldTasks_Klng,
    Default_ShpBldTasks_Rom
  };

  public static ShpBldTask getDefault(int emp, int agenda, int item) {
    val tasks = Default_ShpBldTasks[emp][agenda];
    if (tasks == null || item >= tasks.length)
      return None;
    return tasks[item];
  }
}
