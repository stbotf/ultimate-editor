package ue.edit.exe.trek.seg.emp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * ME Starting Conditions
 * Contains starting populations and credits for major empires.
 */
public class MEStartingConditions extends InternalSegment {

  public static final String FILENAME = CTrekSegments.MEStartCond;
  public static final int SEGMENT_ADDRESS = 0x0018DC1C;
  public static final int SIZE = 400; // 4 * 25 * 4

  // 5 empires * 5 eras
  public static final int[][] Default_StartPopulation = new int[][] {
    { 200, 300, 400, 400, 400 },
    { 210, 310, 335, 335, 335 },
    { 160, 230, 310, 310, 310 },
    { 190, 280, 370, 370, 370 },
    { 180, 270, 355, 355, 355 },
  };

  // 5 empires * 5 eras
  public static final int[][] Default_StartCredits = new int[][] {
    { 400, 800, 2000, 10000, 20000 },
    { 1000, 2000, 5000, 17500, 35000 },
    { 800, 1600, 4000, 20000, 40000 },
    { 500, 1000, 2500, 12500, 25000 },
    { 600, 1200, 3000, 15000, 30000 },
  };

  // %1 - the offending race ID
  private final String errINVALID_ID = Language.getString("MEStartingConditions.0"); //$NON-NLS-1$
  // %1 - empire id
  private final String errINVALID_POP = Language.getString("MEStartingConditions.1"); //$NON-NLS-1$
  // %1 - segment address, %2 error message
  private final String errMSG = Language.getString("MEStartingConditions.2"); //$NON-NLS-1$
  // %1 - the era id
  private final String errINVALID_ERA = Language.getString("MEStartingConditions.3"); //$NON-NLS-1$

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_ERAS = CStbof.NUM_ERAS;

  private int POPULATION[] = new int[NUM_EMPIRES * NUM_ERAS];
  private int CREDITS[] = new int[NUM_EMPIRES * NUM_ERAS];
  private int UNKNOWN_1[] = new int[NUM_EMPIRES * NUM_ERAS];
  private int UNKNOWN_2[] = new int[NUM_EMPIRES * NUM_ERAS];

  public MEStartingConditions() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, MEStartingConditions.class));
  }

  @Override
  public boolean isDefault() {
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      for (int era = 0; era < NUM_ERAS; ++era) {
        if (POPULATION[emp * NUM_ERAS + era] != Default_StartPopulation[emp][era])
          return false;
        if (CREDITS[emp * NUM_ERAS + era] != Default_StartCredits[emp][era])
          return false;
      }
    }
    return true;
  }

  public int getPopulation(int id, int era) {
    this.checkID(id);
    this.checkEra(era);

    return this.POPULATION[(id * NUM_ERAS) + era];
  }

  public int getCredits(int id, int era) {
    this.checkID(id);
    this.checkEra(era);

    return this.CREDITS[(id * NUM_ERAS) + era];
  }

  public void setPopulation(int id, int era, int pop) {
    this.checkID(id);
    this.checkEra(era);

    if (this.POPULATION[(id * NUM_ERAS) + era] == pop) {
      return;
    }

    this.POPULATION[(id * NUM_ERAS) + era] = pop;
    this.markChanged();
  }

  public void setCredits(int id, int era, int cred) {
    this.checkID(id);
    this.checkEra(era);

    if (this.CREDITS[(id * NUM_ERAS) + era] == cred)
      return;

    this.CREDITS[(id * NUM_ERAS) + era] = cred;
    this.markChanged();
  }

  @Override
  public void reset() {
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      for (int era = 0; era < NUM_ERAS; ++era) {
        if (POPULATION[emp * NUM_ERAS + era] != Default_StartPopulation[emp][era]) {
          POPULATION[emp * NUM_ERAS + era] = Default_StartPopulation[emp][era];
          markChanged();
        }
        if (CREDITS[emp * NUM_ERAS + era] != Default_StartCredits[emp][era]) {
          CREDITS[emp * NUM_ERAS + era] = Default_StartCredits[emp][era];
          markChanged();
        }
      }
    }
  }

  private void checkID(int id) {
    if (id >= NUM_EMPIRES) {
      throw new IndexOutOfBoundsException(
        errINVALID_ID.replace("%1", Integer.toString(id))); //$NON-NLS-1$
    }
  }

  private void checkEra(int era) {
    if (era >= NUM_ERAS) {
      throw new IndexOutOfBoundsException(
        errINVALID_ERA.replace("%1", Integer.toString(era))); //$NON-NLS-1$
    }
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#check()
   */
  @Override
  public void check(Vector<String> response) {
    for (int race = 0; race < NUM_EMPIRES; race++) {
      for (int era = 0; era < NUM_ERAS; era++) {
        int n = (race * NUM_ERAS) + era;
        if (this.POPULATION[n] <= 0) {
          response.add(errMSG.replace("%1", //$NON-NLS-1$
            Long.toString(address())).replace("%2", //$NON-NLS-1$
              errINVALID_POP.replace("%1", Integer.toString(n)))); //$NON-NLS-1$
        }
        // negative credits are allowed
      }
    }
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int race = 0; race < NUM_EMPIRES; race++) {
      int idx = (race * NUM_ERAS);

      for (int era = 0; era < NUM_ERAS; era++) {
        POPULATION[idx+era] = StreamTools.readInt(in, true);
      }
      for (int era = 0; era < NUM_ERAS; era++) {
        CREDITS[idx+era] = StreamTools.readInt(in, true);
      }
      for (int era = 0; era < NUM_ERAS; era++) {
        UNKNOWN_1[idx+era] = StreamTools.readInt(in, true);
      }
      for (int era = 0; era < NUM_ERAS; era++) {
        UNKNOWN_2[idx+era] = StreamTools.readInt(in, true);
      }
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int race = 0; race < NUM_EMPIRES; race++) {
      // population
      for (int era = 0; era < NUM_ERAS; era++) {
        out.write(DataTools.toByte(this.POPULATION[(race * NUM_ERAS) + era], true));
      }
      // credits
      for (int era = 0; era < NUM_ERAS; era++) {
        out.write(DataTools.toByte(this.CREDITS[(race * NUM_ERAS) + era], true));
      }
      // unknown
      for (int era = 0; era < NUM_ERAS; era++) {
        out.write(DataTools.toByte(this.UNKNOWN_1[(race * NUM_ERAS) + era], true));
      }
      for (int era = 0; era < NUM_ERAS; era++) {
        out.write(DataTools.toByte(this.UNKNOWN_2[(race * NUM_ERAS) + era], true));
      }
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      for (int era = 0; era < NUM_ERAS; ++era)
        out.write(DataTools.toByte(Default_StartPopulation[emp][era], true));
      for (int era = 0; era < NUM_ERAS; ++era)
        out.write(DataTools.toByte(Default_StartCredits[emp][era], true));
      for (int era = 0; era < NUM_ERAS; ++era)
        out.write(DataTools.toByte(UNKNOWN_1[(emp * NUM_ERAS) + era], true));
      for (int era = 0; era < NUM_ERAS; ++era)
        out.write(DataTools.toByte(UNKNOWN_2[(emp * NUM_ERAS) + era], true));
    }
  }

  @Override
  public void clear() {
    Arrays.fill(POPULATION, 0);
    Arrays.fill(CREDITS, 0);
    Arrays.fill(UNKNOWN_1, 0);
    Arrays.fill(UNKNOWN_2, 0);
    markChanged();
  }
}
