package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.DataSegment;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.SegmentArea;
import ue.edit.exe.trek.sd.SD_MandatoryDilFix;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.exception.InvalidArgumentsException;
import ue.util.stream.StreamTools;

/**
 * Fixture to disable the automatic dilithium refinery creation and activation on game start.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?t=1236
 */
public class MandatoryDilithiumFix extends SegmentArea implements Patch {

  public static final int SIZE = 28;
  public static final int DEFAULT_DilithiumRefineryId = 6;

  private DataSegment orig_refineryCheck;
  private MovRegInt orig_dilId1;
  private DataSegment orig_refineryCreation;
  private MovRegInt orig_dilId2;
  private DataSegment patched_refineryCreation;

  @Getter private boolean patched = false;

  public MandatoryDilithiumFix(SegmentDefinition def) {
    super(def);
    orig_refineryCheck = new DataSegment(address, SD_MandatoryDilFix.OrigRefineryCheck);
    orig_dilId1 = new MovRegInt(address + 4, SD_MandatoryDilFix.OrigDilId1);
    orig_refineryCreation = new DataSegment(address + 9, SD_MandatoryDilFix.OrigRefineryCreation);
    orig_dilId2 = new MovRegInt(address + 23, SD_MandatoryDilFix.OrigDilId2);
    patched_refineryCreation = new DataSegment(address, SD_MandatoryDilFix.PatchedRefineryCreation);
  }

  @Override
  public void load(InputStream in) throws IOException {
    byte[] data = StreamTools.readBytes(in, SIZE);
    patched = !orig_refineryCheck.check(data, 0);

    if (patched) {
      patched_refineryCreation.validate(data);
    } else {
      // read dilithium refinery id
      orig_dilId1.load(data, 4);
      orig_refineryCreation.validate(data, 9);
      orig_dilId2.load(data, 23);
      orig_dilId2.validate(orig_dilId1.intValue());
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    if (patched) {
      patched_refineryCreation.save(out); // 28
    } else {
      orig_refineryCheck.save(out);       // 5
      orig_dilId1.save(out);              // 4
      orig_refineryCreation.save(out);    // 15
      orig_dilId2.save(out);              // 4
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    orig_refineryCheck.saveDefault(out);    // 5
    orig_dilId1.saveDefault(out);           // 4
    orig_refineryCreation.saveDefault(out); // 15
    orig_dilId2.saveDefault(out);           // 4
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    return !patched && orig_dilId1.check(DEFAULT_DilithiumRefineryId)
      && orig_dilId2.check(DEFAULT_DilithiumRefineryId);
  }

  @Override
  public void patch() {
    if (!patched) {
      patched_refineryCreation.markChanged();
      patched = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() {
    if (patched) {
      orig_refineryCheck.markChanged();
      orig_dilId1.markChanged();
      orig_refineryCreation.markChanged();
      orig_dilId2.markChanged();
      patched = false;
      markChanged();
    }
  }

  public int getDilithiumRefineryId() {
    if (patched) {
      throw new UnsupportedOperationException(
        "The patch must be unapplied to read the value.");
    }
    return orig_dilId1.intValue();
  }

  public void setDilithiumRefineryId(int value) throws InvalidArgumentsException {
    if (patched) {
      throw new UnsupportedOperationException(
        "The patch must be unapplied to set the value.");
    }
    orig_dilId1.setValue(value);
    orig_dilId2.setValue(value);
    if (orig_dilId1.madeChanges() || orig_dilId2.madeChanges())
      markChanged();
  }

  @Override
  public void reset() {
    unpatch();
    orig_dilId1.reset();
    orig_dilId2.reset();
    if (orig_dilId1.madeChanges() || orig_dilId2.madeChanges())
      markChanged();
  }

  @Override
  public void clear() {
    orig_refineryCheck.clear();
    orig_dilId1.clear();
    orig_refineryCreation.clear();
    orig_dilId2.clear();
    patched_refineryCreation.clear();
    patched = false;
  }

  @Override
  public void check(Vector<String> response) {
    // is already validated on load
  }

  @Override
  public List<InternalSegment> listSubSegments() {
    return patched
      ? Arrays.asList(patched_refineryCreation)
      : Arrays.asList(orig_refineryCheck, orig_dilId1, orig_refineryCreation, orig_dilId2);
  }
}
