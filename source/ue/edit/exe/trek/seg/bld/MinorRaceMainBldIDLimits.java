package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.CStbof.StructureGroup;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Minor race main building id ranges.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=33131#p33131
 * @author Alan
 */
public class MinorRaceMainBldIDLimits extends InternalSegment {

  public static final String FILENAME = CTrekSegments.MinorMainBldIds;
  public static final int SEGMENT_ADDRESS = 0x0018DDAC;
  public static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  public static final int NUM_TYPES = 4; // main structure types except intel
  public static final int SIZE = 2 * NUM_EMPIRES * NUM_TYPES * Short.BYTES; // 80

  final String ERR_INVALID_MIN = "Minor race start building id [%1] doesn't match detected main building start [%2]. (fixed)";
  final String ERR_INVALID_MAX = "Minor race end building id [%1] doesn't match detected main building end [%2]. (fixed)";

  public short[] Default_startIDs = new short[] {
     88,  97, 116, 125, 134, 143, 162, 171, 180, 189,
    208, 217, 226, 235, 254, 263, 272, 281, 300, 309,
  };
  public short[] Default_endIDs = new short[] {
     96, 106, 124, 133, 142, 152, 170, 179, 188, 198,
    216, 225, 234, 244, 262, 271, 280, 290, 308, 317
  };

  private Stbof stbof;
  private short[] startingIDs = new short[NUM_EMPIRES * NUM_TYPES];
  private short[] endingIDs = new short[NUM_EMPIRES * NUM_TYPES];

  public MinorRaceMainBldIDLimits(Stbof stbof) {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, MinorRaceMainBldIDLimits.class));
    this.stbof = stbof;
  }

  @Override
  public void load(InputStream in) throws IOException {
    // starting ids
    for (int i = 0; i < startingIDs.length; i++) {
      startingIDs[i] = StreamTools.readShort(in, true);
    }

    // ending ids
    for (int i = 0; i < endingIDs.length; i++) {
      endingIDs[i] = StreamTools.readShort(in, true);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (short id : startingIDs)
      out.write(DataTools.toByte(id, true));
    for (short id : endingIDs)
      out.write(DataTools.toByte(id, true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (short id : Default_startIDs)
      out.write(DataTools.toByte(id, true));
    for (short id : Default_endIDs)
      out.write(DataTools.toByte(id, true));
  }

  @Override
  public void clear() {
    Arrays.fill(startingIDs, (short)0);
    Arrays.fill(endingIDs, (short)0);
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    if (stbof != null) {
      try {
        Edifice edifice = (Edifice) stbof.getInternalFile(CStbofFiles.EdificeBst, false);

        for (int idx = 0; idx < NUM_EMPIRES * NUM_TYPES; ++idx) {
          // search edifice id range
          int empire = idx / NUM_TYPES;
          int bldGroup = mapTypeToBldGroup(idx - empire * NUM_TYPES);
          int[] range = edifice.findMainStructureRange(empire, bldGroup);

          // fix id
          if (startingIDs[idx] != range[0]) {
            String err = ERR_INVALID_MIN
              .replace("%1", Integer.toString(startingIDs[idx]))
              .replace("%2", Integer.toString(range[0]));
            response.add(getCheckIntegrityString(InternalSegment.INTEGRITY_CHECK_ERROR, err));
            startingIDs[idx] = (short)range[0];
          }

          // fix id
          if (endingIDs[idx] != range[1]) {
            String err = ERR_INVALID_MAX
              .replace("%1", Integer.toString(endingIDs[idx]))
              .replace("%2", Integer.toString(range[1]));
            response.add(getCheckIntegrityString(InternalSegment.INTEGRITY_CHECK_ERROR, err));
            endingIDs[idx] = (short)range[1];
          }
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, ex.getMessage()));
      }
    }
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    for (int i = 0; i < startingIDs.length; ++i)
      if (startingIDs[i] != Default_startIDs[i])
        return false;
    for (int i = 0; i < endingIDs.length; ++i)
      if (endingIDs[i] != Default_endIDs[i])
        return false;
    return true;
  }

  public void setIndustryIDSpan(int empire, short min, short max) {
    int index = empire * NUM_TYPES;
    checkIndex(index);

    if (startingIDs[index] != min || endingIDs[index] != max) {
      startingIDs[index] = min;
      endingIDs[index] = max;
      markChanged();
    }
  }

  public void setFoodIDSpan(int empire, short min, short max) {
    int index = empire * NUM_TYPES + 1;
    checkIndex(index);

    if (startingIDs[index] != min || endingIDs[index] != max) {
      startingIDs[index] = min;
      endingIDs[index] = max;
      markChanged();
    }
  }

  public void setEnergyIDSpan(int empire, short min, short max) {
    int index = empire * NUM_TYPES + 2;
    checkIndex(index);

    if (startingIDs[index] != min || endingIDs[index] != max) {
      startingIDs[index] = min;
      endingIDs[index] = max;
      markChanged();
    }
  }

  public void setResearchIDSpan(int empire, short min, short max) {
    int index = empire * NUM_TYPES + 3;
    checkIndex(index);

    if (startingIDs[index] != min || endingIDs[index] != max) {
      startingIDs[index] = min;
      endingIDs[index] = max;
      markChanged();
    }
  }

  @Override
  public void reset() {
    for (int i = 0; i < startingIDs.length; ++i) {
      if (startingIDs[i] != Default_startIDs[i]) {
        startingIDs[i] = Default_startIDs[i];
        markChanged();
      }
    }
    for (int i = 0; i < endingIDs.length; ++i) {
      if (endingIDs[i] != Default_endIDs[i]) {
        endingIDs[i] = Default_endIDs[i];
        markChanged();
      }
    }
  }

  private int mapTypeToBldGroup(int bldType) {
    switch (bldType) {
      case 0:
        return StructureGroup.MainIndustry;
      case 1:
        return StructureGroup.MainFood;
      case 2:
        return StructureGroup.MainEnergy;
      case 3:
        return StructureGroup.MainResearch;
      default:
        throw new IllegalArgumentException();
    }
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= NUM_EMPIRES * NUM_TYPES) {
      String err = "Invalid index calculated from empire id: %1"
        .replace("%1", Integer.toString(index));
      throw new IndexOutOfBoundsException(err);
    }
  }
}