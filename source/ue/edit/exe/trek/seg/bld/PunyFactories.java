package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * @author Alan Podlesek
 */
public class PunyFactories extends InternalSegment {

  public static final String FILENAME = CTrekSegments.PunyFactories;
  public static final int SEGMENT_ADDRESS = 0x0004446F;
  public static final int SIZE = 108; // 20 + 1 + 33 + 54

  private static final int[] Default_PunyFactoryIDs = new int[] {
    88, 134, 180, 226, 272
  };

  private static int TEST_MOV_EAX = 0xB8;

  private int FACT_ID[] = new int[5];
  private byte[] UNKNOWN_2 = new byte[32];
  private byte[] UNKNOWN_3 = new byte[17];
  private byte[] UNKNOWN_4 = new byte[17];
  private byte[] UNKNOWN_5 = new byte[17];

  public PunyFactories() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, PunyFactories.class));
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    for (int emp = 0; emp < CStbof.NUM_EMPIRES; ++emp)
      if (FACT_ID[emp] != Default_PunyFactoryIDs[emp])
        return false;
    return true;
  }

  public void setPunyFactory(int empire, int fact_id) {
    if (FACT_ID[empire] == fact_id) {
      return;
    }
    FACT_ID[empire] = fact_id;
    this.markChanged();
  }

  public int getPunyFactory(int empire) {
    return FACT_ID[empire];
  }

  @Override
  public void reset() {
    for (int emp = 0; emp < CStbof.NUM_EMPIRES; ++emp) {
      if (FACT_ID[emp] != Default_PunyFactoryIDs[emp]) {
        FACT_ID[emp] = Default_PunyFactoryIDs[emp];
        markChanged();
      }
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in, TEST_MOV_EAX);                 // 1
    FACT_ID[0] = StreamTools.readInt(in, true); // 5
    StreamTools.read(in, UNKNOWN_2);            // 37
    validate(in, TEST_MOV_EAX);                 // 38
    FACT_ID[1] = StreamTools.readInt(in, true); // 42
    StreamTools.read(in, UNKNOWN_3);            // 59
    validate(in, TEST_MOV_EAX);                 // 60
    FACT_ID[2] = StreamTools.readInt(in, true); // 64
    StreamTools.read(in, UNKNOWN_4);            // 81
    validate(in, TEST_MOV_EAX);                 // 82
    FACT_ID[3] = StreamTools.readInt(in, true); // 86
    StreamTools.read(in, UNKNOWN_5);            // 103
    validate(in, TEST_MOV_EAX);                 // 104
    FACT_ID[4] = StreamTools.readInt(in, true); // 108

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FACT_ID[0], true));
    out.write(UNKNOWN_2);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FACT_ID[1], true));
    out.write(UNKNOWN_3);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FACT_ID[2], true));
    out.write(UNKNOWN_4);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FACT_ID[3], true));
    out.write(UNKNOWN_5);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FACT_ID[4], true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFactoryIDs[0], true));
    out.write(UNKNOWN_2);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFactoryIDs[1], true));
    out.write(UNKNOWN_3);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFactoryIDs[2], true));
    out.write(UNKNOWN_4);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFactoryIDs[3], true));
    out.write(UNKNOWN_5);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFactoryIDs[4], true));
  }

  @Override
  public void clear() {
    Arrays.fill(FACT_ID, 0);
    Arrays.fill(UNKNOWN_2, (byte)0);
    Arrays.fill(UNKNOWN_3, (byte)0);
    Arrays.fill(UNKNOWN_4, (byte)0);
    Arrays.fill(UNKNOWN_5, (byte)0);
    markChanged();
  }
}
