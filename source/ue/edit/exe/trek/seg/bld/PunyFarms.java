package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * @author Alan Podlesek
 */
public class PunyFarms extends InternalSegment {

  public static final String FILENAME = CTrekSegments.PunyFarms;
  public static final int SEGMENT_ADDRESS = 0x0004452F;
  public static final int SIZE = 108; // 20 + 1 + 33 + 54

  private static final int[] Default_PunyFarmIDs = new int[] {
    98, 144, 190, 236, 282
  };

  private static int TEST_MOV_EAX = 0xB8;

  private int FARM_ID[] = new int[5];
  private byte[] UNKNOWN_2 = new byte[32];
  private byte[] UNKNOWN_3 = new byte[17];
  private byte[] UNKNOWN_4 = new byte[17];
  private byte[] UNKNOWN_5 = new byte[17];

  public PunyFarms() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, PunyFarms.class));
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    for (int emp = 0; emp < CStbof.NUM_EMPIRES; ++emp)
      if (FARM_ID[emp] != Default_PunyFarmIDs[emp])
        return false;
    return true;
  }

  public void setPunyFarm(int empire, int farm_id) {
    if (FARM_ID[empire] != farm_id) {
      FARM_ID[empire] = farm_id;
      markChanged();
    }
  }

  public int getPunyFarm(int empire) {
    return FARM_ID[empire];
  }

  @Override
  public void reset() {
    for (int emp = 0; emp < CStbof.NUM_EMPIRES; ++emp) {
      if (FARM_ID[emp] != Default_PunyFarmIDs[emp]) {
        FARM_ID[emp] = Default_PunyFarmIDs[emp];
        markChanged();
      }
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    validate(in.read(), TEST_MOV_EAX);          // 1
    FARM_ID[0] = StreamTools.readInt(in, true); // 5
    StreamTools.read(in, UNKNOWN_2);            // 37
    validate(in, TEST_MOV_EAX);                 // 38
    FARM_ID[1] = StreamTools.readInt(in, true); // 42
    StreamTools.read(in, UNKNOWN_3);            // 59
    validate(in, TEST_MOV_EAX);                 // 60
    FARM_ID[2] = StreamTools.readInt(in, true); // 64
    StreamTools.read(in, UNKNOWN_4);            // 81
    validate(in, TEST_MOV_EAX);                 // 82
    FARM_ID[3] = StreamTools.readInt(in, true); // 86
    StreamTools.read(in, UNKNOWN_5);            // 103
    validate(in, TEST_MOV_EAX);                 // 104
    FARM_ID[4] = StreamTools.readInt(in, true); // 108

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FARM_ID[0], true));
    out.write(UNKNOWN_2);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FARM_ID[1], true));
    out.write(UNKNOWN_3);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FARM_ID[2], true));
    out.write(UNKNOWN_4);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FARM_ID[3], true));
    out.write(UNKNOWN_5);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(FARM_ID[4], true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFarmIDs[0], true));
    out.write(UNKNOWN_2);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFarmIDs[1], true));
    out.write(UNKNOWN_3);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFarmIDs[2], true));
    out.write(UNKNOWN_4);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFarmIDs[3], true));
    out.write(UNKNOWN_5);
    out.write(TEST_MOV_EAX);
    out.write(DataTools.toByte(Default_PunyFarmIDs[4], true));
  }

  @Override
  public void clear() {
    Arrays.fill(FARM_ID, 0);
    Arrays.fill(UNKNOWN_2, (byte)0);
    Arrays.fill(UNKNOWN_3, (byte)0);
    Arrays.fill(UNKNOWN_4, (byte)0);
    Arrays.fill(UNKNOWN_5, (byte)0);
    markChanged();
  }
}
