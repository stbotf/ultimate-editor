package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import lombok.val;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.common.MultiSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_BldId_MartialLaw;
import ue.exception.InvalidArgumentsException;

/**
 * MultiSegment of the martial law buildings.
 */
public class MartialLawBld extends MultiSegment {

  public static final String NAME = SD_BldId_MartialLaw.Prefix;

  // segments
  private List<MovRegInt>     seg_ml1;
  private List<MovRegInt>     seg_ml2;
  private MartialLawBldIdFix  seg_ml3;
  private List<MovRegInt>     seg_ml4;

  public MartialLawBld(Trek trek) {
    super(NAME, trek);
  }

  @Override
  public boolean hasSegment(String name) {
    return name.startsWith(NAME);
  }

  @Override
  public String[] listSegmentNames() {
    return SD_BldId_MartialLaw.Names;
  }

  @Override
  public List<InternalSegment> listSegments() {
    val list = new ArrayList<InternalSegment>();
    list.addAll(seg_ml1);
    list.addAll(seg_ml2);
    list.add(seg_ml3);
    list.addAll(seg_ml4);
    return list;
  }

  @Override
  public boolean isLoaded() throws IOException {
    return seg_ml1 != null;
  }

  @Override
  public void reset() {
    seg_ml1 = null;
    seg_ml2 = null;
    seg_ml3 = null;
    seg_ml4 = null;
  }

  @Override
  public void reload() throws IOException {
    seg_ml1 = loadSegments(MovRegInt.class, SD_BldId_MartialLaw.ML1, true);
    seg_ml2 = loadSegments(MovRegInt.class, SD_BldId_MartialLaw.ML2, true);
    seg_ml3 = (MartialLawBldIdFix) trek.getSegment(SD_BldId_MartialLaw.ML3, true);
    seg_ml4 = loadSegments(MovRegInt.class, SD_BldId_MartialLaw.ML4, true);
  }

  public short getID(int empire) {
    return seg_ml1.get(empire).shortValue();
  }

  public boolean setID(int empire, short id) throws InvalidArgumentsException {
    if (seg_ml1.get(empire).setValue(id)) {
      seg_ml2.get(empire).setValue(id);
      seg_ml3.setID(empire, id);
      seg_ml4.get(empire).setValue(id);
      return true;
    }
    return false;
  }

  @Override
  public void check(Vector<String> response) {
    for (val seg : seg_ml1)
      seg.check(response);
    for (val seg : seg_ml2)
      seg.check(response);
    seg_ml3.check(response);
    for (val seg : seg_ml4)
      seg.check(response);
  }
}
