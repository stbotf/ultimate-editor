package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.sd.SD_BldId_Shipyard;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.exception.InvalidArgumentsException;

/**
 * Shipyard ids
 */
public class ShipyardBldIDs extends InternalSegment {

  public static final int NUM_SHIPYARDS = CStbof.NUM_EMPIRES + 1;
  public static final int SIZE = NUM_SHIPYARDS * (MovRegInt.SIZE + 1);

  private static final int RET = 0xC3;

  private MovRegInt[] values = new MovRegInt[NUM_SHIPYARDS];

  public ShipyardBldIDs(SegmentDefinition seg) {
    super(seg);
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int i = 0; i < values.length; i++) {
      values[i] = new MovRegInt(SD_BldId_Shipyard.Sub[i]);
      values[i].load(in);
      validate(in.read(), RET);
    }
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < values.length; i++) {
      values[i].save(out);
      out.write(RET);
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < values.length; i++) {
      values[i].saveDefault(out);
      out.write(RET);
    }
  }

  @Override
  public void clear() {
    Arrays.fill(values, null);
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    for (int i = 0; i < values.length; ++i)
      if (!values[i].isDefault())
        return false;
    return true;
  }

  public void setID(int empire, int id) throws InvalidArgumentsException {
    if (values[empire].setValue(id))
      markChanged();
  }

  public int getID(int empire) {
    return values[empire].intValue();
  }

  @Override
  public void markSaved() {
    super.markSaved();
    for (int i = 0; i < values.length; ++i)
      values[i].markSaved();
  }

  @Override
  public void reset() {
    for (int i = 0; i < values.length; ++i) {
      if (!values[i].isDefault()) {
        values[i].reset();
        markChanged();
      }
    }
  }
}
