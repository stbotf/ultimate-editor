package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Main building upgrade path, listing the subsequent building upgrades.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=264&t=2299
 */
public class MainBldUpgrades extends InternalSegment {

  public static final String FILENAME = CTrekSegments.MainBldUpgrades;
  public static final int SEGMENT_ADDRESS = 0x0018EDE8; // asm_590FE8
  public static final int SIZE = 450; // 2 * 5 * 5 * 9
  public static final int NUM_UPGRADES = 9;

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // 5 empires * 9 upgrades
  public static short[][] Default_FoodIds = new short[][] {
    {  98,  99, 100, 101, 102, 103, 104, 105, 106 }, // Cardassians
    { 144, 145, 146, 147, 148, 149, 150, 151, 152 }, // Federation
    { 190, 191, 192, 193, 194, 195, 196, 197, 198 }, // Ferengi
    { 236, 237, 238, 239, 240, 241, 242, 243, 244 }, // Klingons
    { 282, 283, 284, 285, 286, 287, 288, 289, 290 }, // Romulans
  };

  // 5 empires * 9 upgrades
  public static short[][] Default_IndustryIds = new short[][] {
    {  88,  89,  90,  91,  92,  93,  94,  95,  96 }, // Cardassians
    { 134, 135, 136, 137, 138, 139, 140, 141, 142 }, // Federation
    { 180, 181, 182, 183, 184, 185, 186, 187, 188 }, // Ferengi
    { 226, 227, 228, 229, 230, 231, 232, 233, 234 }, // Klingons
    { 272, 273, 274, 275, 276, 277, 278, 279, 280 }, // Romulans
  };

  // 5 empires * 9 upgrades
  public static short[][] Default_EnergyIds = new short[][] {
    { 116, 117, 118, 119, 120, 121, 122, 123, 124 }, // Cardassians
    { 162, 163, 164, 165, 166, 167, 168, 169, 170 }, // Federation
    { 208, 209, 210, 211, 212, 213, 214, 215, 216 }, // Ferengi
    { 254, 255, 256, 257, 258, 259, 260, 261, 262 }, // Klingons
    { 300, 301, 302, 303, 304, 305, 306, 307, 308 }, // Romulans
  };

  // 5 empires * 9 upgrades
  public static short[][] Default_ScienceIds = new short[][] {
    { 125, 126, 127, 128, 129, 130, 131, 132, 133 }, // Cardassians
    { 171, 172, 173, 174, 175, 176, 177, 178, 179 }, // Federation
    { 217, 218, 219, 220, 221, 222, 223, 224, 225 }, // Ferengi
    { 263, 264, 265, 266, 267, 268, 269, 270, 271 }, // Klingons
    { 309, 310, 311, 312, 313, 314, 315, 316, 317 }, // Romulans
  };

  // 5 empires * 9 upgrades
  public static short[][] Default_IntelIds = new short[][] {
    { 107, 108, 109, 110, 111, 112, 113, 114, 115 }, // Cardassians
    { 153, 154, 155, 156, 157, 158, 159, 160, 161 }, // Federation
    { 199, 200, 201, 202, 203, 204, 205, 206, 207 }, // Ferengi
    { 245, 246, 247, 248, 249, 250, 251, 252, 253 }, // Klingons
    { 291, 292, 293, 294, 295, 296, 297, 298, 299 }, // Romulans
  };

  private short[][] food_ids = new short[NUM_EMPIRES][NUM_UPGRADES];      // 0x0018EDE8 / asm_590FE8
  private short[][] industry_ids = new short[NUM_EMPIRES][NUM_UPGRADES];  // 0x0018EE42 / asm_591042 (+5A = 90 = 2*5*9)
  private short[][] energy_ids = new short[NUM_EMPIRES][NUM_UPGRADES];    // 0x0018EE9C / asm_59109C (+5A)
  private short[][] science_ids = new short[NUM_EMPIRES][NUM_UPGRADES];   // 0x0018EEF6 / asm_5910F6 (+5A)
  private short[][] intel_ids = new short[NUM_EMPIRES][NUM_UPGRADES];     // 0x0018EF50 / asm_591150 (+5A)

  public MainBldUpgrades() {
    super(new SegmentDefinition(SEGMENT_ADDRESS, null, MainBldUpgrades.class));
  }

  @Override
  public boolean isDefault() {
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      for (int upg = 0; upg < NUM_UPGRADES; ++upg) {
        if (food_ids[emp][upg] != Default_FoodIds[emp][upg])
          return false;
        if (industry_ids[emp][upg] != Default_IndustryIds[emp][upg])
          return false;
        if (energy_ids[emp][upg] != Default_EnergyIds[emp][upg])
          return false;
        if (science_ids[emp][upg] != Default_ScienceIds[emp][upg])
          return false;
        if (intel_ids[emp][upg] != Default_IntelIds[emp][upg])
          return false;
      }
    }
    return true;
  }

  @Override
  public void reset() {
    for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
      for (int upg = 0; upg < NUM_UPGRADES; ++upg) {
        if (food_ids[emp][upg] != Default_FoodIds[emp][upg]) {
          food_ids[emp][upg] = Default_FoodIds[emp][upg];
          markChanged();
        }
        if (industry_ids[emp][upg] != Default_IndustryIds[emp][upg]) {
          industry_ids[emp][upg] = Default_IndustryIds[emp][upg];
          markChanged();
        }
        if (energy_ids[emp][upg] != Default_EnergyIds[emp][upg]) {
          energy_ids[emp][upg] = Default_EnergyIds[emp][upg];
          markChanged();
        }
        if (science_ids[emp][upg] != Default_ScienceIds[emp][upg]) {
          science_ids[emp][upg] = Default_ScienceIds[emp][upg];
          markChanged();
        }
        if (intel_ids[emp][upg] != Default_IntelIds[emp][upg]) {
          intel_ids[emp][upg] = Default_IntelIds[emp][upg];
          markChanged();
        }
      }
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int i = 0; i < NUM_UPGRADES; i++) {
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        food_ids[emp][i] = StreamTools.readShort(in, true);
      }
    }
    for (int i = 0; i < NUM_UPGRADES; i++) {
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        industry_ids[emp][i] = StreamTools.readShort(in, true);
      }
    }
    for (int i = 0; i < NUM_UPGRADES; i++) {
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        energy_ids[emp][i] = StreamTools.readShort(in, true);
      }
    }
    for (int i = 0; i < NUM_UPGRADES; i++) {
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        science_ids[emp][i] = StreamTools.readShort(in, true);
      }
    }
    for (int i = 0; i < NUM_UPGRADES; i++) {
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        intel_ids[emp][i] = StreamTools.readShort(in, true);
      }
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(food_ids[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(industry_ids[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(energy_ids[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(science_ids[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(intel_ids[emp][i], true));
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(Default_FoodIds[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(Default_IndustryIds[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(Default_EnergyIds[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(Default_ScienceIds[emp][i], true));
    for (int i = 0; i < NUM_UPGRADES; i++)
      for (int emp = 0; emp < NUM_EMPIRES; emp++)
        out.write(DataTools.toByte(Default_IntelIds[emp][i], true));
  }

  @Override
  public void clear() {
    for (short[] ids : food_ids)
      Arrays.fill(ids, (short)0);
    for (short[] ids : industry_ids)
      Arrays.fill(ids, (short)0);
    for (short[] ids : energy_ids)
      Arrays.fill(ids, (short)0);
    for (short[] ids : science_ids)
      Arrays.fill(ids, (short)0);
    for (short[] ids : intel_ids)
      Arrays.fill(ids, (short)0);
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  /**
   * @param empire 0 to 4
   * @param index  upgrade index 0 to 8
   * @param id
   */
  public void setFoodID(int empire, int index, short id) {
    if (food_ids[empire][index] != id) {
      food_ids[empire][index] = id;
      markChanged();
    }
  }

  /**
   * @param empire 0 to 4
   * @param index  upgrade index 0 to 8
   * @param id
   */
  public void setIndustryID(int empire, int index, short id) {
    if (industry_ids[empire][index] != id) {
      industry_ids[empire][index] = id;
      markChanged();
    }
  }

  /**
   * @param empire 0 to 4
   * @param index  upgrade index 0 to 8
   * @param id
   */
  public void setEnergyID(int empire, int index, short id) {
    if (energy_ids[empire][index] != id) {
      energy_ids[empire][index] = id;
      markChanged();
    }
  }

  /**
   * @param empire 0 to 4
   * @param index  upgrade index 0 to 8
   * @param id
   */
  public void setScienceID(int empire, int index, short id) {
    if (science_ids[empire][index] != id) {
      science_ids[empire][index] = id;
      markChanged();
    }
  }

  /**
   * @param empire 0 to 4
   * @param index  upgrade index 0 to 8
   * @param id
   */
  public void setIntelID(int empire, int index, short id) {
    if (intel_ids[empire][index] != id) {
      intel_ids[empire][index] = id;
      markChanged();
    }
  }
}
