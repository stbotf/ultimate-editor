package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.sd.SD_BldId_MinorSp;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.exception.InvalidArgumentsException;

/**
 * Minor race special building ids
 */
public class MinorRaceSpBuildingIDs extends InternalSegment {

  public static final int SIZE = 180; // 30 + 150

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_MINORS = CStbof.NUM_MINORS;

  private static final int RET = 0xC3;

  // the special building ids, sorted by race
  private MovRegInt[] values = new MovRegInt[NUM_MINORS];

  public MinorRaceSpBuildingIDs(SegmentDefinition sd) {
    super(sd);
  }

  @Override
  public void load(InputStream in) throws IOException {
    for (int i = 0; i < NUM_MINORS; ++i) {
      // sort values by race
      int idx = toRaceIdx(i);
      values[idx] = new MovRegInt(SD_BldId_MinorSp.Sub[i]);
      values[idx].load(in);
      int b1 = in.read();
      validate(b1, RET);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < values.length; ++i) {
      int idx = toRaceIdx(i);
      values[idx].save(out);
      out.write(RET);
    }
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    for (int i = 0; i < values.length; ++i) {
      int idx = toRaceIdx(i);
      values[idx].saveDefault(out);
      out.write(RET);
    }
  }

  @Override
  public void clear() {
    Arrays.fill(values, null);
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    for (int min = 0; min < NUM_MINORS; ++min)
      if (!values[min].isDefault())
        return false;
    return true;
  }

  public void setID(int race, int id) throws InvalidArgumentsException {
    if (race < NUM_EMPIRES || race > 34) {
      throw new IndexOutOfBoundsException(
        "Invalid minor race id: %1".replace("%1", Integer.toString(race)));
    }

    race = race - NUM_EMPIRES;
    values[race].setValue(id);

    if (values[race].madeChanges())
      markChanged();
  }

  public int getID(int race) {
    if (race < NUM_EMPIRES || race > 34) {
      String err = "Invalid minor race id: %1".replace("%1", Integer.toString(race));
      throw new IndexOutOfBoundsException(err);
    }

    race = race - NUM_EMPIRES;
    return values[race].intValue();
  }

  @Override
  public void reset() {
    for (int min = 0; min < NUM_MINORS; ++min) {
      if (!values[min].isDefault()) {
        values[min].reset();
        markChanged();
      }
    }
  }

  // unfortunetly the entries aren't in order
  private int toRaceIdx(int codeIdx) {
    // fix Ullian Psychohistorical Archive index,
    // which is out of sequence, from 26 to 14
    int bldIdx = codeIdx < 14 || codeIdx > 26 ? codeIdx
      : codeIdx == 14 ? 26 : codeIdx-1;

    // fix swapped Antedean and Antican building index
    int raceIdx = bldIdx > 4 || bldIdx < 3 ? bldIdx
      : bldIdx == 3 ? 4 : 3;

    return raceIdx;
  }
}