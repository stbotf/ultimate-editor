package ue.edit.exe.trek.seg.bld;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import ue.edit.exe.common.OpCode;
import ue.edit.exe.common.Patch;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.CmpRegShort;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.MovRegShort;
import ue.edit.exe.seg.prim.PopReg;
import ue.edit.exe.trek.sd.SD_BldId_MartialLaw;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.res.stbof.common.CStbof;
import ue.exception.InvalidArgumentsException;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Martial law building id fixture segment to set the empires martial law building ids and automatically
 * patch the compare instructions from signed byte value to short if default maximum of 127 is exceeded.
 */
public class MartialLawBldIdFix extends InternalSegment implements Patch {

  public static final int ADDRESS = 0x4C4DD;
  public static final int SIZE = 53;

  public static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  public static final short[] DEFAULTS = SD_BldId_MartialLaw.DefaultIDs;

  private static final byte[] ReturnCode = new byte[] {       // 9
    MovRegInt.EAX, 0x01, 0x00, 0x00, 0x00, PopReg.EDI, PopReg.ESI, PopReg.ECX, OpCode.RETN
  };

  private static final byte[] SkipCodeCheck = new byte[] {    // 5
    CmpRegByte.ESI[0], CmpRegByte.ESI[1], 7, JmpShort.JNZ, 39
  };

  private static final byte[] MumExtPatch = new byte[] {      // 5
    Jmp.JMP, 0x15, (byte)0xDB, 0x23, 0
  };

  private static final byte[] PatchedJmpToEnd = new byte[] {  // 4
    JmpShort.JMP, -88, NOP, NOP
  };

  private short[] ids = new short[NUM_EMPIRES];
  @Getter private boolean patched = false;

  public MartialLawBldIdFix(SegmentDefinition def) {
    super(def);
  }

  @Override
  public void load(InputStream in) throws IOException {
    // validate preceding return, required to increase asm space
    // for the unsigned byte compare fixture
    validate(in, ReturnCode);                               // 9

    // accept custom MUM patch to sneak in the improved code fixture
    byte[] prevSkip = StreamTools.readBytes(in, 5);
    if (!check(prevSkip, MumExtPatch)) {
      // validate unchanged intermediate code
      validate(prevSkip, SkipCodeCheck);                       // 5
    }

    // cmp short value: 0x66 0x83 0xFA
    // cmp byte value:  0x66 0x83 0xFA
    byte[] idSwitch = StreamTools.readBytes(in, 39);        // 39

    // check for CmpRegShort fixture
    if (idSwitch[1] == 0x81) {
      // read full cmp short patch
      for (int i = 0; i < NUM_EMPIRES; ++i) {
        int idx = i * 7;
        validate(idSwitch, idx, CmpRegShort.DX);            // 3

        // switch federation and ferengi martial law ids
        int emp = empOfPos(i);
        ids[emp] = DataTools.toShort(idSwitch, idx+3, true);  // 2

        // reuse preceding return
        validate(idSwitch, idx+5, JmpShort.JZ);             // 1
        byte jmpOffset = (byte)(-21 - idx);
        validate(idSwitch, idx+6, jmpOffset);               // 1
      }

      // jmp to end
      validate(idSwitch, 35, PatchedJmpToEnd);              // 4
      patched = true;
    } else if (check(idSwitch, 25, MovRegShort.BX)) {
      // read MUM partial cmp short patch
      // TODO: Cleanup sneaked fixture once MUM is patched,
      //       including SD_BldId_MartialLaw description.
      for (int i = 0; i < 3; ++i) {
        int idx = i * 6;
        validate(idSwitch, idx, CmpRegByte.DX);             // 3

        // switch federation and ferengi martial law ids
        int emp = empOfPos(i);
        ids[emp] = idSwitch[idx+3];                         // 1

        validate(idSwitch, idx+4, JmpShort.JZ);             // 1
        byte jmpOffset = (byte)(-20 - idx);
        validate(idSwitch, idx+5, jmpOffset);               // 1
      }

      validate(idSwitch, 18, CmpRegShort.DX);               // 3
      ids[3] = DataTools.toShort(idSwitch, 21, true);       // 2
      validate(idSwitch, 23, JmpShort.JZ);                  // 1
      validate(idSwitch, 24, (byte)-39);                    // 1

      validate(idSwitch, 25, MovRegShort.BX);               // 2
      ids[4] = DataTools.toShort(idSwitch, 27, true);       // 2
      validate(idSwitch, 29, OpCode.CMP_BX_DX);             // 3
      validate(idSwitch, 32, JmpShort.JZ);                  // 1
      validate(idSwitch, 33, (byte)-48);                    // 1
      validate(idSwitch, 34, JmpShort.JMP);                 // 1
      validate(idSwitch, 35, (byte)-87);                    // 1
      validate(idSwitch, 36, 3, NOP);                       // 3
      patched = true;
    } else {
      // read unpatched
      for (int i = 0; i < NUM_EMPIRES; ++i) {               // 5*6 = 30
        int idx = i * 6;
        validate(idSwitch, idx, CmpRegByte.DX);             // 3

        // switch federation and ferengi martial law ids
        int emp = empOfPos(i);
        ids[emp] = idSwitch[idx+3];                         // 1

        if (i < 4) {
          validate(idSwitch, idx+4, JmpShort.JZ);           // 1
          byte jmpOffset = (byte)(24 - idx);
          validate(idSwitch, idx+5, jmpOffset);             // 1
        } else {
          // jmp to end
          validate(idSwitch, idx+4, JmpShort.JNZ);          // 1
          validate(idSwitch, idx+5, (byte)-81);             // 1
        }
      }

      // validate original return
      validate(idSwitch, 30, ReturnCode);                   // 9
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    save(out, ids, patched);
  }

  @Override
  public void saveDefault(OutputStream out) throws IOException {
    save(out, DEFAULTS, false);
  }

  @Override
  public int getSize() {
    return SIZE;
  }

  @Override
  public boolean isDefault() {
    return !patched && Arrays.equals(ids, DEFAULTS);
  }

  @Override
  public void patch() {
    if (!patched) {
      patched = true;
      markChanged();
    }
  }

  @Override
  public void unpatch() throws IOException {
    if (patched) {
      for (short id : ids) {
        if (id > 127) {
          throw new IOException("The patch can't be unapplied because it contains an ID"
            + " that exceeds the default max signed byte value limit of 127!");
        }
      }

      patched = false;
      markChanged();
    }
  }

  public short getID(int key) {
    return ids[key];
  }

  public boolean setID(int key, short id) throws InvalidArgumentsException {
    if (ids[key] != id) {
      // apply patch when max signed byte value is exceeded
      // the compare instruction converts ids to short type values, which
      // is why the jump zero instruction is limited to ids of 127 as well
      if (id > 127)
        patch();

      ids[key] = id;
      markChanged();
      return true;
    }
    return false;
  }

  @Override
  public void reset() {
    if (patched) {
      patched = false;
      markChanged();
    }
    for (int i = 0; i < DEFAULTS.length; ++i) {
      if (ids[i] != DEFAULTS[i]) {
        ids[i] = DEFAULTS[i];
        markChanged();
      }
    }
  }

  @Override
  public void clear() {
    ids = Arrays.copyOf(DEFAULTS, DEFAULTS.length);
    patched = false;
  }

  @Override
  public void check(Vector<String> response) {
    // is already validated on load
  }

  // switch federation and ferengi martial law ids
  private static int empOfPos(int pos) {
    switch (pos) {
      case CStbof.Race.Fed:
        return CStbof.Race.Ferg;
      case CStbof.Race.Ferg:
        return CStbof.Race.Fed;
      default:
        return pos;
    }
  }

  private static void save(OutputStream out, short[] ids, boolean patched) throws IOException {
    // write re-used preceding return
    out.write(ReturnCode);                // 9
    // write unchanged intermediate code
    out.write(SkipCodeCheck);          // 5

    if (patched) {
      for (int i = 0; i < NUM_EMPIRES; ++i) {
        // cmp short value: 0x66 0x83 0xFA
        out.write(CmpRegShort.DX);        // 3

        // switch federation and ferengi martial law ids
        int emp = empOfPos(i);
        out.write(DataTools.toByte(ids[emp], true));  // 2

        // reuse preceding return
        out.write(JmpShort.JZ);           // 1
        out.write(-21 - i*7);             // 1
      }

      // jmp to end
      out.write(PatchedJmpToEnd);         // 4
    } else {
      for (int i = 0; i < NUM_EMPIRES; ++i) {
        // cmp byte value: 0x66 0x83 0xFA
        out.write(CmpRegByte.DX);         // 3

        // switch federation and ferengi martial law ids
        int emp = empOfPos(i);
        out.write(ids[emp]);              // 1

        if (i == 4) {
          // jmp to end
          out.write(JmpShort.JNZ);        // 1
          out.write(-81);                 // 1
        } else {
          out.write(JmpShort.JZ);         // 1
          out.write(24 - i*6);            // 1
        }
      }

      // write original return
      out.write(ReturnCode);              // 9
    }
  }
}
