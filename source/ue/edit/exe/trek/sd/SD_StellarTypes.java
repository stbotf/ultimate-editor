package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.trek.seg.map.StellTypeDetailsDescMap;
import ue.edit.exe.trek.seg.map.StellTypeDetailsNameMap;
import ue.edit.exe.trek.seg.map.StellTypeDetailsNameMumFix;
import ue.edit.exe.trek.seg.map.StellTypeGen;
import ue.edit.exe.trek.seg.map.StellTypeGenGALMFix;
import ue.edit.exe.trek.seg.map.StellTypeTooltipGALMFix;
import ue.edit.exe.trek.seg.map.StellTypeTooltipGALMFix2;
import ue.edit.exe.trek.seg.map.StellTypeTooltipNameMap;

/**
 * This interface holds stellar type segment definitions.
 */
public interface SD_StellarTypes {

  public static final String Prefix = "stellType"; //$NON-NLS-1$
  public static final String PrefixNum = Prefix + "_num"; //$NON-NLS-1$
  public static final String PrefixLast = Prefix + "_lastId"; //$NON-NLS-1$

  // unpatched vanilla number of stellar space objects
  public static final int DEFAULT_NUM = 12;
  public static final int MAX_NUM = Integer.min(
    StellTypeDetailsNameMap.MaxSpaceObjects, StellTypeDetailsDescMap.MaxSpaceObjects);

  public static final ValueDefinition<?>[] LastNameIds = Stream.of(
    new ValueDefinition<Byte>(0x000B58CF, PrefixLast, CmpRegByte.class, Register.EAX, (byte)11)
  ).map(x -> x.desc("Number of stellar space object names. (byte)"))
    .toArray(ValueDefinition<?>[]::new);

  public static final SegmentDefinition Gen               = /*0x000AFB34, asm_4B0734*/ StellTypeGen.SEGMENT_DEFINITION;
  public static final SegmentDefinition GenGALMFix        = /*0x000AF924, asm_4B0524*/ StellTypeGenGALMFix.SEGMENT_DEFINITION;
  public static final SegmentDefinition TooltipGALMFix    = /*0x000DE74D, asm_4DF34D*/ StellTypeTooltipGALMFix.SEGMENT_DEFINITION;
  public static final SegmentDefinition TooltipGALMFix2   = /*0x000DECEE, asm_4DF8EE*/ StellTypeTooltipGALMFix2.SEGMENT_DEFINITION;
  public static final SegmentDefinition TooltipNameMap    = /*0x0003CBCC, asm_43D7CC*/ StellTypeTooltipNameMap.SEGMENT_DEFINITION;
  public static final SegmentDefinition DetailsNameMumFix = /*0x000B5824, asm_4B6424*/ StellTypeDetailsNameMumFix.SEGMENT_DEFINITION;
  public static final SegmentDefinition DetailsNameMap    = /*0x000B5C27, asm_4B6827*/ StellTypeDetailsNameMap.SEGMENT_DEFINITION;
  public static final SegmentDefinition DetailsDescMap    = /*0x000FA694, asm_4FB294*/ StellTypeDetailsDescMap.SEGMENT_DEFINITION;

  public static final SegmentDefinition[] All = Stream.of(
    Arrays.stream(LastNameIds),
    Stream.of(
      Gen,
      GenGALMFix,
      TooltipGALMFix,
      TooltipGALMFix2,
      DetailsNameMumFix,
      TooltipNameMap,
      DetailsNameMap,
      DetailsDescMap
    )
  ).flatMap(Function.identity()).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
