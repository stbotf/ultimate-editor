package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * This interface holds file name segment definitions.
 */
public interface SD_FileNames {

  public static final String Prefix = "fileName"; //$NON-NLS-1$

  // #region Edifice Image Names

  // trek.exe[0x1801B4] %s_30.tga   30x30   *senergy.wdf  <2410..6, 2430..6>      energy screen listing (modded %s_50.tga)
  public static final StringDefinition BldEngyIcon =
    new StringDefinition(0x1801B4, Prefix + "_bldEngyIcon", "%s_30.tga", 12)
      .desc("*senergy.wdf [2410..] for the energy screen icons (30x30 pixel)");
  // trek.exe[0x18023C] %s_30.tga   30x30   *buildq.wdf   <2049>                  queued build task (modded %s_50.tga)
  public static final StringDefinition BldQueIcon =
    new StringDefinition(0x18023C, Prefix + "_bldQueIcon", "%s_30.tga", 12)
      .desc("*buildq.wdf [2049] for the build queue icons (30x30 pixel)");
  // trek.exe[0x180384] %s_60.tga   60x50   *solcur.wdf   <2103,_7,12,17,22,27>   already built structures
  public static final StringDefinition BldSysImg =
    new StringDefinition(0x180384, Prefix + "_bldSysImg", "%s_60.tga", 12)
      .desc("*solcur.wdf [2103..] for the already built system structures (60x50 pixel)");
  // trek.exe[0x180214] %s_60.tga   60x50   *senergy.wdf  <2464>                  built orbital battery
  public static final StringDefinition BldOrbImg =
    new StringDefinition(0x180214, Prefix + "_bldQueImg", "%s_60.tga", 12)
      .desc("*senergy.wdf [2464] for the oribital structure image (60x50 pixel)");
  // trek.exe[0x180194] %s_60.tga   60x50   *labor.wdf    <2300,_10,20,30,40>     population assignment
  public static final StringDefinition BldPopImg =
    new StringDefinition(0x180194, Prefix + "_bldPopImg", "%s_60.tga", 12)
      .desc("*labor.wdf [2300..] for the main building population assignment (60x50 pixel)");
  // trek.exe[0x180220] %s.tga      120x100 *solars.wdf   <2010>                  current consumable build task (martial law)
  public static final StringDefinition BldConsImg =
    new StringDefinition(0x180220, Prefix + "_bldConsImg", "%s_120.tga", 8)
      .desc("*solars.wdf [2010] for the consumable martial law & trade goods current build task image (30x30 pixel)");
  // trek.exe[0x180248] %sw.tga     120x100 *solars.wdf   <2010>                  current build task wireframe
  public static final StringDefinition BldTskImg =
    new StringDefinition(0x180248, Prefix + "_bldTskImg", "%sw.tga", 8)
      .desc("*solars.wdf [2010] current build task wireframe (120x100 pixel)");
  // trek.exe[0x17E5DC] %s170.tga   170x142 *d_rinfo.wdf  <5251>                  race info special structure
  public static final StringDefinition BldRaceInfoImg =
    new StringDefinition(0x17E5DC, Prefix + "_bldRaceInfoImg", "%s170.tga", 12)
      .desc("*d_rinfo.wdf [5251] special structure image for the race details (170x142 pixel)");
  // trek.exe[0x180228] %s170.tga   170x142 *solbld.wdf   <2202>                  build selection details
  public static final StringDefinition BldSelImg =
    new StringDefinition(0x180228, Prefix + "_bldSelImg", "%s170.tga", 12)
      .desc("*solbld.wdf [2202] build selection details image (170x142 pixel)");
  // trek.exe[0x17EC34] %s_b.tga    270x225 *techost.wdf  <3320>                  tech database details (modded %s_c.tga)
  public static final StringDefinition BldTecSelImg =
    new StringDefinition(0x17EC34, Prefix + "_bldTecSelImg", "%s_b.tga", 12)
      .desc("*techost.wdf [3320] tech database image for the structure details (270x225 pixel)");

  // #endregion Edifice Image Names

  // #region Ship Image Names

  // trek.exe[0x17D058] i_%s30.tga  30x30   *_ship.wdf    <1213>                  combat ship list
  public static final StringDefinition ShipCbtIcon =
    new StringDefinition(0x17D058, Prefix + "_shipCbtIcon", "i_%s30.tga", 12)
      .desc("*_ship.wdf [1213] ship combat icon (30x30 pixel)");
  // trek.exe[0x17FC50] i_%s30.tga  30x30   *bshpinf.wdf  <6552>                  bottom task force ship list
  public static final StringDefinition ShipListIcon =
    new StringDefinition(0x17FC50, Prefix + "_shipListIcon", "i_%s30.tga", 12)
      .desc("*bshpinf.wdf [6552] task force ship icon (30x30 pixel)");
  // trek.exe[0x1807FC] i_%s30.tga  30x30   *bshpinf.wdf  <6552>                  bottom task force ship list on redeployment
  public static final StringDefinition ShipRdplIcon =
    new StringDefinition(0x1807FC, Prefix + "_shipRdplIcon", "i_%s30.tga", 12)
      .desc("*bshpinf.wdf [6552] ship info icon (30x30 pixel)");
  // trek.exe[0x180250] i_%s30.tga  30x30   *buildq.wdf   <2049>                  queued build task
  public static final StringDefinition ShipBldQueIcon =
    new StringDefinition(0x180250, Prefix + "_shipBldQueIcon", "i_%s30.tga", 12)
      .desc("*buildq.wdf [2049] ship build queue icon (30x30 pixel)");
  // trek.exe[0x17ED28] i_%s60.tga  60x50   *techl.wdf    <3401..4,_13,15>        tech level unlockings
  public static final StringDefinition ShipTecLvlImg =
    new StringDefinition(0x17ED28, Prefix + "_shipTecLvlImg", "i_%s60.tga", 12)
      .desc("*techl.wdf [3401..] tech level unlock list image (60x50 pixel)");
  // trek.exe[0x18025C] i_%s60.tga  60x50   *solcur.wdf   <2103,_7,12,17,22,27>   already built ships (unused)
  // trek.exe[0x1807D4] i_%s60.tga  60x50   *sd_ship.wdf  <1213>                  task force redeployment list
  public static final StringDefinition ShipRdplImg =
    new StringDefinition(0x1807D4, Prefix + "_shipRdplImg", "i_%s60.tga", 12)
      .desc("*sd_ship.wdf [1213] ship redeploy image (60x50 pixel)");
  // trek.exe[0x180268] i_%s120.tga 120x100 *solars.wdf   <2010>                  current consumable ship build task (unused)
  // trek.exe[0x180280] i_%sw.tga   120x100 *solars.wdf   <2010>                  ship build task wireframe
  public static final StringDefinition ShipBldTskImg =
    new StringDefinition(0x180280, Prefix + "_shipBldTskImg", "i_%sw.tga", 12)
      .desc("*solars.wdf [2010] build screen wireframe for the current build task (120x100 pixel)");
  // trek.exe[0x180274] i_%s170.tga 170x142 *solbld.wdf   <2202>                  ship build selection details
  public static final StringDefinition ShipBldSelImg =
    new StringDefinition(0x180274, Prefix + "_shipBldSelImg", "i_%s170.tga", 12)
      .desc("*solbld.wdf [2202] build screen image for the build structure details (170x142 pixel)");
  // trek.exe[0x180724] i_%s170.tga 170x142 *_shpdpy.wdf  <1335>                  ship redeploy selection details
  public static final StringDefinition ShipRdplSelImg =
    new StringDefinition(0x180724, Prefix + "_shipRdplSelImg", "i_%s170.tga", 12)
      .desc("*_shpdpy.wdf [1335] ship image file name for the ship redeployment details (170x142 pixel)");

  // #endregion Ship Image Names

  // #region Starbase Image Names

  // trek.exe[0x17FA54] i_%s30.tga  30x30   *bshpinf.wdf  <6552>                  bottom task force station
  public static final StringDefinition StarbaseIcon =
    new StringDefinition(0x17FA54, Prefix + "_sbIcon", "i_%s30.tga", 12)
      .desc("*bshpinf.wdf [6552] starbase icon (30x30 pixel)");
  // trek.exe[0x17FBEC] i_%s120.tga 120x100 *_tfship.wdf  <1212>                  starbase sector details image
  public static final StringDefinition StarbaseImg =
    new StringDefinition(0x17FBEC, Prefix + "_sbImg", "i_%s120.tga", 12)
      .desc("*_tfship.wdf [1212] starbase image file name for the sector details (120x100 pixel)");
  // trek.exe[0x17FBE0] i_%sw.tga   120x100 *_tfship.wdf  <1212>                  starbase construction wireframe
  public static final StringDefinition StarbaseBldImg =
    new StringDefinition(0x17FBE0, Prefix + "_sbBldImg", "i_%sw.tga", 12)
      .desc("*_tfship.wdf [1212] starbase wireframe file name for starbase construction (120x100 pixel)");

  // #endregion Starbase Image Names

  // #region Technology Field Image Names

  // trek.exe[0x17ED1C] %s_60.tga   60x50   *techl.wdf    <3401..4,13,15>         tech level unlockings & building tech requirements
  public static final StringDefinition TecInfoLstImg =
    new StringDefinition(0x17ED1C, Prefix + "_tecInfoLstImg", "%s_60.tga", 12)
      .desc("*techl.wdf [3401..] for unlocked tech level constructions and building tech requirements (60x50 pixel)");
  // trek.exe[0x17EAE4] %s.tga      120x100 *techr.wdf    <3102,_16,30,44,58,72>  research assignment
  // 1366x768 and 1920x1080 mod relocate '%d' to enlarge the string value to 9 bytes
  // https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=59144#p59144
  // other mods might use the full 12 bytes
  public static final StringDefinition TecResImg =
    new StringDefinition(0x17EAE4, Prefix + "_tecResImg", "%s.tga", 12)
      .desc("*techr.wdf [3102..] research assignment (120x100 pixel)");
  // trek.exe[0x17ECAC] %s_b.tga    270x225 *techf.wdf    <3215>                  technology database details (modded: %s_c.tga)
  public static final StringDefinition TecDBImg =
    new StringDefinition(0x17ECAC, Prefix + "_tecDBImg", "%s_b.tga", 12)
      .desc("*techf.wdf [3215] technology database details (270x225 pixel)");
  // trek.exe[0x178398] %s_b.tga    270x225 *edialog.wdf  <9800>                  technology breakthrough event popup
  public static final StringDefinition TecEvtImg =
    new StringDefinition(0x178398, Prefix + "_tecEvtImg", "%s_b.tga", 12)
      .desc("*edialog.wdf [9800] technology breakthrough event popup (270x225 pixel)");

  // #endregion Technology Field Image Names

  public static final SegmentDefinition[] All = Stream.of(
    BldEngyIcon,
    BldQueIcon,
    BldSysImg,
    BldOrbImg,
    BldPopImg,
    BldConsImg,
    BldTskImg,
    BldRaceInfoImg,
    BldSelImg,
    BldTecSelImg,
    ShipCbtIcon,
    ShipListIcon,
    ShipRdplIcon,
    ShipBldQueIcon,
    ShipTecLvlImg,
    ShipRdplImg,
    ShipBldTskImg,
    ShipBldSelImg,
    ShipRdplSelImg,
    StarbaseIcon,
    StarbaseImg,
    StarbaseBldImg,
    TecInfoLstImg,
    TecResImg,
    TecDBImg,
    TecEvtImg
  ).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
