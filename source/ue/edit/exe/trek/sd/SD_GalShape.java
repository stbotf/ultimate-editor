package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.LongValueSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.common.CTrek;
import ue.edit.exe.trek.seg.map.LargeSpiralAxisFix;

/**
 * This interface holds the GalaxyShapesPatch segment definitions.
 */
public interface SD_GalShape {

  public static final String Prefix = "galaxyShape"; //$NON-NLS-1$
  public static final String Prefix_Ellipse = Prefix + "_ellipseAxis"; //$NON-NLS-1$
  public static final String Prefix_Spiral = Prefix + "_spiralAxis"; //$NON-NLS-1$
  public static final String Prefix_SpiralArc = Prefix + "_spiralArmArc"; //$NON-NLS-1$
  public static final String Prefix_SpiralWidth = Prefix + "_spiralArmWidth"; //$NON-NLS-1$
  public static final String Prefix_RingWidth = Prefix + "_ringWidth"; //$NON-NLS-1$
  public static final String Prefix_RingCore = Prefix + "_ringCoreSize"; //$NON-NLS-1$

  // ellipseGalAxis_address
  public static final ValueDefinition<?>[][] EllipseAxis = new ValueDefinition<?>[][] {
    // small X+Y
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae571, Prefix_Ellipse, MovRegInt.class, Register.ESI, 28)
        .suffix().desc("small ellipse galaxy X-axis"),
      new ValueDefinition<Integer>(0x000ae567, Prefix_Ellipse, MovRegInt.class, Register.EBX, 20)
        .suffix().desc("small ellipse galaxy Y-axis")
    },
    // medium X+Y
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae60d, Prefix_Ellipse, MovRegInt.class, Register.ESI, 33)
        .suffix().desc("medium ellipse galaxy X-axis"),
      new ValueDefinition<Integer>(0x000ae603, Prefix_Ellipse, MovRegInt.class, Register.EBX, 25)
        .suffix().desc("medium ellipse galaxy Y-axis")
    },
    // large X+Y
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae6db, Prefix_Ellipse, MovRegInt.class, Register.ESI, 68)
        .suffix().desc("large ellipse galaxy X-axis"),
      new ValueDefinition<Integer>(0x000ae6d6, Prefix_Ellipse, MovRegInt.class, Register.EBX, 60)
        .suffix().desc("large ellipse galaxy Y-axis")
    }
  };

  // spiralGalEllipseAxis_address
  public static final ValueDefinition<Integer> Shared_LargeSpiralEllipseAxis =
    new ValueDefinition<Integer>(0x000ae740, Prefix_Spiral, MovRegInt.class, Register.EBX, 14)
    .suffix().desc("large ellipse galaxy X-axis, by default also shared for Y-axis");

  public static final ValueDefinition<?>[][] SpiralEllipseAxis = new ValueDefinition<?>[][] {
    // small X (Major) + Y (Minor)
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae5cf, Prefix_Spiral, MovRegInt.class, Register.ESI, 13)
        .suffix().desc("small ellipse galaxy X-axis"),
      new ValueDefinition<Integer>(0x000ae5ca, Prefix_Spiral, MovRegInt.class, Register.EBX, 10)
        .suffix().desc("small ellipse galaxy Y-axis")
    },
    // medium X (Major) + Y (Minor)
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae65f, Prefix_Spiral, MovRegInt.class, Register.ESI, 13)
        .suffix().desc("medium ellipse galaxy X-axis"),
      new ValueDefinition<Integer>(0x000ae65a, Prefix_Spiral, MovRegInt.class, Register.EBX, 10)
        .suffix().desc("medium ellipse galaxy Y-axis")
    },
    // large X (Major) + Y (Minor)
    new ValueDefinition<?>[] {
      Shared_LargeSpiralEllipseAxis,
      Shared_LargeSpiralEllipseAxis
    }
  };

  public static final ValueDefinition<Integer> LargeSpiralAxis = /*0x000ae75d*/ LargeSpiralAxisFix.SEGMENT_DEFINITION;

  // inner, medium, outer
  public static final ValueDefinition<?>[] SpiralArmArcs = new ValueDefinition<?>[] {
    new ValueDefinition<Double>(0x0017d5a0, Prefix_SpiralArc, LongValueSegment.class, 1.5 * CTrek.PI)
      .suffix().desc("inner spiral galaxy arm arc"),
    new ValueDefinition<Double>(0x0017d5a8, Prefix_SpiralArc, LongValueSegment.class, CTrek.PI)
      .suffix().desc("medium spiral galaxy arm arc"),
    new ValueDefinition<Double>(0x0017d5b0, Prefix_SpiralArc, LongValueSegment.class, 0.5 * CTrek.PI)
      .suffix().desc("outer spiral galaxy arm arc")
  };

  // small, medium, large
  public static final ValueDefinition<?>[] SpiralArmWidth = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(0x000ae5c5, Prefix_SpiralWidth, MovRegInt.class, Register.ECX, 5)
      .suffix().desc("small spiral galaxy arm width"),
    new ValueDefinition<Integer>(0x000ae655, Prefix_SpiralWidth, MovRegInt.class, Register.EAX, 6)
      .suffix().desc("medium spiral galaxy arm width"),
    new ValueDefinition<Integer>(0x000ae73b, Prefix_SpiralWidth, MovRegInt.class, Register.ECX, 7)
      .suffix().desc("large spiral galaxy arm width")
  };

  // shared ring width values
  public static final ValueDefinition<Integer> Shared_SmallRingWidth =
    new ValueDefinition<Integer>(0x000ae58f, Prefix_RingWidth, MovRegInt.class, Register.EAX, 4)
      .suffix().desc("small ring galaxy width, shared by min empire distance"
        + "\nunless the RingMinEmpireDistanceFix is applied, defaults to 4");

  public static final ValueDefinition<Integer> Shared_LargeRingWidth =
    new ValueDefinition<Integer>(0x000ae705, Prefix_RingWidth, MovRegInt.class, Register.EAX, 6)
      .suffix().desc("large ring galaxy width, shared by min empire distance"
        + "\nunless the RingMinEmpireDistanceFix is applied, defaults to 6");

  // small, medium, large
  public static final ValueDefinition<?>[] RingWidth = new ValueDefinition<?>[] {
    Shared_SmallRingWidth,
    new ValueDefinition<Integer>(0x000ae622, Prefix_RingWidth, MovRegInt.class, Register.ECX, 5)
      .suffix().desc("medium ring galaxy width"),
    Shared_LargeRingWidth
  };

  // small, medium, large
  public static final ValueDefinition<?>[] RingCoreSize = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(0x000ae594, Prefix_RingCore, MovRegInt.class, Register.EDX, 9)
      .suffix().desc("small ring galaxy core size"),
    new ValueDefinition<Integer>(0x000ae627, Prefix_RingCore, MovRegInt.class, Register.EDI, 12)
      .suffix().desc("medium ring galaxy core size"),
    new ValueDefinition<Integer>(0x000ae70a, Prefix_RingCore, MovRegInt.class, Register.EDX, 15)
      .suffix().desc("large ring galaxy core size")
  };

  public static final SegmentDefinition[] All = Stream.of(
    Arrays.stream(EllipseAxis).flatMap(Arrays::stream),
    Arrays.stream(SpiralEllipseAxis).flatMap(Arrays::stream),
    Arrays.stream(SpiralArmWidth),
    Arrays.stream(SpiralArmArcs),
    Arrays.stream(RingWidth),
    Arrays.stream(RingCoreSize),
    Arrays.stream(SD_MinEmpDistFix.All),
    Stream.of(LargeSpiralAxis)
  ).flatMap(x -> x).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
