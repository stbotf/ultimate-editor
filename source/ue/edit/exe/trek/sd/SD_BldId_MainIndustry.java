package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the main industry building id segment definitions.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=263&t=573
 */
public interface SD_BldId_MainIndustry {

  public static final String Prefix       = "mainIndustryBld"; //$NON-NLS-1$
  public static final String Prefix1Start = Prefix + "1_start"; //$NON-NLS-1$
  public static final String Prefix1End   = Prefix + "1_end"; //$NON-NLS-1$
  public static final String Prefix2Start = Prefix + "2_start"; //$NON-NLS-1$
  public static final String Prefix2End   = Prefix + "2_end"; //$NON-NLS-1$
  public static final String Prefix3Start = Prefix + "3_start"; //$NON-NLS-1$
  public static final String Prefix3End   = Prefix + "3_end"; //$NON-NLS-1$
  public static final String Suffix_Card  = "_Cardassian"; //$NON-NLS-1$
  public static final String Suffix_Fed   = "_Federation"; //$NON-NLS-1$
  public static final String Suffix_Ferg  = "_Ferengi"; //$NON-NLS-1$
  public static final String Suffix_Klng  = "_Klingon"; //$NON-NLS-1$
  public static final String Suffix_Rom   = "_Romulan"; //$NON-NLS-1$

  // sub_486550
  public static final int START1_ADDRESS   = 0x0008597A;
  public static final int START2_ADDRESS   = 0x00085AE0;
  public static final int START3_ADDRESS   = 0x00085C5E;

  public static int[] Default_IndustryBldId_begin   = new int[] { 88, 134, 180, 226, 272 };

  // Main energy buildings
  // shared industry/farm start, ends by SD_BldId_MainFarm.IndFarmEnd
  ValueDefinition<?>[] Start1 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START1_ADDRESS,        Prefix1Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[0])
      .desc("Cardassian industry start building ID at asm_48657A: mov     ecx, 58h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 40,   Prefix1Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[1])
      .desc("Federation industry start building ID at asm_4865A2: mov     ecx, 86h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 64,   Prefix1Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[2])
      .desc("Ferengi industry start building ID at asm_4865BA: mov     ecx, 0B4h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 88,   Prefix1Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[3])
      .desc("Klingon industry start building ID at asm_4865D2: mov     ecx, 0E2h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 112,  Prefix1Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[4])
      .desc("Romulan industry start building ID at asm_4865EA: mov     ecx, 110h"),
  };

  ValueDefinition<?>[] Start2 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START2_ADDRESS,        Prefix2Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[0])
    .desc("Cardassian industry start building ID at asm_4866E0: mov     ecx, 58h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 40,   Prefix2Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[1])
    .desc("Federation industry start building ID at asm_486708: mov     ecx, 86h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 64,   Prefix2Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[2])
    .desc("Ferengi industry start building ID at asm_486720: mov     ecx, 0B4h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 88,   Prefix2Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[3])
    .desc("Klingon industry start building ID at asm_486738: mov     ecx, 0E2h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 112,  Prefix2Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[4])
    .desc("Romulan industry start building ID at asm_486750: mov     ecx, 110h"),
  };

  ValueDefinition<?>[] Start3 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START3_ADDRESS,        Prefix3Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[0])
    .desc("Cardassian industry start building ID at asm_48685E: mov     ecx, 58h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 40,   Prefix3Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[1])
    .desc("Federation industry start building ID at asm_486886: mov     ecx, 86h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 64,   Prefix3Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[2])
    .desc("Ferengi industry start building ID at asm_48689E: mov     ecx, 0B4h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 88,   Prefix3Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[3])
    .desc("Klingon industry start building ID at asm_4868B6: mov     ecx, 0E2h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 112,  Prefix3Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IndustryBldId_begin[4])
    .desc("Romulan industry start building ID at asm_4868CE: mov     ecx, 110h"),
  };

  public static final ValueDefinition<?>[][] Start = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {Start1[0], Start2[0], Start3[0]}, // Cardassians
    new ValueDefinition<?>[] {Start1[1], Start2[1], Start3[1]}, // Federation
    new ValueDefinition<?>[] {Start1[2], Start2[2], Start3[2]}, // Ferengi
    new ValueDefinition<?>[] {Start1[3], Start2[3], Start3[3]}, // Klingons
    new ValueDefinition<?>[] {Start1[4], Start2[4], Start3[4]}  // Romulans
  };

  public static final ValueDefinition<?>[] All = Stream.of( Start1, Start2, Start3 )
    .flatMap(Arrays::stream).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
