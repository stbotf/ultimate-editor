package ue.edit.exe.trek.sd;

import java.util.HashMap;
import java.util.Vector;
import java.util.function.BiConsumer;
import lombok.Data;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.AddRegByte;
import ue.edit.exe.seg.prim.AddRegInt;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.CmpWordPtrByte;
import ue.edit.exe.seg.prim.CmpRegInt;
import ue.edit.exe.seg.prim.CmpRegShort;
import ue.edit.exe.seg.prim.CmpWordPtrShort;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.JmpIf;
import ue.edit.exe.seg.prim.JmpShort;
import ue.edit.exe.seg.prim.LeaMemOffset;
import ue.edit.exe.seg.prim.LongValueSegment;
import ue.edit.exe.seg.prim.MovDSInt;
import ue.edit.exe.seg.prim.MovDwordPtrInt;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.MovWordPtrShort;
import ue.edit.exe.seg.prim.PushInt;
import ue.edit.exe.seg.prim.ShortValueSegment;
import ue.edit.exe.seg.prim.SignedByteValueSegment;
import ue.edit.exe.seg.prim.SubRegByte;

@Data
public class SegmentDefinition {

  public final Class<?> type;
  public final int address;
  @Getter @Accessors(fluent = true) protected int size = -1;
  @Getter @Accessors(fluent = true) protected Register register = Register.EAX;
  @Getter @Accessors(fluent = true) protected int flags = 0;
  @Getter @Accessors(fluent = true) protected int offset = 0;
  @Getter @Accessors(fluent = true) protected String name = null;
  @Getter @Accessors(fluent = true) protected String description = null;

  @Getter @Accessors(fluent = true)
  private BiConsumer<InternalSegment, Vector<String>> cbIntegrityCheck = null;

  @Getter @Accessors(fluent = true) protected boolean hasRegister = false;
  @Getter @Accessors(fluent = true) protected boolean hasOffset = false;

  private static final HashMap<Class<?>, Integer> sizeMap = createSizeMap();

  /**
   * @param address trek.exe address of the segment
   * @param name    name or prefix of the segment
   * @param type    segment class type
   */
  public SegmentDefinition(int address, String name, Class<?> type) {
    this(type, address, name);
    this.size = determineSize(type, Register.EAX, 0, 0);
  }

  /**
   * @param address trek.exe address of the segment
   * @param name    name or prefix of the segment
   * @param type    segment class type
   * @param size    segment size
   */
  public SegmentDefinition(int address, String name, Class<?> type, int size) {
    this(type, address, name);
    this.size = size;
  }

  /**
   * @param address   trek.exe address of the segment
   * @param name      name or prefix of the segment
   * @param type      segment class type
   * @param register  asm register to be validated
   */
  public SegmentDefinition(int address, String name, Class<?> type, Register register) {
    this(type, address, name);
    reg(register);
    this.size = determineSize(type, register, 0, 0);
  }

  /**
   * @param address   trek.exe address of the segment
   * @param name      name or prefix of the segment
   * @param type      segment class type
   * @param register  asm register to be validated
   * @param flags     asm register flags to be validated
   * @param offset    asm register offset to be validated
   */
  public SegmentDefinition(int address, String name, Class<?> type, Register register, int flags, int offset) {
    this(type, address, name);
    reg(register, flags, offset);
    this.size = determineSize(type, register, flags, offset);
  }

  // private initialization helper
  private SegmentDefinition(Class<?> type, int address, String name) {
    this.type = type;
    this.address = address;
    this.name = name != null ? name : segmentName(null, type, address);
  }

  /**
   * Suffixes the name by "_<classType>_<hexAddress>", e.g.: "SuperPatch_MovIntToReg_498724".
   */
  public SegmentDefinition suffix() {
    this.name = segmentName(name, address);
    return this;
  }

  /**
   * Suffixes the name by "_<hexAddress>".
   */
  public SegmentDefinition addrSuffix() {
    this.name = segmentName(name, address);
    return this;
  }

  public SegmentDefinition suffixType() {
    this.name = segmentName(name, type);
    return this;
  }

  public SegmentDefinition suffixTypeAddr() {
    this.name = segmentName(name, type, address);
    return this;
  }

  /**
   * Specifies the optional register.
   */
  public SegmentDefinition reg(Register register) {
    this.register = register;
    hasRegister = true;
    return this;
  }

  /**
   * Specifies the optional register, flags and offset.
   */
  public SegmentDefinition reg(Register register, int flags, int offset) {
    this.register = register;
    this.flags = flags;
    this.offset = offset;
    hasRegister = true;
    hasOffset = true;
    return this;
  }

  public boolean hasFlags() {
    return flags != 0;
  }

  /**
   * Specifies the optional flags.
   */
  public SegmentDefinition flags(int flags) {
    this.flags = flags;
    return this;
  }

  /**
   * Specifies the optional offset.
   */
  public SegmentDefinition offset(int offset) {
    this.offset = offset;
    hasOffset = true;
    return this;
  }

  /**
   * Specifies the optional size.
   */
  public SegmentDefinition size(int size) {
    this.size = size;
    return this;
  }

  /**
   * Specifies the optional description.
   */
  public SegmentDefinition desc(String desc) {
    this.description = desc;
    return this;
  }

  public SegmentDefinition integrityCheck(BiConsumer<InternalSegment, Vector<String>> cb) {
    cbIntegrityCheck = cb;
    return this;
  }

  public static String segmentName(String prefix, int address) {
    String addrStr = Integer.toHexString(address);
    prefix = prefix != null ? prefix + '_' : "";
    return prefix + addrStr;
  }

  public static String segmentName(String prefix, Class<?> type) {
    prefix = prefix != null ? prefix + '_' : "";
    return prefix + type.getSimpleName();
  }

  public static String segmentName(String prefix, Class<?> type, int address) {
    String addrStr = Integer.toHexString(address);
    prefix = prefix != null ? prefix + '_' : "";
    return prefix + type.getSimpleName() + '_' + addrStr;
  }

  public static int determineSize(Class<?> type, Register register, int flags, int offset) {
    int size = sizeMap.getOrDefault(type, -1);
    if (size == -1) {
      if (type == CmpRegByte.class)
        return CmpRegByte.getSize(register);
      else if (type == CmpRegShort.class)
        return CmpRegShort.getSize(register);
      else if (type == CmpWordPtrShort.class)
        return CmpWordPtrShort.getSize(register);
      else if (type == LeaMemOffset.class)
        return LeaMemOffset.getSize(register);
      else if (type == MovDwordPtrInt.class)
        return MovDwordPtrInt.getSize(register, flags, offset);
      else if (type == MovWordPtrShort.class)
        return MovWordPtrShort.getSize(register, flags, offset);
    }
    return size;
  }

  private static HashMap<Class<?>, Integer> createSizeMap() {
    val map = new HashMap<Class<?>, Integer>();
    map.put(AddRegByte.class, AddRegByte.SIZE);
    map.put(AddRegInt.class, AddRegInt.SIZE);
    map.put(ByteValueSegment.class, ByteValueSegment.VALUE_SIZE);
    map.put(CmpWordPtrByte.class, CmpWordPtrByte.SIZE);
    map.put(CmpRegInt.class, CmpRegInt.SIZE);
    map.put(IntValueSegment.class, IntValueSegment.VALUE_SIZE);
    map.put(Jmp.class, Jmp.SIZE);
    map.put(JmpIf.class, JmpIf.SIZE);
    map.put(JmpShort.class, JmpShort.SIZE);
    map.put(LongValueSegment.class, LongValueSegment.VALUE_SIZE);
    map.put(MovDSInt.class, MovDSInt.SIZE);
    map.put(MovRegInt.class, MovRegInt.SIZE);
    map.put(PushInt.class, PushInt.SIZE);
    map.put(ShortValueSegment.class, ShortValueSegment.VALUE_SIZE);
    map.put(SignedByteValueSegment.class, SignedByteValueSegment.VALUE_SIZE);
    map.put(SubRegByte.class, SubRegByte.SIZE);
    return map;
  }

}
