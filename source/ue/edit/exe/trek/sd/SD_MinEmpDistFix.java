package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.Jmp;
import ue.edit.exe.seg.prim.MovDSInt;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.seg.map.RingMinEmpireDistanceFix;

/**
 * This interface holds the RingMinEmpireDistanceFix segment definitions.
 */
public interface SD_MinEmpDistFix {

  public static final int SMALL_RING_ADDRESS = 0xae5b1;
  public static final int LARGE_RING_ADDRESS = 0xae727;
  public static final int SIZE = 15;

  public static final int DEFAULT_SmallWidth = 13;
  public static final int DEFAULT_LargeWidth = 25;
  public static final int DEFAULT_SmallMinEmpireDistance = 4;
  public static final int DEFAULT_LargeMinEmpireDistance = 6;
  public static final byte NOP = (byte)0x90;

  public static final String Prefix = "minEmpDist_fixture"; //$NON-NLS-1$

  // fixture to set values independent from the ring width
  public static final SegmentDefinition SmallRing =
    new SegmentDefinition(SMALL_RING_ADDRESS, Prefix, RingMinEmpireDistanceFix.class)
      .addrSuffix().desc("Fixture to set the small ring galaxy minimum empire distance independent from the ring width."
        + "\nIf patched sets the small ring galaxy minimum empire distance, else sets the small galaxy map width.").size(SIZE);

  public static final SegmentDefinition LargeRing =
    new SegmentDefinition(LARGE_RING_ADDRESS, Prefix, RingMinEmpireDistanceFix.class)
      .addrSuffix().desc("Fixture to set the large ring galaxy minimum empire distance independent from the ring width."
        + "\nIf patched sets the large ring galaxy minimum empire distance, else sets the large galaxy map width.").size(SIZE);

  // orig
  public static final ValueDefinition<Integer> OrigSmallMapWidth =
    new ValueDefinition<Integer>(0, Prefix, MovDSInt.class, DEFAULT_SmallWidth)
      .suffix().desc("small galaxy map width: mov ds:long_edge_5CB314 <value>")
      .offset(0x005CB314);

  public static final ValueDefinition<Integer> OrigLargeMapWidth =
    new ValueDefinition<Integer>(0, Prefix, MovDSInt.class, DEFAULT_LargeWidth)
      .suffix().desc("large galaxy map width: mov ds:long_edge_5CB314 <value>")
      .offset(0x005CB314);

  // patched
  public static final ValueDefinition<Integer> PatchedSmallRingGal =
    new ValueDefinition<Integer>(0, Prefix,  MovRegInt.class, Register.ECX, DEFAULT_SmallMinEmpireDistance)
      .suffix().desc("small ring galaxy minimum empire distance");

  public static final ValueDefinition<Integer> PatchedLargeRingGal =
    new ValueDefinition<Integer>(0, Prefix,  MovRegInt.class, Register.ECX, DEFAULT_LargeMinEmpireDistance)
    .suffix().desc("large ring galaxy minimum empire distance");

  public static final SegmentDefinition PatchedNops =
    new DataDefinition(5, Prefix, new byte[] { NOP, NOP, NOP, NOP, NOP })
      .suffix().desc("fills empty space with nops");

  // shared
  public static final ValueDefinition<Integer> Jmp =
    new ValueDefinition<Integer>(10, Prefix, Jmp.class, 0)
      .suffix().desc("jump instruction that sets map width before exit if patched");

  // lists the main segment area, which encompasses all the sub-segments
  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    SmallRing,
    LargeRing
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
