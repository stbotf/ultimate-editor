package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpWordPtrByte;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the orbital building id segment definitions.
 */
public interface SD_BldId_Orb {

  public static final String Prefix = "orbitalDefenseBld"; //$NON-NLS-1$

  public static final int ORB1_ADDRESS = 0x0006BEBE;
  public static final int ORB2_ADDRESS = 0x0006C70B;
  public static final int ORB3_ADDRESS = 0x0006CC1F;
  public static final int ORB4_ADDRESS = 0x000F0859;
  public static final int ORB5_ADDRESS = 0x000F0907;
  public static final int ORB6_ADDRESS = 0x000F0FD5;
  public static final int ORB7_ADDRESS = 0x0005155D;
  public static final int ORB8_ADDRESS = 0x000516E7;
  public static final int ORB9_ADDRESS = 0x0000AC14;
  public static final int ORB10_ADDRESS = 0x0006BEA0;
  public static final int ORB11_ADDRESS = 0x0006C6ED;
  public static final int ORB12_ADDRESS = 0x000F08F2;
  public static final int ORB13_ADDRESS = 0x000FB667;

  // main segments
  public static final ValueDefinition<?>[] IntVal = Stream.of(
    new ValueDefinition<Integer>(ORB1_ADDRESS, Prefix + "1", MovRegInt.class, Register.EAX, 5),
    new ValueDefinition<Integer>(ORB2_ADDRESS, Prefix + "2", MovRegInt.class, Register.EAX, 5),
    new ValueDefinition<Integer>(ORB3_ADDRESS, Prefix + "3", MovRegInt.class, Register.EAX, 5),
    new ValueDefinition<Integer>(ORB4_ADDRESS, Prefix + "4", MovRegInt.class, Register.EAX, 5),
    new ValueDefinition<Integer>(ORB5_ADDRESS, Prefix + "5", MovRegInt.class, Register.EAX, 5),
    new ValueDefinition<Integer>(ORB7_ADDRESS, Prefix + "7", MovRegInt.class, Register.EDX, 5),
    new ValueDefinition<Integer>(ORB8_ADDRESS, Prefix + "8", MovRegInt.class, Register.EDX, 5),
    new ValueDefinition<Integer>(ORB9_ADDRESS, Prefix + "9", MovRegInt.class, Register.EDX, 5),
    new ValueDefinition<Integer>(ORB10_ADDRESS, Prefix + "10", MovRegInt.class, Register.EDX, 5),
    new ValueDefinition<Integer>(ORB11_ADDRESS, Prefix + "11", MovRegInt.class, Register.EDX, 5),
    new ValueDefinition<Integer>(ORB12_ADDRESS, Prefix + "12", MovRegInt.class, Register.EDX, 5),
    new ValueDefinition<Integer>(ORB13_ADDRESS, Prefix + "13", MovRegInt.class, Register.EDX, 5)
  ).map(x -> x.desc("Orbital building id.")).toArray(ValueDefinition<?>[]::new);

  public static final ValueDefinition<Byte> Orb6 = new ValueDefinition<Byte>(
    ORB6_ADDRESS, Prefix + "6", CmpWordPtrByte.class, Register.EBP, 0, 2, (byte)0x05);

  public static final SegmentDefinition[] All = Stream.of(
    Arrays.stream(IntVal),
    Stream.of(Orb6)
  ).flatMap(Function.identity()).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
