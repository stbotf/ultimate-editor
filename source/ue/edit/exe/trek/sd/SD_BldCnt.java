package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpRegInt;
import ue.edit.exe.seg.prim.CmpRegShort;
import ue.edit.exe.seg.prim.PushInt;

/**
 * This interface holds the BuildingCountPatch segment definitions.
 */
public interface SD_BldCnt {

  public static final String Prefix = "buildingCount"; //$NON-NLS-1$
  public static final String Prefix_BldNum = Prefix + "_num"; //$NON-NLS-1$
  public static final String Prefix_BldMax = Prefix + "_max"; //$NON-NLS-1$

  // building counters
  public static final ValueDefinition<?>[] Num = Stream.of(
    new ValueDefinition<Short>(0x0000ADDC - 2, Prefix_BldNum, CmpRegShort.class, Register.AX, (short)318),  // fileBLD_NUM1
    new ValueDefinition<Short>(0x0000CB55 - 3, Prefix_BldNum, CmpRegShort.class, Register.BX, (short)318),  // fileBLD_NUM2
    new ValueDefinition<Short>(0x00039E65 - 3, Prefix_BldNum, CmpRegShort.class, Register.BX, (short)318),  // fileBLD_NUM3
    new ValueDefinition<Short>(0x000F38C9 - 3, Prefix_BldNum, CmpRegShort.class, Register.DX, (short)318)   // fileBLD_NUM4
  ).map(x -> x.suffix().desc("total number of buildings <short>")).toArray(ValueDefinition<?>[]::new);

  public static final ValueDefinition<?>[] MaxInt = Stream.of(
    new ValueDefinition<Integer>(0x0004F90E - 1, Prefix_BldMax, PushInt.class, 317),                                    // fileMAX_BLD_ID9
    // for 0x000851C3, 0x00085296, 0x00085345, 0x000853D0 and 0x0008544F listed by EdificePatcher see MainResearchBld.end_id
    // for 0x00086240, 0x000862E3 and 0x0008636E listed by EdificePatcher see MainResearchBld2.end_id
    new ValueDefinition<Integer>(0x000869CD - 2, Prefix_BldMax, CmpRegInt.class,   Register.ECX, 317),        // fileMAX_BLD_ID7
    new ValueDefinition<Integer>(0x00086ABD - 2, Prefix_BldMax, CmpRegInt.class,   Register.ECX, 317)         // fileMAX_BLD_ID8
  ).map(x -> x.suffix().desc("max building Id <int>")).toArray(ValueDefinition<?>[]::new);

  public static final ValueDefinition<?>[] MaxShort = Stream.of(
    new ValueDefinition<Short>(0x0004F8E5 - 2, Prefix_BldMax, CmpRegShort.class, Register.AX, (short)317),  // fileMAX_BLD_ID1
    new ValueDefinition<Short>(0x0004FAD8 - 3, Prefix_BldMax, CmpRegShort.class, Register.DX, (short)317),  // fileMAX_BLD_ID2
    new ValueDefinition<Short>(0x00053105 - 3, Prefix_BldMax, CmpRegShort.class, Register.BX, (short)317),  // fileMAX_BLD_ID3
    new ValueDefinition<Short>(0x00085475 - 3, Prefix_BldMax, CmpRegShort.class, Register.CX, (short)317),  // MainResearchBld
    new ValueDefinition<Short>(0x00085926 - 3, Prefix_BldMax, CmpRegShort.class, Register.BX, (short)317),  // MainIntelBld
    new ValueDefinition<Short>(0x00085F39 - 3, Prefix_BldMax, CmpRegShort.class, Register.CX, (short)317),  // MainIndustryFarmsAndEnergyBld
    new ValueDefinition<Short>(0x0008602B - 3, Prefix_BldMax, CmpRegShort.class, Register.BX, (short)317),  // fileMAX_BLD_ID4
    new ValueDefinition<Short>(0x00086394 - 3, Prefix_BldMax, CmpRegShort.class, Register.BX, (short)317),  // MainResearchBld2
    new ValueDefinition<Short>(0x00086755 - 3, Prefix_BldMax, CmpRegShort.class, Register.BX, (short)317),  // MainIntelBld2
    new ValueDefinition<Short>(0x0008680F - 3, Prefix_BldMax, CmpRegShort.class, Register.BX, (short)317),  // fileMAX_BLD_ID5
    new ValueDefinition<Short>(0x000868EC - 3, Prefix_BldMax, CmpRegShort.class, Register.BX, (short)317)   // fileMAX_BLD_ID6
  ).map(x -> x.suffix().desc("max building Id <short>")).toArray(ValueDefinition<?>[]::new);

  public static final SegmentDefinition[] All = Stream.of(
    Num,
    MaxShort,
    MaxInt
  ).flatMap(Arrays::stream).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
