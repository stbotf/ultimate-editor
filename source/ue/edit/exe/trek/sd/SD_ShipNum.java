package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.Vector;
import java.util.stream.Stream;
import ue.UE;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.CmpRegShort;
import ue.edit.exe.seg.prim.ValueSegment;
import ue.edit.exe.trek.seg.shp.ShipLoadLimit;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;

/**
 * Segment definitions for the number of ships in shiplist.sst.
 */
public interface SD_ShipNum {

  public static final String PrefixNum      = "shipNum"; //$NON-NLS-1$
  public static final String PrefixLimit    = "shipLoadLimit"; //$NON-NLS-1$

  public static final int SHIPNUM1_ADDRESS  = 0x0006EF87;
  public static final int SHIPNUM2_ADDRESS  = 0x0006F02D;
  public static final int LIMIT1_ADDRESS    = 0x0006EC08;
  public static final int LIMIT2_ADDRESS    = 0x0006EC66;

  public static final byte Default_ShipNum  = 125; // 7Dh

  public static final String DESC_LimitJL = "Sets the ship model number and patches"
    + " to unsigned byte value when 127 ship models is exceeded.";
  public static final String DESC_LimitJGE = DESC_LimitJL
    + "<br>Further sneaks in to replace nopped code.";

  public static final ValueDefinition<Short> ShipNum1 =
    new ValueDefinition<Short>( SHIPNUM1_ADDRESS, PrefixNum + "1",
      CmpRegShort.class, Register.AX, (short)Default_ShipNum);

  public static final ValueDefinition<Short> ShipNum2 =
    new ValueDefinition<Short>( SHIPNUM2_ADDRESS, PrefixNum + "2",
      CmpRegShort.class, Register.AX, (short)Default_ShipNum);

  public static final ValueDefinition<Byte> ShipLoadLimit1 =
    new ValueDefinition<Byte>( LIMIT1_ADDRESS, PrefixLimit + "1",
      ShipLoadLimit.class, Register.EAX, Default_ShipNum)
    .desc(DESC_LimitJGE).size(ShipLoadLimit.SIZE);

  public static final ValueDefinition<Byte> ShipLoadLimit2 =
    new ValueDefinition<Byte>( LIMIT2_ADDRESS, PrefixLimit + "2",
      ShipLoadLimit.class, Register.EDX, Default_ShipNum)
    .desc(DESC_LimitJL).size(ShipLoadLimit.SIZE);

  public static final ValueDefinition<?>[] All = Stream.of(
      ShipNum1, ShipNum2, ShipLoadLimit1, ShipLoadLimit2
    ).map(x -> x.validator(SD_ShipNum::validateValue)
      .integrityCheck(SD_ShipNum::checkIntegrity))
    .toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

  /**
   * value validation
   */
  static void validateValue(int value) throws InvalidArgumentsException {
    if (value < 6) {
      // msg = Number of ships is less than %1.
      String msg = Language.getString("SD_ShipNum.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(6)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }
  }

  static void validateValue(InternalSegment seg, Object value) throws InvalidArgumentsException {
    int shpId = ((Number)value).intValue();
    validateValue(shpId);
  }

  /**
   * integrity check
   */
  static void checkIntegrity(InternalSegment seg, Vector<String> response) {
    int shpNum = ((ValueSegment)seg).intValue();

    if (shpNum < CStbof.NUM_EMPIRES + 1) {
      // Number of ships is less than %1.
      String msg = Language.getString("SD_ShipNum.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(6)); //$NON-NLS-1$
      msg = seg.getCheckIntegrityString(InternalSegment.INTEGRITY_CHECK_ERROR, msg);
      response.add(msg);
    }

    boolean isDisabled = seg instanceof ShipLoadLimit && ((ShipLoadLimit)seg).isDisabled();
    if (!isDisabled) {
      // check with shiplist.sst
      Stbof stbof = UE.FILES.stbof();
      if (stbof != null) {
        try {
          ShipList sl = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
          int stbof_shpNum = sl.getNumberOfEntries();
          if (shpNum != stbof_shpNum) {
            // Number of registered ships is %1, but should be %2.
            String msg = Language.getString("SD_ShipNum.1") //$NON-NLS-1$
              .replace("%1", Integer.toString(shpNum)) //$NON-NLS-1$
              .replace("%2", Integer.toString(sl.getNumberOfEntries())); //$NON-NLS-1$
            msg = seg.getCheckIntegrityString(InternalSegment.INTEGRITY_CHECK_ERROR, msg);
            msg += " (fixed)";
            response.add(msg);

            // fix ship number
            ((ValueSegment)seg).setIntValue(stbof_shpNum);
          }
        } catch (Exception e) {
          e.printStackTrace();
          response.add(e.toString());
        }
      }
    }
  }
}
