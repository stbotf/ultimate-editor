package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.seg.prim.LongValueSegment;

/**
 * This interface holds ground combat segment definitions for MapGUI.
 */
public interface SD_GroundCombat {

  static final String Prefix = "grndCmbt"; //$NON-NLS-1$

  // GrndCmbtTechMul defaults to 0.1 = 3FB999999999999Ah
  public static final double Default_GrndCmbtTechMul = 0.1;

  // ground combat tech level multiplier
  public static final ValueDefinition<Double> TechMultiplier =
    new ValueDefinition<Double>(0x178B04, Prefix + "_techMul", LongValueSegment.class, Default_GrndCmbtTechMul)
      .desc("Ground combat tech level multiplier.");

  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    TechMultiplier
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
