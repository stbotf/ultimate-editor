package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.CmpRegInt;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the main energy building id segment definitions.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=263&t=573
 */
public interface SD_BldId_MainEnergy {

  public static final String Prefix       = "mainEnergyBld"; //$NON-NLS-1$
  public static final String Prefix1Start = Prefix + "1_start"; //$NON-NLS-1$
  public static final String Prefix2Start = Prefix + "2_start"; //$NON-NLS-1$
  public static final String Prefix2End   = Prefix + "2_end"; //$NON-NLS-1$
  public static final String Prefix3Start = Prefix + "3_start"; //$NON-NLS-1$
  public static final String Prefix3End   = Prefix + "3_end"; //$NON-NLS-1$
  public static final String Prefix4Start = Prefix + "4_start"; //$NON-NLS-1$
  public static final String Prefix4End   = Prefix + "4_end"; //$NON-NLS-1$
  public static final String Prefix5Start = Prefix + "5_start"; //$NON-NLS-1$
  public static final String Prefix5End   = Prefix + "5_end"; //$NON-NLS-1$
  public static final String Suffix_Card  = "_Cardassian"; //$NON-NLS-1$
  public static final String Suffix_Fed   = "_Federation"; //$NON-NLS-1$
  public static final String Suffix_Ferg  = "_Ferengi"; //$NON-NLS-1$
  public static final String Suffix_Klng  = "_Klingon"; //$NON-NLS-1$
  public static final String Suffix_Rom   = "_Romulan"; //$NON-NLS-1$

  // sub_444FA0
  public static final int START1_ADDRESS  = 0x000443AF;
  // sub_486550
  public static final int START2_ADDRESS  = 0x00085A02;
  public static final int START3_ADDRESS  = 0x00085B68;
  public static final int START4_ADDRESS  = 0x00085CE6;
  public static final int START5_ADDRESS  = 0x00085E28;

  public static int[] Default_EnergyBldId_begin   = new int[] { 116, 162, 208, 254, 300 };
  public static int[] Default_EnergyBldId_end     = new int[] { 124, 170, 216, 262, 308 };

  // Main energy buildings
  ValueDefinition<?>[] Start1 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START1_ADDRESS,        Prefix1Start + Suffix_Card,  MovRegInt.class, Register.EAX, Default_EnergyBldId_begin[0])
      .desc("Cardassian energy start building ID at asm_444FAF: mov     eax, 74h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 37,   Prefix1Start + Suffix_Fed,   MovRegInt.class, Register.EAX, Default_EnergyBldId_begin[1])
      .desc("Federation energy start building ID at asm_444FD4: mov     eax, 0A2h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 59,   Prefix1Start + Suffix_Ferg,  MovRegInt.class, Register.EAX, Default_EnergyBldId_begin[2])
      .desc("Ferengi energy start building ID at asm_444FEA: mov     eax, 0D0h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 81,   Prefix1Start + Suffix_Klng,  MovRegInt.class, Register.EAX, Default_EnergyBldId_begin[3])
      .desc("Klingon energy start building ID at asm_445000: mov     eax, 0FEh"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 103,  Prefix1Start + Suffix_Rom,   MovRegInt.class, Register.EAX, Default_EnergyBldId_begin[4])
      .desc("Romulan energy start building ID at asm_445016: mov     eax, 12Ch")
  };

  ValueDefinition<?>[] Start2 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START2_ADDRESS,        Prefix2Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[0])
    .desc("Cardassian energy start building ID at asm_486602: mov     ecx, 74h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 21,   Prefix2Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[1])
    .desc("Federation energy start building ID at asm_486617: mov     ecx, 0A2h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 45,   Prefix2Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[2])
    .desc("Ferengi energy start building ID at asm_48662F: mov     ecx, 0D0h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 69,   Prefix2Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[3])
    .desc("Klingon energy start building ID at asm_486647: mov     ecx, 0FEh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 93,  Prefix2Start + Suffix_Rom,    MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[4])
    .desc("Romulan energy start building ID at asm_48665F: mov     ecx, 12Ch"),
  };

  ValueDefinition<?>[] End2 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START2_ADDRESS + 16,   Prefix2End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_EnergyBldId_end[0])
    .desc("Cardassian energy end building ID at asm_486612: cmp     ecx, 7Ch"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START2_ADDRESS + 37,   Prefix2End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[1])
    .desc("Federation energy end building ID at asm_486627: cmp     ecx, 0AAh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 61,   Prefix2End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[2])
    .desc("Ferengi energy end building ID at asm_48663F: cmp     ecx, 0D8h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 85,   Prefix2End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[3])
    .desc("Klingon energy end building ID at asm_486657: cmp     ecx, 106h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 109,  Prefix2End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[4])
    .desc("Romulan energy end building ID at asm_48666F: cmp     ecx, 134h"),
  };

  ValueDefinition<?>[] Start3 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START3_ADDRESS,        Prefix3Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[0])
    .desc("Cardassian energy start building ID at asm_486768: mov     ecx, 74h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 21,   Prefix3Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[1])
    .desc("Federation energy start building ID at asm_48677D: mov     ecx, 0A2h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 45,   Prefix3Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[2])
    .desc("Ferengi energy start building ID at asm_486795: mov     ecx, 0D0h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 69,   Prefix3Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[3])
    .desc("Klingon energy start building ID at asm_4867AD: mov     ecx, 0FEh"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 93,  Prefix3Start + Suffix_Rom,    MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[4])
    .desc("Romulan energy start building ID at asm_4867C5: mov     ecx, 12Ch"),
  };

  ValueDefinition<?>[] End3 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START3_ADDRESS + 16,   Prefix3End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_EnergyBldId_end[0])
    .desc("Cardassian energy end building ID at asm_486778: cmp     ecx, 7Ch"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START3_ADDRESS + 37,   Prefix3End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[1])
    .desc("Federation energy end building ID at asm_48678D: cmp     ecx, 0AAh"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 61,   Prefix3End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[2])
    .desc("Ferengi energy end building ID at asm_4867A5: cmp     ecx, 0D8h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 85,   Prefix3End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[3])
    .desc("Klingon energy end building ID at asm_4867BD: cmp     ecx, 106h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 109,  Prefix3End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[4])
    .desc("Romulan energy end building ID at asm_4867D5: cmp     ecx, 134h"),
  };

  ValueDefinition<?>[] Start4 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START4_ADDRESS,        Prefix4Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[0])
    .desc("Cardassian energy start building ID at asm_4868E6: mov     ecx, 74h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 21,   Prefix4Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[1])
    .desc("Federation energy start building ID at asm_4868FB: mov     ecx, 0A2h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 45,   Prefix4Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[2])
    .desc("Ferengi energy start building ID at asm_486913: mov     ecx, 0D0h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 69,   Prefix4Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[3])
    .desc("Klingon energy start building ID at asm_48692B: mov     ecx, 0FEh"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 93,  Prefix4Start + Suffix_Rom,    MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[4])
    .desc("Romulan energy start building ID at asm_486943: mov     ecx, 12Ch"),
  };

  ValueDefinition<?>[] End4 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START4_ADDRESS + 16,   Prefix4End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_EnergyBldId_end[0])
    .desc("Cardassian energy end building ID at asm_4868F6: cmp     ecx, 7Ch"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START4_ADDRESS + 37,   Prefix4End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[1])
    .desc("Federation energy end building ID at asm_48690B: cmp     ecx, 0AAh"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 61,   Prefix4End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[2])
    .desc("Ferengi energy end building ID at asm_486923: cmp     ecx, 0D8h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 85,   Prefix4End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[3])
    .desc("Klingon energy end building ID at asm_48693B: cmp     ecx, 106h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 109,  Prefix4End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[4])
    .desc("Romulan energy end building ID at asm_486953: cmp     ecx, 134h"),
  };

  ValueDefinition<?>[] Start5 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START5_ADDRESS,        Prefix5Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[0])
      .desc("Cardassian energy start building ID at asm_486A28: mov     ecx, 74h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 21,   Prefix5Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[1])
      .desc("Federation energy start building ID at asm_486A3D: mov     ecx, 0A2h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 45,   Prefix5Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[2])
      .desc("Ferengi energy start building ID at asm_486A55: mov     ecx, 0D0"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 69,   Prefix5Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[3])
      .desc("Klingon energy start building ID at asm_486A6D: mov     ecx, 0FEh"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 93,  Prefix5Start + Suffix_Rom,    MovRegInt.class, Register.ECX, Default_EnergyBldId_begin[4])
      .desc("Romulan energy start building ID at asm_486A85: mov     ecx, 12Ch"),
  };

  ValueDefinition<?>[] End5 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START5_ADDRESS + 16,   Prefix5End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_EnergyBldId_end[0])
    .desc("Cardassian energy end building ID at asm_486A38: cmp     ecx, 7Ch"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START5_ADDRESS + 37,   Prefix5End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[1])
    .desc("Federation energy end building ID at asm_486A4D: cmp     ecx, 0AAh"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 61,   Prefix5End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[2])
    .desc("Ferengi energy end building ID at asm_486A65: cmp     ecx, 0D8h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 85,   Prefix5End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[3])
    .desc("Klingon energy end building ID at asm_486A7D: cmp     ecx, 106h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 109,  Prefix5End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_EnergyBldId_end[4])
    .desc("Romulan energy end building ID at asm_486A95: cmp     ecx, 134h"),
  };

  public static final ValueDefinition<?>[][] Start = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {Start1[0], Start2[0], Start3[0], Start4[0], Start5[0]}, // Cardassians
    new ValueDefinition<?>[] {Start1[1], Start2[1], Start3[1], Start4[1], Start5[1]}, // Federation
    new ValueDefinition<?>[] {Start1[2], Start2[2], Start3[2], Start4[2], Start5[2]}, // Ferengi
    new ValueDefinition<?>[] {Start1[3], Start2[3], Start3[3], Start4[3], Start5[3]}, // Klingons
    new ValueDefinition<?>[] {Start1[4], Start2[4], Start3[4], Start4[4], Start5[4]}  // Romulans
  };

  public static final ValueDefinition<?>[][] End = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {End2[0], End3[0], End4[0], End5[0]},  // Cardassians
    new ValueDefinition<?>[] {End2[1], End3[1], End4[1], End5[1]},  // Federation
    new ValueDefinition<?>[] {End2[2], End3[2], End4[2], End5[2]},  // Ferengi
    new ValueDefinition<?>[] {End2[3], End3[3], End4[3], End5[3]},  // Klingons
    new ValueDefinition<?>[] {End2[4], End3[4], End4[4], End5[4]}   // Romulans
  };

  public static final ValueDefinition<?>[] All = Stream.of(
    Start1, Start2, Start3, Start4, Start5, End2, End3, End4, End5
  ).flatMap(Arrays::stream).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
