package ue.edit.exe.trek.sd;

import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.common.DataPatchSegment;

public class PatchDefinition extends DataDefinition {

  @Getter @Accessors(fluent = true) private final byte[][] patched;

  /**
   * @param address       trek.exe address of the segment
   * @param name          name or prefix of the segment
   * @param type          the segment class type
   * @param orig          original segment data
   * @param patchOptions  patched segment data options
   */
  public PatchDefinition(int address, String name, Class<?> type, byte[] orig, byte[]... patchOptions) {
    super(address, name, type, orig);
    this.patched = patchOptions;
  }

  /**
   * @param address       trek.exe address of the segment
   * @param name          name or prefix of the segment
   * @param orig          original segment data
   * @param patchOptions  patched segment data options
   */
  public PatchDefinition(int address, String name, byte[] orig, byte[]... patchOptions) {
    this(address, name, DataPatchSegment.class, orig, patchOptions);
  }

  public byte[] defaultValue() {
    return data;
  }

  @Override
  public PatchDefinition suffix() {
    super.suffix();
    return this;
  }

  @Override
  public PatchDefinition addrSuffix() {
    super.addrSuffix();
    return this;
  }

  @Override
  public PatchDefinition suffixType() {
    super.suffixType();
    return this;
  }

  @Override
  public PatchDefinition suffixTypeAddr() {
    super.suffixTypeAddr();
    return this;
  }

  /**
   * Specifies the optional register.
   */
  @Override
  public PatchDefinition reg(Register register) {
    super.reg(register);
    return this;
  }

  /**
   * Specifies the optional register, flags and offset.
   */
  @Override
  public PatchDefinition reg(Register register, int flags, int offset) {
    super.reg(register, flags, offset);
    return this;
  }

  /**
   * Specifies the optional flags.
   */
  @Override
  public PatchDefinition flags(int flags) {
    super.flags(flags);
    return this;
  }

  /**
   * Specifies the optional offset.
   */
  @Override
  public PatchDefinition offset(int offset) {
    super.offset(offset);
    return this;
  }

  /**
   * Specifies the optional size.
   */
  @Override
  public PatchDefinition size(int size) {
    super.size(size);
    return this;
  }

  /**
   * Specifies the optional description.
   */
  @Override
  public PatchDefinition desc(String desc) {
    super.desc(desc);
    return this;
  }
}
