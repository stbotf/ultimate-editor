package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.seg.bld.MandatoryDilithiumFix;

/**
 * This interface holds the NumericSectorIdPatch segment definitions.
 */
public interface SD_MandatoryDilFix {

  public static final String Prefix = "mandatoryDilFix"; //$NON-NLS-1$

  public static final SegmentDefinition DilithiumFix =
    new SegmentDefinition(0x505d1, Prefix, MandatoryDilithiumFix.class)
      .desc("This fixture disables to automatically create and activate "
        + "the major empire dilithium refineries on game start."
        + "\nTo disable the requirement it instead checks shipyard creation and activation a second time."
        + "\nIt further overwrites two dilithium refinery id values that must be set when the patch is not applied.")
      .size(MandatoryDilithiumFix.SIZE);

  // orig
  public static final SegmentDefinition OrigRefineryCheck =
    new DataDefinition(0, Prefix, new byte[]{
      (byte) 0x85, (byte) 0xC0,                                        // test eax, eax
      (byte) 0x75, (byte) 0x0C                                         // jnz  short loc_4511E1 (Refinery_present)
    })
    .suffix().desc("unchanged dilithium refinery check:"
      + "\ntest eax, eax"
      + "\njnz  short loc_4511E1 (Refinery_present)");

  // DilithiumBld1
  public static final ValueDefinition<Integer> OrigDilId1 =
    new ValueDefinition<Integer>(4, Prefix + "_dil1", MovRegInt.class, Register.EDX, 6)
      .suffixType().desc("Dilithium refinery id"
        + "\nmov  edx, 6");

  public static final SegmentDefinition OrigRefineryCreation =
    new DataDefinition(9, Prefix, new byte[]{
      (byte) 0x89, (byte) 0xC8,                                         // mov  eax, ecx
      (byte) 0xE8, (byte) 0x0F, (byte) 0x4C, (byte) 0xFF, (byte) 0xFF,  // call sub_445DF0 (add_building_ID_edx__system_ax)
      // Refinery_present 0x505e1 (asm 4511E1):
      (byte) 0x8B, (byte) 0x84, (byte) 0x24,                            // mov  eax, [esp
      (byte) 0x62, (byte) 0x01, (byte) 0x00, (byte) 0x00,               //      +162h] (168h+var_8+2)
    }).suffix().desc("unchanged dilithium refinery creation and activation:"
      + "\nmov  eax, ecx"
      + "\ncall sub_445DF0 (add_building_ID_edx__system_ax)"
      + "\nRefinery_present 0x505e1 (asm 4511E1):"
      + "\nmov  eax, [esp+162h] (168h+var_8+2)");

  // DilithiumBld4
  public static final ValueDefinition<Integer> OrigDilId2 =
    new ValueDefinition<Integer>(23, Prefix + "_dil2", MovRegInt.class, Register.EDX, 6)
    .suffixType().desc("Dilithium refinery id"
      + "\nmov  edx, 6");

  // patched
  public static final SegmentDefinition PatchedRefineryCreation =
    new DataDefinition(0, Prefix, new byte[]{
      (byte) 0x66, (byte) 0x8B, (byte) 0x84, (byte) 0x24,               // mov  ax, word ptr [esp
      (byte) 0x60, (byte) 0x01, (byte) 0x00, (byte) 0x00,               //      +160h] (168h+var_8)
      (byte) 0xE8, (byte) 0xA6, (byte) 0x94, (byte) 0xFE, (byte) 0xFF,  // call sub_43A684
      (byte) 0x89, (byte) 0xC2,                                         // mov  edx, eax
      (byte) 0x90,                                                      // nop
      (byte) 0x8B, (byte) 0x84, (byte) 0x24,                            // mov  eax, [esp
      (byte) 0x62, (byte) 0x01, (byte) 0x00, (byte) 0x00,               //      +162h] (168h+var_8+2)
      (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90   // nops
    })
    .suffix().desc("repeats the shipyard code to replace the automatic dilithium refinery creation:"
      + "\nmov  ax, word ptr [esp+160h] (168h+var_8)"
      + "\ncall sub_43A684"
      + "\nmov  edx, eax"
      + "\nnop"
      + "\nmov  eax, [esp+162h] (168h+var_8+2)"
      + "\nnops");

  // lists the main segment area, which encompasses all the sub-segments
  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    DilithiumFix
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
