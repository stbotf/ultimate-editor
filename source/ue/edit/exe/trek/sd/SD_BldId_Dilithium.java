package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds dilithium refinery id segment definitions.
 */
public interface SD_BldId_Dilithium {

  public static final String Prefix = "dilithiumBld";
  public static final String Prefix_Dil1 = Prefix + "1"; //$NON-NLS-1$
  public static final String Prefix_Dil2 = Prefix + "2"; //$NON-NLS-1$
  public static final String Prefix_Dil3 = Prefix + "3"; //$NON-NLS-1$
  public static final String Prefix_Dil4 = Prefix + "4"; //$NON-NLS-1$
  public static final String Prefix_Dil5 = Prefix + "5"; //$NON-NLS-1$
  public static final String Prefix_Dil6 = Prefix + "6"; //$NON-NLS-1$
  public static final int DIL1_ADDRESS = 0x0005086E;
  public static final int DIL2_ADDRESS = 0x00050CAC;
  public static final int DIL3_ADDRESS = 0x0005085C;
  public static final int DIL4_ADDRESS = 0x0005087A;
  public static final int DIL5_ADDRESS = 0x00050C87;
  public static final int DIL6_ADDRESS = 0x00050CB8;

  // main segments
  public static final ValueDefinition<?>[] All = Stream.of(
    new ValueDefinition<Integer>(DIL1_ADDRESS, Prefix_Dil1, MovRegInt.class, Register.EDX, 6),
    new ValueDefinition<Integer>(DIL2_ADDRESS, Prefix_Dil2, MovRegInt.class, Register.EDX, 6),
    new ValueDefinition<Integer>(DIL3_ADDRESS, Prefix_Dil3, MovRegInt.class, Register.EDX, 6),
    new ValueDefinition<Integer>(DIL4_ADDRESS, Prefix_Dil4, MovRegInt.class, Register.EDX, 6),
    new ValueDefinition<Integer>(DIL5_ADDRESS, Prefix_Dil5, MovRegInt.class, Register.EDX, 6),
    new ValueDefinition<Integer>(DIL6_ADDRESS, Prefix_Dil6, MovRegInt.class, Register.EDX, 6)
  ).map(x -> x.desc("Dilithium refinery building id.")).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
