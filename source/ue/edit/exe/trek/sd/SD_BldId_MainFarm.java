package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.CmpRegInt;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the federation farm segment definition.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=263&t=573
 */
public interface SD_BldId_MainFarm {

  public static final String Prefix         = "farmBld"; //$NON-NLS-1$
  public static final String Prefix1Start   = Prefix + "1_start"; //$NON-NLS-1$
  public static final String Prefix1End     = Prefix + "1_end"; //$NON-NLS-1$
  public static final String Prefix2Start   = Prefix + "2_start"; //$NON-NLS-1$
  public static final String Prefix2End     = Prefix + "2_end"; //$NON-NLS-1$
  public static final String PrefixIndFarm1 = "indFarm1_end"; //$NON-NLS-1$
  public static final String PrefixIndFarm2 = "indFarm2_end"; //$NON-NLS-1$
  public static final String PrefixIndFarm3 = "indFarm3_end"; //$NON-NLS-1$
  public static final String Suffix_Card    = "_Cardassian"; //$NON-NLS-1$
  public static final String Suffix_Fed     = "_Federation"; //$NON-NLS-1$
  public static final String Suffix_Ferg    = "_Ferengi"; //$NON-NLS-1$
  public static final String Suffix_Klng    = "_Klingon"; //$NON-NLS-1$
  public static final String Suffix_Rom     = "_Romulan"; //$NON-NLS-1$

  // sub_486550
  public static final int INDFARM1_ADDRESS  = SD_BldId_MainIndustry.START1_ADDRESS; // 0x0008597A
  public static final int INDFARM2_ADDRESS  = SD_BldId_MainIndustry.START2_ADDRESS; // 0x00085AE0
  public static final int INDFARM3_ADDRESS  = SD_BldId_MainIndustry.START3_ADDRESS; // 0x00085C5E
  public static final int START1_ADDRESS    = 0x00085DA0;
  public static final int START2_ADDRESS    = 0x00085EA2;

  final int[] Default_Farm_begin  = new int[] { 98, 144, 190, 236, 282 };
  final int[] Default_Farm_end    = new int[] { 106, 152, 198, 244, 290 };

  // main farm buildings
  // shared industry/farm end, begins with SD_BldId_MainIndustry.Start
  ValueDefinition<?>[] IndFarm1End = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      INDFARM1_ADDRESS + 35,   PrefixIndFarm1 + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_Farm_end[0])
    .desc("Cardassian industry end building ID at asm_48659D: cmp     ecx, 6Ah"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      INDFARM1_ADDRESS + 56,   PrefixIndFarm1 + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_Farm_end[1])
    .desc("Federation industry end building ID at asm_4865B2: cmp     ecx, 98h"),
    new ValueDefinition<Integer> (
      INDFARM1_ADDRESS + 80,   PrefixIndFarm1 + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_Farm_end[2])
    .desc("Ferengi industry end building ID at asm_4865CA: cmp     ecx, 0C6h"),
    new ValueDefinition<Integer> (
      INDFARM1_ADDRESS + 104,  PrefixIndFarm1 + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_Farm_end[3])
    .desc("Klingon industry end building ID at asm_4865E2: cmp     ecx, 0F4h"),
    new ValueDefinition<Integer> (
      INDFARM1_ADDRESS + 128,  PrefixIndFarm1 + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_Farm_end[4])
    .desc("Romulan industry end building ID at asm_4865FA: cmp     ecx, 122h"),
  };

  ValueDefinition<?>[] IndFarm2End = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      INDFARM2_ADDRESS + 35,   PrefixIndFarm2 + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_Farm_end[0])
    .desc("Cardassian industry end building ID at asm_486703: cmp     ecx, 6Ah"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      INDFARM2_ADDRESS + 56,   PrefixIndFarm2 + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_Farm_end[1])
    .desc("Federation industry end building ID at asm_486718: cmp     ecx, 98h"),
    new ValueDefinition<Integer> (
      INDFARM2_ADDRESS + 80,   PrefixIndFarm2 + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_Farm_end[2])
    .desc("Ferengi industry end building ID at asm_486730: cmp     ecx, 0C6h"),
    new ValueDefinition<Integer> (
      INDFARM2_ADDRESS + 104,  PrefixIndFarm2 + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_Farm_end[3])
    .desc("Klingon industry end building ID at asm_486748: cmp     ecx, 0F4h"),
    new ValueDefinition<Integer> (
      INDFARM2_ADDRESS + 128,  PrefixIndFarm2 + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_Farm_end[4])
    .desc("Romulan industry end building ID at asm_486760: cmp     ecx, 122h"),
  };

  ValueDefinition<?>[] IndFarm3End = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      INDFARM3_ADDRESS + 35,   PrefixIndFarm3 + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_Farm_end[0])
    .desc("Cardassian industry end building ID at asm_486881: cmp     ecx, 6Ah"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      INDFARM3_ADDRESS + 56,   PrefixIndFarm3 + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_Farm_end[1])
    .desc("Federation industry end building ID at asm_486896: cmp     ecx, 98h"),
    new ValueDefinition<Integer> (
      INDFARM3_ADDRESS + 80,   PrefixIndFarm3 + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_Farm_end[2])
    .desc("Ferengi industry end building ID at asm_4868AE: cmp     ecx, 0C6h"),
    new ValueDefinition<Integer> (
      INDFARM3_ADDRESS + 104,  PrefixIndFarm3 + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_Farm_end[3])
    .desc("Klingon industry end building ID at asm_4868C6: cmp     ecx, 0F4h"),
    new ValueDefinition<Integer> (
      INDFARM3_ADDRESS + 128,  PrefixIndFarm3 + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_Farm_end[4])
    .desc("Romulan industry end building ID at asm_4868DE: cmp     ecx, 122h"),
  };

  ValueDefinition<?>[] Start1 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START1_ADDRESS,        Prefix1Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_Farm_begin[0])
      .desc("Cardassian farm start building ID at asm_4869A0: mov     ecx, 62h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 40,   Prefix1Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_Farm_begin[1])
      .desc("Federation farm start building ID at asm_4869C8: mov     ecx, 90h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 64,   Prefix1Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_Farm_begin[2])
      .desc("Ferengi farm start building ID at asm_4869E0: mov     ecx, 0BEh"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 88,   Prefix1Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_Farm_begin[3])
      .desc("Klingon farm start building ID at asm_4869F8: mov     ecx, 0ECh"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 112,  Prefix1Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_Farm_begin[4])
      .desc("Romulan farm start building ID at asm_486A10: mov     ecx, 11Ah"),
  };

  ValueDefinition<?>[] End1 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START1_ADDRESS + 35,   Prefix1End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_Farm_end[0])
    .desc("Cardassian farm end building ID at asm_4869C3: mov     ecx, 6Ah"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START1_ADDRESS + 56,   Prefix1End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_Farm_end[1])
    .desc("Federation farm end building ID at asm_4869D8: cmp     ecx, 98h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 80,   Prefix1End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_Farm_end[2])
    .desc("Ferengi farm end building ID at asm_4869F0: cmp     ecx, 0C6h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 104,  Prefix1End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_Farm_end[3])
    .desc("Klingon farm end building ID at asm_486A08: cmp     ecx, 0F4h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 128,  Prefix1End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_Farm_end[4])
    .desc("Romulan farm end building ID at asm_486A20: cmp     ecx, 122h"),
  };

  ValueDefinition<?>[] Start2 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START2_ADDRESS,        Prefix2Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_Farm_begin[0])
    .desc("Cardassian farm start building ID at asm_486AA2: mov     ecx, 62h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 23,   Prefix2Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_Farm_begin[1])
    .desc("Federation farm start building ID at asm_486AB9: mov     ecx, 90h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 47,   Prefix2Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_Farm_begin[2])
    .desc("Ferengi farm start building ID at asm_486AD1: mov     ecx, 0BEh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 71,   Prefix2Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_Farm_begin[3])
    .desc("Klingon farm start building ID at asm_486AE9: mov     ecx, 0ECh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 95,   Prefix2Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_Farm_begin[4])
    .desc("Romulan farm start building ID at asm_486B01: mov     ecx, 11Ah"),
  };

  ValueDefinition<?>[] End2 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START2_ADDRESS + 18,   Prefix2End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_Farm_end[0])
    .desc("Cardassian farm end building ID at asm_486AB4: cmp     ecx, 6Ah"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START2_ADDRESS + 39,   Prefix2End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_Farm_end[1])
    .desc("Federation farm end building ID at asm_486AC9: cmp     ecx, 98h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 63,   Prefix2End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_Farm_end[2])
    .desc("Ferengi farm end building ID at asm_486AE1: cmp     ecx, 0C6h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 87,   Prefix2End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_Farm_end[3])
    .desc("Klingon farm end building ID at asm_486AF9: cmp     ecx, 0F4h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 111,  Prefix2End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_Farm_end[4])
    .desc("Romulan farm end building ID at asm_486B11: cmp     ecx, 122h"),
  };

  public static final ValueDefinition<?>[][] Start = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {Start1[0], Start2[0]},  // Cardassians
    new ValueDefinition<?>[] {Start1[1], Start2[1]},  // Federation
    new ValueDefinition<?>[] {Start1[2], Start2[2]},  // Ferengi
    new ValueDefinition<?>[] {Start1[3], Start2[3]},  // Klingons
    new ValueDefinition<?>[] {Start1[4], Start2[4]}   // Romulans
  };

  public static final ValueDefinition<?>[][] End = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {IndFarm1End[0], IndFarm2End[0], IndFarm3End[0], End1[0], End2[0]},  // Cardassians
    new ValueDefinition<?>[] {IndFarm1End[1], IndFarm2End[1], IndFarm3End[1], End1[1], End2[1]},  // Federation
    new ValueDefinition<?>[] {IndFarm1End[2], IndFarm2End[2], IndFarm3End[2], End1[2], End2[2]},  // Ferengi
    new ValueDefinition<?>[] {IndFarm1End[3], IndFarm2End[3], IndFarm3End[3], End1[3], End2[3]},  // Klingons
    new ValueDefinition<?>[] {IndFarm1End[4], IndFarm2End[4], IndFarm3End[4], End1[4], End2[4]}   // Romulans
  };

  public static final ValueDefinition<?>[] All = Stream.of(
    Arrays.stream(IndFarm1End),
    Arrays.stream(IndFarm2End),
    Arrays.stream(IndFarm3End),
    Arrays.stream(Start1),
    Arrays.stream(Start2),
    Arrays.stream(End1),
    Arrays.stream(End2)
  ).flatMap(Function.identity()).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
