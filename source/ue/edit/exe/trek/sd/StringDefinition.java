package ue.edit.exe.trek.sd;

import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.exe.seg.prim.StringSegment;

public class StringDefinition extends SegmentDefinition {

  @Getter @Accessors(fluent = true)
  protected final String value;
  @Getter @Accessors(fluent = true)
  protected final boolean nullTerminated;

  /**
   * @param address         trek.exe address of the segment
   * @param name            name or prefix of the segment
   * @param type            the segment class type
   * @param value           string value
   * @param size            segment size
   * @param nullTerminated  is null terminated
   */
  public StringDefinition(int address, String name, Class<?> type, String value, int size, boolean nullTerminated) {
    super(address, name, type, size);
    this.value = value;
    this.nullTerminated = nullTerminated;
  }

  /**
   * @param address         trek.exe address of the segment
   * @param name            name or prefix of the segment
   * @param value           string value
   * @param size            segment size
   * @param nullTerminated  is null terminated
   */
  public StringDefinition(int address, String name, String value, int size, boolean nullTerminated) {
    this(address, name, StringSegment.class, value, size, nullTerminated);
  }

  /**
   * @param address         trek.exe address of the segment
   * @param name            name or prefix of the segment
   * @param value           string value
   * @param size            segment size
   */
  public StringDefinition(int address, String name, String value, int size) {
    this(address, name, StringSegment.class, value, size, true);
  }

  /**
   * Specifies the optional description.
   */
  @Override
  public StringDefinition desc(String desc) {
    super.desc(desc);
    return this;
  }

  // string length
  public int length() {
    return nullTerminated ? size() - 1 : size();
  }

}
