package ue.edit.exe.trek.sd;

import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.seg.bld.MinorRaceSpBuildingIDs;

/**
 * This interface holds the MinorRaceSpBuildingIDs segment definitions.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=33131#p33131
 */
public interface SD_BldId_MinorSp {

  public static final String BldListName = "minorSpBuildingIDs"; //$NON-NLS-1$
  public static final int SEGMENT_ADDRESS = 0x000C1711;

  // main segments
  public static final SegmentDefinition BldList = new SegmentDefinition(
    SEGMENT_ADDRESS, BldListName, MinorRaceSpBuildingIDs.class)
    .desc("Special building ids of each minor race.");

  // sub segments
  public static final ValueDefinition<?>[] Sub = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(SEGMENT_ADDRESS,       BldListName + "_0",  MovRegInt.class, Register.EAX, 56)
      .desc("Acamarian 'Clan Hall' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 6,   BldListName + "_1",  MovRegInt.class, Register.EAX, 57)
      .desc("Andorian 'War College' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 12,  BldListName + "_2",  MovRegInt.class, Register.EAX, 58)
      .desc("Angosian 'Super Soldier Academy' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 18,  BldListName + "_3",  MovRegInt.class, Register.EAX, 59)
      .desc("Antican 'Mustering Base' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 24,  BldListName + "_4",  MovRegInt.class, Register.EAX, 60)
      .desc("Antedean 'Harvesting Complex' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 30,  BldListName + "_5",  MovRegInt.class, Register.EAX, 61)
      .desc("Bajoran 'Jalanda Forum' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 36,  BldListName + "_6",  MovRegInt.class, Register.EAX, 62)
      .desc("Bandi 'Architectural Center' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 42,  BldListName + "_7",  MovRegInt.class, Register.EAX, 63)
      .desc("Benzite 'Industrial Center' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 48,  BldListName + "_8",  MovRegInt.class, Register.EAX, 64)
      .desc("Betazoid 'Counseling Academy' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 54,  BldListName + "_9",  MovRegInt.class, Register.EAX, 65)
      .desc("Bolian 'Cosmetology Center' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 60,  BldListName + "_10", MovRegInt.class, Register.EAX, 66)
      .desc("Bynar 'Planetary Computer' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 66,  BldListName + "_11", MovRegInt.class, Register.EAX, 67)
      .desc("Caldonian 'Research Think Tank' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 72,  BldListName + "_12", MovRegInt.class, Register.EAX, 68)
      .desc("Chalnoth 'Gladiatorial Arena' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 78,  BldListName + "_13", MovRegInt.class, Register.EAX, 69)
      .desc("Edo 'Palace of Edo' building id"),
    // note, the Ullian Psychohistorical Archive is listed out of sequence in trek.exe
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 84,  BldListName + "_26", MovRegInt.class, Register.EAX, 82)
      .desc("Ullian 'Psychohistorical Archive' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 90,  BldListName + "_14", MovRegInt.class, Register.EAX, 70)
      .desc("Ktarian 'Game Studio' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 96,  BldListName + "_15", MovRegInt.class, Register.EAX, 71)
      .desc("Malcorian 'Kinetics Laboratory' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 102, BldListName + "_16", MovRegInt.class, Register.EAX, 72)
      .desc("Mintakan 'Mintakan Farm' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 108, BldListName + "_17", MovRegInt.class, Register.EAX, 73)
      .desc("Mizarian 'Monument of Surrender' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 114, BldListName + "_18", MovRegInt.class, Register.EAX, 74)
      .desc("Nausicaan 'Recruitment Center' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 120, BldListName + "_19", MovRegInt.class, Register.EAX, 75)
      .desc("Pakled 'Collection Facility' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 126, BldListName + "_20", MovRegInt.class, Register.EAX, 76)
      .desc("Selay 'Mustering Base' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 132, BldListName + "_21", MovRegInt.class, Register.EAX, 77)
      .desc("Sheliak 'Bioengineering Center' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 138, BldListName + "_22", MovRegInt.class, Register.EAX, 78)
      .desc("Takaran 'Physics Institute' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 144, BldListName + "_23", MovRegInt.class, Register.EAX, 79)
      .desc("Talarian 'Defense Network' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 150, BldListName + "_24", MovRegInt.class, Register.EAX, 80)
      .desc("Tamarian 'Mythology Library' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 156, BldListName + "_25", MovRegInt.class, Register.EAX, 81)
      .desc("Trill 'Research Committee' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 162, BldListName + "_27", MovRegInt.class, Register.EAX, 83)
      .desc("Vulcan 'Science Academy' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 168, BldListName + "_28", MovRegInt.class, Register.EAX, 84)
      .desc("Yridian 'Intelligence Service' building id"),
    new ValueDefinition<Integer>(SEGMENT_ADDRESS + 174, BldListName + "_29", MovRegInt.class, Register.EAX, 85)
      .desc("Zakdorn 'Military Academy' building id"),
  };
}
