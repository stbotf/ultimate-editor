package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovDwordPtrInt;

/**
 * This interface holds segment definitions for the average numbers of space objects.
 */
public interface SD_AvgSpaceObjNum {

  public static final String Prefix = "avgSpaceObjectNum"; //$NON-NLS-1$
  public static final String SGalName = Prefix + "Small"; //$NON-NLS-1$
  public static final String MGalName = Prefix + "Medium"; //$NON-NLS-1$
  public static final String LGalName = Prefix + "Large"; //$NON-NLS-1$

  public static final int SGAL_SEGMENT_ADDRESS = 0x000AF8A7;
  public static final int MGAL_SEGMENT_ADDRESS = 0x000AFA21;
  public static final int LGAL_SEGMENT_ADDRESS = 0x000AFA37;

  public static final int Default_SGalObjNum = 48;
  public static final int Default_MGalObjNum = 70;
  public static final int Default_LGalObjNum = 90;

  // AverageNumberOfSpaceObjects for small galaxy
  // mov   dword ptr [esp+6Ch], 30h
  public static final ValueDefinition<Integer> SGal = new ValueDefinition<Integer>(
    SGAL_SEGMENT_ADDRESS, SGalName, MovDwordPtrInt.class, Register.ESP, 0, 0x6C, Default_SGalObjNum);

  // AverageNumberOfSpaceObjects for medium galaxy
  // mov   dword ptr [esp+6Ch], 46h
  public static final ValueDefinition<Integer> MGal = new ValueDefinition<Integer>(
    MGAL_SEGMENT_ADDRESS, MGalName, MovDwordPtrInt.class, Register.ESP, 0, 0x6C, Default_MGalObjNum);

  // AverageNumberOfSpaceObjects for large galaxy
  // mov   dword ptr [esp+6Ch], 5Ah
  public static final ValueDefinition<Integer> LGal = new ValueDefinition<Integer>(
    LGAL_SEGMENT_ADDRESS, LGalName, MovDwordPtrInt.class, Register.ESP, 0, 0x6C, Default_LGalObjNum);

  public static final ValueDefinition<?>[] All = new ValueDefinition<?>[] { SGal, MGal, LGal };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
