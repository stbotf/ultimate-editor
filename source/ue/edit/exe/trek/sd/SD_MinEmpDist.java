package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the MinEmpireDistancePatch segment definitions.
 */
public interface SD_MinEmpDist {

  public static final String Prefix = "minEmpDist"; //$NON-NLS-1$

  // minEmpireDistance_address
  public static final ValueDefinition<?>[][] GalSectors = new ValueDefinition<?>[][] {
    // small: irregular, ellipse, ring, spiral
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae543, Prefix, MovRegInt.class, Register.ECX, 4)
        .suffix().desc("defaults to 4 (irregular galaxy)"),
      new ValueDefinition<Integer>(0x000ae562, Prefix, MovRegInt.class, Register.ECX, 3)
        .suffix().desc("defaults to 3 (ellipse galaxy)"),
      SD_GalShape.Shared_SmallRingWidth, // defaults to 4 (ring galaxy)
      new ValueDefinition<Integer>(0x000ae5e5, Prefix, MovRegInt.class, Register.ECX, 3)
        .suffix().desc("defaults to 3 (spiral galaxy)"),
    },
    // medium: irregular, ellipse, ring, spiral
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae4a0, Prefix, MovRegInt.class, Register.ECX, 4)
        .suffix().desc("defaults to 4 (irregular galaxy)"),
      new ValueDefinition<Integer>(0x000ae5fe, Prefix, MovRegInt.class, Register.ECX, 4)
        .suffix().desc("defaults to 4 (ellipse galaxy)"),
      new ValueDefinition<Integer>(0x000ae642, Prefix, MovRegInt.class, Register.ECX, 4)
        .suffix().desc("defaults to 4 (ring galaxy)"),
      new ValueDefinition<Integer>(0x000ae664, Prefix, MovRegInt.class, Register.ECX, 4)
        .suffix().desc("defaults to 4 (spiral galaxy)"),
    },
    // large: irregular, ellipse, ring, spiral
    new ValueDefinition<?>[] {
      new ValueDefinition<Integer>(0x000ae6f4, Prefix, MovRegInt.class, Register.ECX, 6)
        .suffix().desc("defaults to 6 (irregular galaxy)"),
      new ValueDefinition<Integer>(0x000ae6d1, Prefix, MovRegInt.class, Register.ECX, 6)
        .suffix().desc("defaults to 6 (ellipse galaxy)"),
      SD_GalShape.Shared_LargeRingWidth, // defaults to 6 (ring galaxy)
      new ValueDefinition<Integer>(0x000ae758, Prefix, MovRegInt.class, Register.ECX, 6)
        .suffix().desc("defaults to 6 (spiral galaxy)"),
    }
  };

  public static final SegmentDefinition[] All = Stream.of(
    Arrays.stream(GalSectors).flatMap(Arrays::stream),
    Arrays.stream(SD_MinEmpDistFix.All)
  ).flatMap(x -> x).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
