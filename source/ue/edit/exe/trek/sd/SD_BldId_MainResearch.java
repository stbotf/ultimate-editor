package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpRegInt;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.patch.bld.BuildingCountPatch;

/**
 * This interface holds the main research start building id segment definitions.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=263&t=573
 *
 * Note that the last empire 4 research building happens to match the max building id.
 * Segments: 0x000851C3-2, 0x00085296-2, 0x00085345-2, 0x000853D0-2, 0x0008544F-2
 * @see BuildingCountPatch
 */
public interface SD_BldId_MainResearch {

  public static final String Prefix = "mainResearchBld"; //$NON-NLS-1$
  public static final String Prefix1Start = Prefix + "1_start"; //$NON-NLS-1$
  public static final String Prefix2Start = Prefix + "2_start"; //$NON-NLS-1$
  public static final String Prefix2End   = Prefix + "2_end"; //$NON-NLS-1$
  public static final String Prefix3Start = Prefix + "3_start"; //$NON-NLS-1$
  public static final String Prefix3End   = Prefix + "3_end"; //$NON-NLS-1$
  public static final String Prefix4Start = Prefix + "4_start"; //$NON-NLS-1$
  public static final String Prefix4End   = Prefix + "4_end"; //$NON-NLS-1$
  public static final String Prefix5Start = Prefix + "5_start"; //$NON-NLS-1$
  public static final String Prefix5End   = Prefix + "5_end"; //$NON-NLS-1$
  public static final String Prefix6Start = Prefix + "6_start"; //$NON-NLS-1$
  public static final String Prefix6End   = Prefix + "6_end"; //$NON-NLS-1$
  public static final String Prefix7Start = Prefix + "7_start"; //$NON-NLS-1$
  public static final String Prefix7End   = Prefix + "7_end"; //$NON-NLS-1$
  public static final String Prefix8Start = Prefix + "8_start"; //$NON-NLS-1$
  public static final String Prefix8End   = Prefix + "8_end"; //$NON-NLS-1$
  public static final String Prefix9Start = Prefix + "9_start"; //$NON-NLS-1$
  public static final String Prefix9End   = Prefix + "9_end"; //$NON-NLS-1$
  public static final String Suffix_Card  = "_Cardassian"; //$NON-NLS-1$
  public static final String Suffix_Fed   = "_Federation"; //$NON-NLS-1$
  public static final String Suffix_Ferg  = "_Ferengi"; //$NON-NLS-1$
  public static final String Suffix_Klng  = "_Klingon"; //$NON-NLS-1$
  public static final String Suffix_Rom   = "_Romulan"; //$NON-NLS-1$

  // sub_444EE0
  public static final int START1_ADDRESS  = 0x000442EF;
  // sub_485CF0
  public static final int START2_ADDRESS  = 0x0008513E;
  public static final int START3_ADDRESS  = 0x00085211;
  public static final int START4_ADDRESS  = 0x000852C0;
  public static final int START5_ADDRESS  = 0x00085357;
  public static final int START6_ADDRESS  = 0x000853DB;
  // sub_486C50
  public static final int START7_ADDRESS  = 0x000861BB;
  public static final int START8_ADDRESS  = 0x0008625E;
  public static final int START9_ADDRESS  = 0x000862F5;

  public static int[] Default_ResearchBldStartIds = new int[] { 125, 171, 217, 263, 309 };
  public static int[] Default_ResearchBldEndIds   = new int[] { 133, 179, 225, 271, 317 };

  // Main research buildings
  ValueDefinition<?>[] Start1 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START1_ADDRESS,       Prefix1Start + Suffix_Card,  MovRegInt.class, Register.EAX, Default_ResearchBldStartIds[0])
      .desc("Cardassian research start building ID at asm_444EEF: mov     eax, 7Dh"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 37,  Prefix1Start + Suffix_Fed,   MovRegInt.class, Register.EAX, Default_ResearchBldStartIds[1])
      .desc("Federation research start building ID at asm_444F14: mov     eax, 0ABh"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 59,  Prefix1Start + Suffix_Ferg,  MovRegInt.class, Register.EAX, Default_ResearchBldStartIds[2])
      .desc("Ferengi research start building ID at asm_444F2A: mov     eax, 0D9h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 81,  Prefix1Start + Suffix_Klng,  MovRegInt.class, Register.EAX, Default_ResearchBldStartIds[3])
      .desc("Klingon research start building ID at asm_444F40: mov     eax, 107h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 103, Prefix1Start + Suffix_Rom,   MovRegInt.class, Register.EAX, Default_ResearchBldStartIds[4])
      .desc("Romulan research start building ID at asm_444F56: mov     eax, 135h"),
  };

  ValueDefinition<?>[] Start2 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START2_ADDRESS,       Prefix2Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_485D3E: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 43,  Prefix2Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_485D69: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 67,  Prefix2Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_485D81: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 91,  Prefix2Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_485D99: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 115, Prefix2Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_485DB1: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End2 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START2_ADDRESS + 35,  Prefix2End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_485D61: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 59,  Prefix2End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_485D79: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 83,  Prefix2End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_485D91: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 107, Prefix2End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_485DA9: cmp     ecx, 10Fh"),
    // 0x000851C3 - 2: Edifice patcher => MainResearchBld.end_id(it0,emp4)
    new ValueDefinition<Integer> (
      START2_ADDRESS + 131, Prefix2End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_485DC1: cmp     ecx, 13Dh")
  };

  ValueDefinition<?>[] Start3 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START3_ADDRESS,       Prefix3Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_485E11: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 43,  Prefix3Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_485E3C: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 67,  Prefix3Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_485E54: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 91,  Prefix3Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_485E6C: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 115, Prefix3Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_485E84: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End3 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START3_ADDRESS + 35,  Prefix3End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_485E34: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 59,  Prefix3End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_485E4C: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 83,  Prefix3End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_485E64: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 107, Prefix3End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_485E7C: cmp     ecx, 10Fh"),
    // 0x00085296 - 2: Edifice patcher => MainResearchBld.end_id(it1,emp4)
    new ValueDefinition<Integer> (
      START3_ADDRESS + 131, Prefix3End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_485E94: cmp     ecx, 13Dh")
  };

  ValueDefinition<?>[] Start4 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START4_ADDRESS,       Prefix4Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_485EC0: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 43,  Prefix4Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_485EEB: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 67,  Prefix4Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_485F03: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 91,  Prefix4Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_485F1B: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 115, Prefix4Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_485F33: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End4 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START4_ADDRESS + 35,  Prefix4End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_485EE3: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 59,  Prefix4End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_485EFB: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 83,  Prefix4End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_485F13: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 107, Prefix4End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_485F2B: cmp     ecx, 10Fh"),
    // 0x00085345 - 2: Edifice patcher => MainResearchBld.end_id(it2,emp4)
    new ValueDefinition<Integer> (
      START4_ADDRESS + 131, Prefix4End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_485F43: cmp     ecx, 13Dh")
  };

  ValueDefinition<?>[] Start5 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START5_ADDRESS,       Prefix5Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_485F57: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 31,  Prefix5Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_485F76: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 55,  Prefix5Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_485F8E: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 79,  Prefix5Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_485FA6: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 103, Prefix5Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_485FBE: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End5 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START5_ADDRESS + 23,  Prefix5End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_485F6E: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 47,  Prefix5End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_485F86: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 71,  Prefix5End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_485F9E: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 95,  Prefix5End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_485FB6: cmp     ecx, 10Fh"),
    // 0x000853D0 - 2: Edifice patcher => MainResearchBld.end_id(it3,emp4)
    new ValueDefinition<Integer> (
      START5_ADDRESS + 119, Prefix5End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_485FCE: cmp     ecx, 13Dh")
  };

  ValueDefinition<?>[] Start6 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START6_ADDRESS,       Prefix6Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_485FDB: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 26,  Prefix6Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_485FF5: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 50,  Prefix6Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_48600D: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 74,  Prefix6Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_486025: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 98,  Prefix6Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_48603D: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End6 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START6_ADDRESS + 18,  Prefix6End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_485FED: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 42,  Prefix6End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_486005: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 66,  Prefix6End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_48601D: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 90,  Prefix6End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_486035: cmp     ecx, 10Fh"),
    // 0x0008544F - 2: Edifice patcher => MainResearchBld.end_id(it4,emp4)
    new ValueDefinition<Integer> (
      START6_ADDRESS + 114, Prefix6End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_48604D: cmp     ecx, 13Dh")
  };

  ValueDefinition<?>[] Start7 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START7_ADDRESS,       Prefix7Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_486DBB: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 43,  Prefix7Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_486DE6: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 67,  Prefix7Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_486DFE: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 91,  Prefix7Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_486E16: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 115,  Prefix7Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_486E2E: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End7 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START7_ADDRESS + 35,  Prefix7End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_486DDE: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 59,  Prefix7End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_486DF6: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 83,  Prefix7End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_486E0E: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 107, Prefix7End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_486E26: cmp     ecx, 10Fh"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 131, Prefix7End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_486E3E: cmp     ecx, 13Dh")
  };

  ValueDefinition<?>[] Start8 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START8_ADDRESS,       Prefix8Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_486E5E: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 43,  Prefix8Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_486E89: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 67,  Prefix8Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_486EA1: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 91,  Prefix8Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_486EB9: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 115,  Prefix8Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_486ED1: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End8 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START8_ADDRESS + 35,  Prefix8End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_486E81: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 59,  Prefix8End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_486E99: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 83,  Prefix8End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_486EB1: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 107, Prefix8End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_486EC9: cmp     ecx, 10Fh"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 131, Prefix8End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_486EE1: cmp     ecx, 13Dh")
  };

  ValueDefinition<?>[] Start9 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START9_ADDRESS,       Prefix9Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[0])
    .desc("Cardassian research start building ID at asm_486EF5: mov     ecx, 7Dh"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 31,  Prefix9Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[1])
    .desc("Federation research start building ID at asm_486F14: mov     ecx, 0ABh"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 55,  Prefix9Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[2])
    .desc("Ferengi research start building ID at asm_486F2C: mov     ecx, 0D9h"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 79,  Prefix9Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[3])
    .desc("Klingon research start building ID at asm_486F44: mov     ecx, 107h"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 103, Prefix9Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_ResearchBldStartIds[4])
    .desc("Romulan research start building ID at asm_486F5C: mov     ecx, 135h"),
  };

  ValueDefinition<?>[] End9 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START9_ADDRESS + 23,  Prefix9End + Suffix_Card,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[0])
    .desc("Cardassian research end building ID at asm_486F0C: cmp     ecx, 85h"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 47,  Prefix9End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[1])
    .desc("Federation research end building ID at asm_486F24: cmp     ecx, 0B3h"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 71,  Prefix9End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[2])
    .desc("Ferengi research end building ID at asm_486F3C: cmp     ecx, 0E1h"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 95,  Prefix9End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[3])
    .desc("Klingon research end building ID at asm_486F54: cmp     ecx, 10Fh"),
    new ValueDefinition<Integer> (
      START9_ADDRESS + 119, Prefix9End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_ResearchBldEndIds[4])
    .desc("Romulan research end building ID at asm_486F6C: cmp     ecx, 13Dh")
  };

  public static final ValueDefinition<?>[][] Start = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {Start1[0], Start2[0], Start3[0], Start4[0], Start5[0], Start6[0], Start7[0], Start8[0], Start9[0]}, // Cardassians
    new ValueDefinition<?>[] {Start1[1], Start2[1], Start3[1], Start4[1], Start5[1], Start6[1], Start7[1], Start8[1], Start9[1]}, // Federation
    new ValueDefinition<?>[] {Start1[2], Start2[2], Start3[2], Start4[2], Start5[2], Start6[2], Start7[2], Start8[2], Start9[2]}, // Ferengi
    new ValueDefinition<?>[] {Start1[3], Start2[3], Start3[3], Start4[3], Start5[3], Start6[3], Start7[3], Start8[3], Start9[3]}, // Klingons
    new ValueDefinition<?>[] {Start1[4], Start2[4], Start3[4], Start4[4], Start5[4], Start6[4], Start7[4], Start8[4], Start9[4]}  // Romulans
  };

  public static final ValueDefinition<?>[][] End = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {End2[0], End3[0], End4[0], End5[0], End6[0], End7[0], End8[0], End9[0]},  // Cardassians
    new ValueDefinition<?>[] {End2[1], End3[1], End4[1], End5[1], End6[1], End7[1], End8[1], End9[1]},  // Federation
    new ValueDefinition<?>[] {End2[2], End3[2], End4[2], End5[2], End6[2], End7[2], End8[2], End9[2]},  // Ferengi
    new ValueDefinition<?>[] {End2[3], End3[3], End4[3], End5[3], End6[3], End7[3], End8[3], End9[3]},  // Klingons
    new ValueDefinition<?>[] {End2[4], End3[4], End4[4], End5[4], End6[4], End7[4], End8[4], End9[4]}   // Romulans
  };

  public static final ValueDefinition<?>[] All = Stream.of(
    Start1, Start2, Start3, Start4, Start5, Start6, Start7, Start8, Start9,
    End2, End3, End4, End5, End6, End7, End8, End9
  ).flatMap(Arrays::stream).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
