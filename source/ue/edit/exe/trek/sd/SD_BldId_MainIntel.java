package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.CmpRegInt;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the main intel start building id segment definitions.
 * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=263&t=573
 */
public interface SD_BldId_MainIntel {

  public static final String Prefix       = "mainIntelBld"; //$NON-NLS-1$
  public static final String Prefix1Start = Prefix + "1_start"; //$NON-NLS-1$
  public static final String Prefix2Start = Prefix + "2_start"; //$NON-NLS-1$
  public static final String Prefix2End   = Prefix + "2_end"; //$NON-NLS-1$
  public static final String Prefix3Start = Prefix + "3_start"; //$NON-NLS-1$
  public static final String Prefix3End   = Prefix + "3_end"; //$NON-NLS-1$
  public static final String Prefix4Start = Prefix + "4_start"; //$NON-NLS-1$
  public static final String Prefix4End   = Prefix + "4_end"; //$NON-NLS-1$
  public static final String Prefix5Start = Prefix + "5_start"; //$NON-NLS-1$
  public static final String Prefix5End   = Prefix + "5_end"; //$NON-NLS-1$
  public static final String Prefix6Start = Prefix + "6_start"; //$NON-NLS-1$
  public static final String Prefix6End   = Prefix + "6_end"; //$NON-NLS-1$
  public static final String Prefix7Start = Prefix + "7_start"; //$NON-NLS-1$
  public static final String Prefix7End   = Prefix + "7_end"; //$NON-NLS-1$
  public static final String Prefix8Start = Prefix + "8_start"; //$NON-NLS-1$
  public static final String Prefix8End   = Prefix + "8_end"; //$NON-NLS-1$
  public static final String Prefix9Start = Prefix + "9_start"; //$NON-NLS-1$
  public static final String Prefix9End   = Prefix + "9_end"; //$NON-NLS-1$
  public static final String Suffix_Card  = "_Cardassian"; //$NON-NLS-1$
  public static final String Suffix_Fed   = "_Federation"; //$NON-NLS-1$
  public static final String Suffix_Ferg  = "_Ferengi"; //$NON-NLS-1$
  public static final String Suffix_Klng  = "_Klingon"; //$NON-NLS-1$
  public static final String Suffix_Rom   = "_Romulan"; //$NON-NLS-1$

  // sub_444E20
  public static final int START1_ADDRESS  = 0x0004422F;
  // sub_4860A0
  public static final int START2_ADDRESS  = 0x0008558A;
  public static final int START3_ADDRESS  = 0x000856D2;
  public static final int START4_ADDRESS  = 0x000857DE;
  public static final int START5_ADDRESS  = 0x0008587E;
  // sub_486FC0
  public static final int START6_ADDRESS  = 0x00086531;
  public static final int START7_ADDRESS  = 0x00086601;
  public static final int START8_ADDRESS  = 0x000866AD;

  public static int[] Default_IntelBldStartIds  = new int[] { 107, 153, 199, 245, 291 };
  public static int[] Default_IntelBldEndIds    = new int[] { 115, 161, 207, 253, 299 };

  // Main intel buildings
  ValueDefinition<?>[] Start1 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START1_ADDRESS,       Prefix1Start + Suffix_Card,  MovRegInt.class, Register.EAX, Default_IntelBldStartIds[0])
      .desc("Cardassian intel start building ID at asm_444E2F: mov     eax, 6Bh"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 37,  Prefix1Start + Suffix_Fed,   MovRegInt.class, Register.EAX, Default_IntelBldStartIds[1])
      .desc("Federation intel start building ID at asm_444E54: mov     eax, 99h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 59,  Prefix1Start + Suffix_Ferg,  MovRegInt.class, Register.EAX, Default_IntelBldStartIds[2])
      .desc("Ferengi intel start building ID at asm_444E6A: mov     eax, 0C7h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 81,  Prefix1Start + Suffix_Klng,  MovRegInt.class, Register.EAX, Default_IntelBldStartIds[3])
      .desc("Klingon intel start building ID at asm_444E80: mov     eax, 0F5h"),
    new ValueDefinition<Integer> (
      START1_ADDRESS + 103, Prefix1Start + Suffix_Rom,   MovRegInt.class, Register.EAX, Default_IntelBldStartIds[4])
      .desc("Romulan intel start building ID at asm_444E96: mov     eax, 123h")
  };

  ValueDefinition<?>[] Start2 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START2_ADDRESS,       Prefix2Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[0])
    .desc("Cardassian intel start building ID at asm_48618A: mov     ecx, 6Bh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 40,  Prefix2Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[1])
    .desc("Federation intel start building ID at asm_4861B2: mov     ecx, 99h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 64,  Prefix2Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[2])
    .desc("Ferengi intel start building ID at asm_4861CA: mov     ecx, 0C7h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 88,  Prefix2Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[3])
    .desc("Klingon intel start building ID at asm_4861E2: mov     ecx, 0F5h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 112, Prefix2Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[4])
    .desc("Romulan intel start building ID at asm_4861FA: mov     ecx, 123h")
  };

  ValueDefinition<?>[] End2 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START2_ADDRESS + 35,  Prefix2End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_IntelBldEndIds[0])
    .desc("Cardassian intel end building ID at asm_4861AD: cmp     ecx, 73h"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START2_ADDRESS + 56,  Prefix2End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[1])
    .desc("Federation intel end building ID at asm_4861C2: cmp     ecx, 0A1h"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 80,  Prefix2End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[2])
    .desc("Ferengi intel end building ID at asm_4861DA: cmp     ecx, 0CFh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 104, Prefix2End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[3])
    .desc("Klingon intel end building ID at asm_4861F2: cmp     ecx, 0FDh"),
    new ValueDefinition<Integer> (
      START2_ADDRESS + 128, Prefix2End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[4])
    .desc("Romulan intel end building ID at asm_48620A: cmp     ecx, 12Bh")
  };

  ValueDefinition<?>[] Start3 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START3_ADDRESS,       Prefix3Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[0])
    .desc("Cardassian intel start building ID at asm_4862D2: mov     ecx, 6Bh"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 40,  Prefix3Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[1])
    .desc("Federation intel start building ID at asm_4862FA: mov     ecx, 99h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 64,  Prefix3Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[2])
    .desc("Ferengi intel start building ID at asm_486312: mov     ecx, 0C7h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 88,  Prefix3Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[3])
    .desc("Klingon intel start building ID at asm_48632A: mov     ecx, 0F5h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 112, Prefix3Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[4])
    .desc("Romulan intel start building ID at asm_486342: mov     ecx, 123h")
  };

  ValueDefinition<?>[] End3 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START3_ADDRESS + 35,  Prefix3End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_IntelBldEndIds[0])
    .desc("Cardassian intel end building ID at asm_4862F5: cmp     ecx, 73h"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START3_ADDRESS + 56,  Prefix3End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[1])
    .desc("Federation intel end building ID at asm_48630A: cmp     ecx, 0A1h"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 80,  Prefix3End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[2])
    .desc("Ferengi intel end building ID at asm_486322: cmp     ecx, 0CFh"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 104, Prefix3End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[3])
    .desc("Klingon intel end building ID at asm_48633A: cmp     ecx, 0FDh"),
    new ValueDefinition<Integer> (
      START3_ADDRESS + 128, Prefix3End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[4])
    .desc("Romulan intel end building ID at asm_486352: cmp     ecx, 12Bh")
  };

  ValueDefinition<?>[] Start4 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START4_ADDRESS,       Prefix4Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[0])
    .desc("Cardassian intel start building ID at asm_4863DE: mov     ecx, 6Bh"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 40,  Prefix4Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[1])
    .desc("Federation intel start building ID at asm_486406: mov     ecx, 99h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 64,  Prefix4Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[2])
    .desc("Ferengi intel start building ID at asm_48641E: mov     ecx, 0C7h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 88,  Prefix4Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[3])
    .desc("Klingon intel start building ID at asm_486436: mov     ecx, 0F5h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 112, Prefix4Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[4])
    .desc("Romulan intel start building ID at asm_48644E: mov     ecx, 123h")
  };

  ValueDefinition<?>[] End4 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START4_ADDRESS + 35,  Prefix4End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_IntelBldEndIds[0])
    .desc("Cardassian intel end building ID at asm_486401: cmp     ecx, 73h"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START4_ADDRESS + 56,  Prefix4End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[1])
    .desc("Federation intel end building ID at asm_486416: cmp     ecx, 0A1h"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 80,  Prefix4End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[2])
    .desc("Ferengi intel end building ID at asm_48642E: cmp     ecx, 0CFh"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 104, Prefix4End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[3])
    .desc("Klingon intel end building ID at asm_486446: cmp     ecx, 0FDh"),
    new ValueDefinition<Integer> (
      START4_ADDRESS + 128, Prefix4End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[4])
    .desc("Romulan intel end building ID at asm_48645E: cmp     ecx, 12Bh")
  };

  ValueDefinition<?>[] Start5 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START5_ADDRESS,       Prefix5Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[0])
    .desc("Cardassian intel start building ID at asm_48647E: mov     ecx, 6Bh"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 40,  Prefix5Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[1])
    .desc("Federation intel start building ID at asm_4864A6: mov     ecx, 99h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 64,  Prefix5Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[2])
    .desc("Ferengi intel start building ID at asm_4864BE: mov     ecx, 0C7h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 88,  Prefix5Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[3])
    .desc("Klingon intel start building ID at asm_4864D6: mov     ecx, 0F5h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 112, Prefix5Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[4])
    .desc("Romulan intel start building ID at asm_4864EE: mov     ecx, 123h")
  };

  ValueDefinition<?>[] End5 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START5_ADDRESS + 35,  Prefix5End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_IntelBldEndIds[0])
    .desc("Cardassian intel end building ID at asm_4864A1: cmp     ecx, 73h"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START5_ADDRESS + 56,  Prefix5End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[1])
    .desc("Federation intel end building ID at asm_4864B6: cmp     ecx, 0A1h"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 80,  Prefix5End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[2])
    .desc("Ferengi intel end building ID at asm_4864CE: cmp     ecx, 0CFh"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 104, Prefix5End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[3])
    .desc("Klingon intel end building ID at asm_4864E6: cmp     ecx, 0FDh"),
    new ValueDefinition<Integer> (
      START5_ADDRESS + 128, Prefix5End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[4])
    .desc("Romulan intel end building ID at asm_4864FE: cmp     ecx, 12Bh")
  };

  ValueDefinition<?>[] Start6 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START6_ADDRESS,       Prefix6Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[0])
    .desc("Cardassian intel start building ID at asm_487131: mov     ecx, 6Bh"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 40,  Prefix6Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[1])
    .desc("Federation intel start building ID at asm_487159: mov     ecx, 99h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 64,  Prefix6Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[2])
    .desc("Ferengi intel start building ID at asm_487171: mov     ecx, 0C7h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 88,  Prefix6Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[3])
    .desc("Klingon intel start building ID at asm_487189: mov     ecx, 0F5h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 112, Prefix6Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[4])
    .desc("Romulan intel start building ID at asm_4871A1: mov     ecx, 123h")
  };

  ValueDefinition<?>[] End6 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START6_ADDRESS + 35,  Prefix6End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_IntelBldEndIds[0])
    .desc("Cardassian intel end building ID at asm_487154: cmp     ecx, 73h"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START6_ADDRESS + 56,  Prefix6End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[1])
    .desc("Federation intel end building ID at asm_487169: cmp     ecx, 0A1h"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 80,  Prefix6End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[2])
    .desc("Ferengi intel end building ID at asm_487181: cmp     ecx, 0CFh"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 104, Prefix6End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[3])
    .desc("Klingon intel end building ID at asm_487199: cmp     ecx, 0FDh"),
    new ValueDefinition<Integer> (
      START6_ADDRESS + 128, Prefix6End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[4])
    .desc("Romulan intel end building ID at asm_4871B1: cmp     ecx, 12Bh")
  };

  ValueDefinition<?>[] Start7 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START7_ADDRESS,       Prefix7Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[0])
    .desc("Cardassian intel start building ID at asm_487201: mov     ecx, 6Bh"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 40,  Prefix7Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[1])
    .desc("Federation intel start building ID at asm_487229: mov     ecx, 99h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 64,  Prefix7Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[2])
    .desc("Ferengi intel start building ID at asm_487241: mov     ecx, 0C7h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 88,  Prefix7Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[3])
    .desc("Klingon intel start building ID at asm_487259: mov     ecx, 0F5h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 112, Prefix7Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[4])
    .desc("Romulan intel start building ID at asm_487271: mov     ecx, 123h")
  };

  ValueDefinition<?>[] End7 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START7_ADDRESS + 35,  Prefix7End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_IntelBldEndIds[0])
    .desc("Cardassian intel end building ID at asm_487224: cmp     ecx, 73h"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START7_ADDRESS + 56,  Prefix7End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[1])
    .desc("Federation intel end building ID at asm_487239: cmp     ecx, 0A1h"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 80,  Prefix7End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[2])
    .desc("Ferengi intel end building ID at asm_487251: cmp     ecx, 0CFh"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 104, Prefix7End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[3])
    .desc("Klingon intel end building ID at asm_487269: cmp     ecx, 0FDh"),
    new ValueDefinition<Integer> (
      START7_ADDRESS + 128, Prefix7End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[4])
    .desc("Romulan intel end building ID at asm_487281: cmp     ecx, 12Bh")
  };

  ValueDefinition<?>[] Start8 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer> (
      START8_ADDRESS,       Prefix8Start + Suffix_Card,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[0])
    .desc("Cardassian intel start building ID at asm_4872AD: mov     ecx, 6Bh"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 40,  Prefix8Start + Suffix_Fed,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[1])
    .desc("Federation intel start building ID at asm_4872D5: mov     ecx, 99h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 64,  Prefix8Start + Suffix_Ferg,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[2])
    .desc("Ferengi intel start building ID at asm_4872ED: mov     ecx, 0C7h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 88,  Prefix8Start + Suffix_Klng,  MovRegInt.class, Register.ECX, Default_IntelBldStartIds[3])
    .desc("Klingon intel start building ID at asm_487305: mov     ecx, 0F5h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 112, Prefix8Start + Suffix_Rom,   MovRegInt.class, Register.ECX, Default_IntelBldStartIds[4])
    .desc("Romulan intel start building ID at asm_48731D: mov     ecx, 123h")
  };

  ValueDefinition<?>[] End8 = new ValueDefinition<?>[] {
    new ValueDefinition<Byte> (
      START8_ADDRESS + 35,  Prefix8End + Suffix_Card,  CmpRegByte.class, Register.ECX, (byte)Default_IntelBldEndIds[0])
    .desc("Cardassian intel end building ID at asm_4872D0: cmp     ecx, 73h"),
    // Attention, for all but the Cardassians it is an integer value!
    new ValueDefinition<Integer> (
      START8_ADDRESS + 56,  Prefix8End + Suffix_Fed,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[1])
    .desc("Federation intel end building ID at asm_4872E5: cmp     ecx, 0A1h"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 80,  Prefix8End + Suffix_Ferg,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[2])
    .desc("Ferengi intel end building ID at asm_4872FD: cmp     ecx, 0CFh"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 104, Prefix8End + Suffix_Klng,  CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[3])
    .desc("Klingon intel end building ID at asm_487315: cmp     ecx, 0FDh"),
    new ValueDefinition<Integer> (
      START8_ADDRESS + 128, Prefix8End + Suffix_Rom,   CmpRegInt.class, Register.ECX, Default_IntelBldEndIds[4])
    .desc("Romulan intel end building ID at asm_48732D: cmp     ecx, 12Bh")
  };

  public static final ValueDefinition<?>[][] Start = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {Start1[0], Start2[0], Start3[0], Start4[0], Start5[0], Start6[0], Start7[0], Start8[0]},  // Cardassians
    new ValueDefinition<?>[] {Start1[1], Start2[1], Start3[1], Start4[1], Start5[1], Start6[1], Start7[1], Start8[1]},  // Federation
    new ValueDefinition<?>[] {Start1[2], Start2[2], Start3[2], Start4[2], Start5[2], Start6[2], Start7[2], Start8[2]},  // Ferengi
    new ValueDefinition<?>[] {Start1[3], Start2[3], Start3[3], Start4[3], Start5[3], Start6[3], Start7[3], Start8[3]},  // Klingons
    new ValueDefinition<?>[] {Start1[4], Start2[4], Start3[4], Start4[4], Start5[4], Start6[4], Start7[4], Start8[4]}   // Romulans
  };

  public static final ValueDefinition<?>[][] End = new ValueDefinition<?>[][] {
    new ValueDefinition<?>[] {End2[0], End3[0], End4[0], End5[0], End6[0], End7[0], End8[0]}, // Cardassians
    new ValueDefinition<?>[] {End2[1], End3[1], End4[1], End5[1], End6[1], End7[1], End8[1]}, // Federation
    new ValueDefinition<?>[] {End2[2], End3[2], End4[2], End5[2], End6[2], End7[2], End8[2]}, // Ferengi
    new ValueDefinition<?>[] {End2[3], End3[3], End4[3], End5[3], End6[3], End7[3], End8[3]}, // Klingons
    new ValueDefinition<?>[] {End2[4], End3[4], End4[4], End5[4], End6[4], End7[4], End8[4]}  // Romulans
  };

  public static final ValueDefinition<?>[] All = Stream.of(
    Start1, Start2, Start3, Start4, Start5, Start6, Start7, Start8,
    End2, End3, End4, End5, End6, End7, End8
  ).flatMap(Arrays::stream).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
