package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the special empire building id segment definitions.
 */
public interface SD_BldId_EmpSp {

  public static final String FedFarmLvl1BldName = "fedFarmLvL1"; //$NON-NLS-1$
  public static final String UtopiaPlanitiaName = "utopiaPlanitiaBld"; //$NON-NLS-1$

  public static final int FED1_ADDRESS = 0x0005157E;  // sub_451F20
  public static final int UTPL_ADDRESS = 0x000F2300;  // sub_4F2B6C

  // federation primitive farm
  public static final ValueDefinition<Integer> Fed1 = new ValueDefinition<Integer>(
    FED1_ADDRESS, FedFarmLvl1BldName, MovRegInt.class, Register.ESI, 144)
    .desc("Federation level 1 farm id.");

  // special ships building id
  public static final ValueDefinition<Integer> UtopiaPlanitia = new ValueDefinition<Integer>(
    UTPL_ADDRESS, UtopiaPlanitiaName, MovRegInt.class, Register.EAX, 38)
    .desc("Federation 'Utopia Planitia' special ship building id.");

  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    Fed1,
    UtopiaPlanitia
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
