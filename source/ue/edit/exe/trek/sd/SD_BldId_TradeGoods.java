package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the trade goods building id segment definitions.
 */
public interface SD_BldId_TradeGoods {

  public static final String Prefix = "tradeGoodsBld"; //$NON-NLS-1$

  public static final int TRADE1_ADDRESS = 0x000F8D5C;
  public static final int TRADE2_ADDRESS = 0x000F8E11;
  public static final int TRADE3_ADDRESS = 0x000F8EFD;

  // main segments
  public static final ValueDefinition<?>[] All = Stream.of(
    new ValueDefinition<Integer>(TRADE1_ADDRESS, Prefix + "1", MovRegInt.class, Register.EAX, 18),
    new ValueDefinition<Integer>(TRADE2_ADDRESS, Prefix + "2", MovRegInt.class, Register.EAX, 18),
    new ValueDefinition<Integer>(TRADE3_ADDRESS, Prefix + "3", MovRegInt.class, Register.EAX, 18)
  ).map(x -> x.desc("Trade goods building id.")).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
