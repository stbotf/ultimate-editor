package ue.edit.exe.trek.sd;

import java.util.Vector;
import java.util.function.BiConsumer;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.common.InternalSegment;
import ue.exception.InvalidArgumentsException;
import ue.util.func.FunctionTools.CheckedBiConsumer;

public class ValueDefinition<T> extends SegmentDefinition {

  @Getter @Accessors(fluent = true) private final T defaultValue;

  @Getter @Accessors(fluent = true)
  private CheckedBiConsumer<InternalSegment, Object, InvalidArgumentsException> cbValidate = null;

  /**
   * @param address   trek.exe address of the segment
   * @param name      name or prefix of the segment
   * @param type      segment class type
   * @param orig      segment value
   */
  public ValueDefinition(int address, String name, Class<?> type, T orig) {
    super(address, name, type);
    this.defaultValue = orig;
  }

  /**
   * @param address   trek.exe address of the segment
   * @param name      name or prefix of the segment
   * @param type      segment class type
   * @param register  asm register to be validated
   * @param orig      segment value
   */
  public ValueDefinition(int address, String name, Class<?> type, Register register, T orig) {
    super(address, name, type, register);
    this.defaultValue = orig;
  }

  /**
   * @param address   trek.exe address of the segment
   * @param name      name or prefix of the segment
   * @param type      segment class type
   * @param register  asm register to be validated
   * @param flags     asm register flags to be validated
   * @param offset    asm register offset to be validated
   * @param orig      segment value
   */
  public ValueDefinition(int address, String name, Class<?> type, Register register, int flags, int offset, T orig) {
    super(address, name, type, register, flags, offset);
    this.defaultValue = orig;
  }

  @Override
  public ValueDefinition<T> suffix() {
    super.suffix();
    return this;
  }

  @Override
  public ValueDefinition<T> addrSuffix() {
    super.addrSuffix();
    return this;
  }

  @Override
  public ValueDefinition<T> suffixType() {
    super.suffixType();
    return this;
  }

  @Override
  public ValueDefinition<T> suffixTypeAddr() {
    super.suffixTypeAddr();
    return this;
  }

  /**
   * Specifies the optional register.
   */
  @Override
  public ValueDefinition<T> reg(Register register) {
    super.reg(register);
    return this;
  }

  /**
   * Specifies the optional register, flags and offset.
   */
  @Override
  public ValueDefinition<T> reg(Register register, int flags, int offset) {
    super.reg(register, flags, offset);
    return this;
  }

  /**
   * Specifies the optional flags.
   */
  @Override
  public ValueDefinition<T> flags(int flags) {
    super.flags(flags);
    return this;
  }

  /**
   * Specifies the optional offset.
   */
  @Override
  public ValueDefinition<T> offset(int offset) {
    super.offset(offset);
    return this;
  }

  /**
   * Specifies the optional size.
   */
  @Override
  public ValueDefinition<T> size(int size) {
    super.size(size);
    return this;
  }

  /**
   * Specifies the optional description.
   */
  @Override
  public ValueDefinition<T> desc(String desc) {
    super.desc(desc);
    return this;
  }

  public ValueDefinition<T> validator(CheckedBiConsumer<InternalSegment, Object, InvalidArgumentsException> cb) {
    cbValidate = cb;
    return this;
  }

  @Override
  public ValueDefinition<T> integrityCheck(BiConsumer<InternalSegment, Vector<String>> cb) {
    super.integrityCheck(cb);
    return this;
  }

  public void validate(InternalSegment seg, Object value) throws InvalidArgumentsException {
    if (cbValidate != null)
      cbValidate.accept(seg, value);
  }
}
