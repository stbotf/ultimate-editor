package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.seg.prim.ByteValueSegment;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.JmpShort;

/**
 * This interface holds the special ship building segment definition.
 */
public interface SD_SpShips {

  public static final String SpShipsOffsetName  = "specialShipsOffset"; //$NON-NLS-1$
  public static final String SpStrcJmpName      = "specialStructureJump"; //$NON-NLS-1$

  public static final int   SPSHP_STRUCTURE_ADDRESS = 0x0003AEB8;
  public static final int   SPSHP_OFFSET_ADDRESS    = 0x000F4197;

  public static final byte  Default_StrcJmp = JmpShort.JZ; // 0x74
  public static final int   Default_Offset  = 0xFFFFEFD5;

  // main segments
  public static final ValueDefinition<Byte> SpShpStrcJmp = new ValueDefinition<Byte>(
    SPSHP_STRUCTURE_ADDRESS, SpStrcJmpName, ByteValueSegment.class, Default_StrcJmp)
    .desc("If patched, the special ship building is skipped. No special ships can be build.");

  public static final ValueDefinition<Integer> SpShpOffset = new ValueDefinition<Integer>(
    SPSHP_OFFSET_ADDRESS, SpShipsOffsetName, IntValueSegment.class, Default_Offset)
    .desc("Special ships offset.");

  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    SpShpStrcJmp, SpShpOffset
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
