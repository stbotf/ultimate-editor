package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpWordPtrShort;
import ue.edit.exe.seg.prim.MovDSInt;
import ue.edit.exe.seg.prim.MovDwordPtrInt;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the MapSizePatch segment definitions.
 */
public interface SD_MapSize {

  public static final String Prefix = "mapSize"; //$NON-NLS-1$
  public static final String Prefix_Height = Prefix + "_height"; //$NON-NLS-1$
  public static final String Prefix_Width = Prefix + "_width"; //$NON-NLS-1$
  public static final String Prefix_ScanRange = Prefix + "_scanRange"; //$NON-NLS-1$
  public static final String Prefix_SpaceObj = Prefix + "_spaceObj"; //$NON-NLS-1$

  // shared map width value references for the RingMinEmpireDistancePatch
  public static final ValueDefinition<Integer> SmallRingWidth =
    new ValueDefinition<Integer>(0x000ae54e, Prefix_Width, MovDSInt.class, 13)
    .suffix().desc("horizontal small galaxy sectors, defaults to 13 = Dh"
      + "\nshared by 0xae5b1 when small galaxy RingMinEmpireDistanceFix is applied");

  public static final ValueDefinition<Integer> LargeRingWidth =
    new ValueDefinition<Integer>(0x000ae6e0, Prefix_Width, MovDSInt.class, 25)
    .suffix().desc("horizontal large galaxy sectors, defaults to 25 = 19h"
      + "\nshared by 0xae727 when large galaxy RingMinEmpireDistanceFix is applied");

  // width_address
  public static final ValueDefinition<?>[][] Width = new ValueDefinition<?>[][] {
    // small, defaults to 13 = Dh
    Stream.concat(
      Stream.of(
        new ValueDefinition<Integer>(0x0003cce7, Prefix_Width, MovRegInt.class, Register.EAX, 13),
        new ValueDefinition<Integer>(0x0003ceb9, Prefix_Width, MovRegInt.class, Register.EDX, 13),
        new ValueDefinition<Integer>(0x0004e9f9, Prefix_Width, MovRegInt.class, Register.EAX, 13),
        new ValueDefinition<Integer>(0x000ae57b, Prefix_Width, MovDSInt.class, 13),
        new ValueDefinition<Integer>(0x000ae5ea, Prefix_Width, MovDSInt.class, 13),
        new ValueDefinition<Integer>(0x000b15c9, Prefix_Width, MovDwordPtrInt.class, Register.EAX, 13),
        new ValueDefinition<Integer>(0x000b5fe8, Prefix_Width, MovRegInt.class, Register.EAX, 13),
        new ValueDefinition<Integer>(0x000b60a0, Prefix_Width, MovRegInt.class, Register.EAX, 13),
        new ValueDefinition<Integer>(0x000b615b, Prefix_Width, MovRegInt.class, Register.EBX, 13),
        new ValueDefinition<Integer>(0x000b61c8, Prefix_Width, MovRegInt.class, Register.EAX, 13),
        new ValueDefinition<Integer>(0x000b6570, Prefix_Width, MovRegInt.class, Register.EDX, 13)
      ).map(x -> x.suffix().desc("horizontal small galaxy sectors, defaults to 13 = Dh")),
      Stream.of(
        // 0x000ae54e is shared by 0x000ae5b1 when overridden by the small galaxy RingMinEmpireDistanceFix
        // 0x000ae5b1 therefore is not listed here, but instead the patch is applied on change
        SmallRingWidth
      )
    ).toArray(ValueDefinition<?>[]::new),
    // medium, defaults to 16 = 10h
    Stream.of(
      new ValueDefinition<Integer>(0x0003cd03, Prefix_Width, MovRegInt.class, Register.EAX, 16),
      new ValueDefinition<Integer>(0x0003cef4, Prefix_Width, MovRegInt.class, Register.EDX, 16),
      new ValueDefinition<Integer>(0x0004eb23, Prefix_Width, MovRegInt.class, Register.EAX, 16),
      new ValueDefinition<Integer>(0x000ae4ab, Prefix_Width, MovDSInt.class, 16),
      new ValueDefinition<Integer>(0x000b160b, Prefix_Width, MovDwordPtrInt.class, Register.EAX, 16),
      new ValueDefinition<Integer>(0x000b5fd3, Prefix_Width, MovRegInt.class, Register.EAX, 16),
      new ValueDefinition<Integer>(0x000b608e, Prefix_Width, MovRegInt.class, Register.EAX, 16),
      new ValueDefinition<Integer>(0x000b6167, Prefix_Width, MovRegInt.class, Register.EBX, 16),
      new ValueDefinition<Integer>(0x000b629e, Prefix_Width, MovRegInt.class, Register.EAX, 16),
      new ValueDefinition<Integer>(0x000b6450, Prefix_Width, MovRegInt.class, Register.EBP, 16)
    ).map(x -> x.suffix().desc("horizontal medium galaxy sectors, defaults to 16 = 10h")).toArray(ValueDefinition<?>[]::new),
    // large, defaults to 25 = 19h
    Stream.concat(
      Stream.of(
        new ValueDefinition<Integer>(0x0003ccfc, Prefix_Width, MovRegInt.class, Register.EAX, 25),
        new ValueDefinition<Integer>(0x0003ceed, Prefix_Width, MovRegInt.class, Register.EDX, 25),
        // MovIntToMemOffset 0x000ae75d is shared and overridden by sd_largeSpiralAxisFix below
        new ValueDefinition<Integer>(0x000b163d, Prefix_Width, MovDwordPtrInt.class, Register.EAX, 25),
        new ValueDefinition<Integer>(0x000b5fef, Prefix_Width, MovRegInt.class, Register.EAX, 25),
        new ValueDefinition<Integer>(0x000b60a7, Prefix_Width, MovRegInt.class, Register.EAX, 25),
        new ValueDefinition<Integer>(0x000b6117, Prefix_Width, MovRegInt.class, Register.EBX, 25),
        new ValueDefinition<Integer>(0x000b62a8, Prefix_Width, MovRegInt.class, Register.EAX, 25),
        new ValueDefinition<Integer>(0x000b6590, Prefix_Width, MovRegInt.class, Register.ESI, 25),
        new ValueDefinition<Integer>(0x0004eb2d, Prefix_Width, MovRegInt.class, Register.EAX, 25)
      ).map(x -> x.suffix().desc("horizontal large galaxy sectors, defaults to 25 = 19h")),
      Stream.of(
        // 0x000ae6e0 is shared by 0x000ae727 when overridden by the large galaxy RingMinEmpireDistanceFix
        // 0x000ae727 therefore is not listed here, but instead the patch is applied on change
        LargeRingWidth
      )
    ).toArray(ValueDefinition<?>[]::new)
  };

  // height_address
  public static final ValueDefinition<?>[][] Height = new ValueDefinition<?>[][] {
    // small, defaults to 10 = Ah
    Stream.of(
      new ValueDefinition<Integer>(0x0003ccb7, Prefix_Height, MovRegInt.class, Register.EAX, 10),
      new ValueDefinition<Integer>(0x000b15cf, Prefix_Height, MovDwordPtrInt.class, Register.EAX, 0, 4, 10),
      new ValueDefinition<Integer>(0x000b6160, Prefix_Height, MovRegInt.class, Register.EAX, 10),
      new ValueDefinition<Integer>(0x000b61e0, Prefix_Height, MovRegInt.class, Register.ESI, 10),
      new ValueDefinition<Integer>(0x000b6575, Prefix_Height, MovRegInt.class, Register.ECX, 10),
      new ValueDefinition<Integer>(0x0004e9e2, Prefix_Height, MovRegInt.class, Register.EAX, 10)
    ).map(x -> x.suffix().desc("vertical small galaxy sectors, defaults to 10 = Ah")).toArray(ValueDefinition<?>[]::new),
    // medium, defaults to 12 = Ch
    Stream.of(
      new ValueDefinition<Integer>(0x0003ccd3, Prefix_Height, MovRegInt.class, Register.EAX, 12),
      new ValueDefinition<Integer>(0x000b1611, Prefix_Height, MovDwordPtrInt.class, Register.EAX, 0, 4, 12),
      new ValueDefinition<Integer>(0x000b616c, Prefix_Height, MovRegInt.class, Register.EAX, 12),
      new ValueDefinition<Integer>(0x000b62b4, Prefix_Height, MovRegInt.class, Register.ESI, 12),
      new ValueDefinition<Integer>(0x000b6455, Prefix_Height, MovRegInt.class, Register.EAX, 12),
      new ValueDefinition<Integer>(0x0004eb0d, Prefix_Height, MovRegInt.class, Register.EAX, 12)
    ).map(x -> x.suffix().desc("vertical medium galaxy sectors, defaults to 12 = Ch")).toArray(ValueDefinition<?>[]::new),
    // large, defaults to 18 = 12h
    Stream.of(
      new ValueDefinition<Integer>(0x0003cccc, Prefix_Height, MovRegInt.class, Register.EAX, 18),
      new ValueDefinition<Integer>(0x000b1643, Prefix_Height, MovDwordPtrInt.class, Register.EAX, 0, 4, 18),
      new ValueDefinition<Integer>(0x000b611c, Prefix_Height, MovRegInt.class, Register.EAX, 18),
      new ValueDefinition<Integer>(0x000b62be, Prefix_Height, MovRegInt.class, Register.ESI, 18),
      new ValueDefinition<Integer>(0x000b6595, Prefix_Height, MovRegInt.class, Register.EDI, 18),
      new ValueDefinition<Integer>(0x0004eb17, Prefix_Height, MovRegInt.class, Register.EAX, 18)
    ).map(x -> x.suffix().desc("vertical large galaxy sectors, defaults to 18 = 12h")).toArray(ValueDefinition<?>[]::new)
  };

  // width5_address: small, medium, large
  public static final ValueDefinition<?>[] Width5 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(0x000ae529, Prefix_Width, MovRegInt.class, Register.EDX, 13 * 5)
      .suffix().desc("horizontal small galaxy sectors * 5"),
    new ValueDefinition<Integer>(0x000ae482, Prefix_Width, MovRegInt.class, Register.EDI, 16 * 5)
      .suffix().desc("horizontal medium galaxy sectors * 5"),
    new ValueDefinition<Integer>(0x000ae6b7, Prefix_Width, MovRegInt.class, Register.EDX, 25 * 5)
      .suffix().desc("horizontal large galaxy sectors * 5")
  };

  // height5_address: small, medium, large
  public static final ValueDefinition<?>[] Height5 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(0x000ae524, Prefix_Height, MovRegInt.class, Register.EBP, 10 * 5)
      .suffix().desc("vertical small galaxy sectors * 5"),
    new ValueDefinition<Integer>(0x000ae47d, Prefix_Height, MovRegInt.class, Register.EBP, 12 * 5)
      .suffix().desc("vertical medium galaxy sectors * 5"),
    new ValueDefinition<Integer>(0x000ae6b2, Prefix_Height, MovRegInt.class, Register.EBP, 18 * 5)
      .suffix().desc("vertical large galaxy sectors * 5")
  };

  public static final ValueDefinition<?>[] SpaceObjLimit = Stream.of(
    new ValueDefinition<Short>(0x000afa44, Prefix_SpaceObj, CmpWordPtrShort.class, Register.ESP, 0, 0x58, (short)230),
    new ValueDefinition<Short>(0x000afa8f, Prefix_SpaceObj, CmpWordPtrShort.class, Register.ESP, 0, 0x58, (short)230)
  ).map(x -> x.suffix().desc("maximum total number of space objects, defaults to 230 (one per sector)")).toArray(ValueDefinition<?>[]::new);

  public static final ValueDefinition<Integer> AIScanRange =
    new ValueDefinition<Integer>(0x0001c44f, Prefix_ScanRange, MovRegInt.class, Register.ECX, 25).suffix();

  public static final SegmentDefinition[] All = Stream.of(
    Arrays.stream(Width).flatMap(Arrays::stream),
    Arrays.stream(Height).flatMap(Arrays::stream),
    Arrays.stream(Width5),
    Arrays.stream(Height5),
    Arrays.stream(SpaceObjLimit),
    Stream.of(AIScanRange, SD_GalShape.LargeSpiralAxis)
  ).flatMap(x -> x).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
