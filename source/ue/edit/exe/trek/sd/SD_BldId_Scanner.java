package ue.edit.exe.trek.sd;

import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the scanner building id segment definitions.
 */
public interface SD_BldId_Scanner {

  public static final String BasicName = "basicScannerID"; //$NON-NLS-1$

  public static final int BASIC_ADDRESS = 0x000441BF;
  public static final int Default_BasicScannerId = 12;

  // BasicScannerID
  ValueDefinition<Integer> Basic = new ValueDefinition<Integer> (
    // 00444DBF: mov   eax, 0Ch
    BASIC_ADDRESS, BasicName, MovRegInt.class, Register.EAX, Default_BasicScannerId
  );
}
