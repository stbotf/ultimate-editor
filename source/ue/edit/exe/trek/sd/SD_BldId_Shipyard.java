package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpWordPtrByte;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.seg.bld.ShipyardBldIDs;

/**
 * This interface holds the ship yard building id segment definitions.
 */
public interface SD_BldId_Shipyard {

  public static final String PrefixBasic    = "basicShipyardID"; //$NON-NLS-1$
  public static final String PrefixRegular  = "shipyardIds"; //$NON-NLS-1$

  public static final int BS1_ADDRESS = 0x000D2CC3;
  public static final int BS2_ADDRESS = 0x000D0DAF;
  public static final int BS3_ADDRESS = 0x000D65ED;
  public static final int BS4_ADDRESS = 0x00050C65;
  public static final int BS5_ADDRESS = 0x00050C55;
  public static final int BS6_ADDRESS = 0x00050C71;
  public static final int REG_ADDRESS = 0x00039A96;

  // card, fed, ferg, klng, rom, minors
  public static final int[] Default_RegularIDs = new int[] { 21, 54, 52, 55, 87, 15 };

  // basic shipyard
  public static final ValueDefinition<?>[] Basic1 = Stream.of(
    // cmp     word ptr [eax+50h], X
    new ValueDefinition<Byte>(BS1_ADDRESS, PrefixBasic + "1", CmpWordPtrByte.class, Register.EAX, 0, 0x50, (byte)15),
    new ValueDefinition<Byte>(BS2_ADDRESS, PrefixBasic + "2", CmpWordPtrByte.class, Register.EAX, 0, 0x50, (byte)15),
    new ValueDefinition<Byte>(BS3_ADDRESS, PrefixBasic + "3", CmpWordPtrByte.class, Register.EDX, 0, 0x50, (byte)15)
    ).map(x -> x.desc("Basic shipyard building id. <byte>")).toArray(ValueDefinition<?>[]::new);

  public static final ValueDefinition<?>[] Basic2 = Stream.of(
    new ValueDefinition<Integer>(BS4_ADDRESS, PrefixBasic + "4", MovRegInt.class, Register.EDX, 15),
    new ValueDefinition<Integer>(BS5_ADDRESS, PrefixBasic + "5", MovRegInt.class, Register.EDX, 15),
    new ValueDefinition<Integer>(BS6_ADDRESS, PrefixBasic + "6", MovRegInt.class, Register.EDX, 15)
    ).map(x -> x.desc("Basic shipyard building id. <int>")).toArray(ValueDefinition<?>[]::new);

  // empire shipyards
  public static final SegmentDefinition Regular = new SegmentDefinition(
    REG_ADDRESS, PrefixRegular, ShipyardBldIDs.class)
    .desc("Regular shipyard building ids of each major race plus the minors.");

  // sub segments
  public static final ValueDefinition<?>[] Sub = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(REG_ADDRESS + 2,  PrefixRegular + "_card", MovRegInt.class, Register.EAX, Default_RegularIDs[0])
      .desc("Cardassian shipyard building id"),
    new ValueDefinition<Integer>(REG_ADDRESS + 16, PrefixRegular + "_fed",  MovRegInt.class, Register.EAX, Default_RegularIDs[1])
      .desc("Federation shipyard building id"),
    new ValueDefinition<Integer>(REG_ADDRESS + 30, PrefixRegular + "_ferg", MovRegInt.class, Register.EAX, Default_RegularIDs[2])
      .desc("Ferengi shipyard building id"),
    new ValueDefinition<Integer>(REG_ADDRESS + 44, PrefixRegular + "_klng", MovRegInt.class, Register.EAX, Default_RegularIDs[3])
      .desc("Klingon shipyard building id"),
    new ValueDefinition<Integer>(REG_ADDRESS + 58, PrefixRegular + "_rom",  MovRegInt.class, Register.EAX, Default_RegularIDs[4])
      .desc("Romulan shipyard building id"),
    new ValueDefinition<Integer>(REG_ADDRESS + 58, PrefixRegular + "_min",  MovRegInt.class, Register.EAX, Default_RegularIDs[5])
      .desc("Minor race shipyard building id")
  };

  public static final SegmentDefinition[] All = Stream.of(
    Arrays.stream(Basic1),
    Arrays.stream(Basic2),
    Stream.of(Regular)
  ).flatMap(x -> x).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
