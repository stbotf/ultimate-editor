package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the basic ground defense building id segment definitions.
 */
public interface SD_BldId_GrndDef {

  public static final String Prefix = "basicGroundDefenseId"; //$NON-NLS-1$

  public static final int GRND1_ADDRESS = 0x000517A0;
  public static final int GRND2_ADDRESS = 0x00051790;
  public static final int GRND3_ADDRESS = 0x000517AC;

  // main segments
  public static final ValueDefinition<?>[] All = Stream.of(
    new ValueDefinition<Integer>(GRND1_ADDRESS, Prefix + "1", MovRegInt.class, Register.EDX, 2),
    new ValueDefinition<Integer>(GRND2_ADDRESS, Prefix + "2", MovRegInt.class, Register.EDX, 2),
    new ValueDefinition<Integer>(GRND3_ADDRESS, Prefix + "3", MovRegInt.class, Register.EDX, 2)
  ).map(x -> x.desc("Basic ground defense building id.")).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
