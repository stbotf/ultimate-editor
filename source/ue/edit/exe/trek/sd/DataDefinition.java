package ue.edit.exe.trek.sd;

import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.exe.seg.common.DataSegment;

public class DataDefinition extends SegmentDefinition {

  @Getter @Accessors(fluent = true)
  protected final byte[] data;

  /**
   * @param address       trek.exe address of the segment
   * @param name          name or prefix of the segment
   * @param type          the segment class type
   * @param data          segment data
   */
  public DataDefinition(int address, String name, Class<?> type, byte[] data) {
    super(address, name, type, data.length);
    this.data = data;
  }

  /**
   * @param address       trek.exe address of the segment
   * @param name          name or prefix of the segment
   * @param data          segment data
   */
  public DataDefinition(int address, String name, byte[] data) {
    this(address, name, DataSegment.class, data);
  }

}
