package ue.edit.exe.trek.sd;

import ue.edit.exe.trek.seg.CDprotection;

/**
 * This interface holds general application related segment definitions.
 */
public interface SD_App {

  public static final String CDProtectName = "cdProtection"; //$NON-NLS-1$

  public static final SegmentDefinition CDProtection =
    new SegmentDefinition(CDprotection.SEGMENT_ADDRESS, CDProtectName, CDprotection.class)
      .size(CDprotection.SIZE);
}
