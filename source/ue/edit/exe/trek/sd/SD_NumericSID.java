package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.AddRegByte;
import ue.edit.exe.seg.prim.PushInt;

/**
 * This interface holds the NumericSectorIdPatch segment definitions.
 */
public interface SD_NumericSID {

  public static final String Prefix = "numericSectorId"; //$NON-NLS-1$

  // the anomaly name literal, which suffixes the sector position
  // note, the BOP mod unnecessarily replaces literal1 by file address 0x17d6c1 / asm offset 0x57F8C1,
  // it reads same like numeric_literal1 "%s %d.%d" and likely was used experimentally to further lengthen the string
  // the bop change therefore is ignored but sneakingly overridden
  public static final byte[] orig_literal1      = new byte[]{0x00, 0x25, 0x73, 0x20, 0x25, 0x63, 0x25, 0x64, 0x00}; // "%s %c%d"
  public static final byte[] numeric_literal1   = new byte[]{0x25, 0x73, 0x20, 0x25, 0x64, 0x2E, 0x25, 0x64, 0x00}; // "%s %d.%d"
  // the galaxy map header line and event text sector position
  public static final byte[] orig_literal2      = new byte[]{0x25, 0x63, 0x25, 0x64, 0x00, 0x00, 0x00, 0x00}; // "%c%d"
  public static final byte[] numeric_literal2   = new byte[]{0x25, 0x64, 0x2E, 0x25, 0x64, 0x00, 0x00, 0x00}; // "%d.%d"
  public static final byte[] brackets_literal2  = new byte[]{0x28, 0x25, 0x64, 0x2C, 0x25, 0x64, 0x29, 0x00}; // "(%d,%d)", used by BOP

  //  83  C3 [01]             => adds only 1 not 41h to handle character replacement by '%d' below
  //  68 [57] F9  57  00      => adjusts the shifted Literal_aSCD offset
  // [25] 73  20  25 [64][2E] => replaces '%c' by '%d' and left shifts the string to insert a 0x2E dot
  //  83  C0 [01]             => adds only 1 not 61h to handle character replacement by '%d' below
  //  25 [64][2E] 25  64      => replaces '%c' by '%d' and inserts a 0x2E dot
  public static final int orig_literal1_offset  = 0x0057f958; // Literal_aSCD
  public static final int patch_literal1_offset = 0x0057f957; // features align 4 spacing preceding Literal_aSCD
  public static final int bop_literal1_offset = 0x0057F8C1; // bop instead unnecessarily relocated the numeric_literal1

  public static final ValueDefinition<Byte> Literal1_Conv =
    new ValueDefinition<Byte>(0x000b58da, Prefix, AddRegByte.class, Register.EBX, (byte)0x41)
      .suffix().desc("by default 41h");

  public static final ValueDefinition<Integer> Literal1_Offset =
    new ValueDefinition<Integer>(0x000b58e6, Prefix, PushInt.class, orig_literal1_offset)
      .suffix().desc("push offset Literal_aSCD");

  public static final PatchDefinition Literal1 =
    new PatchDefinition(0x0017d757, Prefix, orig_literal1, numeric_literal1)
      .suffix().desc("align 4, Literal_aSCD");

  public static final ValueDefinition<Byte> Literal2_Conv =
    new ValueDefinition<Byte>(0x000b5f0b, Prefix, AddRegByte.class, Register.EAX, (byte)0x61)
      .suffix().desc("by default 61h");

  public static final PatchDefinition Literal2 =
    new PatchDefinition(0x0017d8fc, Prefix, orig_literal2, numeric_literal2, brackets_literal2)
      .suffix().desc("align 4, literal_aCD_1");

  // numericSectorID_address
  // to make all sector ID show as numeric (e.g. "Sector 1.1")
  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    Literal1_Conv,
    Literal1_Offset,
    Literal1,
    Literal2_Conv,
    Literal2
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
