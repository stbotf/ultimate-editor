package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovDwordPtrInt;

/**
 * This interface holds the SystemAlignmentPatch segment definitions.
 */
public interface SD_SysAlign {

  public static final String Prefix = "sysAlign"; //$NON-NLS-1$

  // systemAlignment_address - galaxy scale factors
  public static final ValueDefinition<?>[][] Scale = new ValueDefinition<?>[][] {
    // small, defaults to 7.6f = 40F33333h
    Stream.of(
      new ValueDefinition<Float>(0xd9c38, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x34, 7.6f), //  8 bytes (byte offset)
      new ValueDefinition<Float>(0xda973, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x8C, 7.6f), // 11 bytes (dword offset)
      new ValueDefinition<Float>(0xdb181, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x08, 7.6f), //  8 bytes (byte offset)
      new ValueDefinition<Float>(0xdb52c, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x58, 7.6f)  //  8 bytes (byte offset)
    ).map(x -> x.suffix().desc("small galaxy, defaults to 7.6f = 40F33333h")).toArray(ValueDefinition<?>[]::new),
    // medium, defaults to 6.2f = 40C66666h
    Stream.of(
      new ValueDefinition<Float>(0xd9c45, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x34, 6.2f),
      new ValueDefinition<Float>(0xda983, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x8C, 6.2f),
      new ValueDefinition<Float>(0xdb18b, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x08, 6.2f),
      new ValueDefinition<Float>(0xdb539, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x58, 6.2f)
    ).map(x -> x.suffix().desc("medium galaxy, defaults to 6.2f = 40C66666h")).toArray(ValueDefinition<?>[]::new),
    // large, defaults to 4.0f = 40800000h
    Stream.of(
      new ValueDefinition<Float>(0xd9c52, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x34, 4.0f),
      new ValueDefinition<Float>(0xda993, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x8C, 4.0f),
      new ValueDefinition<Float>(0xdb198, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x08, 4.0f),
      new ValueDefinition<Float>(0xdb546, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x58, 4.0f)
      ).map(x -> x.suffix().desc("large galaxy, defaults to 4.0f = 40800000h")).toArray(ValueDefinition<?>[]::new),
    // zoom-in, defaults to 12.0f = 41400000h
    Stream.of(
      new ValueDefinition<Float>(0xd9ad0, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x34, 12.0f),
      new ValueDefinition<Float>(0xda7f8, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x8C, 12.0f),
      new ValueDefinition<Float>(0xdb10c, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x08, 12.0f),
      new ValueDefinition<Float>(0xdb3c2, Prefix, MovDwordPtrInt.class, Register.ESP, 0, 0x58, 12.0f)
      ).map(x -> x.suffix().desc("zoom-in mode, defaults to 12.0f = 41400000h")).toArray(ValueDefinition<?>[]::new)
  };

  public static final SegmentDefinition[] All =
    Arrays.stream(Scale).flatMap(Arrays::stream).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
