package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the SectorSizePatch segment definitions.
 */
public interface SD_SectorSize {

  public static final String Prefix = "sectorSize"; //$NON-NLS-1$

  static final String SmallDesc = "small galaxy, defaults to 53 = 35h";
  static final String MedDesc   = "medium galaxy, defaults to 44 = 2Ch";
  static final String LargeDesc = "large galaxy, defaults to 28 = 1Ch";
  static final String ZoomDesc  = "zoom-in mode, defaults to 60 = 3Ch";

  // sectorSize_address
  public static final ValueDefinition<?>[][] Pixels = new ValueDefinition<?>[][] {
    // small, defaults to 53 = 35h
    Stream.of(
      new ValueDefinition<Integer>(0x000db8a7, Prefix, MovRegInt.class, Register.EDI, 53),
      new ValueDefinition<Integer>(0x000da42c, Prefix, MovRegInt.class, Register.EDI, 53),
      new ValueDefinition<Integer>(0x000daf65, Prefix, MovRegInt.class, Register.EAX, 53),
      new ValueDefinition<Integer>(0x000d9c5f, Prefix, MovRegInt.class, Register.EAX, 53),
      new ValueDefinition<Integer>(0x000da682, Prefix, MovRegInt.class, Register.EAX, 53),
      new ValueDefinition<Integer>(0x000da93f, Prefix, MovRegInt.class, Register.EAX, 53),
      new ValueDefinition<Integer>(0x000dafd4, Prefix, MovRegInt.class, Register.EAX, 53),
      new ValueDefinition<Integer>(0x000db08c, Prefix, MovRegInt.class, Register.EAX, 53),
      new ValueDefinition<Integer>(0x000dbcb4, Prefix, MovRegInt.class, Register.EAX, 53)
    ).map(x -> x.suffix().desc(SmallDesc)).toArray(ValueDefinition<?>[]::new),
    // medium, defaults to 44 = 2Ch
    Stream.of(
      new ValueDefinition<Integer>(0x000db8b1, Prefix, MovRegInt.class, Register.EDI, 44),
      new ValueDefinition<Integer>(0x000da436, Prefix, MovRegInt.class, Register.EDI, 44),
      new ValueDefinition<Integer>(0x000daf6b, Prefix, MovRegInt.class, Register.EAX, 44),
      new ValueDefinition<Integer>(0x000d9c69, Prefix, MovRegInt.class, Register.EAX, 44),
      new ValueDefinition<Integer>(0x000da68c, Prefix, MovRegInt.class, Register.EAX, 44),
      new ValueDefinition<Integer>(0x000da949, Prefix, MovRegInt.class, Register.EAX, 44),
      new ValueDefinition<Integer>(0x000dafdb, Prefix, MovRegInt.class, Register.EAX, 44),
      new ValueDefinition<Integer>(0x000db093, Prefix, MovRegInt.class, Register.EAX, 44),
      new ValueDefinition<Integer>(0x000dbcbe, Prefix, MovRegInt.class, Register.EAX, 44)
    ).map(x -> x.suffix().desc(MedDesc)).toArray(ValueDefinition<?>[]::new),
    // large, defaults to 28 = 1Ch
    Stream.of(
      new ValueDefinition<Integer>(0x000db8bb, Prefix, MovRegInt.class, Register.EDI, 28),
      new ValueDefinition<Integer>(0x000da440, Prefix, MovRegInt.class, Register.EDI, 28),
      new ValueDefinition<Integer>(0x000daf71, Prefix, MovRegInt.class, Register.EAX, 28),
      new ValueDefinition<Integer>(0x000d9c73, Prefix, MovRegInt.class, Register.EAX, 28),
      new ValueDefinition<Integer>(0x000da696, Prefix, MovRegInt.class, Register.EAX, 28),
      new ValueDefinition<Integer>(0x000da953, Prefix, MovRegInt.class, Register.EAX, 28),
      new ValueDefinition<Integer>(0x000dafe2, Prefix, MovRegInt.class, Register.EAX, 28),
      new ValueDefinition<Integer>(0x000db09a, Prefix, MovRegInt.class, Register.EAX, 28),
      new ValueDefinition<Integer>(0x000dbcc8, Prefix, MovRegInt.class, Register.EAX, 28)
    ).map(x -> x.suffix().desc(LargeDesc)).toArray(ValueDefinition<?>[]::new),
    // zoom-in, defaults to 60 = 3Ch
    Stream.of(
      new ValueDefinition<Integer>(0x000db69a, Prefix, MovRegInt.class, Register.EDI, 60),
      new ValueDefinition<Integer>(0x000da2a4, Prefix, MovRegInt.class, Register.EDI, 60),
      new ValueDefinition<Integer>(0x000daf5f, Prefix, MovRegInt.class, Register.EAX, 60),
      new ValueDefinition<Integer>(0x000d9b70, Prefix, MovRegInt.class, Register.EAX, 60),
      new ValueDefinition<Integer>(0x000da4bf, Prefix, MovRegInt.class, Register.EAX, 60),
      new ValueDefinition<Integer>(0x000da759, Prefix, MovRegInt.class, Register.EAX, 60),
      new ValueDefinition<Integer>(0x000dafa8, Prefix, MovRegInt.class, Register.EAX, 60),
      new ValueDefinition<Integer>(0x000db019, Prefix, MovRegInt.class, Register.EAX, 60),
      new ValueDefinition<Integer>(0x000dba0b, Prefix, MovRegInt.class, Register.EAX, 60)
    ).map(x -> x.suffix().desc(ZoomDesc)).toArray(ValueDefinition<?>[]::new),
  };

  public static final SegmentDefinition[] All = Arrays.stream(Pixels)
    .flatMap(Arrays::stream).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
