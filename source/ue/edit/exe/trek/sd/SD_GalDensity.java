package ue.edit.exe.trek.sd;

import java.util.Arrays;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.LongValueSegment;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds galactic density related segment definitions.
 */
public interface SD_GalDensity {

  public static final String Prefix = "galDensity"; //$NON-NLS-1$

  // IrrGalDensity defaults to 0.6f = 3F19999Ah
  public static final float Default_IrrGalDensity = 0.6f;

  // StarAnomalyRatio defaults to 1/12 = 0.833.. = rounded up to 3FEAAAAAAAAAAAABh
  public static final double Default_StarAnomalyRatio = 0.8333333333333334;

  // for general galactic density, also refer 0x17D568
  public static final ValueDefinition<Float> Irregular =
    new ValueDefinition<Float>(0xae53e, Prefix, MovRegInt.class, Register.EDX, Default_IrrGalDensity).suffix();

  public static final ValueDefinition<Double> StarAnomalyRatio =
    new ValueDefinition<Double>(0x17d538, Prefix, LongValueSegment.class, Default_StarAnomalyRatio).suffix();

  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    Irregular, StarAnomalyRatio
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
