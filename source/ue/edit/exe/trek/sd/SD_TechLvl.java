package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.CmpRegByte;
import ue.edit.exe.seg.prim.LeaMemOffset;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.SubRegByte;
import ue.edit.exe.trek.seg.tec.BldTechReq;
import ue.edit.exe.trek.seg.tec.BldTechReq2;
import ue.edit.exe.trek.seg.tec.TechCostFixture;
import ue.edit.exe.trek.seg.tec.TechMapSize;
import ue.edit.exe.trek.seg.tec.TechTotal;

/**
 * This interface holds the TechLevelsPatch segment definitions.
 */
public interface SD_TechLvl {

  public static final String Prefix = "techLevels"; //$NON-NLS-1$
  public static final String Prefix_Num = Prefix + "_num"; //$NON-NLS-1$
  public static final String Prefix_Max = Prefix + "_max"; //$NON-NLS-1$
  public static final String Prefix_SndLast = Prefix + "_secondLast"; //$NON-NLS-1$

  // num tech levels = future tech 11 = 0Bh
  public static final ValueDefinition<?>[] Num = Stream.of(
    new ValueDefinition<Byte>(0x00023816, Prefix_Num, CmpRegByte.class, Register.DX,  (byte)11),
    new ValueDefinition<Byte>(0x000239c5, Prefix_Num, CmpRegByte.class, Register.BL,  (byte)11),
    new ValueDefinition<Byte>(0x0004f51d, Prefix_Num, CmpRegByte.class, Register.CH,  (byte)11), // Edifice patcher
    new ValueDefinition<Byte>(0x0004f820, Prefix_Num, CmpRegByte.class, Register.DL,  (byte)11), // Edifice patcher
    new ValueDefinition<Byte>(0x00051bf0, Prefix_Num, CmpRegByte.class, Register.BX,  (byte)11),
    new ValueDefinition<Byte>(0x00051f9f, Prefix_Num, CmpRegByte.class, Register.EBP, (byte)11),
    new ValueDefinition<Byte>(0x0005202a, Prefix_Num, CmpRegByte.class, Register.SI,  (byte)11),
    new ValueDefinition<Byte>(0x000520f8, Prefix_Num, CmpRegByte.class, Register.BX,  (byte)11),
    new ValueDefinition<Byte>(0x0005268a, Prefix_Num, CmpRegByte.class, Register.EAX, (byte)11),
    new ValueDefinition<Byte>(0x000529e7, Prefix_Num, CmpRegByte.class, Register.EDX, (byte)11),
    new ValueDefinition<Byte>(0x00052bce, Prefix_Num, CmpRegByte.class, Register.BL,  (byte)11),
    new ValueDefinition<Byte>(0x00052dbf, Prefix_Num, CmpRegByte.class, Register.BL,  (byte)11),
    new ValueDefinition<Byte>(0x00054608, Prefix_Num, CmpRegByte.class, Register.BX,  (byte)11),
    new ValueDefinition<Byte>(0x000d0d1c, Prefix_Num, CmpRegByte.class, Register.BH,  (byte)11), // Edifice patcher
    new ValueDefinition<Byte>(0x000d2c6e, Prefix_Num, CmpRegByte.class, Register.BH,  (byte)11), // Edifice patcher
    new ValueDefinition<Byte>(0x000d5778, Prefix_Num, CmpRegByte.class, Register.BL,  (byte)11)
  ).map(x -> x.suffix().desc("number of technology levels")).toArray(ValueDefinition<?>[]::new);

  // max tech level 10 = 0Ah
  public static final SegmentDefinition[] Max = Stream.of(
    new ValueDefinition<Integer>(0x000521a7, Prefix_Max, MovRegInt.class, Register.EDX, 10),
    new ValueDefinition<Integer>(0x000526e7, Prefix_Max, MovRegInt.class, Register.EDX, 10),
    new ValueDefinition<Integer>(0x000d1e46, Prefix_Max, MovRegInt.class, Register.EDX, 10)
  ).map(x -> x.suffix().desc("maximal regular technology level")).toArray(ValueDefinition<?>[]::new);

  // max tech level 10 = 0Ah, stored as single byte value
  public static final SegmentDefinition[] MaxByte = Stream.of(
    new ValueDefinition<Byte>(0x00052267, Prefix_Max, SubRegByte.class, Register.ECX, (byte)10),
    new ValueDefinition<Byte>(0x000522b7, Prefix_Max, SubRegByte.class, Register.ECX, (byte)10),
    new ValueDefinition<Byte>(0x00052427, Prefix_Max, CmpRegByte.class, Register.DX, (byte)10),
    new ValueDefinition<Byte>(0x000d1e17, Prefix_Max, CmpRegByte.class, Register.DX, (byte)10)
  ).map(x -> x.suffix().desc("maximal regular technology level (byte)")).toArray(ValueDefinition<?>[]::new);

  // negative max tech level, stored as single byte value
  public static final SegmentDefinition[] MaxNeg = Stream.of(
    // Correction for Instant-Future-Tech: https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=22005#p22005
    new ValueDefinition<Byte>(0x000521b3, Prefix_Max, LeaMemOffset.class, Register.EAX, LeaMemOffset.FROM_EBP, 0, (byte)-10),
    new ValueDefinition<Byte>(0x000526f7, Prefix_Max, LeaMemOffset.class, Register.EAX, LeaMemOffset.FROM_ECX, 0, (byte)-10)
  ).map(x -> x.suffix().desc("maximal regular technology level (negative)")).toArray(ValueDefinition<?>[]::new);

  // before last tech level 9 = 09h
  public static final SegmentDefinition[] SndLast = Stream.of(
    new ValueDefinition<Integer>(0x0005242d, Prefix_SndLast, MovRegInt.class, Register.EDX, 9)
  ).map(x -> x.suffix().desc("second to last regular technology level")).toArray(ValueDefinition<?>[]::new);

  // before last tech level 9 = 09h, stored as single byte value
  public static final SegmentDefinition[] SndLastByte = Stream.of(
    new ValueDefinition<Byte>(0x000521b6, Prefix_SndLast, SubRegByte.class, Register.EBP, (byte)9),
    new ValueDefinition<Byte>(0x000526fa, Prefix_SndLast, SubRegByte.class, Register.ECX, (byte)9),
    new ValueDefinition<Byte>(0x000d1e27, Prefix_SndLast, SubRegByte.class, Register.EAX, (byte)9)
    ).map(x -> x.suffix().desc("second to last regular technology level (byte)")).toArray(ValueDefinition<?>[]::new);

  public static final SegmentDefinition CostFix = /*0x000534ac*/ TechCostFixture.SEGMENT_DEFINITION;
  public static final SegmentDefinition Total   = /*0x000530a0*/ TechTotal.SEGMENT_DEFINITION;
  public static final SegmentDefinition MapSize = /*0x0004fa59*/ TechMapSize.SEGMENT_DEFINITION;
  public static final SegmentDefinition BldReq  = /*0x0004f52b*/ BldTechReq.SEGMENT_DEFINITION;
  public static final SegmentDefinition BldReq2 = /*0x0004f541*/ BldTechReq2.SEGMENT_DEFINITION;

  public static final SegmentDefinition[] SpecialTechLevelSegments = new SegmentDefinition[] {
    CostFix, Total, MapSize, BldReq, BldReq2
  };

  public static final SegmentDefinition[] All = Stream.of(
    Num,
    Max,
    MaxByte,
    MaxNeg,
    SndLast,
    SndLastByte,
    SpecialTechLevelSegments
  ).flatMap(Arrays::stream).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
