package ue.edit.exe.trek.sd;

import java.util.Arrays;

/**
 * This interface holds auto upgrade segment definitions for ConstructionGUI.
 */
public interface SD_AutoUpgrade {

  static final String Prefix = "autoUpgrade"; //$NON-NLS-1$

  static final byte[] auto_disabled = new byte[]{
    (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90, (byte) 0x90
  };
  static final byte[] ship_auto_enabled = new byte[]{
    (byte) 0xE8, (byte) 0xB2, (byte) 0x7C, (byte) 0xFF, (byte) 0xFF
  };
  static final byte[] base_auto_enabled = new byte[]{
    (byte) 0xE8, (byte) 0xB3, (byte) 0x7B, (byte) 0xFF, (byte) 0xFF
  };

  public static final PatchDefinition Ships =
    new PatchDefinition(0x51AA9, Prefix + "_ships", ship_auto_enabled, auto_disabled)
      .desc("This patch disables the ship auto upgrade.")
      .size(5);

  public static final PatchDefinition Base =
    new PatchDefinition(0x51AD8, Prefix + "_base", base_auto_enabled, auto_disabled)
      .desc("This patch disables the outpost and star base auto upgrade.")
      .size(5);

  public static final SegmentDefinition[] All = new SegmentDefinition[] {
    Ships,
    Base
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

}
