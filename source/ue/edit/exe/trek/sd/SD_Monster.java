package ue.edit.exe.trek.sd;

import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;
import java.util.stream.Stream;
import ue.UE;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.seg.prim.MovWordPtrShort;
import ue.edit.exe.seg.prim.ShortValueSegment;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.ID;

/**
 * This interface holds the alien monster ship model index segment definitions.
 */
public interface SD_Monster {

  public static final String Prefix = "alien"; //$NON-NLS-1$

  public static final int ALIEN1_ADDRESS = 0x0004EEDA; // Borg Cube ID:      mov   edi, 73h (115)
  public static final int ALIEN2_ADDRESS = 0x0004F0DA; // Calamarain ID:     mov   edx, 74h (116)
  public static final int ALIEN3_ADDRESS = 0x0004F059; // Chodak ID:         mov   ecx, 75h (117)
  public static final int ALIEN4_ADDRESS = 0x0004F01A; // Crystal Entity ID: mov   esi, 76h (118)
  public static final int ALIEN5_ADDRESS = 0x0004F1F6; // Combat Drone ID:   mov   ecx, 77h (119)
  public static final int ALIEN6_ADDRESS = 0x0004F1B1; // Edo Guardian ID:   mov   esi, 78h (120)
  public static final int ALIEN7_ADDRESS = 0x0004F119; // Gomtuu ID:         mov   word ptr [esp+172h], 79h (121)
  public static final int ALIEN8_ADDRESS = 0x0004F231; // Husnock ID:        mov   edx, 7Ah (122)
  public static final int ALIEN9_ADDRESS = 0x0004F172; // Tarellian ID:      mov   edi, 7Ch (124)

  // default ship model indices
  public static final short Default_Alien1_ShpIdx = 115;
  public static final short Default_Alien2_ShpIdx = 116;
  public static final short Default_Alien3_ShpIdx = 117;
  public static final short Default_Alien4_ShpIdx = 118;
  public static final short Default_Alien5_ShpIdx = 119;
  public static final short Default_Alien6_ShpIdx = 120;
  public static final short Default_Alien7_ShpIdx = 121;
  public static final short Default_Alien8_ShpIdx = 122;
  public static final short Default_Alien9_ShpIdx = 124;

  public static final ValueDefinition<?>[] IntID = Stream.of(
    new ValueDefinition<Integer>( ALIEN1_ADDRESS, Prefix + "1",         // Borg Cube
      MovRegInt.class, Register.EDI, (int)Default_Alien1_ShpIdx),
    new ValueDefinition<Integer>( ALIEN2_ADDRESS, Prefix + "2",         // Calamarain
      MovRegInt.class, Register.EDX, (int)Default_Alien2_ShpIdx),
    new ValueDefinition<Integer>( ALIEN3_ADDRESS, Prefix + "3",         // Chodak
      MovRegInt.class, Register.ECX, (int)Default_Alien3_ShpIdx),
    new ValueDefinition<Integer>( ALIEN4_ADDRESS, Prefix + "4",         // Crystal Entity
      MovRegInt.class, Register.ESI, (int)Default_Alien4_ShpIdx),
    new ValueDefinition<Integer>( ALIEN5_ADDRESS, Prefix + "5",         // Combat Drone
      MovRegInt.class, Register.ECX, (int)Default_Alien5_ShpIdx),
    new ValueDefinition<Integer>( ALIEN6_ADDRESS, Prefix + "6",         // Edo Guardian
      MovRegInt.class, Register.ESI, (int)Default_Alien6_ShpIdx),
    new ValueDefinition<Integer>( ALIEN8_ADDRESS, Prefix + "8",         // Husnock
      MovRegInt.class, Register.EDX, (int)Default_Alien8_ShpIdx),
    new ValueDefinition<Integer>( ALIEN9_ADDRESS, Prefix + "9",         // Tarellian
      MovRegInt.class, Register.EDI, (int)Default_Alien9_ShpIdx)
  ).map(x -> x.validator(SD_Monster::validateValue)
    .integrityCheck(SD_Monster::checkIntegrity))
    .toArray(ValueDefinition<?>[]::new);

  // Gomtuu ID (5)
  public static final ValueDefinition<Short> Monster7 = new ValueDefinition<Short>(
    ALIEN7_ADDRESS, Prefix + "7", MovWordPtrShort.class, Register.ESP, 0, 0x172, Default_Alien7_ShpIdx)
    .validator(SD_Monster::validateValue).integrityCheck(SD_Monster::checkIntegrity);

  // all monsters sorted by ID
  public static final ValueDefinition<?>[] All = new ValueDefinition<?>[] {
    IntID[0],
    IntID[1],
    IntID[2],
    IntID[3],
    IntID[4],
    IntID[5],
    Monster7,
    IntID[6],
    IntID[7]
  };

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);

  public static int valueOf(InternalSegment seg) {
    if (seg instanceof IntValueSegment) {
       return ((IntValueSegment)seg).intValue();
    } else if (seg instanceof ShortValueSegment) {
      return ((ShortValueSegment)seg).shortValue();
    }
    throw new IllegalArgumentException();
  }

  /**
   * value validation
   */
  static void validateValue(int value) throws InvalidArgumentsException {
    if (value < 5) {
      String msg = Language.getString("SD_Monster.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(5)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }
  }

  static void validateValue(InternalSegment seg, Object value) throws InvalidArgumentsException {
    int shpId = ((Number)value).intValue();
    validateValue(shpId);
  }

  /**
   * integrity check
   */
  static void checkIntegrity(InternalSegment seg, Vector<String> response) {
    int index = valueOf(seg);
    if (index < 5) {
      String msg = Language.getString("SD_Monster.1") //$NON-NLS-1$
        .replace("%1", Long.toString(seg.address())) //$NON-NLS-1$
        .replace("%2", Integer.toString(5)); //$NON-NLS-1$
      response.add(msg);
    }

    // compare value with the one in shiplist.sst
    Stbof stbof = UE.FILES.stbof();
    if (stbof != null) {
      try {
        ShipList sl = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
        ID[] id = sl.getIDs(Race.Monster);

        int i = Integer.parseInt(seg.getName().substring(5));
        if (i < 9)
          i--;

        if (index != id[i].ID) {
          String msg = Language.getString("SD_Monster.2"); //$NON-NLS-1$
          msg = msg.replace("%1", Long.toString(seg.address())); //$NON-NLS-1$
          msg = msg.replace("%2", Integer.toString(index)); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(id[i].ID)); //$NON-NLS-1$

          response.add(msg);
        }
      } catch (IOException e) {
        e.printStackTrace();
        response.add(e.getLocalizedMessage());
      }
    }
  }
}
