package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.seg.bld.MartialLawBldIdFix;

/**
 * This interface holds the MartialLawBld segment definitions.
 */
public interface SD_BldId_MartialLaw {

  public static final String Prefix  = "martialLawBld"; //$NON-NLS-1$
  public static final String Prefix1 = Prefix + "1"; //$NON-NLS-1$
  public static final String Prefix2 = Prefix + "2"; //$NON-NLS-1$
  public static final String Prefix3 = Prefix + "3"; //$NON-NLS-1$
  public static final String Prefix4 = Prefix + "4"; //$NON-NLS-1$
  public static final int ML1_ADDRESS = 0x00039C1D;
  public static final int ML2_ADDRESS = 0x00063FBB;
  public static final int ML3_ADDRESS = MartialLawBldIdFix.ADDRESS /*0x0004C4DD*/;
  public static final int ML4_ADDRESS = 0x00039B6E;

  // default martial law building ids by race
  public static final short[] DefaultIDs = new short[] { 27, 39, 33, 44, 51 };

  // main segments
  // Martial law building ids <int> of each empire.
  public static final ValueDefinition<?>[] ML1 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(ML1_ADDRESS + 2,  Prefix1 + "_card", MovRegInt.class, Register.EAX, (int)DefaultIDs[0])
    .desc("Cardassian 'Inquisition' building id <int>"),
    new ValueDefinition<Integer>(ML1_ADDRESS + 30, Prefix1 + "_fed",  MovRegInt.class, Register.EAX, (int)DefaultIDs[1])
    .desc("Federation 'Martial Law' building id <int>"),
    new ValueDefinition<Integer>(ML1_ADDRESS + 16, Prefix1 + "_ferg", MovRegInt.class, Register.EAX, (int)DefaultIDs[2])
    .desc("Ferengi 'Festival of Fun' building id <int>"),
    new ValueDefinition<Integer>(ML1_ADDRESS + 44, Prefix1 + "_klng", MovRegInt.class, Register.EAX, (int)DefaultIDs[3])
    .desc("Klingon 'Police State' building id <int>"),
    new ValueDefinition<Integer>(ML1_ADDRESS + 58, Prefix1 + "_rom",  MovRegInt.class, Register.EAX, (int)DefaultIDs[4])
    .desc("Romulan 'Tribunal' building id <int>")
  };

  // Martial law building ids <int> of each empire.
  public static final ValueDefinition<?>[] ML2 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(ML2_ADDRESS,       Prefix2 + "_card", MovRegInt.class, Register.EAX, (int)DefaultIDs[0])
    .desc("Cardassian 'Inquisition' building id <int>"),
    new ValueDefinition<Integer>(ML2_ADDRESS + 103, Prefix2 + "_fed",  MovRegInt.class, Register.EAX, (int)DefaultIDs[1])
    .desc("Federation 'Martial Law' building id <int>"),
    new ValueDefinition<Integer>(ML2_ADDRESS + 89,  Prefix2 + "_ferg", MovRegInt.class, Register.EAX, (int)DefaultIDs[2])
    .desc("Ferengi 'Festival of Fun' building id <int>"),
    new ValueDefinition<Integer>(ML2_ADDRESS + 117, Prefix2 + "_klng", MovRegInt.class, Register.EAX, (int)DefaultIDs[3])
    .desc("Klingon 'Police State' building id <int>"),
    new ValueDefinition<Integer>(ML2_ADDRESS + 131, Prefix2 + "_rom",  MovRegInt.class, Register.EAX, (int)DefaultIDs[4])
    .desc("Romulan 'Tribunal' building id <int>")
  };

  // Martial law building ids <byte> of each empire.
  public static final SegmentDefinition ML3 = new SegmentDefinition(
    ML3_ADDRESS, Prefix3, MartialLawBldIdFix.class).size(MartialLawBldIdFix.SIZE)
    .desc("Code fixture segment to set the major race martial law building ids and automatically patch"
      + " the compare instructions from signed byte value to short if default maximum of 127 is exceeded."
      + "<br>TODO: Cleanup sneaked fixture once MUM is patched.");

  // Martial law building ids <int> of each empire.
  public static final ValueDefinition<?>[] ML4 = new ValueDefinition<?>[] {
    new ValueDefinition<Integer>(ML4_ADDRESS,      Prefix4 + "_card", MovRegInt.class, Register.EAX, (int)DefaultIDs[0])
      .desc("Cardassian 'Inquisition' building id <int>"),
    new ValueDefinition<Integer>(ML4_ADDRESS + 14, Prefix4 + "_fed",  MovRegInt.class, Register.EAX, (int)DefaultIDs[1])
      .desc("Federation 'Martial Law' building id <int>"),
    new ValueDefinition<Integer>(ML4_ADDRESS + 7,  Prefix4 + "_ferg", MovRegInt.class, Register.EAX, (int)DefaultIDs[2])
      .desc("Ferengi 'Festival of Fun' building id <int>"),
    new ValueDefinition<Integer>(ML4_ADDRESS + 21, Prefix4 + "_klng", MovRegInt.class, Register.EAX, (int)DefaultIDs[3])
      .desc("Klingon 'Police State' building id <int>"),
    new ValueDefinition<Integer>(ML4_ADDRESS + 28, Prefix4 + "_rom",  MovRegInt.class, Register.EAX, (int)DefaultIDs[4])
      .desc("Romulan 'Tribunal' building id <int>")
  };

  public static final SegmentDefinition[] All = Stream.of(
    Arrays.stream(ML1), Arrays.stream(ML2), Stream.of(ML3), Arrays.stream(ML4)
  ).flatMap(Function.identity()).toArray(SegmentDefinition[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
