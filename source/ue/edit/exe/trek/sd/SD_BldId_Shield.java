package ue.edit.exe.trek.sd;

import java.util.Arrays;
import java.util.stream.Stream;
import ue.edit.exe.common.OpCode.Register;
import ue.edit.exe.seg.prim.MovRegInt;

/**
 * This interface holds the shield generator building id segment definitions.
 */
public interface SD_BldId_Shield {

  public static final String Prefix = "shieldGenBld"; //$NON-NLS-1$

  public static final int SHIELD1_ADDRESS = 0x0006CCED;
  public static final int SHIELD2_ADDRESS = 0x0005183D;
  public static final int SHIELD3_ADDRESS = 0x0005182D;

  // main segments
  public static final ValueDefinition<?>[] All = Stream.of(
    new ValueDefinition<Integer>(SHIELD1_ADDRESS, Prefix + "1", MovRegInt.class, Register.EAX, 20),
    new ValueDefinition<Integer>(SHIELD2_ADDRESS, Prefix + "2", MovRegInt.class, Register.EDX, 20),
    new ValueDefinition<Integer>(SHIELD3_ADDRESS, Prefix + "3", MovRegInt.class, Register.EDX, 20)
  ).map(x -> x.desc("Shield generator building id.")).toArray(ValueDefinition<?>[]::new);

  public static final String[] Names = Arrays.stream(All)
    .map(SegmentDefinition::name).toArray(String[]::new);
}
