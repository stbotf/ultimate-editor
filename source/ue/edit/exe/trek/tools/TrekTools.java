package ue.edit.exe.trek.tools;

/**
 * Trek.exe helper routines.
 */
public interface TrekTools {

  public static final int ERROR_UNDERFLOW = -1;
  public static final int ERROR_OVERFLOW = -2;
  public static final int ERROR_INVALID = -3;

  /**
   * Convert trek.exe file offset to assembler address.
   *
   * Errors with:
   * ERROR_UNDERFLOW(-1): offset precedes 0x400, first section start and PE file header end
   * ERROR_OVERFLOW(-2):  offset exceeds 0x1B2E00, the unmodified 'vanilla' trek.exe file end
   *
   * @param fileOffset the file offset
   * @return trek.exe assembler address or error code < 0
   */
  public static int toAsmAddress(int fileOffset) {
    if (fileOffset < 0x400)
      return ERROR_UNDERFLOW;
    if (fileOffset >= 0x1B2E00)
      return ERROR_OVERFLOW;

    // - note,
    // - in addition, section 4 'BSS' is missing from file
    if (fileOffset < 0x172A00) {
      // code section 1: assembler address 0x401000 to 0x5735FF
      fileOffset += 0x400C00;
    } else if (fileOffset < 0x173E00) {
      // data section 2: assembler address 0x574000 to 0x5753FF
      fileOffset += 0x401600;
    } else if (fileOffset < 0x19F400) {
      // data section 3: assembler address 0x576000 to 0x5A15FF
      fileOffset += 0x402200;
    } else {
      // data section 5: assembler address 0x68A000 to 0x69D9FF
      fileOffset += 0x4EAC00;
    }

    return fileOffset;
  }

  /**
   * Convert trek.exe assembler address to file offset.
   *
   * Errors with:
   * ERROR_UNDERFLOW(-1): address precedes asm_401000, first section virtual base address
   * ERROR_OVERFLOW(-2):  address exceeds asm_69DA00, the unmodified 'vanilla' trek.exe file end
   * ERROR_INVALID(-3):   address is virtual only and can't be mapped to a file address
   *
   * @param asmAddress the assembler address
   * @return trek.exe file offset or error code < 0
   */
  public static int toFileOffset(int asmAddress) {
    if (asmAddress < 0x401000)
      return ERROR_UNDERFLOW;
    if (asmAddress >= 0x69DA00)
      return ERROR_OVERFLOW;

    if (asmAddress < 0x573600) {
      // code section 1: file offset 0x000400 to 0x1729FF
      asmAddress -= 0x400C00;
    } else if (asmAddress < 0x574000) {
      // 2nd section alignment
      return ERROR_INVALID;
    } else if (asmAddress < 0x575400) {
      // data section 2: file offset 0x172A00 to 0x173E00
      asmAddress -= 0x401600;
    } else if (asmAddress < 0x576000) {
      // 3rd section alignment
      return ERROR_INVALID;
    } else if (asmAddress < 0x5A1600) {
      // data section 3: file offset 0x173E00 to 0x19F400
      asmAddress -= 0x402200;
    } else if (asmAddress < 0x68A000) {
      // section 4 'BSS', starting at 0x5A2000 but aligned by 0xA00
      // is virtual only and missing from file
      return ERROR_INVALID;
    } else {
      // data section 5: file offset 0x19F400 to 0x1B2DFF
      asmAddress -= 0x4EAC00;
    }

    return asmAddress;
  }
}
