package ue.edit.exe.trek.common;

/**
 * This interface holds common trek constants.
 */
public interface CTrek {

  // trek.exe uses reduced precision for the PI value,
  // which must be regarded when calculating values
  public static final double PI = 3.141592654;

  public static final int NUM_AIAGENDAS = AIAgenda.Count;

  // #region AI
  public interface AIAgenda {
    public static final int NormalExpansion = 0;
    public static final int UnopposedExpansion = 1;
    public static final int NormalConsolidation = 2;
    public static final int DefensiveConsolidation = 3;
    public static final int OffensiveConsolidation = 4;
    public static final int ColdWar = 5;
    public static final int DefensiveWar = 6;
    public static final int OffensiveWar = 7;
    public static final int Jihad = 8;

    public static final int ListEnd = -1;
    public static final int Count = 9;
  }
  // #endregion AI
}
