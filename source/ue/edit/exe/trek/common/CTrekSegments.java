package ue.edit.exe.trek.common;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.val;
import ue.edit.exe.trek.sd.SD_App;
import ue.edit.exe.trek.sd.SD_AutoUpgrade;
import ue.edit.exe.trek.sd.SD_AvgSpaceObjNum;
import ue.edit.exe.trek.sd.SD_BldCnt;
import ue.edit.exe.trek.sd.SD_BldId_Dilithium;
import ue.edit.exe.trek.sd.SD_BldId_EmpSp;
import ue.edit.exe.trek.sd.SD_BldId_GrndDef;
import ue.edit.exe.trek.sd.SD_BldId_MainEnergy;
import ue.edit.exe.trek.sd.SD_BldId_MainFarm;
import ue.edit.exe.trek.sd.SD_BldId_MainIndustry;
import ue.edit.exe.trek.sd.SD_BldId_MainIntel;
import ue.edit.exe.trek.sd.SD_BldId_MainResearch;
import ue.edit.exe.trek.sd.SD_BldId_MartialLaw;
import ue.edit.exe.trek.sd.SD_BldId_MinorSp;
import ue.edit.exe.trek.sd.SD_BldId_Orb;
import ue.edit.exe.trek.sd.SD_BldId_Scanner;
import ue.edit.exe.trek.sd.SD_BldId_Shield;
import ue.edit.exe.trek.sd.SD_BldId_Shipyard;
import ue.edit.exe.trek.sd.SD_BldId_TradeGoods;
import ue.edit.exe.trek.sd.SD_FileNames;
import ue.edit.exe.trek.sd.SD_GalDensity;
import ue.edit.exe.trek.sd.SD_GalShape;
import ue.edit.exe.trek.sd.SD_GroundCombat;
import ue.edit.exe.trek.sd.SD_MandatoryDilFix;
import ue.edit.exe.trek.sd.SD_MapSize;
import ue.edit.exe.trek.sd.SD_MinEmpDist;
import ue.edit.exe.trek.sd.SD_Monster;
import ue.edit.exe.trek.sd.SD_NumericSID;
import ue.edit.exe.trek.sd.SD_SectorSize;
import ue.edit.exe.trek.sd.SD_ShipNum;
import ue.edit.exe.trek.sd.SD_SpShips;
import ue.edit.exe.trek.sd.SD_StellarTypes;
import ue.edit.exe.trek.sd.SD_SysAlign;
import ue.edit.exe.trek.sd.SD_TechLvl;
import ue.edit.exe.trek.sd.SegmentDefinition;

/**
 * This interface holds common trek segment constants.
 */
public interface CTrekSegments {

  // LIST OF INTERNAL SEGMENTS
  public static final String AIShipSets = "aiShpSets"; //$NON-NLS-1$
  public static final String AlienStartingId = "alienStartingId"; //$NON-NLS-1$
  public static final String AlienStartingId2 = "alienStartingId2"; //$NON-NLS-1$
  public static final String AlienStartingId3 = "alienStartingId3"; //$NON-NLS-1$
  public static final String AlienStartingId4 = "alienStartingId4"; //$NON-NLS-1$
  public static final String BaseMapRanges = "baseMapRanges"; //$NON-NLS-1$
  public static final String EmpireImagePrefixes = "empireImgPrefixes"; //$NON-NLS-1$
  public static final String MapRangeBonuses = "mapRangeTechBonuses"; //$NON-NLS-1$
  public static final String MaxMinorsNumber = "maxMinorsNumber"; //$NON-NLS-1$
  public static final String MinMinorsNumber = "minMinorsNumber"; //$NON-NLS-1$
  public static final String MinorMainBldIds = "minorMainBldIDs"; //$NON-NLS-1$
  public static final String MainBldUpgrades = "mainBldUpgrades"; //$NON-NLS-1$
  public static final String MEStartCond = "meStartingConditions"; //$NON-NLS-1$
  public static final String PlanetBonusTypes = "planetBonusTypes"; //$NON-NLS-1$
  public static final String PunyFactories = "punyFactories"; //$NON-NLS-1$
  public static final String PunyFarms = "punyFarms"; //$NON-NLS-1$
  public static final String RaceList = "raceList"; //$NON-NLS-1$
  public static final String RaceListAddresses = "racelistAddresses"; //$NON-NLS-1$
  public static final String ScreensaverModelList = "screenSaverModelList"; //$NON-NLS-1$
  public static final String SelectPhaserHobFile = "selectPhaserHobFile"; //$NON-NLS-1$
  public static final String SelectPlasmaHobFile = "selectPlasmaHobFile"; //$NON-NLS-1$
  public static final String SelectTorpedoHobFile = "selectTorpedoHobFile"; //$NON-NLS-1$
  public static final String ShipModelSegment = "shipmodelSegment"; //$NON-NLS-1$
  public static final String ShipnameCode = "shipNameCode"; //$NON-NLS-1$
  public static final String SpecialShips = "specialShips"; //$NON-NLS-1$

  /**
   * A list of at least partially editable segments.
   */
  public static final String[] SEGMENTLIST = new String[]{
    MEStartCond, AlienStartingId, SpecialShips, ShipModelSegment,
    AlienStartingId2, AlienStartingId3, AlienStartingId4, ScreensaverModelList, RaceList,
    BaseMapRanges, MapRangeBonuses, RaceListAddresses,
    PunyFarms, PunyFactories, AIShipSets, MinMinorsNumber, MaxMinorsNumber, EmpireImagePrefixes,
    MinorMainBldIds, MainBldUpgrades, PlanetBonusTypes,
    ShipnameCode, SelectPlasmaHobFile, SelectPhaserHobFile, SelectTorpedoHobFile
  };

  public static final Map<Integer, SegmentDefinition> AllSegments = Stream.of(
    Arrays.stream(SD_AutoUpgrade.All),
    Arrays.stream(SD_AvgSpaceObjNum.All),
    Arrays.stream(SD_BldCnt.All),
    Arrays.stream(SD_BldId_GrndDef.All),
    Arrays.stream(SD_BldId_Dilithium.All),
    Arrays.stream(SD_BldId_EmpSp.All),
    Arrays.stream(SD_BldId_MainFarm.All),
    Arrays.stream(SD_BldId_MainEnergy.All),
    Arrays.stream(SD_BldId_MainIndustry.All),
    Arrays.stream(SD_BldId_MainIntel.All),
    Arrays.stream(SD_BldId_MainResearch.All),
    Arrays.stream(SD_BldId_MartialLaw.All),
    Arrays.stream(SD_BldId_Orb.All),
    Arrays.stream(SD_BldId_Shield.All),
    Arrays.stream(SD_BldId_Shipyard.All),
    Arrays.stream(SD_BldId_TradeGoods.All),
    Arrays.stream(SD_FileNames.All),
    Arrays.stream(SD_GalDensity.All),
    Arrays.stream(SD_GalShape.All),
    Arrays.stream(SD_GroundCombat.All),
    Arrays.stream(SD_MandatoryDilFix.All),
    Arrays.stream(SD_MapSize.All),
    Arrays.stream(SD_MinEmpDist.All),
    Arrays.stream(SD_Monster.All),
    Arrays.stream(SD_NumericSID.All),
    Arrays.stream(SD_ShipNum.All),
    Arrays.stream(SD_SectorSize.All),
    Arrays.stream(SD_SpShips.All),
    Arrays.stream(SD_StellarTypes.All),
    Arrays.stream(SD_SysAlign.All),
    Arrays.stream(SD_TechLvl.All),
    Stream.of(
      SD_App.CDProtection, SD_BldId_MinorSp.BldList, SD_BldId_Scanner.Basic
    )
  ).flatMap(x -> x).distinct().collect(Collectors.toMap(x -> x.address, Function.identity()));

  public static final Map<String, SegmentDefinition> AllSegmentsByName = AllSegments.values().stream()
    .collect(Collectors.toMap(SegmentDefinition::name, Function.identity()));

  public static String getSegmentDescription(String name) {
    val sd = AllSegmentsByName.get(name);
    return sd != null ? sd.description() : null;
  }

  public static String getSegmentDescription(int address) {
    val sd = AllSegments.get(address);
    return sd != null ? sd.description() : null;
  }
}
