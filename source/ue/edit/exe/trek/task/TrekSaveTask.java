package ue.edit.exe.trek.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import ue.edit.common.InternalFile;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.Trek;
import ue.exception.InvalidArgumentsException;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.stream.StreamTools;

public class TrekSaveTask extends UEWorker<Boolean> {

  private static final String TmpFileName = "trek.tmp"; //$NON-NLS-1$

  private final File sourceFile;
  private final File destFile;
  private final boolean backup;
  private final String ext;

  private List<? extends InternalFile> TODO;
  private File outFile;

  public TrekSaveTask(Trek trek, File destination, boolean backup, String extension) {
    this.backup = backup;
    this.ext = extension;
    sourceFile = trek.getSourceFile();
    outFile = destFile = destination;

    TODO = trek.listChangedFiles();
    RESULT = false;
  }

  @Override
  public Boolean work() throws Exception {
    // this will sort segments by address
    Collections.sort(TODO);

    // create temporary file and backup if destination already exists
    checkDestination();

    if (isCancelled()) {
      // delete temp file created by getWritableLocalFile
      outFile.delete();
      return false;
    }

    // start write
    try (
      FileOutputStream fos = new FileOutputStream(outFile);
      FileInputStream fis = new FileInputStream(sourceFile);
    ){
      int addy = 0;
      InternalSegment last = null;

      for (InternalFile file : TODO) {
        if (isCancelled()) {
          outFile.delete();
          return false;
        }

        if (!(file instanceof InternalSegment)) {
          String msg = Language.getString("TrekSaveTask.2"); //$NON-NLS-1$
          throw new ClassCastException(msg);
        }

        InternalSegment seg = (InternalSegment) file;

        String msg = Language.getString("TrekSaveTask.3"); //$NON-NLS-1$
        msg = msg.replace("%1", seg.getName()); //$NON-NLS-1$
        feedMessage(msg);

        int size = seg.address() - addy; // calculate num of bytes from cur pos to seg start
        addy = seg.address() + seg.getSize(); // new address

        if (size < 0 && last != null) {
          msg = Language.getString("TrekSaveTask.4")
            .replace("%2", seg.getName())
            .replace("%1", last.getName());
          throw new UnsupportedOperationException(msg);
        }

        byte[] b = StreamTools.readBytes(fis, size);
        fos.write(b);

        boolean changed = seg.madeChanges() || seg.isSneaking();
        seg.save(fos);
        fos.flush();

        if (changed) {
          // re-check for file changes
          seg.checkForChanges();
        }

        int segSize = seg.getSize();
        StreamTools.skip(fis, segSize);

        long bytesWritten = fos.getChannel().position() - seg.address();
        if (bytesWritten != segSize) {
          String err = Language.getString("TrekSaveTask.5")
            .replace("%1", Integer.toString(seg.address()));
          throw new InvalidArgumentsException(err);
        }

        last = seg;
      }

      // copy the rest from source
      byte[] b = new byte[1024];
      while (fis.available() > 0) {
        int len = fis.read(b);
        fos.write(b, 0, len);
      }
    }
    catch (Exception e) {
      outFile.delete();
      throw e;
    }

    // last chance to cancel action
    if (isCancelled()) {
      outFile.delete();
      return false;
    }

    // replace destination by temporary file
    applyTempFile();

    // on success mark all segments saved
    for (InternalFile file : TODO) {
      file.markSaved();
    }

    return true;
  }

  private void checkDestination() throws IOException {
    if (!destFile.exists())
      return;

    // create backup
    if (backup) {
      sendMessage(Language.getString("TrekSaveTask.0")); //$NON-NLS-1$

      File file = new File(destFile.getPath() + ext);
      int i = 1;

      while (file.exists()) {
        file = new File(destFile.getPath() + i + ext);
        i++;
      }

      Files.copy(destFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    String msg = Language.getString("TrekSaveTask.1"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", TmpFileName)); //$NON-NLS-1$

    outFile = FileManager.getWritableLocalFile(TmpFileName);
  }

  private void applyTempFile() throws IOException {
    if (outFile == destFile)
      return;

    String msg = Language.getString("TrekSaveTask.7"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

    // replace with TMP file
    // on error keep temp file for manual rename
    Files.copy(outFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

    msg = Language.getString("TrekSaveTask.8"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

    // attempt to delete the temporary file
    boolean deleted = outFile.delete();
    if (!deleted) {
      // the save operation succeeded, just the temp file couldn't be removed
      msg = Language.getString("TrekSaveTask.9"); //$NON-NLS-1$
      logError(msg.replace("%1", outFile.getName())); //$NON-NLS-1$
    }
  }

}
