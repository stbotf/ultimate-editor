package ue.edit.exe.trek.task;

import java.util.Arrays;
import java.util.Optional;
import java.util.TreeMap;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.service.Language;
import ue.service.UEWorker;

public class TrekCheckTask extends UEWorker<Vector<String>> {

  private Trek trek;
  private TreeMap<String, InternalFile> filesToCheck;

  public TrekCheckTask(Trek trek) {
    this.trek = trek;
    RESULT = new Vector<String>();

    // collect files to check for async processing
    filesToCheck = trek.listCachedFiles().stream().collect(Collectors.toMap(
      InternalSegment::getName, Function.identity(), (x,y) -> y, TreeMap::new));
  }

  @Override
  public Vector<String> work() {
    // load core segments
    val coreSegments = trek.tryGetInternalFiles(Arrays.asList(CTrekSegments.SEGMENTLIST), true, Optional.of(x -> {
      feedMessage("Loading " + x + "...");
      return !isCancelled();
    }));

    for (val seg : coreSegments) {
      filesToCheck.put(seg.getName(), seg);
    }

    // report load errors
    for (val err : trek.listLoadErrors().entrySet()) {
      String msg = Language.getString("TrekCheckTask.2"); //$NON-NLS-1$
      msg = msg.replace("%1", err.getKey()); //$NON-NLS-1$
      msg = msg.replace("%2", err.getValue()); //$NON-NLS-1$
      RESULT.add(msg);
    }

    // check cached segments for errors, sorted by segment name
    for (InternalFile seg : filesToCheck.values()) {
      if (isCancelled())
        break;

      try {
        String msg = Language.getString("TrekCheckTask.0"); //$NON-NLS-1$
        msg = msg.replace("%1", seg.getName()); //$NON-NLS-1$
        feedMessage(msg);

        Vector<String> temp = seg.check();
        if (temp != null && !temp.isEmpty())   // add to global results
          RESULT.addAll(temp);
      } catch (Exception f) {
        f.printStackTrace();

        String msg = Language.getString("TrekCheckTask.1"); //$NON-NLS-1$
        msg = msg.replace("%1", seg.getName()); //$NON-NLS-1$
        msg = msg.replace("%2", f.getMessage()); //$NON-NLS-1$

        RESULT.add(msg);
      }
    }

    return RESULT;
  }
}
