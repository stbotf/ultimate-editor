package ue.edit.res.alt.task;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.List;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.alt.Alt;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.LzssEntry;
import ue.util.file.LzssFile;
import ue.util.stream.StreamTools;

public class AltSaveTask extends UEWorker<Boolean> {

  private static final String TmpFileName = "alt.tmp"; //$NON-NLS-1$

  private final Alt alt;
  private final File sourceFile;
  private final File destFile;
  private final boolean backup;
  private final String ext;

  private List<? extends InternalFile> filesChanged;
  private File outFile;

  public AltSaveTask(Alt archive, File destination, boolean backup, String extension) {
    this.alt = archive;
    this.backup = backup;
    this.ext = extension;
    sourceFile = archive.getSourceFile();
    outFile = destFile = destination;

    // first collect the files to save
    // this way the iteration doesn't become undefined when additional files are loaded during save
    filesChanged = archive.listChangedFiles();
    RESULT = false;
  }

  @Override
  public Boolean work() throws Exception {
    // create temporary file and backup if destination already exists
    checkDestination();

    if (isCancelled()) {
      // delete temp file created by getWritableLocalFile
      outFile.delete();
      return false;
    }

    HashSet<String> fileNames = new HashSet<String>();
    outFile.delete();

    // start write
    try (
      LzssFile source = new LzssFile(sourceFile, null);
      //savfile creates the file
      LzssFile dest = new LzssFile(outFile, source.getName());
    ){
      // write file, collecting the written Alt file names
      if (!writeSavOutputStream(dest, source, fileNames)) {
        // delete half written file if cancelled
        outFile.delete();
        return false;
      }
    } catch (Exception e) {
      outFile.delete();
      throw e;
    }

    // last chance to cancel action
    if (isCancelled()) {
      outFile.delete();
      return false;
    }

    // replace destination by temporary file
    applyTempFile();

    // on success mark all files saved
    alt.processCachedFiles(x -> {
      x.markSaved();
      return true;
    });

    return true;
  }

  private boolean writeSavOutputStream(LzssFile dest, LzssFile source, HashSet<String> fileNames)
    throws IOException {
    // first write and collect all cached files
    if (!writeChangedFiles(dest, fileNames))
      return false;

    // copy all source entries not edited by UE
    if (!copySourceEntries(dest, source, fileNames))
      return false;

    // close source since it isn't needed any further
    try {
      source.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    String msg = Language.getString("AltSaveTask.4"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", "mod.lst")); //$NON-NLS-1$ //$NON-NLS-2$

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    alt.modList().save(baos);
    byte[] bub = baos.toByteArray();
    int sizi = bub.length;

    LzssEntry zeg = new LzssEntry("mod.lst"); //$NON-NLS-1$
    zeg.setCompressedSize(-1);
    zeg.setUncompressedSize(sizi);

    dest.putNextEntry(zeg);

    // save and encode mod.lst
    try (OutputStream zos = dest.getOutputStream(zeg, false, false)) {
      zos.write(bub, 0, sizi);
    } finally {
      dest.closeEntry();
    }

    return true;
  }

  private boolean copySourceEntries(LzssFile dest, LzssFile source, HashSet<String> fileNames)
    throws IOException {
    OutputStream out = dest.getRawOutputStream();
    String msg = Language.getString("AltSaveTask.1"); //$NON-NLS-1$

    //GO THRU EXISTING FILES
    for (val ze : source.listFileEntries()) {
      if (isCancelled())
        return false;

      // for name lookup change to lower case
      String lcName = ze.getLcName();

      if (lcName.equals("mod.lst") || alt.isRemoved(lcName)) //$NON-NLS-1$
        continue;

      // check if not already written
      if (!fileNames.contains(lcName)) {
        feedMessage(msg.replace("%1", ze.getName())); //$NON-NLS-1$

        // copy unloaded files
        try (InputStream is = source.getRawInputStream(ze)) {
          dest.putNextEntry(ze);
          StreamTools.transfer(is, out);
        }

        // Complete the entry
        dest.closeEntry();
      }
    }

    return true;
  }

  private boolean writeChangedFiles(LzssFile dest, HashSet<String> fileNames)
    throws IOException {
    String msg = Language.getString("AltSaveTask.4"); //$NON-NLS-1$

    for (InternalFile gef : filesChanged) {
      if (isCancelled())
        return false;

      String fileName = gef.getName();
      // for name lookup change to lower case
      String lcName = fileName.toLowerCase();

      // this should be redundant, but just in case...
      if (lcName.equals("mod.lst") || alt.isRemoved(lcName)) //$NON-NLS-1$
        continue;

      // check if not already written
      if (fileNames.add(lcName)) {
        sendMessage(msg.replace("%1", fileName)); //$NON-NLS-1$

        if (gef.madeChanges())
          alt.recordFileChange(fileName);

        int ln = gef.getSize();
        LzssEntry zeg = new LzssEntry(fileName);
        zeg.setCompressedSize(-1);
        zeg.setUncompressedSize(ln);
        dest.putNextEntry(zeg);

        // save and encode cached files
        try (OutputStream zos = dest.getOutputStream(zeg, false, false)) {
          gef.save(zos);
        }
      }
    }

    return true;
  }

  private void checkDestination() throws IOException {
    if (!destFile.exists())
      return;

    // create backup
    if (backup) {
      sendMessage(Language.getString("AltSaveTask.5")); //$NON-NLS-1$

      File file = new File(destFile.getPath() + ext);
      int i = 1;

      while (file.exists()) {
        file = new File(destFile.getPath() + i + ext);
        i++;
      }

      Files.copy(destFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    String msg = Language.getString("AltSaveTask.6"); //$NON-NLS-1$
    msg = msg.replace("%1", TmpFileName); //$NON-NLS-1$
    sendMessage(msg);

    outFile = FileManager.getWritableLocalFile(TmpFileName);
  }

  private void applyTempFile() throws IOException {
    if (outFile == destFile)
      return;

    String msg = Language.getString("AltSaveTask.1"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

    // replace with TMP file
    // on error keep temp file for manual rename
    Files.copy(outFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

    msg = Language.getString("AltSaveTask.2"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

    // attempt to delete the temporary file
    boolean deleted = outFile.delete();
    if (!deleted) {
      // the save operation succeeded, just the temp file couldn't be removed
      msg = Language.getString("AltSaveTask.3"); //$NON-NLS-1$
      logError(msg.replace("%1", outFile.getName())); //$NON-NLS-1$
    }
  }

}
