package ue.edit.res.alt.task;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.alt.Alt;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.LzssFile;

public class AltCheckTask extends UEWorker<Vector<String>> {

  Alt alt;
  File altFile;
  ArrayList<InternalFile> filesToCheck;

  public AltCheckTask(Alt archive) {
    this.alt = archive;
    altFile = archive.getSourceFile();
    RESULT = new Vector<>();

    // collect files to check for async processing
    filesToCheck = new ArrayList<>(archive.listCachedFiles());
  }

  @Override
  public Vector<String> work() {
    sendMessage(Language.getString("AltCheckTask.0")); //$NON-NLS-1$

    //check all of the file as whole
    try (
      LzssFile su = new LzssFile(altFile, null);
    ){
      Vector<String> temp = su.check();
      if (!temp.isEmpty())
        RESULT.addAll(temp);
    } catch (Exception f) {
      String msg = Language.getString("AltCheckTask.1"); //$NON-NLS-1$
      msg = msg.replace("%1", altFile.getName()); //$NON-NLS-1$
      msg = msg.replace("%2", f.getMessage()); //$NON-NLS-1$
      f.printStackTrace();
      RESULT.add(msg);
    }

    String msg = Language.getString("AltCheckTask.2"); //$NON-NLS-1$
    for (InternalFile file : filesToCheck) {
      try {
        if (isCancelled()) {
          RESULT.add(Language.getString("AltCheckTask.3")); //$NON-NLS-1$
          return RESULT;
        }

        feedMessage(msg.replace("%1", file.getName())); //$NON-NLS-1$

        Vector<String> temp = file.check();
        if (!temp.isEmpty())
          RESULT.addAll(temp);
      } catch (Exception f) {
        msg = Language.getString("AltCheckTask.1"); //$NON-NLS-1$
        msg = msg.replace("%1", file != null ? file.getName() : "??"); //$NON-NLS-1$
        msg = msg.replace("%2", f.getMessage()); //$NON-NLS-1$
        f.printStackTrace();
        RESULT.add(msg);
      }
    }

    return RESULT;
  }
}
