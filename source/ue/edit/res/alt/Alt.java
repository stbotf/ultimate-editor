package ue.edit.res.alt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.function.Predicate;
import java.util.stream.Stream;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.common.DataFile;
import ue.edit.common.FileArchive;
import ue.edit.common.GenericFile;
import ue.edit.common.InternalFile;
import ue.edit.mod.ModList;
import ue.edit.res.alt.task.AltCheckTask;
import ue.edit.res.alt.task.AltSaveTask;
import ue.exception.DataException;
import ue.gui.menu.TrekMenu;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.service.FileManager;
import ue.service.Language;
import ue.util.data.IsCloneable;
import ue.util.file.ArchiveEntry;
import ue.util.file.FileStore;
import ue.util.file.LzssFile;

/**
 * Used for alt.res.
 */
public class Alt extends FileArchive {

  public static final int TYPE = FileManager.P_ALT;

  // keep a cached list of the file entries, sorted by name
  // for the file list and the integrity checks
  private TreeMap<String, ArchiveEntry> SAV_ENTRIES = new TreeMap<>();
  private ModList MODS;
  private boolean ADD_CHANGES_2_MODLIST = true;

  @Getter @Accessors(fluent = true)
  private TrekMenu menu = new TrekMenu();

  //a list of generic files
  private HashMap<String, InternalFile> INTERNAL_FILES = new HashMap<>();
  private HashSet<String> REMOVED = new HashSet<>();

  @Override
  public int type() {
    return TYPE;
  }

  @Override
  public String getFileDescription(String fileName) {
    return null;
  }

  @Override
  public Collection<InternalFile> listCachedFiles() {
    return Collections.unmodifiableCollection(INTERNAL_FILES.values());
  }

  @Override
  public Stream<InternalFile> streamCachedFiles() {
    return INTERNAL_FILES.values().stream();
  }

  /**
   * Get all known alt.res file entries, sorted by name.
   */
  @Override
  public Map<String, ArchiveEntry> getArchiveEntries() {
    return Collections.unmodifiableMap(SAV_ENTRIES);
  }

  /**
   * Get known alt.res file entry by name.
   */
  @Override
  public ArchiveEntry getArchiveEntry(String fileName) {
    return SAV_ENTRIES.get(fileName.toLowerCase());
  }

  @Override
  public void processCachedFiles(Predicate<DataFile> callback) {
    //GO THRU LOADED FILES
    for (val file : INTERNAL_FILES.values()) {
      if (!callback.test(file))
        break;
    }
  }

  /**
   * Process all cached alt.res file entries matching the filter and predicate.
   * @param filter file name filter, can be null
   */
  @Override
  public void processCachedFiles(FilenameFilter filter, Predicate<DataFile> callback) {
    //GO THRU LOADED FILES
    for (val file : INTERNAL_FILES.values()) {
      if (filter != null && !filter.accept(sourceFile, file.getName()))
        continue;
      if (!callback.test(file))
        break;
    }
  }

  @Override
  public int getLoadedFileCount() {
    return INTERNAL_FILES.size();
  }

  @Override
  public int getRemovedFileCount() {
    return REMOVED.size();
  }

  @Override
  public Set<String> cachedFiles() {
    return INTERNAL_FILES.keySet();
  }

  @Override
  public Set<String> removedFiles() {
    return Collections.unmodifiableSet(REMOVED);
  }

  /**
   * Discards all changes.
   */
  @Override
  public void discard() {
    super.discard();
    INTERNAL_FILES.clear();
    REMOVED.clear();
  }

  /**
   * Call this function to discard single file changes.
   */
  @Override
  public void discard(String fileName) {
    super.discard(fileName);

    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    INTERNAL_FILES.remove(fileName);
    REMOVED.remove(fileName);
  }

  @Override
  public void discard(FilenameFilter filter) {
    if (filter == null) {
      discard();
      return;
    }

    INTERNAL_FILES.values().removeIf(x -> {
      String fileName = x.getName();
      if (filter.accept(sourceFile, fileName)) {
        super.discard(fileName);
        return true;
      }
      return false;
    });

    REMOVED.removeIf(x -> {
      if (filter.accept(sourceFile, x)) {
        super.discard(x);
        return true;
      }
      return false;
    });
  }

  public void updateModList() {
    for (InternalFile file : INTERNAL_FILES.values()) {
      // for name lookup change to lower case
      String fileName = file.getName().toLowerCase();

      // skip removed files
      if (REMOVED.contains(fileName))
        continue;
      // skip mod.lst, that by itself is no game change
      if (fileName.equals("mod.lst")) //$NON-NLS-1$
        continue;

      if (file.madeChanges())
        recordFileChange(file.getName());
    }
  }

  public void recordFileChange(String fileName) {
    if (ADD_CHANGES_2_MODLIST)
      MODS.add(fileName);
  }

  /**
   * Saves changes to the specified file.
   *
   * @param dest The file to which changes are to be saved.
   * @return true if succesful else false.
   * @throws IOException
   */
  @Override
  public boolean save(File dest, boolean bkp, String ext) throws IOException {
    // if not specified, save to the open file
    if (dest == null)
      dest = getTargetFile();

    val saveTask = new AltSaveTask(this, dest, bkp, ext);
    boolean success = GuiTools.runUEWorker(saveTask);

    if (success) {
      // keep cached files
      super.discard();
      REMOVED.clear();
      sourceFile = dest;
    }

    return success;
  }

  /**
   * This adds an internal file to alt.res.
   *
   * @param gf the InternalFile to be added.
   */
  @Override
  public void addInternalFile(InternalFile gf) {
    // for name lookup change to lower case
    String fileName = gf.getName().toLowerCase();
    INTERNAL_FILES.put(fileName, gf);
    REMOVED.remove(fileName);
    gf.markChanged();
    markChanged();
  }

  /**
   * This removes a file from alt.res.
   *
   * @param name the name of the file to be removed.
   */
  @Override
  public void removeInternalFile(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    INTERNAL_FILES.remove(fileName);
    REMOVED.add(fileName);
    markChanged();
  }

  /**
   * @param whereFrom The file to open. Must be a valid alt.res file!
   * @return a String array of valid commands that can be used with the edit function.
   * @throws IOException
   */
  @Override
  public void open(File whereFrom) throws IOException {
    sourceFile = null;
    SAV_ENTRIES.clear();
    discard();

    try (LzssFile sf = new LzssFile(whereFrom, null)) {
      MODS = loadModList(sf);
      loadSavFileEntries(sf);
      sourceFile = whereFrom;
    }
  }

  @Override
  public LzssFile access() throws IOException {
    return new LzssFile(sourceFile, null);
  }

  private void loadSavFileEntries(LzssFile sf) {
    SAV_ENTRIES.clear();
    sf.stream().forEach(ze -> SAV_ENTRIES.put(ze.getLcName(), ze));
  }

  private ModList loadModList(LzssFile sf) {
    val se = sf.findEntry("mod.lst"); //$NON-NLS-1$

    if (se.isPresent()) {
      try (InputStream in = sf.getInputStream(se.get(), false)) {
        return new ModList(in);
      } catch (Exception e) {
        Dialogs.displayError("Mod list error", Language.getString("Alt.5")); //$NON-NLS-2$
      }
    }

    return new ModList();
  }

  /**
   * Checks internal files for unusal data and other problems.
   *
   * @return a Vector containing the check results.
   */
  @Override
  public Vector<String> check() {
    val checkTask = new AltCheckTask(this);
    val response = Optional.ofNullable(GuiTools.runUEWorker(checkTask)).orElse(new Vector<>());

    if (checkTask.isCancelled())
      response.add(Language.getString("Alt.18")); //$NON-NLS-1$
    if (response.isEmpty())
      response.add(Language.getString("Alt.7")); //$NON-NLS-1$

    return response;
  }

  @Override
  public InternalFile getCachedFile(String fileName) {
    // for name lookup change to lower case
    return INTERNAL_FILES.get(fileName.toLowerCase());
  }

  @Override
  public void loadReadableFiles(Predicate<String> cbCancel) throws IOException {
    try (LzssFile zf = access()) {
      zf.processFiles((arc, entry) -> {
        String fileName = entry.getName();
        String lcName = entry.getLcName();

        // skip if already loaded or removed files
        if (INTERNAL_FILES.containsKey(lcName) || REMOVED.contains(lcName))
          return true;

        // stop if cancelled
        if (!cbCancel.test(fileName))
          return false;

        try (val lock = getFileLock(fileName)) {
          InternalFile file = createFile(fileName, true);

          try (InputStream in = zf.getInputStream(entry, false)) {
            file.load(in);
            INTERNAL_FILES.put(lcName, file);
            removeLoadError(fileName);
          } catch (IOException e) {
            addLoadError(fileName, e instanceof DataException ? e.getMessage() : e.toString());
          }
        }

        return true;
      });
    }
  }

  @Override
  protected Optional<InternalFile> loadFile(Optional<FileStore> arch, String fileName, int loadFlags) throws IOException {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();
    boolean reload = (loadFlags & LoadFlags.RELOAD) != 0;
    boolean copy = (loadFlags & LoadFlags.COPY) != 0;

    if (reload) {
      REMOVED.remove(lcName);
    } else if (REMOVED.contains(lcName)) {
      if ((loadFlags & LoadFlags.THROW) != 0) {
        String err = Language.getString("Alt.8") //$NON-NLS-1$
          .replace("%1", fileName); //$NON-NLS-1$
        throw new FileNotFoundException(err);
      }
      return Optional.empty();
    }

    // if already loaded, return cached and possibly modified internal file
    InternalFile file = INTERNAL_FILES.get(lcName);
    if (file != null && !reload)
      return Optional.of(copy ? copyFile(fileName, file) : file);

    boolean cache = (loadFlags & LoadFlags.CACHE) != 0;

    try {
      if (file != null) {
        // reuse same old file so any cached references remain valid
        file.clear();
        cache = false;
      } else {
        file = createFile(fileName, true);
      }

      try (LzssFile lzss = arch.isPresent() ? null : access()) {
        val zf = lzss != null ? lzss : arch.get();
        val entry = zf.getEntry(fileName, (loadFlags & LoadFlags.THROW) != 0);
        if (!entry.isPresent())
          return Optional.empty();

        try (InputStream in = zf.getInputStream(entry.get(), false)) {
          file.load(in);
        }
      }

      if (cache)
        INTERNAL_FILES.put(lcName, file);
      if (copy)
        file = copyFile(fileName, file);

      removeLoadError(fileName);
      return Optional.of(file);

    } catch (IOException e) {
      addLoadError(fileName, e instanceof DataException ? e.getMessage() : e.toString());
      throw e;
    }
  }

  private InternalFile copyFile(String fileName, InternalFile file) throws IOException {
    if (file instanceof IsCloneable)
      return (InternalFile) ((IsCloneable) file).clone();

    // fallback to copy the data
    InternalFile copy = createFile(fileName, true);
    copy.load(file.toByteArray());
    return copy;
  }

  /**
   * (non-Javadoc)
   * @see ue.edit.common.CanBeEdited#modList()
   */
  @Override
  public ModList modList() {
    return MODS;
  }

  /* (non-Javadoc)
   * @see ue.edit.CanBeEdited#isModded()
   */
  @Override
  public boolean isModded() {
    return !MODS.isEmpty() || isChanged();
  }

  /**
   * Returns true if the internal file is cached.
   */
  @Override
  public boolean isCached(String fileName) {
    // for name lookup change to lower case
    return INTERNAL_FILES.containsKey(fileName.toLowerCase());
  }

  @Override
  public boolean isChanged(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    if (REMOVED.contains(fileName))
      return true;

    InternalFile file = INTERNAL_FILES.get(fileName);
    return file != null && file.madeChanges();
  }

  @Override
  public boolean isRemoved(String fileName) {
    // for name lookup change to lower case
    return REMOVED.contains(fileName.toLowerCase());
  }

  @Override
  public boolean isChunked(String fileName) {
    return false;
  }

  @Override
  public Class<?> getFileClass(String fileName) {
    return GenericFile.class;
  }

  private InternalFile createFile(String fileName, boolean defaultToGeneric) throws IOException {
    return new GenericFile(fileName);
  }

}
