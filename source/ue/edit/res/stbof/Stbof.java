package ue.edit.res.stbof;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidClassException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.zip.ZipException;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.DataFile;
import ue.edit.common.FileArchive;
import ue.edit.common.GenericFile;
import ue.edit.common.InternalFile;
import ue.edit.mod.ModList;
import ue.edit.res.stbof.common.CStbofFileDesc;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.bin.AIBldReq;
import ue.edit.res.stbof.files.bin.BldSet;
import ue.edit.res.stbof.files.bin.Converse;
import ue.edit.res.stbof.files.bin.Intel;
import ue.edit.res.stbof.files.bin.Meplanet;
import ue.edit.res.stbof.files.bin.Morale;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.edit.res.stbof.files.bin.Shipname;
import ue.edit.res.stbof.files.bin.Starname;
import ue.edit.res.stbof.files.bst.Edifbnft;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.desc.StringFile;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.fnt.TgaFont;
import ue.edit.res.stbof.files.gif.GifImage;
import ue.edit.res.stbof.files.hob.HobFile;
import ue.edit.res.stbof.files.lst.PaletteList;
import ue.edit.res.stbof.files.lst.Texture;
import ue.edit.res.stbof.files.pst.Planet;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.ShipRace;
import ue.edit.res.stbof.files.sst.ShipTech;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.tdb.TextDataBase;
import ue.edit.res.stbof.files.tec.RaceTech;
import ue.edit.res.stbof.files.tec.TecField;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.edit.res.stbof.files.tga.TargaHeader;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.edit.res.stbof.files.wtf.Aiempire;
import ue.edit.res.stbof.files.wtf.Aiminor;
import ue.edit.res.stbof.files.wtf.Colval;
import ue.edit.res.stbof.task.RemoveModelGraphicFilesTask;
import ue.edit.res.stbof.task.RestoreModelGraphicFilesTask;
import ue.edit.res.stbof.task.StbofCheckTask;
import ue.edit.res.stbof.task.StbofSaveTask;
import ue.exception.CorruptedFile;
import ue.exception.DataException;
import ue.gui.menu.StbofMenu;
import ue.gui.util.GuiTools;
import ue.gui.util.dialog.Dialogs;
import ue.service.FileManager;
import ue.service.Language;
import ue.util.data.IsCloneable;
import ue.util.file.ArchiveEntry;
import ue.util.file.FileFilters;
import ue.util.file.FileStore;
import ue.util.file.ZipStore;

/**
 * This class is a representation of the stbof.res file, it communicates with other classes in the
 * edit.res.stbof subpackages. it also sends MainPanel instances to use for editing the file.
 */
public class Stbof extends FileArchive {

  public static final int TYPE = FileManager.P_STBOF;

  // A hash table of all loaded files.
  private ConcurrentHashMap<String, InternalFile> INTERNAL_FILES = new ConcurrentHashMap<String, InternalFile>(CStbofFiles.CoreFiles.length);

  // List of files to be removed
  private HashSet<String> REMOVED = new HashSet<String>();

  // keep a cached list of the file entries, sorted by name
  // for the file list and the integrity checks
  private TreeMap<String, ArchiveEntry> ZIP_ENTRIES = new TreeMap<>();
  private ModList MODS;

  @Getter @Accessors(fluent = true) private StbofMenu menu = new StbofMenu();
  @Getter @Accessors(fluent = true) private StbofFileInterface files;

  // If set to false no changes will be saved to the modlist
  // This is an artifact of the past, still works, but there's no reason to set it
  // to false
  private boolean ADD_CHANGES_2_MODLIST = true;

  /**
   * This creates an instance of the class, but does not open a file. To open the file use the open
   * method. DON'T USE THIS CLASS WITHOUT OPENING A FILE!
   */
  public Stbof() {
    files = new StbofFileInterface(this);
  }

  @Override
  public int type() {
    return TYPE;
  }

  @Override
  public String getFileDescription(String fileName) {
    return CStbofFileDesc.getDescription(fileName);
  }

  @Override
  public Collection<InternalFile> listCachedFiles() {
    return Collections.unmodifiableCollection(INTERNAL_FILES.values());
  }

  @Override
  public Stream<InternalFile> streamCachedFiles() {
    return INTERNAL_FILES.values().stream();
  }

  /**
   * Get all known stbof.res file entries, sorted by name.
   */
  @Override
  public Map<String, ArchiveEntry> getArchiveEntries() {
    return Collections.unmodifiableMap(ZIP_ENTRIES);
  }

  /**
   * Get known stbof.res file entry by name.
   */
  @Override
  public ArchiveEntry getArchiveEntry(String fileName) {
    return ZIP_ENTRIES.get(fileName.toLowerCase());
  }

  @Override
  public void processCachedFiles(Predicate<DataFile> callback) {
    //GO THRU LOADED FILES
    for (val file : INTERNAL_FILES.values()) {
      if (!callback.test(file))
        break;
    }
  }

  /**
   * Process all cached stbof.res file entries matching the filter and predicate.
   * @param filter file name filter, can be null
   */
  @Override
  public void processCachedFiles(FilenameFilter filter, Predicate<DataFile> callback) {
    //GO THRU LOADED FILES
    for (val file : INTERNAL_FILES.values()) {
      if (filter != null && !filter.accept(sourceFile, file.getName()))
        continue;
      if (!callback.test(file))
        break;
    }
  }

  @Override
  public int getLoadedFileCount() {
    return INTERNAL_FILES.size();
  }

  @Override
  public int getRemovedFileCount() {
    return REMOVED.size();
  }

  /**
   * Returns an enumeration of loaded files.
   */
  @Override
  public Set<String> cachedFiles() {
    return INTERNAL_FILES.keySet();
  }

  /**
   * Returns a view on the files to be removed.
   */
  @Override
  public Set<String> removedFiles() {
    return Collections.unmodifiableSet(REMOVED);
  }

  /**
   * This adds a generic file to stbof.res. The file is saved in a hashset, therefore there can't be
   * two files with the same name added to the archive. Should a file with the same name already
   * been added before it will be overwritten. If the file is editable the one from internal files
   * is removed (if present).
   *
   * @param gf the GenericFile to be added.
   * @throws IOException
   */
  @Override
  public void addInternalFile(InternalFile gf) throws IOException {
    gf.markChanged();

    // botf can't handle files with larger file names
    if (gf.getName().length() > 12) {
      // 8 - name, 1 - dot, 3 - extension
      String msg = Language.getString("Stbof.30"); //$NON-NLS-1$
      msg = msg.replace("%1", gf.getName()); //$NON-NLS-1$
      throw new IOException(msg);
    }

    // for name lookup change to lower case
    String lcName = gf.getName().toLowerCase();

    // for generic files, try to reload with proper type
    if (gf instanceof GenericFile) {
      InternalFile a = createFile(Optional.empty(), gf.getName(), false);
      if (a != null) {
        a.load(gf.toByteArray());
        gf = a;
      }
    }

    // added files are considered changed, to make sure the save dialog pops up
    gf.markChanged();
    INTERNAL_FILES.put(lcName, gf);

    // remove from to be removed list in case it is listed there
    REMOVED.remove(lcName);
    markChanged();
  }

  /**
   * This removes the specified file from the generic files which were added. If the provided file
   * is not present in the list, no action is taken.
   *
   * @param fileName the name of the file to remove. Note: All files are identified by name as there
   *             can't be two files with the same name in stbof.res
   */
  @Override
  public void removeInternalFile(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    INTERNAL_FILES.remove(fileName);
    REMOVED.add(fileName);
    MODS.remove(fileName);
    markChanged();
  }

  /**
   * Call this function to discard changes.
   * @throws IOException
   */
  @Override
  public void discard() {
    super.discard();
    INTERNAL_FILES.clear();
    files.clear();
    REMOVED.clear();
  }

  /**
   * Call this function to discard single file changes.
   */
  @Override
  public void discard(String fileName) {
    super.discard(fileName);

    // for name lookup change to lower case
    fileName = fileName.toLowerCase();

    // remove from cache to restore unmodified data
    if (INTERNAL_FILES.remove(fileName) != null)
      files.clear(fileName);

    // clear removed state
    REMOVED.remove(fileName);
  }

  @Override
  public void discard(FilenameFilter filter) {
    if (filter == null) {
      discard();
      return;
    }

    INTERNAL_FILES.values().removeIf(x -> {
      String fileName = x.getName();
      if (filter.accept(sourceFile, fileName)) {
        super.discard(fileName);
        files.clear(fileName);
        return true;
      }
      return false;
    });

    REMOVED.removeIf(x -> {
      if (filter.accept(sourceFile, x)) {
        super.discard(x);
        return true;
      }
      return false;
    });
  }

  public void updateModList() {
    for (InternalFile file : INTERNAL_FILES.values()) {
      // for name lookup change to lower case
      String lcName = file.getName().toLowerCase();

      // skip removed files
      if (REMOVED.contains(lcName))
        continue;
      // skip mod.lst, that by itself is no game change
      if (file.getName().equals("mod.lst")) //$NON-NLS-1$
        continue;

      if (file.madeChanges())
        recordFileChange(file.getName());
    }
  }

  public void recordFileChange(String fileName) {
    if (ADD_CHANGES_2_MODLIST)
      MODS.add(fileName);
  }

  /**
   * Saves changes to the specified file.
   *
   * @param whereTo The file to which changes are to be saved. If the file already exists, it is
   *                overwritten without confirmation.
   * @return true if successful else false.
   */
  @Override
  public boolean save(File whereTo, boolean bkp, String ext) {
    // if not specified, save to the open file
    if (whereTo == null)
      whereTo = getTargetFile();

    val saveTask = new StbofSaveTask(this, whereTo, bkp, ext);
    boolean success = GuiTools.runUEWorker(saveTask);

    if (success) {
      // keep cached files
      super.discard();
      REMOVED.clear();
      sourceFile = whereTo;
    }

    return success;
  }

  /**
   * Opens the specified file and tests it, if it is a valid stbof.res archive.
   * Also it checks whether stbof contains all editable files.
   *
   * @param whereFrom The file to open. Must be a valid stbof.res file!
   * @throws IOException
   */
  @Override
  public void open(File whereFrom) throws IOException {
    loadStbof(whereFrom);
  }

  @Override
  public ZipStore access() throws IOException {
    return new ZipStore(sourceFile);
  }

  private void loadStbof(File whereFrom) throws IOException {
    sourceFile = null;
    ZIP_ENTRIES.clear();
    discard();

    try (ZipStore zf = new ZipStore(whereFrom)) {
      for (String element : CStbofFiles.CoreFiles) {
        if (!zf.contains(element)) {
          String msg = Language.getString("Stbof.32"); //$NON-NLS-1$
          msg = msg.replace("%1", element); //$NON-NLS-1$
          throw new CorruptedFile(msg);
        }
      }

      loadZipEntries(zf);
      sourceFile = whereFrom;
    }
  }

  private void loadZipEntries(ZipStore zf) {
    MODS = null;
    ZIP_ENTRIES.clear();

    // load mod list or create an empty one
    val modLst = zf.findEntry("mod.lst"); //$NON-NLS-1$
    if (modLst.isPresent()) {
      try (InputStream in = zf.getInputStream(modLst.get(), false)) {
        MODS = new ModList(in);
      } catch (Exception ex) {
        // notify on the error but fallback to an empty mod list
        Dialogs.displayError(new Exception(Language.getString("Stbof.33"))); //$NON-NLS-1$
        MODS = new ModList();
      }
    } else {
      MODS = new ModList();
    }

    // load entries
    zf.stream().forEach(ze -> {
      // for name lookup change to lower case
      // in unmodded vanilla stbof.res they however all are in lower case anyhow
      ZIP_ENTRIES.put(ze.getLcName(), ze);
    });
  }

  /**
   * Returns true if the ship graphics string is being used in stbof.res
   * @throws IOException
   * @throws ZipException
   */
  public boolean isShipGraphicsStringUsed(String graphicName) {
    if (hasInternalFile(graphicName + "_a.hob")) //$NON-NLS-1$
      return true;
    if (hasInternalFile(graphicName + "_b.hob")) //$NON-NLS-1$
      return true;
    if (hasInternalFile(graphicName + "_c.hob")) //$NON-NLS-1$
      return true;
    if (hasInternalFile("i_" + graphicName + "w.tga")) //$NON-NLS-1$ //$NON-NLS-2$
      return true;
    if (hasInternalFile("i_" + graphicName + "60.tga")) //$NON-NLS-1$ //$NON-NLS-2$
      return true;
    if (hasInternalFile("i_" + graphicName + "30.tga")) //$NON-NLS-1$ //$NON-NLS-2$
      return true;
    if (hasInternalFile("i_" + graphicName + "120.tga")) //$NON-NLS-1$ //$NON-NLS-2$
      return true;
    if (hasInternalFile("i_" + graphicName + "170.tga")) //$NON-NLS-1$ //$NON-NLS-2$
      return true;

    return false;
  }

  /**
   * @return unused ship graphics string
   * @throws IOException
   * @throws ZipException
   */
  public String getUnusedShipGraphicsString() {
    int i = 0;          // try numbers first
    char c = 'A';
    String g = Integer.toString(i++);

    while ((c <= 'Z') && (isShipGraphicsStringUsed(g) || isShipGraphicsStringUsed(g + "_")
        //$NON-NLS-1$
        || isShipGraphicsStringUsed(g + "__"))) //$NON-NLS-1$
    { // search from 0-9 and A to Z
      if (i < 10) {
        g = Integer.toString(i++);
      } else {
        g = Character.toString(c++);
      }
    }

    if (c > 'Z') {   // search from 00 to 99 and AA to ZZ
      i = 0;
      int j = 0;
      c = 'A';
      char d = 'A';

      g = Integer.toString(i) + Integer.toString(j);

      while ((c <= 'Z') && (isShipGraphicsStringUsed(g) || this
          .isShipGraphicsStringUsed(g + "_"))) //$NON-NLS-1$
      {
        if (i < 10) {
          g = Integer.toString(i) + Integer.toString(j++);

          if (j == 10) {
            j = 0;
            i++;
          }
        } else {
          g = Character.toString(c) + Character.toString(d++);

          if (d == 'Z') {
            d = 'A';
            c++;
          }
        }
      }
    }

    // return null if not found
    return c <= 'Z' ? g : null;
  }

  /**
   * Renames model graphic files and entries in shiplist.sst.
   *
   * @param old old graphics string
   * @param now new graphics string
   * @return true if successful
   * @throws Exception
   */
  public boolean renameModelGraphicFiles(String old, String now) throws Exception {
    ShipList SHIPS = (ShipList) getInternalFile(CStbofFiles.ShipListSst, true);

    // check
    if (isShipGraphicsStringUsed(now))
      return false;

    for (ShipDefinition shipDef : SHIPS.listShips()) {
      if (shipDef.getGraphics().toLowerCase().equals(old.toLowerCase())) {
        shipDef.setGraphics(now);
        break;
      }
    }

    InternalFile file;
    char c = 'a';

    while (c <= 'c') {
      try {
        String sfile = old + "_" + c + ".hob"; //$NON-NLS-1$ //$NON-NLS-2$
        file = getInternalFile(sfile, false);
        removeInternalFile(sfile);
        file.setName(now + "_" + c + ".hob"); //$NON-NLS-1$ //$NON-NLS-2$
        addInternalFile(file);
      } catch (FileNotFoundException fnf) {
      }
      c++;
    }

    try {
      String sfile = "i_" + old + "w.tga"; //$NON-NLS-1$ //$NON-NLS-2$
      file = getInternalFile(sfile, false);
      removeInternalFile(sfile);
      file.setName("i_" + now + "w.tga"); //$NON-NLS-1$ //$NON-NLS-2$
      addInternalFile(file);
    } catch (FileNotFoundException fnf) {
    }

    try {
      String sfile = "i_" + old + "30.tga"; //$NON-NLS-1$ //$NON-NLS-2$
      file = getInternalFile(sfile, false);
      removeInternalFile(sfile);
      file.setName("i_" + now + "30.tga"); //$NON-NLS-1$ //$NON-NLS-2$
      addInternalFile(file);
    } catch (FileNotFoundException fnf) {
    }

    try {
      String sfile = "i_" + old + "60.tga"; //$NON-NLS-1$ //$NON-NLS-2$
      file = getInternalFile(sfile, false);
      removeInternalFile(sfile);
      file.setName("i_" + now + "60.tga"); //$NON-NLS-1$ //$NON-NLS-2$
      addInternalFile(file);
    } catch (FileNotFoundException fnf) {
    }

    try {
      String sfile = "i_" + old + "120.tga"; //$NON-NLS-1$ //$NON-NLS-2$
      file = getInternalFile(sfile, false);
      removeInternalFile(sfile);
      file.setName("i_" + now + "120.tga"); //$NON-NLS-1$ //$NON-NLS-2$
      addInternalFile(file);
    } catch (FileNotFoundException fnf) {
    }

    try {
      String sfile = "i_" + old + "170.tga"; //$NON-NLS-1$ //$NON-NLS-2$
      file = getInternalFile(sfile, false);
      removeInternalFile(sfile);
      file.setName("i_" + now + "170.tga"); //$NON-NLS-1$ //$NON-NLS-2$
      addInternalFile(file);
    } catch (FileNotFoundException fnf) {
    }

    return true;
  }

  @Override
  public InternalFile getCachedFile(String fileName) {
    // for name lookup change to lower case
    return INTERNAL_FILES.get(fileName.toLowerCase());
  }

  @Override
  public void loadReadableFiles(Predicate<String> cbCancel) throws IOException {
    try (ZipStore zf = access()) {
      zf.processFiles((arc, entry) -> {
        String fileName = entry.getName();
        String lcName = entry.getLcName();

        // skip if already loaded or removed files
        if (INTERNAL_FILES.containsKey(lcName) || REMOVED.contains(lcName))
          return true;

        // stop if cancelled
        if (!cbCancel.test(fileName))
          return false;

        try (val lock = getFileLock(fileName)) {
          // create file and pass in the archive, to accelerate loading file dependencies
          InternalFile file = createFile(Optional.of(arc), fileName, true);

          try (InputStream in = zf.getInputStream(entry, false)) {
            file.load(in);
            INTERNAL_FILES.put(lcName, file);
            removeLoadError(fileName);
          } catch (IOException e) {
            addLoadError(fileName, e instanceof DataException ? e.getMessage() : e.toString());
          }
        }

        return true;
      });
    }
  }

  @Override
  protected Optional<InternalFile> loadFile(Optional<FileStore> arch, String fileName, int loadFlags) throws IOException {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();
    boolean reload = (loadFlags & LoadFlags.RELOAD) != 0;
    boolean copy = (loadFlags & LoadFlags.COPY) != 0;

    if (reload) {
      REMOVED.remove(lcName);
    } else if (REMOVED.contains(lcName)) {
      if ((loadFlags & LoadFlags.THROW) != 0)
        throwFileNotFound(fileName);
      return Optional.empty();
    }

    // if already loaded, return cached and possibly modified internal file
    InternalFile file = INTERNAL_FILES.get(lcName);
    if (file != null && !reload)
      return Optional.of(copy ? copyFile(arch, fileName, file) : file);

    boolean cache = (loadFlags & LoadFlags.CACHE) != 0;

    try {
      try (ZipStore zip = arch.isPresent() ? null : access()) {
        val zf = zip != null ? zip : arch.get();

        if (file != null) {
          // reuse same old file so any cached references remain valid
          // e.g. the task force and map helper tools otherwise would have to be reset
          file.clear();
          cache = false;
        } else {
          // create new file and pass in the archive, to accelerate loading file dependencies
          file = createFile(Optional.of(zf), fileName, true);
        }

        val ze = zf.getEntry(fileName, (loadFlags & LoadFlags.THROW) != 0);
        if (!ze.isPresent())
          return Optional.empty();

        try (InputStream in = zf.getInputStream(ze.get(), false)){
          file.load(in);
        }
      }

      if (cache)
        INTERNAL_FILES.put(lcName, file);
      if (copy)
        file = copyFile(arch, fileName, file);

      removeLoadError(fileName);
      return Optional.of(file);

    } catch (IOException e) {
      addLoadError(fileName, e instanceof DataException ? e.getMessage() : e.toString());
      throw e;
    }
  }

  private InternalFile copyFile(Optional<FileStore> arch, String fileName, InternalFile file) throws IOException {
    if (file instanceof IsCloneable)
      return (InternalFile) ((IsCloneable) file).clone();

    // fallback to copy the data
    InternalFile copy = createFile(arch, fileName, true);
    copy.load(file.toByteArray());
    return copy;
  }

  @Override
  public boolean isChunked(String fileName) {
    return false;
  }

  @Override
  public Class<?> getFileClass(String fileName) {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();

    switch (lcName) {
      case CStbofFiles.MoraleBin:     return Morale.class;
      case CStbofFiles.RaceRst:       return RaceRst.class;
      case CStbofFiles.EdifDescBst:
      case CStbofFiles.RaceDescRst:
      case CStbofFiles.ShipDescSst:
      case CStbofFiles.TechDescTec:   return StringFile.class;
      case CStbofFiles.AIMinorWtf:    return Aiminor.class;
      case CStbofFiles.ShipNameBin:   return Shipname.class;
      case CStbofFiles.StarNameBin:   return Starname.class;
      case CStbofFiles.TextureLst:    return Texture.class;
      case CStbofFiles.LexiconDic:    return Lexicon.class;
      case CStbofFiles.ObjStrucSmt:   return Objstruc.class;
      case CStbofFiles.EnvironEst:    return Environ.class;
      case CStbofFiles.PlanSpacBin:   return Planspac.class;
      case CStbofFiles.PlanBoniBin:   return Planboni.class;
      case CStbofFiles.PlanetPst:     return Planet.class;
      case CStbofFiles.MEPlanetBin:   return Meplanet.class;
      case CStbofFiles.TechTreeTec:   return TechTree.class;
      case CStbofFiles.TecFieldTec:   return TecField.class;
      case CStbofFiles.EdifBnftBst:   return Edifbnft.class;
      case CStbofFiles.EdificeBst:    return Edifice.class;
      case CStbofFiles.BldSet1Bin:
      case CStbofFiles.BldSet2Bin:
      case CStbofFiles.BldSet3Bin:
      case CStbofFiles.BldSet4Bin:
      case CStbofFiles.BldSet5Bin:    return BldSet.class;
      case CStbofFiles.RaceTechTec:   return RaceTech.class;
      case CStbofFiles.ShipListSst:   return ShipList.class;
      case CStbofFiles.ShipRaceSst:   return ShipRace.class;
      case CStbofFiles.ShipTechSst:   return ShipTech.class;
      case CStbofFiles.AIEmpireWtf:   return Aiempire.class;
      case CStbofFiles.PaletteLst:    return PaletteList.class;
      case CStbofFiles.CColValWtf:
      case CStbofFiles.HColValWtf:
      case CStbofFiles.FColValWtf:
      case CStbofFiles.KColValWtf:
      case CStbofFiles.RColValWtf:    return Colval.class;
      case CStbofFiles.AIBldReqBin:   return AIBldReq.class;
      case CStbofFiles.ConverseBin:   return Converse.class;
      case CStbofFiles.IntelBin:      return Intel.class;
      default:
        if (FileFilters.Tga.accept(sourceFile, lcName))
          return TargaImage.class;
        else if (FileFilters.Gif.accept(sourceFile, lcName))
          return GifImage.class;
        else if (FileFilters.Ani.accept(sourceFile, lcName) || FileFilters.Cur.accept(sourceFile, lcName))
          return AniOrCur.class;
        else if (FileFilters.Hob.accept(sourceFile, lcName))
          return HobFile.class;
        else if (FileFilters.Fnt.accept(sourceFile, lcName))
          return TgaFont.class;
        else if (FileFilters.Tdb.accept(sourceFile, lcName))
          return TextDataBase.class;
        else if (FileFilters.Wdf.accept(sourceFile, lcName))
          return WindowDefinitionFile.class;
        else
          return GenericFile.class;
    }
  }

  private InternalFile createFile(Optional<FileStore> arc, String fileName, boolean defaultToGeneric) throws IOException {
    // for name lookup change to lower case
    String lcName = fileName.toLowerCase();
    InternalFile file;

    switch (lcName) {
      case CStbofFiles.MoraleBin:
        file = new Morale(this);
        break;
      case CStbofFiles.RaceRst:
        file = new RaceRst(this);
        break;
      case CStbofFiles.RaceDescRst:
        file = new StringFile(this);
        break;
      case CStbofFiles.AIMinorWtf:
        file = new Aiminor(this, UE.FILES.trek());
        break;
      case CStbofFiles.ShipNameBin:
        file = new Shipname(this, UE.FILES.trek());
        break;
      case CStbofFiles.StarNameBin:
        file = new Starname(this);
        break;
      case CStbofFiles.TextureLst:
        file = new Texture();
        break;
      case CStbofFiles.LexiconDic:
        file = new Lexicon(this);
        break;
      case CStbofFiles.ObjStrucSmt:
        file = new Objstruc(this);
        break;
      case CStbofFiles.EnvironEst:
        file = new Environ(this);
        break;
      case CStbofFiles.PlanSpacBin:
        file = new Planspac();
        break;
      case CStbofFiles.PlanBoniBin:
        file = new Planboni();
        break;
      case CStbofFiles.PlanetPst:
        file = new Planet(this);
        break;
      case CStbofFiles.MEPlanetBin:
        file = new Meplanet(this);
        break;
      case CStbofFiles.TechTreeTec:
        file = new TechTree(this);
        break;
      case CStbofFiles.TechDescTec:
        file = new StringFile(this);
        break;
      case CStbofFiles.TecFieldTec:
        file = new TecField();
        break;
      case CStbofFiles.EdifBnftBst:
        file = new Edifbnft(this);
        break;
      case CStbofFiles.EdifDescBst:
        file = new StringFile(this);
        break;
      case CStbofFiles.EdificeBst:
        file = new Edifice(this, UE.FILES.trek());
        break;
      case CStbofFiles.BldSet1Bin:
      case CStbofFiles.BldSet2Bin:
      case CStbofFiles.BldSet3Bin:
      case CStbofFiles.BldSet4Bin:
      case CStbofFiles.BldSet5Bin:
        file = new BldSet(this);
        break;
      case CStbofFiles.RaceTechTec:
        file = new RaceTech(this);
        break;
      case CStbofFiles.ShipListSst:
        file = new ShipList(this);
        break;
      case CStbofFiles.ShipDescSst:
        file = new StringFile(this);
        break;
      case CStbofFiles.ShipRaceSst:
        file = new ShipRace(this);
        break;
      case CStbofFiles.ShipTechSst:
        file = new ShipTech(this);
        break;
      case CStbofFiles.AIEmpireWtf:
        file = new Aiempire(this);
        break;
      case CStbofFiles.PaletteLst:
        file = new PaletteList();
        break;
      case CStbofFiles.CColValWtf:
      case CStbofFiles.HColValWtf:
      case CStbofFiles.FColValWtf:
      case CStbofFiles.KColValWtf:
      case CStbofFiles.RColValWtf:
        file = new Colval(this);
        break;
      case CStbofFiles.AIBldReqBin:
        file = new AIBldReq(this);
        break;
      case CStbofFiles.ConverseBin:
        file = new Converse();
        break;
      case CStbofFiles.IntelBin:
        file = new Intel(this);
        break;
      default:
        if (FileFilters.Tga.accept(sourceFile, lcName))
          file = new TargaImage();
        else if (FileFilters.Gif.accept(sourceFile, lcName))
          file = new GifImage();
        else if (FileFilters.Ani.accept(sourceFile, lcName) || FileFilters.Cur.accept(sourceFile, lcName))
          file = new AniOrCur();
        else if (FileFilters.Hob.accept(sourceFile, lcName))
          file = new HobFile();
        else if (FileFilters.Fnt.accept(sourceFile, lcName))
          file = new TgaFont();
        else if (FileFilters.Tdb.accept(sourceFile, lcName))
          file = new TextDataBase();
        else if (FileFilters.Wdf.accept(sourceFile, lcName))
          file = new WindowDefinitionFile();
        else
          return defaultToGeneric ? new GenericFile(fileName) : null; //$NON-NLS-1$
    }

    file.setName(fileName);
    return file;
  }

  /**
   * Returns true if the internal file is cached.
   */
  @Override
  public boolean isCached(String fileName) {
    // for name lookup change to lower case
    return INTERNAL_FILES.containsKey(fileName.toLowerCase());
  }

  @Override
  public boolean isRemoved(String fileName) {
    return REMOVED.contains(fileName.toLowerCase());
  }

  /**
   * Returns true if internal file exists in the archive.
   */
  public boolean hasInternalFile(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();

    if (REMOVED.contains(fileName))
      return false;
    if (INTERNAL_FILES.containsKey(fileName))
      return true;

    return ZIP_ENTRIES.containsKey(fileName);
  }

  /**
   * Loads an animation represented by the AniOrCur class.
   *
   * @param name the name of the animation file to load and return.
   * @return an animation loaded from specified file, null if the animation can't be loaded.
   * @throws IOException
   */
  public AniOrCur getAnimation(String fileName, int loadFlags) throws IOException {
    val file = getInternalFile(fileName, loadFlags);
    if (file instanceof AniOrCur)
      return (AniOrCur) file;
    if (file instanceof GenericFile) {
      val ani = new AniOrCur(fileName);
      ani.load(((GenericFile)file).getData());
      return ani;
    }
    throw new InvalidClassException(file.getClass().getSimpleName(), "No Ani or Cur: " + fileName);
  }

  /**
   * Loads a wdf file represented by the WindowDefinitionFile object.
   *
   * @param name the name of the file to load and return.
   * @throws IOException
   */
  public WindowDefinitionFile getWDF(String fileName, int loadFlags) throws IOException {
    val file = getInternalFile(fileName, loadFlags);
    if (file instanceof WindowDefinitionFile)
      return (WindowDefinitionFile) file;
    if (file instanceof GenericFile)
      return new WindowDefinitionFile(fileName, ((GenericFile)file).getData());
    throw new InvalidClassException(file.getClass().getSimpleName(), "No WDF: " + fileName);
  }

  /**
   * Loads a font file represented by the TgaFont object.
   *
   * @param name the name of the file to load and return.
   * @throws IOException
   */
  public TgaFont getTgaFont(String fileName, int loadFlags) throws IOException {
    val file = getInternalFile(fileName, loadFlags);
    if (file instanceof TgaFont)
      return (TgaFont) file;
    if (file instanceof GenericFile)
      return new TgaFont(fileName, ((GenericFile)file).getData());
    throw new InvalidClassException(file.getClass().getSimpleName(), "No TGA font: " + fileName);
  }

  /**
   * Loads a tga image file from stbof.res using the TargaImage class.
   *
   * @param name  the name of the tga image to load and return
   * @param cache whether to cache the file
   * @return      the loaded TargaImage or null if failed
   * @throws IOException
   */
  public TargaImage getTargaImage(String fileName, int loadFlags) throws IOException {
    val file = getInternalFile(fileName, loadFlags);
    if (file instanceof TargaImage)
      return (TargaImage) file;
    if (file instanceof GenericFile)
      return new TargaImage(fileName, ((GenericFile)file).getData());
    throw new InvalidClassException(file.getClass().getSimpleName(), "No TGA: " + fileName);
  }

  /**
   * Loads a tga header, but skips all the image data.
   *
   * @param name  the name of the tga file to load
   * @return      a TargaHeader loaded from specified file
   * @throws IOException
   */
  public TargaHeader getTargaHeader(Optional<FileStore> arc, String fileName) throws IOException {
    try (val lock = getFileLock(fileName)) {
      return loadTargaHeader(arc, fileName, true).get();
    }
  }

  /**
   * Attempts to load a tga header, but skips all the image data.
   *
   * @param name  the name of the tga file to load
   * @return      a TargaHeader loaded from specified file, or empty if missing
   * @throws IOException
   */
  public Optional<TargaHeader> tryGetTargaHeader(Optional<FileStore> arc, String fileName) throws IOException {
    try (val lock = getFileLock(fileName)) {
      return loadTargaHeader(arc, fileName, false);
    }
  }

  private Optional<TargaHeader> loadTargaHeader(Optional<FileStore> arc, String fileName, boolean throwIfMissing) throws IOException {
    val file = getCachedFile(fileName);

    if (file != null) {
      if (file instanceof TargaImage) {
        return Optional.of(((TargaImage)file).getHeader());
      }
      else if (file instanceof GenericFile) {
        // reload as TGA
        val bais = new ByteArrayInputStream(file.toByteArray());
        return Optional.of(new TargaHeader(null, bais));
      }
      else {
        String msg = Language.getString("Stbof.43") //$NON-NLS-1$
          .replace("%1", fileName); //$NON-NLS-1$
        throw new InvalidClassException(msg);
      }
    }

    try (ZipStore store = arc.isPresent() ? null : access()) {
      FileStore zf = store != null ? store : arc.get();
      val ze = zf.getEntry(fileName, throwIfMissing);
      if (!ze.isPresent())
        return Optional.empty();

      try (InputStream in = zf.getInputStream(ze.get(), false)) {
        return Optional.of(new TargaHeader(null, in));
      }
    }
  }

  /**
   * Loads a gif image file from stbof.res using the GifImage class.
   *
   * @param name  the name of the gif image to load and return
   * @param cache whether to cache the file
   * @return      the loaded GifImage or null if failed
   * @throws IOException
   */
  public GifImage getGifImage(String fileName, int loadFlags) throws IOException {
    val file = getInternalFile(fileName, loadFlags);
    if (file instanceof GifImage)
      return (GifImage) file;
    if (file instanceof GenericFile)
      return new GifImage(fileName, ((GenericFile)file).getData());
    throw new InvalidClassException(file.getClass().getSimpleName(), "No GIF: " + fileName);
  }

  /**
   * Checks internal files for unusal data and other problems.
   *
   * @return a Vector containing the check results.
   */
  @Override
  public Vector<String> check() {
    StbofCheckTask checkTask = new StbofCheckTask(this);
    val response = Optional.ofNullable(GuiTools.runUEWorker(checkTask)).orElse(new Vector<>());

    if (checkTask.isCancelled())
      response.add(Language.getString("Stbof.55"));
    else if (response.isEmpty())
      response.add(Language.getString("Stbof.45"));

    return response;
  }

  private HashSet<String> hobGraphics(String hobName) {
    HashSet<String> fileNames = new HashSet<String>();
    collectHobTextures(hobName, fileNames);
    fileNames.addAll(Arrays.asList(HobFile.hobFiles(hobName)));
    return fileNames;
  }

  public Set<String> changedModelGraphicFiles() throws IOException {
    Texture texturelist = (Texture) getInternalFile(CStbofFiles.TextureLst, true);

    // check known model graphics
    Set<String> changed = modifiedFiles(FileFilters.ModelGraphics);

    // check cached hob textures
    Collection<String> hobfiles = cachedFiles(FileFilters.Hob);
    for (String hobfile : hobfiles) {
      HobFile hob = (HobFile) getInternalFile(hobfile, false);
      for (int j = 0; j < hob.numberOfTextures(); j++) {
        String t = texturelist.get(hob.getTextureID(j));
        if (isChanged(t))
          changed.add(t);
      }
    }

    return changed;
  }

  public HashSet<String> changedModelGraphicFiles(Set<String> hobNames) {
    HashSet<String> fileNames = new HashSet<String>();
    // first collect all graphics to not check them twice
    hobNames.forEach(x -> fileNames.addAll(hobGraphics(x)));
    fileNames.removeIf(x -> !isChanged(x));
    return fileNames;
  }

  public void collectHobTextures(String hobName, Set<String> fileNames) {
    val texturelist = files.findTextureLst();
    if (!texturelist.isPresent())
      return;

    // load hob file texture names
    for (String hobFile : HobFile.hobFiles(hobName)) { //$NON-NLS-1$ //$NON-NLS-2$
      HobFile hob = (HobFile) tryGetInternalFile(hobFile, false);
      if (hob != null) {
        for (int j = 0; j < hob.numberOfTextures(); j++) {
          String t = texturelist.get().get(hob.getTextureID(j));
          fileNames.add(t);
        }
      }
    }

    // include common model graphics
    fileNames.add("i_" + hobName + "w.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    fileNames.add("i_" + hobName + "30.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    fileNames.add("i_" + hobName + "60.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    fileNames.add("i_" + hobName + "120.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    fileNames.add("i_" + hobName + "170.tga"); //$NON-NLS-1$ //$NON-NLS-2$
  }

  /**
   * @param graphic name strings
   * @return true if succeeded, false if failed or cancelled
   * @throws IOException
   */
  public boolean removeModelGraphicFiles(String[] strings) throws IOException {
    return GuiTools.runUEWorker(new RemoveModelGraphicFilesTask(this, strings));
  }

  public boolean restoreModelGraphicFiles() throws IOException {
    RestoreModelGraphicFilesTask tsk = new RestoreModelGraphicFilesTask(this);
    return Boolean.TRUE.equals(GuiTools.runUEWorker(tsk));
  }

  private void throwFileNotFound(String fileName) throws FileNotFoundException {
    String msg = Language.getString("Stbof.44") //$NON-NLS-1$
      .replace("%1", fileName); //$NON-NLS-1$
    throw new FileNotFoundException(msg);
  }

  /**
   * (non-Javadoc)
   * @see ue.edit.common.CanBeEdited#modList()
   */
  @Override
  public ModList modList() {
    return MODS;
  }

  /*
   * (non-Javadoc)
   *
   * @see ue.edit.CanBeEdited#isModded()
   */
  @Override
  public boolean isModded() {
    return !MODS.isEmpty() || isChanged();
  }

  @Override
  public boolean isChanged(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();
    if (REMOVED.contains(fileName))
      return true;

    InternalFile file = INTERNAL_FILES.get(fileName);
    return file != null && file.madeChanges();
  }

}
