package ue.edit.res.stbof;

import java.io.IOException;
import java.util.Optional;
import ue.UE;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.AIBldReq;
import ue.edit.res.stbof.files.bin.BldSet;
import ue.edit.res.stbof.files.bin.Planboni;
import ue.edit.res.stbof.files.bin.Planspac;
import ue.edit.res.stbof.files.bin.Starname;
import ue.edit.res.stbof.files.bst.Edifbnft;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.lst.PaletteList;
import ue.edit.res.stbof.files.lst.Texture;
import ue.edit.res.stbof.files.pst.Planet;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.edit.res.stbof.files.sst.ShipTech;
import ue.edit.res.stbof.files.tec.RaceTech;
import ue.edit.res.stbof.files.tec.TecField;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.edit.res.stbof.tools.BuildingTools;
import ue.edit.res.stbof.tools.RaceTools;
import ue.edit.res.stbof.tools.StellarTools;

/**
 * This class is a cached interface to the stbof.res internal files.
 */
public class StbofFileInterface {

  private Stbof stbof;
  public BuildingTools buildingTools = null;
  public RaceTools raceTools = null;
  public StellarTools stellarTools = null;

  // common stbof files for better lookup
  public AIBldReq aiBldReq = null;
  public BldSet bldSet[] = new BldSet[5];
  public Edifbnft edifbnft = null;
  public Edifice edifice = null;
  public Environ environ = null;
  public Lexicon lexicon = null;
  public Objstruc objStruc = null;
  public PaletteList paletteLst = null;
  public Planboni planBoni = null;
  public Planspac planSpac = null;
  public Planet planetPst = null;
  public RaceRst raceRst = null;
  public RaceTech raceTech = null;
  public ShipList shipModels = null;
  public ShipTech shipTech = null;
  public Starname starName = null;
  public TecField tecfield = null;
  public TechTree techTree = null;
  public Texture textureLst = null;

  public StbofFileInterface(Stbof stbof) {
    this.stbof = stbof;
  }

  public BuildingTools buildingTools() {
    if (buildingTools == null)
      buildingTools = new BuildingTools(stbof, UE.FILES.trek());
    return buildingTools;
  }

  public RaceTools raceTools() {
    if (raceTools == null)
      raceTools = new RaceTools(stbof);
    return raceTools;
  }

  public StellarTools stellarTools() {
    if (stellarTools == null)
      stellarTools = new StellarTools(stbof, UE.FILES.trek());
    return stellarTools;
  }

  // #region Checked InternalFile getters

  public AIBldReq aiBldReq() throws IOException {
    if (aiBldReq == null)
      aiBldReq = (AIBldReq) stbof.getInternalFile(CStbofFiles.AIBldReqBin, true);
    return aiBldReq;
  }

  public BldSet bldSet(int k) throws IOException {
    BldSet.checkIndex(k);

    if (bldSet[k-1] == null) {
      String filename = "bldset" + k + ".bin";
      bldSet[k-1] = (BldSet) stbof.getInternalFile(filename, true);
    }

    return bldSet[k-1];
  }

  public Edifbnft edifbnft() throws IOException {
    if (edifbnft == null)
      edifbnft = (Edifbnft) stbof.getInternalFile(CStbofFiles.EdifBnftBst, true);
    return edifbnft;
  }

  public Edifice edifice() throws IOException {
    if (edifice == null)
      edifice = (Edifice) stbof.getInternalFile(CStbofFiles.EdificeBst, true);
    return edifice;
  }

  public Lexicon lexicon() throws IOException {
    if (lexicon == null)
      lexicon = (Lexicon) stbof.getInternalFile(CStbofFiles.LexiconDic, true);
    return lexicon;
  }

  public Objstruc objStruc() throws IOException {
    if (objStruc == null)
      objStruc = (Objstruc) stbof.getInternalFile(CStbofFiles.ObjStrucSmt, true);
    return objStruc;
  }

  public Environ environ() throws IOException {
    if (environ == null)
      environ = (Environ) stbof.getInternalFile(CStbofFiles.EnvironEst, true);
    return environ;
  }

  public PaletteList paletteLst() throws IOException {
    if (paletteLst == null)
      paletteLst = (PaletteList) stbof.getInternalFile(CStbofFiles.PaletteLst, true);
    return paletteLst;
  }

  public Planboni planBoni() throws IOException {
    if (planBoni == null)
      planBoni = (Planboni) stbof.getInternalFile(CStbofFiles.PlanBoniBin, true);
    return planBoni;
  }

  public Planspac planSpac() throws IOException {
    if (planSpac == null)
      planSpac = (Planspac) stbof.getInternalFile(CStbofFiles.PlanSpacBin, true);
    return planSpac;
  }

  public Planet planetPst() throws IOException {
    if (planetPst == null)
      planetPst = (Planet) stbof.getInternalFile(CStbofFiles.PlanetPst, true);
    return planetPst;
  }

  public RaceRst raceRst() throws IOException {
    if (raceRst == null)
      raceRst = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
    return raceRst;
  }

  public RaceTech raceTech() throws IOException {
    if (raceTech == null)
      raceTech = (RaceTech) stbof.getInternalFile(CStbofFiles.RaceTechTec, true);
    return raceTech;
  }

  public ShipList shipList() throws IOException {
    if (shipModels == null)
      shipModels = (ShipList) stbof.getInternalFile(CStbofFiles.ShipListSst, true);
    return shipModels;
  }

  public ShipTech shipTech() throws IOException {
    if (shipTech == null)
      shipTech = (ShipTech) stbof.getInternalFile(CStbofFiles.ShipTechSst, true);
    return shipTech;
  }

  public Starname starName() throws IOException {
    if (starName == null)
      starName = (Starname) stbof.getInternalFile(CStbofFiles.StarNameBin, true);
    return starName;
  }

  public TecField tecfield() throws IOException {
    if (tecfield == null)
      tecfield = (TecField) stbof.getInternalFile(CStbofFiles.TecFieldTec, true);
    return tecfield;
  }

  public TechTree techTree() throws IOException {
    if (techTree == null)
      techTree = (TechTree) stbof.getInternalFile(CStbofFiles.TechTreeTec, true);
    return techTree;
  }

  public Texture textureLst() throws IOException {
    if (textureLst == null)
      textureLst = (Texture) stbof.getInternalFile(CStbofFiles.TextureLst, true);
    return textureLst;
  }

  // #endregion Checked InternalFile getters

  // #region Unchecked InternalFile lookup

  public Optional<AIBldReq> findAiBldReq() {
    if (aiBldReq == null)
      aiBldReq = (AIBldReq) stbof.tryGetInternalFile(CStbofFiles.AIBldReqBin, true);
    return Optional.ofNullable(aiBldReq);
  }

  public Optional<BldSet> findBldSet(int k) {
    BldSet.checkIndex(k);

    if (bldSet[k-1] == null) {
      String filename = "bldset" + k + ".bin";
      bldSet[k-1] = (BldSet) stbof.tryGetInternalFile(filename, true);
    }

    return Optional.ofNullable(bldSet[k-1]);
  }

  public Optional<Edifbnft> findEdifbnft() {
    if (edifbnft == null)
      edifbnft = (Edifbnft) stbof.tryGetInternalFile(CStbofFiles.EdifBnftBst, true);
    return Optional.ofNullable(edifbnft);
  }

  public Optional<Edifice> findEdifice() {
    if (edifice == null)
      edifice = (Edifice) stbof.tryGetInternalFile(CStbofFiles.EdificeBst, true);
    return Optional.ofNullable(edifice);
  }

  public Optional<Lexicon> findLexicon() {
    if (lexicon == null)
      lexicon = (Lexicon) stbof.tryGetInternalFile(CStbofFiles.LexiconDic, true);
    return Optional.ofNullable(lexicon);
  }

  public Optional<Objstruc> findObjStruc() {
    if (objStruc == null)
      objStruc = (Objstruc) stbof.tryGetInternalFile(CStbofFiles.ObjStrucSmt, true);
    return Optional.ofNullable(objStruc);
  }

  public Optional<Environ> findEnviron() {
    if (environ == null)
      environ = (Environ) stbof.tryGetInternalFile(CStbofFiles.EnvironEst, true);
    return Optional.ofNullable(environ);
  }

  public Optional<PaletteList> findPaletteLst() {
    if (paletteLst == null)
      paletteLst = (PaletteList) stbof.tryGetInternalFile(CStbofFiles.PaletteLst, true);
    return Optional.ofNullable(paletteLst);
  }

  public Optional<Planboni> findPlanBoni() {
    if (planBoni == null)
      planBoni = (Planboni) stbof.tryGetInternalFile(CStbofFiles.PlanBoniBin, true);
    return Optional.ofNullable(planBoni);
  }

  public Optional<Planspac> findPlanSpac() {
    if (planSpac == null)
      planSpac = (Planspac) stbof.tryGetInternalFile(CStbofFiles.PlanSpacBin, true);
    return Optional.ofNullable(planSpac);
  }

  public Optional<Planet> findPlanetPst() {
    if (planetPst == null)
      planetPst = (Planet) stbof.tryGetInternalFile(CStbofFiles.PlanetPst, true);
    return Optional.ofNullable(planetPst);
  }

  public Optional<RaceRst> findRaceRst() {
    if (raceRst == null)
      raceRst = (RaceRst) stbof.tryGetInternalFile(CStbofFiles.RaceRst, true);
    return Optional.ofNullable(raceRst);
  }

  public Optional<RaceTech> findRaceTech() {
    if (raceTech == null)
      raceTech = (RaceTech) stbof.tryGetInternalFile(CStbofFiles.RaceTechTec, true);
    return Optional.ofNullable(raceTech);
  }

  public Optional<ShipList> findShipList() {
    if (shipModels == null)
      shipModels = (ShipList) stbof.tryGetInternalFile(CStbofFiles.ShipListSst, true);
    return Optional.ofNullable(shipModels);
  }

  public Optional<ShipTech> findShipTech() {
    if (shipTech == null)
      shipTech = (ShipTech) stbof.tryGetInternalFile(CStbofFiles.ShipTechSst, true);
    return Optional.ofNullable(shipTech);
  }

  public Optional<TecField> findTecfield() {
    if (tecfield == null)
      tecfield = (TecField) stbof.tryGetInternalFile(CStbofFiles.TecFieldTec, true);
    return Optional.ofNullable(tecfield);
  }

  public Optional<TechTree> findTechTree() {
    if (techTree == null)
      techTree = (TechTree) stbof.tryGetInternalFile(CStbofFiles.TechTreeTec, true);
    return Optional.ofNullable(techTree);
  }

  public Optional<Texture> findTextureLst() {
    if (textureLst == null)
      textureLst = (Texture) stbof.tryGetInternalFile(CStbofFiles.TextureLst, true);
    return Optional.ofNullable(textureLst);
  }

  // #endregion Unchecked InternalFile lookup

  // #region Cleanup

  /**
   * Call this function to clear all cached files.
   */
  public void clear() {
    aiBldReq = null;
    for (int i = 0; i < bldSet.length; ++i)
      bldSet[i] = null;
    edifbnft = null;
    edifice = null;
    environ = null;
    lexicon = null;
    objStruc = null;
    paletteLst = null;
    planBoni = null;
    planSpac = null;
    planetPst = null;
    raceRst = null;
    raceTech = null;
    shipModels = null;
    shipTech = null;
    starName = null;
    tecfield = null;
    techTree = null;
    textureLst = null;
  }

  /**
   * Call this function to clear single files.
   */
  public void clear(String fileName) {
    // for name lookup change to lower case
    fileName = fileName.toLowerCase();

    switch (fileName) {
      case CStbofFiles.AIBldReqBin:
        aiBldReq = null;
        break;
      case CStbofFiles.BldSet1Bin:
        bldSet[0] = null;
        break;
      case CStbofFiles.BldSet2Bin:
        bldSet[1] = null;
        break;
      case CStbofFiles.BldSet3Bin:
        bldSet[2] = null;
        break;
      case CStbofFiles.BldSet4Bin:
        bldSet[3] = null;
        break;
      case CStbofFiles.BldSet5Bin:
        bldSet[4] = null;
        break;
      case CStbofFiles.EdifBnftBst:
        edifbnft = null;
        break;
      case CStbofFiles.EdificeBst:
        edifice = null;
        break;
      case CStbofFiles.EnvironEst:
        environ = null;
        break;
      case CStbofFiles.LexiconDic:
        lexicon = null;
        break;
      case CStbofFiles.ObjStrucSmt:
        objStruc = null;
        break;
      case CStbofFiles.PaletteLst:
        paletteLst = null;
        break;
      case CStbofFiles.PlanBoniBin:
        planBoni = null;
        break;
      case CStbofFiles.PlanSpacBin:
        planSpac = null;
        break;
      case CStbofFiles.PlanetPst:
        planetPst = null;
        break;
      case CStbofFiles.RaceRst:
        raceRst = null;
        break;
      case CStbofFiles.RaceTechTec:
        raceTech = null;
        break;
      case CStbofFiles.ShipListSst:
        shipModels = null;
        break;
      case CStbofFiles.ShipTechSst:
        shipTech = null;
        break;
      case CStbofFiles.StarNameBin:
        starName = null;
        break;
      case CStbofFiles.TecFieldTec:
        tecfield = null;
        break;
      case CStbofFiles.TechTreeTec:
        techTree = null;
        break;
      case CStbofFiles.TextureLst:
        textureLst = null;
        break;
    }
  }

  // #endregion Cleanup
}
