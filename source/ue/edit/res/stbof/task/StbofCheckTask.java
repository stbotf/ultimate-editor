package ue.edit.res.stbof.task;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;
import java.util.TreeMap;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.FileStore;

public class StbofCheckTask extends UEWorker<Vector<String>> {

  private Stbof stbof;
  private File stbofFile;
  private TreeMap<String, InternalFile> filesToCheck;

  public StbofCheckTask(Stbof stbof) {
    this.stbof = stbof;
    stbofFile = stbof.getSourceFile();
    RESULT = new Vector<>();

    // collect files to check for async processing
    filesToCheck = stbof.listCachedFiles().stream().collect(Collectors.toMap(
      InternalFile::getName, Function.identity(), (x,y) -> y, TreeMap::new));
  }

  @Override
  public Vector<String> work() {
    try {
      // clear load errors
      stbof.clearLoadErrors();

      // load core files
      val coreFiles = stbof.tryGetInternalFiles(Arrays.asList(CStbofFiles.CoreFiles), true, Optional.of(x -> {
        feedMessage("Loading " + x + "...");
        return !isCancelled();
      }));

      for (val file : coreFiles) {
        filesToCheck.put(file.getName(), file);
      }

      // report load errors
      for (val err : stbof.listLoadErrors().entrySet()) {
        String msg = Language.getString("StbofCheckTask.1"); //$NON-NLS-1$
        msg = msg.replace("%1", err.getKey()); //$NON-NLS-1$
        msg = msg.replace("%2", err.getValue()); //$NON-NLS-1$
        RESULT.add(msg);
      }

      // load archive only once
      try (FileStore arc = stbof.access()) {
        checkFiles(arc);
      }
    } catch (Exception e) {
      String msg = Language.getString("StbofCheckTask.3"); //$NON-NLS-1$
      msg = msg.replace("%1", stbofFile.getName()); //$NON-NLS-1$
      msg = msg.replace("%2", e.getMessage()); //$NON-NLS-1$
      RESULT.add(msg);
      e.printStackTrace();
    }

    return RESULT;
  }

  private void checkFiles(FileStore arc) {
    String selfCheckMsg = Language.getString("StbofCheckTask.2"); //$NON-NLS-1$

    for (InternalFile test : filesToCheck.values()) {
      if (isCancelled())
        break;

      try {
        feedMessage(selfCheckMsg.replace("%1", test.getName())); //$NON-NLS-1$
        Vector<String> temp = test.check();
        if (temp != null)
          RESULT.addAll(temp);

      } catch (Exception f) {
        selfCheckMsg = Language.getString("StbofCheckTask.3"); //$NON-NLS-1$
        if (test != null) {
          selfCheckMsg = selfCheckMsg.replace("%1", test.getName()); //$NON-NLS-1$
        } else {
          selfCheckMsg = selfCheckMsg.replace("%1", stbofFile.getName()); //$NON-NLS-1$
        }

        selfCheckMsg = selfCheckMsg.replace("%2", f.getMessage()); //$NON-NLS-1$
        RESULT.add(selfCheckMsg);
        f.printStackTrace();
      }
    }
  }

}
