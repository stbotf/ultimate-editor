package ue.edit.res.stbof.task;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.hob.HobFile;
import ue.edit.res.stbof.files.lst.Texture;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.FileFilters;

public class RestoreModelGraphicFilesTask extends UEWorker<Boolean> {

  private final String restoreFileMsg = Language.getString("RestoreModelGraphicFilesTask.0"); //$NON-NLS-1$
  private final String restoreHobTexturesMsg = Language.getString("RestoreModelGraphicFilesTask.1"); //$NON-NLS-1$

  private Stbof stbof;
  private Texture texturelist;

  private List<InternalFile> cached;
  private Set<String> removed;

  public RestoreModelGraphicFilesTask(Stbof stbof) throws IOException {
    this.stbof = stbof;
    texturelist = (Texture) stbof.getInternalFile(CStbofFiles.TextureLst, true);
    RESULT = false;

    // collect work for async processing
    cached = stbof.streamCachedFiles()
      .filter(x -> FileFilters.isModelGraphic(x.getName()))
      .collect(Collectors.toList());
    removed = stbof.removedFiles(FileFilters.ModelGraphics);
  }

  @Override
  protected Boolean work() throws IOException {
    restoreModelGraphics();
    return !isCancelled();
  }

  private void restoreModelGraphics() throws IOException {
    // discard cached known model graphics
    for (InternalFile file : cached) {
      String fileName = file.getName();

      // notify
      feedMessage(restoreFileMsg.replace("%1", fileName)); //$NON-NLS-1$

      // for hob files also discard all the textures
      if (file instanceof HobFile) {
        try {
          HobFile hob = (HobFile) file;
          for (int j = 0; j < hob.numberOfTextures(); j++)
            stbof.discard(texturelist.get(hob.getTextureID(j)));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      stbof.discard(fileName);
    }

    // restore removed known model graphics
    removed.forEach(x -> stbof.discard(x));

    if (isCancelled())
      return;

    // restore removed hob textures
    for (String fileName : removed) {
      if (!FileFilters.Hob.accept(null, fileName))
        continue;
      if (isCancelled())
        return;

      // notify
      feedMessage(restoreHobTexturesMsg.replace("%1", fileName)); //$NON-NLS-1$

      // to restore all removed hob textures,
      // first the hob needs to be reloaded
      try {
        HobFile hob = (HobFile) stbof.getInternalFile(fileName, false);
        for (int j = 0; j < hob.numberOfTextures(); j++)
          stbof.discard(texturelist.get(hob.getTextureID(j)));
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
