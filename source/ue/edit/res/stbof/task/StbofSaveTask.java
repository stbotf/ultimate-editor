package ue.edit.res.stbof.task;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.ZipHelper;
import ue.util.file.ZipStore;

public class StbofSaveTask extends UEWorker<Boolean> {

  // used as a temporary file during saving
  private static final String TmpFileName = "stbof.tmp"; //$NON-NLS-1$

  private final Stbof stbof;
  private final File sourceFile;
  private final File destFile;
  private final boolean backup;
  private final String ext;

  private List<? extends InternalFile> filesChanged;
  private Set<String> removed;
  private File outFile;

  public StbofSaveTask(Stbof stbof, File destination, boolean backup, String extension) {
    this.stbof = stbof;
    this.backup = backup;
    this.ext = extension;
    sourceFile = stbof.getSourceFile();
    outFile = destFile = destination;

    // collect work for async processing
    // this way the iteration doesn't become undefined when additional files are loaded during save
    filesChanged = stbof.listChangedFiles();
    removed = stbof.removedFiles();
    RESULT = false;
  }

  @Override
  public Boolean work() throws IOException {
    // create temporary file and backup if destination already exists
    checkDestination();

    if (isCancelled()) {
      // delete temp file created by getWritableLocalFile
      outFile.delete();
      return false;
    }

    // write and zip all the data
    try (
      // open source zip that got loaded by UE
      ZipStore source = new ZipStore(sourceFile);
      // create output stream with new empty file
      ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(outFile));
    ) {
      // write zip, collecting the written Stbof file names
      if (!writeZipOutputStream(zos, source)) {
        // delete half written file if cancelled
        outFile.delete();
        return false;
      }
    } catch (Exception e) {
      outFile.delete();
      throw e;
    }

    // last chance to cancel action
    if (isCancelled()) {
      // delete half written file if cancelled
      outFile.delete();
      return false;
    }

    // discard removed files
    for (String fileName : removed)
      stbof.discard(fileName);

    // replace destination by temporary file
    applyTempFile();

    // on success mark all files saved
    for (InternalFile file : filesChanged)
      file.markSaved();

    return true;
  }

  private void checkDestination() throws IOException {
    if (!destFile.exists())
      return;

    // create backup
    if (backup) {
      sendMessage(Language.getString("StbofSaveTask.0")); //$NON-NLS-1$

      File file = new File(destFile.getPath() + ext);
      int i = 1;

      while (file.exists()) {
        file = new File(destFile.getPath() + i + ext);
        i++;
      }

      Files.copy(destFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    String msg = Language.getString("StbofSaveTask.1"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", TmpFileName)); //$NON-NLS-1$
    outFile = FileManager.getWritableLocalFile(TmpFileName);
  }

  private boolean writeZipOutputStream(ZipOutputStream zos, ZipStore source)
    throws IOException {

    // collect already processed files
    HashSet<String> processed = new HashSet<>();

    // first write and collect all cached files
    if (!writeCachedFiles(zos, processed))
      return false;

    // copy all source entries not edited by UE
    if (!copyMissingEntries(zos, source, processed))
      return false;

    // Saving file...
    String msg = Language.getString("StbofSaveTask.2"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", "mod.lst")); //$NON-NLS-1$ //$NON-NLS-2$

    saveZipOutputStream(zos);
    return !isCancelled();
  }

  private boolean copyMissingEntries(ZipOutputStream zos, ZipStore source, Set<String> processed) throws IOException {
    return source.processFiles((arc, ze) -> {
      if (isCancelled())
        return false;

      // for name lookup change to lower case
      String lcName = ze.getLcName();
      if (lcName.equals("mod.lst") || removed.contains(lcName)) //$NON-NLS-1$
        return true;

      // check if not already written
      if (!processed.contains(lcName)) {
        String fileName = ze.getName();
        String msg = Language.getString("StbofSaveTask.3"); //$NON-NLS-1$
        feedMessage(msg.replace("%1", fileName)); //$NON-NLS-1$

        // copy
        ZipHelper.copySourceFile(zos, source, ze);
        zos.flush();
      }

      // Complete the entry
      zos.closeEntry();
      return true;
    });
  }

  private boolean writeCachedFiles(ZipOutputStream zos, Set<String> processed)
    throws IOException {
    String msg = Language.getString("StbofSaveTask.2"); //$NON-NLS-1$

    for (InternalFile gef : filesChanged) {
      if (isCancelled())
        return false;

      String fileName = gef.getName();
      // for name lookup change to lower case
      String lcName = fileName.toLowerCase();

      // this should be redundant, but just in case...
      if (lcName.equals("mod.lst") || removed.contains(lcName)) //$NON-NLS-1$
        continue;

      feedMessage(msg.replace("%1", fileName)); //$NON-NLS-1$

      if (gef.madeChanges())
        stbof.recordFileChange(fileName);

      ZipEntry zeg = new ZipEntry(lcName);
      zeg.setCompressedSize(-1);
      zeg.setMethod(8);
      zeg.setSize(gef.getSize());
      zos.putNextEntry(zeg);

      gef.save(zos);
      processed.add(lcName);
    }

    return true;
  }

  private void saveZipOutputStream(ZipOutputStream zos) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    stbof.modList().save(baos);
    byte[] bub = baos.toByteArray();
    int sizi = bub.length;

    ZipEntry zeg = new ZipEntry("mod.lst"); //$NON-NLS-1$
    zeg.setCompressedSize(-1);
    zeg.setMethod(8);
    zeg.setSize(sizi);
    zos.putNextEntry(zeg);

    zos.write(bub, 0, sizi);
    zos.flush();
    zos.closeEntry();
  }

  private void applyTempFile() throws IOException {
    if (outFile == destFile)
      return;

    String msg = Language.getString("StbofSaveTask.3"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

    // replace with TMP file
    // on error keep temp file for manual rename
    Files.copy(outFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

    msg = Language.getString("StbofSaveTask.5"); //$NON-NLS-1$
    sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

    // attempt to delete the temporary file
    boolean deleted = outFile.delete();
    if (!deleted) {
      // the save operation succeeded, just the temp file couldn't be removed
      msg = Language.getString("StbofSaveTask.6"); //$NON-NLS-1$
      logError(msg.replace("%1", outFile.getName())); //$NON-NLS-1$
    }
  }

}
