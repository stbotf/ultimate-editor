package ue.edit.res.stbof.task;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.hob.HobFile;
import ue.edit.res.stbof.files.lst.Texture;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.FileFilters;

public class RemoveModelGraphicFilesTask extends UEWorker<Boolean> {

  private final String removeMsg = Language.getString("RemoveModelGraphicFilesTask.1"); //$NON-NLS-1$

  private Stbof stbof;
  private Texture texturelist;
  private String[] hobNames;

  public RemoveModelGraphicFilesTask(Stbof stbof, String[] hobNames) throws IOException {
    this.stbof = stbof;
    texturelist = (Texture) stbof.getInternalFile(CStbofFiles.TextureLst, true);
    this.hobNames = hobNames;
    RESULT = false;
  }

  @Override
  protected Boolean work() throws IOException {

    HashSet<String> textures = new HashSet<String>();
    for (String hobName : hobNames) {
      if (isCancelled())
        return false;

      stbof.collectHobTextures(hobName, textures);
      removeHobs(hobName);
      if (isCancelled())
        return false;
    }

    // keep used hob file textures
    excludeUsedHobTextures(textures);
    if (isCancelled())
      return false;

    // remove unused textures
    removeTextures(textures);
    return !isCancelled();
  }

  private void excludeUsedHobTextures(HashSet<String> textures) throws IOException {
    // test and remove textures
    Collection<String> hobfiles = stbof.getFileNames(FileFilters.Hob);
    sendMessage(Language.getString("RemoveModelGraphicFilesTask.0")); //$NON-NLS-1$

    for (String hobfile : hobfiles) {
      if (textures.isEmpty())
        break;

      if (isCancelled())
        break;

      HobFile hob = (HobFile) stbof.getInternalFile(hobfile, false);
      for (int j = 0; j < hob.numberOfTextures(); j++) {
        String t = texturelist.get(hob.getTextureID(j));
        textures.remove(t);
      }
    }
  }

  private void removeTextures(HashSet<String> textures) {
    for (String t : textures) {
      if (isCancelled())
        return;

      // notify
      feedMessage(removeMsg.replace("%1", t)); //$NON-NLS-1$

      // remove
      stbof.removeInternalFile(t);
      int index = texturelist.getTextureIndex(t);
      if (index >= 0) {
        texturelist.remove(index);
      }
    }
  }

  private void removeHobs(String hobName) {
    for (String hobFile : HobFile.hobFiles(hobName)) {
      if (isCancelled())
        return;

      // notify
      feedMessage(removeMsg.replace("%1", hobFile)); //$NON-NLS-1$

      stbof.removeInternalFile(hobFile);
    }
  }
}
