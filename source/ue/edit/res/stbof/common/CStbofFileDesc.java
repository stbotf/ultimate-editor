package ue.edit.res.stbof.common;

import ue.service.Language;
import ue.util.file.FileFilters;

/**
 * This interface holds common stbof file description constants.
 */
public interface CStbofFileDesc {

  public static String getDescription(String fileName) {
    String lcName = fileName.toLowerCase();

    // LIST OF INTERNAL FILE DESCRIPTIONS
    // these must not be statically loaded for language change
    switch (lcName) {
      case CStbofFiles.ConverseBin:
        return Language.getString("CStbofFileDesc.9"); //$NON-NLS-1$
      case CStbofFiles.EdificeBst:
        return Language.getString("CStbofFileDesc.1"); //$NON-NLS-1$
      case CStbofFiles.EnvironEst:
        return Language.getString("CStbofFileDesc.6"); //$NON-NLS-1$
      case CStbofFiles.LexiconDic:
        return Language.getString("CStbofFileDesc.2"); //$NON-NLS-1$
      case CStbofFiles.MEPlanetBin:
        return Language.getString("CStbofFileDesc.8"); //$NON-NLS-1$
      case CStbofFiles.ObjStrucSmt:
        return Language.getString("CStbofFileDesc.7"); //$NON-NLS-1$
      case CStbofFiles.PlanBoniBin:
        return Language.getString("CStbofFileDesc.14"); //$NON-NLS-1$
      case CStbofFiles.PlanetPst:
        return Language.getString("CStbofFileDesc.15"); //$NON-NLS-1$
      case CStbofFiles.PlanSpacBin:
        return Language.getString("CStbofFileDesc.16"); //$NON-NLS-1$
      case CStbofFiles.RaceRst:
        return Language.getString("CStbofFileDesc.3"); //$NON-NLS-1$
      case CStbofFiles.RaceTechTec:
        return Language.getString("CStbofFileDesc.17"); //$NON-NLS-1$
      case CStbofFiles.ShipListSst:
        return Language.getString("CStbofFileDesc.13"); //$NON-NLS-1$
      case CStbofFiles.ShipNameBin:
        return Language.getString("CStbofFileDesc.12"); //$NON-NLS-1$
      case CStbofFiles.StarNameBin:
        return Language.getString("CStbofFileDesc.11"); //$NON-NLS-1$
      case CStbofFiles.TecFieldTec:
        return Language.getString("CStbofFileDesc.4"); //$NON-NLS-1$
      case CStbofFiles.TechTreeTec:
        return Language.getString("CStbofFileDesc.5"); //$NON-NLS-1$
      case CStbofFiles.TextureLst:
        return Language.getString("CStbofFileDesc.10"); //$NON-NLS-1$
    }

    if (FileFilters.Tga.accept(null, lcName)) {
      return Language.getString("CStbofFileDesc.18"); //$NON-NLS-1$
    } else if (FileFilters.Gif.accept(null, lcName)) {
      return Language.getString("CStbofFileDesc.20"); //$NON-NLS-1$
    } else if (FileFilters.Tdb.accept(null, lcName)) {
      return Language.getString("CStbofFileDesc.19"); //$NON-NLS-1$
    } else if (FileFilters.Wdf.accept(null, lcName)) {
      return Language.getString("CStbofFileDesc.21"); //$NON-NLS-1$
    } else if (lcName.startsWith(CStbofFiles.BldSetPrefix)) {
      return Language.getString("CStbofFileDesc.0"); //$NON-NLS-1$
    }

    return null;
  }
}
