package ue.edit.res.stbof.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import javax.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;
import ue.common.CommonStrings;
import ue.edit.common.InternalFile;
import ue.edit.exe.seg.prim.StringSegment;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_FileNames;
import ue.edit.exe.trek.sd.StringDefinition;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.tga.TargaHeader;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget.WDFWidgetType;
import ue.edit.res.stbof.files.wdf.widgets.WDFWindow;
import ue.service.Language;
import ue.util.file.FileStore;
import ue.util.func.FunctionTools.CheckedFunction;

/**
 * The WDF image requirements class lists stbof.res TGA image constraints
 * of the window definition files.
 */
 @Data @AllArgsConstructor
 public class WDFImgReq {
   private String gfxKey;
   private String imgName;
   private String wdfName;
   private int widgetId;
   private int width;
   private int height;
   private boolean mustMatch;

   public String tgaName(String gfx) {
     return imgName.replace("%s", gfx.toLowerCase());
   }

  // see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=57619#p57619
  public static List<WDFImgReq> listEdificeBaseReqs(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    ArrayList<WDFImgReq> imgReqs = new ArrayList<>();
    listEdificeBaseReqs(trek, stbof, imgReqs, errors);
    return imgReqs;
  }

  public static List<WDFImgReq> listShipBaseReqs(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    ArrayList<WDFImgReq> imgReqs = new ArrayList<>();
    listShipBaseReqs(trek, stbof, imgReqs, errors);
    return imgReqs;
  }

  public static List<WDFImgReq> listShipBuildReqs(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    ArrayList<WDFImgReq> imgReqs = new ArrayList<>();
    listShipBuildReqs(trek, stbof, imgReqs, errors);
    return imgReqs;
  }

  public static List<WDFImgReq> listShipRedeployReqs(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    ArrayList<WDFImgReq> imgReqs = new ArrayList<>();
    listShipRedeployReqs(trek, stbof, imgReqs, errors);
    return imgReqs;
  }

  public static List<WDFImgReq> listStarbaseReqs(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    ArrayList<WDFImgReq> imgReqs = new ArrayList<>();
    listStarbaseReqs(trek, stbof, imgReqs, errors);
    return imgReqs;
  }

  public static List<WDFImgReq> listTechReqs(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    ArrayList<WDFImgReq> imgReqs = new ArrayList<>();
    listTechReqs(trek, stbof, imgReqs, errors);
    return imgReqs;
  }

  public static void listEdificeBaseReqs(Trek trek, Stbof stbof, List<WDFImgReq> imgReqs, @Nullable Collection<String> errors)
      throws IOException {
    // queued build task icon
    addImgReq(trek, stbof, imgReqs, SD_FileNames.BldQueIcon, "buildq.wdf", WDFKey.BldQueIcon, true, errors);
    // already built structures
    addImgReq(trek, stbof, imgReqs, SD_FileNames.BldSysImg, "solcur.wdf", WDFKey.BldSysImg, true, errors);
    // unlocked tech level construction list
    addImgReq(trek, stbof, imgReqs, SD_FileNames.TecInfoLstImg, "techl.wdf", WDFKey.TecInfoLstImg, true, errors);
    // tech database details
    addImgReq(trek, stbof, imgReqs, SD_FileNames.BldTecSelImg, "techost.wdf", WDFKey.BldTecSelImg, false, errors);
    // build selection details
    addImgReq(trek, stbof, imgReqs, SD_FileNames.BldSelImg, "solbld.wdf", WDFKey.BldSelImg, true, errors);
  }

  public static Optional<WDFImgReq> getEdificeConsReq(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    // current consumable build task image
    return getImgReq(trek, stbof, SD_FileNames.BldConsImg, "solars.wdf", WDFKey.BldConsIcon, true, errors);
  }

  public static Optional<WDFImgReq> getEdificeStrcReq(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    // current structure build task wireframe
    return getImgReq(trek, stbof, SD_FileNames.BldTskImg, "solars.wdf", WDFKey.BldTskImg, true, errors);
  }

  public static Optional<WDFImgReq> getEdificeMainBldReq(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    // population assignment
    return getImgReq(trek, stbof, SD_FileNames.BldPopImg, "labor.wdf", WDFKey.BldPopImg, true, errors);
  }

  public static Optional<WDFImgReq> getEdificeEngyBldReq(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    // energy structure list icon
    return getImgReq(trek, stbof, SD_FileNames.BldEngyIcon, "senergy.wdf", WDFKey.BldEngyIcon, true, errors);
  }

  public static Optional<WDFImgReq> getEdificeSpMnrReq(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    // race info special structure
    return getImgReq(trek, stbof, SD_FileNames.BldRaceInfoImg, "d_rinfo.wdf", WDFKey.BldRaceInfoImg, true, errors);
  }

  public static Optional<WDFImgReq> getEdificeOrbitalReq(Trek trek, Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    // built orbital battery - for error reporting, first check for extended energy screen patch
    val req = getImgReq(trek, stbof, SD_FileNames.BldOrbImg, "senergy.wdf", WDFKey.BldOrbImg_EngyExt, true, null);
    if (req.isPresent())
      return req;
    return getImgReq(trek, stbof, SD_FileNames.BldOrbImg, "senergy.wdf", WDFKey.BldOrbImg, true, errors);
  }

  public static void listShipBaseReqs(Trek trek, Stbof stbof, List<WDFImgReq> imgReqs, @Nullable Collection<String> errors) throws IOException {
    // combat ship list icon
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipCbtIcon, "_ship.wdf", WDFKey.ShipCbtIcon, true, errors);
    // task force ship list icon
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipListIcon, "bshpinf.wdf", WDFKey.ShipListIcon, true, errors);
  }

  public static void listShipBuildReqs(Trek trek, Stbof stbof, List<WDFImgReq> imgReqs, @Nullable Collection<String> errors) throws IOException {
    // queued build task icon
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipBldQueIcon, "buildq.wdf", WDFKey.ShipBldQueIcon, true, errors);
    // unlocked tech level construction list
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipTecLvlImg, "techl.wdf", WDFKey.ShipTecLvlImg, true, errors);
    // current build task wireframe
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipBldTskImg, "solars.wdf", WDFKey.ShipBldTskImg, true, errors);
    // build selection details
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipBldSelImg, "solbld.wdf", WDFKey.ShipBldSelImg, true, errors);
  }

  public static void listShipRedeployReqs(Trek trek, Stbof stbof, List<WDFImgReq> imgReqs, @Nullable Collection<String> errors) throws IOException {
    // bottom ship list icon
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipRdplIcon, "bshpinf.wdf", WDFKey.ShipRdplIcon, true, errors);
    // center redeployment list
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipRdplImg, "sd_ship.wdf", WDFKey.ShipRdplImg, true, errors);
    // center redeployment details
    addImgReq(trek, stbof, imgReqs, SD_FileNames.ShipRdplSelImg, "_shpdpy.wdf", WDFKey.ShipRdplSelImg, true, errors);
  }

  public static void listStarbaseReqs(Trek trek, Stbof stbof, List<WDFImgReq> imgReqs, @Nullable Collection<String> errors) throws IOException {
    // left task force icon
    addImgReq(trek, stbof, imgReqs, SD_FileNames.StarbaseIcon, "bshpinf.wdf", WDFKey.StarbaseIcon, true, errors);
    // bottom starbase details
    addImgReq(trek, stbof, imgReqs, SD_FileNames.StarbaseImg, "_tfship.wdf", WDFKey.StarbaseImg, false, errors);
    // bottom construction details
    addImgReq(trek, stbof, imgReqs, SD_FileNames.StarbaseBldImg, "_tfship.wdf", WDFKey.StarbaseBldImg, false, errors);
  }

  public static void listTechReqs(Trek trek, Stbof stbof, List<WDFImgReq> imgReqs, @Nullable Collection<String> errors) throws IOException {
    // building tech requirement
    addImgReq(trek, stbof, imgReqs, SD_FileNames.TecInfoLstImg, "techl.wdf", WDFKey.TecInfoLstImg, true, errors);
    // research assignment
    addImgReq(trek, stbof, imgReqs, SD_FileNames.TecResImg, "techr.wdf", WDFKey.TecResImg, true, errors);
    // technology database details
    addImgReq(trek, stbof, imgReqs, SD_FileNames.TecDBImg, "techf.wdf", WDFKey.TecDBImg, true, errors);
    // technology breakthrough event popup
    addImgReq(trek, stbof, imgReqs, SD_FileNames.TecEvtImg, "edialog.wdf", WDFKey.TecEvtImg, true, errors);
  }

  public static Optional<WDFImgReq> getRaceDplReq(Stbof stbof, @Nullable Collection<String> errors) throws IOException {
    // diplomacy race image (checked for Federation)
    return getImgReq(stbof, null, "%s", "hdiplomt.wdf", WDFKey.RaceDplImg, true, errors);
  }

  private static boolean addImgReq(Trek trek, Stbof stbof, Collection<WDFImgReq> imgReqs, StringDefinition sd,
      String wdfName, int widgetIndex, boolean mustMatch, @Nullable Collection<String> errors) throws IOException {

    Optional<WDFImgReq> req = getImgReq(trek, stbof, sd, wdfName, widgetIndex, mustMatch, errors);
    if (req.isPresent()) {
      imgReqs.add(req.get());
      return true;
    }

    return false;
  }

  private static Optional<WDFImgReq> getImgReq(Trek trek, Stbof stbof, StringDefinition sd, String wdfName,
      int widgetIndex, boolean mustMatch, @Nullable Collection<String> errors) throws IOException {

    val imgSeg = (StringSegment) trek.getSegment(sd, true);
    String imgName = imgSeg.value();
    int gfxPos = imgName.indexOf("%s");
    if (gfxPos < 0) {
      if (errors != null) {
        String err = Language.getString("WDFImgReq.8") //$NON-NLS-1$
          .replace("%1", wdfName) //$NON-NLS-1$
          .replace("%2", Integer.toString(widgetIndex)) //$NON-NLS-1$
          .replace("%3", Integer.toHexString(sd.address).toUpperCase()) //$NON-NLS-1$
          .replace("%4", imgName); //$NON-NLS-1$
        errors.add(err);
      }
      return Optional.empty();
    }

    // some image name selectors, like the ship deployment one, miss the "i_" prefix
    int start = gfxPos + 2;
    int end = imgName.endsWith(".tga") ? imgName.length() - 4 : imgName.length();
    String key = imgName.substring(start, end);

    // check the Federation WDFs for image requirements
    return getImgReq(stbof, key, imgName, "h" + wdfName, widgetIndex, mustMatch, errors);
  }

  private static Optional<WDFImgReq> getImgReq(Stbof stbof, String key, String imgName, String wdfName,
      int widgetIndex, boolean mustMatch, @Nullable Collection<String> errors) throws IOException {

    WindowDefinitionFile wdf = (WindowDefinitionFile) stbof.tryGetInternalFile(wdfName, true);
    if (wdf == null) {
      if (errors != null) {
        String err = Language.getString("WDFImgReq.6") //$NON-NLS-1$
          .replace("%1", wdfName) //$NON-NLS-1$
          .replace("%2", Integer.toString(widgetIndex)); //$NON-NLS-1$
        errors.add(err);
      }
      return Optional.empty();
    }

    WDFWindow wnd = wdf.getWindow();
    WDFWidget widget = wnd.getWidget(widgetIndex);
    if (widget == null || widget.type() != WDFWidgetType.PICTUREBOX) {
      if (errors != null) {
        String err = Language.getString("WDFImgReq.7") //$NON-NLS-1$
          .replace("%1", wdfName) //$NON-NLS-1$
          .replace("%2", Integer.toString(widgetIndex)); //$NON-NLS-1$
        errors.add(err);
      }
      return Optional.empty();
    }

    int imgWidth = (int) widget.getDimension().getWidth();
    int imgHeight = (int) widget.getDimension().getHeight();
    return Optional.of(new WDFImgReq(key, imgName, wdfName, widget.id(), imgWidth, imgHeight, mustMatch));
  }

  public enum WDFImgErrorType {
    Missing,
    LoadError,
    Format,
    Size
  }

  @Data
  public static class WDFImgError {
    private String              imageName;
    private WDFImgErrorType     errorType;
    private String              errorMessage;
    private Optional<Exception> exception;

    public WDFImgError(String imageName, WDFImgErrorType errorType, String errorMessage) {
      this.imageName = imageName;
      this.errorType = errorType;
      this.errorMessage = errorMessage;
      exception = Optional.empty();
    }

    public WDFImgError(String imageName, WDFImgErrorType errorType, String errorMessage, Exception e) {
      this.imageName = imageName;
      this.errorType = errorType;
      this.errorMessage = errorMessage;
      this.exception = Optional.of(e);
    }
  }

  public static boolean checkStbofImageReqs(@Nullable FileStore fs, Stbof stbof, InternalFile file,
      String gfx, Collection<WDFImgReq> reqs, Collection<String> errors) {
    boolean hasImgs = true;
    try (FileStore store = fs != null ? null : stbof.access()) {
      final FileStore access = store != null ? store : fs;
      for (WDFImgReq req : reqs)
        hasImgs &= checkStbofImageReq(access, stbof, file, gfx, req, errors);
    }
    catch (IOException e) {
      String err = CommonStrings.getLocalizedString(CommonStrings.File_Read_Error_1, "stbof.res");
      errors.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, err));
      hasImgs = false;
    }
    return hasImgs;
  }

  public static boolean checkStbofImageReq(@Nullable FileStore fs, Stbof stbof, InternalFile file,
      String gfx, WDFImgReq req, Collection<String> errors) {
    return checkStbofImageReq(fs, stbof, gfx, req, err -> {
      int errType = err.errorType == WDFImgErrorType.Size ? InternalFile.INTEGRITY_CHECK_WARNING : InternalFile.INTEGRITY_CHECK_ERROR;
      String msg = file.getCheckIntegrityString(errType, err.getErrorMessage());
      errors.add(msg);
      System.out.println(msg);

      val e = err.getException();
      if (e.isPresent())
        System.out.println(e.get());
    });
  }

  public static boolean checkStbofImageReq(@Nullable FileStore fs, Stbof stbof,
      String gfx, WDFImgReq req, Consumer<WDFImgError> errorHandler) {
    try (FileStore store = fs != null ? null : stbof.access()) {
      final FileStore access = store != null ? store : fs;
      return checkImageReq(gfx, req, tgaName -> {
        return stbof.tryGetTargaHeader(Optional.of(access), tgaName);
      }, errorHandler);
    }
    catch (Exception e) {
      String tgaName = req.tgaName(gfx);
      String err = CommonStrings.getLocalizedString(CommonStrings.File_Read_Error_1, "stbof.res");
      errorHandler.accept(new WDFImgError(tgaName, WDFImgErrorType.LoadError, err, e));
      return false;
    }
  }

  public static boolean checkImageReqs(String gfx, Collection<WDFImgReq> reqs,
      CheckedFunction<String, Optional<TargaHeader>, IOException> loadTga,
      Consumer<WDFImgError> errorHandler) {
    boolean hasImgs = true;
    for (WDFImgReq req : reqs) {
      hasImgs &= checkImageReq(gfx, req, loadTga, errorHandler);
    }
    return hasImgs;
  }

  public static boolean checkImageReq(String gfx, WDFImgReq req,
      CheckedFunction<String, Optional<TargaHeader>, IOException> loadTga,
      Consumer<WDFImgError> errorHandler) {
    String tgaName = req.tgaName(gfx);

    try {
      // load TGA header
      val tgaHeader = loadTga.apply(tgaName);
      if (!tgaHeader.isPresent()) {
        String err = Language.getString("WDFImgReq.0") //$NON-NLS-1$
          .replace("%1", tgaName) //$NON-NLS-1$
          .replace("%2", req.getWdfName()); //$NON-NLS-1$
        errorHandler.accept(new WDFImgError(tgaName, WDFImgErrorType.Missing, err));
        return false;
      }

      // check TGA header
      return checkTgaFormat(tgaName, req, tgaHeader.get(), errorHandler);
    }
    catch (Exception e) {
      String err = Language.getString("WDFImgReq.1") //$NON-NLS-1$
        .replace("%1", tgaName) //$NON-NLS-1$
        .replace("%2", req.getWdfName()); //$NON-NLS-1$
      errorHandler.accept(new WDFImgError(tgaName, WDFImgErrorType.LoadError, err, e));
      return false;
    }
  }

  public static boolean checkTgaFormat(String imgName, WDFImgReq req, TargaHeader tgaHeader,
      Consumer<WDFImgError> errorHandler) {
    if (tgaHeader.getPixelDepth() != 16) {
      String err = Language.getString("WDFImgReq.2") //$NON-NLS-1$
        .replace("%1", imgName) //$NON-NLS-1$
        .replace("%2", req.getWdfName()) //$NON-NLS-1$
        .replace("%3", Integer.toString(tgaHeader.getPixelDepth())); //$NON-NLS-1$
      errorHandler.accept(new WDFImgError(imgName, WDFImgErrorType.Format, err));
      return false;
    }

    if (tgaHeader.getImageType() != 2) {
      String err = Language.getString("WDFImgReq.3") //$NON-NLS-1$
        .replace("%1", imgName) //$NON-NLS-1$
        .replace("%2", req.getWdfName()); //$NON-NLS-1$
      errorHandler.accept(new WDFImgError(imgName, WDFImgErrorType.Format, err));
      return false;
    }

    // check image size
    if (req.isMustMatch()) {
      if (tgaHeader.getWidth() != req.getWidth() || tgaHeader.getHeight() != req.getHeight()) {
        String err = Language.getString("WDFImgReq.4") //$NON-NLS-1$
          .replace("%1", imgName) //$NON-NLS-1$
          .replace("%2", req.getWdfName()) //$NON-NLS-1$
          .replace("%3", Integer.toString(tgaHeader.getImageSize().width)) //$NON-NLS-1$
          .replace("%4", Integer.toString(tgaHeader.getImageSize().height)) //$NON-NLS-1$
          .replace("%5", Integer.toString(req.getWidth())) //$NON-NLS-1$
          .replace("%6", Integer.toString(req.getHeight())) //$NON-NLS-1$
          .replace("%7", Integer.toString(req.getWidgetId())); //$NON-NLS-1$
        errorHandler.accept(new WDFImgError(imgName, WDFImgErrorType.Size, err));
        return false;
      }
    }
    else if (tgaHeader.getWidth() > req.getWidth() || tgaHeader.getHeight() > req.getHeight()) {
      String err = Language.getString("WDFImgReq.5") //$NON-NLS-1$
        .replace("%1", imgName) //$NON-NLS-1$
        .replace("%2", req.getWdfName()) //$NON-NLS-1$
        .replace("%3", Integer.toString(tgaHeader.getImageSize().width)) //$NON-NLS-1$
        .replace("%4", Integer.toString(tgaHeader.getImageSize().height)) //$NON-NLS-1$
        .replace("%5", Integer.toString(req.getWidth())) //$NON-NLS-1$
        .replace("%6", Integer.toString(req.getHeight())) //$NON-NLS-1$
        .replace("%7", Integer.toString(req.getWidgetId())); //$NON-NLS-1$
      errorHandler.accept(new WDFImgError(imgName, WDFImgErrorType.Size, err));
      return false;
    }

    return true;
  }

}
