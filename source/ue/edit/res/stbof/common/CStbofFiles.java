package ue.edit.res.stbof.common;

/**
 * This interface holds common stbof file constants.
 */
public interface CStbofFiles {

  // LIST OF INTERNAL FILES
  // Use these to load appropriate files from outside stbof.res
  public static final String AIBldReqBin = "aibldreq.bin"; //$NON-NLS-1$
  public static final String AIEmpireWtf = "aiempire.wtf"; //$NON-NLS-1$
  public static final String AIMinorWtf = "aiminor.wtf"; //$NON-NLS-1$
  public static final String BldSetPrefix = "bldset"; //$NON-NLS-1$
  public static final String BldSet1Bin = "bldset1.bin"; //$NON-NLS-1$
  public static final String BldSet2Bin = "bldset2.bin"; //$NON-NLS-1$
  public static final String BldSet3Bin = "bldset3.bin"; //$NON-NLS-1$
  public static final String BldSet4Bin = "bldset4.bin"; //$NON-NLS-1$
  public static final String BldSet5Bin = "bldset5.bin"; //$NON-NLS-1$
  public static final String CColValWtf = "ccolval.wtf"; //$NON-NLS-1$
  public static final String ConverseBin = "converse.bin"; //$NON-NLS-1$
  public static final String EdifBnftBst = "edifbnft.bst"; //$NON-NLS-1$
  public static final String EdifDescBst = "edifdesc.bst"; //$NON-NLS-1$
  public static final String EdificeBst = "edifice.bst"; //$NON-NLS-1$
  public static final String EnvironEst = "environ.est"; //$NON-NLS-1$
  public static final String FColValWtf = "fcolval.wtf"; //$NON-NLS-1$
  public static final String HColValWtf = "hcolval.wtf"; //$NON-NLS-1$
  public static final String KColValWtf = "kcolval.wtf"; //$NON-NLS-1$
  public static final String LexiconDic = "lexicon.dic"; //$NON-NLS-1$
  public static final String MEPlanetBin = "meplanet.bin"; //$NON-NLS-1$
  public static final String MoraleBin = "morale.bin"; //$NON-NLS-1$
  public static final String ObjStrucSmt = "objstruc.smt"; //$NON-NLS-1$
  public static final String PaletteLst = "palette.lst"; //$NON-NLS-1$
  public static final String PlanBoniBin = "planboni.bin"; //$NON-NLS-1$
  public static final String PlanetPst = "planet.pst"; //$NON-NLS-1$
  public static final String PlanSpacBin = "planspac.bin"; //$NON-NLS-1$
  public static final String RaceRst = "race.rst"; //$NON-NLS-1$
  public static final String RaceDescRst = "racedesc.rst"; //$NON-NLS-1$
  public static final String RaceTechTec = "racetech.tec"; //$NON-NLS-1$
  public static final String RColValWtf = "rcolval.wtf"; //$NON-NLS-1$
  public static final String ShipDescSst = "shipdesc.sst"; //$NON-NLS-1$
  public static final String ShipListSst = "shiplist.sst"; //$NON-NLS-1$
  public static final String ShipNameBin = "shipname.bin"; //$NON-NLS-1$
  public static final String ShipRaceSst = "shiprace.sst"; //$NON-NLS-1$
  public static final String ShipTechSst = "shiptech.sst"; //$NON-NLS-1$
  public static final String StarNameBin = "starname.bin"; //$NON-NLS-1$
  public static final String TecFieldTec = "tecfield.tec"; //$NON-NLS-1$
  public static final String TechDescTec = "techdesc.tec"; //$NON-NLS-1$
  public static final String TechTreeTec = "techtree.tec"; //$NON-NLS-1$
  public static final String TextureLst = "texture.lst"; //$NON-NLS-1$
  public static final String IntelBin = "intel.bin"; //$NON-NLS-1$

  public static final String[] BldSets = new String[] {
    BldSet1Bin, BldSet2Bin, BldSet3Bin, BldSet4Bin, BldSet5Bin
  };

  /**
   * List of recognized internal core files that always must be present.
   */
  public static final String[] CoreFiles = new String[] {
    CStbofFiles.MoraleBin, CStbofFiles.RaceRst, CStbofFiles.RaceDescRst,
    CStbofFiles.AIMinorWtf, CStbofFiles.ShipNameBin, CStbofFiles.StarNameBin,
    CStbofFiles.TextureLst,
    CStbofFiles.LexiconDic, CStbofFiles.ObjStrucSmt, CStbofFiles.EnvironEst,
    CStbofFiles.PlanSpacBin,
    CStbofFiles.PlanBoniBin, CStbofFiles.PlanetPst, CStbofFiles.MEPlanetBin,
    CStbofFiles.TechTreeTec,
    CStbofFiles.TechDescTec, CStbofFiles.TecFieldTec, CStbofFiles.EdifBnftBst,
    CStbofFiles.EdifDescBst,
    CStbofFiles.EdificeBst, CStbofFiles.BldSet1Bin, CStbofFiles.BldSet2Bin, CStbofFiles.BldSet3Bin,
    CStbofFiles.BldSet4Bin, CStbofFiles.BldSet5Bin, CStbofFiles.RaceTechTec,
    CStbofFiles.ShipListSst,
    CStbofFiles.ShipDescSst, CStbofFiles.ShipRaceSst, CStbofFiles.ShipTechSst,
    CStbofFiles.AIEmpireWtf,
    CStbofFiles.PaletteLst, CStbofFiles.CColValWtf, CStbofFiles.HColValWtf, CStbofFiles.FColValWtf,
    CStbofFiles.KColValWtf, CStbofFiles.RColValWtf, CStbofFiles.AIBldReqBin,
    CStbofFiles.ConverseBin,
    CStbofFiles.IntelBin
  };
}
