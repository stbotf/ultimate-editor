package ue.edit.res.stbof.common;

/**
 * This interface holds common stbof constants.
 */
public interface CStbof {

  // #region Game Characteristics

  public static final int DEFAULT_NUM_TECHLVLS = 11;
  public static final int DEFAULT_PLANET_TYPES = PlanetType.PlanetCount;
  public static final int NUM_BLD_CAT = StructureCategory.Count; // if not StructureGroup.MainCount
  public static final int NUM_ERAS = Era.Count;
  public static final int NUM_EMPIRES = Race.EmpireCount;
  public static final int NUM_GAL_SHAPES = GalShape.Count;
  public static final int NUM_GAL_SIZES = GalSize.Count;
  public static final int NUM_MINOR_SETTINGS = MinorSetting.Count;
  public static final int NUM_MINORS = Race.MinorCount;
  public static final int NUM_MAJOR_SHIP_ROLES = ShipRole.MajorShipRoleCount;
  public static final int NUM_RACES = Race.RaceCount;
  public static final int NUM_TECH_FIELDS = TechField.Count;

  public interface GalShape {
    public static final byte Irregular = 0;
    public static final byte Elliptical = 1;
    public static final byte Ring = 2;
    public static final byte Spiral = 3;

    public static final int Count = 4;
  }

  public interface GalSize {
    public static final byte Small = 0;
    public static final byte Medium = 1;
    public static final byte Large = 2;

    public static final int Count = 3;
  }

  public interface MinorSetting {
    public static final byte None = 0;
    public static final byte Few = 0;
    public static final byte Some = 1;
    public static final byte Many = 2;

    public static final int Count = 4;
  }

  // #endregion Game Characteristics

  // #region Edifice

  public interface CountReq {
    public static final byte None = 0;
    public static final byte OnePerSystem = 1;
    public static final byte OnePerEmpire = 2;
  }

  public interface StructureBonus {
    public static final byte Food = 0;
    public static final byte Construction = 2;
    public static final byte Energy = 7;
    public static final byte Intel = 10;
    public static final byte Research = 13;
    public static final byte BuildsShips = 18;
    public static final byte MartialLaw = 36;

    public static boolean isMainBonus(byte bonusType) {
      switch (bonusType) {
        case StructureBonus.Food:
        case StructureBonus.Construction:
        case StructureBonus.Energy:
        case StructureBonus.Intel:
        case StructureBonus.Research:
        return true;
      }
      return false;
    }
  }

  public interface StructureCategory {
    public static final int Research = 0;
    public static final int Industry = 1;
    public static final int Food = 2;
    public static final int Energy = 3;
    public static final int Defense = 4;

    public static final int Count = 5;
  }

  public interface StructureGroup {
    public static final int MainFood = 0;
    public static final int MainIndustry = 1;
    public static final int MainEnergy = 2;
    public static final int MainIntel = 3;
    public static final int MainResearch = 4;
    public static final int Scanners = 5;
    public static final int Other = 6;
  }

  public interface StructureType {
    public static final int BasicScanner = 0;
    public static final int OrbitalBattery = 1;
    public static final int BasicDilithiumSource = 2;
    public static final int BasicShieldGenerator = 3;
    public static final int TradeGoods = 4;
    public static final int BasicShipyard = 5;
    public static final int BasicGroundDefense = 6;
    /// id sets
    public static final int MartialLaw = 100;
    public static final int Shipyard = 101;
    public static final int MinorShipBuild = 102;
  }

  public interface SystemReq {
    public static final int ArcticPlanet = PlanetType.Arctic;
    public static final int BarrenPlanet = PlanetType.Barren;
    public static final int DesertPlanet = PlanetType.Desert;
    public static final int GasGiant = PlanetType.GasGiant;
    public static final int JunglePlanet = PlanetType.Jungle;
    public static final int OceanicPlanet = PlanetType.Oceanic;
    public static final int TerranPlanet = PlanetType.Terran;
    public static final int VolcanicPlanet = PlanetType.Volcanic;
    public static final int MethanicAtmosphere = PlanetType.Methanic;
    public static final int Asteroid = PlanetType.Asteroid;
    public static final int AsteroidBelt = PlanetType.AsteroidBelt;
    public static final int Dilithium = PlanetType.Dilithium;
    public static final int WormHole = PlanetType.WormHole;
    public static final int RadioPulsar = PlanetType.RadioPulsar;
    public static final int XRayPulsar = PlanetType.XRayPulsar;
  }

  public interface SystemStatusReq {
    public static final byte HomeSystem = 0;
    public static final byte NativeMember = 1;
    public static final byte NonNativeMember = 2;
    public static final byte Subjugated = 3;
    public static final byte Affiliated = 4;
    public static final byte IndependentMinor = 5;
    public static final byte ConqueredHome = 6;
    public static final byte None = 9;
    public static final byte Rebel = 10;
    public static final byte EmptySystem = 11;
  }

  // #endregion Edifice

  // #region Stellar Objects

  public interface StellarType {
    public static final int BlackHole   = 0;
    public static final int XRayPulsar  = 1;
    public static final int Nebula      = 2;
    public static final int NeutronStar = 3;
    public static final int RedGiant    = 4;
    public static final int OrangeStar  = 5;
    public static final int YellowStar  = 6;
    public static final int WhiteStar   = 7;
    public static final int GreenStar   = 8;
    public static final int BlueStar    = 9;
    public static final int WormHole    = 10;
    public static final int RadioPulsar = 11;

    public static final int DefaultStar = YellowStar;
  }

  public interface StellarGfx {
    public static final String BlueStar = "s-b-";
    public static final String GreenStar = "s-g-";
    public static final String OrangeStar = "s-o-";
    public static final String RedGiant = "s-r-";
    public static final String WhiteStar = "s-w-";
    public static final String YellowStar = "s-y-";

    public static final String BlackHole = "blkh";
    public static final String Nebula = "neb-"; // .tga!!
    public static final String NeutronStar = "ns";
    public static final String RadioPulsar = "rpls";
    public static final String WormHole = "worm";
    public static final String XRayPulsar = "xp-";

    public static final String Default = YellowStar;

    public static String map(int stellarType) {
      switch (stellarType) {
        case StellarType.BlackHole:
          return BlackHole;
        case StellarType.XRayPulsar:
          return XRayPulsar;
        case StellarType.Nebula:
          return Nebula;
        case StellarType.NeutronStar:
          return NeutronStar;
        case StellarType.RedGiant:
          return RedGiant;
        case StellarType.OrangeStar:
          return OrangeStar;
        case StellarType.YellowStar:
          return YellowStar;
        case StellarType.WhiteStar:
          return WhiteStar;
        case StellarType.GreenStar:
          return GreenStar;
        case StellarType.BlueStar:
          return BlueStar;
        case StellarType.WormHole:
          return WormHole;
        case StellarType.RadioPulsar:
          return RadioPulsar;
        default:
          return Default;
      }
    }
  }

  // #endregion Stellar Objects

  // #region Planet

  public static final int MAX_PLANETS = 9;

  public static final char[] PlanetCategory = new char[] {
    'P', 'B', 'G', 'B', 'L', 'O', 'M', 'Y',
    'T', 'A', 'E', 'D', 'W', 'R', 'X' // specials
  };

  // amount for planet bonuses
  public interface BonusLevel {
    public static final int Few = 0;
    public static final int Moderate = 1;
    public static final int Much = 2;
  }

  public interface BonusValue {
    public static final short Few = 0;
    public static final short Moderate = 310; // 0x136;
    public static final short Much = 620;   // 0x26C
  }

  public interface PlanetSize {
    public static final short Small = 0;
    public static final short Medium = 1;
    public static final short Large = 2;
  }

  public interface PlanetType {
    public static final int Arctic = 0;
    public static final int Barren = 1;
    public static final int Desert = 2;
    public static final int GasGiant = 3;
    public static final int Jungle = 4;
    public static final int Oceanic = 5;
    public static final int Terran = 6;
    public static final int Volcanic = 7;

    public static final int Methanic = 8; // ??
    public static final int Asteroid = 9; // ??
    public static final int AsteroidBelt = 10;
    public static final int Dilithium = 11; // selector
    public static final int WormHole = 12;
    public static final int RadioPulsar = 13;
    public static final int XRayPulsar = 14;

    public static final int Default = Barren;
    public static final int PlanetCount = 8;

    public static boolean isEnergyType(int type) {
      switch (type) {
        case Arctic:
        case Desert:
        case GasGiant:
        case Volcanic:
        case Asteroid:
        case AsteroidBelt:
          return true;
        default:
          return false;
      }
    }
  }

  public interface PlanetAtmosphere {
    // copied from MapGUI.getAtmoTypeString
    public static final int Methane = 8;
    public static final int None = 9;
    public static final int OxygenRich = 10;
    public static final int Sulfuric = 11;
    public static final int OxygenThin = 12;

    public static final int Default = None;
  }

  public interface PlanetAni {
    public static final String Mercury = "merc";
    public static final String Venus = "venu";
    public static final String Earth = "eart";
    public static final String Mars = "mars";
    public static final String Jupiter = "jupi";
    public static final String Saturn = "satu";
    public static final String Uranus = "uran";
    public static final String Neptun = "nept";
    public static final String Pluto = "plut";

    public static final String arol = "arol"; // arctic large
    public static final String arom = "arom"; // arctic medium
    public static final String aros = "aros"; // arctic small
    public static final String atol = "atol"; // arctic oxygen large
    public static final String atom = "atom"; // arctic oxygen medium
    public static final String atos = "atos"; // arctic oxygen small
    public static final String bnol = "bnol"; // barren large
    public static final String bnom = "bnom"; // barren medium
    public static final String bnos = "bnos"; // barren small
    public static final String btol = "btol"; // barren oxygen large
    public static final String btom = "btom"; // barren oxygen medium
    public static final String btos = "btos"; // barren oxygen small
    public static final String drol = "drol"; // desert large
    public static final String drom = "drom"; // desert medium
    public static final String dros = "dros"; // desert small
    public static final String dtol = "dtol"; // desert oxygen large
    public static final String dtom = "dtom"; // desert oxygen medium
    public static final String dtos = "dtos"; // desert oxygen small
    public static final String gmel = "gmel"; // purple gas giant
    public static final String gmrl = "gmrl"; // orange gas giant with rings
    public static final String gmsl = "gmsl"; // yellow gas giant
    public static final String jrol = "jrol"; // jungle large
    public static final String jrom = "jrom"; // jungle medium
    public static final String jros = "jros"; // jungle small
    public static final String orol = "orol"; // oceanic large
    public static final String orom = "orom"; // oceanic medium
    public static final String oros = "oros"; // oceanic small
    public static final String otol = "otol"; // oceanic oxygen large
    public static final String otom = "otom"; // oceanic oxygen medium
    public static final String otos = "otos"; // oceanic oxygen small
    public static final String trol = "trol"; // terran large
    public static final String trom = "trom"; // terran medium
    public static final String tros = "tros"; // terran small
    public static final String vrol = "vrol"; // volcanic large
    public static final String vrom = "vrom"; // volcanic medium
    public static final String vros = "vros"; // volcanic small
    public static final String vsal = "vsal"; // volcanic sulfuric large
    public static final String vsam = "vsam"; // volcanic sulfuric medium
    public static final String vsas = "vsas"; // volcanic sulfuric small
    public static final String vtol = "vtol"; // volcanic oxygen large
    public static final String vtom = "vtom"; // volcanic oxygen medium
    public static final String vtos = "vtos"; // volcanic oxygen small
    public static final String brgl = "brgl"; // borg large
    public static final String brgm = "brgm"; // borg medium
    public static final String brgs = "brgs"; // borg small

    public static final String Default = bnos;

    static final String[] generic = new String[] {
      /*arctic*/    aros, arom, arol, /*barren*/    bnos, bnom, bnol, /*desert*/  dros, drom, drol,
      /*gas giant*/ gmsl, gmrl, gmel, /*jungle*/    jros, jrom, jrol, /*oceanic*/ oros, orom, orol,
      /*terran*/    tros, trom, trol, /*volcanic*/  vros, vrom, vrol, /*borg*/    brgs, brgm, brgl
    };

    static final String[] atmo = new String[] {
      /*arctic oxygen*/   atos, atom, atol, /*barren oxygen*/     btos, btom, btol,
      /*desert oxygen*/   dtos, dtom, dtol, /*oceanic oxygen*/    otos, otom, otol,
      /*volcanic oxygen*/ vtos, vtom, vtol, /*volcanic sulfuric*/ vsas, vsam, vsal,
    };

    public static String map(int planetType, int planetSize, int atmosphere) {
      switch (atmosphere) {
        case PlanetAtmosphere.Sulfuric:
          if (planetType == PlanetType.Volcanic)
            return atmo[15 + planetSize];
        case PlanetAtmosphere.Methane:
        case PlanetAtmosphere.OxygenThin:
          switch (planetType) {
            case PlanetType.Arctic:
              return atmo[planetSize];
            case PlanetType.Barren:
              return atmo[3 + planetSize];
            case PlanetType.Desert:
              return atmo[6 + planetSize];
            case PlanetType.Oceanic:
              return atmo[9 + planetSize];
            case PlanetType.Volcanic:
              return atmo[12 + planetSize];
          }
      }

      return generic[planetType * 3 + planetSize];
    }
  }

  // #endregion Planet

  // #region Race

  // @see LexHelper.lexRace for the minors
  public interface Race {
    public static final short Card = 0;
    public static final short Fed = 1;
    public static final short Ferg = 2;
    public static final short Kling = 3;
    public static final short Rom = 4;
    // noone, minors (race id 5-34) & rebel systems = 35 (23h)
    public static final short None = 35;
    public static final short Monster = 36;
    public static final short Neutral = 37;

    public static final short EmpireCount = 5;
    public static final short MinorCount = 30;
    public static final short RaceCount = EmpireCount + MinorCount;
  }

  // #endregion Race

  // #region StartCond

  public interface Era {
    public static final int Beginning = 0;
    public static final int Early = 1;
    public static final int Developed = 2;
    public static final int Expanded = 3;
    public static final int Advanced = 4;

    public static final int Count = 5;
  }

  // #endregion StartCond

  // #region SystInfo

  public interface BuildOrder {
    public static final int None = 0;               // after supernova
    public static final int BuildStructure = 1;
    public static final int UpgradeStructure = 2;
    public static final int BuildShip = 3;
    public static final int TradeGoods = 5;         // also if uninhabited
    public static final int UnrestOrder = 7;
  }

  public interface ControlType {
    public static final int None = 0;   // uninhabited
    public static final int AI = 1;     // major or minor
    public static final int Player = 2;
  }

  public interface MoraleLevel {
    public static final short Rebellious = -4;  // morale 1-24
    public static final short Defiant = -3;     // morale 25-50
    public static final short Disgruntled = -2; // morale 51-75
    public static final short Apathetic = -1;   // morale 76-99
    public static final short Content = 0;      // morale 100-125
    public static final short Pleased = 1;      // morale 126-150
    public static final short Loyal = 2;        // morale 151-175
    public static final short Fanatic = 3;      // morale 176-194
  }

  public interface SystemStatus {
    public static final byte Home = 0;          // uninhabited / major race home system
    public static final byte Native = 1;        // inhabited by the controlling race
    public static final byte Membered = 2;      // membered minor race systems
    public static final byte Subjugated = 3;    // system is subjugated
    public static final byte Affiliated = 4;    // system is affiliated
    public static final byte Independent = 5;   // independent minor or uninhabited systems
    public static final byte ConqueredHome = 6; // occupied home system of another race
    public static final byte Any = 9;
    public static final byte Rebel = 10;
    public static final byte Extra = 11;        // uninhabited extra starting system (BOP special)
  }

  public interface UpgradeType {
    public static final short CardFood = 0;
    public static final short FedFood = 1;
    public static final short FergFood = 2;
    public static final short KlngFood = 3;
    public static final short RomFood = 4;

    public static final short CardIndustry = 6;
    public static final short FedIndustry = 7;
    public static final short FergIndustry = 8;
    public static final short KlngIndustry = 9;
    public static final short RomIndustry = 10;

    public static final short CardEnergy = 12;
    public static final short FedEnergy = 13;
    public static final short FergEnergy = 14;
    public static final short KlngEnergy = 15;
    public static final short RomEnergy = 16;

    public static final short CardIntel = 18;
    public static final short FedIntel = 19;
    public static final short FergIntel = 20;
    public static final short KlngIntel = 21;
    public static final short RomIntel = 22;

    public static final short CardResearch = 24;
    public static final short FedResearch = 25;
    public static final short FergResearch = 26;
    public static final short KlngResearch = 27;
    public static final short RomResearch = 28;
  }

  // #endregion SystInfo

  // #region TaskForce

  public interface ShipRange {
    public static final int None = -1;
    public static final int Short = 0;
    public static final int Medium = 1;
    public static final int Long = 2;
    public static final int Towable = 3;
  }

  public interface ShipRole {
    public static final short None = -1;
    public static final short Scout = 0;
    public static final short Destroyer = 1;
    public static final short Cruiser = 2;
    public static final short StrikeCruiser = 3;
    public static final short BattleShip = 4;
    public static final short ColonyShip = 5;
    public static final short Outpost = 6;
    public static final short Starbase = 7;
    public static final short Monster = 8;
    public static final short TroopTransport = 9;

    public static final short MajorShipRoleCount = 7;

    public static boolean isAlien(short role) { return role == Monster; }
    public static boolean isCombatant(short role) {
      switch (role) {
        case Scout:
        case Destroyer:
        case Cruiser:
        case StrikeCruiser:
        case BattleShip:
          return true;
        default:
          return false;
      }
    }
    public static boolean isNonCombatant(short role) { return role == ColonyShip || role == TroopTransport; }
    public static boolean isShip(short role) { return isCombatant(role) || isNonCombatant(role); }
    public static boolean isStarBase(short role) { return role == Outpost || role == Starbase; }
  }

  public interface ShipStealth {
    public static final int None = 0;
    public static final int Stealth_I = 1;
    public static final int Stealth_II = 2;
    public static final int Stealth_III = 3;
    public static final int Cloak_I = 4;
    public static final int Cloak_II = 5;
    public static final int Cloak_III = 6;
  }

  // #endregion TaskForce

  // #region TechInfo

  public interface TechField {
    public static final int Biotech = 0;
    public static final int Computer = 1;
    public static final int Construction = 2;
    public static final int Energy = 3;
    public static final int Propulsion = 4;
    public static final int Weapons = 5;
    public static final int Sociology = 6;

    public static final int Count = 7;
  }

  // #endregion TechInfo

}
