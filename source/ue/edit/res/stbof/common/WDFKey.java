package ue.edit.res.stbof.common;

/**
 * This interface holds stbof.res WDF key constants.
 */
public interface WDFKey {

  // #region Edifice Image Keys

  // trek.exe[0x1801B4] %s_30.tga   30x30   *senergy.wdf  <2410..6, 2430..6>      energy screen listing, modded %s_50.tga
  public static final int BldEngyIcon = 2410;
  // trek.exe[0x18023C] %s_30.tga   30x30   *buildq.wdf   <2049>                  queued build task, modded %s_50.tga
  public static final int BldQueIcon = 2049;
  // trek.exe[0x180194] %s_60.tga   60x50   *solcur.wdf   <2103,_7,12,17,22,27>   already built structures
  public static final int BldSysImg = 2103;
  // trek.exe[0x180214] %s_60.tga   60x50   *senergy.wdf  <2464>                  built orbital battery
  public static final int BldOrbImg = 2464;
  public static final int BldOrbImg_EngyExt = 2409; // with energy screen extension
  // trek.exe[0x180384] %s_60.tga   60x50   *labor.wdf    <2300,_10,20,30,40>     population assignment
  public static final int BldPopImg = 2300;
  // trek.exe[0x180220] %s.tga      120x100 *solars.wdf   <2010>                  current consumable build task (martial law)
  public static final int BldConsIcon = 2010;
  // trek.exe[0x180248] %sw.tga     120x100 *solars.wdf   <2010>                  current build task wireframe
  public static final int BldTskImg = 2010;
  // trek.exe[0x17E5DC] %s170.tga   170x142 *d_rinfo.wdf  <5251>                  race info special structure
  public static final int BldRaceInfoImg = 5251;
  // trek.exe[0x180228] %s170.tga   170x142 *solbld.wdf   <2202>                  build selection details
  public static final int BldSelImg = 2202;
  // trek.exe[0x17EC34] %s_b.tga    270x225 *techost.wdf  <3320>                  tech database details, modded %s_c.tga
  public static final int BldTecSelImg = 3320;

  // #endregion Edifice Image Keys

  // #region Ship Image Keys

  // trek.exe[0x17D058] i_%s30.tga  30x30   *_ship.wdf    <1213>                  combat ship list
  public static final int ShipCbtIcon = 1213;
  // trek.exe[0x17FC50] i_%s30.tga  30x30   *bshpinf.wdf  <6552>                  bottom task force ship list
  public static final int ShipListIcon = 6552;
  // trek.exe[0x1807FC] i_%s30.tga  30x30   *bshpinf.wdf  <6552>                  bottom task force ship list on redeployment
  public static final int ShipRdplIcon = 6552;
  // trek.exe[0x180250] i_%s30.tga  30x30   *buildq.wdf   <2049>                  queued build task
  public static final int ShipBldQueIcon = 2049;
  // trek.exe[0x17ED28] i_%s60.tga  60x50   *techl.wdf    <3401..4,_13,15>        tech level unlockings
  public static final int ShipTecLvlImg = 3401;
  // trek.exe[0x18025C] i_%s60.tga  60x50   *solcur.wdf   <2103,_7,12,17,22,27>   already built ships (unused)
  // trek.exe[0x1807D4] i_%s60.tga  60x50   *sd_ship.wdf  <1213>                  task force redeployment list
  public static final int ShipRdplImg = 1213;
  // trek.exe[0x180268] i_%s120.tga 120x100 *solars.wdf   <2010>                  current consumable ship build task (unused)
  // trek.exe[0x180280] i_%sw.tga   120x100 *solars.wdf   <2010>                  ship build task wireframe
  public static final int ShipBldTskImg = 2010;
  // trek.exe[0x180274] i_%s170.tga 170x142 *solbld.wdf   <2202>                  ship build selection details
  public static final int ShipBldSelImg = 2202;
  // trek.exe[0x180724] i_%s170.tga 170x142 *_shpdpy.wdf  <1335>                  ship redeploy selection details
  public static final int ShipRdplSelImg = 1335;

  // #endregion Ship Image Keys

  // #region Starbase Image Keys

  // trek.exe[0x17FA54] i_%s30.tga  30x30   *bshpinf.wdf  <6552>                  bottom task force station
  public static final int StarbaseIcon = 6552;
  // trek.exe[0x17FBEC] i_%s120.tga 120x100 *_tfship.wdf  <1212>                  starbase sector details image
  public static final int StarbaseImg = 1212;
  // trek.exe[0x17FBE0] i_%sw.tga   120x100 *_tfship.wdf  <1212>                  starbase construction wireframe
  public static final int StarbaseBldImg = 1212;

  // #endregion Starbase Image Keys

  // #region Technology Field Image Keys

  // trek.exe[0x17ED1C] %s_60.tga   60x50   *techl.wdf    <3401..4,_13,15>        tech level unlockings & building tech requirements
  public static final int TecInfoLstImg = 3401;
  // trek.exe[0x17EAE4] %s.tga      120x100 *techr.wdf    <3102,_16,30,44,58,72>  research assignment
  public static final int TecResImg = 3102;
  // trek.exe[0x17ECAC] %s_b.tga    270x225 *techf.wdf    <3215>                  technology database details (modded: %s_c.tga)
  public static final int TecDBImg = 3215;
  // trek.exe[0x178398] %s_b.tga    270x225 *edialog.wdf  <9800>                  technology breakthrough event popup
  public static final int TecEvtImg = 9800;

  // #endregion Technology Field Image Keys

  // #region Race Image Keys

  // <race-name>.tga                160x170 *diplomt.wdf  <5004>                  diplomacy race image
  public static final int RaceDplImg = 5004;

  // #endregion Race Image Keys

}
