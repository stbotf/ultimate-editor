package ue.edit.res.stbof.files.gif;

import java.awt.Dimension;
import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import javax.imageio.ImageIO;
import ue.edit.common.InternalFile;
import ue.exception.UnsupportedImage;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.HexTools;
import ue.util.img.GifApplicationExtension;
import ue.util.img.GifColorMap;
import ue.util.img.GifControlExtension;
import ue.util.img.GifImageData;
import ue.util.stream.StreamTools;

/**
 * This class is used for loading gif images. Not all kinds of gif's are supported. The variables
 * that are static and final have set values with which the gif being loaded must comply.
 */
public class GifImage extends InternalFile {

  private static final boolean  GLOBAL_COLOR_TABLE_FLAG = true;
  private static final byte     PIXEL_ASPECT_RATIO = 0x00;
  private static final String   SIGNATURE = "GIF"; // 3 bytes (ASCII) //$NON-NLS-1$
  private static final int      HEADER_SIZE = 23;

  private final String errNOTGIF = Language.getString("GifImage.0"); //$NON-NLS-1$
  private final String errVERSION = Language.getString("GifImage.1"); //$NON-NLS-1$

  // #region fields

  // for the file format, refer https://en.wikipedia.org/wiki/GIF
  private String VERSION;                     // 3 bytes (ASCII)
  // LOGICAL SCREEN DESCRIPTOR
  private int LOGICAL_SCREEN_WIDTH;           // 2 bytes (unsigned short)
  private int LOGICAL_SCREEN_HEIGHT;          // 2 bytes (unsigned short)
  // GLOBAL_COLOR_TABLE_FLAG                  // 1 bit   (boolean)
  private byte COLOR_RESOLUTION;              // 3 bit
  // SORT_FLAG                                // 1 bit   (boolean)
  private byte GLOBAL_COLOR_TABLE_SIZE;       // 3 bit
  private byte BACKGROUND_COLOR_INDEX;        // 1 byte  (byte)
  // PIXEL_ASPECT_RATIO                       // 1 byte  (byte)
  // GLOBAL COLOR TABLE
  private GifColorMap initialColorMap;
  // IMAGE DESCRIPTOR
  private byte SEGMENT_SENTINEL;              // 1 byte  (byte)

  // THE REST OF THE FILE
  private byte[] unknown;                     // unknown data + '3B' file termination

  private GifColorMap currentColorMap;
  private GifControlExtension controlExtension;
  private GifApplicationExtension appExtension;
  private int unknownExtension = -1;
  private GifImageData imageData;

  // #endregion fields

  // #region constructor

  public GifImage() {
  }

  public GifImage(String fileName) {
    super(fileName);
  }

  public GifImage(String fileName, InputStream in) throws IOException {
    super(fileName);
    load(in);
  }

  public GifImage(String fileName, byte[] data) throws IOException {
    super(fileName);
    load(data);
  }

  // #endregion constructor

  // #region data access

  /**
   * Sets GifColorMap to use when rendering the image.
   */
  public void setGifColorMap(GifColorMap map) {
    currentColorMap = map;
    GLOBAL_COLOR_TABLE_SIZE = currentColorMap.colorTableSize();
    // don't mark changed when set by TextureGUI
    // this sneakingly patches scar1.gif when viewed by TextureGUI but leaves othher files untouched
    // markChanged();
  }

  /**
   * Returns the GifColorMap contained in the gif.
   */
  public GifColorMap getInitialGifColorMap() {
    return initialColorMap;
  }

  /**
   * Returns image.
   */
  public Image getImage() throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    save(baos);
    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
    return ImageIO.read(bais);
  }

  /**
   * Returns size.
   */
  public Dimension getResolution() {
    return new Dimension(LOGICAL_SCREEN_WIDTH, LOGICAL_SCREEN_HEIGHT);
  }

  // #endregion data access

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    //HEADER
    byte[] b = StreamTools.readBytes(in, 6);                  // READ 6 BYTES

    //SIGNATURE
    String sig = DataTools.fromNullTerminatedBotfString(b, 0, 3);
    if (!sig.equals(SIGNATURE)) {
      String err = errNOTGIF.replace("%1", sig); //$NON-NLS-1$
      throw new UnsupportedImage(err);
    }

    //VERSION
    VERSION = DataTools.fromNullTerminatedBotfString(b, 3, 3);
    if (!VERSION.equals("89a") && !VERSION.equals("87a")) //$NON-NLS-1$ //$NON-NLS-2$
    {
      String err = errVERSION.replace("%1", VERSION); //$NON-NLS-1$
      throw new UnsupportedImage(err);
    }

    //LOGICAL_SCREEN_WIDTH
    LOGICAL_SCREEN_WIDTH = StreamTools.readShort(in, true);   // READ 2 BYTES
    if (LOGICAL_SCREEN_WIDTH < 0)
      LOGICAL_SCREEN_WIDTH = 65535 + LOGICAL_SCREEN_WIDTH;

    //LOGICAL_SCREEN_HEIGHT
    LOGICAL_SCREEN_HEIGHT = StreamTools.readShort(in, true);  // READ 2 BYTES
    if (LOGICAL_SCREEN_HEIGHT < 0)
      LOGICAL_SCREEN_HEIGHT = 65535 + LOGICAL_SCREEN_HEIGHT;

    //GLOBAL_COLOR_TABLE_FLAG
    int flags = in.read();                                    // READ 1 BYTE
    if (GLOBAL_COLOR_TABLE_FLAG != ((0x80 & flags) == 0x80))
      throw new UnsupportedImage(Language.getString("GifImage.2")); //$NON-NLS-1$

    //COLOR_RESOLUTION
    /*
     * You get COLOR_RESOLUTION -1 from file.
     * It tells you how many bits are used by a basic color (red, green or blue)
     */
    COLOR_RESOLUTION = (byte) (((flags >>> 4) & 0x07) + 1);

    //SORT_FLAG
    // ignored
    //if (SORT_FLAG != ((0x08 & b[0]) == 0x08))
    //    throw new UnsupportedImage("Unsupported gif: global color table is sorted.");
    //GLOBAL_COLOR_TABLE_SIZE
    GLOBAL_COLOR_TABLE_SIZE = (byte) (0x07 & flags);

    //background color
    BACKGROUND_COLOR_INDEX = (byte)in.read();                 // READ 1 BYTE

    //pixel aspect ratio
    byte aspectRatio = (byte)in.read();                       // READ 1 BYTE
    if (PIXEL_ASPECT_RATIO != aspectRatio)
      throw new UnsupportedImage(Language.getString("GifImage.3")); //$NON-NLS-1$

    // GLOBAL COLOR TABLE
    int size = 1 << (GLOBAL_COLOR_TABLE_SIZE + 1);
    currentColorMap = initialColorMap = new GifColorMap(in, size, false);

    // read image segments
    loadSegments(in);

    //NOW COMES LZW and other unknown stuff
    unknown = StreamTools.readAllBytes(in);
  }

  // #endregion load

  // #region save

  /*Saves this gif.*/
  public void save(OutputStream out) throws IOException {             // 23 + color map size + lzw size
    //HEADER
    //SIGNATURE
    out.write(DataTools.toByte(SIGNATURE, SIGNATURE.length(), SIGNATURE.length())); // 3
    //VERSION
    out.write(DataTools.toByte(VERSION, VERSION.length(), VERSION.length()));       // 3

    //LOGICAL_SCREEN_WIDTH
    out.write(DataTools.toByte((short)LOGICAL_SCREEN_WIDTH, true));   // 2
    //LOGICAL_SCREEN_HEIGHT
    out.write(DataTools.toByte((short)LOGICAL_SCREEN_HEIGHT, true));  // 2

    //GLOBAL_COLOR_TABLE_FLAG
    int flags = 0x80;
    //COLOR_RESOLUTION
    flags |= ((COLOR_RESOLUTION - 1) << 4);
    //GLOBAL_COLOR_TABLE_SIZE
    flags |= GLOBAL_COLOR_TABLE_SIZE;
    out.write(flags);                                                 // 1

    //BACKGROUND_COLOR_INDEX
    out.write(BACKGROUND_COLOR_INDEX);                                // 1
    //PIXEL_ASPECT_RATIO
    out.write(PIXEL_ASPECT_RATIO);                                    // 1
    //GLOBAL COLOR TABLE
    currentColorMap.save(out, false);                                 // 256 for scar1.gif, else 1024

    saveSegments(out);

    //NOW COMES THE LZW_SHIT
    out.write(unknown);
  }

  // #endregion save

  // #region maintenance

  public int getSize() {
    return HEADER_SIZE
      + (currentColorMap != null ? currentColorMap.size() : 0)
      + (controlExtension != null ? controlExtension.size() : 0)
      + (appExtension != null ? appExtension.size() : 0)
      + (unknownExtension != -1 ? 1 : 0)
      + (imageData != null ? imageData.size() : 0)
      + (unknown != null ? unknown.length : 0);
  }

  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public void clear() {
    VERSION = null;
    LOGICAL_SCREEN_WIDTH = 0;
    LOGICAL_SCREEN_HEIGHT = 0;
    COLOR_RESOLUTION = 0;
    GLOBAL_COLOR_TABLE_SIZE = 0;
    BACKGROUND_COLOR_INDEX = 0;
    initialColorMap = null;
    SEGMENT_SENTINEL = 0;
    currentColorMap = null;
    controlExtension = null;
    appExtension = null;
    unknownExtension = -1;
    imageData = null;
    unknown = null;
  }

  // #endregion maintenance

  // #region private helper routines

  private void loadSegments(InputStream in) throws IOException {
    while (true) {
      // read next data segment identifier
      SEGMENT_SENTINEL = (byte)in.read();                     // READ 1 BYTE

      switch (SEGMENT_SENTINEL) {
        // 21 introduces an extension block
        // 21 F9 = Graphic Control Extension
        // 21 FF = Animated GIF Application Extension
        case 0x21:
          // On unknown extensions, there is no chance to estimate the remaining block size.
          // Therefore stop the segment load but load all the rest as unknown binary data.
          if (!loadExtension(in))
            return;
          break;
        // 2C introduces the image descritpor
        case 0x2C:
          imageData = new GifImageData(in);
          break;
        // 3B = the trailer, which always is written to the end of the file
        default:
          return;
      }
    }
  }

  private boolean loadExtension(InputStream in) throws IOException {
    int extension = in.read();                                // READ 1 BYTE
    switch (extension) {
      case GifApplicationExtension.ExtensionSentinel:
        appExtension = new GifApplicationExtension(in);       // READ 17 BYTE
        return true;
      case GifControlExtension.ExtensionSentinel:
        controlExtension = new GifControlExtension(in);       // READ 6 BYTE
        return true;
      default: {
        String err = "Unknown gif extention %1 detected!"
          .replace("%1", HexTools.toHex(extension));
        System.out.println(err);
        unknownExtension = extension;
        return false;
      }
    }
  }

  private void saveSegments(OutputStream out) throws IOException {
    if (appExtension != null)
      appExtension.save(out);                                         // 19
    if (controlExtension != null)
      controlExtension.save(out);                                     // 8
    if (imageData != null)
      imageData.save(out);                                      // 11

    // write next unknown or termination segment identifier
    out.write(SEGMENT_SENTINEL);                                      // 1

    // Write unknown extension last. If there is any, no further segments are being read.
    if (unknownExtension != -1)
      out.write(unknownExtension);                                    // 1
  }

  // #endregion private helper routines

}
