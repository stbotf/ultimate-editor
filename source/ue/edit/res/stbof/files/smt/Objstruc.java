package ue.edit.res.stbof.files.smt;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_StellarTypes;
import ue.edit.exe.trek.seg.map.StellTypeGen;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

// Lists the available stellar object types and frequency.
public class Objstruc extends InternalFile {

  public static final int MaxGraphicsLen = 15;

  private short ENTRIES;
  private Stbof STBOF;
  private ArrayList<ObjEntry> ENTRY = new ArrayList<ObjEntry>();

  public Objstruc(Stbof st) {
    STBOF = st;
  }

  /**
   * @param in the input stream to read value from
   */
  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    // number of entries
    ENTRIES = StreamTools.readShort(in, true);

    // loop
    for (short i = 0; i < ENTRIES; ++i) {
      ENTRY.add(new ObjEntry(in));
    }

    in.close();
    markSaved();
  }

  /**
   * @param out the output stream to write to
   */
  @Override
  public void save(OutputStream out) throws IOException {
    //set up
    ENTRIES = (short) ENTRY.size();
    out.write(DataTools.toByte(ENTRIES, true));

    for (short i = 0; i < ENTRIES; i++) {
      ObjEntry oe = ENTRY.get(i);
      //index
      out.write(DataTools.toByte(i, true));
      //unknown
      out.write(DataTools.toByte(oe.unknown, true));
      //frequency
      out.write(DataTools.toByte(oe.FREQUENCY, true));
      //graphics
      out.write(DataTools.toByte(oe.GRAPH, MaxGraphicsLen+1, MaxGraphicsLen));
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    ENTRIES = 0;
    markChanged();
  }

  public float allFreq() {
    float flt = 0;

    for (int i = 0; i < ENTRY.size(); i++) {
      ObjEntry le = ENTRY.get(i);
      flt += le.FREQUENCY;
    }

    return flt;
  }

  public int numberOfEntries() {
    return ENTRY.size();
  }

  public boolean hasIndex(int index) {
    return index >= 0 && index < ENTRY.size();
  }

  public void add(short index) {
    ENTRY.add(index, new ObjEntry(index));
    markChanged();
  }

  public void remove(short index) {
    ENTRY.remove(index);
    markChanged();
  }

  public String getGraph(int index) {
    checkIndex(index);
    ObjEntry le = ENTRY.get(index);
    return le.GRAPH;
  }

  public boolean setGraph(int index, String ent) throws InvalidArgumentsException {
    checkIndex(index);
    if (ent != null && ent.length() > MaxGraphicsLen)
      throw new InvalidArgumentsException("Max graph length is exceeded!");

    ObjEntry le = ENTRY.get(index);
    if (!StringTools.equals(ent, le.GRAPH)) {
      le.GRAPH = ent;
      markChanged();
      return true;
    }

    return false;
  }

  public float getFreq(int index) {
    checkIndex(index);
    ObjEntry le = ENTRY.get(index);
    return le.FREQUENCY;
  }

  public boolean setFreq(int index, float ent) {
    checkIndex(index);

    ObjEntry le = ENTRY.get(index);
    if (le.FREQUENCY != ent) {
      le.FREQUENCY = ent;
      markChanged();
      return true;
    }

    return false;
  }

  public String[] getGraphList() {
    ArrayList<String> tut = new ArrayList<String>();
    int n = ENTRY.size();

    for (int i = 0; i < n; i++) {
      ObjEntry le = ENTRY.get(i);
      if ("neb-".equals(le.GRAPH)) { //$NON-NLS-1$
        tut.add(le.GRAPH + ".tga"); //$NON-NLS-1$
      } else {
        tut.add(le.GRAPH + ".ani"); //$NON-NLS-1$
      }
      tut.add(le.GRAPH + "map.tga"); //$NON-NLS-1$
      tut.add(le.GRAPH + "maps.tga"); //$NON-NLS-1$
    }

    return tut.toArray(new String[0]);
  }

  /**
   * @return the uncompressed size of the file
   **/
  @Override
  public int getSize() {
    int n = ENTRY.size();
    int size = 2 + n * 24;
    return size;
  }

  @Override
  public void check(Vector<String> response) {
    checkFrequency(response);

    for (int n = 0; n < numberOfEntries(); ++n) {
      try {
        if (getGraph(n).length() > MaxGraphicsLen) {
          String msg = Language.getString("Objstruc.2") //$NON-NLS-1$
            .replace("%1", Integer.toString(n)) //$NON-NLS-1$
            .replace("%2", getGraph(n)); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      } catch (Exception er) {
        String msg = Language.getString("Objstruc.3") //$NON-NLS-1$
          .replace("%1", er.getMessage()); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    }

    String[] pt = getGraphList();
    for (int i = 0; i < pt.length; i++) {
      if (pt[i].endsWith(".ani")) { //$NON-NLS-1$
        AniOrCur AOC = null;

        try {
          AOC = STBOF.getAnimation(pt[i], LoadFlags.UNCACHED);
        } catch (Exception fnf) {
          String msg = Language.getString("Objstruc.3") //$NON-NLS-1$
            .replace("%1", fnf.getMessage()); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }

        if (AOC == null) {
          String msg = Language.getString("Objstruc.4") //$NON-NLS-1$
            .replace("%1", pt[i]); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      } else {
        TargaImage tar = null;

        try {
          tar = STBOF.getTargaImage(pt[i], LoadFlags.UNCACHED);
        } catch (IOException fnf) {
          String msg = Language.getString("Objstruc.3") //$NON-NLS-1$
            .replace("%1", fnf.getMessage()); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }

        if (tar == null) {
          String msg = Language.getString("Objstruc.5") //$NON-NLS-1$
            .replace("%1", pt[i]); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        }
      }
    }
  }

  // in vanilla BotF, the galaxy generator has certain limitations
  // - 0: black hole + 1: x-ray pulsar + 2: nebula + 3: neutron star
  //   the first 4 entries together must have a frequency strictly below 1.0f,
  //   and not all of them must be zero, else the new game galaxy generation loops endless
  // - 4-9: star frequency is unrestricted, but not all must be zero (tested 0 to 1000.0f)
  // - 10+: further anomalies are unrestricted (tested 0 to 1000.0f)
  // - in sum all the frequencies together must be at least 0.995f
  private void checkFrequency(Vector<String> response) {
    Trek trek = UE.FILES.trek();
    StellTypeGen stellTypeGen = trek != null ? (StellTypeGen) trek.tryGetSegment(SD_StellarTypes.Gen, true) : null;
    float sysFreq = 0.0f;

    // check invalid frequencies
    for (int n = 0; n < numberOfEntries(); ++n) {
      try {
        float freq = getFreq(n);
        if (freq < 0) {
          String msg = Language.getString("Objstruc.1") //$NON-NLS-1$
            .replace("%1", Integer.toString(n)) //$NON-NLS-1$
            .replace("%2", Float.toString(freq)); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
        } else if (stellTypeGen != null && stellTypeGen.isSystem(n)) {
          // collect system frequencie
          sysFreq += freq;
        }
      } catch (Exception er) {
        String msg = Language.getString("Objstruc.3") //$NON-NLS-1$
          .replace("%1", er.getMessage()); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    }

    // check combined vanilla main anomaly frequencies
    try {
      float blackHoleFreq = getFreq(0);
      float xRayFreq = getFreq(1);
      float nebulaFreq = getFreq(2);
      float neutronStarFreq = getFreq(3);
      float sum = blackHoleFreq + xRayFreq + nebulaFreq + neutronStarFreq;
      if (sum <= 0.0f || sum >= 1.0f) {
        String msg = Language.getString("Objstruc.9") //$NON-NLS-1$
          .replace("%1", Float.toString(sum)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    } catch (Exception er) {
      String msg = Language.getString("Objstruc.3") //$NON-NLS-1$
        .replace("%1", er.getMessage()); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }

    // check system frequencies
    if (stellTypeGen != null && sysFreq <= 0.0f) {
      String msg = Language.getString("Objstruc.10") //$NON-NLS-1$
        .replace("%1", Float.toString(sysFreq)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }

    // check frequency sum
    float freq = allFreq();
    if (freq != 1.0f) {
      if (freq < 0.995f) {
        // all below 0.995f crash
        String msg = Language.getString("Objstruc.7") //$NON-NLS-1$
          .replace("%1", Float.toString(freq)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      } else {
        // warn if deviation is greater than 0.01f
        float diff = Math.abs(1f - freq);
        String msg = Language.getString("Objstruc.8") //$NON-NLS-1$
          .replace("%1", Float.toString(freq)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(diff > 0.01f ?
          INTEGRITY_CHECK_WARNING : INTEGRITY_CHECK_INFO, msg));
      }
    }
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("Objstruc.6") //$NON-NLS-1$
        .replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private class ObjEntry {

    @SuppressWarnings("unused")
    public short INDEX;         //2
    public short unknown = 0;   //2
    public float FREQUENCY = 0; //4
    public String GRAPH;        //16

    public ObjEntry(short index) {
      INDEX = index;
    }

    public ObjEntry(InputStream in) throws IOException {
      INDEX = StreamTools.readShort(in, true);
      unknown = StreamTools.readShort(in, true);
      FREQUENCY = StreamTools.readFloat(in, true);
      GRAPH = StreamTools.readNullTerminatedBotfString(in, 16);
    }
  }
}
