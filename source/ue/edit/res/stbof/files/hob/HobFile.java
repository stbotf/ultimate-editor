package ue.edit.res.stbof.files.hob;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class represents the file format *.hob. The hob files contain the models of ships and other
 * objects. If anyone has ANY useful info on this file, please contact me.
 */
public class HobFile extends InternalFile {

  private int FILE_SIZE;                    //   0 -   3 (4 bytes)
  private int TEXTURE_ID_ADDRESS;           //   4 -   7 (4 bytes)
  private byte[] UNKNOWN_1 = new byte[16];  //   8 -  23 (16 bytes)
  private int TEXTURE_NUM;                  //  24 -  27 (4 bytes)
  private byte[] UNKNOWN_2 = new byte[52];  //  28 -  79 (52 bytes)
  private float[] MIN_XYZ = new float[3];   //  80 -  91 (12 bytes)
  private float[] MAX_XYZ = new float[3];   //  92 - 103 (12 bytes)
  private float MODEL_SIZE;                 // 104 - 107 (4 bytes)
  private byte[] UNKNOWN_3;                 // 108 - ??? (texture_id_address - 108 bytes)
  private int[] TEXTURE_ID;                 // ??? - ??? (texture_num*4 bytes)
  private byte[] UNKNOWN_4;                 // ??? - EOF

  public static String[] hobFiles(String hobName) {
    String[] fileNames = new String[3];
    for (int i = 0; i < 3; ++i) {
      char c = (char)('a' + i); //$NON-NLS-1$
      fileNames[i] = hobName + "_" + c + ".hob"; //$NON-NLS-1$ //$NON-NLS-2$
    }
    return fileNames;
  }

  public HobFile() {
  }

  public HobFile(HobFile hob) {
    NAME = hob.NAME;
    FILE_SIZE = hob.FILE_SIZE;
    TEXTURE_ID_ADDRESS = hob.TEXTURE_ID_ADDRESS;
    System.arraycopy(hob.UNKNOWN_1, 0, UNKNOWN_1, 0, UNKNOWN_1.length);
    TEXTURE_NUM = hob.TEXTURE_NUM;
    System.arraycopy(hob.UNKNOWN_2, 0, UNKNOWN_2, 0, UNKNOWN_2.length);
    System.arraycopy(hob.MIN_XYZ, 0, MIN_XYZ, 0, MIN_XYZ.length);
    System.arraycopy(hob.MAX_XYZ, 0, MAX_XYZ, 0, MAX_XYZ.length);
    MODEL_SIZE = hob.MODEL_SIZE;
    UNKNOWN_3 = hob.UNKNOWN_3.clone();
    TEXTURE_ID = hob.TEXTURE_ID.clone();
    UNKNOWN_4 = hob.UNKNOWN_4.clone();
  }

  public HobFile(String name, InputStream in) throws IOException {
    NAME = name;
    load(in);
  }

  public HobFile(String name, byte[] data) throws IOException {
    NAME = name;
    load(data);
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    //get file size
    FILE_SIZE = StreamTools.readInt(in, true);
    //get position where texture id's are found
    TEXTURE_ID_ADDRESS = StreamTools.readInt(in, true);
    if (TEXTURE_ID_ADDRESS < 8) {
      String msg = Language.getString("HobFile.5") //$NON-NLS-1$
        .replace("%1", getName()); //$NON-NLS-1$
      throw new IOException(msg);
    }
    //unknown 1
    StreamTools.read(in, UNKNOWN_1);
    //number of texture id's
    TEXTURE_NUM = StreamTools.readInt(in, true);
    if (TEXTURE_NUM < 0) {
      String msg = Language.getString("HobFile.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(TEXTURE_NUM)); //$NON-NLS-1$
      throw new IOException(msg);
    }
    //unknown 2
    StreamTools.read(in, UNKNOWN_2);
    // minimum x,y,z (placement)
    StreamTools.read(in, MIN_XYZ, true);
    // maximum x,y,z (placement)
    StreamTools.read(in, MAX_XYZ, true);
    //model size (radius)
    MODEL_SIZE = StreamTools.readFloat(in, true);
    //unknown 3
    UNKNOWN_3 = StreamTools.readBytes(in, TEXTURE_ID_ADDRESS - 108);
    //texture id's
    TEXTURE_ID = new int[TEXTURE_NUM];
    for (int i = 0; i < TEXTURE_NUM; i++) {
      TEXTURE_ID[i] = StreamTools.readInt(in, true);
    }
    //unknown 4
    UNKNOWN_4 = StreamTools.readAllBytes(in);

    markSaved();
  }

  /**
   * Used to save changes.
   *
   * @param out the OutputStream where the data should be written to
   * @return true if data has been written
   */
  @Override
  public void save(OutputStream out) throws IOException {
    //file size
    out.write(DataTools.toByte(FILE_SIZE, true));
    //position where texture id's are found
    out.write(DataTools.toByte(TEXTURE_ID_ADDRESS, true));
    //unknown 1
    out.write(UNKNOWN_1);
    //number of texture id's
    out.write(DataTools.toByte(TEXTURE_NUM, true));
    //unknown 2
    out.write(UNKNOWN_2);
    //minimal x,y,z
    for (int i = 0; i < 3; i++) {
      out.write(DataTools.toByte(MIN_XYZ[i], true));
    }
    //maximal x,y,z
    for (int i = 0; i < 3; i++) {
      out.write(DataTools.toByte(MAX_XYZ[i], true));
    }
    //model size
    out.write(DataTools.toByte(MODEL_SIZE, true));
    //unknown 3
    out.write(UNKNOWN_3);
    //texture id's
    for (int i = 0; i < TEXTURE_NUM; i++) {
      out.write(DataTools.toByte(TEXTURE_ID[i], true));
    }
    //unknown 4
    out.write(UNKNOWN_4);
  }

  @Override
  public void clear() {
    FILE_SIZE = 0;
    TEXTURE_ID_ADDRESS = 0;
    Arrays.fill(UNKNOWN_1, (byte)0);
    TEXTURE_NUM = 0;
    Arrays.fill(UNKNOWN_2, (byte)0);
    Arrays.fill(MIN_XYZ, 0.0f);
    Arrays.fill(MAX_XYZ, 0.0f);
    MODEL_SIZE = 0;
    UNKNOWN_3 = null;
    TEXTURE_ID = null;
    UNKNOWN_4 = null;
    markChanged();
  }

  private int unk3Size() {
    return UNKNOWN_3 != null ? UNKNOWN_3.length : 0;
  }

  private int unk4Size() {
    return UNKNOWN_4 != null ? UNKNOWN_4.length : 0;
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return 108 + unk3Size() + 4 * TEXTURE_NUM + unk4Size();
  }

  /**
   * @return the number of textures used by this model.
   */
  public int numberOfTextures() {
    return TEXTURE_NUM;
  }

  /**
   * @return the model size (radius).
   */
  public float getModelSize() {
    return MODEL_SIZE;
  }

  /**
   * Sets the model size.
   */
  public void setModelSize(float size) throws InvalidArgumentsException {
    if (size < 0) {
      String msg = Language.getString("HobFile.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Float.toString(size)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (Math.abs(Float.compare(MODEL_SIZE, size)) >= 0.01f) {
      MODEL_SIZE = size;
      markChanged();
    }
  }

  /**
   * @return the placement (min_x, max_x, min_y, max_y, min_z, max_z <- in this order).
   */
  public float[] getModelPlacement() {
    float[] ret = new float[6];
    for (int i = 0; i < 3; i++) {
      ret[2 * i] = MIN_XYZ[i];
      ret[2 * i + 1] = MAX_XYZ[i];
    }
    return ret;
  }

  /**
   * Sets the model placement.  (min_x, max_x, min_y, max_y, min_z, max_z <- in this order)
   */
  public void setModelPlacement(float[] place) throws InvalidArgumentsException {
    if (place.length < 6) {
      String msg = Language.getString("HobFile.2"); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    for (int i = 0; i < 3; i++) {
      if (Math.abs(Float.compare(MIN_XYZ[i], place[2 * i])) > 0.01f) {
        MIN_XYZ[i] = place[2 * i];
        markChanged();
      }
      if (Math.abs(Float.compare(MAX_XYZ[i], place[2 * i + 1])) > 0.01f) {
        MAX_XYZ[i] = place[2 * i + 1];
        markChanged();
      }
    }
  }

  /**
   * @return the textures id.
   */
  public int getTextureID(int index) {
    if (index < 0 || index >= TEXTURE_NUM) {
      String msg = Language.getString("HobFile.3"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
    return TEXTURE_ID[index];
  }

  /**
   * Set the textures id.
   */
  public void setTextureID(int index, int newID) {
    if (index < 0 || index >= TEXTURE_NUM) {
      String msg = Language.getString("HobFile.3"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }

    if (TEXTURE_ID[index] != newID) {
      TEXTURE_ID[index] = newID;
      markChanged();
    }
  }

  @Override
  public void check(Vector<String> response) {
    if (MODEL_SIZE <= 0) {
      String msg = Language.getString("HobFile.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Float.toString(MODEL_SIZE)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }
  }
}
