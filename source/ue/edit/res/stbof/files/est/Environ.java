package ue.edit.res.stbof.files.est;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

/**
 * This class represents the file environ.est, which contains data on types of environments - planet
 * types. This file is still a mystery. In an unmodded game there are 13! types listed here, BUT
 * BotF ignores some of them for unknown reason. It seems though BotF can handle up to 32 planet
 * types, since there are 32 slots in total used for frequencies. Until there's an answer to the
 * question why BotF ignores types and how to get around that - adding planet types will not be
 * implemented. And be warned: changing anything here may have global consequences - if the indexes
 * get mixed up - all hell will break loose ;)
 */
public class Environ extends InternalFile {

  private ArrayList<EnvEntry> ENTRY = new ArrayList<EnvEntry>();

  public Environ(Stbof stbof) {
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    // read number of entries
    short numEntries = StreamTools.readShort(in, true);

    // read frequencies
    for (int i = 0; i < numEntries; i++) {
      val ent = new EnvEntry();
      ent.loadFreq(in);
      ENTRY.add(ent);
    }

    // skip empty slots
    if (numEntries < 32)
      in.skip(Float.BYTES * (32 - numEntries));

    // go thru all entries
    for (short i = 0; i < numEntries; ++i) {
      val entry = ENTRY.get(i);
      entry.loadEntry(in);
      // override index to ensure they are in sync with the ArrayList
      entry.setIndex(i);
    }

    markSaved();
  }

  /**
   * Used to save changes.
   *
   * @param out the OutputStream where the data should be written to
   * @return true if data has been written
   */
  @Override
  public void save(OutputStream out) throws IOException {
    // number of entries
    short numEntries = (short) ENTRY.size();
    out.write(DataTools.toByte(numEntries, true));

    // frequencies
    for (short i = 0; i < numEntries; i++) {
      EnvEntry en = ENTRY.get(i);
      en.saveFreq(out);
    }

    // fill empty slots
    for (short i = numEntries; i < 32; i++) {
      out.write(DataTools.toByte(0f, true));
    }

    // entries
    for (short i = 0; i < numEntries; i++) {
      EnvEntry en = ENTRY.get(i);
      en.saveEntry(out);
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return (32 * 4) + 2 + (numEntries() * 50);
  }

  @Override
  public void check(Vector<String> response) {
    if (ENTRY.size() == 0) {
      String msg = Language.getString("Environ.0"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      response.add(msg);
    }
    if (getFreqSum() != 1) {
      String msg = Language.getString("Environ.1"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      response.add(msg);
    }
    for (EnvEntry entry : ENTRY) {
      if (entry.FREQ < 0) {
        String msg = Language.getString("Environ.2"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%1", Float.toString(entry.FREQ)); //$NON-NLS-1$
        response.add(msg);
      }
    }
  }

  public int numEntries() {
    return ENTRY.size();
  }

  public int numPlanetTypes() {
    return (int) ENTRY.stream().filter(x -> x.FREQ > 0).count();
  }

  /**
   * Sets the name of a planet type.
   *
   * @param index the index of the planet type to be changed
   * @param name  the new name which will replace the old one
   * @return true if the name was changed
   */
  public boolean setName(int index, String name) {
    if (name.length() > 39)
      return false;

    checkIndex(index);
    EnvEntry en = ENTRY.get(index);

    if (!StringTools.equals(en.NAME, name)) {
      en.NAME = name;
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Gets the name of a planet type.
   *
   * @param index the index of the planet type, whose name is to be returned
   * @return the name of the planet type
   */
  public String getName(int index){
    checkIndex(index);
    EnvEntry en = ENTRY.get(index);
    return en.NAME;
  }

  public ID[] planetGfx() {
    return ENTRY.stream().filter(x -> x.FREQ > 0).map(x -> new ID(x.NAME, x.INDEX)).toArray(ID[]::new);
  }

  public ID[] atmosphereGfx() {
    return ENTRY.stream().filter(x -> x.FREQ == 0).map(x -> new ID(x.NAME, x.INDEX)).toArray(ID[]::new);
  }

  /**
   * Sets the frequency of a planet type.
   *
   * @param index the index of the planet type whose frequency is to be changed
   * @param freq  the new frequency which will replace the old one
   * @return true if the frequency was changed
   */
  public boolean setFrequency(int index, float freq) {
    checkIndex(index);
    EnvEntry en = ENTRY.get(index);
    if (en.FREQ != freq) {
      en.FREQ = freq;
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Gets the frequency of a planet type.
   *
   * @param index the index of the planet type, whose frequency is to be returned
   * @return the frequency of the planet type
   */
  public float getFrequency(int index) {
    checkIndex(index);
    return ENTRY.get(index).FREQ;
  }

  // Lookup planet type & atmosphere frequencies by name,
  // for in case new planet types are added. These include:
  // "methane", "sulfric", "none", "oxygen rich", "thin oxygen"
  public float getFrequency(String name) {
    return ENTRY.stream().filter(x -> StringTools.equals(x.NAME, name))
      .map(x -> x.FREQ).findFirst().orElse(0.0f);
  }

  /**
   * Return the sum of all frequencies.
   */
  public float getFreqSum() {
    float ret = 0;
    for (EnvEntry entry : ENTRY) {
      ret += entry.FREQ;
    }
    return ret;
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("Environ.3"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private class EnvEntry implements Comparable<EnvEntry> {

    // entry properies
    public String NAME;
    public short INDEX;
    // some signed byte values, e.g. -25, -75, -50, -85 (for oxygen rich)
    private byte[] unknown_1;
    // some short type value, e.g. 18000 (for oxygen rich)
    private short unknown_2;
    // some short type value, e.g. 10000 (for oxygen rich)
    private short unknown_3;

    // frequency
    public float FREQ;

    public EnvEntry() {
    }

    public void setIndex(short id) {
      if (INDEX != id) {
        INDEX = id;
        markChanged();
      }
    }

    public void loadFreq(InputStream in) throws IOException {
      FREQ = StreamTools.readFloat(in, true);
    }

    public void loadEntry(InputStream in) throws IOException {
      NAME = StreamTools.readNullTerminatedBotfString(in, 40);
      //index 40-2
      INDEX = StreamTools.readShort(in, true);
      //unknown 42-8
      unknown_1 = StreamTools.readBytes(in, 4);
      unknown_2 = StreamTools.readShort(in, true);
      unknown_3 = StreamTools.readShort(in, true);
    }

    public void saveFreq(OutputStream out) throws IOException {
      out.write(DataTools.toByte(FREQ, true));
    }

    public void saveEntry(OutputStream out) throws IOException {
      out.write(DataTools.toByte(NAME, 40, 39));
      out.write(DataTools.toByte(INDEX, true));
      out.write(unknown_1);
      out.write(DataTools.toByte(unknown_2, true));
      out.write(DataTools.toByte(unknown_3, true));
    }

    @Override
    public int compareTo(EnvEntry en) {
      return (INDEX - en.INDEX);
    }

    @Override
    public boolean equals(Object o) {
      if (o instanceof EnvEntry) {
        EnvEntry en = (EnvEntry) o;
        return en.INDEX == INDEX;
      }

      return false;
    }

    @Override
    public int hashCode() {
      return INDEX;
    }
  }
}
