package ue.edit.res.stbof.files.tga;

import java.awt.Dimension;
import java.awt.image.DirectColorModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.AccessLevel;
import lombok.Getter;
import ue.exception.UnsupportedImage;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.IsCloneable;
import ue.util.img.ImageConversion;
import ue.util.stream.StreamTools;

/*
+---------------------------------------------------------------------+
|                                TargaImage                           |
+---------------------------------------------------------------------+
 */

@Getter
public class TargaHeader implements IsCloneable {

  // #region constants

  public static final DirectColorModel COLOR_MODEL_15BIT = new DirectColorModel(15, ImageConversion.COLOR_MASK_16BIT_RED,
    ImageConversion.COLOR_MASK_16BIT_GREEN, ImageConversion.COLOR_MASK_16BIT_BLUE);
  public static final DirectColorModel COLOR_MODEL_16BIT = new DirectColorModel(16, ImageConversion.COLOR_MASK_16BIT_RED,
    ImageConversion.COLOR_MASK_16BIT_GREEN, ImageConversion.COLOR_MASK_16BIT_BLUE, ImageConversion.COLOR_MASK_16BIT_ALPHA);
  public static final DirectColorModel COLOR_MODEL_24BIT = new DirectColorModel(24, ImageConversion.COLOR_MASK_32BIT_RED,
    ImageConversion.COLOR_MASK_32BIT_GREEN, ImageConversion.COLOR_MASK_32BIT_BLUE);
  public static final DirectColorModel COLOR_MODEL_32BIT = new DirectColorModel(32, ImageConversion.COLOR_MASK_32BIT_RED,
    ImageConversion.COLOR_MASK_32BIT_GREEN, ImageConversion.COLOR_MASK_32BIT_BLUE, ImageConversion.COLOR_MASK_32BIT_ALPHA);
  public static final int ATTRIBUTES_BIT_MASK = 3;
  public static final int FIXED_SIZE = 18;
  private final String errTYPE = Language.getString("TargaHeader.1"); //$NON-NLS-1$

  // #endregion constants

  // #region fields

  private int idLength;           // 1
  private int colorMapType;       // 2
  private int imageType;          // 3
  private int cMapStart;          // 5
  private int cMapLength;         // 7
  private int cMapDepth;          // 8
  private int xOffset;            // 10
  private int yOffset;            // 12
  private int width;              // 14
  private int height;             // 16
  private int pixelDepth;         // 17
  private int imageDescriptor;    // 18
  private DirectColorModel cm = COLOR_MODEL_16BIT;

  @Getter(AccessLevel.NONE)
  private TargaImage image;

  // #endregion fields

  // #region constructor

  public TargaHeader(TargaImage image) {
    this.image = image;
    imageType = 2;
    pixelDepth = 16;
    imageDescriptor = 1;
    cm = COLOR_MODEL_16BIT;
  }

  public TargaHeader(TargaImage image, int width, int height) {
    this(image);
    this.width = width;
    this.height = height;
  }

  public TargaHeader(TargaImage image, File srcFile) throws IOException {
    this.image = image;
    try (FileInputStream fis = new FileInputStream(srcFile)) {
      load(fis);
    }
  }

  public TargaHeader(TargaImage image, InputStream in) throws IOException {
    this.image = image;
    load(in);
  }

  // #endregion constructor

  // #region properties

  public Dimension getImageSize() {
    return new Dimension(width, height);
  }

  public int getPixelBytes() {
    return ImageConversion.bitDepthBytes(pixelDepth);
  }

  public static DirectColorModel getColorModel(int pixelDepth) throws UnsupportedImage {
    switch (pixelDepth) {
      case 15:
        return COLOR_MODEL_15BIT;
      case 16:
        return COLOR_MODEL_16BIT;
      case 24:
        return COLOR_MODEL_24BIT;
      case 32:
        return COLOR_MODEL_32BIT;
      default:
        String err = Language.getString("TargaHeader.0"); //$NON-NLS-1$
        throw new UnsupportedImage(err.replace("%1", Integer.toString(pixelDepth))); //$NON-NLS-1$
    }
  }

  public DirectColorModel getColorModel(boolean alpha) {
    return alpha ? cm : pixelDepth <= 16 ? COLOR_MODEL_15BIT : COLOR_MODEL_24BIT;
  }

  public int getAttributeBits() {
    return imageDescriptor & ATTRIBUTES_BIT_MASK;
  }

  public int redMask() {
    return pixelDepth <= 16 ? ImageConversion.COLOR_MASK_16BIT_RED : ImageConversion.COLOR_MASK_32BIT_RED;
  }

  public int greenMask() {
    return pixelDepth <= 16 ? ImageConversion.COLOR_MASK_16BIT_GREEN : ImageConversion.COLOR_MASK_32BIT_GREEN;
  }

  public int blueMask() {
    return pixelDepth <= 16 ? ImageConversion.COLOR_MASK_16BIT_BLUE : ImageConversion.COLOR_MASK_32BIT_BLUE;
  }

  public int alphaMask() {
    return pixelDepth <= 16 ? ImageConversion.COLOR_MASK_16BIT_ALPHA : ImageConversion.COLOR_MASK_32BIT_ALPHA;
  }

  public int rgbMask() {
    return pixelDepth <= 16 ? ImageConversion.COLOR_MASK_16BIT_RGB : ImageConversion.COLOR_MASK_32BIT_RGB;
  }

  public int redShift() {
    return pixelDepth <= 16 ? 10 : 16;
  }

  public int greenShift() {
    return pixelDepth <= 16 ? 5 : 8;
  }

  public int blueShift() {
    return 0;
  }

  public int alphaShift() {
    return pixelDepth <= 16 ? 15 : 24;
  }

  // #endregion properties

  // #region editing


  public void setWidth(int width) {
    if (this.width != width) {
      this.width = width;
      image.markChanged();
    }
  }

  public void setHeight(int height) {
    if (this.height != height) {
      this.height = height;
      image.markChanged();
    }
  }

  public void setImageSize(int width, int height) {
    if (this.width != width || this.height != height) {
      this.width = width;
      this.height = height;
      image.markChanged();
    }
  }

  public void setPixelDepth(int bitDepth) throws UnsupportedImage {
    if (this.pixelDepth != bitDepth) {
      cm = getColorModel(pixelDepth);
      this.pixelDepth = bitDepth;
      image.markChanged();
    }
  }

  public void setImageDescriptor(int descriptor) {
    if (this.imageDescriptor != descriptor) {
      this.imageDescriptor = descriptor;
      image.markChanged();
    }
  }

  // #endregion editing

  // #region load

  public void load(InputStream in) throws IOException {
    /* --- read targa header info --- */
    idLength = StreamTools.readUByte(in);
    colorMapType = StreamTools.readUByte(in);
    imageType = StreamTools.readUByte(in);
    if (imageType != 2)
      throw new UnsupportedImage(errTYPE.replace("%1", Integer.toString(imageType))); //$NON-NLS-1$

    cMapStart = StreamTools.readUShort(in, true);
    cMapLength = StreamTools.readUShort(in, true);
    cMapDepth = StreamTools.readUByte(in);
    xOffset = StreamTools.readUShort(in, true);
    yOffset = StreamTools.readUShort(in, true);
    width = StreamTools.readUShort(in, true);
    height = StreamTools.readUShort(in, true);
    pixelDepth = StreamTools.readUByte(in);

    cm = getColorModel(pixelDepth);
    imageDescriptor = StreamTools.readUByte(in);

    /* --- skip over image id info (if present) --- */
    if (idLength > 0)
      StreamTools.skip(in, idLength);
  }

  // #endregion load

  // #region save

  public void save(OutputStream fos) throws IOException {
    save(fos, pixelDepth);
  }

  /**
   * saves image header with specified bit depth
   */
  public void save(OutputStream fos, int pixelDepth) throws IOException {
    //header
    fos.write(idLength);                                    // 1
    fos.write(colorMapType);                                // 2
    fos.write(imageType);                                   // 3
    fos.write(DataTools.toByte((short) cMapStart, true));   // 5
    fos.write(DataTools.toByte((short) cMapLength, true));  // 7
    fos.write(cMapDepth);                                   // 8
    fos.write(DataTools.toByte((short) xOffset, true));     // 10
    fos.write(DataTools.toByte((short) yOffset, true));     // 12
    fos.write(DataTools.toByte((short) width, true));       // 14
    fos.write(DataTools.toByte((short) height, true));      // 16
    fos.write(pixelDepth);                                  // 17
    fos.write(imageDescriptor);                             // 18
  }

  // #endregion save

  // #region maintenance

  public int getSize() {
    return (FIXED_SIZE + idLength);
  }

  @Override
  public TargaHeader clone() {
    try {
      return (TargaHeader) super.clone();
    }
    catch (CloneNotSupportedException e) {
      // cloneable is implemented so it must not be thrown
      throw new RuntimeException(e);
    }
  }

  public void clear() {
    idLength = 0;
    colorMapType = 0;
    imageType = 0;
    cMapStart = 0;
    cMapLength = 0;
    cMapDepth = 0;
    xOffset = 0;
    yOffset = 0;
    width = 0;
    height = 0;
    pixelDepth = 0;
    imageDescriptor = 0;
    cm = COLOR_MODEL_16BIT;
  }

  // #endregion maintenance

}
