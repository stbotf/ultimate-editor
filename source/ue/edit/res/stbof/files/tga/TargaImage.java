package ue.edit.res.stbof.files.tga;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.exception.UnsupportedImage;
import ue.gui.util.gfx.GraphicTools;
import ue.gui.util.gfx.OcclusionColorModel;
import ue.service.Language;
import ue.util.data.IsCloneable;
import ue.util.img.ImageConversion;
import ue.util.img.PixelBuffer;
import ue.util.stream.StreamTools;

/*
+---------------------------------------------------------------------+
|                                TargaImage                           |
+---------------------------------------------------------------------+
 */
public class TargaImage extends InternalFile implements IsCloneable {

  public enum AlphaMode {
    Opaque,
    AlphaChannel,
    FirstPixelAlpha
  }

  @Getter private TargaHeader header = new TargaHeader(this); // 18
  private PixelBuffer pixelBuffer;
  private byte[] meta = null; // creator details & meta data
  private boolean hasAlpha16 = false;

  public TargaImage() {
  }

  public TargaImage(int width, int height, int bitDepth, boolean hasAlpha) throws UnsupportedImage {
    header.setWidth(width);
    header.setHeight(height);
    header.setPixelDepth(bitDepth);
    if (hasAlpha) {
      header.setImageDescriptor(1);
      hasAlpha16 = true;
    }
    pixelBuffer = PixelBuffer.allocate(width, height, ImageConversion.bitDepthBytes(bitDepth));
  }

  // create 16bit TGA, with lines auto-inverted on save
  public TargaImage(short[] pixels, int width, int height, boolean hasAlpha) throws UnsupportedImage {
    this(width, height, 16, hasAlpha);
    if (pixels == null)
      throw new UnsupportedImage(Language.getString("TargaImage.0")); //$NON-NLS-1$

    // load buffer
    pixelBuffer = PixelBuffer.wrap(pixels, width, height);
  }

  // create 32bit TGA, with lines auto-inverted on save
  public TargaImage(int[] pixels, int width, int height, boolean hasAlpha) throws UnsupportedImage {
    this(width, height, 32, hasAlpha);
    if (pixels == null)
      throw new UnsupportedImage(Language.getString("TargaImage.0")); //$NON-NLS-1$

    // load buffer
    pixelBuffer = PixelBuffer.wrap(pixels, width, height);
  }

  public TargaImage(File srcFile) throws IOException {
    try (FileInputStream fis = new FileInputStream(srcFile)) {
      load(fis);
    }
  }

  public TargaImage(String name, InputStream in) throws IOException {
    this.NAME = name;
    load(in);
  }

  public TargaImage(String name, byte[] data) throws IOException {
    this.NAME = name;
    load(data);
  }

  @Override
  public void load(InputStream in) throws IOException {
    // read targa header info
    header.load(in);

    int pixelDepth = header.getPixelDepth();
    int pixelBytes = pixelDepth >>> 3;
    int width = header.getWidth();
    int height = header.getHeight();

    // read inverted pixel data lines
    pixelBuffer = new PixelBuffer(width, height, pixelBytes);
    pixelBuffer.load(in, true);

    meta = StreamTools.readAllBytes(in);
    markSaved();
  }

  public int getPixelCount() {
    return pixelBuffer.getPixelCount();
  }

  public int getRawPixel(int index) {
    return pixelBuffer.getPixel(index);
  }

  public int getRawPixelRed(int index) {
    return (getRawPixel(index) & header.redMask()) >>> header.redShift();
  }

  public int getRawPixelGreen(int index) {
    return (getRawPixel(index) & header.greenMask()) >>> header.greenShift();
  }

  public int getRawPixelBlue(int index) {
    return (getRawPixel(index) & header.blueMask()) >>> header.blueShift();
  }

  public int getRawPixelAlpha(int index) {
    return (getRawPixel(index) & header.alphaMask()) >>> header.alphaShift();
  }

  public int rawColorPixel(int red, int green, int blue, int alpha) {
    return red << header.redShift() | green << header.greenShift() | blue << header.blueShift() | alpha << header.alphaShift();
  }

  public void resize(int width, int height) {
    resize(width, height, 0, true);
  }

  public void resize(int width, int height, int fillColor, boolean center) {
    pixelBuffer = pixelBuffer.resizedCopy(width, height, fillColor, center);
    header.setImageSize(width, height);
  }

  public TargaImage resizedCopy(int width, int height) {
    val image = new TargaImage();
    image.header = header.clone();
    header.setImageSize(width, height);
    image.meta = meta != null ? Arrays.copyOf(meta, meta.length) : null;
    image.hasAlpha16 = hasAlpha16;
    image.pixelBuffer = pixelBuffer.resizedCopy(width, height, 0, true);
    return image;
  }

  public void setRawPixel(int index, int pixel) {
    pixelBuffer.setPixel(index, pixel);
    markChanged();
  }

  /**
   * BotF 16bit TGA transparency fixture.
   * DCER: On some images the top left pixel color is treated as transparent by botf.
   * Since this is happening with cursors and animations (as far as I'm aware), I thought
   * UE should do the same so modders get a sense of how it'll look in game minus the background.
   * @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=23233#p23233
   */
  public void convertFirstPixelAlpha(boolean force) {
    // skip if transparent color is already set
    int firstPixel = getRawPixel(0);
    if (force || (firstPixel & header.alphaMask()) != 0) {
      // use first pixel as transparency mask
      applyAlphaColor(firstPixel, force);
    }
  }

  public void applyAlphaColor(int alphaColor, boolean force) {
    // only relevant to 16-bit TGA files
    if (!force && header.getPixelDepth() != 16)
      return;
    // skip to convert when overlay bit is used
    if (!force && hasAlpha16)
      return;

    // ignore current alpha
    int rgbMask = header.rgbMask();
    int alphaMask = header.alphaMask();
    alphaColor &= rgbMask;

    for (int i = 0; i < pixelBuffer.getPixelCount(); ++i) {
      int pixel = getRawPixel(i);
      int alphaPixel = pixel & rgbMask;
      if (alphaPixel == alphaColor) {
        if (alphaPixel != pixel)
          setRawPixel(i, alphaPixel);
      } else {
        alphaPixel = pixel | alphaMask;
        if (alphaPixel != pixel)
          setRawPixel(i, alphaPixel);
      }
    }

    hasAlpha16 = true;
    markChanged();
  }

  @Override
  public TargaImage clone() {
    val image = new TargaImage();
    image.header = header.clone();
    image.pixelBuffer = pixelBuffer.clone();
    image.meta = meta != null ? Arrays.copyOf(meta, meta.length) : null;
    image.hasAlpha16 = hasAlpha16;
    return image;
  }

  @Override
  public void save(OutputStream fos) throws IOException {
    // header
    header.save(fos); // 18
    // inverted pixel lines
    pixelBuffer.save(fos, true);
    // meta data like authorship
    if (meta != null)
      fos.write(meta);
  }

  public void save(OutputStream fos, int bitDepth) throws IOException {
    header.save(fos, bitDepth); // 18
    // convert bit depth & inverted pixel lines
    pixelBuffer.save(fos, true, bitDepth);
    // meta data like authorship
    if (meta != null)
      fos.write(meta);
  }

  @Override
  public void clear() {
    header.clear();
    pixelBuffer = null;
    meta = null;
    markChanged();
  }

  public Image getImage(AlphaMode alphaMode) {
    ColorModel colorModel = getColorModel(alphaMode);
    return getImage(colorModel);
  }

  public Image getImage(int alphaColor) {
    ColorModel colorModel = getColorModel(alphaColor);
    return getImage(colorModel);
  }

  public Image getImage(ColorModel colorModel) {
    // set up an image from memory and return it
    return GraphicTools.createImage(pixelBuffer, colorModel);
  }

  public Image getThumbnail(int maxSize, boolean smooth, AlphaMode alphaMode) {
    ColorModel colorModel = getColorModel(alphaMode);
    return getThumbnail(maxSize, smooth, colorModel);
  }

  public Image getThumbnail(int maxSize, boolean smooth, int alphaColor) {
    ColorModel colorModel = getColorModel(alphaColor);
    return getThumbnail(maxSize, smooth, colorModel);
  }

  public Image getThumbnail(int maxSize, boolean smooth, ColorModel colorModel) {
    int width = pixelBuffer.getWidth();
    int height = pixelBuffer.getHeight();
    if (width <= maxSize && height <= maxSize)
      return GraphicTools.createImage(pixelBuffer, colorModel);

    // skip smooth if either extend does fit
    if ((width == maxSize) && (height == maxSize))
      smooth = false;

    int thumbnailWidth = 0;
    int thumbnailHeight = 0;

    // shrink to max size but keep image aspect ratio
    if (width >= height) {
      thumbnailWidth = maxSize;
      thumbnailHeight = (Math.round(((float) height / (float) width) * maxSize));
    } else {
      thumbnailWidth = (Math.round(((float) width / (float) height) * maxSize));
      thumbnailHeight = maxSize;
    }

    PixelBuffer thumbnailData = PixelBuffer.allocate(thumbnailWidth, thumbnailHeight, header.getPixelBytes());
    double multiplier = (double) width / (double) thumbnailWidth;

    for (int i = 0; i < thumbnailHeight; i++) {
      int srcY = (int) (i * multiplier);

      for (int j = 0; j < thumbnailWidth; j++) {
        int srcX = (int) (j * multiplier);

        // center pixel or smoothed neighbors
        int pixel = smooth ? smoothPixel(srcX, srcY)
          : getRawPixel((srcY * width) + srcX);

        int index = i * thumbnailWidth + j;
        thumbnailData.setPixel(index, pixel);
      }
    }

    // set up an image from memory and return it
    return GraphicTools.createImage(thumbnailData, colorModel);
  }

  public Dimension getImageSize() {
    return new Dimension(header.getWidth(), header.getHeight());
  }

  public int getImageType() {
    return header.getImageType();
  }

  public int getPixelDepth() {
    return header.getPixelDepth();
  }

  public boolean hasAlphaChannel() {
    return hasAlpha16 || header.getPixelDepth() == 32;
  }

  public ColorModel getColorModel(AlphaMode alphaMode) {
    switch (alphaMode) {
      case FirstPixelAlpha:
        return getColorModel(getRawPixel(0));
      case AlphaChannel:
        return header.getColorModel(hasAlphaChannel());
      default:
        return header.getColorModel(false);
    }
  }

  public ColorModel getColorModel(int alphaColor) {
    return new OcclusionColorModel(header.getPixelDepth(), header.redMask(),
      header.greenMask(), header.blueMask(), alphaColor);
  }

  public int getHeaderSize() {
    return header.getSize();
  }

  public int getDataSize() {
    return pixelBuffer.size() + meta.length;
  }

  @Override
  public int getSize() {
    return header.getSize() + getDataSize();
  }

  public void printImage(TargaImage image, int x_offset, int y_offset) {
    if (getPixelDepth() != image.getPixelDepth()) {
      String err = "Combined image pixel depth conversion is not supported.";
      throw new UnsupportedOperationException(err);
    }

    int bgWidth = header.getWidth();
    int bgHeight = header.getHeight();
    int imgWidth = image.header.getWidth();
    int imgHeight = image.header.getHeight();

    int line;
    int imgX, imgLine;

    // height
    for (int y2 = (bgHeight - 1); y2 >= y_offset; y2--) {
      if (y2 < 0)
        continue;

      line = y2 * bgWidth;
      imgLine = (y2 - y_offset) * imgWidth;
      if (imgLine < 0 || imgLine >= imgWidth * imgHeight)
        continue;

      // width
      for (int x2 = x_offset; x2 < bgWidth; x2++) {
        if (x2 < 0)
          continue;

        imgX = x2 - x_offset;
        if (imgX < 0 || imgX >= imgWidth)
          continue;

        // attention, no color conversion is applied here, so both must be same
        int imgXPixel = image.getRawPixel(imgLine + imgX);
        if (imgXPixel == image.getRawPixel(0))
          continue;

        setRawPixel(line + x2, imgXPixel);
      }
    }
  }

  @Override
  public void check(Vector<String> response) {
  }

  // Smoothing algorithm (nearest neighbor - t pattern)
  private int smoothPixel(int srcX, int srcY) {
    int width = header.getWidth();
    int height = header.getHeight();

    // Don't smooth as much if image is already square
    int smoothArea = width == height ? 1 : 2;

    int[] kernel = new int[5];
    // center pixel
    kernel[2] = getRawPixel((srcY * width) + srcX);
    // top smooth
    kernel[0] = (srcY - smoothArea) < 0 ? kernel[2]
      : getRawPixel(((srcY - smoothArea) * width) + srcX);
    // left smooth
    kernel[1] = (srcX - smoothArea) < 0 ? kernel[2]
      : getRawPixel((srcY * width) + srcX - smoothArea);
    // right smooth
    kernel[3] = (srcX + smoothArea) > (width - 1) ? kernel[2]
      : getRawPixel((srcY * width) + srcX + smoothArea);
    // bottom smooth
    kernel[4] = (srcY + smoothArea) > (height - 1) ? kernel[2]
      : getRawPixel(((srcY + smoothArea) * width) + srcX);

    int red = 0;
    int green = 0;
    int blue = 0;
    int alpha = 0;

    for (int k = 0; k < kernel.length; k++) {
      red += getRawPixelRed(kernel[k]);
      green += getRawPixelGreen(kernel[k]);
      blue += getRawPixelBlue(kernel[k]);
      alpha += getRawPixelAlpha(kernel[k]);
    }

    red /= kernel.length;
    green /= kernel.length;
    blue /= kernel.length;
    alpha /= kernel.length;
    return rawColorPixel(red, green, blue, alpha);
  }

}
