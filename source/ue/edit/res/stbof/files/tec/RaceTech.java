package ue.edit.res.stbof.files.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.tec.data.TechEntry;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// The race technology file lists tech entries with buildable structures
// and research progress for each technology level of each race.
// @see TechEntry
public class RaceTech extends InternalFile {

  private ArrayList<TechEntry> ENTRY = new ArrayList<TechEntry>();
  private int loadedBldMaskLongs = 0;
  private int bldMaskLongs = TechEntry.DEFAULT_BldMaskLongs;

  private Edifice edifice;

  public RaceTech(Stbof stbof) throws IOException {
    edifice = (Edifice) stbof.getInternalFile(CStbofFiles.EdificeBst, true);
  }

  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    // read first entry address but skip remaining address map
    final int entryStartAddress = StreamTools.readInt(in, true);
    StreamTools.skip(in, entryStartAddress - Integer.BYTES);

    // calculate entry count from address map entries
    final int entryCount = entryStartAddress / Integer.BYTES;
    // calculate entry size by dividing the remaining data
    final int entrySize = in.available() / entryCount;
    // calculate the building mask size
    loadedBldMaskLongs = bldMaskLongs = TechEntry.calcBuildMaskLongs(entrySize);

    while (in.available() > 0) {
      ENTRY.add(new TechEntry(this, in, loadedBldMaskLongs));
    }

    markSaved();
  }

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws InvalidArgumentsException
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    final int maxCnt = getMaxBuidings();
    final int num = edifice.getNumberOfEntries();

    // disable non-existing buildings
    if (num < maxCnt) {
      for (TechEntry entry : ENTRY) {
        for (int id = num; id < maxCnt; ++id) {
          entry.setAvailable(id, false);
        }
      }
    }

    // calculate first entry start address
    int entryAddress = ENTRY.size() * Integer.BYTES;
    // calculate entry size
    int entrySize = TechEntry.calcSize(bldMaskLongs);

    // write address map
    for (int i = 0; i < ENTRY.size(); i++) {
      out.write(DataTools.toByte(entryAddress, true));
      entryAddress += entrySize;
    }

    // write entries
    for (TechEntry tech : ENTRY) {
      // force to write the configured size and zero or cut the rest
      tech.save(out, bldMaskLongs);
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
    Vector<String> temp = new Vector<String>();

    int reqBldMaskLongs = edifice.getReqBldMaskLongs();
    if (reqBldMaskLongs > bldMaskLongs) {
      int numBuildings = edifice.getNumberOfEntries();
      int maxBuildings = reqBldMaskLongs * Long.BYTES * 8;
      String msg = Language.getString("RaceTech.3");
      msg = msg.replace("%1", Integer.toString(numBuildings));
      msg = msg.replace("%2", Integer.toString(maxBuildings));
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    for (int i = 0; i < ENTRY.size(); i++) {
      try {
        TechEntry test = ENTRY.get(i);
        temp = test.check(this);

        if (!temp.isEmpty())
          response.addAll(temp);
      } catch (Exception f) {
        String msg = Language.getString("RaceTech.0"); //$NON-NLS-1$
        msg = msg.replace("%1", Integer.toString(i)); //$NON-NLS-1$
        msg = msg.replace("%2", f.getMessage()); //$NON-NLS-1$
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
    }
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return ENTRY.size() * 112;
  }

  public int getBldMaskLongs() {
    return bldMaskLongs;
  }

  public int getMaxBuidings() {
    return bldMaskLongs * Long.BYTES * 8;
  }

  public short[] getAll() {
    short[] res = new short[ENTRY.size()];
    for (int i = 0; i < ENTRY.size(); i++) {
      TechEntry tec = ENTRY.get(i);
      res[i] = tec.raceId();
    }
    return res;
  }

  /**
   * @return number of entries in this file
   */
  public int getNumberOfEntries() {
    return ENTRY.size();
  }

  public TechEntry getTechEntry(int index) {
    checkIndex(index);
    return ENTRY.get(index);
  }

  /**
   * Shifts the build mask flags for the inserted building.
   * If the max supported building id is exceeded it is ignored.
   *
   * @param buildingId  the index of the inserted building
   * @param available   the availability of the inserted building
   */
  public void shiftBuildingIndices(int buildingId, boolean available) {
    // ignore if max buildings is exceeded
    int num = getMaxBuidings();
    if (buildingId >= num)
      return;

    for (TechEntry tech : ENTRY) {
      tech.shiftBuildings(buildingId, available);
    }
    markChanged();
  }

  /**
   * Shifts back build mask flags for the removed building.
   * If the max supported building id is exceeded it is ignored.
   *
   * @param buildingId  the index of the removed building
   */
  public void removeBuildingIndex(int buildingId) {
    // ignore if max buildings is exceeded
    int num = getMaxBuidings();
    if (buildingId >= num)
      return;

    for (TechEntry tech : ENTRY) {
      tech.removeBuilding(buildingId);
    }
    markChanged();
  }

  /**
   * Switches the given race tech availability indices.
   * Non-existent indices are ignored, availability is defaulted to false.
   *
   * @param idx1
   * @param idx2
   */
  public void switchBuildingIndex(int idx1, int idx2) {
    for (TechEntry tech : ENTRY) {
      tech.switchBuildingIndex(idx1, idx2);
    }
  }

  public void resizeBldMask() {
    int numBuildings = edifice.getNumberOfEntries();
    int reqBldMaskLongs = DataTools.ceilDiv(numBuildings, 64);

    // if not exceeded, stick to original size
    reqBldMaskLongs = Integer.max(reqBldMaskLongs, loadedBldMaskLongs);

    if (reqBldMaskLongs != bldMaskLongs) {
      for (TechEntry tech : ENTRY) {
        tech.resizeBldMask(reqBldMaskLongs);
      }
      bldMaskLongs = reqBldMaskLongs;
      markChanged();
    }
  }

  /**
   * Adjusting the build mask longs doesn't increase the build mask.
   * It however sets the actual size written on save.
   */
  public void adjustBldMaskLongs() {
    int numBuildings = edifice.getNumberOfEntries();
    int reqBldMaskLongs = DataTools.ceilDiv(numBuildings, 64);

    // if not exceeded, stick to original size
    reqBldMaskLongs = Integer.max(reqBldMaskLongs, loadedBldMaskLongs);

    if (reqBldMaskLongs != bldMaskLongs) {
      bldMaskLongs = reqBldMaskLongs;
      markChanged();
    }
  }


  /**
   * Adjusting the build mask longs doesn't increase the build mask.
   * It however sets the actual size written on save.
   */
  public void setBldMaskLongs(int bldMaskLongs) {
    if (this.bldMaskLongs != bldMaskLongs) {
      this.bldMaskLongs = bldMaskLongs;
      markChanged();
    }
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("RaceTech.2"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }
}
