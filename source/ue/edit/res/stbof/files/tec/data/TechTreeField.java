package ue.edit.res.stbof.files.tec.data;

import java.util.TreeMap;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.exception.KeyNotFoundException;
import ue.service.Language;
import ue.util.data.ID;

public class TechTreeField {

  TechTree techTree;
  int fieldIndex;

  // sort and index tech entries by the tech level
  private TreeMap<Integer, TechTreeEntry> entries = new TreeMap<Integer, TechTreeEntry>();

  public TechTreeField(TechTree techTree, int fieldIndex) {
    this.techTree = techTree;
    this.fieldIndex = fieldIndex;
  }

  public int getSize() {
    return entries.size() * 72;
  }

  // gets
  public int getMaxTechLevel() {
    return entries.isEmpty() ? 0 : entries.lastEntry().getValue().level();
  }

  /**
   * Returns a list of technology names and levels of this tech field.
   */
  public ID[] getTechList() {
    return entries.entrySet().stream().map(x -> new ID(x.getValue().getName(), x.getKey())).toArray(ID[]::new);
  }

  /**
   * Returns the number of techs
   */
  public int getNumberOfEntries() {
    return entries.size();
  }

  public TechTreeEntry getEntry(int level) throws KeyNotFoundException {
    TechTreeEntry te = entries.get(level);
    if (te == null) {
      String msg = Language.getString("TechTreeField.3"); //$NON-NLS-1$
      msg = msg.replace("%1", "(" + fieldIndex + ", " + level
          + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      throw new KeyNotFoundException(msg);
    }
    return te;
  }

  // sets
  public void add(TechTreeEntry techEntry) {
    entries.put(techEntry.level(), techEntry);
  }

  public TechTreeEntry add(String name, String desc) {
    TechTreeEntry newTech = null;

    if (entries.isEmpty()) {
      newTech = new TechTreeEntry(techTree, 0, name, (byte) fieldIndex, (byte) 0);
      entries.put(0, newTech);
    } else {
      TechTreeEntry lastTech = entries.lastEntry().getValue();
      newTech = new TechTreeEntry(lastTech);
      int level = lastTech.level() + 1;

      // fix
      newTech.setLevel(level);
      newTech.setName(name);

      entries.put(level, newTech);
    }

    newTech.setDesc(desc);

    return newTech;
  }

  public void removeEntry(int level) {
    if (entries.remove(level) != null) {
      techTree.markChanged();

      // reduce all higher tech levels by one
      int[] updates = entries.keySet().stream().filter(x -> x > level)
        .mapToInt(Integer::intValue).toArray();

      for(int u : updates) {
        TechTreeEntry lvl = entries.remove(u);
        lvl.setLevel(u - 1);
        entries.put(u - 1, lvl);
      }
    }
  }
}
