package ue.edit.res.stbof.files.tec.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.res.stbof.files.desc.Descriptable;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ValueCast;
import ue.util.stream.StreamTools;

public class TechTreeEntry extends Descriptable implements Comparable<TechTreeEntry> {

  @Getter @Accessors(fluent = true)
  private int index;  // unsigned short
  @Getter @Accessors(fluent = true)
  private String image;
  private byte field;
  private byte level;
  private byte[] unknown1 = new byte[3];
  @Getter @Accessors(fluent = true)
  private long cost;

  // constructors
  public TechTreeEntry(TechTree techTree, InputStream in) throws IOException {
    super(techTree);

    // name 0-40
    name = StreamTools.readNullTerminatedBotfString(in, 40);
    // index 40-2
    index = Short.toUnsignedInt(StreamTools.readShort(in, true));
    // image 42-13
    image = StreamTools.readNullTerminatedBotfString(in, 13);

    // field & level 55-2
    field = (byte)in.read();
    level = (byte)in.read();
    // unknown 57-3
    StreamTools.read(in, unknown1);
    // cost 60-8
    cost = StreamTools.readLong(in, true);
    // vertical 68-4
    loadDescAddress(in);
  }

  public TechTreeEntry(TechTree techTree, int index, String name, byte field, byte level) {
    super(techTree);
    this.name = name;
    this.index = index;
    this.field = field;
    this.level = level;
  }

  public TechTreeEntry(TechTreeEntry me) {
    super(me);
    name = me.name;
    index = me.index;
    image = me.image;
    field = me.field;
    level = me.level;
    cost = me.cost;
    unknown1 = me.unknown1;
  }

  public int field() {
    return Byte.toUnsignedInt(field);
  }

  public int level() {
    return Byte.toUnsignedInt(level);
  }

  public boolean setField(int field) {
    ValueCast.checkUByte(field);
    if (this.field != (byte) field) {
      this.field = (byte) field;
      markChanged();
      return true;
    }
    return false;
  }

  public boolean setLevel(int level) {
    ValueCast.checkUByte(level);
    if (this.level != (byte) level) {
      this.level = (byte) level;
      markChanged();
      return true;
    }
    return false;
  }

  public boolean setIndex(int index) {
    ValueCast.checkUShort(index);
    if (this.index != index) {
      this.index = index;
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Sets the image of a tech.
   * @param image the tech image
   * @return whether the image got changed
   */
  public boolean setImage(String image) {
    if (!this.image.equals(image)) {
      this.image = image;
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Sets the cost of a tech.
   * @param cost  the tech cost
   * @return whether the cost got changed
   */
  public boolean setCost(long cost) throws InvalidArgumentsException {
    if (cost < 0) {
      String msg = Language.getString("TechTreeEntry.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Long.toString(cost)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (this.cost != cost) {
      this.cost = cost;
      markChanged();
      return true;
    }
    return false;
  }

  // save
  @Override
  public void save(OutputStream out) throws IOException {
    // name
    out.write(DataTools.toByte(name, 40, 39));
    // index
    out.write(DataTools.toByte((short)index, true));
    // image
    out.write(DataTools.toByte(image, 13, 12));
    // field & level
    out.write(field);
    out.write(level);
    // unknown
    out.write(unknown1);
    // cost
    out.write(DataTools.toByte(cost, true));
    // description
    out.write(DataTools.toByte(descAddress, true));
  }

  @Override
  public int compareTo(TechTreeEntry arg) {
    // -1 if before, 0 is same and +1 if after
    return (field * 256 + level) - (arg.field * 256 + arg.level);
  }

  @Override
  public void check(Vector<String> response) {
    super.check(response);
    String fileName = file.getName();

    if (field < 0 || field > 7) {
      String msg = Language.getString("TechTreeEntry.1"); //$NON-NLS-1$
      msg = msg.replace("%1", fileName); //$NON-NLS-1$
      msg = msg.replace("%2", name); //$NON-NLS-1$
      msg = msg.replace("%3", Integer.toString(field)); //$NON-NLS-1$
      response.add(msg);
    }
    if (level < 0 || level > 100) {
      String msg = Language.getString("TechTreeEntry.2"); //$NON-NLS-1$
      msg = msg.replace("%1", fileName); //$NON-NLS-1$
      msg = msg.replace("%2", name); //$NON-NLS-1$
      msg = msg.replace("%3", Integer.toString(level)); //$NON-NLS-1$
      response.add(msg);
    }
    if (cost < 0) {
      String msg = Language.getString("TechTreeEntry.3"); //$NON-NLS-1$
      msg = msg.replace("%1", fileName); //$NON-NLS-1$
      msg = msg.replace("%2", name); //$NON-NLS-1$
      msg = msg.replace("%3", Long.toString(cost)); //$NON-NLS-1$
      response.add(msg);
    }
    if (name.length() > 39) {
      String msg = Language.getString("TechTreeEntry.5"); //$NON-NLS-1$
      msg = msg.replace("%1", fileName); //$NON-NLS-1$
      msg = msg.replace("%2", name); //$NON-NLS-1$
      response.add(msg);
    }
  }
}
