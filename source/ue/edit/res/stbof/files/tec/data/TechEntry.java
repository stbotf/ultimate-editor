package ue.edit.res.stbof.files.tec.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

// The technology entry specifies buildable structures at a single tech level,
// as well as the reasearch progress of the different tech areas.
public class TechEntry {

  public static final int BASE_SIZE = 60;
  public static final int DEFAULT_BldMaskLongs = 6;
  public static final int DEFAULT_SIZE = BASE_SIZE + DEFAULT_BldMaskLongs * Long.BYTES; // = 108

  public static int calcSize(int bldMaskLongs) {
    return BASE_SIZE + bldMaskLongs * Long.BYTES;
  }

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  public class TechArea {
    public short techLevel;
    private short unknown1;
    public int researchDone;

    public TechArea(InputStream in) throws IOException {
      techLevel = StreamTools.readShort(in, true);
      unknown1 = StreamTools.readShort(in, true);
      researchDone = StreamTools.readInt(in, true);
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(techLevel, true));
      out.write(DataTools.toByte(unknown1, true));
      out.write(DataTools.toByte(researchDone, true));
    }
  }

  private InternalFile file;

  @Getter @Accessors(fluent = true)
  private short raceId;
  private short unknown1;
  //biotech, computer, construction, energy, propulsion, weapons, sociology[not used]
  private TechArea[] techAreas = new TechArea[CStbof.NUM_TECH_FIELDS];
  // buildable buildings, same order as in stbof.res and UE
  // bitwise XOR, but little endian: mirrored 8bit blocks
  private long[] buildingMask;

  public TechEntry(InternalFile file, InputStream in, int bldMaskLongs) throws IOException {
    this.file = file;

    //race 0-2
    raceId = StreamTools.readShort(in, true);
    //unknown 2-2
    unknown1 = StreamTools.readShort(in, true);
    //tech areas 4-8*7=56
    for (int i = 0; i < techAreas.length; ++i)
      techAreas[i] = new TechArea(in);

    //buildings masks 60-6*8=48
    buildingMask = new long[bldMaskLongs];
    for (int i = 0; i < buildingMask.length; i++)
      buildingMask[i] = StreamTools.readLong(in, true);
  }

  public short getTechLvl(int tech) {
    checkTechType(tech);
    return techAreas[tech].techLevel;
  }

  public int getTechPnt(int tech) {
    checkTechType(tech);
    return techAreas[tech].researchDone;
  }

  public void setTechLvl(int tech, short lvl) {
    checkTechType(tech);
    if (techAreas[tech].techLevel != lvl) {
      techAreas[tech].techLevel = lvl;
      file.markChanged();
    }
  }

  public void setTechPnt(int tech, int pnt) {
    checkTechType(tech);
    if (techAreas[tech].researchDone != pnt) {
      techAreas[tech].researchDone = pnt;
      file.markChanged();
    }
  }

  public static int calcBuildMaskLongs(int entrySize) throws IOException {
    int bldMaskSize = entrySize - BASE_SIZE;
    int bldMaskLongs = bldMaskSize / Long.BYTES;

    if (entrySize != calcSize(bldMaskLongs)) {
      String msg = Language.getString("TechEntry.5")
        .replace("%1", Integer.toString(entrySize));
      throw new IOException(msg);
    }

    return bldMaskLongs;
  }

  public int maxBuildingCount() {
    return buildingMask.length * Long.BYTES * 8;
  }

  /**
   * Get availability of the given building.
   * If building mask is exceeded it is defaulted to false.
   *
   * This is for in case any mod doesn't increase racetech.tec build mask size
   * along with the edifice.bst building count.
   *
   * @param buildingId the building index to check
   * @return whether it is marked available
   */
  public boolean isAvailable(int buildingId) {
    int maskIdx = buildingId / 64;

    // default values out of range to 0
    if (maskIdx >= buildingMask.length)
      return false;

    long mask = buildingMask[maskIdx];
    int bit = buildingId - (maskIdx * 64);
    long flag = 1L << bit;

    return (mask & flag) == flag;
  }

  /**
   * Set availability of the given building.
   * Other than the getter, this one throws on invalid building mask indices.
   *
   * @param buildingId the building index to check
   * @return whether it is marked available
   * @throws IndexOutOfBoundsException  thrown when the building mask size is exceeded
   *                                    and the availability can't be set
   */
  public boolean setAvailable(int buildingId, boolean enable) {
    int maskIdx = buildingId / 64;
    long mask = buildingMask[maskIdx];
    int bit = buildingId - (maskIdx * 64);
    long flag = 1L << bit;

    if (enable) {
      if ((mask & flag) != flag) {
        buildingMask[maskIdx] = mask | flag;
        file.markChanged();
        return true;
      }
    } else if ((mask & flag) == flag) {
      buildingMask[maskIdx] = mask ^ flag;
      file.markChanged();
      return true;
    }

    return false;
  }

  /**
   * Shift the build mask flags for inserted buildings.
   * If the max supported building id is exceeded it is ignored.
   *
   * @param buildingId  the index of the inserted building
   * @param available   the availability of the inserted building
   */
  public void shiftBuildings(int buildingId, boolean available) {
    final int bldCnt = maxBuildingCount();

    // ignore if building count is exceeded
    if (buildingId >= bldCnt)
      return;

    // shift in reverse order to not overwrite the read value
    for (int i = bldCnt - 1; i > buildingId; --i) {
      boolean av = isAvailable(i - 1);
      setAvailable(i, av);
    }

    // reset added building availability
    setAvailable(buildingId, available);
    file.markChanged();
  }

  public void removeBuilding(int buildingId) {
    final int bldCnt = maxBuildingCount();

    // ignore if building count is exceeded
    if (buildingId >= bldCnt)
      return;

    for (int i = buildingId + 1; i < bldCnt; ++i) {
      boolean av = isAvailable(i);
      setAvailable(i - 1, av);
    }

    // reset last entry
    setAvailable(bldCnt - 1, false);
    file.markChanged();
  }

  /**
   * Switches the given race tech availability indices.
   * Non-existent indices are ignored, availability is defaulted to false.
   *
   * @param idx1
   * @param idx2
   */
  public void switchBuildingIndex(int idx1, int idx2) {
    final int bldCnt = maxBuildingCount();
    boolean av1 = isAvailable(idx1);
    if (idx1 < bldCnt)
      setAvailable(idx1, isAvailable(idx2));
    if (idx2 < bldCnt)
      setAvailable(idx2, av1);
    file.markChanged();
  }

  public void resizeBldMask(int bldMaskLongs) {
    // ignore shrinks, setting the bldMaskLongs should suffice
    if (bldMaskLongs > buildingMask.length) {
      buildingMask = Arrays.copyOf(buildingMask, bldMaskLongs);
      file.markChanged();
    }
  }

  public void save(OutputStream out, int bldMaskLongs) throws IOException {
    // race                           2 -> 2
    out.write(DataTools.toByte(raceId, true));
    // unknown                        2 -> 4
    out.write(DataTools.toByte(unknown1, true));
    // tech era/unknown/points   8*7=56 -> 60
    for (TechArea area : techAreas)
      area.save(out);

    // buildings masks           6*8=48 -> 108
    // force to write the configured size and zero or cut the rest
    for (int i = 0; i < bldMaskLongs; ++i) {
      long val = i < buildingMask.length ? buildingMask[i] : 0;
      out.write(DataTools.toByte(val, true));
    }
  }

  public Vector<String> check(InternalFile parent) {
    Vector<String> response = new Vector<String>();

    if (raceId < 0 || raceId == CStbof.Race.Monster || raceId == CStbof.Race.Neutral) {
      String msg = Language.getString("TechEntry.1");
      msg = msg.replace("%1", Integer.toString(raceId));
      response.add(parent.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    for (TechArea techArea : techAreas) {
      if (techArea.techLevel < 0) {
        String msg = Language.getString("TechEntry.2")
          .replace("%1", Integer.toString(raceId))
          .replace("%2", Integer.toString(techArea.techLevel));
        response.add(parent.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if (techArea.techLevel == 0 && raceId < NUM_EMPIRES) {
        String msg = Language.getString("TechEntry.3")
          .replace("%1", Integer.toString(raceId));
        response.add(parent.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if (techArea.researchDone < 0) {
        String msg = Language.getString("TechEntry.4")
          .replace("%1", Integer.toString(raceId));
        response.add(parent.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
    }
    return response;
  }

  private void checkTechType(int tech) {
    if (tech < 0 || tech > 6) {
      String msg = Language.getString("TechEntry.6"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(tech)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }
}
