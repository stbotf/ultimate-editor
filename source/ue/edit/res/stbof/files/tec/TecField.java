package ue.edit.res.stbof.files.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of the tecfield.tec file. It holds the tech fields of the game.
 */
public class TecField extends InternalFile {

  //fields
  private ArrayList<ID> ENTRY = new ArrayList<ID>();

  @Override
  public void check(Vector<String> response) {
    String msg;
    if (ENTRY.size() == 0) {
      msg = Language.getString("TecField.0"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      response.add(msg);
    }

    for (int i = 0; i < ENTRY.size(); i++) {
      ID tec = ENTRY.get(i);
      if ((tec.toString()).length() > 39) {
        msg = Language.getString("TecField.1"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", tec.toString()); //$NON-NLS-1$
        response.add(msg);
      }
    }
  }

  @Override
  public int getSize() {
    return ENTRY.size() * 41 + 2;
  }

  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    short num = StreamTools.readShort(in, true);

    for (int i = 0; i < num; ++i) {
      String name = StreamTools.readNullTerminatedBotfString(in, 40);
      // skip index
      StreamTools.skip(in, 1);
      ENTRY.add(new ID(name, i));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    short size = (short) ENTRY.size();
    out.write(DataTools.toByte(size, true));

    for (byte i = 0; i < size; i++) {
      ID se = ENTRY.get(i);
      //string
      out.write(DataTools.toByte(se.toString(), 40, 39));
      //index
      out.write(i);
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  //gets
  public int getNumberOfFields() {
    return ENTRY.size();
  }

  /**
   * Return a list of all tech fields.
   */
  public String[] getFields() {
    return ENTRY.stream().map(x -> x.toString()).toArray(String[]::new);
  }

  /**
   * @param index
   * @return
   */
  public String getField(int index) throws InvalidArgumentsException {
    if (ENTRY.size() <= index) {
      throw new InvalidArgumentsException(Language.getString("TecField.2")
        .replace("%1", Integer.toString(index))); //$NON-NLS-1$ //$NON-NLS-2$
    }

    return ENTRY.get(index).toString();
  }

  /**
   * @param index
   * @param name
   */
  public void setField(int index, String name) throws InvalidArgumentsException {
    if (ENTRY.size() <= index) {
      throw new InvalidArgumentsException(Language.getString("TecField.2")
        .replace("%1", Integer.toString(index))); //$NON-NLS-1$ //$NON-NLS-2$
    }

    ENTRY.set(index, new ID(name, index));
    markChanged();
  }
}
