package ue.edit.res.stbof.files.tec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Vector;
import org.apache.commons.lang3.mutable.MutableInt;
import lombok.val;
import ue.UE;
import ue.edit.exe.trek.Trek;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.WDFImgReq;
import ue.edit.res.stbof.files.desc.DescDataFile;
import ue.edit.res.stbof.files.tec.data.TechTreeEntry;
import ue.edit.res.stbof.files.tec.data.TechTreeField;
import ue.exception.KeyNotFoundException;
import ue.service.Language;
import ue.util.data.Pair;
import ue.util.file.FileStore;

/**
 * This class is a representation of the techtree.tec file. It holds the tech tree of the game.
 */
public class TechTree extends DescDataFile {

  // #region data fields

  private ArrayList<TechTreeEntry> entries = new ArrayList<TechTreeEntry>();
  private ArrayList<TechTreeField> fields = new ArrayList<TechTreeField>(6);

  // #endregion data fields

  // #region constructor

  public TechTree(Stbof stbof) throws IOException {
    super(CStbofFiles.TechDescTec, stbof);
  }

  // #endregion constructor

  // #region getters

  public TechTreeField field(int field) {
    return fields.size() > field ? fields.get(field) : null;
  }

  /**
   * Returns the number of techs
   */
  @Override
  public int getNumberOfEntries() {
    return entries.size();
  }

  @Override
  public TechTreeEntry getEntry(int index) {
    checkIndex(index);
    return entries.get(index);
  }

  /**
   * Return the tech entry of a given technology field and level.
   *
   * @param field the tech field
   * @param level the level of the tech
   * @throws KeyNotFoundException if not found
   */
  public Pair<TechTreeEntry, Integer> getEntry(int field, int level) throws KeyNotFoundException {
    int i = 0;
    for (TechTreeEntry te : entries) {
      if (te.field() == field && te.level() == level) {
        return new Pair<TechTreeEntry, Integer>(te, i);
      }
      i++;
    }

    String msg = Language.getString("TechTree.13"); //$NON-NLS-1$
    msg = msg.replace("%1",
      "(" + field + ", " + level + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
    throw new KeyNotFoundException(msg);
  }

  public String getDesc(int sel_field, int sel_level) throws KeyNotFoundException {
    return getEntry(sel_field, sel_level).LEFT.getDesc();
  }

  public int getMaxTechLevel() {
    int max = 0;
    for (TechTreeField field : fields) {
      max = Integer.max(max, field.getMaxTechLevel());
    }
    return max;
  }

  // #endregion getters

  // #region add & remove

  public int add(String name, byte fieldIndex, String desc) {
    TechTreeField field = field(fieldIndex);
    if (field == null) {
      field = new TechTreeField(this, fieldIndex);
      fields.set(fieldIndex, field);
    }

    TechTreeEntry newTech = field.add(name, desc);
    entries.add(newTech);

    // sort to update the entry indices of any following fields
    sort();

    descChanged();
    markChanged();
    return newTech.index();
  }

  public void removeEntry(int field, int level) {
    MutableInt intVal = new MutableInt(Integer.MAX_VALUE);

    // first update the entry list to check former tech levels
    entries.removeIf(te -> {

      if (te.field() == field) {
        int teLvl = te.level();
        if (teLvl == level) {
          intVal.setValue(te.index());
          te.removeDesc();
          descChanged();
          markChanged();
          return true;
        }
      }

      // update all entry indices that follow
      if (te.index() > intVal.intValue())
        te.setIndex(te.index() - 1);
      return false;
    });

    // next update the tech fields and successive levels
    val tf = field(field);
    tf.removeEntry(level);
  }

  // #endregion add & remove

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    entries.clear();
    fields.clear();

    while (in.available() > 0) {
      TechTreeEntry se = new TechTreeEntry(this, in);
      entries.add(se);

      int fieldIndex = se.field();
      TechTreeField field = fields.size() > fieldIndex ? fields.get(fieldIndex) : null;
      if (field == null) {
        // add missing TechTreeField
        field = new TechTreeField(this, fieldIndex);

        // keep tech fields sorted on index
        if (fieldIndex < fields.size()) {
          fields.set(fieldIndex, field);
        } else {
          // fill missing tech fields
          fields.ensureCapacity(fieldIndex + 1);
          for (int i = fields.size(); i < fieldIndex; i++) {
            fields.add(null);
          }

          fields.add(field);
        }
      }

      // keep a second list on the tech fields
      field.add(se);
    }

    sort();
    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    int size = entries.size();
    for (int i = 0; i < size; i++) {
      TechTreeEntry se = entries.get(i);
      se.setIndex(i);
      se.save(out);
    }
  }

  // #endregion save

  // #region maintenance

  @Override
  public int getSize() {
    return entries.size() * 72;
  }

  @Override
  public void clear() {
    entries.clear();
    fields.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region checks

  @Override
  public void check(Vector<String> response) {
    super.check(response);

    if (entries.size() == 0) {
      String msg = Language.getString("TechTree.0"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      response.add(msg);
    }

    for (TechTreeEntry se : entries) {
      se.check(response);
    }

    // now check images
    checkImages(response);
  }

  private void checkImages(Vector<String> response) {
    Trek trek = UE.FILES.trek();

    try (FileStore fs = stbof.access()) {
      val techReqs      = WDFImgReq.listTechReqs(trek, stbof, response);
      HashSet<String> alreadyChecked = new HashSet<>();

      for (TechTreeEntry tech : entries) {
        String gfx = tech.image();
        if (alreadyChecked.add(gfx))
          WDFImgReq.checkStbofImageReqs(fs, stbof, this, gfx, techReqs, response);
      }
    }
    catch (Exception e) {
      String err = "Failed to check " + NAME + " images.";
      response.add(err);
    }
  }

  // #endregion checks

  // #region private helpers

  private void checkIndex(int index) {
    if ((index < 0) || (index >= entries.size())) {
      String msg = Language.getString("TechTree.13"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private void sort() {
    // sort by field and level
    Collections.sort(entries);

    // update indices
    int index = 0;
    for (TechTreeEntry entry : entries) {
      entry.setIndex(index++);
    }
  }

  // #endregion private helpers

}
