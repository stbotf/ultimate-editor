package ue.edit.res.stbof.files.desc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;
import ue.util.stream.StreamTools;

/**
 * This class is used for string files such as racedesc.rst.
 */
public class StringFile extends InternalFile {

  private ArrayList<Description> DESCRIPTIONS = new ArrayList<Description>();
  private int length = 0;

  public StringFile(Stbof stbof) {
  }

  @Override
  public void load(InputStream in) throws IOException {
    DESCRIPTIONS.clear();
    length = 0;

    // read descriptions
    while (in.available() > 0) {
      String str = StreamTools.readNullTerminatedBotfString(in);
      DESCRIPTIONS.add(new Description(str, length));
      length += str.length() + 1;
    }

    markSaved();
  }

  /**
   * Returns the number of descriptions/strings in the file.
   */
  public int getNumDescriptions() {
    return DESCRIPTIONS.size();
  }

  /**
   * Used to get the uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return length;
  }

  public Description getEntry(int i) {
    return DESCRIPTIONS.get(i);
  }

  public Description findEntry(int addr) {
    int i = findIndex(addr);
    return i >= 0 ? DESCRIPTIONS.get(i) : null;
  }

  /**
   * Used to look up descriptions by address.
   *
   * @param addr the address of the description
   * @return description
   */
  public String getDescByAddr(int addr) {
    Description desc = findEntry(addr);
    if (desc == null || desc.desc == null)
      return null;

    int offset = addr - desc.address;
    return offset > 0 ? desc.desc.substring(offset) : desc.desc;
  }

  public void removeDescByAddr(int addr) {
    int i = findIndex(addr);
    if (i >= 0) {
      DESCRIPTIONS.remove(i);
      markChanged();
    }
  }

  /**
   * Adds a new description to the end of the list.
   *
   * @return the address of the new description
   */
  public int add(String text) {
    int addr = 0;
    if (!DESCRIPTIONS.isEmpty()) {
      int idx = DESCRIPTIONS.size()-1;
      Description desc = DESCRIPTIONS.get(idx);
      addr = desc.address + desc.getSize();
    }

    Description desc = new Description(text, addr);
    DESCRIPTIONS.add(desc);
    length += desc.getSize();
    markChanged();

    return addr;
  }

  @Override
  public void clear() {
    DESCRIPTIONS.clear();
    length = 0;
    markChanged();
  }

  /**
   * Save changes.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    Description d;

    for (int x = 0; x < DESCRIPTIONS.size(); x++) {
      d = DESCRIPTIONS.get(x);
      byte[] fiu = (d.toString()).getBytes("ISO-8859-1"); //$NON-NLS-1$
      out.write(fiu);
      out.write(0x00);
    }
  }

  private int findIndex(int addr) {
    for (int i = 0; i < DESCRIPTIONS.size(); ++i) {
      Description desc = DESCRIPTIONS.get(i);
      int descAddr = desc.getAddress();
      if (descAddr <= addr && addr < descAddr + desc.getSize() )
        return i;
    }

    return -1;
  }

  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < DESCRIPTIONS.size(); i++) {
      Description ra = DESCRIPTIONS.get(i);
      ra.check(response);
    }
  }

  // class
  public class Description {

    @Getter private String desc;
    @Getter private int address = 0;

    public Description(String desc, int address) {
      this.desc = desc != null ? desc : "";
      this.address = address;
    }

    public int getSize() {
      // +1 cause the save routine appends a 0x00 byte to each description
      return 1 + (desc != null ? desc.length() : 0);
    }

    @Override
    public String toString() {
      return desc;
    }

    public void check(Vector<String> response) {
      if (address < 0) {
        String msg = getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR,
            Language.getString("StringFile.0")
                .replace("%2", Integer.toString(address))); //$NON-NLS-1$ //$NON-NLS-2$

        response.add(msg);
      }
    }
  }
}
