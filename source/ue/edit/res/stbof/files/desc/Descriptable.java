package ue.edit.res.stbof.files.desc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.service.Language;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

/**
 * Represents an description data file entry.
 */
public abstract class Descriptable {

  @Getter protected int descAddress = -1;     // description address
  @Getter private boolean isModified = false;
  @Getter protected String name = null;
  protected InternalFile file = null;
  private String description = null;

  public Descriptable(DescDataFile file) {
    this.file = file;
  }

  public Descriptable(Descriptable source) {
    this.file = source.file;
    name = source.name;

    //the description addresses must not be the same !!!
    //change externally because this has no access to other entries
    descAddress = source.descAddress;
    description = source.description;
  }

  protected void loadDescAddress(InputStream in) throws IOException {
    descAddress = StreamTools.readInt(in, true);
  }

  public String getDesc() {
    // try to load missing description
    if (description == null && descAddress >= 0)
      description = ((DescDataFile)file).DESCRIPTIONS.getDescByAddr(descAddress);

    return description;
  }

  public void removeDesc() {
    description = null;
    if (descAddress >= 0) {
      ((DescDataFile)file).DESCRIPTIONS.removeDescByAddr(descAddress);
      descAddress = -1;
      markChanged();
    }
  }

  /**
   * Sets the name of a tech.
   * @param name  the tech name
   * @return whether the name got changed
   */
  public boolean setName(String name) {
    if (!StringTools.equals(name, this.name)) {
      this.name = name;
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Sets the description of a tech.
   * @param name  the description
   * @return whether the description got changed
   */
  public boolean setDesc(String text) {
    if (!StringTools.equals(text, description)) {
      description = text;
      // mark manually edited
      isModified = true;
      ((DescDataFile)file).descChanged();
      return true;
    }
    return false;
  }

  /**
   * Sets the description address.
   */
  public void setDescAddress(int address) {
    if (address != descAddress) {
      descAddress = address;
      ((DescDataFile)file).descChanged();
    }
  }

  public void resetDesc() {
    description = null;
  }

  /**
   * Marks this file as changed.
   */
  public void markChanged() {
    file.markChanged();
  }

  /**
   * Saves changes.
   * @throws IOException the function may throw an exception if an error occurs.
   */
  public abstract void save(OutputStream out) throws IOException;

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   * @param response A Vector object containing results of the check as Strings.
   */
  public void check(Vector<String> response) {
    String fileName = file.getName();

    if (!isModified && descAddress < 0) {
      String msg = Language.getString("Descriptable.0"); //$NON-NLS-1$
      msg = msg.replace("%1", fileName); //$NON-NLS-1$
      msg = msg.replace("%2", name); //$NON-NLS-1$
      msg = msg.replace("%3", Long.toString(descAddress)); //$NON-NLS-1$
      response.add(msg);
    }
  }
}
