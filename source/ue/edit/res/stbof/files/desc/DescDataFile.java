package ue.edit.res.stbof.files.desc;

import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;
import org.apache.commons.lang3.tuple.Pair;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.desc.StringFile.Description;
import ue.service.Language;
import ue.util.data.StringTools;

/**
 * Represents a base class to description data files.
 */
public abstract class DescDataFile extends InternalFile {

  protected StringFile DESCRIPTIONS;
  protected Stbof stbof;

  @Getter private boolean descChanged = false;

  public DescDataFile(String fileName, Stbof stbof) throws IOException {
    this.stbof = stbof;
    DESCRIPTIONS = (StringFile) stbof.getInternalFile(fileName, true);
  }

  public abstract int getNumberOfEntries();
  protected abstract Descriptable getEntry(int index);

  /**
   * Sets race description address
   *
   * @param index   The index of the data entry to change.
   * @param address The new address to be assigned.
   */
  public void setDescAddress(int index, int address) {
    Descriptable r = getEntry(index);
    r.setDescAddress(address);
  }

  /**
   * Gets race description address.
   *
   * @param index The data entry index.
   * @return      The description address.
   */
  public int getDescAddress(int index) {
    return (getEntry(index)).getDescAddress();
  }

  public String getDesc(int index) {
    Descriptable entry = getEntry(index);
    return entry.getDesc();
  }

  public void setDesc(int index, String text) {
    Descriptable entry = getEntry(index);
    entry.setDesc(text);
  }

  public void descChanged() {
    descChanged = true;
  }

  @Override
  public boolean madeChanges() {
    // also check for not yet applied description changes
    return descChanged || super.madeChanges();
  }

  @Override
  public void markSaved() {
    super.markSaved();
    descChanged = false;
  }

  // re-maps all descriptions by index
  public void remapDescriptions() {
    int entryNum = getNumberOfEntries();
    int descNum = DESCRIPTIONS.getNumDescriptions();
    descChanged = entryNum != descNum;

    for (int i = 0; i < entryNum; i++) {
      Descriptable entry = getEntry(i);
      if (entry.isModified())
        descChanged = true;

      // reset cached descriptions
      entry.resetDesc();

      if (i < descNum) {
        Description desc = DESCRIPTIONS.getEntry(i);
        entry.setDescAddress(desc.getAddress());
      } else {
        entry.setDescAddress(-1);
      }
    }
  }

  // discards any description entry offsets
  public void resetDescriptionOffsets() {
    int entryNum = getNumberOfEntries();
    int descNum = DESCRIPTIONS.getNumDescriptions();
    descChanged = entryNum != descNum;

    // sets the description addresses to the beginning of the found entry
    for (int i = 0; i < entryNum; i++) {
      Descriptable entry = getEntry(i);
      // skip added or manually edited entries
      if (entry.isModified()) {
        descChanged = true;
        continue;
      }

      int addr = entry.getDescAddress();
      // reset cached descriptions
      entry.resetDesc();

      Description desc = DESCRIPTIONS.findEntry(addr);
      addr = desc != null ? desc.getAddress() : -1;
      entry.setDescAddress(addr);
    }
  }

  // write description changes to the StringFile
  public void flushDescriptions() {
    if (!isDescChanged())
      return;

    // load remaining descriptions
    for (int i = 0; i < getNumberOfEntries(); i++)
      getEntry(i).getDesc();

    // write new descriptions & update description addresses
    DESCRIPTIONS.clear();
    for (int i = 0; i < getNumberOfEntries(); i++) {
      Descriptable entry = getEntry(i);
      int addr = DESCRIPTIONS.add(entry.getDesc());
      entry.setDescAddress(addr);
    }

    // file is updated for pending description changes
    descChanged = false;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // check for duplicate description addresses
    checkForDuplicates(response);

    // check whether description addresses are offset
    checkForAddressMismatch(response);
  }

  protected void checkForDuplicates(Vector<String> response) {
    // some modders might use same address for multiple entries for convenience
    // however this can make editing descriptions in UE difficult
    HashMap<Integer, Pair<Integer, Descriptable>> used = new HashMap<Integer, Pair<Integer, Descriptable>>();
    for (int i = 0; i < getNumberOfEntries(); ++i) {
      Descriptable entry = getEntry(i);
      int addr = entry.getDescAddress();
      if (addr < 0)
        continue;

      Pair<Integer, Descriptable> prev = used.putIfAbsent(addr, Pair.of(i, entry));
      if (prev != null) {
        String currentId = "[" + Integer.toString(i) + "]"; //$NON-NLS-1$
        String prevId = "[" + Integer.toString(prev.getKey()) + "]"; //$NON-NLS-1$
        String current = StringTools.joinNotEmpty(" ", currentId, entry.getName()); //$NON-NLS-1$
        String previous = StringTools.joinNotEmpty(" ", prevId, prev.getValue()); //$NON-NLS-1$

        String msg = Language.getString("DescDataFile.0"); //$NON-NLS-1$
        msg = msg.replace("%1", current); //$NON-NLS-1$
        msg = msg.replace("%2", previous); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(addr)); //$NON-NLS-1$
        msg += " (patch manually)";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
      }
    }
  }

  private void checkForAddressMismatch(Vector<String> response) {
    for (short i = 0; i < getNumberOfEntries(); ++i) {
      Descriptable entry = getEntry(i);
      // skip added or manually edited entries
      if (entry.isModified())
        continue;

      int entryAddr = entry.getDescAddress();
      Description desc = DESCRIPTIONS.findEntry(entryAddr);
      if (desc == null) {
        long len = DESCRIPTIONS.getSize();
        String msg = "Description address %1 of %2 exceeds description length %3."
          .replace("%1", Integer.toString(entryAddr))
          .replace("%2", "[" + Integer.toString(i) + "] " + entry.getName())
          .replace("%3", Long.toString(len));
        msg += " (patch manually)";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
        continue;
      }

      int descAddr = desc.getAddress();
      if (entryAddr != descAddr) {
        String msg = "Description address %1 of %2 is offset from the actual entry address %3."
          .replace("%1", Integer.toString(entryAddr))
          .replace("%2", "[" + Integer.toString(i) + "] " + entry.getName())
          .replace("%3", Integer.toString(descAddr));
        msg += " (patch manually)";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
      }
    }
  }
}
