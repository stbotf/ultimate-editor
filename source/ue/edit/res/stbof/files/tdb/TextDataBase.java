package ue.edit.res.stbof.files.tdb;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.tdb.data.TdbEntry;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

public class TextDataBase extends InternalFile {

  private byte[] UNKNOWN_1;
  private ArrayList<TdbEntry> MAP = new ArrayList<TdbEntry>();

  public TextDataBase() {
  }

  public TextDataBase(String name) {
    this.NAME = name;
  }

  public TextDataBase(String name, InputStream in) throws IOException {
    this.NAME = name;
    load(in);
  }

  public TextDataBase(String name, byte[] data) throws IOException {
    this(name, new ByteArrayInputStream(data));
  }

  /**
   * Returns string.
   */
  public String getString(int index) {
    checkIndex(index);
    TdbEntry t = MAP.get(index);
    return t.STR;
  }

  /**
   * Sets a string.
   */
  public boolean setString(int index, String newString) {
    checkIndex(index);
    TdbEntry tdb = MAP.get(index);

    if (!StringTools.equals(tdb.STR, newString)) {
      tdb.STR = newString;
      tdb.LENGTH = newString.length() + 3;
      markChanged();
      return true;
    }
    return false;
  }

  /*Specified by StbofEdit*/
  @Override
  public void check(Vector<String> response) {
  }

  @Override
  public int getSize() {
    int size = 16 + MAP.size() * 19;
    for (int i = 0; i < MAP.size(); i++) {
      TdbEntry t = MAP.get(i);
      size += t.STR.length();
    }
    return size;
  }

  @Override
  public void load(InputStream in) throws IOException {
    MAP.clear();

    //number of entries
    int num = StreamTools.readInt(in, true);
    //unknown
    UNKNOWN_1 = StreamTools.readBytes(in, 4);

    //largest string size
    int maxLen = StreamTools.readInt(in, true);
    validateInt(in, 0);
    //read map
    for (int i = 0; i < num; i++) {
      MAP.add(new TdbEntry(in));
    }

    //read strings
    byte[] data = StreamTools.readAllBytes(in);
    for (int i = 0; i < num; i++) {
      TdbEntry te = MAP.get(i);
      // skip entry separating space character (len - 1, START_POS + 1)
      int len = Integer.min(data.length - te.START_POS, maxLen) - 1;
      // entries are terminated by "\n\0" (0A 00), but still the entries can contain line feeds
      String str = DataTools.fromNullTerminatedBotfString(data, te.START_POS + 1, len);
      // strip final '\n' line feed character
      te.STR = str.substring(0, str.length() - 1);
    }

    markSaved();
  }

  @Override
  public void save(java.io.OutputStream out) throws IOException {
    //number
    out.write(DataTools.toByte(MAP.size(), true));
    //unknown
    out.write(UNKNOWN_1);

    //largest string size and clean up
    int maxLen = 0;
    for (int i = 0; i < MAP.size(); i++) {
      TdbEntry t = MAP.get(i);
      t.LENGTH = t.STR.length() + 3;
      if (t.LENGTH > maxLen)
        maxLen = t.LENGTH;
    }

    out.write(DataTools.toByte(maxLen, true));
    StreamTools.write(out, 0, 4);
    int addr = 0; //address

    //map
    for (int i = 0; i < MAP.size(); i++) {
      TdbEntry t = MAP.get(i);
      t.START_POS = addr;
      addr += t.LENGTH;
      t.dump(out);
    }

    //strings
    for (int i = 0; i < MAP.size(); i++) {
      TdbEntry t = MAP.get(i);
      out.write(32);
      out.write(t.STR.getBytes("ISO-8859-1")); //$NON-NLS-1$
      out.write(10);
      out.write(0);
    }
  }

  @Override
  public void clear() {
    Arrays.fill(UNKNOWN_1, (byte)0);
    MAP.clear();
    markChanged();
  }

  public String[] getStrings() {
    return MAP.stream().map(x -> x.STR).toArray(String[]::new);
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= MAP.size()) {
      String msg = Language.getString("TextDataBase.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }
}
