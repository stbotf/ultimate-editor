package ue.edit.res.stbof.files.tdb.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class reprisents entry map data of a *.tdb file.
 */
public class TdbEntry implements Comparable<TdbEntry> {

  private byte[] UNKNOWN_1;
  /**
   * The length of the string.
   */
  public int LENGTH;
  private byte[] UNKNOWN_2;
  /**
   * The starting position of the string.
   */
  public int START_POS;
  public String STR;

  /**
   * Constructor.
   * @throws IOException
   */
  public TdbEntry(InputStream in) throws IOException {
    //unknown
    UNKNOWN_1 = StreamTools.readBytes(in, 4);
    //length
    LENGTH = StreamTools.readInt(in, true);
    //unknown
    UNKNOWN_2 = StreamTools.readBytes(in, 4);
    //start pos
    START_POS = StreamTools.readInt(in, true);
  }

  /**
   * Dumps data to the OutputStream.
   */
  public void dump(OutputStream out) throws IOException {
    out.write(UNKNOWN_1);
    out.write(DataTools.toByte(LENGTH, true));
    out.write(UNKNOWN_2);
    out.write(DataTools.toByte(START_POS, true));
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof TdbEntry)) {
      return false;
    }
    return (hashCode() == o.hashCode());
  }

  /**
   * Returns the file's hashcode.
   */
  @Override
  public int hashCode() {
    return START_POS;
  }

  @Override
  public int compareTo(TdbEntry obj) {
    return (hashCode() - obj.hashCode());
  }
}
