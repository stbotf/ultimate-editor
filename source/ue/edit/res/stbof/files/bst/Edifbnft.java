package ue.edit.res.stbof.files.bst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of the edifbnft.bst file.
 * It holds a list of building's bonus descriptions.
 */
public class Edifbnft extends InternalFile {

  //fields
  private ArrayList<String> ENTRY = new ArrayList<String>();

  /**
   * Constructor.
   */
  public Edifbnft(Stbof stbof) throws IOException {
  }

  //specified by StbofEdit
  @Override
  public void check(Vector<String> response) {
    String msg;
    for (int i = 0; i < ENTRY.size(); i++) {
      String str = ENTRY.get(i);
      if (str.length() == 0) {
        msg = Language.getString("Edifbnft.0"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(i)); //$NON-NLS-1$
        response.add(msg);
      }
    }
  }

  @Override
  public int getSize() {
    int size = 4 + ENTRY.size() * 4;
    for (int i = 0; i < ENTRY.size(); i++) {
      String str = ENTRY.get(i);
      size += str.length() + 1;
    }
    return size;
  }

  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    //address
    int address = StreamTools.readInt(in, true);
    byte[] data = StreamTools.readBytes(in, address - 4);
    //string addresses
    int t = (in.available() / 4);

    //strings
    for (int i = 0; i < t; i++) {
      int addy = StreamTools.readInt(in, true) - 4;
      if (addy >= 0 && addy < data.length)
        ENTRY.add(DataTools.fromNullTerminatedBotfString(data, addy,  data.length - addy));
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    int numEntries = ENTRY.size();

    // compute map address
    int mapAddress = 4;
    for (int z = 0; z < numEntries; z++) {
      String str = ENTRY.get(z);
      // null terminated string length
      mapAddress += str.length() + 1;
    }

    // save map address
    out.write(DataTools.toByte(mapAddress, true));

    // set up addresses
    int[] addy = new int[numEntries];
    int address = 4;

    // write strings
    for (int z = 0; z < numEntries; z++) {
      addy[z] = address;
      String str = ENTRY.get(z);
      int len = str.length();
      out.write(DataTools.toByte(str, len + 1, len));
      address += len + 1;
    }

    //save map
    for (int z = 0; z < numEntries; z++) {
      out.write(DataTools.toByte(addy[z], true));
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  /**
   * Returns bonus description.
   */
  public String getBoniDesc(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("Edifbnft.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
    return ENTRY.get(index);
  }

  /**
   * Sets bonus description.
   */
  public void setBoniDesc(int index, String desc) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("Edifbnft.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
    String str = ENTRY.get(index);
    if (str.equals(desc)) {
      return;
    }
    ENTRY.set(index, desc);
    this.markChanged();
  }

  /**
   * Returns the number of entries.
   */
  public int getNumberOfEntries() {
    return ENTRY.size();
  }
}