package ue.edit.res.stbof.files.bst.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.StructureBonus;
import ue.edit.res.stbof.common.CStbof.StructureCategory;
import ue.edit.res.stbof.common.CStbof.StructureGroup;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.desc.Descriptable;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.tec.data.TechTreeField;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of a building.
 */
@Getter
public class Building extends Descriptable implements Comparable<Building> {

  public static final int SIZE = 176;
  public static final short NO_UPGRADE = Short.MAX_VALUE;
  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // #region data fields

  // load order for Edifice to not unnecessarily sneak in any sorting changes
  private final int loadOrder;

  // private String NAME_SINGULAR;                          // 40 bytes 40
  private String  pluralName;                               // 40 bytes 80
  @Accessors(fluent = true)
  private short   id;                                       // 02 bytes 82
  private short   upgradeFor;                               // 02 bytes 84 Short.MAX_VALUE 7F FF if unset
  @Getter(AccessLevel.NONE)
  private byte[]  unknown_1;                                // 04 bytes 88
  private String  imagePrefix;                              // 08 bytes 96
  @Getter(AccessLevel.NONE)
  private byte[]  unknown_2;                                // 05 bytes 101
  private byte    techField;                                // 01 bytes 102 TecField science type requirement
  private byte    techLevel;                                // 01 byte 103  science level requirement
  private byte    excludeEmpire;                            // 01 byte 104  mask indicating which races can't build the building
  private long    residentRaceReq;                          // 08 bytes 112 mask indicating which resident races are needed
  private byte    politicReq;                               // 01 byte 113
  private byte    systemReq;                                // 01 byte 114
  private byte    bonusType;                                // 01 byte 115
  private byte    buildLimit;                               // 01 byte 116
  private byte    group;                                    // 01 byte 117  Edifice Group
  private byte    category;                                 // 01 byte 118  Edifice Category
  private short   unknown_3;                                // 02 bytes 120
  private int     buildCost;                                // 02 bytes 122
  private short   unknown_4;                                // 02 bytes 124
  private short   energyCost;                               // 02 bytes 126
  private short   unknown_5;                                // 02 bytes 128
  private short   populationCost;                           // 02 bytes 130
  private short   moraleBonus;                              // 02 bytes 132
  private short   unknown_6;                                // 02 bytes 134
  @Getter(AccessLevel.NONE)
  private short[] bonusValue = new short[NUM_EMPIRES + 1];  // 12 bytes 146
  @Getter(AccessLevel.NONE)
  private short[] unrestBonus = new short[NUM_EMPIRES + 1]; // 12 bytes 158 unrest bonus
  @Getter(AccessLevel.NONE)
  private byte[]  unknown_7;                                // 14 bytes 172
  // private int  super.descAddress;                        // 04 bytes 176

  @Getter(AccessLevel.NONE)
  private Edifice edifice;

  // #endregion data fields

  // #region constructor

  public Building(Edifice edifice, InputStream in, int loadOrder) throws IOException {
    super(edifice);
    this.edifice = edifice;
    this.loadOrder = loadOrder;
    load(in);
  }

  public Building(Building bg) {
    super(bg);
    this.edifice = bg.edifice;
    // append copied new buildings to the end
    loadOrder = Integer.MAX_VALUE;
    copy(bg);
  }

  // #endregion constructor

  // #region getters

  /**
   * Returns bonus.
   */
  public short getBonusValue(int race) {
    checkBonusIndex(race);
    return bonusValue[race];
  }

  public boolean isConstructable() {
    return !isConsumable();
  }

  public boolean isConsumable() {
    return bonusType == StructureBonus.MartialLaw && energyCost == 0
      && buildCost == 0 && populationCost == 0;
  }

  public boolean canBeBuiltBy(int ctrlRace, int residentRace) {
    boolean result = true;

    // check excluded major empires
    if (ctrlRace >= 0 && ctrlRace < NUM_EMPIRES) {
      byte emp = (byte) (1 << ctrlRace);
      emp = (byte) ((emp ^ ((byte) 0x1f) & 0x1f));
      result = ((excludeEmpire & emp) == excludeEmpire);
    }

    long mask = 1L << residentRace;
    return result && ((mask & residentRaceReq) == mask);
  }

  /**
   * Returns whether the downgrade id is set.
   */
  public boolean isUpgrade() {
    return upgradeFor != NO_UPGRADE;
  }

  public short getUnrestBonus(int race) {
    checkBonusIndex(race);
    return unrestBonus[race];
  }

  /**
   * @return true if the building is one of the main buildings
   */
  public boolean isMainBuilding() {
    int group = this.getGroup();
    return ((group == StructureGroup.MainFood) || (group == StructureGroup.MainIndustry)
        || (group == StructureGroup.MainEnergy) || (group == StructureGroup.MainIntel)
        || (group == StructureGroup.MainResearch));
  }

  /**
   * @returns one of the group field values.
   */
  public int getGroup() {
    // this is warped a bit, my guess is the additional slot is meant for neutral
    // it should be:
    // - main food (bonus id 0): group ids 0-4 (cards - roms)
    // - main industry (bonus id 13): 6-10
    // - main energy (bonus id 2): 12-16
    // - main intel (id 10): 18-22
    // - main research (id 7): 24-28
    // - scanners (id 29): 30
    // - others: 31

    if (group <= 5)
      return StructureGroup.MainFood;
    if (group <= 11)
      return StructureGroup.MainIndustry;
    if (group <= 17)
      return StructureGroup.MainEnergy;
    if (group <= 23)
      return StructureGroup.MainIntel;
    if (group <= 29)
      return StructureGroup.MainResearch;
    if (group == 30)
      return StructureGroup.Scanners;

    // GROUP == 31
    return StructureGroup.Other;
  }

  // #endregion getters

  // #region setters

  /**
   * Sets building plural name
   */
  public void setPluralName(String str) {
    if (!pluralName.equals(str)) {
      pluralName = str;
      markChanged();
    }
  }

  public void setId(short id) {
    if (this.id != id) {
      this.id = id;
      markChanged();
    }
  }

  /**
   * Sets downgrade id.
   */
  public void setUpgradeFor(short id) {
    if (upgradeFor != id) {
      upgradeFor = id;
      markChanged();
    }
  }

  /**
   * Sets picture prefix.
   */
  public void setPicture(String pic) {
    if (imagePrefix.compareToIgnoreCase(pic) != 0) {
      imagePrefix = pic;
      markChanged();
    }
  }

  /**
   * Sets tech index. 0-biotech 1-computer 2-construction 3-energy 4-propulsion 5-weapons 6-all
   */
  public void setTechReq(byte tech) {
    if (techField != tech) {
      techField = tech;
      markChanged();
    }
  }

  /**
   * Sets the technology level.
   */
  public void setTechLvl(byte tech) {
    if (techLevel != tech) {
      techLevel = tech;
      markChanged();
    }
  }

  /**
   * Sets industry/build cost.
   */
  public void setBuildCost(int cost) {
    if (buildCost != cost) {
      buildCost = cost;
      markChanged();
    }
  }

  /**
   * Sets energy cost.
   */
  public boolean setEnergyCost(short cost) {
    if (energyCost != cost) {
      energyCost = cost;
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Sets tech index.
   */
  public void setSysReq(byte req) {
    if (systemReq != req) {
      systemReq = req;
      markChanged();
    }
  }

  /**
   * Sets category.
   */
  public void setCategory(byte cat) {
    if (category != cat) {
      category = cat;
      markChanged();
    }
  }

  /**
   * Sets politic req.
   */
  public void setPoliticReq(byte req) {
    if (politicReq != req) {
      politicReq = req;
      markChanged();
    }
  }

  /**
   * Sets freq.
   */
  public void setFreq(byte freq) {
    if (buildLimit != freq) {
      buildLimit = freq;
      markChanged();
    }
  }

  /**
   * Sets morale boni.
   */
  public void setMoraleBonus(short boni) {
    if (moraleBonus != boni) {
      moraleBonus = boni;
      markChanged();
    }
  }

  /**
   * Sets bonus type.
   */
  public void setBonusType(byte type) {
    if (bonusType != type) {
      bonusType = type;
      markChanged();
    }
  }

  /**
   * Sets bonus.
   */
  public void setBonusValue(int race, short val) {
    checkBonusIndex(race);
    if (bonusValue[race] != val) {
      bonusValue[race] = val;
      markChanged();
    }
  }

  /**
   * Sets major races that can't build the building.
   */
  public void setExcludeEmpire(byte mask) {
    if (excludeEmpire != mask) {
      excludeEmpire = mask;
      this.setGroup(this.getGroup());
      markChanged();
    }
  }

  /**
   * Sets which races are needed.
   */
  public void setReqRace(long req) {
    if (residentRaceReq != req) {
      residentRaceReq = req;
      markChanged();
    }
  }

  /**
   * Sets population maintenance.
   */
  public void setPopCost(short cost) {
    if (populationCost != cost) {
      populationCost = cost;
      markChanged();
    }
  }

  /**
   * Sets bonus.
   */
  public void setUnrestBonus(int race, short val) {
    checkBonusIndex(race);
    if (unrestBonus[race] != val) {
      unrestBonus[race] = val;
      markChanged();
    }
  }

  public void setGroup(int group) {
    byte res = mapBackStructureGroup(group);
    if (group != res) {
      group = res;
      markChanged();
    }
  }

  // #endregion setters

  // #region load

  public void load(InputStream in) throws IOException {
    // name 0-80
    name = StreamTools.readNullTerminatedBotfString(in, 40);
    pluralName = StreamTools.readNullTerminatedBotfString(in, 40);
    // id 80-2
    id = StreamTools.readShort(in, true);
    // upgrade for 82-2
    upgradeFor = StreamTools.readShort(in, true);
    // unknown 84-4
    unknown_1 = StreamTools.readBytes(in, 4);
    // picture 88-8
    imagePrefix = StreamTools.readNullTerminatedBotfString(in ,8);
    // unknown 96-5
    unknown_2 = StreamTools.readBytes(in, 5);
    // science type 101-1
    techField = (byte) in.read();
    // science level 102-1
    techLevel = (byte) in.read();
    // major that can't build it? (mask) 103-1
    excludeEmpire = (byte) in.read();
    // race that needs to be on the system (mask) 104-8
    residentRaceReq = StreamTools.readLong(in, true);
    // politic requirement/or rather needed faction 112-1
    politicReq = (byte) in.read();
    // system requirement/what the system needs to be like 113-1
    systemReq = (byte) in.read();
    // bonus type 114-1
    bonusType = (byte) in.read();
    // frequency - how many times the building can be built in a system 115-1
    buildLimit = (byte) in.read();
    // unknown 116-1
    group = (byte) in.read();
    // category 117-1
    category = (byte) in.read();
    // unknown
    unknown_3 = StreamTools.readShort(in, true);
    // industry cost
    buildCost = Short.toUnsignedInt(StreamTools.readShort(in, true));
    // unknown
    unknown_4 = StreamTools.readShort(in, true);
    // energy cost
    energyCost = StreamTools.readShort(in, true);
    // unknown
    unknown_5 = StreamTools.readShort(in, true);
    // people cost
    populationCost = StreamTools.readShort(in, true);
    // morale bonus
    moraleBonus = StreamTools.readShort(in, true);
    // unknown
    unknown_6 = StreamTools.readShort(in, true);

    // bonus
    for (int i = 0; i < 6; i++) {
      bonusValue[i] = StreamTools.readShort(in, true);
    }

    // bonus #2
    for (int i = 0; i < 6; i++) {
      unrestBonus[i] = StreamTools.readShort(in, true);
    }

    // unknown
    unknown_7 = StreamTools.readBytes(in, 14);
    // description address
    loadDescAddress(in);
  }

  public void copy(Building bg) {
    pluralName = bg.pluralName;
    // id
    id = bg.id;
    upgradeFor = bg.upgradeFor;
    // unknown
    unknown_1 = bg.unknown_1;
    // picture
    imagePrefix = bg.imagePrefix;
    // science type
    techField = bg.techField;
    // science level
    techLevel = bg.techLevel;
    // major that can't build it? (mask)
    excludeEmpire = bg.excludeEmpire;
    // race
    residentRaceReq = bg.residentRaceReq;
    // politic requirement/or rather needed faction
    politicReq = bg.politicReq;
    // system requirement/what the system needs to be like
    systemReq = bg.systemReq;
    // bonus type
    bonusType = bg.bonusType;
    // frequency - how many times the building can be built in a system
    buildLimit = bg.buildLimit;
    // unknown
    unknown_2 = bg.unknown_2;
    group = bg.group;
    // category
    category = bg.category;
    // unknown
    unknown_7 = bg.unknown_7;
    // industry cost
    buildCost = bg.buildCost;
    // unknown
    unknown_3 = bg.unknown_3;
    // energy cost
    energyCost = bg.energyCost;
    // unknown
    unknown_4 = bg.unknown_4;
    // people cost
    populationCost = bg.populationCost;
    // morale bonus
    moraleBonus = bg.moraleBonus;
    // unknown
    unknown_5 = bg.unknown_5;

    // bonus
    for (int i = 0; i < 6; i++) {
      bonusValue[i] = bg.bonusValue[i];
    }
    for (int i = 0; i < 6; i++) {
      unrestBonus[i] = bg.unrestBonus[i];
    }

    // unknown
    unknown_6 = bg.unknown_6;
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    // name
    out.write(DataTools.toByte(name, 40, 39));
    out.write(DataTools.toByte(pluralName, 40, 39));
    // id
    out.write(DataTools.toByte(id, true));
    // upgrade
    out.write(DataTools.toByte(upgradeFor, true));
    // unknown
    out.write(unknown_1);
    // picture
    out.write(DataTools.toByte(imagePrefix, 8, 7));
    // unknown
    out.write(unknown_2);
    // science type
    out.write(techField);
    // science level
    out.write(techLevel);
    // major that can't build it? (mask)
    out.write(excludeEmpire);
    // race
    out.write(DataTools.toByte(residentRaceReq, true));
    // politic requirement/or rather needed faction
    out.write(politicReq);
    // system requirement/what the system needs to be like
    out.write(systemReq);
    // bonus type
    out.write(bonusType);
    // frequency - how many times the building can be built in a system
    out.write(buildLimit);
    // unknown
    out.write(group);
    // category
    out.write(category);
    // unknown
    out.write(DataTools.toByte(unknown_3, true));
    // industry cost
    out.write(DataTools.toByte((short)buildCost, true));
    // unknown
    out.write(DataTools.toByte(unknown_4, true));
    // energy cost
    out.write(DataTools.toByte(energyCost, true));
    // unknown
    out.write(DataTools.toByte(unknown_5, true));
    // people cost
    out.write(DataTools.toByte(populationCost, true));
    // morale boni
    out.write(DataTools.toByte(moraleBonus, true));
    // unknown
    out.write(DataTools.toByte(unknown_6, true));

    // bonus
    for (int i = 0; i < 6; i++) {
      out.write(DataTools.toByte(bonusValue[i], true));
    }

    // bonus 2
    for (int i = 0; i < 6; i++) {
      out.write(DataTools.toByte(unrestBonus[i], true));
    }

    // unknown
    out.write(unknown_7);
    // description address
    out.write(DataTools.toByte(descAddress, true));
  }

  // #endregion save

  // #region maintenance

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Building))
      return false;

    Building gu = (Building) o;
    return id == gu.id;
  }

  /**
   * Returns the file's hash code.
   */
  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public int compareTo(Building obj) {
    return (id - obj.id);
  }

  @Override
  public String toString() {
    return name;
  }

  // #endregion maintenance

  // #region check

  @Override
  public void check(Vector<String> response) {
    super.check(response);

    val techTree = edifice.stbof().files().findTechTree();

    try {
      if (name.length() > 39) {
        String msg = Language.getString("Building.2"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if (pluralName.length() > 39) {
        String msg = Language.getString("Building.2"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", pluralName); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if (id < 0) {
        String msg = Language.getString("Building.3"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if (imagePrefix.length() > 12) {
        String msg = Language.getString("Building.4"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", imagePrefix); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if ((techField < 0) || (techField > 6)) {
        String msg = Language.getString("Building.5"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(techField)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if (techTree.isPresent()) {
        TechTreeField field = techTree.get().field(techField);
        if ((techLevel < 0) || (techLevel > field.getMaxTechLevel())) {
          String msg = Language.getString("Building.6"); //$NON-NLS-1$
          msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
          msg = msg.replace("%2", name); //$NON-NLS-1$
          msg = msg.replace("%3", LexHelper.mapTechField(techField)); //$NON-NLS-1$
          msg = msg.replace("%4", Integer.toString(techLevel)); //$NON-NLS-1$
          response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        }
      }
      if ((systemReq < 0) || (systemReq > 14)) {
        String msg = Language.getString("Building.7"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(systemReq)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if ((buildCost < 0) && (buildCost > 65535)) {
        String msg = Language.getString("Building.8"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(buildCost)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if (energyCost < 0) {
        String msg = Language.getString("Building.9"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(energyCost)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if ((category < 0) || (category > StructureCategory.Defense)) {
        String msg = Language.getString("Building.10"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(category)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if ((politicReq < 0) || (politicReq > 11) || (politicReq == 7) || (politicReq == 8)) {
        String msg = Language.getString("Building.11"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(politicReq)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if ((buildLimit < 0) || (buildLimit > 2)) {
        String msg = Language.getString("Building.12"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(buildLimit)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }
      if ((populationCost != 0) && (populationCost != 10)) {
        String msg = Language.getString("Building.13"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(populationCost)); //$NON-NLS-1$
        response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        int mask = (byte) (1 << emp);

        if ((excludeEmpire & mask) == 0) {
          if (bonusValue[emp] == 0) {
            if (bonusType != 18) {
              String msg = Language.getString("Building.22"); //$NON-NLS-1$
              msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
              msg = msg.replace("%2", name); //$NON-NLS-1$

              response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
              break;
            }
          }
        }
      }

      if ((upgradeFor >= 0) && (upgradeFor < edifice.getNumberOfEntries())
          && ((excludeEmpire & (byte) 0x1F) != (byte) 0x1F)) {
        Building b = edifice.getEntry(upgradeFor);

        if (((excludeEmpire & b.excludeEmpire) != 0) && ((residentRaceReq & b.residentRaceReq) != 0)) {
          if (b.buildCost > buildCost) {
            String msg = Language.getString("Building.20"); //$NON-NLS-1$
            msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
            msg = msg.replace("%2", b.name); //$NON-NLS-1$
            msg = msg.replace("%3", name); //$NON-NLS-1$
            response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
          } else if (b.buildCost == buildCost) {
            String msg = Language.getString("Building.21"); //$NON-NLS-1$
            msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
            msg = msg.replace("%2", b.name); //$NON-NLS-1$
            msg = msg.replace("%3", name); //$NON-NLS-1$
            response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
          }

          // if this is a main building, check buildings from other races
          if (bonusType == b.bonusType && StructureBonus.isMainBonus(bonusType)) {
            for (int j = 0; j < edifice.getNumberOfEntries(); j++) {
              Building b2 = edifice.getEntry(j);

              if ((b.bonusType == b2.bonusType) && (b.techLevel == b2.techLevel)
                  && (b.id != b2.id) && ((b2.excludeEmpire & (byte) 0x1F) != (byte) 0x1F)) {
                if (b2.buildCost > buildCost) {
                  String msg = Language.getString("Building.20"); //$NON-NLS-1$
                  msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
                  msg = msg.replace("%2", b2.name); //$NON-NLS-1$
                  msg = msg.replace("%3", name); //$NON-NLS-1$
                  response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
                } else if (b2.buildCost == buildCost) {
                  String msg = Language.getString("Building.21"); //$NON-NLS-1$
                  msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
                  msg = msg.replace("%2", b2.name); //$NON-NLS-1$
                  msg = msg.replace("%3", name); //$NON-NLS-1$
                  response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
                }
              }
            }
          }
        }
      }
    } catch (Exception ftz) {
      String msg = Language.getString("Building.16"); //$NON-NLS-1$
      msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
      msg = msg.replace("%2", name); //$NON-NLS-1$
      msg = msg.replace("%3", ftz.getMessage()); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  // #endregion check

  // #region private helpers

  private void checkBonusIndex(int index) {
    if ((index < 0) || (index > 5)) {
      String msg = Language.getString("Building.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  // map structure group back to the edifice group
  private byte mapBackStructureGroup(int group) {
    switch (group) {
      case StructureGroup.Scanners:
        return 30;
      case StructureGroup.Other:
        return 34;
      default: {
        // default value
        byte res = (byte) (group * 6);

        // reset to actual value
        for (byte emp = 4; emp >= 0; emp--) {
          byte mask = (byte) (1 << emp);

          if ((excludeEmpire & mask) == 0) {
            res = (byte) (res + emp);
            break;
          }
        }

        return res;
      }
    }
  }

  // #endregion private helpers

}
