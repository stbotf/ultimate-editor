package ue.edit.res.stbof.files.bst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Stream;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.edit.exe.seg.common.InternalSegment;
import ue.edit.exe.seg.prim.CmpWordPtrByte;
import ue.edit.exe.seg.prim.IntValueSegment;
import ue.edit.exe.seg.prim.MovRegInt;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.patch.bld.MainStructurePatch;
import ue.edit.exe.trek.sd.SD_BldCnt;
import ue.edit.exe.trek.sd.SD_BldId_Dilithium;
import ue.edit.exe.trek.sd.SD_BldId_EmpSp;
import ue.edit.exe.trek.sd.SD_BldId_GrndDef;
import ue.edit.exe.trek.sd.SD_BldId_MainEnergy;
import ue.edit.exe.trek.sd.SD_BldId_MainFarm;
import ue.edit.exe.trek.sd.SD_BldId_MainIndustry;
import ue.edit.exe.trek.sd.SD_BldId_MainIntel;
import ue.edit.exe.trek.sd.SD_BldId_MainResearch;
import ue.edit.exe.trek.sd.SD_BldId_MartialLaw;
import ue.edit.exe.trek.sd.SD_BldId_MinorSp;
import ue.edit.exe.trek.sd.SD_BldId_Orb;
import ue.edit.exe.trek.sd.SD_BldId_Scanner;
import ue.edit.exe.trek.sd.SD_BldId_Shield;
import ue.edit.exe.trek.sd.SD_BldId_Shipyard;
import ue.edit.exe.trek.sd.SD_BldId_TradeGoods;
import ue.edit.exe.trek.sd.SD_MandatoryDilFix;
import ue.edit.exe.trek.sd.SegmentDefinition;
import ue.edit.exe.trek.seg.bld.MainBldUpgrades;
import ue.edit.exe.trek.seg.bld.MandatoryDilithiumFix;
import ue.edit.exe.trek.seg.bld.MartialLawBld;
import ue.edit.exe.trek.seg.bld.MinorRaceMainBldIDLimits;
import ue.edit.exe.trek.seg.bld.MinorRaceSpBuildingIDs;
import ue.edit.exe.trek.seg.bld.PunyFactories;
import ue.edit.exe.trek.seg.bld.PunyFarms;
import ue.edit.exe.trek.seg.bld.ShipyardBldIDs;
import ue.edit.exe.trek.seg.shp.SpecialShips;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.WDFImgReq;
import ue.edit.res.stbof.common.CStbof.StructureGroup;
import ue.edit.res.stbof.common.CStbof.StructureType;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.desc.DescDataFile;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.tec.TechTree;
import ue.edit.value.ByteValue;
import ue.edit.value.IntValue;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.calc.Calc;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.file.FileStore;
import ue.util.stream.StreamTools;

/**
 * This class implements to read and edit the building list of the edifice.bst file.
 *
 * The file format is structured as follows:
 *
 *  _ Header: ___________________________________________________
 * | Offset 0x0000 to 0x0003: number of buildings    (  4 bytes) |
 * | Offset 0x0004 to 0x0007: buildings map address  (  4 bytes) |
 *  ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
 *  _ Tech Map: _________________________________________________
 * | Offset 0x0008 to 0x01F3: tech field counter     ( 16 bytes) |
 * | -> lists counters for the different tech field requirements |
 * | Offset 0x0008 to 0x01F3: tech level counter     (476 bytes) |
 * | -> lists file offsets & counters for each tech field level  |
 *  ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
 *  _ Ignore List: ______________________________________________
 * | Offset 0x01F4 to 0x02E3: ignore list            (240 bytes) |
 * | -> flags to which races the buildings are not available     |
 *  ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
 *  _ Building List: ____________________________________________
 * | Offset 0x02E4 to 0x???? building data       (n * 176 bytes) |
 * | -> lists the actual building details                        |
 *  ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
 *  _ Building Address Map: _____________________________________
 * | Followed by the buildings map                 (n * 4 bytes) |
 * | -> an address map with file offsets for each building       |
 *  ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
 */
public class Edifice extends DescDataFile {

  // #region constants

  public static final String[] MiscSegmentsEdited = new String[] {
    CTrekSegments.MainBldUpgrades,
    CTrekSegments.MinorMainBldIds,
    CTrekSegments.PunyFactories,
    CTrekSegments.PunyFarms,
    SD_BldId_MinorSp.BldListName,
    SD_BldId_Scanner.BasicName
  };

  public static final String[] SegmentsEdited = Stream.of(
    SD_BldCnt.Names,
    SD_BldId_Dilithium.Names,
    SD_BldId_EmpSp.Names,
    SD_BldId_MainFarm.Names,
    SD_BldId_GrndDef.Names,
    SD_BldId_MainEnergy.Names,
    SD_BldId_MainIndustry.Names,
    SD_BldId_MainIntel.Names,
    SD_BldId_MainResearch.Names,
    SD_BldId_MartialLaw.Names,
    SD_BldId_Orb.Names,
    SD_BldId_Shield.Names,
    SD_BldId_Shipyard.Names,
    SD_BldId_TradeGoods.Names,
    MiscSegmentsEdited
  ).flatMap(Arrays::stream).toArray(String[]::new);

  // general constants
  private static final int MaxLongFlags = Long.BYTES * 8;
  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;
  private static final int NUM_MAINBLD_UPGRADES = MainBldUpgrades.NUM_UPGRADES;
  private static final int MASK_ALL_EMP = 1|2|4|8|0x10;

  // building count + map address
  private static final int HeaderSize = 8;
  // tech field constants
  private static final int TechFieldCnt = CStbof.NUM_TECH_FIELDS;
  private static final int TechField_Alignment = Short.BYTES;
  private static final int TechFieldEntry_Alignment = Short.BYTES;
  private static final int TechFieldCntrSize = TechFieldCnt * Short.BYTES + TechFieldEntry_Alignment;
  // tech level constants
  private static final int TechFieldLvl_Size = Integer.BYTES + Short.BYTES;
  private static final int DEFAULT_TechLvlCount = CStbof.DEFAULT_NUM_TECHLVLS;
  private static final int DEFAULT_TechField_Size = DEFAULT_TechLvlCount * TechFieldLvl_Size + TechField_Alignment; // 68 bytes
  private static final int DEFAULT_TechMap_Size = TechFieldCnt * DEFAULT_TechField_Size; // 476 bytes
  // ignore list constants
  private static final int DEFAULT_BldMask_Longs = 6; // bitmask of 6 long values per empire
  private static final int DEFAULT_BldMask_Size = DEFAULT_BldMask_Longs * Long.BYTES; // bitmask of 6 * 8 byte long values per empire
  private static final int DEFAULT_IgnoreList_Size = NUM_EMPIRES * DEFAULT_BldMask_Size; // 240 bytes
  // extension sizes
  private static final int TechLvl_IncSize = TechFieldCnt * TechFieldLvl_Size; // 42 bytes per tech level increment
  private static final int BldMask_IncSize = NUM_EMPIRES * Long.BYTES; // 20 bytes per build mask increment

  // #endregion constants

  // #region data fields

  @Getter @Accessors(fluent = true)
  private Stbof stbof;
  private Trek trek;
  private TechTree techTree;
  private MartialLawBld martialLaw;
  private MainStructurePatch mainStrcPatch;

  // ignore list, by default: 5 major races * 6 long values * 8 bytes * 8 bits = 384 buildings max
  private long[][] ignoreList = null;
  private ArrayList<Building> buildings = new ArrayList<Building>();

  // 6 longs by default for the ignore list per empire
  @Getter private int loadedBldMaskLongs = 0;
  @Getter private int bldMaskLongs = DEFAULT_BldMask_Longs;
  // 11 tech levels by default per tech field per empire
  @Getter private int loadedTechLvls = 0;
  @Getter private int techLvls = DEFAULT_TechLvlCount;

  // #endregion data fields

  // #region constructor

  /**
   * Constructor.
   * @throws IOException
   */
  public Edifice(Stbof stbof, Trek trek) throws IOException {
    super(CStbofFiles.EdifDescBst, stbof);
    this.stbof = stbof;
    this.trek = trek;
    techTree = (TechTree) stbof.tryGetInternalFile(CStbofFiles.TechTreeTec, true);
    mainStrcPatch = new MainStructurePatch(trek);
    martialLaw = new MartialLawBld(trek);
  }

  // #endregion constructor

  // #region getters

  /**
   * Returns the number of entries.
   */
  @Override
  public int getNumberOfEntries() {
    return buildings.size();
  }

  public int getLoadedMaxBuildings() {
    return loadedBldMaskLongs * Long.BYTES * 8;
  }

  public int getMaxBuildings() {
    return bldMaskLongs * Long.BYTES * 8;
  }

  public int getMaxTechLevel() {
    int max = 0;
    for (Building bld : buildings) {
      if (bld.getTechLevel() > max)
        max = bld.getTechLevel();
    }
    return max;
  }

  public int getReqBldMaskLongs() {
    return DataTools.ceilDiv(buildings.size(), 64);
  }

  @Override
  public Building getEntry(int index) {
    checkIndex(index);
    return buildings.get(index);
  }

  public ArrayList<Building> buildings() {
    return new ArrayList<Building>(buildings);
  }

  /**
   * @returns the building at the specified index.
   */
  public Building building(int index) {
    checkIndex(index);
    return buildings.get(index);
  }

  public void processBuildings(Function<Building, Boolean> callback) {
    for (Building bld : buildings) {
      if (!callback.apply(bld))
        return;
    }
  }

  /**
   * Returns a list of all building names.
   */
  public String[] getNames() {
    return buildings.stream().map(x -> x.getName()).toArray(String[]::new);
  }

  // Building filters, used by the MEStartCondGUI
  public interface FilterMask {

    public static final int All = 0;

    // Filters for buildings that can be build by:
    public static final int Cards = 1;
    public static final int Feds = 2;
    public static final int Fergs = 3;
    public static final int Klngs = 4;
    public static final int Roms = 5;

    // Filter for buildings that can be build by all empires:
    public static final int AllEmps = 6;

    // Filters for buildings that exclusively can be build by:
    public static final int CardsOnly = 7;
    public static final int FedsOnly = 8;
    public static final int FergsOnly = 9;
    public static final int KlngsOnly = 10;
    public static final int RomsOnly = 11;

    // Filter for buildings that depend on the resident system race:
    public static final int SingleResident = 12;
  }

  /**
   * List buildings filtered by the filter mask.
   *
   * @param mask the race filter mask, @see FilterMask
   * @return the filtered building list
   */
  public ArrayList<Building> buildings(int mask) {
    ArrayList<Building> res = new ArrayList<Building>(buildings.size());

    // collect buildings by selection mask
    processBuildings(mask, x -> {
      res.add(x);
      return true;
    });
    return res;
  }

  /**
   * Process buildings filtered by the filter mask.
   *
   * @param filter   the race filter mask, @see FilterMask
   * @param callback called on each match, needs to return true to proceed
   */
  public void processBuildings(int filter, Function<Building, Boolean> callback) {
    if (filter == FilterMask.All) {
      // process unfiltered
      for (Building bld : buildings) {
        if (!callback.apply(bld))
          return;
      }
    } else if (filter < FilterMask.AllEmps) {
      // process selected empire buildings only
      int mask = 1 << (filter - 1);
      for (Building bld : buildings) {
        // skip if all selected races are excluded
        if ((bld.getExcludeEmpire() & mask) == mask)
          continue;
        if (!callback.apply(bld))
          return;
      }
    } else if (filter == FilterMask.AllEmps) {
      // process buildings that can be build by all the empires
      for (Building bld : buildings) {
        if (bld.getExcludeEmpire() != 0)
          continue;
        if (!callback.apply(bld))
          return;
      }
    } else if (filter == FilterMask.SingleResident) {
      // process buildings that have an exclusive resident race dependency
      for (Building bld : buildings) {
        if (Long.bitCount(bld.getResidentRaceReq()) != 1)
          continue;
        if (!callback.apply(bld))
          return;
      }
    } else {
      // process buildings that can exclusively be build by one single empire
      int mask = 1 << ((filter - FilterMask.AllEmps) - 1);
      for (Building bld : buildings) {
        // skip if not all but the filtered race are excluded
        if (((bld.getExcludeEmpire() ^ mask) & 0x1F) != 0x1F)
          continue;
        if (!callback.apply(bld))
          return;
      }
    }
  }

  /**
   * Returns a list of building IDs and names.
   * @param mask the race filter mask, @see FilterMask
   */
  public ID[] getIDs(int mask) {
    ArrayList<ID> ret = new ArrayList<ID>(buildings.size());

    processBuildings(mask, x -> {
      ret.add(new ID(x.getName(), x.id()));
      return true;
    });

    return ret.toArray(new ID[0]);
  }

  /**
   * Returns a list of building IDs and shortened names.
   * @param mask the race filter mask, @see FilterMask
   */
  public ID[] getShortIDs(int mask, int maxLen) {
    ArrayList<ID> ret = new ArrayList<ID>(buildings.size());

    processBuildings(mask, x -> {
      String name = x.getName();
      name = name.length() > maxLen
        ? name.substring(0, maxLen - 3) + "..." //$NON-NLS-1$
        : x.getName();
      ret.add(new ID(name, x.id()));
      return true;
    });

    return ret.toArray(new ID[0]);
  }

  /**
   * List used tech levels for the filtered buildings.
   *
   * @param mask the race filter mask, @see FilterMask
   * @return
   */
  public int[] usedTechLevels(int mask) {
    TreeSet<Integer> techLvls = new TreeSet<Integer>();

    processBuildings(mask, x -> {
      techLvls.add(Byte.toUnsignedInt(x.getTechLevel()));
      return true;
    });

    return techLvls.stream().mapToInt(Integer::intValue).toArray();
  }

  public int[] findMainStructureRange(int emp, int group) {
    byte mask = (byte) (1 << emp);
    int numEntries = getNumberOfEntries();

    // search for a continuous line of buildings
    for (int i = 0; i < numEntries; i++) {
      Building building = buildings.get(i);
      if (!matchesEmpireGroup(building, group, mask))
        continue;

      // found first building
      int firstBldIdx = i;

      // read next until not the same
      for (++i; i < numEntries; ++i) {
        // break on buildings that don't match the search criteria
        // for the main building blocks there must be no gaps
        building = buildings.get(i);
        if (!matchesEmpireGroup(building, group, mask))
          break;
      }

      // found last
      int lastBldIdx = i - 1;
      return new int[] {firstBldIdx, lastBldIdx};
    }

    return null;
  }

  public int getTrekExeID(int idType) throws IOException {
    val seg = (IntValueSegment)getSegment(idType, -1);
    return seg.intValue();
  }

  public int getTrekExeID(int idSetType, int race) throws IOException {
    val seg = getSegment(idSetType, race);

    if (seg instanceof MovRegInt)
      return ((MovRegInt) seg).intValue();
    else if (seg instanceof ShipyardBldIDs)
      return ((ShipyardBldIDs) seg).getID(race);
    else if (seg instanceof MinorRaceSpBuildingIDs)
      return ((MinorRaceSpBuildingIDs) seg).getID(race);
    else
      throw new UnsupportedOperationException("Unexpected data type.");
  }

  // #endregion getters

  // #region add & remove

  /**
   * Adds a building to the end of the list.
   *
   * @param copyIndex the index of the building to copy settings from.
   * @throws IOException
   * @throws InvalidArgumentsException
   */
  public Building add(int copyIndex, boolean increaseBldMask) throws IOException, InvalidArgumentsException {
    return add(buildings.size(), copyIndex, increaseBldMask);
  }

  /**
   * Adds a building to the specified index. Updates indices for the replaced and all the following
   * buildings. Updates upgrade indices for all the buildings.
   *
   * @param index     the position to insert the new building.
   * @param copyIndex the index of the building to copy settings from.
   * @param increaseBldMask
   * @throws IOException
   * @throws InvalidArgumentsException
   */
  public Building add(int index, int copyIndex, boolean increaseBldMask) throws IOException, InvalidArgumentsException {
    checkIndex(copyIndex);

    Building ne = new Building(buildings.get(copyIndex));
    ne.setId((short)index);
    ne.setUpgradeFor(Short.MAX_VALUE);
    buildings.add(index, ne);

    // when not added to the end, all the buildings need to be processed
    // to update the building upgrade indices
    if (index + 1 < buildings.size()) {
      // update building ids for the entries that follow
      // but process full list to also update the building upgrades
      // process in reverse order to not relocate the very same entry
      for (short revIdx = (short) (buildings.size() - 1); revIdx >= 0; revIdx--) {
        Building bld = buildings.get(revIdx);

        // update moved entries
        if (revIdx > index) {
          // fix ids in trek.exe
          relocateTrekID(bld.id(), revIdx);
          bld.setId(revIdx);
        }

        if (!bld.isUpgrade())
          continue;

        // update building upgrade indices
        short upgrade = bld.getUpgradeFor();
        if (upgrade >= index)
          bld.setUpgradeFor(++upgrade);
      }
    }

    // increase ignore list if necessary
    // note that the actually stored size is determined by the bldMaskLength member
    if (increaseBldMask)
      resizeBldMask();

    descChanged();
    markChanged();
    return ne;
  }

  /**
   * Removes a building from the list.
   *
   * @param index the index of the building to be removed.
   * @throws IOException
   * @throws InvalidArgumentsException
   */
  public void remove(final int index) throws IOException, InvalidArgumentsException {
    checkIndex(index);
    buildings.remove(index);

    // fix ids in trek.exe
    relocateTrekID(index, -1);

    // update ids
    for (int i = 0; i < buildings.size(); i++) {
      Building bld = buildings.get(i);

      if (i >= index) {
        // fix ids in trek.exe
        relocateTrekID(bld.id(), i);
        // update index
        bld.setId((short) i);
      }

      if (!bld.isUpgrade())
        continue;

      // update building upgrade indices
      short upgrade = bld.getUpgradeFor();
      if (upgrade == index)
        bld.setUpgradeFor(Short.MAX_VALUE);
      else if (upgrade > index)
        bld.setUpgradeFor(--upgrade);
    }

    descChanged();
    markChanged();
  }

  // #endregion add & remove

  // #region setters

  public void setTrekExeID(int idType, int id) throws IOException, InvalidArgumentsException {
    val seg = (IntValueSegment)getSegment(idType, -1);
    seg.setValue(id);
  }

  public void setTrekExeID(int idSetType, int race, int id) throws IOException, InvalidArgumentsException {
    val seg = getSegment(idSetType, race);

    if (seg instanceof MovRegInt)
      ((MovRegInt) seg).setValue(id);
    else if (seg instanceof ShipyardBldIDs)
      ((ShipyardBldIDs) seg).setID(race, id);
    else if (seg instanceof MinorRaceSpBuildingIDs)
      ((MinorRaceSpBuildingIDs) seg).setID(race, id);
    else
      throw new UnsupportedOperationException("Unexpected data type.");
  }

  public void setBldMaskLongs(int bldMaskLongs) {
    if (this.bldMaskLongs != bldMaskLongs) {
      this.bldMaskLongs = bldMaskLongs;
      markChanged();
    }
  }

  public void setTechLevels(int techLvls) {
    if (this.techLvls != techLvls) {
      this.techLvls = techLvls;
      markChanged();
    }
  }

  /**
   * sets the bonus for all buildings of the specified type.
   *
   * @param boni_type the type of building to edit
   * @param boni_calc bonus - a calculation with variable L (level) - see Calc.calculate()
   * @param empire    the required empire (built by)
   */
  public void multiSetBonus(byte boni_type, String[] calc, byte empire) throws InvalidArgumentsException {
    for (Building bld : buildings) {
      if ((bld.getBonusType() == boni_type) && ((bld.getExcludeEmpire() & empire) == 0)) {
        for (int j = 0; j < NUM_EMPIRES+1; j++) {
          String c = prepareCalc(bld, calc[j]);

          double val = Calc.calculate(c);
          val = Math.min(val, Short.MAX_VALUE);
          val = Math.max(val, Short.MIN_VALUE);

          bld.setBonusValue(j, (short)val);
          markChanged();
        }
      }
    }
  }

  /**
   * Sets the industry cost for all buildings of the specified type.
   *
   * @param boni_type the type of building to edit
   * @param cost_calc industry cost - a calculation with variable L (level) - see Calc.calculate()
   * @param empire    the required empire (built by)
   */
  public void multiSetIndustryCost(byte boni_type, String calc, byte empire) throws InvalidArgumentsException {
    for (Building bld : buildings) {
      if ((bld.getBonusType() == boni_type) && ((bld.getExcludeEmpire() & empire) == 0)) {
        String c = prepareCalc(bld, calc);

        double val = Calc.calculate(c);
        val = Math.min(val, Integer.MAX_VALUE);
        val = Math.max(val, Integer.MIN_VALUE);

        bld.setBuildCost((int)val);
        markChanged();
      }
    }
  }

  /**
   * Sets the morale bonus for all buildings of the specified type.
   *
   * @param boni_type the type of building to edit
   * @param calc      bonus - a calculation with variable L (level) - see Calc.calculate()
   * @param empire    the required empire (built by)
   */
  public void multiSetMorale(byte boni_type, String calc, byte empire) throws InvalidArgumentsException {
    for (Building bld : buildings) {
      if ((bld.getBonusType() == boni_type) && ((bld.getExcludeEmpire() & empire) == 0)) {
        String c = prepareCalc(bld, calc);
        double val = Calc.calculate(c);
        val = Math.min(val, Short.MAX_VALUE);
        val = Math.max(val, Short.MIN_VALUE);

        bld.setMoraleBonus((short)val);
        markChanged();
      }
    }
  }

  /**
   * Sets the energy maintenance for all buildings of the specified type.
   *
   * @param boni_type the type of building to edit
   * @param calc      bonus - a calculation with variable L (level) - see Calc.calculate()
   * @param empire    the required empire (built by)
   */
  public void multiSetEnergy(byte boni_type, String calc, byte empire) throws InvalidArgumentsException {
    for (Building bld : buildings) {
      if ((bld.getBonusType() == boni_type) && ((bld.getExcludeEmpire() & empire) == 0)) {
        String c = prepareCalc(bld, calc);
        double val = Calc.calculate(c);

        short engyReq = (short) Math.max(0, Math.min(val, Short.MAX_VALUE));
        short prevReq = bld.getEnergyCost();
        bld.setEnergyCost(engyReq);

        // if same, update AI building requirements
        val req = stbof.files().aiBldReq.getReq(bld.id()).orElse(null);
        if (req != null && req.getEnergyReq() == prevReq)
          req.setEnergyReq(engyReq);
      }
    }
  }

  /**
   * Sets the unrest bonus for all buildings of the specified type.
   *
   * @param boni_type the type of building to edit
   * @param calc      bonus - a calculation with variable L (level) - see Calc.calculate()
   * @param empire    the required empire (built by)
   */
  public void multiSetUnrestBonus(byte boni_type, String[] calc, byte empire) throws InvalidArgumentsException {
    for (Building bld : buildings) {
      if ((bld.getBonusType() == boni_type) && ((bld.getExcludeEmpire() & empire) == 0)) {
        for (int j = 0; j < NUM_EMPIRES+1; j++) {
          String c = prepareCalc(bld, calc[j]);

          double val = Calc.calculate(c);
          val = Math.min(val, Short.MAX_VALUE);
          val = Math.max(val, Short.MIN_VALUE);

          bld.setUnrestBonus(j, (short)val);
          markChanged();
        }
      }
    }
  }

  // #endregion setters

  // #region public helpers

  public void updateExe() throws IOException, InvalidArgumentsException {
    if (trek == null)
      return;

    // first load basic trek structure ids for the CTrekSegments:
    // OrbitalDefenseBld, DilithiumBld2, ShieldGenBld
    // TradeGoodsBld, CmpBasicShipyardId, BasicGroundDefenseId1;
    // MartialLawBld, ShipyardIds, MinorSPBuildingIds
    TrekIds trekIds = new TrekIds();
    trekIds.load();

    // READ ONLY
    val specialShips        = (SpecialShips) trek.getInternalFile(CTrekSegments.SpecialShips, false);

    // EDITED
    // num total buildings
    val bldCount            = trek.buildingCount();
    martialLaw.reload();

    // general building types
    val basicDilithium      = trek.getSegments(SD_BldId_Dilithium.All, true);
    val basicGroundDefenses = trek.getSegments(SD_BldId_GrndDef.All, true);
    val basicShields        = trek.getSegments(SD_BldId_Shield.All, true);
    val basicShipyards1     = trek.getSegments(SD_BldId_Shipyard.Basic1, true);
    val basicShipyards2     = trek.getSegments(SD_BldId_Shipyard.Basic2, true);
    val orbitals            = trek.getSegments(SD_BldId_Orb.IntVal, true);
    val orbital6            = (CmpWordPtrByte) trek.getSegment(SD_BldId_Orb.Orb6, true);
    val shipyards           = (ShipyardBldIDs) trek.getSegment(SD_BldId_Shipyard.Regular, true);
    val specialShipConstr   = (MovRegInt) trek.getSegment(SD_BldId_EmpSp.UtopiaPlanitia, true);
    val tradeGoods          = trek.getSegments(SD_BldId_TradeGoods.All, true);
    val manDil              = (MandatoryDilithiumFix) trek.getSegment(SD_MandatoryDilFix.DilithiumFix, true);

    // empire building types
    val fedFarm1            = (MovRegInt) trek.getSegment(SD_BldId_EmpSp.Fed1, true);
    val minorMainBlds       = (MinorRaceMainBldIDLimits) trek.getInternalFile(CTrekSegments.MinorMainBldIds, true);
    val mainBldUpgs         = (MainBldUpgrades) trek.getInternalFile(CTrekSegments.MainBldUpgrades, true);
    val punyFactories       = (PunyFactories) trek.getInternalFile(CTrekSegments.PunyFactories, true);
    val punyFarms           = (PunyFarms) trek.getInternalFile(CTrekSegments.PunyFarms, true);
    // main building ids
    mainStrcPatch.load();

    // num total buildings
    bldCount.setValue((short)getNumberOfEntries());

    // copy general building type ids
    // this step is required since both in relocateTrekID and in switchTrekIDs
    // only the single segment read by getTrekExeID is getting updated
    {
      // copy id from CTrekSegments.SpecialShips to SpecialShipsBld
      specialShipConstr.setValue(specialShips.getSPBuilding()); // utopia planitia

      // copy id from DilithiumBld2 to CTrekSegments:
      // DilithiumBld2, DilithiumBld3, DilithiumBld5,
      // DilithiumBld6, DilithiumBld7, DilithiumBld8
      for (val bld : basicDilithium)
        ((IntValue)bld).setValue(trekIds.basicDilithiumSource);

      // set auto-created major empire refinery id
      // if not disabled like with the BOP mod
      if (manDil.isDefault())
        manDil.setDilithiumRefineryId(trekIds.basicDilithiumSource);

      // copy id from BasicGroundDefenseId1 to CTrekSegments:
      // BasicGroundDefenseId1, BasicGroundDefenseId2, BasicGroundDefenseId3
      for (val bld : basicGroundDefenses)
        ((IntValue)bld).setValue(trekIds.basicGroundDefense);

      // copy id from ShieldGenBld to CTrekSegments:
      // ShieldGenBld, ShieldGenBld2, ShieldGenBld3
      for (val bld : basicShields)
        ((IntValue)bld).setValue(trekIds.basicShieldGenerator);

      // copy id from BasicShipyardId4 to CTrekSegments:
      // CmpBasicShipyardId, CmpBasicShipyardId2, CmpBasicShipyardId3,
      for (val bld : basicShipyards1)
        ((ByteValue)bld).setValue(trekIds.basicShipyard);
      // BasicShipyardId4, BasicShipyardId5, BasicShipyardId6
      for (val bld : basicShipyards2)
        ((IntValue)bld).setValue(trekIds.basicShipyard);

      // copy id from OrbitalDefenseBld to CTrekSegments:
      // OrbitalDefenseBld, OrbitalDefenseBld2, OrbitalDefenseBld3, OrbitalDefenseBld4,
      // OrbitalDefenseBld5, OrbitalDefenseBld6, OrbitalDefenseBld7, OrbitalDefenseBld8,
      // OrbitalDefenseBld9, OrbitalDefenseBld10, OrbitalDefenseBld11, OrbitalDefenseBld12,
      // OrbitalDefenseBld13
      for (val bld : orbitals)
        ((IntValue)bld).setValue(trekIds.orbitalBattery);
      orbital6.setValue(trekIds.orbitalBattery);

      // copy id from TradeGoodsBld to CTrekSegments:
      // TradeGoodsBld, TradeGoodsBld2, TradeGoodsBld3
      for (val bld : tradeGoods)
        ((IntValue)bld).setValue(trekIds.tradeGoods);

      // empire building types
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        // default removed CTrekSegments.ShipyardIds to CmpBasicShipyardId
        if (trekIds.shipyard[emp] < 0)
          shipyards.setID(emp, trekIds.basicShipyard);

        // copy id from MartialLawBld to CTrekSegments:
        // MartialLawBld, MartialLawBld2, MartialLawBld3, MartialLawBld4
        martialLaw.setID(emp, (short)trekIds.martialLaw[emp]);
      }

      // set minor race shipyard
      // when the checkbox is toggled, only CTrekSegments.CmpBasicShipyardId is set
      shipyards.setID(5, trekIds.basicShipyard);
    }

    // update main buildings
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      for (int group = 0; group < 5; group++) {
        int[] range = findMainStructureRange(emp, group);
        if (range == null)
          throw new IOException("No main builings found for filter group " + mapStructureGroupName(group));

        int upgIdx = 0;
        for (int id = range[0]; id < range[1]; id++) {
          Building building = buildings.get(id);

          if (group == StructureGroup.MainFood) {
            // set fed farm id
            if (emp == CStbof.Race.Fed && building.getTechLevel() == 1)
              fedFarm1.setValue(id);
          }

          // Update the building upgrades,
          // but don't enforce upgrades each tech level.
          // Mods like BOP and MUM skip some tech levels!
          if (upgIdx < NUM_MAINBLD_UPGRADES) {
            switch (group) {
              case StructureGroup.MainResearch:
                mainBldUpgs.setScienceID(emp, upgIdx, (short)id);
                break;
              case StructureGroup.MainIntel:
                mainBldUpgs.setIntelID(emp, upgIdx, (short)id);
                break;
              case StructureGroup.MainEnergy:
                mainBldUpgs.setEnergyID(emp, upgIdx, (short)id);
                break;
              case StructureGroup.MainFood:
                // skip primitive farms from upgrade table
                // they are however used below to update the 'MinorRaceMainBldIDLimits'
                if (upgIdx > 0)
                  mainBldUpgs.setFoodID(emp, upgIdx-1, (short)id);
                break;
              case StructureGroup.MainIndustry:
                mainBldUpgs.setIndustryID(emp, upgIdx, (short)id);
                break;
            }
          }

          upgIdx++;
        }

        // set remaining upgrade slots to last valid building
        for (; upgIdx < NUM_MAINBLD_UPGRADES; upgIdx++) {
          switch (group) {
            case StructureGroup.MainResearch:
              mainBldUpgs.setScienceID(emp, upgIdx, (short) range[1]);
              break;
            case StructureGroup.MainIntel:
              mainBldUpgs.setIntelID(emp, upgIdx, (short) range[1]);
              break;
            case StructureGroup.MainEnergy:
              mainBldUpgs.setEnergyID(emp, upgIdx, (short) range[1]);
              break;
            case StructureGroup.MainFood:
              mainBldUpgs.setFoodID(emp, upgIdx, (short) range[1]);
              break;
            case StructureGroup.MainIndustry:
              mainBldUpgs.setIndustryID(emp, upgIdx, (short) range[1]);
              break;
          }
        }

        // set trek.exe values
        switch (group) {
          case StructureGroup.MainResearch: {
            mainStrcPatch.setMinId(emp, StructureGroup.MainResearch, range[0]);
            mainStrcPatch.setMaxId(emp, StructureGroup.MainResearch, range[1]);
            minorMainBlds.setResearchIDSpan(emp, (short) range[0], (short) range[1]);
            break;
          }
          case StructureGroup.MainIntel: {
            mainStrcPatch.setMinId(emp, StructureGroup.MainIntel, range[0]);
            mainStrcPatch.setMaxId(emp, StructureGroup.MainIntel, range[1]);
            break;
          }
          case StructureGroup.MainIndustry: {
            punyFactories.setPunyFactory(emp, range[0]);
            // shared industry / food end is updated by StructureGroup.MainFood
            // it may have disabled intermediate entries but both blocks always must follow each other
            // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=9314#p9314
            mainStrcPatch.setMinId(emp, StructureGroup.MainIndustry, range[0]);
            minorMainBlds.setIndustryIDSpan(emp, (short) range[0], (short) range[1]);
            break;
          }
          case StructureGroup.MainEnergy: {
            mainStrcPatch.setMinId(emp, StructureGroup.MainEnergy, range[0]);
            mainStrcPatch.setMaxId(emp, StructureGroup.MainEnergy, range[1]);
            minorMainBlds.setEnergyIDSpan(emp, (short) range[0], (short) range[1]);
            break;
          }
          case StructureGroup.MainFood: {
            minorMainBlds.setFoodIDSpan(emp, (short) range[0], (short) range[1]);

            // ignore primitive farms for major races
            while ((buildings.get(range[0]).getTechLevel() == 0) && (range[0] < range[1])) {
              range[0]++;
            }

            mainStrcPatch.setMinId(emp, StructureGroup.MainFood, range[0]);
            mainStrcPatch.setMaxId(emp, StructureGroup.MainFood, range[1]);
            punyFarms.setPunyFarm(emp, range[0]);
            break;
          }
        }
      }
    }
  }

  /**
   * Updates the ignore list and groups.
   */
  public void updateIgnoreList() {
    int maxBuildings = Integer.min(buildings.size(), getMaxBuildings());

    for (int bldId = 0; bldId < maxBuildings; bldId++) {
      Building bld = buildings.get(bldId);

      // ignore list
      byte emp_mask = bld.getExcludeEmpire();

      // compute mask index and flag offset
      int maskIndex = 0;
      int flagOffset = bldId;
      while (flagOffset >= MaxLongFlags) {
        maskIndex++; // the part (0-5)
        flagOffset = flagOffset - MaxLongFlags;
      }

      // the bit used for this building is positive
      long flag = 1L << flagOffset;

      for (int empire = 0; empire < NUM_EMPIRES; empire++) {
        long[] bldMask = ignoreList[empire];

        // get current buildings mask
        long mask = bldMask[maskIndex];

        // check race mask of the iterated building for
        // whether it can be built by the given empire
        int race_mask = 1 << empire;
        boolean canBuild = (race_mask & emp_mask) == 0;

        if (canBuild) {
          // means the building is buildable by this empire
          // don't ignore-set as 0
          // AND
          // bld_mask  ~bld_bit  result
          // 0         0         = 0    -> means used, stays the same
          // 0         1         = 0    -> stays the same for rest of ids
          // 1         0         = 0    -> change to not be ignored
          // 1         1         = 1    -> stays the same for rest of ids
          // in ~bld_bit all bits are set as 1 except the one now being set
          // in bld_mask there are both bits 1 and 0
          // all bits must remain as in the original, except the bit to be set
          bldMask[maskIndex] = mask & (~flag);
        } else {
          // ignore-set as 1
          // OR
          // bld_mask  bld_bit   result
          // 0         0         = 0    -> stays as it was
          // 0         1         = 1    -> set to ignore
          // 1         0         = 1    -> stays as it was
          // 1         1         = 1    -> set to ignore
          // in bld_bit all bits are set as 0 except the one now being set
          // in bld_mask there are both bits 1 and 0
          // all bits must remain as in the original, except the bit to be set
          bldMask[maskIndex] = mask | flag;
        }
      }
    }
  }

  public void adjustBldMaskLongs() {
    int reqBldMaskLongs = getReqBldMaskLongs();

    // if not exceeded, stick to original size
    reqBldMaskLongs = Integer.max(reqBldMaskLongs, loadedBldMaskLongs);

    if (reqBldMaskLongs != bldMaskLongs) {
      bldMaskLongs = reqBldMaskLongs;
      markChanged();
    }
  }

  public void adjustTechLevels() {
    int reqTechLvls = 1 + getMaxTechLevel();
    if (reqTechLvls > techLvls) {
      techLvls = reqTechLvls;
      markChanged();
    }
  }

  /**
   * @param index1 index of the buildings to be switched
   * @param index2 index of the buildings to be switched
   * @throws IOException
   * @throws InvalidArgumentsException
   */
  public void switchIndex(int index1, int index2) throws IOException, InvalidArgumentsException {
    checkIndex(index1);
    checkIndex(index2);

    Building bld = buildings.get(index1);
    Building bld2 = buildings.get(index2);

    bld.setId((short)index2);
    bld2.setId((short)index1);

    buildings.set(index1, bld2);
    buildings.set(index2, bld);

    for (Building bld3 : buildings) {
      if (bld3.getUpgradeFor() == bld.id())
        bld3.setUpgradeFor(bld2.id());
      else if (bld3.getUpgradeFor() == bld2.id())
        bld3.setUpgradeFor(bld.id());
    }

    // fix ids in trek.exe
    switchTrekIDs(index1, index2);
    markChanged();
  }

  public String getTrekExeIDDescription(int idType) {
    switch (idType) {
      case StructureType.BasicScanner:
        return "Basic scanner";
      case StructureType.OrbitalBattery:
        return "Orbital battery";
      case StructureType.BasicDilithiumSource:
        return "Basic dilithium source";
      case StructureType.BasicShieldGenerator:
        return "Basic shield generator";
      case StructureType.TradeGoods:
        return "Trade goods";
      case StructureType.BasicShipyard:
        return "Basic shipyard";
      case StructureType.BasicGroundDefense:
        return "Basic ground defense";

      // id sets
      case StructureType.MartialLaw:
        return "%1 martial law";
      case StructureType.Shipyard:
        return "%1 shipyard";
      case StructureType.MinorShipBuild:
        return "%1 special building";
      default:
        return null;
    }
  }

  // #endregion public helpers

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    buildings.clear();

    // HEADER
    int numBuildings = StreamTools.readInt(in, true);               // 4 bytes
    // read address of the map where buildings addresses are located
    int mapAddress = StreamTools.readInt(in, true);                 // 4 bytes

    // FILE SPECIFIED OFFSETS
    // calculate building address
    int bldListAddress = mapAddress - numBuildings * Building.SIZE;
    // calculate remaining tech map & ignore list size
    int techIgnoreSize = bldListAddress - (8 + TechFieldCntrSize);  // building address map offset - 24 bytes

    // VARIABLE OFFSETS
    // to function properly, the ignore list must be increased when there are more than 6*8*8 = 384 buildings
    // => 6 longs * 5 empires * 8 bytes makes 240 bytes for the ignore list by default
    loadedBldMaskLongs = DEFAULT_BldMask_Longs;                     // 6 longs by default
    // for max possible tech map size subtract default ignore list  // 476 bytes by default
    int techMapSize = techIgnoreSize - DEFAULT_IgnoreList_Size;
    // for the tech level count first subtract 7 * 2 = 14 bytes of tech field spacing
    int techLvlSize = techMapSize - TechFieldCnt * TechField_Alignment;
    loadedTechLvls = techLvlSize / TechLvl_IncSize;

    // first check defaults, even if exceeded by modding
    if (techMapSize != DEFAULT_TechMap_Size) {
      System.out.println("Changed tech map or ignore list size detected.");

      // determine excessive tech map size
      int excessive = techLvlSize % TechLvl_IncSize;

      // calculate tech levels
      if (excessive > 0) {
        // expect build mask to hold all buildings, but at least the default max 384 ones
        int expBldMaskLongs = Integer.max(DEFAULT_BldMask_Longs, DataTools.ceilDiv(numBuildings, 64));
        int expIgnoreListSize = expBldMaskLongs * Long.BYTES * NUM_EMPIRES;

        // check whether the required buildings count is matched
        if (excessive + DEFAULT_IgnoreList_Size == expIgnoreListSize) {
          loadedBldMaskLongs = expBldMaskLongs;
        } else {
          // expect num techtree.tec tech field levels
          int expTechMapLvls = techTree != null ? 1 + techTree.getMaxTechLevel() : DEFAULT_TechLvlCount;
          int expTechMapSize = expTechMapLvls * TechLvl_IncSize + TechFieldCnt * TechField_Alignment;

          // check whether there is a match on the computed total size requirements
          // which might be true when calculated techLvls > expTechMapLvls
          if (expTechMapSize + expIgnoreListSize == techIgnoreSize) {
            loadedBldMaskLongs = expBldMaskLongs;
            techMapSize = expTechMapSize;
            loadedTechLvls = expTechMapLvls;
          } else {
            // adjust build mask for the excessive space
            loadedBldMaskLongs = DEFAULT_BldMask_Longs + excessive / BldMask_IncSize;

            // calculate tech levels to subtract for extending the building mask
            // therefore leverage the 2 bytes offset of TechLvl_IncSize - BldMask_IncSize = 2 bytes
            // 40 bytes remaining = 1 building mask increment of 64 buildings per empire
            // 38 bytes remaining = 2 building mask increments
            // ...
            int rem = excessive % BldMask_IncSize;
            int lvlSubs = rem > 0 ? (BldMask_IncSize - rem) / 2 : 0;

            // check that there is actually enough space available
            if (lvlSubs > 0 && lvlSubs < loadedTechLvls) {
              // add 1 additional long value and the subs when there is enough space found
              // to make use of the remaining excessive space
              loadedBldMaskLongs += 1 + lvlSubs;

              int ignoreListSize = loadedBldMaskLongs * BldMask_IncSize;
              techMapSize = techIgnoreSize - ignoreListSize;
              loadedTechLvls -= lvlSubs;
            }
          }
        }
      }

      int bldFlags = loadedBldMaskLongs * Long.BYTES * 8;
      String msg = "Detected %1 tech levels and %2 max building mask flags."
        .replace("%1", Integer.toString(loadedTechLvls))
        .replace("%2", Integer.toString(bldFlags));
      System.out.println(msg);
    }

    techLvls = loadedTechLvls;
    bldMaskLongs = loadedBldMaskLongs;
    ignoreList = new long[NUM_EMPIRES][loadedBldMaskLongs];

    // skip the tech field counters & tech level map
    // it will be calculated on save
    StreamTools.skip(in, TechFieldCntrSize + techMapSize);

    // for each empire
    for (int i = 0; i < ignoreList.length; i++) {
      // 5*8 bytes = 40
      for (int j = 0; j < loadedBldMaskLongs; j++) {
        // The bits flow from left to right in file, but here they are reversed to ease
        // calculation of masks. They are then again reversed when saving.
        ignoreList[i][j] = StreamTools.readLong(in, true);
      }
    }

    for (int i = 0; i < numBuildings; ++i) {
      Building bld = new Building(this, in, i);
      buildings.add(bld);
    }

    // sort by the building IDs
    Collections.sort(buildings);
    markSaved();
  }

  // #endregion load

  // #region save

  /**
   * Saves file to OutputStream.
   * @throws Exception
   */
  @Override
  public void save(OutputStream out) throws IOException {
    updateIgnoreList();

    // parameters
    final int numBuildings = buildings.size();
    final int bldMaskSize = bldMaskLongs * Long.BYTES;

    // compute address offsets
    final int techFieldSize = techLvls * TechFieldLvl_Size + TechFieldEntry_Alignment; // 68 bytes by default
    final int techLvlMapSize = TechFieldCnt * techFieldSize; // 476 bytes by default
    final int techMapSize = TechFieldCnt * Short.BYTES + TechFieldEntry_Alignment + techLvlMapSize; // 492 bytes by default
    final int ignoreListSize = NUM_EMPIRES * bldMaskSize; // 240 bytes by default
    final int buildingListSize = numBuildings * Building.SIZE; // 176*x bytes
    final int bldStartAddress = HeaderSize + techMapSize + ignoreListSize;
    final int mapAddress = bldStartAddress + buildingListSize;

    // write number of buildings -> 0+4 = 4 bytes
    out.write(DataTools.toByte(numBuildings, true));
    // write map address -> 4+4 = 8 bytes
    out.write(DataTools.toByte(mapAddress, true));

    // write tech field counters & tech level map 8+500
    {
      // tech field building counters
      short[] techFieldBldCnt = new short[TechFieldCnt];
      // building counters for each of the tech field levels (7 techs * 11 lvls)
      short[] techLvlBldCnt = new short[TechFieldCnt * techLvls];

      for (Building bld : buildings) {
        // count buildings per tech field
        techFieldBldCnt[bld.getTechField()]++;
        // count buildings per tech field and level
        // add all above tech 10 levels to last counter
        int techLvl = Integer.min(bld.getTechLevel(), techLvls-1);
        techLvlBldCnt[bld.getTechField() * techLvls + techLvl]++;
      }

      // write tech fields -> 8+7*2 = 22 bytes
      out.write(DataTools.toByte(techFieldBldCnt, true));

      // 2 byte alignment -> 22+2 = 24 bytes
      out.write(0);
      out.write(0);

      // write tech level map of building addresses and numbers -> 24+7*(11*6+2) = 500 bytes
      int bldAddr = bldStartAddress;
      for (int tech = 0; tech < TechFieldCnt; tech++) {
        // compute & write addresses per tech level
        for (int lvl = 0; lvl < techLvls; lvl++) {
          int bldCnt = techLvlBldCnt[tech * techLvls + lvl];
          int addr = bldCnt > 0 ? bldAddr : 0;

          // for tech level 0 fallback to list the NEXT tech level building
          // this might not be relevant, but so it is done with unnmodified vanilla BotF
          if (addr == 0 && lvl == 0 && techLvlBldCnt[tech * techLvls + 1] > 0)
            addr = bldAddr;

          out.write(DataTools.toByte(addr, true));
          bldAddr += bldCnt * Building.SIZE;
        }

        // numbers
        for (int lvl = 0; lvl < techLvls; lvl++) {
          short bldCnt = techLvlBldCnt[tech * techLvls + lvl];
          out.write(DataTools.toByte(bldCnt, true));
        }

        // 2 byte alignment
        out.write(0);
        out.write(0);
      }
    }

    // ignore list -> 500+5*6*8 = 740 bytes
    // for each major race
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      long[] ign = ignoreList[emp];
      // force to write the configured size and zero or cut the rest
      for (int i = 0; i < bldMaskLongs; ++i) {
        long val = i < ign.length ? ign[i] : 0;
        out.write(DataTools.toByte(val, true));
      }
    }

    // For the tech map, the buildings must be sorted by tech type and by tech level.
    // In unmodified vanilla BotF the id and race however were left out of order.
    // To not sneak in a ton of unnecessary changes, fallback to sort by the load order.
    ArrayList<Building> techLvlSorted = new ArrayList<Building>(buildings);
    techLvlSorted.sort(new Comparator<Building>() {
      @Override
      public int compare(Building b1, Building b2) {
        int comp = Byte.compare(b1.getTechField(), b2.getTechField());
        if (comp == 0)
          comp = Byte.compare(b1.getTechLevel(), b2.getTechLevel());
        if (comp == 0)
          comp = Integer.compare(b1.getLoadOrder(), b2.getLoadOrder());
        return comp;
      }
    });

    // Now save the buildings while computing the address map. -> 740+176*x
    int[] addresses = new int[buildings.size()];
    int bldAddr = bldStartAddress;

    for (Building building : techLvlSorted) {
      // write the building data
      building.save(out);
      // save current (building) address
      addresses[building.id()] = bldAddr;
      bldAddr += Building.SIZE;
    }

    // write address map of the building ids -> +4*x
    out.write(DataTools.toByte(addresses, true));
  }

  // #endregion save

  // #region maintenance

  /**
   * Returns the size of the file
   */
  @Override
  public int getSize() {
    return 740 + buildings.size() * 180;
  }

  @Override
  public void clear() {
    for (long[] ignors : ignoreList)
      Arrays.fill(ignors, 0);
    buildings.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region check

  @Override
  public void check(Vector<String> response) {
    super.check(response);
    // check & list the main structure id ranges
    checkMainStructures(response);
    // check empire ignore mask
    checkBuildingMask(response);
    // check tech level mappings
    checkTechLevels(response);
    // check buildings
    checkBuildings(response);
    // check trek.exe segment & data integrity
    checkTrekExe(response);
    // now check images
    checkImages(response);
  }

  private int[] checkMainStructures(int empire, int structureGroup, Vector<String> response) {
    String groupName = mapStructureGroupName(structureGroup);

    // find main structures including the primitive farms,
    // that is set for the MinorRaceMainBldIDLimits
    int[] range = findMainStructureRange(empire, structureGroup);

    // check if any has been found
    if (range == null || range[0] > range[1]) {
      String msg = Language.getString("Edifice.8") //$NON-NLS-1$
        .replace("%1", groupName); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      return null;
    }

    // list found building range
    String msg = Language.getString("Edifice.7"); //$NON-NLS-1$
    msg = msg.replace("%1", Integer.toString(empire)); //$NON-NLS-1$
    msg = msg.replace("%2", groupName); //$NON-NLS-1$
    msg = msg.replace("%3", Integer.toString(range[0])); //$NON-NLS-1$
    msg = msg.replace("%4", Integer.toString(range[1])); //$NON-NLS-1$
    response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));

    // check whether there are any further buildings of same type
    int numEntries = getNumberOfEntries();
    byte mask = (byte) (1 << empire);
    for (int id = range[1]+1; id < numEntries; ++id) {
      Building building = buildings.get(id);
      if (matchesEmpireGroup(building, structureGroup, mask)) {
        String err = Language.getString("Edifice.13") //$NON-NLS-1$
          .replace("%1", Integer.toString(empire)) //$NON-NLS-1$
          .replace("%2", groupName) //$NON-NLS-1$
          .replace("%3", Integer.toString(id)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, err));
        break;
      }
    }

    return range;
  }

  private void checkMainStructures(Vector<String> response) {
    for (int emp = 0; emp < NUM_EMPIRES; emp++) {
      int[] foodRange = null;
      int[] industryRange = null;
      try {
        // check & lists the main structure id ranges
        industryRange = checkMainStructures(emp, StructureGroup.MainIndustry, response);
        foodRange =     checkMainStructures(emp, StructureGroup.MainFood, response);
        checkMainStructures(emp, StructureGroup.MainIntel, response);
        checkMainStructures(emp, StructureGroup.MainEnergy, response);
        checkMainStructures(emp, StructureGroup.MainResearch, response);
      } catch (Exception e) {
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR,
          e.getLocalizedMessage()));
      }

      if (foodRange != null && industryRange != null) {
        // check that the industry group is followed by food group
        if (foodRange[0] <= industryRange[1]) {
          String fr = Integer.toString(foodRange[0]) + '-' + Integer.toString(foodRange[1]);
          String ir = Integer.toString(industryRange[0]) + '-' + Integer.toString(industryRange[1]);
          String err = Language.getString("Edifice.11") //$NON-NLS-1$
            .replace("%1", fr).replace("%2", ir); //$NON-NLS-1$  //$NON-NLS-2$
          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, err));
        }

        // check that there are no intermediates but disabled structures
        // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=9314#p9314
        for (int id = industryRange[1]+1; id < foodRange[0]; ++id) {
          Building bld = buildings.get(id);
          int exemp = bld.getExcludeEmpire();

          boolean disabled = (exemp & MASK_ALL_EMP) == MASK_ALL_EMP;
          if (!disabled) {
            String fr = Integer.toString(foodRange[0]) + '-' + Integer.toString(foodRange[1]);
            String ir = Integer.toString(industryRange[0]) + '-' + Integer.toString(industryRange[1]);
            String err = Language.getString("Edifice.12") //$NON-NLS-1$
              .replace("%1", fr).replace("%2", ir); //$NON-NLS-1$  //$NON-NLS-2$
            response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, err));
            break;
          }
        }
      }
    }
  }

  private void checkBuildingMask(Vector<String> response) {
    // check that the empire ignore mask is large enough to flag all the ships
    int reqBldMaskLongs = getReqBldMaskLongs();
    if (reqBldMaskLongs > bldMaskLongs) {
      int numBuildings = buildings.size();
      int maxBuildings = reqBldMaskLongs * Long.BYTES * 8;
      String msg = Language.getString("Edifice.9") //$NON-NLS-1$
        .replace("%1", Integer.toString(numBuildings))
        .replace("%2", Integer.toString(maxBuildings));
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  private void checkTechLevels(Vector<String> response) {
    // check that the max tech level of the tech map is not exceeded
    int reqTechLvls = 1 + getMaxTechLevel();
    if (reqTechLvls > techLvls) {
      String msg = Language.getString("Edifice.10") //$NON-NLS-1$
        .replace("%1", Integer.toString(reqTechLvls))
        .replace("%2", Integer.toString(techLvls));
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  private void checkBuildings(Vector<String> response) {
    HashMap<Short, Short> updatedIds = new HashMap<>();
    short i = 0;
    short next = 0;

    for (Building bld : buildings) {
      // correct ids so they all are unique and in sequence
      if (bld.id() != i) {
        // only report id fixes that are out of sequence
        if (bld.id() != next) {
          String msg = Language.getString("Edifice.14") //$NON-NLS-1$
            .replace("%1", Short.toString(next))
            .replace("%2", Integer.toString(bld.id() - 1));
          msg += " (fixed)";
          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        }

        next = (short)(bld.id() + 1);
        updatedIds.put(bld.id(), i);
        bld.setId(i);
      }

      // check building properties
      bld.check(response);
      i++;
    }

    // fix building upgrades
    for (Building bld : buildings) {
      Short newId = updatedIds.get(bld.getUpgradeFor());
      if (newId != null) {
        bld.setUpgradeFor(newId);
      } else if (bld.getUpgradeFor() != Short.MAX_VALUE && bld.getUpgradeFor() >= buildings.size()) {
        String msg = Language.getString("Edifice.15") //$NON-NLS-1$
          .replace("%1", Short.toString(bld.getUpgradeFor()))
          .replace("%2", Short.toString(bld.id()))
          .replace("%3", Integer.toString(buildings.size()));
        msg += " (unset)";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));

        // reset invalid building upgrades
        bld.setUpgradeFor(Short.MAX_VALUE);
      }
    }
  }

  private void checkTrekExe(Vector<String> response) {
    // check trek.exe key structure type availability
    checkTrekExe_KeyStructures(response);
  }

  private void checkTrekExe_KeyStructures(Vector<String> response) {
    try {
      int id = getTrekExeID(StructureType.BasicScanner);
      if (id < 0) {
        String msg = "Missing basic scanner available to all races.";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      id = getTrekExeID(StructureType.OrbitalBattery);
      if (id < 0) {
        String msg = "Missing orbital battery available to all races.";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      } else if (id > Byte.MAX_VALUE) {
        String msg = "Orbital battery found but has id > %1."
            .replace("%1", Byte.toString(Byte.MAX_VALUE));
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      id = getTrekExeID(StructureType.BasicDilithiumSource);
      if (id < 0) {
        String msg = "Missing basic dilithium source available to all races.";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      id = getTrekExeID(StructureType.BasicShieldGenerator);
      if (id < 0) {
        String msg = "Missing basic shield generator available to all races.";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      id = getTrekExeID(StructureType.TradeGoods);
      if (id < 0) {
        String msg = "Missing trade goods available to all races.";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      id = getTrekExeID(StructureType.BasicGroundDefense);
      if (id < 0) {
        String msg = "Missing basic ground defense available to all races.";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      id = getTrekExeID(StructureType.BasicShipyard);
      if (id < 0) {
        String msg = "Missing basic shipyard available to all races.";
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
      }

      RaceRst race = null;
      try {
        race = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);
      } catch (IOException ex) {
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, ex.getMessage()));
      }

      // check martial law
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        id = getTrekExeID(StructureType.MartialLaw, emp);
        if (id < 0) {
          String msg = "Missing martial law building for the %1.";

          if (race != null) {
            try {
              msg = msg.replace("%1", race.getName(emp));
            } catch (Exception ex) {
              String err = race.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, ex.getMessage());
              response.add(err);
            }
          } else {
            msg = msg.replace("%1", "empire with id %1.".replace("%1", Integer.toString(emp)));
          }

          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        }
      }

      // check minors special building
      if (race != null) {
        for (int r = NUM_EMPIRES; r < race.getNumberOfEntries(); r++) {
          id = getTrekExeID(StructureType.MinorShipBuild, r);
          if (id < 0) {
            String msg = "Missing special minor race building for the %1."
              .replace("%1", race.getName(r));
            response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
          }
        }
      }
    } catch (Exception ex) {
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, ex.getMessage()));
    }
  }

  private void checkImages(Vector<String> response) {
    Trek trek = UE.FILES.trek();

    try (FileStore fs = stbof.access()) {
      int orbId = getTrekExeID(StructureType.OrbitalBattery);
      val spMinorBlds = listSpMinorBuildings();

      val bldBaseReqs = WDFImgReq.listEdificeBaseReqs(trek, stbof, response);
      val bldConsReq = WDFImgReq.getEdificeConsReq(trek, stbof, response);
      val bldStrcReq = WDFImgReq.getEdificeStrcReq(trek, stbof, response);
      val bldMainReq = WDFImgReq.getEdificeMainBldReq(trek, stbof, response);
      val bldEngyReq = WDFImgReq.getEdificeEngyBldReq(trek, stbof, response);
      val bldSpMnrReq = WDFImgReq.getEdificeSpMnrReq(trek, stbof, response);
      val bldOrbReq = WDFImgReq.getEdificeOrbitalReq(trek, stbof, response);
      HashSet<String> alreadyChecked = new HashSet<>();

      for (Building bld : buildings) {
        String gfx = bld.getImagePrefix();
        if (!alreadyChecked.add(gfx))
          continue;

        WDFImgReq.checkStbofImageReqs(fs, stbof, this, gfx, bldBaseReqs, response);

        if (bld.isConsumable()) {
          // don't add morale buildings *w.tga image
          if (bldConsReq.isPresent())
            WDFImgReq.checkStbofImageReq(fs, stbof, this, gfx, bldConsReq.get(), response);
        }
        else {
          if (bldStrcReq.isPresent())
            WDFImgReq.checkStbofImageReq(fs, stbof, this, gfx, bldStrcReq.get(), response);

          if (bld.isMainBuilding()) {
            if (bldMainReq.isPresent())
              WDFImgReq.checkStbofImageReq(fs, stbof, this, gfx, bldMainReq.get(), response);
          }
          else if (bld.getEnergyCost() > 0) {
            if (bldEngyReq.isPresent())
              WDFImgReq.checkStbofImageReq(fs, stbof, this, gfx, bldEngyReq.get(), response);
          }
          else if (spMinorBlds.contains((int) bld.id())) {
            if (bldSpMnrReq.isPresent())
              WDFImgReq.checkStbofImageReq(fs, stbof, this, gfx, bldSpMnrReq.get(), response);
          }
          else if (bld.id() == orbId) {
            if (bldOrbReq.isPresent())
              WDFImgReq.checkStbofImageReq(fs, stbof, this, gfx, bldOrbReq.get(), response);
          }
        }
      }
    }
    catch (Exception e) {
      String err = "Failed to check " + NAME + " images.";
      response.add(err);
    }
  }

  // #endregion check

  // #region private helpers

  private void checkIndex(int index) {
    if ((index < 0) || (index >= buildings.size())) {
      String msg = Language.getString("Edifice.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private Set<Integer> listSpMinorBuildings() throws IOException {
    RaceRst raceRst = stbof.files().raceRst();
    int numRaces = raceRst.getNumberOfEntries();
    HashSet<Integer> spMinorBlds = new HashSet<>(numRaces);
    for (int i = NUM_EMPIRES; i < numRaces; ++i)
      spMinorBlds.add(getTrekExeID(StructureType.MinorShipBuild, i));

    return spMinorBlds;
  }

  private boolean matchesEmpireGroup(Building building, int group, byte empireMask) {
    // check main building group
    if (building.getGroup() != group)
      return false;

    // check main building population assignment
    if (building.getPopulationCost() < 1)
      return false;

    // check whether it can be build by the race mask
    byte exemp = building.getExcludeEmpire();
    return (exemp & empireMask) == 0;
  }

  private String mapStructureGroupName(int structureGroup) {
    switch (structureGroup) {
      case StructureGroup.MainResearch:
        return Language.getString("Edifice.1"); //$NON-NLS-1$
      case StructureGroup.MainIntel:
        return Language.getString("Edifice.2"); //$NON-NLS-1$
      case StructureGroup.MainIndustry:
        return Language.getString("Edifice.3"); //$NON-NLS-1$
      case StructureGroup.MainEnergy:
        return Language.getString("Edifice.4"); //$NON-NLS-1$
      case StructureGroup.MainFood:
        return Language.getString("Edifice.5"); //$NON-NLS-1$
      default:
        throw new IllegalArgumentException();
    }
  }

  private void resizeBldMask() {
    int reqBldMaskLongs = getReqBldMaskLongs();

    // if not exceeded, stick to original size
    reqBldMaskLongs = Integer.max(reqBldMaskLongs, loadedBldMaskLongs);

    if (bldMaskLongs != reqBldMaskLongs) {
      for (int i = 0; i < ignoreList.length; ++i) {
        // ignore shrinks, setting the bldMaskLongs should suffice
        if (reqBldMaskLongs > ignoreList[i].length)
          ignoreList[i] = Arrays.copyOf(ignoreList[i], reqBldMaskLongs);
      }
      bldMaskLongs = reqBldMaskLongs;
      markChanged();
    }
  }

  // replace variables with values
  private String prepareCalc(Building bld, String calc) {
    String c = calc;

    c = c.replaceAll("L", Integer.toString(bld.getTechLevel())); //$NON-NLS-1$
    c = c.replaceAll("U0", Integer.toString(bld.getUnrestBonus(0))); //$NON-NLS-1$
    c = c.replaceAll("U1", Integer.toString(bld.getUnrestBonus(1))); //$NON-NLS-1$
    c = c.replaceAll("U2", Integer.toString(bld.getUnrestBonus(2))); //$NON-NLS-1$
    c = c.replaceAll("U3", Integer.toString(bld.getUnrestBonus(3))); //$NON-NLS-1$
    c = c.replaceAll("U4", Integer.toString(bld.getUnrestBonus(4))); //$NON-NLS-1$
    c = c.replaceAll("U5", Integer.toString(bld.getUnrestBonus(5))); //$NON-NLS-1$
    c = c.replaceAll("E", Integer.toString(bld.getEnergyCost())); //$NON-NLS-1$
    c = c.replaceAll("M", Integer.toString(bld.getMoraleBonus())); //$NON-NLS-1$
    c = c.replaceAll("C", Integer.toString(bld.getBuildCost())); //$NON-NLS-1$
    c = c.replaceAll("B0", Integer.toString(bld.getBonusValue(0))); //$NON-NLS-1$
    c = c.replaceAll("B1", Integer.toString(bld.getBonusValue(1))); //$NON-NLS-1$
    c = c.replaceAll("B2", Integer.toString(bld.getBonusValue(2))); //$NON-NLS-1$
    c = c.replaceAll("B3", Integer.toString(bld.getBonusValue(3))); //$NON-NLS-1$
    c = c.replaceAll("B4", Integer.toString(bld.getBonusValue(4))); //$NON-NLS-1$
    c = c.replaceAll("B5", Integer.toString(bld.getBonusValue(5))); //$NON-NLS-1$

    return c;
  }

  private void relocateTrekID(int oldID, int newID) throws IOException, InvalidArgumentsException {
    for (int idType = StructureType.BasicScanner; idType <= StructureType.BasicGroundDefense; idType++) {
      if (getTrekExeID(idType) == oldID)
        setTrekExeID(idType, newID);
    }

    for (int idType = StructureType.MartialLaw; idType <= StructureType.Shipyard; idType++) {
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        int id = getTrekExeID(idType, emp);
        if (id == oldID)
          setTrekExeID(idType, emp, newID);
      }
    }

    if (stbof != null) {
      RaceRst races = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);

      for (int idType = StructureType.MinorShipBuild; idType <= StructureType.MinorShipBuild; idType++) {
        for (int race = NUM_EMPIRES; race < races.getNumberOfEntries(); race++) {
          int id = getTrekExeID(idType, race);
          if (id == oldID)
            setTrekExeID(idType, race, newID);
        }
      }
    }
  }

  private void switchTrekIDs(int oldID, int newID) throws IOException, InvalidArgumentsException {
    for (int idType = StructureType.BasicScanner; idType <= StructureType.BasicGroundDefense; idType++) {
      int id = getTrekExeID(idType);

      if (id == oldID)
        setTrekExeID(idType, newID);
      else if (id == newID)
        setTrekExeID(idType, oldID);
    }

    for (int idType = StructureType.MartialLaw; idType <= StructureType.Shipyard; idType++) {
      for (int emp = 0; emp < NUM_EMPIRES; emp++) {
        int id = getTrekExeID(idType, emp);

        if (id == oldID)
          setTrekExeID(idType, emp, newID);
        else if (id == newID)
          setTrekExeID(idType, emp, oldID);
      }
    }

    if (stbof != null) {
      RaceRst races = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);

      for (int idType = StructureType.MinorShipBuild; idType <= StructureType.MinorShipBuild; idType++) {
        for (int race = NUM_EMPIRES; race < races.getNumberOfEntries(); race++) {
          int id = getTrekExeID(idType, race);

          if (id == oldID)
            setTrekExeID(idType, race, newID);
          else if (id == newID)
            setTrekExeID(idType, race, oldID);
        }
      }
    }
  }

  private InternalSegment getSegment(int idType, int race) throws IOException {
    if (trek == null)
      throw new RuntimeException("No trek.exe open.");
    return trek.getSegment(getSegmentDefinition(idType, race), true);
  }

  private SegmentDefinition getSegmentDefinition(int idType, int race) {
    switch (idType) {
      case StructureType.BasicScanner:
        return SD_BldId_Scanner.Basic;
      case StructureType.OrbitalBattery:
        return SD_BldId_Orb.IntVal[0];
      case StructureType.BasicDilithiumSource:
        return SD_BldId_Dilithium.All[1];
      case StructureType.BasicShieldGenerator:
        return SD_BldId_Shield.All[0];
      case StructureType.TradeGoods:
        return SD_BldId_TradeGoods.All[0];
      case StructureType.BasicShipyard:
        return SD_BldId_Shipyard.Basic2[0];
      case StructureType.BasicGroundDefense:
        return SD_BldId_GrndDef.All[0];
      case StructureType.MartialLaw:
        return SD_BldId_MartialLaw.ML1[race];
      case StructureType.Shipyard:
        return SD_BldId_Shipyard.Regular;
      case StructureType.MinorShipBuild:
        return SD_BldId_MinorSp.BldList;
      default:
        throw new RuntimeException("Invalid id type.");
    }
  }

  // #endregion private helpers

  // #region TrekIds

  class TrekIds {

    int basicGroundDefense;
    int orbitalBattery;
    int basicDilithiumSource;
    int basicShieldGenerator;
    int tradeGoods;
    int basicShipyard;
    int[] martialLaw = new int[NUM_EMPIRES];
    int[] shipyard = new int[NUM_EMPIRES];

    public void load() throws IOException {
      RaceRst race = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);

      int basicScanner = getTrekExeID(StructureType.BasicScanner);
      if (basicScanner < 0)
        throw new IOException("Missing basic scanner available to all races.");

      basicGroundDefense = getTrekExeID(StructureType.BasicGroundDefense);
      if (basicGroundDefense < 0)
        throw new IOException("Missing basic ground defense building available to all races.");

      orbitalBattery = getTrekExeID(StructureType.OrbitalBattery);
      if (orbitalBattery < 0 || orbitalBattery > Byte.MAX_VALUE)
        throw new IOException("Missing valid orbital battery available to all races.");

      basicDilithiumSource = getTrekExeID(StructureType.BasicDilithiumSource);
      if (basicDilithiumSource < 0)
        throw new IOException("Missing valid basic dilithium source available to all races.");

      basicShieldGenerator = getTrekExeID(StructureType.BasicShieldGenerator);
      if (basicShieldGenerator < 0)
        throw new IOException("Missing basic shield generator available to all races.");

      tradeGoods = getTrekExeID(StructureType.TradeGoods);
      if (tradeGoods < 0)
        throw new IOException("Missing trade goods available to all races.");

      basicShipyard = getTrekExeID(StructureType.BasicShipyard);
      if (basicShipyard < 0)
        throw new IOException("Missing basic shipyard available to all races.");

      for (int emp = 0; emp < NUM_EMPIRES; ++emp) {
        martialLaw[emp] = getTrekExeID(StructureType.MartialLaw, emp);
        if (martialLaw[emp] < 0)
          throw new IOException("Missing martial law building for the %1".replace("%1", race.getName(emp)));

        shipyard[emp] = getTrekExeID(StructureType.Shipyard, emp);
      }
    }
  }

  // #endregion TrekIds

}
