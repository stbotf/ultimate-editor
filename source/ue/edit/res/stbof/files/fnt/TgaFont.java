package ue.edit.res.stbof.files.fnt;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.util.data.DataTools;
import ue.util.img.ImageConversion;
import ue.util.stream.StreamTools;

/**
 * Use for loading and manipulating fnt files.
 *
 * @author Alan Podlesek
 */
public class TgaFont extends InternalFile {

  public static final int NUMBERS_COLUMN = 0;
  public static final int UPPER_CASE_COLUMN = 1;
  public static final int LOWER_CASE_COLUMN = 2;
  public static final int SYMBOL_COLUMN = 3;
  public static final int SPECIAL_COLUMN = 4;

  private static final int[] SYMBOL_CONV = new int[]{
      96, 45, 61, 92, 91, 93, 59, 39, 44, 46, 47, 126, 33, 64, 35, 36, 37, 94,
      38, 42, 40, 41, 95, 43, 124, 123, 125, 58, 34, 60, 62, 63
  };

  private static final int[] SPECIAL_CONV = new int[]{
      0xC3A4, 0xC3A2, 0xC3A0, 0xC3AA, 0xC3A8, 0xC3A9, 0xC3AF, 0xC3AE, 0xC3B6,
      0xC3B4, 0xC3BC, 0xC3B1, 0xC387, 0xC3A7, 0xC384, 0xC396, 0xC39C, 0xC39F,
      0xC2AE, 0xC2A9, 0xE284A2, 0xE280A6, 0xE28093, 0xC2BF, 0xC2A1, 0xC2B7
  };

  // HEADER
  private byte row_height;                            //  1
  private byte[] UNKNOWN_1 = new byte[5];             //  6
  private short number_of_chars;                      //  8
  private short img_width;                            // 10
  private short img_height;                           // 12
  private int imageDataSize;                          // 16
  private byte[] UNKNOWN_2 = new byte[4];             // 20
  private short[] number_char_width = new short[10];  // 40
  private short[] upper_char_width = new short[26];   // 92
  private short[] lower_char_width = new short[26];   //144
  private short[] simbol_char_width = new short[32];  //208
  private short[] special_char_width = new short[26]; //260

  // img
  private TargaImage image;

  public TgaFont() {
  }

  public TgaFont(String name, InputStream in) throws IOException {
    this.NAME = name;
    load(in);
  }

  public TgaFont(String name, byte[] data) throws IOException {
    this(name, new ByteArrayInputStream(data));
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#check()
   */
  @Override
  public void check(Vector<String> response) {
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#getImageSize()
   */
  @Override
  public int getSize() {
    return 260 + (image != null ? image.getSize() : 0);
  }

  public Image getChar(char c) {
    if (Character.isDigit(c)) {
      int val = c - '0' - 1;

      if (val < 0) {
        val += 10;
      }

      return getCharImage(NUMBERS_COLUMN, val);
    } else if (Character.isLetter(c) && c < 255) {
      if (Character.isLowerCase(c)) {
        return getCharImage(LOWER_CASE_COLUMN, c - 'a');
      } else {
        return getCharImage(UPPER_CASE_COLUMN, c - 'A');
      }
    } else if (c == ' ') {
      BufferedImage img = new BufferedImage(UNKNOWN_1[1], row_height - 1,
          BufferedImage.TYPE_INT_RGB);
      Graphics g = img.getGraphics();
      g.setColor(Color.BLACK);
      g.fillRect(0, 0, img.getWidth(), img.getHeight());
      return img;
    } else {
      for (int i = 0; i < SYMBOL_CONV.length; i++) {
        if (SYMBOL_CONV[i] == c) {
          return getCharImage(TgaFont.SYMBOL_COLUMN, i);
        }
      }

      for (int i = 0; i < SPECIAL_CONV.length; i++) {
        if (SPECIAL_CONV[i] == c) {
          return getCharImage(TgaFont.SPECIAL_COLUMN, i);
        }
      }

      return null;
    }
  }

  /**
   * Use this to get character image
   *
   * @param column character column (0-4)
   * @param index  character index
   * @return character image
   */
  public Image getCharImage(int column, int index) {
    short[] val;

    switch (column) {
      case 0: {
        val = number_char_width;
        break;
      }
      case 1: {
        val = upper_char_width;
        break;
      }
      case 2: {
        val = lower_char_width;
        break;
      }
      case 3: {
        val = simbol_char_width;
        break;
      }
      default: {
        val = special_char_width;
        break;
      }
    }

    int x = 0;
    for (int i = 0; i < index; i++) {
      x += val[i];
    }

    int y = row_height * column + 1;
    int i = y * image.getImageSize().width + x;

    int w = val[index];
    int h = row_height - 1;

    // skip green pixels
    int greenPixel = image.getRawPixel(1);
    while (image.getRawPixel(i) == greenPixel && ++i < image.getPixelCount()) {
      x++;
      w--;
    }
    x++;

    // create dummy image when nothing to show
    if (w <= 0 || h <= 0)
      return Toolkit.getDefaultToolkit().createImage(new byte[0]);

    Image img = image.getImage(AlphaMode.Opaque);

    img = Toolkit.getDefaultToolkit().createImage(
        new FilteredImageSource(img.getSource(), new CropImageFilter(x, y, w, h)));

    int greenPixel32 = ImageConversion.convert_16to32bit(greenPixel);

    return Toolkit.getDefaultToolkit().createImage(
      new FilteredImageSource(img.getSource(), new RGBImageFilter() {
        @Override
        public int filterRGB(int x, int y, int rgb) {
          // convert green frames to black
          if (greenPixel32 == rgb)
            return Color.BLACK.getRGB();

          return rgb;
        }
      }));
  }

  public int getFontSize() {
    return row_height;
  }

  public int getNumberOfEntries(int column) {
    switch (column) {
      case 0: {
        return number_char_width.length;
      }
      case 1: {
        return upper_char_width.length;
      }
      case 2: {
        return lower_char_width.length;
      }
      case 3: {
        return simbol_char_width.length;
      }
      default: {
        return special_char_width.length;
      }
    }
  }

  @Override
  public void load(InputStream in) throws IOException {

    row_height = (byte)in.read();
    StreamTools.read(in, UNKNOWN_1);
    number_of_chars = StreamTools.readShort(in, true);
    img_width = StreamTools.readShort(in, true);
    img_height = StreamTools.readShort(in, true);
    imageDataSize = StreamTools.readInt(in, true);
    StreamTools.read(in, UNKNOWN_2);

    for (int i = 0; i < number_char_width.length; i++) {
      number_char_width[i] = StreamTools.readShort(in, true);
    }

    for (int i = 0; i < upper_char_width.length; i++) {
      upper_char_width[i] = StreamTools.readShort(in, true);
    }

    for (int i = 0; i < lower_char_width.length; i++) {
      lower_char_width[i] = StreamTools.readShort(in, true);
    }

    for (int i = 0; i < simbol_char_width.length; i++) {
      simbol_char_width[i] = StreamTools.readShort(in, true);
    }

    for (int i = 0; i < special_char_width.length; i++) {
      special_char_width[i] = StreamTools.readShort(in, true);
    }

    image = new TargaImage(NAME, in);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(row_height);
    out.write(UNKNOWN_1);
    out.write(DataTools.toByte(number_of_chars, true));
    img_width = (short) image.getImageSize().width;
    img_height = (short) image.getImageSize().height;
    out.write(DataTools.toByte(img_width, true));
    out.write(DataTools.toByte(img_height, true));
    imageDataSize = image.getDataSize();
    out.write(DataTools.toByte(imageDataSize, true));
    out.write(UNKNOWN_2);

    for (int i = 0; i < number_char_width.length; i++) {
      out.write(DataTools.toByte(number_char_width[i], true));
    }

    for (int i = 0; i < upper_char_width.length; i++) {
      out.write(DataTools.toByte(upper_char_width[i], true));
    }

    for (int i = 0; i < lower_char_width.length; i++) {
      out.write(DataTools.toByte(lower_char_width[i], true));
    }

    for (int i = 0; i < simbol_char_width.length; i++) {
      out.write(DataTools.toByte(simbol_char_width[i], true));
    }

    for (int i = 0; i < special_char_width.length; i++) {
      out.write(DataTools.toByte(special_char_width[i], true));
    }

    image.save(out);
  }

  @Override
  public void clear() {
    row_height = 0;
    Arrays.fill(UNKNOWN_1, (byte)0);
    number_of_chars = 0;
    img_width = 0;
    img_height = 0;
    imageDataSize = 0;
    Arrays.fill(UNKNOWN_2, (byte)0);
    Arrays.fill(number_char_width, (short)0);
    Arrays.fill(upper_char_width, (short)0);
    Arrays.fill(lower_char_width, (short)0);
    Arrays.fill(simbol_char_width, (short)0);
    Arrays.fill(special_char_width, (short)0);
    image = null;
    markChanged();
  }
}
