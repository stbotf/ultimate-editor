package ue.edit.res.stbof.files.rst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;
import lombok.val;
import ue.common.CommonStrings;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.WDFImgReq;
import ue.edit.res.stbof.files.desc.DescDataFile;
import ue.edit.res.stbof.files.desc.Descriptable;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.file.FileStore;
import ue.util.stream.StreamTools;

/**
 * Represents race.rst
 */
public class RaceRst extends DescDataFile {

  // #region data fields

  protected Stbof STBOF;
  private ArrayList<RaceEntry> RACES = new ArrayList<RaceEntry>();

  // #endregion data fields

  // #region constructor

  public RaceRst(Stbof st) throws IOException {
    super(CStbofFiles.RaceDescRst, st);
    STBOF = st;
  }

  // #endregion constructor

  // #region getters

  /**
   * Returns number of entries (races)
   */
  @Override
  public int getNumberOfEntries() {
    return RACES.size();
  }

  @Override
  protected Descriptable getEntry(int index) {
    testIndex(index);
    return RACES.get(index);
  }

  /**
   * Gets race name
   *
   * @param index The index of the race.
   * @return Race name
   */
  public String getName(int index) {
    if (checkIndex(index))
      return (RACES.get(index)).getName();
    else
      return "Race " + Integer.toString(index);
  }

  public String getName(int index, boolean stripEndingS) {
    String name = getName(index);
    if (stripEndingS && name.endsWith("s"))
      name = name.substring(0, name.length() - 1);
    return name;
  }

  public String getOwnerName(int index) {
    String name = getName(index);
    if (name.endsWith("s"))
      name = name.substring(0, name.length()-1);
    return name;
  }

  /**
   * Gets planet index
   *
   * @param index The index of the race.
   * @return home planet index
   */
  public short getPlanet(int index) {
    testIndex(index);
    return (RACES.get(index)).planet_index;
  }

  /**
   * Gets race picture
   *
   * @param index The index of the race.
   * @return Race picture
   */
  public String getPic(int index) {
    testIndex(index);
    return (RACES.get(index)).picture;
  }

  /**
   * Gets race base morale
   *
   * @param index   The index of the race.
   * @param forRace The major to check for.
   * @return Race base morale
   */
  public byte getBaseMorale(int index, int forRace) {
    testIndex(index);
    return (RACES.get(index)).base_morale[forRace];
  }

  /**
   * Gets race combat multiplier
   *
   * @param index The index of the race.
   * @return Race combat multiplier
   */
  public float getCombatMulti(int index) {
    testIndex(index);
    return (RACES.get(index)).combat_multiplier;
  }

  /**
   * @param index race index
   * @return starting population
   */
  public short getStartPop(int index) {
    testIndex(index);
    return (RACES.get(index)).starting_pop;
  }

  /**
   * @param index race index
   * @return true if the race system has dilithium
   */
  public boolean getDilithium(int index) {
    testIndex(index);

    if ((RACES.get(index)).dilithium == 1) {
      return true;
    }

    return false;
  }

  /**
   * @param index race index
   * @return unrest threshold
   */
  public int getMoraleThreshold(int index) {
    testIndex(index);
    return (RACES.get(index)).unrest_treshold;
  }

  /**
   * @param index race index
   * @return minor race grownt rate
   */
  public float getMinorRaceGrowthRate(int index) {
    testIndex(index);
    return (RACES.get(index)).minor_race_growth_rate;
  }

  /**
   * @param name race name
   * @return true if a race with the given name exists here
   */
  public boolean containsRace(String name) {
    return getIndex(name) >= 0;
  }

  /**
   * @param name race name
   * @return race index or -1
   */
  public int getIndex(String name) {
    for (int i = 0; i < RACES.size(); i++) {
      if (RACES.get(i).getName().equals(name)) {
        return i;
      }
    }
    return -1;
  }

  // #endregion getters

  // #region setters

  /**
   * Sets race name
   *
   * @param index The index of the race to change the name.
   * @param name  The new name to be assigned to the race.
   */
  public void setName(int index, String name) {
    testIndex(index);

    if (name != null) {
      RaceEntry r = RACES.get(index);
      r.setName(name);
    }
  }

  /**
   * Sets race picture
   *
   * @param index   The index of the race to change the picture.
   * @param picture The picture to be assigned to the race.
   */
  public void setPic(int index, String picture) {
    testIndex(index);

    if (picture != null) {
      RaceEntry r = RACES.get(index);

      if (!picture.equals(r.picture)) {
        r.picture = picture;
        this.markChanged();
      }
    }
  }

  /**
   * Sets race base morale
   *
   * @param index  The index of the race to change the base morale.
   * @param morale The base morale to be assigned to the race.
   */
  public void setBaseMorale(int index, int forRace, byte morale) {
    testIndex(index);

    if (morale > 21 && morale < 128 // TODO: lower boundary should be read from trek.exe
        && forRace >= 0 && forRace <= 5 // I don't really expect invalid values here
    ) {
      RaceEntry r = RACES.get(index);

      if (morale != r.base_morale[forRace]) {
        r.base_morale[forRace] = morale;
        this.markChanged();
      }
    }
  }

  /**
   * Sets race combat multiplier
   *
   * @param index The index of the race to change the combat multiplier.
   * @param multi The combat multiplier to be assigned to the race.
   */
  public void setCombatMulti(int index, float multi) {
    testIndex(index);

    RaceEntry r = RACES.get(index);

    if (multi != r.combat_multiplier) {
      r.combat_multiplier = multi;
      this.markChanged();
    }
  }

  /**
   * @param index race index
   * @param pop   starting population
   */
  public void setStartPop(int index, short pop) {
    testIndex(index);

    if (pop > 0) {
      RaceEntry r = RACES.get(index);

      if (pop != r.starting_pop) {
        r.starting_pop = pop;
        this.markChanged();
      }
    }
  }

  /**
   * @param index  race index
   * @param planet planet index
   */
  public void setPlanet(int index, short planet) {
    testIndex(index);

    if (planet >= 0) {
      RaceEntry r = RACES.get(index);

      if (planet != r.planet_index) {
        r.planet_index = planet;
        this.markChanged();
      }
    }
  }

  /**
   * @param index race index
   * @param d     true -> has dilithium
   */
  public void setDilithium(int index, boolean d) {
    testIndex(index);

    if (d != getDilithium(index)) {
      RaceEntry r = RACES.get(index);

      if (d) {
        r.dilithium = 1;
      } else {
        r.dilithium = 0;
      }
      this.markChanged();
    }
  }

  /**
   * @param index race index
   * @param thres the new unrest threshold
   */
  public void setMoraleThreshold(int index, short thres) {
    testIndex(index);

    RaceEntry r = RACES.get(index);

    if (thres != r.unrest_treshold) {
      r.unrest_treshold = thres;
      this.markChanged();
    }
  }

  /**
   * @param index race index
   * @param rate  growth rate
   */
  public void setMinorRaceGrowthRate(int index, float rate) {
    testIndex(index);

    RaceEntry r = RACES.get(index);

    if (rate != r.minor_race_growth_rate) {
      r.minor_race_growth_rate = rate;
      this.markChanged();
    }
  }

  // #endregion setters

  // #region add & remove

  /**
   * Adds a new race at the end of the list, the settings are copied from the race specified.
   * WARNING: if not set the race description address of the new race equals the one from the
   * original race!
   *
   * @param index the index of the race from which to copy settings
   * @return returns the index of the new race
   */
  public int add(int index) {
    testIndex(index);
    RaceEntry re = new RaceEntry(RACES.get(index));
    RACES.add(re);
    descChanged();
    markChanged();
    return RACES.size() - 1;
  }

  /**
   * Removes a race WARNING: don't forget to delete the description as well or it will remain
   *
   * @param index the index of the race to remove
   */
  public void remove(int index) {
    testIndex(index);
    RACES.remove(index);
    descChanged();
    markChanged();
  }

  // #endregion add & remove

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    RACES.clear();

    // number of races
    short num = StreamTools.readShort(in, true);

    // read races
    for (short i = 0; i < num; ++i) {
      RACES.add(new RaceEntry(this, in));
    }

    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    //number of entries
    short ent = (short) RACES.size();
    out.write(DataTools.toByte(ent, true));

    for (RaceEntry r : RACES) {
      r.save(out);
    }
  }

  // #endregion save

  // #region maintenance

  /**
   * Returns file size
   */
  @Override
  public int getSize() {
    return (RACES.size() * 108 + 2);
  }

  @Override
  public void clear() {
    RACES.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region checks

  @Override
  public void check(Vector<String> response) {
    super.check(response);
    for (RaceEntry ra : RACES)
      ra.check(response);

    // now check images
    checkImages(response);
  }

  private void checkImages(Vector<String> response) {
    try (FileStore fs = stbof.access()) {
      val dplReq = WDFImgReq.getRaceDplReq(stbof, response);
      if (!dplReq.isPresent())
        return;

      HashSet<String> alreadyChecked = new HashSet<>();
      for (RaceEntry ra : RACES) {
        if (alreadyChecked.add(ra.picture))
          WDFImgReq.checkStbofImageReq(fs, stbof, this, ra.picture, dplReq.get(), response);
      }
    }
    catch (Exception e) {
      String err = "Failed to check " + NAME + " images.";
      response.add(err);
    }
  }

  // #endregion checks

  // #region private helpers

  private boolean checkIndex(int index) {
    return index >= 0 && index < RACES.size();
  }

  /**
   * Throws exception on invalid index
   *
   * @param index
   */
  private void testIndex(int index) {
    if (!checkIndex(index)) {
      throw new IndexOutOfBoundsException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(index)));
    }
  }

  // #endregion private helpers

  // #region RaceEntry

  /**
   * Represents an entry in race.rst
   */
  private class RaceEntry extends Descriptable {

    public String picture;
    // public int super.descAddress;
    public short planet_index;
    public short starting_pop;

    //base morale value on < impossible (cars, feds, fers, klis, roms)
    public byte[] base_morale;
    public short dilithium;
    public int unrest_treshold;
    public float combat_multiplier;
    public float minor_race_growth_rate = 0;

    public byte[] unknown_2;
    public byte[] unknown_3;

    //constructor
    // each entry consists of 108 bytes
    public RaceEntry(DescDataFile file, InputStream in) throws IOException {
      super(file);

      //race name: 0-40 bytes
      name = StreamTools.readNullTerminatedBotfString(in, 40);

      //race pic: 40-20 bytes
      picture = StreamTools.readNullTerminatedBotfString(in, 20);

      //race description address: 60-4 bytes
      loadDescAddress(in);

      //planet index (for planet.pst): 64-2 bytes
      planet_index = StreamTools.readShort(in, true);

      //starting home population: 66-2 bytes //used only for minor races?
      starting_pop = StreamTools.readShort(in, true);

      //growth rate: 68-4 bytes
      minor_race_growth_rate = StreamTools.readFloat(in, true);

      //normal base morale values: 72-6 bytes
      base_morale = StreamTools.readBytes(in, 6);

      //dilithium: 78-2
      dilithium = StreamTools.readShort(in, true);

      //unknown: 80-4 bytes
      unknown_2 = StreamTools.readBytes(in, 4);
      //System.out.println(race_name + ": " + Tools.toHex(unknown_2));

      //morale treshold: 84-4
      unrest_treshold = StreamTools.readInt(in, true);

      //unknown: 88-16 bytes
      unknown_3 = StreamTools.readBytes(in, 16);
      //System.out.println(race_name + ": " + Tools.toHex(unknown_3));

      //ground combat multiplier: 104-4
      combat_multiplier = StreamTools.readFloat(in, true);
    }

    //copy constructor
    public RaceEntry(RaceEntry source) {
      super(source);
      picture = source.picture;
      planet_index = source.planet_index;
      starting_pop = source.starting_pop;
      System.arraycopy(source.base_morale, 0, base_morale, 0, base_morale.length);
      dilithium = source.dilithium;
      combat_multiplier = source.combat_multiplier;
      minor_race_growth_rate = source.minor_race_growth_rate;
      unknown_3 = source.unknown_3;
      unknown_2 = source.unknown_2;
      unrest_treshold = source.unrest_treshold;
    }

    @Override
    public String toString() {
      return name;
    }

    //saves
    @Override
    public void save(OutputStream out) throws IOException {
      //race name (40)
      byte[] writeBuffer = DataTools.toByte(name, 40, 39);
      out.write(writeBuffer);

      //race picture (20)
      writeBuffer = DataTools.toByte(picture, 20, 19);
      out.write(writeBuffer);

      //race description address (4)
      writeBuffer = DataTools.toByte(descAddress, true);
      out.write(writeBuffer);

      //planet index (2)
      writeBuffer = DataTools.toByte(planet_index, true);
      out.write(writeBuffer);

      //starting population (2)
      writeBuffer = DataTools.toByte(starting_pop, true);
      out.write(writeBuffer);

      //unknown (4)
      out.write(DataTools.toByte(minor_race_growth_rate, true));

      //base morales (6)
      out.write(base_morale);

      //dilithium (2)
      writeBuffer = DataTools.toByte(dilithium, true);
      out.write(writeBuffer);

      //unknown (4)
      out.write(unknown_2);

      //threshold morale (4)
      out.write(DataTools.toByte(unrest_treshold, true));

      //unknown (16)
      out.write(unknown_3);

      //ground combat multiplier (4)
      writeBuffer = DataTools.toByte(combat_multiplier, true);
      out.write(writeBuffer);
    }

    @Override
    public void check(Vector<String> response) {
      super.check(response);

      val planetPst = STBOF.files().findPlanetPst();

      if (name.length() > 39) {
        String msg = Language.getString("RaceRst.1"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        response.add(msg);
      }
      if (picture.length() > 19) {
        String msg = Language.getString("RaceRst.2"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", picture); //$NON-NLS-1$
        response.add(msg);
      }
      if (planet_index < 0) {
        String msg = Language.getString("RaceRst.8"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(planet_index)); //$NON-NLS-1$
        response.add(msg);
      }
      //planet
      if (planetPst.isPresent()) {
        if (planetPst.get().getIndex(planet_index) < 0) {
          String msg = Language.getString("RaceRst.9"); //$NON-NLS-1$
          msg = msg.replace("%1", getName()); //$NON-NLS-1$
          msg = msg.replace("%2", name); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(planet_index)); //$NON-NLS-1$
          response.add(msg);
        }
      }

      if (starting_pop < 0) {
        String msg = Language.getString("RaceRst.11"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(starting_pop)); //$NON-NLS-1$
        response.add(msg);
      }

      if (dilithium != 0 && dilithium != 1) {
        String msg = Language.getString("RaceRst.12"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        response.add(msg);
      }
      for (int j = 0; j < 5; j++) {
        if (base_morale[j] < 21) {
          String msg = Language.getString("RaceRst.13"); //$NON-NLS-1$
          msg = msg.replace("%1", getName()); //$NON-NLS-1$
          msg = msg.replace("%2", name); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(j)); //$NON-NLS-1$
          msg = msg.replace("%4", Integer.toString(base_morale[j])); //$NON-NLS-1$
          response.add(msg);
        }
      }
      if (combat_multiplier < 0) {
        String msg = Language.getString("RaceRst.14"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Double.toString(combat_multiplier)); //$NON-NLS-1$
        response.add(msg);
      }
      if (unrest_treshold < 0 || unrest_treshold > 195) {
        String msg = Language.getString("RaceRst.15"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", name); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(unrest_treshold)); //$NON-NLS-1$
        response.add(msg);
      }
    }
  }

  // #endregion RaceEntry

}
