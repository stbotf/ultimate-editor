package ue.edit.res.stbof.files.ani;

import java.awt.Dimension;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import ue.UE;
import ue.UE.DebugFlags;
import ue.edit.common.GenericFile;
import ue.edit.res.stbof.files.tga.TargaHeader;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.file.FileFilters;

/**
 * This class handles botf's ani/cur files.
 */
public class AniOrCur extends GenericFile {

  private boolean CURSOR;       // is this a cursor? (.cur)
  @SuppressWarnings("unused")
  private short CURSOR_LOC_Y;   // cursor location within the frame
  @SuppressWarnings("unused")
  private short CURSOR_LOC_X;   // -||-
  private short FRAME_W;        // the width of a single frame
  private short FRAME_H;        // the height of a single frame
  private short FRAMES;         // the number of frames
  private short DELAY;          // the delay between switching frames (only for .cur)
  private short TGA_W;          // full image width
  private short TGA_H;          // full image height
  private int SIZE;             // size of the file

  /**
   * Main constructor.
   */
  public AniOrCur() {
  }

  public AniOrCur(String fileName) {
    super(fileName);
  }

  /**
   * Main constructor.
   * @param name the name of the file being loaded
   */
  public AniOrCur(InputStream in, String name) throws IOException {
    super(name);
    load(in);
  }

  public void load(InputStream in) throws IOException {
    // read all data
    data = new byte[in.available()];
    int len, i = 0;
    while ((len = in.read(data, i, in.available())) > 0) {
      i += len;
    }

    // read botf file header
    byte[] b = new byte[2];
    b[0] = data[0];
    b[1] = data[1];
    FRAME_H = DataTools.toShort(b, true);
    b[0] = data[2];
    b[1] = data[3];
    FRAME_W = DataTools.toShort(b, true);
    b[0] = data[4];
    b[1] = data[5];
    FRAMES = DataTools.toShort(b, true);
    b[0] = data[6];
    b[1] = data[7];
    DELAY = DataTools.toShort(b, true);
    b[0] = data[8];
    b[1] = data[9];
    TGA_W = DataTools.toShort(b, true);
    b[0] = data[10];
    b[1] = data[11];
    TGA_H = DataTools.toShort(b, true);
    b = new byte[4];
    b[0] = data[12];
    b[1] = data[13];
    b[2] = data[14];
    b[3] = data[15];
    SIZE = DataTools.toInt(b, true);

    if (UE.DEBUG_MODE && UE.DEBUG_FLAGS.contains(DebugFlags.AniOrCur)) {
      System.out.println("Loaded ani/cur: " + NAME); //$NON-NLS-1$
      System.out.println("FRAME_H = " + FRAME_H); //$NON-NLS-1$
      System.out.println("FRAME_W = " + FRAME_W); //$NON-NLS-1$
      System.out.println("FRAMES  = " + FRAMES); //$NON-NLS-1$
      System.out.println("DELAY   = " + DELAY); //$NON-NLS-1$
      System.out.println("TGA_W   = " + TGA_W); //$NON-NLS-1$
      System.out.println("TGA_H   = " + TGA_H); //$NON-NLS-1$
      System.out.println("SIZE    = " + SIZE); //$NON-NLS-1$
    }

    // make sure things are right
    if (FileFilters.Cur.accept(null, NAME)) {
      // fix frame width in case it's broken
      FRAME_W = TGA_W;
      FRAME_H = (short) (TGA_H / FRAMES);
      // cursors come as a long vertical strip of frames
      // animations can have more than one frame per horisontal "line"
      CURSOR_LOC_X = (short) (FRAME_W / 2);
      CURSOR_LOC_Y = (short) (FRAME_H / 2);

      CURSOR = true;
    } else {
      // no cursor for the wicked
      CURSOR_LOC_X = 0;
      CURSOR_LOC_Y = 0;
      // the delay parameter has no effect on actual speed switching frames
      // I'd take a guess the actual delay is stored in the relevant *.wdf files
      // this value is close to the actual values and is used when
      // the animation or cursor is displayed in UE
      DELAY = 150;
      CURSOR = false;
    }
  }

  public AniOrCur(byte[] data, String name) throws IOException {
    this(new ByteArrayInputStream(data), name);
  }

  /**
   * Returns the number of frames.
   *
   * @return number of frames
   */
  public short getFrameNum() {
    return FRAMES;
  }

  /**
   * Returns if this is a cursor or not. If not then it's an animation and the delay parameter has
   * no effect on actual speed at which the frames move.
   *
   * @return true if this is a cursor
   */
  public boolean isCursor() {
    return CURSOR;
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= FRAMES)
      throw new IndexOutOfBoundsException(Language.getString("AniOrCur.0")); //$NON-NLS-1$
  }

  /**
   * Returns the requested frame as a TargaImage object.
   *
   * @param index the index of the frame, numbered from left to right, top down
   * @return the frame as a TargaImage object
   * @throws IOException
   */
  public TargaImage getFrame(int index) throws IOException {
    checkIndex(index);

    // A tga file needs to be made for the frame
    // Tga format facts:
    // - each pixel is 2 bytes
    //   therefore the size of the frame will be
    //   CURSOR_LOC_X*CURSOR_LOC_Y*2 + header
    // - header is 18 bytes
    int frameSize = FRAME_H * FRAME_W * Short.BYTES;
    byte[] frame = new byte[frameSize + TargaHeader.FIXED_SIZE];
    // Copy tga header from main file,
    // but change image size
    for (int i = 0; i < TargaHeader.FIXED_SIZE; i++) {
      frame[i] = data[i + 16];
    }

    byte[] b = DataTools.toByte(FRAME_W, true);
    frame[12] = b[0];
    frame[13] = b[1];
    b = DataTools.toByte(FRAME_H, true);
    frame[14] = b[0];
    frame[15] = b[1];

    //exclude bottom lines

    // lines of frames (in case it not a vertical strip of frames lines != FRAMES)
    // drop bottom extra bytes
    int lines = (int) Math.floor(((double) TGA_H) / FRAME_H);
    // number of frames in a single line
    int inline = (int) Math.ceil(((double) FRAMES) / lines);

    // calculate the number of frame lines we need to skip
    int left = lines - (index / inline) - 1;

    // skip the data of lines to skip
    int lineSize = FRAME_H * TGA_W * Short.BYTES;

    // data byte index
    int d_index = left * lineSize;
    // frame byte index
    int f_index = 0;

    // make the index point at a frame in the lines
    // instead of whole line
    // let's say the image has 5 frames per frame line
    // you want the frame index 7, which is the index 7-5 = 2 in the frame line
    // this is because the data in the file would be placed like this:
    // [6] [7] [8] [9] [A]
    // [1] [2] [3] [4] [5]
    // we skip the first line of frames and only care for the index
    // on the line we currently are
    index %= inline;

    // Get the image data
    for (int z = 0; z < FRAME_H; z++) {
      //exclude lines of bytes for frames that are left from this frame
      d_index += index * FRAME_W * Short.BYTES;
      //include bytes of this frame
      for (int t = 0; t < FRAME_W; t++) {
        frame[f_index + TargaHeader.FIXED_SIZE] = data[d_index + 34];
        frame[f_index + TargaHeader.FIXED_SIZE + 1] = data[d_index + 35];
        d_index += Short.BYTES;
        f_index += Short.BYTES;
      }
      //exclude lines  of bytes for frames that follow this frame
      d_index += (inline - (index + 1)) * FRAME_W * Short.BYTES;
    }

    return new TargaImage(NAME + "_" + index, frame);
  }

  /**
   * Returns an array of all targa image frames this animation contains.
   *
   * @return an array of targa image frames
   * @throws IOException if the frames can't be loaded
   */
  public TargaImage[] getAllFrames() throws IOException {
    TargaImage[] img = new TargaImage[FRAMES];

    for (int i = 0; i < FRAMES; i++) {
      img[i] = getFrame(i);
    }

    return img;
  }

  /**
   * Returns the size in pixels of a single frame.
   *
   * @return the size of a single frame.
   */
  public Dimension getFrameDim() {
    return new Dimension(FRAME_W, FRAME_H);
  }

  /**
   * Returns the number of frames this animation contains.
   *
   * @return the number of frames
   */
  public short getNumFrames() {
    return FRAMES;
  }

  /**
   * Returns the size of the tga file in pixels
   *
   * @return size in pixels
   */
  public Dimension getTgaDim() {
    return new Dimension(TGA_W, TGA_H);
  }

  /**
   * Returns the size of the tga file in bytes.
   *
   * @return tga size
   */
  public int getTgaSize() {
    return SIZE;
  }

  public int getFirstPixelRawColor() {
    // lines of frames (in case it not a vertical strip of frames lines != FRAMES)
    int lines = (int) Math.floor(((double) TGA_H) / FRAME_H);

    // skip the data of lines to skip
    int lineSize = FRAME_H * TGA_W * Short.BYTES;

    int d_first = (lines - 1) * lineSize;
    return DataTools.toShort(data, d_first + 34, true);
  }

  /**
   * Sets the delay between frames. works only for cursors.
   *
   * @param delay the delay
   * @return true if the delay has changed
   */
  public boolean setDelay(short delay) {
    if (DELAY != delay) {
      DELAY = delay;

      byte[] b = DataTools.toByte(delay, true);
      data[6] = b[0];
      data[7] = b[1];

      markChanged();
      return true;
    }

    return false;
  }

  /**
   * Returns the delay between frames.
   *
   * @return the delay
   */
  public short getDelay() {
    return DELAY;
  }

}
