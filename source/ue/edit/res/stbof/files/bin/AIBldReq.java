package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.StructureBonus;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.data.AIBldReqEntry;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.exception.KeyNotFoundException;
import ue.service.Language;
import ue.util.data.CollectionTools;

/**
 * Allows manipulation of the aibldreq.bin file.
 */
public class AIBldReq extends InternalFile {

  private static final String strUError = "AIBldReq.2"; //$NON-NLS-1$
  private static final String strInvalidID = "AIBldReq.3"; //$NON-NLS-1$
  private static final String strNEn = "AIBldReq.4"; //$NON-NLS-1$
  private static final String strNPop = "AIBldReq.5"; //$NON-NLS-1$
  private static final String strNMaxPop = "AIBldReq.6"; //$NON-NLS-1$
  private static final String strEnReq = "AIBldReq.7"; //$NON-NLS-1$
  private static final String strInvID = "AIBldReq.8"; //$NON-NLS-1$

  private Stbof stbof;
  private ArrayList<AIBldReqEntry> list = new ArrayList<>();
  private Map<Integer, AIBldReqEntry> reqMap = new HashMap<>();

  public AIBldReq(Stbof stbof) {
    this.stbof = stbof;
  }

  public List<AIBldReqEntry> listReqs() {
    return Collections.unmodifiableList(list);
  }

  /**
   * @param id building id
   * @return true if the building is listed in this file
   */
  public boolean contains(int id) {
    return reqMap.containsKey(id);
  }

  public void switchIDs(int id1, int id2) {
    AIBldReqEntry req1 = reqMap.remove(id1);
    AIBldReqEntry req2 = reqMap.remove(id2);
    if (req1 != null) {
      req1.setId(id2);
      reqMap.put(id2, req1);
    }
    if (req2 != null) {
      req2.setId(id1);
      reqMap.put(id1, req2);
    }
  }

  /**
   * @param id id of the building
   * @return AI building requirement entry
   */
  public Optional<AIBldReqEntry> getReq(int id) {
    return Optional.ofNullable(reqMap.get(id));
  }

  /**
   * Moves the building to a higher position in the list.
   *
   * @param id building id
   * @throws KeyNotFoundException if building id is not in the list
   */
  public void moveUp(int id) throws KeyNotFoundException {
    int i = getIndex(id);

    if (i > 1) {
      AIBldReqEntry br = list.remove(i);
      list.add(i - 1, br);
      markChanged();
    }
  }

  /**
   * Moves the building to a lower position in the list.
   *
   * @param id building id
   * @throws KeyNotFoundException if building id is not in the list
   */
  public void moveDown(int id) throws KeyNotFoundException {
    int i = getIndex(id);

    if (i < list.size() - 1) {
      AIBldReqEntry br = list.remove(i);
      list.add(i + 1, br);
      markChanged();
    }
  }

  /**
   * Adds building to the list unless it's already on it
   *
   * @param id building id
   * @throws IOException
   * @returns the added AI building requirement entry
   */
  public AIBldReqEntry add(int id) throws IOException {
    AIBldReqEntry entry = reqMap.get(id);
    if (entry != null)
      return entry;

    Edifice ed = (Edifice) stbof.getInternalFile(CStbofFiles.EdificeBst, true);
    Building bld = ed.building(id);
    int en = bld.getEnergyCost();

    entry = new AIBldReqEntry(this, id);
    entry.setEnergyReq(en);

    list.add(entry);
    reqMap.put(id, entry);
    markChanged();

    return entry;
  }

  /**
   * Removes the building from the list if it's there
   *
   * @param id building id
   */
  public void remove(int id) {
    if (reqMap.remove(id) != null) {
      list.removeIf(x -> x.id() == id);
      markChanged();
    }
  }

  /**
   * Removes requirements for the given build index and updates all building indices on the removed
   * building.
   *
   * @param id removed building id
   */
  public void removedBuidingIndex(int id) {
    // update list and the building ids
    boolean removed = list.removeIf(x -> {
      if (x.id() > id) {
        x.setId(x.id() - 1);
        return false;
      }
      return x.id() == id;
    });

    reqMap = list.stream().collect(Collectors.toMap(AIBldReqEntry::id, Function.identity()));

    if (removed)
      markChanged();
  }

  /**
   * @return array containing ids present in this file
   */
  public int[] getIDList() {
    return list.stream().mapToInt(AIBldReqEntry::id).toArray();
  }

  private int findIndex(int id) {
    return CollectionTools.findIndex(list, x -> x.id() == id);
  }

  private int getIndex(int id) throws KeyNotFoundException {
    int i = findIndex(id);
    if (i < 0) {
      String err = Language.getString(strInvID)
        .replace("%1", getName()) //$NON-NLS-1$
        .replace("%2", Integer.toString(id)); //$NON-NLS-1$
      throw new KeyNotFoundException(err);
    }

    return i;
  }

  @Override
  public void check(Vector<String> response) {
    try {
      Edifice edifice = (Edifice) stbof.getInternalFile(CStbofFiles.EdificeBst, true);

      for (int i = 0; i < list.size(); i++) {
        AIBldReqEntry br = list.get(i);

        if ((br.id() >= edifice.getNumberOfEntries()) || (br.id() < 0)) {
          String msg = Language.getString(strInvalidID)
              .replace("%1", Integer.toString(br.id())); //$NON-NLS-1$
          msg += " (removed)";
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

          list.remove(i);
          i--;
        } else {
          Building bld = edifice.building(br.id());
          if (br.getEnergyReq() != bld.getEnergyCost()) {
            String msg = Language.getString(AIBldReq.strEnReq);
            msg = msg.replace("%1", Integer.toString(br.id())); //$NON-NLS-1$
            msg = msg.replace("%2", Integer.toString(br.getEnergyReq())); //$NON-NLS-1$
            msg = msg.replace("%3", Integer.toString(bld.getEnergyCost())); //$NON-NLS-1$
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));
          }

          if (bld.isMainBuilding()) {
            String msg = "%1 is a main building (now removed from list).";
            msg = msg.replace("%1", bld.getName());
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

            list.remove(i);
            i--;
            continue;
          }

          // Check bonus type, but accept BuildsShips for shipyards to help AI expansion.
          if (bld.getBonusType() == StructureBonus.MartialLaw) {
            String msg = "%1 is martial law type task (now removed from list).";
            msg = msg.replace("%1", bld.getName());
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

            list.remove(i);
            i--;
            continue;
          }
        }
      }
    } catch (Exception e) {
      response.add(Language.getString(AIBldReq.strUError).replace("%1", getName())
          .replace("%2", e.getLocalizedMessage())); //$NON-NLS-1$ //$NON-NLS-2$
      e.printStackTrace();
    }

    for (int i = 0; i < list.size(); i++) {
      AIBldReqEntry br = list.get(i);

      if (br.getEnergyReq() < 0) {
        String msg = Language.getString(AIBldReq.strNEn);
        msg = msg.replace("%1", Integer.toString(br.id())); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      if (br.getMaxPopReq() < 0) {
        String msg = Language.getString(AIBldReq.strNPop);
        msg = msg.replace("%1", Integer.toString(br.id())); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
      if (br.getPopReq() < 0) {
        String msg = Language.getString(AIBldReq.strNMaxPop);
        msg = msg.replace("%1", Integer.toString(br.id())); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    }
  }

  @Override
  public int getSize() {
    return list.size() * 24;
  }

  /**
   * @param in stream to read from
   * @throws IOException
   */
  @Override
  public void load(InputStream in) throws IOException {
    list.clear();
    reqMap.clear();

    while (in.available() >= 24) {
      AIBldReqEntry bldReq = new AIBldReqEntry(this, in);
      list.add(bldReq);
      reqMap.put(bldReq.id(), bldReq);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < list.size(); i++)
      list.get(i).save(out);
  }

  @Override
  public void clear() {
    list.clear();
    reqMap.clear();
    markChanged();
  }

}
