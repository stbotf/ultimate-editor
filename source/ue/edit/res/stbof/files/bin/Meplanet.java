package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Optional;
import java.util.Vector;
import lombok.Data;
import lombok.val;
import lombok.var;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.pst.Planet;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

// Meplanet defines the home system planets for the five major empires and by which evolution level they are colonized.
public class Meplanet extends InternalFile {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  @Data
  private class RacePlanets {
    private int raceId;
    private String raceName;
    private ArrayList<MeplanetEntry> planets = new ArrayList<>();

    public RacePlanets(int raceId, String raceName) {
      this.raceId = raceId;
      this.raceName = raceName;
    }
  }

  // map entries by name to properly handle race lookup
  // the id might not be known and pure list search is unnecessary cumbersome
  private HashMap<String, RacePlanets> racePlanets = new HashMap<>();
  // keep a second list that reflects the loaded race sorting
  private ArrayList<RacePlanets> loadSorted = new ArrayList<>();
  // also keep a raceId counter for new entries added
  int nextRaceId = 0;

  private Stbof stbof;
  private Optional<RaceRst> raceRst;

  public Meplanet(Stbof stbof) {
    this.stbof = stbof;
    this.raceRst = stbof.files().findRaceRst();
  }

  @Override
  public void check(Vector<String> response) {
    try {
      if (racePlanets.size() != 5) {
        String msg = Language.getString("Meplanet.0"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(racePlanets.size())); //$NON-NLS-1$
        response.add(msg);
      }

      Planet pst = (Planet) stbof.getInternalFile(CStbofFiles.PlanetPst, true);
      String[] pstnam = pst.getNames();

      for (RacePlanets rp : loadSorted) {
        int size = rp.planets.size();

        for (int i = 0; i < size; i++) {
          try {
            MeplanetEntry me = rp.planets.get(i);

            if (me.PLANET.length() > 39) {
              String msg = Language.getString("Meplanet.1"); //$NON-NLS-1$
              msg = msg.replace("%1", getName()); //$NON-NLS-1$
              msg = msg.replace("%2", me.PLANET); //$NON-NLS-1$
              response.add(msg);
            }
            if (me.RACE.length() > 39) {
              String msg = Language.getString("Meplanet.2"); //$NON-NLS-1$
              msg = msg.replace("%1", getName()); //$NON-NLS-1$
              msg = msg.replace("%2", me.RACE); //$NON-NLS-1$
              response.add(msg);
            }
            if (me.COUNT != size) {
              me.COUNT = size; // auto-fix
            }
            if (me.LEVEL < 0) {
              String msg = Language.getString("Meplanet.3"); //$NON-NLS-1$
              msg = msg.replace("%1", getName()); //$NON-NLS-1$
              msg = msg.replace("%2", Integer.toString(me.LEVEL)); //$NON-NLS-1$
              msg = msg.replace("%3", me.PLANET); //$NON-NLS-1$
              response.add(msg);
            }

            if (stbof != null) {
              //check if planetes are in planet.pst
              boolean knownPlanet = Arrays.stream(pstnam).anyMatch(x -> x.equals(me.PLANET));
              if (!knownPlanet) {
                String msg = Language.getString("Meplanet.4"); //$NON-NLS-1$
                msg = msg.replace("%1", getName()); //$NON-NLS-1$
                msg = msg.replace("%2", me.PLANET); //$NON-NLS-1$
                response.add(msg);
              }

              //check if races are in race.pst
              if (raceRst.isPresent()) {
                boolean knownRace = false;
                for (int h = 0; h < NUM_EMPIRES; h++) {
                  if ((raceRst.get().getName(h)).equals(me.RACE)) {
                    knownRace = true;
                    break;
                  }
                }

                if (!knownRace) {
                  String msg = Language.getString("Meplanet.5"); //$NON-NLS-1$
                  msg = msg.replace("%1", getName()); //$NON-NLS-1$
                  msg = msg.replace("%2", me.RACE); //$NON-NLS-1$
                  response.add(msg);
                }
              }
            }
          } catch (Exception z2) {
            z2.printStackTrace();
            String msg = Language.getString("Meplanet.6"); //$NON-NLS-1$
            msg = msg.replace("%1", getName()); //$NON-NLS-1$
            msg = msg.replace("%2", z2.getMessage()); //$NON-NLS-1$
            response.add(msg);
          }
        }
      }
    } catch (Exception z) {
      z.printStackTrace();
      String msg = Language.getString("Meplanet.6"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", z.getMessage()); //$NON-NLS-1$
      response.add(msg);
    }
  }

  @Override
  public int getSize() {
    int size = 0;
    for (RacePlanets rp : loadSorted) {
      if (rp != null)
        size += rp.planets.size() * 88;
    }
    return size;
  }

  @Override
  public void load(InputStream in) throws IOException {
    racePlanets.clear();
    loadSorted.clear();
    nextRaceId = 0;

    while (in.available() > 0) {
      MeplanetEntry me = new MeplanetEntry(in);
      val prev = racePlanets.get(me.RACE);

      if (prev != null) {
        // append to existing race list
        prev.getPlanets().add(me);
      } else {
        // for new entries, try to lookup the race id
        int raceId = -1;

        // the entries actually should be sorted by race id
        // UE in the past however messed them up so check for the race name
        if (raceRst.isPresent()) {
          raceId = raceRst.get().getIndex(me.RACE);
          nextRaceId = Integer.max(nextRaceId, raceId + 1);
        } else {
          raceId = nextRaceId++;
        }

        val rmap = new RacePlanets(raceId, me.RACE);
        rmap.planets.add(me);
        racePlanets.put(me.RACE, rmap);

        // remember the load order of new race entries
        loadSorted.add(rmap);
      }
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    // Sort the planet listings by race id, like it is done in the original meplanet.bin file.
    // However don't mess with current sorting to not invalidate the GUI indexes.
    val list = new ArrayList<>(loadSorted);
    list.sort(Comparator.comparingInt(RacePlanets::getRaceId));

    for (var rp : list) {
      if (rp == null)
        continue;

      int size = rp.planets.size();
      for (MeplanetEntry me : rp.planets) {
        // overwrite the planet count
        me.COUNT = size;
        me.save(out);
      }
    }
  }

  @Override
  public void clear() {
    racePlanets.clear();
    loadSorted.clear();
    nextRaceId = 0;
    markChanged();
  }

  /**
   * Returns a list of planets for the given load sorted entry index.
   * Note that the vanilla raceId order got messed with previous versions of UE.
   *
   * @param entryIdx the race entry index, according to the load order
   * @return an array of planet names for the race or null if there are none
   */
  public String[] getPlanets(int entryIdx) {
    RacePlanets rp = loadSorted.get(entryIdx);
    return rp.planets.stream().map(x -> x.PLANET).toArray(String[]::new);
  }

  /**
   * Returns all owners of listed planets
   */
  public String[] getOwners() {
    return loadSorted.stream().map(x -> x.raceName).toArray(String[]::new);
  }

  /**
   * Sets the owner of a group of planets
   *
   * @param entryIdx  the race entry index, according to the load order
   * @param name      the new owner name
   */
  public boolean setOwner(int entryIdx, String name) throws InvalidArgumentsException {
    RacePlanets rp = loadSorted.get(entryIdx);
    if (rp == null) {
      String msg = Language.getString("Meplanet.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (StringTools.equals(rp.raceName, name))
      return false;

    var existing = racePlanets.get(name);
    if (existing != null) {
      // move planets over to existing new owner
      existing.planets.addAll(rp.planets);
      racePlanets.remove(rp.raceName);
      loadSorted.remove(entryIdx);
    } else {
      // just rename the owner
      racePlanets.remove(rp.raceName);
      rp.raceName = name;
      racePlanets.put(name, rp);
    }

    // update the planet names
    for (MeplanetEntry me : rp.planets) {
      me.RACE = name;
    }

    markChanged();
    return true;
  }

  /**
   * Sets the planet name
   *
   * @param entryIdx    the race entry index, according to the load order
   * @param planetIdx   the planet index
   * @param planetName  the new planet name
   */
  public boolean setPlanet(int entryIdx, int planetIdx, String planetName) throws InvalidArgumentsException {
    val entry = loadSorted.get(entryIdx);
    if (entry == null) {
      String msg = Language.getString("Meplanet.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    MeplanetEntry me = entry.planets.get(planetIdx);
    if (me == null) {
      String msg = Language.getString("Meplanet.8"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(planetIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (!me.PLANET.equals(planetName)) {
      me.PLANET = planetName;
      markChanged();
      return true;
    }

    return false;
  }

  /**
   * Adds owner to list.
   *
   * @param owner the new owner to be added
   */
  public int addOwner(String owner) throws InvalidArgumentsException {
    val rmap = new RacePlanets(nextRaceId, owner);

    // check whether the race already exists
    val prev = racePlanets.putIfAbsent(owner, rmap);
    if (prev != null)
      return prev.raceId;

    loadSorted.add(rmap);
    nextRaceId++;
    markChanged();

    return rmap.raceId;
  }

  /**
   * Adds planet to list.
   *
   * @param entryIdx  the race entry index, according to the load order
   * @param planet    the planet name
   */
  public boolean addPlanet(int entryIdx, String planet) throws InvalidArgumentsException {
    val entry = loadSorted.get(entryIdx);
    if (entry == null) {
      String msg = Language.getString("Meplanet.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    MeplanetEntry me = new MeplanetEntry(entry.raceName, planet);
    entry.planets.add(me);
    markChanged();

    return true;
  }

  /**
   * Removes an owner from the list.
   *
   * @param entryIdx the race entry index, according to the load order
   */
  public boolean removeOwner(int entryIdx) {
    val removed = loadSorted.remove(entryIdx);
    if (removed != null) {
      racePlanets.remove(removed.getRaceName());
      markChanged();
      return true;
    }
    return false;
  }

  /**
   * Removes a planet of an owner from the list.
   *
   * @param entryIdx  the race entry index, according to the load order
   * @param planetIdx the index of the planet to be removed
   */
  public boolean removePlanet(int entryIdx, int planetIdx) throws InvalidArgumentsException {
    val entry = loadSorted.get(entryIdx);
    if (entry == null) {
      String msg = Language.getString("Meplanet.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    entry.planets.remove(planetIdx);
    markChanged();
    return true;
  }

  /**
   * Sets the starting evolution level at which the planet will be colonized.
   *
   * @param entryIdx  the race entry index, according to the load order
   * @param planetIdx the index of the planet
   * @param level the starting evolution level, 10 for non-colonizable gas giants
   */
  public boolean setColonized(int entryIdx, int planetIdx, int level) throws InvalidArgumentsException {
    val entry = loadSorted.get(entryIdx);
    if (entry == null) {
      String msg = Language.getString("Meplanet.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    MeplanetEntry me = entry.planets.get(planetIdx);
    if (me == null) {
      String msg = Language.getString("Meplanet.8"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(planetIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (me.LEVEL != level) {
      me.LEVEL = level;
      markChanged();
      return true;
    }

    return false;
  }

  /**
   * Gets the starting evolution level at which the planet will be colonized.
   *
   * @param entryIdx  the race entry index, according to the load order
   * @param planetIdx the index of the planet
   */
  public int getColonized(int entryIdx, int planetIdx) throws InvalidArgumentsException {
    val entry = loadSorted.get(entryIdx);
    if (entry == null) {
      String msg = Language.getString("Meplanet.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    MeplanetEntry me = entry.planets.get(planetIdx);
    if (me == null) {
      String msg = Language.getString("Meplanet.8"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(entryIdx)); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(planetIdx)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    return me.LEVEL;
  }

  public void renamePlanets(String oldName, String newName) {
    for (var entry : loadSorted) {
      for (MeplanetEntry e : entry.planets) {
        if (StringTools.equals(e.PLANET, oldName)) {
          e.PLANET = newName;
          return;
        }
      }
    }
  }

  private class MeplanetEntry {

    public String PLANET;
    public String RACE;
    public int LEVEL;
    public int COUNT;

    //constructors
    public MeplanetEntry(InputStream in) throws IOException {
      //planet
      PLANET = StreamTools.readNullTerminatedBotfString(in, 40);
      //race
      RACE = StreamTools.readNullTerminatedBotfString(in, 40);
      //colonize level
      LEVEL = StreamTools.readInt(in, true);
      //counter
      COUNT = StreamTools.readInt(in, true);
    }

    public MeplanetEntry(String owner, String name) {
      PLANET = name;
      RACE = owner;
      LEVEL = 0;
      COUNT = 0;
    }

    //save
    public boolean save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(PLANET, 40, 39));
      out.write(DataTools.toByte(RACE, 40, 39));
      //colonize level
      out.write(DataTools.toByte(LEVEL, true));
      //counter
      out.write(DataTools.toByte(COUNT, true));
      return true;
    }
  }
}
