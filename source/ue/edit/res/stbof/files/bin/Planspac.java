package ue.edit.res.stbof.files.bin;

/**
 * This class is a representation of planspac.bin, which contains maximum population values for
 * different planet types and sizes. If a planet doesn't have an entry here, it means the planet is
 * not inhabitable.
 */
public class Planspac extends PlanMap {

  /**
   * Sets a population value.
   *
   * @param type            the type of the planet
   * @param size            the size (0-small, 1-medium, 2-large)
   * @param growthBonusLvl  food or max population bonus (0, 1 or 2)
   * @param energyBonusLvl  energy bonus (0, 1 or 2)
   * @param value           the max population to be set for this planet
   */
  public boolean set(int type, int size, int growthBonusLvl, int energyBonusLvl, int value) {
    return super.set(type, size, growthBonusLvl, energyBonusLvl, value);
  }

  /**
   * Maps planet bonuses to actual population bonus amounts.
   *
   * Even though only one resulting bonus can be seen in-game,
   * each planet actually has three bonus levels (food + energy + growth),
   * that effect each other. @see Planboni.get
   *
   * For some reason the max population bonus indeed is based on the energy
   * and the growth bonus, and disregards the food bonus.
   *
   * @param type            the type of the planet
   * @param size            the size (0-small, 1-medium, 2-large)
   * @param growthBonusLvl  food or max population bonus (0, 1 or 2)
   * @param energyBonusLvl  energy bonus (0, 1 or 2)
   * @return the population bonus, defaults to 0 if missing
   */
  public int get(int type, int size, int growthBonusLvl, int energyBonusLvl) {
    return super.get(type, size, growthBonusLvl, energyBonusLvl);
  }
}
