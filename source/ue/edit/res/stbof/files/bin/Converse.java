package ue.edit.res.stbof.files.bin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Converse.bin lists voice groups with descriptions for all the english.snd or german.snd voices.
 * Each of those groups may either directly group multiple sounds,
 * or it may group multiple lists of several sounds.
 *
 * @author Alan Podlesek
 */
public class Converse extends InternalFile {

  public class VoiceInfo {
    public int index;
    public int race;
    public int groupIndex;
    public int groupType;
    public int listIndex = -1;
    public int listType = -1;
  }

  private int voice_groups;
  private int map_address;
  private int voice_lists;
  private short unknown1; // always 1

  // followed by VoiceLists, VoiceGroups, VoiceGroup map entries, VoiceList map entries
  private ArrayList<VoiceList> lists = new ArrayList<VoiceList>();
  private ArrayList<VoiceGroup> groups = new ArrayList<VoiceGroup>();

  public Converse() {
  }

  @Override
  public String toString() {
    String str = "";
    for (int i = 0; i < groups.size(); i++) {
      str = str + "Group " + i + ":\n" + groups.get(i).toString() + "\n";
    }
    return str;
  }


  /* (non-Javadoc)
   * @see ue.edit.InternalFile#check()
   */
  @Override
  public void check(Vector<String> response) {

    for (VoiceGroup conf : groups)
      response.addAll(conf.check());

    // check voice groups for referenced voice list duplicates
    HashSet<Integer> set = new HashSet<Integer>();
    for (int gid = 0; gid < groups.size(); ++gid) {
      VoiceGroup group = groups.get(gid);
      if (group.type == VoiceGroup.TYPE_VOICE_LIST_INDEX) {
        for (val voice : group.voices) {
          int ilist = voice.index - VoiceGroup.vlist_offset;

          if (ilist < lists.size() && ilist >= 0) {
            if (!set.add((int)voice.index)) {
              String msg = Language.getString("Converse.8") //$NON-NLS-1$
                .replace("%1", Short.toString(voice.index)) //$NON-NLS-1$
                .replace("%2", Integer.toString(gid)); //$NON-NLS-1$
              msg = getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg);
              response.add(msg);
            }
          }
        }
      }
    }

    // check voice groups for unreferenced voice list
    if (set.size() != lists.size()) {
      ArrayList<Integer> missing = new ArrayList<Integer>();
      for (int i = 0; i < lists.size(); ++i) {
        if (!set.remove(i))
          missing.add(i);
      }

      String msg = Language.getString("Converse.9"); //$NON-NLS-1$
      msg += missing.stream().map(x -> Integer.toString(x)).collect(Collectors.joining(", "));
      msg = getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg);
      response.add(msg);
    }
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#getSize()
   */
  @Override
  public int getSize() {
    int size = 14;

    for (VoiceGroup conf : groups) {
      size += 16 + conf.getSize();
    }

    for (VoiceList seg : lists) {
      size += 16 + seg.getSize();
    }

    return size;
  }

  @Override
  public void load(InputStream in) throws IOException {
    groups.clear();
    lists.clear();

    voice_groups = StreamTools.readInt(in, true);
    map_address = StreamTools.readInt(in, true);
    voice_lists = StreamTools.readInt(in, true);

    unknown1 = StreamTools.readShort(in, true);
    if (unknown1 != 1)
      System.out.println("Error: Invalid Converse flag: " + unknown1);

    byte[] data = StreamTools.readBytes(in, map_address - 14);
    ByteArrayInputStream bais = new ByteArrayInputStream(data);

    // read VoiceGroups
    for (int i = 0; i < voice_groups; i++) {
      // read map entry
      int type = StreamTools.readInt(in, true);   // 0 or 1
      int empty = StreamTools.readInt(in, true);  // always 0
      if (empty != 0)
        System.out.println("Error: Invalid VoiceGroup entry special: " + empty);
      int size = StreamTools.readInt(in, true);
      int address = StreamTools.readInt(in, true);

      // read data
      address -= 14; // subtract file header
      bais.reset();
      StreamTools.skip(bais, address);
      groups.add(new VoiceGroup(bais, size, type));
    }

    // read VoiceLists
    for (int i = 0; i < voice_lists; i++) {
      // read map entry
      int address = StreamTools.readInt(in, true);
      int num = StreamTools.readInt(in, true);
      int type = StreamTools.readInt(in, true);   // 0, 1 or 4
      int empty = StreamTools.readInt(in, true);  // always 0
      if (empty != 0)
        System.out.println("Error: Invalid VoiceList entry special: " + empty);

      // read data
      address -= 14; // subtract file header
      bais.reset();
      StreamTools.skip(bais, address);
      lists.add(new VoiceList(bais, num, type));
    }

    bais.close();
    markSaved();
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#save(java.io.OutputStream)
   */
  @Override
  public void save(OutputStream out) throws IOException {
    voice_groups = groups.size();

    out.write(DataTools.toByte(voice_groups, true));

    map_address = 14;

    for (VoiceGroup conf : groups) {
      map_address += conf.getSize();
    }

    for (VoiceList seg : lists) {
      map_address += seg.getSize();
    }

    out.write(DataTools.toByte(map_address, true));

    voice_lists = lists.size();

    out.write(DataTools.toByte(voice_lists, true));
    out.write(DataTools.toByte(unknown1, true));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    int address = 14;
    byte[] skip = new byte[4];

    // write VoiceLists
    for (VoiceList seg : lists) {
      seg.save(out);
      address += seg.getSize();
    }

    // write VoiceGroups
    for (int i = 0; i < groups.size(); ++i ) {
      VoiceGroup conf = groups.get(i);
      conf.save(out);

      // save map entry
      baos.write(DataTools.toByte(conf.type, true));
      baos.write(skip);

      // for the last VoiceGroup retain full size for the double end delimeter
      // this looks like it was meant for convention to signal the final sound termination
      // and matches the unpatched original game files
      int size = i + 1 == groups.size() ? conf.getSize() : conf.getSize() - 1;
      baos.write(DataTools.toByte(size, true));
      baos.write(DataTools.toByte(address, true));

      // adjust address
      address += conf.getSize();
    }

    // write voicelist map entries
    address = 14;

    for (VoiceList seg : lists) {
      // save map entry
      baos.write(DataTools.toByte(address, true));
      baos.write(DataTools.toByte(seg.entryAddr.length, true));
      baos.write(DataTools.toByte(seg.type, true));
      baos.write(skip);

      // adjust address
      address += seg.getSize();
    }

    // write map
    out.write(baos.toByteArray());
  }

  @Override
  public void clear() {
    voice_groups = 0;
    map_address = 0;
    voice_lists = 0;
    unknown1 = 1;
    groups.clear();
    lists.clear();
    markChanged();
  }

  public VoiceInfo getInfo(short snd_index) {
    VoiceInfo info = null;
    for (int gid = 0; gid < groups.size(); ++gid) {
      VoiceGroup group = groups.get(gid);
      info = group.getInfo(snd_index);
      if (info != null) {
        info.groupIndex = gid;
        break;
      }
    }
    return info;
  }

  /**
   * Get the sound's race index.
   *
   * @param snd_index index of the voice in english.snd
   * @return the race index or -1 if not found. 36 is alien, 37 is neutral.
   */
  public int getVoiceOwner(short snd_index) {
    int race = -1;
    for (VoiceGroup group : groups) {
      val entry = group.findEntry(snd_index);
      if (entry != null) {
        race = entry.race;
        break;
      }
    }

    return race;
  }

  /**
   * Set the sound's race index.
   *
   * @param snd_index index of the voice in english.snd
   * @param race      the race index to set. 36 is alien, 37 is neutral.
   * @throws KeyNotFoundException
   */
  public void setVoiceOwner(short snd_index, int race) throws KeyNotFoundException {
    for (VoiceGroup group : groups) {
      val entry = group.findEntry(snd_index);
      if (entry != null) {
        if (race != entry.race) {
          entry.race = race;
          markChanged();
        }
        return;
      }
    }

    // throw KeyNotFoundException for explicit exception handling
    String msg = Language.getString("Converse.7") //$NON-NLS-1$
      .replace("%1", Short.toString(snd_index)); //$NON-NLS-1$
    throw new KeyNotFoundException(msg);
  }

  /**
   * Will switch sound indexes of given races.
   *
   * @param race1
   * @param race2
   * @param both  if false will set race1 to use race2 voices
   * @throws InvalidArgumentsException
   * @throws KeyNotFoundException
   */
  public void switchRaces(int race1, int race2, boolean both) throws InvalidArgumentsException {
    if (race1 == race2)
      return;

    ArrayList<VoiceGroup.VoiceGroupEntry> race1todo = new ArrayList<VoiceGroup.VoiceGroupEntry>();
    ArrayList<VoiceGroup.VoiceGroupEntry> race2todo = new ArrayList<VoiceGroup.VoiceGroupEntry>();
    int direct[] = new int[2];  // race1 + race2 direct sound counters
    int list[] = new int[2];    // race1 + race2 voice list counters

    for (VoiceGroup grp : groups) {
      for (VoiceGroup.VoiceGroupEntry vg : grp.voices) {
        if (vg.race == race1) {
          race1todo.add(vg);

          if (grp.type == VoiceGroup.TYPE_DIRECT_SND_INDEX) {
            direct[0]++;
          } else if (grp.type == VoiceGroup.TYPE_VOICE_LIST_INDEX) {
            list[0]++;
          }
        } else if (vg.race == race2) {
          race2todo.add(vg);

          if (grp.type == VoiceGroup.TYPE_DIRECT_SND_INDEX) {
            direct[1]++;
          } else if (grp.type == VoiceGroup.TYPE_VOICE_LIST_INDEX) {
            list[1]++;
          }
        }
      }
    }

    if (race1todo.size() != race2todo.size()) {
      String msg = Language.getString("Converse.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(race1todo.size())) //$NON-NLS-1$
        .replace("%2", Integer.toString(race2todo.size())); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (direct[0] != direct[1] || list[0] != list[1]) {
      String msg = Language.getString("Converse.1") //$NON-NLS-1$
        .replace("%1", this.getName()); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    ArrayList<Short> notFound = new ArrayList<Short>();
    while (race1todo.size() > 0) {
      val a = race1todo.remove(0);
      boolean lowOffset = a.index < VoiceGroup.vlist_offset;

      boolean found = false;
      for (int i = 0; i < race2todo.size(); ++i) {
        val b = race2todo.get(i);
        if (b.index < VoiceGroup.vlist_offset == lowOffset) {
          race2todo.remove(i);

          // switch sound indexes
          short tmp = a.index;
          a.index = b.index;
          if (both)
            b.index = tmp;

          found = true;
          break;
        }
      }

      if (!found)
        notFound.add(a.index);
    }

    if (!notFound.isEmpty()) {
      // throw KeyNotFoundException for explicit exception handling
      String notFoundMsg = Language.getString("Converse.6"); //$NON-NLS-1$
      String msg = notFound.stream().map(x -> notFoundMsg.replace("%1", x.toString()))
        .collect(Collectors.joining("\n"));
      throw new KeyNotFoundException(msg);
    }

    markChanged();
  }

  private class VoiceGroup {

    private static final int TYPE_DIRECT_SND_INDEX = 0; // sound_index is index in .snd
    private static final int TYPE_VOICE_LIST_INDEX = 1; // sound_index is VoiceList index + 15000
    private static final int vlist_offset = 15000;

    private int type;

    private static final int entry_delimiter = 254; // FE
    private static final int end_of_group = 252;    // FC
    private static final int entry_size = 11;

    private VoiceGroupEntry[] voices;

    // for unmodded english.snd size is 56 = 5 entries,
    // except 5 groups that have a size of 12 = 1 entry
    public VoiceGroup(InputStream in, int size, int type) throws IOException {
      this.type = type;

      int num = (size - 1 /*end_of_group delimiter*/) / entry_size;

      voices = new VoiceGroupEntry[num];

      int delimiter = in.read();
      if (delimiter != entry_delimiter)
        System.out.println("Error: Invalid VoiceGroup entry delimiter: " + delimiter);

      for (int i = 0; i < num; i++) {
        voices[i] = new VoiceGroupEntry(in);  // 10n
        StreamTools.skip(in, 1);              // delimiter or end of group
      }

      int end = in.read(); // FC end_of_group // 11n + 2
      if (end != end_of_group)
        System.out.println("Error: Invalid VoiceGroup end delimiter: " + end);
    }

    public int getSize() {
      return voices.length * 11 + 2;
    }

    public void save(OutputStream out) throws IOException {

      for (int i = 0; i < voices.length; i++) {
        out.write(entry_delimiter);
        voices[i].save(out);        // 11n
      }

      // the end delimiter is written twice
      // it is no start delimiter of the next group,
      // since it both doesn't precede first group
      // plus it is also written after the last entry
      out.write(end_of_group);      // 11n + 1
      out.write(end_of_group);      // 11n + 2
    }

    /**
     * VoiceGroupEntry lookup.
     * @param snd_index index of the voice in english.snd
     * @return the VoiceGroupEntry if found.
     */
    private VoiceGroupEntry findEntry(short snd_index) {
      if (type == TYPE_DIRECT_SND_INDEX) {
        for (VoiceGroupEntry voice : voices) {
          if (voice.index == snd_index)
            return voice;
        }
      } else if (type == TYPE_VOICE_LIST_INDEX) {
        for (VoiceGroupEntry voice : voices) {
          int ilist = voice.index - vlist_offset;

          if (ilist < lists.size() && ilist >= 0) {
            VoiceList vlist = lists.get(ilist);

            for (short idx : vlist.snd_index) {
              if (idx == snd_index)
                return voice;
            }
          }
        }
      }

      return null;
    }

    private VoiceInfo getInfo(short snd_index) {
      if (type == TYPE_DIRECT_SND_INDEX) {
        for (VoiceGroupEntry voice : voices) {
          if (voice.index == snd_index) {
            VoiceInfo info = new VoiceInfo();
            info.index = voice.index;
            info.race = voice.race;
            info.groupType = type;
            return info;
          }
        }
      } else if (type == TYPE_VOICE_LIST_INDEX) {
        for (VoiceGroupEntry voice : voices) {
          int ilist = voice.index - vlist_offset;

          if (ilist < lists.size() && ilist >= 0) {
            VoiceList vlist = lists.get(ilist);

            for (short idx : vlist.snd_index) {
              if (idx == snd_index) {
                VoiceInfo info = new VoiceInfo();
                info.index = voice.index;
                info.race = voice.race;
                info.groupType = type;
                info.listIndex = ilist;
                info.listType = vlist.type;
                return info;
              }
            }
          }
        }
      }

      return null;
    }

    protected Vector<String> check() {
      Vector<String> response = new Vector<String>();

      if (type == TYPE_DIRECT_SND_INDEX) {
        for (VoiceGroupEntry vg : voices) {
          if (vg.index > 1363 || vg.index < 0) {
            String msg = Language.getString("Converse.2")
              .replace("%1", Short.toString(vg.index)); //$NON-NLS-1$ //$NON-NLS-2$
            msg = getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg);
            response.add(msg);
          }
        }
      } else if (type == TYPE_VOICE_LIST_INDEX) {

        for (VoiceGroupEntry vg : voices) {
          int ilist = vg.index - vlist_offset;

          if (ilist >= lists.size() || ilist < 0) {
            String msg = Language.getString("Converse.3")
              .replace("%1", Short.toString(vg.index)); //$NON-NLS-1$ //$NON-NLS-2$
            msg = getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg);
            response.add(msg);
          } else {
            VoiceList vlist = lists.get(ilist);

            for (short idx : vlist.snd_index) {
              if (idx > 1363 || idx < 0) {
                String msg = Language.getString("Converse.4")
                  .replace("%1", Short.toString(idx)); //$NON-NLS-1$ //$NON-NLS-2$
                msg = getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg);
                response.add(msg);
              }
            }
          }
        }
      } else {
        String msg = Language.getString("Converse.5")
          .replace("%1", Integer.toString(type)); //$NON-NLS-1$ //$NON-NLS-2$
        msg = getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg);
        response.add(msg);
      }

      return response;
    }

    @Override
    public String toString() {
      String str = "";

      for (int i = 0; i < voices.length; i++) {
        str = str + "Race " + voices[i].race + ": ";

        if (this.type == TYPE_DIRECT_SND_INDEX) {
          str = str + voices[i].index + "\n";
        } else {
          VoiceList lst = lists.get(voices[i].index - vlist_offset);

          for (int j = 0; j < lst.snd_index.length; j++) {
            if (j + 1 < lst.snd_index.length) {
              str = str + lst.snd_index[j] + ", ";
            } else {
              str = str + lst.snd_index[j] + "\n";
            }
          }
        }
      }

      return str;
    }

    private class VoiceGroupEntry {

      int race;         // race index 0-4, 36 for alien, 37 for neutral voice
      byte[] unknown;   // 25 01 00 30 00 5B 00, always the same
      short index;

      public VoiceGroupEntry(InputStream in) throws IOException {
        race = in.read();
        unknown = StreamTools.readBytes(in, 7);
        index = StreamTools.readShort(in, true);
      }

      public void save(OutputStream out) throws IOException {
        out.write(race);
        out.write(unknown);
        out.write(DataTools.toByte(index, true));
      }
    }
  }

  interface VoiceListTypes {
    // unspecified/misc, includes task force assignments,
    // alien encounters and game setup
    public static final int Unspecified = 0;
    // race specific voices like combat encounter and battle commands
    public static final int RaceSpecific = 1;
    // diplomatic introductions on minor race encounter
    // one large list per empire
    public static final int DiplomaticIntro = 4;
  }

  private class VoiceList {
    // VoiceListTypes: 0, 1 or 4
    int type;
    // addresses of the VoiceList entries below
    short entryAddr[];
    // VoiceList entries <unknown2, snd_index>
    short unknown2[];   // always 1
    short snd_index[];

    // for unmodded english.snd num is 4, 5 or 6, except 5 lists that have 14 entries
    public VoiceList(InputStream in, int num, int type) throws IOException {
      this.type = type;

      entryAddr = new short[num];
      unknown2 = new short[num];
      snd_index = new short[num];

      for (int i = 0; i < num; i++) {
        entryAddr[i] = StreamTools.readShort(in, true); // 2n
      }

      for (int i = 0; i < num; i++) {
        unknown2[i] = StreamTools.readShort(in, true);  // 4n
        if (unknown2[i] != 1)
          System.out.println("Error: Invalid VoiceList flags: " + unknown2[i]);

        snd_index[i] = StreamTools.readShort(in, true); // 6n
      }
    }

    public int getSize() {
      return 6 * entryAddr.length;
    }

    public void save(OutputStream out) throws IOException {
      for (int i = 0; i < entryAddr.length; i++) {
        out.write(DataTools.toByte(entryAddr[i], true));
      }

      for (int i = 0; i < unknown2.length; i++) {
        out.write(DataTools.toByte(unknown2[i], true));
        out.write(DataTools.toByte(snd_index[i], true));
      }
    }
  }
}
