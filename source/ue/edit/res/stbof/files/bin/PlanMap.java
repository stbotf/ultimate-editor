package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TreeMap;
import java.util.Vector;
import lombok.val;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.PlanetType;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.exception.CorruptedFile;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is a base class to the planboni.bin and planspac.bin file implementations.
 * It serves a two dimensional int[] map per planet type & size.
 */
public class PlanMap extends InternalFile {

  // pre-calculated planet type + size to value mappings
  // feature TreeMap to keep them sorted according to vanilla game files
  private TreeMap<Integer, int[]> PLAN = new TreeMap<>();

  //specified by StbofEdit
  @Override
  public void check(Vector<String> response) {
    for (int[] inta : PLAN.values()) {
      for (int i = 0; i < inta.length; i++) {
        if (inta[i] < 0) {
          String msg = Language.getString("PlanMap.0"); //$NON-NLS-1$
          msg = msg.replace("%1", Integer.toString(inta[i])); //$NON-NLS-1$
          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        }
      }
    }

    // check for missing default planet types & sizes
    for (int type = 0; type < CStbof.DEFAULT_PLANET_TYPES; type++) {
      // skip gas giants that by default have no entry
      if (type == PlanetType.GasGiant)
        continue;

      for (int size = 0; size < 3; size++) {
        int index = toIndex(type, size);
        if (!PLAN.containsKey(index)) {
          // add missing types & sizes
          add(type, size);

          String msg = Language.getString("PlanMap.1"); //$NON-NLS-1$
          msg = msg.replace("%1", LexHelper.mapPlanetType(type)); //$NON-NLS-1$
          msg = msg.replace("%2", LexHelper.mapPlanetSize(size)); //$NON-NLS-1$
          msg += " (fixed)";
          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        }
      }
    }
  }

  @Override
  public int getSize() {
    return PLAN.size() * 40;
  }

  @Override
  public void load(InputStream in) throws IOException {
    PLAN.clear();

    if (in.available() % 10 != 0) {
      String msg = Language.getString("PlanMap.2"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(in.available())); //$NON-NLS-1$
      throw new CorruptedFile(msg);
    }

    // read file
    while (in.available() > 0) {
      // read index as big endian to retain the sort order
      int index = StreamTools.readInt(in, false);

      // values
      int[] inta = new int[9];
      for (int i = 0; i < inta.length; i++)
        inta[i] = StreamTools.readInt(in, true);

      PLAN.put(index, inta);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    for (val entry : PLAN.entrySet()) {
      val key = entry.getKey();
      int[] inta = entry.getValue();

      // write key
      out.write(DataTools.toByte(key, false));

      // write values (int[9])
      for (int i = 0; i < inta.length; i++)
        out.write(DataTools.toByte(inta[i], true));
    }
  }

  @Override
  public void clear() {
    PLAN.clear();
    markChanged();
  }

  public int numEntries() {
    return PLAN.size();
  }

  /**
   * Returns an array of bonus or population values.
   *
   * @param type the type of the planet
   * @param size the size (0-small, 1-medium, 2-large)
   * @return the values or null if missing
   */
  public int[] getValues(int type, int size) {
    int index = toIndex(type, size);
    return PLAN.get(index);
  }

  /**
   * returns a list of planet type indexes found in this file
   */
  public int[] getPlanets() {
    int[] types = new int[PLAN.size()];
    int i = 0;

    for (int key : PLAN.keySet()) {
      types[++i] = key >>> 24;
    }
    return types;
  }

  /**
   * Adds a value array if missing for the key.
   * All values are set to 0.
   *
   * @param type the type of the planet
   * @param size the size (0-small, 1-medium, 2-large)
   * @return the added or existing value array
   */
  public int[] add(int type, int size) {
    int index = toIndex(type, size);
    int[] inta = new int[9];

    return PLAN.merge(index, inta, (x, y) -> {
      if (x == null) {
        markChanged();
        return y;
      } else {
        return x;
      }
    });
  }

  /**
   * Removes a value array.
   *
   * @param type the type of the planet
   * @param size the size (0-small, 1-medium, 2-large)
   */
  public void remove(int type, int size) {
    int index = toIndex(type, size);
    if (PLAN.remove(index) != null)
      markChanged();
  }

  /**
   * Returns true if this type of planet is registered.
   *
   * @param type the type of the planet
   * @param size the size (0-small, 1-medium, 2-large)
   */
  public boolean contains(int type, int size) {
    int index = toIndex(type, size);
    return PLAN.containsKey(index);
  }

  /**
   * Sets a planet value mapping.
   *
   * @param type  the type of the planet
   * @param size  the size (0-small, 1-medium, 2-large)
   * @param key1  first 2-dimensional integer map key (0, 1 or 2)
   * @param key2  second 2-dimensional integer map key (0, 1 or 2)
   * @param value the value to be set for this planet
   */
  public boolean set(int type, int size, int key1, int key2, int value) {
    // add type & size if missing
    int[] inta = add(type, size);

    int idx = key1 * 3 + key2;
    if (inta[idx] != value) {
      inta[idx] = value;
      markChanged();
      return true;
    }

    return false;
  }

  /**
   * Maps planet properties to actual data values.
   *
   * @param type  the type of the planet
   * @param size  the size (0-small, 1-medium, 2-large)
   * @param key1  first 2-dimensional integer map key (0, 1 or 2)
   * @param key2  second 2-dimensional integer map key (0, 1 or 2)
   * @return the mapped value, defaults to 0 if missing
   */
  public int get(int type, int size, int key1, int key2) {
    int index = toIndex(type, size);
    int[] inta = PLAN.get(index);

    // default planetary values, missing ones
    // should be checked by the integration checks
    if (inta == null)
      return 0;

    // get mapped value
    int idx = key1 * 3 + key2;
    return inta[idx];
  }

  private static int toIndex(int type, int size) {
    return (type << 24) | (size << 16);
  }
}
