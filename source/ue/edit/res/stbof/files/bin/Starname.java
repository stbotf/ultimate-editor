package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is the representation of the starname.bin file, it contains names of generic stars -
 * those that aren't home systems to any race.
 */
public class Starname extends InternalFile {

  private ArrayList<String> STARS = new ArrayList<String>();

  // #region constructor

  public Starname(Stbof stbof) {
  }

  // #endregion constructor

  // #region properties

  public int count() {
    return STARS.size();
  }

  /**
   * @return an array of all the star names in the file
   */
  public List<String> entries() {
    return Collections.unmodifiableList(STARS);
  }

  public String get(int index) {
    return STARS.get(index);
  }

  /**
   * Adds a name to the list.
   *
   * @param name the name to add
   * @return true if the name was added else false
   */
  public boolean add(String name) {
    if (STARS.contains(name))
      return false;
    if (name.length() > 19)
      return false;

    STARS.add(name);
    Collections.sort(STARS, String.CASE_INSENSITIVE_ORDER);
    markChanged();
    return true;
  }

  /**
   * Remove a name to the list.
   *
   * @param name the name to remove
   */
  public void remove(String name) {
    STARS.remove(name);
    Collections.sort(STARS, String.CASE_INSENSITIVE_ORDER);
    markChanged();
  }

  /**
   * Changes a name in the list.
   *
   * @param oldName the name to be changed
   * @param name    the new name
   * @return true if the name was changed else false
   */
  public boolean set(String oldName, String name) {
    if (name.length() > 19)
      return false;

    int index = STARS.indexOf(oldName);
    if (index == -1)
      return false;

    STARS.set(index, name);
    Collections.sort(STARS, String.CASE_INSENSITIVE_ORDER);
    markChanged();
    return true;
  }

  // #endregion properties

  // #region generic info

  /**
   * @return uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return 4 + STARS.size() * 24;
  }

  // #endregion generic info

  // #region helper routines

  @Override
  public void clear() {
    STARS.clear();
    markChanged();
  }

  // #endregion helper routines

  // #region load

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    STARS.clear();

    // read num and throw away
    int num = StreamTools.readInt(in, true);

    // read entries
    for (int i = 0; i < num; ++i) {
      // star name
      String strName = StreamTools.readNullTerminatedBotfString(in, 20);
      STARS.add(strName);
      // skip name length but depend on \0 termination
      in.skip(4);
    }

    Collections.sort(STARS, String.CASE_INSENSITIVE_ORDER);
    markSaved();
  }

  // #endregion load

  // #region save

  /**
   * Used to save changes.
   *
   * @param out the OutputStream to write the file to.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    // actualy ignores saved data and counts num of entries instead
    out.write(DataTools.toByte(STARS.size(), true));

    // entries
    for (int i = 0; i < STARS.size(); i++) {
      String name = STARS.get(i);
      // write name
      out.write(DataTools.toByte(name, 20, 19));
      // write name length + \0 termination
      out.write(DataTools.toByte(name.length() + 1, true));
    }
  }

  // #endregion save

  // #region check

  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < STARS.size(); i++) {
      String str = STARS.get(i);
      if (str.length() > 19) {
        String msg = Language.getString(Language.getString("Starname.0")); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", str); //$NON-NLS-1$
        response.add(msg);
      }
    }
  }

  // #endregion check

}
