package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.exception.CorruptedFile;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of morale.bin, contains values of morale changes for specific
 * events - those events are hard-coded.
 * <p>
 * This class was made thanks to a guide on Joker's site (www.members.aon.at/zelli/) which says the
 * tutorial is from SkyWolf. Thanks goes to both of them.
 **/
public class Morale extends InternalFile {

  /*NOTE: it's final for a reason...*/
  //private
  private static final int MAJORS = 5;
  private static final int EVENTS = 23;
  private static final int SIZE = 460;

  ///the values are integers but apparently they are read as shorts.
  private int VALUES[] = new int[115]; // 23*5=115

  public Morale(Stbof stbof) {
  }

  /**
   * Changes values.
   *
   * @param empire can be 0 to 4 (major race indexes).
   * @param event  can be 0 to 22 (event indexes).
   * @param value  the value to be assigned.
   */
  public void changeValue(int empire, int event, int value) throws InvalidArgumentsException {
    checkIndex(empire, event);

    /*NOTE: although the values are integers, they are read as short from botf, hence only short values are allowed.*/
    if (value > Short.MAX_VALUE || value < Short.MIN_VALUE) {
      String msg = Language.getString("Morale.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(empire)); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(event)); //$NON-NLS-1$
      msg = msg.replace("%3", Integer.toString(value)); //$NON-NLS-1$

      throw new InvalidArgumentsException(msg);
    }

    int i = event * 5 + empire;
    if (VALUES[i] == value) {
      return;
    }
    VALUES[i] = value;
    this.markChanged();
  }

  /**
   * Returns morale value.
   *
   * @param empire can be 0 to 4 (major race indexes).
   * @param event  can be 0 to 22 (event indexes).
   * @return morale value as int
   * @throws IndexOutOfBoundsException if the provided parameters aren't allowed.
   */
  public int getValue(int empire, int event) {
    checkIndex(empire, event);
    return VALUES[event * 5 + empire];
  }

  @Override
  public void load(InputStream in) throws IOException {
    int size = in.available();
    if (size != SIZE) {
      String msg = Language.getString("Morale.0"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(size)); //$NON-NLS-1$
      throw new CorruptedFile(msg);
    }

    // read 23x5 values: events X majors
    for (int i = 0; i < EVENTS; i++) {
      for (int j = 0; j < MAJORS; j++) {
        VALUES[i * MAJORS + j] = StreamTools.readInt(in, true);
      }
    }

    markSaved();
  }

  /**
   * @param out the OutputStream to write to
   * @return true if everything went ok
   */
  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < VALUES.length; i++) {
      out.write(DataTools.toByte(VALUES[i], true));
    }
  }

  @Override
  public void clear() {
    Arrays.fill(VALUES, 0);
    markChanged();
  }

  /**
   * @return the uncompressed size of the file
   **/
  @Override
  public int getSize() {
    return SIZE;
  }

  private void checkIndex(int empire, int event) {
    if (empire < 0 || event < 0 || empire >= MAJORS || event >= EVENTS) {
      String msg = Language.getString("Morale.2"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(empire)); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(event)); //$NON-NLS-1$

      throw new IndexOutOfBoundsException(msg);
    }
  }

  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < EVENTS; i++) {
      for (int j = 0; j < MAJORS; j++) {
        int z = i * 5 + j;
        if (VALUES[z] > Short.MAX_VALUE || VALUES[z] < Short.MIN_VALUE) {
          String msg = Language.getString("Morale.3"); //$NON-NLS-1$
          msg = msg.replace("%1", getName()); //$NON-NLS-1$
          msg = msg.replace("%2", Integer.toString(VALUES[z])); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(i)); //$NON-NLS-1$
          msg = msg.replace("%4", Integer.toString(j)); //$NON-NLS-1$

          response.add(msg);
        }
      }
    }
  }
}