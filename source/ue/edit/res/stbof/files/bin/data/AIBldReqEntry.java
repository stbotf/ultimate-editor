package ue.edit.res.stbof.files.bin.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.res.stbof.files.bin.AIBldReq;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class AIBldReqEntry {

  @Accessors(fluent = true)
  private int id;
  private int popReq = 0;     // required minimal existing population
  private int maxPopReq = 0;  // requires at least this maximum population
  private int energyReq = 0;  // energy requirement
  private byte[] unknown_1 = new byte[8];

  private final AIBldReq aiBldReq;

  public AIBldReqEntry(AIBldReq aiBldReq, int id) {
    this.aiBldReq = aiBldReq;
    this.id = id;
  }

  public AIBldReqEntry(AIBldReq aiBldReq, InputStream in) throws IOException {
    this.aiBldReq = aiBldReq;
    load(in);
  }

  public void setId(int id) {
    if (this.id != id) {
      this.id = id;
      aiBldReq.markChanged();
    }
  }

  /**
   * @param en energy requirement
   * @throws KeyNotFoundException if building id is not in the list
   */
  public void setEnergyReq(int en) {
    if (energyReq != en) {
      energyReq = en;
      aiBldReq.markChanged();
    }
  }

  /**
   * @param pop population requirement
   * @throws KeyNotFoundException if building id is not in the list
   */
  public void setPopReq(int pop) {
    if (popReq != pop) {
      popReq = pop;
      aiBldReq.markChanged();
    }
  }

  /**
   * @param pop population requirement
   * @throws KeyNotFoundException if building id is not in the list
   */
  public void setMaxPopReq(int pop) {
    if (maxPopReq != pop) {
      maxPopReq = pop;
      aiBldReq.markChanged();
    }
  }

  public void load(InputStream in) throws IOException {
    id = StreamTools.readInt(in, true);
    popReq = StreamTools.readInt(in, true);
    maxPopReq = StreamTools.readInt(in, true);
    energyReq = StreamTools.readInt(in, true);
    StreamTools.read(in, unknown_1);
  }

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(id, true));
    out.write(DataTools.toByte(popReq, true));
    out.write(DataTools.toByte(maxPopReq, true));
    out.write(DataTools.toByte(energyReq, true));
    out.write(unknown_1);
  }

}
