package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.CollectionTools;
import ue.util.stream.StreamTools;

/**
 * Is used for loading bldsetX.bin files, where X is a number between 1-5 Bldset sets the buildings
 * each empire starts with on their home world on any of the 5 eras.
 */
public class BldSet extends InternalFile {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private Stbof STBOF;

  // use sorted tree maps to restore vanilla building id order
  // this way the hex code also becomes more readable
  private TreeMap<Short, Integer> CBLD_SET = new TreeMap<Short, Integer>();
  private TreeMap<Short, Integer> HBLD_SET = new TreeMap<Short, Integer>();
  private TreeMap<Short, Integer> FBLD_SET = new TreeMap<Short, Integer>();
  private TreeMap<Short, Integer> KBLD_SET = new TreeMap<Short, Integer>();
  private TreeMap<Short, Integer> RBLD_SET = new TreeMap<Short, Integer>();

  public BldSet(Stbof stbof) {
    STBOF = stbof;
  }

  public static void checkIndex(int k) {
    if (k < 1 || k > CStbof.NUM_ERAS) {
      String msg = Language.getString("BldSet.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(k)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  @Override
  public void load(InputStream in) throws IOException {
    CBLD_SET.clear();
    HBLD_SET.clear();
    FBLD_SET.clear();
    KBLD_SET.clear();
    RBLD_SET.clear();

    while (in.available() > 0) {
      //race index
      short hm = StreamTools.readShort(in, true);
      //building index/id
      short id = StreamTools.readShort(in, true);
      //freq
      int num = StreamTools.readInt(in, true);

      //save info
      switch (hm) {
        case CStbof.Race.Card:
          CBLD_SET.put(id, num);
          break;
        case CStbof.Race.Fed:
          HBLD_SET.put(id, num);
          break;
        case CStbof.Race.Ferg:
          FBLD_SET.put(id, num);
          break;
        case CStbof.Race.Kling:
          KBLD_SET.put(id, num);
          break;
        default:
          // if there's a problem with race index,
          // the building will be added to romulans
          RBLD_SET.put(id, num);
          break;
      }
    }

    markSaved();
  }

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    // c
    saveSingleSet(CStbof.Race.Card, CBLD_SET, out);
    // h
    saveSingleSet(CStbof.Race.Fed, HBLD_SET, out);
    // f
    saveSingleSet(CStbof.Race.Ferg, FBLD_SET, out);
    // k
    saveSingleSet(CStbof.Race.Kling, KBLD_SET, out);
    // r
    saveSingleSet(CStbof.Race.Rom, RBLD_SET, out);
  }

  @Override
  public void clear() {
    CBLD_SET.clear();
    HBLD_SET.clear();
    FBLD_SET.clear();
    KBLD_SET.clear();
    RBLD_SET.clear();
    markChanged();
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
    Edifice building = null;
    short max_id = Short.MAX_VALUE;

    if (STBOF != null) {
      building = (Edifice) STBOF.tryGetInternalFile(CStbofFiles.EdificeBst, true);
      if (building != null)
        max_id = (short) building.getNumberOfEntries();
    }

    //check all values
    singleSetCheck(max_id, CBLD_SET, response);
    singleSetCheck(max_id, HBLD_SET, response);
    singleSetCheck(max_id, FBLD_SET, response);
    singleSetCheck(max_id, KBLD_SET, response);
    singleSetCheck(max_id, RBLD_SET, response);
  }

  /**
   * Used to get the uncompressed size of the file.
   */
  @Override
  public int getSize() {
    return (CBLD_SET.size() + HBLD_SET.size() + FBLD_SET.size()
        + KBLD_SET.size() + RBLD_SET.size()) * 8;
  }

  /**
   * Returns the number of buildings.
   */
  public int getNum(short race, short bld) throws InvalidArgumentsException {
    TreeMap<Short, Integer> map = getMap(race);
    return map.getOrDefault(bld, 0);
  }

  /**
   * Sets the number of buildings.
   */
  public void setNum(short race, short bld, int num) throws InvalidArgumentsException {
    if (num < 0) {
      String msg = Language.getString("BldSet.4"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(num)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    TreeMap<Short, Integer> map = getMap(race);

    if (num == 0) {
      if (map.remove(bld) != null)
        markChanged();
    } else {
      Integer prev = map.put(bld, num);
      if (prev == null || prev != num)
        markChanged();
    }
  }

  /**
   * Returns an array of building ids.
   */
  public short[] getBuildings(short race) throws InvalidArgumentsException {
    return CollectionTools.toShortArray(getMap(race).keySet());
  }

  private void saveSingleSet(int raceId, TreeMap<Short, Integer> map, OutputStream out)
    throws IOException {
    for (Entry<Short, Integer> entry : map.entrySet()) {
      out.write(DataTools.toByte((short) raceId, true));
      out.write(DataTools.toByte(entry.getKey(), true));
      out.write(DataTools.toByte(entry.getValue(), true));
    }
  }

  private void singleSetCheck(short max_id, TreeMap<Short, Integer> map, Vector<String> response) {
    Short[] keys = map.keySet().toArray(new Short[0]);

    for (Short id : keys) {
      if (id < 0 || id >= max_id) {
        String msg = Language.getString("BldSet.1"); //$NON-NLS-1$
        msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
        msg += " (removed)";
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

        map.remove(id);
      } else {
        int num = map.get(id);

        if (num < 1) {
          String msg = Language.getString("BldSet.2"); //$NON-NLS-1$
          msg = msg.replace("%1", Short.toString(id)); //$NON-NLS-1$
          msg += " (removed)";
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));

          map.remove(id);
        }
      }
    }
  }

  private TreeMap<Short, Integer> getMap(short race) throws InvalidArgumentsException {
    if (race < 0 || race > 4) {
      String msg = Language.getString("BldSet.3"); //$NON-NLS-1$
      msg = msg.replace("%1", Short.toString(race)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    switch (race) {
      case CStbof.Race.Card:
        return CBLD_SET;
      case CStbof.Race.Fed:
        return HBLD_SET;
      case CStbof.Race.Ferg:
        return FBLD_SET;
      case CStbof.Race.Kling:
        return KBLD_SET;
      default:
        return RBLD_SET;
    }
  }

  private void setMap(short race, TreeMap<Short, Integer> map) throws InvalidArgumentsException {
    if (race < 0 || race > 4) {
      String msg = Language.getString("BldSet.3"); //$NON-NLS-1$
      msg = msg.replace("%1", Short.toString(race)); //$NON-NLS-1$

      throw new InvalidArgumentsException(msg);
    }

    switch (race) {
      case CStbof.Race.Card:
        CBLD_SET = map;
        break;
      case CStbof.Race.Fed:
        HBLD_SET = map;
        break;
      case CStbof.Race.Ferg:
        FBLD_SET = map;
        break;
      case CStbof.Race.Kling:
        KBLD_SET = map;
        break;
      default:
        RBLD_SET = map;
    }
  }

  public void shiftBuidingIndices(short addedIndex) {
    try {
      for (short empire = 0; empire < NUM_EMPIRES; empire++) {
        TreeMap<Short, Integer> map = getMap(empire);
        TreeMap<Short, Integer> copy = new TreeMap<Short, Integer>();

        boolean changed = false;
        for (Entry<Short, Integer> entry : map.entrySet()) {
          short key = entry.getKey();
          if (key >= addedIndex) {
            key++;
            changed = true;
          }

          copy.put(key, entry.getValue());
        }

        if (changed) {
          setMap(empire, copy);
          markChanged();
        }
      }
    } catch (InvalidArgumentsException e) {
      e.printStackTrace();
    }
  }

  public void removeBuidingIndex(short removedIndex) {
    try {
      for (short empire = 0; empire < NUM_EMPIRES; empire++) {
        TreeMap<Short, Integer> map = getMap(empire);
        TreeMap<Short, Integer> copy = new TreeMap<Short, Integer>();

        boolean changed = false;
        for (Entry<Short, Integer> entry : map.entrySet()) {
          short key = entry.getKey();
          if (key > removedIndex) {
            // update keys following the removed one
            key--;
            changed = true;
          } else if (key == removedIndex) {
            // skip removed entry
            changed = true;
            continue;
          }

          copy.put(key, entry.getValue());
        }

        if (changed) {
          setMap(empire, copy);
          markChanged();
        }
      }
    } catch (InvalidArgumentsException e) {
      e.printStackTrace();
    }
  }

  public void switchedBuidingIndex(short idx1, short idx2) {
    try {
      for (short empire = 0; empire < NUM_EMPIRES; empire++) {
        TreeMap<Short, Integer> map = getMap(empire);
        Integer num1 = map.remove(idx1);
        Integer num2 = map.remove(idx2);

        if (num1 != null)
          map.put(idx2, num1);
        if (num2 != null)
          map.put(idx1, num2);
      }
    } catch (InvalidArgumentsException e) {
      e.printStackTrace();
    }
  }
}
