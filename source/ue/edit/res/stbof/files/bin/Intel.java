package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.common.CommonStrings;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 *
 */
public class Intel extends InternalFile {

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  private IntelEntry[] entries = new IntelEntry[NUM_EMPIRES];

  public Intel(Stbof stbof) {
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#check()
   */
  @Override
  public void check(Vector<String> response) {
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#getSize()
   */
  @Override
  public int getSize() {
    return 96 * entries.length;
  }

  @Override
  public void load(InputStream in) throws IOException {
    if (in.available() < getSize()) {
      throw new IOException(CommonStrings.getLocalizedString(
        CommonStrings.File_Is_Corrupted_1, "intel.bin"));
    }

    for (int i = 0; i < NUM_EMPIRES && in.available() > 0; i++) {
      entries[i] = new IntelEntry(in);
    }

    markSaved();
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#save(java.io.OutputStream)
   */
  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < entries.length; i++) {
      entries[i].save(out);
    }
  }

  @Override
  public void clear() {
    Arrays.fill(entries, null);
    markChanged();
  }

  public int getBaseEspAtkMul(int empire) {
    checkIndex(empire);
    return entries[empire].base_esp_attack_mult;
  }

  public int getBaseSabAtkMul(int empire) {
    checkIndex(empire);
    return entries[empire].base_sab_attack_mult;
  }

  public int getTargetEmpAtkMul(int empire, int target) {
    checkIndex(empire);
    checkIndex(target, 4);
    return entries[empire].target_emp_attack_mult[target];
  }

  public int getDiffLvlAtkMul(int empire, int level) {
    checkIndex(empire);
    checkIndex(level, 4);
    return entries[empire].diff_level_attack_mult[level];
  }

  public int getMoraleLvlAtkMul(int empire, int level) {
    checkIndex(empire);
    checkIndex(level, 7);
    return entries[empire].morale_level_attack_mult[level];
  }

  public int getTargetTreatyAtkMul(int empire, int treaty) {
    checkIndex(empire);
    checkIndex(treaty, 3);
    return entries[empire].target_treaty_attack_mult[treaty];
  }

  public void setBaseEspAtkMul(int empire, int mult) {
    checkIndex(empire);

    if (entries[empire].base_esp_attack_mult != mult) {
      entries[empire].base_esp_attack_mult = mult;
      markChanged();
    }
  }

  public void setBaseSabAtkMul(int empire, int mult) {
    checkIndex(empire);
    if (entries[empire].base_sab_attack_mult != mult) {
      entries[empire].base_sab_attack_mult = mult;
      markChanged();
    }
  }

  public void setTargetEmpAtkMul(int empire, int target, int mult) {
    checkIndex(empire);
    checkIndex(target, 4);
    if (entries[empire].target_emp_attack_mult[target] != mult) {
      entries[empire].target_emp_attack_mult[target] = mult;
      markChanged();
    }
  }

  public void setDiffLvlAtkMul(int empire, int level, int mult) {
    checkIndex(empire);
    checkIndex(level, 4);

    if (entries[empire].diff_level_attack_mult[level] != mult) {
      entries[empire].diff_level_attack_mult[level] = mult;
      markChanged();
    }
  }

  public void setMoraleLvlAtkMul(int empire, int level, int mult) {
    checkIndex(empire);
    checkIndex(level, 7);

    if (entries[empire].morale_level_attack_mult[level] != mult) {
      entries[empire].morale_level_attack_mult[level] = mult;
      markChanged();
    }
  }

  public void setTargetTreatyAtkMul(int empire, int treaty, int mult) {
    checkIndex(empire);
    checkIndex(treaty, 3);

    if (entries[empire].target_treaty_attack_mult[treaty] != mult) {
      entries[empire].target_treaty_attack_mult[treaty] = mult;
      markChanged();
    }
  }

  private void checkIndex(int index) {
    if (index >= entries.length) {
      throw new IndexOutOfBoundsException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(index)));
    }
  }

  private void checkIndex(int index, int max) {
    if (index > max) {
      throw new IndexOutOfBoundsException(CommonStrings.getLocalizedString(
        CommonStrings.InvalidIndex_1, Integer.toString(index)));
    }
  }

  private class IntelEntry {

    public int base_esp_attack_mult;                            // base multiplier for espionage attack threshold (4)
    public int base_sab_attack_mult;                            // base multiplier for sabotage attack threshold (8)
    public int[] target_emp_attack_mult = new int[NUM_EMPIRES]; // 5x enemy empires multiplier for attack threshold (28)
    public int[] diff_level_attack_mult = new int[5];           // 5x difficulty level = multiplier for attack threshold (48)
    public int[] morale_level_attack_mult = new int[8];         // 8x empsInfo morale labels = security modifier%
    // written to intelInfo+2C (80)
    public int[] target_treaty_attack_mult = new int[4];        // 4x empsInfo treaty labels = attack threshold modifier
    // written to intelInfo+4C (96)

    public IntelEntry(InputStream in) throws IOException {
      base_esp_attack_mult = StreamTools.readInt(in, true);
      base_sab_attack_mult = StreamTools.readInt(in, true);

      int i;
      for (i = 0; i < NUM_EMPIRES; i++) {
        target_emp_attack_mult[i] = StreamTools.readInt(in, true);
      }
      for (i = 0; i < 5; i++) {
        diff_level_attack_mult[i] = StreamTools.readInt(in, true);
      }
      for (i = 0; i < 8; i++) {
        morale_level_attack_mult[i] = StreamTools.readInt(in, true);
      }
      for (i = 0; i < 4; i++) {
        target_treaty_attack_mult[i] = StreamTools.readInt(in, true);
      }
    }

    public void save(OutputStream out) throws IOException {
      out.write(DataTools.toByte(base_esp_attack_mult, true));
      out.write(DataTools.toByte(base_sab_attack_mult, true));

      int i;
      for (i = 0; i < NUM_EMPIRES; i++) {
        out.write(DataTools.toByte(target_emp_attack_mult[i], true));
      }
      for (i = 0; i < 5; i++) {
        out.write(DataTools.toByte(diff_level_attack_mult[i], true));
      }
      for (i = 0; i < 8; i++) {
        out.write(DataTools.toByte(morale_level_attack_mult[i], true));
      }
      for (i = 0; i < 4; i++) {
        out.write(DataTools.toByte(target_treaty_attack_mult[i], true));
      }
    }
  }
}
