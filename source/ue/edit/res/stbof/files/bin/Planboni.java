package ue.edit.res.stbof.files.bin;

/**
 * This class is a representation of planboni.bin, which contains bonus values for different planet
 * types and sizes. If a planet doesn't have an entry here, it means the planet has no bonus. The
 * bonus is calculated like this: [bonus value]x5%
 */
public class Planboni extends PlanMap {

  /**
   * Sets a planet bonus value mapping.
   *
   * @param type            the type of the planet
   * @param size            the size (0-small, 1-medium, 2-large)
   * @param foodBonusLvl    food bonus (0, 1 or 2)
   * @param energyBonusLvl  energy bonus (0, 1 or 2)
   * @param value           the bonus value (x * 5%) to be set for this planet
   */
  public boolean set(int type, int size, int foodBonusLvl, int energyBonusLvl, int value) {
    return super.set(type, size, foodBonusLvl, energyBonusLvl, value);
  }

  /**
   * Maps planet bonuses to actual food or energy bonus amounts.
   *
   * Even though only one resulting bonus can be seen in-game,
   * each planet actually has three bonus levels (food + energy + growth),
   * that effect each other. @see Planspac.get
   *
   * Only by the planet type it is defined whether the mapped value
   * applies to a food or energy bonus.
   *
   * @param type            the type of the planet
   * @param size            the size (0-small, 1-medium, 2-large)
   * @param foodBonusLvl    planet food bonus level (0, 1 or 2)
   * @param energyBonusLvl  planet energy bonus level (0, 1 or 2)
   * @return the bonus multiplier x in increments of 5%, defaults to 0 if missing
   */
  public int get(int type, int size, int foodBonusLvl, int energyBonusLvl) {
    return super.get(type, size, foodBonusLvl, energyBonusLvl);
  }
}
