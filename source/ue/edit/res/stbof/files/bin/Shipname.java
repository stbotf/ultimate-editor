package ue.edit.res.stbof.files.bin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.shp.ShipnameCode;
import ue.edit.res.stbof.Stbof;
import ue.exception.CorruptedFile;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of shipname.bin, which contains generic ship names divided between
 * groups.
 * <p>
 * File format looks like: Header: NameEntryCount[int]*GroupCount EntryListPointer[int] EntryList:
 * NameEntry[44bytes]*NumEntries: GroupIndex[short] Alignment[0xFFFF] ShipName[40bytes]
 */
public class Shipname extends InternalFile {

  private static final int ENTRY_SIZE = 44;
  private static final int ORIG_GROUP_CNT = 62;
  private static final int EXT_GROUP_CNT = 248;

  private static final String errMaxGroupsExceeded = "Index exceeds max number of shipname groups of %d.";

  private Trek trek = null;
  private ArrayList<ArrayList<String>> GROUP = new ArrayList<ArrayList<String>>(10);
  private boolean extendedGroups = false;

  public Shipname(Stbof stbof, Trek trek) {
    this.trek = trek;
  }

  /**
   * @param in the InputStream to read the file from
   */
  @Override
  public void load(InputStream in) throws IOException {
    GROUP.clear();

    extendedGroups = readGroupHeader(in);
    int groupCount = extendedGroups ? EXT_GROUP_CNT : ORIG_GROUP_CNT;
    GROUP.ensureCapacity(groupCount);

    // go thru all entries
    // reading the index first, then ship name
    // ship names might be placed randomly, index must be checked
    // when saved, indexes will go in an incrementing order
    while (in.available() >= ENTRY_SIZE) {
      // read group index
      short groupIndex = StreamTools.readShort(in, true);
      if (groupIndex >= groupCount) {
        String msg = String.format(errMaxGroupsExceeded, groupCount);
        throw new CorruptedFile(msg);
      }

      if (groupIndex < 0)
        groupIndex = 0;

      // add missing groups
      while (GROUP.size() <= groupIndex) {
        GROUP.add(new ArrayList<String>());
      }

      // 0xFFFF for all ship groups but the Sheliak one.
      // For the Sheliak it is 0x1B = 27, which might relate to their race id, which is 26 (0-based).
      // The value is not known to have any effect and is overridden and ignored by UE.
      short flags = StreamTools.readShort(in, true);
      if (flags != 27)
        validate(flags, (short)-1);

      // read ship name
      String shipName = StreamTools.readNullTerminatedBotfString(in, 40);

      GROUP.get(groupIndex).add(shipName);
    }

    markSaved();
  }

  private boolean readGroupHeader(InputStream in) throws IOException {
    // skip unmodified ship name group header
    StreamTools.skip(in, ORIG_GROUP_CNT * 4); // 62 entries * 4 bytes

    // read in the pointer to the start of the entry list
    int entryListPtr = StreamTools.readInt(in, true);

    // In unmodified shipname.bin, the entry list pointer points to the next following read position,
    // which is rather useless, but here is checked for extension compatibility.
    boolean extended = entryListPtr != 0xFC;
    if (extended) {
      // skip additional 186 header entries * 4 bytes
      StreamTools.skip(in, (EXT_GROUP_CNT - ORIG_GROUP_CNT) * 4);
    }

    return extended;
  }

  /**
   * used to save changes.
   *
   * @param out the OutputStream to write the file to
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    int fileGroups = getGroupLimit();
    int groupCount = GROUP.size();

    if (groupCount > fileGroups) {
      String err = "Shipname groups %d exceed max storage limit of %d.";
        err = String.format(err, groupCount, fileGroups);
      throw new IOException(err);
    }

    // write name group entry counts
    for (int i = 0; i < groupCount; i++) {
      ArrayList<String> arr = GROUP.get(i);
      byte[] b = DataTools.toByte(arr.size(), true);
      out.write(b);
    }

    // fill remaining empty groups
    for (int i = groupCount; i < fileGroups; i++) {
      byte[] b = DataTools.toByte(0, true);
      out.write(b);
    }

    // write entry list pointer
    int entryListPtr = (fileGroups * 4) + 4;
    byte[] b = DataTools.toByte(entryListPtr, true);
    out.write(b);

    // write name entries
    for (short groupIndex = 0; groupIndex < GROUP.size(); groupIndex++) {
      ArrayList<String> arr = GROUP.get(groupIndex);
      for (String shipName : arr) {
        // group index
        out.write(DataTools.toByte(groupIndex, true));
        // alignment
        out.write(DataTools.toByte((short) -1, true));
        // ship name
        b = DataTools.toByte(shipName, 40, 39);
        out.write(b);
      }
    }
  }

  private int getGroupLimit() {
    if (trek != null) {
      ShipnameCode fix = (ShipnameCode) trek.tryGetInternalFile(CTrekSegments.ShipnameCode, false);
      if (fix != null)
        return fix.getActiveOption() == ShipnameCode.OPTION_EXTENDED ? EXT_GROUP_CNT : ORIG_GROUP_CNT;
    }

    // if already flagged to be extendend on load, assume extended limit
    return extendedGroups ? EXT_GROUP_CNT : ORIG_GROUP_CNT;
  }

  @Override
  public void clear() {
    GROUP.clear();
    markChanged();
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return (4 * 63) + (entries() * 44);
  }

  /**
   * Used to get the number of ship names in file.
   *
   * @return number of ship names
   */
  public int entries() {
    int ent = 0;
    for (int i = 0; i < GROUP.size(); i++) {
      ent += GROUP.get(i).size();
    }
    return ent;
  }

  @Override
  public void check(Vector<String> response) {
    for (int i = 0; i < GROUP.size(); i++) {
      ArrayList<String> arr = GROUP.get(i);

      // For the original game, empty groups would mean an error.
      // By modding however there might be no ships using this group.
      // Therefore just notify on empty groups, but check for missing ones by shiplist.sst.
      if (arr.size() == 0) {
        String msg = Language.getString("Shipname.0"); //$NON-NLS-1$
        msg = msg.replace("%", Integer.toString(i)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
      }
    }
  }

  /**
   * Adds a shipname to the specified group
   *
   * @param group the group to add the name to
   * @param name  the ship name to be added to the group
   * @throws InvalidArgumentsException if incorrect parameters are given
   */
  public void add(int group, String name) throws InvalidArgumentsException {
    try {
      ArrayList<String> arr = GROUP.get(group);
      if (arr.contains(name))
        return;

      if (name.length() > 39)
        name = name.substring(0, 38);

      arr.add(name);
      Collections.sort(arr);
      markChanged();
    } catch (Exception e) {
      String msg = Language.getString("Shipname.2"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(group)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }
  }

  /**
   * Removes a shipname from the specified group
   *
   * @param group the group from which the name should be removed
   * @param name  the ship name to be removed
   * @throws InvalidArgumentsException if incorrect parameters are given
   */
  public void remove(int group, String name) throws InvalidArgumentsException {
    try {
      ArrayList<String> arr = GROUP.get(group);
      arr.remove(name);
      Collections.sort(arr);
      markChanged();
    } catch (Exception e) {
      String msg = Language.getString("Shipname.2"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(group)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }
  }

  /**
   * Sets a shipname in the specified group.
   *
   * @param group   the group to be edited
   * @param oldName the old ship name to be changed
   * @param name    the name to replace the old one
   * @return true if the name was changed, else false
   * @throws InvalidArgumentsException if incorrect parameters are given
   */
  public void set(int group, String oldName, String name) throws InvalidArgumentsException {
    try {
      if (name.length() > 39)
        name = name.substring(0, 38);

      ArrayList<String> arr = GROUP.get(group);
      int i = arr.indexOf(oldName);
      if (i < 0)
        return;

      arr.set(i, name);
      Collections.sort(arr);
      markChanged();
    } catch (Exception e) {
      String msg = Language.getString("Shipname.2"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(group)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }
  }

  public boolean hasEntries(int group) {
    if (group >= GROUP.size())
      return false;
    return GROUP.get(group).size() > 0;
  }

  /**
   * Return a list of ship names.
   *
   * @param group the group from which the shipname are to be returned
   * @return an array of Strings containing ship names from the specified group
   */
  public String[] getEntries(int group) {
    try {
      ArrayList<String> arr = GROUP.get(group);
      Collections.sort(arr);
      return arr.toArray(new String[0]);
    } catch (IndexOutOfBoundsException e) {
      String msg = Language.getString("Shipname.2"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(group)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  public int getNumberOfGroups() {
    return GROUP.size();
  }

  public void addGroup() throws Exception {
    if (GROUP.size() >= getGroupLimit())
      throw new Exception("Maximum number of name groups reached.");

    GROUP.add(new ArrayList<String>());
    markChanged();
  }

  public void removeGroup(int index) {
    GROUP.remove(index);
    markChanged();
  }

  public ArrayList<ID> AvailableNameGroups() {
    ArrayList<ID> nameGroups = new ArrayList<ID>();
    int i = 0;
    for (ArrayList<String> entryList : GROUP) {
      if (!entryList.isEmpty()) {
        nameGroups.add(new ID(Integer.toString(i), i));
      }
      ++i;
    }
    return nameGroups;
  }
}
