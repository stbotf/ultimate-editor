package ue.edit.res.stbof.files.lst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class represents texture.lst.
 */
public class Texture extends InternalFile {

  private static final int END = -1;

  private ArrayList<String> ENTRY = new ArrayList<String>();

  public Texture() {
  }

  public Texture(InputStream in) throws IOException {
    load(in);
  }

  /**
   * Adds the specified string to the end of the list.
   *
   * @param n the string to be added.
   * @return index of the added texture
   */
  public int add(String n) {
    if ((n.length() > 31) || (n.length() < 1)) {
      return -1;
    }
    this.ENTRY.add(n);
    this.markChanged();
    return this.ENTRY.size() - 1;
  }

  public String get(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      throw new IndexOutOfBoundsException(Language.getString("Texture.2")
        .replace("%1", Integer.toString(index))); //$NON-NLS-1$ //$NON-NLS-2$
    }
    return ENTRY.get(index);
  }

  /**
   * Returns true if the texture is in the list.
   *
   * @param n the string to be removed.
   */
  public boolean contains(String n) {
    return this.ENTRY.contains(n);
  }

  public boolean anyTextureFileBeginsWith(String prefix) {
    prefix = prefix.toLowerCase();

    for (int i = 0; i < ENTRY.size(); i++) {
      if (ENTRY.get(i).toLowerCase().startsWith(prefix)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Sets the value at the specified index.
   *
   * @param index the index of the entry to change.
   * @param n     the string to replace the old one.
   */
  public void set(int index, String n) {
    if ((n.length() > 31) || (n.length() < 1)) {
      return;
    }
    this.ENTRY.set(index, n);
    this.markChanged();
  }

  public boolean isTextureValid(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      return false;
    }

    return ENTRY.get(index).toLowerCase().endsWith(".gif");
  }

  /**
   * Returns a list of all the entries in the file.
   */
  public String[] getEntries() {
    ArrayList<String> valid = new ArrayList<String>();

    String[] x = new String[1];

    for (int i = 0; i < ENTRY.size(); i++) {
      x[0] = ENTRY.get(i);

      if (isTextureValid(i)) {
        valid.add(x[0]);
      }
    }

    x = valid.toArray(new String[0]);

    return x;
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    //num of entries
    int num = StreamTools.readInt(in, true);

    //entries
    for (int i = 0; i < num; ++i) {
      //index
      int index = StreamTools.readInt(in, true);
      if (index != -1) {
        //entry
        String entry = StreamTools.readNullTerminatedBotfString(in, 32);
        entry = entry.toLowerCase();
        // add - this can be really really bad
        // fixing this won't be easy because other classes depend on this behaviour
        // does botf read this file in a similiar fashion?
        ENTRY.add(index, entry);
      }
    }

    markSaved();
  }

  /**
   * used to save changes.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    // num of entries
    int num = ENTRY.size();
    out.write(DataTools.toByte(num, true));

    // entries
    for (int i = 0; i < num; i++) {
      // index
      out.write(DataTools.toByte(i, true));
      // entry
      String entry = ENTRY.get(i);
      out.write(DataTools.toByte(entry, 32, 31));
    }

    out.write(DataTools.toByte(END, true));
  }

  @Override
  public void clear() {
    ENTRY.clear();
    markChanged();
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return 8 + ENTRY.size() * 36;
  }

  public int getMaxIndex() {
    return ENTRY.size() - 1;
  }

  public int getTextureNum() {
    return this.getEntries().length;
  }

  public int getTextureIndex(String texture) {
    for (int i = 0; i < this.ENTRY.size(); i++) {
      if (texture.toLowerCase().equals(this.ENTRY.get(i).toLowerCase())) {
        return i;
      }
    }

    return -1;
  }

  /**
   * @param index index of the texture to remove
   */
  public void remove(int index) {
    this.ENTRY.set(index, "X"); //$NON-NLS-1$
    this.markChanged();
  }

  public int getUnusedIndex() {
    for (int i = 0; i < ENTRY.size(); i++) {
      if (!ENTRY.get(i).toLowerCase().endsWith(".gif")) {
        return i;
      }
    }

    return -1;
  }

  @Override
  public void check(Vector<String> response) {
    try {
      // get registered palette count
      PaletteList paletteList = UE.FILES.stbof().files().paletteLst();
      int num = paletteList.getNumberOfEntries();

      // check texture palette count
      if (ENTRY.size() > num) {
        String msg = Language.getString("Texture.0"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(this.ENTRY.size() - num)); //$NON-NLS-1$
        response.add(msg);
      }
    } catch (Exception e) {
      String msg = Language.getString("Texture.1"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%2", e.getMessage()); //$NON-NLS-1$
      response.add(msg);
    }
  }

}
