package ue.edit.res.stbof.files.lst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Vector;
import lombok.val;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.img.GifColorMap;
import ue.util.stream.StreamTools;

/**
 * This class represents pallete.lst.
 */
public class PaletteList extends InternalFile {

  //contains palette indexes (maps texture index->palette index)
  private ArrayList<Integer> ENTRY = new ArrayList<Integer>();
  //list of palettes (palette index -> palette)
  private ArrayList<GifColorMap> PALETTE = new ArrayList<GifColorMap>();

  public PaletteList() {
  }

  public PaletteList(InputStream in) throws IOException {
    load(in);
  }

  /**
   * @param in the InputStream to read from
   */
  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();
    PALETTE.clear();

    //number of entries (needs to be equal to number of textures in texture.lst)
    int n = StreamTools.readInt(in, true);

    //entries - each has contains and index pointing to the actual palette
    for (int i = 0; i < n; i++) {
      ENTRY.add(StreamTools.readInt(in, true));
    }

    // number of palettes
    int z = StreamTools.readInt(in, true);

    // data - the palettes
    for (int i = 0; i < z; i++) {
      PALETTE.add(new GifColorMap(in, 256, true));
    }

    markSaved();
  }

  /**
   * used to save changes.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    //number of entries
    int numo = ENTRY.size();
    out.write(DataTools.toByte(numo, true));

    //entries
    for (int i = 0; i < numo; i++) {
      out.write(DataTools.toByte((ENTRY.get(i)).intValue(), true));
    }

    //data
    out.write(DataTools.toByte(PALETTE.size(), true));
    for (int i = 0; i < PALETTE.size(); i++) {
      GifColorMap gf = PALETTE.get(i);
      gf.save(out, true);
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    PALETTE.clear();
    markChanged();
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return PALETTE.size() * 1024 + ENTRY.size() * 4 + 8;
  }

  public int getNumberOfEntries() {
    return ENTRY.size();
  }

  public int getPalettesNum() {
    return PALETTE.size();
  }

  /**
   * Checks if the color maps match.
   *
   * @param index the index of the gif in texture.lst
   * @param pal   the color map of the gif
   * @return true if the color maps are equal
   */
  public boolean checkColorMap(int index, GifColorMap pal) {
    GifColorMap pal2 = getInternalColorMap(index);
    return pal.equals(pal2);
  }

  /**
   * Sets the color map. Works also for gifs not registered in palette.lst yet.
   *
   * @param index the index of the gif in texture.lst
   * @param pal   the color map of the gif
   */
  public void setColorMap(int index, GifColorMap pal) {
    //make room
    while (index >= ENTRY.size()) {
      ENTRY.add(0);
      markChanged();
    }

    //check if color map is already registered, then assign
    GifColorMap gcm = getColorMap(index);
    if (pal.equals(gcm))
      return;

    for (int i = 0; i < PALETTE.size(); i++) {
      gcm = PALETTE.get(i);

      if (pal.equals(gcm)) {
        ENTRY.set(index, i);
        markChanged();
        return;
      }
    }

    //obviously not, add new
    PALETTE.add(new GifColorMap(pal));
    ENTRY.set(index, (PALETTE.size() - 1));
    markChanged();
  }

  /**
   * Get registered palette.lst color map.
   * @param index the index of the gif in texture.lst
   * @return the color map or null if out of range
   */
  public GifColorMap getColorMap(int index) {
    val colorMap = getInternalColorMap(index);
    return colorMap != null ? new GifColorMap(colorMap) : null;
  }

  /**
   * Returns the palette on the specified index.
   *
   * @param index the index of the palette
   */
  public GifColorMap getPalette(int index) {
    checkPaletteIndex(index);
    return new GifColorMap(PALETTE.get(index));
  }

  /**
   * Returns the palette index used by the given texture.
   */
  public int getPaletteIndex(int texture) {
    checkTextureIndex(texture);
    return ENTRY.get(texture);
  }

  public class TrimResult {
    public int palettesRemoved = 0;
    public int entriesUpdated = 0;
    public boolean hasChanged() { return entriesUpdated > 0 || palettesRemoved > 0; }
  }

  /**
   * Removes unused entries and thus reduces the size of the file.
   */
  public TrimResult trim(Texture tex) {
    TrimResult result = new TrimResult();

    if (tex != null) {
      // find a valid texture and get it's palette index
      String[] textures = tex.getEntries();

      if (textures.length > 0) {
        // get palette index from first texture
        int texIdx = tex.getTextureIndex(textures[0]);
        int palIdx = ENTRY.get(texIdx);

        // set same palette index for all deleted textures
        for (int i = 0; i < ENTRY.size(); i++) {
          if (!tex.isTextureValid(i)) {
            ENTRY.set(i, palIdx);
            result.entriesUpdated++;
            markChanged();
          }
        }
      }
    }

    // find "deleted" textures and assign
    // mod[i] stores info on how many places will be deleted before i
    int[] mod = new int[PALETTE.size()];
    for (int i = 0; i < PALETTE.size(); i++) {
      if (!ENTRY.contains(i)) {
        for (int j = i; j < PALETTE.size(); j++) {
          mod[j]++;
        }
      }
    }

    // deletion
    // rem makes sure we're testing the right index, since indexes are lowered by 1 on each deletion
    int rem = 0;
    for (int i = 0; i < PALETTE.size(); i++) {
      if (!ENTRY.contains(i + rem)) {
        PALETTE.remove(i--);
        result.palettesRemoved++;
        markChanged();
        rem++;
      }
    }

    // fix all old indexes to point to the new ones
    // making use of mod[]
    for (int i = 0; i < ENTRY.size(); i++) {
      int g = ENTRY.get(i);
      int trimmed = mod[g];

      if (trimmed != 0) {
        ENTRY.set(i, g - trimmed);
        result.entriesUpdated++;
        markChanged();
      }
    }

    if (result.hasChanged()) {
      String msg = Language.getString("PaletteList.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(result.entriesUpdated)) //$NON-NLS-1$
        .replace("%2", Integer.toString(result.palettesRemoved)); //$NON-NLS-1$
        System.out.println(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));
    }

    return result;
  }

  @Override
  public void check(Vector<String> response) {
    Stbof stbof = UE.FILES.stbof();

    TrimResult trimRes;
    if (stbof != null) {
      try {
        Texture tex = (Texture) stbof.getInternalFile(CStbofFiles.TextureLst, true);
        trimRes = trim(tex);
      } catch (IOException ex) {
        response.add(getCheckIntegrityString(PaletteList.INTEGRITY_CHECK_ERROR, ex.getMessage()));
        trimRes = trim(null);
      }
    } else {
      trimRes = trim(null);
    }

    if (trimRes.hasChanged()) {
      String msg = Language.getString("PaletteList.0") //$NON-NLS-1$
        .replace("%1", Integer.toString(trimRes.entriesUpdated)) //$NON-NLS-1$
        .replace("%2", Integer.toString(trimRes.palettesRemoved)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));
    }

    for (int i = 0; i < ENTRY.size(); i++) {
      int z = ENTRY.get(i);
      if (z >= PALETTE.size() || z < 0) {
        String msg = Language.getString("PaletteList.1") //$NON-NLS-1$
          .replace("%1", Integer.toString(z)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(PaletteList.INTEGRITY_CHECK_ERROR, msg));

        ENTRY.set(i, 0);
        markChanged();
      }
    }

    if (PALETTE.size() > 127) {
      String msg = Language.getString("PaletteList.2") //$NON-NLS-1$
        .replace("%1", getName()); //$NON-NLS-1$
      response.add(getCheckIntegrityString(PaletteList.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  private void checkTextureIndex(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("PaletteList.3") //$NON-NLS-1$
        .replace("%1", Integer.toString(index)); //$NON-NLS-1$
      msg = getCheckIntegrityString(PaletteList.INTEGRITY_CHECK_ERROR, msg);
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private void checkPaletteIndex(int index) {
    if (index < 0 || index >= PALETTE.size()) {
      String msg = Language.getString("PaletteList.3") //$NON-NLS-1$
        .replace("%1", Integer.toString(index)); //$NON-NLS-1$
      msg = getCheckIntegrityString(PaletteList.INTEGRITY_CHECK_ERROR, msg);
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private GifColorMap getInternalColorMap(int index) {
    checkTextureIndex(index);
    int z = ENTRY.get(index);
    return z >= 0 && z < PALETTE.size() ? PALETTE.get(z) : null;
  }
}
