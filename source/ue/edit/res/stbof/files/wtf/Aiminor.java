package ue.edit.res.stbof.files.wtf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.emp.RaceList;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class Aiminor extends InternalFile {

  public static final int PERSONALITY_SETTING = 0;
  public static final int EXPLORER_SETTING = 1;
  public static final int HOSTILITY1_SETTING = 2;
  public static final int HOSTILITY2_SETTING = 3;
  public static final int HOSTILITY3_SETTING = 4;
  public static final int HOSTILITY4_SETTING = 5;
  public static final int HOSTILITY5_SETTING = 6;

  private Stbof stbof;
  private Trek trek;
  private byte[] UNKNOWN_1;
  private String[] SETTING;
  private String[] MINOR;
  //[race index][setting]
  private double[][] VALUE;

  public Aiminor(Stbof stbof, Trek trek) {
    this.stbof = stbof;
    this.trek = trek;
  }

  public boolean containsRace(String race) {
    for (String r : MINOR) {
      if (r.equals(race)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Used to change entry name
   *
   * @param index the index of the entry to be changed
   * @param name  the new name to be aplied
   */
  public void setName(int index, String name) {
    checkIndex(index);
    if (name.length() == 0 || name.length() > 32) {
      return;
    }
    if (name.equals(getName(index))) {
      return;
    }
    MINOR[index] = name;
    this.markChanged();
  }

  /**
   * Used to get entry name
   *
   * @param index the index of the entry
   */
  public String getName(int index) {
    checkIndex(index);
    return MINOR[index];
  }

  /**
   * Used to get entry index
   */
  public int getIndex(String name) {
    int numMinors = numMinors();
    for (int i = 0; i < numMinors; i++) {
      if (MINOR[i].equalsIgnoreCase(name)) {
        return i;
      }
    }

    return -1;
  }

  public String[] getNames() {
    return MINOR;
  }

  /**
   * used to remove entry
   *
   * @param index the index of the entry
   */
  public void remove(int index) {
    checkIndex(index);

    short num = (short) (MINOR.length - 1);

    String[] min = new String[num];
    double[][] val = new double[num][SETTING.length];

    System.arraycopy(MINOR, 0, min, 0, index);

    for (int i = 0; i < index; i++) {
      System.arraycopy(VALUE[i], 0, val[i], 0, SETTING.length);
    }

    System.arraycopy(MINOR, index + 1, min, index, min.length - index);

    for (int i = index; i < val.length; i++) {
      System.arraycopy(VALUE[i + 1], 0, val[i], 0, SETTING.length);
    }

    MINOR = min;
    VALUE = val;

    this.markChanged();
  }

  public void add() {
    add(-1);
  }

  /**
   * Used to add entry
   *
   * @param index the index of the entry to copy values from
   */
  public int add(int index) {
    if (index >= 0)
      checkIndex(index);

    int numMinors = numMinors();
    int numSettings = numSettings();

    if (SETTING == null)
      SETTING = new String[0];

    // copy array entries
    if (MINOR != null) {
      MINOR = Arrays.copyOf(MINOR, numMinors + 1);

      // copy added entry name
      if (index >= 0)
        MINOR[numMinors] = MINOR[index];
    } else {
      MINOR = new String[numMinors+1];
    }

    if (VALUE != null) {
      VALUE = Arrays.copyOf(VALUE, numMinors + 1);

      // copy added entry values
      VALUE[numMinors] = new double[numSettings];
      if (index >= 0) {
        for (int x = 0; x < numSettings; x++) {
          VALUE[numMinors][x] = VALUE[index][x];
        }
      }
    } else {
      VALUE = new double[numMinors+1][numSettings];
    }

    markChanged();
    return MINOR.length - 1;
  }

  /**
   * Used to get value of entry
   *
   * @param index   the index of the entry to get values from
   * @param setting the setting you want to get
   */
  public double getValue(int index, int setting) {
    checkIndex(index);
    checkSettingIndex(setting);
    return VALUE[index][setting];
  }

  /**
   * Used to get number of entries
   */
  public int getNumber() {
    return numMinors();
  }

  /**
   * Used to set a value of an entry
   *
   * @param index   the index of the entry to set values from
   * @param setting the setting you want to set
   * @param value   the new value to assign
   * @throws InvalidArgumentsException
   */
  public void setValue(int index, int setting, double value) throws InvalidArgumentsException {
    checkIndex(index);
    checkSettingIndex(setting);

    if (value == getValue(index, setting)) {
      return;
    }

    if (value < 0 || value > 8) {
      String msg = Language.getString("Aiminor.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Double.toString(value)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (setting == 0 && value > 4) {
      String msg = Language.getString("Aiminor.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Double.toString(value)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    if (setting == 1 && value > 1) {
      String msg = Language.getString("Aiminor.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Double.toString(value)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }

    VALUE[index][setting] = value;
    this.markChanged();
  }

  @Override
  public void load(InputStream in) throws IOException {
    //unknown
    UNKNOWN_1 = StreamTools.readBytes(in, 4);

    //settings
    int SETTINGS = StreamTools.readShort(in, true);
    //minors
    int MINORS = StreamTools.readShort(in, true);
    //values
    StreamTools.skip(in, 4);

    //set
    SETTING = new String[SETTINGS];
    MINOR = new String[MINORS];
    VALUE = new double[MINORS][SETTINGS];

    //read settings
    for (int i = 0; i < SETTINGS; i++) {
      SETTING[i] = StreamTools.readNullTerminatedBotfString(in, 33);
    }

    //read minors
    for (int i = 0; i < MINORS; i++) {
      MINOR[i] = StreamTools.readNullTerminatedBotfString(in, 33);
    }

    //read values
    for (int i = 0; i < MINORS; i++) {
      for (int j = 0; j < SETTINGS; j++) {
        VALUE[i][j] = StreamTools.readDouble(in, true);
      }
    }

    markSaved();
  }

  /**
   * Used to save changes.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    //unknown
    out.write(UNKNOWN_1);
    //settings
    byte[] b = DataTools.toByte((short) SETTING.length, true);
    out.write(b);
    //minors
    b = DataTools.toByte((short) MINOR.length, true);
    out.write(b);
    //values
    b = DataTools.toByte(SETTING.length * MINOR.length, true);
    out.write(b);
    //write settings
    for (int i = 0; i < SETTING.length; i++) {
      b = DataTools.toByte(SETTING[i], 33, 32);
      out.write(b);
    }
    //write minors
    for (int i = 0; i < MINOR.length; i++) {
      b = DataTools.toByte(MINOR[i], 33, 32);
      out.write(b);
    }
    //write values
    for (int i = 0; i < MINOR.length; i++) {
      for (int j = 0; j < SETTING.length; j++) {
        b = DataTools.toByte(VALUE[i][j], true);
        out.write(b);
      }
    }
  }

  @Override
  public void clear() {
    Arrays.fill(UNKNOWN_1, (byte)0);
    SETTING = null;
    MINOR = null;
    VALUE = null;
    markChanged();
  }

  public int numSettings() {
    return SETTING != null ? SETTING.length : 0;
  }

  public int numMinors() {
    return MINOR != null ? MINOR.length : 0;
  }

  /**
   * Used to get the uncompressed size of the file.
   */
  @Override
  public int getSize() {
    int numSettings = numSettings();
    int numMinors = numMinors();
    return (12 + (numSettings + numMinors) * 33 + (numSettings * numMinors) * 8);
  }

  @Override
  public void check(Vector<String> response) {
    // check number of settings
    int numSettings = numSettings();
    if (numSettings != 7) {
      String msg = Language.getString("Aiminor.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(numSettings)); //$NON-NLS-1$
      response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
    }

    // check sanity of set values
    int numMinors = numMinors();
    for (int i = 0; i < numMinors; i++) {
      for (int j = 0; j < numSettings; j++) {
        if (j == 0) {
          if (VALUE[i][j] < 0 || VALUE[i][j] > 4) {
            String msg = Language.getString("Aiminor.2"); //$NON-NLS-1$
            msg = msg.replace("%1", MINOR[i]); //$NON-NLS-1$
            msg = msg.replace("%2", Double.toString(VALUE[i][j])); //$NON-NLS-1$
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        } else if (j == 1) {
          if (VALUE[i][j] < 0 || VALUE[i][j] > 1) {
            String msg = Language.getString("Aiminor.3"); //$NON-NLS-1$
            msg = msg.replace("%1", MINOR[i]); //$NON-NLS-1$
            msg = msg.replace("%2", Double.toString(VALUE[i][j])); //$NON-NLS-1$
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        } else {
          if (VALUE[i][j] < 0 || VALUE[i][j] > 8) {
            String msg = Language.getString("Aiminor.4"); //$NON-NLS-1$
            msg = msg.replace("%1", MINOR[i]); //$NON-NLS-1$
            msg = msg.replace("%2", Double.toString(VALUE[i][j])); //$NON-NLS-1$
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
          }
        }
      }
    }

    if (stbof != null) {
      try {
        // check if races match race.rst
        RaceRst race = (RaceRst) stbof.getInternalFile(CStbofFiles.RaceRst, true);

        boolean found[] = new boolean[race.getNumberOfEntries()];
        boolean allFound = true;

        int numMinorsExpected = race.getNumberOfEntries() - 5;
        if (numMinorsExpected != numMinors) {
          String msg = Language.getString("Aiminor.7")
              .replace("%1", CStbofFiles.RaceRst);  //$NON-NLS-1$ //$NON-NLS-2$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
        }

        for (int i = 5; i < race.getNumberOfEntries(); i++) {
          String minor = race.getName(i);

          if (!this.containsRace(minor)) {
            allFound = found[i] = false;    // not found in aiminor

            String msg = Language.getString("Aiminor.8")
                .replace("%1", minor); //$NON-NLS-1$ //$NON-NLS-2$
            msg = msg.replace("%2", CStbofFiles.RaceRst); //$NON-NLS-1$
            response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
          } else {
            found[i] = true;
          }
        }

        // get trek.exe
        if (trek != null) {
          RaceList rlst = (RaceList) trek.getInternalFile(CTrekSegments.RaceList, true);

          // compare trek.exe race names with stbof.res
          for (int i = 0; i < race.getNumberOfEntries(); i++) {
            if (!rlst.getRaceString(i).equals(race.getName(i))) {
              String msg = Language.getString("Aiminor.9"); //$NON-NLS-1$
              msg = msg.replace("%1", rlst.getRaceString(i)); //$NON-NLS-1$
              msg = msg.replace("%2", CStbofFiles.RaceRst); //$NON-NLS-1$
              msg = msg.replace("%3", race.getName(i));
              msg += " (fixed)";

              // rename entry in trek.exe to match race.rst
              rlst.setRaceString(i, race.getName(i));

              response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
            }
          }

          // compare aiminor.wtf race names with stbof.res
          for (int i = 0; i < race.getNumberOfEntries(); i++) {
            if (i > 4 && !found[i]) {
              for (int j = 0; j < numMinors; j++) {
                if (!race.containsRace(MINOR[j])) {
                  String msg = Language.getString("Aiminor.10"); //$NON-NLS-1$
                  msg = msg.replace("%1", MINOR[j]); //$NON-NLS-1$
                  msg = msg.replace("%2", CStbofFiles.RaceRst); //$NON-NLS-1$

                  MINOR[j] = race.getName(i);
                  this.markChanged();

                  msg = msg.replace("%3", MINOR[j]); //$NON-NLS-1$
                  msg += " (fixed)";
                  response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));

                  found[i] = true;
                  break;
                }
              }

              if (!found[i]) {
                // on empty list index -1 the copy is skipped
                this.add(numMinors - 1);
                numMinors++;

                MINOR[MINOR.length - 1] = race.getName(i);
                this.markChanged();
                found[i] = true;

                String msg = Language.getString("Aiminor.11")
                    .replace("%1", race.getName(i)); //$NON-NLS-1$ //$NON-NLS-2$
                msg += " (added)";
                response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));
              }
            }
          }

          // now remove the extras
          // remove from the back to not mess the index
          for (int j = numMinors - 1; j >= 0; --j) {
            if (!race.containsRace(MINOR[j])) {
              String msg = Language.getString("Aiminor.13"); //$NON-NLS-1$
              msg = msg.replace("%1", MINOR[j]); //$NON-NLS-1$
              msg += " (removed)";
              response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));

              this.remove(j);
            }
          }
        } else if (!allFound) {
          // can't fix without trek.exe
          String msg = Language.getString("Aiminor.12"); //$NON-NLS-1$
          response.add(getCheckIntegrityString(INTEGRITY_CHECK_INFO, msg));
        }
      } catch (Exception e) {
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, e.getMessage()));
      }
    }
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= numMinors()) {
      String msg = Language.getString("Aiminor.5"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private void checkSettingIndex(int setting) {
    if (setting < 0 || setting >= numSettings()) {
      String msg = Language.getString("Aiminor.6"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(setting)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }
}