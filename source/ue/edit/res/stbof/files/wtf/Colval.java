package ue.edit.res.stbof.files.wtf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class Colval extends InternalFile {

  private byte[] UNKNOWN_1;
  private short SETTINGS = 0;
  private short SETS = 0;
  private int VALUES = 0;
  private String[] SETTING;
  private String[] SET;
  //[set][setting]
  private double[][] VALUE;

  public Colval(Stbof stbof) {
  }

  /**
   * used to get value of entry
   *
   * @param index   the index of the entry to get values from
   * @param setting the setting you want to get
   */
  public double getValue(int index, int setting) {
    checkSetIndex(index);
    checkSettingIndex(setting);
    return VALUE[index][setting];
  }

  /**
   * used to set a value of an entry
   *
   * @param index   the index of the entry to set values from
   * @param setting the setting you want to set
   * @param value   the new value to assign
   */
  public void setValue(int index, int setting, double value) {
    checkSetIndex(index);
    checkSettingIndex(setting);
    if (value == VALUE[index][setting])
      return;

    VALUE[index][setting] = value;
    this.markChanged();
  }

  @Override
  public void load(InputStream in) throws IOException {
    //unknown
    UNKNOWN_1 = StreamTools.readBytes(in, 4);
    //settings
    SETTINGS = StreamTools.readShort(in, true);
    //sets
    SETS = StreamTools.readShort(in, true);
    //values
    VALUES = StreamTools.readInt(in, true);

    //check for error and declare
    VALUES = SETTINGS * SETS;
    SETTING = new String[SETTINGS];
    SET = new String[SETS];
    VALUE = new double[SETS][SETTINGS];

    //read settings
    for (int i = 0; i < SETTINGS; i++) {
      try {
        SETTING[i] = StreamTools.readNullTerminatedBotfString(in, 33);
      } catch (Exception ef) {
        ef.printStackTrace();
      }
    }

    //read sets
    for (int i = 0; i < SETS; i++) {
      try {
        SET[i] = StreamTools.readNullTerminatedBotfString(in, 33);
      } catch (Exception ef) {
        ef.printStackTrace();
      }
    }

    //read values
    for (int i = 0; i < SETS; i++) {
      for (int j = 0; j < SETTINGS; j++) {
        VALUE[i][j] = StreamTools.readDouble(in, true);
      }
    }

    markSaved();
  }

  /**
   * used to save changes.
   */
  @Override
  public void save(OutputStream out) throws IOException {
    //unknown
    out.write(UNKNOWN_1);
    //settings
    byte[] b = DataTools.toByte(SETTINGS, true);
    out.write(b);
    //sets
    b = DataTools.toByte(SETS, true);
    out.write(b);
    //values
    b = DataTools.toByte(VALUES, true);
    out.write(b);
    //write settings
    for (int i = 0; i < SETTINGS; i++) {
      b = DataTools.toByte(SETTING[i], 33, 32);
      out.write(b);
    }
    //write sets
    for (int i = 0; i < SETS; i++) {
      b = DataTools.toByte(SET[i], 33, 32);
      out.write(b);
    }
    //write values
    for (int i = 0; i < SETS; i++) {
      for (int j = 0; j < SETTINGS; j++) {
        b = DataTools.toByte(VALUE[i][j], true);
        out.write(b);
      }
    }
  }

  @Override
  public void clear() {
    Arrays.fill(UNKNOWN_1, (byte)0);
    SETTINGS = 0;
    SETS = 0;
    VALUES = 0;
    SETTING = null;
    SET = null;
    VALUE = null;
    markChanged();
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return (12 + (SETTINGS + SETS) * 33 + VALUES * 8);
  }

  @Override
  public void check(Vector<String> response) {
    if (SETTINGS != 13) {
      String msg = Language.getString("Colval.0"); //$NON-NLS-1$
      msg = msg.replace("%1", getName()); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(SETTINGS)); //$NON-NLS-1$
      response.add(msg);
    }
  }

  private void checkSetIndex(int index) {
    if (index < 0 || index >= SETS) {
      String msg = Language.getString("Colval.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private void checkSettingIndex(int setting) {
    if (setting < 0 || setting >= SETTINGS) {
      String msg = Language.getString("Colval.2"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(setting)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }
}
