package ue.edit.res.stbof.files.wtf;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Represents aiempire.wtf.
 */
public class Aiempire extends InternalFile {

  private byte[] UNKNOWN_1;
  private short SETTINGS = 0;
  private short EMPIRES = 0;
  private int VALUES = 0;
  private String[] SETTING;
  private String[] EMPIRE;
  //[race index][setting]
  private double[][] VALUE;

  public Aiempire(Stbof stbof) {
  }

  @Override
  public void load(InputStream in) throws IOException {
    //unknown 4
    UNKNOWN_1 = StreamTools.readBytes(in, 4);
    //settings 2
    SETTINGS = StreamTools.readShort(in, true);
    //minors 2
    EMPIRES = StreamTools.readShort(in, true);
    //values 4
    VALUES = StreamTools.readInt(in, true);

    //check for error and declare
    VALUES = SETTINGS * EMPIRES;
    SETTING = new String[SETTINGS];
    EMPIRE = new String[EMPIRES];
    VALUE = new double[EMPIRES][SETTINGS];

    //read settings
    for (int i = 0; i < SETTINGS; i++) {
      SETTING[i] = StreamTools.readNullTerminatedBotfString(in, 33);
    }
    //read minors
    for (int i = 0; i < EMPIRES; i++) {
      EMPIRE[i] = StreamTools.readNullTerminatedBotfString(in, 33);
    }

    //read values
    for (int i = 0; i < EMPIRES; i++) {
      for (int j = 0; j < SETTINGS; j++) {
        VALUE[i][j] = StreamTools.readDouble(in, true);
      }
    }

    markSaved();
  }

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    //unknown
    out.write(UNKNOWN_1);
    //settings
    byte[] b = DataTools.toByte(SETTINGS, true);
    out.write(b);
    //minors
    b = DataTools.toByte(EMPIRES, true);
    out.write(b);
    //values
    b = DataTools.toByte(VALUES, true);
    out.write(b);
    //write settings
    for (int i = 0; i < SETTINGS; i++) {
      b = DataTools.toByte(SETTING[i], 33, 33);
      out.write(b);
    }
    //write minors
    for (int i = 0; i < EMPIRES; i++) {
      b = DataTools.toByte(EMPIRE[i], 33, 33);
      out.write(b);
    }
    //write values
    for (int i = 0; i < EMPIRES; i++) {
      for (int j = 0; j < SETTINGS; j++) {
        b = DataTools.toByte(VALUE[i][j], true);
        out.write(b);
      }
    }
  }

  @Override
  public void clear() {
    Arrays.fill(UNKNOWN_1, (byte)0);
    SETTINGS = 0;
    EMPIRES = 0;
    VALUES = 0;
    SETTING = null;
    EMPIRE = null;
    VALUE = null;
    markChanged();
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
    if (SETTINGS != 9) {
      String msg = Language.getString("Aiempire.0") //$NON-NLS-1$
        .replace("%1", getName()) //$NON-NLS-1$
        .replace("%2", Integer.toString(SETTINGS)); //$NON-NLS-1$
      response.add(msg);
    }
  }

  public int numEmpires() {
    return EMPIRE != null ? EMPIRE.length : 0;
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return (12 + (SETTINGS + EMPIRES) * 33 + VALUES * 8);
  }

  /**
   * used to set a value of an entry
   *
   * @param index   the index of the entry to set values from
   * @param setting the setting you want to set
   * @param value   the new value to assign
   */
  public void setValue(int index, int setting, double value) throws InvalidArgumentsException {
    if (value < 0) {
      String msg = Language.getString("Aiempire.1"); //$NON-NLS-1$
      msg = msg.replace("%1", Double.toString(value)); //$NON-NLS-1$
      throw new InvalidArgumentsException(msg);
    }
    try {
      if (VALUE[index][setting] == value)
        return;

      VALUE[index][setting] = value;
      markChanged();
    } catch (Exception e) {
      String msg = Language.getString("Aiempire.2"); //$NON-NLS-1$
      msg = msg.replace("%1", "(" + index + ", " + setting
          + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      throw new InvalidArgumentsException(msg);
    }
  }

  /**
   * used to get value of entry
   *
   * @param index   the index of the entry to get values from
   * @param setting the setting you want to get
   */
  public double getValue(int index, int setting) throws InvalidArgumentsException {
    try {
      return VALUE[index][setting];
    } catch (Exception e) {
      String msg = Language.getString("Aiempire.2"); //$NON-NLS-1$
      msg = msg.replace("%1", "(" + index + ", " + setting
          + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      throw new InvalidArgumentsException(msg);
    }
  }

  /**
   * Returns empire names:
   */
  public String[] getEmpires() {
    return Arrays.copyOf(EMPIRE, EMPIRE.length);
  }
}
