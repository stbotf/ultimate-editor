package ue.edit.res.stbof.files.dic.idx;

import ue.edit.res.stbof.files.dic.idx.LexMenuIdx.Combat.Encounter;

/**
 * Events listed by the event summary menu.
 *
 * Note, in stbof.ini there are three filter options: ECONFILTER, EVENTFILTER and MILITARYFILTER.
 * None of them seems to have any effect.
 *
 * TODO Includes many event related popup notifications, that should be moved to EventDetails.
 *
 * This class holds constants to index the lexicon.dic file.
 * The labels and grouping might not be accurate, but bring some order to the wide spread values.
 * In UE they might be used to index fixed game values by name rather than by id.
 */
public abstract class LexEventIdx {
  public abstract class Global {
    // EMPIRE_CEDED 266
    public static final int EmpireCeded = 265;        // "%s1 ceded their empire to %2"
    public static final int EmpireFallen = 966;       // "This will be a day long remembered.  %1 has fallen."
    // Defeated 215
    public static final int CardassianDefeat_1 = 834; // "Your management skills were adequate, but they pale beside the impressive accomplishments of those that have gone"
    public static final int CardassianDefeat_2 = 835; // "before you.  If you wish to improve yourself, you would do well to study their methods to find flaws in your strategies... or"
    public static final int CardassianDefeat_3 = 836; // "better still, find flaws with theirs."
    public static final int FederationDefeat_1 = 840; // "Unfortunately, your efforts on behalf of the Federation have not matched the success of previous players.  However,"
    public static final int FederationDefeat_2 = 841; // "Academy records show that practice and study can improve any skill in any student.  We advise you to review your"
    public static final int FederationDefeat_3 = 842; // "strategy, make some changes in your procedures, and try again."
    public static final int FerengiDefeat_1 = 837;    // "Your degree of success does not merit your inclusion into the Hall of Fame.  Clearly, your marketing tactics were not"
    public static final int FerengiDefeat_2 = 838;    // "sufficiently aggressive to dominate the galaxy as thoroughly as those who have gone before you.  As you prepare to"
    public static final int FerengiDefeat_3 = 839;    // "try again, consider Rule 65: Win or lose, there's always Hyperian beetle snuff."
    public static final int KlingonDefeat_1 = 843;    // "You have failed to improve upon the accomplishments of those who have gone before you.  Your achievements may be"
    public static final int KlingonDefeat_2 = 844;    // "honorable, but you have not earned a place in the Hall of Fame.  If you believe you are worthy, then do not hesitate -- try"
    public static final int KlingonDefeat_3 = 845;    // "again immediately!  There is always a chance."
    public static final int RomulanDefeat_1 = 846;    // "Your governance has been interesting, but you have not fared as well as those who have gone before you.  If you are truly"
    public static final int RomulanDefeat_2 = 847;    // "a loyal citizen of the Empire, you must immediately start a new game and attempt to better yourself.  Anything less is an"
    public static final int RomulanDefeat_3 = 848;    // "insult to the glory of the Romulan Star Empire."
    // Victorious 1410
    public static final int AllianceDominance = 30;   // "Your alliance has achieved dominance."
    public static final int Dominance = 247;          // "Every major government has admitted our galactic dominance. Shall we accept their surrender?"
    public static final int XDominance = 1408;        // "%s vanquished all their enemies and won the game."
  }

  public abstract class Random {
    // global events
    public static final int AlienArtifactsDiscovered = 39;  // "Artifacts have been discovered in the %1 system... acquired %d2 teraquads..."
    public static final int NamedBenefactor = 90;           // "A wealthy merchant from %1 has perished in a tragic accident and left his estate to %2. (%d3 Credits added to treasury)"
    public static final int ResearchSetback = 999;          // "Prominent %1 scientists in the %2 field were killed in a transport accident near the %3 system.  (%d4 teraquads of data lost)"
    public static final int HullDegradingVirus = 1124;      // "%s1 Ships in sector %2 infected with Hull Degrading Virus."
    public static final int SpaceDistortion = 1167;         // "A subspace anomaly near the edge of the galaxy is causing a subspace disruption.  Warp drive is inoperable throughout the galaxy."
    public static final int SpaceDistortionEnd_1 = 1166;    // "The subspace anomaly has receded.  All ships can travel normally."
    // space distortion end, reads "<1168>: <1169>" in summary
    public static final int SPACE_DISTORTION_END = 1168;    // confirmed by value change: "SPACE DISTORTION HAS ENDED"
    public static final int SpaceDistortionEnd_2 = 1169;    // confirmed by value change: "The subspace anomaly has disappeared.  Warp drives are back online."
    // system events
    public static final int CometStrike_1 = 171;            // "A comet has struck the %s planet of the %s system.  The planet is undergoing a climate change.  %d million casualties have been reported."
    public static final int CometStrike_2 = 176;            // "A comet has struck the %1 planet of the %2 system.  It is undergoing a climate change.  %d3 million casualties have been reported."
    public static final int Earthquake_1 = 254;             // "A series of earthquakes have devastated %1.  %d2 structures were destroyed in the catastrophe.  %d3 million people were killed."
    public static final int Earthquake_2 = 255;             // "A series of earthquakes have devastated %1.  A structure was destroyed in the catastrophe.  %d3 million people were killed."
    public static final int EnergyMeltdown_1 = 285;         // "A catastrophic meltdown has destroyed a power plant in the %s system."
    public static final int EnergyMeltdown_2 = 286;         // "A catastrophic meltdown has destroyed %d power plants in the %s system."
    public static final int ImmigrantsBoom_1 = 480;         // "Citizens flock to %s for its promise of long term prosperity."
    public static final int ImmigrantsBoom_2 = 1021;        // "Immigrants flock to %s; +%d million citizens."
    public static final int Nova = 875;                     // "All life in the %s system has been destroyed by the Nova."
    public static final int NegativePlanetShift = 911;      // "An unexpected climate shift has changed %1 to a %2."
    public static final int PositivePlanetShift = 913;      // "A rotational shift has changed %1 to a %2."
    public static final int PlanetShift = 1027;             // "Planet %d1 in system %2 has become a %3 planet."
    // guild strikes
    public static final int TRADE_GUILD_STRIKE_1 = 1268;    // "TRADE GUILD STRIKE SETTLED"
    public static final int TRADE_GUILD_STRIKE_2 = 1269;    // "TRADE GUILD STRIKE SETTLED"
    public static final int CreditStrike = 299;             // "Credit strike in %s system"
    public static final int CreditStrikeEnd = 1015;         // "Credit strike in %s system"
    public static final int GeneralStrike = 300;            // "General strike in %s system"
    public static final int GeneralStrikeEnd = 1020;        // "General strike in %s system"
    public static final int IndustryStrike = 302;           // "Industry strike in %s system"
    public static final int IndustryStrikeEnd = 1023;       // "Industry strike in %s system"
    public static final int IntelStrike = 303;              // "Intelligence strike in %s system"
    public static final int IntelStrikeEnd = 1024;          // "Intel strike in %s system"
    public static final int ResearchStrike = 306;           // "Research strike in %s system"
    public static final int ResearchStrikeEnd = 1037;       // "Research strike in %s system"
    public static final int TraideRouteStrike = 1265;       // "No trade routes will produce credits."
    public static final int TraideRouteStrikeEnd = 1267;    // "The strike is ended and trade will resume immediately."
    // other
    public static final int GomtuuHasBefriended = 419;      // "Gomtuu has befriended %1"
    public static final int LoyalCardassians = 314;         // "Loyal Cardassians contribute extra intelligence"
    public static final int PopulationIncreased = 1030;     // confirmed by value change: "Population increased by %d million in %s."
    public static final int AugmentedProduction = 1031;     // "Augmented Production in %s;  +%d industry."
    public static final int ProductionBonus = 1032;         // "%s1 Production Bonus: an additional %2 was constructed."
    public static final int EconomicProsperity = 1033;      // "Economic Prosperity in %s; +%d credits generated."
  }

  public abstract class Galactic {
    // EXPAND_TERRITORY 484                                     -> confirmed by value change: critics 679|680|681|682|683
    // INVASION_SUCCESS 486                                     -> confirmed by value change: details 521|506|505|493|508
    public static final int XConquered = 1044;                  // confirmed by value change: "%s1 conquered %2."
    // DAMAGED_IN_NEUTRON_STAR 489
    public static final int DamagedInNeutronStar = 512;         // "%s1 in sector %2 damaged by a neutron star."
    public static final int ShipsDestroyed = 516;               // "%d1 ships destroyed by a %2 in sector %3."
    // XLOST 490                                                -> confirmed by value change
    // @see Data.Galaxy.StellarObjects
    public static final int ShipLostInWormhole = 1128;          // confirmed by value change, details 1128: "One ship lost in a wormhole in sector %1."
    public static final int ShipsLostInWormhole = 1143;         // confirmed by value change, details 1143: "%d1 ships lost in a wormhole in sector %2."
    public static final int XYColon = 1144;                     // "%s1 %2:"
    // SYSTEM_LOST 491
    public static final int SystemTaken = 519;                  // "%s1 system taken by %2."
    // TERRAFORM_COMPLETED 492
    public static final int PlanetTerraformed = 522;            // "Planet terraformed in system %s."
    public static final int PlanetXTerraformed_1 = 308;         // "Planet %d terraformed in %s system"
    public static final int PlanetXTerraformed_2 = 1028;        // "Planet %d terraformed in system %s"
    public static final int TerraformingCompleted = 1047;       // confirmed by value change: "Terraforming completed in %s."
    // FORCES_SUFFER_DEFEAT 497                                 -> confirmed by value change: no summary, details 501|523|505|508
    // SIEGE_SYSTEM 520                                         -> confirmed by value change: critics 669|671|670|672|673, details 506|504|505|508|494|515
    // LIBERATION 604
    public static final int SystemLiberated = 518;              // "System %s liberated!"
    // Range 973
    public static final int RangeExtended = 974;                // %s1 range extended to %2 sectors.
    // Raid 1045                                                -> confirmed by value change: details 1045|496|499|498|510
    public static final int SuccessfullyRaid = 1042;            // confirmed by value change: "%s1 successfully raided %2 system."
    public static final int AllTradeIncomeLost = 500;           // opposite of 499 -> "All trade income from %s was lost."
    // SuccessfulLiberation 1188
    public static final int XLiberatedSystem = 605;             // "%s1 liberated the %2 system."

    // FURTHER EVENTS
    public static final int AffectedMorale = 689;               // "An event in the last turn has affected the morale of your citizens..."
    public static final int InconclusiveBattle = 59;            // "%s engaged in an inconclusive battle"
    public static final int ShipLost = 513;                     // "Ship destroyed by a %1 in sector %2."
    public static final int ShipsLost = 1142;                   // "Your forces have been eliminated."
    public static final int ShipsRegroupedAndRetreated = 1145;  // "Ships regrouped as a single task force and retreated to sector %s"
    // colonization
    public static final int SystemColonized_1 = 298;            // "System %s colonized"
    public static final int SystemColonized_2 = 1043;           // "System %s colonized"
    public static final int XColonized = 1034;                  // "%s1 colonized by %2."
  }

  public abstract class System {
    // POWER_OUTAGE 849                                             -> confirmed by value change: details 850
    public static final int PowerOutage_1 = 1025;                   // confirmed by value change: "Insufficient energy in %s system, structures have been shut down."
    // STARVATION 1175
    public static final int Starvation_1 = 307;                     // "Starvation in %s system"
    public static final int DeathByStarvation = 1176;               // "The %s system is not producing enough food.  The shortfall is causing a population decrease."
    // PLAGUE_STRIKE 908                                            -> confirmed by value change, details: 909
    public static final int PopulationDied = 1029;                  // confirmed by value change: "%d million population in %s system die tragically."
    // REBELLION 978
    public static final int RebellionPrefectFled = 979;             // "The Prefect has fled.", seems to refer Dukat, Prefect of Bajor
    public static final int RebellionCoup = 980;                    // "A bloody coup has erupted on %1 and overthrown the rightful %2 government. %s3."
    public static final int RebellionGovernorExecuted = 981;        // "The Governor has been executed."
    public static final int RebellionSystemIndependence = 982;      // "%s1 has declared its independence from %2."
    public static final int RebellionCommandantImprisioned = 983;   // "The Commandant has been imprisoned."
    public static final int RebellionRejoined = 985;                // "%s1 has broken free from %2 and rejoined %3."
    public static final int IndependenceDeclared = 1022;            // "%s1 system has declared independence from %2."
    // TRADE 1363
    public static final int NewTradeRoute = 1049;                   // "%s system has enough population for a new trade route."

    // FURTHER EVENTS with no header
    // starvation
    public static final int Starvation_2 = 1041;                    // confirmed by value change: "Starvation in %s system"
    // energy
    public static final int TransferredEnergy = 287;                // "%s transferred %d energy to %s"
    public static final int PowerOutage_2 = 304;                    // "Insufficient energy in %s system"
    // construction
    public static final int ShipBuilt = 1120;                       // "Ship %s built in %s"
    public static final int BoughtAndBuiltOn = 1007;                // "%s bought and built on %s"
    public static final int BoughtAndBuiltIn = 1008;                // "%s bought and built in %s"
    public static final int BuiltOn = 1009;                         // confirmed by value change: "%s built on %s"
    public static final int ConstructionCompleted = 1012;           // confirmed by value change: "%s1 construction completed in sector %2"
    public static final int XScrapped = 1039;                       // "%d %s scrapped on %s"
    public static final int XBuilt = 1040;                          // confirmed by value change: "%s1 built in %2."
    public static final int XUpgraded = 1052;                       // "%d %s upgraded on %s"
    public static final int ShipyardShutDown = 1146;                // "A shipyard has been shut down or destroyed.  Starship range may have been reduced, and some starships may be stranded as a result."
    public static final int Change = 1390;                          // "%d to %d %s", upgrade or planetary shift
    public static final int UpgradeComplete = 1401;                 // confirmed by value change: "Upgrade to %1 completed in %2 system."
    // morale
    public static final int UnrestIncrease_1 = 309;                 // "Unrest increase in %s system"
    public static final int UnrestIncrease_2 = 1051;                // "Unrest increase in %s system"
    public static final int XTurn = 1050;                           // "%d turn"
    public static final int HolidayCelebration = 1010;              // "Citizens in %s declare a holiday to celebrate."
    public static final int CivilWar = 1011;                        // "Civil war in %s system: %d million casualties"
    public static final int RiotDeaths = 1038;                      // "Riots and Protests in %s system have left %d million dead"
  }

  public abstract class Intel {
    // ASSASSINATION 43
    public static final int UnauthorizedAssassinationAttempt = 44;  // "Rogue Elements of %1 ... unauthorized assassination attempt on %2.  Relations with %3 have worsened."
    public static final int FoiledAssassinationAttempt = 45;        // "%s1 Counter-intelligence forces have foiled an assassination attempt by %2."
    // ExternalIntelligence 313
    // @see Data.Intel.FormalAgencies
    public static final int FrameActions = 534;                     // "Who should our agents frame for their actions against %s?"
    public static final int FramedActions = 535;                    // "Our agents have framed %s for their action against %s."
    // UrgentIntelligenceReport 485
    public static final int CardassianCounterMeasures_1 = 369;      // "The Obsidian Order can employ some mercenaries to attack %2 %1 operations.  Which mercenaries should we expend on this operation?"
    public static final int CardassianCounterMeasures_2 = 374;      // "The Obsidian Order can employ some mercenaries to attack %2 %1 operations.  Which mercenaries should we expend on this operation?"
    public static final int FerengiCounterMeasures_1 = 370;         // "We have an opportunity to acquire information about %2 %1 operations.  We can buy this through many channels.  Whose trail should we leave in our transaction?"
    public static final int FerengiCounterMeasures_2 = 375;         // "Success!  The Ferengi Espionage Agency has an opportunity to disrupt %1 operations in %4.  Which of our rivals should take the blame?"
    public static final int FederationCounterMeasures_1 = 371;      // "Starfleet Intelligence has a deep cover operative about to commit a class-one %1 operation.  Which empire should the operative implicate to %2 new services?"
    public static final int FederationCounterMeasures_2 = 376;      // "Starfleet Intelligence has a deep cover operative about to commit a class-one %1 operation.  Which empire should the operative implicate to %2 news services?"
    public static final int KlingonCounterMeasures_1 = 372;         // "The Klingon Security Force has successfully infiltrated a %2 %1 operation, and can frame another empire for the intrusion.  Which of our rivals should we implicate?"
    public static final int KlingonCounterMeasures_2 = 377;         // "The Klingon Security Force has successfully sabotaged a %2 %1 operation, and can frame another empire for the intrusion.  Which of our rivals should we implicate?"
    public static final int RomulanCounterMeasures_1 = 373;         // "TalShiar Operatives have an opportunity to implicate another power in our %1 operations against %4.  Whose incriminating evidence should we leave?"
    public static final int RomulanCounterMeasures_2 = 378;         // "TalShiar Operatives have an opportunity to implicate another power in our %1 operations against %4.  Whose incriminating evidence should we leave?"
    // CreditDeficit 976
    public static final int FundsMissing = 260;                     // "Treasury records on %1 indicate widespread embezzlement in %2.  The missing funds are estimated at %d3 credits."
    public static final int CreditsGained = 1013;                   // "%d credits gained."
    public static final int MarketCrash = 1014;                     // "Market Crash: %d credits lost."
    // RESEARCH_THEFT 1000
    public static final int ResearchTheft = 1001;                   // "%s Research facilities on %s have been compromised...  (%d teraquads of data stolen)"
    public static final int ResearchLost = 1035;                    // "%d %s research points lost."
    public static final int ResearchStolen = 1036;                  // "%d %s research points stolen."
    // TERRORISTS_STRIKE 1262
    public static final int TerrorXDestroyed = 1263;                // "Terrorists have destroyed a %1 in the %2 system."
    public static final int TerrorExplosiveDevice = 1264;           // "A multi-phasic explosive device was set off by terrorists in the %1 system.  The %2 was completely destroyed."
    public static final int Terrorism = 1048;                       // "Terrorism in %1 system! %s2 destroyed."
    // external
    public static final int OwnAgentsFramed = 1459;                 // confirmed by value change: "Our agents have been framed for actions against %s."
  }

  public abstract class Research {
    // ResearchBreakthrough 488
    public static final int ResearchBreakthrough = 511;             // "%s researchers achieve a historic breakthrough."
    // other
    public static final int ResearchDiscovery_1 = 1016;             // "%s discovered %s in level %d %s research"
    public static final int ResearchDiscovery_2 = 1017;             // confirmed by value change: "%s discovered %s"
    // likely science event labels
    public static final int Scientific = 1101;                      // "scientific"
  }

  // treaty offers are both used for events and in diplomacy screen
  public abstract class Diplomatic {
    // shared
    public static final int You = 1454;
    public static final int They = 1350;
    // AgreedPeaceTreaty offers 18
    public static final int JoinRequest = 993;                // "Your request to join"
    public static final int PeaceOffer = 1458;                // "You Propose Agreed Peace Treaty"
    // DefeatedPeaceTreaty 216
    public static final int XAllianceIgnored = 23;            // "%s1 ignored a alliance proposal from %2"
    public static final int XAllianceRejected = 25;           // confirmed by value change: "%s1 rejected an alliance proposal from %2"
    public static final int XAllianceWithdrawn = 31;          // "%s1 withdrew an alliance proposal sent to %2"
    public static final int XMembershipIgnored = 634;         // "%s1 ignored a membership proposal from %2"
    public static final int XMembershipRejected = 636;        // "%s1 rejected a membership proposal from %2"
    public static final int XMembershipWithdrawn = 640;       // "%s1 withdrew a membership proposal sent to %2"
    public static final int XAffiliationIgnored = 10;         // "%s1 ignored an affiliation proposal from %2"
    public static final int XAffiliationRejected = 12;        // "%s1 rejected an affiliation proposal from %2"
    public static final int XAffiliationWithdrawn = 16;       // "%s1 withdrew an affiliation proposal sent to %2"
    public static final int XFriendshipRejected = 386;        // confirmed by value change: "%s1 rejected a friendship proposal from %2"
    public static final int XFriendshipIgnored = 384;         // "%s1 ignored a friendship proposal from %2"
    public static final int XFriendshipWithdrawn = 389;       // "%s1 withdrew a friendship proposal sent to %2"
    public static final int XNonAggressionIgnored = 867;      // confirmed by value change: "%s1 ignored a non-aggression proposal from %2"
    public static final int XNonAggressionRejected = 869;     // confirmed by value change: "%s1 rejected a non-aggression treaty offer from %2"
    public static final int XNonAggressionWithdrawn = 871;    // "%s1 withdrew a non-aggression proposal sent to %2"
    public static final int XBribeIgnored = 103;              // "%s1 ignored a bribe from %2"
    public static final int XBribeRejected = 105;             // "%s1 rejected a bribe from %2"
    public static final int XBribeWithdrawn = 106;            // "%s1 withdrew a bribe sent to %2"
    public static final int XDemandIgnored = 223;             // "%s1 ignored a demand from %2"
    public static final int XDemandRejected = 225;            // confirmed by value change: "%s1 rejected a demand from %2"
    public static final int XDemandWithdrawn = 226;           // "%s1 withdrew a demand sent to %2"
    public static final int XGiftWithdrawn = 415;             // "%s1 withdrew a gift offer to %2"
    public static final int XWarPactIgnored = 1431;           // confirmed by value change: "%s1 ignored a %2 war pact proposal"
    public static final int XWarPactRejected = 1433;          // confirmed by value change: "%s1 rejected a %2 war pact proposal"
    public static final int XWarPactWithdrawn = 1434;         // "%s1 withdrew a war pact proposal sent to %2"
    // ALLIANCE_BROKEN 22
    public static final int XAllianceTerminated = 21;         // "%s1 terminated their alliance with %2"
    public static final int XMembershipTerminated =  /*633*/ LexDataIdx.Diplomacy.Proposals.XMembershipTermination; // "%s1 terminated their membership treaty with %2"
    // ALLIANCE_SIGNED 27
    // PEACE_SIGNED 487                                       -> confirmed by value change
    public static final int XAffiliationAccepted = 7;         // confirmed by value change: "%s1 accepted an affiliation proposal from %2."
    public static final int XFriendshipAccepted = 380;        // confirmed by value change: "%s1 accepted a friendship proposal from %2"
    public static final int XNonAggressionAccepted = 864;     // confirmed by value change: "%s1 accepted a non-aggression proposal from %2"
    // PEACE_SIGNED 901
    public static final int PeaceSigned_1 = 507;              // "%s1 have signed a peace treaty with %2"
    public static final int PeaceSigned_2 = 902;              // "%s1 has signed a peace treaty with %2"
    // CREDIT 191                                             -> confirmed by value change
    public static final int XCredits =               /*196*/ LexDataIdx.Diplomacy.TreatyTerms.XCredits;         // confirmed by value change: "%s1 give %d2 credits to %3"
    public static final int XCreditsPerTurn =        /*887*/ LexDataIdx.Diplomacy.TreatyTerms.XCreditsPerTurn;  // "%s1 give %d2 credits per turn to %3"
    public static final int XCreditsReceived = 648;           // "%s1 received %2 credits from %3"
    public static final int XFoodGift = 367;                  // "%s transferred %d food to %s"
    public static final int XDilithiumGift = 242;             // "%s transferred %d dilithium to %s"
    // MakeStatement 618 / NewStatement 823
    // Statement 1178                                         -> confirmed by value change: diplomacy event
    public static final int XStatementMade = 1179;            // confirmed by value change: "%s1 made a statement to %2."
    // WAR_DECLARED 1422                                      -> confirmed by value change: details 1421
    public static final int XWarDeclared =          /*1421*/ LexDataIdx.Diplomacy.Proposals.XWarDeclaration;    // confirmed by value change: "%s1 declare war against %2"

    // FURTHER EVENTS with no header
    // see Data.Diplomacy.Proposals
    // diplomatic offers
    public static final int XAllianceProposal =      /* 24*/ LexDataIdx.Diplomacy.Proposals.XAlliance;      // confirmed by value change: "%s1 offer an alliance proposal to %2", for majors
    public static final int XMembershipProposal =    /*635*/ LexDataIdx.Diplomacy.Proposals.XMembership;    // confirmed by value change: "%s1 offer a membership proposal to %2", for minors
    public static final int XAffiliationProposal =   /* 11*/ LexDataIdx.Diplomacy.Proposals.XAffiliation;   // confirmed by value change: "%s1 offer an affiliation proposal to %2"
    public static final int XFriendshipProposal =    /*385*/ LexDataIdx.Diplomacy.Proposals.XFriendship;    // confirmed by value change: "%s1 offer a friendship proposal to %2"
    public static final int XNonAggressionProposal = /*868*/ LexDataIdx.Diplomacy.Proposals.XNonAggression; // confirmed by value change: "%s1 offer a non-aggression proposal to %2"
    public static final int XGift =                  /*413*/ LexDataIdx.Diplomacy.Proposals.XGift;          // confirmed by value change: "%s1 presented a gift to %2"
    public static final int XBribe =                 /*104*/ LexDataIdx.Diplomacy.Proposals.XBribe;         // confirmed by value change: "%s1 offer a bribe to %2", for minors
    public static final int XDemand =                /*224*/ LexDataIdx.Diplomacy.Proposals.XDemand;        // confirmed by value change: "%s1 issue a demand to %2", for majors
    public static final int XWarPactProposal =      /*1432*/ LexDataIdx.Diplomacy.Proposals.XWarPact;       // confirmed by value change: "%s1 propose a war pact to %2", for majors
    // peace signed
    public static final int XDemandAccepted = 222;            // confirmed by value change: "%s1 accepted a demand from %2"
    public static final int XBribeAccepted = 102;             // "%s1 accepted a bribe from %2"
    public static final int XWarPactAccepted = 1430;          // confirmed by value change: "%s1 accepted a %2 war pact"
    public static final int TaskForcesDeployed = 227;
    public static final int XCededTerritories = 1260;         // "%s1 ceded disputed territories to %2."
    public static final int XCedeTerritories = 1261;          // confirmed by value change: "%s1 cede disputed territories to %2."
    // peace terminated
    public static final int FriendshipTerminated = 1351;      // "They Break Trade Treaty"
    public static final int XAffiliationTerminated = 8;       // "%s1 terminated their affiliation treaty with %2"
    public static final int XAffiliationExpired = 9;          // "Affiliation treaty between %1 and %2 expired"
    public static final int XFriendshipTerminated = 382;      // confirmed by value change: "%s1 terminated their friendship treaty with %2"
    public static final int XFriendshipExpired = 383;         // "Friendship treaty between %1 and %2 expired"
    public static final int XNonAggressionTerminated = 865;   // confirmed by value change: "%s1 terminated their non-aggression treaty with %2"
    public static final int XNonAggressionExpired = 866;      // "Non-aggression treaty between %1 and %2 expired"
    // war
    public static final int Declare = 213;
    public static final int DeclareWar = 214;
    public static final int War = 1423;                       // "War has been declared between two major powers..."
    // credits
    public static final int XCreditsTransferred = 192;        // confirmed by value change: "%s transferred %d credits to %s"
    // other party treaties
    public static final int XAllianceAccepted = 20;           // confirmed by value change: "%s1 accepted an alliance proposal from %2"
    // race encounters
    public static final int RaceEncountered = 967;
    public static final int RaceXEncountered = 1026;          // confirmed by value change: "You encountered %1 in sector %2."
    public static final int XDiscoveredYourSystem = 1141;     // "%s discover one of your systems"
  }

  public abstract class Critics {
    public abstract class Card {
      public static final int Bombardment = 669;              // "The bombardment of the %s system has silenced several dissidents."
      public static final int Betrayment = 674;               // "Many Cardassian citizens were shocked to hear that we have betrayed %s."
      public static final int Colonization = 679;             // confirmed by value change: EXPAND_TERRITORY 484, details 495|813 -> "Citizens raised a glass of kanar to celebrate the colonization of %s."
      public static final int War = 684;                      // "Our people strongly favor the war against %s."
      public static final int FallenEmpire = 690;             // "All Cardassians rejoice at the collapse of %s."
      public static final int LiberationCelebration = 695;    // "The citizens are pleased that %s have been set free."
      public static final int LiberationAppreciation = 700;   // "The liberation of %s has raised the spirits of all loyal Cardassians."
      public static final int Defeat = 710;                   // "The defeat in sector %s has led citizens to openly criticize our policies."
      public static final int DefeatMorale = 720;             // confirmed by value change: "Cardassian citizens were disappointed to hear of our defeat in sector %s."
      public static final int DefeatSupportLoss = 725;        // "Cardassian laborers are disenchanted by the defeat in sector %s."
      public static final int HomeSystemLoss = 705;           // "The loss of Cardassia Prime has led to widespread disillusionment and several suicides."
      public static final int SystemLoss = 715;               // "Cardassian citizens are disheartened by the loss of %s to enemy forces."
      public static final int SystemLossMorale = 730;         // "Cardassian citizens are disheartened by the loss of the %s system."
      public static final int Rebels = 736;                   // "Many Cardassian citizens appear to sympathize with the rebels of %s."
      public static final int RejectedPeace = 741;            // confirmed by value change: "Our citizens are disappointed to hear that there will be no peace with %s."
      public static final int Alliance = 746;                 // "The public response to our alliance with %s is very favorable."
      public static final int Member = 751;                   // "%s have been received warmly by our citizens."
      public static final int Peace = 756;                    // confirmed by value change: PEACE_SIGNED_1 487 -> "News of our peace with %s has been received warmly by our citizens."
      public static final int Subjugation = 769;              // confirmed by value change: "Cardassian citizens are glad to hear that the %s system is under control."
      public static final int DeclaredWar = 774;              // "The chance to prove ourselves against %s has boosted enlistments."
      public static final int Victory = 779;                  // "Cardassian citizens are deeply inspired by the great victory in sector %s."
      public static final int VictoryMoraleIncrease = 784;    // "Cardassian citizens were mildly pleased to hear of the victory in sector %s."
      public static final int VictorySupportGain = 789;       // "The people are clearly excited by our victory in sector %s."
    }

    public abstract class Fed {
      public static final int Bombardment = 671;              // "The bombing of %s has eroded popular support for our military."
      public static final int Betrayment = 676;               // confirmed by value change: "Many citizens feel that in betraying %s, we have betrayed our own people."
      public static final int Colonization = 681;             // confirmed by value change: EXPAND_TERRITORY 484, details 495|813 -> "Citizens throughout the Federation celebrate the colonization of %s."
      public static final int War = 686;                      // confirmed by value change: "Several popular editorials have strongly criticized the war against %s."
      public static final int FallenEmpire = 692;             // "The fall of %s is celebrated enthusiastically by many Federation citizens."
      public static final int LiberationCelebration = 697;    // "Celebrations are planned to honor the heroes who liberated %s."
      public static final int LiberationAppreciation = 702;   // "The liberation of the %s system has generated new support for the war."
      public static final int Defeat = 712;                   // "Anti-war demonstrations are more frequent since the defeat in sector %s."
      public static final int DefeatMorale = 722;             // "Federation citizens are mildly displeased by our defeat in sector %s."
      public static final int DefeatSupportLoss = 727;        // "Our military leaders have been ridiculed since the defeat in sector %s."
      public static final int HomeSystemLoss = 707;           // "Protesters are demanding an explanation for our failure to protect the Sol system."
      public static final int SystemLoss = 717;               // "Citizens are outraged by our inability to properly defend %s."
      public static final int SystemLossMorale = 732;         // "The people are not pleased by our failure to control %s."
      public static final int Rebels = 738;                   // "The rebels of %s have a growing base of sympathy on other Federation worlds."
      public static final int RejectedPeace = 743;            // confirmed by value change: "Activists have strongly condemned our refusal of peace with %s."
      public static final int Alliance = 748;                 // confirmed by value change: "Popular support for our alliance with %s is truly overwhelming."
      public static final int Member = 753;                   // "%s have been welcomed into the Federation with open arms."
      public static final int Peace = 758;                    // confirmed by value change: PEACE_SIGNED_1 487 -> "Our treaty with %s has quelled the fears of many concerned citizens."
      public static final int Subjugation = 771;              // confirmed by value change: "The brutal subjugation of the %s system has triggered widespread protests."
      public static final int DeclaredWar = 776;              // "The war against %s has increased support for our military policies."
      public static final int Victory = 781;                  // confirmed by value change: "The victory in sector %s has raised the enthusiasm of Federation citizens."
      public static final int VictoryMoraleIncrease = 786;    // "Spirits on the home front were raised somewhat by the victory in sector %s."
      public static final int VictorySupportGain = 791;       // confirmed by value change: "The victory in sector %s is the lead story in most civilian news services."
    }

    public abstract class Ferg {
      public static final int Bombardment = 670;              // confirmed by value change: "The Exchange posted strong gains today in response to the bombing of %s."
      public static final int Betrayment = 675;               // "Our breach of contract with %s will be costly."
      public static final int Colonization = 680;             // EXPAND_TERRITORY 484, details 495|813 -> "Investors are enthusiastic about opportunities in the new %s colony."
      public static final int War = 685;                      // "Investors are reacting positively to the news of war against %s."
      public static final int FallenEmpire = 691;             // "The fall of %s lifted the Exchange to record highs."
      public static final int LiberationCelebration = 696;    // "Financiers are pleased that %s can resume trade."
      public static final int LiberationAppreciation = 701;   // "The restoration of the %s market is good news to all Ferengi exporters."
      public static final int Defeat = 711;                   // "Military certificates plunged in value following the defeat in sector %s."
      public static final int DefeatMorale = 721;             // confirmed by value change: "The defeat in sector %s has raised the concerns of many businessmen."
      public static final int DefeatSupportLoss = 726;        // confirmed by value change: "The defeat in sector %s has triggered a drop in consumer confidence."
      public static final int HomeSystemLoss = 706;           // "The loss of Ferenginar has pushed the Exchange down to an all-time low."
      public static final int SystemLoss = 716;               // "The loss of %s from our consumer base is a disaster for Ferengi exporters."
      public static final int SystemLossMorale = 731;         // "Consumer confidence dropped slightly on reports that we have lost %s."
      public static final int Rebels = 737;                   // "The rebellion in sector %s caused a serious drop in the Exchange."
      public static final int RejectedPeace = 742;            // confirmed by value change: "Investors were disappointed that there will be no peace with %s."
      public static final int Alliance = 747;                 // "The Exchange hit a record high as brokers celebrated our alliance with %s."
      public static final int Member = 752;                   // "Investors are looking forward to new business opportunities among %s."
      public static final int Peace = 757;                    // PEACE_SIGNED_1 487 -> "Entrepreneurs are looking forward to new opportunities among %s."
      public static final int Subjugation = 770;              // confirmed by value change: "Investors are relieved to hear that the %s system is now a stable market."
      public static final int DeclaredWar = 775;              // "Analysts believe the war against %s will be very profitable."
      public static final int Victory = 780;                  // "Investors were impressed by the great military success in sector %s."
      public static final int VictoryMoraleIncrease = 785;    // "The victory in sector %s led to a slight increase in consumer confidence."
      public static final int VictorySupportGain = 790;       // "Brokers are buying stock in our military after the victory in sector %s."
    }

    public abstract class Kling {
      public static final int Bombardment = 672;              // confirmed by value change: "It is felt that the bombardment of %s proves that we are still Klingon."
      public static final int Betrayment = 677;               // "Our breach of honor with %s has not been well received."
      public static final int Colonization = 682;             // EXPAND_TERRITORY 484, details 495|813 -> "The %s system will give our people some long-overdue breathing room."
      public static final int War = 687;                      // "All true warriors look forward to the war against %s."
      public static final int FallenEmpire = 693;             // "Warriors celebrated the fall of %s with bloodwine and song."
      public static final int LiberationCelebration = 698;    // "Our warriors look with pride on the liberation of %s."
      public static final int LiberationAppreciation = 703;   // "Many warriors will join the Order of the Bat'leth for their actions at %s."
      public static final int Defeat = 713;                   // "The defeat in sector %s has raised deep concerns among many warriors."
      public static final int DefeatMorale = 723;             // "The defeat in sector %s has lowered the morale of our people."
      public static final int DefeatSupportLoss = 728;        // "Warriors are demanding that the deaths in sector %s be avenged."
      public static final int HomeSystemLoss = 708;           // "Many warriors have chosen honorable suicide over life without the homeworld."
      public static final int SystemLoss = 718;               // "In failing to defend %s, we have lost a great deal of honor."
      public static final int SystemLossMorale = 733;         // "Every Klingon warrior is shamed by our failure to defend the %s system."
      public static final int Rebels = 739;                   // "Many warriors support the %s rebels more strongly than the High Council."
      public static final int RejectedPeace = 744;            // "Our people are pleased to hear that peace has been averted."
      public static final int Alliance = 749;                 // confirmed by value change: "The people believe we have found worthy allies in %s."
      public static final int Member = 754;                   // "Warriors are pleased that %s will now be considered their brothers."
      public static final int Peace = 759;                    // PEACE_SIGNED_1 487 -> "The people are disappointed that there will be no battles with %s."
      public static final int Subjugation = 772;              // confirmed by value change: "Our warriors revel in the glory of our victory in the %s system."
      public static final int DeclaredWar = 777;              // "All true warriors look forward eagerly to the war against %s."
      public static final int Victory = 782;                  // "Warriors are deeply inspired by the great victory in sector %s."
      public static final int VictoryMoraleIncrease = 787;    // "The victory in sector %s has raised the spirits of our people."
      public static final int VictorySupportGain = 792;       // confirmed by value change: "The victory in sector %s has renewed our faith in the teachings of Kahless."
    }

    public abstract class Rom {
      public static final int Bombardment = 673;              // confirmed by value change: "Many vocal dissidents have been silenced by the bombardment of %s."
      public static final int Betrayment = 678;               // confirmed by value change: "Some restless citizens have questioned our breach of treaty with %s."
      public static final int Colonization = 683;             // confirmed by value change: EXPAND_TERRITORY 484, details 495|813 -> "Citizens are glad to hear that the %s system is now part of our Empire."
      public static final int War = 688;                      // confirmed by value change: "The people are pleased that we will settle accounts with %s."
      public static final int FallenEmpire = 694;             // "An Imperial holiday commemorates the fall of %s."
      public static final int LiberationCelebration = 699;    // "The liberation of %s has raised the spirits of many concerned citizens."
      public static final int LiberationAppreciation = 704;   // "The rescue of %s has become a popular cause for celebration."
      public static final int Defeat = 714;                   // "Citizens have openly condemned our failure in sector %s."
      public static final int DefeatMorale = 724;             // confirmed by value change: "Citizens were displeased to hear of the defeat in sector %s."
      public static final int DefeatSupportLoss = 729;        // confirmed by value change: "The disaster in sector %s has eroded popular support for the military."
      public static final int HomeSystemLoss = 709;           // "The people believe that the loss of Romulus is a clear sign of governmental incompetence."
      public static final int SystemLoss = 719;               // confirmed by value change: "The people are strongly displeased by our failure to protect %s."
      public static final int SystemLossMorale = 734;         // confirmed by value change: "Many citizens have expressed disappointment at the loss of the %s system."
      public static final int Rebels = 740;                   // "There appears to be some popular sympathy for the rebels of %s."
      public static final int RejectedPeace = 745;            // confirmed by value change: "The people are pleased that our status with %s will not change."
      public static final int Alliance = 750;                 // "The people believe that our alliance with %s will be our salvation."
      public static final int Member = 755;                   // "The people seem happy to accept %s as members of our great Empire."
      public static final int Peace = 760;                    // confirmed by value change: PEACE_SIGNED_1 487 -> "The people seem to approve of our arrangement with %s."
      public static final int Subjugation = 773;              // confirmed by value change: "Citizens are relieved that there will be no more trouble in %s."
      public static final int DeclaredWar = 778;              // confirmed by value change: "The people are pleased that we will settle accounts with %s."
      public static final int Victory = 783;                  // confirmed by value change: "The great victory in sector %s was welcome news to all loyal Romulans."
      public static final int VictoryMoraleIncrease = 788;    // "The people are pleased by our military success in sector %s."
      public static final int VictorySupportGain = 793;       // confirmed by value change: "Popular support for the military was reinforced by the victory in sector %s."
    }
  }

  public abstract class Combat {
    // defeat
    public static final int XDefeated = 1140;                 // confirmed by value change: "%s defeated %s"
    public static final int XDestroyed = 231;                 // confirmed by value change: "%s destroyed"
    public static final int XList = /*800*/ Encounter.XList;  // confirmed by value change
  }
}
