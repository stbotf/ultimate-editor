package ue.edit.res.stbof.files.dic.idx;

/**
 * Data labels used by combo boxes and value descriptions.
 *
 * This class holds constants to index the lexicon.dic file.
 * The labels and grouping might not be accurate, but bring some order to the wide spread values.
 * In UE they might be used to index fixed game values by name rather than by id.
 */
public abstract class LexDataIdx {
  public abstract class Shared {
    public static final int OFF = 880;          // confirmed by value change: game setup
    public static final int Medium = 627;       // confirmed by value change: game setup (galaxy size) and space craft (ship range)
    public static final int MANUAL = 621;       // confirmed by value change: game options (tactical timer & system build)
    public static final int AUTOMATIC = 52;     // confirmed by value change: game options (tactical timer & system build)
    public static final int None = 872;         // confirmed by value change: game options & diplomacy
    public static final int Unknown = 1393;     // confirmed by value change: intel, diplomacy, fleet info and summary screen (relationships & build task)
    public static final int NotAvailable = 798; // confirmed by value change: intel & diplomacy -> "n/a"
    public static final int Neutral = 809;      // confirmed by value change: diplomacy relationship & reputation
    public static final int Invalid = 553;      // confirmed by value change: SpaceCraft category descriptions
  }

  public abstract class GameSettings {
    public abstract class StartCondition {
      public static final int BEGINNING = 88;                       // confirmed by value change
      public static final int EARLY = 252;                          // confirmed by value change
      public static final int DEVELOPED = 234;                      // confirmed by value change
      public static final int EXPANDED = 312;                       // confirmed by value change
      public static final int ADVANCED = 4;                         // confirmed by value change
    }

    // load savegame
    public abstract class PlayerRace {
      public static final int Cardassians = 125;                    // confirmed by value change
      public static final int Federation = 319;                     // confirmed by value change
      public static final int Ferengi = 323;                        // confirmed by value change
      public static final int Klingons = 563;                       // confirmed by value change
      public static final int Romulans = 1061;                      // confirmed by value change
    }

    public abstract class MinorRaces {
      public static final int None = /*872*/ Shared.None;           // confirmed by value change
      public static final int FEW = 358;                            // confirmed by value change
      public static final int SOME = 1161;                          // confirmed by value change
      public static final int MANY = 622;                           // confirmed by value change
    }

    public abstract class Difficulty {
      public static final int SIMPLE = 1150;                        // confirmed by value change
      public static final int EASY = 256;                           // confirmed by value change
      public static final int NORMAL = 873;                         // confirmed by value change
      public static final int HARD = 433;                           // confirmed by value change
      public static final int IMPOSSIBLE = 482;                     // confirmed by value change
    }

    public abstract class Timer {
      public static final int None = /*872*/ Shared.None;           // confirmed by value change
    }

    public abstract class RandomEvents {
      public static final int OFF = /*880*/ Shared.OFF;             // confirmed by value change
      public static final int ON = 882;                             // confirmed by value change
    }

    public abstract class TacticalCombat {
      public static final int MANUAL = /*621*/ Shared.MANUAL;       // confirmed by value change
      public static final int AUTOMATIC = /*52*/ Shared.AUTOMATIC;  // confirmed by value change
    }

    public abstract class VictoryCondition {
      public static final int DOMINATION = 248;                     // confirmed by value change
      public static final int TEAM_PLAY = 1253;                     // confirmed by value change
      public static final int VENDETTA = 1407;                      // confirmed by value change
    }

    // save game labels
    public abstract class GalaxyShape {
      public static final int Irregular = 555;                      // confirmed by value change
      public static final int Elliptical = 264;                     // confirmed by value change
      public static final int Ring = 1056;                          // confirmed by value change
      public static final int Spiral = 1172;                        // confirmed by value change
    }

    // save game labels
    public abstract class GalaxySize {
      public static final int Large = 596;                          // confirmed by value change
      public static final int Medium = /*627*/ Shared.Medium;       // confirmed by value change
      public static final int Small = 1158;                         // confirmed by value change
    }
  }

  public abstract class Galaxy {
    // galaxy map tooltips, system panel & events
    // stellar object names are stored and loaded from savegames
    public abstract class StellarObjects {
      public static final int ProtoStar = 956;
      public static final int BlueStar = 96;      // confirmed by value change: galaxy map
      public static final int GreenStar = 420;    // confirmed by value change: galaxy map
      public static final int YellowStar = 1452;  // confirmed by value change: galaxy map
      public static final int OrangeStar = 889;   // confirmed by value change: galaxy map
      public static final int WhiteStar = 1446;   // confirmed by value change: galaxy map
      public static final int NeutronStar = 810;  // confirmed by value change: galaxy map
      public static final int RedGiant = 987;     // confirmed by value change: galaxy map
      public static final int BlackHole = 94;     // confirmed by value change: galaxy map
      public static final int BlackDwarf = 93;
      public static final int BrownDwarf = 107;
      public static final int WhiteDwarf = 1445;
      public static final int Nebula = 802;       // confirmed by value change: system panel
      public static final int Wormhole = 1447;    // confirmed by value change: galaxy map & event 490
      public static final int RadioPulsar = 968;  // confirmed by value change: galaxy map
      public static final int XRayPulsar = 1450;  // confirmed by value change: galaxy map
    }

    // galaxy map (sector header & tooltips), events & combat encounter
    // note, the german translation has a pronoun 'of the'
    // english: "Cardassian", "Federation", "Ferengi", "Klingon", "Romulan", "Minor Race"
    // german:  "der Cardassianer", "der Föderation", "der Ferengi", "der Romulaner", "der Kleine Rassen"
    public abstract class Ownership {
      public static final int Cardassian = 122;                   // confirmed by value change
      public static final int Federation = 320;                   // confirmed by value change
      public static final int Ferengi = 324;                      // confirmed by value change
      public static final int Klingon = 560;                      // confirmed by value change
      public static final int Romulan = 1058;                     // confirmed by value change
      public static final int MinorRace = 647;
      public static final int Unknown = /*1393*/ Shared.Unknown;  // confirmed by value change: unknown ship / unknown task force
    }
  }

  public abstract class System {
    // system screen (header), system panel (top left), galaxy map (system name & tooltips), combat (encounter & battle) & events (raid,...)
    // For system names it is only generated once on new game start. Thereafter it is stored and read from the save games.
    public abstract class HomeWorld {
      public static final int Cardassia = 121;            // confirmed by value change
      public static final int Sol = 1160;                 // confirmed by value change
      public static final int Ferenginar = 357;           // confirmed by value change
      public static final int QoNos = 958;                // confirmed by value change
      public static final int Romulus = 1092;             // confirmed by value change
    }

    // planet details
    public abstract class PlanetTypes {
      public static final int Arctic = 37;                // confirmed by value change
      public static final int Asteroid = 47;
      public static final int Barren = 55;                // confirmed by value change, also for the borg
      public static final int Desert = 229;               // confirmed by value change
      public static final int Desert_2 = 1409;
      public static final int Jungle = 558;               // confirmed by value change
      public static final int Oceanic = 878;              // confirmed by value change
      public static final int Terran = 1258;              // confirmed by value change
      public static final int Volcanic = 1414;            // confirmed by value change
      public static final int GasGiant = 410;             // confirmed by value change
    }

    // planet bonus types
    public abstract class BonusType {
      public static final int Energy = /*279*/ System.Production.Energy;  // confirmed by value change
      public static final int Food = /*365*/ System.Production.Food;      // confirmed by value change
    }

    // planet bonus levels
    public abstract class BonusLevel {
      public static final int Little = 607;
      public static final int Medium = Shared.Medium;
      public static final int Large = GameSettings.GalaxySize.Large;
    }

    // planet population race
    public abstract class Population {
      // galactic system panel
      public static final int HUMANS = 445;           // confirmed by value change: head line
      public static final int Uninhabitable = 1387;   // confirmed by value change: planet details
      public static final int Uninhabited = 1388;     // confirmed by value change: head line
      // other, that might be used for events
      public static final int People = 903;
    }

    // system info
    public abstract class Production {
      public static final int Credits = 193;        // confirmed by value change: also galaxy map economics
      public static final int Food = 365;           // confirmed by value change: also research, planet bonus tooltip & summary
      public static final int Industry = 528;       // confirmed by value change: also galaxy map & build list
      public static final int Energy = 279;         // confirmed by value change: also research & planet bonus tooltip
      public static final int Intelligence = 548;   // confirmed by value change
      public static final int Research = 996;       // confirmed by value change
    }

    // system & empire morale
    public abstract class Morale {
      public static final int Fanatic = 765;        // confirmed by value change: summary
      public static final int Loyal = 766;          // confirmed by value change: summary
      public static final int Pleased = 767;        // confirmed by value change: summary
      public static final int Content = 762;        // confirmed by value change: summary
      public static final int Apathetic = 761;      // confirmed by value change: summary
      public static final int Disgruntled = 764;    // confirmed by value change: summary
      public static final int Defiant = 763;
      public static final int Rebellious = 768;
    }

    public abstract class Restrictions {
      // count
      public static final int OnePerEmpire = 884;           // confirmed by value change
      public static final int OnePerSystem = 885;           // confirmed by value change
      // system
      public static final int AnySystem = 35;
      public static final int HomeSystem = 437;             // confirmed by value change
      public static final int ConqueredHomeSystem = 184;
      public static final int NativeMemberSystem = 801;     // confirmed by value change
      public static final int NonNativeMemberSystem = 863;  // confirmed by value change
      public static final int AffiliatedSystem = 6;
      public static final int IndependentMinorSystem = 527;
      public static final int RebelSystem = 977;
      public static final int SubjugatedSystem = 1187;
      public static final int EmptySystem = 277;
      // planet
      public static final int ArcticPlanet = 38;            // confirmed by value change
      public static final int BarrenPlanet = 56;            // confirmed by value change
      public static final int DesertPlanet = 230;
      public static final int JunglePlanet = 559;
      public static final int OceanicPlanet = 879;          // confirmed by value change
      public static final int TerranPlanet = 1259;
      public static final int VolcanicPlanet = 1415;
      // resources
      public static final int Dilithium = 239;              // confirmed by value change
      public static final int AsteroidBeltDilithium = 48;
    }
  }

  public abstract class Intel {
    // likely used by FramedActions 534|535
    public abstract class FormalAgencies {
      public static final int TheObsidianOrder = 536;
      public static final int TheFerengiEspionageAgency = 537;
      public static final int StarfleetIntelligence = 538;
      public static final int TheKlingonSecurityForce = 539;
      public static final int TheTalShiar = 540;
    }

    // possibly used by FramedActions 534|535
    public abstract class Agencies {
      public static final int ObsidianOrder = 541;
      public static final int FerengiIntelligence = 542;
      public static final int StarfleetIntelligence = 543;
      public static final int Section31 = 544;
      public static final int KlingonInternalSecurity = 545;
      public static final int TalShiar = 546;
    }
  }

  public abstract class Research {
    public abstract class Field {
      public static final int Biotech = 92;                               // confirmed by value change: research database, not management
      public static final int Computer = 179;                             // confirmed by value change: research database, not management
      public static final int Construction = 185;                         // confirmed by value change: research database, not management
      public static final int Energy = /*279*/ System.Production.Energy;  // confirmed by value change: research database, not management
      public static final int Propulsion = 955;                           // confirmed by value change: research database, not management
      public static final int Weapons = 1439;                             // confirmed by value change: research database, not management
      public static final int Sociology = 1159;                           // "sociology"
      public static final int FutureBiotech = 390;
      public static final int FutureComputerTech = 392;
      public static final int FutureConstructionTech = 395;
      public static final int FutureEnergyTech = 397;
      public static final int FuturePropulsionTech = 399;
      public static final int FutureTech = 400;
      public static final int FutureWeaponTech = 403;
    }
  }

  public abstract class Diplomacy {
    // diplomacy proposal details & summary events
    // seems to depend on which race is being played
    // Menu.DiplomacyScreen.Propoals: 11, 24, 104, 224, 385, 413, 635, 868, 1421, 1432  // confirmed by value change
    public abstract class FormalRaces {
      public static final int TheAcamarians_1 = 1270;   // confirmed by value change: used for events 1026
      public static final int TheAcamarians_2 = 1271;   // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheAndorians_1 = 1272;
      public static final int TheAndorians_2 = 1273;    // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheAngosians_1 = 1274;    // confirmed by value change: used for events 413
      public static final int TheAngosians_2 = 1275;    // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheAntedeans_1 = 1276;    // confirmed by value change: used for events 382
      public static final int TheAntedeans_2 = 1277;    // confirmed by value change: used for proposals 224, 385, 868, 1421
      public static final int TheAnticans_1 = 1278;
      public static final int TheAnticans_2 = 1279;     // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheBajorans_1 = 1280;     // confirmed by value change: used for events 1179
      public static final int TheBajorans_2 = 1281;
      public static final int TheBandi_1 = 1282;
      public static final int TheBandi_2 = 1283;        // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheBenzites_1 = 1284;
      public static final int TheBenzites_2 = 1285;     // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheBetazoids_1 = 1286;    // confirmed by value change: used for events 382
      public static final int TheBetazoids_2 = 1287;
      public static final int TheBolians_1 = 1288;
      public static final int TheBolians_2 = 1289;      // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheBynars_1 = 1290;
      public static final int TheBynars_2 = 1291;       // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheCaldonians_1 = 1292;
      public static final int TheCaldonians_2 = 1293;   // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheCardassians_1 = 1296;  // confirmed by value change: used for proposals 11 & events 7, 13, 224, 386, 678, 745, 760, 864, 868, 870, 1026, 1179
      public static final int TheCardassians_2 = 1297;  // confirmed by value change: used for proposals 224, 382, 385, 688, 758, 865, 868, 869, 1421, 1422
      public static final int TheChalnoth_1 = 1298;
      public static final int TheChalnoth_2 = 1299;
      public static final int TheEdo_1 = 1300;          // confirmed by value change: used for events 385
      public static final int TheEdo_2 = 1301;          // confirmed by value change: used for events 380
      public static final int TheFederation_1 = 1304;   // confirmed by value change: used for proposals 11, 1421 & events 224, 225, 386, 487, 749, 760, 778, 864, 868, 1026, 1196, 1421
      public static final int TheFederation_2 = 1305;   // confirmed by value change: used for proposals 224, 385, 868 & events 20, 222, 380, 385, 487, 521, 688, 864, 865, 866, 868, 869, 870, 1179, 1196, 1421
      public static final int TheFerengi_1 = 1308;      // confirmed by value change: used for proposals 11 & events 386, 487, 745, 864, 867, 870, 1179, 1422, 1432, 1459
      public static final int TheFerengi_2 = 1309;      // confirmed by value change: used for proposals 224, 225, 385, 866, 868 & events 24, 380, 1179
      public static final int TheKlingons_1 = 1312;     // confirmed by value change: used for proposals 224, 385, 868 & events 20, 24, 222, 224, 413, 748, 760, 1179, 1459
      public static final int TheKlingons_2 = 1313;     // confirmed by value change: used for proposals 11 & events 864, 870, 1179
      public static final int TheKtarians_1 = 1314;
      public static final int TheKtarians_2 = 1315;     // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheMalcorians_1 = 1316;
      public static final int TheMalcorians_2 = 1317;
      public static final int TheMintakans_1 = 1318;
      public static final int TheMintakans_2 = 1319;
      public static final int TheMizarians_1 = 1320;
      public static final int TheMizarians_2 = 1321;
      public static final int TheNausicaans_1 = 1322;
      public static final int TheNausicaans_2 = 1323;   // confirmed by value change: used for proposals 224, 385, 868
      public static final int ThePakleds_1 = 1324;      // confirmed by value change: used for events 688, 1026
      public static final int ThePakleds_2 = 1325;      // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheRomulans_1 = 1328;     // confirmed by value change: used for proposals 11 & events 224, 380, 385, 676, 686, 741, 756, 864, 865, 868, 869, 1026, 1421, 1432
      public static final int TheRomulans_2 = 1329;     // confirmed by value change: used for proposals 224, 385, 868, 1421 & events 7, 13, 382, 385, 386, 487, 521, 864, 867, 868, 870, 1179, 1421
      public static final int TheSelay_1 = 1330;        // confirmed by value change: used for events 380, 1026
      public static final int TheSelay_2 = 1331;        // confirmed by value change: used for proposals 224, 385, 868, 1421
      public static final int TheSheliak_1 = 1332;      // confirmed by value change: used for events 1026
      public static final int TheSheliak_2 = 1333;      // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheTakarans_1 = 1334;
      public static final int TheTakarans_2 = 1335;     // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheTalarians_1 = 1336;
      public static final int TheTalarians_2 = 1337;
      public static final int TheTamarians_1 = 1338;    // confirmed by value change: used for events 1026
      public static final int TheTamarians_2 = 1339;    // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheTrill_1 = 1340;
      public static final int TheTrill_2 = 1341;        // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheUllians_1 = 1342;
      public static final int TheUllians_2 = 1343;
      public static final int TheVulcans_1 = 1344;
      public static final int TheVulcans_2 = 1345;      // confirmed by value change: used for proposals 224, 385, 868
      public static final int TheYridians_1 = 1346;
      public static final int TheYridians_2 = 1347;
      public static final int TheZakdorn_1 = 1348;
      public static final int TheZakdorn_2 = 1349;      // confirmed by value change: used for proposals 224, 385, 868
    }

    // diplomacy & summary empire relationships
    public abstract class Relationship {
      public static final int Alliance = 19;                              // confirmed by value change
      public static final int Affiliated = 5;                             // confirmed by value change
      public static final int Member = 632;                               // confirmed by value change
      public static final int PEACE = 900;                                // confirmed by value change
      public static final int Friendly = 379;                             // confirmed by value change
      public static final int Neutral = /*809*/ Shared.Neutral;           // confirmed by value change
      public static final int WAR = 1420;                                 // confirmed by value change
      public static final int Subjugated = 1186;                          // confirmed by value change
      public static final int Unknown = /*1393*/ Shared.Unknown;          // confirmed by value change
      public static final int NotAvailable = /*798*/ Shared.NotAvailable; // confirmed by value change: "n/a", for own race relationship
    }

    // proposals, diplomacy screen events & summary events
    public abstract class Proposals {
      public static final int XAlliance = 24;               // confirmed by value change: "%s1 offer an alliance proposal to %2", for majors
      public static final int XMembership = 635;            // confirmed by value change: "%s1 offer a membership proposal to %2", for minors
      public static final int XAffiliation = 11;            // confirmed by value change: "%s1 offer an affiliation proposal to %2"
      public static final int XFriendship = 385;            // confirmed by value change: "%s1 offer a friendship proposal to %2"
      public static final int XNonAggression = 868;         // confirmed by value change: "%s1 offer a non-aggression proposal to %2"
      public static final int XGift = 413;                  // confirmed by value change: "%s1 presented a gift to %2"
      public static final int XBribe = 104;                 // confirmed by value change: "%s1 offer a bribe to %2", for minors
      public static final int XDemand = 224;                // confirmed by value change: "%s1 issue a demand to %2", for majors
      public static final int XMembershipTermination = 633; // confirmed by value change: "%s1 terminated their membership treaty with %2"
      public static final int XWarDeclaration = 1421;       // confirmed by value change: "%s1 declare war against %2"
      public static final int XWarPact = 1432;              // confirmed by value change: "%s1 propose a war pact to %2", for majors
    }

    // active treaty list & diplomacy screen events
    public abstract class Treaties {
      public static final int AllianceTreaty = 29;          // confirmed by value change: active treaty & event list
      public static final int MembershipTreaty = 639;       // confirmed by value change: active treaty
      public static final int AffiliationTreaty = 15;       // confirmed by value change: active treaty
      public static final int FriendshipTreaty = 388;       // confirmed by value change: active treaty
      public static final int NonAgression = 861;           // confirmed by value change: active treaty
      public static final int GiftTreaty = 414;             // confirmed by value change: diplomcy screen event list
      public static final int RightsOfPassage = 1055;
      public static final int WarPact = 1424;               // confirmed by value change: "War Pact Treaty"
      public static final int None = /*872*/ Shared.None;   // confirmed by value change: in proposal treaty list
    }

    // active treaty details & diplomacy screen events
    public abstract class TreatyTerms {
      public static final int XCredits = 196;               // confirmed by value change: "%s1 give %d2 credits to %3"
      public static final int XCreditsPerTurn = 887;        // "%s1 give %d2 credits per turn to %3"
    }

    // active treaty list & details, proposal, and diplomacy screen events
    public abstract class TreatyDuration {
      // if not Menu.Shared.TURNS 1372
      public static final int Indefinite = 526;                           // confirmed by value change
      public static final int NotAvailable = /*798*/ Shared.NotAvailable; // confirmed by value change
    }

    public abstract class TreatyFrequency {
      public static final int Immediately = 479;                          // confirmed by value change: proposal details
      public static final int PerTurn = 904;                              // confirmed by value change: proposal details & diplomacy screen events
    }

    public abstract class TreatyStatus {
      public static final int Active = 3;                                 // confirmed by value change: active treaty list
      public static final int NotAvailable = /*798*/ Shared.NotAvailable; // confirmed by value change: proposals
      public static final int Accept = 1;                                 // confirmed by value change: diplomacy screen events
      public static final int Propose = 954;                              // confirmed by value change: diplomacy screen events
      public static final int Reject = 990;                               // confirmed by value change: diplomacy screen events
      public static final int Renegotiate = 991;
    }

    // diplomacy screen empire reputations
    public abstract class Reputation {
      public static final int Worshipful = 1449;
      public static final int Enthusiastic = 289;
      public static final int Cordial = 190;                              // confirmed by value change
      public static final int Receptive = 986;                            // confirmed by value change
      public static final int Neutral = /*809*/ Shared.Neutral;           // confirmed by value change
      public static final int Uncooperative = 1383;                       // confirmed by value change
      public static final int Icy = 476;                                  // confirmed by value change
      public static final int Hostile = 442;                              // confirmed by value change
      public static final int Enraged = 288;                              // confirmed by value change
    }
  }

  public abstract class SpaceCraft {
    // ship details & object database
    // named in addition to the actual ship class
    // like e.g. B'rel, the klingon scout ship class
    public abstract class Categories {
      public static final int Starbase = 1173;      // confirmed by value change
      public static final int Outpost = 894;        // confirmed by value change
      public static final int BattleShip = 60;      // confirmed by value change
      public static final int StrikeCruiser = 1183; // confirmed by value change (artillery)
      public static final int Cruiser = 204;        // confirmed by value change
      public static final int Destroyer = 232;      // confirmed by value change
      public static final int ScoutShip = 1103;     // confirmed by value change
      public static final int ColonyShip = 166;     // confirmed by value change
      public static final int Transport = 1366;     // confirmed by value change
      public static final int Manifestation = 620;  // "manifestation"
      public static final int Unknown = /*1393*/ Shared.Unknown;
    }

    // ship details & object database
    public abstract class CategoryDescriptions {
      public static final int Starbase = 1174;      // confirmed by value change: "For defense, extending range, and ship repairs"
      public static final int Outpost = 896;        // confirmed by value change: "For extending ship range"
      public static final int BattleShip = 172;     // confirmed by value change: "For command and control"
      public static final int StrikeCruiser = 42;   // confirmed by value change: "For planetary assault"
      public static final int Cruiser = 1436;       // confirmed by value change: "For attack, defense, and escort duty"
      public static final int Raider = 971;         // confirmed by value change: "For raiding strikes or border patrols"
      public static final int ScoutShip = 1102;     // confirmed by value change: "For exploration and reconnaissance"
      public static final int ColonyShip = 165;     // confirmed by value change: "For colonization and terraforming"
      public static final int Transport = 1367;     // confirmed by value change: "For invasion, outpost and starbase construction"
      public static final int Invalid = /*553*/ Shared.Invalid; // confirmed by value change: for monsters if you change ownership
    }

    // fleet & ship details
    public abstract class Range {
      public static final int Long = 611;                     // confirmed by value change: object database & ship details
      public static final int Medium = /*627*/ Shared.Medium; // confirmed by value change: object database & ship details
      public static final int Short = 1147;                   // confirmed by value change: object database & ship details
    }

    // outpost status
    public abstract class Condition {
      public static final int OPERATIONAL = 58;     // confirmed by value change: fleet list & galaxy outpost tooltip
    }

    // ship details & combat ship info
    public abstract class CrewExperience {
      public static final int Legendary = 201;
      public static final int Elite = 198;
      public static final int Veteran = 203;              // confirmed by value change
      public static final int Regular = 202;              // confirmed by value change
      public static final int Green = 200;                // confirmed by value change
    }

    // galaxy map fleet commands
    public abstract class FleetCommand {
      public static final int None = /*872*/ Shared.None; // = move
      public static final int ATTACK_SYSTEM = 651;        // confirmed by value change
      public static final int AVOID = 652;                // confirmed by value change
      public static final int BUILD_OUTPOST = 653;        // confirmed by value change
      public static final int BUILD_STARBASE = 654;       // confirmed by value change
      public static final int COLONIZE_SYSTEM = 656;      // confirmed by value change
      public static final int ENTER_WORMHOLE = 657;       // confirmed by value change
      public static final int ENGAGE = 658;               // confirmed by value change
      public static final int INTERCEPT = 659;            // confirmed by value change: "INTERCEPT %d %%"
      public static final int Invalid = 660;
      public static final int RAID = 661;                 // confirmed by value change
      public static final int SCRAP = 662;                // confirmed by value change
      public static final int TERRAFORM_PLANET = 663;     // confirmed by value change
      public static final int TRAIN_CREW = 664;           // confirmed by value change
      public static final int UPGRADE = 665;
      public static final int BREAKAWAY = 101;            // might or might not be a command
      public static final int DEFEND = 217;               // might or might not be a command
      public static final int MELEE_FAST = 630;           // might or might not be a command
      public static final int MELEE_SLOW = 631;           // might or might not be a command
    }

    // ship details & battle events
    public abstract class BeamWeapons {
      public static final int PhaserBanks_1 = 63;         // confirmed by value change
      public static final int PhaserBanks_2 = 64;         // confirmed by value change
      public static final int DisruptorTypeI_1 = 65;      // confirmed by value change
      public static final int DisruptorTypeI_2 = 66;      // confirmed by value change
      public static final int PhaserEmitters_1 = 67;      // confirmed by value change
      public static final int PhaserEmitters_2 = 68;      // confirmed by value change
      public static final int DisruptorTypeII_1 = 69;     // confirmed by value change
      public static final int DisruptorTypeII_2 = 70;     // confirmed by value change
      public static final int PhaserArrays_1 = 71;        // confirmed by value change
      public static final int PhaserArrays_2 = 72;        // confirmed by value change
      public static final int DisruptorTypeIII_1 = 73;    // confirmed by value change
      public static final int DisruptorTypeIII_2 = 74;    // confirmed by value change
      public static final int DisruptorTypeIV = 75;       // confirmed by value change
      public static final int BorgRay = 76;               // confirmed by value change
      public static final int CalamarainRay = 77;         // confirmed by value change
      public static final int ChodakRay = 78;             // confirmed by value change
      public static final int GravitonBeam = 79;          // confirmed by value change (Crystal Entity)
      public static final int PhaserArray = 80;           // confirmed by value change (Echo Papa 607 combat drone)
      public static final int MultiphasicDisruptor = 81;
      public static final int ForcedPlasmaBeams = 82;     // confirmed by value change
      public static final int GomtuuRay = 83;
      public static final int HusnockRay = 84;            // confirmed by value change
      public static final int Phasers = 85;
      public static final int Lasers = 86;                // confirmed by value change (Tarellians)
      public static final int NoBeamWeapons = 87;         // confirmed by value change
    }

    // ship details
    public abstract class TorpedoWeapons {
      public static final int CETorp = 929;               // "CE Torp"
      public static final int PhotonTorpedoes_1 = 933;    // confirmed by value change
      public static final int PhotonTorpedoes_2 = 935;    // confirmed by value change
      public static final int PhotonTorpedoes_3 = 946;
      public static final int PlasmaTorpedoesI_1 = 934;   // confirmed by value change
      public static final int PlasmaTorpedoesI_2 = 936;   // confirmed by value change
      public static final int PlasmaTorpedoesII_1 = 938;  // confirmed by value change
      public static final int PlasmaTorpedoesII_2 = 940;  // confirmed by value change
      public static final int PlasmaTorpedoesIII_1 = 941; // confirmed by value change
      public static final int PlasmaTorpedoesIII_2 = 942; // confirmed by value change
      public static final int QuantumTorpedoes_1 = 937;   // confirmed by value change
      public static final int QuantumTorpedoes_2 = 939;   // confirmed by value change
      public static final int BorgTorp = 943;
      public static final int CalamarainTorp = 944;
      public static final int QuantumTorpedoes = 945;
      public static final int AntiplasmaBolts = 947;
      public static final int GomtuuTorp = 948;
      public static final int HusnockTorp = 949;
      public static final int IonTorpedoes = 950;         // confirmed by value change
      public static final int MerculiteTorpedoes = 951;
      public static final int NuclearMissile = 952;
      public static final int NoTorpedoes = 953;          // confirmed by value change
    }
  }

  // combat encounter, combat enemy menu, combat fleet details, combat events & summary events
  public abstract class Monsters {
    public static final int Gomtuu = 418;                     // confirmed by value change
    public static final int Raider = /*970*/ ShipName.Raider; // the raider by exception displays the loaded shipname: "Husnock raider"
    public static final int TheBorg = 98;                     // confirmed by value change: combat encounter, combat fleet details
    public static final int TheCalamarain = 115;              // confirmed by value change
    public static final int TheChodak = 159;                  // confirmed by value change
    public static final int TheCrystalEntity = 205;           // confirmed by value change
    public static final int TheEdoGuardian = 262;             // confirmed by value change: combat encounter, combat fleet details, combat events & summary events
    public static final int TheMinosians = 649;               // confirmed by value change: Echo Papa 607 combat drone
    public static final int TheTarellians = 1249;

    // galaxy map tooltip & system panel tooltip
    public abstract class ShipName {
      public static final int Gomtuu = /*418*/ Monsters.Gomtuu;                   // confirmed by code analysis (SCT)
      public static final int Raider = 970;                                       // confirmed by code analysis (SCT): Husnock raider
      public static final int XCube = 206;                                        // confirmed by code analysis (SCT): "Cube %d of %d"
      public static final int Calamarain = /*115*/ Monsters.TheCalamarain;        // confirmed by code analysis (SCT)
      public static final int Chodak = 160;                                       // confirmed by code analysis (SCT): randomly choosen -> "Enormous|Juggernaut|Gigantic|Colossus|Leviathan|Tremendous"
      public static final int CrystalEntity = /*205*/ Monsters.TheCrystalEntity;  // confirmed by code analysis (SCT)
      public static final int EdoGuardian = /*262*/ Monsters.TheEdoGuardian;      // confirmed by code analysis (SCT) and by value change: galaxy tooltip & system panel tooltip
      public static final int EchoPapa607 = 1389;                                 // confirmed by code analysis (SCT)
      public static final int EchoPapa607_2 = 257;
      public static final int Outcast = 893;                                      // confirmed by code analysis (SCT): Tarellians
    }
  }

  public abstract class Combat {
    // combat encounter, combat selection details (fleet, enemy, ship details & tactics) -> confirmed by value change
    public abstract class ShipGroup {
      public static final int Starbase = 57;
      public static final int Outpost = 895;        // confirmed by value change: battle events
      // used for all large battle ships but artillery
      public static final int Command = 1157;       // confirmed by value change: combat encounter & battle
      public static final int Strike = 612;         // confirmed by value change: combat encounter & battle -> artillery
      public static final int FastAttack = 318;     // confirmed by value change: combat encounter & battle
      public static final int NonCombatant = 862;   // confirmed by value change: combat encounter & battle
    }

    // combat ship details
    public abstract class ShipGroupDescriptions {
      // COMMAND SLOW_GROUP
      public static final int GalaxyClass = 1376;   // confirmed by value change: "These vessels allow other ships in the area to coordinate attacks with more precision."
      public static final int Command = 1380;       // confirmed by value change: "These ships combine deadly firepower and strong defense."
      // STRIKE LONG_RANGE group
      public static final int Strike = 1375;        // confirmed by value change: "These cruisers are designed for long-range attack and bombardment..."
      // FAST_ATTACK FAST_GROUP
      public static final int Destroyers = 1378;    // confirmed by value change: "These light warships are highly maneuverable."
      public static final int Scouts = 1379;        // confirmed by value change: "These lightly armed ships are highly maneuverable."
      // NON_COMBATANT NON_COMBAT group
      public static final int NonCombatants = 1377; // confirmed by value change: "These non-combatant ships are usually liabilities in engagements."
    }

    // combat group details
    public abstract class VesselType {
      public static final int Ships = 1139;         // confirmed by value change: also for galaxy fleet & system build -> for all major races & Chodak (159)
      public static final int BorgCubes = 207;      // confirmed by value change
      public static final int Devices = 235;        // confirmed by value change: Minosian (649) "devices" (Echo Papa 607 combat drone)
      public static final int XY = 1151;            // confirmed by value change: "%1 %2" for Crystal_entity, Husnock_raider, Gaseous_lifeform, Unknown_lifeform and Edo_god
    }

    // combat group details
    public abstract class ShipGroupRole {
      public static final int SLOW_GROUP = 1138;  // confirmed by value change: combat -> Command
      public static final int LONG_RANGE = 1136;  // confirmed by value change: Strike / Artillery
      public static final int FAST_GROUP = 1135;  // confirmed by value change: FastAttack
      public static final int NON_COMBAT = 1137;  // confirmed by value change: NonCombatant
    }

    // combat group command
    public abstract class CombatManeuver {
      public static final int Assault = 46;       // confirmed by value change: combat (close & distance attack)
      public static final int Circle = 161;       // confirmed by value change: combat
      public static final int FlyBy = 364;        // confirmed by value change: combat
      public static final int Evade = 295;        // confirmed by value change: combat
      public static final int Hail = 429;         // confirmed by value change: combat
      public static final int Ram = 972;          // confirmed by value change: combat
      public static final int Retreat = 1053;     // confirmed by value change: combat
      public static final int Attack = 49;
      public static final int Flank = 363;
      public static final int Hold = 436;         // confirmed by value change: combat events on cloaked turn
      public static final int Scatter = 1098;     // might or might not be a command
      public static final int Swarm = 1192;       // might or might not be a command
      public static final int Break = 99;         // might or might not be a command
      public static final int BreakAway = 100;    // might or might not be a command
      public static final int MeleeFast = 628;    // might or might not be a command
      public static final int MeleeSlow = 629;    // might or might not be a command
      public static final int Picard = 906;       // might or might not be a command
      public static final int Pincer = 907;       // might or might not be a command
      public static final int Tail = 1248;        // might or might not be a command
    }

    // combat tactics, displayed in left info tab while selecting a command
    public abstract class CombatTactics {
      public static final int Charge = 1220;                           // confirmed by value change
      public static final int Assault = 1218;                          // confirmed by value change
      public static final int Circle = /*161*/ CombatManeuver.Circle;  // confirmed by value change
      public static final int Harry = 1224;
      public static final int Cloak = 1226;
      public static final int FlyBy = 1230;                            // confirmed by value change
      public static final int Strafe = 1232;                           // confirmed by value change
      public static final int Evade = 1228;                            // confirmed by value change
      public static final int Hail = 1234;                             // confirmed by value change: "Open Hailing Frequencies"
      public static final int Ram = 1237;                              // confirmed by value change
      public static final int Retreat = 1239;                          // confirmed by value change
      public static final int SelectGroup = 1243;
      public static final int SelectShip = 1245;
      // unused
      public static final int Circle_2 = 1222;
    }

    // combat tactic details, displayed in bottom fleet details while selecting a command
    public abstract class CombatTacticInfo {
      public static final int Charge = 1221;            // confirmed by value change: "Directly attack enemy vessels at close range..."
      public static final int Assault = 1219;           // confirmed by value change: "Directly attack enemy vessels at long range..."
      public static final int Circle = 1223;            // confirmed by value change: "Circle enemy vessels while firing at long range..."
      public static final int Harry = 1225;             // "Circle enemy vessels while firing at close range..."
      public static final int Cloak = 1227;             // "Drop shields and engage cloaking devices..."
      public static final int FlyBy = 1231;             // confirmed by value change: "Fly past enemy vessels while firing at long range..."
      public static final int Strafe = 1233;            // confirmed by value change: "Fly past enemy vessels while firing at close range..."
      public static final int Evade = 1229;             // confirmed by value change: "Take evasive action..."
      public static final int Ram = 1238;               // confirmed by value change: "Set a collision course for enemy ships..."
      public static final int RetreatSelected = 1240;   // confirmed by value change: "Withdraw the selected ship(s) from the combat..."
      public static final int RetreatAll = 1241;        // "Withdraw all ships of this type from the combat."
      public static final int DEBUG_RemoveShip = 1242;  // "Remove this ship from the combat."
      public static final int Hail = 1235;              // confirmed by value change: "Open Hailing Frequencies"
      public static final int HoldPosition = 1236;
      public static final int SelectGroup = 1244;
      public static final int SelectShip = 1246;
    }
  }

  // data value groups not yet confirmed to be used
  public abstract class Other {
    // view descriptions that don't seem to be used, even with tooltips active
    // possibly been planned for a tutorial
    public abstract class ViewDescriptions {
      public static final int DiplomacyView = 1440;     // "This is the Diplomacy View.  This screen is used to review active treaties, propose new treaties..."
      public static final int IntelligenceView = 1441;  // "This is the Intelligence View.  This screen is used to manage internal security..."
      public static final int MainGalacticView = 1442;  // "This is the Main Galactic View.  This screen is used to move starships..."
      public static final int ResearchView = 1443;      // "This is the Research View.  This screen is used to allocate research points..."
      public static final int SolarSystemView = 1444;   // "This is the Solar System View.  This screen is used to build, upgrade, and scrap..."
    }

    // planet atmosphere, possibly used by events
    public abstract class AtmosphereTypes {
      public static final int Methane = 641;
      public static final int OxygenRich = 898;
      public static final int Sulfuric = 1189;
      public static final int ThinOxygen = 1352;
    }

    // unknown happiness, possibly for events
    public abstract class Happiness {
      public static final int Happiness = 431;
      public static final int Fanatic = 317;
      public static final int Happy = 432;
      public static final int Content = 186;
      public static final int Apathetic = 36;
      public static final int Miserable = 650;
      public static final int Unhappy = 1386;
      public static final int Disgruntled = 246;
      public static final int Defiant = 221;
      public static final int Riotous = 1057;
      public static final int Rebellious = 984;
      public static final int Unrest = 1397;
    }

    // likely galaxy map system label
    public abstract class Races {
      public static final int Cardassians = /*125*/ GameSettings.PlayerRace.Cardassians;
      public static final int Federation = /*319*/ GameSettings.PlayerRace.Federation;
      public static final int Ferengi = /*323*/ GameSettings.PlayerRace.Ferengi;
      public static final int Klingons = /*563*/ GameSettings.PlayerRace.Klingons;
      public static final int Romulans = /*1061*/ GameSettings.PlayerRace.Romulans;
    }

    // retire empire stats & possibly events
    public abstract class Empires {
      public static final int TheCardassianUnion = 124;               // confirmed by value change: retire empire stats
      public static final int TheCardassianUnion_1 = 1294;
      public static final int TheCardassianUnion_2 = 1295;
      public static final int TheUnitedFederationOfPlanets = 322;     // confirmed by value change: retire empire stats
      public static final int TheUnitedFederationOfPlanets_1 = 1302;
      public static final int TheUnitedFederationOfPlanets_2 = 1303;
      public static final int TheFerengiAlliance = 326;               // confirmed by value change: retire empire stats
      public static final int TheFerengiAlliance_1 = 1306;
      public static final int TheFerengiAlliance_2 = 1307;
      public static final int TheKlingonEmpire = 562;                 // confirmed by value change: retire empire stats
      public static final int TheKlingonEmpire_1 = 1310;
      public static final int TheKlingonEmpire_2 = 1311;
      public static final int TheRomulanStarEmpire = 1060;            // confirmed by value change: retire empire stats
      public static final int TheRomulanStarEmpire_1 = 1326;
      public static final int TheRomulanStarEmpire_2 = 1327;
    }

    // possibly for events
    public abstract class Emperors {
      public static final int TheCardassianLegate = 597;
      public static final int TheFerengiNagus = 598;
      public static final int TheFederationPresident = 599;
      public static final int TheKlingonChancellor = 600;
      public static final int TheRomulanPraetor = 601;
    }

    // fleet labels it seems
    public abstract class ShipRole {
      public static final int CommandShip = 173;
      public static final int Warship = 1435;     // likely used for mixed warship fleets
      public static final int UnarmedVessel = 1133;
      public static final int SUPPORT = 1190;     // colony ship + troop transport
      public static final int UnknownShip = 1396;
    }

    // unknown ship orders
    public abstract class ShipCommand {
      public static final int Cloak = 162;
      public static final int Bombard = 97;
      public static final int Invade = 552;
      public static final int Terraform = 1256;
      public static final int Scan = 1095;        // might or might not be a command
      public static final int Screen = 1105;      // might or might not be a command
    }

    // unknown counter, likely for fleet slot
    public abstract class Counter {
      public static final int First = 362;
      public static final int Second = 1106;
      public static final int Third = 1353;
      public static final int Fourth = 368;
      public static final int Fifth = 359;
      public static final int Sixth = 1155;
      public static final int Seventh = 1112;
      public static final int Eighth = 263;
      public static final int Ninth = 827;
    }

    // unkonwn amounts
    public abstract class Amount {
      public static final int Amount = 33;
      public static final int Lots = 613;
      public static final int Medium_1 = 34;
      public static final int Medium_2 = 1156;
      public static final int Even = 296;
      public static final int Never = 812;
    }

    // unknown power
    public abstract class Power {
      public static final int MAX = 477;
      public static final int RICH = 1054;
      public static final int STRONG = 1184;
      public static final int WEAK = 1437;
      public static final int POOR = 918;
    }
  }
}
