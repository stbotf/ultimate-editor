package ue.edit.res.stbof.files.dic.idx;

import java.util.Optional;
import com.google.common.base.Strings;
import lombok.val;
import ue.UE;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.BonusLevel;
import ue.edit.res.stbof.common.CStbof.PlanetAtmosphere;
import ue.edit.res.stbof.common.CStbof.PlanetSize;
import ue.edit.res.stbof.common.CStbof.PlanetType;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.ShipStealth;
import ue.edit.res.stbof.common.CStbof.ShipRange;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.common.CStbof.StellarType;
import ue.edit.res.stbof.common.CStbof.SystemStatusReq;
import ue.edit.res.stbof.common.CStbof.TechField;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.sav.common.CSavGame.CrewExperience;
import ue.edit.sav.common.CSavGame.Difficulty;
import ue.edit.sav.common.CSavGame.GalaxyShape;
import ue.edit.sav.common.CSavGame.GalaxySize;
import ue.edit.sav.common.CSavGame.MinorAmount;
import ue.edit.sav.common.CSavGame.Mission;
import ue.edit.sav.common.CSavGame.TacticalCombat;
import ue.edit.sav.common.CSavGame.VictoryConditions;
import ue.edit.sav.files.emp.AlienInfo.AlienRelationship;
import ue.edit.sav.files.emp.AlienInfo.RaceStatus;
import ue.edit.sav.files.emp.EmpsInfo.Relationship;
import ue.edit.sav.files.emp.Treaty.PeaceType;
import ue.edit.sav.files.emp.Treaty.TreatyType;
import ue.service.Language;
import ue.util.func.FunctionTools;

/**
 * A helper class to lookup BotF Lexicon values.
 */
public abstract class LexHelper {

  private static String DEFAULT = "???"; //$NON-NLS-1$

  public static Optional<Lexicon> lexicon() {
    Stbof stbof = UE.FILES.stbof();
    return stbof != null ? stbof.files().findLexicon() : Optional.empty();
  }

  public static String mapEntry(int idx) {
    return mapEntry(idx, DEFAULT);
  }

  public static String mapEntry(int idx, String defaultValue) {
    val lex = lexicon();
    return FunctionTools.defaultIfThrown(() -> lex.isPresent() ?
      lex.get().getEntry(idx) : defaultValue, defaultValue);
  }

  // alternatively lookup names in stbof race.rst
  public static int lexRace(int raceId) {
    switch (raceId) {
      case Race.Card:
        return LexDataIdx.GameSettings.PlayerRace.Cardassians;
        // return LexDataIdx.Diplomacy.FormalRaces.TheCardassians_1;
      case Race.Fed:
        return LexDataIdx.GameSettings.PlayerRace.Federation;
        // return LexDataIdx.Diplomacy.FormalRaces.TheFederation_1;
      case Race.Ferg:
        return LexDataIdx.GameSettings.PlayerRace.Ferengi;
        // return LexDataIdx.Diplomacy.FormalRaces.TheFerengi_1;
      case Race.Kling:
        return LexDataIdx.GameSettings.PlayerRace.Klingons;
        // return LexDataIdx.Diplomacy.FormalRaces.TheKlingons_1;
      case Race.Rom:
        return LexDataIdx.GameSettings.PlayerRace.Romulans;
        // return LexDataIdx.Diplomacy.FormalRaces.TheRomulans_1;
      case 5:
        return LexDataIdx.Diplomacy.FormalRaces.TheAcamarians_1;
      case 6:
        return LexDataIdx.Diplomacy.FormalRaces.TheAndorians_1;
      case 7:
        return LexDataIdx.Diplomacy.FormalRaces.TheAngosians_1;
      case 8:
        return LexDataIdx.Diplomacy.FormalRaces.TheAntedeans_1;
      case 9:
        return LexDataIdx.Diplomacy.FormalRaces.TheAnticans_1;
      case 10:
        return LexDataIdx.Diplomacy.FormalRaces.TheBajorans_1;
      case 11:
        return LexDataIdx.Diplomacy.FormalRaces.TheBandi_1;
      case 12:
        return LexDataIdx.Diplomacy.FormalRaces.TheBenzites_1;
      case 13:
        return LexDataIdx.Diplomacy.FormalRaces.TheBetazoids_1;
      case 14:
        return LexDataIdx.Diplomacy.FormalRaces.TheBolians_1;
      case 15:
        return LexDataIdx.Diplomacy.FormalRaces.TheBynars_1;
      case 16:
        return LexDataIdx.Diplomacy.FormalRaces.TheCaldonians_1;
      case 17:
        return LexDataIdx.Diplomacy.FormalRaces.TheChalnoth_1;
      case 18:
        return LexDataIdx.Diplomacy.FormalRaces.TheEdo_1;
      case 19:
        return LexDataIdx.Diplomacy.FormalRaces.TheKtarians_1;
      case 20:
        return LexDataIdx.Diplomacy.FormalRaces.TheMalcorians_1;
      case 21:
        return LexDataIdx.Diplomacy.FormalRaces.TheMintakans_1;
      case 22:
        return LexDataIdx.Diplomacy.FormalRaces.TheMizarians_1;
      case 23:
        return LexDataIdx.Diplomacy.FormalRaces.TheNausicaans_1;
      case 24:
        return LexDataIdx.Diplomacy.FormalRaces.ThePakleds_1;
      case 25:
        return LexDataIdx.Diplomacy.FormalRaces.TheSelay_1;
      case 26:
        return LexDataIdx.Diplomacy.FormalRaces.TheSheliak_1;
      case 27:
        return LexDataIdx.Diplomacy.FormalRaces.TheTakarans_1;
      case 28:
        return LexDataIdx.Diplomacy.FormalRaces.TheTalarians_1;
      case 29:
        return LexDataIdx.Diplomacy.FormalRaces.TheTamarians_1;
      case 30:
        return LexDataIdx.Diplomacy.FormalRaces.TheTrill_1;
      case 31:
        return LexDataIdx.Diplomacy.FormalRaces.TheUllians_1;
      case 32:
        return LexDataIdx.Diplomacy.FormalRaces.TheVulcans_1;
      case 33:
        return LexDataIdx.Diplomacy.FormalRaces.TheYridians_1;
      case 34:
        return LexDataIdx.Diplomacy.FormalRaces.TheZakdorn_1;
      case Race.Monster:
        return LexDataIdx.SpaceCraft.Categories.Manifestation;
      case Race.Neutral:
        return LexDataIdx.Shared.Neutral;
      default:
        return LexMenuIdx.IntelScreen.DetailReport.MinorRace;
    }
  }

  // alternatively lookup names in stbof race.rst
  public static String mapRace(int raceId) {
    if (raceId == Race.Monster)
      return Language.getString("LexHelper.0");

    val lex = lexicon();
    return lex == null ? "<race " + Integer.toString(raceId) + ">"
      : mapEntry(lex.get(), LexHelper.lexRace(raceId));
  }

  // alternatively lookup names in stbof race.rst
  public static int lexOwner(int raceId) {
    switch (raceId) {
      case Race.Card:
        return LexDataIdx.Galaxy.Ownership.Cardassian;
      case Race.Fed:
        return LexDataIdx.Galaxy.Ownership.Federation;
      case Race.Ferg:
        return LexDataIdx.Galaxy.Ownership.Ferengi;
      case Race.Kling:
        return LexDataIdx.Galaxy.Ownership.Klingon;
      case Race.Rom:
        return LexDataIdx.Galaxy.Ownership.Romulan;
      case Race.Monster:
        return LexDataIdx.SpaceCraft.Categories.Manifestation;
      default:
        return LexDataIdx.Galaxy.Ownership.MinorRace;
    }
  }

  // alternatively lookup names in stbof race.rst
  public static String mapOwner(int raceId) {
    if (raceId == Race.Monster)
      return Language.getString("LexHelper.1");

    val lex = lexicon();
    return lex.isPresent() ? "<race " + Integer.toString(raceId) + ">"
      : mapEntry(lex.get(), LexHelper.lexOwner(raceId));
  }

  public static int lexTreatyType(int type) {
    switch (type) {
      case TreatyType.Peace:
        // check PeaceType this case
        return LexDataIdx.Diplomacy.Relationship.PEACE;
      case TreatyType.Alliance:
        return LexDataIdx.Diplomacy.Treaties.AllianceTreaty;
      case TreatyType.WarPact:
        return LexDataIdx.Diplomacy.Treaties.WarPact;
      case TreatyType.AcceptedDemand:
        // label is not read from the lexicon
        return LexMenuIdx.Other.Request;
      case TreatyType.Friendship:
        return LexDataIdx.Diplomacy.Treaties.FriendshipTreaty;
      case TreatyType.Affiliation:
        return LexDataIdx.Diplomacy.Treaties.AffiliationTreaty;
      case TreatyType.Membership:
        return LexDataIdx.Diplomacy.Treaties.MembershipTreaty;
      case TreatyType.Gift:
        return LexDataIdx.Diplomacy.Treaties.GiftTreaty;
      default:
        return LexDataIdx.Diplomacy.Relationship.Unknown;
    }
  }

  public static String mapTreatyType(int type) {
    val lex = lexicon();
    return lex == null ? "<treaty " + Integer.toString(type) + ">"
     : mapEntry(lex.get(), LexHelper.lexTreatyType(type));
  }

  public static int lexRelationship(int key) {
    switch (key) {
      case Relationship.Neutral:
        return LexDataIdx.Diplomacy.Relationship.Neutral;
      case Relationship.War:
        return LexDataIdx.Diplomacy.Relationship.WAR;
      case Relationship.Alliance:
        return LexDataIdx.Diplomacy.Relationship.Alliance;
      case Relationship.Peace:
        return LexDataIdx.Diplomacy.Relationship.PEACE;
      default:
        return LexDataIdx.Diplomacy.Relationship.Unknown;
    }
  }

  public static String mapRelationship(int key) {
    val lex = lexicon();
    return lex == null ? "<relationship " + Integer.toString(key) + ">"
      : mapEntry(lex.get(), LexHelper.lexRelationship(key));
  }

  public static int lexPeaceType(int type) {
    switch (type) {
      case PeaceType.NonAgression:
        return LexDataIdx.Diplomacy.Treaties.NonAgression;
      case PeaceType.Friendship:
        return LexDataIdx.Diplomacy.Treaties.FriendshipTreaty;
      case PeaceType.Affiliation:
        return LexDataIdx.Diplomacy.Treaties.AffiliationTreaty;
      default:
        return LexDataIdx.Diplomacy.Relationship.Unknown;
    }
  }

  public static String mapPeaceType(int type) {
    val lex = lexicon();
    return lex == null ? "<peace type " + Integer.toString(type) + ">"
      : mapEntry(lex.get(), LexHelper.lexPeaceType(type));
  }

  public static int lexAlienRelationship(int idx) {
    switch (idx) {
      case AlienRelationship.Neutral:
        return LexDataIdx.Diplomacy.Relationship.Neutral;
      case AlienRelationship.Friendship:
        return LexDataIdx.Diplomacy.Relationship.Friendly;
      case AlienRelationship.Affiliation:
        return LexDataIdx.Diplomacy.Relationship.Affiliated;
      case AlienRelationship.Membership: // == Treaty.Alliance
        return LexDataIdx.Diplomacy.Relationship.Member;
      case AlienRelationship.War:
        return LexDataIdx.Diplomacy.Relationship.WAR;
      default:
        return LexDataIdx.Diplomacy.Relationship.Unknown;
    }
  }

  public static String mapAlienRelationship(int key) {
    val lex = lexicon();
    return lex == null ? "<relationship " + Integer.toString(key) + ">"
      : mapEntry(lex.get(), LexHelper.lexAlienRelationship(key));
  }

  public static int lexMinorRaceStatus(Integer status) {
    switch (status) {
      case RaceStatus.Independent:
        return LexDataIdx.Shared.None;
      case RaceStatus.Subjugated:
        return LexDataIdx.Diplomacy.Relationship.Subjugated;
      case RaceStatus.Member:
        return LexDataIdx.Diplomacy.Relationship.Member;
      default:
        return LexDataIdx.Diplomacy.Relationship.Unknown;
    }
  }

  public static String mapMinorRaceStatus(int status) {
    val lex = lexicon();
    return lex == null ? "<race status " + Integer.toString(status) + ">"
      : mapEntry(lex.get(), LexHelper.lexMinorRaceStatus(status));
  }

  public static int lexStellarType(int type) {
    switch (type) {
      case StellarType.BlackHole:
        return LexDataIdx.Galaxy.StellarObjects.BlackHole;
      case StellarType.XRayPulsar:
        return LexDataIdx.Galaxy.StellarObjects.XRayPulsar;
      case StellarType.Nebula:
        return LexDataIdx.Galaxy.StellarObjects.Nebula;
      case StellarType.NeutronStar:
        return LexDataIdx.Galaxy.StellarObjects.NeutronStar;
      case StellarType.RedGiant:
        return LexDataIdx.Galaxy.StellarObjects.RedGiant;
      case StellarType.OrangeStar:
        return LexDataIdx.Galaxy.StellarObjects.OrangeStar;
      case StellarType.YellowStar:
        return LexDataIdx.Galaxy.StellarObjects.YellowStar;
      case StellarType.WhiteStar:
        return LexDataIdx.Galaxy.StellarObjects.WhiteStar;
      case StellarType.GreenStar:
        return LexDataIdx.Galaxy.StellarObjects.GreenStar;
      case StellarType.BlueStar:
        return LexDataIdx.Galaxy.StellarObjects.BlueStar;
      case StellarType.WormHole:
        return LexDataIdx.Galaxy.StellarObjects.Wormhole;
      case StellarType.RadioPulsar:
        return LexDataIdx.Galaxy.StellarObjects.RadioPulsar;
      default:
        return LexDataIdx.Shared.Unknown;
    }
  }

  public static String mapStellarType(int type) {
    val lex = lexicon();
    if (lex != null) {
      int lexId = LexHelper.lexStellarType(type);
      String name = mapEntry(lex.get(), lexId);
      if (lexId == LexDataIdx.Shared.Unknown)
        name += "-" + Integer.toString(type);
      return name;
    }

    return "<stellar type " + Integer.toString(type) + ">";
  }

  public static int lexStellarDesc(int type) {
    switch (type) {
      case StellarType.BlackHole:
      return LexMenuIdx.Galaxy.SystemPanel.StellarDescriptions.BlackHole;
      case StellarType.XRayPulsar:
      return LexMenuIdx.Galaxy.SystemPanel.StellarDescriptions.RadiativeEmission;
      case StellarType.Nebula:
        return LexMenuIdx.Galaxy.SystemPanel.StellarDescriptions.Nebula;
      case StellarType.NeutronStar:
        return LexMenuIdx.Galaxy.SystemPanel.StellarDescriptions.NeutronStarEmission;
      case StellarType.RedGiant:
        return LexDataIdx.Galaxy.StellarObjects.RedGiant;
      case StellarType.OrangeStar:
        return LexDataIdx.Galaxy.StellarObjects.OrangeStar;
      case StellarType.YellowStar:
        return LexDataIdx.Galaxy.StellarObjects.YellowStar;
      case StellarType.WhiteStar:
        return LexDataIdx.Galaxy.StellarObjects.WhiteStar;
      case StellarType.GreenStar:
        return LexDataIdx.Galaxy.StellarObjects.GreenStar;
      case StellarType.BlueStar:
        return LexDataIdx.Galaxy.StellarObjects.BlueStar;
      case StellarType.WormHole:
        return LexMenuIdx.Galaxy.SystemPanel.StellarDescriptions.Wormhole;
      case StellarType.RadioPulsar:
        return LexMenuIdx.Galaxy.SystemPanel.StellarDescriptions.RadioPulsarEmission;
      default:
        return LexDataIdx.Shared.NotAvailable;
    }
  }

  public static String mapStellarDesc(int type) {
    val lex = lexicon();
    if (lex != null) {
      int lexId = LexHelper.lexStellarType(type);
      String name = mapEntry(lex.get(), lexId);
      if (lexId == LexDataIdx.Shared.Unknown)
        name += "-" + Integer.toString(type);
      return name;
    }

    return "<stellar type " + Integer.toString(type) + ">";
  }

  public static int lexPlanetSize(int idx) {
    switch (idx) {
      case PlanetSize.Small:
        return LexDataIdx.GameSettings.GalaxySize.Small;
      case PlanetSize.Medium:
        return LexDataIdx.GameSettings.GalaxySize.Medium;
      case PlanetSize.Large:
        return LexDataIdx.GameSettings.GalaxySize.Large;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapPlanetSize(int size) {
    val lex = lexicon();
    return lex == null ? "<planet size " + Integer.toString(size) + ">"
      : mapEntry(lex.get(), LexHelper.lexPlanetSize(size));
  }

  // arctic, asteroid, barren, desert, jungle, oceanic, terran, volcanic
  public static int lexPlanetType(int type) {
    switch (type) {
      case PlanetType.Arctic:
        return LexDataIdx.System.PlanetTypes.Arctic;
      case PlanetType.Barren:
        return LexDataIdx.System.PlanetTypes.Barren;
      case PlanetType.Desert:
        return LexDataIdx.System.PlanetTypes.Desert;
      case PlanetType.GasGiant:
        return LexDataIdx.System.PlanetTypes.GasGiant;
      case PlanetType.Jungle:
        return LexDataIdx.System.PlanetTypes.Jungle;
      case PlanetType.Oceanic:
        return LexDataIdx.System.PlanetTypes.Oceanic;
      case PlanetType.Terran:
        return LexDataIdx.System.PlanetTypes.Terran;
      case PlanetType.Volcanic:
        return LexDataIdx.System.PlanetTypes.Volcanic;

      // modding specials
      case PlanetType.Methanic:
        return LexDataIdx.Other.AtmosphereTypes.Methane;
      case PlanetType.Asteroid:
        return LexDataIdx.System.PlanetTypes.Asteroid;
      case PlanetType.AsteroidBelt:
        return LexDataIdx.System.Restrictions.AsteroidBeltDilithium;
      case PlanetType.Dilithium:
        return LexDataIdx.System.Restrictions.Dilithium;
      case PlanetType.WormHole:
        return LexDataIdx.Galaxy.StellarObjects.Wormhole;
      case PlanetType.RadioPulsar:
        return LexDataIdx.Galaxy.StellarObjects.RadioPulsar;
      case PlanetType.XRayPulsar:
        return LexDataIdx.Galaxy.StellarObjects.XRayPulsar;
      default:
        return LexDataIdx.Shared.Unknown;
    }
  }

  public static String mapPlanetType(int type) {
    val lex = lexicon();
    return lex == null ? "<planet type " + Integer.toString(type) + ">"
      : mapEntry(lex.get(), LexHelper.lexPlanetType(type));
  }

  public static int lexPlanetAtmosphere(int type) {
    switch (type) {
      case PlanetAtmosphere.Methane:
        return LexDataIdx.Other.AtmosphereTypes.Methane;
      case PlanetAtmosphere.None:
        return LexDataIdx.Shared.None;
      case PlanetAtmosphere.OxygenRich:
        return LexDataIdx.Other.AtmosphereTypes.OxygenRich;
      case PlanetAtmosphere.Sulfuric:
        return LexDataIdx.Other.AtmosphereTypes.Sulfuric;
      case PlanetAtmosphere.OxygenThin:
        return LexDataIdx.Other.AtmosphereTypes.ThinOxygen;

      default:
        return LexDataIdx.Shared.Unknown;
    }
  }

  public static String mapPlanetAtmosphere(int type) {
    val lex = lexicon();
    return lex == null ? "<planet atmosphere " + Integer.toString(type) + ">"
      : mapEntry(lex.get(), LexHelper.lexPlanetAtmosphere(type));
  }

  public static int lexBonusLevel(int bonus) {
    switch (bonus) {
      case BonusLevel.Few:
        return LexDataIdx.System.BonusLevel.Little;
      case BonusLevel.Moderate:
        return LexDataIdx.System.BonusLevel.Medium;
      case BonusLevel.Much:
        return LexDataIdx.System.BonusLevel.Large;
      default:
        return LexDataIdx.Shared.Unknown;
    }
  }

  public static String mapBonusLevel(int bonus) {
    val lex = lexicon();
    return lex == null ? "<bonus lvl " + Integer.toString(bonus) + ">"
      : mapEntry(lex.get(), LexHelper.lexBonusLevel(bonus));
  }

  // system status requirements / restrictions
  public static int lexSystemStatusReq(int type) {
    switch (type) {
      case SystemStatusReq.HomeSystem:
        return LexDataIdx.System.Restrictions.HomeSystem;
      case SystemStatusReq.NativeMember:
        return LexDataIdx.System.Restrictions.NativeMemberSystem;
      case SystemStatusReq.NonNativeMember:
        return LexDataIdx.System.Restrictions.NonNativeMemberSystem;
      case SystemStatusReq.Subjugated:
        return LexDataIdx.System.Restrictions.SubjugatedSystem;
      case SystemStatusReq.Affiliated:
        return LexDataIdx.System.Restrictions.AffiliatedSystem;
      case SystemStatusReq.IndependentMinor:
        return LexDataIdx.System.Restrictions.IndependentMinorSystem;
      case SystemStatusReq.ConqueredHome:
        return LexDataIdx.System.Restrictions.ConqueredHomeSystem;
      case SystemStatusReq.None:
        return LexDataIdx.System.Restrictions.AnySystem;
      case SystemStatusReq.Rebel:
        return LexDataIdx.System.Restrictions.RebelSystem;
      case SystemStatusReq.EmptySystem:
        return LexDataIdx.System.Restrictions.EmptySystem;
      default:
        return LexDataIdx.Shared.Unknown;
    }
  }

  public static String mapSystemStatusReq(int sysReq) {
    val lex = lexicon();
    return lex == null ? "<system requirement " + Integer.toString(sysReq) + ">"
      : mapEntry(lex.get(), LexHelper.lexSystemStatusReq(sysReq));
  }

  public static int lexMoraleLvl(int lvl) {
    switch (lvl) {
      case 3:
        return LexDataIdx.System.Morale.Fanatic;
      case 2:
        return LexDataIdx.System.Morale.Loyal;
      case 1:
        return LexDataIdx.System.Morale.Pleased;
      case 0:
        return LexDataIdx.System.Morale.Content;
      case -1:
        return LexDataIdx.System.Morale.Apathetic;
      case -2:
        return LexDataIdx.System.Morale.Disgruntled;
      case -3:
        return LexDataIdx.System.Morale.Defiant;
      case -4:
        return LexDataIdx.System.Morale.Rebellious;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapMoraleLvl(int lvl) {
    val lex = lexicon();
    return lex == null ? "<morale level " + Integer.toString(lvl) + ">"
      : mapEntry(lex.get(), LexHelper.lexMoraleLvl(lvl));
  }

  public static short lexShipRole(short role) {
    switch (role) {
      case ShipRole.Scout:
        return LexDataIdx.SpaceCraft.Categories.ScoutShip;
      case ShipRole.Destroyer:
        return LexDataIdx.SpaceCraft.Categories.Destroyer;
      case ShipRole.Cruiser:
        return LexDataIdx.SpaceCraft.Categories.Cruiser;
      case ShipRole.StrikeCruiser:
        return LexDataIdx.SpaceCraft.Categories.StrikeCruiser;
      case ShipRole.BattleShip:
        return LexDataIdx.SpaceCraft.Categories.BattleShip;
      case ShipRole.ColonyShip:
        return LexDataIdx.SpaceCraft.Categories.ColonyShip;
      case ShipRole.Outpost:
        return LexDataIdx.SpaceCraft.Categories.Outpost;
      case ShipRole.Starbase:
        return LexDataIdx.SpaceCraft.Categories.Starbase;
      case ShipRole.Monster:
        return LexDataIdx.SpaceCraft.Categories.Manifestation;
      case ShipRole.TroopTransport:
        return LexDataIdx.SpaceCraft.Categories.Transport;
      default:
        return LexDataIdx.SpaceCraft.Categories.Unknown;
    }
  }

  public static String mapShipRole(short role) {
    if (role == ShipRole.Monster)
      return Language.getString("LexHelper.2");

    val lex = lexicon();
    return lex == null ? "<ship role " + Short.toString(role) + ">"
      : mapEntry(lex.get(), LexHelper.lexShipRole(role));
  }

  public static int lexShipRange(int range) {
    switch (range) {
      case ShipRange.None:
        return LexDataIdx.Shared.None;
      case ShipRange.Short:
        return LexDataIdx.SpaceCraft.Range.Short;
      case ShipRange.Medium:
        return LexDataIdx.SpaceCraft.Range.Medium;
      case ShipRange.Long:
        return LexDataIdx.SpaceCraft.Range.Long;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapShipRange(int range) {
    if (range == ShipRange.Towable)
      return Language.getString("LexHelper.3");

    val lex = lexicon();
    return lex == null ? "<ship range " + Integer.toString(range) + ">"
      : mapEntry(lex.get(), LexHelper.lexShipRange(range));
  }

  public static int lexShipStealth(int stealth) {
    switch (stealth) {
      case ShipStealth.None:
      case ShipStealth.Stealth_I:
      case ShipStealth.Stealth_II:
      case ShipStealth.Stealth_III:
        return LexDataIdx.Shared.None;
      case ShipStealth.Cloak_I:
      case ShipStealth.Cloak_II:
      case ShipStealth.Cloak_III:
        return LexDataIdx.Other.ShipCommand.Cloak;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapShipStealth(int stealth) {
    if (stealth >= ShipStealth.None) {
      if (stealth == ShipStealth.None)
        return Language.getString("LexHelper.4");
      else if (stealth < 4)
        return Language.getString("LexHelper.5") + Strings.repeat("I", stealth);
      else if (stealth < 7)
        return Language.getString("LexHelper.6") + Strings.repeat("I", stealth - 3);
    }

    val lex = lexicon();
    return lex == null ? "<ship cloak " + Integer.toString(stealth) + ">"
      : mapEntry(lex.get(), LexHelper.lexShipStealth(stealth));
  }

  public static int lexCrewExperience(int exp) {
    switch (exp) {
      case CrewExperience.Green:
        return LexDataIdx.SpaceCraft.CrewExperience.Green;
      case CrewExperience.Regular:
        return LexDataIdx.SpaceCraft.CrewExperience.Regular;
      case CrewExperience.Veteran:
        return LexDataIdx.SpaceCraft.CrewExperience.Veteran;
      case CrewExperience.Elite:
        return LexDataIdx.SpaceCraft.CrewExperience.Elite;
      case CrewExperience.Legendary:
        return LexDataIdx.SpaceCraft.CrewExperience.Legendary;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapCrewExperience(int exp) {
    val lex = lexicon();
    return lex == null ? "<crew exp " + Integer.toString(exp) + ">"
      : mapEntry(lex.get(), LexHelper.lexCrewExperience(exp));
  }

  public static int lexTFMission(int mission) {
    switch (mission) {
      case Mission.Move:
        return LexDataIdx.SpaceCraft.FleetCommand.None;
      case Mission.Terraform:
        return LexDataIdx.SpaceCraft.FleetCommand.TERRAFORM_PLANET;
      case Mission.Colonize:
        return LexDataIdx.SpaceCraft.FleetCommand.COLONIZE_SYSTEM;
      case Mission.Raid:
        return LexDataIdx.SpaceCraft.FleetCommand.RAID;
      case Mission.Intercept:
        return LexDataIdx.SpaceCraft.FleetCommand.INTERCEPT;
      case Mission.Evade:
        return LexDataIdx.SpaceCraft.FleetCommand.AVOID;
      case Mission.Engage:
        return LexDataIdx.SpaceCraft.FleetCommand.ENGAGE;
      case Mission.BuildOutpost:
        return LexDataIdx.SpaceCraft.FleetCommand.BUILD_OUTPOST;
      case Mission.BuildStarbase:
        return LexDataIdx.SpaceCraft.FleetCommand.BUILD_STARBASE;
      case Mission.TrainCrew:
        return LexDataIdx.SpaceCraft.FleetCommand.TRAIN_CREW;
      case Mission.Expand:
        return LexDataIdx.SpaceCraft.FleetCommand.UPGRADE; // might be incorrect
      case Mission.Scrap:
        return LexDataIdx.SpaceCraft.FleetCommand.SCRAP;
      case Mission.EnterWormhole:
        return LexDataIdx.SpaceCraft.FleetCommand.ENTER_WORMHOLE;
      case Mission.AttackSystem:
        return LexDataIdx.SpaceCraft.FleetCommand.ATTACK_SYSTEM;
      case Mission.Invalid:
      default:
        return LexDataIdx.SpaceCraft.FleetCommand.Invalid;
    }
  }

  public static String mapTFMission(int mission) {
    if (mission == Mission.Move)
      return "move";

    val lex = lexicon();
    return lex == null ? "<TF mission " + Integer.toString(mission) + ">"
      : stripValues(mapEntry(lex.get(), LexHelper.lexTFMission(mission)));
  }

  public static int lexTechField(int techLevel) {
    switch (techLevel) {
      case TechField.Biotech:
        return LexDataIdx.Research.Field.Biotech;
      case TechField.Computer:
        return LexDataIdx.Research.Field.Computer;
      case TechField.Construction:
        return LexDataIdx.Research.Field.Construction;
      case TechField.Energy:
        return LexDataIdx.Research.Field.Energy;
      case TechField.Propulsion:
        return LexDataIdx.Research.Field.Propulsion;
      case TechField.Weapons:
        return LexDataIdx.Research.Field.Weapons;
      case TechField.Sociology:
        return LexDataIdx.Research.Field.Sociology;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapTechField(int techLevel) {
    val lex = lexicon();
    return lex == null ? "<tech field " + Integer.toString(techLevel) + ">"
      : mapEntry(lex.get(), LexHelper.lexTechField(techLevel));
  }

  public static int lexEvoLvl(int idx) {
    switch (idx) {
      case 0:
        return LexDataIdx.GameSettings.StartCondition.BEGINNING;
      case 1:
        return LexDataIdx.GameSettings.StartCondition.EARLY;
      case 2:
        return LexDataIdx.GameSettings.StartCondition.DEVELOPED;
      case 3:
        return LexDataIdx.GameSettings.StartCondition.EXPANDED;
      case 4:
        return LexDataIdx.GameSettings.StartCondition.ADVANCED;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapEvoLvl(int evoLevel) {
    val lex = lexicon();
    return lex == null ? "<evo level " + Integer.toString(evoLevel) + ">"
      : mapEntry(lex.get(), LexHelper.lexEvoLvl(evoLevel));
  }

  public static int lexMinorAmount(int idx) {
    switch (idx) {
      case MinorAmount.None:
        return LexDataIdx.GameSettings.MinorRaces.None;
      case MinorAmount.Few:
        return LexDataIdx.GameSettings.MinorRaces.FEW;
      case MinorAmount.Some:
        return LexDataIdx.GameSettings.MinorRaces.SOME;
      case MinorAmount.Many:
        return LexDataIdx.GameSettings.MinorRaces.MANY;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapMinorAmount(int amount) {
    val lex = lexicon();
    return lex == null ? "<amount " + Integer.toString(amount) + ">"
      : mapEntry(lex.get(), LexHelper.lexMinorAmount(amount));
  }

  public static int lexDifficulty(int idx) {
    switch (idx) {
      case Difficulty.Simple:
        return LexDataIdx.GameSettings.Difficulty.SIMPLE;
      case Difficulty.Easy:
        return LexDataIdx.GameSettings.Difficulty.EASY;
      case Difficulty.Normal:
        return LexDataIdx.GameSettings.Difficulty.NORMAL;
      case Difficulty.Hard:
        return LexDataIdx.GameSettings.Difficulty.HARD;
      case Difficulty.Impossible:
        return LexDataIdx.GameSettings.Difficulty.IMPOSSIBLE;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapDifficulty(int difficulty) {
    val lex = lexicon();
    return lex == null ? "<difficulty " + Integer.toString(difficulty) + ">"
      : mapEntry(lex.get(), LexHelper.lexDifficulty(difficulty));
  }

  public static int lexTacticalCombat(int idx) {
    switch (idx) {
      case TacticalCombat.Manual:
        return LexDataIdx.GameSettings.TacticalCombat.MANUAL;
      case TacticalCombat.Auto:
        return LexDataIdx.GameSettings.TacticalCombat.AUTOMATIC;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapTacticalCombat(int mode) {
    val lex = lexicon();
    return lex == null ? "<mode " + Integer.toString(mode) + ">"
      : mapEntry(lex.get(), LexHelper.lexTacticalCombat(mode));
  }

  public static int lexVictoryConditions(int idx) {
    switch (idx) {
      case VictoryConditions.Domination:
        return LexDataIdx.GameSettings.VictoryCondition.DOMINATION;
      case VictoryConditions.TeamPlay:
        return LexDataIdx.GameSettings.VictoryCondition.TEAM_PLAY;
      case VictoryConditions.Vendetta:
        return LexDataIdx.GameSettings.VictoryCondition.VENDETTA;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapVictoryCondition(int mode) {
    val lex = lexicon();
    return lex == null ? "<mode " + Integer.toString(mode) + ">"
      : mapEntry(lex.get(), LexHelper.lexVictoryConditions(mode));
  }

  public static int lexGalSize(int idx) {
    switch (idx) {
      case GalaxySize.Small:
        return LexDataIdx.GameSettings.GalaxySize.Small;
      case GalaxySize.Medium:
        return LexDataIdx.GameSettings.GalaxySize.Medium;
      case GalaxySize.Large:
        return LexDataIdx.GameSettings.GalaxySize.Large;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapGalSize(int size) {
    val lex = lexicon();
    return lex == null ? "<size " + Integer.toString(size) + ">"
      : mapEntry(lex.get(), LexHelper.lexGalSize(size));
  }

  public static int lexGalShape(int idx) {
    switch (idx) {
      case GalaxyShape.Irregular:
        return LexDataIdx.GameSettings.GalaxyShape.Irregular;
      case GalaxyShape.Elliptical:
        return LexDataIdx.GameSettings.GalaxyShape.Elliptical;
      case GalaxyShape.Ring:
        return LexDataIdx.GameSettings.GalaxyShape.Ring;
      case GalaxyShape.Spiral:
        return LexDataIdx.GameSettings.GalaxyShape.Spiral;
      default:
        return LexDataIdx.Shared.Invalid;
    }
  }

  public static String mapGalShape(int shape) {
    val lex = lexicon();
    return lex.isPresent() ? "<shape " + Integer.toString(shape) + ">"
      : mapEntry(lex.get(), LexHelper.lexGalShape(shape));
  }

  public static String stripValues(String txt) {
    return txt.replace("%%", "").replace("%d", "").replace("%f", "").replace("%s", "").trim();
  }

  private static String mapEntry(Lexicon lex, int idx) {
    return FunctionTools.defaultIfThrown(() -> lex.getEntry(idx), DEFAULT);
  }

}
