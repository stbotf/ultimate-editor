package ue.edit.res.stbof.files.dic.idx;

/**
 * Game notification head lines for popups not related to the turn progress.
 * E.g. for error feedback or input confirmation, but not for summary events.
 *
 * This class holds constants to index the lexicon.dic file.
 * The labels and grouping might not be accurate, but bring some order to the wide spread values.
 * In UE they might be used to index fixed game values by name rather than by id.
 */
public abstract class LexNotificationHeaderIdx {
  public abstract class App {
    public static final int CDRequired = 157;         // "CD-ROM required"
    public static final int NoCD = 830;               // "NO CD-ROM"
    public static final int DIRECT_SOUND_ERROR = 290;
    public static final int MSS_SOUND_ERROR = 291;
    public static final int ConnectionError = 182;
    public static final int Failed = 316;
  }

  public abstract class GalaxyMap {
    public static final int NO_RELATIONSHIP = 832;    // confirmed by value change: notifications 315|856|831 -> "NO DIPLOMATIC RELATIONSHIP"
  }

  public abstract class System {
    public static final int CREDITS_MISSING = 259;    // "CREDITS MISSING IN %s"
  }
}
