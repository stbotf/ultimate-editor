package ue.edit.res.stbof.files.dic.idx;

/**
 * Detailed turn event descriptions shown by popup notifications.
 *
 * Note, in stbof.ini there are three filter options: ECONFILTER, EVENTFILTER and MILITARYFILTER.
 * None of them seems to have any effect.
 *
 * This class holds constants to index the lexicon.dic file.
 * The labels and grouping might not be accurate, but bring some order to the wide spread values.
 * In UE they might be used to index fixed game values by name rather than by id.
 */
public abstract class LexEventDetailsIdx {
  public abstract class Galactic {
    // EXPAND_TERRITORY 484                                   -> confirmed by value change: summary 679|680|681|682|683
    public static final int SystemColonyFounded = 495;        // confirmed by value change, first line: "System %s colony is founded"
    public static final int ColonizedSystem = 813;            // confirmed by value change, details:    "You have colonized a new system..."
    // XLOST 490                                              -> confirmed by value change: summary 1128|1143|1144
    public static final int ShipLostInWormhole = /*1128*/ LexEventIdx.Galactic.ShipLostInWormhole;    // confirmed by value change
    public static final int ShipsLostInWormhole = /*1143*/ LexEventIdx.Galactic.ShipsLostInWormhole;  // confirmed by value change

    // INVASION_SUCCESS 486                                   -> confirmed by value change: summary 1044
    public static final int SystemConquered = 521;            // confirmed by value change, first line: "System %1 taken from %2."
    // FORCES_SUFFER_DEFEAT 497                               -> confirmed by value change: no summary
    public static final int SystemRepelsInvasion = 501;       // confirmed by value change, first line: "System %1 repels the %2 invasion."
    public static final int TransportShipLost = 523;          // confirmed by value change: FORCES_SUFFER_DEFEAT 497 -> "1 transport ship lost in the attempt."
    public static final int TransportShipsLost = 524;         // "%d transport ships lost in the attempt."
    // SIEGE_SYSTEM 520                                       -> confirmed by value change: summary 672
    public static final int PlanetaryDefenseLoss = 514;       // confirmed by value change: "Planetary defense in %s destroyed 1 ship."
    public static final int PlanetaryDefenseLosses = 515;     // confirmed by value change: SIEGE_SYSTEM 520 -> "Planetary defense in %1 destroyed %d2 of %d3 ships."
    // RaceDetected 667                                       -> confirmed by value change: Borg encounter, 494|920
    public static final int PeopleKilled = 920;               // confirmed by value change: RaceDetected 667 -> "%d million people were killed."
    // - shared invasion & siege result list 486|497|520|667
    public static final int NoAttackingShipsDestroyed = 506;  // confirmed by value change: INVASION_SUCCESS 486, SIEGE_SYSTEM 520 -> "No attacking ships destroyed."
    public static final int XDestroyed = 502;                 // confirmed by value change: orbital batteries "%s destroyed"
    public static final int XOfDestroyed = 504;               // confirmed by value change: SIEGE_SYSTEM 520 -> "%d1 of %d2 %3 destroyed."
    public static final int BombedStructuresDestroyed = 505;  // confirmed by value change: INVASION_SUCCESS 486, FORCES_SUFFER_DEFEAT 497, SIEGE_SYSTEM 520 -> "%d1 of %d2 structures destroyed by bombardment."
    public static final int BombedStructureDestroyed = 517;   // "1 structure destroyed by bombardment."
    public static final int DeployedTroop = 493;              // confirmed by value change: INVASION_SUCCESS 486 -> "One transport deployed troops to maintain order."
    public static final int DeployedTroops = 525;             // "%d transports deployed troops to maintain order."
    public static final int BombedPopulationKilled = 508;     // confirmed by value change: INVASION_SUCCESS 486, FORCES_SUFFER_DEFEAT 497, SIEGE_SYSTEM 520 -> "%d million population killed by bombardment."
    public static final int AllLifeOnDestroyed = 494;         // confirmed by value change: SIEGE_SYSTEM 520, RaceDetected 667 -> "All life on %s destroyed."
    public static final int SiegeSystem = 1046;               // "Siege of %s system"

    // RaidSystem 1045                                        -> confirmed by value change: summary 1042
    public static final int RaidSystem = /*1045*/ LexEventHeaderIdx.Galactic.RaidSystem; // confirmed by value change: first line -> "%s1 raid %2 system"
    public static final int CreditsStolen = 496;              // confirmed by value change: second row, shared with galaxy tooltips -> "%d credits stolen."
    // - optional raid list 1045
    public static final int ScannersHinderedRaid = 509;       // "%s and scanners hindered raid operations"
    public static final int AllTradeIncomeSeized = 499;       // confirmed by value change: opposite of 500 -> "All trade income from %s was seized."
    public static final int WereDetected = 498;               // confirmed by value change: "%d %s were detected"
    public static final int OneDetected = 503;                // "1 %s was detected"
    public static final int HinderedRaid = 510;               // confirmed by value change: "%s hindered raid"
  }

  public abstract class System {
    // POWER_OUTAGE 849                                       -> confirmed by value change, summary 1025
    public static final int PowerOutage_1 = 850;              // confirmed by value change: "Insufficient energy in %s system.  Structures have been shut down."
    public static final int PowerOutage_2 = 614;              // "One of your colonies is producing insufficient energy..."
    // PLAGUE_STRIKE 908                                      -> confirmed by value change, summary 1029
    public static final int PlagueStrike = 909;               // confirmed by value change: "Plague has stricken the citizens of %1.  The death toll is estimated at %d2 million."
    // STARVATION 1175
    public static final int Starvation = 1177;                // "One of your systems has insufficient food to support its current population..."
  }

  public abstract class Intel {
    // UrgentIntelligenceReport 485                           -> confirmed by value change, summary not from lexicon
    public static final int Source = 1162;                    // confirmed by value change: first row
    public static final int Unknown = /*1393*/ LexDataIdx.Shared.Unknown;  // confirmed by value change: first row
    public static final int Target = 1250;                    // confirmed by value change: second row
    public static final int Location = 610;                   // confirmed by value change: third row
    public static final int NotAvailable = /*798*/ LexDataIdx.Shared.NotAvailable; // confirmed by value change: third row -> "n/a"
  }

  public abstract class Diplomatic {
    // PEACE_SIGNED 487                                       -> confirmed by value change, summary 7
    public static final int XMembershipSigned = 637;          // "%s1 joined %2"
    public static final int XAllianceSigned = 26;             // "%s1 signed an alliance with %2"
    public static final int XAffiliationSigned = 13;          // confirmed by value change, first line:   "%s1 signed an affiliation treaty with %2"
    public static final int XFriendshipSigned  = 387;         // confirmed by value change: "%s1 signed a friendship treaty with %2"
    public static final int XNonAggressionSigned = 870;       // confirmed by value change, first line:   "%s1 signed a non-aggression treaty with %2"
    public static final int MinorMembership = 638;            // "You have signed a membership agreement with a minor race..."
    public static final int AllianceSigned = 28;              // "You have signed an alliance with another major power..."
    public static final int AffilitionSigned = 14;            // "You have signed an affiliation treaty with another race..."
    public static final int FriendshipSigned = 381;           // "You have signed a friendship agreement with another race..."
    public static final int NonAggressionSigned = 860;        // confirmed by value change, second line:  "You have signed a Non-Aggression agreement with another race..."
    // Contact
    public static final int MajorContact = 616;               // "You have made First Contact with another major power..."
    public static final int MinorContact = 645;               // "You have made First Contact with a minor race..."
    // WAR_DECLARED 1422                                                                  -> confirmed by value change: summary 1421
    public static final int XWarDeclared = /*1421*/ LexEventIdx.Diplomatic.XWarDeclared;  // confirmed by value change: "%s1 declare war against %2"

    // relationship strains
    // NEGATIVE_PROPAGANDA 1193
    public static final int NegativePropaganda = 1194;        // "A prominent citizen has publicly questioned our motives... %1 %2."
    // MINOR_DIPLOMATIC_BLUNDER 1195                          -> confirmed by value change
    public static final int MinorDiplomaticBlunder = 1196;    // confirmed by value change: "A member of our diplomatic corps failed to observe proper etiquette... %1 %2."
    // NEGATIVE_POLITICAL_SHIFT 1197
    public static final int NegativePoliticalShift = 1198;    // "The government of the %1 has changed... %2."
    // MAJOR_DIPLOMATIC_BLUNDER 1199
    public static final int MajorDiplomaticBlunder = 1200;    // "A member of our diplomatic corps inadvertently forwarded a very insulting letter... %1 %2."
    // ASSASSINATION_ATTEMPT 1201
    public static final int AssassinationAttempt = 1202;      // "A prominent leader of %1 was nearly assassinated... %2."

    // relationship backings
    // POSITIVE_PROPAGANDA 1203
    public static final int PositivePropaganda = 1204;        // "A prominent citizen has publicly supported our policies... %1 %2."
    // PRIVATE_GIFT 1205
    public static final int PrivateGift = 1206;               // "One of our most wealthy citizens offered a substantial gift... %1 %2."
    // POSITIVE_POLITICAL_SHIFT 1207
    public static final int PositivePoliticalShift = 1208;    // "The leadership of %1 has changed... %2."
    // RESEARCHER_HONORED 1209
    public static final int ResearcherHonored = 1210;         // "One of our xenobiologists has earned the praise of %1 ... %2."
    // DISPUTE_MEDIATED 1211
    public static final int DisputeMediated = 1212;           // "A serious governmental crisis among %1 has been mediated... %2."
  }
}