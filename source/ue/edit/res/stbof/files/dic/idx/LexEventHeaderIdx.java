package ue.edit.res.stbof.files.dic.idx;

/**
 * The topics of turn event popup notifications.
 *
 * Note, in stbof.ini there are three filter options: ECONFILTER, EVENTFILTER and MILITARYFILTER.
 * None of them seems to have any effect.
 *
 * This class holds constants to index the lexicon.dic file.
 * The labels and grouping might not be accurate, but bring some order to the wide spread values.
 * In UE they might be used to index fixed game values by name rather than by id.
 */
public abstract class LexEventHeaderIdx {
  public abstract class Global {
    public static final int EMPIRE_CEDED = 266;
    public static final int Defeated = 215;
    public static final int Victorious = 1410;
  }

  public abstract class Random {
    // global events
    public static final int ALIEN_ARTIFACTS_DISCOVERED = 40;
    public static final int NAMED_BENEFACTOR = 89;              // "%u1 NAMED BENEFACTOR"
    public static final int RESEARCH_SETBACK = 998;             // "%s RESEARCH SETBACK"
    public static final int HULL_DEGRADING_VIRUS_DETECTED = 1123;
    public static final int SPACE_DISTORTION = 1165;            // "FABRIC OF SPACE DISTURBED"
    // system events
    public static final int COMET_STRIKE_1 = 170;               // "COMET STRIKES IN %s"
    public static final int COMET_STRIKE_2 = 175;               // "COMET STRIKES IN %s"
    public static final int EARTHQUAKE_1 = 253;                 // "EARTHQUAKE IN %s SYSTEM"
    public static final int Earthquake_2 = 1018;                // "Earthquake in %s system"
    public static final int ENERGY_MELTDOWN_1 = 284;            // "ENERGY MELTDOWN IN %s SYSTEM"
    public static final int EnergyMeltdown_2 = 1019;            // "Energy Meltdown in %s system"
    public static final int IMMIGRATION_BOOM = 481;             // "IMMIGRATION BOOM IN %s"
    public static final int NOVA = 876;                         // "NOVA IN %s"
    public static final int NEGATIVE_PLANET_SHIFT = 910;        // "NEGATIVE PLANET SHIFT in %s"
    public static final int POSITIVE_PLANET_SHIFT = 912;        // "POSITIVE PLANET SHIFT in %s"
    // guild strikes
    public static final int TRADE_GUILD_STRIKE = 1266;          // "STACIUS TRADE GUILD STRIKE"
    // relationship strains
    public static final int NEGATIVE_PROPAGANDA = 1193;         // details 1194
    public static final int MINOR_DIPLOMATIC_BLUNDER = 1195;    // confirmed by value change: details 1196
    public static final int NEGATIVE_POLITICAL_SHIFT = 1197;    // details 1198
    public static final int MAJOR_DIPLOMATIC_BLUNDER = 1199;    // details 1200
    public static final int ASSASSINATION_ATTEMPT = 1201;       // details 1202
    // relationship backings
    public static final int POSITIVE_PROPAGANDA = 1203;         // details 1204
    public static final int PRIVATE_GIFT = 1205;                // details 1206
    public static final int POSITIVE_POLITICAL_SHIFT = 1207;    // details 1208
    public static final int RESEARCHER_HONORED = 1209;          // details 1210
    public static final int DISPUTE_MEDIATED = 1211;            // details 1212
  }

  public abstract class Galactic {
    // GALACTIC
    public static final int EXPAND_TERRITORY = 484;             // confirmed by value change: summary 679|680|681|682|683 -> "%s EXPAND TERRITORY"
    public static final int INVASION_SUCCESS = 486;             // confirmed by value change: summary 1044, details 521|506|505|493|508
    public static final int SIEGE_SYSTEM = 520;                 // confirmed by value change: critics 669|671|670|672|673, details 506|504|505|508|494|515 -> "%1 SIEGE %2 SYSTEM"
    public static final int LIBERATION = 604;
    public static final int SuccessfulLiberation = 1188;
    public static final int SYSTEM_LOST = 491;                  // "%s SYSTEM LOST"
    public static final int TERRAFORM_COMPLETED = 492;
    public static final int FORCES_SUFFER_DEFEAT = 497;         // confirmed by value change: no summary, details 501|523|505|508 -> "%s FORCES SUFFER DEFEAT"
    public static final int RaceDetected = 667;                 // confirmed by value change: borg 98 encounter, details 494|920 -> "%s detected in %s system"
    // SHIPS
    public static final int DAMAGED_IN_NEUTRON_STAR = 489;      // "%s DAMAGED IN NEUTRON STAR"
    // @see Data.Galaxy.StellarObjects
    public static final int XLOST = 490;                                          // confirmed by value change: "%1 LOST IN %2"
    public static final int Ship = /*1119*/ LexMenuIdx.Shared.Ship;               // confirmed by value change: XLOST
    public static final int Ships = /*1139*/ LexDataIdx.Combat.VesselType.Ships;  // confirmed by value change: XLOST
    // OTHER
    public static final int Range = 973;
    public static final int RaidSystem = 1045;                  // confirmed by value change: summary: 1042 -> "%s1 raid %2 system"
  }

  public abstract class System {
    public static final int POWER_OUTAGE = 849;                 // confirmed by value change: summary 1025, details 850 -> "POWER OUTAGE IN %s"
    public static final int PLAGUE_STRIKE = 908;                // confirmed by value change: summary 1029, details 909 -> "PLAGUE STRIKES %s"
    public static final int STARVATION = 1175;                  // "STARVATION IN %S SYSTEM"
    public static final int REBELLION = 978;                    // "REBELLION IN %s!"
    public static final int TRADE = 1363;
  }

  public abstract class Intel {
    public static final int ASSASSINATION = 43;
    public static final int ExternalIntelligence = 313;
    public static final int UrgentIntelligenceReport = 485;     // confirmed by value change: summary not from lexicon, details 1162|1250|610
    public static final int CreditDeficit = 976;
    public static final int RESEARCH_THEFT = 1000;              // "%s RESEARCH THEFT"
    public static final int TERRORISTS_STRIKE = 1262;           // "TERRORISTS STRIKE %s"
  }

  public abstract class Research {
    public static final int Breakthrough = 488;                 // confirmed by value change: "%s breakthrough in %s", summary 1017, no details entry
  }

  public abstract class Diplomatic {
    public static final int AgreedPeaceTreaty = 18;
    public static final int DefeatedPeaceTreaty = 216;
    public static final int ALLIANCE_BROKEN = 22;
    public static final int ALLIANCE_SIGNED = 27;
    public static final int PEACE_SIGNED_1 = 487;               // confirmed by value change: summary 7, details 13|387|860|870
    public static final int PEACE_SIGNED_2 = 901;
    public static final int CREDIT = 191;
    public static final int Greetings = 421;                    // "Greetings!!"
    public static final int MakeStatement = 618;                // "Make Statement"
    public static final int NewStatement = 823;                 // "New Statement"
    public static final int Statement = 1178;
    public static final int WAR_DECLARED = 1422;                // confirmed by value change: summary 1421, 688|..., details 1421
  }

   // greetings and info displayed on encounter
  public abstract class RaceEncounter {
    public static final int GreetingsFromCardassians = 123;     // confirmed by value change: "Greetings from the Cardassian Union..."
    public static final int GreetingsFromFederation = 321;      // confirmed by value change: "Greetings from the United Federation of Planets..."
    public static final int GreetingsFromFerengi = 325;         // confirmed by value change: "Greetings from the Ferengi Alliance..."
    public static final int GreetingsFromKlingons = 561;        // confirmed by value change: "Greetings from the Klingon Empire..."
    public static final int GreetingsFromRomulans = 1059;       // confirmed by value change: "Greetings from the Romulan Star Empire..."

    public abstract class ByCard {
      public static final int Acamarians = 126;   // "DIPLOMATIC REPORT:  The Acamarians are technologically advanced,..."
      public static final int Andorians = 127;    // "DIPLOMATIC REPORT:  The Andorians are strong and cunning warriors..."
      public static final int Angosians = 128;    // "DIPLOMATIC REPORT:  The Angosians use enhanced soldiers for defense..."
      public static final int Antedeans = 129;    // "DIPLOMATIC REPORT:  The Antedeans are too weak to survive space travel..."
      public static final int Anticans = 130;     // "DIPLOMATIC REPORT:  The Anticans are spirited warriors of limited intelligence..."
      public static final int Bajorans = 131;     // "DIPLOMATIC REPORT:  The Bajorans guard their independence fiercely..."
      public static final int Bandi = 132;        // "DIPLOMATIC REPORT:  The Bandi have a talent for architecture..."
      public static final int Benzites = 133;     // "DIPLOMATIC REPORT:  The Benzites are motivated and efficient workers..."
      public static final int Betazoids = 134;    // "DIPLOMATIC REPORT:  The Betazoids have telepathic abilities..."
      public static final int Bolians = 135;      // "DIPLOMATIC REPORT:  The Bolians have a talent for cosmetic surgery..."
      public static final int Bynars = 136;       // "DIPLOMATIC REPORT:  The Bynars live in symbiosis with their computers..."
      public static final int Caldonians = 137;   // "DIPLOMATIC REPORT:  The Caldonians are fanatical researchers, and poor warriors..."
      public static final int Chalnoth = 138;     // "DIPLOMATIC REPORT:  The Chalnoth are strong warriors, but easily manipulated..."
      public static final int Edo = 139;          // "DIPLOMATIC REPORT:  The Edo live only for pleasure..."
      public static final int Ktarians = 140;     // "DIPLOMATIC REPORT:  The Ktarians are cunning saboteurs..."
      public static final int Malcorians = 141;   // "DIPLOMATIC REPORT:  The Malcorians are concerned about the effects of alien contact..."
      public static final int Mintakans = 142;    // "DIPLOMATIC REPORT:  The Mintakans are a wholly agrarian society..."
      public static final int Mizarians = 143;    // "DIPLOMATIC REPORT:  The Mizarians are pacifists, afraid to defend themselves..."
      public static final int Nausicaans = 144;   // "DIPLOMATIC REPORT:  The Nausicaans are sturdy warriors..."
      public static final int Pakleds = 145;      // "DIPLOMATIC REPORT:  The Pakleds do not understand the technology they have stolen..."
      public static final int Selay = 146;        // "DIPLOMATIC REPORT:  The Selay are spirited warriors of limited intelligence..."
      public static final int Sheliak = 147;      // "DIPLOMATIC REPORT:  The Sheliak believe themselves superior to us..."
      public static final int Takarans = 148;     // "DIPLOMATIC REPORT:  The Takarans are incredibly resilient, and highly intelligent..."
      public static final int Talarians = 149;    // "DIPLOMATIC REPORT:  The Talarians are fierce warriors..."
      public static final int Tamarians = 150;    // "DIPLOMATIC REPORT:  The Tamarians speak unintelligibly, but fight bravely..."
      public static final int Trill = 151;        // "DIPLOMATIC REPORT:  The Trill enjoy long lives..."
      public static final int Ullians = 152;      // "DIPLOMATIC REPORT:  The Ullians have an insight into alien psychology..."
      public static final int Vulcans = 153;      // "DIPLOMATIC REPORT:  The Vulcans are disciplined thinkers..."
      public static final int Yridians = 154;     // "DIPLOMATIC REPORT:  The Yridians are peddlers of information..."
      public static final int Zakdorn = 155;      // "DIPLOMATIC REPORT:  The Zakdorn have an incredible genius for strategy..."
    }

    public abstract class ByFed {
      public static final int Acamarians = 446;   // "DIPLOMATIC REPORT:  The Acamarians wish to preserve their social structure.  We must take steps to ensure that they can."
      public static final int Andorians = 447;    // "DIPLOMATIC REPORT:  The Andorians have a bloody past, but desire a more peaceful future -- much like mankind."
      public static final int Angosians = 448;    // "DIPLOMATIC REPORT:  The Angosians have a nearly perfect society, but they pay a high price.  Their enhanced soldiers deserve social as well as military respect."
      public static final int Antedeans = 449;    // "DIPLOMATIC REPORT:  The Antedeans have advanced technology, but do not travel in space.  They find it 'uncomfortable'."
      public static final int Anticans = 450;     // "DIPLOMATIC REPORT:  The Anticans are not as advanced as they would have us believe.  An important first step would be a resolution of their conflict with the Selay."
      public static final int Bajorans = 451;     // "DIPLOMATIC REPORT:  The Bajoran Jalanda Forum is a model of open discourse, which can only improve relations between Federation members."
      public static final int Bandi = 452;        // "DIPLOMATIC REPORT:  Despite their lack of warp technology, the Bandi use methods of construction that compare favorably to our own."
      public static final int Benzites = 453;     // "DIPLOMATIC REPORT:  The Benzite culture values industry and efficiency.  They would be welcome members of the Federation."
      public static final int Betazoids = 454;    // "DIPLOMATIC REPORT:  Betazoid telepathy is an asset that our rivals may try to exploit.  We should offer Betazed our protection."
      public static final int Bolians = 455;      // "DIPLOMATIC REPORT:  The Bolian talent for cosmetic alteration will be coveted by enemy intelligence agents."
      public static final int Bynars = 456;       // "DIPLOMATIC REPORT:  The Bynars have an understanding of computers that could revolutionize our research."
      public static final int Caldonians = 457;   // "DIPLOMATIC REPORT:  The Caldonians are disciplined researchers, and share our love of knowledge for its own sake."
      public static final int Chalnoth = 458;     // "DIPLOMATIC REPORT:  The Chalnoth are a brutal race, and should be approached with caution and respect."
      public static final int Edo = 459;          // "DIPLOMATIC REPORT:  The Edo enjoy an idyllic existence, but their legal system is a high price to pay for their bliss."
      public static final int Ktarians = 460;     // "DIPLOMATIC REPORT:  The Ktarians are a very intelligent and cunning people.  It remains to be seen whether they are indeed trustworthy."
      public static final int Malcorians = 461;   // "DIPLOMATIC REPORT:  Malcorian society suffers from xenophobic tendencies, but technologically they are nearly capable of warp travel."
      public static final int Mintakans = 462;    // "DIPLOMATIC REPORT:  The Mintakans are primitive, but remarkably talented in agriculture.  The Prime Directive compels us to protect them from alien interference."
      public static final int Mizarians = 463;    // "DIPLOMATIC REPORT:  The Mizarians are absolute pacifists.  Apparently, this is the main reason that they've survived the last few centuries."
      public static final int Nausicaans = 464;   // "DIPLOMATIC REPORT:  The Nausicaans are a proud and strong race.  They are always at war, but never have a shortage of new recruits."
      public static final int Pakleds = 465;      // "DIPLOMATIC REPORT:  The Pakleds are a shining example of what happens when the Prime Directive is ignored.  However, their skill at obtaining technology is to be admired."
      public static final int Selay = 466;        // confirmed by value change: "DIPLOMATIC REPORT:  The Selay are not as advanced as they would have us believe.  An important first step would be a resolution of their conflict with the Anticans."
      public static final int Sheliak = 467;      // "DIPLOMATIC REPORT:  The Sheliak interpret their treaties very strictly.  We should exercise extreme caution in making any agreements with them."
      public static final int Takarans = 468;     // "DIPLOMATIC REPORT:  The Takarans are notable for their proficiency in theoretical physics, as well as their unusual biochemistry."
      public static final int Talarians = 469;    // "DIPLOMATIC REPORT:  The Talarians are skilled warriors, and highly territorial.  We should approach them with caution and respect."
      public static final int Tamarians = 470;    // "DIPLOMATIC REPORT:  The Tamarians appear to have ideals similar to our own.  If we can overcome the language barrier, we will both profit from this contact."
      public static final int Trill = 471;        // "DIPLOMATIC REPORT:  The joined Trill live several times as long as the oldest humans.  Their wisdom could be a great asset to the Federation."
      public static final int Ullians = 472;      // "DIPLOMATIC REPORT:  The Ullians act as historians for many alien cultures.  Their knowledge of this galaxy would be of enormous academic interest."
      public static final int Vulcans = 473;      // "DIPLOMATIC REPORT:  The Vulcans have suppressed their emotions in favor of pure logic.  Their clarity of thought would be an asset to our growing Federation."
      public static final int Yridians = 474;     // "DIPLOMATIC REPORT:  The Yridians survive by selling secret information.  We must be extremely cautious and circumspect in dealing with them."
      public static final int Zakdorn = 475;      // "DIPLOMATIC REPORT:  The Zakdorn talent for strategy cannot be overstated.  Their genius could be a great boon to our officer training programs."
    }

    public abstract class ByFerg {
      public static final int Acamarians = 327;   // "DIPLOMATIC REPORT:  The Acamarians have warp technology... so they must have something of value."
      public static final int Andorians = 328;    // "DIPLOMATIC REPORT:  The Andorians are experienced warriors.  If we acquire them, they could train our crews!"
      public static final int Angosians = 329;    // "DIPLOMATIC REPORT:  The Angosians have one very valuable asset... their soldiers!"
      public static final int Antedeans = 330;    // "DIPLOMATIC REPORT:  The Antedeans won't leave their home system... we could monopolize their export industry!"
      public static final int Anticans = 331;     // "DIPLOMATIC REPORT:  The Anticans will buy almost anything, as long as they can use it against the Selay..."
      public static final int Bajorans = 332;     // "DIPLOMATIC REPORT:  The Bajorans are deeply religious.  Trade with 'em, but don't trust their ale."
      public static final int Bandi = 333;        // "DIPLOMATIC REPORT:  If we can acquire the Bandi, we can use their architectural designs in our new structures!"
      public static final int Benzites = 334;     // "DIPLOMATIC REPORT:  The Benzites are very efficient workers.  They would be an asset to the Ferengi economy!"
      public static final int Betazoids = 335;    // "DIPLOMATIC REPORT:  The Betazoids can read minds, but apparently they can't read ours... we could make a fortune here!"
      public static final int Bolians = 336;      // "DIPLOMATIC REPORT:  Bolian cosmetics are truly revolutionary.  All they need is a good market strategy."
      public static final int Bynars = 337;       // "DIPLOMATIC REPORT:  The Bynars have incredibly advanced computer theories.  They could be very valuable."
      public static final int Caldonians = 338;   // "DIPLOMATIC REPORT:  The Caldonians enjoy research for its own sake.  They're not much use economically."
      public static final int Chalnoth = 339;     // "DIPLOMATIC REPORT:  The Chalnoth are savage and hostile.  Trading with them would be dangerous."
      public static final int Edo = 340;          // "DIPLOMATIC REPORT:  The Edo have nothing but leisure time.  They're the ultimate consumers!"
      public static final int Ktarians = 341;     // "DIPLOMATIC REPORT:  Ktarian games are highly addictive.  We could make a fortune as their sole distributor."
      public static final int Malcorians = 342;   // "DIPLOMATIC REPORT:  The Malcorians don't want their people to know about aliens.  It's a tough market to crack, but there won't be much competition..."
      public static final int Mintakans = 343;    // "DIPLOMATIC REPORT:  The Mintakans are hard-working and simple.  That's what I call a perfect employee!"
      public static final int Mizarians = 344;    // "DIPLOMATIC REPORT:  The Mizarians won't defend themselves if we attack 'em.  How can we possibly go wrong?"
      public static final int Nausicaans = 345;   // "DIPLOMATIC REPORT:  The Nausicaans are big, strong, and mean.  And even worse, they're practically broke!"
      public static final int Pakleds = 346;      // "DIPLOMATIC REPORT:  The Pakleds don't even understand the technology they have... which makes them very exploitable customers."
      public static final int Selay = 347;        // "DIPLOMATIC REPORT:  The Selay will buy almost anything, as long as they can use it against the Anticans..."
      public static final int Sheliak = 348;      // "DIPLOMATIC REPORT:  If we want to trade with the Sheliak, we'll have to find a way to get their attention."
      public static final int Takarans = 349;     // "DIPLOMATIC REPORT:  The Takaran physiology is unlike any other.  They have unique needs... which we may be able to exploit."
      public static final int Talarians = 350;    // "DIPLOMATIC REPORT:  Talarian defenses are almost impenetrable.  I recommend a few large bribes."
      public static final int Tamarians = 351;    // "DIPLOMATIC REPORT:  The Tamarians have a strange language, but they have plenty of latinum.  Surely, we can make some kind of a deal..."
      public static final int Trill = 352;        // "DIPLOMATIC REPORT:  The Trill are friendly and civil, but they have long memories.  We must treat them with caution."
      public static final int Ullians = 353;      // "DIPLOMATIC REPORT:  The Ullians can read our minds.  This is not a desirable trait among customers."
      public static final int Vulcans = 354;      // "DIPLOMATIC REPORT:  The Vulcans don't have any emotions at all.  They'll be tough customers."
      public static final int Yridians = 355;     // "DIPLOMATIC REPORT:  The Yridians sell information... a partnership would be very profitable."
      public static final int Zakdorn = 356;      // "DIPLOMATIC REPORT:  The Zakdorn may be willing to share their tactical expertise... for the right price."
    }

    public abstract class ByKling {
      public static final int Acamarians = 564;   // "DIPLOMATIC REPORT:  The Acamarians are cunning, but not strong.  We can defeat them."
      public static final int Andorians = 565;    // "DIPLOMATIC REPORT:  The Andorians are great warriors.  They will make fine opponents and finer allies."
      public static final int Angosians = 566;    // "DIPLOMATIC REPORT:  The Angosians use science to create their warriors... but true warriors are born, not made."
      public static final int Antedeans = 567;    // "DIPLOMATIC REPORT:  The Antedeans are too weak to travel in space.  They are not even worthy to be our enemies."
      public static final int Anticans = 568;     // "DIPLOMATIC REPORT:  With proper training, the Anticans could become skilled warriors."
      public static final int Bajorans = 569;     // "DIPLOMATIC REPORT:  Beneath their soft exterior, the Bajorans have strong hearts.  They can be an inspiration to our people."
      public static final int Bandi = 570;        // "DIPLOMATIC REPORT:  The Bandi are too cowardly to pose any serious threat to us."
      public static final int Benzites = 571;     // "DIPLOMATIC REPORT:  The Benzites are hard workers, but poor fighters."
      public static final int Betazoids = 572;    // "DIPLOMATIC REPORT:  The Betazoids are weak, but their telepathy can make them dangerous."
      public static final int Bolians = 573;      // "DIPLOMATIC REPORT:  The Bolians are not warriors.  They are of no importance to us."
      public static final int Bynars = 574;       // "DIPLOMATIC REPORT:  The Bynars have advanced computers, but poor defenses."
      public static final int Caldonians = 575;   // "DIPLOMATIC REPORT:  The Caldonians are researchers.  There is not a warrior among them."
      public static final int Chalnoth = 576;     // "DIPLOMATIC REPORT:  The Chalnoth are primitive, but their hearts are strong and their weapons are impressive."
      public static final int Edo = 577;          // "DIPLOMATIC REPORT:  The Edo live only for pleasure.  They are of no importance."
      public static final int Ktarians = 578;     // "DIPLOMATIC REPORT:  The Ktarians fight with devious tricks and diversions.  They have no honor, but they are dangerous."
      public static final int Malcorians = 579;   // "DIPLOMATIC REPORT:  The Malcorians wish to remain isolated from alien contact.  Cowards!"
      public static final int Mintakans = 580;    // "DIPLOMATIC REPORT:  The Mintakans are primitive, but they are not fools.  Watch them carefully."
      public static final int Mizarians = 581;    // "DIPLOMATIC REPORT:  The Mizarians will not defend themselves. They deserve no respect."
      public static final int Nausicaans = 582;   // "DIPLOMATIC REPORT:  The Nausicaans are mighty, but they have no cunning.  We will defeat them."
      public static final int Pakleds = 583;      // "DIPLOMATIC REPORT:  The Pakleds are weak fools with a talent for stealing alien technology.  They are dangerous only to other fools."
      public static final int Selay = 584;        // "DIPLOMATIC REPORT:  With proper training, the Selay could become skilled warriors."
      public static final int Sheliak = 585;      // "DIPLOMATIC REPORT:  The Sheliak are strong, but they are not Klingons!  We will make them respect us."
      public static final int Takarans = 586;     // "DIPLOMATIC REPORT:  The Takarans have strength in both mind and body.  They could be worthy allies."
      public static final int Talarians = 587;    // "DIPLOMATIC REPORT:  The Talarians are cunning warriors, yet they fight with honor.  Invasion will be difficult, but they may come to see us as allies."
      public static final int Tamarians = 588;    // "DIPLOMATIC REPORT:  The Tamarians make no sense when they speak, but their weapons speak clearly enough."
      public static final int Trill = 589;        // "DIPLOMATIC REPORT:  The Trill are fragile, but their frailty hides an inner strength.  Their longevity leads to great wisdom, and that wisdom gives them power."
      public static final int Ullians = 590;      // "DIPLOMATIC REPORT:  The Ullians are not warriors, but we must be wary of their mental powers."
      public static final int Vulcans = 591;      // "DIPLOMATIC REPORT:  The Vulcans are peaceful, but only by choice.  They can be powerful warriors if the need arises."
      public static final int Yridians = 592;     // "DIPLOMATIC REPORT:  The Yridians are cowards who sell secrets to other cowards.  We must teach them the price of cowardice."
      public static final int Zakdorn = 593;      // "DIPLOMATIC REPORT:  The Zakdorn reputation for strategy is not undeserved.  Treat them with caution."
    }

    public abstract class ByRom {
      public static final int Acamarians = 1062;  // "DIPLOMATIC REPORT:  The Acamarians have discovered the warp drive. They may be of some use to us."
      public static final int Andorians = 1063;   // "DIPLOMATIC REPORT:  The Andorians have a long and violent history.  Their tactical expertise could be of value."
      public static final int Angosians = 1064;   // "DIPLOMATIC REPORT:  The Angosians use innovative techniques to prepare their soldiers.  We could make use of this knowledge."
      public static final int Antedeans = 1065;   // "DIPLOMATIC REPORT:  The Antedeans have little of value.  We can be sure that they will not intrude in our space."
      public static final int Anticans = 1066;    // confirmed by value change: "DIPLOMATIC REPORT:  The Anticans know nothing but their hatred of the Selay.  They are of no use to us, unless their hatred can be redirected."
      public static final int Bajorans = 1067;    // confirmed by value change: "DIPLOMATIC REPORT:  The Bajorans have a strong spirit that reflects our own.  They could be worthy allies, one day."
      public static final int Bandi = 1068;       // "DIPLOMATIC REPORT:  The Bandi have a gift for architecture which would aid our research."
      public static final int Benzites = 1069;    // "DIPLOMATIC REPORT:  The Benzites have an impressive work ethic;  // they are highly productive laborers."
      public static final int Betazoids = 1070;   // "DIPLOMATIC REPORT:  Betazoid empathy could be of use to our intelligence forces."
      public static final int Bolians = 1071;     // "DIPLOMATIC REPORT:  The Bolians have a skill in cosmetic surgery which could assist our external agents."
      public static final int Bynars = 1072;      // confirmed by value change: "DIPLOMATIC REPORT:  The Bynars have a gift for computer research which could be made to serve our needs."
      public static final int Caldonians = 1073;  // "DIPLOMATIC REPORT:  The Caldonians care only for research.  If we can protect them, they will greatly enhance our intellectual resources."
      public static final int Chalnoth = 1074;    // "DIPLOMATIC REPORT:  The Chalnoth are brutal and savage, and do not have warp drive.  They are probably best left alone."
      public static final int Edo = 1075;         // confirmed by value change: "DIPLOMATIC REPORT:  The Edo live only for pleasure.  In themselves they are useless to us, but their presence would raise the morale of our citizens."
      public static final int Ktarians = 1076;    // "DIPLOMATIC REPORT:  The Ktarians have innovative methods of sabotage which would be of interest to our intelligence operatives."
      public static final int Malcorians = 1077;  // "DIPLOMATIC REPORT:  The Malcorians have unique propulsion theories.  Their ideas could be of use to us."
      public static final int Mintakans = 1078;   // "DIPLOMATIC REPORT:  The Mintakans are an offshoot of our own genetic precursors.  They would work well within our ranks."
      public static final int Mizarians = 1079;   // "DIPLOMATIC REPORT:  The Mizarians have no courage, no conviction, and no dignity.  They care only for their own survival.  They cannot be trusted."
      public static final int Nausicaans = 1080;  // "DIPLOMATIC REPORT:  The Nausicaans are fierce warriors, but mentally inferior.  We could control them easily."
      public static final int Pakleds = 1081;     // "DIPLOMATIC REPORT:  The Pakleds have advanced technology, but it is not of their own making.  We will learn nothing from them, but they are not without their uses."
      public static final int Selay = 1082;       // "DIPLOMATIC REPORT:  The Selay know nothing but their hatred of the Anticans.  They are of no use to us, unless their hatred can be redirected."
      public static final int Sheliak = 1083;     // confirmed by value change: "DIPLOMATIC REPORT:  The Sheliak believe themselves superior to all other races...ourselves included.  It would be a pleasure to prove otherwise."
      public static final int Takarans = 1084;    // "DIPLOMATIC REPORT:  The Takarans have a superior understanding of physics.  Their gifts would enhance our research significantly."
      public static final int Talarians = 1085;   // "DIPLOMATIC REPORT:  The Talarians are capable of defending themselves, but they have little that is worth defending."
      public static final int Tamarians = 1086;   // "DIPLOMATIC REPORT:  The Tamarians are an advanced species.  They will enhance our intellectual resources... if we can communicate with them."
      public static final int Trill = 1087;       // "DIPLOMATIC REPORT:  The symbiotic Trill live for centuries.  Their accumulated knowledge is beyond price."
      public static final int Ullians = 1088;     // "DIPLOMATIC REPORT:  The Ullians have used their telepathy to gather historical data.  This data could be very useful to our intelligence operatives."
      public static final int Vulcans = 1089;     // "DIPLOMATIC REPORT:  The Vulcans are our distant cousins.  They have rejected emotion for pure logic, but perhaps the time has come for reunification."
      public static final int Yridians = 1090;    // "DIPLOMATIC REPORT:  The Yridians trade military secrets for wealth.  If they keep records of the secrets they learn, our intelligence operatives will be very interested."
      public static final int Zakdorn = 1091;     // "DIPLOMATIC REPORT:  The Zakdorn reputation for tactical genius is well-deserved.  We cannot afford to let them join our rivals."
    }
  }
}
