package ue.edit.res.stbof.files.dic.idx;

/**
 * Popup notifications
 * They often have no head lines but are plain message boxes.
 *
 * This class holds constants to index the lexicon.dic file.
 * The labels and grouping might not be accurate, but bring some order to the wide spread values.
 * In UE they might be used to index fixed game values by name rather than by id.
 */
public abstract class LexNotificationIdx {
  public abstract class App {
    public static final int OptionsChange = 0;                    // "The 3d accelerator options have changed..."
    public static final int CDRequired = 156;                     // "Star Trek: Birth of the Federation CD-ROM required"
    public static final int NoCD = 829;                           // "Cannot find the Star Trek: Birth of the Federation CD-ROM"
    public static final int DirectSoundError = 119;               // "Cannot Initialize Direct Sound. Check DirectX Install"
    public static final int MSSSoundError = 120;                  // "Cannot Initialize Miles Sound System Library"
    public static final int ConnectionError = 183;                // "Connection lost for"
    public static final int SynchronizationError = 1213;          // "A synchronization error has occurred..."
    public static final int NotEnoughSpace = 1164;                // "You don't have enough space"
  }

  public abstract class GameSetup {
    public static final int SelectRace = 917;                     // "Please select a race to play"
    public static final int SelectedX = 1457;                     // "You have selected the"
    public static final int ConfirmRaceSelection = 557;           // "Is this the race you wish to play?"
  }

  public abstract class Multiplayer {
    public static final int AllEmpiresTaken = 360;                // "All available races have been either taken or eliminated..."
    public static final int Connecting = 181;
    public static final int ContinueMultiplayer = 189;            // "The previous game was saved as a multiplayer game..."
    public static final int Disconnected = 245;
    public static final int GameAlreadyInProgress = 818;          // "You have chosen a multiplayer game that is already in progress..."
    public static final int GameNameRequired = 806;               // "You must enter a game name before a connection can continue"
    public static final int JoinRequest = 440;                    // "requests permission to join your game.  Will you allow the new player to join?"
    public static final int PasswordIncorrect = 899;              // "Your password is incorrect"
    public static final int PasswordInvalid = 554;                // "Invalid Password"
    public static final int PlayerBecomesHost = 438;              // "The host player has been disconnected.  You have been appointed to be the new host."
    public static final int PlayerDisconnected = 439;             // "The host player has been disconnected"
    public static final int PlayerInCombat = 444;                 // "A human player is in a tactical combat"
    public static final int PlayerNameRequired = 807;             // "You must enter a player name before a connection can continue"
    public static final int SessionFull = 1111;                   // "The session is full"
    public static final int UnknownPlayer = 1395;
    public static final int WaitingForJoinAcceptance = 819;       // "You chose a multiplayer game that is already in progress..."
    public static final int WaitingForPlayers = 441;              // "Another player must join before a game can begin"
    public static final int WaitingForSaveGame = 820;             // "Please wait for the host to transmit the autosave file."
    public static final int WaitingOnEmpireSelection = 1417;      // "All players must select an empire before a game can begin"
    public static final int WaitingOnNewPlayer = 881;             // "A new player is joining the game. Please wait for the new player to start."
    public static final int WaitingOnOrderSubmission = 1419;      // "Waiting for orders submissions from: multiplayer"
    public static final int WaitingOnPlayerCombat = 1418;         // "Waiting for %1 to finish a combat"
  }

  public abstract class GalaxyMap {
    // NO_RELATIONSHIP 832
    public static final int NoTradeAtWar = 315;                   // "Trade is not permitted between parties at war."
    public static final int NoTradeFriendship = 856;              // confirmed by value change: "You must establish a Friendship (or better) treaty with another race in order to trade."
    public static final int NoTradeRelationship = 831;            // confirmed by value change: "You must contact another race in order to trade."
    // system tooltip
    public static final int OrbitalBatteries = 219;               // "Orbital Batteries: %d."
    public static final int OrbitalBatteriesShield = 220;         // "Orbital Batteries: %d.  Shield Generator active."
    // processing turn
    public static final int ProcessingTurn = 1371;                // confirmed by value change
    // other
    public static final int AttackProhibited = 859;               // "You cannot attack this system unless war has been formally declared against %s."
    public static final int CancelMission = 794;                  // confirmed by value change: "Movement will cancel the %s mission for this taskforce."
    public static final int CancelMovement = 655;                 // confirmed by value change: "The selected mission will cancel the movement orders of this task force."
    public static final int CantTerraform = 855;                  // "This planet cannot be terraformed."
    public static final int ColonizationProhibited = 858;         // "You cannot colonize this system unless war has been declared.."
    public static final int ConfirmLiberation = 606;              // "Do you wish to liberate %1 from %2 rule?"
  }

  public abstract class System {
    // CREDITS_MISSING 259
    public static final int InsufficientCredits = 804;            // "Insufficient Credits: need additional %d credits for %s."
    public static final int InsufficientCreditsForUpgrade = 805;  // "Insufficient Credits: need additional %d1 credits for upgrade to %2"
    public static final int MissingCredits = 1149;                // "You can't afford to buy this"
    public static final int Deficit = 1455;                       // "You would be %d credits in deficit."
    // buy current build task
    public static final int ConfirmBuy = 959;                     // confirmed by value change: "Buy %1 for %d2 credits?"
    public static final int DeficitCredits = 1456;                // confirmed by value change: "You have %d credits."
    // full production queue
    public static final int ProductionQueueFull = 925;            // confirmed by value change: "Production Queue is full"
    // scrap structure
    public static final int ScrapX = 961;                         // confirmed by value change: "Scrap %s?"
    public static final int ScrapXNum = 962;                      // confirmed by value change: "Number of %s to scrap?"
    // no martial law effect
    public static final int NoMoraleEffect = 857;                 // confirmed by value change: "This would have no further effect on morale."
    // other
    public static final int CancelPurchase = 236;                 // "Do you wish to cancel this purchase?"
    public static final int CancelBuild = 237;                    // "%d industry invested.  Delete current build?"
    public static final int CannotBuildShip = 118;                // "Cannot build ship;  need ship yard first"
    public static final int ConfirmUpgrade = 960;                 // "Buy upgrade from %1 to %2 for %d3 credits?"
    public static final int ConfirmShipyardShutdown = 963;        // "Shutting down %s may strand ships because of range.  Are you sure?"
  }

  public abstract class Combat {
    public static final int CloakedTurn = 164;                    // confirmed by value change: "Cloaked ships get a free move..."
    public static final int NoTarget = 854;                       // "likely a combat label or error notification"
  }
}
