package ue.edit.res.stbof.files.dic.idx;

/**
 * Descriptive UI labels of the many menus.
 * Excluding the displayed data values.
 *
 * This class holds constants to index the lexicon.dic file.
 * The labels and grouping might not be accurate, but bring some order to the wide spread values.
 * In UE they might be used to index fixed game values by name rather than by id.
 */
public abstract class LexMenuIdx {
  public abstract class Shared {
    public static final int Defense = 218;                    // confirmed by value change: galaxy, system (& likely combat)
    public static final int Turn = 1370;                      // confirmed by value change: galaxy, system, intel, research, diplomacy
    public static final int TURNS = 1372;                     // confirmed by value change: system & diplomacy
    public static final int TURNS_REMAINING = 1373;           // confirmed by value change: system & diplomacy
    public static final int AvailablePointsAndBonuses = 54;   // confirmed by value change: research & empire intel info
    public static final int Status = 1181;                    // confirmed by value change: research & diplomacy
    public static final int MORALE = 668;                     // confirmed by value change: system, galaxy map & empire status
    public static final int System = 1214;                    // confirmed by value change: economics, system panel & system screen
    public static final int Population = 919;                 // confirmed by value change: galaxy tooltip & system panel
    public static final int LABOR_POOL = 595;                 // confirmed by value change: system & galaxy map
    public static final int Ship = 1119;                      // confirmed by value change: galaxy fleet & events
    public static final int TradeGoods = 1364;                // confirmed by value change: build queue, summary & economics
    public static final int YES = 1453;                       // confirmed by value change: options & proposal
    public static final int NO = 828;                         // confirmed by value change: options & proposal
  }

  public abstract class BuildingStats {
    public static final int IndustryBuildCost = 529;          // confirmed by value change: system & database
    public static final int PopulationMaintenanceCost = 921;  // confirmed by value change: system & database
    public static final int EnergyMaintenanceCost = 282;      // confirmed by value change: system & database
    // @see Data.System.Restrictions
    public static final int Restriction = 1005;               // confirmed by value change: system & database
    public static final int Restrictions = 1006;              // confirmed by value change: system & database, if multiple
    public static final int MORALE = /*668*/ Shared.MORALE;   // confirmed by value change: system & database
  }

  public abstract class ShipStats {
    // SYSTEM BUILD & OBJECT DATABASE
    public static final int XClass = 1122;                                      // confirmed by value change: "%s1 Class %2"
    // for short description, see Data.SpaceCraft.ShipCategoryDescriptions
    public static final int CanCloak = 116;                                     // confirmed by value change: galaxy, system & database -> "Can cloak."
    public static final int Industry = /*528*/ LexDataIdx.System.Production.Industry; // confirmed by value change: system & database
    public static final int BuildCost = 109;                                    // confirmed by value change: system & database
    public static final int HitPointsPerShields = 435;                          // confirmed by value change: system & database -> "Hit Points / Shields"
    // for weapons, see Data.SpaceCraft.BeamWeapons & .TorpedoWeapons
    public static final int XEach = 877;                                        // confirmed by value change: system & database -> "%d each" weapon damage
    public static final int GalSpeed = 405;                                     // confirmed by value change: database, fleet & redeploy details
    public static final int GalRange = 404;                                     // confirmed by value change: database, fleet & redeploy details
    public static final int MaintenanceCost = 615;                              // confirmed by value change: database, galaxy map & system build
    // SHIP DETAILS
    public static final int HitPoints = 434;                                    // confirmed by value change: galaxy, database & combat ship details
    public static final int ShieldStrength = 1115;                              // confirmed by value change: galaxy
    public static final int Defense = /*218*/ Shared.Defense;                   // confirmed by value change: system
    public static final int SCANNER_RANGE = 1097;                               // confirmed by value change: fleet & redeploy details
    // STATUS
    public static final int Complete = 178;                                     // confirmed by value change: outpost in build
  }

  public abstract class GameSetup {
    public abstract class NewGame {
      // for game configuration, see Data.GameSettings
      public abstract class RaceInfo {
        public static final int Cardassians = 270;  // confirmed by value change: "The Cardassians will go to any length to ensure the loyalty of their subjects..."
        public static final int Federation  = 272;  // confirmed by value change: "The Federation is a free society, and its people value liberty and equality..."
        public static final int Ferengi     = 271;  // confirmed by value change: "The Ferengi have built an entire society around the profit motive..."
        public static final int Klingons    = 273;  // confirmed by value change: "The Klingons are among the most feared warriors of the galaxy..."
        public static final int Romulans    = 274;  // confirmed by value change: "The Romulans are a secretive people, enigmatic and often unpredictable..."
      }
    }

    public abstract class LoadGame {
      // suffix to unnamed savegames, for others labels refer Data.GameSettings
      public static final int Galaxy = 406;                                   // confirmed by value change
    }

    public abstract class Options {
      public static final int VOLUME_CONTROL = 1416;                          // confirmed by value change
      public static final int VOICE = 1413;                                   // confirmed by value change
      public static final int VIDEO = 1411;                                   // confirmed by value change
      public static final int MUSIC = 797;                                    // confirmed by value change
      public static final int SFX = 1113;                                     // confirmed by value change
      public static final int STRATEGIC_TIMER = 1182;                         // confirmed by value change
      public static final int TACTICAL_TIMER = 1247;                          // confirmed by value change
      // tooltips, random events, autosave
      public static final int YES = /*1453*/ Shared.YES;                      // confirmed by value change
      public static final int NO = /*828*/ Shared.NO;                         // confirmed by value change
      // tactical combat
      public static final int MANUAL = /*621*/ LexDataIdx.Shared.MANUAL;      // confirmed by value change
      public static final int AUTOMATIC = /*52*/ LexDataIdx.Shared.AUTOMATIC; // confirmed by value change
      public static final int None = /*872*/ LexDataIdx.Shared.None;          // confirmed by value change
    }
  }

  public abstract class Galaxy {
    public static final int Turn = /*1370*/ Shared.Turn;                                    // confirmed by value change

    // left top empire info stats
    public abstract class EmpireInfo {
      public static final int Turn = /*1370*/ Shared.Turn;                                  // confirmed by value change
      public static final int TOTAL_CREDITS = 1361;                                         // confirmed by value change
      public static final int CREDITS_PER_TURN = 194;                                       // confirmed by value change
      public static final int Dilithium = /*239*/ LexDataIdx.System.Restrictions.Dilithium; // confirmed by value change
      public static final int Events = 310;                                                 // confirmed by value change
    }

    public abstract class Map {
      // galaxy header line, used for sector info
      // @see fleet MoveProgress & Data.Galaxy.Ownership
      public static final int UnclaimedSector = 1382;         // confirmed by value change: "Unclaimed Sector %s"
      public static final int ContestedSector = 187;          // confirmed by value change: "Contested Sector %s"
      public static final int SectorX = 1107;                 // confirmed by value change: "Sector %s"
      public static final int UnexploredSector = 1385;        // confirmed by value change: "Unexplored Sector %s"
      public static final int ScanStrength = 1096;            // confirmed by value change: "Scan Strength %d"
      public static final int SelectPlanetToTerraform = 1109; // confirmed by value change
      // sector info tooltips
      // @see Data.Galaxy.StellarObjects
      public static final int Defense = /*218*/ Shared.Defense;                             // confirmed by value change
      public static final int GroundCombat = 422;                                           // confirmed by value change
      public static final int Planets = 623;                                                // confirmed by value change
      public static final int PlanetaryShields = 914;                                       // confirmed by value change
      public static final int Planet = 1153;                                                // confirmed by value change: 0 planet by supernova
      public static final int TaskForce = 1252;                                             // confirmed by value change
      public static final int Population = /*919*/ Shared.Population;                       // confirmed by value change
      public static final int OrbitalBatteries = 890;                                       // confirmed by value change
      public static final int OPERATIONAL = /*58*/ LexDataIdx.SpaceCraft.Condition.OPERATIONAL;   // confirmed by value change: outpost status
      public static final int Complete = /*178*/ ShipStats.Complete;                        // confirmed by value change
      // economic tooltips
      public static final int CreditsStolen = /*496*/ LexEventDetailsIdx.Galactic.CreditsStolen;  // confirmed by value change: "%d credits stolen."
    }

    // fleet & ship info
    public abstract class Fleet {
      public static final int Ship = /*1119*/ Shared.Ship;                                  // confirmed by value change: unknown ship, see Data.Ownership
      public static final int Ships = /*1139*/ LexDataIdx.Combat.VesselType.Ships;          // confirmed by value change
      public static final int TURNS = /*1372*/ Shared.TURNS;                                // confirmed by value change

      // SHIP STATS
      // for commands, see Data.SpaceCraft.FleetCommand
      // for weapons, see Data.SpaceCraft.BeamWeapons & .TorpedoWeapons
      public static final int CanCloak = /*116*/ ShipStats.CanCloak;                        // confirmed by value change: "Can cloak."
      // for experience, see Data.SpaceCraft.CrewExperience
      public static final int GalSpeed = /*405*/ ShipStats.GalSpeed;                        // confirmed by value change: fleet & redeploy details
      public static final int GalRange = /*404*/ ShipStats.GalRange;                        // confirmed by value change: fleet & redeploy details
      public static final int CREW_EXP = 199;                                               // confirmed by value change: redeploy details
      public static final int MaintenanceCost = /*615*/ ShipStats.MaintenanceCost;          // confirmed by value change: tooltip & redeploy details
      // FURTHER STATS
      public static final int HitPoints = /*434*/ ShipStats.HitPoints;                      // confirmed by value change: tooltip & redeploy details
      public static final int ShieldStrength = /*1115*/ ShipStats.ShieldStrength;           // confirmed by value change: tooltip & redeploy details
      public static final int SCANNER_RANGE = /*1097*/ ShipStats.SCANNER_RANGE;             // confirmed by value change: fleet & redeploy details
      // STATUS
      public static final int OPERATIONAL = /*58*/ LexDataIdx.SpaceCraft.Condition.OPERATIONAL;   // confirmed by value change: outpost status
      public static final int MoveProgress = 293;                                           // confirmed by value change: "eta", also displayed in galaxy header
      public static final int Complete = /*178*/ ShipStats.Complete;                        // confirmed by value change: outpost build progress
    }

    // the bottom system panel
    public abstract class SystemPanel {
      // @see Data.Galaxy.PlanetTypes, .Population & .PlanetBonus
      public static final int System = /*1214*/ Shared.System;                          // confirmed by value change
      public static final int Population = /*919*/ Shared.Population;                   // confirmed by value change
      public static final int MaxPop = 626;                                             // confirmed by value change
      public static final int GrowthRate = 425;                                         // confirmed by value change
      public static final int Orbitals = 891;                                           // confirmed by value change
      // system panel tooltips
      public static final int DilithiumPresent = 240;                                   // confirmed by value change: sun tooltip
      // stellar objects
      public static final int Description = 228;                                        // confirmed by value change

      // galaxy map selection in system panel
      public abstract class StellarDescriptions {
        public static final int BlackHole = 95;             // confirmed by value change: "The gravity well caused by the Black Hole makes sensor readings of this sector unreliable..."
        public static final int Nebula = 803;               // confirmed by value change: "Sensor readings are less reliable within the Nebula..."
        public static final int NeutronStarEmission = 811;  // confirmed by value change: "Radiative emissions from the Neutron Star make sensor readings of this sector unreliable..."
        public static final int RadioPulsarEmission = 969;  // confirmed by value change: "Radiative emissions from the Radio Pulsar disrupt sensors in this sector and adjacent sectors..."
        public static final int Wormhole = 1448;            // confirmed by value change: "Wormholes are subspace shortcuts.  A wormhole allows starships..."
        public static final int RadiativeEmission = 1451;   // confirmed by value change: "Radiative emissions from the X-Ray Pulsar disrupt sensors in this sector..."
      }
    }

    // left context menu
    public abstract class Events {
      public static final int EVENT_SECTOR = 305;                                       // confirmed by value change: "EVENT|SECTOR" combined string
    }

    // left context menu
    public abstract class Economy {
      public static final int System = /*1214*/ Shared.System;                          // confirmed by value change
      // @see Data.System.Morale
      public static final int MORALE = /*668*/ Shared.MORALE;                           // confirmed by value change
      public static final int Credits = /*193*/ LexDataIdx.System.Production.Credits;   // confirmed by value change
      public static final int LABOR_POOL = /*595*/ Shared.LABOR_POOL;                   // confirmed by value change
      public static final int CURRENT_BUILD = 208;                                      // confirmed by value change
      public static final int TradeGoods = /*1364*/ Shared.TradeGoods;                  // confirmed by value change
      public static final int Unassigned = 1381;                                        // confirmed by value change: trade routes
    }

    public abstract class Summary {
      // for build tasks, refer SystemScreen.BuildTasks
      // events
      public static final int EventsThisTurn = 311;                                     // confirmed by value change
      // relationship
      public static final int Name = 799;                                               // confirmed by value change: relationships & systems
      // systems
      public static final int Industry = /*528*/ LexDataIdx.System.Production.Industry; // confirmed by value change
      public static final int Food = /*365*/ LexDataIdx.System.Production.Food;         // confirmed by value change
      public static final int MORALE = /*668*/ Shared.MORALE;                           // confirmed by value change
      public static final int Build = 108;                                              // confirmed by value change
      public static final int XTurns = 624;                                             // confirmed by value change: "%d turns"
      public static final int OneTurn = 886;                                            // confirmed by value change: "1 turn"
      public static final int CommencingX = 174;                                        // confirmed by value change: "Commencing %s" tribunal
    }
  }

  public abstract class SystemScreen {
    // for system panel, see galaxy map
    public static final int Turn = /*1370*/ Shared.Turn;                                        // confirmed by value change
    public static final int System = /*1214*/ Shared.System;                                    // confirmed by value change

    // left panel system stats
    public abstract class SystemInfo {
      public static final int SYSTEM_INFO = 1215;                                               // confirmed by value change
      // @see Data.System.Morale
      public static final int MORALE = /*668*/ Shared.MORALE;                                   // confirmed by value change
      public static final int Credits = /*193*/ LexDataIdx.System.Production.Credits;           // confirmed by value change
      public static final int Food = /*365*/ LexDataIdx.System.Production.Food;                 // confirmed by value change
      public static final int Industry = /*528*/ LexDataIdx.System.Production.Industry;         // confirmed by value change
      public static final int Energy = /*279*/ LexDataIdx.System.Production.Energy;             // confirmed by value change
      public static final int Intelligence = /*548*/ LexDataIdx.System.Production.Intelligence; // confirmed by value change
      public static final int Research = /*996*/ LexDataIdx.System.Production.Research;         // confirmed by value change
      // tooltips
      public static final int GeneratedCredits = 412;                                           // confirmed by value change
      public static final int CreditsSpent = 195;                                               // confirmed by value change
      public static final int FoodGenerated = 366;                                              // confirmed by value change
      public static final int MaxGrowthFoodDemand = 625;                                        // confirmed by value change
      public static final int SustenanceFoodDemand = 1191;                                      // confirmed by value change
      public static final int IndustryGenerated = 530;                                          // confirmed by value change
      public static final int EnergyGenerated = 280;                                            // confirmed by value change
      public static final int MinimumEnergyDemand = 644;                                        // confirmed by value change
      public static final int MoralePoints = 735;                                               // confirmed by value change: "%d Morale Points"
    }

    // shared by build queue and system summary
    public abstract class BuildTasks {
      public static final int TradeGoods = /*1364*/ Shared.TradeGoods;                          // confirmed by value change
      public static final int UpgradeX = 1399;                                                  // confirmed by value change: "Upgrade %s"
      public static final int UpgradeScanner = 1404;                                            // confirmed by value change
      public static final int UpgradeStructure = 1406;
      public static final int Unknown = /*1393*/ LexDataIdx.Shared.Unknown;                     // confirmed by value change
    }

    // right build order queue
    public abstract class BuildQueue {
      // for build tasks, refer BuildTasks
      public static final int BUILD_QUEUE = 112;                                                // confirmed by value change
      public static final int AUTOMATIC = /*52*/ LexDataIdx.Shared.AUTOMATIC;                   // confirmed by value change
      public static final int MANUAL = /*621*/ LexDataIdx.Shared.MANUAL;                        // confirmed by value change
      public static final int DILITHIUM_SHORTAGE = 241;                                         // confirmed by value change
      public static final int TURNS_REMAINING = /*1373*/ Shared.TURNS_REMAINING;                // confirmed by value change
      public static final int BuildPurchased = 111;                                             // confirmed by value change
      public static final int Empty = 276;                                                      // confirmed by value change
      public static final int ShipYardUnpowered = 1134;                                         // confirmed by value change
      public static final int InProgress = 483;                                                 // might be for martial law
      public static final int OnHold = 883;                                                     // (On Hold)
      // tooltips
      public static final int RemoveAllTooltip = 1356;  // confirmed by value change: "Shift-click to remove all."
      public static final int RemoveTooltip = 1357;     // confirmed by value change: "Click to remove %s."
      public static final int UpgradeTooltip = 1358;    // confirmed by value change: "Click to remove upgrade to %s"
    }

    public abstract class ProductionSummary {
      public static final int PRODUCTION_SUMMARY = 928;                                         // confirmed by value change
      public static final int LABOR_POOL = /*595*/ Shared.LABOR_POOL;                           // confirmed by value change
      public static final int OUTPUT = 897;                                                     // confirmed by value change
      public static final int NoStructures = 853;                                               // confirmed by value change: "No %s structures"
      public static final int NoShipYard = 852;
      public static final int Food = /*365*/ LexDataIdx.System.Production.Food;                 // confirmed by value change
      public static final int Industry = /*528*/ LexDataIdx.System.Production.Industry;         // confirmed by value change
      public static final int Energy = /*279*/ LexDataIdx.System.Production.Energy;             // confirmed by value change
      public static final int Intelligence = /*548*/ LexDataIdx.System.Production.Intelligence; // confirmed by value change
      public static final int Research = /*996*/ LexDataIdx.System.Production.Research;         // confirmed by value change
      // tooltips
      public static final int TechBonus = 1360;                                             // confirmed by value change: " +%d%% Technology Bonus"
      public static final int SystemBonus = 1359;                                           // confirmed by value change: " +%d%% System Bonus"
      public static final int EmpireBonus = 1355;                                           // " +%d%% Empire Bonus"
      public static final int MORALE = /*668*/ Shared.MORALE;                               // confirmed by value change
    }

    public abstract class EnergyManagement {
      // note, the orbital batteries label is not read from the lexicon
      public static final int ENERGY_MANAGEMENT = 283;                                      // confirmed by value change
      public static final int Defense = /*218*/ Shared.Defense;                             // confirmed by value change
      public static final int SHORT_ENERGY = 1148;                                          // confirmed by value change
      public static final int Special = 1170;                                               // confirmed by value change
      // tooltips
      public static final int MORALE = /*668*/ Shared.MORALE;                               // confirmed by value change
    }

    // structure build menu
    public abstract class BuildList {
      public static final int TURNS = /*1372*/ Shared.TURNS;                                // confirmed by value change
      public static final int BUILD_LIST = 110;                                             // confirmed by value change
      public static final int STRUCTURES = 1185;                                            // confirmed by value change
      // STRUCTURE STATS
      public static final int IndustryBuildCost = /*529*/ BuildingStats.IndustryBuildCost;                  // confirmed by value change
      public static final int PopulationMaintenanceCost = /*921*/ BuildingStats.PopulationMaintenanceCost;  // confirmed by value change
      public static final int EnergyMaintenanceCost = /*282*/ BuildingStats.EnergyMaintenanceCost;          // confirmed by value change
      // @see Data.System.Restrictions
      public static final int Restriction = /*1005*/ BuildingStats.Restriction;             // confirmed by value change
      public static final int Restrictions = /*1006*/ BuildingStats.Restrictions;           // confirmed by value change: if multiple
      public static final int MORALE = /*668*/ BuildingStats.MORALE;                        // confirmed by value change
      // build task
      public static final int UpgradeToY = 1400;                                            // confirmed by value change: "Upgrade to %s"
      public static final int UpgradeNXToY = 1402;                                          // confirmed by value change: "Upgrade %d1 %2 to %3"
      public static final int UpgradeXToY = 1403;                                           // "Upgrade %1 to %2"
      public static final int TotalIncrease = 1362;                                         // confirmed by value change
      public static final int UnitIncrease = 1391;                                          // confirmed by value change
      public static final int UNKNOWN_BUILD_TYPE = 1394;
    }

    // ship build menu
    // looks identical to the build list but has a few different labels
    public abstract class ShipBuild {
      public static final int TURNS = /*1372*/ Shared.TURNS;                                // confirmed by value change
      public static final int SHIP_LIST = 1125;                                             // confirmed by value change
      public static final int Ships = /*1139*/ LexDataIdx.Combat.VesselType.Ships;          // confirmed by value change
      // SHIP STATS
      public static final int XClass = /*1122*/ ShipStats.XClass;                           // confirmed by value change
      // for short description, see Data.SpaceCraft.ShipCategoryDescriptions
      public static final int CanCloak = /*116*/ ShipStats.CanCloak;                        // confirmed by value change
      public static final int Industry = /*528*/ ShipStats.Industry;                        // confirmed by value change
      public static final int BuildCost = /*109*/ ShipStats.BuildCost;                      // confirmed by value change
      public static final int HitPointsPerShields = /*435*/ ShipStats.HitPointsPerShields;  // confirmed by value change
      // for weapons, see Data.SpaceCraft.BeamWeapons & .TorpedoWeapons
      public static final int XEach = /*877*/ ShipStats.XEach;                              // confirmed by value change: weapon damage
      public static final int GalRange = /*404*/ ShipStats.GalRange;                        // confirmed by value change
      public static final int GalSpeed = /*405*/ ShipStats.GalSpeed;                        // confirmed by value change
      public static final int MaintenanceCost = /*615*/ ShipStats.MaintenanceCost;          // confirmed by value change
    }

    // existing structures
    public abstract class StructureList {
      public static final int CURRENT_STRUCTURES = 209;         // confirmed by value change
      public static final int Scrap = 1104;                     // confirmed by value change
    }
  }

  public abstract class IntelScreen {
    public static final int Turn = /*1370*/ Shared.Turn;        // confirmed by value change

    public abstract class EmpireIntelInfo {
      public static final int EmpireIntelInfo = 267;            // confirmed by value change
      public static final int AvailablePointsAndBonuses = /*54*/ Shared.AvailablePointsAndBonuses;  // confirmed by value change
      public static final int IntelStrength = 547;              // confirmed by value change
      public static final int IntSecurity = 533;                // confirmed by value change
      public static final int Espionage = 292;                  // confirmed by value change
      public static final int Sabotage = 1093;                  // confirmed by value change
      public static final int EconomyDept = 261;                // confirmed by value change
      public static final int ScienceDept = 1100;               // confirmed by value change
      public static final int MilitaryDept = 643;               // confirmed by value change
    }

    public abstract class InternalSecurity {
      // for espionage 292 & sabotage 1093, see above
      public static final int InternalSecurity = 550;           // confirmed by value change
      public static final int AgentSpecialty = 17;              // confirmed by value change
    }

    public abstract class DetailReport {
      // for agency IntSecurity 533, see above
      // for source, see Data.Galaxy.Ownership
      public static final int DetailReport = 233;               // confirmed by value change
      public static final int NoReport = 851;                   // confirmed by value change
      // empire status
      public static final int Economic = 258;                   // confirmed by value change
      public static final int Science = 1099;                   // confirmed by value change
      public static final int Military = 642;                   // confirmed by value change
      public static final int MinorRace = 646;                  // confirmed by value change
      public static final int MORALE = /*668*/ Shared.MORALE;   // confirmed by value change
      public static final int SystemsHeld = 1217;               // confirmed by value change
    }
  }

  public abstract class ResearchScreen {
    public static final int Turn = /*1370*/ Shared.Turn;        // confirmed by value change

    public abstract class TechInfo {
      public static final int EmpireTechInfo = 275;                                                 // confirmed by value change
      public static final int AvailablePointsAndBonuses = /*54*/ Shared.AvailablePointsAndBonuses;  // confirmed by value change
      public static final int GrpPoints = 427;                                                      // confirmed by value change
    }

    public abstract class Management {
      // research fields are hard-coded, not of Data.Research.Field
      public static final int CurrentTechLevel = 210;           // confirmed by value change
      public static final int Researching = 1003;               // confirmed by value change
      public static final int Allocation = 426;                 // confirmed by value change
      public static final int Progress = 930;                   // confirmed by value change
      public static final int XFutureBiotech = 391;             // "Future Biotech %d"
      public static final int XFutureComputerTech = 393;        // "Future Computer %d"
      public static final int XFutureConstructionTech = 394;    // "Future Construction %d"
      public static final int XFutureEnergy = 396;              // "Future Energy %d"
      public static final int XFuturePropulsion = 398;          // "Future Propulsion %d"
      public static final int XFutureTech = 401;                // "Future Tech %d"
      public static final int XFutureWeapon = 402;              // "Future Weapon %d"
    }

    public abstract class TechnologyDatabase {
      // general, below tech image
      public static final int Status = /*1181*/ Shared.Status;  // confirmed by value change
      public static final int Available = 53;                   // confirmed by value change
      public static final int NotAvailable = 874;               // confirmed by value change
      public static final int ReqRP = 994;                      // confirmed by value change: required research points -> "Req. RP"
      public static final int Prerequisite = 923;               // confirmed by value change: "Prerequisite For"
      // propulsion technology descriptions
      public static final int ShipRange = 975;                  // confirmed by value change: "Ship Range: %d1/%d2/%d3"
    }

    public abstract class ObjectDatabase {
      // general, below image / ship view
      public static final int STATUS = /*1181*/ Shared.Status;                                              // confirmed by value change
      public static final int Available = /*53*/ TechnologyDatabase.Available;                              // confirmed by value change
      public static final int NotAvailable = /*874*/ TechnologyDatabase.NotAvailable;                       // confirmed by value change
      // requirements, see Data.Research.Field
      public static final int Requirement = 995;                                                            // confirmed by value change
      // statistic tab
      // STRUCTURE STATS
      public static final int IndustryBuildCost = /*529*/ BuildingStats.IndustryBuildCost;                  // confirmed by value change
      public static final int PopulationMaintenanceCost = /*921*/ BuildingStats.PopulationMaintenanceCost;  // confirmed by value change
      public static final int EnergyMaintenanceCost = /*282*/ BuildingStats.EnergyMaintenanceCost;          // confirmed by value change
      // @see Data.System.Restrictions
      public static final int Restriction = /*1005*/ BuildingStats.Restriction;             // confirmed by value change
      public static final int Restrictions = /*1006*/ BuildingStats.Restrictions;           // confirmed by value change: if multiple
      public static final int MORALE = /*668*/ BuildingStats.MORALE;                        // confirmed by value change
      // SHIP STATS
      public static final int XClass = /*1122*/ ShipStats.XClass;                           // confirmed by value change
      // for short descriptions, see Data.SpaceCraft.ShipCategoryDescriptions
      public static final int CanCloak = /*116*/ ShipStats.CanCloak;                        // confirmed by value change
      public static final int Industry = /*528*/ ShipStats.Industry;                        // confirmed by value change
      public static final int BuildCost = /*109*/ ShipStats.BuildCost;                      // confirmed by value change
      public static final int HitPointsPerShields = /*435*/ ShipStats.HitPointsPerShields;  // confirmed by value change
      // for weapons, see Data.SpaceCraft.BeamWeapons & .TorpedoWeapons
      public static final int XEach = /*877*/ ShipStats.XEach;                              // confirmed by value change: weapon damage
      public static final int GalSpeed = /*405*/ ShipStats.GalSpeed;                        // confirmed by value change
      public static final int GalRange = /*404*/ ShipStats.GalRange;                        // confirmed by value change
      public static final int MaintenanceCost = /*615*/ ShipStats.MaintenanceCost;          // confirmed by value change
    }
  }

  public abstract class DiplomacyScreen {
    public static final int Turn = /*1370*/ Shared.Turn;        // confirmed by value change
    public static final int DiplomaticRelations = 243;          // confirmed by value change

    // shared for active treaties & proposals
    public abstract class TreatyList {
      public static final int Race = 965;                       // confirmed by value change
      // @see Data.Diplomacy.Treaties
      public static final int Treaty = 1368;                    // confirmed by value change
      // @see Data.Diplomacy.TreatyDuration
      public static final int Duration = 251;                   // confirmed by value change: listing & details
      // @see Data.Diplomacy.TreatyStatus
      public static final int Status = /*1181*/ Shared.Status;  // confirmed by value change
      public static final int Give = 417;                       // confirmed by value change: event list -> 414 GiftTreaty
    }

    // active treaty details
    public abstract class Treaties {
      // for terms & status refer Data.Diplomacy
      public static final int TREATY_TERMS = 1369;                                                      // confirmed by value change
      public static final int Duration =         /*251*/ TreatyList.Duration;                           // confirmed by value change
      public static final int TURNS_REMAINING = /*1373*/ Shared.TURNS_REMAINING;                        // confirmed by value change
      public static final int XCredits =         /*196*/ LexDataIdx.Diplomacy.TreatyTerms.XCredits;     // confirmed by value change: "%s1 give %d2 credits to %3"
      public static final int PerTurn =          /*904*/ LexDataIdx.Diplomacy.TreatyFrequency.PerTurn;  // confirmed by value change: "%s1 give %d2 credits per turn to %3"
    }

    // propose
    public abstract class Proposal {
      // for terms & status refer Data.Diplomacy
      // territories & war pact war
      public static final int YES = /*1453*/ Shared.YES;        // confirmed by value change
      public static final int NO = /*828*/ Shared.NO;           // confirmed by value change
      // duration, see Data.Diplomacy.TreatyDuration
      public static final int TURNS = /*1372*/ Shared.TURNS;    // confirmed by value change
    }

    // treaty event details
    public abstract class Events {
      // for terms & status refer Data.Diplomacy
      public static final int TREATY_TERMS = /*1369*/ Treaties.TREATY_TERMS;                        // confirmed by value change
      public static final int Duration =      /*251*/ TreatyList.Duration;                          // confirmed by value change
      public static final int TURNS =        /*1372*/ Shared.TURNS;                                 // confirmed by value change
      public static final int XCredits =      /*196*/ LexDataIdx.Diplomacy.TreatyTerms.XCredits;    // confirmed by value change: "%s1 give %d2 credits to %3"
      public static final int PerTurn =       /*904*/ LexDataIdx.Diplomacy.TreatyFrequency.PerTurn; // confirmed by value change: "%s1 give %d2 credits per turn to %3"
      public static final int XWarPact = 1425;                                                      // confirmed by value change: "%s1 declares war as well."
      public static final int XWar = 278;                                                           // confirmed by value change: "%s1 declares war on %2"
    }
  }

  public abstract class Combat {
    // combat encounter, also displayed in battle when pointing no ships
    public abstract class Encounter {
      public static final int CombatOccurs = 167;               // confirmed by value change: "Combat occurring in sector %1"
      public static final int Ships = /*1139*/ LexDataIdx.Combat.VesselType.Ships;  // confirmed by value change
      public static final int XList = 800;                      // confirmed by value change: "%s{{, %s} and %s}"
      public static final int XYSpace = 1126;                   // confirmed by value change: " %d1 %2"
      public static final int XYComma = 1127;                   // confirmed by value change: ", %d1 %2"
    }

    // combat view
    public abstract class BattleView {
      // top right info text
      public static final int PLAYBACK_MODE = 915;              // confirmed by value change
      public static final int SelectTarget = 1110;              // confirmed by value change
    }

    // bottom fleet panel
    public abstract class FleetPanel {
      public static final int X2ColonY2 = 1251;                 // confirmed by value change: "%1 %2: %d3 %4"
      // @see Data.Combat.VesselType                            // confirmed by value change: Ships (1139), BorgCubes (207), Devices (235), XY (1151)
    }

    // left details tab
    public abstract class ShipInfo {
      // @see Data.Combat.ShipGroup
      // @see Data.Combat.CombatTactics
      public static final int Orders = 892;                             // confirmed by value change
      public static final int Defense = /*218*/ Shared.Defense;         // confirmed by value change
      // @see Data.SpaceCraft.CrewExperience
      public static final int Crew = 197;                               // confirmed by value change
      public static final int HitPoints = /*434*/ ShipStats.HitPoints;  // confirmed by value change
      public static final int Shields = 1116;                           // confirmed by value change
      // @see Data.SpaceCraft.BeamWeapons
      // @see Data.SpaceCraft.TorpedoWeapons
      // @see Data.Combat.ShipGroupDescriptions
      public static final int DamageControl = 211;                      // confirmed by value change
    }

    // fleets / enemy menu selection
    // @see Data.Combat.ShipGroup
    // @see Data.Galaxy.Ownership

    public abstract class BattleEvents {
      // FIRST LINE
      public static final int X4_1 = 423;                                           // confirmed by value change: "%s1 %2 %3 %4"
      // @see Data.Combat.ShipGroupRole
      public static final int Ships = /*1139*/ LexDataIdx.Combat.VesselType.Ships;  // confirmed by value change
      // @see Data.Combat.CombatTactics

      // PROGRESS
      // @see Data.SpaceCraft.BeamWeapons
      public static final int XDamage = 61;         // confirmed by value change: "The %1 fires %2 at %3 doing %d4 points of damage."
      public static final int XMiss = 62;           // confirmed by value change: "%s1 fires %2 at %3 and misses."
      // @see Data.SpaceCraft.TorpedoWeapons
      public static final int XFired = 931;         // confirmed by value change: "%s1 fired by %2."
      public static final int XHit = 932;           // confirmed by value change: "%s1 hit by %2 taking %d3 points of damage."
      public static final int XShields = 1117;      // confirmed by value change: "Shields at %d1%%."
      public static final int XHull = 443;          // confirmed by value change: "Hull at %d1%%."
      public static final int XDestroyed = /*231*/ LexEventIdx.Combat.XDestroyed; // confirmed by value change: "%s destroyed"
      public static final int XDown = 1118;         // "Down"
      public static final int XRams = 1129;         // "%s1 rams %2"
      public static final int XRetreats = 1130;     // "%s1 retreats."
      public static final int X4_2 = 1131;          // "%s1 %2 %3 %4"
      public static final int XTargeting_1 = 424;   // "Targeting %s %s ships"
      public static final int XTargeting_2 = 1152;  // "Targeting %s %s"
      public static final int XTargeting_3 = 1154;  // "Targeting %s %s"
      public static final int XDamaged = 212;       // "%s1 takes %d2 points of damage."
    }
  }

  // not found to be used in any menu yet
  // but grouped for better lookup
  public abstract class Other {
    // likely button label leftovers that have been dismissed
    public abstract class ButtonLabels {
      // main menu, missing savegame, options & retire
      public static final int Continue = 188;
      public static final int NewSinglePlayerGame = 822;
      public static final int NewMultiplayerGame = 816;
      public static final int LoadGame = 608;
      public static final int HallOfFame = 430;
      public static final int QuitGame = 964;
      // menu navigation for new game, new multiplayer,...
      public static final int Cancel = 117;
    }

    // for debug output or something?
    public abstract class GameStatus {
      public static final int Difficulty = 238;
      public static final int GalaxyShape = 407;
      public static final int GalaxySize = 408;
      public static final int EmpirePopulation = 268;
      public static final int EmpireResearch = 269;
      public static final int Money = 666;                // possibly used by some event
    }

    // new proposal selections
    public abstract class NewProposal {
      public static final int WarDeclaration = 814;       // "New Declaration of War"
      public static final int GiftTreaty = 815;           // "New Gift Treaty"
      public static final int PeaceTreaty = 817;          // "New Peace Treaty"
      public static final int TradeTreaty = 824;          // "New Trade Treaty"
      public static final int WarPact = 825;              // "New War Pact Treaty"
    }

    // possibly unused categories or event labels
    // see Menu.IntelScreen.DetailReport
    public abstract class EmpireCapability {
      public static final int General = 411;
      public static final int Financial = 361;
      public static final int Production = 926;
      public static final int Wealth = 1438;
    }

    // Game setup
    public static final int CommandPromptCursor = 157;    // ":>"
    public static final int Configuration = 180;
    public static final int Level = 602;
    public static final int LevelOne = 603;               // "Level 1"
    public static final int Loading = 609;
    public static final int Saving = 1094;
    public static final int Space = 1163;
    // Multiplayer
    public static final int Network = 808;
    public static final int Player = 916;
    public static final int Game = 409;
    public static final int ProcessingTurn = 924;         // duplicate of single player popup notification 1371
    public static final int UnsubmitTurn = 1398;
    // Galactic
    public static final int XSystem = 1216;               // "%s SYSTEM"
    public static final int SectorETA = 795;              // "Sector %s ETA %d"
    public static final int NoEvents = 833;               // events
    public static final int TerraformPoints = 1257;
    public static final int Habitability = 428;
    public static final int X_COMPLETE = 905;             // outpost related? "%d%% COMPLETE"
    public static final int Regroup = 989;
    public static final int CurrentBuildTurns = 1354;     // "%d T", not used by galaxy economy, system, summary,...
    public static final int Warehouse = 1426;
    public static final int WarehouseEnergy = 1427;       // "Warehouse energy"
    public static final int WarehouseFood = 1428;         // "Warehouse food"
    public static final int WarehouseLocation = 1429;     // "Warehouse location"
    // System
    public static final int BuildingShip = 113;
    public static final int BuildingStructure = 114;
    public static final int Commodity = 177;
    public static final int BuildProgress = 294;          // "%s eta %d turns\n%s"
    public static final int Info = 531;
    public static final int Inhabitants = 532;
    public static final int Labor = 594;
    public static final int Make = 617;
    public static final int MakingTradeGoods = 619;
    public static final int POP = 922;
    public static final int ProdEfficiency = 927;         // "Prod. Efficiency"
    public static final int Purchased = 957;
    public static final int XCreditsRefunded = 988;       // "(%d credits refunded)"
    public static final int Unemployment = 1384;
    // Intel
    public static final int OperativeEfficiency = 888;
    public static final int Statistics = 1180;
    // Research
    public static final int TechLevel = 1254;             // "tech level"
    public static final int Prerequisite = 1255;          // "Pre-Requisite For", not tech database
    public static final int ReplacesX = 992;              // "Replaces %s"
    // Diplomacy
    public static final int Allows = 32;
    public static final int Benefit = 91;
    public static final int DiplomaticStatus = 244;
    public static final int DrawFire = 249;
    public static final int DRAW_FIRE = 250;
    public static final int GiveGift = 416;
    public static final int Ignore = 478;
    public static final int Intergalactic = 549;
    public static final int Interstellar = 551;
    public static final int Denied = 556;                 // "is denied"
    public static final int ShareIntelligence = 1114;
    public static final int Request = 1365;
    public static final int UnityX = 1392;                // "Unity %c%c"
    // SpaceCraft
    public static final int Artillery = 41;
    public static final int EnergyLevelShipStatus = 281;  // "Energy Level ship status"
    public static final int SHIP_CLASS = 1121;
    public static final int ShipType = 1132;
    public static final int SPEED = 1171;
    // Combat
    public static final int Action = 2;
    public static final int CombatAutoResolved = 50;      // "Combat being resolved automatically..."
    public static final int Automated = 51;
    public static final int Cloaked = 163;
    public static final int CombatRange = 168;
    public static final int CombatSpeed = 169;
    public static final int SelectOrder = 1108;
    public static final int Visible = 1412;
    // Events
    public static final int Event = 297;
    public static final int EventHistory = 301;
    public static final int MultipleInstances = 796;
    public static final int NewShipType = 821;
    public static final int NEXT = 826;
    public static final int ResearchGenerated = 997;
    public static final int Researched = 1002;
    public static final int Restoring = 1004;
    // Other
    public static final int Type = 1374;
    public static final int U = 1405;
  }
}
