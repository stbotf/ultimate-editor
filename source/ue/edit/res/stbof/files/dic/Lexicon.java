package ue.edit.res.stbof.files.dic;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of the lexicon.dic file. It contains strings used by botf's
 * interface.
 */
public class Lexicon extends InternalFile {

  private int MAPADDRESS;
  private int unk;
  private short ENTRIES;
  private ArrayList<LecEntry> ENTRY = new ArrayList<LecEntry>();

  public Lexicon(Stbof stbof) {
  }

  /**
   * @param in the input stream to read value from
   */
  @Override
  public void load(InputStream in) throws IOException {
    ENTRY.clear();

    //wrap
    BufferedInputStream bis = new BufferedInputStream(in);
    bis.mark(bis.available());
    //address
    MAPADDRESS = StreamTools.readInt(bis, true);
    //unknown
    bis.reset();

    StreamTools.skip(bis, MAPADDRESS);
    unk = StreamTools.readInt(bis, true);

    //entries
    ENTRIES = StreamTools.readShort(bis, true);

    for (int i = 0; i < ENTRIES; i++) {
      bis.reset();
      bis.mark(bis.available());

      StreamTools.skip(bis, MAPADDRESS + 6 + (i * 8));

      //address
      int loc = StreamTools.readInt(bis, true);
      //length
      int len = StreamTools.readInt(bis, true);

      //string
      bis.reset();
      bis.mark(bis.available());

      StreamTools.skip(bis, loc);
      String str = StreamTools.readNullTerminatedBotfString(bis, len);

      //add
      ENTRY.add(new LecEntry(str, len));
    }

    markSaved();
  }

  /**
   * @param out the output stream to write to
   */
  @Override
  public void save(OutputStream out) throws IOException {
    ENTRIES = (short) ENTRY.size();

    // accumulates the size of all entries
    // to compute the new mapaddress offset
    MAPADDRESS = 4;
    for (int i = 0; i < ENTRIES; i++) {
      LecEntry le = ENTRY.get(i);
      MAPADDRESS += le.getSize();
    }
    out.write(DataTools.toByte(MAPADDRESS, true));

    for (int i = 0; i < ENTRIES; i++) {
      LecEntry le = ENTRY.get(i);
      String str = le.getString();
      out.write(DataTools.toByte(str, str.length() + 1, str.length()));
    }

    //write map
    out.write(DataTools.toByte(unk, true));
    out.write(DataTools.toByte(ENTRIES, true));

    // computes
    int ad = 4;
    for (int i = 0; i < ENTRIES; i++) {
      out.write(DataTools.toByte(ad, true));
      LecEntry le = ENTRY.get(i);
      int se = le.getSize();
      out.write(DataTools.toByte(se, true));
      ad += se;
    }
  }

  @Override
  public void clear() {
    ENTRY.clear();
    MAPADDRESS = 0;
    unk = 0;
    ENTRIES = 0;
    markChanged();
  }

  /**
   * @return the current entry count
   **/
  public int getNumEntries() {
    return ENTRY.size();
  }

  /**
   * @return a list of all the lexicon string values.
   */
  public List<String> getEntries() {
    return ENTRY.stream().map(LecEntry::getString).collect(Collectors.toList());
  }

  /**
   * set all the lexicon string values.
   */
  public void setEntries(List<String> lines) {
    ENTRY.clear();
    lines.stream().forEach(l -> ENTRY.add(new LecEntry(l)));
  }

  /**
   * @return an array of strings the lexicon file contains.
   */
  public String[] getFullEntries() {
    return ENTRY.stream().map(x -> x.getString()).toArray(String[]::new);
  }

  /**
   * @param index the index of the string to return
   * @return full string requested
   */
  public String getEntry(int index) {
    checkIndex(index);
    LecEntry le = ENTRY.get(index);
    return le.getString();
  }

  /**
   * @param index the index of the string to return
   * @param fallback default value to return when index is out of bounds
   * @return full string requested
   */
  public String getEntryOrDefault(int index, String fallback) {
    if (index < 0 || index + 1 > ENTRY.size())
      return fallback;

    LecEntry le = ENTRY.get(index);
    return le.getString();
  }

  /**
   * Changes entry.
   *
   * @param index the index of the entry to change
   * @param ent   the string to replace the previous one
   * @return true if value has been changed
   */
  public boolean setEntry(int index, String ent) {
    checkIndex(index);
    LecEntry le = ENTRY.get(index);
    if (ent.equals(le.getString()))
      return false;

    le.setString(ent);
    markChanged();
    return true;
  }

  /**
   * @param index the index where to insert a new entry
   * @param text the new entry's text
   */
  public void addEntry(int index, String text) {
    checkIndex(index);
    ENTRY.add(index, new LecEntry(text));
    ENTRIES++;
    markChanged();
  }

  /**
   * @param index the index of the entry to remove
   */
  public void removeEntry(int index) {
    checkIndex(index);
    ENTRY.remove(index);
    ENTRIES--;
    markChanged();
  }

  /**
   * @return the uncompressed size of the file
   **/
  @Override
  public int getSize() {
    int n = ENTRY.size();
    int size = 10 + n * 8;
    LecEntry le;
    for (int i = 0; i < n; i++) {
      le = ENTRY.get(i);
      size += le.getSize();
    }
    return size;
  }

  public int findNext(String searchText, int startIdx, boolean loop) {
    if (ENTRY.isEmpty())
      return -1;

    // skip negative index and adjust to not keep looping
    if (startIdx < 0 || startIdx >= ENTRY.size())
      startIdx = 0;

    searchText = searchText.toLowerCase();
    int idx = startIdx;

    do {
      String entryText = ENTRY.get(idx).entry.toLowerCase();
      int match = entryText.indexOf(searchText);
      if (match >= 0)
        return idx;

      if (++idx >= ENTRY.size()) {
        if (!loop)
          return -1;
        idx = 0;
      }
    } while (idx != startIdx);

    return -1;
  }

  public int findPrev(String searchText, int startIdx, boolean loop) {
    if (ENTRY.isEmpty())
      return -1;
    // skip negative index and adjust to not keep looping
    if (startIdx < 0 || startIdx >= ENTRY.size())
      startIdx = ENTRY.size()-1;

    searchText = searchText.toLowerCase();
    int idx = startIdx;

    do {
      String entryText = ENTRY.get(idx).entry.toLowerCase();
      int match = entryText.indexOf(searchText);
      if (match >= 0)
        return idx;

      if (--idx < 0) {
        if (!loop)
          return -1;
        idx = ENTRY.size()-1;
      }
    } while (idx != startIdx);

    return -1;
  }

  private void checkIndex(int index) {
    if (index < 0 || index >= ENTRY.size()) {
      String msg = Language.getString("Lexicon.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  @Override
  public void check(Vector<String> response) {
    if (ENTRY.size() != 1460) {
      String msg = Language.getString("Lexicon.1"); //$NON-NLS-1$
      msg = msg.replace("%", Integer.toString(ENTRY.size())); //$NON-NLS-1$
      response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_INFO, msg));
    }
  }

  private class LecEntry {

    private String entry;
    private int address;

    // to be used for new entries with unknown address
    public LecEntry(String e) {
      entry = e;
    }

    public LecEntry(String e, int a) {
      entry = e;
      address = a;
    }

    public String getString() {
      return entry;
    }

    @SuppressWarnings("unused")
    public int getAddress() {
      return address;
    }

    public void setString(String g) {
      entry = g;
    }

    @SuppressWarnings("unused")
    public void setAddress(int ad) {
      address = ad;
    }

    public int getSize() {
      return entry.length() + 1;
    }
  }

  // used placeholder values: "%%", "%1", "%2", "%3", "%4", "%c", "%d1", "%d2", "%d3", "%d4", "%d", "%s", "%s1", "%s2", "%s3"
  private static final String[] placeholders = { "%%", "%\\d", "%c", "%d\\d*", "%s\\d*" };
  private static final String placeholderFilter = String.join("|", placeholders);
  private static final Pattern placeholderPattern = Pattern.compile(placeholderFilter);

  public void NumberAll() {
    for (int i = 0; i < ENTRIES; i++) {
      LecEntry le = ENTRY.get(i);
      String txt = le.getString();
      String lbl = "#" + i;

      // extract and append the placeholders
      Matcher m = placeholderPattern.matcher(txt);
      while(m.find())
        lbl += "_" + m.group();

      if (lbl.equals(txt))
        continue;

      le.setString(lbl);
      markChanged();
    }
  }
}
