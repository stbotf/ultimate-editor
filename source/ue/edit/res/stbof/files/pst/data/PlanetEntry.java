package ue.edit.res.stbof.files.pst.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Optional;
import java.util.Vector;
import lombok.Getter;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof.PlanetAni;
import ue.edit.res.stbof.common.CStbof.PlanetAtmosphere;
import ue.edit.res.stbof.common.CStbof.PlanetType;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of a planet entry in planet.pst, which contains info on particular
 * planet. This class is used by planet.pst as well as systInfo.
 *
 * @see for PlanetType, PlanetSize, BonusAmount & BonusValue refer to Planet class
 */
@Getter
public class PlanetEntry extends PlanetKey {

  private final InternalFile file;
  private final Optional<SystemEntry> system;

  // #region data fields

  private String  name;             // 0x00 + 40 = 40 -> planet name, max 39 chars
  private String  animation;        // 0x28 + 14 = 54 -> animation file name, max 11? chars
  private short   index;            // 0x36 +  2 = 56 -> planet number
  // Key planetSize                 // 0x38 +  2 = 58
  private int     terraformPoints;  // 0x3A +  2 = 60 -> <unsigned> total terraforming points
  private int     terraformed;      // 0x3C +  2 = 62 -> <unsigned> terraforming points done
  // Key bonuses                    // 0x3E +  6 = 68

  private int     index_2;          // 0x44 +  4 = 72 -> planet number duplicate
  // Key planetType;                // 0x48 +  1 = 73
  // Key atmosphere;                // 0x49 +  1 = 74
  private short   empty;            // 0x4A +  2 = 76

  // #endregion data fields

  // #region constructor

  public PlanetEntry(InternalFile file, short index, Optional<SystemEntry> system) {
    this.file = file;
    this.system = system;
    Objects.requireNonNull(file);
    this.index = index;
    this.index_2 = index;

    name = "Planet " + index;
    planetType = PlanetType.Default;
    atmosphere = PlanetAtmosphere.Default;
    animation = PlanetAni.Default + ".ani";
  }

  public PlanetEntry(InternalFile file, InputStream in, Optional<SystemEntry> system) throws IOException {
    this.file = file;
    this.system = system;
    Objects.requireNonNull(file);
    load(in);
  }

  public PlanetEntry(InternalFile file, PlanetEntry source, Optional<SystemEntry> system) {
    super(source);
    this.file = file;
    this.system = system;
    Objects.requireNonNull(file);

    // name
    name = source.name;
    // planet ani picture
    animation = source.animation;
    // index
    index = source.index;
    // terra_points
    terraformPoints = source.terraformPoints;
    // terraformed
    terraformed = source.terraformed;
    // unknown
    index_2 = source.index_2;
    // unknown
    empty = source.empty;
  }

  // #endregion constructor

  // #region properties

  // #region property gets

  public boolean isTerraformed() {
    return terraformed >= terraformPoints;
  }

  // #endregion property gets

  // #region property sets

  public void setIndex(short index) {
    if (this.index != index) {
      this.index = index;
      file.markChanged();
    }
  }

  /**
   * @param name the new name of the planet
   * @return true if changes were made
   */
  public boolean setName(String name) {
    if (name.length() > 39)
      name = name.substring(0, 39);

    if (!StringTools.equals(this.name, name)) {
      this.name = name;
      file.markChanged();
      return true;
    }

    return false;
  }

  /**
   * @param ani the new planet's animation file
   * @return true if changes were made
   */
  public boolean setAnimation(String ani) {
    if (ani.length() > 12) {
      String err = "Animation %1 can be max 12 characters!"
        .replace("%1", ani);
      throw new IllegalArgumentException(err);
    }

    if (!StringTools.equals(animation, ani)) {
      animation = ani;
      file.markChanged();
      return true;
    }

    return false;
  }

  /**
   * @param type  the planet type
   */
  @Override
  public void setPlanetType(int type) {
    if (planetType != type) {
      planetType = type;
      file.markChanged();

      if (system.isPresent())
        system.get().updateSystemTraits();
    }
  }

  /**
   * @param atmo  the planet atmosphere
   */
  @Override
  public void setAtmosphere(int atmo) {
    if (atmosphere != atmo) {
      atmosphere = atmo;
      file.markChanged();

      if (system.isPresent())
        system.get().updateSystemTraits();
    }
  }

  /**
   * @param size  the new planet size, can be 0, 1 or 2
   * @return true if changes were made
   */
  @Override
  public void setPlanetSize(short size) {
    if (size < 0 || size > 2) {
      String msg = Language.getString("PlanetEntry.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Short.toString(size)); //$NON-NLS-1$
      throw new IllegalArgumentException(getName() + ": " + msg);
    }

    if (planetSize != size) {
      planetSize = size;
      file.markChanged();

      if (system.isPresent())
        system.get().updateSystemTraits();
    }
  }

  /**
   * @param terra the terraforming points
   * @return true if data has been changed
   */
  public void setTerraformPoints(int terra) {
    // limit to unsigned short type value
    terra = terra & 0xFFFF;

    if (this.terraformPoints != terra) {
      this.terraformPoints = terra;
      file.markChanged();
    }
  }

  /**
   * @param terra how many terraforming points have been terraformed
   * @return true if data has been changed
   */
  public void setTerraformed(int terra) {
    // limit to unsigned short type value
    terra = terra & 0xFFFF;

    if (this.terraformed != terra) {
      boolean wasTerraformed = isTerraformed();
      this.terraformed = terra;
      file.markChanged();

      if (system.isPresent() && wasTerraformed != isTerraformed())
        system.get().updateSystemTraits();
    }
  }

  @Override
  public void setFoodBonus(short bonus) {
    if (foodBonus != bonus) {
      foodBonus = bonus;
      file.markChanged();

      if (system.isPresent())
        system.get().updateSystemTraits();
    }
  }

  @Override
  public void setEnergyBonus(short bonus) {
    if (energyBonus != bonus) {
      energyBonus = bonus;
      file.markChanged();

      if (system.isPresent())
        system.get().updateSystemTraits();
    }
  }

  @Override
  public void setGrowthBonus(short bonus) {
    if (growthBonus != bonus) {
      growthBonus = bonus;
      file.markChanged();

      if (system.isPresent())
        system.get().updateSystemTraits();
    }
  }

  // #endregion property sets

  // #endregion properties

  // #region generic info

  public int getEntrySize() {
    return 76;
  }

  /**
   * Compare entries like they are sorted in unmodified original stbof.res file:
   * type -> atmosphere -> size -> foodBonus -> energyBonus -> growthBonus -> index
   */
  @Override
  public int compareTo(PlanetKey pk) {
    int cmp = super.compareTo(pk);
    if (cmp != 0)
      return cmp;

    // fallback to compare the loaded index
    // for original version you e.g. find Saturn, Venus and Mercury sorted by name
    return pk instanceof PlanetEntry ? index - ((PlanetEntry)pk).index : 0;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof PlanetEntry))
      return false;

    PlanetEntry pe = (PlanetEntry) obj;
    return index == pe.index;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 59 * hash + this.index;
    return hash;
  }

  // #endregion generic info

  // #region load

  public void load(InputStream in) throws IOException {
    // name 40
    name = StreamTools.readNullTerminatedBotfString(in, 40);
    // planet ani picture 54
    animation = StreamTools.readNullTerminatedBotfString(in, 14);
    // index 56
    index = StreamTools.readShort(in, true);
    // size 58
    planetSize = StreamTools.readShort(in, true);
    // terra_points 60
    terraformPoints = StreamTools.readShort(in, true) & 0xFFFF;
    // terraformed 62
    terraformed = StreamTools.readShort(in, true) & 0xFFFF;
    // food bonus 64
    foodBonus = StreamTools.readShort(in, true);
    // energy bonus 66
    energyBonus = StreamTools.readShort(in, true);
    // population bonus 68
    growthBonus = StreamTools.readShort(in, true);
    // index_2 72
    index_2 = StreamTools.readInt(in, true);
    // type 73
    planetType = in.read();
    // atmosphere 74
    atmosphere = in.read();
    // unknown 76
    empty = StreamTools.readShort(in, true);
  }

  // #endregion load

  // #region save

  public void save(OutputStream out) throws IOException {
    // name
    out.write(DataTools.toByte(name, 40, 39));
    // planet ani picture
    out.write(DataTools.toByte(animation, 14, 13));
    // index
    out.write(DataTools.toByte(index, true));
    // size
    out.write(DataTools.toByte(planetSize, true));
    // terra_points
    out.write(DataTools.toByte((short) (terraformPoints & 0xFFFF), true));
    // terraformed
    out.write(DataTools.toByte((short) (terraformed & 0xFFFF), true));
    // food bonus
    out.write(DataTools.toByte(foodBonus, true));
    // energy bonus
    out.write(DataTools.toByte(energyBonus, true));
    // population bonus
    out.write(DataTools.toByte(growthBonus, true));
    // unknown
    out.write(DataTools.toByte(index_2, true));
    // type
    out.write((byte)planetType);
    // atmosphere
    out.write((byte)atmosphere);
    // unknown
    out.write(DataTools.toByte(empty, true));
  }

  // #endregion save

  // #region check

  public void check(Vector<String> response, Optional<SystemEntry> system) {
    final String prefix = system.isPresent() ? Language.getString("PlanetEntry.4")
      .replace("%1", system.get().descriptor()) + " " : ""; //$NON-NLS-1$

    super.check(name, file, response, system);

    // name
    if (name.length() > 39) {
      String msg = prefix + Language.getString("PlanetEntry.5") //$NON-NLS-1$
        .replace("%1", name); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    // planet ani picture
    if (animation.length() > 11) {
      String msg = prefix + Language.getString("PlanetEntry.6") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", animation); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    // terra point
    if (terraformPoints < 0) {
      String msg = prefix + Language.getString("PlanetEntry.8") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(terraformPoints)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    // terra point
    if (terraformed < 0) {
      String msg = prefix + Language.getString("PlanetEntry.9") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(terraformed)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  // #endregion check

}
