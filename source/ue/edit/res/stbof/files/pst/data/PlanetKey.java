package ue.edit.res.stbof.files.pst.data;

import java.util.Optional;
import java.util.Vector;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.common.CStbof.BonusLevel;
import ue.edit.res.stbof.common.CStbof.BonusValue;
import ue.edit.res.stbof.files.pst.Planet;
import ue.edit.sav.files.map.data.SystemEntry;
import ue.service.Language;

/**
 * This class hold the key attributes for the planet entries of planet.pst.
 * It is used for binary search.
 *
 * @see for PlanetType, PlanetSize, BonusAmount & BonusValue refer to Planet class
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class PlanetKey implements Comparable<PlanetKey>, Cloneable {

  // #region data fields

  // unsigned byte planet type, @see ue.edit.res.stbof.CStbof.PlanetType
  protected int   planetType;
  // unsigned byte atmosphere, @see ue.edit.res.stbof.CStbof.PlanetAtmosphere
  protected int   atmosphere;
  // planet size: small/medium/large, @see ue.edit.res.stbof.CStbof.PlanetSize
  protected short planetSize;

  // bonus values can be few = 0, normal = 0x136, much = 0x26C, everything else is defaulted to normal
  // @see ue.edit.res.stbof.CStbof.BonusValue
  protected short foodBonus;    // food bonus
  protected short energyBonus;  // energy bonus
  protected short growthBonus;  // population growth & trade route bonus

  // #endregion data fields

  // #region constructor

  public PlanetKey(int planetType, short planetSize) {
    this.planetType = planetType;
    atmosphere = -1;
    this.planetSize = planetSize;
    foodBonus = -1;
    energyBonus = -1;
    growthBonus = -1;
  }

  public PlanetKey(int planetType, int atmosphere, short planetSize) {
    this.planetType = planetType;
    this.atmosphere = atmosphere;
    this.planetSize = planetSize;
    foodBonus = -1;
    energyBonus = -1;
    growthBonus = -1;
  }

  public PlanetKey(PlanetKey key) {
    planetType = key.planetType;
    atmosphere = key.atmosphere;
    planetSize = key.planetSize;
    foodBonus = key.foodBonus;
    energyBonus = key.energyBonus;
    growthBonus = key.growthBonus;
  }

  // #endregion constructor

  // #region properties

  // #region property gets

  // @returns the food bonus level (0,1,2)
  public int getFoodBonusLvl() {
    return bonusValueToLevel(foodBonus);
  }

  // @returns the energy bonus level (0,1,2)
  public int getEnergyBonusLvl() {
    return bonusValueToLevel(energyBonus);
  }

  // @returns the growth & trade route bonus level (0,1,2)
  public int getGrowthBonusLvl() {
    return bonusValueToLevel(growthBonus);
  }

  // #endregion property gets

  // #region property sets

  /**
   * @param bonusLvl  the food bonus, can be 0, 1 or 2
   */
  public void setFoodBonusLvl(int bonusLvl) {
    if (bonusLvl < 0 || bonusLvl > 2) {
      String msg = Language.getString("PlanetKey.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(bonusLvl)); //$NON-NLS-1$
      throw new IllegalArgumentException(msg);
    }
    setFoodBonus(bonusLevelToValue(bonusLvl));
  }

  /**
   * @param bonusLvl  the energy bonus, can be 0, 1 or 2
   */
  public void setEnergyBonusLvl(int bonusLvl) {
    if (bonusLvl < 0 || bonusLvl > 2) {
      String msg = Language.getString("PlanetKey.8"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(bonusLvl)); //$NON-NLS-1$
      throw new IllegalArgumentException(msg);
    }
    setEnergyBonus(bonusLevelToValue(bonusLvl));
  }

  /**
   * @param bonusLvl  the growth bonus, can be 0, 1 or 2
   */
  public void setGrowthBonusLvl(int bonusLvl) {
    if (bonusLvl < 0 || bonusLvl > 2) {
      String msg = Language.getString("PlanetKey.9"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(bonusLvl)); //$NON-NLS-1$
      throw new IllegalArgumentException(msg);
    }
    setGrowthBonus(bonusLevelToValue(bonusLvl));
  }

  // #endregion property sets

  // #endregion properties

  // #region generic info

  /**
   * Compare entries like they are sorted in unmodified original stbof.res file:
   * type -> atmosphere -> size -> foodBonus -> energyBonus -> growthBonus
   */
  @Override
  public int compareTo(PlanetKey key) {
    int cmp = planetType - key.planetType;
    if (cmp != 0)
      return cmp;

    if (key.atmosphere >= 0) {
      cmp = atmosphere - key.atmosphere;
      if (cmp != 0)
        return cmp;
    }

    if (key.planetSize >= 0) {
      cmp = planetSize - key.planetSize;
      if (cmp != 0)
        return cmp;
    }

    if (key.foodBonus >= 0) {
      cmp = foodBonus - key.foodBonus;
      if (cmp != 0)
      return cmp;
    }

    if (key.energyBonus >= 0) {
      cmp = energyBonus - key.energyBonus;
      if (cmp != 0)
      return cmp;
    }

    if (key.growthBonus >= 0) {
      cmp = growthBonus - key.growthBonus;
      if (cmp != 0)
        return cmp;
    }

    return 0;
  }

  @Override
  public PlanetKey clone() {
    return new PlanetKey(planetType, atmosphere, planetSize, foodBonus, energyBonus, growthBonus);
  }

  // #endregion generic info

  // #region check

  public void check(String name, InternalFile file, Vector<String> response, Optional<SystemEntry> system) {
    final String prefix = system.isPresent() ? Language.getString("PlanetKey.0")
      .replace("%1", system.get().descriptor()) + " " : ""; //$NON-NLS-1$

    // size
    if (planetSize < 0 || planetSize > 3) {
      String msg = prefix + Language.getString("PlanetKey.1") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(planetSize)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    // bonuses
    if (!Planet.isValidBonusValue(foodBonus)) {
      String msg = prefix + Language.getString("PlanetKey.2") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(foodBonus)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (!Planet.isValidBonusValue(energyBonus)) {
      String msg = prefix + Language.getString("PlanetKey.3") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(energyBonus)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (!Planet.isValidBonusValue(growthBonus)) {
      String msg = prefix + Language.getString("PlanetKey.4") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(growthBonus)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    // type
    if (planetType < 0 || planetType > 10) {
      String msg = prefix + Language.getString("PlanetKey.5") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(planetType)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    //atmosphere
    if (atmosphere < 8 || atmosphere > 12) {
      String msg = Language.getString("PlanetKey.6") //$NON-NLS-1$
        .replace("%1", name) //$NON-NLS-1$
        .replace("%2", Integer.toString(atmosphere)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
  }

  // #endregion check

  // #region private helpers

  private short bonusLevelToValue(int level) {
    return level >= BonusLevel.Much ? BonusValue.Much
      : level >= BonusLevel.Moderate ? BonusValue.Moderate : BonusValue.Few;
  }

  private int bonusValueToLevel(short value) {
    return value >= BonusValue.Much ? BonusLevel.Much
      : value >= BonusValue.Moderate ? BonusLevel.Moderate : BonusLevel.Few;
  }

  // #endregion private helpers

}
