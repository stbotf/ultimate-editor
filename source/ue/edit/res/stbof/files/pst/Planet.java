package ue.edit.res.stbof.files.pst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.Vector;
import java.util.stream.Collectors;
import lombok.var;
import ue.edit.common.InternalFile;
import ue.edit.common.FileArchive.LoadFlags;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof.BonusValue;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.ani.AniOrCur;
import ue.edit.res.stbof.files.est.Environ;
import ue.edit.res.stbof.files.pst.data.PlanetEntry;
import ue.edit.res.stbof.files.pst.data.PlanetKey;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.ID;
import ue.util.stream.StreamTools;

/**
 * This class is a representation of planet.pst, which contains a list of planets (surprise!). Some
 * of the planets are home planets others are placed randomly on the map.
 */
public class Planet extends InternalFile {

  private class InfoEntry {
    public short unknown;
    // frequency for map generation
    // overridden by Environ frequency on save
    @SuppressWarnings("unused")
    public float freq;
    // number of consequential planets of same type
    // overridden on save
    @SuppressWarnings("unused")
    public short num;
  }

  // #region fields

  private InfoEntry[] infos = new InfoEntry[8];
  private byte[] unknown;
  private ArrayList<PlanetEntry> planets = new ArrayList<PlanetEntry>();

  private Stbof stbof;

  // #endregion fields

  // #region constructor

  public Planet(Stbof st) {
    stbof = st;
  }

  // #endregion constructor

  // #region properties

  /**
   * Returns the planet entry of specified index.
   * @param index the index of the planet
   */
  public PlanetEntry getPlanet(int index) {
    checkIndex(index);
    return planets.get(index);
  }

  public Optional<PlanetEntry> findPlanet(PlanetKey key) {
    int idx = Collections.binarySearch(planets, key);
    return idx >= 0 ? Optional.of(planets.get(idx)) : Optional.empty();
  }

  // #endregion properties

  // #region generic info

  @Override
  public int getSize() {
    return 260 + planets.size() * 76;
  }

  // #endregion generic info

  // #region helpers

  public static boolean isValidBonusValue(int bonus) {
    switch (bonus) {
      case BonusValue.Much:
      case BonusValue.Moderate:
      case BonusValue.Few:
      return true;
      default:
      return false;
    }
  }

  /**
   * Returns the index of the planet with the specified ID.
   *
   * @param ID the id of the planet.
   * @return -1 if the planet is not found
   */
  public int getIndex(short ID) {
    for (int idx = 0; idx < planets.size(); idx++) {
      PlanetEntry planet = planets.get(idx);
      if (planet.getIndex() == ID)
        return idx;
    }

    return -1;
  }

  /**
   * returns an array of planet names.
   */
  public String[] getNames() {
    return planets.stream().map(PlanetEntry::getName).toArray(String[]::new);
  }

  /**
   * returns a list of animation files used
   */
  public List<String> getAnimationFiles() {
    return planets.stream().map(PlanetEntry::getAnimation).distinct().collect(Collectors.toList());
  }

  /**
   * Returns the IDs of planets.
   */
  public ID[] getIDs(int planetType) {
    if (planetType == -1) {
      ID[] ret = new ID[planets.size()];
      for (int j = 0; j < planets.size(); j++) {
        PlanetEntry planet = planets.get(j);
        ret[j] = new ID(planet.getName(), planet.getIndex());
      }

      return ret;
    } else {
      ArrayList<ID> hmp = new ArrayList<ID>();
      for (int j = 0; j < planets.size(); j++) {
        PlanetEntry planet = planets.get(j);
        if (planet.getPlanetType() == planetType)
          hmp.add(new ID(planet.getName(), planet.getIndex()));
      }

      ID[] ret = new ID[hmp.size()];
      for (int j = 0; j < hmp.size(); j++) {
        ret[j] = hmp.get(j);
      }

      return ret;
    }
  }

  public void addPlanet(int planetType) throws Exception {
    if (planets.size() == Short.MAX_VALUE)
      throw new Exception("Maximum number of planets reached.");

    // find unused id
    short id;
    for (id = 1; ; id++) {
      if (getIndex(id) < 0)
        break;
    }

    PlanetEntry planet = new PlanetEntry(this, planets.get(planets.size() - 1), Optional.empty());

    // fix
    planet.setIndex(id);
    planet.setName("Planet " + Short.toString(id));
    planet.setPlanetType(planetType);

    // add
    planets.add(planet);
    markChanged();
  }

  public void removePlanet(short id) throws Exception {
    if (planets.size() == 1)
      throw new Exception("Minimum number of planets reached.");

    int index = getIndex(id);
    if (index >= 0)
      planets.remove(index);

    markChanged();
  }

  /**
   * Sort entries like they are sorted in unmodified original stbof.res file:
   * type -> atmosphere -> size -> foodBonus -> energyBonus -> growthBonus -> index
   */
  public void sortEntries() {
    planets.sort(PlanetEntry::compareTo);
  }

  @Override
  public void clear() {
    Arrays.fill(infos, null);
    Arrays.fill(unknown, (byte)0);
    planets.clear();
    markChanged();
  }

  // #endregion helpers

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    planets.clear();

    // number of entries
    short num = StreamTools.readShort(in, true);

    // infos
    for (int i = 0; i < infos.length; ++i) { // 8*8=64
      InfoEntry info = new InfoEntry();
      info.unknown = StreamTools.readShort(in, true);
      info.freq = StreamTools.readFloat(in, true);
      info.num = StreamTools.readShort(in, true);
      infos[i] = info;
    }

    // unknown
    unknown = StreamTools.readBytes(in, 194);

    // planet entries
    for (short i = 0; i < num; ++i) {
      PlanetEntry pa = new PlanetEntry(this, in, Optional.empty());
      planets.add(pa);
    }

    // sort by properties to match original stbof.res sorting
    sortEntries();
    markSaved();
  }

  // #endregion load

  // #region save

  @Override
  public void save(OutputStream out) throws IOException {
    // entries -> 2
    out.write(DataTools.toByte((short) planets.size(), true));

    // probability and number plus unknown bytes
    Environ env = (Environ) stbof.getInternalFile(CStbofFiles.EnvironEst, true);
    for (byte k = 0; k < 8; k++) { // 8*8=64 -> 66
      // unknown
      out.write(DataTools.toByte(infos[k].unknown, true));
      // frequency
      float freq = env.getFrequency(k);
      out.write(DataTools.toByte(freq, true));
      // number
      short num = getNumSequentialPlanets(k);
      out.write(DataTools.toByte(num, true));
    }

    // unknown
    for (int k = 0; k < 194; ++k) {
      out.write(unknown[k]);
    }

    // planet entries
    for (int i = 0; i < planets.size(); i++) {
      PlanetEntry pa = planets.get(i);
      pa.save(out);
    }
  }

  // #endregion save

  // #region check

  @Override
  public void check(Vector<String> response) {
    // check animations
    List<String> tmp = getAnimationFiles();
    AniOrCur AOC = null;

    for (int i = 0; i < tmp.size(); i++) {
      try {
        AOC = stbof.getAnimation(tmp.get(i), LoadFlags.UNCACHED);
      } catch (Exception fnf) {
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, fnf.getMessage()));
      }
      if (AOC == null) {
        String msg = Language.getString("Planet.1") //$NON-NLS-1$
          .replace("%1", tmp.get(i)); //$NON-NLS-1$
        response.add(getCheckIntegrityString(INTEGRITY_CHECK_ERROR, msg));
      }
    }

    // check planet entries
    Hashtable<Short, String> idset = new Hashtable<Short, String>();
    ArrayList<PlanetEntry> fix_id = new ArrayList<PlanetEntry>();

    for (int i = 0; i < planets.size(); i++) {
      PlanetEntry pe = planets.get(i);

      // check for id duplicates
      if (idset.containsKey(pe.getIndex())) {
        String msg = Language.getString("Planet.2") //$NON-NLS-1$
          .replace("%1", idset.get(pe.getIndex())) //$NON-NLS-1$
          .replace("%2", pe.getName()) //$NON-NLS-1$
          .replace("%3", Short.toString(pe.getIndex())); //$NON-NLS-1$
        msg += " (fixed)";

        fix_id.add(pe);

        response.add(getCheckIntegrityString(INTEGRITY_CHECK_WARNING, msg));
      } else {
        idset.put(pe.getIndex(), pe.getName());
      }

      // check planet properties
      pe.check(response, Optional.empty());
    }

    // fix ids
    while (!fix_id.isEmpty()) {
      PlanetEntry pe = fix_id.get(0);
      short id = 0;

      while (pe != null) {
        if (!idset.containsKey(id)) {
          pe.setIndex(id);
          fix_id.remove(0);
          pe = null;
        }
        id++;
      }
    }
  }

  // #endregion check

  // #region private helpers

  private void checkIndex(int index) {
    if (index < 0 || index >= planets.size()) {
      String msg = Language.getString("Planet.14"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  /**
   * returns the number of consequential planets of the same type.
   */
  private short getNumSequentialPlanets(int type) {
    short num = 0;

    for (int i = 0; i < planets.size(); ++i) {
      PlanetEntry plan = planets.get(i);
      if (plan.getPlanetType() == type)
        num++;
      else if (num != 0)
        break;
    }

    return num;
  }

  public String getAnimation(PlanetKey key) {
    var entry = findPlanet(key);
    if (!entry.isPresent()) {
      entry = findPlanet(new PlanetKey(key.getPlanetType(), key.getAtmosphere(), key.getPlanetSize()));
      if (!entry.isPresent()) {
        entry = findPlanet(new PlanetKey(key.getPlanetType(), key.getPlanetSize()));
        if (!entry.isPresent())
          entry = Optional.of(planets.get(0));
      }
    }

    return entry.get().getAnimation();
  }

  // #endregion private helpers

}
