package ue.edit.res.stbof.files.sst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Represents shiprace.sst. It was previously thought the file contains the first id of a ship of
 * each race. But it seems they aren't id's at all, but offsets in shiplist.sst.
 */
public class ShipRace extends InternalFile {

  private int[] SHIP_START = null;

  public ShipRace(Stbof stbof) {
  }

  @Override
  public void load(InputStream in) throws IOException {
    SHIP_START = new int[in.available() / 4];

    int i = 0;
    while (in.available() > 0) {
      SHIP_START[i++] = StreamTools.readInt(in, true);
    }

    markSaved();
  }

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < SHIP_START.length; i++) {
      out.write(DataTools.toByte(SHIP_START[i], true));
    }
  }

  @Override
  public void clear() {
    SHIP_START = null;
    markChanged();
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return SHIP_START != null ? SHIP_START.length * 4 : 0;
  }

  /**
   * Set ships starting id's
   */
  public void setShipStarts(int[] sh) {
    if (SHIP_START == null || sh.length != SHIP_START.length) {
      SHIP_START = sh;
      this.markChanged();
    } else {
      for (int i = 0; i < sh.length; i++) {
        if (sh[i] != SHIP_START[i]) {
          SHIP_START[i] = sh[i];
          this.markChanged();
        }
      }
    }
  }
}
