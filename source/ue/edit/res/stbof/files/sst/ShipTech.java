package ue.edit.res.stbof.files.sst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * Represents shiptech.sst.
 */
public class ShipTech extends InternalFile {

  private byte[][] TECH_LVL;
  private int[] RACE;
  private short[] PARENT_ID;

  public ShipTech(Stbof stbof) {
  }

  @Override
  public void load(InputStream in) throws IOException {
    TECH_LVL = new byte[6][in.available() / 16];
    RACE = new int[in.available() / 16];
    PARENT_ID = new short[in.available() / 16];

    while (in.available() > 0) {
      //race 2
      int rac = StreamTools.readInt(in, true);
      //id 4
      short i = StreamTools.readShort(in, true);
      //set race
      RACE[i] = rac;
      //upgrade 6
      PARENT_ID[i] = StreamTools.readShort(in, true);

      //req tech lvls 12
      for (int j = 0; j < 6; j++) {
        TECH_LVL[j][i] = (byte) in.read();
      }

      //ship 2
      validate(in, 2, 0);
    }

    markSaved();
  }

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    for (short i = 0; i < RACE.length; i++) {
      //race 4
      out.write(DataTools.toByte(RACE[i], true));
      //id 6
      out.write(DataTools.toByte(i, true));
      //upgrade 8
      out.write(DataTools.toByte(PARENT_ID[i], true));
      //req tech lvls 14
      for (int j = 0; j < 6; j++) {
        out.write(TECH_LVL[j][i]);
      }
      //ship 16
      out.write(new byte[2]);
    }
  }

  @Override
  public void clear() {
    TECH_LVL = null;
    RACE = null;
    PARENT_ID = null;
    markChanged();
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
    String msg;
    for (int i = 0; i < RACE.length; i++) {
      if (RACE[i] < 0 || RACE[i] > 36) {
        msg = Language.getString("ShipTech.0"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(RACE[i])); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(i)); //$NON-NLS-1$
        response.add(msg);
      }
      for (int j = 0; j < 6; j++) {
        if (TECH_LVL[j][i] < 0) {
          msg = Language.getString("ShipTech.1"); //$NON-NLS-1$
          msg = msg.replace("%1", getName()); //$NON-NLS-1$
          msg = msg.replace("%2", Integer.toString(TECH_LVL[j][i])); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(i)); //$NON-NLS-1$
          response.add(msg);
        }
      }
      if (PARENT_ID[i] == i) {
        msg = Language.getString("ShipTech.2"); //$NON-NLS-1$
        msg = msg.replace("%1", getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(PARENT_ID[i])); //$NON-NLS-1$
        msg = msg.replace("%3", Integer.toString(i)); //$NON-NLS-1$
        response.add(msg);
      }
      if (PARENT_ID[i] >= 0) {
        for (int j = 0; j < 6; j++) {
          if (TECH_LVL[j][i] < TECH_LVL[j][PARENT_ID[i]]) {
            msg = Language.getString("ShipTech.3"); //$NON-NLS-1$
            msg = msg.replace("%1", getName()); //$NON-NLS-1$
            msg = msg.replace("%2", Integer.toString(i)); //$NON-NLS-1$
            response.add(msg);
          }
        }
        if (RACE[i] != RACE[PARENT_ID[i]]) {
          msg = Language.getString("ShipTech.4"); //$NON-NLS-1$
          msg = msg.replace("%1", getName()); //$NON-NLS-1$
          msg = msg.replace("%2", Integer.toString(RACE[i])); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(PARENT_ID[i])); //$NON-NLS-1$
          response.add(msg);
        }
      }
    }
  }

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return RACE != null ? RACE.length * 16 : 0;
  }

  /**
   * Reorders data.
   * @throws InvalidArgumentsException
   */
  public void reOrder(int[] id) throws InvalidArgumentsException {
    if (id.length != RACE.length) {
      throw new InvalidArgumentsException(Language.getString("ShipTech.5")); //$NON-NLS-1$
    }
    byte[][] tech = new byte[6][id.length];
    int[] race = new int[id.length];
    short[] parent = new short[id.length];

    for (int i = 0; i < id.length; i++) {//for each entry
      if (i != id[i]) {
        this.markChanged();
      }

      for (int j = 0; j < 6; j++) {//switch each level requirement
        tech[j][i] = TECH_LVL[j][id[i]];
      }
      race[i] = RACE[id[i]];//switch owner
      short u = PARENT_ID[id[i]];//get previous parent
      if (u < 0) {
        parent[i] = -1;
      } else {
        short gum = -1;
        for (short hi = 0; hi < id.length && gum < 0; hi++) {
          if (id[hi] == u) {
            gum = hi;
          }
        }
        parent[i] = gum;
      }
    }

    TECH_LVL = tech;
    RACE = race;
    PARENT_ID = parent;
  }

  /**
   * Sets race
   */
  public void setRace(int index, int race) {
    checkRaceIndex(index);
    if (RACE[index] == race) {
      return;
    }
    RACE[index] = race;
    this.markChanged();
  }

  /**
   * Returns race
   */
  public int getRace(int index) {
    checkRaceIndex(index);
    return RACE[index];
  }

  /**
   * Returns parent id
   */
  public int getParent(int id) {
    checkParentIndex(id);
    return PARENT_ID[id];
  }

  /**
   * Sets parent
   */
  public void setParent(int id, short par) {
    checkParentIndex(id);
    if (PARENT_ID[id] == par) {
      return;
    }
    PARENT_ID[id] = par;
    this.markChanged();
  }

  /**
   * Returns tech levels
   */
  public byte[] getTechLvls(int id) {
    checkParentIndex(id);
    byte[] ret = new byte[6];
    for (int i = 0; i < 6; i++) {
      ret[i] = TECH_LVL[i][id];
    }
    return ret;
  }

  /**
   * Sets tech levels
   */
  public void setTechLvls(int id, byte[] par) {
    checkParentIndex(id);
    for (int i = 0; i < 6; i++) {
      if (TECH_LVL[i][id] != par[i]) {
        TECH_LVL[i][id] = par[i];
        this.markChanged();
      }
    }
  }

  /**
   * Inserts new slot.
   */
  public void insert(int id) {
    checkParentIndex(id);

    byte[][] tech = new byte[6][PARENT_ID.length + 1];
    int[] race = new int[PARENT_ID.length + 1];
    short[] parent = new short[PARENT_ID.length + 1];

    for (int i = 0; i < id; i++) { // copy entries from 0 to not including id
      for (int j = 0; j < 6; j++) {
        tech[j][i] = TECH_LVL[j][i];
      }
      race[i] = RACE[i];
      if (PARENT_ID[i] < id) {
        parent[i] = PARENT_ID[i];
      } else {
        parent[i] = (short) (PARENT_ID[i] + 1);
      }
    }
    for (int i = id; i < PARENT_ID.length; i++) { // copy entries from id to end
      for (int j = 0; j < 6; j++) {
        tech[j][i + 1] = TECH_LVL[j][i];
      }
      race[i + 1] = RACE[i];
      if (PARENT_ID[i] < id) {
        parent[i + 1] = PARENT_ID[i];
      } else {
        parent[i + 1] = (short) (PARENT_ID[i] + 1);
      }
    }
    // the new one
    for (int j = 0; j < 6; j++) {
      tech[j][id] = TECH_LVL[j][id];
    }
    race[id] = RACE[id];
    parent[id] = PARENT_ID[id];
    parent[id + 1] = -1;

    TECH_LVL = tech;
    RACE = race;
    PARENT_ID = parent;
    this.markChanged();
  }

  /**
   * Removes slot.
   */
  public void remove(int id) {
    checkParentIndex(id);

    byte[][] tech = new byte[6][PARENT_ID.length - 1];
    int[] race = new int[PARENT_ID.length - 1];
    short[] parent = new short[PARENT_ID.length - 1];

    for (int i = 0; i < id; i++) {
      for (int j = 0; j < 6; j++) {
        tech[j][i] = TECH_LVL[j][i];
      }
      race[i] = RACE[i];
      if (PARENT_ID[i] < id) {
        parent[i] = PARENT_ID[i];
      } else {
        parent[i] = (short) (PARENT_ID[i] - 1);
      }
    }

    for (int i = id + 1; i < PARENT_ID.length; i++) {
      for (int j = 0; j < 6; j++) {
        tech[j][i - 1] = TECH_LVL[j][i];
      }
      race[i - 1] = RACE[i];
      if (PARENT_ID[i] < id) {
        parent[i - 1] = PARENT_ID[i];
      } else {
        parent[i - 1] = (short) (PARENT_ID[i] - 1);
      }
    }

    TECH_LVL = tech;
    RACE = race;
    PARENT_ID = parent;
    this.markChanged();
  }

  private void checkRaceIndex(int index) {
    if (index < 0 || index >= RACE.length) {
      String msg = Language.getString("ShipTech.6"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private void checkParentIndex(int index) {
    if (index < 0 || index >= PARENT_ID.length) {
      String msg = Language.getString("ShipTech.7"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  public int getMaxTechLevel() {
    int max = 0;

    for (int i = 0; i < TECH_LVL.length; i++) {
      for (int j = 0; j < TECH_LVL[i].length; j++) {
        if (TECH_LVL[i][j] > max) {
          max = TECH_LVL[i][j];
        }
      }
    }

    return max;
  }
}
