package ue.edit.res.stbof.files.sst;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.edit.common.InternalFile;
import ue.edit.exe.trek.Trek;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.common.WDFImgReq;
import ue.edit.res.stbof.common.CStbof.Race;
import ue.edit.res.stbof.common.CStbof.ShipRole;
import ue.edit.res.stbof.files.bin.Shipname;
import ue.edit.res.stbof.files.desc.DescDataFile;
import ue.edit.res.stbof.files.desc.Descriptable;
import ue.edit.res.stbof.files.sst.data.ShipDefinition;
import ue.edit.res.stbof.files.sst.data.ShipCollection;
import ue.service.Language;
import ue.util.data.ID;
import ue.util.file.FileStore;

/**
 * Represents shiplist.sst.
 */
public class ShipList extends DescDataFile implements ShipCollection {

  // #region constants

  public static final int FILTER_ALL = -1;      // all races
  public static final int FILTER_MAJORS = -2;   // major races
  public static final int FILTER_MINORS = -3;   // minor races

  private static final int NUM_EMPIRES = CStbof.NUM_EMPIRES;

  // #endregion constants

  // #region data fields

  private ArrayList<ShipDefinition> SHIP = new ArrayList<ShipDefinition>();

  @Getter @Accessors(fluent = true)
  private Stbof stbof = null;

  // #endregion data fields

  // #region constructor

  public ShipList(Stbof st) throws IOException {
    super(CStbofFiles.ShipDescSst, st);
    stbof = st;
  }

  // #endregion constructor

  // #region getters

  public boolean hasIndex(int index) {
    return index >= 0 && index < SHIP.size();
  }

  /**
   * Returns an array of ship model ID's according to race and ship role.
   *
   * @param race          raceId, -1 for all, -2 for major races, -3 for minor races, -4 for
   *                      monsters
   * @param roles         ship roles or null
   * @param roleExclusion ship roles to exclude or null
   */
  public ID[] getIDs(int race, int[] roles, int[] roleExclusion) {
    ArrayList<ID> ret = new ArrayList<ID>();
    for (int i = 0; i < SHIP.size(); ++i) {
      ShipDefinition shp = SHIP.get(i);
      short shipRace= shp.getRace();

      // filter races
      if (race == FILTER_MAJORS) {
        // filter for major races
        if (shipRace > 4)
          continue;
      } else if (race == FILTER_MINORS) {
        // filter for minor races
        if (shipRace < NUM_EMPIRES || shipRace == Race.Monster)
          continue;
      } else if (race != -1) {
        if (shipRace != race)
          continue;
      }

      // filter excluded ship roles
      if (roleExclusion != null) {
        boolean found = false;
        for (int role : roleExclusion) {
          if (shp.getShipRole() == role) {
            found = true;
            break;
          }
        }

        if (found)
          continue;
      }

      // filter included ship roles
      if (roles != null) {
        boolean found = false;
        for (int role : roles) {
          if (shp.getShipRole() == role) {
            found = true;
            break;
          }
        }

        if (!found)
          continue;
      }

      ret.add(new ID(shp.getName(), i));
    }

    return ret.toArray(new ID[0]);
  }

  /**
   * DEPRECATED Returns an array of ship ID's according to either race or ship role.
   */
  public ID[] getIDs(int filter) {
    if (filter == Race.None) {
      // all
      ID[] ret = new ID[SHIP.size()];
      for (int i = 0; i < SHIP.size(); i++) {
        ShipDefinition shp = SHIP.get(i);
        ret[i] = new ID(shp.getName(), i);
      }
      return ret;
    } else if (filter < 35 || filter == 36) {
      // minors and aliens
      ArrayList<ID> ret = new ArrayList<ID>();
      for (int i = 0; i < SHIP.size(); i++) {
        ShipDefinition shp = SHIP.get(i);
        if (shp.getRace() == filter)
          ret.add(new ID(shp.getName(), i));
      }
      return ret.toArray(new ID[0]);
    } else if (filter == 45) {
      // troop transports
      // handled separately since by default it follows the alien ship role
      filter = ShipRole.TroopTransport;
      ArrayList<ID> ret = new ArrayList<ID>();
      for (int i = 0; i < SHIP.size(); i++) {
        ShipDefinition shp = SHIP.get(i);
        if (shp.getShipRole() == filter)
          ret.add(new ID(shp.getName(), i));
      }
      return ret.toArray(new ID[0]);
    } else {
      // other ship roles
      filter = filter - 37;
      ArrayList<ID> ret = new ArrayList<ID>();
      for (int i = 0; i < SHIP.size(); i++) {
        ShipDefinition shp = SHIP.get(i);
        if (shp.getShipRole() == filter)
          ret.add(new ID(shp.getName(), i));
      }
      return ret.toArray(new ID[0]);
    }
  }

  /**
   * Return number of races
   */
  @Override
  public int getNumberOfEntries() {
    return SHIP.size();
  }

  public List<ShipDefinition> listShips() {
    return Collections.unmodifiableList(SHIP);
  }

  @Override
  public Descriptable getEntry(int index) {
    checkShipIndex(index);
    return SHIP.get(index);
  }

  public ShipDefinition getShipDefinition(int shipType) {
    checkShipIndex(shipType);
    return SHIP.get(shipType);
  }

  public int getShipType(short raceId, int shipRole) {
    int firstRaceShip = -1;
    for (ShipDefinition sh : SHIP) {
      if (sh.getRace() == raceId) {
        if (sh.getShipRole() == shipRole)
          return sh.index();
        else if (firstRaceShip == -1)
          firstRaceShip = sh.index();
      }
    }
    return firstRaceShip;
  }

  /**
   * Returns ship starting id's, this doesn't include the alien ships.
   */
  public int[] getStartingIDs() {
    Collections.sort(SHIP);
    int[] ret = new int[35];
    for (int i = 0; i < 35; i++) {
      ID[] x = getIDs(i);
      if (x.length < 1) {
        ret[i] = -1;
      } else {
        ret[i] = x[0].hashCode();
      }
    }
    return ret;
  }

  public Set<String> getGraphics() {
    HashSet<String> gfx = new HashSet<>();
    for (short i = 0; i < SHIP.size(); i++) {
      gfx.add(SHIP.get(i).getGraphics());
    }
    return gfx;
  }

  private boolean hasShipIndex(int index) {
    return index >= 0 && index < SHIP.size();
  }

  /**
   * @param g
   * @return
   */
  @Override
  public String getClassByPrefix(String g) {
    g = g.toLowerCase();

    for (ShipDefinition sh : SHIP) {
      if (sh.getGraphics().toLowerCase().equals(g))
        return sh.getShipClass();
    }

    return null;
  }

  /**
   * returns -1 if not found
   *
   * @param g
   * @return
   */
  public int getFirstIDWithPrefix(String g) {
    g = g.toLowerCase();

    for (int i = 0; i < SHIP.size(); i++) {
      ShipDefinition sh = SHIP.get(i);
      if (sh.getGraphics().toLowerCase().equals(g))
        return i;
    }

    return -1;
  }

  @Override
  public long getFileSize(String fileName) throws IOException {
    return stbof.getInternalFileSize(fileName);
  }

  @Override
  public String getAuthorByPrefix(String prefix) {
    return null;
  }

  @Override
  public boolean containsFile(String fileName) {
    return stbof.hasInternalFile(fileName);
  }

  // #endregion getters

  // #region setters

  /**
   * Sets race.
   *
   * @return true if changed
   */
  public boolean setRace(int index, short race, boolean adjust_ovr) {
    checkShipIndex(index);
    ShipDefinition sh = SHIP.get(index);

    if (sh.setRace(race, adjust_ovr)) {
      if (adjust_ovr)
        sort();
      return true;
    }
    return false;
  }

  // #endregion setters

  // #region public helpers

  public void sort() {
    Collections.sort(SHIP);
  }

  /**
   * Returns ship starting id's
   */
  public int[] allignIDs() {
    Collections.sort(SHIP);
    int[] ret = new int[SHIP.size()];
    for (short i = 0; i < SHIP.size(); i++) {
      ShipDefinition sh = SHIP.get(i);
      ret[i] = sh.index();
      sh.setIndex(i);
    }
    return ret;
  }

  /**
   * Moves the ship up. (lowers it's id)
   */
  public int[] moveUp(int index) {
    checkShipIndex(index);
    if (index - 1 < 0)
      return allignIDs();

    ShipDefinition sh = SHIP.get(index);
    ShipDefinition sh2 = SHIP.get(index - 1);
    if (sh.getRace() != sh2.getRace())
      return allignIDs();

    int[] ids = new int[SHIP.size()];
    for (int i = 0; i < SHIP.size(); i++) {
      if (i == index) {
        ids[i] = index - 1;
      } else if (i == index - 1) {
        ids[i] = index;
      } else {
        ids[i] = i;
      }
    }

    short id = sh2.index();
    sh2.setIndex(sh.index());
    sh.setIndex(id);
    markChanged();
    return ids;
  }

  /**
   * Moves the ship down. (increases it's id)
   */
  public int[] moveDown(int index) {
    checkShipIndex(index);
    if (index + 1 >= SHIP.size())
      return allignIDs();

    ShipDefinition sh = SHIP.get(index);
    ShipDefinition sh2 = SHIP.get(index + 1);
    if (sh.getRace() != sh2.getRace())
      return allignIDs();

    int[] ids = new int[SHIP.size()];
    for (int i = 0; i < SHIP.size(); i++) {
      if (i == index) {
        ids[i] = index + 1;
      } else if (i == index + 1) {
        ids[i] = index;
      } else {
        ids[i] = i;
      }
    }

    short id = sh2.index();
    sh2.setIndex(sh.index());
    sh.setIndex(id);
    markChanged();
    return ids;
  }

  // #endregion public helpers

  // #region add & remove

  /**
   * Creates a copy of the ship with the specified index, moves ships after the id down.
   */
  public void createShip(int index) {
    checkShipIndex(index);

    ShipDefinition sh = new ShipDefinition(SHIP.get(index));
    SHIP.add(index, sh);

    // add and fix id's
    // other files that depend on these ids must be fixed seperatly.
    // you need to: add a description, fix shiptech and shiprace
    for (short i = 0; i < SHIP.size(); i++) {
      sh = SHIP.get(i);
      sh.setIndex(i);
    }

    descChanged();
    markChanged();
  }

  /**
   * Removes the ship with the specified id.
   */
  public void removeShip(int index) {
    checkShipIndex(index);
    SHIP.remove(index);

    // add and fix id's
    // other files that depend on these ids must be fixed seperatly.
    // you need to: fix shiptech and shiprace, remove description
    ShipDefinition sh;
    for (short i = 0; i < SHIP.size(); i++) {
      sh = SHIP.get(i);
      sh.setIndex(i);
    }

    descChanged();
    markChanged();
  }

  // #endregion add & remove

  // #region load

  @Override
  public void load(InputStream in) throws IOException {
    ShipTech tech = (ShipTech) stbof.getInternalFile(CStbofFiles.ShipTechSst, true);
    SHIP.clear();

    while (in.available() > 0) {
      ShipDefinition sh = new ShipDefinition(this, in);
      sh.setRace((short) tech.getRace(sh.index()), false);
      SHIP.add(sh);
    }

    markSaved();
  }

  // #endregion load

  // #region save

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    for (ShipDefinition sh : SHIP) {
      sh.save(out);
    }
  }

  // #endregion save

  // #region maintenance

  /**
   * Used to get the uncompressed size of the file.
   **/
  @Override
  public int getSize() {
    return SHIP.size() * 288;
  }

  @Override
  public void clear() {
    SHIP.clear();
    markChanged();
  }

  // #endregion maintenance

  // #region checks

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
    super.check(response);

    for (ShipDefinition sh : SHIP)
      sh.check(response);

    // check for missing name group
    checkNameGroups(response);

    // check for matching ship tech race id
    checkShipTechRace(response);

    // now check images
    checkImages(response);
  }

  private void checkNameGroups(Vector<String> response) {
    try {
      Shipname shn = (Shipname) stbof.getInternalFile(CStbofFiles.ShipNameBin, true);
      for (int i = 0; i < SHIP.size(); i++) {
        ShipDefinition sh = SHIP.get(i);
        if (!shn.hasEntries(sh.getNameGroup())) {
          String msg = Language.getString("ShipList.4"); //$NON-NLS-1$
          msg = msg.replace("%1", Integer.toString(sh.index())); //$NON-NLS-1$
          msg = msg.replace("%2", sh.getName()); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(sh.getNameGroup())); //$NON-NLS-1$
          response.add(getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
        }
      }
    } catch (Exception et) {
      response.add(getGenericErrorString(et.getMessage()));
    }
  }

  private void checkShipTechRace(Vector<String> response) {
    try {
      ShipTech sht = (ShipTech) stbof.getInternalFile(CStbofFiles.ShipTechSst, true);
      for (int i = 0; i < SHIP.size(); i++) {
        ShipDefinition sh = SHIP.get(i);
        if (sh.getRace() != sht.getRace(i)) {
          String msg = Language.getString("ShipList.0"); //$NON-NLS-1$
          msg = msg.replace("%1", getName()); //$NON-NLS-1$
          msg = msg.replace("%2", Integer.toString(sh.getRace())); //$NON-NLS-1$
          msg = msg.replace("%3", Integer.toString(sht.getRace(i))); //$NON-NLS-1$
          msg = msg.replace("%4", CStbofFiles.ShipTechSst); //$NON-NLS-1$
          response.add(msg);
          sht.setRace(i, sh.getRace());
          markChanged();
        }
      }
    } catch (Exception et) {
      response.add(getGenericErrorString(et.getMessage()));
    }
  }

  private void checkImages(Vector<String> response) {
    Trek trek = UE.FILES.trek();

    try (FileStore fs = stbof.access()) {
      val shipBaseReqs      = WDFImgReq.listShipBaseReqs(trek, stbof, response);
      val buildReqs         = WDFImgReq.listShipBuildReqs(trek, stbof, response);
      val shipRedeployReqs  = WDFImgReq.listShipRedeployReqs(trek, stbof, response);
      val starbaseReqs      = WDFImgReq.listStarbaseReqs(trek, stbof, response);
      HashSet<String> alreadyChecked = new HashSet<>();

      for (ShipDefinition ship : SHIP) {
        String gfx = ship.getGraphics();
        if (!alreadyChecked.add(gfx))
          continue;

        short shipRole = ship.getShipRole();
        WDFImgReq.checkStbofImageReqs(fs, stbof, this, gfx, shipBaseReqs, response);

        if (shipRole == ShipRole.Outpost || shipRole == ShipRole.Starbase) {
          WDFImgReq.checkStbofImageReqs(fs, stbof, this, gfx, starbaseReqs, response);
        }
        else if (shipRole != ShipRole.Monster) {
          WDFImgReq.checkStbofImageReqs(fs, stbof, this, gfx, buildReqs, response);
          WDFImgReq.checkStbofImageReqs(fs, stbof, this, gfx, shipRedeployReqs, response);
        }
      }
    }
    catch (Exception e) {
      String err = "Failed to check " + NAME + " images.";
      response.add(err);
    }
  }

  // #endregion checks

  // #region private helpers

  private void checkShipIndex(int index) {
    if (!hasShipIndex(index)) {
      String msg = Language.getString("ShipList.8"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
      throw new IndexOutOfBoundsException(msg);
    }
  }

  private String getGenericErrorString(String error) {
    String msg = Language.getString("ShipList.3"); //$NON-NLS-1$
    msg = msg.replace("%1", getName()); //$NON-NLS-1$
    msg = msg.replace("%2", error); //$NON-NLS-1$
    return msg;
  }

  // #endregion private helpers

}
