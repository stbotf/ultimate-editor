package ue.edit.res.stbof.files.sst.data;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Optional;
import java.util.TreeMap;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javax.swing.ImageIcon;
import org.apache.commons.lang3.RandomStringUtils;
import lombok.val;
import ue.edit.common.GenericFile;
import ue.edit.common.InternalFile;
import ue.edit.exe.trek.seg.shp.ShipModelSegment;
import ue.edit.exe.trek.seg.shp.data.ShipModelEntry;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.gif.GifImage;
import ue.edit.res.stbof.files.hob.HobFile;
import ue.edit.res.stbof.files.lst.PaletteList;
import ue.edit.res.stbof.files.lst.Texture;
import ue.edit.res.stbof.files.tga.TargaHeader;
import ue.edit.res.stbof.files.tga.TargaImage;
import ue.edit.res.stbof.files.tga.TargaImage.AlphaMode;
import ue.gui.util.GuiTools;
import ue.service.Language;
import ue.service.UEWorker;
import ue.util.file.ArchiveEntry;
import ue.util.file.FileFilters;
import ue.util.file.ZipStore;
import ue.util.img.GifColorMap;
import ue.util.stream.StreamTools;

/**
 * Represents a ship package file.
 * It must contain graphics files, the "phasers" file and texture.lst.
 */
public class ShipModelPackage implements ShipCollection {

  // keep a cached list of the file entries, sorted by name
  private TreeMap<String, ArchiveEntry> ZIP_ENTRIES = new TreeMap<>();
  private Hashtable<String, InternalFile> FILES = new Hashtable<>();
  private Vector<String> HOBS = new Vector<String>();
  private Vector<String> REMOVED = new Vector<String>();

  private Stbof stbof;
  private File pkg;
  private Texture TEXTURES = null;
  private PaletteList PALETTES = null;
  private ShipModelSegment PHASERS = null;
  private PrefixToStringList CLASS_LIST = null;
  private PrefixToStringList AUTHOR_LIST = null;

  public ShipModelPackage(Stbof stbof, File zip) throws IOException {
    this.stbof = stbof;
    load(zip);
  }

  public boolean hasFile(FilenameFilter filter) {
    // to process package files in sequence
    // first check the archive entries
    for (String lcName : ZIP_ENTRIES.keySet()) {
      if (filter == null || filter.accept(pkg, lcName)) {
        if (!REMOVED.contains(lcName))
          return true;
      }
    }

    // for added files also check the file cache
    for (String fileName : FILES.keySet())
      if (filter == null || filter.accept(pkg, fileName))
        return true;

    return false;
  }

  public HobFile getHobFile(String fileName) throws IOException {
    val file = getFile(fileName);
    if (file instanceof HobFile)
      return (HobFile) file;
    return new HobFile(fileName.toLowerCase(), ((GenericFile)file).getData());
  }

  public TargaImage getTargaImage(String fileName) throws IOException {
    val file = getFile(fileName);
    if (file instanceof TargaImage)
      return (TargaImage) file;
    return new TargaImage(fileName.toLowerCase(), ((GenericFile)file).getData());
  }

  public Optional<TargaHeader> tryGetTargaHeader(String fileName) throws IOException {
    val file = tryGetFile(fileName);
    if (!file.isPresent())
      return Optional.empty();

    InternalFile inf = file.get();
    if (inf instanceof TargaImage)
      return Optional.of(((TargaImage)inf).getHeader());

    val bais = new ByteArrayInputStream(((GenericFile)inf).getData());
    return Optional.of(new TargaHeader(null, bais));
  }

  public Optional<TargaImage> tryGetTargaImage(String fileName) throws IOException {
    val file = tryGetFile(fileName);
    if (!file.isPresent())
      return Optional.empty();

    InternalFile inf = file.get();
    if (inf instanceof TargaImage)
      return Optional.of((TargaImage)inf);

    val bais = new ByteArrayInputStream(((GenericFile)inf).getData());
    return Optional.of(new TargaImage(null, bais));
  }

  public GifImage getGifImage(String fileName) throws IOException {
    val file = getFile(fileName);
    if (file instanceof GifImage)
      return (GifImage) file;
    return new GifImage(fileName, ((GenericFile)file).getData());
  }

  public ImageIcon getImageIcon(String fileName, AlphaMode alphaMode) throws IOException {
    val file = getFile(fileName);
    if (file instanceof TargaImage) {
      Image img = ((TargaImage)file).getImage(alphaMode);
      return new ImageIcon(img);
    }

    byte[] data = ((GenericFile)file).getData();
    return new ImageIcon(data);
  }

  public InternalFile getFile(String fileName) throws IOException {
    return loadFile(fileName, true).get();
  }

  public Optional<InternalFile> tryGetFile(String fileName) {
    try {
      return loadFile(fileName, false);
    }
    catch (IOException e) {
      e.printStackTrace();
      return Optional.empty();
    }
  }

  @Override
  public long getFileSize(String string) throws IOException {
    String lc_string = string.toLowerCase();

    if (REMOVED.contains(lc_string)) {
      String msg = Language.getString("ShipModelPackage.1") //$NON-NLS-1$
        .replace("%1", string); //$NON-NLS-1$
      throw new IOException(msg);
    }

    if (FILES.containsKey(lc_string))
      return FILES.get(lc_string).getSize();

    // read files
    try (ZipFile zf = new ZipFile(pkg)) {
      Enumeration<? extends ZipEntry> en = zf.entries();

      while (en.hasMoreElements()) {
        ZipEntry ze = en.nextElement();
        if (ze.getName().toLowerCase().equals(lc_string))
          return ze.getSize();
      }
    }

    String msg = Language.getString("ShipModelPackage.1").replace("%1", string); //$NON-NLS-1$ //$NON-NLS-2$
    throw new IOException(msg);
  }

  /**
   * @param graphics ship model prefix
   * @return ship model data entry
   */
  public ShipModelEntry getShipModelEntry(String graphics) {
    return PHASERS.getEntry(graphics);
  }

  /**
   * @returns available ship prefixes.
   * @throws IOException
   * @throws ZipException
   */
  public String[] getCandidates() throws ZipException, IOException {
    ArrayList<String> a = new ArrayList<String>();

    for (int i = 0; i < PHASERS.getNumberOfEntries(); i++) {
      String s = PHASERS.getEntry(i).getGraphics();

      if (isGraphicsStringUsed(s)) {
        if (!a.contains(s))
          a.add(s);
      }
    }

    return a.toArray(new String[0]);
  }

  public void addHobFile(HobFile file) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    file.save(baos);

    GenericFile gen = new GenericFile(file.getName().toLowerCase(), baos.toByteArray());
    FILES.put(gen.getName(), gen);

    if (!HOBS.contains(gen.getName()))
      HOBS.add(gen.getName());

    REMOVED.remove(gen.getName());
  }

  public void addFile(InternalFile inf) {
    inf.setName(inf.getName().toLowerCase());
    REMOVED.remove(inf.getName());
    FILES.put(inf.getName(), inf);
  }

  public int addTexture(InternalFile gf, GifColorMap gcm) {
    if (TEXTURES == null)
      TEXTURES = new Texture();

    gf.setName(gf.getName().toLowerCase());

    while (TEXTURES.contains(gf.getName())) {
      gf.setName(RandomStringUtils.randomAlphanumeric(5).toLowerCase() + ".gif"); //$NON-NLS-1$
    }

    REMOVED.remove(gf.getName());
    FILES.put(gf.getName(), gf);

    int index;

    if (TEXTURES.getUnusedIndex() >= 0) {
      index = TEXTURES.getUnusedIndex();
      TEXTURES.set(index, gf.getName());
    } else {
      index = TEXTURES.add(gf.getName());
    }

    PALETTES.setColorMap(index, gcm);
    return index;
  }

  /**
   * @param str the graphics string (length 3)
   * @return true if the string is being used in this package
   * @throws IOException
   * @throws ZipException
   */
  public boolean isGraphicsStringUsed(String str) throws ZipException, IOException {
    str = str.toLowerCase();

    ArrayList<String> array = new ArrayList<String>();
    array.add("i_" + str + "w.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    array.add("i_" + str + "60.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    array.add("i_" + str + "30.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    array.add("i_" + str + "120.tga"); //$NON-NLS-1$ //$NON-NLS-2$
    array.add("i_" + str + "170.tga"); //$NON-NLS-1$ //$NON-NLS-2$

    for (int i = 0; i < array.size(); i++) {
      if (FILES.containsKey(array.get(i)))
        return true;
    }

    array.add(str + "_a.hob"); //$NON-NLS-1$
    array.add(str + "_b.hob"); //$NON-NLS-1$
    array.add(str + "_c.hob"); //$NON-NLS-1$

    for (int i = array.size() - 4; i < array.size(); i++) {
      if (HOBS.contains(array.get(i)))
        return true;
    }

    // gif
    Enumeration<String> files = FILES.keys();

    while (files.hasMoreElements()) {
      String f = files.nextElement();
      if (array.contains(f) || (f.startsWith(str + "_") && f.endsWith(".gif"))) //$NON-NLS-1$ //$NON-NLS-2$
        return true;
    }

    try (ZipFile zf = new ZipFile(pkg)) {
      Enumeration<? extends ZipEntry> zenum = zf.entries();

      while (zenum.hasMoreElements()) {
        ZipEntry ze = zenum.nextElement();

        if (array.contains(ze.getName().toLowerCase()))
          return true;

        if (ze.getName().toLowerCase().startsWith(str + "_") //$NON-NLS-1$
            && ze.getName().toLowerCase().endsWith(".gif")) //$NON-NLS-1$
          return true;
      }
    }

    return false;
  }

  public String getUnusedGraphicsString() throws ZipException, IOException {
    String g = RandomStringUtils.randomAlphanumeric(3);
    while (isGraphicsStringUsed(g)) {
      g = RandomStringUtils.randomAlphanumeric(3);
    }
    return g;
  }

  public void clearGraphicsString(String str) throws IOException {
    str = str.toLowerCase();
    String[] en = HOBS.toArray(new String[0]);
    ArrayList<Integer> tex = new ArrayList<Integer>();

    for (String f : en) {
      HobFile hob = getHobFile(f);

      if (hob.getName().startsWith(str + "_")) { //$NON-NLS-1$
        HOBS.remove(hob.getName());
        FILES.remove(hob.getName());
        REMOVED.add(hob.getName());
      } else {
        for (int j = 0; j < hob.numberOfTextures(); j++) {
          int index = hob.getTextureID(j);
          if (!tex.contains(index))
            tex.add(index);
        }
      }
    }

    // remove unused textures
    for (int j = TEXTURES.getTextureNum() - 1; j >= 0; --j) {
      if (!tex.contains(j) && TEXTURES.isTextureValid(j)) {
        FILES.remove(TEXTURES.get(j));
        REMOVED.add(TEXTURES.get(j));
        // remove from the back to not invalidate the iteration index
        // (even though they actually are just marked, not removed)
        TEXTURES.remove(j);
      }
    }

    // graphics files
    String file = "i_" + str + "120.tga"; //$NON-NLS-1$ //$NON-NLS-2$
    FILES.remove(file);
    REMOVED.add(file);

    file = "i_" + str + "w.tga"; //$NON-NLS-1$ //$NON-NLS-2$
    FILES.remove(file);
    REMOVED.add(file);

    file = "i_" + str + "30.tga"; //$NON-NLS-1$ //$NON-NLS-2$
    FILES.remove(file);
    REMOVED.add(file);

    file = "i_" + str + "60.tga"; //$NON-NLS-1$ //$NON-NLS-2$
    FILES.remove(file);
    REMOVED.add(file);

    file = "i_" + str + "170.tga"; //$NON-NLS-1$ //$NON-NLS-2$
    FILES.remove(file);
    REMOVED.add(file);

    // phasers
    PHASERS.removeEntry(str);

    // class list
    if (CLASS_LIST != null)
      CLASS_LIST.remove(str);

    // author list
    if (AUTHOR_LIST != null)
      AUTHOR_LIST.remove(str);
  }

  /**
   * @param entry
   */
  public void addShipModelDataEntry(ShipModelEntry entry) {
    if (PHASERS == null)
      PHASERS = new ShipModelSegment(stbof);
    PHASERS.addEntry(entry);
  }

  public boolean save(File dest) {
    if (dest == null)
      dest = pkg;

    SaveTask task = new SaveTask(dest);
    return GuiTools.runUEWorker(task);
  }

  public int getTextureNum() {
    return TEXTURES.getTextureNum();
  }

  public boolean isTextureValid(int i) {
    return TEXTURES.isTextureValid(i);
  }

  public void removeTexture(int i) {
    TEXTURES.remove(i);
  }

  public byte[] getInternalFileHash(String fileName) throws IOException, NoSuchAlgorithmException {
    MessageDigest m = MessageDigest.getInstance("MD5"); //$NON-NLS-1$
    val file = getFile(fileName);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    file.save(baos);
    m.update(baos.toByteArray(), 0, baos.size());

    return m.digest();
  }

  public int getTextureIndex(String texture) {
    return TEXTURES != null ? TEXTURES.getTextureIndex(texture) : -1;
  }

  public GifColorMap getColorMap(int i) {
    return PALETTES.getColorMap(i);
  }

  public String getTexture(Integer i) {
    return TEXTURES.get(i);
  }

  @Override
  public String getClassByPrefix(String prefix) {
    return CLASS_LIST != null ? CLASS_LIST.getClassByPrefix(prefix) : null;
  }

  public void setClassForPrefix(String prefix, String cls) {
    if (CLASS_LIST == null)
      CLASS_LIST = new PrefixToStringList();
    CLASS_LIST.put(prefix, cls);
  }

  @Override
  public String getAuthorByPrefix(String prefix) {
    return AUTHOR_LIST != null ? AUTHOR_LIST.getClassByPrefix(prefix) : null;
  }

  public void setAuthorForPrefix(String prefix, String author) {
    if (AUTHOR_LIST == null)
      AUTHOR_LIST = new PrefixToStringList();
    AUTHOR_LIST.put(prefix, author);
  }

  /**
   * @param string the name of the file
   * @return true if the file is in this ship pack
   * @throws IOException
   * @throws ZipException
   */
  @Override
  public boolean containsFile(String string) throws  IOException {
    String lc_string = string.toLowerCase();

    if (!REMOVED.contains(lc_string)) {
      if (FILES.containsKey(lc_string))
        return true;

      try (ZipFile zf = new ZipFile(pkg)) {
        // read files
        Enumeration<? extends ZipEntry> en = zf.entries();

        while (en.hasMoreElements()) {
          ZipEntry ze = en.nextElement();
          File file = new File(ze.getName());
          if (file.getName().equalsIgnoreCase(lc_string))
            return true;
        }
      }
    }

    return false;
  }

  private void load(File zip) throws IOException {
    ZIP_ENTRIES.clear();
    pkg = zip;

    if (pkg.exists()) {
      try (ZipStore zf = new ZipStore(zip)) {
        // read files
        zf.processFiles((fs, ze) -> {
          loadZipEntry(zf, ze);
          return true;
        });
      }

      if (PHASERS == null)
        throw new IOException(Language.getString("ShipModelPackage.0")); //$NON-NLS-1$
    } else {
      PHASERS = new ShipModelSegment(stbof);
      PALETTES = new PaletteList();
      TEXTURES = new Texture();

      zip.createNewFile();

      try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip))) {
        out.putNextEntry(new ZipEntry("phasers")); //$NON-NLS-1$
        PHASERS.save(out);
        out.closeEntry();

        out.putNextEntry(new ZipEntry(CStbofFiles.TextureLst));
        TEXTURES.save(out);
        out.closeEntry();

        out.putNextEntry(new ZipEntry(CStbofFiles.PaletteLst));
        PALETTES.save(out);
        out.closeEntry();
      }
    }
  }

  private void loadZipEntry(ZipStore zf, ArchiveEntry ze) throws IOException {
    // cache entry list
    ZIP_ENTRIES.put(ze.getLcName(), ze);

    File file = new File(ze.getName());
    String fileName = file.getName().toLowerCase();

    switch (fileName) {
      case CStbofFiles.TextureLst:
        try (InputStream in = zf.getInputStream(ze, false)) {
          TEXTURES = new Texture(in);
        }
        break;
      case CStbofFiles.PaletteLst:
        try (InputStream in = zf.getInputStream(ze, false)) {
          PALETTES = new PaletteList(in);
        }
        break;
      default: {
        if (FileFilters.Hob.accept(file, fileName)) {
          HOBS.add(file.getName().toLowerCase());
        } else if (fileName.endsWith("phasers")) { //$NON-NLS-1$
          try (InputStream in = zf.getInputStream(ze, false)) {
            // read from in, read until end of stream
            PHASERS = new ShipModelSegment(stbof, in, ShipModelSegment.UNLIMITED_MAX_SHIPMODELS);
          }
        } else if (FileFilters.ClassList.accept(file, fileName)) {
          try (InputStream in = zf.getInputStream(ze, false)) {
            CLASS_LIST = new PrefixToStringList(in);
          }
        } else if (FileFilters.AuthorList.accept(file, fileName)) {
          try (InputStream in = zf.getInputStream(ze, false)) {
            AUTHOR_LIST = new PrefixToStringList(in);
          }
        }
      }
    }
  }

  private Optional<InternalFile> loadFile(String fileName, boolean throwIfMissing) throws IOException {
    String lc_fileName = fileName.toLowerCase();

    // check if removed
    if (REMOVED.contains(lc_fileName)) {
      if (throwIfMissing) {
        String msg = Language.getString("ShipModelPackage.1") //$NON-NLS-1$
          .replace("%1", fileName); //$NON-NLS-1$
        throw new FileNotFoundException(msg);
      }
      return Optional.empty();
    }

    // check if cached
    if (FILES.containsKey(lc_fileName))
      return Optional.of(FILES.get(lc_fileName));

    return loadZipFile(fileName, throwIfMissing);
  }

  private Optional<InternalFile> loadZipFile(String fileName, boolean throwIfMissing) throws IOException {
    try (ZipFile zf = new ZipFile(pkg)) {
      Enumeration<? extends ZipEntry> en = zf.entries();

      while (en.hasMoreElements()) {
        ZipEntry ze = en.nextElement();

        if (ze.getName().equalsIgnoreCase(fileName)) {
          try (InputStream in = zf.getInputStream(ze)) {
            byte[] b = StreamTools.readAllBytes(in);
            GenericFile f = new GenericFile(fileName, b);
//          FILES.put(fileName.toLowerCase(), f);
            return Optional.of(f);
          }
          catch (Exception e) {
            String msg = Language.getString("ShipModelPackage.2") //$NON-NLS-1$
              .replace("%1", fileName); //$NON-NLS-1$
            throw new IOException(msg, e);
          }
        }
      }
    }

    // not found
    if (throwIfMissing) {
      String msg = Language.getString("ShipModelPackage.1").replace("%1", fileName); //$NON-NLS-1$ //$NON-NLS-2$
      throw new FileNotFoundException(msg);
    }
    return Optional.empty();
  }

  private class SaveTask extends UEWorker<Boolean> {

    private static final String TmpFileName = "pkg.tmp"; //$NON-NLS-1$

    File DEST;

    public SaveTask(File dest) {
      DEST = dest;
      RESULT = false;
    }

    @Override
    public Boolean work() throws IOException {
      String msg = Language.getString("ShipModelPackage.3"); //$NON-NLS-1$

      //where to
      if (DEST.equals(pkg))
        DEST = new File(TmpFileName);

      DEST.delete();
      DEST.createNewFile();

      try (
        ZipFile source = new ZipFile(pkg);
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(DEST));
      ) {
        for (Enumeration<? extends ZipEntry> e = source.entries(); e.hasMoreElements(); ) {
          if (isCancelled()) {
            DEST.delete();
            return false;
          }

          ZipEntry ze = e.nextElement();
          String filename = ze.getName();

          if (REMOVED.contains(filename.toLowerCase())
            || filename.equals(CStbofFiles.TextureLst)
            || filename.equals(CStbofFiles.PaletteLst)
            || filename.endsWith("phasers") //$NON-NLS-1$
            || filename.endsWith("class.lst") //$NON-NLS-1$
            || filename.endsWith("author.lst")) //$NON-NLS-1$
            continue;

          if (!FILES.containsKey(filename.toLowerCase())) {
            ZipEntry zfl = new ZipEntry(filename);
            feedMessage(msg.replace("%1", filename)); //$NON-NLS-1$

            try (InputStream is = source.getInputStream(zfl)) {
              int ln = is.available();
              zfl.setSize(ln);
              zfl.setCompressedSize(-1);
              zfl.setMethod(8);
              zos.putNextEntry(zfl);

              int len;
              byte[] buf = new byte[1024];

              while ((len = is.read(buf)) > 0) {
                zos.write(buf, 0, len);
              }
            }

            zos.flush();
          }

          // Complete the entry
          zos.closeEntry();
        }

        //go thru loaded files
        val itg = FILES.elements();

        while (itg.hasMoreElements()) {
          if (isCancelled()) {
            DEST.delete();
            return false;
          }

          InternalFile file = itg.nextElement();
          feedMessage(msg.replace("%1", file.getName())); //$NON-NLS-1$

          ZipEntry zeg = new ZipEntry(file.getName().toLowerCase());
          zeg.setCompressedSize(-1);
          zeg.setMethod(8);
          zeg.setSize(file.getSize());
          zos.putNextEntry(zeg);

          file.save(zos);
          zos.flush();
          zos.closeEntry();
        }

        try {
          ZipEntry zeg = new ZipEntry("phasers"); //$NON-NLS-1$
          zeg.setCompressedSize(-1);
          zeg.setMethod(8);
          zeg.setSize(PHASERS.getSize());
          zos.putNextEntry(zeg);
          PHASERS.save(zos);
          zos.flush();
          zos.closeEntry();

          zeg = new ZipEntry(CStbofFiles.TextureLst);
          zeg.setCompressedSize(-1);
          zeg.setMethod(8);
          zeg.setSize(TEXTURES.getSize());
          zos.putNextEntry(zeg);
          TEXTURES.save(zos);
          zos.flush();
          zos.closeEntry();

          zeg = new ZipEntry(CStbofFiles.PaletteLst);
          zeg.setCompressedSize(-1);
          zeg.setMethod(8);
          zeg.setSize(TEXTURES.getSize());
          zos.putNextEntry(zeg);
          PALETTES.save(zos);
          zos.flush();
          zos.closeEntry();

          if (CLASS_LIST != null) {
            zeg = new ZipEntry("class.lst"); //$NON-NLS-1$
            zeg.setCompressedSize(-1);
            zeg.setMethod(8);
            zeg.setSize(CLASS_LIST.getSize());
            zos.putNextEntry(zeg);
            CLASS_LIST.save(zos);
            zos.flush();
            zos.closeEntry();
          }

          if (AUTHOR_LIST != null) {
            zeg = new ZipEntry("author.lst"); //$NON-NLS-1$
            zeg.setCompressedSize(-1);
            zeg.setMethod(8);
            zeg.setSize(AUTHOR_LIST.getSize());
            zos.putNextEntry(zeg);
            AUTHOR_LIST.save(zos);
            zos.flush();
            zos.closeEntry();
          }
        } catch (Exception ze) {
          DEST.delete();
          throw ze;
        }
      }

      if (isCancelled()) {
        DEST.delete();
        return false;
      }

      if (DEST.getName().equals(TmpFileName)) {
        // replace with TMP file
        // on error keep temp file for manual rename
        Files.copy(DEST.toPath(), pkg.toPath(), StandardCopyOption.REPLACE_EXISTING);

        if (!DEST.delete()) {
          // the save operation succeeded, just the temp file couldn't be removed
          msg = Language.getString("ShipModelPackage.4"); //$NON-NLS-1$
          logError(msg.replace("%1", DEST.getName())); //$NON-NLS-1$
        }
      }

      return true;
    }
  }

}
