package ue.edit.res.stbof.files.sst.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * @author Alan Podlesek
 */
public class PrefixToStringList extends InternalFile {

  private Hashtable<String, String> table = new Hashtable<String, String>();

  public PrefixToStringList() {
  }

  public PrefixToStringList(InputStream in) throws IOException {
    load(in);
  }

  public void put(String prefix, String cls) {
    table.put(prefix.toUpperCase(), cls);
  }

  public void remove(String prefix) {
    table.remove(prefix.toUpperCase());
  }

  public String getClassByPrefix(String prefix) {
    return table.get(prefix.toUpperCase());
  }

  /**
   * @return file size
   */
  @Override
  public int getSize() {
    return table.size() * 44;
  }

  @Override
  public void load(InputStream in) throws IOException {
    table.clear();

    while (in.available() > 0) {
      String prefix = StreamTools.readNullTerminatedBotfString(in, 4);
      String cls = StreamTools.readNullTerminatedBotfString(in, 40);
      table.put(prefix.toUpperCase(), cls);
    }

    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    Enumeration<String> en = table.keys();
    String key;

    while (en.hasMoreElements()) {
      key = en.nextElement();

      out.write(DataTools.toByte(key.toUpperCase(), 4, 3));
      out.write(DataTools.toByte(table.get(key), 40, 39));
    }
  }

  @Override
  public void clear() {
    table.clear();
    markChanged();
  }

  /* (non-Javadoc)
   * @see ue.edit.InternalFile#check()
   */
  @Override
  public void check(Vector<String> response) {
  }
}
