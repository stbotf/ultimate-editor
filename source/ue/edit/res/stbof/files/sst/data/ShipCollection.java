package ue.edit.res.stbof.files.sst.data;

import java.io.IOException;
import java.util.zip.ZipException;

/**
 * Interface for a ship package and stbof ship list file.
 */
public interface ShipCollection {

  public long getFileSize(String string) throws IOException;

  public String getClassByPrefix(String prefix);

  public String getAuthorByPrefix(String prefix);

  /**
   * @param string the name of the file
   * @return true if the file is in this ship pack
   * @throws IOException
   * @throws ZipException
   */
  public boolean containsFile(String string) throws IOException;
}
