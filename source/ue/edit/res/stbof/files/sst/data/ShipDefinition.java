package ue.edit.res.stbof.files.sst.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import java.util.Vector;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.files.desc.Descriptable;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.edit.res.stbof.files.sst.ShipList;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

/**
 * Reprisents a ship in shiplist.sst.
 */
@Getter
public class ShipDefinition extends Descriptable implements Comparable<ShipDefinition> {

  //  public String NAME;                 // 0x00  + 40 = 40
  // '4048AB' written to the last 4 bytes of the class name looks like a trek.exe address pointer
  // that address however is invalid and by previous analysis it is found to be part of the class name
  // @see https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?t=2729
  private String shipClass;               // 0x28  + 44 = 84
  // private int super.descAddrss;        // 0x54  +  4 = 88
  private String graphics;                // 0x58  + 14 = 102
  @Accessors(fluent=true)
  private short index;                    // 0x66  +  2 = 104
  private short shipRole;                 // 0x68  +  2 = 106 -> ship type / function
  private short nameGroup;                // 0x6A  +  2 = 108 -> shipname group used
  private int beamAccuracy;               // 0x6C  +  4 = 112
  private int beamNumber;                 // 0x70  +  4 = 116
  private int beamMultiplier;             // 0x74  +  4 = 120 -> takes effect after hitting shields (effects only hull)
  private int beamDamage;                 // 0x78  +  4 = 124
  private int beamPenetration;            // 0x7C  +  4 = 128
  private int torpedoAccuracy;            // 0x80  +  4 = 132
  private int torpedoNumber;              // 0x84  +  4 = 136
  private int torpedoMultiplier;          // 0x88  +  4 = 140
  private int torpedoDamage;              // 0x8C  +  4 = 144
  private int shieldLevel;                // 0x90  +  4 = 148
  private int shieldStrength;             // 0x94  +  4 = 152
  private int shieldRecharge;             // 0x98  +  4 = 156
  // auto-calculate by: (weapon num * damage * multiplier * accuracy * 0.01) [modifier/X?]
  // added for each weapon type: Phaser + Torpedo + Third hidden beam stats
  // @see SCT https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=56589#p56589
  private int offensiveStrength;          // 0x9C  +  4 = 160 -> AI offensive capability
  // auto-calculate by: (shields + hull) * defense * 0.01 * modifier X?
  private int defensiveStrength;          // 0xA0  +  4 = 164 -> AI defensive capability
  private int unknown_1;                  // 0xA4  +  4 = 168
  private int hullStrength;               // 0xA8  +  4 = 172
  private int defense;                    // 0xAC  +  4 = 176
  private int stealth;                    // 0xB0  +  4 = 180
  private int superRayNumber;             // 0xB4  +  4 = 184 e.g. 1,    assumed plasma weapon slots that actually are beam weapons
  private int superRayAccuracy;           // 0xB8  +  4 = 188 e.g. 150   likely for the refitted future TNG Enterprise-D heavy cannon
  private int superRayMultiplier;         // 0xBC  +  4 = 192 e.g. 8     from episode "All Good Things..."
  private int superRayDamage;             // 0xC0  +  4 = 196 e.g. 80
  private int superRayPenetration;        // 0xC4  +  4 = 200 e.g. 12
  private int unknown_2;                  // 0xC8  +  4 = 204 e.g. 1
  private long unknown_3;                 // 0xCC  +  8 = 212 e.g. 0
  private double agility;                 // 0xD4  +  8 = 220
  // @see SCT https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?p=52676#p52676
  private double firingArc;               // 0xDC  +  8 = 228
  private double maxBeamRange;            // 0xE4  +  8 = 236
  private double minTorpedoRange;         // 0xEC  +  8 = 244
  private double maxTorpedoRange;         // 0xF4  +  8 = 252
  private double maxSuperRayRange;        // 0xFC  +  8 = 260 e.g. 350
  private double mapRange;                // 0x104 +  8 = 268
  private double mapSpeed;                // 0x10C +  8 = 276
  private int scanRange;                  // 0x114 +  2 = 278 unsigned short!
  private int production;                 // 0x116 +  2 = 280 unsigned short!
  private short race;                     // 0x118 +  2 = 282
  private char phaserPrefix;              // 0x11A +  1 = 283 works only if the appropriate trek.exe patch is applied
  private char torpedoPrefix;             // 0x11B +  1 = 284 works only if the appropriate trek.exe patch is applied
  private int buildCost;                  // 0x11C +  2 = 286 unsigned short!
  private int maintenance;                // 0x11E +  2 = 288 unsigned short!

  // this is not part of ship data, but is used for overriding ship race
  private short race_override;

  private ShipList shipList;

  public ShipDefinition(ShipList shipList, InputStream in) throws IOException {
    super(shipList);
    this.shipList = shipList;
    load(in);
  }

  // #region getters

  public String descriptor() {
    Stbof stbof = shipList.stbof();
    Optional<RaceRst> raceRst = stbof != null ? stbof.files().findRaceRst() : Optional.empty();
    return descriptor(raceRst, shipList);
  }

  private String descriptor(Optional<RaceRst> raceRst, ShipList shipModels) {
    String raceName = raceRst.isPresent() ? raceRst.get().getOwnerName(race) : "Race " + Integer.toString(race);
    String catName = LexHelper.mapShipRole(shipRole);
    String shpId = Integer.toString(index);
    return raceName + " " + catName + " " + shpId + " \"" + name + "\"";
  }

  // #endregion getters

  // #region setters

  /**
   * Sets the ship index.
   */
  public void setIndex(short index) {
    if (this.index != index) {
      this.index = index;
      markChanged();
    }
  }

  /**
   * Sets graphics image name prefix.
   */
  public void setGraphics(String gfx) {
    if (!StringTools.equals(graphics, gfx)) {
      graphics = gfx;
      markChanged();
    }
  }

  /**
   * Sets ship class.
   */
  public void setShipClass(String shipClass) {
    if (!StringTools.equals(this.shipClass, shipClass)) {
      this.shipClass = shipClass;
      markChanged();
    }
  }

  /**
   * Sets ship role.
   */
  public void setShipRole(short shipRole) {
    if (this.shipRole != shipRole) {
      this.shipRole = shipRole;
      markChanged();
    }
  }

  /**
   * Sets name group.
   */
  public void setNameGroup(short nameGroup) {
    if (this.nameGroup != nameGroup) {
      this.nameGroup = nameGroup;
      markChanged();
    }
  }

  /**
   * Sets map range.
   */
  public void setMapRange(double range) {
    if (this.mapRange != range) {
      this.mapRange = range;
      markChanged();
    }
  }

  /**
   * Sets map speed.
   */
  public void setMapSpeed(double speed) {
    if (this.mapSpeed != speed) {
      this.mapSpeed = speed;
      markChanged();
    }
  }

  /**
   * Sets scan range.
   */
  public void setScanRange(int scanRange) {
    if (this.scanRange != scanRange) {
      this.scanRange = scanRange;
      markChanged();
    }
  }

  /**
   * Sets race.
   * @return true if changed
   */
  public boolean setRace(short race, boolean adjust_ovr) {
    if (this.race != race) {
      // race override is not being used
      if (adjust_ovr && this.race == race_override)
        race_override = race;

      this.race = race;
      markChanged();
      return true;
    }

    return false;
  }

  /**
   * Sets build cost.
   */
  public void setBuildCost(int cost) {
    if (this.buildCost != cost) {
      this.buildCost = cost;
      markChanged();
    }
  }

  /**
   * Sets maintenance.
   */
  public void setMaintenance(int maintenance) {
    if (this.maintenance != maintenance) {
      this.maintenance = maintenance;
      markChanged();
    }
  }

  /**
   * Sets stealth level. If above 3 it can cloak.
   */
  public void setStealth(int stealth) {
    if (this.stealth != stealth) {
      this.stealth = stealth;
      markChanged();
    }
  }

  /**
   * Sets beam accuracy.
   */
  public void setBeamAccuracy(int accuracy) {
    if (this.beamAccuracy != accuracy) {
      this.beamAccuracy = accuracy;
      markChanged();
    }
  }

  /**
   * Sets beam number.
   */
  public void setBeamNumber(int number) {
    if (this.beamNumber != number) {
      this.beamNumber = number;
      markChanged();
    }
  }

  /**
   * Sets beam multiplier.
   */
  public void setBeamMultiplier(int multi) {
    if (this.beamMultiplier != multi) {
      this.beamMultiplier = multi;
      markChanged();
    }
  }

  /**
   * Sets beam damage.
   */
  public void setBeamDamage(int damage) {
    if (this.beamDamage != damage) {
      this.beamDamage = damage;
      markChanged();
    }
  }

  /**
   * Sets beam shield penetration.
   */
  public void setBeamPenetration(int penetration) {
    if (this.beamPenetration != penetration) {
      this.beamPenetration = penetration;
      markChanged();
    }
  }

  /**
   * Sets firing arc.
   */
  public void setFiringArc(double firingArc) {
    if (this.firingArc != firingArc) {
      this.firingArc = firingArc;
      markChanged();
    }
  }

  /**
   * Sets max beam range.
   */
  public void setMaxBeamRange(double maxRange) {
    if (this.maxBeamRange != maxRange) {
      this.maxBeamRange = maxRange;
      markChanged();
    }
  }

  /**
   * Sets torpedo accuracy.
   */
  public void setTorpedoAccuracy(int accuracy) {
    if (this.torpedoAccuracy != accuracy) {
      this.torpedoAccuracy = accuracy;
      markChanged();
    }
  }

  /**
   * Sets torpedo number.
   */
  public void setTorpedoNumber(int number) {
    if (this.torpedoNumber != number) {
      this.torpedoNumber = number;
      markChanged();
    }
  }

  /**
   * Sets torpedo multiplier.
   */
  public void setTorpedoMultiplier(int multi) {
    if (this.torpedoMultiplier != multi) {
      this.torpedoMultiplier = multi;
      markChanged();
    }
  }

  /**
   * Sets torpedo damage.
   */
  public void setTorpedoDamage(int damage) {
    if (this.torpedoDamage != damage) {
      this.torpedoDamage = damage;
      markChanged();
    }
  }

  /**
   * Sets min torpedo range.
   */
  public void setMinTorpedoRange(double minRange) {
    if (this.minTorpedoRange != minRange) {
      this.minTorpedoRange = minRange;
      markChanged();
    }
  }

  /**
   * Sets max torpedo range.
   */
  public void setMaxTorpedoRange(double maxRange) {
    if (this.maxTorpedoRange != maxRange) {
      this.maxTorpedoRange = maxRange;
      markChanged();
    }
  }

  /**
   * Sets super ray number.
   */
  public void setSuperRayNumber(int number) {
    if (this.superRayNumber != number) {
      this.superRayNumber = number;
      markChanged();
    }
  }

  /**
   * Sets super ray accuracy.
   */
  public void setSuperRayAccuracy(int accuracy) {
    if (this.superRayAccuracy != accuracy) {
      this.superRayAccuracy = accuracy;
      markChanged();
    }
  }

  /**
   * Sets super ray multiplier.
   */
  public void setSuperRayMultiplier(int multiplier) {
    if (this.superRayMultiplier != multiplier) {
      this.superRayMultiplier = multiplier;
      markChanged();
    }
  }

  /**
   * Sets super ray damage.
   */
  public void setSuperRayDamage(int damage) {
    if (this.superRayDamage != damage) {
      this.superRayDamage = damage;
      markChanged();
    }
  }

  /**
   * Sets super ray shield penetration.
   */
  public void setSuperRayPenetration(int penetration) {
    if (this.superRayPenetration != penetration) {
      this.superRayPenetration = penetration;
      markChanged();
    }
  }

  /**
   * Sets max super ray range.
   */
  public void setMaxSuperRayRange(double maxRange) {
    if (this.maxSuperRayRange != maxRange) {
      this.maxSuperRayRange = maxRange;
      markChanged();
    }
  }

  /**
   * Sets shield level.
   */
  public void setShieldLvl(int shieldLvl) {
    if (this.shieldLevel != shieldLvl) {
      this.shieldLevel = shieldLvl;
      markChanged();
    }
  }

  /**
   * Sets shield strength.
   */
  public void setShieldStrength(int shield) {
    if (this.shieldStrength != shield) {
      this.shieldStrength = shield;
      markChanged();
    }
  }

  /**
   * Sets shield recharge.
   */
  public void setShieldRecharge(int recharge) {
    if (this.shieldRecharge != recharge) {
      this.shieldRecharge = recharge;
      markChanged();
    }
  }

  /**
   * Sets hull strength.
   */
  public void setHullStrength(int hull) {
    if (this.hullStrength != hull) {
      this.hullStrength = hull;
      markChanged();
    }
  }

  /**
   * Sets defense.
   */
  public void setDefense(int defense) {
    if (this.defense != defense) {
      this.defense = defense;
      markChanged();
    }
  }

  /**
   * Sets production.
   */
  public void setProduction(int production) {
    if (this.production != production) {
      this.production = production;
      markChanged();
    }
  }

  /**
   * Sets agility.
   */
  public void setAgility(double agility) {
    if (this.agility != agility) {
      this.agility = agility;
      markChanged();
    }
  }

  /**
   * Sets offensive strength.
   */
  public void setOffensiveStrength(int strength) {
    if (this.offensiveStrength != strength) {
      this.offensiveStrength = strength;
      markChanged();
    }
  }

  /**
   * Sets defensive strength.
   */
  public void setDefensiveStrength(int strength) {
    if (this.defensiveStrength != strength) {
      this.defensiveStrength = strength;
      markChanged();
    }
  }

  /**
   * Sets unused race override.
   */
  public void setRaceOverride(short race) {
    race_override = race;
  }

  public void setPhaserPrefix(char prefix) {
    if (this.phaserPrefix != prefix) {
      this.phaserPrefix = prefix;
      markChanged();
    }
  }

  public void setTorpedoPrefix(char prefix) {
    if (this.torpedoPrefix != prefix) {
      this.torpedoPrefix = prefix;
      markChanged();
    }
  }

  // #endregion setters

  private void load(InputStream in) throws IOException {
    name = StreamTools.readNullTerminatedBotfString(in, 40);                    // 0-40
    shipClass = StreamTools.readNullTerminatedBotfString(in, 44);               // 40-84
    loadDescAddress(in);                                                        // 84-88
    graphics = StreamTools.readNullTerminatedBotfString(in, 14).toLowerCase();  // 88-102
    index = StreamTools.readShort(in, true);                                    // 102-104
    shipRole = StreamTools.readShort(in, true);                                 // 104-106
    nameGroup = StreamTools.readShort(in, true);                                // 106-108
    beamAccuracy = StreamTools.readInt(in, true);                               // 108-112
    beamNumber = StreamTools.readInt(in, true);                                 // 112-116
    beamMultiplier = StreamTools.readInt(in, true);                             // 116-120
    beamDamage = StreamTools.readInt(in, true);                                 // 120-124
    beamPenetration = StreamTools.readInt(in, true);                            // 124-128
    torpedoAccuracy = StreamTools.readInt(in, true);                            // 128-132
    torpedoNumber = StreamTools.readInt(in, true);                              // 132-136
    torpedoMultiplier = StreamTools.readInt(in, true);                          // 136-140
    torpedoDamage = StreamTools.readInt(in, true);                              // 140-144
    shieldLevel = StreamTools.readInt(in, true);                                // 144-148
    shieldStrength = StreamTools.readInt(in, true);                             // 148-152
    shieldRecharge = StreamTools.readInt(in, true);                             // 152-156
    offensiveStrength = StreamTools.readInt(in, true);                          // 156-160
    defensiveStrength = StreamTools.readInt(in, true);                          // 160-164
    unknown_1 = StreamTools.readInt(in, true);                                  // 164-168
    hullStrength = StreamTools.readInt(in, true);                               // 168-172
    defense = StreamTools.readInt(in, true);                                    // 172-176
    stealth = StreamTools.readInt(in, true);                                    // 176-180
    superRayNumber = StreamTools.readInt(in, true);                             // 180-184
    superRayAccuracy = StreamTools.readInt(in, true);                           // 184-188
    superRayMultiplier = StreamTools.readInt(in, true);                         // 188-192
    superRayDamage = StreamTools.readInt(in, true);                             // 192-196
    superRayPenetration = StreamTools.readInt(in, true);                        // 196-200
    unknown_2 = StreamTools.readInt(in, true);                                  // 200-204
    unknown_3 = StreamTools.readLong(in, true);                                 // 204-212
    agility = StreamTools.readDouble(in, true);                                 // 212-220
    firingArc = StreamTools.readDouble(in, true);                               // 220-228
    maxBeamRange = StreamTools.readDouble(in, true);                            // 228-236
    minTorpedoRange = StreamTools.readDouble(in, true);                         // 236-244
    maxTorpedoRange = StreamTools.readDouble(in, true);                         // 244-252
    maxSuperRayRange = StreamTools.readDouble(in, true);                        // 252-260
    mapRange = StreamTools.readDouble(in, true);                                // 260-268
    mapSpeed = StreamTools.readDouble(in, true);                                // 268-276
    scanRange = StreamTools.readUShort(in, true);                               // 276-278
    production = StreamTools.readUShort(in, true);                              // 278-280
    race = StreamTools.readShort(in, true);                                     // 280-282
    race_override = race;
    phaserPrefix = (char) in.read();                                            // 282-283
    torpedoPrefix = (char) in.read();                                           // 283-284
    buildCost = StreamTools.readUShort(in, true);                               // 284-286
    maintenance = StreamTools.readUShort(in, true);                             // 286-288
  }

  /**
   * Saves changes.
   *
   * @return true if save has been successful.
   * @throws IOException
   */
  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(name, 40, 39));
    out.write(DataTools.toByte(shipClass, 44, 43));
    out.write(DataTools.toByte(descAddress, true));
    out.write(DataTools.toByte(graphics, 14, 13));
    out.write(DataTools.toByte(index, true));
    out.write(DataTools.toByte(shipRole, true));
    out.write(DataTools.toByte(nameGroup, true));
    out.write(DataTools.toByte(beamAccuracy, true));
    out.write(DataTools.toByte(beamNumber, true));
    out.write(DataTools.toByte(beamMultiplier, true));
    out.write(DataTools.toByte(beamDamage, true));
    out.write(DataTools.toByte(beamPenetration, true));
    out.write(DataTools.toByte(torpedoAccuracy, true));
    out.write(DataTools.toByte(torpedoNumber, true));
    out.write(DataTools.toByte(torpedoMultiplier, true));
    out.write(DataTools.toByte(torpedoDamage, true));
    out.write(DataTools.toByte(shieldLevel, true));
    out.write(DataTools.toByte(shieldStrength, true));
    out.write(DataTools.toByte(shieldRecharge, true));
    out.write(DataTools.toByte(offensiveStrength, true));
    out.write(DataTools.toByte(defensiveStrength, true));
    out.write(DataTools.toByte(unknown_1, true));
    out.write(DataTools.toByte(hullStrength, true));
    out.write(DataTools.toByte(defense, true));
    out.write(DataTools.toByte(stealth, true));
    out.write(DataTools.toByte(superRayNumber, true));
    out.write(DataTools.toByte(superRayAccuracy, true));
    out.write(DataTools.toByte(superRayMultiplier, true));
    out.write(DataTools.toByte(superRayDamage, true));
    out.write(DataTools.toByte(superRayPenetration, true));
    out.write(DataTools.toByte(unknown_2, true));
    out.write(DataTools.toByte(unknown_3, true));
    out.write(DataTools.toByte(agility, true));
    out.write(DataTools.toByte(firingArc, true));
    out.write(DataTools.toByte(maxBeamRange, true));
    out.write(DataTools.toByte(minTorpedoRange, true));
    out.write(DataTools.toByte(maxTorpedoRange, true));
    out.write(DataTools.toByte(maxSuperRayRange, true));
    out.write(DataTools.toByte(mapRange, true));
    out.write(DataTools.toByte(mapSpeed, true));
    out.write(DataTools.toByte((short)scanRange, true));
    out.write(DataTools.toByte((short)production, true));
    out.write(DataTools.toByte(race_override, true));
    out.write(phaserPrefix);
    out.write(torpedoPrefix);
    out.write(DataTools.toByte((short)buildCost, true));
    out.write(DataTools.toByte((short)maintenance, true));
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @param name The parent ShipList file name.
   * @param response A Vector object containing results of the check as Strings.
   */
  @Override
  public void check(Vector<String> response) {
    super.check(response);

    String descriptor = this.descriptor();

    if (name.length() > 39) {
      String msg = Language.getString("ShipDefinition.0") //$NON-NLS-1$
        .replace("%1", descriptor); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (shipClass.length() > 43) {
      String msg = Language.getString("ShipDefinition.1") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", shipClass); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (graphics.length() > 13) {
      String msg = Language.getString("ShipDefinition.3") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", graphics); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (shipRole < 0 || shipRole > 9) {
      String msg = Language.getString("ShipDefinition.4") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(shipRole)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (nameGroup < 0 || nameGroup >= 248) {
      String msg = Language.getString("ShipDefinition.5") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(nameGroup)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (beamAccuracy < 0) {
      String msg = Language.getString("ShipDefinition.6") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(beamAccuracy)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (beamNumber < 0) {
      String msg = Language.getString("ShipDefinition.7") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(beamNumber)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (beamDamage < 0) {
      String msg = Language.getString("ShipDefinition.8") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(beamDamage)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (torpedoAccuracy < 0) {
      String msg = Language.getString("ShipDefinition.9") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(torpedoAccuracy)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (torpedoNumber < 0) {
      String msg = Language.getString("ShipDefinition.10") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(torpedoNumber)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (torpedoDamage < 0) {
      String msg = Language.getString("ShipDefinition.11") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(torpedoDamage)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (shieldStrength < 0) {
      String msg = Language.getString("ShipDefinition.12") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(shieldStrength)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (shieldRecharge < 0) {
      String msg = Language.getString("ShipDefinition.13") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(shieldRecharge)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_WARNING, msg));
    }
    if (hullStrength < 0) {
      String msg = Language.getString("ShipDefinition.14") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(hullStrength)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (defense < 0) {
      String msg = Language.getString("ShipDefinition.15") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(defense)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (stealth < 0 || stealth > 6) {
      String msg = Language.getString("ShipDefinition.16") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(stealth)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (mapRange < -1 || mapRange > 3) {
      String msg = Language.getString("ShipDefinition.17") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Double.toString(mapRange)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (mapSpeed < -1) {
      String msg = Language.getString("ShipDefinition.18") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Double.toString(mapSpeed)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (scanRange < 0) {
      String msg = Language.getString("ShipDefinition.19") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(scanRange)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (production < 0 || production > 65535) {
      String msg = Language.getString("ShipDefinition.20") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(production)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (race < 0 || race > 36) {
      String msg = Language.getString("ShipDefinition.21") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(race)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (buildCost < 0 || buildCost > 65535) {
      String msg = Language.getString("ShipDefinition.22") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(buildCost)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (maintenance < 0 || maintenance > 65535) {
      String msg = Language.getString("ShipDefinition.23") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Integer.toString(maintenance)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (minTorpedoRange < 0 || maxTorpedoRange < 0) {
      String msg = Language.getString("ShipDefinition.24") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", "(" + minTorpedoRange + "," + maxTorpedoRange
          + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (firingArc < 0 || firingArc > 360.0) {
      String msg = Language.getString("ShipDefinition.25") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", "(" + firingArc + "," + maxBeamRange
          + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (maxBeamRange < 0) {
      String msg = Language.getString("ShipDefinition.26") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", "(" + firingArc + "," + maxBeamRange
          + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }
    if (agility < 0) {
      String msg = Language.getString("ShipDefinition.27") //$NON-NLS-1$
        .replace("%1", descriptor) //$NON-NLS-1$
        .replace("%2", Double.toString(agility)); //$NON-NLS-1$
      response.add(file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg));
    }

    // graphics
    if (!graphics.toLowerCase().equals("t02")) { // $NON-NLS-1$
      String str = graphics + "_a.hob"; //$NON-NLS-1$
      str = str.toLowerCase();
      Stbof stbof = ((ShipList)this.file).stbof();

      if (!stbof.hasInternalFile(str)) {
        response.add(getFileNotFoundString(str));
      }

      str = graphics + "_b.hob"; //$NON-NLS-1$
      str = str.toLowerCase();
      if (!stbof.hasInternalFile(str)) {
        response.add(getFileNotFoundString(str));
      }
      str = graphics + "_c.hob"; //$NON-NLS-1$
      str = str.toLowerCase();
      if (!stbof.hasInternalFile(str)) {
        response.add(getFileNotFoundString(str));
      }
    }
  }

  private String getFileNotFoundString(String fileName) {
    String msg = Language.getString("ShipDefinition.28") //$NON-NLS-1$
      .replace("%1", fileName); //$NON-NLS-1$
    return file.getCheckIntegrityString(InternalFile.INTEGRITY_CHECK_ERROR, msg);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof ShipDefinition)) {
      return false;
    }
    ShipDefinition gu = (ShipDefinition) o;
    return hashCode() == gu.hashCode();
  }

  /**
   * Returns the file's hashcode.
   */
  @Override
  public int hashCode() {
    return ((race * 6000) + index);
  }

  @Override
  public int compareTo(ShipDefinition obj) {
    return (hashCode() - obj.hashCode());
  }

  /* Creates a copy of the provided ship */
  public ShipDefinition(ShipDefinition ship) {
    super(ship);
    shipList = ship.shipList;

    name = ship.name;
    graphics = ship.graphics;
    shipClass = ship.shipClass;
    index = -1;
    shipRole = ship.shipRole;
    nameGroup = ship.nameGroup;
    beamAccuracy = ship.beamAccuracy;
    beamNumber = ship.beamNumber;
    beamMultiplier = ship.beamMultiplier;
    beamDamage = ship.beamDamage;
    beamPenetration = ship.beamPenetration;
    torpedoAccuracy = ship.torpedoAccuracy;
    torpedoNumber = ship.torpedoNumber;
    torpedoMultiplier = ship.torpedoMultiplier;
    torpedoDamage = ship.torpedoDamage;
    shieldLevel = ship.shieldLevel;
    shieldStrength = ship.shieldStrength;
    shieldRecharge = ship.shieldRecharge;
    offensiveStrength = ship.offensiveStrength;
    defensiveStrength = ship.defensiveStrength;
    unknown_1 = ship.unknown_1;
    hullStrength = ship.hullStrength;
    defense = ship.defense;
    stealth = ship.stealth;
    superRayNumber = ship.superRayNumber;
    superRayAccuracy = ship.superRayAccuracy;
    superRayMultiplier = ship.superRayMultiplier;
    superRayDamage = ship.superRayDamage;
    superRayPenetration = ship.superRayPenetration;
    unknown_2 = ship.unknown_2;
    unknown_3 = ship.unknown_3;
    agility = ship.agility;
    firingArc = ship.firingArc;
    maxBeamRange = ship.maxBeamRange;
    minTorpedoRange = ship.minTorpedoRange;
    maxTorpedoRange = ship.maxTorpedoRange;
    maxSuperRayRange = ship.maxSuperRayRange;
    mapRange = ship.mapRange;
    mapSpeed = ship.mapSpeed;
    scanRange = ship.scanRange;
    production = ship.production;
    race = ship.race;
    phaserPrefix = ship.phaserPrefix;
    torpedoPrefix = ship.torpedoPrefix;
    buildCost = ship.buildCost;
    maintenance = ship.maintenance;
    race_override = ship.race_override;
  }

}
