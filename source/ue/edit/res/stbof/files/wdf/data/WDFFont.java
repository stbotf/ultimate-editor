package ue.edit.res.stbof.files.wdf.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import ue.edit.res.stbof.files.wdf.widgets.WDFWidget;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * @author Alan
 */
public class WDFFont {

  protected int[] rgb = new int[3];     // 3 bytes
  protected int[] unknown = new int[7]; // 7 * integer, 28 bytes
  protected boolean overrideExeDefaults;
  protected String fontFile;

  public WDFFont() {
    fontFile = WDFWidget.NO_IMAGE;
  }

  public WDFFont(InputStream in) throws IOException {
    for (int i = 0; i < 3; i++) {
      rgb[i] = in.read() & 0xFF;
    }

    for (int i = 0; i < unknown.length; i++) {
      unknown[i] = StreamTools.readInt(in, true);
    }

    overrideExeDefaults = StreamTools.readInt(in, true) != 0;
    fontFile = StreamTools.readNullTerminatedBotfString(in, 13);
    fontFile = fontFile.trim().toLowerCase();
  }

  public WDFFont(WDFFont font) {
    setValues(font);
  }

  public void setValues(WDFFont font) {
    System.arraycopy(font.rgb, 0, rgb, 0, 3);
    System.arraycopy(font.unknown, 0, unknown, 0, 7);

    overrideExeDefaults = font.overrideExeDefaults;
    fontFile = font.fontFile;
  }

  public void save(OutputStream out) throws IOException {
    for (int i = 0; i < 3; i++) {
      out.write(rgb[i]);
    }

    for (int i = 0; i < unknown.length; i++) {
      out.write(DataTools.toByte(unknown[i], true));
    }

    out.write(DataTools.toByte((overrideExeDefaults) ? 1 : 0, true));

    out.write(DataTools.toByte(fontFile, 13, 12));  // 48
  }

  @Override
  public String toString() {
    return "Font: " + fontFile;
  }

  public String getFontFile() {
    return fontFile;
  }

  public void setFontFile(String fontFile) {
    this.fontFile = fontFile;
  }

  public int getUnknown(int index) {
    return unknown[index];
  }

  public void setUnknown(int index, int value) {
    this.unknown[index] = value;
  }

  public int getRGBColor() {
    return (rgb[0] << 16) | (rgb[2] << 8) | (rgb[1]);
  }

  public void setRGBColor(int rgb) {
    this.rgb[0] = (rgb >>> 16) & 0xFF;
    this.rgb[2] = (rgb >>> 8) & 0xFF;
    this.rgb[1] = rgb & 0xFF;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final WDFFont other = (WDFFont) obj;
    if (!Arrays.equals(this.rgb, other.rgb)) {
      return false;
    }
    if (!Arrays.equals(this.unknown, other.unknown)) {
      return false;
    }
    if (this.overrideExeDefaults != other.overrideExeDefaults) {
      return false;
    }
    if ((this.fontFile == null) ? (other.fontFile != null)
        : !this.fontFile.equals(other.fontFile)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 79 * hash + Arrays.hashCode(this.rgb);
    hash = 79 * hash + Arrays.hashCode(this.unknown);
    hash = 79 * hash + (this.fontFile != null ? this.fontFile.hashCode() : 0);
    return hash;
  }

  public boolean overrideExeDefaults() {
    return overrideExeDefaults;
  }

  public boolean setOverrideExeDefaults(boolean overrideExeDefaults) {
    if (this.overrideExeDefaults != overrideExeDefaults) {
      this.overrideExeDefaults = overrideExeDefaults;
      return true;
    }
    return false;
  }
}
