package ue.edit.res.stbof.files.wdf;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import ue.edit.common.InternalFile;
import ue.edit.res.stbof.files.wdf.widgets.WDFWindow;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 *
 */
public class WindowDefinitionFile extends InternalFile {

  private int type;
  protected WDFWindow window;

  public WindowDefinitionFile() {
  }

  public WindowDefinitionFile(String name, InputStream in) throws IOException {
    this.NAME = name;
    load(in);
  }

  public WindowDefinitionFile(String name, byte[] data) throws IOException {
    this(name, new ByteArrayInputStream(data));
  }

  @Override
  public void load(InputStream in) throws IOException {
    type = StreamTools.readInt(in, true);
    StreamTools.skip(in, 4); // widgetType Integer
    window = new WDFWindow(this, in);
    markSaved();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(type, true));

    window.save(out);
  }

  @Override
  public void clear() {
    type = 0;
    window = null;
    markChanged();
  }

  @Override
  public void check(Vector<String> response) {
    // TODO: add integrity checking
  }

  @Override
  public int getSize() {
    return 4 + window.getSize();
  }

  public WDFWindow getWindow() {
    return window;
  }
}
