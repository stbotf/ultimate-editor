package ue.edit.res.stbof.files.wdf.widgets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;
import lombok.val;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class WDFWindow extends WDFWidget {

  // widgets grouped by type
  // grouped in strict order to satisfy the file format (@see save)
  protected TreeMap<Integer, ArrayList<WDFWidget>> typeMap = new TreeMap<>();
  protected HashMap<Integer, WDFWidget> widgets = new HashMap<>();

  public WDFWindow(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.WINDOW, in);

    // Read the entry count to skip excessive data.
    // Memory overlappings like in c_tfship.wdf, where the 13 letter tga name is being cut,
    // clearly show it is memory leftovers, that need to be skipped.
    int sum = StreamTools.readInt(in, true);

    // double check the entry count by numbers per type
    // indexed by WDFWidgetType, starting with WDFWidgetType.WINDOW
    int sumByType = 0;
    for (int i = 0; i < 15; ++i) {
      sumByType += StreamTools.readInt(in, true);
    }

    if (sum != sumByType) {
      String msg = "WDF %1 window %2 records %3 widgets, but by the type counters it is %4 widgets in total!"
        .replace("%1", wdf.getName()) //$NON-NLS-1$
        .replace("%2", Integer.toString(id)) //$NON-NLS-1$
        .replace("%3", Integer.toString(sum)) //$NON-NLS-1$
        .replace("%4", Integer.toString(sumByType)); //$NON-NLS-1$
      System.out.println(msg);
      sum = Integer.max(sum, sumByType);
    }

    // load window widgets
    for (; sum > 0 && in.available() > 0; sum--) {
      int widgetType = StreamTools.readInt(in, true);

      WDFWidget widget;
      switch (widgetType) {
        case WDFWidgetType.WINDOW:
          widget = new WDFWindow(wdf, in);
          break;
        case WDFWidgetType.BUTTON:
          widget = new WDFButton(wdf, in);
          break;
        case WDFWidgetType.MENU:
          widget = new WDFMenu(wdf, in);
          break;
        case WDFWidgetType.SLIDER:
          widget = new WDFSlider(wdf, in);
          break;
        case WDFWidgetType.SCROLLBAR:
          widget = new WDFScrollBar(wdf, in);
          break;
        case WDFWidgetType.LISTBOX:
          widget = new WDFListBox(wdf, in);
          break;
        case WDFWidgetType.PICTUREBOX:
          widget = new WDFPicture(wdf, in);
          break;
        case WDFWidgetType.TEXTBOX:
          widget = new WDFTextBox(wdf, in);
          break;
        default:
          throw new IOException("Unknown widget type encountered: %1"
              .replace("%1", Integer.toString(widgetType)));
      }

      typeMap.computeIfAbsent(widgetType, key -> new ArrayList<>()).add(widget);
      widgets.put(widget.id(), widget);
    }
  }

  public void addWidget(WDFWidget widget, int index) {
    typeMap.computeIfAbsent(widget.type(), key -> new ArrayList<>()).add(index, widget);
    widgets.put(widget.id(), widget);
    wdf.markChanged();
  }

  public void removeWidget(WDFWidget widget) {
    val list = typeMap.get(widget.type());
    if (list != null && list.remove(widget)) {
      widgets.remove(widget.id());
      wdf.markChanged();
    }
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    // numbers total & by type
    int[] sum = new int[16];
    typeMap.forEach((k, v) -> {
      sum[k] = v.size();
      sum[0] += sum[k];
    });

    for (int i = 0; i < sum.length; i++) {
      out.write(DataTools.toByte(sum[i], true));
    }

    // the widgets must be grouped in strict order,
    // matching the number table written above
    // otherwise the game crashes on load
    for (ArrayList<WDFWidget> widgetList : typeMap.values()) {
      for (WDFWidget widget : widgetList) {
        widget.save(out);
      }
    }
  }

  @Override
  public int getSize() {
    int size = super.getSize() + 64;

    for (ArrayList<WDFWidget> widgetList : typeMap.values())
      for (WDFWidget widget : widgetList)
        size += widget.getSize();

    return size;
  }

  @Override
  public String toString() {
    String str = "Window: Size: " + getSize() + " Widgets: ";

    for (ArrayList<WDFWidget> widgetList : typeMap.values()) {
      for (WDFWidget widget : widgetList) {
        str = str + "\n" + widget;
      }
    }

    return str;
  }

  public WDFWidget getWidget(int id) {
    return widgets.get(id);
  }

  public List<WDFWidget> getWidgets(int type) {
    return Optional.ofNullable(typeMap.get(type))
      .map(Collections::unmodifiableList)
      .orElse(new ArrayList<WDFWidget>());
  }

}
