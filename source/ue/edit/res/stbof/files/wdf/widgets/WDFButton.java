package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class WDFButton extends WDFWidget {

  public static final int TYPE_PUSH = 0;
  public static final int TYPE_TOGGLE = 2;

  protected int unknown;
  protected int buttonType;         // single byte
  protected boolean repeating;      // repeating action
  protected int repeatsPerSecond;   // either RepeatInterval/RepeatsPerSecond/RepeatIncrement
  protected String normalImage;
  protected String selectedImage;

  public WDFButton(WindowDefinitionFile wdf) {
    this(wdf, new Point(), new Dimension(110,25));
  }

  public WDFButton(WindowDefinitionFile wdf, Point position, Dimension dimension) {
    super(wdf, WDFWidgetType.BUTTON, position, dimension);
    normalImage = WDFWidget.NO_IMAGE;
    selectedImage = WDFWidget.NO_IMAGE;
  }

  public WDFButton(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.BUTTON, in);

    unknown = StreamTools.readInt(in, true);
    buttonType = in.read();
    repeating = StreamTools.readInt(in, true) != 0;
    repeatsPerSecond = StreamTools.readInt(in, true);
    normalImage = StreamTools.readNullTerminatedBotfString(in, 13);
    normalImage = normalImage.trim().toLowerCase();
    selectedImage = StreamTools.readNullTerminatedBotfString(in, 13);
    selectedImage = selectedImage.trim().toLowerCase();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    out.write(DataTools.toByte(unknown, true));           // 4
    out.write(buttonType);                                // 5
    out.write((repeating) ? 1 : 0);
    out.write(0);
    out.write(0);
    out.write(0);                                         // 9
    out.write(DataTools.toByte(repeatsPerSecond, true));  // 13
    out.write(DataTools.toByte(normalImage, 13, 12));     // +13 = 26
    out.write(DataTools.toByte(selectedImage, 13, 12));   // +13 = 39
  }

  @Override
  public int getSize() {
    return super.getSize() + 39;
  }

  @Override
  public String toString() {
    return "Widget: Type: " + type + " Size: " + getSize() + " Files: "
        + this.backgroundImage + ", " + this.normalImage + ", " + this.selectedImage;
  }

  public boolean setButtonType(int buttonType) {
    if (this.buttonType != buttonType) {
      this.buttonType = buttonType;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setNormalImage(String normalImage) {
    if (!this.normalImage.equalsIgnoreCase(normalImage)) {
      this.normalImage = normalImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setRepeatingClicks(boolean repeat) {
    if (this.repeating = repeat) {
      this.repeating = repeat;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setRepeatsPerSecond(int repeatsPerSecond) {
    if (this.repeatsPerSecond != repeatsPerSecond) {
      this.repeatsPerSecond = repeatsPerSecond;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setSelectedImage(String selectedImage) {
    if (!this.selectedImage.equalsIgnoreCase(selectedImage)) {
      this.selectedImage = selectedImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setUnknown(int unknown) {
    if (this.unknown != unknown) {
      this.unknown = unknown;
      wdf.markChanged();
      return true;
    }
    return false;
  }

}
