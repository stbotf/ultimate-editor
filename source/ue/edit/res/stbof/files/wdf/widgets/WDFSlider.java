package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class WDFSlider extends WDFWidget {

  protected short unknown1;
  protected int maxValue;           // maximum value
  protected int units;              // number of selection stripes
  protected int width;              // slider dimension width
  protected int unknown2;

  protected String cellOffImage;
  protected String cellOnImage;
  protected String markerOffImage;
  protected String markerOnImage;

  protected int maxValue2;          // max value copy
  protected int units2;             // units copy
  protected int width2;             // width copy

  public WDFSlider(WindowDefinitionFile wdf) {
    this(wdf, new Point(), new Dimension(250, 30), 10, 100);
  }

  public WDFSlider(WindowDefinitionFile wdf, Point position, Dimension dimension, int maxValue, int units) {
    super(wdf, WDFWidgetType.SLIDER, position, dimension);
    this.maxValue = maxValue;
    this.maxValue2 = maxValue;
    this.units = units;
    this.units2 = units;
    this.width = dimension.width;
    this.width2 = dimension.width;
    this.cellOffImage = WDFWidget.NO_IMAGE;
    this.cellOnImage = WDFWidget.NO_IMAGE;
    this.markerOffImage = WDFWidget.NO_IMAGE;
    this.markerOnImage = WDFWidget.NO_IMAGE;
  }

  public WDFSlider(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.SLIDER, in);

    unknown1 = StreamTools.readShort(in, true);
    maxValue = StreamTools.readInt(in, true);
    units = StreamTools.readInt(in, true);
    width = StreamTools.readInt(in, true);
    unknown2 = StreamTools.readInt(in, true);

    cellOffImage = StreamTools.readNullTerminatedBotfString(in, 13);
    cellOffImage = cellOffImage.trim().toLowerCase();
    cellOnImage = StreamTools.readNullTerminatedBotfString(in, 13);
    cellOnImage = cellOnImage.trim().toLowerCase();
    markerOffImage = StreamTools.readNullTerminatedBotfString(in, 13);
    markerOffImage = markerOffImage.trim().toLowerCase();
    markerOnImage = StreamTools.readNullTerminatedBotfString(in, 13);
    markerOnImage = markerOnImage.trim().toLowerCase();

    maxValue2 = StreamTools.readInt(in, true);
    units2 = StreamTools.readInt(in, true);
    width2 = StreamTools.readInt(in, true);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    out.write(DataTools.toByte(unknown1, true));          // 2
    out.write(DataTools.toByte(maxValue, true));
    out.write(DataTools.toByte(units, true));
    out.write(DataTools.toByte(width, true));
    out.write(DataTools.toByte(unknown2, true));          // +4*4 = 18

    out.write(DataTools.toByte(cellOffImage, 13, 12));
    out.write(DataTools.toByte(cellOnImage, 13, 12));     // +26 = 44
    out.write(DataTools.toByte(markerOffImage, 13, 12));
    out.write(DataTools.toByte(markerOnImage, 13, 12));   // +26 = 70

    out.write(DataTools.toByte(maxValue2, true));
    out.write(DataTools.toByte(units2, true));
    out.write(DataTools.toByte(width2, true));            // +3*4 = 82
  }

  @Override
  public int getSize() {
    return super.getSize() + 82;
  }

  @Override
  public String toString() {
    return "Widget: Type: " + type + " Size: " + getSize() + " Files: "
        + backgroundImage + ", " + cellOffImage + ", " + cellOnImage + ", " + markerOffImage
        + ", " + markerOnImage;
  }

  public boolean setCellOffImage(String cellOffImage) {
    if (!this.cellOffImage.equals(cellOffImage)) {
      this.cellOffImage = cellOffImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setCellOnImage(String cellOnImage) {
    if (!this.cellOnImage.equals(cellOnImage)) {
      this.cellOnImage = cellOnImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMarkerOffImage(String markerOffImage) {
    if (!this.markerOffImage.equals(markerOffImage)) {
      this.markerOffImage = markerOffImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMarkerOnImage(String markerOnImage) {
    if (!this.markerOnImage.equals(markerOnImage)) {
      this.markerOnImage = markerOnImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setUnknown1(short value) {
    if (unknown1 != value) {
      unknown1 = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMaxValue(int value) {
    if (maxValue != value) {
      maxValue = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setUnits(int value) {
    if (units != value) {
      units = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setWidth(int value) {
    if (width != value) {
      width = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setUnknown2(int value) {
    if (unknown2 != value) {
      unknown2 = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMaxValue2(int value) {
    if (maxValue2 != value) {
      maxValue2 = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setUnits2(int value) {
    if (units2 != value) {
      units2 = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setWidth2(int value) {
    if (width2 != value) {
      width2 = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

}
