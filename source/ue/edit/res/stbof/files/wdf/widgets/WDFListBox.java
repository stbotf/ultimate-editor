package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.AccessLevel;
import lombok.Getter;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.edit.res.stbof.files.wdf.data.WDFFont;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class WDFListBox extends WDFWidget {

  protected int unknown1;
  protected WDFFont font1;
  protected WDFFont font2;
  @Getter(AccessLevel.NONE)
  protected int[] columnWidth = new int[10];
  protected int columns;

  public WDFListBox(WindowDefinitionFile wdf) {
    this(wdf, new Point(), new Dimension(180, 200), 1);
  }

  public WDFListBox(WindowDefinitionFile wdf, Point position, Dimension dimension, int columns) {
    super(wdf, WDFWidgetType.LISTBOX, position, dimension);
    if (columns < 1 || columns > 10)
      throw new IllegalArgumentException();

    this.columns = columns;
    for (int i = 0; i < columns; ++i) {
      columnWidth[i] = ((i+1) * dimension.width) / columns;
    }

    font1 = new WDFFont();
    font2 = new WDFFont();
  }

  public WDFListBox(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.LISTBOX, in);

    unknown1 = StreamTools.readInt(in, true);
    font1 = new WDFFont(in);
    font2 = new WDFFont(in);

    for (int i = 0; i < columnWidth.length; i++) {
      columnWidth[i] = StreamTools.readInt(in, true);
    }

    columns = StreamTools.readInt(in, true);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    out.write(DataTools.toByte(unknown1, true));          // 4
    font1.save(out);                                      // +48 = 52
    font2.save(out);                                      // +48 = 100

    for (int i = 0; i < columnWidth.length; i++) {        // +10*4 = 140
      out.write(DataTools.toByte(columnWidth[i], true));
    }

    out.write(DataTools.toByte(columns, true));           // +4 = 144
  }

  @Override
  public int getSize() {
    return super.getSize() + 144;
  }

  @Override
  public String toString() {
    return "Widget: Type: " + type + " Size: " + getSize() + " Files: "
        + this.backgroundImage + ", " + font1 + ", " + font2;
  }

  public boolean setFont1(WDFFont font1) {
    if (!this.font1.equals(font1)) {
      this.font1.setValues(font1);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setFont2(WDFFont font2) {
    if (!this.font2.equals(font2)) {
      this.font2.setValues(font2);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public int getColumnWidth(int index) {
    return columnWidth[index];
  }

  public boolean setUnknown1(int value) {
    if (unknown1 != value) {
      unknown1 = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setColumnWidth(int index, int value) {
    if (columnWidth[index] != value) {
      columnWidth[index] = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setColumns(int value) {
    if (columns != value) {
      columns = value;
      wdf.markChanged();
      return true;
    }
    return false;
  }

}
