package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.edit.res.stbof.files.wdf.data.WDFFont;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class WDFTextBox extends WDFWidget {

  protected Point textPosition;
  protected Dimension textDimension;
  protected WDFFont font;

  public WDFTextBox(WindowDefinitionFile wdf) {
    this(wdf, new Point(), new Dimension(220, 60), new WDFFont());
  }

  public WDFTextBox(WindowDefinitionFile wdf, Point position, Dimension dimension, WDFFont font) {
    super(wdf, WDFWidgetType.TEXTBOX, new Point(position), new Dimension(dimension));
    this.textPosition = position;
    this.textDimension = dimension;
    this.font = font;
  }

  public WDFTextBox(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.TEXTBOX, in);

    int xw = StreamTools.readInt(in, true);
    int yh = StreamTools.readInt(in, true);
    textPosition = new Point(xw, yh);

    xw = StreamTools.readInt(in, true);
    yh = StreamTools.readInt(in, true);
    textDimension = new Dimension(xw, yh);

    font = new WDFFont(in);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    out.write(DataTools.toByte(textPosition.x, true)); // 4
    out.write(DataTools.toByte(textPosition.y, true)); // 8
    out.write(DataTools.toByte(textDimension.width, true)); // 12
    out.write(DataTools.toByte(textDimension.height, true)); // 16
    font.save(out); // +48 = 64
  }

  @Override
  public int getSize() {
    return super.getSize() + 64;
  }

  @Override
  public String toString() {
    return "Widget: Type: " + type + " Size: " + getSize() + " Files: "
        + this.backgroundImage + ", " + font;
  }

  public boolean setFont(WDFFont font) {
    if (!this.font.equals(font)) {
      this.font.setValues(font);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setTextDimension(Dimension textDimension) {
    if (!this.textDimension.equals(textDimension)) {
      this.textDimension = new Dimension(textDimension);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setTextPosition(Point textPosition) {
    if (!this.textPosition.equals(textPosition)) {
      this.textPosition = new Point(textPosition);
      wdf.markChanged();
      return true;
    }
    return false;
  }

}
