package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class WDFScrollBar extends WDFWidget {

  protected int unknown;
  protected String handleOnImage;
  protected String handleOffImage;
  protected int minValue;
  protected int maxValue;

  public WDFScrollBar(WindowDefinitionFile wdf) {
    this(wdf, new Point(), new Dimension(20, 240), 0, 100);
  }

  public WDFScrollBar(WindowDefinitionFile wdf, Point position, Dimension dimension, int min, int max) {
    super(wdf, WDFWidgetType.SCROLLBAR, position, dimension);
    this.handleOnImage = WDFWidget.NO_IMAGE;
    this.handleOffImage = WDFWidget.NO_IMAGE;
    this.minValue = min;
    this.maxValue = max;
  }

  public WDFScrollBar(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.SCROLLBAR, in);
    unknown = StreamTools.readInt(in, true);

    handleOnImage = StreamTools.readNullTerminatedBotfString(in, 13);
    handleOnImage = handleOnImage.trim().toLowerCase();
    handleOffImage = StreamTools.readNullTerminatedBotfString(in, 13);
    handleOffImage = handleOffImage.trim().toLowerCase();

    minValue = StreamTools.readInt(in, true);
    maxValue = StreamTools.readInt(in, true);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    out.write(DataTools.toByte(unknown, true)); // 4
    out.write(DataTools.toByte(handleOnImage, 13, 12));
    out.write(DataTools.toByte(handleOffImage, 13, 12));  // +26 = 30
    out.write(DataTools.toByte(minValue, true));
    out.write(DataTools.toByte(maxValue, true));    // +8 = 38
  }

  @Override
  public int getSize() {
    return super.getSize() + 38;
  }

  @Override
  public String toString() {
    return "Widget: Type: " + type + " Size: " + getSize() + " Files: "
        + this.backgroundImage + ", " + handleOffImage + ", " + handleOnImage;
  }

  public boolean setHandleOffImage(String handleOffImage) {
    if (!this.handleOffImage.equalsIgnoreCase(handleOffImage)) {
      this.handleOffImage = handleOffImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setHandleOnImage(String handleOnImage) {
    if (!this.handleOnImage.equalsIgnoreCase(handleOnImage)) {
      this.handleOnImage = handleOnImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMaxValue(int maxValue) {
    if (this.maxValue == maxValue) {
      this.maxValue = maxValue;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMinValue(int minValue) {
    if (this.minValue == minValue) {
      this.minValue = minValue;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setUnknown(int unknown) {
    if (this.unknown == unknown) {
      this.unknown = unknown;
      wdf.markChanged();
      return true;
    }
    return false;
  }

}
