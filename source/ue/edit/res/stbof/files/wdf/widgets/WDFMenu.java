package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.edit.res.stbof.files.wdf.data.WDFFont;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

@Getter
public class WDFMenu extends WDFWidget {

  protected Point menuPosition;
  protected Dimension menuDimension;

  protected WDFFont font1;
  protected WDFFont font2;
  protected String menuFile;
  protected String leftImage;
  protected String topImage;
  protected String rightImage;
  protected String bottomImage;
  protected String unknownFile;

  public WDFMenu(WindowDefinitionFile wdf) {
    this(wdf, new Point(), new Dimension(170, 110));
  }

  public WDFMenu(WindowDefinitionFile wdf, Point position, Dimension dimension) {
    super(wdf, WDFWidgetType.MENU, new Point(position), new Dimension(dimension));
    this.menuPosition = position;
    this.menuDimension = dimension;
    this.font1 = new WDFFont();
    this.font2 = new WDFFont();
    this.leftImage = WDFWidget.NO_IMAGE;
    this.topImage = WDFWidget.NO_IMAGE;
    this.rightImage = WDFWidget.NO_IMAGE;
    this.bottomImage = WDFWidget.NO_IMAGE;
  }

  public WDFMenu(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.MENU, in);

    int xw = StreamTools.readInt(in, true);
    int yh = StreamTools.readInt(in, true);
    menuPosition = new Point(xw, yh);

    xw = StreamTools.readInt(in, true);
    yh = StreamTools.readInt(in, true);
    menuDimension = new Dimension(xw, yh);

    font1 = new WDFFont(in);
    font2 = new WDFFont(in);

    menuFile = StreamTools.readNullTerminatedBotfString(in, 13);
    menuFile = menuFile.trim().toLowerCase();
    leftImage = StreamTools.readNullTerminatedBotfString(in, 13);
    leftImage = leftImage.trim().toLowerCase();
    topImage = StreamTools.readNullTerminatedBotfString(in, 13);
    topImage = topImage.trim().toLowerCase();
    rightImage = StreamTools.readNullTerminatedBotfString(in, 13);
    rightImage = rightImage.trim().toLowerCase();
    bottomImage = StreamTools.readNullTerminatedBotfString(in, 13);
    bottomImage = bottomImage.trim().toLowerCase();
    unknownFile = StreamTools.readNullTerminatedBotfString(in, 13);
    unknownFile = unknownFile.trim().toLowerCase();
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    out.write(DataTools.toByte(menuPosition.x, true));        // 4
    out.write(DataTools.toByte(menuPosition.y, true));        // 4
    out.write(DataTools.toByte(menuDimension.width, true));   // 4
    out.write(DataTools.toByte(menuDimension.height, true));  // 4

    font1.save(out);                                    // +48 = 64
    font2.save(out);                                    // +48 = 112

    out.write(DataTools.toByte(menuFile, 13, 12));
    out.write(DataTools.toByte(leftImage, 13, 12));     // +26 = 138
    out.write(DataTools.toByte(topImage, 13, 12));
    out.write(DataTools.toByte(rightImage, 13, 12));    // +26 = 164
    out.write(DataTools.toByte(bottomImage, 13, 12));
    out.write(DataTools.toByte(unknownFile, 13, 12));   // + 26 = 190
  }

  @Override
  public int getSize() {
    return super.getSize() + 190;
  }

  @Override
  public String toString() {
    return "Widget: Type: " + type + " Size: " + getSize() + " Files: "
        + backgroundImage + ", " + font1 + ", " + font2 + ", " + menuFile
        + ", " + leftImage + ", " + topImage + ", " + rightImage + ", "
        + bottomImage + ", " + unknownFile;
  }

  public boolean setBottomImage(String bottomImage) {
    if (!this.bottomImage.equals(bottomImage)) {
      this.bottomImage = bottomImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setFont1(WDFFont font1) {
    if (!this.font1.equals(font1)) {
      this.font1.setValues(font1);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setFont2(WDFFont font2) {
    if (!this.font2.equals(font2)) {
      this.font2.setValues(font2);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setLeftImage(String leftImage) {
    if (!StringTools.equals(this.leftImage, leftImage)) {
      this.leftImage = leftImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMenuFile(String menuFile) {
    if (!StringTools.equals(this.menuFile, menuFile)) {
      this.menuFile = menuFile;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setRightImage(String rightImage) {
    if (!StringTools.equals(this.rightImage, rightImage)) {
      this.rightImage = rightImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setTopImage(String topImage) {
    if (!StringTools.equals(this.topImage, topImage)) {
      this.topImage = topImage;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMenuPosition(Point value) {
    if (!menuPosition.equals(value)) {
      this.menuPosition = new Point(value);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setMenuDimension(Dimension value) {
    if (!menuDimension.equals(value)) {
      this.menuDimension = new Dimension(value);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public void setUnknownFile(String unknownFile) {
    if (!StringTools.equals(this.unknownFile, unknownFile)) {
      this.unknownFile = unknownFile;
      wdf.markChanged();
    }
  }

}
