package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import lombok.experimental.Accessors;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public abstract class WDFWidget implements Comparable<WDFWidget> {

  public interface WDFWidgetType {
    public static final int WINDOW = 1;
    public static final int BUTTON = 2;
    public static final int MENU = 5;
    public static final int SLIDER = 7;
    public static final int SCROLLBAR = 8;
    public static final int LISTBOX = 12;
    public static final int PICTUREBOX = 13;
    public static final int TEXTBOX = 14;

    public static String toString(int type) {
      switch (type) {
        case WINDOW:
          return "Window";
        case BUTTON:
          return "Button";
        case MENU:
          return "Menu";
        case SLIDER:
          return "Slider";
        case SCROLLBAR:
          return "Scrollbar";
        case LISTBOX:
          return "Listbox";
        case PICTUREBOX:
          return "Picture placeholder";
        case TEXTBOX:
          return "Textbox";
        default:
          return "Invalid type";
      }
    }
  }

  public static final String NO_IMAGE = "none";

  @Accessors(fluent  = true)
  protected int id;                 // widget id
  @Accessors(fluent  = true)
  protected int type;               // widget type
  protected Point position;         // widget position
  protected Dimension dimension;    // widget dimensions
  protected String backgroundImage; // filename of background image, may be "none"

  @Accessors(fluent  = true)
  protected WindowDefinitionFile wdf;

  public WDFWidget(WindowDefinitionFile wdf, int typeID) {
    this(wdf, typeID, new Point(), new Dimension(100, 100));
  }

  public WDFWidget(WindowDefinitionFile wdf, int type, Point position, Dimension dimension) {
    this.wdf = wdf;
    this.type = type;
    this.position = position;
    this.dimension = dimension;
    this.backgroundImage = NO_IMAGE;
  }

  public WDFWidget(WindowDefinitionFile wdf, int typeID, InputStream in) throws IOException {
    this.wdf = wdf;
    this.type = typeID;

    // widget id
    id = StreamTools.readInt(in, true);

    // position
    int xw = StreamTools.readInt(in, true);
    int yh = StreamTools.readInt(in, true);
    position = new Point(xw, yh);

    // dimensions
    xw = StreamTools.readInt(in, true);
    yh = StreamTools.readInt(in, true);
    dimension = new Dimension(xw, yh);

    backgroundImage = StreamTools.readNullTerminatedBotfString(in, 13);
    backgroundImage = backgroundImage.trim().toLowerCase();
  }

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(type, true));
    out.write(DataTools.toByte(id, true));
    out.write(DataTools.toByte(position.x, true));
    out.write(DataTools.toByte(position.y, true));
    out.write(DataTools.toByte(dimension.width, true));
    out.write(DataTools.toByte(dimension.height, true));
    out.write(DataTools.toByte(backgroundImage, 13, 12));
  }

  @Override
  public int compareTo(WDFWidget o) {
    return (type - o.type) * 100000 + (id - o.id);
  }

  public int getSize() {
    return 37;
  }

  public void setBackgroundImage(String backgroundImage) {
    if (!this.backgroundImage.equalsIgnoreCase(backgroundImage)) {
      this.backgroundImage = backgroundImage;
      wdf.markChanged();
    }
  }

  public boolean setDimension(Dimension dimension) {
    if (!this.dimension.equals(dimension)) {
      this.dimension = new Dimension(dimension);
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setId(int widgetId) {
    if (id != widgetId) {
      id = widgetId;
      wdf.markChanged();
      return true;
    }
    return false;
  }

  public boolean setPosition(Point position) {
    if (!this.position.equals(position)) {
      this.position = new Point(position);
      wdf.markChanged();
      return true;
    }

    return false;
  }

}
