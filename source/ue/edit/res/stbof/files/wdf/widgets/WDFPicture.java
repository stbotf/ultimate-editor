package ue.edit.res.stbof.files.wdf.widgets;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import lombok.Getter;
import ue.edit.res.stbof.files.wdf.WindowDefinitionFile;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

@Getter
public class WDFPicture extends WDFWidget {

  protected int unknown1;

  public WDFPicture(WindowDefinitionFile wdf) {
    this(wdf, new Point(), new Dimension(150, 150));
  }

  public WDFPicture(WindowDefinitionFile wdf, Point position, Dimension dimension) {
    super(wdf, WDFWidgetType.PICTUREBOX, position, dimension);
  }

  public WDFPicture(WindowDefinitionFile wdf, InputStream in) throws IOException {
    super(wdf, WDFWidgetType.PICTUREBOX, in);
    unknown1 = StreamTools.readInt(in, true);
  }

  @Override
  public void save(OutputStream out) throws IOException {
    super.save(out);

    out.write(DataTools.toByte(unknown1, true)); // 4
  }

  @Override
  public int getSize() {
    return super.getSize() + 4;
  }

  @Override
  public String toString() {
    return "Widget: Type: " + type + " Size: " + getSize() + " Files: "
        + this.backgroundImage;
  }

  public boolean setUnknown1(int unknown1) {
    if (this.unknown1 != unknown1) {
      this.unknown1 = unknown1;
      wdf.markChanged();
      return true;
    }
    return false;
  }

}
