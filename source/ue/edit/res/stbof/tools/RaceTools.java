package ue.edit.res.stbof.tools;

import lombok.val;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.files.dic.idx.LexHelper;

/**
 * Contains race helper routines for stbof.res.
 */
public class RaceTools {

  // stbof.res files
  private StbofFileInterface stif;

  public RaceTools(Stbof stbof) {
    stif = stbof.files();
  }

  public String mapRace(int raceId) {
    // to not keep outdated file references, lookup files on demand
    val raceRst = stif.findRaceRst();

    // prefer the stbof.res race.rst name if available
    if (raceRst.isPresent())
      return raceRst.get().getName(raceId);

    // default to vanilla race index
    return LexHelper.mapRace(raceId);
  }
}
