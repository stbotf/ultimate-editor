package ue.edit.res.stbof.tools;

import java.io.IOException;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.common.CTrekSegments;
import ue.edit.exe.trek.seg.shp.SpecialShips;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.common.CStbof;
import ue.edit.res.stbof.common.CStbof.StructureBonus;
import ue.edit.res.stbof.files.bin.AIBldReq;
import ue.edit.res.stbof.files.bin.data.AIBldReqEntry;
import ue.edit.res.stbof.files.bst.Edifice;
import ue.edit.res.stbof.files.bst.data.Building;
import ue.edit.res.stbof.files.tec.RaceTech;
import ue.exception.InvalidArgumentsException;
import ue.service.Language;

/**
 * Contains methods for common edit operations on the stbof buildings.
 */
public class BuildingTools {

  private StbofFileInterface stfi;
  private Trek trek;

  public BuildingTools(Stbof stbof, Trek trek) {
    this.stfi = stbof.files();
    this.trek = trek;
  }

  public void addBuilding(final int selIdx, final int copyIdx, boolean increaseBldMask, boolean increaseRTBldMask)
    throws IOException, InvalidArgumentsException {
    Edifice edifice = stfi.edifice();
    RaceTech raceTech = stfi.raceTech();

    // add to Edifice buildings
    Building bld = edifice.add(selIdx, copyIdx, increaseBldMask);

    bld.setName(Language.getString("BuildingTools.1")); //$NON-NLS-1$
    bld.setPluralName(Language.getString("BuildingTools.2")); //$NON-NLS-1$

    // update description addresses
    for (int i = selIdx + 1; i < edifice.getNumberOfEntries(); i++) {
      Building bx = edifice.building(i);
      int oldAddr = bx.getDescAddress();

      // increase by one given the added description
      // has only the 0x00 termination character written
      bx.setDescAddress(oldAddr + 1);
    }

    // update race tech availability indices
    if (increaseRTBldMask)
      raceTech.resizeBldMask();
    raceTech.shiftBuildingIndices(selIdx, false);

    // add AI building requirements entry
    if (!bld.isMainBuilding() && bld.getBonusType() != StructureBonus.BuildsShips
        && bld.getBonusType() != StructureBonus.MartialLaw) {
      AIBldReq aiBldReq = stfi.aiBldReq();
      // just append new buildings to the end of the list
      AIBldReqEntry bldReq = aiBldReq.add(selIdx);
      AIBldReqEntry cpyReq = aiBldReq.getReq(copyIdx).orElse(null);
      if (cpyReq != null) {
        bldReq.setEnergyReq(cpyReq.getEnergyReq());
        bldReq.setMaxPopReq(cpyReq.getMaxPopReq());
        bldReq.setPopReq(cpyReq.getPopReq());
      }
    }

    // update starting building indices
    for (int k = 1; k <= CStbof.NUM_ERAS; k++) {
      stfi.bldSet(k).shiftBuidingIndices((short) selIdx);
    }

    // update special ship building index
    if (trek != null) {
      SpecialShips bub = (SpecialShips) trek.getInternalFile(CTrekSegments.SpecialShips, true);
      bub.shiftBuildingIndices(selIdx);
    }
  }

  public void removeBuilding(final int index) throws IOException, InvalidArgumentsException {
    AIBldReq aiBldReq = stfi.aiBldReq();
    Edifice edifice = stfi.edifice();
    RaceTech raceTech = stfi.raceTech();

    // remove from Edifice buildings
    edifice.remove(index);

    // update race tech availability indices
    raceTech.removeBuildingIndex(index);

    // remove AI build requirements
    aiBldReq.removedBuidingIndex(index);

    // update starting building indices
    for (int k = 1; k <= CStbof.NUM_ERAS; k++) {
      stfi.bldSet(k).removeBuidingIndex((short) index);
    }

    // update special ship building index
    if (trek != null) {
      SpecialShips bub = (SpecialShips) trek.getInternalFile(CTrekSegments.SpecialShips, true);
      bub.removeBuildingIndex(index);
    }
  }

  public void switchIndex(final int idx1, final int idx2) throws IOException, InvalidArgumentsException {
    if (idx1 == idx2)
      return;

    AIBldReq aiBldReq = stfi.aiBldReq();
    Edifice edifice = stfi.edifice();
    RaceTech raceTech = stfi.raceTech();

    // switch Edifice buildings
    edifice.switchIndex(idx1, idx2);

    // switch race tech availability indices
    raceTech.switchBuildingIndex(idx1, idx2);

    // switch AI building requirements
    aiBldReq.switchIDs(idx1, idx2);

    // switch starting building indices
    for (int k = 1; k <= CStbof.NUM_ERAS; k++) {
      stfi.bldSet(k).switchedBuidingIndex((short) idx1, (short) idx2);
    }

    // update special ship building index
    if (trek != null) {
      SpecialShips bub = (SpecialShips) trek.getInternalFile(CTrekSegments.SpecialShips, true);
      bub.switchedBuildingIndex(idx1, idx2);
    }
  }
}
