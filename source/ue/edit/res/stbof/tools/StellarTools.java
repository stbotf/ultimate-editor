package ue.edit.res.stbof.tools;

import java.io.IOException;
import java.util.ArrayList;
import lombok.val;
import ue.edit.exe.trek.Trek;
import ue.edit.exe.trek.sd.SD_StellarTypes;
import ue.edit.exe.trek.seg.map.StellTypeDetailsDescMap;
import ue.edit.exe.trek.seg.map.StellTypeDetailsNameMap;
import ue.edit.exe.trek.seg.map.StellTypeGen;
import ue.edit.exe.trek.seg.map.StellTypeTooltipNameMap;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.StbofFileInterface;
import ue.edit.res.stbof.files.dic.Lexicon;
import ue.edit.res.stbof.files.dic.idx.LexDataIdx;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.edit.res.stbof.files.smt.Objstruc;
import ue.util.data.ID;
import ue.util.data.StringTools;

/**
 * Contains stellar object helper routines for stbof.res.
 */
public class StellarTools {

  // stbof.res files
  private StbofFileInterface stif;
  // trek.exe segments
  private Trek trek = null;

  public StellarTools(Stbof stbof, Trek trek) {
    stif = stbof.files();
    this.trek = trek;
  }

  public String mapStellarType(int type) {
    return mapStellarType(type, null);
  }

  public String mapStellarType(int type, String graph) {
    val lex = stif.findLexicon();
    if (lex.isPresent()) {
      // to not keep outdated file references, lookup files on demand
      val tooltipNameMap = trek != null ? (StellTypeTooltipNameMap) trek.tryGetSegment(
        StellTypeTooltipNameMap.SEGMENT_DEFINITION, true) : null;
      val detailsNameMap = trek != null ? (StellTypeDetailsNameMap) trek.tryGetSegment(
        StellTypeDetailsNameMap.SEGMENT_DEFINITION, true) : null;

      // prefer the tooltip name if available
      if (tooltipNameMap != null) {
        int lexId = tooltipNameMap.getLexId(type);
        if (lexId >= 0)
          return lex.get().getEntry(lexId);
      }

      // alternatively lookup the details name
      if (detailsNameMap != null) {
        int lexId = detailsNameMap.getLexId(type);
        if (lexId >= 0)
          return lex.get().getEntry(lexId);
      }

      // fallback to map the graphics file
      if (graph != null) {
        String name = mapStellarGraph(lex.get(), graph);
        if (!StringTools.isNullOrEmpty(name))
          return name;
      }
    }

    // default to vanilla stellar type index
    return LexHelper.mapStellarType(type);
  }

  public String mapStellarDesc(int type) {
    val lex = stif.findLexicon();
    if (!lex.isPresent())
      return "???"; //$NON-NLS-1$

    // to not keep outdated file references, lookup files on demand
    val detailsDescMap = trek != null ? (StellTypeDetailsDescMap) trek.tryGetSegment(
      StellTypeDetailsDescMap.SEGMENT_DEFINITION, true) : null;

    if (detailsDescMap != null) {
      int lexId = detailsDescMap.getLexId(type);
      if (lexId >= 0)
        return lex.get().getEntry(lexId);
    }

    // default to vanilla stellar type index
    return LexHelper.mapStellarDesc(type);
  }

  private String mapStellarGraph(Lexicon lex, String graph) {
    switch (graph) {
      case "blkh":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.BlackHole);
      case "xp-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.XRayPulsar);
      case "neb-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.Nebula);
      case "ns":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.NeutronStar);
      case "s-r-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.RedGiant);
      case "s-o-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.OrangeStar);
      case "s-y-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.YellowStar);
      case "s-w-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.WhiteStar);
      case "s-g-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.GreenStar);
      case "s-b-":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.BlueStar);
      case "worm":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.Wormhole);
      case "rpls":
        return lex.getEntry(LexDataIdx.Galaxy.StellarObjects.RadioPulsar);
    }

    return null;
  }

  public ArrayList<ID> listStellarTypes() throws IOException {
    Objstruc stObj = stif.objStruc();
    StellTypeGen stGen = trek != null ? (StellTypeGen) trek.tryGetSegment(SD_StellarTypes.Gen, true) : null;

    // add stellar type menu selections
    ArrayList<ID> ids = new ArrayList<ID>();
    for (int i = 0; i < stObj.numberOfEntries(); ++i) {
      if (stGen == null || !stGen.isSystem(i))
        ids.add(new ID(mapStellarType(i), i));
    }

    return ids;
  }

  public ArrayList<ID> listSystemTypes() throws IOException {
    Objstruc stObj = stif.objStruc();
    StellTypeGen stGen = trek != null ? (StellTypeGen) trek.tryGetSegment(SD_StellarTypes.Gen, true) : null;

    // add system type menu selections
    ArrayList<ID> ids = new ArrayList<ID>();
    for (int i = 0; i < stObj.numberOfEntries(); ++i) {
      if (stGen == null || stGen.isSystem(i))
        ids.add(new ID(mapStellarType(i), i));
    }

    return ids;
  }
}
