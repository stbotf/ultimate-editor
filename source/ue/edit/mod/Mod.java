package ue.edit.mod;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This class is used to store info on a particualar mod. Including mod name, version, creator,
 * comment and a list of files modified by this mod.
 */
public class Mod implements Comparable<Mod> {

  private String NAME;
  private String VERSION;
  private String CREATOR;
  private String COMMENT;
  private int INDEX;                  // -1 - user modifications
  private int UNINSTALLER;            //  0 - not uninstaller, 1 - uninstaler
  private HashSet<String> FILES;

  /**
   * Creates an empty mod.
   */
  public Mod(String name) {
    this.NAME = name;
    this.VERSION = ""; //$NON-NLS-1$
    this.CREATOR = ""; //$NON-NLS-1$
    this.COMMENT = ""; //$NON-NLS-1$
    this.INDEX = -1;
    this.UNINSTALLER = 0;
    this.FILES = new HashSet<String>();
  }

  /**
   * Creates a mod with the given data.
   */
  public Mod(String name, String version, String creator, String comment, int index) {
    this.NAME = name;
    this.VERSION = version;
    this.CREATOR = creator;
    this.COMMENT = comment;
    this.INDEX = index;
    this.UNINSTALLER = 0;
    this.FILES = new HashSet<String>();
  }

  /**
   * Copy constructor.
   */
  public Mod(Mod sor) {
    this.NAME = sor.NAME;
    this.VERSION = sor.VERSION;
    this.CREATOR = sor.CREATOR;
    this.COMMENT = sor.COMMENT;
    this.INDEX = sor.INDEX;
    this.UNINSTALLER = sor.UNINSTALLER;
    this.FILES = new HashSet<String>(sor.FILES);
  }

  /**
   * Reads data from the InputStream.
   */
  public Mod(InputStream in) throws IOException {
    FILES = new HashSet<String>();

    // name
    int length = StreamTools.readInt(in, true);
    NAME = StreamTools.readNullTerminatedBotfString(in, length);

    // version
    length = StreamTools.readInt(in, true);
    VERSION = StreamTools.readNullTerminatedBotfString(in, length);

    // creator
    length = StreamTools.readInt(in, true);
    CREATOR = StreamTools.readNullTerminatedBotfString(in, length);

    // comment
    length = StreamTools.readInt(in, true);
    COMMENT = StreamTools.readNullTerminatedBotfString(in, length);

    // index
    INDEX = StreamTools.readInt(in, true);

    // uninstaller
    UNINSTALLER = StreamTools.readInt(in, true);

    // files
    int num = StreamTools.readInt(in, true);
    for (int i = 0; i < num; i++) {
      length = StreamTools.readInt(in, true);
      String fileName = StreamTools.readNullTerminatedBotfString(in, length);
      FILES.add(fileName);
    }
  }

  /**
   * Writes all data to the OutputStream.
   */
  public void save(OutputStream out) throws IOException {
    // name
    int length = NAME.length();
    out.write(DataTools.toByte(length, true));
    out.write(DataTools.toByte(NAME, length, length));

    // version
    length = VERSION.length();
    out.write(DataTools.toByte(length, true));
    out.write(DataTools.toByte(VERSION, length, length));

    // creator
    length = CREATOR.length();
    out.write(DataTools.toByte(length, true));
    out.write(DataTools.toByte(CREATOR, length, length));

    // comment
    length = COMMENT.length();
    out.write(DataTools.toByte(length, true));
    out.write(DataTools.toByte(COMMENT, length, length));

    // index
    out.write(DataTools.toByte(INDEX, true));

    // uninstaller
    out.write(DataTools.toByte(UNINSTALLER, true));

    // files
    length = FILES.size();
    out.write(DataTools.toByte(length, true));
    for (String fileName : FILES) {
      length = fileName.length();
      out.write(DataTools.toByte(length, true));
      out.write(DataTools.toByte(fileName, length, length));
    }
  }

  /**
   * Copies mod info only - but not the list of files.
   */
  public void copyInfo(Mod sor) {
    this.NAME = sor.NAME;
    this.VERSION = sor.VERSION;
    this.CREATOR = sor.CREATOR;
    this.COMMENT = sor.COMMENT;
    this.INDEX = sor.INDEX;
    this.UNINSTALLER = sor.UNINSTALLER;
  }

  /**
   * Returns true if this mod is an uninstaller
   */
  public boolean isUninstaller() {
    return UNINSTALLER != 0;
  }

  /**
   * Sets if this mod is an uninstaller
   */
  public void setUninstaller(boolean tr) {
    if (tr) {
      UNINSTALLER = 1;
    } else {
      UNINSTALLER = 0;
    }
  }

  /**
   * Checks if the provided file is listed in this mod.
   */
  public boolean containsFile(String str) {
    return FILES.contains(str);
  }

  /**
   * Adds a file to the list.
   */
  public boolean add(String filename) {
    return FILES.add(filename);
  }

  /**
   * Removes a file to the list.
   */
  public boolean remove(String filename) {
    return FILES.remove(filename);
  }

  /**
   * Returns an Iterator to go thru all files.
   */
  public Iterator<String> iterator() {
    return FILES.iterator();
  }

  /**
   * Returns an unmodifiable view on the files set.
   */
  public Set<String> list() {
    return Collections.unmodifiableSet(FILES);
  }

  /**
   * Returns if this mod is empty (has no files added).
   */
  public boolean isEmpty() {
    return FILES.isEmpty();
  }

  /**
   * Returns this mod's index.
   */
  public int getIndex() {
    return INDEX;
  }

  /**
   * Sets this mod's index.
   */
  public void setIndex(int ind) {
    this.INDEX = ind;
  }

  /**
   * Returns this mod's name.
   */
  public String getName() {
    return NAME;
  }

  /**
   * Sets this mod's name.
   */
  public void setName(String name) {
    this.NAME = name;
  }

  /**
   * Sets this mod's comment.
   */
  public void setComment(String com) {
    this.COMMENT = com;
  }

  /**
   * Sets this mod's version.
   */
  public void setVersion(String com) {
    this.VERSION = com;
  }

  /**
   * Sets this mod's creator.
   */
  public void setCreator(String com) {
    this.CREATOR = com;
  }

  /**
   * Returns this mod's version.
   */
  public String getVersion() {
    return VERSION;
  }

  /**
   * Returns this mod's creator.
   */
  public String getCreator() {
    return CREATOR;
  }

  /**
   * Returns this mod's comment.
   */
  public String getComment() {
    return COMMENT;
  }

  /**
   * Compares two mods by their index.
   */
  @Override
  public int compareTo(Mod m) {
    return (INDEX - m.INDEX);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Mod)) {
      return false;
    }
    Mod m = (Mod) o;
    return (m.NAME.equals(NAME) && m.VERSION.equals(VERSION) && m.CREATOR
        .equals(CREATOR));
  }

  @Override
  public int hashCode() {
    return (NAME + VERSION + CREATOR).hashCode();
  }
}