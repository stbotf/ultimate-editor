package ue.edit.mod;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.commons.lang3.RandomStringUtils;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * This used to be used for containing mod info, now it's used only to keep track of modified
 * files.
 */
public class ModList {

  private static final int VERSION = 1;

  // for identification, the stored mod name must be unique and never be translated
  public static final String UserChangesModName = "User Modifications"; //$NON-NLS-1$

  private String I = "THIS FILE HOLDS INFO ON INSTALLED MODS AND IS USED BY UE. DO NOT MODIFY!"; //$NON-NLS-1$
  private String ID;   // unique file identification
  private ArrayList<Mod> MODS = new ArrayList<Mod>();
  private Mod UserChanges = null;

  /**
   * Creates a mod list with the default empty mod
   */
  public ModList() {
    ID = RandomStringUtils.randomAlphanumeric(8);
  }

  /**
   * Constructs a ModList from the data read from the InputStream
   *
   * @param in
   * @throws IOException
   */
  public ModList(InputStream in) throws IOException {
    MODS = new ArrayList<Mod>();

    // Info/warning
    int length = StreamTools.readInt(in, true);
    I = StreamTools.readNullTerminatedBotfString(in, length);
    // id
    length = StreamTools.readInt(in, true);
    ID = StreamTools.readNullTerminatedBotfString(in, length);
    // version
    in.skip(4);

    // mods
    while (in.available() > 0) {
      Mod m = new Mod(in);
      MODS.add(m);
      if (UserChangesModName.equals(m.getName()))
        UserChanges = m;
    }
  }

  /**
   * Stores the mod list to the OutputStream
   *
   * @param out
   * @throws IOException
   */
  public synchronized void save(OutputStream out) throws IOException {
    // Info/warning
    int length = I.length();
    out.write(DataTools.toByte(length, true));
    out.write(DataTools.toByte(I, length, length));
    // id
    length = ID.length();
    out.write(DataTools.toByte(length, true));
    out.write(DataTools.toByte(ID, length, length));
    // version
    out.write(DataTools.toByte(ModList.VERSION, true));

    // mods
    for (Mod m : MODS) {
      m.save(out);
    }
  }

  public byte[] toByteArray() throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    save(baos);
    return baos.toByteArray();
  }

  /**
   * Returns a description of clashes of the mod Provided and mods in the list.
   */
  public String checkHits(Mod MOD) {
    String ret = ""; //$NON-NLS-1$
    Mod[] m = getAll();

    synchronized (this) {
      String str;
      Iterator<String> it = MOD.iterator();
      while (it.hasNext()) {
        str = it.next();
        for (Mod element : m) {
          if (element.containsFile(str)) {
            ret = ret + element.getName() + "  " + element.getVersion() + " (" + str
                + ")\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
          }
        }
      }
    }

    return ret;
  }

  /**
   * Returns true if a *valid and unedited* mod is installed.
   */
  public synchronized boolean hasMod() {
    int siz = MODS.size();
    if (siz != 0) {
      for (int i = 0; i < siz; i++) {
        Mod m = MODS.get(i);
        if ((m.getIndex() != -1) && !m.isEmpty())
          return true;
      }
    }

    return false;
  }

  /**
   * Returns a lIst of all *valid* mods installed including the user mod.
   * A mod is considered valid if it has file entries.
   */
  public synchronized Mod[] getAll() {
    ArrayList<Mod> ret = new ArrayList<Mod>();
    int siz = MODS.size();

    if (siz == 0)
      return new Mod[0];

    for (int i = 0; i < siz; i++) {
      Mod m = MODS.get(i);
      if (!m.isEmpty())
        ret.add(m);
    }

    return ret.toArray(new Mod[0]);
  }

  /**
   * Adds a file to the unnamed user mod.
   */
  public synchronized boolean add(String fileName) {
    fileName = fileName.toLowerCase();
    if (fileName.equals("mod.lst")) //$NON-NLS-1$
      return false;

    //remove from others
    for (int i = 0; i < MODS.size(); i++) {
      Mod m = MODS.get(i);
      m.remove(fileName);
    }

    if (UserChanges == null) {
      UserChanges = new Mod(UserChangesModName);
      MODS.add(UserChanges);
    }
    return UserChanges.add(fileName);
  }

  /**
   * Returns the index of the said mod.
   */
  public synchronized int indexOf(Mod m) {
    int i = this.MODS.indexOf(m);
    if (i == -1)
      return i;

    Mod mu = MODS.get(i);
    int f = mu.getIndex();
    return f;
  }

  /**
   * Adds a mod to the end of this mod list.
   */
  public synchronized boolean add(Mod mod) {
    //cleansing
    Iterator<String> it = mod.iterator();
    while (it.hasNext()) {
      String str = it.next();
      for (int j = 0; j < MODS.size(); j++) {
        Mod m = MODS.get(j);
        m.remove(str);
      }
    }

    Mod m = new Mod(mod);
    MODS.add(m);
    m.setIndex(MODS.size() - 1);

    return true;
  }

  /**
   * Removes a file from the mod list.
   */
  public synchronized void remove(String fileName) {
    fileName = fileName.toLowerCase();
    for (Mod m : MODS)
      m.remove(fileName);
  }

  /**
   * Removes a mod from the list.
   */
  public synchronized boolean remove(Mod mod) {
    return MODS.remove(mod);
  }

  /**
   * Returns if the mod is last on the list.
   */
  public synchronized boolean isLast(Mod m) {
    int i = MODS.indexOf(m);
    return MODS.size() == i + 1;
  }

  /**
   * Returns if all mods are empty (have no files added).
   */
  public synchronized boolean isEmpty() {
    int siz = MODS.size();
    for (int i = 0; i < siz; i++) {
      Mod m = MODS.get(i);
      if (!m.isEmpty())
        return false;
    }
    return true;
  }
}
