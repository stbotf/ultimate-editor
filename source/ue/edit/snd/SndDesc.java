package ue.edit.snd;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.data.StringTools;
import ue.util.stream.StreamTools;

public class SndDesc {

  private boolean CHANGED = false;
  private Hashtable<Integer, DescEntry> ENTRY = new Hashtable<Integer, DescEntry>();

  public SndDesc() {
  }

  public SndDesc(InputStream in) throws IOException {
    while (in.available() > 0) {
      int index = StreamTools.readInt(in, true);
      ENTRY.put(index, new DescEntry(in));
    }
  }

  public void save(OutputStream out) throws IOException {
    Enumeration<Integer> KEY = ENTRY.keys();
    int index;
    DescEntry desc;
    while (KEY.hasMoreElements()) {
      index = KEY.nextElement();
      desc = ENTRY.get(index);
      out.write(DataTools.toByte(index, true));
      desc.save(out);
    }
    CHANGED = false;
  }

  public byte[] toByteArray() throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    save(baos);
    return baos.toByteArray();
  }

  public void export(OutputStream out, int num, RaceRst race) throws IOException {
    for (int i = 0; i < num; i++) {
      try {
        String str = i + ":\t"; //$NON-NLS-1$
        int g = getRace(i);
        switch (g) {
          case 0: {
            str = str + Language.getString("SndDesc.0"); //$NON-NLS-1$
            break;
          }
          case 1: {
            str = str + Language.getString("SndDesc.1"); //$NON-NLS-1$
            break;
          }
          case 7: {
            str = str + Language.getString("SndDesc.12"); //$NON-NLS-1$
            break;
          }
          default: {
            str = str + race.getName(g - 2);
          }
        }
        str = str + ":\t\t"; //$NON-NLS-1$
        g = getType(i);
        switch (g) {
          case 1: {
            str = str + Language.getString("SndDesc.2"); //$NON-NLS-1$
            break;
          }
          case 2: {
            str = str + Language.getString("SndDesc.3"); //$NON-NLS-1$
            break;
          }
          case 3: {
            str = str + Language.getString("SndDesc.4"); //$NON-NLS-1$
            break;
          }
          case 4: {
            str = str + Language.getString("SndDesc.5"); //$NON-NLS-1$
            break;
          }
          case 5: {
            str = str + Language.getString("SndDesc.6"); //$NON-NLS-1$
            break;
          }
          case 6: {
            str = str + Language.getString("SndDesc.7"); //$NON-NLS-1$
            break;
          }
          case 7: {
            str = str + Language.getString("SndDesc.8"); //$NON-NLS-1$
            break;
          }
          case 8: {
            str = str + Language.getString("SndDesc.9"); //$NON-NLS-1$
            break;
          }
          default: {
            str = str + Language.getString("SndDesc.10"); //$NON-NLS-1$
            break;
          }
        }
        str = str + ":\t\t" + getDesc(i) + "\n"; //$NON-NLS-1$ //$NON-NLS-2$
        out.write(DataTools.toByte(str, str.length(), str.length()));
      } catch (Exception t) {
        t.printStackTrace();
      }
    }
    CHANGED = false;
  }

  public short getRace(int index) {
    DescEntry desc = ENTRY.get(index);
    return desc != null ? desc.RACE : 0;
  }

  public short getType(int index) {
    DescEntry desc = ENTRY.get(index);
    return desc != null ? desc.TYPE : 0;
  }

  public String getShortName(int index) {
    DescEntry desc = ENTRY.get(index);
    String name;

    if (desc != null && !StringTools.isNullOrEmpty(desc.DESC)) {
       name = desc.DESC.length() < 26 ? desc.DESC :
         desc.DESC.substring(0, 22) + "..."; //$NON-NLS-1$
    } else {
      name = defaultSoundName(index);
    }

    return name;
  }

  public String getDesc(int index) {
    DescEntry desc = ENTRY.get(index);
    return desc != null ? desc.DESC : null;
  }

  public void setRace(int index, short race) {
    DescEntry desc = ENTRY.get(index);
    if (desc == null) {
      desc = new DescEntry(index);
      ENTRY.put(index, desc);
    }
    else if (desc.RACE == race) {
      return;
    }
    desc.RACE = race;
    CHANGED = true;
  }

  public void setType(int index, short type) {
    DescEntry desc = ENTRY.get(index);
    if (desc == null) {
      desc = new DescEntry(index);
      ENTRY.put(index, desc);
    }
    else if (desc.TYPE == type) {
      return;
    }
    desc.TYPE = type;
    CHANGED = true;
  }

  public void setDesc(int index, String desci) {
    DescEntry desc = ENTRY.get(index);

    // empty descriptions should not create a new entry
    // so check for description changes first
    String txt = desc != null ? desc.DESC : null;
    if (StringTools.equals(txt, desci))
      return;

    if (desc == null) {
      desc = new DescEntry(index);
      ENTRY.put(index, desc);
    }
    desc.DESC = desci;
    CHANGED = true;
  }

  public boolean madeChanges() {
    return CHANGED;
  }

  private static String defaultSoundName(int index) {
    return Language.getString("SndDesc.11") //$NON-NLS-1$
      .replace("%1", Integer.toString(index)); //$NON-NLS-1$
  }

  private class DescEntry {

    /*RACE:
    0-unknown
    1-neutral
    */
    public short RACE;
    /*TYPE:
    0-unknown
    1-tactical battle
    2-diplomatic report
    3-cpu
    4-weapon fire
    5-ship propulsion
    6-ambient music
    */
    public short TYPE;
    public String DESC;

    public DescEntry(int index) {
      RACE = 0;
      TYPE = 0;
      DESC = defaultSoundName(index);
    }

    public DescEntry(InputStream in) throws IOException {
      RACE = StreamTools.readShort(in, true);
      TYPE = StreamTools.readShort(in, true);
      int len = StreamTools.readInt(in, true);
      DESC = StreamTools.readNullTerminatedBotfString(in, len); //FIXME: Should read/save as UTF!
    }

    public void save(OutputStream out) throws IOException {
      byte[] b = DataTools.toByte(RACE, true);
      out.write(b);
      b = DataTools.toByte(TYPE, true);
      out.write(b);
      b = DataTools.toByte(DESC.length(), true);
      out.write(b);
      b = DataTools.toByte(DESC, DESC.length(), DESC.length());
      out.write(b);
    }
  }
}
