package ue.edit.snd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import lombok.val;
import ue.UE;
import ue.audio.Wave;
import ue.audio.Wave.WaveType;
import ue.edit.mod.ModList;
import ue.exception.InvalidArgumentsException;
import ue.exception.UnsupportedWave;
import ue.gui.menu.MenuCommand;
import ue.gui.util.GuiTools;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.UEWorker;
import ue.util.data.DataTools;
import ue.util.data.Pair;
import ue.util.stream.StreamTools;

public class Music extends SndFile {

  public static final int TYPE = FileManager.P_MUSIC;

  // enforce default number of max 151 sounds
  public static final int DEFAULT_NUM_SOUNDS = 151;
  public static final int MAX_SOUNDS = DEFAULT_NUM_SOUNDS;

  private boolean changed = false;
  private ArrayList<SndEntry> MAP = new ArrayList<SndEntry>(MAX_SOUNDS);
  private ArrayList<Pair<Integer, Integer>> MINI_MAP = new ArrayList<Pair<Integer, Integer>>(16);
  private Hashtable<Integer, Wave> WAVE = new Hashtable<Integer, Wave>(MAX_SOUNDS);
  public boolean ADD_CHANGES_2_MODLIST = true;

  @Override
  public int type() {
    return TYPE;
  }

  @Override
  public String getName() {
    return MenuCommand.mapCommandName(MenuCommand.Music);
  }

  @Override
  public void markChanged() {
    changed = true;
  }

  public void updateModList() {
    Enumeration<Integer> keys = WAVE.keys();
    while (keys.hasMoreElements()) {
      MODS.add(keys.nextElement().toString());
    }
  }

  @Override
  public int getNumberOfSounds() {
    return MAP.size();
  }

  /**
   * Saves changes.
   *
   * @param dest File where everything should be saved to. If null save to the same file.
   * @return true if save has been successful.
   * @throws IOException
   **/
  @Override
  public boolean save(File dest, boolean bkp, String ext) throws IOException {
    // if not specified, save to the open file
    if (dest == null)
      dest = getTargetFile();

    val saveTask = new SaveTask(dest, bkp, ext);
    boolean success = GuiTools.runUEWorker(saveTask);

    if (success) {
      sourceFile = dest;
      changed = false;
    }

    return success;
  }

  /**
   * There you go, returns Wave.
   * @throws InvalidArgumentsException
   * @throws IOException
   * @throws UnsupportedWave
   */
  @Override
  public Wave getWave(int index) throws IOException {
    // check for cached modified wave files
    Wave cached = WAVE.get(index);
    if (cached != null)
      return cached;

    val snd = MAP.get(index);
    byte[] data = getSndEntryData(snd);

    // the music entries lack the wave header,
    // so create Wave with pure content data
    return new Wave(WaveType.English, data, snd.FRAMES);
  }

  /**
   * Returns the pointed sound entry data.
   * @param snd the sound entry to read
   * @return the raw sound entry data
   * @throws IOException
   */
  public byte[] getSndEntryData(SndEntry snd) throws IOException {
    try (FileInputStream in = new FileInputStream(sourceFile)) {
      StreamTools.skip(in, snd.ADDRESS);
      return StreamTools.readBytes(in, snd.SIZE);
    }
  }

  /**
   * Returns the given wav file as a byte array.
   * @param index the wav file index
   * @return the wav byte array
   * @throws IOException
   */
  public byte[] getWaveAsByteArray(int index) throws IOException {
    // reconstruct full wave from raw data which lacks the header info
    Wave wave = getWave(index);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    wave.save(baos);
    return baos.toByteArray();
  }

  /**
   * Opens the provided file.
   * @param whereFrom the File to be opened.
   * @throws IOException
   **/
  @Override
  public void open(File whereFrom) throws IOException {
    sourceFile = null;
    changed = false;
    SndEntry modsEntry = null;
    SndEntry descEntry = null;

    try (FileInputStream in = new FileInputStream(whereFrom)) {
      int totalSounds = 0;

      // read mini map
      MINI_MAP.clear();
      for (int i = 0; i < 16; i++) {
        int num = StreamTools.readInt(in, true);
        int sndEntryAddress = StreamTools.readInt(in, true);
        MINI_MAP.add(new Pair<Integer, Integer>(num, sndEntryAddress));
        totalSounds += num;
      }

      // skip to first sound entry
      int firstSndEntryAddress = MINI_MAP.get(0).RIGHT.intValue();
      // sub 16 * 8 bytes of the mini map
      int firstSndEntryOffset = firstSndEntryAddress - 128;
      StreamTools.skip(in, firstSndEntryOffset);

      // read sound entries
      // 151 sounds by default
      for (int j = 0; j < totalSounds; j++)
        MAP.add(new SndEntry(in));

      sourceFile = whereFrom;

      if (in.available() > 0)
        modsEntry = new SndEntry(in);
      if (in.available() > 0)
        descEntry = new SndEntry(in);
    }

    // read mod list
    if (modsEntry != null) {
      try (FileInputStream in = new FileInputStream(whereFrom)) {
        StreamTools.skip(in, modsEntry.ADDRESS);
        byte[] b = StreamTools.readBytes(in, modsEntry.SIZE);
        ByteArrayInputStream bais = new ByteArrayInputStream(b);
        MODS = new ModList(bais);
      } catch (Exception e) {
        e.printStackTrace();
        MODS = new ModList();
      }
    } else {
      MODS = new ModList();
    }

    // read descriptions
    if (descEntry != null) {
      try (FileInputStream in = new FileInputStream(whereFrom)) {
        StreamTools.skip(in, descEntry.ADDRESS);
        byte[] b = StreamTools.readBytes(in, descEntry.SIZE);
        ByteArrayInputStream bais = new ByteArrayInputStream(b);
        DESCRIPTION = new SndDesc(bais);
      } catch (Exception e) {
        e.printStackTrace();
        DESCRIPTION = new SndDesc();
      }
    } else {
      String fn = "music.dsc"; //$NON-NLS-1$
      File fdesc = new File(fn);

      if (fdesc.exists()) {
        FileInputStream filod = new FileInputStream(fdesc);
        DESCRIPTION = new SndDesc(filod);
      } else {
        DESCRIPTION = new SndDesc();
      }
    }
  }

  /**
   * @return true if changes were made, else false.
   */
  @Override
  public boolean isChanged() {
    return (changed || DESCRIPTION.madeChanges());
  }

  @Override
  public boolean isSneaked() {
    return false;
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   * @return A Vector object containing results of the check as Strings.
   */
  @Override
  public Vector<String> check() {
    Vector<String> response = new Vector<String>();

    if (MAP.size() != DEFAULT_NUM_SOUNDS) {
      String msg = Language.getString("Music.7"); //$NON-NLS-1$
      msg = msg.replace("%1", sourceFile.getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(MAP.size())); //$NON-NLS-1$
      response.add(msg);
    }

    SndEntry snd = MAP.get(0);
    for (int i = 1; i < MAP.size(); i++) {
      SndEntry snd2 = MAP.get(i);
      if (snd.ADDRESS + snd.SIZE != snd2.ADDRESS) {
        String msg = Language.getString("Music.8"); //$NON-NLS-1$
        msg = msg.replace("%1", sourceFile.getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(snd.ADDRESS)); //$NON-NLS-1$
        response.add(msg);
      }
      snd = snd2;
    }

    if (response.isEmpty())
      response.add(Language.getString("Music.9")); //$NON-NLS-1$

    return response;
  }

  /**
   * Extracts a playable wav file to the specified destination.
   *
   * @param index the index of the sound file
   * @param dest  the destination file
   * @throws IOException
   * @throws InvalidArgumentsException
   * @throws UnsupportedWave
   */
  @Override
  public void extract(int index, File dest) throws IOException {
    Wave tmp = getWave(index);
    try (FileOutputStream nu = new FileOutputStream(dest)) {
      tmp.save(nu);
    }
  }

  /**
   * Replaces a sound in snd with the specified one.
   * @param index  the index of the sound file
   * @param source the source file
   */
  @Override
  public void replace(int index, File source) throws IOException {
    if (index < 0 || index >= MAX_SOUNDS)
      throw new IndexOutOfBoundsException(getIndexNotFoundString(index));

    byte[] zuz;
    try (FileInputStream sig = new FileInputStream(source)) {
      zuz = new byte[sig.available()];
      int tz = 0;
      while (sig.available() > 0)
        tz = sig.read(zuz, tz, sig.available());
    }

    Wave m = new Wave(zuz);
    if (m.getSndType() != 3)
      throw new IOException(Language.getString("Music.10")); //$NON-NLS-1$

    WAVE.put(index, m);
    changed = true;
  }

  /**
   * Replaces a sound in snd with the specified one.
   * @param index  the index of the sound file
   * @param source the source file
   */
  public void replace(int index, Wave source) throws IOException {
    if (index < 0 || index >= MAX_SOUNDS)
      throw new IndexOutOfBoundsException(getIndexNotFoundString(index));
    if (source.getSndType() != 3)
      throw new IOException(Language.getString("Music.10")); //$NON-NLS-1$

    WAVE.put(index, source);
    //check memory, force save
    Runtime rn = Runtime.getRuntime();
    rn.gc();

    changed = true;
  }

  /**
   * Creates one big playable file.
   * @param dest the destination file
   */
  @Override
  public void thinkBig(File dest) {
    GuiTools.runUEWorker(new ThinkBigTask(dest));
  }

  /**
   * Plays the specified sound.
   * @param index the index of the sound
   */
  @Override
  public void play(int index) {
    // Java AudioSystem doesn't support DVI_IMA_ADPCM (17)
    // @see com.sun.media.sound.WaveFileFormat
    throw new UnsupportedOperationException(Language.getString("Music.12")); //$NON-NLS-1$
  }

  /**
   * Returns true if the play function is supported by the class.
   */
  @Override
  public boolean isPlayable() {
    return false;
  }

  @Override
  public boolean isModded() {
    return !MODS.isEmpty() || isChanged();
  }

  private String getIndexNotFoundString(int index) {
    String msg = Language.getString("Music.13"); //$NON-NLS-1$
    msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
    return msg;
  }

  public int computeSoundSize() {
    int numSndEntries = MAP.size();
    int size = 0;

    for (int i = 0; i < numSndEntries; i++) {
      if (WAVE.containsKey(i)) {
        Wave w = WAVE.get(i);
        size += w.getDataSize();
      } else {
        SndEntry sus = MAP.get(i);
        size += sus.SIZE;
      }
    }

    return size;
  }

  private class SaveTask extends UEWorker<Boolean> {

    private static final String TmpFileName = "music.tmp"; //$NON-NLS-1$

    private final File destFile;
    private final boolean backup;
    private final String ext;

    private File outFile;

    public SaveTask(File destination, boolean backup, String extension) {
      outFile = destFile = destination;
      this.backup = backup;
      this.ext = extension;
      RESULT = false;
    }

    @Override
    public Boolean work() throws IOException {
      // create temporary file and backup if destination already exists
      checkDestination();

      if (isCancelled()) {
        // delete temp file created by getWritableLocalFile
        outFile.delete();
        return false;
      }

      if (ADD_CHANGES_2_MODLIST)
        updateModList();

      if (isCancelled()) {
        outFile.delete();
        return false;
      }

      // start write
      try {
        // calculate map address
        sendMessage(Language.getString("Music.6")); //$NON-NLS-1$

        // cache mod list data
        byte[] mldata = MODS.toByteArray();
        // cache description data
        byte[] ddata = DESCRIPTION.toByteArray();

        // check settings to write a separate description file
        if (UE.SETTINGS.getProperty(SettingsManager.STORE_SND_DESC).equals(SettingsManager.YES)) {
          String fn = "music.dsc"; //$NON-NLS-1$
          try (FileOutputStream gre = new FileOutputStream(FileManager.getWritableLocalFile(fn))) {
            gre.write(ddata);
          }
        }

        // compute sound entry map address
        // with 16 * 8 bytes = 128 bytes offset of the mini map
        final int mapAddress = 128 + computeSoundSize() + mldata.length + ddata.length;
        final int numSndEntries = MAP.size();

        // write
        try (FileOutputStream out = new FileOutputStream(outFile)) {
          // write mini map of multiple sound entry addresses
          for (int i = 0; i < 16; i++) {
            int value = (int) ((double) (i * 100) / 15);
            String msg = Language.getString("Music.17") //$NON-NLS-1$
              .replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
            feedMessage(msg);

            int sndIndex = getMiniMapSoundIndex(i);
            int sndEntryAddress = mapAddress + sndIndex * SndEntry.ENTRY_SIZE;
            val miniMapEntry = MINI_MAP.get(i);
            miniMapEntry.RIGHT = sndEntryAddress;

            out.write(DataTools.toByte(miniMapEntry.LEFT.intValue(), true));
            out.write(DataTools.toByte(sndEntryAddress, true));
          }

          // set address to the start of the sound data
          int address = 128;

          // write sounds
          for (int i = 0; i < numSndEntries; i++) {
            int value = (int) ((double) (i * 100) / (numSndEntries-1));
            String msg = Language.getString("Music.15") //$NON-NLS-1$
              .replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
            feedMessage(msg);

            if (isCancelled()) {
              outFile.delete();
              return false;
            }

            SndEntry snd = MAP.get(i);
            snd.ADDRESS = address;
            Wave w = getWave(i);
            snd.SIZE = w.getDataSize();
            snd.FRAMES = w.getFrames();
            byte[] b = w.getRawData();
            out.write(b);
            address += snd.SIZE;
          }

          // write mod list
          SndEntry mlEntry = new SndEntry();
          mlEntry.ADDRESS = address;
          mlEntry.SIZE = mldata.length;
          address += mlEntry.SIZE;
          out.write(mldata);

          // write descriptions
          SndEntry dscEntry = new SndEntry();
          dscEntry.ADDRESS = address;
          dscEntry.SIZE = ddata.length;
          address += dscEntry.SIZE;
          out.write(ddata);

          // write sound entries
          for (int i = 0; i < numSndEntries; i++) {
            int value = (int) ((double) (i * 100) / (numSndEntries-1));
            String msg = Language.getString("Music.16") //$NON-NLS-1$
              .replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
            feedMessage(msg);

            SndEntry snd = MAP.get(i);
            snd.save(out);
          }

          // write mod list entry
          mlEntry.save(out);
          // write description entry
          dscEntry.save(out);
        }
      } catch (Exception e) {
        outFile.delete();
        throw e;
      }

      // last chance to cancel action
      if (isCancelled()) {
        outFile.delete();
        return false;
      }

      // replace destination by temporary file
      applyTempFile();

      // write completed, do final cleanup
      WAVE.clear();
      return true;
    }

    private int getMiniMapSoundIndex(int i) {
      switch (i) {
        case 0:   return 0;   // +5
        case 1:   return 5;   // +15
        case 2:   return 20;  // +16
        case 3:   return 36;  // +10
        case 4:   return 46;  // +9
        case 5:   return 55;  // +11
        case 6:   return 66;  // +4
        case 7:   return 70;  // +3
        case 8:   return 73;  // +15
        case 9:   return 88;  // +15
        case 10:  return 103; // +15
        case 11:  return 118; // +10
        case 12:  return 128; // +12
        case 13:  return 140; // +4
        case 14:  return 144; // +3
        case 15:  return 147; // +4 = MAX_SOUNDS = 151 sound entries
      }
      throw new IndexOutOfBoundsException();
    }

    private void checkDestination() throws IOException {
      if (!destFile.exists())
        return;

      // create backup
      if (backup) {
        sendMessage(Language.getString("Music.22")); //$NON-NLS-1$

        File file = new File(destFile.getPath() + ext);
        int i = 1;

        while (file.exists()) {
          file = new File(destFile.getPath() + i + ext);
          i++;
        }

        Files.copy(destFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
      }

      String msg = Language.getString("Music.11"); //$NON-NLS-1$
      msg = msg.replace("%1", TmpFileName); //$NON-NLS-1$
      sendMessage(msg);

      outFile = FileManager.getWritableLocalFile(TmpFileName);
    }

    private void applyTempFile() throws IOException {
      if (outFile == destFile)
        return;

      String msg = Language.getString("Music.18"); //$NON-NLS-1$
      sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

      // replace with TMP file
      // on error keep temp file for manual rename
      Files.copy(outFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

      msg = Language.getString("Music.20"); //$NON-NLS-1$
      sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

      // attempt to delete the temporary file
      boolean deleted = outFile.delete();
      if (!deleted) {
        // the save operation succeeded, just the temp file couldn't be removed
        msg = Language.getString(Language.getString("Music.21")); //$NON-NLS-1$
        logError(msg.replace("%1", outFile.getName())); //$NON-NLS-1$
      }
    }
  }

  private class ThinkBigTask extends UEWorker<Void> {

    static final int strRIFF = 1179011410;
    static final int strWAVE = 1163280727;
    static final int strFMT = 544501094;
    static final int WAVE_CHUNK_SIZE = 20;
    static final short FORMAT = 17;
    static final short CHANNELS = 2;
    static final int FREQUENCY = 22050;
    static final int BYTES_PER_SECOND = 11100;
    static final short BLOCK_ALIGMENT = 1024;
    static final short BITS_PER_SAMPLE = 4;
    static final short EXTRA_FMT = 2;
    static final short SAMPLES_PER_BLOCK = 1017;
    static final int strFACT = 1952670054;
    static final int intFACT = 4;
    static final int strDATA = 1635017060;

    File path;

    public ThinkBigTask(File dest) {
      super();
      path = dest;
    }

    @Override
    public Void work() throws IOException {
      int numSndEntries = MAP.size();
      int frames = 0;
      int size = 0;

      for (int i = 0; i < numSndEntries; i++) {
        if (WAVE.containsKey(i)) {
          Wave w = getWave(i);
          size += w.getDataSize();
          frames += w.getFrames();
        } else {
          SndEntry se = MAP.get(i);
          frames += se.FRAMES;
          size += se.SIZE;
        }
      }

      try (FileOutputStream out = new FileOutputStream(path)) {
        //RIFF
        out.write(DataTools.toByte(strRIFF, true));
        //size-8
        out.write(DataTools.toByte((size + 52), true));
        //WAVE
        out.write(DataTools.toByte(strWAVE, true));
        //FMT
        out.write(DataTools.toByte(strFMT, true));
        //wave chunk size
        out.write(DataTools.toByte(WAVE_CHUNK_SIZE, true));
        //format
        out.write(DataTools.toByte(FORMAT, true));
        //channels
        out.write(DataTools.toByte(CHANNELS, true));
        //frequency
        out.write(DataTools.toByte(FREQUENCY, true));
        //bytes per sec
        out.write(DataTools.toByte(BYTES_PER_SECOND, true));
        //block aligment
        out.write(DataTools.toByte(BLOCK_ALIGMENT, true));
        //bits per sample
        out.write(DataTools.toByte(BITS_PER_SAMPLE, true));
        //extra chunk size
        out.write(DataTools.toByte(EXTRA_FMT, true));
        //samples per block
        out.write(DataTools.toByte(SAMPLES_PER_BLOCK, true));
        //FACT
        out.write(DataTools.toByte(strFACT, true));
        //intFACT
        out.write(DataTools.toByte(intFACT, true));
        //frames/4
        out.write(DataTools.toByte((frames), true));
        //DATA
        out.write(DataTools.toByte(strDATA, true));
        //data size
        out.write(DataTools.toByte(size, true));

        //write data
        for (int i = 0; i < numSndEntries; i++) {
          if (isCancelled()) {
            path.delete();
            return null;
          }

          int value = (int) (i * 100 / (double) (numSndEntries-1));
          String msg = Language.getString("Music.14"); //$NON-NLS-1$
          msg = msg.replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
          feedMessage(msg);

          Wave w = getWave(i);
          byte[] b = w.getRawData();
          out.write(b);
        }
      }

      return null;
    }
  }
}
