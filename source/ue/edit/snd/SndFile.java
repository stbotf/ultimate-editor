package ue.edit.snd;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import ue.audio.Wave;
import ue.edit.common.MainFile;
import ue.edit.mod.ModList;
import ue.edit.res.stbof.files.rst.RaceRst;
import ue.exception.KeyNotFoundException;

public abstract class SndFile extends MainFile {

  protected SndDesc DESCRIPTION;
  protected ModList MODS;

  @Override
  public abstract String getName();

  /**
   * Returns the number of sounds in this file.
   */
  public abstract int getNumberOfSounds();

  /**
   * Extracts a playable wav file to the specified destination.
   *
   * @param index the index of the sound file
   * @param dest  the destination file
   * @throws IOException
   */
  public abstract void extract(int index, File dest) throws IOException;

  /**
   * replaces a sound in snd with the specified one.
   *
   * @param index  the index of the sound file
   * @param source the source file
   */
  public abstract void replace(int index, File source) throws IOException;

  /**
   * Creates one big playable file.
   *
   * @param dest the destination file
   */
  public abstract void thinkBig(File dest);

  /**
   * Plays the specified sound.
   *
   * @param index the index of the sound
   * @throws Exception
   */
  public abstract void play(int index) throws Exception;

  /**
   * There you go, returns Wave.
   * @throws IOException
   */
  public abstract Wave getWave(int i) throws IOException;

  /**
   * Returns true if the play function is supported by the class.
   */
  public abstract boolean isPlayable();

  public short getRaceDescriptor(int index) {
    return DESCRIPTION.getRace(index);
  }

  public short getTypeDescriptor(int index) {
    return DESCRIPTION.getType(index);
  }

  public String getShortName(int index) {
    return DESCRIPTION.getShortName(index);
  }

  public String getDescription(int index) {
    return DESCRIPTION.getDesc(index);
  }

  public void setTypeDescriptor(int index, short type) {
    DESCRIPTION.setType(index, type);
  }

  public void setRaceDescriptor(int index, short race) throws KeyNotFoundException {
    DESCRIPTION.setRace(index, race);
  }

  public void exportDescriptions(OutputStream out, int num, RaceRst race) throws IOException {
    DESCRIPTION.export(out, getNumberOfSounds(), race);
  }

  public void setDescription(int index, String desc) {
    DESCRIPTION.setDesc(index, desc);
  }

  @Override
  public ModList modList() {
    return MODS;
  }

}
