package ue.edit.snd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import ue.service.Language;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class SndEntry {

  public static final int ENTRY_SIZE = 16;

  public int ADDRESS;
  public int SIZE;
  public int FRAMES;
  public int UNKNOWN;

  public SndEntry() {
    ADDRESS = 0;
    SIZE = 0;
    FRAMES = 0;
    UNKNOWN = 0;
  }

  public SndEntry(InputStream in) throws IOException {
    ADDRESS = StreamTools.readInt(in, true);
    SIZE = StreamTools.readInt(in, true);
    FRAMES = StreamTools.readInt(in, true);
    UNKNOWN = StreamTools.readInt(in, true);

    if (ADDRESS < 0 || SIZE < 0 || FRAMES < 0) {
      String msg = Language.getString("SndEntry.0"); //$NON-NLS-1$
      msg = msg.replace("%1", Integer.toString(ADDRESS)); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(SIZE)); //$NON-NLS-1$
      msg = msg.replace("%3", Integer.toString(FRAMES)); //$NON-NLS-1$
      throw new IOException(msg);
    }
  }

  public void save(OutputStream out) throws IOException {
    out.write(DataTools.toByte(ADDRESS, true));
    out.write(DataTools.toByte(SIZE, true));
    out.write(DataTools.toByte(FRAMES, true));
    out.write(DataTools.toByte(UNKNOWN, true));
  }
}
