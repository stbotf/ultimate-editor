package ue.edit.snd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import lombok.val;
import ue.UE;
import ue.audio.Wave;
import ue.audio.Wave.WaveType;
import ue.edit.mod.ModList;
import ue.edit.res.stbof.Stbof;
import ue.edit.res.stbof.common.CStbofFiles;
import ue.edit.res.stbof.files.bin.Converse;
import ue.edit.res.stbof.files.dic.idx.LexHelper;
import ue.exception.InvalidArgumentsException;
import ue.exception.KeyNotFoundException;
import ue.gui.menu.MenuCommand;
import ue.gui.util.GuiTools;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.UEWorker;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

/**
 * English.snd or german.snd file.
 *
 * @author Alan Podlesek
 */
public class English extends SndFile {

  public static final int TYPE = FileManager.P_ENGLISH;
  public static final int DEFAULT_NUM_SOUNDS = 1364;

  private FileCache fileCache = new FileCache();
  private boolean isEnglish;
  private boolean changed = false;
  private ArrayList<SndEntry> MAP = new ArrayList<SndEntry>(DEFAULT_NUM_SOUNDS);
  private Hashtable<Integer, Wave> WAVE = new Hashtable<Integer, Wave>(DEFAULT_NUM_SOUNDS);
  public boolean ADD_CHANGES_2_MODLIST = true;

  public English(boolean english) {
    isEnglish = english;
  }

  @Override
  public int type() {
    return TYPE;
  }

  @Override
  public String getName() {
    return MenuCommand.mapCommandName(MenuCommand.Voices);
  }

  @Override
  public void markChanged() {
    changed = true;
  }

  public void updateModList() {
    Enumeration<Integer> keys = WAVE.keys();
    while (keys.hasMoreElements()) {
      MODS.add(keys.nextElement().toString());
    }
  }

  @Override
  public int getNumberOfSounds() {
    return MAP.size();
  }

  /**
   * Saves changes.
   *
   * @param dest File where everything should be saved to. If null save to the same file.
   * @return true if save has been successful.
   * @throws IOException
   */
  @Override
  public boolean save(File dest, boolean bkp, String ext) throws IOException {
    // if not specified, save to the open file
    if (dest == null)
      dest = getTargetFile();

    val saveTask = new SaveTask(dest, bkp, ext);
    boolean success = GuiTools.runUEWorker(saveTask);

    if (success) {
      sourceFile = dest;
      changed = false;
    }

    return success;
  }

  /**
   * There you go, returns Wave.
   */
  @Override
  public Wave getWave(int index) throws IOException {
    // check for cached modified wave files
    Wave cached = WAVE.get(index);
    if (cached != null)
      return cached;

    val snd = MAP.get(index);
    byte[] data = getSndEntryData(snd);

    // the voice entries lack the wave header,
    // so create Wave with pure content data
    return new Wave(WaveType.English, data, snd.FRAMES);
  }

  /**
   * Returns the pointed sound entry data.
   * @param snd the sound entry to read
   * @return the raw sound entry data
   * @throws IOException
   */
  public byte[] getSndEntryData(SndEntry snd) throws IOException {
    try (FileInputStream in = new FileInputStream(sourceFile)) {
      StreamTools.skip(in, snd.ADDRESS);
      return StreamTools.readBytes(in, snd.SIZE);
    }
  }

  /**
   * Returns the given wav file as a byte array.
   * @param index the wav file index
   * @return the wav byte array
   * @throws IOException
   */
  public byte[] getWaveAsByteArray(int index) throws IOException {
    // reconstruct full wave from raw data which lacks the header info
    Wave wave = getWave(index);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    wave.save(baos);
    return baos.toByteArray();
  }

  /**
   * Opens the provided file.
   *
   * @param whereFrom the File to be opened.
   * @throws IOException
   */
  @Override
  public void open(File whereFrom) throws IOException {
    fileCache.reset();
    sourceFile = null;
    changed = false;
    SndEntry modsEntry = null;
    SndEntry descEntry = null;

    try (FileInputStream in = new FileInputStream(whereFrom)) {
      int mapaddy = StreamTools.readInt(in, true);
      StreamTools.skip(in, mapaddy - 4);

      // 1364 sounds by default
      while (in.available() > 0) {
        SndEntry entry = new SndEntry(in);
        if (entry.FRAMES > 0 || in.available() > 16) {
          MAP.add(entry);
        } else if (modsEntry == null) {
          // feature second from last additional entry for the mods list
          modsEntry = entry;
        } else {
          // feature last additional entry for the description list
          descEntry = entry;
        }
      }
    }

    sourceFile = whereFrom;

    // mod list
    if (modsEntry != null) {
      try (FileInputStream in = new FileInputStream(whereFrom)) {
        StreamTools.skip(in, modsEntry.ADDRESS);
        byte[] b = StreamTools.readBytes(in, modsEntry.SIZE);
        MODS = new ModList(new ByteArrayInputStream(b));
      } catch (Exception e) {
        e.printStackTrace();
        MODS = new ModList();
      }
    } else {
      MODS = new ModList();
    }

    // desc list
    if (descEntry != null) {
      try (FileInputStream in = new FileInputStream(whereFrom)) {
        StreamTools.skip(in, descEntry.ADDRESS);
        byte[] b = StreamTools.readBytes(in, descEntry.SIZE);
        DESCRIPTION = new SndDesc(new ByteArrayInputStream(b));
      } catch (Exception e) {
        e.printStackTrace();
        DESCRIPTION = new SndDesc();
      }
    } else {
      String fn = isEnglish ? "english.dsc" : "german.dsc"; //$NON-NLS-1$ //$NON-NLS-2$
      File fdesc = new File(fn);
      if (fdesc.exists()) {
        try (FileInputStream filod = new FileInputStream(fdesc)) {
          DESCRIPTION = new SndDesc(filod);
        } catch (Exception e) {
          e.printStackTrace();
          DESCRIPTION = new SndDesc();
        }
      } else {
        DESCRIPTION = new SndDesc();
      }
    }
  }

  @Override
  public String getShortName(int index) {
    Converse conv = fileCache.converse();
    if (conv == null)
      return DESCRIPTION.getShortName(index);

    val info = conv.getInfo((short) index);
    String name = Integer.toString(index) + ": ";

    if (info != null) {
      name += LexHelper.mapRace(info.race) + (info.groupType == 0
        ? " [" + info.groupIndex + "]"
        : " [" + info.groupIndex + "/" + info.listIndex + ", type " + info.listType + "]");
    } else {
      name += "Unused";
    }

    return name;
  }

  @Override
  public short getRaceDescriptor(int index) {
    Converse conv = fileCache.converse();
    if (conv == null)
      return super.getRaceDescriptor(index);

    short race = (short) conv.getVoiceOwner((short) index);
    if (race >= 0 && race < 5) {
      // adjust race for 'Unknown' and 'Neutral'
      race += 2;
    } else if (race == 36 || race == 37) {
      // map both 36 (alien) and 37 (neutral) to neutral
      race = 1;
    } else if (race < 0 || race == 255) {
      race = 7;
    } else {
      race = 0;
    }

    return race;
  }

  @Override
  public void setRaceDescriptor(int index, short race) throws KeyNotFoundException {
    if (race < 0 || race > 255)
      throw new IndexOutOfBoundsException(Language.getString("English.23")); //$NON-NLS-1$

    Converse conv = fileCache.converse();
    if (conv == null)
      throw new UnsupportedOperationException(Language.getString("English.24")); //$NON-NLS-1$

    int convRace =
      race >= 2 && race < 7 ? race - 2  // revert 'Unknown' and 'Neutral' alignment
        : race == 1 ? 37                // alien/neutral -> neutral
        : race == 7 ? -1                // unused -> unused
        : 255;                          // unknown -> unknown

    // set converse first to make sure we have a valid entry which is not 'unused'
    // the race for unused entries can't be set yet, since there exist no voice group assignments
    conv.setVoiceOwner((short) index, convRace);
    super.setRaceDescriptor(index, race);
  }

  /**
   * @return true if changes were made, else false.
   */
  @Override
  public boolean isChanged() {
    return (changed || DESCRIPTION.madeChanges());
  }

  @Override
  public boolean isSneaked() {
    return false;
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   *
   * @return A Vector object containing results of the check as Strings.
   */
  @Override
  public Vector<String> check() {
    Vector<String> response = new Vector<String>();

    // check for default number of sounds
    if (MAP.size() != DEFAULT_NUM_SOUNDS) {
      String msg = Language.getString("English.6"); //$NON-NLS-1$
      msg = msg.replace("%1", sourceFile.getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(MAP.size())); //$NON-NLS-1$
      response.add(msg);
    }
    SndEntry snd = MAP.get(0);

    for (int i = 1; i < MAP.size(); i++) {
      SndEntry snd2 = MAP.get(i);
      if (snd.ADDRESS + snd.SIZE != snd2.ADDRESS) {
        String msg = Language.getString("English.7"); //$NON-NLS-1$
        msg = msg.replace("%1", sourceFile.getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(snd.ADDRESS)); //$NON-NLS-1$
        response.add(msg);
      }
      snd = snd2;
    }

    Converse conv = fileCache.converse();
    if (conv != null)
      response.add("Info:\n\nSound groups:\n\n" + conv.toString());
    if (response.isEmpty())
      response.add(Language.getString("English.8")); //$NON-NLS-1$

    return response;
  }

  /**
   * Extracts a playable wav file to the specified destination.
   *
   * @param index the index of the sound file
   * @param dest  the destination file
   * @throws IOException
   */
  @Override
  public void extract(int index, File dest) throws IOException {
    Wave tmp = getWave(index);
    try (FileOutputStream nu = new FileOutputStream(dest)) {
      tmp.save(nu);
    }
  }

  /**
   * replaces a sound in snd with the specified one.
   *
   * @param index  the index of the sound file
   * @param source the source file
   */
  @Override
  public void replace(int index, File source) throws IOException {
    if (index < 0 || index >= MAP.size())
      throw new IndexOutOfBoundsException(getIndexNotFoundString(index));

    byte[] data;
    try (FileInputStream fis = new FileInputStream(source)) {
      int read = 0;
      data = new byte[fis.available()];
      while (fis.available() > 0)
        read += fis.read(data, read, fis.available());
    }

    Wave m = new Wave(data);
    if (m.getSndType() != 1)
      throw new IOException(Language.getString("English.9")); //$NON-NLS-1$

    WAVE.put(index, m);
    changed = true;
  }

  /**
   * replaces a sound in snd with the specified one.
   *
   * @param index  the index of the sound file
   * @param source the source file
   */
  public void replace(int index, Wave source) throws InvalidArgumentsException {
    if (index < 0 || index >= MAP.size())
      throw new InvalidArgumentsException(getIndexNotFoundString(index));
    if (source.getSndType() != 1)
      throw new InvalidArgumentsException(Language.getString("English.9")); //$NON-NLS-1$

    WAVE.put(index, source);
    changed = true;
  }

  /**
   * Creates one big playable file.
   *
   * @param whereTo the destination file
   */
  @Override
  public void thinkBig(File whereTo) {
    GuiTools.runUEWorker(new ThinkBigTask(whereTo));
  }

  /**
   * Plays the specified sound.
   *
   * @param index the index of the sound
   */
  @Override
  public void play(int index) {
    // Java AudioSystem doesn't support DVI_IMA_ADPCM (17)
    // @see com.sun.media.sound.WaveFileFormat
    throw new UnsupportedOperationException(Language.getString("English.11")); //$NON-NLS-1$
  }

  /**
   * Returns true if the play function is supported by the class.
   */
  @Override
  public boolean isPlayable() {
    return false;
  }

  private String getIndexNotFoundString(int index) {
    String msg = Language.getString("English.13"); //$NON-NLS-1$
    msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
    return msg;
  }

  @Override
  public boolean isModded() {
    return !MODS.isEmpty() || isChanged();
  }

  public int computeSoundSize() {
    int numSndEntries = MAP.size();
    int size = 0;

    for (int i = 0; i < numSndEntries; i++) {
      if (WAVE.containsKey(i)) {
        Wave w = WAVE.get(i);
        size += w.getDataSize();
      } else {
        SndEntry sus = MAP.get(i);
        size += sus.SIZE;
      }
    }

    return size;
  }

  private class SaveTask extends UEWorker<Boolean> {

    private static final String TmpFileName = "english.tmp"; //$NON-NLS-1$

    private final File destFile;
    private final boolean backup;
    private final String ext;

    private File outFile;

    /**
     * @param destination destination file
     * @param backup make backup?
     * @param extension backup extension
     */
    public SaveTask(File destination, boolean backup, String extension) {
      outFile = destFile = destination;
      this.backup = backup;
      this.ext = extension;
      RESULT = false;
    }

    @Override
    public Boolean work() throws IOException {
      // create temporary file and backup if destination already exists
      checkDestination();

      if (isCancelled()) {
        // delete temp file created by getWritableLocalFile
        outFile.delete();
        return false;
      }

      if (ADD_CHANGES_2_MODLIST)
        updateModList();

      if (isCancelled()) {
        outFile.delete();
        return false;
      }

      // start write
      try {
        // calculate map address
        sendMessage(Language.getString("English.15")); //$NON-NLS-1$

        // cache mod list data
        byte[] mldata = MODS.toByteArray();
        // cache description data
        byte[] ddata = DESCRIPTION.toByteArray();

        // check settings to write a separate description file
        if (UE.SETTINGS.getProperty(SettingsManager.STORE_SND_DESC).equals(SettingsManager.YES)) {
          String fn = isEnglish ? "english.dsc" : "german.dsc"; //$NON-NLS-1$ //$NON-NLS-2$
          try (FileOutputStream gre = new FileOutputStream(FileManager.getWritableLocalFile(fn))) {
            gre.write(ddata);
          }
        }

        // compute sound entry map address
        // with 4 bytes for the written map address
        final int mapAddress = 4 + computeSoundSize() + mldata.length + ddata.length;
        final int numSndEntries = MAP.size();

        // write
        try (FileOutputStream out = new FileOutputStream(outFile)) {
          // write sound entry map address
          out.write(DataTools.toByte(mapAddress, true));
          int address = 4;

          // write sounds
          for (int i = 0; i < numSndEntries; i++) {
            int value = (int) ((double) (i * 100) / (numSndEntries-1));
            String msg = Language.getString("English.16") //$NON-NLS-1$
              .replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
            feedMessage(msg);

            if (isCancelled()) {
              outFile.delete();
              return false;
            }

            Wave w = getWave(i);
            SndEntry snd = MAP.get(i);
            snd.ADDRESS = address;
            snd.SIZE = w.getDataSize();
            snd.FRAMES = w.getFrames();
            byte[] b = w.getRawData();
            out.write(b);
            address += snd.SIZE;
          }

          // write mod list
          SndEntry mlEntry = new SndEntry();
          mlEntry.ADDRESS = address;
          mlEntry.SIZE = mldata.length;
          address += mlEntry.SIZE;
          out.write(mldata);

          // write descriptions
          SndEntry dscEntry = new SndEntry();
          dscEntry.ADDRESS = address;
          dscEntry.SIZE = ddata.length;
          address += dscEntry.SIZE;
          out.write(ddata);

          // write sound entries
          for (int i = 0; i < numSndEntries; i++) {
            int value = (int) ((double) (i * 100) / (numSndEntries-1));
            String msg = Language.getString("English.17") //$NON-NLS-1$
              .replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
            feedMessage(msg);

            SndEntry snd = MAP.get(i);
            snd.save(out);
          }

          // write mod list entry
          mlEntry.save(out);
          // write description entry
          dscEntry.save(out);
        }
      } catch (Exception e) {
        outFile.delete();
        throw e;
      }

      // last chance to cancel action
      if (isCancelled()) {
        updateStatus(UEWorker.STATUS_CANCEL);
        outFile.delete();
        return false;
      }

      // replace destination by temporary file
      applyTempFile();

      // write completed, do final cleanup
      WAVE.clear();
      return true;
    }

    private void checkDestination() throws IOException {
      if (!destFile.exists())
        return;

      // create backup
      if (backup) {
        sendMessage(Language.getString("English.22")); //$NON-NLS-1$

        File file = new File(destFile.getPath() + ext);
        int i = 1;

        while (file.exists()) {
          file = new File(destFile.getPath() + i + ext);
          i++;
        }

        Files.copy(destFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
      }

      String msg = Language.getString("English.12"); //$NON-NLS-1$
      msg = msg.replace("%1", TmpFileName); //$NON-NLS-1$
      sendMessage(msg);

      outFile = FileManager.getWritableLocalFile(TmpFileName);
    }

    private void applyTempFile() throws IOException {
      if (outFile == destFile)
        return;

      String msg = Language.getString("English.20"); //$NON-NLS-1$
      sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

      // replace with TMP file
      // on error keep temp file for manual rename
      Files.copy(outFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

      msg = Language.getString("English.19"); //$NON-NLS-1$
      sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

      // attempt to delete the temporary file
      boolean deleted = outFile.delete();
      if (!deleted) {
        // the save operation succeeded, just the temp file couldn't be removed
        msg = Language.getString(Language.getString("English.21")); //$NON-NLS-1$
        logError(msg.replace("%1", outFile.getName())); //$NON-NLS-1$
      }
    }
  }

  private class ThinkBigTask extends UEWorker<Void> {

    static final int strRIFF = 1179011410;
    static final int strWAVE = 1163280727;
    static final int strFMT = 544501094;
    static final int WAVE_CHUNK_SIZE = 20;
    static final short FORMAT = 17;
    static final short CHANNELS = 1;
    static final int FREQUENCY = 22050;
    static final int BYTES_PER_SECOND = 11100;
    static final short BLOCK_ALIGNMENT = 512;
    static final short BITS_PER_SAMPLE = 4;
    static final short EXTRA_FMT = 2;
    static final short SAMPLES_PER_BLOCK = 1017;
    static final int strFACT = 1952670054;
    static final int intFACT = 4;
    static final int strDATA = 1635017060;

    File path;

    public ThinkBigTask(File dest) {
      path = dest;
    }

    @Override
    public Void work() throws IOException {
      int numSndEntries = MAP.size();
      int frames = 0;
      int size = 0;

      // calculate
      for (int i = 0; i < numSndEntries; i++) {
        if (WAVE.containsKey(i)) {
          Wave w = getWave(i);
          size += w.getDataSize();
          frames += w.getFrames();
        } else {
          SndEntry se = MAP.get(i);
          frames += se.FRAMES;
          size += se.SIZE;
        }
      }

      // write
      try (
        FileOutputStream out = new FileOutputStream(path);
        RandomAccessFile in = new RandomAccessFile(sourceFile, "r");
      ) {
        //RIFF
        out.write(DataTools.toByte(strRIFF, true));
        //size-8
        out.write(DataTools.toByte((size + 52), true));
        //WAVE
        out.write(DataTools.toByte(strWAVE, true));
        //FMT
        out.write(DataTools.toByte(strFMT, true));
        //wave chunk size
        out.write(DataTools.toByte(WAVE_CHUNK_SIZE, true));
        //format
        out.write(DataTools.toByte(FORMAT, true));
        //channels
        out.write(DataTools.toByte(CHANNELS, true));
        //frequency
        out.write(DataTools.toByte(FREQUENCY, true));
        //bytes per sec
        out.write(DataTools.toByte(BYTES_PER_SECOND, true));
        //block alignment
        out.write(DataTools.toByte(BLOCK_ALIGNMENT, true));
        //bits per sample
        out.write(DataTools.toByte(BITS_PER_SAMPLE, true));
        //extra chunk size
        out.write(DataTools.toByte(EXTRA_FMT, true));
        //samples per block
        out.write(DataTools.toByte(SAMPLES_PER_BLOCK, true));
        //FACT
        out.write(DataTools.toByte(strFACT, true));
        //intFACT
        out.write(DataTools.toByte(intFACT, true));
        //frames/4
        out.write(DataTools.toByte((frames), true));
        //DATA
        out.write(DataTools.toByte(strDATA, true));
        //data size
        out.write(DataTools.toByte(size, true));

        //write data
        for (int i = 0; i < numSndEntries; i++) {
          if (isCancelled()) {
            path.delete();
            return null;
          }

          int value = (int) ((double) (i * 100) / (numSndEntries-1));
          String msg = Language.getString("English.14"); //$NON-NLS-1$
          msg = msg.replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
          feedMessage(msg);

          if (WAVE.containsKey(i)) {
            Wave w = getWave(i);
            byte[] b = w.getRawData();
            out.write(b);
          } else {
            SndEntry snd = MAP.get(i);
            byte[] b = new byte[snd.SIZE];
            in.seek(snd.ADDRESS);
            in.readFully(b);
            out.write(b);
          }
        }
      }

      return null;
    }
  }

  private class FileCache {
    private Converse converse = null;

    public Converse converse() {
      if (converse == null) {
        Stbof stbof = UE.FILES.stbof();
        converse = stbof != null ? (Converse) stbof.tryGetInternalFile(CStbofFiles.ConverseBin, true) : null;
      }
      return converse;
    }

    public void reset() {
      converse = null;
    }
  }
}
