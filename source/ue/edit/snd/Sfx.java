package ue.edit.snd;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import lombok.Getter;
import lombok.val;
import lombok.experimental.Accessors;
import ue.UE;
import ue.audio.SoundSystem;
import ue.audio.Wave;
import ue.audio.Wave.WaveType;
import ue.edit.mod.ModList;
import ue.gui.menu.MenuCommand;
import ue.gui.menu.MusicMenu;
import ue.gui.util.GuiTools;
import ue.service.FileManager;
import ue.service.Language;
import ue.service.SettingsManager;
import ue.service.UEWorker;
import ue.util.data.DataTools;
import ue.util.stream.StreamTools;

public class Sfx extends SndFile {

  public static final int TYPE = FileManager.P_SFX;
  public static final int DEFAULT_NUM_SOUNDS = 163;

  private boolean changed = false;
  private ArrayList<SndEntry> MAP = new ArrayList<SndEntry>(DEFAULT_NUM_SOUNDS);
  private Hashtable<Integer, Wave> WAVE = new Hashtable<Integer, Wave>(DEFAULT_NUM_SOUNDS);
  public boolean ADD_CHANGES_2_MODLIST = true;
  @Getter @Accessors(fluent = true) private MusicMenu menu = new MusicMenu();

  @Override
  public int type() {
    return TYPE;
  }

  @Override
  public String getName() {
    return MenuCommand.mapCommandName(MenuCommand.GameSounds);
  }

  @Override
  public void markChanged() {
    changed = true;
  }

  public void updateModList() {
    Enumeration<Integer> keys = WAVE.keys();
    while (keys.hasMoreElements())
      MODS.add(keys.nextElement().toString());
  }

  @Override
  public int getNumberOfSounds() {
    return MAP.size();
  }

  /**
   * Saves changes.
   *
   * @param dest File where everything should be saved to. If null save to the same file.
   * @return true if save has been successful.
   * @throws IOException
   **/
  @Override
  public boolean save(File dest, boolean bkp, String ext) throws IOException {
    // if not specified, save to the open file
    if (dest == null)
      dest = getTargetFile();

    val saveTask = new SaveTask(dest, bkp, ext);
    boolean success = GuiTools.runUEWorker(saveTask);

    if (success) {
      sourceFile = dest;
      changed = false;
    }

    return success;
  }

  /**
   * There you go, returns Wave.
   * @throws IOException
   */
  @Override
  public Wave getWave(int index) throws IOException {
    // check for cached modified wave files
    Wave cached = WAVE.get(index);
    if (cached != null)
      return cached;

    val snd = MAP.get(index);
    byte[] data = getSndEntryData(snd);

    // the sfx entries include the wave header
    // therefore read and verify the full header
    return new Wave(WaveType.Sfx, data);
  }

  /**
   * Returns the pointed sound entry data.
   * @param snd the sound entry to read
   * @return the raw sound entry data
   * @throws IOException
   */
  public byte[] getSndEntryData(SndEntry snd) throws IOException {
    try (FileInputStream in = new FileInputStream(sourceFile)) {
      StreamTools.skip(in, snd.ADDRESS);
      return StreamTools.readBytes(in, snd.SIZE);
    }
  }

  /**
   * Returns the given wav file as a byte array.
   * @param index the wav file index
   * @return the wav byte array
   * @throws IOException
   */
  public byte[] getWaveAsByteArray(int index) throws IOException {
    val snd = MAP.get(index);
    return getSndEntryData(snd);
  }

  /**
   * Opens the provided file.
   *
   * @param whereFrom the File to be opened.
   * @return a String array of edit options specific for the opened file.
   * @throws IOException
   **/
  @Override
  public void open(File whereFrom) throws IOException {
    sourceFile = null;
    changed = false;
    SndEntry modsEntry = null;
    SndEntry descEntry = null;

    try (FileInputStream in = new FileInputStream(whereFrom)) {
      int mapaddy = StreamTools.readInt(in, true);
      StreamTools.skip(in, mapaddy - 4);

      // 163 sounds by default
      while (in.available() > 0) {
        SndEntry entry = new SndEntry(in);
        if (entry.FRAMES > 0 || in.available() > 0) {
          MAP.add(entry);
        } else if (modsEntry == null) {
          // feature second from last additional entry for the mods list
          modsEntry = entry;
        } else {
          // feature last additional entry for the description list
          descEntry = entry;
        }
      }
    }

    sourceFile = whereFrom;

    // mod list
    if (modsEntry != null) {
      try (FileInputStream in = new FileInputStream(whereFrom)) {
        StreamTools.skip(in, modsEntry.ADDRESS);
        byte[] b = StreamTools.readBytes(in, modsEntry.SIZE);
        MODS = new ModList(new ByteArrayInputStream(b));
      } catch(Exception e) {
        e.printStackTrace();
        MODS = new ModList();
      }
    } else {
      MODS = new ModList();
    }

    // desc list
    if (descEntry != null) {
      try (FileInputStream in = new FileInputStream(whereFrom)) {
        StreamTools.skip(in, descEntry.ADDRESS);
        byte[] b = StreamTools.readBytes(in, descEntry.SIZE);
        DESCRIPTION = new SndDesc(new ByteArrayInputStream(b));
      } catch (Exception e) {
        e.printStackTrace();
        DESCRIPTION = new SndDesc();
      }
    } else {
      String fn = "sfx.dsc"; //$NON-NLS-1$
      File fdesc = new File(fn);
      if (fdesc.exists()) {
        try (FileInputStream filod = new FileInputStream(fdesc)) {
          DESCRIPTION = new SndDesc(filod);
        } catch (Exception e) {
          e.printStackTrace();
          DESCRIPTION = new SndDesc();
        }
      } else {
        DESCRIPTION = new SndDesc();
      }
    }
  }

  /**
   * @return true if changes were made, else false.
   */
  @Override
  public boolean isChanged() {
    return (changed || DESCRIPTION.madeChanges());
  }

  @Override
  public boolean isSneaked() {
    return false;
  }

  /**
   * This function checks the file for known problems/errors that may occur while editing the file.
   * @return A Vector object containing results of the check as Strings.
   */
  @Override
  public Vector<String> check() {
    Vector<String> response = new Vector<String>();

    if (MAP.size() != DEFAULT_NUM_SOUNDS) {
      String msg = Language.getString("Sfx.8"); //$NON-NLS-1$
      msg = msg.replace("%1", sourceFile.getName()); //$NON-NLS-1$
      msg = msg.replace("%2", Integer.toString(MAP.size())); //$NON-NLS-1$
      response.add(msg);
    }

    SndEntry snd = MAP.get(0);
    for (int i = 1; i < MAP.size(); i++) {
      SndEntry snd2 = MAP.get(i);
      if (snd.ADDRESS + snd.SIZE != snd2.ADDRESS) {
        String msg = Language.getString("Sfx.9"); //$NON-NLS-1$
        msg = msg.replace("%1", sourceFile.getName()); //$NON-NLS-1$
        msg = msg.replace("%2", Integer.toString(snd.ADDRESS)); //$NON-NLS-1$
        response.add(msg);
      }
      snd = snd2;
    }

    if (response.isEmpty())
      response.add(Language.getString("Sfx.10")); //$NON-NLS-1$

    return response;
  }

  /**
   * Extracts a playable wav file to the specified destination.
   * @param index the index of the sound file
   * @param dest  the destination file
   */
  @Override
  public void extract(int index, File dest) throws IOException {
    Wave tmp = getWave(index);
    try (FileOutputStream nu = new FileOutputStream(dest)) {
      tmp.save(nu);
    }
  }

  /**
   * Replaces a sound in snd with the specified one.
   * @param index  the index of the sound file
   * @param source the source file
   */
  @Override
  public void replace(int index, File source) throws IOException {
    if (index < 0 || index >= MAP.size())
      throw new IndexOutOfBoundsException(getIndexNotFoundString(index));

    byte[] zuz;
    try (FileInputStream sig = new FileInputStream(source)) {
      zuz = new byte[sig.available()];
      int tz = 0;
      while (sig.available() > 0)
        tz = sig.read(zuz, tz, sig.available());
    }

    Wave m = new Wave(zuz);
    if (m.getSndType() != 2)
      throw new IOException(Language.getString("Sfx.11")); //$NON-NLS-1$

    WAVE.put(index, m);
    changed = true;
  }

  /**
   * Replaces a sound in snd with the specified one.
   * @param index  the index of the sound file
   * @param source the source file
   */
  public void replace(int index, Wave source) throws IOException {
    if (index < 0 || index >= MAP.size())
      throw new IndexOutOfBoundsException(getIndexNotFoundString(index));
    if (source.getSndType() != 2)
      throw new IOException(Language.getString("Sfx.11")); //$NON-NLS-1$

    WAVE.put(index, source);

    //check memory, force save
    Runtime rn = Runtime.getRuntime();
    rn.gc();
    changed = true;
  }

  /**
   * Creates one big playable file.
   * @param dest the destination file
   */
  @Override
  public void thinkBig(File dest) {
    GuiTools.runUEWorker(new ThinkBigTask(dest));
  }

  /**
   * Plays the specified sound.
   * @param index the index of the sound
   * @throws Exception
   */
  @Override
  public void play(int index) throws Exception {
    if (index < 0 || index >= MAP.size())
      throw new IndexOutOfBoundsException(getIndexNotFoundString(index));

    byte[] data = getWaveAsByteArray(index);
    SoundSystem.playSound(data);
  }

  /**
   * Returns true if the play function is supported by the class.
   */
  @Override
  public boolean isPlayable() {
    return true;
  }

  @Override
  public boolean isModded() {
    return !MODS.isEmpty() || isChanged();
  }

  private String getIndexNotFoundString(int index) {
    String msg = Language.getString("Sfx.13"); //$NON-NLS-1$
    msg = msg.replace("%1", Integer.toString(index)); //$NON-NLS-1$
    return msg;
  }

  public int computeSoundSize() {
    int numSndEntries = MAP.size();
    int size = 0;

    for (int i = 0; i < numSndEntries; i++) {
      if (WAVE.containsKey(i)) {
        Wave w = WAVE.get(i);
        size += w.getDataSize();
      } else {
        SndEntry sus = MAP.get(i);
        size += sus.SIZE;
      }
    }

    return size;
  }

  private class SaveTask extends UEWorker<Boolean> {

    private static final String TmpFileName = "sfx.tmp"; //$NON-NLS-1$

    private final File destFile;
    private final boolean bkp;
    private final String ext;

    private File outFile;

    public SaveTask(File destination, boolean backup, String extension) {
      outFile = destFile = destination;
      this.bkp = backup;
      this.ext = extension;
      RESULT = false;
    }

    @Override
    public Boolean work() throws IOException {
      RESULT = false;
      updateStatus(UEWorker.STATUS_WORKING);

      // create temporary file and backup if destination already exists
      checkDestination();

      if (isCancelled()) {
        // delete temp file created by getWritableLocalFile
        outFile.delete();
        return false;
      }

      if (ADD_CHANGES_2_MODLIST)
        updateModList();

      if (isCancelled()) {
        outFile.delete();
        return false;
      }

      // start write
      try {
        // calculate map address
        sendMessage(Language.getString("Sfx.15")); //$NON-NLS-1$

        // cache mod list data
        byte[] mldata = MODS.toByteArray();
        // cache description data
        byte[] ddata = DESCRIPTION.toByteArray();

        // check settings to write a separate description file
        if (UE.SETTINGS.getProperty(SettingsManager.STORE_SND_DESC).equals(SettingsManager.YES)) {
          String fn = "sfx.dsc"; //$NON-NLS-1$
          try (FileOutputStream gre = new FileOutputStream(FileManager.getWritableLocalFile(fn))) {
            gre.write(ddata);
          }
        }

        // compute sound entry map address
        // with 4 bytes for the written map address
        final int mapAddress = 4 + computeSoundSize() + mldata.length + ddata.length;
        final int numSndEntries = MAP.size();

        //oh joy
        try (FileOutputStream out = new FileOutputStream(outFile)) {
          // write sound entry map address
          out.write(DataTools.toByte(mapAddress, true));

          SndEntry snd = new SndEntry();
          snd.ADDRESS = 0;
          snd.SIZE = 4;

          // write sounds
          for (int i = 0; i < numSndEntries; i++) {
            int value = (int) ((double) (i * 100) / (numSndEntries-1));
            String msg = Language.getString("Sfx.16") //$NON-NLS-1$
              .replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
            feedMessage(msg);

            if (isCancelled()) {
              outFile.delete();
              return false;
            }

            SndEntry snd2 = MAP.get(i);

            //if loaded
            if (WAVE.containsKey(i)) {
              Wave w = WAVE.get(i);
              snd2.SIZE = w.getSize();
              snd2.FRAMES = w.getFrames();
              w.save(out);
            } else {
              // copy original sound effect
              try (FileInputStream in = new FileInputStream(sourceFile)) {
                StreamTools.skip(in, snd2.ADDRESS);
                byte[] b = StreamTools.readBytes(in, snd2.SIZE);
                out.write(b);
              }
            }

            snd2.ADDRESS = snd.ADDRESS + snd.SIZE;
            snd = snd2;
          }

          // write mod list
          SndEntry mlEntry = new SndEntry();
          mlEntry.ADDRESS = snd.ADDRESS + snd.SIZE; //set address
          mlEntry.SIZE = mldata.length;
          out.write(mldata);

          // write descriptions
          SndEntry dscEntry = new SndEntry();
          dscEntry.ADDRESS = mlEntry.ADDRESS + mlEntry.SIZE;
          dscEntry.SIZE = ddata.length;
          out.write(ddata);

          // write sound entries
          for (int i = 0; i < numSndEntries; i++) {
            int value = (int) ((double) (i * 100) / (numSndEntries-1));
            String msg = Language.getString("Sfx.17") //$NON-NLS-1$
              .replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
            feedMessage(msg);
            snd = MAP.get(i);
            snd.save(out);
          }

          // write mod list entry
          mlEntry.save(out);
          // write description entry
          dscEntry.save(out);
        }
      } catch (Exception e) {
        outFile.delete();
        throw e;
      }

      // last chance to cancel action
      if (isCancelled()) {
        outFile.delete();
        return false;
      }

      // replace destination by temporary file
      applyTempFile();

      // write completed, do final cleanup
      WAVE.clear();
      return true;
    }

    private void checkDestination() throws IOException {
      if (!destFile.exists())
        return;

      // create backup
      if (bkp) {
        sendMessage(Language.getString("Sfx.22")); //$NON-NLS-1$

        File file = new File(destFile.getPath() + ext);
        int i = 1;

        while (file.exists()) {
          file = new File(destFile.getPath() + i + ext);
          i++;
        }

        Files.copy(destFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
      }

      String msg = Language.getString("Sfx.12"); //$NON-NLS-1$
      sendMessage(msg.replace("%1", TmpFileName)); //$NON-NLS-1$

      outFile = FileManager.getWritableLocalFile(TmpFileName);
    }

    private void applyTempFile() throws IOException {
      if (outFile == destFile)
        return;

      String msg = Language.getString("Sfx.20"); //$NON-NLS-1$
      sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

      // replace with TMP file
      // on error keep temp file for manual rename
      Files.copy(outFile.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

      msg = Language.getString("Sfx.19"); //$NON-NLS-1$
      sendMessage(msg.replace("%1", outFile.getName())); //$NON-NLS-1$

      // attempt to delete the temporary file
      boolean deleted = outFile.delete();
      if (!deleted) {
        // the save operation succeeded, just the temp file couldn't be removed
        msg = Language.getString(Language.getString("Sfx.21")); //$NON-NLS-1$
        logError(msg.replace("%1", outFile.getName())); //$NON-NLS-1$
      }
    }
  }

  private class ThinkBigTask extends UEWorker<Void> {

    static final int strRIFF = 1179011410;
    static final int strWAVE = 1163280727;
    static final int strFMT = 544501094;
    static final int WAVE_CHUNK_SIZE = 16;
    static final short FORMAT = 1;
    static final short CHANNELS = 1;
    static final int FREQUENCY = 22050;
    static final int BYTES_PER_SECOND = 44100;
    static final short BLOCK_ALIGMENT = 2;
    static final short BITS_PER_SAMPLE = 16;
    static final int strDATA = 1635017060;

    File path;

    public ThinkBigTask(File dest) {
      path = dest;
    }

    @Override
    public Void work() throws IOException {
      int numSndEntries = MAP.size();
      int size = 0;

      for (int i = 0; i < numSndEntries; i++) {
        if (WAVE.containsKey(i)) {
          Wave w = getWave(i);
          size += w.getDataSize();
        } else {
          SndEntry se = MAP.get(i);
          size += se.SIZE;
        }
      }

      try (
        FileOutputStream out = new FileOutputStream(path);
        RandomAccessFile in = new RandomAccessFile(sourceFile, "r");
      ) {
        //RIFF
        out.write(DataTools.toByte(strRIFF, true));
        //size-8
        out.write(DataTools.toByte((size + 52), true));
        //WAVE
        out.write(DataTools.toByte(strWAVE, true));
        //FMT
        out.write(DataTools.toByte(strFMT, true));
        //wave chunk size
        out.write(DataTools.toByte(WAVE_CHUNK_SIZE, true));
        //format
        out.write(DataTools.toByte(FORMAT, true));
        //channels
        out.write(DataTools.toByte(CHANNELS, true));
        //frequency
        out.write(DataTools.toByte(FREQUENCY, true));
        //bytes per sec
        out.write(DataTools.toByte(BYTES_PER_SECOND, true));
        //block aligment
        out.write(DataTools.toByte(BLOCK_ALIGMENT, true));
        //bits per sample
        out.write(DataTools.toByte(BITS_PER_SAMPLE, true));
        //DATA
        out.write(DataTools.toByte(strDATA, true));
        //data size
        out.write(DataTools.toByte(size, true));

        //write data
        for (int i = 0; i < numSndEntries; i++) {
          if (isCancelled()) {
            path.delete();
            return null;
          }

          int value = (int) (i * 100 / (double) (numSndEntries-1));
          String msg = Language.getString("Sfx.14"); //$NON-NLS-1$
          msg = msg.replace("%1", value + "%"); //$NON-NLS-1$ //$NON-NLS-2$
          feedMessage(msg);

          // the sfx entries include the wave header
          // therefore read full wave to extract the actual data
          // otherwise you hear noise at the start of each sound effect
          Wave w = getWave(i);

          // extract pure wave data
          // note that for continuous sounds that abruptly change, it is normal
          // that you still hear some noise by the start/end cuts of the wave form
          byte[] b = w.getRawData();
          out.write(b);
        }
      }

      return null;
    }
  }
}
