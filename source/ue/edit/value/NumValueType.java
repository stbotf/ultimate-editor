package ue.edit.value;

public interface NumValueType extends ValueType {
  boolean isFloatingPoint();
}
