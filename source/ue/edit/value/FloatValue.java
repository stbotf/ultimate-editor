package ue.edit.value;

public interface FloatValue extends NumValueType {

  public static final int VALUE_SIZE = 4;

  @Override
  public default int getSize() {
    return VALUE_SIZE;
  }

  public float floatValue();
  public void  setValue(float value);

  public default void setValue(float value, float tolerableVariance) {
    float val = floatValue();
    if (Math.abs(value - val) >= tolerableVariance)
      setValue(value);
  }

  @Override
  public default Float value() {
    return floatValue();
  }

  public default int getInt() {
    return Float.floatToIntBits(floatValue());
  }

  public default void setInt(int value) {
    setValue(Float.intBitsToFloat(value));
  }

  public default boolean check(int value) {
    return floatValue() == Float.intBitsToFloat(value);
  }

  public default boolean check(float value) {
    return floatValue() == value;
  }
}
