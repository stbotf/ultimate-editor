package ue.edit.value;

import ue.exception.InvalidArgumentsException;

public interface ByteValue extends NumValueType {

  public static final int VALUE_SIZE = 1;

  @Override
  public default boolean isFloatingPoint() {
    return false;
  }

  @Override
  public default int getSize() {
    return VALUE_SIZE;
  }

  public int      byteValue();
  public boolean  setValue(int value) throws InvalidArgumentsException;

  // @return integer value for unsigned byte value segments
  @Override
  public default Integer value() {
    return byteValue();
  }

  public default byte getByte() {
    return (byte) byteValue();
  }
  public default void setByte(byte value) throws InvalidArgumentsException {
    setValue(Byte.toUnsignedInt(value));
  }

  public default boolean check(int value) {
    return byteValue() == value;
  }

  public default boolean check(byte value) {
    return byteValue() == Byte.toUnsignedInt(value);
  }
}
