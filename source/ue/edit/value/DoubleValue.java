package ue.edit.value;

public interface DoubleValue extends NumValueType {

  public static final int VALUE_SIZE = 8;

  @Override
  public default int getSize() {
    return VALUE_SIZE;
  }

  public double doubleValue();
  public void   setValue(double value);

  public default void setValue(double value, double tolerableVariance) {
    double val = doubleValue();
    if (Math.abs(value - val) >= tolerableVariance)
      setValue(value);
  }

  @Override
  public default Double value() {
    return doubleValue();
  }

  public default long getLong() {
    return Double.doubleToLongBits(doubleValue());
  }

  public default void setLong(long value) {
    setValue(Double.longBitsToDouble(value));
  }

  public default boolean check(long value) {
    return doubleValue() == Double.longBitsToDouble(value);
  }

  public default boolean check(double value) {
    return doubleValue() == value;
  }
}
