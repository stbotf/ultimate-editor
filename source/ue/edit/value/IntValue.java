package ue.edit.value;

import ue.exception.InvalidArgumentsException;

public interface IntValue extends NumValueType {

  public static final int VALUE_SIZE = 4;

  @Override
  public default int getSize() {
    return VALUE_SIZE;
  }

  public int      intValue();
  public boolean  setValue(int value) throws InvalidArgumentsException;

  @Override
  public default Number value() {
    if (isFloatingPoint())
      return getFloat();
    return intValue();
  }

  public default float getFloat() {
    return Float.intBitsToFloat(intValue());
  }

  public default void setFloat(float value) throws InvalidArgumentsException {
    setValue(Float.floatToIntBits(value));
  }

  public default void setFloat(float value, float tolerableVariance) throws InvalidArgumentsException {
    float val = getFloat();
    if (Math.abs(value - val) >= tolerableVariance)
      setValue(Float.floatToIntBits(value));
  }

  public default boolean check(int value) {
    return intValue() == value;
  }

  public default boolean check(float value) {
    return getFloat() == value;
  }
}