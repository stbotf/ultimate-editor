package ue.edit.value;

import ue.edit.common.Storable;

public interface ValueType extends Storable {
  Object value();
}
