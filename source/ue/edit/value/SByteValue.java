package ue.edit.value;

import ue.exception.InvalidArgumentsException;

// signed byte value type
public interface SByteValue extends ByteValue {

  @Override
  public default void setByte(byte value) throws InvalidArgumentsException {
    setValue(value);
  }

  @Override
  public default boolean check(byte value) {
    return byteValue() == value;
  }
}
