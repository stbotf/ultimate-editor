package ue.edit.value;

import ue.exception.InvalidArgumentsException;

public interface StringValue extends ValueType {

  @Override
  public String   value();
  public boolean  setValue(String value) throws InvalidArgumentsException;

}
