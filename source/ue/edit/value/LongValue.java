package ue.edit.value;

import ue.exception.InvalidArgumentsException;

public interface LongValue extends NumValueType {

  public static final int VALUE_SIZE = 8;

  @Override
  public default int getSize() {
    return VALUE_SIZE;
  }

  public long longValue();
  public void setValue(long value) throws InvalidArgumentsException;

  @Override
  public default Number value() {
    if (isFloatingPoint())
      return getDouble();
    return longValue();
  }

  public default double getDouble() {
    return Double.longBitsToDouble(longValue());
  }

  public default void setDouble(double value) throws InvalidArgumentsException {
    setValue(Double.doubleToLongBits(value));
  }

  public default void setDouble(double value, double tolerableVariance) throws InvalidArgumentsException {
    double val = getDouble();
    if (Math.abs(value - val) >= tolerableVariance)
      setValue(Double.doubleToLongBits(value));
  }

  public default boolean check(int value) {
    return longValue() == value;
  }

  public default boolean check(double value) {
    return getDouble() == value;
  }
}
