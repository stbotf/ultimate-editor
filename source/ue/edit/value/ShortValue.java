package ue.edit.value;

import ue.exception.InvalidArgumentsException;

public interface ShortValue extends NumValueType {

  public static final int VALUE_SIZE = 2;

  @Override
  public default boolean isFloatingPoint() {
    return false;
  }

  @Override
  public default int getSize() {
    return VALUE_SIZE;
  }

  public short  shortValue();
  public void   setValue(short value) throws InvalidArgumentsException;

  @Override
  public default Short value() {
    return shortValue();
  }

  public default boolean check(int value) {
    return shortValue() == value;
  }
}