#!/bin/bash

SRC="source"
RES="res"
LNG="language.properties"
TMP="lng.tmp"

PROP[1]="language_en.properties"
PROP[2]="language_de.properties"
PROP[3]="language_sl.properties"
PROP_NUM=3

INDEX=0

echo "This will take a while..."

# remove old temporary file
rm -f $TMP

# remove unused entries from language.properties
echo "Searching for unused entries in $LNG..."

while read -r LINE
do
	let INDEX=`expr index "$LINE" =`

	if [ $INDEX -ne 0 ]; then
		let INDEX=INDEX-1
		RESULT=${LINE:0:$INDEX}

		let INDEX=`expr index "$RESULT" .`

		let INDEX=INDEX-1
		FILE=${RESULT:0:$INDEX}

		R=`grep -o -m 1 --include=$FILE.java -r \"$RESULT\" $SRC/* | wc -l`

		if [ $R -ge 1 ]; then
			echo "$LINE" >> $TMP
		else
			echo "Removed $RESULT from $LNG..."
		fi
	else
		# copy over comments etc
		echo "$LINE" >> $TMP
	fi
done < "$RES/$LNG"

# replace old language.properties with the new one
mv $TMP $RES/$LNG

# now do the same to other prop files
for I in `seq 1 $PROP_NUM`; do
	echo "Searching for unused entries in ${PROP[$I]}..."

	rm -f $TMP

	while read -r LINE
	do
		let INDEX=`expr index "$LINE" =`

		if [ $INDEX -ne 0 ]; then
			let INDEX=INDEX-1
			RESULT=${LINE:0:$INDEX}

			R=`grep -o -m 1 "$RESULT=" $RES/$LNG | wc -l`

			if [ $R -ge 1 ]; then
				echo "$LINE" >> $TMP
			else
				echo "Removed $RESULT from ${PROP[$I]}..."
			fi
		else
			# copy over comments etc
			echo "$LINE" >> $TMP
		fi
	done < $RES/${PROP[$I]}

	mv $TMP "$RES/${PROP[$I]}"
done

# add missing entries to prop files
echo "Searching for missing entries in language specific property files..."

while read -r LINE
do
	let INDEX=`expr index "$LINE" =`

	if [ $INDEX -ne 0 ]; then
		let INDEX=INDEX-1
		RESULT=${LINE:0:$INDEX}

		for I in `seq 1 $PROP_NUM`; do

			R=`grep -o -m 1 "$RESULT=" $RES//${PROP[$I]} | wc -l`

			if [ $R -le 0 ]; then
				echo "$LINE" >> $RES//${PROP[$I]}
				echo "Added $RESULT to ${PROP[$I]}..."
			fi
		done
	fi
done < "$RES/$LNG"
