Ultimate Editor is a modding tool for the game "Star Trek: The Next Generation - Birth of the Federation", created and released by DCER under the GPL2 license.

[[_TOC_]]

## Overview

This readme includes instructions to build the Ultimate Editor.\
Details on running the app, you can find in [app/readme](/app/Readme.txt).

For detailed information and support, visit https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=125&t=22 \
Older source and binary releases up to version 0.7.2dev5c you can find at: https://www.4shared.com/dir/248238/25afb1fc/Ultimate_Editor.html

To read, edit, compile and debug the code, I recommend to either install Visual Studio Code or Eclipse. Ofc. there are also another few good editor options, but these both I used myself and can provide some guidance.

## Download

For binaries, check the [release page](https://gitlab.com/stbotf/ultimate-editor/-/releases).\
For the latest source code, either use Git to clone the whole repository with the links listed by the top blue 'Clone' button, or use the 'Download' button next to it.

## VSCode Install

Visual Studio Code from https://code.visualstudio.com/ is a rather lightway but very modern free and open-source based code editor. It isn't specifically made for Java coding, but instead when you open the project folder, it will suggest to install all necessary dependencies. This even includes lombok and gradle, but there are many more good plugins like GitLense you might want to install.

As a fully open-source alternative, you might also check on the VSCodium sibling from https://vscodium.com/ \
It disables telemetry by default, but is told to lack some few market place extensions from Microsoft.

One of the main benefits with Visual Studio Code is the multi-cursor editing. This specially helps when you refactor alot of code.\
It further comes with a rather clean and streamlined interface, where you have easy access to all the relevant stuff. And it has some nice modern additions like a code minimap while also being highly customizable.

To not mess the code, you should however change a few settings to trim excessive spacings and stop inlay hints.\
For that see the detailed build instructions.

<img align="left" src="https://gitlab.com/stbotf/ultimate-editor/-/raw/7de90b5da3df28eb8a8e75d105281750ee85c3cb/img/readme/vscode_gradle_build.jpg" height="350" class="image-left">
<!-- workaround for image margin -->
<img align="left" src="https://gitlab.com/stbotf/ultimate-editor/-/raw/7de90b5da3df28eb8a8e75d105281750ee85c3cb/img/readme/1x1.png" width="40" height="350" class="image-left">

### Short VSCode Build Instructions

1. Download & install Visual Studio Code.
2. Open the UE source code folder or drop it to the editor.
3. Follow the plugin install suggestions to install:
   - 'JDK 8' or newer
   - 'Extension Pack for Java' (beside of all the basic Java stuff, it also includes lombok support)
   - 'Gradle for Java'
   - and optionally 'GitLense', which will also require to install Git
4. Build the project
   - on the left bar, click the elephant icon to open the 'Gradle Tasks' tab
   - double-click the build task
   - wait a few seconds for completion

That's it. Once build, you find the binary distribution zip in build/distributions folder.

<br clear="left"/>

## Eclipse Install

Eclipse is a fully fledged free open-source IDE that is out for long, based on Java and fully committed to Java. You can get it from https://www.eclipse.org/downloads/ \
It comes bundled with a JDK (Java Development Kit) as well as the Gradle Plugin and alot of other good stuff.

Compared to other modern IDEs, you however will notice that the software is a bit aged. The menus are all overloaded, there is no multi-cursor support, no code minimap, and the build setup and configuration is a bit more complex.

On install it will ask you to set your default working directory, which is where new projects are placed by default. But this step is not relevant and the working directory can still be changed later.

In case you are not used to programming and never have seen an IDE like Eclipse, don't panic by the many options, just follow below steps and you are fine.

### Short Eclipse Build Instructions

1. Download & install Eclipse.
2. Import the ultimate-editor project
   - select File -> Import
   - run 'Existing Gradle Project' import wizard\
     ![import wizard](img/readme/import.jpg)
   - follow default steps
3. Install the Lombok plugin to your IDE
   - follow installation instructions on the [official website](https://projectlombok.org/setup/overview)
4. Build the project
   - in the middle bottom open the 'Gradle Tasks' tab\
     ![Gradle Build](img/readme/eclipse_gradle_build.jpg)
   - double-click the build task
   - wait a few seconds for completion

That's it. Once build, you find the binary distribution zip in build/distributions folder.

### Eclipse Project File Updates

JUST ONE SMALL HINT: For Eclipse, when you pull new changes and run into build errors, or when you modify the gradle build scripts, you might have to run 'Refresh Gradle Project' for Eclipse to recognize any project changes like changed library dependencies.

![Gradle Build](img/readme/refresh_project.jpg)

## Custom Build & Build Details

To not confuse on side details and optional tasks or build setup and configuration, if interested, check the [detailed build instructions](Detailed_Build_Instuctions.md).
