These are the detailed build instructions with additional side information and optional build configuration steps. For quick steps on building the app, check the [main readme](Readme.md).

[[_TOC_]]

## Build System

Former releases of the Ultimate Editor source included hand-crafted build scripts for both windows and linux.\
They are now replaced by the Gradle build system, which is an evolution on the Maven and the Ant build system, and both speeds incremental builds and has a more flexible but comprehensible syntax.

The main use of a build system is to fully automate all build steps, download all the needed dependencies, and be independent on whatever IDE (Independent Development Environment) you prefer. In addition it includes alot of useful tooling and allows for a much less sophisticate build configuration.

## VSCode Build

Visual Studio Code is a very customizable modern cross-platform code editor that works very well with UE.
Compared to Eclipse the best benefit to me is the multi-cursor editing ability. It further doesn't come packaged with a JDK and stuff, but when opening the project folder it automatically detects the necessary dependencies and suggests to install them.

### Setup

While the basic setup is straight forward and probably all auto-detected, and even the Lombok support comes with the auto-suggested 'Extension Pack for Java', make sure to have the Gradle for Java plugin installed. Another very recommendable plugin is GitLens to view and compare the file change history.

To not mess your commits, in user settings you should configure:
```
    "files.trimTrailingWhitespace": true,
    "files.trimFinalNewlines": true,
    "editor.inlayHints.enabled": "offUnlessPressed",
```

For debugging, further configure the skipClasses user setting to skip unrelevant internal JDK exceptions:
```
"java.debug.settings.exceptionBreakpoint.skipClasses": [
    // selectively ignore exceptions thrown from the specified packages and classes
    // which gives more granular debug control over the global "$JDK" ignore
    "java.io.*",
    "java.lang.Class*",
    "java.lang.invoke.*",
    "java.lang.Integer",
    "java.lang.Long",
    "java.net.*",
    "java.security.*",
    "java.util.concurrent.*",
    "java.util.ResourceBundle",
    "javax.crypto.*",
    "com.sun.*",
    "com.google.*",
    "org.junit.*",
    "sun.*",
],
```
<details>
<summary markdown="span">A more detailed list would be:</summary>
```
"java.debug.settings.exceptionBreakpoint.skipClasses": [
    // app load
    "java.io.FileInputStream",                                  // throws java.io.FileNotFoundException
    "java.lang.Class",                                          // throws java.lang.ClassNotFoundException + NoSuchMethodException
    "java.lang.ClassLoader",                                    // throws java.lang.ClassNotFoundException
    "java.lang.invoke.MemberName*",                             // throws java.lang.NoSuchMethodException
    "java.lang.invoke.MethodHandleNatives",                     // throws java.lang.NoSuchFieldError
    "java.net.URLClassLoader",                                  // throws java.lang.ClassNotFoundException
    "java.security.AccessController",                           // throws java.security.PrivilegedActionException
    "sun.misc.URLClassPath*",                                   // throws java.io.FileNotFoundException
    "sun.reflect.misc.MethodUtil",                              // throws java.lang.ClassNotFoundException
    // file load
    "java.lang.Integer",                                        // throws java.lang.NumberFormatException
    "sun.misc.FloatingDecimal",                                 // throws java.lang.NumberFormatException
    "sun.nio.fs.WindowsException",                              // throws java.nio.file.NoSuchFileException
    "sun.nio.fs.WindowsNativeDispatcher",                       // throws sun.nio.fs.WindowsException
    // file open dialog
    "java.util.concurrent.FutureTask",                          // throws java.lang.InterruptedException
    "java.util.concurrent.locks.AbstractQueuedSynchronizer",    // throws java.lang.InterruptedException
    "sun.awt.shell.ShellFolder",                                // throws java.lang.InterruptedException
    "sun.awt.shell.Win32ShellFolderManager2*",                  // throws java.lang.InterruptedException
    // internal file hex copy
    "java.util.ResourceBundle"                                  // throws java.util.MissingResourceException
    // network
    "java.net.NetworkInterface*",                               // throws java.util.NoSuchElementException
    // update check
    "java.lang.Long",                                           // throws java.lang.NumberFormatException
    "java.security.Signature",                                  // throws java.security.NoSuchAlgorithmException
    "javax.crypto.Cipher",                                      // throws java.lang.IllegalStateException
    "com.sun.crypto.provider.CipherCore",                       // throws java.lang.IllegalStateException
    "com.sun.crypto.provider.GaloisCounterMode",                // throws javax.crypto.AEADBadTagException
    "com.sun.org.apache.xerces.internal.impl.XMLEntityScanner", // throws XMLEntityScanner$1
    "sun.security.x509.CertificateExtensions",                  // throws java.io.IOException
    "sun.security.x509.X509CertImpl",                           // throws java.security.cert.CertificateParsingException
    // unit tests
    "java.net.SocketInputStream",                               // throws java.net.SocketException
    "java.net.URI*",                                            // throws java.net.URISyntaxException
    "java.net.URL",                                             // throws java.net.MalformedURLException
    "com.google.common.jimfs.DirectoryEntry",                   // throws java.nio.file.NoSuchFileException
    "com.google.common.jimfs.FileSystemView",                   // throws java.nio.file.NoSuchFileException
    "org.junit.platform.commons.function.Try*",                 // throws java.lang.ClassNotFoundException
    "sun.nio.cs.StreamDecoder",                                 // throws java.net.SocketException
],
```
</details>

And possibly also disable the many audioCues.* sound notifications.

### Build

The project build command you find in 'Terminal'->'Run Build task...' menu. But you can as well just run or debug the application and it will build automatically.

For the gradle build and deployment on the left bar you find a little elephant for the gradle build menu.
No special imports needed.

## Eclipse Build

### Import

Once installed, go to File -> Import and import the UE project with the 'Existing Gradle Project' import wizard, which is listed under the Gradle folder. Just follow the default steps:\
![import wizard](img/readme/import.jpg)

This should open the ultimate-editor project on the left tab in either the Package Explorer or the Project Explorer view.\
If right clicking the project folder there, in the Gradle sub-menu you find 'Refresh Gradle Project', an option that you always have to perform when you changed the gradle build project 'build.gradle' to refresh eclipse for it. Doesn't hurt to refresh it now, even though it should all be updated by the import already.\
![import wizard](img/readme/refresh_project.jpg)

### Lombok plugin

Some project code uses the Lombok library which requires an additional setup step to the standard Gradle import.
Lombok is used to reduce boilerplate code and Java source code verbosity so that the developers can focus on the parts of the code that matter most.
For a more detailed explanation on what the library does, and the installation instructions for your IDE check out the [Lombok project website](https://projectlombok.org/).

If you do not set up Lombok in your IDE, your IDE will be unable to properly validate the code and will likely show errors in the project where there aren't any.

### Build

At the middle bottom you find a Gradle Tasks tab. Expand the ultimate-editor folder and you find the available tasks to run:\
![Gradle Build](img/readme/eclipse_gradle_build.jpg)

If at any time you added a gradle task or some is missing, in the menu bar to the right you find a 'Refresh Tasks for All Projects' button that updates the task list. Further, to the right of it in the dotted menu, you can check 'Show all Tasks' cause by default only those assigned to a group are visible.\
![import wizard](img/readme/refresh_tasks.jpg)

In build folder then double-click the build task. This should auto-resolve all dependencies, build the project and even package the distribution files.

Note that this is not the common way you build Eclipse projects. When you use the normal project build options from menu, Eclipse uses its own build setup and compiles to the 'bin' sub-folder of the project directory, instead of the 'build' folder used by Gradle. And while the Gradle build also packs all the help files and stuff and even bundles distribution ready jar and zip files, the Eclipse build just includes a basic compile step of all the classes.

That option was not replaced by the Gradle plugin for compatibility reasons. Instead Gradle configures the Eclipse project so it includes all dependencies from Gradle, but works independent from Gradle. And when you run the Gradle build task, the project is build twice - both with Eclipse and with Gradle. This might change with some future release, but for now the Eclipse build is mostly for error listing and debugging, while the Gradle build is for distribution ready release builds.

## Custom Build

### Build Environment

All you need for your build setup is a JDK (Java Development Kit) of at least version 8.

If using Eclipse, you might skip to install a JDK cause it comes bundled with a JDK. That case, for testing and using the Ultimate Editor, I however recommend to at least install a Java Runtime Environment e.g. from https://java.com/

Alternatively you can either grab:
- the latest JDK from Oracle https://www.oracle.com/java/technologies/javase-downloads.html
  which by now is limited to non-commercial use
- the latest OpenJDK from oracle http://jdk.java.net/
  which is the official open source branch from oracle
- the latest AdoptOpenJDK / Eclipse Adoptium from https://adoptopenjdk.net/
  which likely is the most common alternative OpenJDK based source
- or some other OpenJDK based JDK from the many other distributors\
Note that with later releases you can still build for Java 8 runtime environment compatibility.

### Gradle Build System

To manually build with gradle, you might configure your JAVA_HOME path by pointing the environment variable to your installed JDK. All other dependencies, including Gradle itself, are downloaded by the gradlew.bat, or (for linux) gradlew.sh script.

To install the configured gradle version, call the gradle wrapper script with `gradlew.bat wrapper`.\
To build call `gradlew.bat build`. For detailed gradle commands refer `gradlew.bat -h`.

## Distribution

### Language file cleanup

In the project root directory, you find a 'clean_properties.sh' script, that can be used to identify and clean up obsolete text and translation mappings from the 'language.properties' files located in the 'res' folder. It detects and removes all entries missing from the corresponding .java files and afterwards appends those to the 'language.properties' main file, that have been found in the .java files, but have not been listed by the text mappings.

This script however has false detection when you deviate from the naming conventions and the text mapping names don't match with the .java file names. So you better check the changes it applied and search for removed values.

### Testing

When the app is build, in the Gradle Tasks tab, open the 'application' folder and double-click the 'run' task to check that the app is working.

Further, when you right click some project file in the left Package- or Project Explorer, and select
* VSCode: 'Reveal in File Explorer'
* Eclipse: 'Show In' -> 'System Explorer'
it opens up the usual windows explorer where in the project root folder you have a run.bat and a run.sh script you can execute to launch the app.

In both cases it changes the working directory to the 'build/runtime' folder, where all the required runtime files are being copied by the build, and then runs the build 'UE.jar' file from 'build/libs'.

But you can ofc also just unzip and run the zipped UE distribution from 'build/distributions' folder.

### Distribution

Building the app, it already packs a distribution ready 'UE.zip' file to 'build/distributions' folder.\
You however can also update the 'app' files to include and re-run the 'distApp' task you find in distribution folder of the Gradle Tasks tab.
