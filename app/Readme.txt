Introduction
============

Ultimate Editor is a modding tool for the game "Star Trek: The Next Generation - Birth of the Federation", created and released by DCER under the GPL2 license.

For detailed information and support, visit https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=125&t=22
For bug reports, please include the error.log and a description of the bug.

For binary releases and the source code, visit https://gitlab.com/stbotf/ultimate-editor


Installation and Usage
======================

-- Java Runtime Environment --

To run the editor, you need to have the Java Runtime Environment version 8 or above installed.
The most recent recommended end user distribution you find on https://www.java.com/
By the time of writing (13.04.2021), this is still version 8.

More recent SE versions you can find on https://www.oracle.com/java/technologies/java-se-glance.html,
they however lack features partially required by other old java software. And beside version 8, there is only version 11 and the not yet released version 17 with long time support, check the support roadmap at https://www.oracle.com/java/technologies/java-se-support-roadmap.html

-- Install & Run --

Beside Java, the binary distribution of Ultimate Editor includes all files needed to run the application.
Just unzip the files to whatever location you like.

To run the application, double click on UE.jar (or UE if Windows hides the extension).

UE currently opens stbof.res, alt.res, trek.exe, botf's *.snd files and saved games. After opening one of these, additional commands will apear under the Edit menu.

Don't forget to record and save your changes. UE is always meant to be a beta version and if used wrong, it may easily corrupt your game files!

-- Troubleshooting --

In case you encounter some 'OutOfMemoryError' exception, try execute UE with the included run.bat startup script.

It runs the app with the additional '-Xmx2048m' flag, to set the max allowed heap memory to 2048MB.
Your system default depends on the installed system memory size and the Java version used. Typically it is set to 25% of your system memory size. You can determine the default by executing "java -XX:+PrintFlagsFinal -version | findstr HeapSize" in the command shell.
